--
-- Share nvim libs to outside using
--
-- Helper to run test outside nvim with part of it functionaliti
-- binds function from: vim
-- /usr/share/nvim/runtime/lua/vim/shared.lua
--
-- 17-05-2023 @author Swarg
--
-- package.path = package.path .. ';./lua/?.lua'
-- require 'busted.runner' ()

local M = {}

-- if this code launched outside neovim try to find source code of nvim
if not vim then
  package.path = package.path .. ';/usr/share/nvim/runtime/lua/?.lua'
  vim = vim or require('vim.shared')              -- define Global vim vriable
  vim.inspect = vim.inspect or require('inspect') -- from system, installed by luarocks
  vim.api = vim.api or {}                         -- C ?
  vim.json = vim.json or require('cjson')
  vim.wait = M.wait
  -- for launch alogger outside nvim
  ---@diagnostic disable-next-line: duplicate-set-field
  vim.in_fast_event = function() return false end
  ---@diagnostic disable-next-line: unused-local, duplicate-set-field
  vim.notify = function(msg, log_level, notify_opt)
  end
  --? local assert = require('luassert') -- used by busted has in nvim +
end

-- print('Single') print(vim.inspect(vim))

function M.init()
  --
end

-- sleep for given seconds
-- See :h wait
function M.wait(time, fcondition)
  local duration = os.time() + time
  while os.time() < duration do
    if type(fcondition) == 'function' then -- TODO check impl
      if not fcondition() then
        return
      end
    end
  end
end

return M

-- For adding 3rd-party library to current 'class path'(java term) or to
-- namespaces that will be seen by lsp_lua you need:
-- Add to lsp-settings for 'lua' lsp, for example in config it can be here:
-- ~/.config/nvim/lua/user/lsp/settings/lua_ls.lua
-- add to 'settings.Lua.workspace.library' table, like this:
-- library = {
--   [vim.fn.expand "$VIMRUNTIME/lua"] = true,
--   [vim.fn.stdpath "config" .. "/lua"] = true,
--   ["${3rd}/busted/library"] = true,            -- Add Libraries installed
--   ["${3rd}/luassert/library"] = true,          -- in system by luarocks
-- },
--
-- ~/.config/nvim/lua/user/lsp/mason.lua :
-- lspconfig = require'lspconfig'
-- opts = {on_attach = .., capabilities = ...,}
--
-- lspconfig['lua_ls'].setup(opts)
--                            ^^--table with settings.Lua/workspace.library ..
--

-- 16-05-2023 @author Swarg
local M = {}

---@diagnostic disable-next-line
local su = require('env.sutil')
local bu = require('env.bufutil')
local u8 = require("env.util.utf8")
local parser = require("env.util.code_parser")
local cu = require("env.util.commands")
---@diagnostic disable-next-line
local log = require('alogger')
local c4lv = require("env.util.cmd4lua_vim")


-- cmds: apply-escapes|fold|slash|split|wrap-words|key
-- Action with current line under the cursor inside a buffer
-- Implemented:
-- fold - break big string to small lines with given length ( <= 80 )
--
function M.reg_cmd_EnvLine()
  cu.reg_cmd("EnvLine", function(opts)
    local w = c4lv.newCmd4Lua(opts)
        :root(':EnvLine')
        :about('Actions with current line')

    if w:is_cmd('fold', 'f') then
      M.fold_current_line(w)
      -- fix slash '\' -> '/' win to unix and virse versa
    elseif w:is_cmd('slash', '/') then
      M.current_line_change_slashes(w)
      -- '\n' -> to new lines
    elseif w:is_cmd('apply-escapes', 'ae') then
      M.curr_line_apply_escapes(w)
      --
    elseif w:is_cmd('split', 's') then -- split line to multilines
      M.current_line_split(w)
      --
    elseif w:is_cmd('wrap-words', 'ww') then
      M.current_line_wrap_words(w) -- "abc def g" -> 'abc', 'def', 'g'
      --
    elseif w:is_cmd('debug-print', 'dp') then
      M.current_line_debug_print(w) -- "k1, k2" -> print('[DEBUG]', 'k1:', k1, 'k2:', k2)
      --
    elseif w:is_cmd('key', 'k') then
      M.current_line_as_key(w) -- "word" -> "['word'] = true,"
      --
    elseif w:is_cmd('convert', 'c') then
      M.cmd_convert_line(w)
    end

    w:show_help_or_errors()
  end, { nargs = '*' })
end

--
---@param w Cmd4Lua
function M.cmd_convert_line(w)
  w:about('Convert')
      :desc('datetime epoch to readable datetime')
      :cmd('epoch2date', 'e2d', M.subcmd_epoch2date)

      :desc('date(from http-header) to epoch')
      :cmd('hdate2epoch', 'hd2e', M.subcmd_hdate2epoch)

      :desc('datetime (Y-M-DTh:m:s.msZ) to epoch')
      :cmd('date2epoch', 'd2e', M.subcmd_date2epoch)

      :run()
end

local function clean_comment(s)
  if string.match(s, "^%-%-") then
    s = string.gsub(s, "^%-%-%s*", "")
  end
  return s
end

--
-- insert given line before current line-num in current or given buffer
---@param line string|table|any multilines is not supported
---@param bufnr number? default is a current(0)
local function insert_or_print(line, bufnr)
  local A = vim.api
  bufnr = bufnr or 0
  if A.nvim_buf_get_option(bufnr, 'modifiable') then
    local lnum = (A.nvim_win_get_cursor(0) or {})[1] or 1
    local lines
    local lt = type(line)
    if lt == 'table' then
      lines = line
      --
    elseif lt == 'string' and line:find("\n") then
      lines = {}
      for line0 in line:gmatch("([^\n]*)\n?") do
        if line0 then table.insert(lines, line) end
      end
    else
      lines = { tostring(line) }
    end
    local ok, err = pcall(A.nvim_buf_set_lines, 0, lnum - 1, lnum - 1, true, lines)

    if ok then
      return
    else
      print(err)
    end
  end

  print(line)
end


-- 1705971789 --> 2024-01-23T04:03:09.0Z
---@param w Cmd4Lua
function M.subcmd_epoch2date(w)
  local epoch = w:desc('seconds from epoch (def currline)')
      :def(vim.api.nvim_get_current_line()):opt('--value', '-v')

  local fmt = w:desc('output format'):def('%Y-%m-%dT%H:%M:%S.0Z')
      :opt('--format', '-f')

  if not w:is_input_valid() then return end
  local n = tonumber(clean_comment(epoch))
  if not epoch then
    print('expected number got: ', epoch)
  else
    insert_or_print(os.date(fmt, n))
  end
end

--
local function get_www_auth()
  local ok, www_auth = pcall(require, 'www-auth')
  if not ok then
    print('luarock "www-auth" not installed')
    return {}
  end
  return www_auth
end

--
-- date from http-header
-- date: Tue, 23 Jan 2024 04:03:09 GMT  -->  1705971789
---@param w Cmd4Lua
function M.subcmd_hdate2epoch(w)
  local dt = w:desc('datetime from http head to epoch seconds')
      :def(vim.api.nvim_get_current_line()):opt('--value', '-v')

  if not w:is_input_valid() then return end

  dt = clean_comment(dt)
  if string.match(dt, "^%s*date:") then
    dt = string.gsub(dt, "^%s*date:%s*", "")
  end
  local ok, out = pcall(get_www_auth().parse_data_header, dt)
  if not ok then
    print('Cannot parse date: "' .. dt .. '"')
  else
    insert_or_print(out)
  end
end

--
-- 2024-01-23T04:03:09.0Z  -->  1705971789
---@param w Cmd4Lua
function M.subcmd_date2epoch(w)
  local dt = w:desc('datetime to parse (default from current line)')
      :def(vim.api.nvim_get_current_line()):opt('--value', '-v')

  if not w:is_input_valid() then return end

  dt = clean_comment(dt)

  local ok, out = pcall(get_www_auth().parse_string_data, dt)
  if not ok then
    print('Cannot parse date: "' .. dt .. '"')
  else
    insert_or_print(out)
  end
end

-- Make lua debug.traceback more readable
-- Remove project_root/src from each trace-entry
---@param traceback string
---@param ctx table (module, function, project)
---@return table<string>
function M.fancy_traceback(ctx, traceback)
  if traceback then
    local lines = vim.split(traceback, '\n', { trimempty = false })
    if ctx and ctx.module and ctx.project and ctx.project.project_root then
      -- /full/path/to/proj-name --> /projname
      local proj_src = ctx.project.project_root:match('.*(/[^/]+)')
      if proj_src then
        proj_src = proj_src .. '/' .. ctx.project.src .. '/'
        local repl_pattern = '%.%.%.[%w_%-%/]+' .. su.regex_escape(proj_src)
        for i, line in pairs(lines) do
          lines[i] = string.gsub(line, repl_pattern, '')
        end
      end
    end
    return lines
  end
  return {}
end

-- break one long line into several small ones that fit in the given width(80)
---@param w Cmd4Lua
function M.fold_current_line(w)
  local desc = 'max charecters number per one line'
  local width = w:def(80):optn('--width', '-w', desc)

  if w:is_input_valid() then
    local cbi = bu.current_buf_info()
    if cbi.can_edit and cbi.current_line then
      local row = cbi.cursor_row
      ---@cast width number
      -- local lines = su.fold_line(cbi.current_line, width)
      local lines = u8.utf8_fold(cbi.current_line, width)
      vim.api.nvim_buf_set_lines(0, row - 1, row, true, lines)
    end
  end
end

---@param w Cmd4Lua
---@diagnostic disable-next-line: unused-local
function M.curr_line_apply_escapes(w)
  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row = cbi.cursor_row
    local lines = su.apply_escapes(cbi.current_line)
    vim.api.nvim_buf_set_lines(0, row - 1, row, true, lines)
  end
end

--  for fast convertation win paths to unix
---@param w Cmd4Lua
function M.current_line_change_slashes(w)
  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row = cbi.cursor_row
    local line

    w:about('Change slashes in the current line')

    if w:is_cmd('unix', 'u') then
      line = string.gsub(cbi.current_line, '\\', '/')
    elseif w:is_cmd('win', 'w') then
      line = string.gsub(cbi.current_line, '/', '\\')
    end

    if w:is_input_valid() then
      if line then
        vim.api.nvim_buf_set_lines(0, row - 1, row, true, { line })
      end
    end
  else
    print('Cannot edit this buffer')
  end
end

-- EnvLine split <cmd>
-- fast split line into multiple lines
-- cmd -  for split too long bash command into multiline cmd
---@param w Cmd4Lua
function M.current_line_split(w)
  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row = cbi.cursor_row
    local lines

    w:about('Split current line')

    if w:desc('bash cmd with "\" in the ends of lines')
        :is_cmd('bash-cmd', 'bc') then --
      local line = string.gsub(cbi.current_line, ' ', ' \\\n  ')
      lines = su.split_range(line, "\n")
      --
      -- "some\nstring" --> "some\n" ..(newline) "string"  keep \n
    elseif w:desc('make a long-line in source code readable')
        :is_cmd('code-string', 'cs') then
      lines = parser.split_code_string_to_multilines(cbi.current_line)
      --
    elseif w:desc('split one-line to multilines by given sep (support quotes)')
        :is_cmd('by-separator', 'bs') then
      lines = M.cmd_split_by_separator(w, cbi.current_line)
      --
    elseif w:desc('format json line by jq')
        :is_cmd('json', 'j') then
      lines = M.cmd_format_json_line(w, cbi.current_line)
      --
    elseif w:desc('split line by given anchor word')
        :is_cmd('anchor ', 'a') then
      lines = M.cmd_split_liny_by_anchor_word(w, cbi.current_line)
    end

    if w:is_input_valid() then -- print autohelp if needed
      if lines then
        vim.api.nvim_buf_set_lines(0, row - 1, row, true, lines)
      end
      return lines
    end
  end
end

--
-- EnvLine split bt-separator [OPTS]
-- smart split by separator and an groups supporting
--
---@param w Cmd4Lua
---@param line string
function M.cmd_split_by_separator(w, line)
  local sep = w:desc('A char separator used to split line')
      :def(','):opt('-s', '--separator')
  local unescape = w:desc('unescape \"key\" -> "key"')
      :has_opt('--unescape', '-u')
  local keep_groups = w:desc('Smart split, keep groups like [({"\'')
      :has_opt('--groups', '-g')
  local no_trim_spaces = w:desc('Not trim spaces')
      :has_opt('--no-trim', '-n')
  local unquote = w:desc('Unquote result lines "a b", "e" -> a b\ne ')
      :has_opt('--unquote', '-u')


  if w:is_input_valid() then
    if sep == '\t' or sep == 'tab' then sep = "\t" end
    if not sep or #sep > 1 then
      print('Separator should be a char. String separator not supported.')
      return false
    end

    local lines = nil
    if unescape and line then
      line = su.unescape0(line)
    end
    if keep_groups then
      -- local D = require("dprint")
      -- D.enable_all_modules(false) -- of tokenizer used in cmd4lua
      -- D.enable_module(su, true)
      -- log.debug(vim.inspect(D.status()))
      lines = su.split_groups(line, sep, 1, #line, not no_trim_spaces)
    else
      lines = su.split(line, sep)
    end

    if unquote and lines then
      for i = 1, #lines do
        local line0 = lines[i]
        if string.match(line0, '^%s+["\']') then
          line0 = string.match(line0, "^%s+(.*)%s*$")
        end
        local fc = line0:sub(1, 1)
        if fc == '"' or fc == "'" then
          if line0:sub(-1, -1) == fc then
            line0 = line0:sub(2, -2)
          end
        end
        lines[i] = line0
      end
    end
    return lines
  end
end

---@param w Cmd4Lua
---@param line string
function M.cmd_format_json_line(w, line)
  local unescape = w:desc('\"key\" -> "key"'):has_opt('--unescape', '-u')

  if w:is_input_valid() then
    if unescape then
      line = string.gsub(line, '\\"', '"')
    end
    local fs = require('env.files')
    local lines = fs.execr("echo '" .. line .. "' | jq")
    if lines then
      return su.split_range(lines, "\n")
    end
  end
  return nil
end

---@param w Cmd4Lua
---@param line string
function M.cmd_split_liny_by_anchor_word(w, line)
  local anchor = w
      :desc('the anchor word befor which to insert a line separator'):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast anchor string

  local t, p, anchor_len, sub = {}, 1, #anchor, string.sub

  while true do
    local i = string.find(line, anchor, p, true)
    if not i then
      t[#t + 1] = sub(line, p - anchor_len, #line)
      break
    end
    local s = sub(line, p - anchor_len, i - 1)
    if #s > 0 then
      t[#t + 1] = s
    end

    p = i + #anchor
  end
  return t
end

-- EnvLine wrap-words
-- wrap each word in line to quotes
-- "abc def g" -> 'abc', 'def', 'g'
function M.current_line_wrap_words(w)
  w:about('Wrap each word in line to quotes')

  local double = w:desc('use for wrapping a double quotes')
      :has_opt('--double', '-d')
  local q = w:desc('use " for wrapping, \' is used by default')
      :def("'"):optional():pop():arg()
  local lua_table = w:desc('wrap to luatable')
      :has_opt('--lua-table', '-t')

  if not w:is_input_valid() then return end

  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row, line = cbi.cursor_row, ''
    if double then q = '"' end

    for word in cbi.current_line:gmatch('([^%s]+)') do
      if #line > 0 then line = line .. ', ' end
      line = line .. q .. word .. q
    end
    if line and line ~= '' then
      if lua_table then
        line = '{ ' .. line .. ' }'
      end
      vim.api.nvim_buf_set_lines(0, row - 1, row, true, { line })
    end
  end
end

-- EnvLine debug-print
-- "k1, k2" -> print('[DEBUG]', 'k1:', k1, 'k2:', k2)
function M.current_line_debug_print(w)
  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row, q, line, sep, desc = cbi.cursor_row, "'", '', ',', nil
    w:about('Build debug print for variables')

    desc = 'use " for wrapping, \' is used by default'
    if w:has_opt('--double', '-d', desc) then
      q = '"'
    end

    desc = 'use " for wrapping, \' is used by default'
    sep = w:def(','):opt('--separator', '-s', desc)

    if w:is_input_valid() then
      line = 'print(\'[DEBUG] \','
      local vns = su.split_range(cbi.current_line, sep, 1, #cbi.current_line)
      if not vns then
        return print('Cannot split current line')
      end
      for _, vn in pairs(vns) do
        line = line .. q .. vn .. ':' .. q .. ', ' .. vn .. ', '
      end
      line = line:sub(1, #line - 2) .. ')'
      if line and line ~= '' then
        vim.api.nvim_buf_set_lines(0, row - 1, row, true, { line })
      end
    end
  end
end

-- :EnvLine key
-- "word" -> "['word'] = true,"
function M.current_line_as_key(w)
  local cbi = bu.current_buf_info()
  if cbi.can_edit and cbi.current_line then
    local row, q, desc, line = cbi.cursor_row, "'", nil, nil
    w:about('Convert word to a lua table key')

    local value = w:desc('value of the key. "true" is default')
        :def('true'):opt('true', '--value', '-v', desc)

    if tonumber(cbi.current_line) then
      q = '' -- 0 --> [0]  not ['0']
    end

    if w:is_input_valid() then
      line = '[' .. q .. cbi.current_line .. q .. '] = ' .. value .. ','
      vim.api.nvim_buf_set_lines(0, row - 1, row, true, { line })
    end
  end
end

function M.get_oneline_comment(ext)
  if ext == 'lua' then
    return '--'
  elseif ext == 'c' then
    return '/* */' -- ansic
  elseif ext == 'sh' then
    return '#'
    --
    -- ; ?
  else -- other
    return '//'
  end
end

-- value -> 'value'
-- ['value'] -> ['value']
-- 'value' -> 'value'
function M.mk_tbl_value(s, q)
  if tonumber(s) then
    return s
  end
  local c = s:sub(1, 1)
  if c ~= '\'' and c ~= '"' and c ~= '[' then
    q = q or '\''
    return q .. s .. q
  end
  return s
end

function M.mk_tbl_key(s)
  local need_wrap = false
  if tonumber(s) then
    need_wrap = true
  else
    local c = s:sub(1, 1)
    if c == '\'' or c == '"' then
      need_wrap = true
    elseif string.find(s, ' ', 1, true) then -- case:  ab cd --> ['ab cd']
      need_wrap = true
      s = "'" .. s .. "'"
    end
  end
  if need_wrap then
    return '[' .. s .. ']'
  end
  return s
end

return M

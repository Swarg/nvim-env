-- Configuration and the tools to change it on the fly
--
-- Commands Registration
local M = {}

local api = vim.api
local ui = require('env.ui')
-- local fs = require('env.files')
local bu = require('env.bufutil')
local cu = require("env.util.commands")
local log = require('alogger')
local lplugins = require('env.plugins')
local lpackages = require('env.packages')
local research = require('env.research')
local sourcecode = require("env.sourcecode")
local nvim_tree = require 'env.bridges.nvim-tree'


local _config = {};
local LAST_OPENED_FILENAMES_DUMP = "/tmp/nvim-last-opened"

-- merge "default" config with given
---@param config table|nil
function M.init(config)
  -- nil - valid value - set default
  if config and type(config) ~= "table" then
    ui.echohl('Invalid config type ' .. vim.inspect(config), "Error")
    return false
  end
  _config = config or {}

  -- default terminal for open an os comands in the external window
  _config.unix_term = '/usr/bin/x-terminal-emulator'

  -- _config = vim.tbl_deep_extend("force", {}, config or {})
  _config.hotswap = false -- (in gradle: --continuous)

  _config.last_opened = _config.last_opened or LAST_OPENED_FILENAMES_DUMP
  _config.hotswap = _config.hotswap or false -- apply classes changes (jvm)
  _config.no_daemon = _config.no_daemon or false

  -- logging
  _config.logger = _config.logger or {}
  -- _config.logger.level = log.levels.DEBUG
  -- `ALOGGER_DEBUG=1 NVIM_CONF_DEBUG=1 nvim file.ext`
  _config.logger.level = os.getenv('ALOGGER_DEBUG') == '1' and log.levels.DEBUG or nil
  _config.logger.appname = 'env'
  _config.logger.app_root = lplugins.self_root() -- TODO more smart way!
  _config.logger = log.setup(_config.logger)
  -- user commands
  if _config.add_commands ~= false then
    M.add_commands()
  end
  if _config.no_nvim_tree_integration ~= true then
    nvim_tree.setup_integration()
  end
end

---@private
function M._get_config()
  return _config
end

-- return copy of config ?
function M.get_extended_config(opts)
  opts = opts or {}
  opts = vim.tbl_deep_extend("force", vim.deepcopy(_config), opts)
  return opts
end

-- aka Plugin Status or check health
function M.show_info_wnd()
  print("TODO")
end

-- for changing the config on the fly
---@param key string
---@param value string
function M.set_property(key, value)
  key = key or "ls"
  local msg
  if key == "ls" or key == "" then
    msg = "list of possible keys: "

    -- set clean task for cleaning before run|test compile
  elseif key == "clean-task" then
    _config.clean_task = value == "true"
    msg = key .. " " .. tostring(_config.clean_task)

    -- for on/off hotswap for java: auto apply class changes in jvm
    --   in gradle for jvm implemened by --continuous flag
  elseif key == "hotswap" then
    _config.hotswap = value == "true"
    msg = key .. " " .. tostring(_config.hotswap)
    --
  elseif key == "no-daemon" then
    _config.no_daemon = value == "true"
    msg = key .. " " .. tostring(_config.no_daemon)
    --
  elseif key == "last-opened-path" then
    _config.last_opened = value or LAST_OPENED_FILENAMES_DUMP
    msg = key .. " " .. tostring(_config.last_opened)
  else
    msg = "Uknown key: " .. tostring(key) -- vim.inspect(key) strange error!
  end
  ui.echohl(msg)
end

-- change the plugin config properties on the fly
-- props used for DAP and debugging code
local function reg_cmd_EnvSet()
  cu.reg_cmd('EnvSet', function(opts)
      ---@diagnostic disable-next-line: deprecated
      M.set_property(unpack(cu.fargs_safely(opts)))
    end,
    -- complete
    {
      nargs = "*",
      complete = function()
        --print(o) --ui.echohl(vim.inspect(o))
        return vim.tbl_keys({
          clean_task = "clean-task",
          hotswap = "hotswap",
          no_daemon = "no-daemon",
          last_opened_path = "last-opened-path",
        })
      end
    }
  )
end
-- close current buf and jump to buf from which this was opened
local function reg_cmd_EnvBufClose()
  ---@diagnostic disable-next-line: unused-local
  cu.reg_cmd('EnvBufClose', function(opts)
    local bufnr = api.nvim_get_current_buf()

    local ok, prev = pcall(api.nvim_buf_get_var, bufnr, 'prev_buf')
    if not ok then
      print('Not Found prev_buf for', bufnr)
    end
    vim.api.nvim_exec(':Bdelete!', true)

    if ok and prev then
      print('switch to prev:', prev)
      vim.api.nvim_exec(':b ' .. prev, true)
    end
  end, {})
end

-- Register commands
-- lua print(require("env.setup").add_commands())
function M.add_commands()
  local reg0 = cu.reg_cmd
  local reg = function(cmd, module)
    cu.reg_handler(cmd, module, _config) -- di
  end

  reg0("Wa", function() vim.cmd(':wa') end, {}) -- typo :Wa instead :wa

  reg0("EnvConfig", function()
    print('config: ' .. vim.inspect(_config))
  end, {})

  reg("EnvNew", "env.command.new")
  reg("EnvPM", "env.command.project_management")
  reg("EnvRefactor", "env.command.refactor")
  reg("EnvDecompile", "env.command.decompile")
  reg("EnvJvm", "env.langs.java.command.jvm")
  reg("EnvGenerate", "env.command.generate")
  reg("EnvProject", "env.command.project")
  reg("EnvBreakPoint", "env.command.break_point")
  reg("EnvJarViewer", "env.command.jar_viewer")
  reg0("EnvInfo", function() M.show_info_wnd() end, {})
  -- Modify selected lines
  reg("EnvLinesModify", "env.command.lines_modify")
  reg("EnvLM", "env.command.lines_modify")
  -- run lua function from the current line under the cursor
  reg('EnvCallFunc', "env.command.call_func")
  -- Run text from current line in the buffer as a vim command
  reg('EnvLineExec', "env.command.line_exec")
  reg('EnvLineInsert', "env.command.line_insert") -- simple code-actions impl
  reg('EnvLog', "env.command.log")
  reg('EnvXC', 'env.draw.command.main')           -- text canvas editor
  reg('EnvDprint', "env.command.dprint")
  reg('EnvLsp', "env.command.lsp")
  reg('EnvVim', "env.command.nvim")
  reg('EnvOS', "env.command.osystem")
  reg('EnvTranslate', 'env.command.translate')
  reg('EnvInspectTable', 'env.command.inspect_table')
  reg('EnvReload', 'env.command.reload') -- reload self and command code on fly
  reg('EnvConvert', 'env.command.convert')
  reg('EnvCompare', 'env.command.compare')

  -- dev & debug
  reg('EnvCache', 'env.command.cache')
  reg('EnvLang', 'env.command.lang') -- interact and configure Lang
  reg("EnvMpv", "env.command.mpv")
  reg("EnvDB", "env.command.db")
  reg("EnvYaml", "env.command.yaml")

  -- specific to a particular programming language
  -- EnvMaven == EnvPM -PL java mvn

  reg_cmd_EnvSet()
  reg_cmd_EnvBufClose()           -- close current buf and jump to prev
  research.reg_cmds_EnvBufInfos()
  sourcecode.reg_cmd_EnvLine()    -- break big current line to several
  lplugins.reg_cmd_EnvPlugin()    -- interact with installed plugins
  lpackages.reg_cmd_EnvPackages() -- interact with loaded Packages(require)


  -- api.nvim_create_user_command("EnvJCmd", function() end, {})

  require('env').add_commands()

  -- Session-like-usage
  cu.reg_cmd("EnvFileNamesSaveOpened", function(opts)
    M.save_opened_filenames(cu.fargs_safely(opts)[1])
  end, { nargs = '*' }) -- TODO complete=file

  cu.reg_cmd("EnvFileNamesOpenSaved", function(opts)
    M.open_saved_filenames(cu.fargs_safely(opts)[1])
  end, { nargs = '*' })

  --vim.cmd [[command! -buffer -nargs=? -complete=file EnvFileNamesSaveOpened lua require('env.setup').save_opened_filenames(<f-args>)]]
  --vim.cmd [[command! -buffer -nargs=? -complete=file EnvFileNamesOpenSaved lua require('env.setup').open_saved_filenames(<f-args>)]]
end

-- Save all names of currently opened files to given or default file
---@param path string|nil
function M.save_opened_filenames(path)
  if not path or path == "()" or path == "" then
    path = _config.last_opened
  end
  print(vim.inspect(path))
  if (bu.get_opened_files_count(path) > 0) then
    local t = bu.save_opened_filenames(path)
    ui.echohl(string.format("Saved: %s To:%s", t.count, t.name))
  else
    ui.echohl("Nothing to save")
  end
end

-- Reopen files from given or defaul file
-- Opens only those files that are not yet open in the editor
---@param path string|nil
function M.open_saved_filenames(path)
  if not path or path == "()" or path == "" then
    path = _config.last_opened
  end
  print(vim.inspect(path))
  if path then
    local t = bu.reopen_last_files(path) or {}
    ui.echohl(string.format("Opened: %s Skipped:%s", t.opened, t.skipped))
  else
    ui.echohl("Default file for last-opened-files not configured", "Error")
  end
end

return M

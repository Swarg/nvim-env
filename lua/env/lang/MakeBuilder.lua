-- 12-03-2024 @author Swarg
-- Goal:
--   - simple support of Makefile
--   - provide a generic way to simple raw-snippet "project"
--   - run tests and jump from source to tests and vice versa

local class = require 'oop.class'

local log = require('alogger')
local fs = require('env.files')
-- local library_cache = require("env.cache.library")

local Builder = require('env.lang.Builder')

class.package 'env.lang'
---@class env.lang.MakeBuilder : env.lang.Builder
---@field project_root    string
---@field buildscript     string   mix.exs (config-file with a settings)
---@field buildscript_lm       number?  last modify time of the rockspec-file
--                                 todo: checks changes and reload config
--- field settings        table?   parsed a mix.exs file (buildscript)(config)
---@field workspace_libs  table?   module -> absolute path (deps-mappings)
--                                 mappings for all dependencies(rock-packages)
--                                 specified in the rockspec-file in secrion
--                                 dependencies. real paths taken via luarocks show
local MakeBuilder = class.new_class(Builder, 'MakeBuilder', {
  name = 'make',
  project_props = nil,
  modules_map = nil, -- .rockspec  build.modules
})

-- local E = {}

-- public static
-- check is the given directory is Root of Source Code (has empty .git directory)
---@param path string
---@return string?  path to script file or nil
---@diagnostic disable-next-line: duplicate-set-field
---@param userdata table?
function MakeBuilder.isProjectRoot(path, userdata)
  log.debug("isProjectRoot", path)
  if path then
    local script_file = fs.join_path(path, 'Makefile')
    if fs.file_exists(script_file) then
      if userdata then
        userdata.buildscript = script_file
        userdata.project_root = path
        userdata.src = './'
        userdata.test = './'
      end

      return script_file
    end
  end

  return nil
end

--
---@param cn string
function MakeBuilder:findInnerPathForModule(cn)
  log.debug("findInnerPathForModule", cn)
  -- todo
  return nil
end

class.build(MakeBuilder)
return MakeBuilder

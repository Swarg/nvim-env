-- 20-09-2023 @author Swarg
local class = require("oop.class")

-- Abstract Class for env.lang.Lang

class.package 'env.lang'
---@class env.lang.Task: oop.Object
---@field lang env.lang.Lang
---@field status string
local Task = class.new_class(nil, 'Task', {
  lang = nil,
  status = nil, -- idle
})


---@return self
function Task:run()
  -- You must override this method
  return self
end

class.build(Task)
return Task

-- 26-08-2023 @author Swarg

-- Abstract Builder Class with basic logic for any Build System
-- Abstract class for inheritance and implementation specific logic

-- Notes:
-- Abount Lua Annotations See:
-- https://github.com/LuaLS/lua-language-server/wiki/Annotations
-- Note: https://emmylua.github.io/annotations/class.html

local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'

local ExecParams = require 'env.lang.ExecParams'
local FileWriter = require 'env.lang.FileWriter'
local ErrLogger = require 'olua.util.ErrLogger'


class.package 'env.lang'
---@class env.lang.Builder: oop.Object
---@field new fun(self, o:table?, project_root:string?, src:string?, test:string?): env.lang.Builder
---@field errs table - executable name of build tool
---@field name string          - executable name of build tool
---@field buildscript string
---@field buildscript_lm number?  last modify time of the buildscript file
---@field project_root string
---@field file_writer env.lang.FileWriter?
---@field errlogger olua.util.ErrLogger?
---@field parent env.lang.Builder - false or instance of Builder with parent of the
---                               current project
---@field subprojects table<env.lang.Builder> - list of builder of suprojects(in subdirs)
---@field src string           - path to sources
---@field test string          - path to test files
---@field settings table?      - private
local Builder = class.new_class(nil, 'Builder', {
  -- name = nil,          -- the executable name of the build tool
  -- buildscript = nil,   -- name of build script like makeFile, pom.xml, build.gradle
  -- project_root = nil,
  -- ...
  -- settings = nil,

  -- methods of the Class:
  -- new = function(self, o) -- this is same that:  GradleBuilder:new(o)
  --    ..
  -- end
})

--
-- Constructor
--
-- instead of deprecated bindContext
---@param project_root string?
---@param src string? src
---@param test string? test
function Builder:_init(project_root, src, test)
  self.project_root = project_root or self.project_root
  self.project_root = fs.ensure_dir(self.project_root) -- ensure ends with '/'
  -- must be overriden in implementation child class ("static" field
  assert(type(self.buildscript) == 'string', 'buildscript')

  self.src = src or self.src
  self.test = test or self.test
end

---@diagnostic disable-next-line: unused-local
local E, log_debug, log_trace = {}, log.debug, log.trace

-- public static abstract
-- to test if given path contains supported buildscript for this Builder class
-- ? for getParentProject
---@param path string? most often it already founded project root_dir
---@param userdata table?
---@param filename string? currently open file from which the search is performed
---@diagnostic disable-next-line: unused-local
function Builder.isProjectRoot(path, userdata, filename)
  -- error('abstract isProjectRoot must be implemented by own inherited Class')
  return nil
end

-- factory used after isProjectRoot
---@param project_root string
---@param props table?
---@return env.lang.Builder
function Builder:of(project_root, props) -- Builder.of(MyBuilderClass, dir, opts)
  -- local builder = clazz:new(props)
  local builder = self:new(props, project_root)
  builder:getSettings() -- load buildscript
  return builder
end

-- -- Constructor of the class
-- function Builder:new(o) -- function Builder.new(Builder, o)
--   o = o or {}
--   self.__index = self   -- Builder.__index = Builder
--   setmetatable(o, self)
--   return o
-- end
--
-- -- Class of current instance ( to instantiate new objects)
-- function Builder:getClass()
--   return getmetatable(self)
-- end
--
-- -- check is current instance inherite of given class
-- ---@param class table
-- function Builder:instanceof(class)
--   if class then
--     local i = 1
--     local instance = self
--     while instance and i < 64 do
--       local obj_class = getmetatable(instance) -- getClass()
--       if obj_class == class then return true end
--       instance = obj_class
--       i = i + 1
--     end
--   end
--   return false
-- end

---@return env.lang.FileWriter
function Builder:getFileWriter()
  if not self.file_writer then
    self.file_writer = FileWriter:new(nil, self.project_root)
  end
  return self.file_writer
end

---@return boolean
---@return string
function Builder:error(fmsg, ...)
  return self:getErrLogger():error(fmsg, ...)
end

---@return olua.util.ErrLogger
function Builder:getErrLogger()
  self.errlogger = self.errlogger or ErrLogger:new()
  return self.errlogger
end

---@return boolean
function Builder:hasErrors()
  return self.errlogger ~= nil and self.errlogger:hasErrors()
end

-- executable name of the build tool
---@return string
function Builder:getName()
  return self.name
end

---@return string
function Builder:getProjectRoot()
  return self.project_root
end

-- abstract
---@return string?
function Builder:getSubProjects()
  return nil
end

---@return self
---@param src string
---@param test string
function Builder:setSrcAndTestPaths(src, test)
  assert(type(src) == 'string', 'src')
  assert(type(test) == 'string', 'test')
  self.src = src
  self.test = test
  return self
end

-- @Deprecated
-- Links the context and the builder together
-- takes from ctx values of project_root, USER;
-- and update(set) values src, test, builder in ctx table
---@param ctx table { project_root, USER, src, test, builder}
---@deprecated TODO rewrite for env.lang.Lang
function Builder:bindContext(ctx)
  if ctx then
    self.USER = ctx.USER
    self.project_root = ctx.project_root
    ctx.builder = self:getName() -- @Deprecated! used env.lang.Builder not string
    ctx.src = self.src
    ctx.test = self.test
  end
  return self
end

---@return env.lang.ExecParams
function Builder:newExecParams(cmd, args, envs, desc)
  assert(self.project_root, 'project_root')
  return ExecParams.of(self.project_root, cmd, args, envs, desc)
end

-- return full path to the buildscript
---@param subproject string?
function Builder:getBuildScriptPath(subproject)
  log_debug("getBuildScriptPath root:%s buildscript:%s", self.project_root, self.buildscript)
  subproject = subproject or nil
  if self.project_root and self.buildscript then
    return fs.join_path(self.project_root, subproject, self.buildscript)
  end
  return self.buildscript
end

--
-- check whether the given path is the path to the build script of this Builder
-- (for overriding by descendant classes)
--
---@return boolean
function Builder.isBuildScript(path)
  log_debug("Builder.isBuildScript: ", path)
  return false;
end

--
-- alter self.settings - loads from buildscript if not loaded yet
--
-- Settings from buildscript (rockspec|build.gradle|pom.xml|mix.exs|etc)
---@return table
function Builder:getSettings()
  log_debug("getSettings", type(self.settings))
  if not self.settings then
    local t, err = self:loadBuildScript() -- updates self.buildscript_lm
    if not t then
      log.error(err or 'no errmsg from loadBuildScript')
      error('dev-mode') -- to show trace
      t = {}
    end
    self.settings = t
  end
  log_debug("getSettings loaded: ", type(self.settings))
  return self.settings
end

---@return false|table (settings
---@return string? errmsg
function Builder:loadBuildScript()
  local path = self:getBuildScriptPath() --fs.join_path(self.project_root, self.buildscript)
  log_debug("loadBuildScript", path)
  local exists, lm_time = fs.is_file(path)
  if not exists then
    return false, 'file not found ' .. tostring(path)
  end

  local prev_lm = self.buildscript_lm or 0
  self.buildscript_lm = lm_time or -1 -- update to current

  local bin = fs.read_all_bytes_from(path)
  if not bin then
    return false, 'Cannot load ' .. tostring(path)
  end

  return self:parseBuildScript(path, bin, prev_lm)
end

-- Abstract
--
---@param fn string -- self:getBuildScriptPath()
---@param bin string
---@param prev_lm number
---@return false|table
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function Builder:parseBuildScript(fn, bin, prev_lm)
  error('Abstract: not implement yet! you need to override this method!')
end

-- isBuildScript(Config)
-- check is a config(buildscript) was changed by lastmodify time
-- if the time is not specified (nil) assume that it has changed
---@param time number
function Builder:isBuildScriptChanged(time, update_lm)
  if update_lm then
    self.buildscript_lm = fs.last_modified(self:getBuildScriptPath())
  end
  log_debug("isBuildScriptChanged lm:%s time:%s buildscript:%s",
    self.buildscript_lm, time, self.buildscript)
  return not time or (self.buildscript_lm and self.buildscript_lm > time)
end

-- called automaticaly on rewrite buildscript
-- See setup in the env/env.lua:
--
--        vim.api.nvim_create_autocmd({ "BufWritePost" }, {
--          pattern = {
--            "*.rockspec",
--            -- TODO build.gralde, pom.xml, mix.exs, etc
--          },
--          callback = M.refresh_buildscript
--        })
--
--  First Introduced in LuaRocksBuilder to hot update luarocks deps packages
--  definve in the per-project *.rockspec file
--
---@param path string
---@diagnostic disable-next-line: unused-local
function Builder:refreshBuildscript(path)
  log_debug("refreshBuildscript", path)
  self.settings = nil -- to reload in getSettings inside resolveDependencies
  -- self.buildscript_lm = nil -- ?

  self:resolveDependencies()
end

-- see LuaRocksBuilder
function Builder:resolveDependencies()
  log_debug("resolveDependencies")
  self:getSettings()
end

---@param deps_to_add table
---@diagnostic disable-next-line: unused-local
function Builder:addDependencies(deps_to_add)
  error('abstract addDependencies must be implemented in a child class')
end

-- Return template for generate build script or
-- list(table) of the templates to generating multiples files
-- absctact
---@param pp table?
---@return string|table
---@diagnostic disable-next-line: unused-local
function Builder:getBuildScriptTemplate(pp)
  error('abstract getBuildScriptTemplate must be implemented in a child class')
end

-- /full/path/to/project_dir -> project_dir
function Builder:getProjectDirName()
  if self and self.project_root then
    local dir = fs.get_parent_dirname(self.project_root)
    return dir
  end
end

-- return Builder of ParentProject(DirectoryUp) or false
---@return env.lang.Builder|false
function Builder:getParentBuilder()
  if self.parent == false or self.parent then
    return self.parent
  end

  if self.project_root then
    local dir = fs.get_parent_dirpath(self.project_root)
    if self.isProjectRoot(dir) then
      local Class = getmetatable(self)
      self.parent = Class:new({ project_root = dir })
      -- bind self as child subproject
      self.parent.subprojects = self.parent.subprojects or {}
      local dirname = self:getProjectDirName()
      assert(dirname ~= nil, 'has own dirname')
      self.parent.subprojects[dirname] = self
    end
    return self.parent
  end
  return false
end

-- -- abstract
-- ---@diagnostic disable-next-line: unused-local
-- function JavaBuilder:getSubProject(dirname)
--   error('not implement yet! you need to override this method!')
-- end

-- Create instance of <? extends Builder> for given subproject by dirname
-- return nil if in given subdir specific buildsystem not found
---@return env.lang.Builder? "instance of new <? extends Builder> or nil"
function Builder:getSubProject(dirname)
  self.subprojects = self.subprojects or {}
  if not self.subprojects[dirname] then
    local child_root = fs.join_path(self.project_root, dirname)
    local Class = getmetatable(self) --  specific class of successor
    if Class.isProjectRoot(child_root) then
      self.subprojects[dirname] = Class:new({
        project_root = child_root
      })
    end
  end
  return self.subprojects[dirname]
end

---@param project_props table?
---@return boolean
---@return string? error
function Builder:isValidProjProps(project_props)
  if not self.project_root or self.project_root == '' then
    return self:error('no project_root')
  end
  if type(project_props) ~= 'table' then
    return self:error('no project_props')
  end
  if not self.buildscript or self.buildscript == '' then
    return self:error('no buildscript')
  end
  return true
end

-- Create one or multiples build scripts based getBuildScriptTemplate()
-- For multiples script write only to not exists files(Skip existing files)
--
-- by default FileWriter:createFile does not rewrite already existed files
-- to force do it specify pp.force_rewrite = true
--
---@param pp table? project properties
--                  (for java like: GROUP_ID, ARTIFACT_ID, VERSION, PROJECT_NAME
---@param subproject string? emtpy for root project
---@return number created files
function Builder:createBuildScript(pp, subproject)
  if not self:isValidProjProps(pp) then
    return 0
  end ---@cast pp table

  local fw = self:getFileWriter()
  local prev = fw:getEntriesCount()

  -- self.project_props = pp
  local templ = self:getBuildScriptTemplate(pp)
  local ttype = type(templ)

  if ttype == 'string' then
    local fn = self:getBuildScriptPath(subproject)
    fw:createFileFrTempl(fn, templ, pp, false)
    --
    -- multiple scripts:( for example build.gralde, gradle.properties, so on)
  elseif ttype == 'table' then
    for k_relative_path, v_templ in pairs(templ) do
      local fn = fs.join_path(self.project_root, subproject, k_relative_path)
      fw:createFileFrTempl(fn, v_templ, pp)
    end
  else
    self:error('expected valid buildscript template got: ' .. tostring(templ))
  end

  return fw:getEntriesCount() - prev
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

--
-- RawSnippets - this is a mode in which it is considered that the root directory
-- of the project contains a set of sources-test-pairs independent from each other.
-- Can content multiple files(sources-test-pairs) of same code (iterations)
--
-- The goal is to provide a way to run specific test files individually and
-- independently of each other. (Without any BuildSystem and test frameworks)
-- (the idea first came while working on env.langs.elixir.MixBuilder)
--
-- `RawSnippets` are stored in a flat structure so isFlatStructure = true
-- but `isFlatStructure` may be not only for disparate source-test pairs, but
-- also for simple flat projects where the sources are interconnected
--
---@param self self
---@return boolean
function Builder.isRawSnippets(self)
  return (((self or E).settings or E).project or E).raw_snippets == true
end

--
-- src+dir in same directory (Flat Project)
--
-- to provide support for simple flat projects (all in one directory)
--
-- unlike isRawSnippets, sources are considered interrelated with each other
-- but for simplicity (for educational purposes when learning a programming
-- language) everything is in one directory
--
---@return boolean
function Builder:isFlatStructure()
  -- error('abstract isFlatStructure')
  return self.src == './' and self.test == './'
end

--------------------------------------------------------------------------------
--                                  TOOLS                                     --
--------------------------------------------------------------------------------
local developerName

-- get User Name from git config or from System Env Variable $USER
---@return string?
function Builder.getDeveloperName()
  if not developerName then
    -- take from git
    local user
    user = fs.execr('git config user.name')
    if user then
      user = user:gsub('\n', '');
    else
      user = os.getenv('USER')
    end
    if user then
      developerName = user:sub(1, 1):upper() .. user:sub(2)
    end
  end
  return developerName
end

-- abstract
---@return env.lang.ExecParams
---@diagnostic disable-next-line: unused-local
function Builder:argsRunProject(opts) error('abstract') end

---@return env.lang.ExecParams
---@diagnostic disable-next-line: unused-local
function Builder:argsBuildProject(opts) error('abstract') end

--
class.build(Builder)
return Builder

---@diagnostic disable: unused-local
-- 19-09-2023 @author Swarg

-- Abstract Lang Class with basic logic for any Programming Language Environment
--[[
UseCases:
 EnvNew
 EnvProject build
 EnvProject run
 EnvProject debug

 EnvToggleTestSourceJump  -- openTestOrSource (open opposite for src/test pair)

 EnvRunTestFile           -- runSingleTestFile
 EnvRunMainInFile         -- runSingleSrcFile

 EnvDebugTestFile         --
 EnvDebugMainInFile
 EnvAttachToDebugger

 EnvGotoSource
 EnvSourceNameOf          -- sourceNameOf
]]
local class = require("oop.class")

local fs = require('env.files')
local su = require('env.sutil')
local log = require('alogger')
local tu = require 'env.util.tables'
local pcache = require("env.cache.projects")
local Cmd4Lua = require("cmd4lua")

local ucmd = require 'env.lang.utils.command'
local Editor = require("env.ui.Editor")
local Task = require('env.lang.Task')
local Report = require('env.lang.Report')
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require('env.lang.ClassResolver')
local DockerCompose = require('env.lang.DockerCompose')
local ExecParams = require 'env.lang.ExecParams'


class.package 'env.lang'
---@class env.lang.Lang : oop.Object
---@field new fun(self, o:table?, project_root:string?, builder:env.lang.Builder?): env.lang.Lang
---@field ext string
---@field ext2 string?            -- for case like Elixir-lang: ex + exs(4tests)
---@field pkg_sep string
---@field test_suffix string
---@field root_markers table
---@field project_root string?
---@field src string?
---@field test string?
---@field gen env.lang.oop.LangGen?  (FileWriter)
---@field builder env.lang.Builder   (FileWriter)
---@field testframework table
---@field already_existed boolean?
---@field dockerCompose env.lang.DockerCompose?
---@field editor env.ui.Editor
---@field task env.lang.Task?          -- currect task
---@field storage table?
---@field cimap table?                 -- mappings path|classname --> ClassInfo
local Lang = class.new_class(nil, 'Lang', {
  ext = '',
  ext2 = nil,    -- case then has compilable-source file and script file
  pkg_sep = '.', -- separator used in package(namespace) of class, module
  test_suffix = 'Test',
  root_markers = { ".git" },
  no_cache = false, -- by default use caches.projects
  src = 'src' .. fs.path_sep,
  test = 'test' .. fs.path_sep,
})

--
-- Constructor
--
-- instead of deprecated bindContext
---@param project_root string?
---@param builder env.lang.Builder
function Lang:_init(project_root, builder)
  self.project_root = project_root or self.project_root
  self.project_root = fs.ensure_dir(self.project_root) -- ensure ends with '/'
  if builder then
    -- sync
    builder.project_root = self.project_root
    builder.src = self.src
    builder.test = self.test
    self.builder = builder
  end
end

local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug
local log_trace = log.trace
local is_empty_str = ucmd.is_empty_str
local starts_with = su.starts_with
local file_exists, join_path = fs.file_exists, fs.join_path

---@return any? (strng)
local function validated_dirname(s)
  if type(s) ~= 'string' or s == '' then --- or f , 'expected project_root')
    error('expected not empty string dirname got: ' .. v2s(s))
  end
  return fs.ensure_dir(s)
end

--------------------------------------------------------------------------------
-- list of registered programming languages (Classes)
local languages = {}

-- reused instances of Lang Class storing already resolved project settings
-- mappings root --> instance
local projects = {}

--
-- Close all opened project
-- -- ?
--
---@return table
function Lang.clearCache()
  local prev = projects
  projects = {}

  pcache.full_cleanup() -- clean per-dir settings and project_roots

  return prev
end

--------------------------------------------------------------------------------
--           To keep state on lua-code runtime reloading (hotswap)

function Lang.dump_state()
  _G.__env_lang_state = {
    projects = projects,
  }
end

function Lang.restore_state()
  local state = _G.__env_lang_state
  _G.__env_lang_state = nil
  projects = {} --state.projects

  -- You can't just go ahead and make a copy here.
  -- because after a hot code update, the class code will also be reloaded and
  -- old classes will be unloaded and new ones will take their place, but not in
  -- objects.
  local clone_with_new_class = class.serialize_helper.clone_with_new_class
  for root_dir, lang in pairs(state.projects) do ---@cast lang env.lang.Lang
    projects[root_dir] = clone_with_new_class(lang)
  end
end

--------------------------------------------------------------------------------


---@param lang_cls env.lang.Lang
function Lang.register(lang_cls)
  if lang_cls and lang_cls.ext and class.instanceof(lang_cls, Lang) and
      not (languages[lang_cls.ext] or languages[lang_cls.ext .. 'lang'])
  then
    languages[lang_cls.ext] = lang_cls
    log.debug('Registered ', lang_cls.ext, lang_cls:getClassName())
    if lang_cls.ext2 and lang_cls.ext2 ~= '' then
      languages[lang_cls.ext2] = lang_cls
    end
  else
    log.debug('Cannot register ', lang_cls, class.name(lang_cls))
  end
end

-- returns the names of known languages (file-extension)
---@return table map ext->class
function Lang.getRegisteredLangs()
  local t = {}
  for k, lang_class in pairs(languages) do
    t[k] = lang_class
  end
  return t
end

--
---@return table list of ext
function Lang.getRegisteredLangsAliases()
  local t = {}
  for ext, _ in pairs(languages) do
    t[#t + 1] = ext
  end
  return t
end

--
---@param filename string
---@param ext string?
---@return env.lang.Lang? (class if found)
function Lang.getLang(filename, ext)
  if is_empty_str(filename) and is_empty_str(ext) then
    error(fmt('expected not empty string filename or extension got: "%s" "%s"',
      v2s(filename), v2s(ext)))
  end
  local klass, lang_instance = nil, nil
  ext = ext or fs.extract_extension(filename)

  if not is_empty_str(ext) then
    klass = languages[ext] or languages[ext .. 'lang']
    if klass then
      lang_instance = klass:new() -- new instance of registered LangClass
    end
  end
  log_debug('getLang for ext:%s give instanceof :%s', ext, class.name(klass))
  -- TODO call isLang(filename) foreach langhandler from languages list
  return lang_instance
end

--
-- Get Opened Project for current filename or directory or given extension
--
-- common logic for cli-handlers based on Cmd4Lua
--
-- find Lang for current context (opened file or cwd or given extension)
-- by default add_pl_opt=true, resolve=true, show_err=true
--
---@param w Cmd4Lua
---@param add_pl_opt boolean?  add opt: --programming-lang|-PL
---@param resolve    boolean?  apply Lang:resolve() for existed current-bufname
---@param show_err   boolean?  in Cmd4Lua command handler
---@return env.lang.Lang?
function Lang.getLangByCtx(w, add_pl_opt, resolve, show_err)
  local ext = nil
  if add_pl_opt then
    ext = ucmd.defineSelectLangByExt(w)
    ucmd.removeOptSelectLangByExt(w)
  end
  local dir, bufname = ucmd.get_current_directory() -- ?
  log_debug("getLangFrCtx opt:%s resolve:%s ext:%s dir:%s fn:%s",
    add_pl_opt, resolve, ext, dir, bufname)

  -- give the way to specify the specified Lang
  if w.vars.programming_language then
    local lang = Lang.getLang('', ext)
    log_debug('Lang %s for ext:%s', (lang == nil and 'not ' or ''), ext)
    return lang
  end

  local lang = not is_empty_str(dir) and Lang.getOpenedProject(dir, true)

  if lang then  -- not need to resolve already existing Langs
    log_debug('Lang found by directory:', dir)
    return lang -- lang:newStuff(w)
  end

  if not is_empty_str(bufname) then ---@cast bufname string
    lang = Lang.getLang(bufname, nil)
    if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
      log_debug('Lang found by bufname:"%s" resolve:%s', bufname, resolve)
      if resolve ~= false then -- nil or true
        pcall(function()
          lang = lang:resolve(bufname)
        end)
        if lang == nil then -- ask by user
          local ret = Editor.askValue("Define Project Root: ", bufname)
          if not ret or ret == '' or ret == 'q' then
            w:error('canceled - Cannot determine project root. markers are not found.')
          end
        end
      elseif w.vars.resolve_on_cache then
        log_debug('Resolving without caching')
        lang.no_cache = true         -- trigger full resolving without caching in the
        lang = lang:resolve(bufname) -- opened projects (for EnvProject info)
      end
    else
      -- cehck is given file is a buildscript for some Lang
      lang = Lang.getLangByBuildScript(bufname)
      log_debug("lang via buildscript", lang)
    end
  end
  --
  if not lang and not is_empty_str(ext) then
    lang = Lang.getLang('', ext)
    log_debug('Lang %s found by ext:%s', (lang == nil and 'not ' or ''), ext)
    -- resolve ? but by what?
  end

  if not lang and show_err ~= false then
    w:error(fmt('Not Found Lang for current context dir:%s fn:%s ext:%s',
      v2s(dir), v2s(bufname), v2s(ext)))
  end

  ---@cast lang env.lang.Lang?
  return lang
end

--
-- for overriding by descendant classes
--
---@return env.lang.Builder?
function Lang.getBuilderClassForBuildScript(path)
  return nil;
end

--
-- Determine by the path to the file whether this file is a file from one of the
-- build systems and, if so, determine the Lang for it and create its instance.
--
-- This can be useful when you need to start working with a project not from
-- the source file, but from the build script file of a certain build system
-- used in the project.
--
---@param path string?
---@return env.lang.Lang?
function Lang.getLangByBuildScript(path)
  if type(path) ~= 'string' or #path < 3 or path:sub(-1, -1) == fs.path_sep then
    return nil
  end

  for ext, klass in pairs(languages) do
    local builder = klass.getBuilderClassForBuildScript(path)
    if builder ~= nil then
      return klass:new() -- new instance of registered LangClass
    end
  end
  return nil
end

---@return table
function Lang.getActiveProjects()
  return projects
end

--
-- full(meticulously) search of all open projects (subdirs too):
-- useful when you need to find a project in a given subdirectory and
-- not by the exactly the root directory of the known project(Lang)
--
---@param project_root string
---@param full boolean?
function Lang.getOpenedProject(project_root, full)
  local lang = projects[project_root or false]

  -- check by subdirs:
  if not lang and full and not is_empty_str(project_root) then
    local fdir = fs.ensure_dir(project_root)
    for dir, lang0 in pairs(projects) do
      if su.starts_with(fdir, dir) then
        return lang0
      end
    end
  end

  return lang
end

---@param path string
---@return env.lang.Lang?
function Lang.findOpenedProjectByPath(path)
  log_debug("findOpenedProjectByPath", path)
  if path and path ~= '' then
    local candidates = {}
    for project_root, lang in pairs(projects) do
      log_debug("project_root", project_root)
      if project_root and su.starts_with(path, project_root) then
        local c = path:sub(#project_root, #project_root)
        log_debug('c:', c)
        if c == fs.path_sep then
          candidates[#candidates + 1] = lang
        end
      end
    end
    log_debug("candidates:", candidates)
    table.sort(candidates, function(a, b)
      return #a.project_root > #b.project_root
    end)
    return candidates[1]
  end
  return nil
end

---@return env.lang.Lang?
function Lang.pickOpenedProjectInteractively()
  local dirs = tu.sorted_keys(projects or E)
  local idx = Editor.pickItemIndex(dirs)
  if type(idx) == 'number' and idx > 0 then
    return projects[dirs[idx] or false]
  end
  return nil
end

--
-- close already opened project
--
---@param project_root string
function Lang.closeProject(project_root)
  local closed = false
  if projects[project_root] then
    projects[project_root] = nil
    pcache.clear_enrty(project_root)
    closed = true
  end
  log.debug("closeProject", closed, project_root)
  return closed
end

--
-- is given lang instance are already resolved active project
-- used in `:EnvProject info`
--
-- i.e is project opened(or Active or alive)
-- what is this for: before determining the language type for an open file, a
-- search must take place in the build system, and for this it need an instance
-- of env.lang.Lang class, therefore, while searching for a project root
-- for the currently open file, a temporary instance is created within which this
-- happens search for a builder. then it is cached and considered an open project
---@param lang env.lang.Lang? (instance not a class)
---@return string
function Lang.getInstanceType(lang)
  if not lang then
    return 'nil'
  end
  if class.is_object(lang) then
    for _, lang0 in pairs(projects) do
      if lang == lang0 then
        return 'active' -- opened|resolved or cached
      end
    end
    -- inMemOnly ?
    return 'temporary' --
  elseif class.is_class(lang) then
    return 'class ' .. v2s(class.name(lang))
  end
  return 'invalid'
end

--------------------------------------------------------------------------------

---@return table
function Lang:getCIMap()
  if not self.cimap then
    self.cimap = {}
    self.cimap.path2ci = {}
    self.cimap.cn2ci = {}
  end

  return self.cimap
end

---@return table
function Lang:getMapClassName2CI()
  self.cn2ci = self.cn2ci or {}
  return self.cn2ci
end

---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo? prev existed in same path
function Lang:setCI(ci)
  local path = assert((ci or E).path, 'ci.path')
  local cn = assert((ci or E).classname, 'ci.classname')

  local cimap = self:getCIMap()
  local last = cimap.path2ci[path]
  local last2 = cimap.cn2ci[cn]

  -- in come langs classname for CT.TESTDIR may be same as for CT.SOURCE
  local isdir = ci:isTestDir()
  -- validate 2maps sync
  if not last == last2 and not isdir then
    local ts = tostring
    error('sync path2ci:' .. ts(last ~= nil) .. ' cn2ci:' .. ts(last2 ~= nil))
  end

  if last ~= ci then
    cimap.path2ci[path] = ci
    cimap.cn2ci[cn] = ci
    return last
  end
end

--
-- getCIbyPath -- lookup only on in-mem map
-- the file is first searched in memory then on disk,
-- if found on disk it is added to the memcache (cimap)
--
---@param filename string
function Lang:findCIbyPath(filename)
  log.debug("findCIbyPath", filename)
  local ci = self:getCIbyPath(filename) -- in cache cimap
  if not ci then
    ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo()
  end
  return ci
end

---@param path string
---@return env.lang.ClassInfo?
function Lang:getCIbyPath(path)
  return ((self.cimap or E).path2ci or E)[path]
end

-- remove and return prev value
---@return env.lang.ClassInfo?
function Lang:popCIbyPath(path)
  -- local at = type(path)
  assert(type(path) == 'string', 'path')

  local ci = ((self.cimap or E).path2ci or E)[path]
  if ci then
    self.cimap.path2ci[path] = nil
    self.cimap.cn2ci[ci.classname] = nil -- ?
  end

  return ci
end

---@return env.lang.ClassInfo?
function Lang:getCIbyClassName(cn)
  return ((self.cimap or E).cn2ci or E)[cn or false]
end

-- remove and return prev value
---@return env.lang.ClassInfo?
function Lang:popCIbyClassName(cn)
  -- local at = type(path)
  assert(type(cn) == 'string', 'classname')

  local ci = ((self.cimap or E).cn2ci or E)[cn]
  if ci then
    self.cimap.cn2ci[cn] = nil
    self.cimap.path2ci[ci.path] = nil
  end

  return ci
end

---@param ci env.lang.ClassInfo
---@param cascade boolean? true  -- true to remove all related ClassInfos
function Lang:removeCI(ci, cascade)
  assert((ci or E).classname and (ci or E).path, 'ci must has path and classname')
  cascade = cascade == nil or cascade
  local c = 0

  if self.cimap then
    local cn2ci, path2ci = self.cimap.cn2ci, self.cimap.path2ci

    if path2ci[ci.path] == ci or cn2ci[ci.classname] == ci then
      cn2ci[ci.classname] = nil
      path2ci[ci.path] = nil
      c = c + 1
    end

    local pci = ci.pair
    if ci:hasTestCases() then
      for _, tci in pairs(ci:getTestCases() or E) do
        c = c + self:removeCI(tci, cascade)
        -- tci.parent == ci
      end
    end

    if pci then
      ci.pair = nil -- avoid infinity loop
      c = c + self:removeCI(pci, cascade)
    end
  end

  return c
end

--------------------------------------------------------------------------------

--
-- find already "opened" existed(in cache) project for given file
-- step1 - find project_root in the project cache
-- step2 - by project_root find existed project
--
---@param filename string?
---@return env.lang.Lang?
function Lang.findOpenProject(filename)
  if filename then
    local project_root = pcache.find_root_in_cache(nil, filename)
    if project_root then
      return projects[project_root]
    end
  end
end

---@return boolean
function Lang:hasUI()
  return Editor.hasUI()
end

---@return env.ui.Editor
function Lang:getEditor(filename)
  if not self.editor then
    self.editor = Editor:new()
  end
  return self.editor
end

--------------------------------------------------------------------------------

function Lang:getExt()
  return self.ext
end

---@return string
function Lang:getTestSuffix()
  return self.test_suffix or 'Test'
end

-- for tests
function Lang:noCache()
  self.no_cache = true
  return self
end

--
-- Find the Project Root for given file and either read the settings for this
-- project root (Builder, TestFramework, and etc)
-- or get a already existed instance of Lang from memory for same project_root
--
-- Note: then pass extactly project root add '/' to the end of the dir path
---@param filename string
---@return self
function Lang:resolve(filename)
  -- log_debug("resolve", class.name(self), filename)
  local instance, marker = self:findProjectRoot(filename)
  if not self.already_existed then
    log_debug('Resolve a new Project found by marker: %s', marker)
    instance:findProjectBuilder(filename)
    instance:findTestFramework(filename)
  end
  return instance
end

---@param self self
---@param project_root string
---@return self
local function cached(self, project_root, marker)
  log_debug("caching %s no_cache:%s %s", project_root, (self or E).no_cache, marker)

  project_root = self:setProjectRoot(project_root):getProjectRoot() -- ends "/"

  if self.no_cache then
    self.already_existed = false
    return self
  end

  local instance = projects[project_root]
  self.already_existed = instance ~= nil

  -- put current one into memory for reuse in the future
  if not instance then
    log.debug('Caching new Project', project_root)
    -- cached Lang instance for already known project_dir
    projects[project_root] = self
    instance = self

    -- add project_root of a new Project(dummpy flat)
    if not self.no_cache then
      -- this cache holds access to filesystem only without Lang instances
      -- this is intended to reduce disk access when searching for the
      -- root directory of the project for a particular file
      pcache.add_project_root(project_root)
    end
    --
  elseif log.is_debug() then
    log.debug('Take Already Existed Project[%s]: %s',
      class.name(instance), instance:getProjectRoot())
  end

  return instance
end

--
---@param project_root string
local function uncache(project_root)
  if project_root then
    local prev_lang = project_root[project_root]
    log_debug("uncache", class.name(prev_lang), project_root)
    project_root = fs.ensure_dir(project_root)
    projects[project_root] = nil
    -- pcache.clear_enrty(project_root)
  end
  return false
end

--
-- case: the builder change project_root of the Lang
-- (to support EmbededLuaTester)
--
---@param self self
---@param old_root string
---@param new_root string
local function recache(self, old_root, new_root)
  if old_root and old_root ~= new_root then
    log_debug('recache proj_root %s to %s', old_root, new_root)
    local prev_lang = projects[old_root]
    if prev_lang and class.name(prev_lang) == class.name(self) then
      uncache(old_root)
    end
    -- for the case when there are two paths in the map and the short path of the
    -- main project is taken first, preventing the embeded path from being obtained
    -- (pcache.projects_cache[old_root] or {}).embeded = new_root
    -- todo check

    cached(self, fs.ensure_dir(new_root), nil)
  end
end

--
-- Find Directory with some of markers files like .git or buildscript
-- Caches and reuses cache when searching for project_root paths
-- For an already loaded project for the same path to the project root,
-- it takes an already existing Lang-instance, and discarding the current one(self)
--
-- Note: if you want to pass exactly project_root dir
-- then you should definitely specify the slash at the end of the directory path
--
---@param filename string
---@return self
---@return string? marker
function Lang:findProjectRoot(filename)
  log_debug("findProjectRoot(%s) %s", class.name(self), filename)
  assert(type(filename) == 'string', 'filename')

  if not self.root_markers or #self.root_markers == 0 then
    error('No root_markers')
  end
  local project_root, marker
  if self.no_cache then
    log_debug('searching without cache')
    project_root, marker = fs.find_root(self.root_markers, filename)
    local t = pcache.readDirConf(project_root, self.no_cache)
    project_root, marker = pcache.updateRootDir(t, filename, project_root, marker)
  else
    project_root, marker = pcache.find_root(self.root_markers, filename)
  end

  if not project_root then
    log.debug('Cannot find project root for %s', filename)
    error('Project root not found: ' .. tostring(filename))
  end

  return cached(self, project_root, marker), marker
end

-- always ends with path_separator ('/') in the ends of a value
---@param path string -- a full path to the project directory with '/' in the end
---@return self
function Lang:setProjectRoot(path)
  self.project_root = fs.ensure_dir(path) -- ensure ends with '/'
  return self
end

function Lang:getProjectRoot()
  return self.project_root
end

function Lang:getProjectBuilder()
  return self.builder
end

-- to override
---@return env.lang.TestDiagnost?
function Lang:getTestDiagnost() return nil end

---@param builder env.lang.Builder
---@return self
function Lang:setProjectBuilder(builder)
  log_debug("setProjectBuilder", builder ~= nil)
  assert(self.builder == nil, 'project already has the builder')
  assert(type(builder) == 'table', 'builder')
  -- assert(class.instanceof(builder, Builder) == 'table', 'builder')
  self.builder = builder
  return self:syncSrcAndTestPathsWithBuilder()
end

---@param deps_to_add table
function Lang:addDependencies(deps_to_add)
  local builder = self:getProjectBuilder()
  if builder then
    return builder:addDependencies(deps_to_add)
  end
  return false
end

---@return boolean
function Lang:hasTestFramework()
  return self.testframework ~= nil
end

---@return table
function Lang:getTestFramework()
  return self.testframework
end

-- binary file to run testfile
function Lang:getTestFrameworkExecutable()
  if self.testframework then
    return (self.testframework or {}).executable
  end
end

--
---@return env.lang.DockerCompose?
function Lang:getDockerCompose()
  if not self.dockerCompose then
    self.dockerCompose = DockerCompose:new():lookup(self.project_root)
  end
  return self.dockerCompose
end

-- -- has Project Root and given filename is string
-- -- builder not check
-- function Lang:canWork(filename)
--   return type(filename) == 'string' and self.project_root ~= nil
-- end

function Lang:isPathInProjectRoot(path)
  assert(self.project_root ~= nil) -- DEBUG
  local b = false
  -- ensure project_root ends with /
  if self.project_root and path then
    b = path:sub(1, #self.project_root) == self.project_root

    if b and self.project_root:sub(-1, -1) ~= fs.path_sep then
      b = path:sub(#self.project_root + 1, #self.project_root + 1) == fs.path_sep
    end
  end

  log.debug("isPathInProjectRoot: %s path:%s pr:%s", b, path, self.project_root)
  return b
end

--
-- find bufnr in already opened buffers or open file if given path is exists
--
---@param path string
---@param openIfExists boolean?
---@param jumpto false|number|string?
---@return number?
function Lang:getBufnrForPath(path, openIfExists, jumpto)
  local bufnr = nil
  if path and self:isPathExists(path) then
    bufnr = self:getEditor().getBufnrForPath(path)
    if (not bufnr or bufnr == 0) and openIfExists then
      self:getEditor():open(path, jumpto)
      bufnr = self:getEditor().getCurrentBufnr()
    end
  end
  log_debug("getBufnrForPath", bufnr, path, openIfExists)
  return bufnr
end

---@return number
function Lang:getCurrentBufnr()
  return self:getEditor().getCurrentBufnr()
end

--
--
---@param self table -- env.lang.Lang
---@param path string
local function get_path_with_second_ext(self, path)
  local ext1, ext2

  if su.ends_with(path, '.' .. tostring(self.ext)) then
    ext1, ext2 = self.ext, self.ext2
  elseif su.ends_with(path, '.' .. tostring(self.ext2)) then
    ext1, ext2 = self.ext2, self.ext
  else
    return nil
  end

  return path:sub(1, #path - #ext1) .. self.ext2
end

--
-- check is given path (or ClassInfo.path) realy exists in the FileSystem
--
-- for classinfo only update the value of ClassInfo.exists field.
-- does not use a pre-known "exists" value, but directly finds out through io
--
-- For languages that have two extension variants for source files,
-- the presence of a second extension can be checked here if this is specified
-- explicitly(check_ext2)
--
---@param path string|env.lang.ClassInfo?
---@return boolean
---@param check_second_ext boolean?
function Lang:isPathExists(path, check_second_ext)
  local t, b = type(path), false

  if t == 'table' then
    if class.instanceof(path, ClassInfo) then
      local ci = path ---@cast ci env.lang.ClassInfo
      path = ci:getPath()
      b = type(path) == 'string' and fs.file_exists(path)

      -- check both variants ext and ext2
      if not b and check_second_ext and path and self.ext2 then
        local path2 = get_path_with_second_ext(self, path)
        if fs.file_exists(path2) then ---@cast path2 string
          ci:setPath(path2)
          b, path = true, path2
        end
        log.trace('check-ext2', self.ext, self.ext2, b)
      end

      ci:setPathExists(b) -- update ClassInfo.exists
      -- if no remove from classes?
    end
    --
  elseif t == 'string' and path ~= '' then
    b = fs.file_exists(path)
  end

  log.debug("isPathExists", path, b)
  return b
end

--------------------------------------------------------------------------------
--          Abstract methods to override by own Lang implementation           --
--------------------------------------------------------------------------------

---@param filename string
---@return boolean
---@diagnostic disable-next-line: unused-local
function Lang.isLang(filename) return false end

---@param filename string? - current opened filename
function Lang:findProjectBuilder(filename) self.builder = nil end

---@param filename string?
function Lang:findTestFramework(filename) self.testframework = nil end

-- Convert innerpath(relative path inside project without ext) into classname
-- innter-path --> classname
---@param innerpath string?
---@return string
function Lang:getClassNameForInnerPath(innerpath) error('abstract') end

-- Convert full classname into innerpath(relative path inside project_root
-- without extension
-- classname --> innter-path
---@param classname string
---@return string
function Lang:getInnerPathForClassName(classname) error('abstract') end

-- a simple and straightforward classname mappings: one source to one test file
-- this used by ClassResolver(ClassInfo) as base
function Lang:getTestClassForSrcClass(src_classname) error('abstract') end

-- a simple and straightforward classname mappings: one test to one source file
-- this used by ClassResolver(ClassInfo) as base
function Lang:getSrcClassForTestClass(test_classname) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return boolean
function Lang:lookupTestSuite(classinfo) error('abstract') end -- ? or just false

---@param filename string?
---@return boolean
function Lang:isSrcPath(filename) error('abstract') end

---@param filename string?
---@return boolean
function Lang:isTestPath(filename) error('abstract') end

-- for languages that run inside virtual machines and support hot code replacement
---@param filename string?
function Lang:hotswap(filename) error('absctract') end

-- source manager
---@return env.lang.Sourcer
function Lang:getSourcer() error 'abstract' end

-- simple check only by the test_suffix in the file name
---@param innerpath string?
---@return boolean
function Lang:isTestInnerPath(innerpath)
  return su.ends_with(innerpath, self.test_suffix or '_test')
end

---@return string
function Lang:getSrcPath()
  return (self.builder or {}).src or self.src
end

---@return string
function Lang:getTestPath()
  return (self.builder or {}).test or self.test
end

-- update self.src and self.test from obj
---@return self
function Lang:syncSrcAndTestPathsWithBuilder()
  assert(type(self.builder) == 'table', 'builder')

  self.src = self.builder.src or self.src
  self.test = self.builder.test or self.test
  local prev_project_root = self.project_root
  self.project_root = self.builder.project_root or self.project_root
  self.testframework = (self.builder.settings or E).testframework

  -- to support embeded LuaTester into another Lang Projects
  if prev_project_root and prev_project_root ~= self.project_root then
    recache(self, prev_project_root, self.project_root)
  end

  if log.is_debug() then
    log.debug('Lang:    %s src:%s test:%s project_root:%s',
      class.name(self), self.src, self.test, self.project_root)
    if self.builder ~= nil then
      local b = self.builder
      log.debug('Builder: %s src:%s test:%s project_root:%s',
        class.name(b), b.src, b.test, b.project_root)
    end
  end

  return self
end

--
-- classinfo must has opposite source file to generate test for it
--
-- UseCases:
-- -- first jump from the source file for testing with confirmation of generation
--  1) generate on the first "jump" from the source file to the test file
--     upon user confirmation
--  2) via cli and commands
--
---@param tci env.lang.ClassInfo
---@return string|false  -- path -- path to generated file or false
---@return string?       -- errmsg
function Lang:generateTestFile(tci, opts)
  log_debug("generateTestFile", tci)
  opts = opts or {}
  local gen = self:getLangGen()
  if not tci then
    return gen:getErrLogger():error('no-classinfo')
  end
  if not tci:isTestClass() then
    return gen:getErrLogger():error(
      'expected the test classinfo got: %s ', tci:getTypeName())
  end
  return gen:createTestFile(tci, opts)
end

-- ---@param classinfo env.lang.ClassInfo
-- function Lang:generateSourceFile(classinfo) end

---@param classinfo env.lang.ClassInfo
function Lang:doSyncProjectSettings(classinfo) end

---@param w Cmd4Lua
function Lang:doCodeCheck(ci, w) end

--
-- create a new Project
--
-- create only instance of Lang(Project) in mem for Flat Project
-- (to supports jump from sources to tests and back)
--   or
-- generate new Project for specific Programming Language - e.g. with the
--
--  - getLangGen():createProjectConfigs() - for Bilder (BuildSystem)
--  - getLangGen():createProjectFiles()   - for Project Files and DirsStructure
--
---@param project_root string
---@param opts table?
---@return env.lang.Builder
function Lang:createBuilder(project_root, opts)
  error('abstract createBuilder: must be implemented in specific Lang Class')
end

-- to create Project Config file (BuildScript like build.gradle, mix.exs, etc)
-- moved to env.lang.Lang
--
--@param opts table?
--@return number
--@diagnostic disable-next-line: unused-local
function Lang:createProjectConfigs(opts) error('abstract') end

-- goto source file
---@return boolean
---@param line string?
function Lang:gotoDefinition(line) error('abstract') end

--
-- resolve abs path to defintion (class, method, variable,...)
-- inner part of gotoDefinition
--
---@return string?
---@return number|string?
---@return number? resolved lexeme type
function Lang:resolveDefinition() error 'abstract' end

--
-- static ! to override by specific Lang impl
--
-- from some source code
-- main goal: jump to source file from stacktrace with errors
-- for example, from errors in tests
--
---@param editor env.ui.Editor
---@param line string?
---@return boolean
function Lang.gotoDefinitionFromNonSource(editor, line)
  return false
end

-- launchers

---@param classinfo env.lang.ClassInfo
---@return env.lang.ExecParams
---@param opts table?
function Lang:getArgsToRunSingleTestFile(classinfo, opts) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return env.lang.ExecParams
function Lang:getArgsToRunSingleSrcFile(classinfo) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return string?
function Lang:doValidateDebugger(classinfo) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return string?
function Lang:doAttachToDebugger(classinfo) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return env.lang.ExecParams
function Lang:getArgsToDebugSingleTestFile(classinfo) error('abstract') end

---@param classinfo env.lang.ClassInfo
---@return env.lang.ExecParams
function Lang:getArgsToDebugSingleSrcFile(classinfo) error('abstract') end

-- for testings
---@param opts table?
---@return self
function Lang:setup(opts) return self end

---@return env.lang.oop.LangGen? -- the instance of a LangGen class (ready to work)
function Lang:newLangGen()
  error('abstract newLangGen must be overrided by implemented child class')
  -- self.gen = env.lang.LangGen:new({ lang = self })
  -- return self.gen;
end

--
-- LangGen keeps FileWriter with report of last saved files
-- -- the instance of a LangGen class (ready to work)
--
---@return env.lang.oop.LangGen
function Lang:getLangGen()
  if not self.gen then
    self.gen = self:newLangGen()
  end
  return self.gen
end

--
-- factory to create {cwd, cmd, args, env} wrapped into env.lang.ExecParams
--
---@param cmd string?
---@param args table?
---@param envs table?
---@param desc string?
---@return env.lang.ExecParams
function Lang:newExecParams(cmd, args, envs, desc)
  return ExecParams:new(nil, self.project_root, cmd, args, envs, desc)
end

--------------------------------------------------------------------------------
--                                TOOLS
--------------------------------------------------------------------------------

--
-- Join Package(Namespace) with relative sub Package with ClassName
-- pkg(namespace) rel_classname
-- 'org.comp'     'sub.classname'  ->  'org.comp.sub.classname'
-- 'org.comp.'    'sub.classname'  ->  'org.comp.sub.classname'
-- 'org.comp.'    '.sub.classname' ->  'org.comp.sub.classname'
function Lang.joinNs(pkg, rel_classname, dot)
  dot = dot or '.'
  if pkg and rel_classname then
    local has_left = pkg:sub(-1, -1) == dot
    local has_right = rel_classname:sub(1, 1) == dot
    if not has_left and not has_right then
      return pkg .. dot .. rel_classname
    elseif has_left and has_right then
      return pkg:sub(1, -2) .. rel_classname
    else
      return pkg .. rel_classname
    end
  end
  return nil
end

-- /root/dir/ & /root/dir/inner/subdir/file.e   -->  inner/subdir/file.e
---@return string?
function Lang.getInnerPathFor(root, filename)
  assert(type(root) == 'string', 'root')
  assert(type(filename) == 'string', 'filename')

  if filename:sub(1, #root) == root then
    local len = string.len(root)
    -- inner path must starts without any slashes fix1:
    if root:sub(-1, -1) ~= fs.path_sep then
      if filename:sub(#root + 1, #root + 1) ~= fs.path_sep then
        return nil
      end
      len = len + 1
    end
    local s -- inner path
    s = string.sub(filename, len + 1)
    -- fix2: autofix dup slashes
    if s:sub(1, 1) == fs.path_sep then
      s = s:sub(2)
    end
    return s
  end
end

--
-- remove substring from rigth in path if matched given suffix
--
---@param path string
---@param suffix string
---@return string
function Lang.getPathWithoutSuffix(path, suffix)
  assert(type(path) == 'string', 'path')
  assert(type(suffix) == 'string', 'suffix')

  -- if path ends with suffix
  if #path > #suffix and path:sub(#path - #suffix + 1) == suffix then
    -- replace from end
    path = path:sub(1, #path - #suffix)
  end
  return path
end

--
-- add suffix if not already exists
--
---@param path string
---@param suffix string
---@return string
function Lang.getWithSuffix(path, suffix)
  assert(type(path) == 'string', 'path')
  assert(type(suffix) == 'string', 'suffix')

  if path:sub(#path - #suffix + 1) ~= suffix then
    path = path .. suffix
  end
  return path
end

-- extract path to class-file in project_root without project_root and extension
-- InnerPath or Namespace
-- /home/dev/prject/src/Auth/Entity/Id.php  -->  src/Auth/Entity/Id
--
--- protected
---@param filename string
---@return string|nil
-- function Lang:getNamespacePath(filename)
function Lang:getInnerPath(filename)
  log.trace("Lang:getInnerPath")
  if self:isPathInProjectRoot(filename) then
    local ip = Lang.getInnerPathFor(self.project_root, filename)
    if ip then
      local ext = self.ext
      if self.ext2 and self.ext2 ~= "" and su.ends_with(ip, self.ext2) then
        ext = self.ext2
      end
      return Lang.getPathWithoutSuffix(ip, '.' .. ext)
    end
  end
end

-- Convert filename to full classname (with namespace|package)
-- override
---@param filename string|nil
---@return string|nil
function Lang:getClassName(filename)
  if filename then
    local ns = self:getInnerPath(filename)
    if ns then
      return self:getClassNameForInnerPath(ns)
    end
  end
end

function Lang:getStorage(filename)
  if not self.storage then
    self.storage = {}
  end
  return self.storage
end

---@param innerpath string
---@param ext string?
function Lang:getFullPathForInnerPath(innerpath, ext)
  ext = ext or self.ext
  return fs.join_path(self.project_root, innerpath) ..
      (ext == '' and '' or '.' .. ext)
end

-- ---@param classname string? full classname inside project
-- ---@return string? path  -- fullpath to classname
-- ---@deprecated
-- function Lang:getFullPathForClass(classname)
--   if classname then
--     local innerpath = self:getInnerPathForClassName(classname)
--     return fs.join_path(self.project_root, innerpath) .. '.' .. self.ext
--   end
-- end

---@return env.lang.Report
function Lang:newReport(title)
  return self:getEditor():newReport(title)
end

---@param ci env.lang.ClassInfo?
function Lang:askToGenerateFile(ci)
  if ci and not fs.file_exists(ci:getPath()) then
    self:getEditor():showErr(string.format(
      "Not Found %s File for Class: '%s' (%s)",
      ci:getTypeName(),                             -- Test of Source
      (ci:getClassName() or ''):gsub('\\', '\\\\'), -- fix backslash issue in fmt
      (ci:getPath() or 'nil')
    ))

    -- self.task = classResolver
    self:getEditor():askToAction(string.format(
        "Create New %s Class %s at %s",
        ci:getTypeName(),         -- Test or Source
        ci:getClassName(),
        ci:getInnerPath()         -- inner path inside project without extension
      ),
      self.generateFile, self, ci -- callback self:generateFile(ci)
    )
  end
end

-- Create a new file with code for source or test.
-- classinfo - must already contain the classname and the full path to classfile
-- UseCases:
--   jump from source to not exists test file
---@param classinfo env.lang.ClassInfo
function Lang:generateFile(classinfo)
  log.debug("generateFile")

  if classinfo then
    if classinfo:isTestClass() then
      log.debug("generate Test File")
      self:generateTestFile(classinfo)
      -- self:getEditor():updateMethodName(classinfo)
      --
      -- elseif classinfo:isSourceClass() then
      --   log.debug("generate Source File")
      --   self:generateSourceFile(classinfo)
      --   -- self:getEditor():updateMethodName(classinfo)
    else
      log.debug("Uknown classfile type")
    end
  end
end

--
-- Resolve File with code (Class|Module) for given path
--
-- Goals:
--  - find Class for given path
--  - find Class with mappings:
--   - "one source to one test and vice versa"
--   - "many test-files to one source file"  and  "one source to many tests"
--     (Then tests placed in subdir with short-class-name)
--
-- Convert full path to file in project based ClassResolver
-- src->test  or test->src
--
-- UseCases:
--  - toggle jumps from->to: src->test, test->src         (default no req_type)
--  - get a specified required type from a src/test pair  (via req_type)
--
---@param filename string
---@param req_type number?    -- the required class type from pair src/test
---@return env.lang.ClassResolver
function Lang:resolveClass(filename, req_type)
  log.trace('resolveClass', filename, req_type)

  return ClassResolver:new({
    lang = self,
    find = filename, -- ? dup path
    path = filename, -- ClassInfo
    classname = nil, -- ClassInfo
    -- classinfo = ClassInfo:new({ lang = self, path = filename, classname = nil }),
    type = req_type  -- required class from a src/test opposite pair
  }):run()
end

--
-- get for given full classname (modulename) the absolute path
-- for only_existed=true - checks is resolved path exists and return nil if not
-- for all=true - find in all actived(opened) projects
--
---@return string?
---@param req_type number?
---@param only_existed boolean? only existed or nil
---@param all boolean? search in all active(opened) projects
function Lang:resolvePathByClassName(classname, req_type, only_existed, all)
  log_debug("resolvePathByClassName", classname, req_type, only_existed, all)
  local path, c = nil, 1

  local ci = self:lookupByClassName(classname, req_type)
  if ci then
    path = ci:getPath()
    if only_existed and not self:isPathExists(path) then
      path = nil
    end
  end

  local lang_cn = class.name(self)
  if not path and all then
    path, c = Lang.resolvePathByClassNameInAll(classname, req_type, lang_cn)
    c = c + 1
  end
  log_debug("(%s) checked:%s resolvedto: %s", lang_cn, c, path)
  return path
end

--
-- resolve fiven classname in all opened projects
--
---@param cn string classname to resolve
---@param req_type number?
---@param lang_cn string?
---@param skip_instance env.lang.Lang?  to skip self if already checked
---@return string? path
---@return number  cnt of checked instances
function Lang.resolvePathByClassNameInAll(cn, req_type, lang_cn, skip_instance)
  local path, c = nil, 0
  for ext, lang in pairs(Lang.getActiveProjects()) do
    c = c + 1
    if lang ~= skip_instance and not (lang_cn or class.name(lang) == lang_cn) then
      path = lang:resolvePathByClassName(cn, req_type, false)
      if path then
        break
      end
    end
  end
  return path, c
end

-- find abs path by given short filename of (test) class without package
function Lang.findOpenedFile(fn)
  return Editor.findOpenedFile(fn)
end

---@param ci env.lang.ClassInfo
function Lang:getOppositeClassInfo(ci)
  return ClassResolver:new({ lang = self }):getOppositeClass(ci)
end

--
-- factory to get ready to work ClassResolver
-- to find needs to specify one source for target - filename(path) or classname
--
-- if specified filename search will be by absolute path to file
-- if specified classname - find by class name
--
---@param req_type number?  (default is ClassInfo.CT.CURRENT)
---@param classname string?
---@param filename string?
function Lang:getClassResolver(req_type, filename, classname)
  assert(not is_empty_str(filename) or not is_empty_str(classname),
    'no source of target: need either arg#2(filename) or arg#3(classname)')

  -- req_type validate in _init
  return ClassResolver:new({
    lang = self, path = filename, classname = classname, type = req_type,
  })
end

-- resolve CI from given classname (inner + full path for given classname)
---@param classname string
---@param req_type number? --? experimental
---@return env.lang.ClassInfo?
function Lang:lookupByClassName(classname, req_type)
  -- issue: classname without test-suffix and req_type=Test ?? give Source class
  local ci = ClassResolver.of(self, req_type):lookupByClassName(classname, req_type)

  if req_type and ci and not ClassInfo.isValidType(ci) then
    ci.type = req_type -- SOURCE | TEST
  end
  return ci
end

--
--
-- throws error when ci:getInnerPath() or ci:getPath() return nil
-- (then given ClassInfo is empty or without resolved InnerPath, without lookup)
--
---@param ci env.lang.ClassInfo?
---@param force boolean?
---@return boolean?
function Lang:ensurePathExists(ci, force)
  if not ci then
    error('Illegal Classinfo - nil', 2)
  end

  if not ci:getInnerPath() or ci:getInnerPath() == '' then
    error('Illegal Classinfo - no inner-path. Lookup first!', 2)
  end

  local path = ci:getPath()
  if not path or path == '' then
    error('Illegal Classinfo - no path. Lookup first!', 2)
  end

  return self:isPathExists(path) -- check and update in ci
end

-- path-to-source-class --> path-to-test-class  or
-- path-to-source-class --> path-to-test-dir + test-class-file-names in dir
--
-- supports: one to one and one to many (ClassResolver) see resolveClass
--
-- find full path to testfile for give source file
-- if test splited into subdir with source class name - when
--   return full path to test dir and all test-flies in this directory
--
-- filename is full name to the source-class
---@return string?, table?<string>  -- path, nil  or  dir, files
function Lang:findTestForSource(sfilename)
  local tc = self:resolveClass(sfilename, ClassInfo.CT.TEST):getClassInfo()
  if tc then
    return tc:getPath(), tc:getTestCases()
  end
end

-- path-to-test-class --> path-to-source-class
-- supports: one to one and one to many (ClassResolver) see resolveClass
--
-- tfilename is full name to the test-class
---@return string?
function Lang:findSourceForTest(tfilename)
  local sc = self:resolveClass(tfilename, ClassInfo.CT.SOURCE):getClassInfo()
  return sc and sc:getPath()
end

--------------------------------------------------------------------------------
--                            USE CASES
--------------------------------------------------------------------------------
--- "plug"
-- local INTERFACE = function() end
--
-- -- The List of a interfaces:
-- --
-- Lang.newStuff = INTERFACE
-- Lang.sourceNameOf = INTERFACE
-- Lang.openTestOrSource = INTERFACE
-- Lang.runSingleTestFile = INTERFACE
-- Lang.debugSingleTestFile = INTERFACE
-- Lang.debugSingleSrcFile = INTERFACE
-- Lang.validateDebugger = INTERFACE
-- Lang.syncProjectSettings = INTERFACE
-- Lang.codeCheck = INTERFACE

-- implementations of the UseCases:

--- abstract
---@param conf table
---@return env.lang.Lang
function Lang:updateLspSettings(conf) return conf end

function Lang:cmdProjectManagement(w)
  self:getLangGen():dispatchCommand(w, 'project_management')
end

--- abstract
--- current filename takes from the getEditor().getContext()
---@param w Cmd4Lua
---@return boolean
function Lang:newStuff(w)
  self:getLangGen():newStuff(w)
  return true
end

function Lang:cmdRefactor(w)
  self:getLangGen():dispatchCommand(w, 'refactor')
  return true
end

-- langs/<LangName>/command/decompile.lua
function Lang:cmdDecompile(w)
  self:getLangGen():dispatchCommand(w, 'decompile')
  return true
end

-- function Lang:createBuildScript()
--   local builder = assert(self.builder, 'builder')
--   local gen = self:getLangGen()
--
-- end

--
-- just a dispatcher from EnvProject new into handler-func
--
-- implements the default behavior - a convention according to which the
-- handler for the command to create a new project should be located in
-- module with name "<package-of-current-lang>.command.new_project"
--
-- used in `:EnvProject new <lang>`
--
---@param w Cmd4Lua      - in order to pass the incoming command to the handler
---@param lang_clazz env.lang.Lang
---@return boolean
function Lang.cmdNewProject(w, lang_clazz)
  ucmd.pass_cmd_to_handler(w, '.command.new_project', lang_clazz)
  -- local cn = class.name(lang_clazz)
  -- local ns = class.get_package_name(cn)
  -- local modname = v2s(ns) ..
  -- local ok, modt = pcall(require, modname)
  -- if not ok then
  --   error(fmt('Not found module: "%s" for class "%s": %s', modname, v2s(cn), v2s(modt)))
  -- end
  -- modt.handle(w)
  return true
end

---@param self self
---@param project_root string?
---@return boolean
function Lang.validateUniqueProject(self, project_root)
  -- / in the end of project_root
  project_root = project_root or self.project_root
  local instance = projects[project_root or false]
  if instance and instance ~= self then
    error('Attempt to override already existed Project: ' .. v2s(project_root))
  end
  return true
end

--
--
-- Create new Project
--
-- called from env.langs.<lang>.command.new_project.handle
--
---@param project_root string
---@param opts table?
---@return self
function Lang:newProject(project_root, opts)
  log.debug("newProject dir:%s hasBuilder:%s", project_root, self.builder ~= nil)
  project_root = validated_dirname(project_root)

  self:validateUniqueProject(project_root)

  self.builder = self.builder or self:createBuilder(project_root, opts)
  self.project_root = project_root

  self:syncSrcAndTestPathsWithBuilder()

  return cached(self, self.project_root)
end

---@param filename string
function Lang:onNewFile(filename)
  log_debug("onNewFile", filename)
  -- stub for overriding
end

-- triggers for files and directories
---@param oldname string
---@param newname string
function Lang:onFileRenamed(oldname, newname)
  log_debug("onFileRenamed", oldname, newname)
  -- stub for overriding
end

--------------------------------------------------------------------------------

-- return module or class name of a given path
---@param filename string
function Lang:sourceNameOf(filename)
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo()
  if not ci then
    log.debug("No ClassInfo for: %s", filename)
    return
  end
  return ci:getClassName()
end

-- jump from source into test or from test to source
function Lang:openTestOrSource(filename)
  log.debug("openTestOrSource: %s", filename)
  local classResolver = self:resolveClass(filename, ClassInfo.CT.OPPOSITE)
  local classinfo = classResolver:getClassInfo()
  if not classinfo then
    log.debug("Cannot find opposite ClassInfo for: %s", filename)
    return
  end

  -- is cursor under the some method|function name? if so hold it in the Context
  -- it will be used in code generation or jump to method definition
  local jumpto = self:getEditor():resolveMethodOrFunction() -- :pickMethodNameTo(classinfo)

  if ClassResolver.isNotExistsTestClass(classinfo) then
    self:askToGenerateFile(classinfo) -- can change method name needs to pass for search
  end

  -- open
  if classResolver:isPathExists(classinfo) then
    if classinfo:isSourceClass() then -- jump from test to source
      local sci, tci = classinfo, self:getCIbyPath(filename)
      if not tci and sci.pair then
        tci = sci.pair
      end
      -- validate
      if not tci or tci:getPair() ~= sci then
        log.debug("\n tci: %s\n sci: %s", tci, sci)
        error('expected tci paired with sci, got: ' .. (tci or E).classname)
      end

      sci.pair = tci -- mark this test-file as work to quick back from src
      --
    elseif classinfo:isTestDir() then
      -- TODO:
      -- select a one test-case-file by the current context in the source file
      -- just to open first test-file from suite of cases
      classinfo = classinfo:getFirstCase() or classinfo
    end

    self:getEditor():open(classinfo:getPath(), jumpto) -- open existed file
    -- path can be directory for many-test-for-one-source
    self.editor:findWord()                             -- search(jump to method) in opened file
    -- with nvim-tree.lua plugin then open a directory you can pick file from it
  else
    log.debug("Path Not Exists %s", classinfo:getPath())
  end
  -- if jump src->test and test not exists ask to generete a new test file
end

-- parent/subproj
---@return string
---@param self self
local function fullProjectName(self)
  local pdir = (self or E).project_root
  local name = pcache.get_full_project_name(pdir) or pdir
  return name or '?'
end

--
--
---@param bname string
---@param params env.lang.ExecParams
---@return number
function Lang:runSysCmd(bname, params)
  log_debug("runSysCmd: %s", params)
  -- bname = bname or params.bname -- tite for buffer with output

  -- something to prepare the state for running a scheduled task (cmd)
  -- with the production of the final command to launch what should be shown
  -- in the buffer.
  -- For example, running tests. First we compile everything we need, then
  -- the commands to run the tests themselves.
  if type(params.callback) == 'function' then -- closure with quequed tasks
    local msg = params.callback(self, params) -- pass env.lang.Lang to callback
    self:getEditor():echoInStatus(msg)
    return 0                                  -- ok
  end

  return self:getEditor():runCmdInBuffer(bname, params)
end

function Lang:reloadBuildSystemConfig()
  log_debug("reloadBuildSystemConfig hasBuilder:%s", self.builder ~= nil)
  if self.builder then
    self.builder.settings = nil
    return self.builder:getSettings() ~= nil
  end
  return false
end

---@return number?
---@return string?
function Lang:runProject(opts)
  log_debug("runProject", opts)
  if not self.builder then
    return -1, 'no builder'
  end
  local params = self.builder:argsRunProject(opts)
  return self:runSysCmd("RunProject: " .. fullProjectName(self), params)
end

--
--
---@return number
---@return string?
function Lang:buildProject(opts)
  if not self.builder then
    return -1, 'no builder'
  end
  local params = self.builder:argsBuildProject(opts)
  return self:runSysCmd("BuildProject: " .. fullProjectName(self), params)
end

--
--
-- filename full path to test or source file (automaticaly switch to a test)
---@param filename string
---@return number
function Lang:runSingleTestFile(filename)
  log.debug("runSingleTestFile: %s", filename)
  -- test classinfo
  local tci = self:resolveClass(filename, ClassInfo.CT.TEST):getClassInfo(true)
  if not tci then return -1 end

  -- is cursor under the some method|function name? if so hold it in the Context
  -- it will be used in code generation or jump to method definition
  local jumpto = self:getEditor():resolveMethodOrFunction() --:pickMethodNameTo(tci)

  local params = self:getArgsToRunSingleTestFile(tci, { method_name = jumpto })

  local tdiag = self:getTestDiagnost()
  if tdiag then
    params.bufnr = self:getBufnrForPath(tci:getPath(), true, jumpto)
    params = tdiag:enrichParamsRunSingleTest(params)
  end

  return self:runSysCmd("Run Test: " .. tci:getClassName(), params)
end

-- Run single Source File (like main in c or java)
-- filename full path to source or test file (automaticaly switch to a source)
function Lang:runSingleSrcFile(filename)
  log.debug("runSingleSrcFile: %s", filename)

  local sci = self:resolveClass(filename, ClassInfo.CT.SOURCE):getClassInfo(true)
  if not sci then return end

  local params = self:getArgsToRunSingleSrcFile(sci)
  self:runSysCmd("Run Src: " .. sci:getClassName(), params)
end

function Lang:validateDebugger(filename)
  log.debug("validateDebugger: %s", filename)
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo(true)
  if not ci then return end
  return self:doValidateDebugger(ci)
end

function Lang:attachToDebugger(filename)
  log_debug("attachToDebugger")
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo(true)
  if not ci then return end
  return self:doAttachToDebugger(ci)
end

---
function Lang:debugSingleTestFile(filename)
  log.debug("debugSingleTestFile: %s", filename)
  -- test classinfo
  local tci = self:resolveClass(filename, ClassInfo.CT.TEST):getClassInfo(true)
  if not tci then return end

  local params = self:getArgsToDebugSingleTestFile(tci)
  params.desc = 'DebugSingleTestFile'

  local bname = "Debug Test: " .. tci:getClassName()
  return self:getEditor():runCmdInBuffer(bname, params)
end

function Lang:debugSingleSrcFile(filename)
  log.debug("debugSingleSrcFile: %s", filename)
  local sci = self:resolveClass(filename, ClassInfo.CT.SOURCE):getClassInfo(true)
  if not sci then return end

  local params = self:getArgsToDebugSingleSrcFile(sci)
  params.desc = 'DebugSingleSrcFile'

  local bname = "Debug Src: " .. sci:getClassName()
  return self:getEditor():runCmdInBuffer(bname, params)
end

function Lang:syncProjectSettings(filename)
  log.debug("syncProjectSettings: %s", filename)
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo(true)
  if not ci then return end
  ---@cast ci env.lang.ClassInfo
  self:doSyncProjectSettings(ci)
end

-- fires when saving the buffer in which the build script is open
function Lang:refreshBuildscript(path)
  if self.builder then
    self.builder:refreshBuildscript(path)
  end
end

---@param w Cmd4Lua
function Lang:codeCheck(filename, w)
  log.debug("codeCheck: %s", filename)
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo(true)
  if ci then
    ---@cast ci env.lang.ClassInfo
    self:doCodeCheck(ci, w)
  end
end

--
-- Used in simple flat dir with source file in same place with tests
-- for educational purposes without a project at all
--
-- known issue:
--  ip == ./filename.ext  - does not treat as base name.
--
---@param innerpath string inner path inside project root directory w/o ext
---@param src string
---@param test string
function Lang.isFlatDirPart(innerpath, src, test)
  if src == './' and test == './' and not is_empty_str(innerpath) then
    return fs.is_basename(innerpath) or -- script
        (not starts_with(innerpath, src) and not starts_with(innerpath, test))
    -- pkg.Main
  end
  return false
end

--
-- Event: onClassResolver no existed resolved file
--
-- provide way to custom behavior on event in ClassResolver when it cannot
-- find the opposite test file for given source file
-- usecases:
--   - support "RawSnippets as Project" Iterations one test-file many src-files
--
---@param classinfo env.lang.ClassInfo?
---@return env.lang.ClassInfo?
function Lang:onResolveCINoExistedPath(classinfo, ftype)
  log.trace("onResolveCINoExistedPath")
  return classinfo
end

---@param project_root string?
---@return boolean
function Lang.isProjectWithoutMarkers(project_root)
  return pcache.isProjectWithoutMarkers(project_root)
end

--
-- return child subdirs for given dir which configured by per-dir-conf file
-- as a separate projects (without any markers)
--
---@param dir string
---@return table of strings
---@param reload boolean?
function Lang.getProjectsWithoutMarkers(dir, reload)
  return pcache.getProjectsWithoutMarkers(dir, reload)
end

---@return boolean
function Lang:isIgnoredByLsp()
  return false
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

--
-- callback for gotoDefinition and gotoDefinitionFromNonSource
-- check is given path is already existed and open it with jump to line_num or
-- to first found word(jumpto)
--
---@param editor env.ui.Editor
---@param path any
---@param jumpto any
---@return boolean
function Lang.openExistedFileWithJumpTo(editor, path, jumpto)
  if type(path) == 'string' then
    -- normalize to absolute path
    if path:sub(1, 2) == '~/' then
      path = fs.join_path(os.getenv('HOME'), path:sub(3))
    end

    local exists = fs.file_exists(path)
    log_debug("openExistedFileWithJumpTo exists:", exists, path)

    if exists then
      editor:open(path, jumpto or 1)
      return true
    end
  end
  return false
end

--
-- from known sources or from known-non-sources files(logs,traces and so on)
---@param line string? - to jump via given line (from clipboard)
---@return boolean
function Lang.performGotoDefinition(line)
  local editor = Editor:new()
  local bufname = editor:getContext():getCurrentBufname()
  log_debug("performGotoDefinition bname:%s line:|%s|", bufname, line)

  if su.ends_with(bufname or '', '/breakpoints') then
    return Lang.gotoDefinitionFromBreakpointsWindow(editor)
  end

  if bufname ~= "" and bufname then -- skip lookup sources from float window
    local lang = Lang.getLang(bufname)
    if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
      local resolved_lang = nil
      pcall(function() resolved_lang = lang:resolve(bufname) end)
      if resolved_lang then
        resolved_lang:gotoDefinition(line)
      end
    end
  end

  -- from logs, traces and so on
  for _, lang0 in pairs(languages) do ---@cast lang0 env.lang.Lang
    if lang0.gotoDefinitionFromNonSource(editor, line) then
      return true -- performed
    end
  end

  return false
end

--
-- integration with `:EnvBreakPoint edit-all`
-- allow quick jump to source file from edit-all-breakpoints window
-- you need to place cursor into line with filename, next line is line number
-- in source file
--
---@return boolean
---@param editor env.ui.Editor
function Lang.gotoDefinitionFromBreakpointsWindow(editor)
  local line = editor:getContext():getCurrentLine()
  log_debug("gotoDefinitionFromBreakpoints", line)
  if is_empty_str(line) then return false end

  local match = string.match
  local filename = match(line, "'([^']+)'")
  if not filename then return false end
  local path = fs.join_path(os.getenv('PWD'), filename)
  if not fs.file_exists(path) then return false end

  local ctx = editor:getContext()
  local clnum = ctx:getCurrentLineNumber()
  local next_line = (ctx:getLines(clnum + 1, clnum + 1) or E)[1]
  local lnum = tonumber(match(next_line, '^%s*(%d+),%s*$')) or 1

  log_debug("resolved to %s:%s", path, lnum)
  editor:open(path, lnum)
  return true
end

--
-- substitute right value from test diagnostic output
-- Thing that will speed up development of test
--
---@return boolean
function Lang:usePassedIn()
  local tdia = self:getTestDiagnost()
  return tdia ~= nil and tdia:usePassedIn() or false
end

--
-- update diagnostic from file-report without re-run single test file
--
---@param w Cmd4Lua
---@return boolean
function Lang:updateTestDiagnostic(w)
  log_debug("updateTestDiagnostic")
  local tdia = self:getTestDiagnost()
  return tdia ~= nil and tdia:updateTestDiagnostic(w) or false
end

--
-------------------------------------------------------------------------------
--                           Refactoing Helpers
-------------------------------------------------------------------------------

-- for EnvRefactor add source-file
---@param path string
---@param opts table?
---@return string? inner_path to copied(moved) file inside current project
---@return string? errmsg
function Lang:addExternalSourceFile(path, opts)
  return self:getSourcer():addExternalSourceFile(path, opts)
end

class.build(Lang)
return Lang

-- 20-09-2023 @author Swarg
--
local uv = require('luv')
local fs = require('env.files')
local su = require('env.sutil')
local log = require('alogger')

local class = require("oop.class")

local Task = require('env.lang.Task')
---@diagnostic disable-next-line unused-local
local ClassInfo = require("env.lang.ClassInfo")

--
-- default behavior  - resolve full info (classname, inner_path, path) by one
-- given either absolute path to clasfile or by full classname

local E = {}
local CT_UNKNOWN = ClassInfo.CT.UKNOWN
local CT_SOURCE = ClassInfo.CT.SOURCE
local CT_TEST = ClassInfo.CT.TEST
local CT_TESTDIR = ClassInfo.CT.TESTDIR
local CT_OPPOSITE = ClassInfo.CT.OPPOSITE
local CT_CURRENT = ClassInfo.CT.CURRENT -- the default behavior

class.package 'env.lang'
---@class env.lang.ClassResolver: env.lang.Task
---@field lang env.lang.Lang             -- parent
--
---@field find string?                   -- name or path of the class to resolve TODO remove?
---@field path string?                   -- to find
---@field classname string?              -- to find
---@field type number?                   -- CT_SOURCE | CT_TEST | CT_OPPOSITE
--
---@field classinfo env.lang.ClassInfo?  -- founded result
---@field cachedci table?                -- map with classinfo taken from cache
--
---@field project_root string       -- always with '/' in the end
local ClassResolver = class.new_class(Task, 'ClassResolver', {
  -- lang = nil, -- strictly required
  -- type = nil, -- define what needs to be found from the given value(cn|path)
})

---@class env.lang.ClassResolver: env.lang.Task
---@field new fun(self, table): env.lang.ClassResolver

-- constructor used in new
function ClassResolver:_init()
  assert(self.lang ~= nil, 'lang strictly required')
  self.project_root = fs.ensure_dir(self.lang:getProjectRoot()) or '/'

  self.type = self.type or CT_CURRENT -- default value
  -- CT_CURRENT to resolve full info by given either classname either full path
  assert(type(self.type) == 'number', 'type - how to search')
end

--
-- static factory
---@param lang env.lang.Lang
---@param req_type number? default is CT_CURRENT
---@return env.lang.ClassResolver
function ClassResolver.of(lang, req_type)
  req_type = req_type or CT_CURRENT
  return ClassResolver:new({ lang = lang, type = req_type })
end

--
-- override Task:run
---@return self
function ClassResolver:run()
  log.trace('ClassResolver run >>>>>>>>', '::BACKTRACE::')
  assert(self.lang, 'lang')

  -- self.lang.task = self -- ????
  self.classinfo = nil -- ? clear prev result if exists
  self.cachedci = nil

  local classinfo = self:lookup(self.path, self.classname)

  if classinfo then
    local opposite = self.type == CT_OPPOSITE
    -- if a specific desired type of class from a src/test pair is specified
    if classinfo:isOppositeForType(self.type) then
      opposite = true
    end

    if opposite then
      -- old classinfo sets into a new classinfor.pair
      classinfo = self:getOppositeClass(classinfo)
      if not classinfo then
        log.debug('Not Found Opposite Class', self.path, self.classname)
        -- result is nil
        return self
      end
    end

    if self:isAlreadyCached(classinfo) then
      if self.type == CT_TESTDIR and -- req_type
          (classinfo.type == CT_TEST or classinfo == CT_TESTDIR) then
        classinfo = classinfo:getTopCasesDir()
      end
      -- many-tests--to--one-src
    else
      if not self.lang:isPathExists(classinfo, true) or self.type == CT_TESTDIR then
        if opposite or self.type == CT_TESTDIR then
          classinfo = self:lookupInSubDirs(classinfo, self.type)
        end
      end

      if classinfo and not self:isPathExists(classinfo) then
        classinfo = self.lang:onResolveCINoExistedPath(classinfo, self.type)
      end
    end

    self:cache(classinfo)
    --
  else
    log.debug('Cannot resolve class')
  end

  log.trace('ClassResolve run <<<<<<<<')

  self.classinfo = classinfo -- result
  return self
end

--
-- by one of the given values or path or classname, determines complete
-- information for this class - wraped into instace of ClassInfo
--
-- works via lookupByClassName or lookupByClassName
--
-- determine:
--   - classname or path
--   - inner_path (namespace inside project_root without extension)
--   - testsuite_{name|path} if this is TestClass
--   - type -- Source or Test
--
-- throws error if no value is specified by which to search
-- if cannot find classname|path for given path|classname return nil
--
---@param path string?
---@param classname string?
---@return env.lang.ClassInfo?
function ClassResolver:lookup(path, classname)
  assert(path or classname, 'need one target to seach: path or classname')
  log.trace('ClassResolver:lookup')

  if path then
    return self:lookupByPath(path)
  elseif classname then
    return self:lookupByClassName(classname)
  else
    error('no path or classname')
  end
end

--
-- by given path determine:
--   - inner_path (namespace inside project_root without extension)
--   - testsuite_{name|path} if this is TestClass
--   - type -- Source or Test
--   - classname
--
-- return nil if cannot find classname for given path
--
---@param path string  - the full path to the class-file
---@param ctype number?
---@return env.lang.ClassInfo?
function ClassResolver:lookupByPath(path, ctype)
  log.trace('lookupByPath[%s]:', ctype, path)
  assert(path, 'path')
  assert(ctype == nil or type(ctype) == 'number', 'ctype')

  local ip, classname, ci

  ci = self:findCIinCacheByPath(path)
  if ci then return ci end

  ip = self.lang:getInnerPath(path)
  classname = self.lang:getClassNameForInnerPath(ip)

  log.trace('inner-path: %s classname:%s', ip, classname)

  if ip and classname then
    ci = self:updateClassType(ClassInfo:new({
      classname = classname, inner_path = ip, path = path, type = ctype,
    }))
  end

  return ci
end

--
-- generate fullpath for the given full class name (suggested path)
-- Without checking whether the generated file exists or not
--
-- determine:
--   - path
--   - inner_path (namespace inside project_root without extension)
--   - testsuite_{name|path} if this is TestClass
--   - type -- Source or Test
--
-- if type of given classname alreay known(SOURCE) and
-- passed as ctype then updateClassType will be skipped
--
-- return nil if cannot generate path for given classname
--
---@param classname string
---@param ctype number?
---@return env.lang.ClassInfo?
function ClassResolver:lookupByClassName(classname, ctype)
  log.trace('lookupByClassName[%s]:', ctype, classname)
  assert(classname, 'classname')

  local ip, path, ci

  ci = self:findCIinCacheByClassName(classname)
  if ci then return ci end

  ip = self.lang:getInnerPathForClassName(classname)
  path = self.lang:getFullPathForInnerPath(ip)

  if path then
    ci = self:updateClassType(ClassInfo:new({
      classname = classname, inner_path = ip, path = path, type = ctype,
    }))
  end

  log.trace('InnerPath: %s path: %s type:%s', ip, path, (ci or E).type)
  return ci
end

--
-- for already known classinfo type (without updateClassType)
-- keep same instance of ClassInfo and existed pair
-- modify and return given ci or
-- return nil if for given new path classname is not found
--
---@param ci env.lang.ClassInfo
---@param path string
---@param exists boolean?
---@return env.lang.ClassInfo?
function ClassResolver:updateByPath(ci, path, exists)
  assert(ci, 'ci')
  assert(path, 'path')
  assert(type(ci.type) == 'number', 'ci.type must be resolved')

  local ip, classname

  ip = self.lang:getInnerPath(path)
  classname = self.lang:getClassNameForInnerPath(ip)

  log.trace('updateByPath: %s inner-path: %s classname:%s', path, ip, classname)

  return self:updateCI(ci, classname, ip, path, exists)
end

--
-- for classinfo with already known classinfo type (without updateClassType)
-- keep same instance of ClassInfo and existed pair
-- modify and return given ci, or
-- return nil if path not found for given classname
--
---@param ci env.lang.ClassInfo
---@param classname string
---@return env.lang.ClassInfo?
function ClassResolver:updateByClassName(ci, classname)
  assert(classname, 'classname')

  local ip, path, exists

  ip = self.lang:getInnerPathForClassName(classname)
  path = self.lang:getFullPathForInnerPath(ip)
  exists = nil -- needs to check

  log.trace('updateByClassName: %s inner-path: %s path:%s', classname, ip, path)

  return self:updateCI(ci, classname, ip, path, exists)
end

--
---@param ci env.lang.ClassInfo
---@param classname string?
---@param path string?
---@param inner_path string?
---@param exists boolean?
---@return env.lang.ClassInfo?
function ClassResolver:updateCI(ci, classname, inner_path, path, exists)
  if not classname or not inner_path or not path then
    return nil -- has invalid fields
  end

  -- keep testdir with dir suffix
  -- fix classname conflict in Lang.cimap cache
  if ci.type == CT_TESTDIR then
    classname = self.lang.getWithSuffix(classname, self.lang.pkg_sep)
    inner_path = self.lang.getWithSuffix(inner_path, fs.path_sep)
    path = self.lang.getWithSuffix(path, fs.path_sep)
  end

  ci:update(classname, inner_path, path, exists)
  return ci
end

--
-- determine type: CT_SOURCE | CT_TEST
--
-- class.type + check testsuite paths
--
-- check by testframework is inner_path belongs some testsuite path
--
---@param force boolean?
---@param ci env.lang.ClassInfo
function ClassResolver:updateClassType(ci, force)
  log.trace('updateClassType was:', (ci or E).type, (ci or E).inner_path)
  assert(ci and ci.inner_path, 'classinfo')

  -- skip if already known that ci is source class
  -- used for opposite then already known that is not a test
  if ci.type == CT_SOURCE and not force then
    return ci
  end

  ci.type = CT_UNKNOWN

  -- can trigger findout inner_path by classname of by path
  self.lang:lookupTestSuite(ci)

  -- if (ci.type == CT_TEST) and not force then return ci -- ? end

  -- testsuite if has and classtype
  if ci.testsuite_name and ci.testsuite_path then
    log.trace('has testsuite')
    ci.type = CT_TEST
  else
    local src, test = self.lang:getSrcPath(), self.lang:getTestPath()

    -- flat directory with sources and tests in same place
    if self.lang.isFlatDirPart(ci.inner_path, src, test) then
      log.trace('detected flat-src+test-dir')
      ci.type = self.lang:isTestInnerPath(ci.inner_path) and CT_TEST or CT_SOURCE
      --
    elseif src and su.starts_with(ci.inner_path, src) then
      log.trace('starts with src:', src)
      ci.type = CT_SOURCE
    else
      -- According to the design, this should never happen because
      -- this case should work through testsuite
      test = self.lang:getTestPath()
      if test and su.starts_with(ci.inner_path, test) then
        log.trace('starts with test:', test)
        ci.type = CT_TEST
      end
    end
    log.trace('updateClassType: was checked src:', src, 'test:', test)
  end

  log.trace('updateClassType to:', ci.type, ci.inner_path)
  return ci
end

--
-- src -> test, test->src
-- lookup opposite class type, move own into 'from'-field
--
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo?  if not found return nil
function ClassResolver:getOppositeClass(ci)
  assert(self.lang, 'Lang')
  assert(ci ~= nil and ci.classname, 'classinfo with classname')
  log.trace('>> getOppositeClass for cn:%s type:%s', ci.classname, ci.type,
    '::BACKTRACE::')

  local classname, ctype = nil, nil

  if ci.pair then
    log.trace('take already known paired: ', ci.pair.classname)
    self:makrAsCached(ci.pair)
    return ci.pair
  end

  if ci.type == CT_SOURCE then
    ctype = CT_TEST
    classname = self.lang:getTestClassForSrcClass(ci.classname)
  elseif ci.type == CT_TEST or ci.type == CT_TESTDIR then
    ctype = CT_SOURCE
    classname = self.lang:getSrcClassForTestClass(ci.classname)
  else
    error('[DEBUG] Uknown type ' .. tostring(ci.type))
  end

  if not classname then
    error('[DEBUG] Not Found opposite ClassName for ' .. tostring(ci))
  end

  -- can be used (CT_SOURCE) at the lookupByClassName
  -- add path, inner_path and testsuite if on some of it
  local oci = self:lookupByClassName(classname, ctype)
  ci:bindPair(oci)

  log.trace('<< getOppositeClass ret: ', (oci or E).classname)
  return oci
end

--
-- case many-test-one-source
--
-- fires then simple flat mapping not give the opposite pair
--
-- lookup more sophistical way to structuring the test subdirs (variants)
-- then for one source class exists many test files in a subdirectory
-- with the short name of the original class
--
---@param classinfo env.lang.ClassInfo
---@return env.lang.ClassInfo?
function ClassResolver:lookupInSubDirs(classinfo, req_type)
  local path, ci = classinfo:getPath(), nil
  log.trace('lookupInSubDirs for path: %s req_type:', path, req_type)

  if classinfo:isTestDir() then
    log.trace('already known(cache)')
    ci = classinfo -- already from cache
    --
  elseif classinfo:isTestClass() then
    ci = self:lookupTestInSubDir(classinfo, req_type, false)
    --
  elseif classinfo:isSourceClass() then
    local path0 = self:lookupSourceInSubDir(path) -- return exists path or nil
    if path0 and path ~= path0 then
      ci = self:updateByPath(classinfo, path0, true)
    else
      log.debug('lookupSourceInSubDir failed', path, path0)
    end
  end

  log.debug('After lookupInSubDir Path: %s', (ci or E).path)
  return ci
end

--
-- check is given TestCaseDir exists and if so then return all its files(tests)
--
-- correct path to test when jumping from a source file into test file
-- already checked that test_path does not exists
--   Has one source class(file) and many test-files (subtests) placed in
--   sub directory with original class shortname
--
-- src/Auth/Test/Unit/Entity/User/Token.php               - suggested
-- src/Auth/Test/Unit/Entity/User/Token/ValidateTest.php  - real exists
--
---@param tci env.lang.ClassInfo?
---@param recursive boolean?
---@return env.lang.ClassInfo?
function ClassResolver:lookupTestInSubDir(tci, req_type, recursive)
  log.debug("lookupTestInSubDir", (tci or E).path)
  if type(tci) ~= 'table' then return nil end
  assert(tci and tci.path, 'tci has path')

  local res, tc_dir = self:lookupTestCases(tci, recursive)

  if not res then
    if req_type == CT_TESTDIR and tc_dir then -- many-to-one
      tci.type = CT_TESTDIR
      return self:updateByPath(tci, tc_dir, false)
      --
    elseif req_type ~= CT_TESTDIR then
      log.trace('not found test-cases-dirs. keep as is - one-to-one relation')
      return tci
    end
  end

  return tci
end

--
-- populate ClassInfo.tcases if tci.path via getTestCaseDir containes tests
--
-- retrun nil if tci:getTestCaseDir not exists
--
-- UseCases:
--   - check is tci.path is a TestCaseDir
--   - populate TestCases for many-test-one-source relation (many-to-one)
--
---@param tci env.lang.ClassInfo
---@return env.lang.ClassInfo|false
---@return string tc_dir
---@param recursive boolean?
function ClassResolver:lookupTestCases(tci, recursive)
  log.debug("lookupTestCases", (tci or E).classname)
  local tc_dir, handle, errmsg

  tc_dir = self:getTestCaseDir(tci)

  -- assert(self.lang:getCIbyPath(tc_dir) == nil, '[DEBUG] already has in cache!')

  if tc_dir then
    handle, errmsg = uv.fs_scandir(tc_dir) -- + is dir exists inside
  end

  if type(handle) == "string" or not handle then
    log.debug('lookupTestCases no dir', errmsg)
    -- not a CT.TESTDIR
    return false, tc_dir
  end

  local tcases = {}
  self.type = CT_TESTDIR
  assert(self:updateByPath(tci, tc_dir, true) ~= nil, 'tci exists after update')
  tci:setTestCases(tcases)

  while true do
    local name, ftype = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    local ct = ftype == 'directory' and CT_TESTDIR or CT_TEST
    log.trace('%s %s', ftype, name, ct)
    tcases[name] = self:mkTestCaseCI(tci, name, ct, true)

    if ct == CT_TESTDIR and recursive then
      self:lookupTestCases(tcases[name], true)
    end
  end

  return tci, tc_dir
end

--
---@param parent env.lang.ClassInfo
---@param name string -- the file or directory name
---@param ct number
---@param exists boolean?
function ClassResolver:mkTestCaseCI(parent, name, ct, exists)
  assert(type(name) == 'string', 'name of the file or directory')
  assert(type(ct) == 'number', 'ct')

  local dir, name_wo_ext, path, ip, cn, pkg_sep
  name_wo_ext = self.lang.getPathWithoutSuffix(name, '.' .. self.lang.ext)
  pkg_sep = self.lang.pkg_sep
  dir = parent.path

  assert(parent.path:sub(-1, -1) == fs.path_sep, 'ends with /') -- innpath same
  assert(parent.classname:sub(-1, -1) == pkg_sep, 'ends with .')

  path = dir .. name
  ip = parent.inner_path .. name_wo_ext
  cn = parent.classname .. name_wo_ext

  if ct == CT_TESTDIR then
    path = path .. fs.path_sep -- sure dirname/
    ip = ip .. fs.path_sep
    cn = cn .. pkg_sep         -- ends with package separator (a dot)
  end

  local tc = ClassInfo:new({
    path = path, inner_path = ip, classname = cn, type = ct, exists = exists
  })

  tc.parent = parent
  tc.pair = parent.pair -- only one way (no sci->tc)

  return tc
end

-- correct path to source when jumping from one of subtests into source file
-- already checked that src_path does not exists
-- Case:
--   Has one source class(file) and multiple test-files (subtests) placed in
--   sub directory with original class shortname
--
-- 'src/Auth/Entity/User/Token/Create.php' -- suggest path to source from test
-- 'src/Auth/Entity/User/Token.php'        -- really exists path to source
--
--  src/Auth/Test/Unit/Entity/User/User/JoinByEmail/ConfirmTest.php -- test
--  src/Auth/Entity/User/User/JoinByEmail/Confirm.php               -- suggest
--  src/Auth/Entity/User/User.php                                  <-- real
--
---@return string? return sure existed path or nil
function ClassResolver:lookupSourceInSubDir(src_path)
  log.debug("lookupSourceInSubDir", src_path)
  assert(self.project_root ~= nil, 'ensure has project_root')

  if not self:isPathInProjectRoot(src_path) then
    log.debug('Path [%s] is not Project Root[%s]', src_path, self.project_root)
    return nil
  end

  local ext = fs.extract_extension(src_path)
  if src_path and ext then
    local dir = fs.get_parent_dirpath(src_path)
    local dirname = fs.extract_filename(src_path)

    while dir and not fs.dir_exists(dir) do
      if #dir < #self.project_root then -- limit the deep
        break
      end
      dirname = fs.get_parent_dirname(dir)
      dir = fs.get_parent_dirpath(dir)
    end
    log.debug('now up to dir:', dir, dirname)

    local path = fs.join_path(dir, dirname) .. '.' .. ext
    if fs.file_exists(path) then
      return path
    end
  end
end

---@return string?
function ClassResolver:getPath()
  return self.classinfo and self.classinfo:getPath()
end

---@param path string?
---@return boolean
function ClassResolver:isPathInProjectRoot(path)
  assert(self.project_root ~= nil)
  if path then
    return path:sub(1, #self.project_root) == self.project_root
  end
  return false
end

---@param ci env.lang.ClassInfo?
function ClassResolver:isPathExists(ci)
  log.debug("isPathExists ci.exists:", (ci or E).exists, 'has ci:', ci ~= nil)
  if ci == nil or not ci.path or ci:isPathExists() == false then
    return false
  end
  return self.lang:isPathExists(ci)
end

--
-- has path for current classinfo but not existed in filesystem
--
---@param ci env.lang.ClassInfo?
---@return boolean
function ClassResolver.isNotExistsTestClass(ci)
  return ci ~= nil and ci:isTestClass() and not ci:isPathExists()
end

--
-- has path for current classinfo but not existed in filesystem
-- TestDir is a Dir with test cases for one source file (many-to-one)
-- is used to toggle beetwen src-test
--
---@param ci env.lang.ClassInfo?
---@return boolean
function ClassResolver.isNotExistsTestDir(ci)
  return ci ~= nil and ci:isTestDir() and not ci:isPathExists()
end

--
-- ClassResolver hold search result in  self.classinfo
-- this method validate this search result(classinfo)
-- and throws exceptions if something goes wrong
--
-- Checks that given ci has inner_path and path to a existed file
-- throw error if ci:getInnerPath() or getPath() return a nil value
--
-- if ClassInfo.path exists update value ClassInfo.exists to true
--
---param filename string
---param ci env.lang.ClassInfo?
---@return boolean
function ClassResolver:ensureFoundExisted(force)
  local ci = self.classinfo
  if not ci then
    log.debug("Not Found ClassInfo for: %s", self.path, self.classname)
    return false
  end
  local f = self.lang:ensurePathExists(ci, force)
  if not f then
    local msg = ('Not Exists %s File %s'):format(ci:getTypeName(), ci:getPath())
    log.debug(msg)
    self.lang:getEditor():echoInStatus(msg, 'Error') -- print red color
    f = false
  end
  return f
end

--------------------------------------------------------------------------------


--
-- for resolved test classes
--
-- UseCase: many test-files for one source file in mirrored test path with
-- basename of source file itself instead <source><suffix><ext>
-- Goal: provide potencialy existed path to a dir with a test files for the
-- one source file. Transform simple flat mirrored test-path into the tree.
--
-- lookupTestInSubDir
--
-- TestCase - is a basename of the test file itself without suffix and extendsion
-- /app/proj/spec/app/module_spec.lua - > /app/proj/spec/app/module
--
---@param ci env.lang.ClassInfo
---@return string
function ClassResolver:getTestCaseDir(ci)
  assert(ci:getPath(), 'expected resolved Test ClassInfo with path')
  assert(ci.type == CT_TEST or ci.type == CT_TESTDIR,
    'expected classinfo.type == Test or TestDir, got:' .. tostring(ci.type))

  local dir, ext = ci:getPath(), nil ---@cast dir string

  if ci.type ~= CT_TESTDIR then
    ext = fs.extract_extension(dir)
    -- if has extension - emove suffix
    -- if self.path already is a TestCaseDir - then it will be without ext and suffix
    if ext then
      local suffix = self.lang:getTestSuffix() .. '.' .. ext
      -- is ends with suffix then replace suffix
      dir = self.lang.getPathWithoutSuffix(dir, suffix)
    end
    dir = self.lang.getWithSuffix(dir, fs.path_sep) -- ensure with /
  end

  log.trace("getTestCaseDir ret: %s ext: %s", dir, ext)
  return dir
end

--
-- return the result of the searching
-- with validate if onlyExistsOrNil == true
--
---@param onlyExistsOrNil boolean?
---@param force boolean? - to recheck "ci.exists" flag via access to FS
---@return env.lang.ClassInfo?
function ClassResolver:getClassInfo(onlyExistsOrNil, force)
  local ci = self.classinfo
  if onlyExistsOrNil then
    if not self:ensureFoundExisted(force) then
      ci = nil
    end
  end
  log.trace('Resolved to: %s', ci)
  return ci
end

--
--------------------------------------------------------------------------------
--                          Cached ClassInfo
--------------------------------------------------------------------------------

--
-- ask CI by absolute path from Lang
--
---@param path string
---@return env.lang.ClassInfo?
function ClassResolver:findCIinCacheByPath(path)
  local ci = self.lang:getCIbyPath(path)
  if ci then
    self:makrAsCached(ci)
    return ci
  end
end

--
-- ask CI by full classname from Lang
--
---@param cn string
---@return env.lang.ClassInfo?
function ClassResolver:findCIinCacheByClassName(cn)
  local ci = self.lang:getCIbyClassName(cn)
  if ci then
    self:makrAsCached(ci)
    return ci
  end
end

--
-- goal: not recache CI alreay taken from cache
---@param ci env.lang.ClassInfo
function ClassResolver:isAlreadyCached(ci)
  local cached = (self.cachedci or E)[ci] == true
  log.trace("isAlreadyCached: ", cached)
  return cached
end

---@param ci env.lang.ClassInfo
---@param cached boolean? default is true
function ClassResolver:makrAsCached(ci, cached)
  assert((ci or E).classname, 'ci.classname')
  log.trace('found in cache', ci.classname)

  cached = cached == nil or cached
  self.cachedci = self.cachedci or {}
  self.cachedci[ci] = cached

  return ci
end

--
--
-- add to cache ClassInfo with all subchilds for TestDir
-- ClassInfo and subchilds must be already checked to exists in the FileSystem
--
---@param ci env.lang.ClassInfo
---@param self env.lang.ClassResolver
local function cacheCiRecursive(self, ci)
  assert(self and self.lang, 'self')
  assert(ci and ci.classname and ci.path, 'ci')
  assert(ci.exists == true, 'must exists!')
  log.trace('add to cache', ci.classname)

  self.lang:setCI(ci)

  if ci:hasTestCases() then -- TestDir
    for _, tci in pairs(ci:getTestCases() or E) do
      cacheCiRecursive(self, tci)
    end
  end
end

--
-- add to cache all relations - pair and childs of TestCasesDir
-- can add only CI with path ensude existed in FS
--
-- UseCase: to reuse the results of previous searches
-- (Relevant for many-tests-one-source relations)
--
---@param ci env.lang.ClassInfo?
function ClassResolver:cache(ci)
  log.trace("cache cn:", (ci or E).classname)

  if ci then
    -- ci:isPathExists - because already must be checked by the caller
    if ci:isPathExists() and not self:isAlreadyCached(ci) then
      cacheCiRecursive(self, ci)
    end

    local pci = ci:getPair()
    if pci then
      log.trace('check is needs to add to the cache ci.pair: ', pci.classname)
      -- self:isPathExists -- to ensure exists use checked exists or recheck
      if self:isPathExists(pci) and not self:isAlreadyCached(pci) then
        cacheCiRecursive(self, pci)
      else
        -- remove pair ?
      end
    end
  end
end

--
class.build(ClassResolver)
return ClassResolver

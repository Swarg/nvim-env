-- 16-07-2024 @author Swarg
-- Goal:
-- - save generated content to specified files with logging of how
--   the file was written


local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'

local base = require 'env.lang.utils.base'
local TU = require 'env.lang.utils.templater'

local NEW_DIR_MODE = 493 -- 755


class.package 'env.lang'
---@class env.lang.FileWriter : oop.Object
---@field new fun(self, o:table?, project_root:string): env.lang.FileWriter
---@field project_root string
---@field entries table? -- saved files reports
---@field notify_callback function?
local C = class.new_class(nil, 'FileWriter', {
})

C.OK_SAVED = base.OK_SAVED
C.NO_CONTENT = base.ERR_NO_CONTENT
C.SKIP_EXISTS = base.SKIP_EXISTS

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug, log_trace = log.debug, log.trace
local is_empty_str = base.is_empty_str

---@param project_root string
function C:_init(project_root)
  self.project_root = project_root or self.project_root
  assert(type(self.project_root) == 'string', 'project_root')
end

--
-- generate new file in the project root based on template
-- project_root + basename + ext
--
---@param basename string
---@param ext string
---@param content string
---@param opts table? substitution for template
---@return number code
---@return string path
function C:createFile(basename, ext, content, opts)
  log_debug('createFile', basename)
  -- assert(self.lang, 'has Lang')
  local path = self:getFullPathForInnerPath(basename, ext)
  local exists_checked = false
  local code = self:saveFile(exists_checked, path, content, opts)
  return code, path
end

-- build content from given template and substitution and save it into given
-- basename (relatively from project_root directory)
--
---@param basename string
---@param template string
---@param substitutions table -- and options for saving in the same table
---@param upperkey boolean? -- do uppder key for building the content from templ
---@return number code
---@return string path
function C:createFileFrTempl(basename, template, substitutions, upperkey)
  log_debug('createFileFrTempl %s templ:%s', basename, not is_empty_str(template))
  log_trace('substitutions:', substitutions)

  local content = TU.substitute(template, substitutions, upperkey)
  local opts = substitutions -- is a good idea??
  -- for now I did it this way, because usually the parameters for inserting into
  -- templates come through the cli and additional options. the options also come
  -- from there, so there's one table for different things
  return self:createFile(basename, '', content, opts)
end

--
-- save content to the given not-existed file (or rewrite with opts.force_rewrite)
--
---@param checked boolean already checked is file exists
---@param path string?
---@param body string?
---@param opts table? {dry_run, force_rewrite}
function C:saveFile(checked, path, body, opts)
  log_debug("saveFile", path)
  assert(type(checked) == 'boolean', 'checked')
  opts = opts or {}

  if not checked and not opts.append and fs.file_exists(path) then
    log_debug('File already exists "%s" force:%s', path, opts.force_rewrite)
    if not opts.force_rewrite then
      return self:addEntry(path, base.SKIP_EXISTS) -- exit
    end
    self:addEntry(path, base.REWRITE_EXISTS)       -- continue...
  end

  log_debug("saveFile path: %s body.len: %s", path, #(body or ''))

  if not path or not body then
    return self:addEntry(path, base.NO_PATH) -- exit
  end

  if opts.dry_run then
    log_debug("dry-run mode - only generate the source without saving to file")
    opts.class_body = body -- testing
    opts.class_file = path
    if not opts.quiet then -- show generated body of the class in the editor
      self:echoInStatus(path, body)
    end

    return self:addEntry(path, base.DRY_RUN) -- exit
  end

  local code = base.saveFile(path, body, NEW_DIR_MODE, opts.append)
  log_debug('file saved:', (code == base.OK_SAVED), base.save_file_codes[code])

  return self:addEntry(path, code)
end

--
-- get from the relative path the full path relative to the project root
--
-- if the given "innerpath" starts from the self.project_root then it is
-- considered the full path and is returned as is:
-- it is assumed that if the full path is specified, then the extension has
-- already been added by the caller.
--
---@param innerpath string  - can be fullpath in project_root
--
---@param ext string?
function C:getFullPathForInnerPath(innerpath, ext)
  assert(type(innerpath) == 'string', 'expected string innerpath')
  if base.is_empty_str(innerpath) then
    error('expected not empty innerpath got: ' .. tostring(innerpath))
  end

  if base.str_starts_with(innerpath, self.project_root) then
    return innerpath -- this is full path (assumed with extension)
  end
  return fs.join_path(self.project_root, innerpath) ..
      ((ext == '' or not ext) and '' or '.' .. ext)
end

---@return number
function C:getEntriesCount()
  return type(self.entries) == 'table' and #self.entries or 0
end

--
-- add Save State Entry path and code
--
-- report about generated(saved) files
---@param fn string?
---@param code number
---@param errmsg string? (optional)
---@return number
function C:addEntry(fn, code, errmsg)
  self.entries = self.entries or {}
  local index = nil
  if fn ~= nil and code ~= nil then
    local t = self.entries
    index = #t + 1
    t[index] = { fn, code, errmsg }
  end
  log_debug("addEntry with index:%s code:%s %s", index, code, fn)
  return code
end

--
-- get entry(path, code) with given index
-- you can use index = -1 to get last entry
--
---@param index number -- supports negative values (pick elm from tail)
---@return string? path
---@return number? code
function C:getEntry(index)
  local cnt = self:getEntriesCount()
  if cnt > 0 then
    if index < 0 then
      index = cnt + index + 1
    end
    -- print("[DEBUG] index:", index, #self.entries )
    local t = self.entries[index] or E
    return t[1], t[2] -- filename, code
  end
  return nil, nil
end

---@return number?
---@return string?
function C:getLastEntryCodeAndPath()
  local path, code = self:getEntry(-1)
  return code, path
end

--
-- generate readable report based on collected saved stats
--
---@param prefix string?
---@return string
function C:getReadableReport(prefix)
  local s = prefix ~= nil and (tostring(prefix) .. "\n") or ''
  if type(self.entries) == 'table' then
    for _, e in ipairs(self.entries) do
      if type(e) == 'table' then
        local fn, coden, errmsg = e[1], e[2], e[3]
        local code = base.save_file_codes[coden or false]
        local details = errmsg ~= nil and (' : ' .. tostring(errmsg)) or ''
        s = s .. tostring(code) .. ': ' .. tostring(fn) .. details .. "\n"
      end
    end
  end
  if not prefix and #(self.entries or E) == 1 then s = s:sub(1, -2) end

  return s
end

---@return self
function C:clearEntries()
  self.entries = nil
  return self
end

---@param path string
---@param body string
function C:echoInStatus(path, body)
  -- notify_callback
  if type(self.notify_callback) == 'function' then
    self.notify_callback(path, body)
    -- self.lang:getEditor():echoInStatus(body, 'Comment')
    -- todo
  end
end

class.build(C)
return C

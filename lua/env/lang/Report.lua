-- 23-09-2023 @author Swarg
local class = require("oop.class")

local su = require 'env.sutil'

-- Goal: intermediary(proxy) in the cain Editor(nvim) <-> report producer
--       interactive line-by-line output to the editor as results accumulate

class.package 'env.lang'
---@class env.lang.Report: oop.Object
---@field writer function callback
local Report = class.new_class(nil, 'Report', {
  writer = nil
})


---@param fmt string
function Report:add(fmt, ...)
  local line = su.format(fmt, ...)
  if type(self.writer) == 'function' then
    self.writer(line)
  end
  return line
end

Report.defaultWriter = function(line)
  print(line)
end


class.build(Report)
return Report

-- 17-01-2025 @author Swarg
--
-- ISourceManager (Interface)
-- The task of this class is to manage and interact with the ClassDefinition -
-- the output of the JParser (parsed source code of java)
--

local log = require 'alogger'
local fs = require 'env.files'
local su = require 'env.sutil'
local Editor = require 'env.ui.Editor'
local FileIterator = require 'olua.util.FileIterator'
local ClassDefinition = require 'env.lang.oop.ClassDefinition'

local class = require 'oop.class'

class.package 'env.lang'
---@class env.lang.Sourcer : oop.Object
---@field new fun(self, o:table?, lang:env.lang.Lang): env.lang.Sourcer
---@field lang env.lang.Lang
---@field map table classname->ClassDefinition (from impl of the AParser)
---@field rename_pkg_mapping table? old-pkg-name --> new-pkg-name
local C = class.new_class(nil, 'Sourcer', {
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
-- local is_empty_str = ucmd.is_empty_str
local starts_with = su.starts_with
local file_exists, join_path = fs.file_exists, fs.join_path
local ends_with = su.ends_with

--
-- constructor
--
-- local sourcer = Sourcer:new(nil, lang)
--
---param lang env.lang.Lang
function C:_init(lang)
  self.lang = lang

  self.map = {}                 -- (string) classname -> ClassDefintion
  self.rename_pkg_mapping = nil -- use self:getRenamePackageMapping()
end

---@return olua.util.lang.AParser
function C:getParserClass() error 'abstract' end

--
---@param path string?
---@param stop_predicate function?
---@param lines table?
---@return env.lang.oop.ClassDefinition?
---@return string? errmsg
function C:loadClassDef(path, stop_predicate, lines)
  if not path then return nil, 'no path' end

  lines = lines or {}
  local iter = FileIterator:new(nil, path, lines)
  local parser = self:getParserClass():new(nil, iter):prepare(stop_predicate);
  pcall(FileIterator.free, iter)  -- iter:free() - to close opened file

  local data = parser:getResult() -- parser.clazz
  if not data then
    return nil, 'cannot parse source code: ' .. v2s(path)
  end
  local stop_info = nil
  if type(stop_predicate) == 'function' then
    stop_info = parser:getStopInfo(stop_predicate)
  end

  return ClassDefinition:new(nil, data, path, stop_info)
end

---@param classname string
---@param path string?
---@param stop_predicate function
---@return env.lang.oop.ClassDefinition?
---@return string?
---@diagnostic disable-next-line: unused-local
function C:getClassDef(classname, path, stop_predicate) error 'abstract' end

--
-- parse given ci to extract classname from "A extends B"
--
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo?
---@return boolean? has_parent
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function C:getParentClass(ci) error 'abstract' end

---@param path string
---@diagnostic disable-next-line: unused-local
function C:fixPackageInClassFile(path) error 'abstract' end

---@return string?
---@diagnostic disable-next-line: unused-local
function C:getInnerPathFromSource(path) error 'abstract' end

--
-- update the content of the file by given path and line-number mappings
--
-- replace specified lines in the original file with lines from mapping
--
---@param path string
---@param mapping table
---@return boolean
---@return string? errmsg
function C:updateFileContent(path, mapping) -- lnum, lines)
  local empty_mapping = type(mapping) ~= 'table' or not next(mapping)
  log_debug("updateFileContent has-mapping:%s %s", not empty_mapping, path)

  if empty_mapping then
    return false, 'empty mapping to update the content of the file'
  end

  local lines, lnum, has_changes = {}, 0, false

  -- read original file
  local h, err = io.open(path, 'rb')
  if not h then return false, err end

  while true do
    lnum = lnum + 1
    local orig_line = h:read("*l")
    if not orig_line then
      break
    end
    local updated_line = mapping[lnum] -- line(s) to replace original line
    if updated_line == nil then
      lines[#lines + 1] = orig_line
    else
      local typ = type(updated_line)
      if typ == 'string' then
        lines[#lines + 1] = updated_line
        has_changes = true
        --
      elseif typ == 'table' then
        -- to replace original line to given lines
        for _, line0 in ipairs(updated_line) do
          lines[#lines + 1] = line0
        end
        has_changes = true
        --
      elseif updated_line == false then -- to delete the original line
        -- do not add original line (delete effect)
        has_changes = true
      end
    end
  end
  assert(h:close())

  -- write to the same file
  if not has_changes then
    return false, 'no changes'
  end

  h, err = io.open(path, 'wb')
  if not h then return false, err end

  h:write(table.concat(lines, "\n") .. "\n")

  if type(mapping.append) == 'table' then
    h:write(table.concat(mapping.append, "\n") .. "\n")
  end
  assert(h:close())

  return true -- file was updated
end

----

local apply_pkg_mapping

--
-- Lang:getInnerPath (alloy to work only with paths in the project_root)
--
---@param path string
---@return string? inner_path
---@return string? errmsg
---@return string? ext
function C:getInnerPath(path, src_dir)
  src_dir = src_dir or self.lang:getSrcPath() -- like src/main/java

  local i = string.find(path, src_dir, 1, true)
  if not i then
    local test_dir = self.lang:getTestPath()
    i = string.find(path, test_dir or src_dir, 1, true)
    if not i then
      return nil, 'cannot determine the source root of ' .. v2s(path)
          .. ' srcdir:' .. v2s(src_dir) .. ' testdir:' .. v2s(test_dir)
    end
    src_dir = test_dir
  end

  -- /path/to/proj/src/File.ext  -->   src/File.ext
  local inner_path = path:sub(i, #path)
  local ext = nil
  inner_path, ext = match(inner_path, '^(.-)%.([%w_]+)$')

  return inner_path, nil, ext
end

--
-- add(move) already exists source file from another project to the current one
--
-- with package name fixing (when changes the inner_path)
--
-- when adding an external file, the package rename mapping
-- (Sourcer.rename_pkg_mapping) will be automatically applied.
-- Sourcer.rename_pkg_mapping must be specified in advance via command
-- `EnvRefactor pm add old.pkg.name new.name'
--
---@param path string absolute path to source file in another project
---@param opts? table{quiet, fix_pkg}
---@return string? abs path to copied source inside project
---@return string?
function C:addExternalSourceFile(path, opts)
  local proj_root = self.lang ~= nil and self.lang:getProjectRoot() or 'nil-lang'
  log_debug("addExternalSourceFile", path, proj_root)
  opts = opts or E

  if not path or path == '' then
    return nil, 'no source-file to add'
  end
  if starts_with(path, proj_root) then
    return nil, 'cannot add source-file from the same project'
  end
  if not path or not file_exists(path) then
    return nil, 'not found ' .. v2s(path)
  end

  local inner_path, err, ext = self:getInnerPath(path)
  if not inner_path then
    -- ask the user the root of source
    local v = nil
    if opts.source_root then
      log_debug('pick source-root from opt:', opts.source_root)
      v = opts.source_root
    elseif not opts.quiet then
      v = Editor.askValue("source root:", fs.get_parent_dirpath(path))
    end
    if v and v ~= '' and fs.dir_exists(v) then
      inner_path = join_path(self.lang:getSrcPath(), path:sub(#v, #path))
      ext = nil
    end
    if not inner_path then
      return nil, err
    end
  end

  local new_inner_path
  new_inner_path = apply_pkg_mapping(self.rename_pkg_mapping, inner_path)
  if ext ~= nil and ext ~= '' then
    new_inner_path = new_inner_path .. '.' .. v2s(ext)
  end

  if not opts.quiet then
    new_inner_path = Editor.askValue('Copy to: ', new_inner_path) or ''
  end
  if not new_inner_path or new_inner_path == '' then
    return nil, 'canceled'
  end

  if opts.fix_pkg or ends_with(new_inner_path, " fix-pkg") then
    inner_path, err, new_inner_path = self:restoreInnerPathFromSource(path, opts)
    if not inner_path then return nil, err end
  end

  log_debug('inner_path:%s new_inner_path:%s', inner_path, new_inner_path)
  local new_path = join_path(proj_root, new_inner_path)
  if file_exists(new_path) then
    return nil, 'file already exists: ' .. v2s(new_path)
  end

  -- copy or move given sourcefile to specified place
  fs.mkdir(fs.get_parent_dirpath(new_path) or proj_root)

  if opts.move == true then
    os.rename(path, new_path)
    if not file_exists(new_path) then
      return nil, 'cannot move to ' .. v2s(new_path)
    end
  else
    if not fs.copy(path, new_path) then
      return nil, 'cannot copy to ' .. v2s(new_path)
    end
  end

  local need_to_fix_package = new_inner_path ~= inner_path
  log_debug("need to fix package: %s", need_to_fix_package)
  if need_to_fix_package then -- if package was changed
    local ok, err2 = self:fixPackageInClassFile(new_path)
    log_debug("fixPackageInClassFile:%s %s", ok, err2)
  end

  return new_path, nil
end

--
-- to fix new_inner_path by package in the copied java file
-- (e.g. to copy decompiled output file without package subdirs)
--
---@return string? inner-path
---@return string? err
---@return string? new-inner-path
function C:restoreInnerPathFromSource(path, opts)
  local inner_path, new_inner_path
  local fixed_ipath = self:getInnerPathFromSource(path)
  if not fixed_ipath then
    return nil, 'cannot fix package for ' .. v2s(path)
  end
  inner_path = join_path(self.lang:getSrcPath(), fixed_ipath) .. '.java'
  new_inner_path = apply_pkg_mapping(self.rename_pkg_mapping, inner_path)
  if not opts.quiet then
    new_inner_path = Editor.askValue('Copy to: ', new_inner_path) or ''
  end
  if not new_inner_path or new_inner_path == '' then
    return nil, 'canceled'
  end
  return inner_path, nil, new_inner_path
end

--

---@return table
function C:getRenamePackageMapping()
  self.rename_pkg_mapping = self.rename_pkg_mapping or {}
  return self.rename_pkg_mapping
end

function C:clearRenamePackageMapping()
  self.rename_pkg_mapping = {}
end

-- for EnvRefactor
--
---@param map table?
---@param inner_path string
function apply_pkg_mapping(map, inner_path)
  local cnt = nil
  if inner_path and type(map) == 'table' then
    cnt = 0
    for from, to in pairs(map) do
      cnt = cnt + 1
      local i = string.find(inner_path, from, 1, true)
      if i then
        log.debug("apply_pkg_mapping", from, to, inner_path)
        inner_path = inner_path:gsub(from, to)
        break
      end
    end
  end

  log.debug("apply_pkg_mapping", cnt, inner_path)
  return inner_path
end

---@param fn string
---@param can_rewrite boolean?
---@return number?
---@return string? errmsg
function C:saveRenamePackageMapping(fn, can_rewrite)
  if not can_rewrite and fs.file_exists(fn) then
    return nil, "file already exists: " .. v2s(fn)
  end
  local keys = {}
  for k, _ in pairs(self.rename_pkg_mapping) do keys[#keys + 1] = k end
  table.sort(keys)

  local t = {}
  for _, key in pairs(keys) do
    t[#t + 1] = v2s(key) .. ':' .. v2s(self.rename_pkg_mapping[key])
  end
  t[#t + 1] = "";

  -- write to file
  local h, err = io.open(fn, "wb")
  if not h then
    return nil, err
  end
  assert(h:write(table.concat(t, "\n")))
  assert(h:close())

  return #t - 1
end

local function merge_rename_pkg_mapping(self, mapping)
  assert(type(mapping) == 'table', 'mapping')
  self.rename_pkg_mapping = self.rename_pkg_mapping or {}
  for k, v in pairs(mapping) do
    self.rename_pkg_mapping[k] = v
  end
end

---@param fn string
---@param replace_existed boolean?
---@return number?
---@return string? errmsg
function C:loadRenamePackageMapping(fn, replace_existed)
  if not fs.file_exists(fn) then
    return nil, "file not found: " .. v2s(fn)
  end
  local h, err = io.open(fn, "rb")
  if not h then
    return nil, err
  end
  local mapping = {}
  local c = 0
  while true do
    local line = h:read("*l")
    if not line then
      break
    end
    local old, new = match(line, '^([^#].-)%s*:%s*(.-)%s*$')
    if old and new and old ~= '' and new ~= '' then
      mapping[old] = new
      c = c + 1
    end
  end
  assert(h:close())

  if not replace_existed then -- merge
    self.rename_pkg_mapping = self.rename_pkg_mapping or {}
    merge_rename_pkg_mapping(mapping)
  else
    self.rename_pkg_mapping = mapping
  end

  return c
end

---@return string? from
---@return string? to
local function parse_git_status_rename_line(line)
  local from, to
  from, to = match(line, "^%s*renamed:%s*(.-)%s*%->%s*(.-)%s*$")
  return from, to
end

function C:getClassNameFromInnerPath(s)
  local ipath = match(s, '^(.-)%.[^%.]+$') -- without extension
  if ipath then
    return self.lang:getClassNameForInnerPath(ipath)
    -- local src = self.lang:getSrcPath()
    -- if su.starts_with(ipath, src) then
    --   return ipath:sub(#src, #ipath)
    -- end
  end
  return s
end

--
-- parse output of `git status` to build rename package mapping
---@param lines table? output from git status in project root
---@return number?
---@return string? errmsg
function C:addRenamePkgMappingFromGitStatus(lines)
  lines = lines or fs.execrl("git status")
  if type(lines) ~= 'table' then
    return nil, 'no output from git status'
  end

  local mapping = {}
  local c = 0;
  if lines then
    for _, line in ipairs(lines) do
      local from, to = parse_git_status_rename_line(line)
      if from and from ~= '' and to then
        local from0 = self:getClassNameFromInnerPath(from)
        local to0 = self:getClassNameFromInnerPath(to)
        if from0 and to0 then
          mapping[from0] = to0
          c = c + 1
        end
      end
    end
  end
  merge_rename_pkg_mapping(self, mapping)
  return c
end

--

if _G.TEST then
  C.test = {
    apply_pkg_mapping = apply_pkg_mapping,
    parse_git_status_rename_line = parse_git_status_rename_line,
  }
end

class.build(C)
return C

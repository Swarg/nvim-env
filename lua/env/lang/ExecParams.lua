-- 06-08-2024 @author Swarg
--
-- simple wrapper around table with cwd, cmd, args, envs to run system command
-- used by
--  - env.lang.Lang:newExecParams
--  - env.ui.Editor.run ...
--

local log = require 'alogger'
local class = require 'oop.class'

local log_debug = log.debug
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


class.package 'env.lang'
---@class env.lang.ExecParams : oop.Object
---@field new fun(self, o:table?, cwd:string?, cmd:string?, args:table?, envs:table?, desc:string?): env.lang.ExecParams
---@field cwd string?
---@field cmd string? executable system command
---@field args table?
---@field envs table?
---@field opts table?
---@field desc string? -- description for debugging and logging
---@field callback function?
---@field isValid fun(self, msg): boolean
---@field bufnr number? bufnr with output
local C = class.new_class(nil, 'ExecParams', {
})

---@param cwd string?
---@param cmd string?
---@param args table?
---@param envs table?
---@param desc string?
function C:_init(cwd, cmd, args, envs, desc)
  self.cwd = cwd or self.cwd
  self.cmd = cmd or self.cmd
  self.args = args or self.args
  self.envs = envs or self.envs
  self.desc = desc or self.desc
  -- self.callback = nil,
  -- self.bufnr = nil -- buffer with output
end

---@param cwd string?
---@param cmd string?
---@param args table?
---@param envs table?
---@param desc string?
function C.of(cwd, cmd, args, envs, desc)
  return C:new(nil, cwd, cmd, args, envs, desc)
end

---@param cwd string?
---@param self self
---@return self
function C:withCwd(cwd)
  assert(type(cwd) == 'string' and cwd ~= '', 'expected not empty string cwd')
  self.cwd = cwd
  return self
end

---@param args table
---@param self self
---@return self
function C:withArgs(args)
  self.args = args
  return self
end

---@param opts table
---@param self self
---@return self
function C:withOpts(opts)
  self.opts = opts
  return self
end

function C:withDesc(desc)
  self.desc = desc
  return self
end

function C:withCallback(callback)
  self.callback = callback
  return self
end

--
-- Checks that has params to execure system command:
-- cwd, cmd and args
-- or cwd args  and callback
--
-- old "canExecute"
--
---@param msg string
---param cwd string?
---param cmd string|function?
---param args table|string|nil
---@return boolean
function C:isValid(msg)
  if type(self) ~= 'table' then
    error('expected params:table{cwd, cmd, args} got:' .. type(self))
  end
  msg = msg or self.desc

  local cmdtyp = type(self.cmd)
  local cbtyp = type(self.callback)
  local hasCmd = (cmdtyp == 'string' and self.cmd ~= '')
  local hasCmdOrCallback = hasCmd or cbtyp == 'function'

  if not hasCmdOrCallback then -- todo only in dev-mode
    error(fmt('expected cmd:string or callback:function got: cmd:"%s" cb:%s',
      v2s(self.cmd), type(self.callback)))
  end

  if not self.cwd or not hasCmdOrCallback or not self.args then
    log_debug('Bad params %s expected (cwd,cmd|callback,args) got:%s', msg, self)
    return false
  end
  return true
end

---@param self self
---@param bname string?
---@return self
function C:validate(bname)
  if type(self) ~= 'table' then
    error('Expected ExecParams for "' .. v2s(bname) .. '" got' .. type(self))
  end
  return self
end

function C:__tostring()
  if type(self) == 'table' then
    local envs = (self.envs ~= nil) and log.format('%s', self.envs) or ''
    local opts = (self.opts ~= nil) and log.format('%s', self.opts) or ''

    local s = fmt('cwd:%s cmd:%s args:[%s] envs:%s desc:%s opts:%s callback:%s',
      v2s(self.cwd), v2s(self.cmd), table.concat(self.args or {}, ' '), envs,
      v2s(self.desc or ''), opts, type(self.callback))
    return s
  end
  return type(self)
end

return C

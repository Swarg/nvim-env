-- 06-08-2024 @author Swarg
--
-- Goal: Test output handler from running tests for displaying in diagnostics
-- in the Nvim buffer

local log = require 'alogger'
local class = require 'oop.class'


class.package 'env.lang'
---@class env.lang.TestDiagnost : oop.Object
---@field new fun(self, o:table?, lang:env.lang.Lang, namespace:string): env.lang.TestDiagnost
---@field namespace_id number
---@field namespace string
---@field lang env.lang.Lang
local C = class.new_class(nil, 'TestDiagnost', {
})

local log_debug = log.debug
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


-- Constructor
--
---@param namespace string
---@param lang table
function C:_init(lang, namespace)
  log_debug("_init", lang ~= nil, namespace)
  assert(type(lang) == 'table', 'lang')
  assert(type(namespace) == 'string', 'namespace')

  self.lang = lang
  self.namespace = "NVIM_ENV_" .. namespace

  if vim and vim.api and vim.api.nvim_create_namespace then
    -- Creates a new *namespace* or gets an existing one.
    self.namespace_id = vim.api.nvim_create_namespace(namespace)
    log_debug('Namespace:"%s" ID:%s', namespace, self.namespace_id)
  else
    self.namespace_id = 2 -- for testing
  end
end

---@return table? dlist
---@return number bufnr
---@return table cur_pos?
function C:getBufDiagnostic()
  local api = vim.api
  local bufnr, cur_pos = api.nvim_get_current_buf(), api.nvim_win_get_cursor(0)
  local dlist = vim.diagnostic.get(0, { namespace = self.namespace_id })
  return dlist, bufnr, cur_pos
end

---@param bufnr number
---@param diagnostic table?
function C:setBufDiagnostic(bufnr, diagnostic)
  local id = self.namespace_id
  log_debug("setBufDiagnostic %s buf:%s has:%s", id, bufnr, diagnostic ~= nil)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(self.namespace_id) == 'number', 'namespace_id')

  diagnostic = diagnostic or {}
  vim.diagnostic.set(self.namespace_id, bufnr, diagnostic)
end

--
---@param diagnostics table
function C:showAllDiagnostics(diagnostics)
  log_debug("showAllDiagnostics %s %s", self.namespace_id, self.namespace)
  assert(type(self.namespace_id) == 'number', 'namespace_id')

  if diagnostics then
    local i = 0
    for bufnr, bufnr_diagnostics in pairs(diagnostics) do
      vim.diagnostic.set(self.namespace_id, bufnr, bufnr_diagnostics)
      i = i + 1
    end
    log_debug('a number of diagnostics passed to nvim %s', i)
  end
end

--
-- to process output from test framework and show it in nvim.Diagnostics
--
---@param output string
---@param pp table
---@param exit_ok boolean
---@param signal number
---@diagnostic disable-next-line: unused-local
function C:processSingleTestOutput(output, pp, exit_ok, signal)
  error('abstract')
end

--
-- substitute the resulting value into the current test context taken
-- from the test nvim.diagnostics (in current line)
--
---@return boolean
---@return string? errmsg
function C:usePassedIn() error('abstract') end

--
-- update the test diagnostic from the report-files without re-run tests
--
---@param w Cmd4Lua
---@return boolean
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function C:updateTestDiagnostic(w) error 'abstract' end

--
---@return table - enriched args
---@param params env.lang.ExecParams
---@diagnostic disable-next-line: unused-local
function C:enrichedArgsRunSingleTest(params) error('abstract') end

--
-- enrich params to run single test file for specific test framework
--
-- add a callback that will start parsing the output of the test results
-- and displaying them in nvim diagnostics
--
---@param params env.lang.ExecParams
---@return env.lang.ExecParams
function C:enrichParamsRunSingleTest(params)
  log_debug("enrichParamsRunSingleTest", params)
  local bufnr = params.bufnr or self.lang:getCurrentBufnr() or 0

  log_debug('clear prev Diagnostics for bufnr:', bufnr)
  self:setBufDiagnostic(bufnr, nil) -- clear prev diagnostic for current buffer

  return params:withArgs(self:enrichedArgsRunSingleTest(params)):withOpts({
    src_bufnr = bufnr,
    handler_on_close = function(output, pp, exit_ok, signal)
      log_debug("handler_on_close")
      self:processSingleTestOutput(output, pp, exit_ok, signal)
    end
  })
end

class.build(C)
return C

-- 15-07-2024 @author Swarg
local M = {}

M.LEXEME_TYPE = {
  EMPTY       = 0,
  UNKNOWN     = 1,
  SPACE       = 2,
  KEYWORD     = 3,
  OPERATOR    = 4,
  IDENTIFIER  = 5, -- name of class filed, name of method, variable name, ..
  TYPE        = 6, -- Map.Entry<String,Object> Entry<K,V>
  NUMBER      = 7,
  STRING      = 8,
  ARRAY       = 9,
  FUNCTION    = 10, -- method()
  METHOD      = 11, -- self:doStuff(param)
  CLASS       = 12, -- MyClass:new()   class name (type) without subtypes E<K,V>
  ANNOTATION  = 13, -- @Autowired
  CLASSFIELD  = 14, -- this.fieldName or self.field_name
  COMMENT     = 15, -- this.fieldName or self.field_name
  OPSEP       = 16, -- ;
  BLOCK_OPEN  = 17, -- {
  BLOCK_CLOSE = 18, -- }
  BRACKET     = 19, -- ()[]<>
  VARNAME     = 20, -- identifier? todo remove?
  -- extra
  TAG_SINGLE  = 21, -- <Compoment att=42 /> xml, html, svetle, react
  TAG_PAIRED  = 22, -- <tag ..> </tag>
  ATTRIBUTE   = 23, -- <tag att=42 ..
  COMPONENT   = 24, -- import Inner from './Inner.svelte'  -- class?
}

local LT = M.LEXEME_TYPE

M.LEXEME_NAMES = {
  [LT.EMPTY]       = 'EMPTY',
  [LT.UNKNOWN]     = 'UNKNOWN',
  [LT.SPACE]       = 'SPACE',
  [LT.KEYWORD]     = 'KEYWORD',
  [LT.OPERATOR]    = 'OP',
  [LT.IDENTIFIER]  = 'ID',
  [LT.VARNAME]     = 'VARNAME',
  [LT.TYPE]        = 'TYPE',
  [LT.NUMBER]      = 'NUMBER',
  [LT.STRING]      = 'STRING',
  [LT.ARRAY]       = 'ARRAY',
  [LT.FUNCTION]    = 'FUNCTION',
  [LT.METHOD]      = 'METHOD',
  [LT.CLASS]       = 'CLASS',
  [LT.CLASSFIELD]  = 'CLASSFIELD',
  [LT.COMMENT]     = 'COMMENT',
  [LT.OPSEP]       = 'OPSEP',
  [LT.BLOCK_OPEN]  = 'BOPEN',
  [LT.BLOCK_CLOSE] = 'BCLOSE',
  [LT.BRACKET]     = 'BRACKET',
  [LT.TAG_SINGLE]  = 'TAG_SINGLE',
  [LT.TAG_PAIRED]  = 'TAG_PAIRED',
  [LT.ATTRIBUTE]   = 'ATTRIBUTE',

}

return M

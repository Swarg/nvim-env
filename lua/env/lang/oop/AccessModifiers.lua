-- 30-09-2023 @author Swarg
local AccessModifiers = {}
--

AccessModifiers.ID = {
  PRIVATE = 1,
  PUBLIC = 2,
  PROTECTED = 3,
  PRIVATE_STATIC = 4,
  PUBLIC_STATIC = 5,
  PROTECTED_STATIC = 6,
}

AccessModifiers.NAMES = {
  'private',          -- 1,
  'public',           -- 2,
  'protected',        -- 3,
  'private static',   -- 4,
  'public static',    -- 5,
  'protected static', -- 6,
}

---@param s string 'private'|'public'|'public_static' etc
---@return number?
---@param default number|nil
function AccessModifiers.getAccessModifierId(s, default)
  if s then
    s = string.upper(s)
    return AccessModifiers.ID[s]
  end
  return default -- AccessModifiers.ID.PRIVATE
end

---@param id number
---@param default string|nil
---@return string?
function AccessModifiers.getAccessModifier(id, default)
  if type(id) == 'number' and id > 0 and id <= #AccessModifiers.NAMES then
    return AccessModifiers.NAMES[id]
  end
  return default -- ACCESS_MODIFIERS_NAMES[AccessModifiers.PRIVATE]
end

return AccessModifiers

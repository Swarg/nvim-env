-- 30-09-2023 @author Swarg
local class = require("oop.class")

local AMods = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line: unused-local
local Field = require("env.lang.oop.Field")

class.package 'env.lang.oop'
---@class env.lang.oop.Method: oop.Object
---@field mtype number        -- method type:  constructor, getter, ...
---@field amod number         -- access_modifiers:  public, private, ..
---@field name string
---@field params table<string, env.lang.oop.Field>?
---@field fields table<string, env.lang.oop.Field>?
---@field rettype string
---@field retpref string? def is ''
---@field body string?
local Method = class.new_class(nil, 'Method', {
  amod    = AMods.ID.PUBLIC,
  mtype   = 0,      -- method_type UNKNOWN
  name    = nil,
  params  = nil,    -- {filed.name = field}
  retpref = nil,    -- for lang in which: func name(): rettype
  rettype = 'void', --                               ^^
})

Method.TYPE = {
  UNKNOWN = 0,
  CONSTRUCTOR = 1,
  GETTER = 2,
  SETTER = 3,
  EQUALS = 4,
  CUSTOM = 5,
  TEST = 6,
}

function Method:getMethodType()
  return self.mtype or Method.TYPE.UNKNOWN
end

-- for substitute in template via templater.substitute
---@return table
function Method.getDefaultProps()
  return {
    ACESSMOD = AMods.getAccessModifier(AMods.ID.PUBLIC),
    NAME = 'method',
    PARAMS = '',
    RETURN_TYPE = 'void',
    RETURN_PREF = '',
    BODY = '',
  }
end

-- for testing
function Method:toArray()
  local t = {
    mtype   = self.mtype,
    amod    = self.amod,
    name    = self.name,
    params  = nil,
    rettype = self.rettype,
    retpref = self.retpref,
    body    = self.body,
    -- classinfo?
  }
  if self.params then
    t.params = {}
    for name, field in pairs(self.params) do
      t.params[name] = field:toArray()
    end
  end
  return t
end

-- testing
function Method.toListOfArrays(methods)
  if methods then
    local t = {}
    for _, m in pairs(methods) do
      table.insert(t, m:toArray())
    end
    return t
  end
  return nil
end

class.build(Method)
return Method

-- 09-10-2023 @author Swarg
local class = require("oop.class")

---@diagnostic disable-next-line: unused-local
local AMods = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line: unused-local
local Field = require("env.lang.oop.Field")
local Method = require("env.lang.oop.Method")

class.package 'env.lang.oop'
---@class env.lang.oop.Class : oop.Object
---@field name string
---@field package string                -- namespace
---@field imports table<string>?
---@field extends string?
---@field implements table<string>?
---@field fields table<string, env.lang.oop.Field>?  -- name -> Filed
---@field methods table<env.lang.oop.Method>?
local Class = class.new_class(nil, 'Class', {
})

---@return env.lang.oop.Class
function Class:addField(f)
  if f and class.instanceof(f, Field) then
    assert(f.name, 'has field name')
    self.fields = self.fields or {}
    self.fields[f.name] = f
  end
  return self
end

---@return env.lang.oop.Class
function Class:addMethod(m)
  if m and class.instanceof(m, Method) then
    self.methods = self.methods or {}
    -- todo check is already existed?
    table.insert(self.methods, m)
  end
  return self
end

---@return env.lang.oop.Class
function Class:addMethods(list)
  if list then
    for _, m in pairs(list) do
      self:addMethod(m)
    end
  end
  return self
end

---@return env.lang.oop.Class
function Class:addFields(list)
  if list then
    for _, f in pairs(list) do
      self:addField(f)
    end
  end
  return self
end

--
-- for testing
function Class:toArray()
  local t = {
    name    = self.name,
    package = self.package,
  }
  if self.fields then
    t.fields = {}
    for name, field in pairs(self.fields) do
      t.fields[name] = field:toArray()
    end
  end
  if self.methods then
    t.methods = Method.toListOfArrays(self.methods)
  end
  return t
end

class.build(Class)
return Class

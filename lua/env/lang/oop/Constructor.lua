-- 30-09-2023 @author Swarg

local class = require("oop.class")

local AccessModifiers = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line: unused-local
local Field = require("env.lang.oop.Field")
local Method = require("env.lang.oop.Method")

class.package 'env.lang.oop'
---@class env.lang.oop.Constructor : env.lang.oop.Method
---@field classinfo env.lang.ClassInfo
---@field amod number  -- access_mod
---@field name string
---@field params table<string, env.lang.oop.Field>
---@field opts table
local Constructor = class.new_class(Method, 'Constructor', {
  -- lang    = nil,
  classinfo = nil,
  amod      = AccessModifiers.ID.PUBLIC,
  name      = nil, -- java - ShortClassName, php - '__constructor'
  params    = nil, -- {filed.name = field}
  opts      = nil, -- addition checks inside body: AssertNotNull srttolower
})


class.build(Constructor)
return Constructor

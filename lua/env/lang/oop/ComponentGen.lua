-- 30-09-2023 @author Swarg

local class = require 'oop.class'


class.package 'env.lang.oop'
---@class env.lang.oop.ComponentGen: oop.Object
---@field gen env.lang.oop.LangGen
---@field classinfo env.lang.ClassInfo
local ComponentGen = class.new_class(nil, 'ComponentGen', {
  classinfo = nil
})

ComponentGen.debug = true

---@return env.lang.ClassInfo
function ComponentGen:getClassInfo()
  return self.classinfo
end

function ComponentGen:setClassInfo(ci)
  self.classinfo = ci
  return self
end

-- return empty table if has no the LangGen in the classinfo
---@return env.lang.oop.LangGen
function ComponentGen:getLangGen()
  return self.gen
end

--
-- return empty table if has no Lang in classinfo
---@return env.lang.Lang?
function ComponentGen:getLang()
  return self.gen ~= nil and self.gen:getLang() or nil
end

function ComponentGen:getLangGenAndClassInfo()
  return self.gen or false, self.classinfo
end

-- ?
-- function ComponentGen:bindTo(comp)
--   if comp and Object.instanceof(comp, ComponentGen) then
--     self.classinfo = comp:getClassInfo()
--   else
--     error('[DEBUG] Illegal Class ComponentGen expected.')
--   end
--   return self
-- end

-- has classinfo and langGen on it
function ComponentGen:canWork()
  if ComponentGen.debug then
    if not self.classinfo then
      error('[DEBUG] No ClassInfo!')
    elseif not self.gen then
      error('[DEBUG] No LangGen')
    else
      return true
    end
  end
  return self.classinfo and self.gen ~= nil
end

-- Identation
function ComponentGen:getTab()
  return self.gen and self.gen:getTab() or '    ';
end

-- pref + name  -->  prefName
function ComponentGen.getNameWithPreffix(prefix, field_name)
  assert(field_name ~= nil, 'field_name')
  prefix = prefix or ''
  return prefix .. string.upper(field_name:sub(1, 1)) .. field_name:sub(2, -1)
end

--
-- set empty string to false to remove on substitute()
---@param s string|boolean
---@return string|boolean
function ComponentGen.notEmptyOrFalse(s)
  return s and s ~= '' and s or false
end

class.build(ComponentGen)
return ComponentGen

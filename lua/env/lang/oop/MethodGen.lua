-- 30-09-2023 @author Swarg
local class = require("oop.class")

local su = require('env.sutil')

local ComponentGen = require("env.lang.oop.ComponentGen")
local AMods = require("env.lang.oop.AccessModifiers")
local Field = require("env.lang.oop.Field")
local Method = require("env.lang.oop.Method")
local Constructor = require("env.lang.oop.Constructor")
local TU = require 'env.lang.utils.templater'


class.package 'env.lang.oop'
---@class env.lang.oop.MethodGen: env.lang.oop.ComponentGen
local MethodGen = class.new_class(ComponentGen, 'MethodGen', {
})

local E = {}

---@return string|nil
function MethodGen:getMethodTemplate()
  if self:getLangGen() then
    return self:getLangGen():getClassTemplate(self.classinfo)
  end
end

function MethodGen.validateMethods(methods)
  assert(type(methods) == 'table', 'fields')
  for _, m in pairs(methods) do
    assert(class.instanceof(m, Method), 'must be an instance of Method')
  end
end

---@param field env.lang.oop.Field
---@param name string|nil  a custom name for the method if nil - take from filed.name
---@return env.lang.oop.Method?
function MethodGen:genGetter(field, name)
  if field then
    local gname = name
    -- if name not defined or name == filed.name
    if field.name and (not name or name == field.name) then
      gname = self.getNameWithPreffix('get', field.name)
    end
    return Method:new({
      mtype     = Method.TYPE.GETTER,
      classinfo = self:getClassInfo(), -- ??
      amod      = AMods.ID.PUBLIC,
      name      = gname,
      params    = nil,
      fields    = { field },
      rettype   = field.type,
    })
  end
end

---@param fields table<string, env.lang.oop.Field>
---@param out table?
---@return table<env.lang.oop.Method>?
function MethodGen:genGetters(fields, out)
  if self:canWork() then
    out = out or {}
    for name, field in pairs(fields) do
      local getter = self:genGetter(field, name)
      table.insert(out, getter)
    end
    return out
  end
end

---@param field env.lang.oop.Field
---@param name string|nil  if specified it will be the full name ot the method
---@return env.lang.oop.Method?
function MethodGen:genSetter(field, name)
  if field and not field.read_only then
    local gname = name
    -- if name not defined or name == filed.name
    if field.name and (not name or name == field.name) then
      gname = self.getNameWithPreffix('set', field.name)
    end
    return Method:new({
      mtype     = Method.TYPE.SETTER,
      classinfo = self:getClassInfo(),
      amod      = AMods.ID.PUBLIC,
      name      = gname,
      params    = { field },
      fields    = { field },
      rettype   = 'void',
    })
  end
end

---@param out table?
function MethodGen:genSetters(fields, out)
  if self:canWork() then --?
    out = out or {}
    for name, field in pairs(fields) do ---@cast field env.lang.oop.Field
      if not field.read_only then
        local setter = self:genSetter(field, name)
        table.insert(out, setter)
      end
    end
    return out
  end
end

--
---@param opts table {params[,amod]}
function MethodGen:genConstructor(opts)
  assert(type(opts) == 'table', 'opts')

  local gen, ci = self:getLangGenAndClassInfo()
  if gen and ci and opts then
    return Constructor:new({
      classinfo = ci,
      mtype     = Method.TYPE.CONSTRUCTOR,
      amod      = opts.amod or AMods.ID.PUBLIC,
      name      = gen:getConstructorName(ci),
      params    = opts.params,
      rettype   = nil,
    })
  end
end

---@param opts_list table<table>
---@param t table?
function MethodGen:genConstructors(opts_list, t)
  if opts_list then
    t = t or {}
    for _, opts in pairs(opts_list) do
      local constructor = self:genConstructor(opts)
      if constructor then
        table.insert(t, constructor)
      end
    end
    return t
  end
end

---@param fields env.lang.oop.Field[]
function MethodGen:genIsEqualTo(fields, name)
  local gen, ci = self:getLangGenAndClassInfo()
  if gen and ci and fields then
    return Method:new({
      mtype     = Method.TYPE.EQUALS,
      classinfo = self.classinfo,
      amod      = AMods.ID.PUBLIC,
      name      = name or 'isEqualTo',
      params    = { Field.ofClassInfo(ci) },
      fields    = fields,
      rettype   = gen:getKWordBoolean()
    })
  end
end

function MethodGen:genCustom(amod, name, params, rettype, body)
  local gen, ci = self:getLangGenAndClassInfo()
  if gen and ci and name then
    return Method:new({
      mtype     = Method.TYPE.CUSTOM,
      classinfo = ci,
      amod      = amod or AMods.ID.PUBLIC,
      name      = name,
      params    = params,
      fields    = nil,
      rettype   = rettype,
      body      = body or '',
    })
  end
end

function MethodGen:genTest(name, params, body)
  local gen, ci = self:getLangGenAndClassInfo()
  if gen and ci and name then
    return Method:new({
      mtype     = Method.TYPE.TEST,
      classinfo = ci,
      amod      = AMods.ID.PUBLIC,
      name      = name,
      params    = params or nil,
      fields    = nil,
      rettype   = nil,
      body      = body or '',
    })
  end
end

--- method to code
---@param m env.lang.oop.Method?
function MethodGen:toCode(m)
  if m then
    local type = m:getMethodType()
    if type == Method.TYPE.CONSTRUCTOR then
      return self:genConstructorCode(m)
      --
    elseif type == Method.TYPE.GETTER then
      return self:genGetterCode(m)
      --
    elseif type == Method.TYPE.SETTER then
      return self:genSetterCode(m)
      --
    elseif type == Method.TYPE.EQUALS then
      return self:genIsEqualToCode(m)
      --
    elseif type == Method.TYPE.TEST then
      return self:genTestCode(m)
      --
    elseif type == Method.TYPE.CUSTOM then
      return self:genCustomCode(m)
    else
      error('[DEBUG] Not Supported type Of Method ')
    end
  end
end

function MethodGen:buildParams(m)
  local gen = self:getLangGen()
  if gen and m and m.params then
    local templ, sep = gen:getParamTemplate()
    local code, subs, i = '', {}, 0

    local varprefix = gen:getVarPrefix() or ''
    for _, field in pairs(m.params) do
      subs['TYPE'] = field.type
      subs['NAME'] = varprefix .. tostring(field.name)
      local res = TU.substitute(templ, subs)
      if res then
        if i > 0 then
          code = code .. sep .. ' '
        end
        code = code .. res
      end
      i = i + 1
    end
    return code or ''
  end
  return ''
end

---@return string
function MethodGen:buildCode(templ, m, body)
  local t = Method.getDefaultProps()
  t.ACCESSMOD = AMods.getAccessModifier(m.amod)
  t.NAME = m.name or ''
  t.PARAMS = self:buildParams(m)
  t.BODY = body or ''
  t.RETURN_TYPE = m.rettype or ''
  if t.RETURN_TYPE ~= '' then
    t.RETURN_PREF = self:getLangGen():getReturnPrefix() -- ': ' or ''
  end

  local code = TU.substitute(templ, t)
  return code or ''
end

---@param m env.lang.oop.Method
---@return string
function MethodGen:genConstructorCode(m)
  -- if not self:canWork() then return end
  local gen         = self:getLangGen()
  local templ, body = gen:getConstructorTemplate(self.classinfo), ''

  local fields      = m.fields or m.params
  -- may be empty and have neither parameters nor assignment to class fields
  if fields then
    local i = 0
    local t, this, opsep = gen:getTab(), gen:getKWordThis(), gen:getOpSep()
    local attrdot = gen:getAttrDot()
    local assign, varprefix = gen:getOpAssign(), gen:getVarPrefix()

    for _, field in pairs(fields) do
      if i > 0 then body = body .. "\n" end
      assert(field.name, 'field.name at iter:' .. i)
      body = body .. string.format("%s%s%s%s%s %s %s%s%s",
        t, t, this, attrdot, field.name, assign, varprefix, field.name, opsep
      )
      i = i + 1
    end
  end
  return self:buildCode(templ, m, body)
end

---@param m env.lang.oop.Method
---@return string
function MethodGen:genGetterCode(m)
  local gen         = self:getLangGen()
  local templ, body = gen:getMethodTemplate(self.classinfo), ''
  local field       = (m.fields or E)[1]
  -- the getter cannot be empty
  assert(field ~= nil, 'the getter must have a return class field')

  local t, this, opsep = gen:getTab(), gen:getKWordThis(), gen:getOpSep()
  local attrdot = gen:getAttrDot()

  body = string.format("%s%sreturn %s%s%s%s",
    t, t, this, attrdot, field.name, opsep
  )
  return self:buildCode(templ, m, body)
end

---@param m env.lang.oop.Method
---@return string
function MethodGen:genSetterCode(m)
  local gen         = self:getLangGen()
  local templ, body = gen:getMethodTemplate(self.classinfo), ''
  local field       = (m.fields or E)[1]
  -- the setter cannot be empty
  assert(field ~= nil, 'the setter must have a return class field')

  local t, this, opsep = gen:getTab(), gen:getKWordThis(), gen:getOpSep()
  local attrdot = gen:getAttrDot()
  local assign, varprefix = gen:getOpAssign(), gen:getVarPrefix()

  body = body .. string.format("%s%s%s%s%s %s %s%s%s",
    t, t, this, attrdot, field.name, assign, varprefix, field.name, opsep
  )

  return self:buildCode(templ, m, body)
end

--
-- public function isEqualsTo(NetworkId $networkId): bool
-- {
--     return
--         $this->id === $networkId->id &&
--         $this->token === $networkId->token;
-- }
---@param m env.lang.oop.Method
---@return string
function MethodGen:genIsEqualToCode(m)
  local gen = self:getLangGen()
  local templ, body = gen:getMethodTemplate(self.classinfo), ''
  -- the setter cannot be empty
  assert(m.fields ~= nil, 'the isEquals must have a fields to compare')

  -- local ci = self:getClassInfo()
  local t, this, opsep = gen:getTab(), gen:getKWordThis(), gen:getOpSep()
  local op_and = " " .. tostring(gen:getOpAnd())
  local adot, param = gen:getAttrDot(), m.params[1] --ci:getVarNameForClassName('$')
  local identical = gen:getOpCompareIdentical()
  local varprefix = gen:getVarPrefix()
  body = t .. t .. 'return'
  local i, fields_cnt, t3 = 1, su.table_size(m.fields), t .. t .. t

  for _, field in pairs(m.fields) do
    --         $this->id === $networkId->id &&
    local line_ends = op_and
    if i >= fields_cnt then
      line_ends = opsep
    end
    body = body .. string.format("\n%s%s%s%s %s %s%s%s%s%s",
      t3, this, adot, field.name,
      identical,
      varprefix, param.name, adot, field.name, line_ends
    )
    i = i + 1
  end

  return self:buildCode(templ, m, body)
end

function MethodGen:genTestCode(m)
  local gen = self:getLangGen()
  local templ, body = gen:getMethodTemplate(self.classinfo), m.body or ''
  return self:buildCode(templ, m, body)
end

---@diagnostic disable-next-line: unused-local
function MethodGen:genCustomCode(m)
  local gen = self:getLangGen()
  local templ, body = gen:getMethodTemplate(self.classinfo), m.body or ''
  return self:buildCode(templ, m, body)
end

class.build(MethodGen)
return MethodGen

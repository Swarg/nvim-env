-- 13-10-2023 @author Swarg
-- Goal:
--  - find, get and insert lines from doc block(comments) of source file
--  - container for codegen
--

local class = require("oop.class")
local u8 = require("env.util.utf8")

---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")

class.package 'env.lang.oop'
---@class env.lang.oop.Docblock: oop.Object
---@field gen env.lang.oop.LangGen
---@field classinfo env.lang.ClassInfo?
---@field elName string?
---@field elType number?
---@field bufnr number?
---@field lnum number?
---@field lnum_end number?
---@field lines table<string>?
---@field tab string?
---@field max_length number
--
local Docblock = class.new_class(nil, 'Docblock', {
  max_length = 80
})

Docblock.ID = {
  HEAD = 1,
  CLASS = 2,
  METHOD = 3,
  FIELD = 4,
}

---@param el_type number
---@param el_name string
---@return self
function Docblock:find(el_type, el_name)
  assert(self.gen ~= nil, 'gen')
  self.elType = el_type
  self.elName = el_name

  local editor = self.gen:getEditor()
  local ctx = editor:getContext()
  local lines_cnt = editor:getLinesCount(ctx.bufnr)
  local last_start
  local find_pattern = '^%s*class ' .. el_name

  for i = 0, lines_cnt do
    local line = editor:getLines(ctx.bufnr, i, i)[1]
    if line then
      if line:match('^%s*%/%*%*') then -- /**
        last_start = i
      elseif line:match(find_pattern) then
        self.bufnr = ctx.bufnr
        self.lnum_end = i - 1
        self.lnum = last_start or self.lnum_end
        if last_start then
          self.lines = editor:getLines(ctx.bufnr, self.lnum, self.lnum_end)
        else
          self.lines = {} -- empty
        end
        break
      end
    end
  end
  return self
end

---@return boolean
function Docblock:isValid()
  if self.elName and self.bufnr and self.lnum and self.lnum_end then
    return true
  end
  return false
end

---@return table<string>?
function Docblock:getLines()
  return self.lines
end

---@param line string
---@return boolean
function Docblock:addLine(line)
  assert(self.gen, 'gen')
  if self.lines and line then
    local tab = self.tab or ''
    if self.lnum_end == self.lnum then
      self.lines = {
        tab .. '/**',
        tab .. ' * ' .. line,
        tab .. ' */'
      }
    else
      self.lines[#self.lines] = tab .. ' * ' .. line
      self.lines[#self.lines + 1] = tab .. ' */'
    end
    self.gen:getEditor()
        :setLines(self.bufnr, self.lnum, self.lnum_end, self.lines)
    if self.lnum_end ~= self.lnum then
      self.lnum_end = self.lnum_end + 1
    else
      self.lnum_end = self.lnum_end + 2
    end
    return true
  end
  return false
end

-- for code gen only

-- init for code gen
---@param lines table?
---@param tab string
---@return self
function Docblock:newMultiline(lines, tab)
  self.tab = tab or self.tab or ''
  self.lines = lines or {}
  self.lnum = #self.lines + 1 -- begining of the comment
  self.lines[self.lnum] = self.tab .. '/**'
  self.lines[#self.lines + 1] = self.tab .. ' */'
  return self
end

---@param line string
---@return table
local function splitToLines(line, max_length)
  local t, p = {}, 1 -- t is lines
  if line == '' then
    t[#t + 1] = ''
    return t
  end

  local function add(s)
    u8.utf8_fold(s, max_length, t) -- t[#t + 1] = s
  end

  while p < #line do
    local i = string.find(line, "\n", p, true)
    if not i then
      add(line:sub(p, #line))
      break
    end
    add(line:sub(p, i - 1))
    p = i + 1
  end

  return t
end

---@param line string
---@return self
function Docblock:add(line)
  if type(line) ~= 'string' then
    return self
  end
  local tab = self.tab or ''
  self.lines = self.lines or {}
  local lines = splitToLines(line, self.max_length)
  self.lines[#self.lines] = nil -- remove */
  for _, line0 in ipairs(lines) do
    line0 = line0:match('^(.-)%s*$')
    local pref = (line0 == '') and ' *' or ' * '
    self.lines[#self.lines + 1] = tab .. pref .. line0
  end
  self.lines[#self.lines + 1] = tab .. ' */'
  return self
end

--
-- turn multiline into oneliner if it's possible
-- aka compressToOneLineIfPossible
-- for the code gen
--
---@return boolean
function Docblock:zip()
  if not self.lines or #self.lines == 0 or not self.lnum then
    return false
  end
  local sz = #self.lines - self.lnum
  if sz ~= 2 then
    return false
  end
  local s = self.lines[self.lnum + 1]
  local i = string.find(s, ' *', 1, true)
  -- assert(i, 'position of the  multiline doc asterisk')
  s = s:sub(1, i - 1) .. '/*' .. s:sub(i + 1) .. ' */'

  self.lines[self.lnum] = s
  for j = #self.lines, self.lnum + 1, -1 do
    self.lines[j] = nil
  end
  return true
end

class.build(Docblock)
return Docblock

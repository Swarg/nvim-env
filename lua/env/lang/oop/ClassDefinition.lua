-- 10-01-2025 @author Swarg
--
-- wrapper around parser.clazz - the result of parsing the java source
-- + timestamp to track updates
--
-- Unlike ClassInfo, this class stores the structure of the class definition
--
-- ClassInfo stores information about the location of the class in the project
-- directory, its connection with test classes and the type of this class
-- ClassInfo More generalized and designed to be as independent as possible
-- from specific programming languages and their syntax.
--
-- Whereas ClassDefinition is designed to store the name of the internal
-- structure of a class for a specific programming language.
--

local log = require 'alogger'
local class = require 'oop.class'

class.package 'env.lang.oop'
---@class env.lang.oop.ClassDefinition : oop.Object
---@field new fun(self, o:table?, clazz:table, path:string, stop_info:table?, mtime:number?) : env.lang.oop.ClassDefinition
---@field path string
---@field mtime number -- to check the uptodate or not
---@field stop_info table? the moment until which this class was parsed
--                         (classdef-header, fist-constructor, whole file)
---@field pkg string
---@field pkg_ln number
---@field name string
---@field mods number    (public private protected etc encoded Modifiers)
---@field imports table
---@field import_lns table
---@field extends string -- parent short-classname or full-classname
---@field implements table -- ?
---@field fields table
---@field constructors table
---@field methods table
--parser?
local C = class.new_class(nil, 'ClassDefinition', {
})


local log_debug = log.debug
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
-- the stop predicate is needed to know to what point this class was parsed
-- used to avoid parsing the entire class where it is not needed.
--
---@param clazz table
---@param path string absoulte path to the class-file
---@param stop_info table?
---@param mtime number?
function C:_init(clazz, path, stop_info, mtime)
  log_debug("_init", path, (clazz or E).name)

  self.path = path
  self.mtime = mtime or 0
  self.stop_info = stop_info

  assert(type(clazz) == 'table', 'parsed clazz from AParserImpl')

  self.pkg = clazz.pkg
  self.pkg_ln = clazz.pkg_ln
  self.name = clazz.name
  self.mods = clazz.mods or 0
  self.imports = clazz.imports
  self.import_lns = clazz.import_lns
  self.extends = clazz.extends
  self.implements = clazz.implements
  self.fields = clazz.fields
  self.constructors = clazz.constructors
  self.methods = clazz.methods
  -- static ?
end

---@return string?
function C:getPackage()
  return self.pkg
end

--
-- ChildClass extends ParentClass
--
---@return string?
function C:getParentName()
  return self.extends
end

-- for example, to find out the full name of the parent class and set it
---@param fcn string
function C:setParentName(fcn)
  self.extends = fcn
end

---@return string?
function C:getName()
  return self.name
end

--
-- helper

---@return boolean
---@param pref string?
local function ends_with(str, sub, pref)
  pref = pref or ''
  if str and sub and #str >= (#sub + #pref) and str:sub(- #sub) == sub then
    return #pref == 0 and true or str:sub(-(#pref + #sub), -(#sub + 1)) == pref
  end

  return false
end

--
-- find full classname by given short or full class name
--
-- -- todo support for "import pkg.same.*"
--
---@param cn string?
---@return string?
function C:findClassInImport(cn)
  local found = nil
  if cn ~= nil and cn ~= '' then
    for _, import in ipairs(self.imports or E) do
      if cn == import or ends_with(import, cn, '.') then
        found = import
        break
      end
    end
  end
  log_debug("findClassInImport cn: %s found: %s", cn, found)
  return found -- not found
end

--
-- check if mtime of original file has changed
--
---@return boolean
function C:isUptodate()
  -- todo
  return true
end

--

if _G.TEST then
  C.test = {}
  C.test.ends_with = ends_with
end

class.build(C)
return C

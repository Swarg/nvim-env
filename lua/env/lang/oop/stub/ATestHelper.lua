-- 30-09-2023 @author Swarg
local M = {}

local class = require("oop.class")

-- TODO find the way to move from lua/ into test/ dir
local log = require 'alogger'

local fs = require('env.files')
local su = require('env.sutil')

-- for testing only
local AMods = require("env.lang.oop.AccessModifiers")
local Lang = require("env.lang.Lang")
local LangGen = require("env.lang.oop.LangGen")
local StubLang = require("env.lang.oop.stub.StubLang")
-- local StubLangGen = require("env.lang.oop.stub.StubLangGen")
local ClassResolver = require("env.lang.ClassResolver")
local ClassInfo = require("env.lang.ClassInfo")
local Field = require("env.lang.oop.Field")
local Method = require("env.lang.oop.Method")

M.MT = Method.TYPE
M.AM = AMods.ID
M.CT = ClassInfo.CT

local D = require 'dprint'
local dprint = D.dprint

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug


--------------------------------------------------------------------------------
--                         Helpers and Stubs

--
-- split text to lines by given separator  "\n" is default separator
--
---@param s string
---@param sep string?
function M.split(s, sep)
  sep = sep or "\n"
  return su.split_range(s, sep)
end

-- ('/root', 'relative')  -->  "/root/", "/root/relative"
-- local rnf = H.root_and_file
function M.root_and_file(root, relative)
  return fs.ensure_dir(root), fs.join_path(root, relative)
end

-- stubs: implementation for Lang
---@param classname string
---@param project_root string|nil default is /app
function M.mkClassInfoAndLangGen0(classname, project_root)
  return M.mkClassInfoAndLangGen(StubLang, classname, project_root)
end

--
---@param langClass env.lang.Lang
---@param project_root string|nil default is /tmp/app
---@return env.lang.Lang
---@return string project_root
function M.mkLang(langClass, project_root)
  dprint("mkLang:", class.name(langClass), 'pr:', project_root)
  assert(class.instanceof(langClass, Lang),
    'Wrong Class Expected Lang, has: ' .. class.tostring(langClass))

  project_root = project_root or '/tmp/app/'
  local lang = langClass:new({ project_root = project_root }):setup()
  ---@cast lang env.lang.Lang
  return lang, project_root
end

---
---@param project_root string|nil default is /tmp/app
---@return env.lang.oop.StubLang
---@return string project_root
function M.mkStubLang(project_root)
  local lang, pr = M.mkLang(StubLang, project_root)
  ---@cast lang env.lang.oop.StubLang
  return lang, pr
end

--
---@param langClass env.lang.Lang
---@param classname string  the name of class or full path to class file
---@param project_root string|nil default is /app
-- return ClassInfo, LangGen, Lang
function M.mkClassInfoAndLangGen(langClass, classname, project_root)
  local lang
  lang, project_root = M.mkLang(langClass, project_root)

  local inner_path, path

  if su.starts_with(classname, project_root) and classname:match('/.*%.') then
    path = classname
    dprint('Build ClassInfo from path', path)
    inner_path = lang:getInnerPath(path)
    assert(type(inner_path) == 'string', 'inner from path:' .. tostring(path))
    classname = lang:getClassNameForInnerPath(inner_path)
    dprint('inner_path:', inner_path)
  else
    dprint('Build ClassInfo from classname', path)
    inner_path = lang:getInnerPathForClassName(classname)
    path = lang:getFullPathForInnerPath(inner_path)
  end

  local ci = ClassInfo:new({
    classname = classname,
    inner_path = inner_path,
    path = path,
    type = ClassInfo.CT.SOURCE
  })

  if classname:match('Test$') or classname:match('_spec$') then
    ci.type = ClassInfo.CT.TEST
  end

  -- the code generator
  local gen = lang:getLangGen()
  return ci, gen, lang
end

---@param gen env.lang.oop.LangGen
---@param name string
---@param type string|nil  default is "String"
---@param amod number|nil
---@return table Field
function M.mkField(gen, name, type, amod)
  assert(gen ~= nil, 'Needs a correct ClassInfo with the PhpGen')
  assert(class.instanceof(gen, LangGen),
    'Wrong Class Expected PhpGen, has: ' .. class.tostring(gen))

  return Field:new({
    amod = amod or gen:getDefaultFieldAMod(),
    type = type or gen:getDefaultFieldType(),
    name = name
  })
end

function M.mkFields(gen, opts)
  local t = {}
  for _, fo in pairs(opts) do
    local name, type, amod = fo[1], fo[2], fo[3]
    t[#t + 1] = M.mkField(gen, name, type, amod)
  end
  return t
end

M.myClassName = 'org.pkg.MyClass'
-- M.myClass = M.mkClassInfo(
--   M.myClassName,
--   '/home/dev/project/src/org/pkg/MyClass.java'
-- )

--------------------------------------------------------------------------------
--                              Cmd4Lua
--------------------------------------------------------------------------------

local Cmd4Lua = require("cmd4lua")
local c4lv = require("env.util.cmd4lua_vim")


---@param cmd_line string
---@param gen env.lang.oop.LangGen?
---@return Cmd4Lua
function M.new_cli(gen, cmd_line)
  assert(type(cmd_line) == 'string', 'cmd_line')
  -- inject dependencies add tokenizer to parse_line [ ]
  return Cmd4Lua.of(cmd_line, c4lv.Implementations)
      :set_var('gen', gen)
      :set_print_callback(function(msg)
        error(msg)
      end)
end

--------------------------------------------------------------------------------
--
function M.inherit(module)
  assert(type(module) == 'table', 'module')
  for k, v in pairs(M) do
    assert(module[k] == nil,
      'cannot override already existed key: ' .. tostring(k) .. ' ' .. type(v))
    module[k] = v
  end
end

--------------------------------------------------------------------------------

--
-- mk new instance of ClassResolver with Lang for given or defualt project_root
--
-- from project_root or from state-table
--
-- default project_root is "/tmp/app/"  use o.pr = 'new/root/'
-- defualt lang is LangGen use o.lang = instance or o.langClass = Class
-- default query type is ClassInfo.CT.CURRENT
--
---@param o string|table { pr?, lang? }
function M.mk_ClassResolver(o, langMod)
  local to = type(o)
  if to == 'string' then
    local pr = o
    o = { pr = pr }
  end
  -- mk StubLang

  if not o.lang then
    local pr
    if o.pr then
      pr = o.pr
      o.pr = nil
    else
      pr = '/tmp/app/' -- project_root
    end
    if o.langClass then
      o.lang = M.mkLang(o.langClass, pr)
    else
      o.lang = M.mkStubLang(pr)
    end
  end

  if type(langMod) == 'table' then
    local ext, test, test_suffix = langMod[1], langMod[2], langMod[3]
    M.modLang(o.lang, ext, test, test_suffix)
  end

  o.type = o.type or ClassInfo.CT.CURRENT

  return ClassResolver:new(o)
end

--
-- Update consts for given Lang
-- UsageExample:
-- modLang(cr.lang, 'lua', 'spec/', '_spec')
--
---@param lang table
function M.modLang(lang, ext, test, test_suffix)
  assert(type(lang) == 'table', 'lang')
  lang.ext = ext or lang.ext
  lang.test = test or lang.test
  lang.test_suffix = test_suffix or lang.test_suffix
end

--
-- roll out the project with given files in emulated OS and FS
--
---@param langClass oop.Object
---@param sys stub.os.OSystem
---@param t table{project_root}
---@param files table?{fn=body}
---@return env.langs.svelte.SLang
function M.setupProject(langClass, sys, t, files)
  local lang = langClass:new(nil, t.project_root)

  for k, v in pairs(t) do
    if type(k) == 'string' then
      local kname, suffix = match(k, "^([%w%-_/]+)_(%w+)$")
      local path, body = nil, nil

      -- something_body
      if kname and suffix == 'body' then
        path, body = t[kname], v
        -- something_path
      elseif kname and suffix == 'path' then
        path, body = v, t[kname]     -- case: classfile_path classfile
        if not body then
          body = t[kname .. '_body'] -- case: classfile_path classfile_body
        end
      end

      if path and body then
        log_debug('create file:%s body_type:%s', path, type(body))
        sys:set_file_content(path, body)
      end
    end
  end

  if type(files) == 'table' then
    for fn, body in pairs(files) do
      assert(type(fn) == 'string', 'fn')
      local btyp = type(body)
      if btyp ~= 'string' and btyp ~= 'table' then
        body = 'just_empty_body'
      end
      log_debug('create file:%s body_type:%s', fn, type(body))
      sys:set_file_content(fn, body)
    end
  end

  return lang
end

return M

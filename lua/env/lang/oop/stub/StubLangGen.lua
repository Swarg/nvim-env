-- 04-10-2023 @author Swarg
---@diagnostic disable: unused-local
--
-- Goal: Stub in testing
-- TODO find the way to move from lua/ into test/ dir
local class = require("oop.class")

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')
local Cmd4Lua = require("cmd4lua")
local base = require("env.lang.utils.base")

local Object = require("oop.Object")
local Lang = require('env.lang.Lang') -- Abstract class Lang (Base)
local Builder = require('env.lang.Builder')
local ComposerBuilder = require('env.langs.php.ComposerBuilder')
local PhpUnit = require 'env.langs.php.PhpUnit'
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")

local AccessModifiers = require("env.lang.oop.AccessModifiers")
local LangGen = require("env.lang.oop.LangGen")
local ClassGen = require("env.lang.oop.ClassGen")
local MethodGen = require("env.lang.oop.MethodGen")
local Constructor = require("env.lang.oop.Constructor")
local Field = require("env.lang.oop.Field")

class.package 'env.lang.oop.stub'
--- define class StubLangGen extends LangGen
---@class env.lang.oop.stub.StubLangGen : env.lang.oop.LangGen
---@field lang env.lang.Lang?
---@field ext string
---@field tab string
---@field default_field_type string
---@field default_field_amod number  -- access modifier (private|public, etc)
local StubLangGen = class.new_class(LangGen, 'StubLangGen', {
  lang = nil,
  ext = 'ext', -- extension of source files
  test = 'tests',
  default_field_type = 'String',
  default_field_amod = AccessModifiers.ID.PRIVATE
})

local template_method = [[
    ${ACCESSMOD} ${RETURN_TYPE} ${NAME}(${PARAMS})${RETURN_PREF}
    {
${BODY}
    }
]]

local template_constructor = [[
    ${ACCESSMOD} ${NAME}(${PARAMS})
    {
${BODY}
    }
]]

local template_field = [[    ${ACCESSMOD} ${TYPE} ${NAME}${DEFAULT_VALUE};]]
local template_asign_to_self_field = [[${THIS} ${EQUALS} ${VALUE};]]

local def_phpunit_test_imports = [[
use PHPUnit\Framework\TestCase;
]]


local template_class = [[
package ${PACKAGE};

${IMPORTS}
/**
 *${COVERS}
 *${DATE}
 *${AUTHOR}
 */
class ${CLASS}
{
${FIELDS}
${METHODS}
}
]]

-- override
function StubLangGen:getClassTemplate()
  return template_class
end

function StubLangGen:getMethodTemplate()
  return template_method
end

function StubLangGen:getConstructorName(classinfo)
  return classinfo:getShortClassName() --'__construct'
end

---@return string
function StubLangGen:getFieldTemplate()
  return template_field
end

function StubLangGen:getKWordThis() return 'this' end

---@return string
function StubLangGen:getVarPrefix() return '' end

---@return string
function StubLangGen:getAttrDot() return '.' end

---@return string
function StubLangGen:getConstructorTemplate()
  return template_constructor
end

---@return string
function StubLangGen:getGetterTemplate() return '' end

---@return string
function StubLangGen:getSetterTemplate() return '' end

---@return string
function StubLangGen:getEqualsTemplate() return '' end

function StubLangGen:getDefaultFieldType()
  return self.default_field_type
end

function StubLangGen:getDefaultFieldAMod()
  return self.default_field_amod
end

-- TODO
function StubLangGen:genOptsForTestFile(tci, opts)
  error('Not implements yet')
end

class.build(StubLangGen)
return StubLangGen

-- 05-10-2023 @author Swarg
local class = require("oop.class")

-- Goal: Stub for testing - a minimal Lang implementation
local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')


local Lang = require('env.lang.Lang') -- Abstract class Lang (Base)
local StubLangGen = require("env.lang.oop.stub.StubLangGen")
---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")



class.package 'env.lang.oop.stub'

-- define class StubLang extends Lang
---@class env.lang.oop.StubLang :env.lang.Lang
---@field executable       string   bin of the lang interprater
---@field namespace_map   table created by ComposerBuilder
---@field modules_test_namespaces table - created by ComposerBuilder based on
--                        the PhpUnit conf after findTestFramework()
local StubLang = class.new_class(Lang, 'StubLang', {
  ext = 'stub', -- extension of source files
  executable = 'ls',
  root_markers = { '.git', 'composer.json' },
  -- src = src/  test = test/ from Lang
})

-- Lang.register(StubLang)


--------------------------------------------------------------------------------
--                                 INIT
--------------------------------------------------------------------------------

-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework

---@param fn string filename
---@return boolean
---@diagnostic disable-next-line: unused-local
function StubLang.isLang(fn)
  return fn ~= nil
end

-- for testing, instead of a constructor
---@diagnostic disable-next-line: unused-local
function StubLang:setup(opts)
  return self
end

--------------------------------------------------------------------------------

--
-- src/app/inner/MyClass      --> app.inner.MyClass
-- test/app/inner/MyClassTest --> app.inner.MyClassTest
--
---@param ns string? -- the namespace or innerpath (without root and extendsion)
--- override
function StubLang:getClassNameForInnerPath(ns)
  -- return fs.path_to_classname(ns) -- class_name
  if ns then
    local pref = ''
    if su.starts_with(ns, self:getSrcPath()) then
      pref = self:getSrcPath()
    elseif su.starts_with(ns, self:getTestPath()) then
      pref = self:getTestPath()
    end

    local cn = ns:sub(#pref + 1)
    return fs.path_to_classname(cn, '%.') -- class_name
  end
  return ns
end

--
-- app.inner.MyClass     --> src/app/inner/MyClass
-- app.inner.MyClassTest --> test/app/inner/MyClassTest
--
--- override
---@param cn string -- classname
---@return string
function StubLang:getInnerPathForClassName(cn)
  if cn then
    if su.ends_with(cn, self.test_suffix) then
      return fs.join_path(self:getTestPath(), fs.classname_to_path(cn, '%.'))
    else
      return fs.join_path(self:getSrcPath(), fs.classname_to_path(cn, '%.'))
    end
  end
  return cn
end

-- --- override
-- ---@param cn string -- classname
-- function StubLang:getInnerPathForClassName(cn)
--   return fs.classname_to_path(cn)
-- end

-- --- override
-- ---@param filename string
-- ---@return boolean
-- function StubLang:isSrcPath(filename)
--   return filename ~= nil -- SRC
-- end
--
-- --- override
-- ---@param filename string
-- ---@return boolean|nil
-- function StubLang:isTestPath(filename)
--   filename = filename
--   return false
-- end

---@return env.lang.oop.LangGen? -- the instance of a LangGen class (ready to work)
function StubLang:getLangGen()
  return StubLangGen:new({ lang = self })
end

--- override
---@param filename string
---@return boolean
function StubLang:isSrcPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getSrcPath())
  end
  return false
end

--- override
---@param filename string
---@return boolean|nil
function StubLang:isTestPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getTestPath())
  end
  return false
end

--
-- for simple mappings src->test without any testframework
-- simple implementation
--
---@param classinfo env.lang.ClassInfo
function StubLang:lookupTestSuite(classinfo)
  local ip = classinfo:getInnerPath()
  log.debug("lookupTestSuite inner:", ip)
  if ip then
    if su.starts_with(ip, self:getTestPath()) then
      local testsuite_name, testsuite_path
      log.trace("set default testsuite")
      testsuite_name, testsuite_path = 'unit', self:getTestPath()
      classinfo:setTestSuite(testsuite_name, testsuite_path)
      return true
    end
  end

  log.trace("classinfo is not a test class")
  return false
end

-- move this code int Lang ??

--
-- a simple and straightforward classname mappings: one source to one test
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- source-class: app.module.init
-- return      : app.module.init_spec
--- override
---@param src_classname string
function StubLang:getTestClassForSrcClass(src_classname)
  local tcn = Lang.getWithSuffix(src_classname, self.test_suffix)
  log.debug("getTestClassForSrcClass scn:%s > tcn:%s", src_classname, tcn)
  return tcn
end

--
-- a simple and straightforward classname mappings: one test to one source
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- test-class: app.module.init_spec
-- return    : app.module.init
--
--- override
function StubLang:getSrcClassForTestClass(test_classname)
  local scn = Lang.getPathWithoutSuffix(test_classname, self.test_suffix)
  log.debug('getSrcClassForTestClass tcn:%s > scn:%s', test_classname, scn)
  return scn
end

class.build(StubLang)
return StubLang

-- 30-09-2023 @author Swarg

local class = require("oop.class")

local log = require('alogger')
local base = require("env.lang.utils.base")

local Class = require("env.lang.oop.Class")
local ComponentGen = require("env.lang.oop.ComponentGen")
local MethodGen = require("env.lang.oop.MethodGen")
local FieldGen = require("env.lang.oop.FieldGen")
local TU = require 'env.lang.utils.templater'

-- local Field = require("env.lang.oop.Field")
-- local Method = require("env.lang.oop.Method")


class.package 'env.lang.oop'
---@class env.lang.oop.ClassGen: env.lang.oop.ComponentGen
---@field opts table|nil  -- aditional options for generate ClassBody
local ClassGen = class.new_class(ComponentGen, 'ClassGen', {
  -- classinfo,gen - from ComponentGen
  opts = nil,
})

--
-- Generate Class by given opts
---@param opts table {fields, constructor, methods, object_value, dry_run}
---@return env.lang.oop.Class
function ClassGen:genClass(opts)
  assert(type(opts) == 'table', 'opts')
  assert(type(self.classinfo) == 'table', 'classinfo')

  local gen, methodGen, klass
  gen = self:getLangGen()
  assert(type(gen) == 'table', 'gen')

  methodGen = gen:getMethodGen(self.classinfo)
  klass = self:newClass(opts)

  if opts.fields then
    FieldGen.validateFields(gen, opts.fields)
    klass:addFields(opts.fields)
  end

  if opts.constructor then
    klass:addMethod(methodGen:genConstructor({ params = opts.fields }))
  end

  if opts.object_value and opts.fields then
    klass:addMethod(methodGen:genIsEqualTo(opts.fields))
    klass:addMethods(methodGen:genGetters(opts.fields))
  end

  if opts.methods then
    MethodGen.validateMethods(opts.methods)
    klass:addMethods(opts.methods)
  end

  if opts.dry_run then -- for testing and debuggins
    opts.class_obj = klass:toArray()
  end

  return klass
end

---@diagnostic disable-next-line: unused-local
function ClassGen:newClass(opts)
  local ci = self:getClassInfo()
  assert(ci ~= nil, 'has classinfo')

  return Class:new({
    name = ci:getShortClassName(),
    package = ci:getPackage()
  })
end

-- TODO remove from Builder to ClassGen

---@return table DATE AUTHOR PACKAGE CLASS
function ClassGen.getDefaultProps()
  return {
    DATE = os.date(' %d-%m-%Y'),
    COVERS = "", -- in the class docs-comment section
    AUTHOR = "",
    AUTHOR_PREFIX = "@author",
    PACKAGE = "", -- namespace
    IMPORTS = "",
    CLASS = "",
    METHODS = "",
    FIELDS = "",
    EXTENDS = "", -- parent class
  }
end

---@return string?
function ClassGen:getClassTemplate()
  if self:canWork() then
    return self:getLangGen():getClassTemplate(self.classinfo)
  end
end

---@return env.lang.oop.ClassGen
function ClassGen:setOpts(opts)
  self.opts = opts
  return self
end

--
---@param klass env.lang.oop.Class
function ClassGen:toCode(klass) -- old genClassBody
  assert(klass ~= nil, 'has class')

  local gen = self:getLangGen()
  assert(self.classinfo ~= nil, 'classinfo')
  assert(gen ~= nil, 'langGen')

  local mg = gen:getMethodGen(self.classinfo)
  local fg = gen:getFieldGen(self.classinfo)

  local fields, methods = '', ''

  if klass.fields then
    for _, field in pairs(klass.fields) do
      fields = fields .. "\n" .. fg:toCode(field)
    end
  end
  -- constructors, getters, setters, equals, ...
  if klass.methods then
    for _, method in pairs(klass.methods) do
      local code = mg:toCode(method)
      if code then
        methods = methods .. "\n" .. code
      end
    end
  end

  local opts = self.opts or {}
  opts.fields = self.notEmptyOrFalse(fields)
  opts.methods = self.notEmptyOrFalse(methods)

  return self:buildClassBody(opts)
end

-- Create text body for given class by already generated code parts
-- ClassInof must have full path and full classname (with a pakage part)
--- param ci ClassInfo
---@return string?  -- the body of the built class
-- function ClassGen:buildClassBody(ci, opts)
function ClassGen:buildClassBody(opts) -- toCode
  log.debug("buildClassBody")
  local ci, gen = self.classinfo, self:getLangGen()
  local class_template = self:getClassTemplate()

  if not ci or not opts or not class_template then
    log.debug("Cannot createClassBody no classinfo or templ or opts ")
    return nil
  end

  local scn = ci:getShortClassName()
  local t = self.getDefaultProps() -- substitutions table
  -- some.pkg.Class -> some.pkg;
  local pkg = ci:getPackage()
  t["PACKAGE"] = (pkg or '') -- .. gen:getOpSep()
  t["CLASS"] = scn
  t["PAIR_CLASSNAME"] = ci:getPairClassName()
  t["AUTHOR"] = base.getDeveloperName() or ''

  if pkg and pkg ~= '' then
    t["FULL_CLASSNAME"] = pkg .. tostring(gen:getPkgDot()) .. scn
  else
    t["FULL_CLASSNAME"] = scn
  end

  if opts then
    if opts.author and opts.author ~= '' then
      t["AUTHOR"] = opts.author
    end
    if opts.extends and opts.extends ~= '' then
      t['EXTENDS'] = gen:getKWordExtands() .. opts.extends
    end
    t["IMPORTS"] = self.notEmptyOrFalse(opts.imports)
    t["FIELDS"] = self.notEmptyOrFalse(opts.fields)
    t["METHODS"] = self.notEmptyOrFalse(opts.methods)
    t["CLASS_DOC"] = self.notEmptyOrFalse(opts.class_doc)
    local covers = ''
    if opts.covers then
      if type(opts.covers) == 'string' then
        covers = covers .. ' @covers ' .. opts.covers
      elseif type(opts.covers) == 'table' then
        error('Not implement yet!')
      end
    end
    t["COVERS"] = covers
    -- for testring
    if opts.no_date then t["DATE"] = '' end
  end
  -- log.debug("CreateClass subs: %s", t)
  --
  if t.AUTHOR and t.AUTHOR ~= '' and t.AUTHOR_PREFIX then
    t.AUTHOR = ' ' .. tostring(t.AUTHOR_PREFIX) .. ' ' .. tostring(t.AUTHOR)
  end
  log.debug('substitutions:', t)
  local body = TU.substitute(class_template, t)
  return body
end

-- if opts.fields then
--   assert(type(opts.fields) == 'table')
--
--   local t, fields, construct_body, construct_params = self.tab, '', '', ''
--   local scn, getters, is_equals = sci:getShortClassName(), '', 'return\n'
--   local vn = self.getVarNameForClassName(scn)
--
--   -- fields of class + constructor
--   local amod = 'public'
--   if opts.constructor then
--     amod = 'private'
--   end
--
--   for _, f in pairs(opts.fields) do
--     local type = 'string' -- field type
--
--     if PhpGen.isClassName(f) then
--       type = ClassInfo.getShortClassNameOf(f) or type
--       f = string.lower(f)
--     end
--     -- gen Class Fields
--     fields = fields .. t .. amod .. ' ' .. type .. ' $' .. f .. ";\n"
--
--     -- gen construct_body
--     if opts.constructor then
--       local param = '$' .. f
--       if opts.str_lower and type == 'string' then
--         param = 'mb_strtolower(' .. param .. ')'
--       end
--       construct_body = construct_body ..
--           string.format("%s%s$this->%s = %s;\n", t, t, f, param)
--
--       if #construct_params > 0 then
--         construct_params = construct_params .. ', '
--       end
--       construct_params = construct_params .. type .. ' $' .. f
--     end
--
--     -- gen Getters
--     if amod ~= 'public' then
--       getters = getters .. "\n" .. PhpGen:genGetter(f, type)
--     end
--     -- gen isEqualsTo
--     is_equals = is_equals ..
--         string.format("%s%s%s$this->%s === %s->%s &&\n", t, t, t, f, vn, f)
--   end
--   opts.fields = fields -- table to text
--
--   if opts.constructor then
--     local constructor = self:genConstructor(construct_body, construct_params)
--     is_equals = is_equals:sub(1, #is_equals - 4) .. ';'
--     is_equals = self:genMethod('isEqualsTo', is_equals, scn .. ' ' .. vn, 'bool')
--
--     -- generated code
--     opts.methods = constructor .. "\n" .. is_equals .. getters
--   end
-- end
--


class.build(ClassGen)
return ClassGen

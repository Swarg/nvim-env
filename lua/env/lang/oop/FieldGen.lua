-- 30-09-2023 @author Swarg
local class = require("oop.class")

local log = require('alogger')

local Object = require("oop.Object")
local ClassInfo = require("env.lang.ClassInfo")
local AMods = require("env.lang.oop.AccessModifiers")
local ComponentGen = require("env.lang.oop.ComponentGen")
local Field = require("env.lang.oop.Field")
local Docblock = require 'env.lang.oop.Docblock'
local TU = require 'env.lang.utils.templater'

class.package 'env.lang.oop'
---@class env.lang.oop.FieldGen : env.lang.oop.ComponentGen
---@class env.lang.oop.FieldGen : env.lang.oop.ComponentGen
local FieldGen = class.new_class(ComponentGen, 'FieldGen', {
})


function FieldGen.validateFields(gen, fields)
  -- assert(Object.instanceof(gen, LangGen), 'langGen')
  assert(type(gen) == 'table' and gen.getDefaultFieldAMod, 'langGen')
  assert(type(fields) == 'table', 'fields')

  for _, field in pairs(fields) do
    assert(Object.instanceof(field, Field), 'must be an instance of Field')
    -- set defaults
    field.amod = field.amod or gen:getDefaultFieldAMod()
    field.type = field.type or gen:getDefaultFieldType()
    field.name = field.name or 'field'
  end
end

--------------------------------------------------------------------------------
--                            CLI Parser
--------------------------------------------------------------------------------

--- id,
---@param slist string|table<string> list of cli defined fields (raw-strings)
---@return table <string, env.lang.oop.Field>|string|nil -- name -> Field
function FieldGen.parseFieldsFromStrings(slist)
  if type(slist) == 'string' then slist = { slist } end
  assert(type(slist) == 'table')

  local cnt, fields = 0, {}

  for _, field_str in pairs(slist) do
    local field = FieldGen.parseCliStr(field_str, '-')
    if field then
      fields[field.name] = field -- keep unique names
      -- is time to set default here?
      -- for now default values are set before code generation
      cnt = cnt + 1
    end
  end
  if cnt > 0 then
    return fields
  end
end

--
-- Parse string with filed definition into table{access, type, name}
--
-- TODO def value?
--
---@param field_str string  - raw string - the field definition
---@param sep string?  def is '-' is a char that cannot be a part of classname
---@return table?
function FieldGen.parseCliStr(field_str, sep)
  sep = sep or '-'

  if field_str then
    local args = {}
    for arg in string.gmatch(field_str, '([^' .. sep .. ']+)') do
      table.insert(args, arg)
    end
    local count = #args

    local field_type, field_name, field_amod = nil, nil, nil
    -- field_amod = Field.default_field_amod -- private

    -- one arg - Type|Name
    if count == 1 then
      field_name = args[1]
      -- error only access modified and no type|name
      if AMods.getAccessModifierId(field_name, nil) then
        -- field_name = nil -- error, skip this filed
        return nil -- error stop all parsing
      end
      local scn = ClassInfo.getShortClassNameOrNil(field_name)
      if scn then
        field_type = scn
        field_name = ClassInfo.getVarNameForClassName(scn, '')
      end
    end
    -- Type+Name or Mod+Type or Mod+Name
    if count == 2 then
      local mod = AMods.getAccessModifierId(args[1], nil)
      -- Mod+Type, Mod+Name
      if mod then
        field_amod = mod
        local scn = ClassInfo.getShortClassNameOrNil(args[2])
        if scn then -- Mod+Type  make name
          field_type = scn
          field_name = ClassInfo.getVarNameForClassName(scn, '')
        else               -- Mod+Name  take def type
          field_type = nil -- default_field_type substitute at Field.toCode
          field_name = args[2]
        end
        -- Type+Name
      else
        field_type = args[1]
        field_name = args[2]
      end
    end
    -- Mod+Type+Name
    if count == 3 then
      field_amod = AMods.getAccessModifierId(args[1])
      field_type = args[2]
      field_name = args[3]
    end

    -- add parsed data only if all params is valid
    if field_name then     -- and field_amod then
      return Field:new({
        amod = field_amod, -- AModifiers.getAccessModifier(field_amod),
        type = field_type, -- or Field.default_field_type,
        name = field_name,
        -- todo default_value
      })
    else
      log.debug('Has invalid filed definition [%s]', field_str)
    end
  end
  return nil
end

---@param f env.lang.oop.Field
---@return string?
function FieldGen:toCode(f)
  if f then
    local t = Field.getDefaultProps()
    local amod = f.amod or self.gen:getDefaultFieldAMod()
    t.ACCESSMOD = AMods.getAccessModifier(amod)
    ---@diagnostic disable-next-line: undefined-field
    t.TYPE = f.type or self.gen:getDefaultFieldType()
    t.NAME = f.name or 'field'
    t.NAME = (self.gen:getVarPrefix() or '') .. t.NAME

    local defval = f.defval or f['default']
    if type(defval) == 'table' and next(defval) == nil then
      defval = '{}' -- todo lang depended List.empty() e.g. [] for json
    end
    if defval ~= nil then
      local q = type(defval) == 'string' and '"' or ''
      t.DEFAULT_VALUE = ' = ' .. q .. tostring(defval) .. q
    end
    local templ = self.gen:getFieldTemplate(self.classinfo)
    local code = TU.substitute(templ, t)

    if FieldGen.hasDocBlockElements(f) then
      local tab = string.match(templ, '^(%s*)')
      local comment = self:buildDocBlockComment(f, tab)
      if comment ~= nil and comment ~= '' then
        code = comment .. "\n" .. code
      end
    end
    return code
  end
  return nil
end

--
-- from title, desc, enum, example
--
---@param f env.lang.oop.Field
function FieldGen:buildDocBlockComment(f, tab)
  local doc = Docblock:new({
    gen = self.gen, classinfo = self.classinfo, tab = tab
  }):newMultiline()

  FieldGen.buildDocBlockComment0(doc, f)
  doc:zip()

  return table.concat(doc.lines, "\n")
end

---@param t table?
function FieldGen.hasDocBlockElements(t)
  if not t then return false end
  return t.desc or t.title or t.enum or t.example or t.format or t.maxLength or
      t.pattern or t.description or t.readOnly
end

---@param doc env.lang.oop.Docblock
---@param f table{title, desc, enum, example}
function FieldGen.buildDocBlockComment0(doc, f)
  if f.title then
    doc:add(f.title)
  end
  if type(f.desc) == 'string' then
    doc:add(f.desc)
  end
  if type(f.description) == 'string' then
    doc:add(f.description)
  end
  if f.enum then
    local enum = f.enum
    if type(enum) == 'table' then
      local s = 'enum:'
      for _, v in ipairs(enum) do s = s .. ' ' .. tostring(v) end
      doc:add(s)
    else
      doc:add("enum: " .. tostring(enum))
    end
  end
  if f.format then doc:add("format: " .. tostring(f.format)) end
  if f.pattern then doc:add("pattern: " .. tostring(f.pattern)) end
  if f.maxLength then doc:add("maxLength: " .. tostring(f.maxLength)) end
  if f.min then doc:add("min: " .. tostring(f.min)) end
  if f.max then doc:add("max: " .. tostring(f.max)) end

  if f.example ~= nil then
    if type(f.example) == 'table' then --
      if #f.example > 0 then
        doc:add("example:")
        for _, line in ipairs(f.example) do
          doc:add('  ' .. tostring(line))
        end
      else
        doc:add("example: []") -- empty json list
      end
    else
      doc:add("example: " .. tostring(f.example))
    end
  end
end

class.build(FieldGen)
return FieldGen

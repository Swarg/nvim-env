-- 21-09-2023 @author Swarg
local class = require("oop.class")

local fs = require('env.files')
local su = require('env.sutil')
local log = require('alogger')

local state_registry = require 'env.state_registry'
local lapi_consts = require 'env.lang.api.constants'
local FileWriter = require 'env.lang.FileWriter'
local Editor = require 'env.ui.Editor'
local ErrLogger = require 'olua.util.ErrLogger'
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")
local ComponentGen = require("env.lang.oop.ComponentGen")
local ClassGen = require("env.lang.oop.ClassGen")
local MethodGen = require("env.lang.oop.MethodGen")
local FieldGen = require("env.lang.oop.FieldGen")
local Docblock = require("env.lang.oop.Docblock")
local TU = require 'env.lang.utils.templater'

local CT = ClassInfo.CT
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

class.package 'env.lang.oop'
---@class env.lang.oop.LangGen : oop.Object
---@field lang env.lang.Lang
---@field ext string
---@field src string
---@field test string
---@field tab string  -- indentation
---@field file_writer env.lang.FileWriter?
---@field cmdhandler_new_stuff string? a full module name for a newStuff command
local LangGen = class.new_class(nil, 'LangGen', {
  -- ext = '',
  src = 'src',
  test = 'test',
  tab = '    ',
})

local E = {}
local log_debug, log_trace = log.debug, log.trace

---@return env.lang.Lang
function LangGen:getLang()
  return self.lang
end

---@return env.lang.FileWriter
function LangGen:getFileWriter()
  if not self.file_writer then
    self.file_writer = FileWriter:new(nil, self.lang:getProjectRoot())
  end
  return self.file_writer
end

---@return boolean
---@return string
function LangGen:error(fmsg, ...)
  return self:getErrLogger():error(fmsg, ...)
end

---@return olua.util.ErrLogger
function LangGen:getErrLogger()
  self.errlogger = self.errlogger or ErrLogger:new()
  return self.errlogger
end

---@return boolean
function LangGen:hasErrors()
  return self.errlogger ~= nil and self.errlogger:hasErrors()
end

--
-- a readable report on how the files were saved by the FileWriter
--
---@param prefix string?
---@return string
function LangGen:getReadableSaveReport(prefix)
  return self:getFileWriter():getReadableReport(prefix)
end

-- helper for testing
---@param index number
---@return string? path
---@return number? code
function LangGen:getSavedState(index)
  return self:getFileWriter():getEntry(index)
end

---@return env.ui.Editor
function LangGen:getEditor()
  return self.lang:getEditor()
end

function LangGen:hasUI()
  return self.lang:hasUI()
end

---@return env.ui.Context
---@param update boolean|nil  - default is false
function LangGen:getContext(update)
  log_debug("getContext update:", update)
  return self.lang:getEditor():getContext(update)
end

function LangGen:getCurrentOpenedFileName(s)
  local ctx = self.lang:getEditor():getContext(true)
  if s == 'full' or not s then
    return ctx.bufname
  elseif s == 'basename' then -- without extension
    return fs.extract_filename(ctx.bufname)
  end
end

function LangGen:getMethodUnderUICursor()
  if not self:hasUI() then
    log.debug("getMethodUnderUICursor: no UI")
    return nil
  end

  local context = self:getContext(true)
  local method = context:element(LEXEME_TYPE.FUNCTION) or
      context:element(LEXEME_TYPE.METHOD)
  log_debug("getMethodUnderUICursor: found method: ", method)
  return method
end

---@return env.lang.oop.ClassGen
function LangGen:getClassGen(ci)
  return ClassGen:new({ gen = self, classinfo = ci })
end

---@return env.lang.oop.MethodGen
function LangGen:getMethodGen(ci)
  return MethodGen:new({ gen = self, classinfo = ci })
end

---@return env.lang.oop.FieldGen
function LangGen:getFieldGen(ci)
  return FieldGen:new({ gen = self, classinfo = ci })
end

-- factory for classes extends the ComponentGen
-- Usage Example:
--   local classGen = gen:get(ClassGen, classinfo)
--   local mGen = gen:get(MethodGen, classinfo)
--   local fGen = gen:get(FieldGen, classinfo)
--
---@param klass env.lang.oop.ComponentGen -- a Class what extends the ComponentGen
---@param ci env.lang.ClassInfo
---@return env.lang.oop.ComponentGen
function LangGen:get(klass, ci)
  assert(klass, 'class')
  assert(class.instanceof(klass, ComponentGen))

  return klass:new({ gen = self, classinfo = ci })
end

-- indentation
---@return string
function LangGen:getTab() return self.tab end

--------------------------------------------------------------------------------
--          Abstract methods to override by own Lang implementation           --
--------------------------------------------------------------------------------

--
-- to create Project Config file (BuildScript like build.gradle, mix.exs, etc)
--
---@param opts table?
---@return table
---@diagnostic disable-next-line: unused-local
function LangGen:createProjectFiles(opts) error('abstract') end

--
---@return table lua-module of test-codegen
---@return string testframework name
function LangGen:getTestCodeGenMod() error 'abstract' end

--
-- to create Project Config file (BuildScript like build.gradle, mix.exs, etc)
-- moved to env.lang.Lang
--
---param opts table?
---return table
---diagnostic disable-next-line: unused-local
-- function LangGen:createProjectConfigs(opts) error('abstract') end

---@param ci env.lang.ClassInfo
---@diagnostic disable-next-line: unused-local
function LangGen:getClassTemplate(ci) return '' end

---@return string
---@param ci env.lang.ClassInfo?
---@diagnostic disable-next-line: unused-local
function LangGen:getFieldTemplate(ci) return '' end

---@return string, string  template and separator
function LangGen:getParamTemplate() return '${TYPE} ${NAME}', ',' end

---@return string
function LangGen:getVarPrefix() return '' end

-- for lang has: public func name(params): void
---@return string
function LangGen:getReturnPrefix() return '' end

---@return string  operation separator
function LangGen:getOpSep() return ';' end

function LangGen:getOpAnd() return '&&' end

---@return string
function LangGen:getOpAssign() return '=' end

---@return string
function LangGen:getOpCompareIdentical() return '==' end

---@return string
function LangGen:getOpCompareEqual() return '==' end

---@return string
function LangGen:getAttrDot() return '.' end -- for lua is ':', for php is '->'

function LangGen:getPkgDot() return '.' end  -- in php is \\(backslash)

---@return string
function LangGen:getKWordThis() return 'this' end

---@return string
function LangGen:getKWordBoolean() return 'boolean' end

---@return string
function LangGen:getKWordExtands() return ' extends ' end

---@param ci env.lang.ClassInfo?
---@return string
---@diagnostic disable-next-line: unused-local
function LangGen:getConstructorTemplate(ci) return '' end

---@param ci env.lang.ClassInfo?
---@return string
---@diagnostic disable-next-line: unused-local
function LangGen:getGetterTemplate(ci) return '' end

---@param ci env.lang.ClassInfo?
---@return string
---@diagnostic disable-next-line: unused-local
function LangGen:getSetterTemplate(ci) return '' end

---@param ci env.lang.ClassInfo?
---@return string
---@diagnostic disable-next-line: unused-local
function LangGen:getEqualsTemplate(ci) return '' end

---@param ci env.lang.ClassInfo?
---@return string
---@diagnostic disable-next-line: unused-local
function LangGen:getMethodTemplate(ci) return '' end

---@return string
function LangGen:getDefaultFieldType() return '' end

---@return number
function LangGen:getDefaultFieldAMod() return 1 end

-- default
---@param classinfo env.lang.ClassInfo
---@return string
function LangGen:getConstructorName(classinfo)
  return classinfo and classinfo:getShortClassName() or ''
end

---@param tci env.lang.ClassInfo?
---@param opts table?
---@return table
---@diagnostic disable-next-line: unused-local
function LangGen:genOptsForTestFile(tci, opts) return {} end

--------------------------------------------------------------------------------


---
---@param w Cmd4Lua
function LangGen:newStuff(w)
  log.debug("Gen:newStuff")
  assert(self.lang ~= nil, 'expected self.lang got:' .. tostring(self.lang))

  -- w:set_print_callback(self:getEditor().echoInStatus, self:getEditor())
  w:set_var('gen', self)

  local ok, module = pcall(require, self.cmdhandler_new_stuff)
  if ok and type(module) == 'table' then
    if not state_registry.is_registered(self.cmdhandler_new_stuff) and
        state_registry.is_statefull_module(module) then
      state_registry.register(module)
    end
    module.handle(w)
  else
    log.debug('cannot find module: "%s"', self.cmdhandler_new_stuff)
  end
end

function LangGen:dispatchCommand(w, command)
  log_debug("Gen:cmdRefactor")
  assert(self.lang ~= nil, 'expected self.lang got:' .. tostring(self.lang))
  assert(type(command) == 'string' and command ~= "", "command name")
  w:set_var('gen', self)

  local pkg = class.get_package_name(self:getClassName())

  local modulename = pkg .. '.command.' .. tostring(command)
  local ok, module = pcall(require, modulename)
  log_debug('require %s %s %s', modulename, ok, type(module))
  if ok then
    if type(module) == 'table' then
      module.handle(w)
    else
      w:error('expected module table but got:' .. type(module))
    end
  else
    local v2s = tostring
    w:say(string.format('cannot find module: "%s"', modulename .. v2s(module)))
  end
end

--
-- check is given classinfo not nil and has path and
-- has no already existed file with classinfoclassinfo.path or file is empty
--
---@param classinfo env.lang.ClassInfo?
---@param type string? Source|Test
---@param rewrite_existed boolean?
---@return boolean
function LangGen:canCreateNewClass(classinfo, type, rewrite_existed)
  type = type or ''
  if not classinfo then
    log.debug("No %s ClassInfo.", type)
    return false
  end
  if not classinfo:getPath() then
    log.debug("No %s Path in ClassInfo.", type)
    return false
  end

  if self.lang:isPathExists(classinfo) then
    local m = type .. ' File Already exists ' .. tostring(classinfo:getPath())
    log.debug("%s rewrite_existed: %s", m, rewrite_existed)
    -- allow overwriting only empty files
    if fs.file_size(classinfo:getPath()) > 4 then
      if not rewrite_existed then
        print(m)
        return false
      end
    end
  end
  return true
end

---@param ci env.lang.ClassInfo
---@param opts table{params, amod}
--- param params Field[]
--- param amod number?|nil
function LangGen:getConstructorCode(ci, opts)
  local mg = self:get(MethodGen, ci)
  opts = opts or {}
  ---@cast mg env.lang.oop.MethodGen
  local method = mg:genConstructor(opts)
  return mg:toCode(method)
end

---@param ci env.lang.ClassInfo
---@param field env.lang.oop.Field
---@param name string?|nil
function LangGen:getGetterCode(ci, field, name)
  local mg = self:get(MethodGen, ci)
  ---@cast mg env.lang.oop.MethodGen
  local method = mg:genGetter(field, name)
  return mg:toCode(method)
end

---@param ci env.lang.ClassInfo
---@param field env.lang.oop.Field
---@param name string?|nil
function LangGen:getSetterCode(ci, field, name)
  local mg = self:get(MethodGen, ci)
  ---@cast mg env.lang.oop.MethodGen
  local method = mg:genSetter(field, name)
  return mg:toCode(method)
end

---@param ci env.lang.ClassInfo
---@param params env.lang.oop.Field[]
---@param rettype string
---@param body string?|nil
function LangGen:getCustomMethodCode(ci, name, params, rettype, body)
  local mg = self:get(MethodGen, ci)
  ---@cast mg env.lang.oop.MethodGen
  local method = mg:genCustom(ci, name, params, rettype, body)
  return mg:toCode(method)
end

--
-- substitute values from "substitutions" into given template
--
-- wrapper around env.lang.util.templator.substitution
--
--
---@param template string
---@param substitutions table - key-value map used to produce content from templ
---@param upperkey boolean? when in config keynames is lowercase but in the
--                          template is same word in uppercase
---@return string
function LangGen.buildTempl(template, substitutions, upperkey)
  return TU.substitute(template, substitutions, upperkey)
end

---@param checked boolean already checked is file exists
---@param path string
---@param body string
---@param opts table{dry_run, append}
---@return boolean
function LangGen:createFile(checked, path, body, opts)
  log_debug("createFile %s checked:%s", path, checked, opts)
  local fw = self:getFileWriter()
  local saved = fw:saveFile(checked, path, body, opts) == fw.OK_SAVED
  log_trace("saved %s", saved, path)
  return saved
end

---@param path string
---@param existst boolean?
---@param opts table{dry_run, openInEditor }
function LangGen:openFile(path, existst, opts)
  -- to open generated or already existed
  if not opts.dry_run and existst then
    if opts.openInEditor then
      self.lang:getEditor():open(path)
      -- todo jump to method like:
    else
      log.debug('No opts.openInEditor')
    end
  end
end

--
--
---@param ci env.lang.ClassInfo?
---@param opts table
---@return string|false  -- path
---@return string?       -- errmsg
function LangGen:createClassFile(ci, opts)
  log_debug("createClassFile", ci)
  if not ClassInfo.isValidFile(ci) then
    return self:error('classinfo is not valid: %s', tostring(ci))
  end
  ---@cast ci env.lang.ClassInfo

  local path = ci:getPath()
  local body = self:createClassBody(ci, opts)
  if not body then
    return self:error('cannot create body for class %s', tostring(ci))
  end

  if self:createFile(true, path, body, opts) then
    ci:setPathExists(true)
  end

  self:openFile(path, ci:isPathExists(), opts)

  return path or false
end

--
-- generate Class from opts
---@param ci env.lang.ClassInfo
---@param opts table
--         .extends
--         .fields
--         .constructor
--         .object_value   (getters + isEqualTo)
--         .test_class
---@return string?
function LangGen:createClassBody(ci, opts)
  log.debug("createClassBody")
  if not self:canCreateNewClass(ci, 'Source') then
    return nil
  end

  opts = opts or {}
  local classGen, klass

  classGen = self:getClassGen(ci):setOpts(opts)

  klass = classGen:genClass(opts)

  local body = classGen:toCode(klass)
  return body
end

---@param tci env.lang.ClassInfo?
---@param opts table
---@return string|false  -- path -- path to generated file or false
---@return string?       -- errmsg
function LangGen:createTestFile(tci, opts)
  local valid = ClassInfo.isValidFile(tci)
  log.debug("createTestFile", valid)

  if not valid then
    return self:error('not a valid file in tci:%s', tostring(tci))
  end

  opts = self:genOptsForTestFile(tci, opts)
  return self:createClassFile(tci, opts)
end

-- find the Docs block for given element type and name in opened class file (ci)
---@param ci env.lang.ClassInfo
---@param el_type number class, method, function etc
---@param el_name string?
function LangGen:getDocblock(ci, el_type, el_name)
  assert(ci ~= nil and ci:getPath(), 'classinfo')
  return Docblock:new({ gen = self, classinfo = ci }):find(el_type, el_name)
end

--
-- UseCase: "many-test-for-one-source-file"
--
-- for one source-file multiples testcases in separate files in dir with same
-- name what sourse-file itself
--
-- src/app/module/sourcefile.c  -> test/app/module/sourcefile/case_a_test.c
--
---@param opts table {name}
---@return string|false  -- path
---@return table         -- opts
function LangGen:createNewTestCaseFile(opts)
  log.debug("createNewTestCaseFile", opts)
  assert(type(opts) == 'table', 'opts')

  local ctx = self:getEditor():getContext(true)
  local fn = ctx.bufname

  local tci = self.lang:resolveClass(fn, CT.TESTDIR):getClassInfo(false)
  opts.tci = tci
  if not tci then
    log.debug('cannot resolve testfile for ', fn)
    return false, opts
  end
  log.trace('resolved tci class: %s path: %s tcases: %s',
    tci:getClassName(), tci:getPath(), tci:getTestCases() ~= nil)
  assert(tci:isTestDir(), 'testcases dir expected')

  -- find testcase name from current line(mehtod-name) or ask from user
  if not opts.name then
    ctx:resolveWords()
    local LT = LEXEME_TYPE
    opts.name = ctx:element(LT.METHOD) or ctx:element(LT.FUNCTION)
  end

  local tcdir = tci:getPath() -- getTestCaseDir()

  local suff = self.lang:getTestSuffix() .. '.' .. self.lang:getExt()
  -- validate tcdir + opts.name to ensure not exists and ask new name if needs
  opts.tcpath, opts.name = self:askNewPath(opts.quiet, tcdir, opts.name, suff)

  -- no name ans cannot ask from user - exit
  if not opts.name or not opts.tcpath then
    log.debug('cancel: no test-case name', opts.name, opts.tcpath)
    return false, opts
  end
  opts.tci = ClassResolver.of(self.lang):lookupByPath(opts.tcpath, CT.TEST)
  ClassInfo.bind(opts.tci, tci:getPair()) -- many-to-one relation (tci0.pair = sci)

  -- support opts.dry_run opts!
  local path, _ = self:createTestFile(opts.tci, opts)
  return path, opts
end

--
-- validate path to given dir0 and name to make sure path not already exists
-- if path already exists - and not quiet - ask a new name from user and
-- check input.
--
-- Goal: obtain path to ensure not existed file in dir0 or its subdirs
-- not only ask value from user - if quiet then just validate given and if it
-- passed just return full path without any prompts
--
-- if not quiet asked a new not existed names in limit of 64 times
--
-- on success return a full path to ensure not existed file and
-- relative name (from dir0)
--
---@param quiet boolean?
---@param name string?
---@param dir0 string
---@return string?, string? -- path, name
function LangGen:askNewPath(quiet, dir0, name, suffix)
  log.debug("askNewPath dir0:'%s' name:'%s'", dir0, name)
  assert(type(dir0) == 'string', 'dir0')
  suffix = suffix or ''

  local lang, path = self:getLang(), nil
  local limit = 64
  name = name or Editor.askValue('Name:')

  ---@return string?, nil
  local function ask(msg, aquiet, adir0, apath)
    if aquiet then
      return nil, nil
    end -- stop
    local ip = tostring(lang.getInnerPathFor(adir0, apath))
    return Editor.askValue(string.format(msg, ip), ip), nil
    -- note secod value is nil to clear path i.g. is already existed - needs new
  end

  -- while name and dir0 and limit > 0 do
  while name and limit > 0 do
    -- before create a testcase file first check is here already existed the
    -- directory with a same name (without suffix and extension)
    path = fs.join_path(dir0, name) -- testcases

    local ftype = fs.file_type(path)

    if ftype == 'directory' or ftype == 'file' then
      log.debug('%s already exists: "%s" q:%s', ftype, path, quiet)
      name, path = ask("Dir: %s already exists. New name: ", quiet, dir0, path)
    else
      local has_suff = su.ends_with(path, suffix)
      if not has_suff then
        path = path .. suffix
      end

      if fs.file_exists(path) then
        log.debug('file already exists: "%s" q:%s', path, quiet)
        name, path = ask("File: %s already exists. New name: ", quiet, dir0, path)

        -- try specify full filename with suffix and ext,
        -- then has existed subdir with same name
      elseif has_suff then
        local path0 = lang.getPathWithoutSuffix(path, suffix)
        if fs.file_exists(path0) then -- dir or file
          log.debug('file already exists: "%s" q:%s', path0, quiet)
          name, path = ask("File: %s already exists. New name: ", quiet, dir0, path0)
        else
          break -- ok
        end
      else
        break
      end
    end
    limit = limit - 1
  end

  return path, name
end

--
-- generate source code for new test method (from source to test file)
-- if opts.method_name is empty try to resolve method under the cursor
-- insert generated code into bottom of the paired test file
--
---@param opts table?{name}
function LangGen:createNewTestMethod(opts)
  log_debug("createNewTestMethod")
  opts = opts or E
  local ctx = self:getEditor():getContext(true)
  local fn = ctx.bufname

  local sci = self.lang:resolveClass(fn, CT.CURRENT):getClassInfo(false)
  if not sci then
    log.debug('cannot resolve source file:', fn)
    return false, opts
  end

  local tci
  if sci:isTestClass() then
    tci = sci -- swap
    sci = tci:getPair()
  else
    tci = sci:getPair()
  end
  if not tci then
    print('test class not found for ' .. tostring(sci:getClassName()))
    return false
  end

  -- find testcase name from current line(mehtod-name) or ask from user
  if not opts.method_name then
    ctx:resolveWords()
    opts.method_name = self:getMethodUnderUICursor()
    opts.get_ctx = function() return ctx end -- callback to use AParser in future
  end

  opts.tab = self:getTab()
  local testgen_mod = self:getTestCodeGenMod()
  local body, tmn, offset_lnum, col = testgen_mod.gen_testmethod_body(sci, opts)
  offset_lnum = offset_lnum or 1
  col = col or 8

  local methodGen = self:getMethodGen(tci)
  local params = false
  local method = methodGen:genTest(tmn, params, body)
  local code = methodGen:toCode(method)

  -- insert to nvim buffer
  local lines = su.split_range(code, "\n") or {}
  self:getEditor():open(tci:getPath())
  ctx:update():setLines(-1, -2, lines)
  ctx:setCursorPos(-(#lines - offset_lnum), col)

  return true
end

class.build(LangGen)
return LangGen

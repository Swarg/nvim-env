-- 30-09-2023 @author Swarg
local class = require("oop.class")

local ClassInfo = require("env.lang.ClassInfo")
local AMods = require("env.lang.oop.AccessModifiers")

class.package 'env.lang.oop'
---@class env.lang.oop.Field : oop.Object
---@field amod number  -- access_mod
---@field type string
---@field name string
---@field defval any?
---@field read_only boolean? aka final or for no setters
-- for comment and DocBlock
---@field desc string?
---@field title string?
---@field enum string|table?
---@field example string?
local Field = class.new_class(nil, 'Field', {
})


-- used in field pattern
---@return table
function Field.getDefaultProps()
  return {
    ACCESSMOD = AMods.getAccessModifier(AMods.ID.PUBLIC),
    TYPE = "",
    NAME = "",
    DEFAULT_VALUE = "",
  }
end

---@return table?
function Field:toArray()
  if self then
    return {
      amod = self.amod,
      type = self.type,
      name = self.name,
    }
  end
  return nil
end

-- factory to create Filed from ClassInfo (take type and name as lowercase)
---@param ci env.lang.ClassInfo?
---@param amod number|nil
---@return env.lang.oop.Field?
function Field.ofClassInfo(ci, amod)
  if ci then
    local scn = ci:getShortClassName()
    if scn then
      local type = scn
      local name = ClassInfo.getVarNameForClassName(scn, '')
      amod = amod or AMods.ID.PRIVATE
      return Field:new({ amod = amod, type = type, name = name })
    end
  end
  return nil
end

class.build(Field)
return Field

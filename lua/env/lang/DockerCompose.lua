-- 22-09-2023 @author Swarg
-- Dependencies:
-- apt install libyaml-dev && luarocks install lyaml
-- https://github.com/gvvaughan/lyaml
--
local class = require("oop.class")

local fs = require('env.files')
-- local su = require('env.sutil')
-- local log = require('alogger')
local lyaml = require "lyaml"



class.package 'env.lang'
---@class env.lang.DockerCompose: oop.Object
---@field executable string
---@field workdir? string (working directory there was found yml-file)
---@field markers table
---@field yml? table
---@field ymlfile? string
---@field run_with_autoclear boolean default -- true use `--rm` flag for run cmd
local DockerCompose = class.new_class(nil, 'DockerCompose', {
  executable = 'docker',
  markers = { "docker-compose.yml" },
  run_with_autoclear = true,
})

--
-- When find docker-compose.yml then parse it and return self
-- otherwise return flase (false as flag: already searched for this project)
--
---@param path string
---@return self|false
function DockerCompose:lookup(path)
  if path then
    local root, marker = fs.find_root(self.markers, path)
    if root and marker then
      local yml = fs.join_path(root, marker)
      if self:parseYaml(yml) then
        self.workdir = root
        return self
      end
    end
  end
  return false
end

function DockerCompose.parseYaml0(fn)
  if fn and fs.file_exists(fn) then
    local t = lyaml.load(fs.read_all_bytes_from(fn), { all = true })
    return type(t) == 'table' and t or nil
  end
  return nil
end

---@param ymlfile string
---@return boolean
function DockerCompose:parseYaml(ymlfile)
  if ymlfile and fs.file_exists(ymlfile) then
    self.ymlfile = ymlfile
    self.yml = DockerCompose.parseYaml0(ymlfile)
    return self.yml ~= nil and (self.yml[1] or {}).services ~= nil
  end
  return false
end

function DockerCompose:getExecutable()
  return self.executable
end

---@private
---@return table?
function DockerCompose:_services()
  if (self.yml) then
    return self.yml.services or (self.yml[1] or {}).services
  end
end

-- Fetch service configuration by service-name
---@param name string?
---@return table?
function DockerCompose:getService(name)
  if self.yml and name then
    return (self:_services() or {})[name]
  end
  return nil
end

-- Return list of all defined services names
---@return table?
function DockerCompose:getServicesNames()
  local services = self:_services()
  if services then
    local t = {}
    for k, _ in pairs(services) do
      table.insert(t, k)
    end
    return t
  end
  return nil
end

-- Find from all services one that match given: prefix [word [suffix]]
---@param prefix string
---@param word string?
---@param suffix string?
function DockerCompose:findServiceName(prefix, word, suffix)
  if prefix then
    local services = self:_services()
    if services then
      for name, _ in pairs(services) do
        local p = string.find(name, prefix, 1, true)
        if p and word then
          p = string.find(name, word, p + #prefix + 1, true)
          if p and suffix then
            p = string.find(name, suffix, p + #word + 1, true)
          end
        end
        if p then return name end
      end
    end
  end
  return nil
end

---@param dc_args table
---@param service string
---@param envs table
---@param cmd string?
---@param args table|string|nil
---@return table
function DockerCompose._joinArgs(dc_args, service, envs, cmd, args)
  -- environment variables
  if envs and type(envs) == 'table' then
    for _, env in pairs(envs) do
      if env and env ~= '' then
        table.insert(dc_args, '-e')
        table.insert(dc_args, env)
      end
    end
  end
  -- image name (service name)
  table.insert(dc_args, service)

  -- command and args
  if cmd and cmd ~= '' then
    table.insert(dc_args, cmd)

    if type(args) == 'table' then
      for _, arg in pairs(args) do
        table.insert(dc_args, arg)
      end
    elseif type(args) == 'string' then
      table.insert(dc_args, args)
    end
  end
  return dc_args
end

---@param service string -- a docker-compose serice name
---@param cmd string
---@param args table?
---@param envs table?
function DockerCompose:getArgsToRunService(service, cmd, args, envs)
  local dc_args = { 'compose', 'run' }
  if self.run_with_autoclear then
    table.insert(dc_args, '--rm')
  end
  envs = envs or {}
  return DockerCompose._joinArgs(dc_args, service, envs, cmd, args)
end

---@param service string -- a docker-compose serice name
---@param cmd string
---@param args table
---@param envs table?
function DockerCompose:getArgsToExecInService(service, cmd, args, envs)
  local dc_args = { 'compose', 'exec' }
  -- args for docker to exec cmd in container
  envs = envs or {}
  return DockerCompose._joinArgs(dc_args, service, envs, cmd, args)
end

--
-- a list of pairs: inside_container -> realpath
--
-- Returns a volumes configuration from yml-config for given service_name
-- For keys use paths inside docker container for values - full paths in system
-- Substitute full paths in a system based self.workdir
-- from with then yml config was founded by lookup
---@param service_name string
---@return table?
function DockerCompose:getVolumesPathMappings(service_name)
  local t = self:getService(service_name)
  if t then -- docker compse configuration block for given service name
    -- local ctx = (t.build or E).context
    -- local df = (t.build or E).dockerfile
    if t.volumes then
      local pm = {}
      for _, paths in pairs(t.volumes) do
        local machine_path, container_path = paths:match("([^:]+):([^:]+)")
        if machine_path and container_path then
          container_path = fs.ensure_no_dir_slash(container_path)
          pm[container_path] = fs.build_path(self.workdir, machine_path)
        end
      end
      return pm
    end
  end
end

--[[ Example of docker-compose.yml for dev with Xdebug
build = {
    context = "api/docker",
    dockerfile = "development/php-cli/Dockerfile"
  },
volumes = { "./api:/app" }
]]

class.build(DockerCompose)
return DockerCompose

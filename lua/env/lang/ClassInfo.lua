-- 20-09-2023 @author Swarg
--
local class = require("oop.class")
local log = require('alogger')

local Object = require("oop.Object")


-- enum of classtype
local CT_UNKNOWN      = 0
local CT_SOURCE       = 1
local CT_TEST         = 2
local CT_TESTDIR      = 3 -- many-tests-for-one-source current CI is a TCaseDir
local CT_OPPOSITE     = 4
local CT_CURRENT      = 5

local CT_NAMES        = {}
CT_NAMES[CT_UNKNOWN]  = 'Unknown'
CT_NAMES[CT_SOURCE]   = 'Source'
CT_NAMES[CT_TEST]     = 'Test'
CT_NAMES[CT_TESTDIR]  = 'TestDir' -- directory with multiple tests for one src
CT_NAMES[CT_OPPOSITE] = 'Opposite'
CT_NAMES[CT_CURRENT]  = 'Current'

local E               = {}


class.package 'env.lang'
---@class env.lang.ClassInfo : oop.Object
---@field new fun(self, table): env.lang.ClassInfo
---@field classname      string?
---@field path           string?           full filename to this class
---@field exists         boolean?  the path is actualy exists in the filesystem
---@field inner_path     string?
---@field testsuite_name string?
---@field testsuite_path string?
---@field tcases  table?    -- list of test files for one src class
---@field type           number    -- enum:  Source|Test  -- type of the class
---@field pair env.lang.ClassInfo? -- paired class (src->test test->src)
---@field parent env.lang.ClassInfo? parent of the current TestCase (TestDir)
local ClassInfo = class.new_class(Object, 'ClassInfo', {
  type = CT_UNKNOWN,
})

ClassInfo.CT = {
  UKNOWN   = CT_UNKNOWN,
  SOURCE   = CT_SOURCE,
  TEST     = CT_TEST,
  TESTDIR  = CT_TESTDIR,
  OPPOSITE = CT_OPPOSITE,
  CURRENT  = CT_CURRENT,
}

--
-- check is given ci not nil and has classname, path and a right class type
-- (not a directory with test-cases)
--
-- used to verify before generate new classfile
--
---@param ci env.lang.ClassInfo?
---@return boolean
---@param exttype number?
function ClassInfo.isValidFile(ci, exttype)
  return ci ~= nil and ci.classname ~= nil and ci.path ~= nil and ci.type ~= nil
      and (ci.type == CT_SOURCE or ci.type == CT_TEST or ci.type == exttype)
end

function ClassInfo.isValidType(ci)
  local ct = (ci or E).type
  return ct == CT_SOURCE or ct == CT_TEST or ct == CT_TESTDIR
end

---@param self env.lang.ClassInfo?
---@return boolean
function ClassInfo.isEmpty(self)
  return self == nil or not self.classname
end

---@return string?
function ClassInfo:getTypeName()
  return CT_NAMES[self.type or CT_UNKNOWN] or CT_NAMES[CT_UNKNOWN]
end

---@return boolean
function ClassInfo:isSourceClass()
  return self.type == CT_SOURCE
end

---@return boolean
function ClassInfo:isTestClass()
  return self.type == CT_TEST
end

-- used to maps many-tests-for-one-source
-- in this case:
--  - path      - is a directory not a test-file itself
--  - classname - is a "package" not a full realy existed test class
function ClassInfo:isTestDir()
  return self.type == CT_TESTDIR
end

---@param type number?
function ClassInfo:isOppositeForType(type)
  return
      type == CT_SOURCE and self.type == CT_TEST or
      type == CT_SOURCE and self.type == CT_TESTDIR or

      type == CT_TEST and self.type == CT_SOURCE or
      type == CT_TESTDIR and self.type == CT_SOURCE
end

---@return string
function ClassInfo:getClassName()
  return self.classname
end

---@return string?
function ClassInfo:getShortClassName()
  return ClassInfo.getShortClassNameOf(self.classname)
end

---@return string?
function ClassInfo.getShortClassNameOf(classname)
  if type(classname) == 'string' then
    return classname:match('([%w]+)$')
  end
  return nil
end

---@return string?
function ClassInfo:getPackage()
  local short = self:getShortClassName()
  if self.classname and short then
    return self.classname:sub(1, #self.classname - #short - 1)
  end
  return nil
end

--
-- update all fields except type
--
-- usecase: rename already known classinfo type
--
-- can break ClassInfo state (Set values without verify)
--
---@param classname string?
---@param path string?
---@param inner_path string?
---@param exists boolean?
function ClassInfo:update(classname, inner_path, path, exists)
  log.trace("update", classname, inner_path, path, exists)
  self.classname = classname
  self.path = path
  self.inner_path = inner_path
  self.exists = exists
  if exists and not path then
    self.exists = nil
  end
  return self
end

---@return string
function ClassInfo:getPath()
  return self.path
end

---@param path string
--- param update boolean? if true apply lookupByPath
function ClassInfo:setPath(path)
  log.trace("setPath", path)
  if self.path ~= path then
    self.exists = nil -- needs recheck
  end

  self.path = path

  return self
end

--
-- true - the path of this ClassInfo existed in fs
-- false - checked and do not exists
-- nil - not checked yet
--
---@param exist boolean?
---@return boolean? return self.exists
function ClassInfo:setPathExists(exist)
  log.trace("setPathExists", self.path, exist)
  assert(exist == nil or type(exist) == 'boolean', 'exist bool or nil')
  self.exists = exist
  return self.exists
end

--
-- only return prev checked state without actual checking fs
-- nil - this ClassInfo do not echecked yet
--
---@return boolean?
function ClassInfo:isPathExists()
  local exists = self.exists
  if not self.path then
    self.exists = nil
    exists = false
  end
  log.trace("isPathExists: %s exists: %s", self.path, self.exists)
  return exists
end

-- function ClassInfo:setPath(path, update)
--   assert(type(path) == 'string') -- ?
--   if path then
--     self.path = path
--     if update then
--       self:lookupByPath()
--     end
--   end
--   return self
-- end

--
-- relation: one-to-one
--
-- used then maps src->test or test->src
---@param classinfo env.lang.ClassInfo?
function ClassInfo:bindPair(classinfo)
  log.debug("bindPair %s < %s", self.classname, (classinfo or E).classname)
  self.pair = classinfo
  if classinfo then
    classinfo.pair = self
  end
  return self
end

function ClassInfo:getPair()
  log.trace('getPair %s:%s', self.classname, self.pair ~= nil, '::BACKTRACE::')
  return self.pair
end

--
-- relation: many-to-one  (many testcases for one source)
--
-- set own pair to given ci if not nil
-- to share source classinfo for multiples testcases in one testcasesdir
--
---@param self env.lang.ClassInfo?  test or testcasesdir
---@param sci env.lang.ClassInfo?   source
function ClassInfo.bind(self, sci)
  if self and sci then
    assert(self.type == CT_TEST or self.type == CT_TESTDIR, 'arg#1 test')
    assert(sci.type == CT_SOURCE, 'arg#2 source classinfo')
    self.pair = sci
    -- if self.parent = nil then sci.pair = self ??
    if self.tcases then
      for _, tci in pairs(self.tcases) do
        ClassInfo.bind(tci, sci)
      end
    end
    return true
  end
  return false
end

--
function ClassInfo:getPairClassName()
  return self.pair and self.pair.classname or nil
end

-- use lookup before!
-- inner path is a relative path in project without project_root and extendsion
---@return string?
function ClassInfo:getInnerPath()
  return self.inner_path
end

---@param name string
---@param path string
function ClassInfo:setTestSuite(name, path)
  log.trace('setTestSuite', name, path)
  self.testsuite_name = name
  self.testsuite_path = path
end

---@return string?, string? -- testsuite_name, testsuite_path
function ClassInfo:getTestSuite()
  return self.testsuite_name, self.testsuite_path
end

--
--
---@return string
function ClassInfo:toString()
  if self then
    local s = 'classname: ' .. tostring(self.classname) .. "\n\t" ..
        'path: ' .. tostring(self.path) .. "\n\t" ..
        'ipath: ' .. tostring(self.inner_path) .. "\n\t" ..
        'type: ' .. tostring(self.type) .. ':' .. self:getTypeName()
    if self.testsuite_name or self.testsuite_path then
      s = s .. "\n\ttestsuite: " .. tostring(self.testsuite_name) ..
          ':' .. tostring(self.testsuite_path)
    end
    -- varianst
    return s
  end
  return 'nil'
end

--
-- make from regulat ClassInfo for just test file a test case suite (dir) with
-- related childrens another ClassInfo placed in the specified directory
--
-- for TestDir (many-tests-for-one-source)
--
---@param tcases table
---@param dir string?        if not specify use self.path
---@return self
function ClassInfo:setTestCases(tcases, dir)
  self.type = CT_TESTDIR
  self.path = dir or self.path
  self.tcases = tcases
  log.trace("setTestCases %s for testcase dir:%s", tcases ~= nil, dir)
  return self
end

---@return table?
function ClassInfo:getTestCases()
  return self.tcases
end

---@return boolean
function ClassInfo:hasTestCases()
  return self.type == CT_TESTDIR and self.tcases ~= nil
      and next(self.tcases) ~= nil
end

--
-- return a first ClassInfo.type == CT_TEST (file not a dir) or nil
--
-- for example, to quickly open a test-case-file instead of a test directory
--
---@return env.lang.ClassInfo?
function ClassInfo:getFirstCase()
  if self.tcases then
    -- first level find file not a dir
    for _, case in pairs(self.tcases) do
      if case:isTestClass() then
        return case
      end
    end
    -- subdirs
    for _, case in pairs(self.tcases) do
      if case:isTestDir() then
        local file = case:getFirstCase()
        if file then
          return file
        end
      end
    end
  end
end

--
-- return top level parent main TestCasesDir for current TestCaseClassInfo
--
---@return env.lang.ClassInfo?
function ClassInfo:getTopCasesDir()
  local parent, prev_pa = self.parent, self.parent
  local limit = 256 -- avoid infinity loop

  while parent and limit > 0 do
    parent = parent.parent
    if parent then
      prev_pa = parent
    end
    limit = limit - 1
  end

  return prev_pa
end

--------------------------------------------------------------------------------

function ClassInfo.findPkgSep(s)
  if s then
    local i = string.find(s, '.', 1, true) -- java like
    if i then
      return '.'
    else
      i = string.find(s, '\\', 1, true) -- php like
      if i then
        return '\\'
      end
    end
  end
  return '.' -- default
end

---@param s string?
---@param pkgsep string|nil defualt is dot for java-like langs
---@return boolean
function ClassInfo.isClassName(s, pkgsep)
  pkgsep = pkgsep or ClassInfo.findPkgSep(s)
  if s and type(s) == 'string' and #s > 0 then
    local fc = s:sub(1, 1)
    if fc == string.upper(fc) then
      return true
    end
    local i = string.find(s, pkgsep, 1, true)
    if i then
      return ClassInfo.isClassName(s:sub(i + 1), pkgsep)
    end
  end
  return false
end

-- check is str is a classname then return its short name
---@param str string
---@return string?
function ClassInfo.getShortClassNameOrNil(str)
  if ClassInfo.isClassName(str) then
    return ClassInfo.getShortClassNameOf(str)
  end
end

-- for Short Class Name create variable name (lower the first letter)
-- UserId -> userId         (no prefix)
-- JWTHelper -> jwtHelper
-- DAO    -> dao
-- prefix = '$': Id -> $id
---@param scn string
---@param prefix string|nil
function ClassInfo.getVarNameForClassName(scn, prefix)
  prefix = prefix or ''
  if scn then
    if string.match(scn, '^%u+$') then -- DAO -> dao
      return prefix .. string.lower(scn)
    end

    local firstUppers = string.match(scn, '^(%u+)%u%l')
    if firstUppers and #firstUppers > 0 then -- JWTHelper -> jwtHelper
      local len = #firstUppers
      return prefix .. string.lower(firstUppers) .. scn:sub(len + 1, -1)
    end

    -- UserId -> userId
    return prefix .. string.lower(scn:sub(1, 1)) .. scn:sub(2, -1)
  end
  return nil
end

--
--
-- for testing
function ClassInfo:toArray()
  local pair = nil
  if self.pair then
    local tmp = self.pair.pair
    self.pair.pair = nil --  avoid endless looping
    pair = self.pair:toArray()
    self.pair.pair = tmp
    if tmp == self then
      pair.pair = 'self'
    end
  end
  return {
    classname      = self.classname,
    type           = self.type,
    inner_path     = self.inner_path,
    path           = self.path,
    exists         = self.exists,
    testsuite_name = self.testsuite_name,
    testsuite_path = self.testsuite_path,
    tcases         = self.tcases,
    pair           = pair,
  }
end

class.build(ClassInfo)
return ClassInfo

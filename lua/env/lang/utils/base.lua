-- 05-10-2023 @author Swarg
local M = {}
--
local fs = require('env.files')
local log = require('alogger')

-- https://github.com/manoelcampos/xml2lua/tree/master/xmlhandler
local ok_xml2lua, xml2lua = pcall(require, "xml2lua")

local developerName

---@return boolean
function M.is_empty_str(s)
  return not (type(s) == 'string' and s ~= '' and string.match(s, "^%s*$") == nil)
end

---@param s string
---@param shead string
function M.str_starts_with(s, shead)
  return type(s) == 'string' and shead ~= nil and s:sub(1, #shead) == shead
end

---@param s string?
---@param se string ends (tail)
function M.str_ends_with(s, se)
  return type(s) == 'string' and se ~= nil and (se == "" or s:sub(- #se) == se)
end

-- get User Name from git config or from System Env Variable $USER
---@return string?
function M.getDeveloperName()
  if not developerName then
    -- take from git
    local user
    user = fs.execr('git config user.name')
    if user then
      user = user:gsub('\n', '');
    else
      user = os.getenv('USER')
    end
    if user then
      developerName = user:sub(1, 1):upper() .. user:sub(2)
    end
  end
  return developerName
end

function M.clearDevNameCache()
  developerName = nil
end

--
-- return day month year of now if date not given, or parsed from date string
--
---@param date string?
---@return number day
---@return number month
---@return number year
function M.getTodayDayMonthYear(date)
  date = (date or tostring(os.date("%d-%m-%Y")))
  local d, m, y = string.match(date, '^(%d+)%-(%d+)%-(%d+)$')
  d, m, y = tonumber(d), tonumber(m), tonumber(y)
  assert(type(d) == 'number', 'day')
  assert(type(m) == 'number', 'month')
  assert(type(y) == 'number', 'year')
  return d, m, y
end

---@param filename string
---@param mode number?
---@return boolean
---@return string?
function M.mkDirs(filename, mode)
  local dir = fs.extract_path(filename)
  if not fs.dir_exists(dir) then
    local ok, path, ftyp = fs.mkdir(dir, mode)
    if not ok then
      -- show last created file-dir type
      -- to display an error when there is a file in the path instead of a directory
      return false, tostring(ftyp) .. ':' .. tostring(path) -- fail
    end
  end
  return true
end

-- save_file_codes
M.NO_PATH = 0 -- no path or content inner error
M.OK_SAVED = 1
M.ERR_NO_FILENAME = 2
M.ERR_NO_CONTENT = 3
M.ERR_NEW_DIR = 4
M.ERR_OPEN = 5
M.ERR_WRITE = 6
M.SKIP_EXISTS = 7 -- file already exists
M.REWRITE_EXISTS = 8

M.DRY_RUN = 999 -- no path or content inner error

--
-- readable
M.save_file_codes = {
  [M.NO_PATH] = 'no path or body',
  [M.OK_SAVED] = 'ok',
  [M.ERR_NO_FILENAME] = 'no filename',
  [M.ERR_NO_CONTENT] = 'no content',
  [M.ERR_NEW_DIR] = 'cannot create directory',
  [M.ERR_OPEN] = 'cannot open file to write',
  [M.ERR_WRITE] = 'write error',
  [M.SKIP_EXISTS] = 'already exists',
  [M.REWRITE_EXISTS] = 'rewrite exists',
  [M.DRY_RUN] = 'dry-run',
}

---
---
---
---@param filename string
---@param content string
---@param dirmode number?
---@return number  1 - ok
---@return string? error
---@param append? boolean
function M.saveFile(filename, content, dirmode, append)
  log.debug('saving file:', filename)
  if not filename then
    return M.ERR_NO_FILENAME
  end
  if not content then
    return M.ERR_NO_CONTENT
  end
  local ok, errmsg = M.mkDirs(filename, dirmode)
  if not ok then
    log.debug('on mkdir ret last successfule (ftype:path) = "%s"', errmsg)
    return M.ERR_NEW_DIR
  end
  local mode = (append == true) and 'ab' or 'wb'
  local fd, err = io.open(filename, mode)
  if not fd then
    log.debug('err on open file to write', tostring(err))
    return M.ERR_OPEN, err
  end

  local oldout = io.output() -- without argument returns actual output
  io.output(fd)
  io.write(content)
  io.close(fd)
  if oldout == io.stdout then -- can rise error: attempt to use a closed file,
    io.output(oldout)         -- set back to oldout ( normally io.stdout )
  end
  -- collectgarbage("collect") -- Flush & Close open File Handle

  -- changing io.output() back should also close() and flush() the "file"
  -- But better doublecheck this when using userdata without experience
  return M.OK_SAVED -- File Successfuly Created
end

-- some_module --> SomeModule
-- lib/namespace/some_module --> ?
--
---@param fn string filename of the module without extension
---@return string
function M.toPascalCase(fn)
  local res = ''
  if fn then
    for word in string.gmatch(fn, '([^_]+)') do
      res = res .. word:sub(1, 1):upper() .. word:sub(2)
    end
  end
  return res
end

-- SomeModule -> some_module
-- NameSpace.SomeModule  -> name_space/some_module ?
--
-- support only ASCII chars
---@param s string
---@return string
function M.pascalCaseToSnake(s)
  local res, prev = '', nil
  for i = 1, #s do
    local c = string.sub(s, i, i)
    if c ~= '.' and c ~= '/' and c ~= '_' and string.upper(c) == c and
        (c < "0" or c > "9") -- not a digit
    then
      if #res > 0 and prev ~= '.' and prev ~= '/' and prev ~= '_' then
        res = res .. '_'
      end
      c = string.lower(c)
    end
    res = res .. c
    prev = c
  end
  return res
end

--------------------------------------------------------------------------------
--                              CLI
--------------------------------------------------------------------------------

---@param cmd_args table like {"-key", "value"}
function M.get_value_from_args(cmd_args, key)
  if type(cmd_args) == 'table' and key then
    for i = 1, #(cmd_args) do
      local arg = cmd_args[i]
      if arg == key then
        return cmd_args[i + 1]
      end
      local k, v = string.match(arg, "^(%-[%w%-%.]+)=(.*)")
      if k and k == key then
        return v
      end
    end
  end
  return nil
end

--------------------------------------------------------------------------------
--                               XML
--------------------------------------------------------------------------------

---@return false|table
---@return string? errmsg
function M.parse_xml_body(xml, tag)
  if not ok_xml2lua then
    return false, 'not found luarock "xml2lua"'
  end
  -- local handler = xmlhandler:new()
  --
  local handler = require "xmlhandler.tree"
  -- attempt to clear state before parsing
  handler.root = {}
  handler._stack = { handler.root }

  local parser = xml2lua.parser(handler)
  parser:parse(xml)
  -- print("[DEBUG] parseBuildScript:", require "inspect" (handler.root))
  local t = handler.root
  handler.root = nil -- ?
  if type(t) ~= 'table' then
    return false, ': cannot parse given body ' .. tostring(tag or '')
  end

  return t
end

return M

-- 16-07-2024 @author Swarg
-- Goal: accumulate frequently used code in one place
-- Note: cannot require Lang here - e.g used by env.lang.Lang


local M = {}

local class = require 'oop.class'
local base = require 'env.lang.utils.base'
local utbl = require 'env.util.tables'
local fs = require 'env.files'


-- proxy
M.is_empty_str = base.is_empty_str
M.ensure_dir = fs.ensure_dir
M.join_path = fs.join_path
M.get_parent_dirpath = fs.get_parent_dirpath
M.extract_extension = fs.extract_extension


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local is_empty_str = base.is_empty_str
local tbl_merge_flat = utbl.tbl_merge_flat


M.UI_TITLE_NEW_PROJECT = {
  "Create New Project",
  'to create a new project with provided properties enter the `:w` vim command',
  'to validate and update curren project properties use `:e!`'
}

function M.getDefaultProjectProperties()
  return {
    UI_TITLE = "New Project", -- for ObjEditor
    UI_ERRORS = nil,          -- for ObjEditor
    PROJECT_NAME = "app",
    project_root = nil,
    -- CREATE_PROJECT = false,
  }
end

---@return table{bufnr, bufname, ext, cwd}
function M.get_current_context()
  local api = (vim or E).api

  local bufnr, current_filename, ext
  if api then
    bufnr = api.nvim_get_current_buf()
    current_filename = api.nvim_buf_get_name(bufnr)
    if is_empty_str(current_filename) then
      current_filename = nil
    else
      ext = string.match(current_filename, "([^%.%/%s]+)$") or '';
    end
  end

  return {
    bufnr = bufnr,
    bufname = current_filename,
    ext = ext,
    cwd = os.getenv('PWD'),
  }
end

function M.is_java_project(ctx)
  local ext = (ctx or E).ext
  return ext == 'java'
end

---@return string
---@return string? full buffname
function M.get_current_directory()
  local api = (vim or E).api
  local dir, bufname
  if api then
    local bufnr = api.nvim_get_current_buf()
    bufname = bufnr and api.nvim_buf_get_name(bufnr) or nil -- full file name
  end

  if bufname and not is_empty_str(bufname) then
    dir = M.get_parent_dirpath(bufname)
  else
    dir = os.getenv('PWD')
    bufname = nil
  end

  assert(dir and not is_empty_str(dir), 'expected not empty current-directory')
  return fs.ensure_dir(dir), bufname
end

---@param w Cmd4Lua
---@param title table? -- {[1] = "New X Project"}
---@param defpp table?
function M.define_project_properties(w, title, defpp)
  -- table to override
  local t = defpp or M.getDefaultProjectProperties()
  t = tbl_merge_flat(t, w.vars.props)
  t.project_root = M.get_current_directory()
  t.UI_TITLE = tbl_merge_flat(M.UI_TITLE_NEW_PROJECT, title) -- for ObjEditor
  t.CREATE_PROJECT = false                                   -- false to generate the buildscript only

  -- fill project property table(t) by cli options:
  -- w:opt_override("-g", "--group_id", t, "GROUP_ID")
  -- w:opt_override("-a", "--artifact_id", t, "ARTIFACT_ID")
  -- w:opt_override("-v", "--version", t, "VERSION")
  -- w:opt_override('-c', '--main-class', t, "MAINCLASS")
  w:opt_override("-n", "--project_name", t, "PROJECT_NAME")
  w:opt_override("-r", "--project_root", t, "project_root")
  -- TODO:
  -- w:def('no'):opt_override_flag('-p', '--new-project', t, 'CREATE_PROJECT',desc)
  -- t["CREATE_PROJECT"] = "yes"

  if w:desc('Create a New Project (Directory Structure with files)')
      :has_opt("--new-project", "-p") then
    t["CREATE_PROJECT"] = true -- hack to pass value  from opts to lua-table
  end
  -- Notes: In the gradle artifactId by default takes from PROJECT_NAME

  w:desc('Automatic yes to prompts;')
      :desc('assume "yes" as answer to all prompts and run non-interactively.')
      :v_has_opt('--yes', '-y')

  return t
end

---@param pp table
function M.define_raw_snippets_opt(w, pp)
  pp.RAW_SNIPPETS = false
  -- w:opt_override("-S", "--raw-snippets", pp, "RAW_SNIPPETS")
  --
  if w:desc('Consider the sources in the project as independent snippets')
      :has_opt("--raw-snippets", "-S") then
    pp["RAW_SNIPPETS"] = true -- hack to pass value  from opts to lua-table
  end

  return pp
end

--
-- define commonly used options that can be used for generate a NewStuff(files):
-- -R|--dry-run
-- -Q|--quiet     no interaction with user (no prompt) (dont ask any quistions)
-- -D|--no-date
-- -A|--author
-- -X|--not-open  then optkey not passed - open result in the editor
--
-- UsageExample:
--    local gen = w:var('gen') ---@type env.langs.lua.LuaGen
--    gen.defineStdGenOpts(w)  --dry-run --quiet --autho --no-date --not-open
--                             --force-rewrite --verbose
--
---@param w Cmd4Lua
function M.defineStdGenOpts(w)
  w:v_opt_dry_run('-R') --dry-run
  w:v_opt_verbose('-V') --verbose
  w:v_opt_quiet('-Q')   --quiet
  w:v_has_opt('--no-date', '-D')
  w:v_opt('--author', '-A')

  -- to open generated file in the editor:
  w:tag('openInEditor'):def(true):defOptVal(false):optb('--not-open', '-X')
  -- no "--not-open" - return true, has - return false

  w:desc('force verwrite already existing files')
      :v_has_opt('--force-rewrite', '-F')
end

--
-- so that you can manually specify the desired language when
-- there is not a single created source file for this language yet
--
---@param w Cmd4Lua
---@return string?
function M.defineSelectLangByExt(w)
  local ext = w:desc('to select Lang directly via extension')
      :opt('--programming-language', '-PL') ---@cast ext string?

  return ext
end

function M.removeOptSelectLangByExt(w)
  if w and w.keys then
    w.keys['-PL'], w.keys['--programming-language'] = nil, nil
  end
end

---@param w Cmd4Lua
function M.clearSelectLangByExt(w)
  if w.keys then
    w.keys['-PL'] = nil
    w.keys['--programming-language'] = nil
  end
end

--
-- '.command.new_project'
--
---@param modname string module name relative or full module name
---@param lang_clazz env.lang.Lang?
---@param w Cmd4Lua
---@return boolean
function M.pass_cmd_to_handler(w, modname, lang_clazz)
  assert(type(modname) == 'string' and modname ~= '', 'expected string modname')
  local cn = nil
  if lang_clazz then
    cn = class.name(lang_clazz)
    local ns = class.get_package_name(cn)
    modname = tostring(ns) .. modname --
  end
  local ok, modt = pcall(require, modname)
  if not ok then
    error(fmt('Not found module: "%s" for class "%s" errmsg: %s',
      modname, v2s(cn), v2s(modt)))
  end
  modt.handle(w)
  return true
end

return M

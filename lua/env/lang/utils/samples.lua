-- 10-08-2024 @author Swarg
local log = require 'alogger'
local tu = require 'env.util.tables'
local Editor = require 'env.ui.Editor'
local Context = require 'env.ui.Context'
local hdgen = require 'hdgen'

local M = {}

-- word:format:items:extra
local MATCH_PATTERN = "^([%w_%.]+):?([%w%-_%.]*):?([%w%-_%.]*):?([%w%-_%.]*)$"

M.samples = {
  constellations = {
    "Andromeda", "Corvus", "Centaurus", "Lynx", "Coma Berenices", "Ursa Major",
    "Virgo", "Sculptor", "Ursa Major", "Circinus", "Coma Berenices", "Sculptor",
    "Pavo", "Sextans", "Eridanus", "Canes Venatici", "Cygnus and Cepheus",
    "Lynx", "Canes Venatici", "Serpens Caput", "Dorado/Mensa", "Volans",
    "Pegasus", "Coma Berenices", "Ursa Major", "Sculptor", "Coma Berenices",
    "Tucana", "Ursa Major", "Sagittarius (centre)", "Coma Berenices", "Cetus",
    "Pegasus", "Hydra", "Ursa Major", "Sculptor", "Virgo", "Hydra", "Boötes",
    "Canes Venatici", "Draco", "Triangulum", "Canes Venatici"
  },
  planets = {
    'Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune'
  },
  trees = {
    'Apple', 'Ash', 'Aspen', 'Balsam', 'Birch', 'Boxwood', 'Butternut', 'Cedar',
    'Dogwood', 'Fir', 'Larch', 'Lemon', 'Linden', 'Mango', 'Maple', 'Oak',
    'Olive', 'Palm', 'Pine', 'Poplar', 'Spruce',
  },
  fruits = {
    'Ackee', 'Apple', 'Apricot', 'Banana', 'Date', 'Durian', 'Fig',
    'Kiwi', 'Mango', 'Olive', 'Orange', 'Papaya', 'Pineapple', 'Tangerine',
    'Watermelon'
  },
  berries = {
    'Raspberry', 'Currant', 'Blueberry', 'Cranberry', 'Gooseberry', 'Strawberry',
  },
  vegetable = {
    root = {
      "Beetroot", "Carrot", "Turnip", "Radish", "Celeriac", "Sugar beet",
      "Horseradish", "Parsnip",
    },
    leafy = {
      "Cabbage", "Spinach", "Mint", "Mustard Green", "Spring Onion", "Arugula",
      "Coriander Leave", "Celery", "Lettuce", "Bok Choy", "Rapini", "Kale",
    },
    stem = {
      "Lemon grass", "Asparagus", "Celery", "Kohlrabi", "Celtuce",
      "Rhubarb", "Swiss chard", "Cardoon",
    },
    flower = {
      "Cauliflower", "Broccoli", "Artichoke", "Banana Flower", "Zucchini",
    },
    bulb = {
      "Onion", "Spring Onion", "Garlic", "Leek", "Fennel",
    },
    tuber = {
      "Potato", "Ginger", "Turmeric", "Chinese Potato", "Arrowroot", "Tapioca",
      "Elephant yam", "Greater yam", "Purple yam", "Arracacia",
    },
    fruit = {
      "Cucumber", "Pumpkin", "Tomato", "Peppers", "Eggplant", "String beans",
      "Green peas", "Corn", "Lady’s finger", "Beans", "Chickpeas"
    }
  },
  months = {
    'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'
  },
  days = {
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'
  },
  lorem = {
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
    'sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi',
    'ut aliquip ex ea commodo consequat.',
    'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum',
    'dolore eu fugiat nulla pariatur.',
    'Excepteur sint occaecat cupidatat non proident,',
    'sunt in culpa qui officia deserunt mollit anim id est laborum.'
  },

  -- https://www.lua.org/history.html
  luahist = {
    "In this paper, we report the history of the Lua programming language.",
    "Since its inception, as an in-house language for two specific projects, ",
    "Lua has gone far beyond our most optimistic expectations. ",
    "We think that the main reasons for this success lie in ",
    "our original design decisions: keep the language simple and small; ",
    "keep the implementation simple, small, fast, portable, and free."
  },

  late2party = {
    "I'm arriving a bit late to this particular party,",
    "but there is an issue with this code."
  }

  -- see also samples in spinppets.lua for specific lang
}
-- aliases
M.samples.ipsum = M.samples.lorem
M.samples.vegs = M.samples.vegetable

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


local function get_known_samples()
  local s = ''
  for k, _ in pairs(M.samples) do
    if s ~= '' then s = s .. ' ' end
    s = s .. v2s(k)
  end
  return s
end

-- split text by newlines
---@param text string
local function split(text)
  local t = {}
  for s in string.gmatch(text, '([^\n]+)') do t[#t + 1] = s end
  return t
end

local random_aliases = { r = 1, random = 1, shuffle = 1, rand = 1, rnd = 1 }

local USAGE = [[
This is a handler for inserting a set of placeholder words(samples)

# Syntax:
name:format:items:extra

- format: one of [lua|json|one-line|text] - output format
- items: start-end - to pick sublist of items
- extra: shuffle|random

# Usage Example:
planets:lua:1-4:rnd
]]

--
-- "${HANDLER_ADD_SAMPLE}",
function M.handler_add_sample()
  local ctx = Editor:resolveWordSimple('[%w_%.%-:]')
  -- todo nested structure with dot
  local word_n_fmt, errmsg = ctx:getWordElement(Context.LEXEME_TYPE.UNKNOWN)
  if not word_n_fmt then
    return false, errmsg
  end

  -- key:format:items
  local word, format, items, extra = match(word_n_fmt or '', MATCH_PATTERN)
  log.debug("word_n_fmt:%s w:%s f:%s i:%s e:%s",
    word_n_fmt, word, format, items, extra)

  if word == 'help' then
    return false, USAGE
  end
  if not M.samples[word or false] then
    local details = format ~= '' and '(' .. v2s(word_n_fmt) .. ')' or ''

    return false, fmt('unknown word sample: "%s"%s known: [%s] (use help)',
      v2s(word), v2s(details), get_known_samples())
  end

  local lines, err = M.do_add_sample(word, format, items, extra)
  if not lines then
    return false, err
  end

  return ctx:replaceCurrentWordByLines(lines)
end

--
--
---@param name string
---@param format string
---@return false|table:string
---@return string? errmsg
function M.do_add_sample(name, format, items, extra)
  if name == 'help' then
    return false, USAGE
  end
  local t = M.samples[name or false]
  if not t then
    return false, fmt('unknown word sample: "%s" known: [%s]',
      v2s(name), get_known_samples())
  end

  local lines
  local opts = { items = items, extra = extra }

  if format == 'lua' or format == 'lua-table' or format == 'l' then
    lines = require 'inspect' (M.apply_mods(t, opts, true))
    --
  elseif format == 'json' or format == 'j' then
    local ok, cjson = pcall(require, 'cjson')
    if not ok then return false, cjson end -- errmsg if not installed
    lines = split(cjson.encode(M.apply_mods(t, opts, true)))
    --
  elseif format == 'one-line' or format == 'ol' or format == 'inline' then
    lines = { table.concat(M.apply_mods(t, opts, false), ' ') }
    --
  elseif format == 'text' or format == 't' or format == '' or format == 's' then
    lines = M.apply_mods(t, opts, false)
    --
  elseif format:sub(1, 5) == 'html-' then
    lines = M.wrap_to_html(M.apply_mods(t, opts, false), format:sub(6))
  else
    return false, 'supported format: text one-line lua json' ..
        ', got:' .. v2s(format)
  end

  return lines
end

--
---@param t table
---@param opts table{items:string, extra}
---@param as_obj boolean
---@return table
function M.apply_mods(t, opts, as_obj)
  if type(t) ~= 'table' then
    t = split(tostring(t))
  end
  if not as_obj then
    -- case: nested table vegetable.root
    t = tu.flatten_map_to_list(t, {})
  end

  if random_aliases[opts.extra or false] then
    t = tu.shuffle2new(t)
    -- todo depth foreach to shuffle nested subtables
  end

  -- only specified count from start(3) or sublist(1-5)
  if opts.items and opts.items ~= '' then
    local s, e = match(opts.items, '^(%d+)-?(%d*)$')
    s, e = tonumber(s), tonumber(e)
    if e == nil then
      s, e = 1, (s or 1) -- count from start not a range or first
    end
    t = tu.get_sublist(t, s, e)
  end

  return t
end

---@param lines table
---@param tagname string
function M.wrap_to_html(lines, tagname)
  local h = hdgen.new({ format = true, tag = '  ' })
  local a, li = h.a, h.li

  local t = {}
  if tagname == 'ul' or tagname == 'ol' then -- ordered list
    local elm = h[tagname](function(add)
      for _, line in ipairs(lines) do
        add(li(line))
      end
    end)
    h:render_lines(elm, 1, t)
  elseif tagname == 'a' then
    h:render_lines(h.div(function(add)
      for _, line in ipairs(lines) do
        add(a { hred = "/" .. line, line })
      end
    end), 1, t)
  elseif tagname == 'p' or tagname == 'span' then
    local tag = h[tagname]
    h:render_lines(h.div(function(add)
      for _, line in ipairs(lines) do
        add(tag { line })
      end
    end), 1, t)
  end

  return t
end

return M

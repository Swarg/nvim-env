-- 16-07-2024 @author Swarg
local M = {}
-- Goal: utility for working with templates

M.SUBSTITUTE_PATTERN = "%${([%w_]+)}" -- ${KEY} ${THE_KEY}


---@param substitutions table
---@param upperkey boolean?
local function normalize_subs(substitutions, upperkey)
  local sub_key_pattern = M.SUBSTITUTE_PATTERN
  -- prepare and normalize substitutions for insertion into template
  local t = {}
  for key, value in pairs(substitutions) do
    local key0 = upperkey == true and string.upper(key) or key
    t[key0] = value or ''
  end

  -- resolve placeholders in the substitutions-map-values
  for k, v in pairs(t) do
    local typ = type(v)
    if typ == 'string' then
      if string.find(v, sub_key_pattern, 1, false) then -- recursively
        t[k] = string.gsub(v, sub_key_pattern, t)
      end
    else
      -- print("[DEBUG] v:", typ, require"inspect"(v))
    end
  end
  return t
end

--
-- build string from template based substitutions
-- all keys with false value will tryed to removed with empty line
--
-- in template keys must be specified in this manear:
-- "${KEY}=..." in the "substitutions" table  keys must be without "${" and "}"
-- See ComponentGen.SUBSTITUTE_PATTERN
--
---@param template string
---@param substitutions table
---@return string
function M.substitute(template, substitutions, upperkey)
  assert(type(template) == 'string', 'expected string template')

  local kv_map = normalize_subs(substitutions or {}, upperkey)

  -- normalize template - remove lines with placeholders what not provided
  for key, value in pairs(kv_map) do
    if not value or value == '' then
      -- remove whole lines this one placeholder to empty value
      local p = "\n%${" .. key .. "}\n" -- \n
      if string.match(template, p) then
        template = string.gsub(template, p, "\n")
      else
        kv_map[key] = ''
      end
    end
  end

  local s = string.gsub(template, M.SUBSTITUTE_PATTERN, kv_map)

  -- remove empty lines with whitespaces only "..   \n" -> "..\n"
  -- %s consume and \n too that's why use only spaces('% ') here, not '%s'
  s = string.gsub(s, "% +\n", "") -- ! spaces + \n
  return s
end

--
-- append to end of string value in given table for given key
-- modify given table
--
---@param t table
---@param key string
---@param value string
---@param sep string? def is newline
function M.append(t, key, value, sep)
  assert(type(t) == 'table', 't')
  sep = sep or "\n"
  local s = t[key]
  s = s or ''
  if s ~= '' then
    s = s .. sep
  end
  s = s .. value
  t[key] = s
  return s
end

---@param t table
---@param key string
---@param value string
function M.set_if_empty(t, key, value)
  local s = t[key]
  if s == nil or s == '' then
    t[key] = value
  end
  return t
end

return M

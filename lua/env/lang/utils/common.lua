-- 30-08-2024 @author Swarg

local su = require 'env.sutil'
-- local fs = require 'env.files'
local utables = require 'env.util.tables'
local Editor = require 'env.ui.Editor'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local M = {}

function M.handler_mk_file_ref()
  local editor = Editor:new()
  local fn = editor:getCurrentFile()
  local cwd = os.getenv('PWD')
  if cwd and fn and su.starts_with(fn, cwd) then
    fn = './' .. fn:sub(#cwd + 2, #fn)
  end
  editor:getContext():insertAfterCurrentLine(v2s(fn))
  return true
end

function M.short_dep_name(fullname)
  local sn = ''
  for s in string.gmatch(fullname, '([^%-_]+)') do
    local num = string.match(s, ".(%d+)$") or ''
    sn = sn .. s:sub(1, 1) .. num
  end
  return sn
end

---@param map table
---@param name string
---@return table?
function M.find_dependency_by_name(map, name)
  local dep = map[name or false]
  if not dep and name and #name > 1 and #name < 6 then
    for k, d in pairs(map) do
      if M.short_dep_name(k) == name then
        return d
      end
    end
  end
  return dep
end

---@param map table
---@param names table
---@return table?
function M.pick_deps_by_names(map, names)
  local t = nil
  if type(map) == 'table' and next(map) ~= nil then
    t = {}
    for _, name in ipairs(names) do
      t[#t + 1] = M.find_dependency_by_name(map, name)
    end
  end
  return t
end

---@param deps_map table
---@return table?
function M.pick_deps_interactively(deps_map)
  local names = utables.sorted_keys(deps_map)
  local selected_names = Editor:new():askItems(names, 'Choose Dependencies')
  return M.pick_deps_by_names(deps_map, selected_names)
end

return M

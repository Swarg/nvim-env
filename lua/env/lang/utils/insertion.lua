-- 15-03-2024 @author Swarg
-- Utils for EnvLineInsert
local M = {}

local log = require('alogger')
local class = require("oop.class")
local bu = require('env.bufutil')
local u8 = require("env.util.utf8")
local parser = require("env.util.code_parser")
local clipboard = require("env.util.clipboard")

local E, v2s, fmt = {}, tostring, string.format

local S = require 'env.lang.utils.samples'

local shared_handlers = {
  ADD_SAMPLE = S.handler_add_sample --
}

-- shared insertion for all extensions (EnvLineInsert)
-- Goal: allow to define handlers that can works based on world in the current
-- line like: fruits:text:8 -> `:LineInsert 0`(shortcut) -> substitute sample
M.shared_insertions = {
  ['0'] = "${HANDLER_ADD_SAMPLE}", -- forks fine only with big insertion list
  [0] = "${HANDLER_ADD_SAMPLE}",   -- to fix vim.fn.inputlist issue(ret only number)
}


-- current buff info
---@return table{bufrn, bufnamr, cursor_row, cursor_col, current_line, can_edit}
function M.get_context(opts)
  local cbi = bu.current_buf_info()
  cbi.selection = bu.get_selection_range(0, opts)
  cbi.visual_mode = bu.is_visual_mode()
  return cbi
end

---@param subscriber function?
---@param cbi table{mappings}
local function call_subscriber(subscriber, key, cbi, str, word)
  assert(type(cbi) == 'table', 'cbi')
  assert(type(cbi.mappings) == 'table', 'mappings')

  if subscriber and type(subscriber) == 'function' then
    return subscriber(key, cbi, str, word)
  end

  return str
end

local toggle_bools = {
  ['true'] = 'false',
  ['false'] = 'true',
  ['T'] = 'F',
  ['F'] = 'T',
  ['0'] = '1',
  ['1'] = '0',
}

function M.mk_capitalized_word(word, sep)
  local s = word:sub(1, 1):upper() .. word:sub(2)
  local j = string.find(s, ' ')
  if j then
    sep = sep or ''
    s = s:sub(1, j - 1) .. sep .. s:sub(j + 1, j + 1):upper() .. s:sub(j + 2)
  end
  return s
end

-- Substitute values for inner varibles
-- Implemented: ${MODULE}, ${FUNCTION}
---@param cbi table
---@param str string -- an original string to modify
---@return string|false
function M.substitute_vars(cbi, str)
  log.debug("substitute_vars index:", (cbi or E).index)

  if cbi and str and str ~= '' then
    assert(cbi.index, 'index of the snippet')
    local subscriber = (cbi.subscribers or E)[cbi.index]
    log.debug('substitute handler:%s\n%s', subscriber, str)

    if str:find('${WORD}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        str = str:gsub('${WORD}', word)
        cbi.word_s, cbi.word_e = s, e
        str = call_subscriber(subscriber, '${WORD}', cbi, str, word)
      end
    end
    if str:find('${IDENTIFIER}') then -- container.nested.id
      local word, s, e = M.buf_word_under_cursor(cbi, nil, "[%w_%.%$]")
      if word then
        str = str:gsub('${IDENTIFIER}', word)
        cbi.word_s, cbi.word_e = s, e
        str = call_subscriber(subscriber, '${IDENTIFIER}', cbi, str, word)
      end
    end


    if str:find('${CAPITALIZE_WORD}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        local capitalized_word = M.mk_capitalized_word(word)
        str = str:gsub('${CAPITALIZE_WORD}', capitalized_word)
        cbi.word_s, cbi.word_e = s, e
      end
    end

    -- CAPITALIZE_WORD + QUOTE - order item -> "Order Item"
    if str:find('${CAPITALIZE_WORD_WITH_SPACE}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        local name = M.mk_capitalized_word(word, ' ')
        str = str:gsub('${CAPITALIZE_WORD_WITH_SPACE}', name)
        cbi.word_s, cbi.word_e = s, e
      end
    end

    if str:find('${CAPITALIZE_WORD_WITH_UNDERSCORE}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        local name = M.mk_capitalized_word(word, '_')
        str = str:gsub('${CAPITALIZE_WORD_WITH_UNDERSCORE}', name)
        cbi.word_s, cbi.word_e = s, e
      end
    end

    if str:find('${WORD_UTF8}') then
      local word, s, e = M.buf_word_under_cursor_utf8(cbi)
      if word then
        str = str:gsub('${WORD_UTF8}', word)
        cbi.word_s, cbi.word_e = s, e
        -- with cbi.words and cbi.word_idx
        str = call_subscriber(subscriber, '${WORD_UTF8}', cbi, str, word)
      end
    end
    if str:find('${SWORD}') then -- space
      local word, s, e = M.buf_word_under_cursor(cbi, 0, '[^%s]+')
      word = word or ''
      local lc = word:sub(-1, -1)
      if lc == ',' or lc == '.' or lc == ';' or lc == ':' then
        word = word:sub(1, -2)
        e = e - 1
      end
      str = str:gsub('${SWORD}', word)
      cbi.word_s, cbi.word_e = s, e
      str = call_subscriber(subscriber, '${SWORD}', cbi, str, word)
    end
    if str:find('${TOGGLE_BOOL}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        word = toggle_bools[word] or word
        str = str:gsub('${TOGGLE_BOOL}', word)
        cbi.word_s, cbi.word_e = s, e
        -- str = call_subscriber(subscriber, '${WORD}', cbi, str, word)
      end
    end
    if str:find('${MD_TITLE}') then
      local title = cbi.current_line:lower():gsub('[%s:]', '-')
      str = str:gsub('${MD_TITLE}', title)
      cbi.word_s, cbi.word_e = 1, -1 -- replace all line
    end
    if str:find('${ADD_PKG2WORD}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      local pkg = call_subscriber(subscriber, '${ADD_PKG2WORD}', cbi, str, word)
      if not pkg then
        return false -- cancel
      end
      str = str:gsub('${ADD_PKG2WORD}', pkg)
      cbi.word_s, cbi.word_e = s, e
    end
    if str:find('${PREV_WORD}') then
      local prev_word = M.buf_word_under_cursor(cbi, -1) or ''
      if prev_word then
        str = str:gsub('${PREV_WORD}', prev_word)
      end
    end
    if str:find('${NEXT_WORD}') then
      local next_word = M.buf_word_under_cursor(cbi, 1)
      if next_word then
        str = str:gsub('${NEXT_WORD}', next_word)
      end
    end
    if str:find('${TAB}') then
      local tab = cbi.current_line:match('^(%s*)') --:sub(1, -2)
      str = str:gsub('${TAB} ', tab)
    end
    if str:find('${DATE}') then
      local date = os.date('%d-%m-%Y')
      str = str:gsub('${DATE}', date)
    end
    if str:find('${TIME}') then
      local time = os.date('%H:%M:%S')
      str = str:gsub('${TIME}', time)
    end
    if str:find('${AUTHOR}') then
      local user = os.getenv('USER')
      if user then
        user = user:sub(1, 1):upper() .. user:sub(2)
        str = str:gsub('${AUTHOR}', user)
      end
    end
    -- todo rewrite to use instances of the Lang
    local has_module = str:find('${MODULE}')
    if has_module then
      local project = parser.project_of_buf(cbi)
      local module = parser.get_module(project, cbi.bufname)
      if module and module ~= '' then
        str = str:gsub('${MODULE}', module)
      end
    end
    if str:find('${PACKAGE}') then -- module without filename
      local project = parser.project_of_buf(cbi)
      local module = parser.get_module(project, cbi.bufname)
      if module and module ~= '' then
        local pkg = class.get_package_name(module)
        if pkg then
          str = str:gsub('${PACKAGE}', pkg)
        end
      end
    end
    local has_function = str:find('${FUNCTION}')
    if (has_function) then
      local func = parser.get_function_name(cbi.current_line)
      if func and func ~= '' then
        str = str:gsub('${FUNCTION}', func)
      end
    end

    local has_func_params = str:find('${FUNC_PARAMS}')
    if (has_func_params) then
      local fp = parser.get_function_params(cbi.current_line, cbi.cursor_col)
      if fp then
        str = str:gsub('${FUNC_PARAMS}', fp)
      end
    end

    local has_class = str:find('${CLASS}') -- class of the current method
    if (has_class) then
      local klass = ''
      if cbi.ext == 'lua' then
        local lp = require('env.lua_parser')
        local class0 = lp.get_signature_of_method(cbi.bufnr, cbi.cursor_row)
        if class0 then klass = class0 end
      end
      str = str:gsub('${CLASS}', klass)
    end

    if str:find('${VARS_TOSTRING}') then
      local v = call_subscriber(subscriber, '${VARS_TOSTRING}', cbi, str, nil)
      str = str:gsub('${VARS_TOSTRING}', v)
    end

    if str:find('${DEF_LOCAL}') then
      local word, s, e = M.buf_word_under_cursor(cbi)
      if word then
        cbi.word_s, cbi.word_e = s, e
        word = toggle_bools[word] or word
      end
      local v = call_subscriber(subscriber, '${DEF_LOCAL}', cbi, str, word)
      str = str:gsub('${DEF_LOCAL}', v)
    end
  end
  return str
end

-- call function in nvim under the cursor in current opened buffer
-- cmd verbose mem status clean
-- get word in current buffer under the cursor (by column value)
---@param cbi table{current_line, cursor_col}
---@param offset number? word offset 0 - no, -1 prev word, 1 - next word
function M.buf_word_under_cursor(cbi, offset, chars_pattern)
  if cbi then
    local col = cbi.cursor_col + 1 -- NOTE! vim return index from 0, lua works 1
    if cbi.current_line and col then
      -- case then cursor under space between tho words
      -- 'word_left<SPACE>word_right'  --> "word_left word_right"
      if col > 1 and cbi.current_line:sub(col, col) == ' ' then
        return parser.get_word_at_ex(cbi.current_line, col, offset, chars_pattern)
      else
        return parser.get_word_at(cbi.current_line, col, offset, chars_pattern)
      end
    end
  end
  return nil
end

---@param cbi table{} add fields: words and word_idx
---@param offset number?
---@param sep_pattern string?
---@diagnostic disable-next-line: unused-local
function M.buf_word_under_cursor_utf8(cbi, offset, sep_pattern)
  if cbi then
    local col = cbi.cursor_col + 1
    if cbi.current_line and col then
      local words = u8.split_words(cbi.current_line, true)
      if words then
        local word, idx = u8.get_word_at(words, col, false)
        local range = words.brange[idx or false] or {}
        cbi.words = words
        cbi.word_idx = idx
        return word, range[1], range[2]
      end
    end
  end
  return nil
end

---@param insertion string
function M.get_handler_name(insertion)
  return string.match(insertion, '${HANDLER_([%w_]+)}')
end

---@return string? cmd
---@return string? argline
function M.get_vim_command(s)
  local cmd, argline = string.match(s, '^:(%u[%w]+)%s*(.-)%s*<CR>$')
  return cmd, argline
end

--
---@param cbi table
---@param insertion string - snippet
---@param handler_name string?
function M.call_handler(cbi, insertion, handler_name)
  local handler = (cbi.subscribers or E)[handler_name] or
      shared_handlers[handler_name]

  if type(handler) ~= 'function' then
    error(fmt('expected function handler for "%s" got: %s',
      v2s(handler_name), type(handler)))
  end

  local ok, errmsg = handler(cbi, insertion)
  if not ok then
    cbi.errmsg = v2s(errmsg or ('error in ' .. v2s(handler_name)))
    return false
  end

  return true
end

---@param cbi table current buffer info (n, name, cursor pos + mappings + index
---@param insertion string line for insertion to buffer
---@return number|false
function M.line_insert_str_to_cursor(cbi, insertion)
  assert(cbi.mappings, 'has mappings')

  if cbi and insertion and insertion ~= "" then
    local rowb, rowe, col = cbi.cursor_row - 1, cbi.cursor_row, cbi.cursor_col

    local full_line_pos = { string.find(insertion, "${LINE}", 1, true) }
    if next(full_line_pos) then
      return M.handle_full_line_insertion(cbi, full_line_pos, insertion)
    end

    local cooked = M.substitute_vars(cbi, insertion) -- cooked

    -- cancel
    if cooked == false then return false end

    insertion = cooked

    if not cbi.can_edit then
      clipboard.copy_to_clipboard(insertion)
      cbi.errmsg = 'Not Modifiable buffer. Copied to clipboard'
      return false
    end

    local ln1, ln2 = cbi.selection.line1, cbi.selection.line2
    if ln1 and ln1 ~= ln2 then
      rowb, rowe = ln1, ln2
    end
    -- check if string to insert has multiples lines
    local lines = {}

    -- case: insert before or after current-line
    if insertion:find('\n') then
      local before = insertion:sub(1, 1) == '\n'
      if not before then -- current line stay before inserted
        table.insert(lines, cbi.current_line)
      else
        insertion = insertion:sub(2)
      end
      M.append_line(lines, insertion) -- can insert empty lines in the middle
      if lines[#lines] == '' then lines[#lines] = nil end
      -- for line in insertion:gmatch("([^\n]*)\n?") do -- without empty lines
      --   if line and line ~= '' then table.insert(lines, line) end
      -- end
      if before then -- current line stay after inserted lines
        table.insert(lines, cbi.current_line)
      end
      -- case insert into current-line
    else
      local s, e = col, col             -- replacement range in original string
      if cbi.word_s and cbi.word_e then -- works only for one ${WORD} in pattern
        s, e = cbi.word_s, cbi.word_e
      end
      local newline
      if s == 1 and e == -1 then
        newline = insertion
      else
        local left = cbi.current_line:sub(1, s - 1)
        local right = cbi.current_line:sub(e + 1, #cbi.current_line)
        newline = left .. insertion .. right
      end
      table.insert(lines, newline) -- lines = { newline }
    end

    -- vim.api.nvim_buf_set_lines(0, row - 1, row, true, lines)
    vim.api.nvim_buf_set_lines(0, rowb, rowe, true, lines)
    return #lines
  end
  return false
end

--
-- "abc\nde" -> {abc} {de}        (split by "\n")
-- Goal: if line has newlines split to sublines and add into t
-- overwise add as one line to t
--
---@param t table
---@param line string
function M.append_line(t, line)
  t = t or {}
  local prevp = 1

  while true do
    local p = string.find(line, "\n", prevp, true)
    if not p then break end
    t[#t + 1] = string.sub(line, prevp, p - 1)
    prevp = p + 1
  end
  t[#t + 1] = string.sub(line, prevp)

  return t
end

-- special case of insertion ${LINE} with multiline supporting
--
---@param cbi table
---@param pos table pos for ${LINE} in template
---@param str string insertion template
---@return number|false
function M.handle_full_line_insertion(cbi, pos, str)
  assert(type(pos) == 'table' and pos[1] ~= nil and pos[2] ~= nil, 'pos')
  local ln1, ln2 = cbi.selection.line1, cbi.selection.line2
  local t, tab = {}, nil

  local tmpl_left = str:sub(1, pos[1] - 1)
  local tmpl_right = str:sub(pos[2] + 1)
  local old_lines_count

  if ln1 and ln2 and ln1 ~= ln2 then
    assert(ln1 < ln2, 'ln1 < ln2')
    local lines = vim.api.nvim_buf_get_lines(0, ln1, ln2, true)
    M.append_line(t, tmpl_left)
    if t[#t] == '' then t[#t] = nil end
    for _, line in ipairs(lines) do
      t[#t + 1] = line
    end
    if tmpl_right:sub(1, 1) == "\n" then tmpl_right = tmpl_right:sub(2) end
    if pos[2] < #str then
      M.append_line(t, tmpl_right)
    end
    old_lines_count = #lines
  else
    tab = string.match(cbi.current_line, '^(%s*)')
    M.append_line(t, tab .. tmpl_left .. cbi.current_line:sub(#tab) .. tmpl_right)
    -- log.trace("IN:  %s\nOUT: %s", line, updated)
    old_lines_count, ln1 = 1, ln1 - 1
  end

  if not cbi.can_edit then
    clipboard.copy_to_clipboard(table.concat(t, "\n"))
    print('Not Modifiable buffer. Copied to clipboard')
    return false
  end

  vim.api.nvim_buf_set_lines(0, ln1, ln2, true, t)

  return old_lines_count
end

--
--
---@param mappings table
---@param key string
---@param value string
function M.update_mappings(mappings, key, value)
  M.remove_mappings(mappings, key)
  -- delete
  if not value or value == 'nil' then
    return true
  end
  -- add new
  return M.add_mappings(mappings, key, value)
end

---@param mappings table
---@param key string
function M.remove_mappings(mappings, key)
  if mappings then
    for k, v in pairs(mappings) do
      assert(type(v) == 'table', 'v')
      local cnt = #v
      for i, n in ipairs(v) do
        if n == key then
          if i < cnt then -- keep it without holes
            v[i] = v[cnt]
            i = cnt
          end
          v[i] = nil -- remove
          return k
        end
      end
    end
  end
end

---@param mappings table
---@param key string
function M.mappings_index(mappings, key)
  for i, n in pairs(mappings) do
    if n == key then
      return i
    end
  end
end

---@param mappings table
---@param key string
---@param value string
function M.add_mappings(mappings, key, value)
  assert(type(key) == 'string', 'key')
  assert(type(value) == 'string', 'value')

  local t = mappings[value] or {}
  if not M.mappings_index(mappings, key) then
    t[#t + 1] = key
    mappings[value] = t
    return true
  end
  return false
end

-- key -> replacement
function M.map_word(mappings, word)
  for k, v in pairs(mappings) do
    assert(type(v) == 'table', 'v')
    for _, n in pairs(v) do
      if n == word then
        return k
      end
    end
  end
end

-- find package definition via "oop" rock:  class.package 'io.package'
---@return string?
function M.find_class_package(lang, bufnr, limit)
  -- log.debug("find_class_package", lang)
  bufnr = bufnr or 0
  limit = limit or 64
  if lang == 'lua' then -- oop.class
    local line, n = '', 0

    while line and n < limit do
      line = (vim.api.nvim_buf_get_lines(bufnr, n, n + 1, true) or E)[1]
      if line then
        local pkg = line:match("^class.package%s*%(?['\"]([%w%.%_]+)['\"]%)?")
        if pkg then
          -- log.debug("found class_package at lnum", n)
          return pkg
        end
      end
      n = n + 1
    end
  end
  -- log.debug("not found class_package")
end

-- Defaults templates for insertion

local insert_line_items = {
  def = {
    '${DATE} ${TIME}',
    '#!/bin/bash\n\n',
    '#!/usr/bin/env bash\n',
    '#!/usr/bin/env lua\n',
  }
}

function M.add_defaults(t)
  for k, v in pairs(insert_line_items) do
    t.insert_line_items[k] = v
  end

  -- aliases
  -- t.insert_line_items.sh = t.insert_line_items.bash
end

-- register insertions for given extension
--
---@param ext string|table{string}  - one or multiple extension (sh + bash)
---@param items table     - list of strings (items to insert)
---@param mappings table? - state-config for dynamic handler for correspond item
---@param handlers table?
function M.register_ext(t, ext, items, mappings, handlers, samples)
  local et = type(ext)
  if (et ~= 'string' and et == 'table' and type(ext[1]) ~= 'string') then
    local el1 = et == 'table' and ext[1] or nil
    error('ext must be string or table of strings, got: ' ..
      type(ext) .. ' elm-1: ' .. type(el1))
  end

  assert(type(items) == 'table', 'items')
  assert(type(t) == 'table', 't main storage of the items')

  if (t.insert_line_items[ext]) then log.debug('overriding lang', ext) end
  local exts = ext
  if et == 'string' then exts = { ext } end
  assert(type(exts) == 'table', 'exts')

  for _, ext0 in pairs(exts) do
    t.insert_line_items[ext0] = items
    t.insert_line_mappings[ext0] = mappings
    t.insert_line_subscribers[ext0] = handlers
  end

  if type(samples) == 'table' then
    for key, value in pairs(samples) do
      local rewrite = S.samples[key] ~= nil -- override previous value
      S.samples[key] = value
      log.debug('EnvLineInsert: %s: add key:%s override prev', ext, key, rewrite)
    end
  end
end

function M.build_insertion(t, config)
  M.add_defaults(t)

  local cb = function(ext, items, mappings, handlers, samples)
    M.register_ext(t, ext, items, mappings, handlers, samples)
  end

  -- todo move all langs to same namespace into env.langs.* and add
  -- automaticly loading the parrens from env.langs.*.patterns
  require('env.langs.c.snippets').add(cb)
  require('env.langs.lua.snippets').add(cb)
  require('env.langs.java.snippets').add(cb)
  require('env.langs.php.snippets').add(cb)
  require('env.langs.elixir.snippets').add(cb)
  require('env.langs.markdown.snippets').add(cb)
  require('env.langs.bash.snippets').add(cb)
  require('env.langs.vtt.snippets').add(cb)
  require('env.langs.svelte.snippets').add(cb)
  require('env.langs.js.snippets').add(cb)
  require('env.langs.yaml.snippets').add(cb)
  -- TODO load from config
  config = config
end

--
-- used for swap-items on the fly
-- caches indexes on first access for quick access on subsequent accesses
--
---@param subscriber function
---@param subscribers table
function M.get_subscriber_index(subscriber, subscribers)
  assert(subscribers, 'has subscribers')

  -- to cache indexes on the runtime to fast access
  subscribers.map = subscribers.map or {}

  local idx = subscribers.map[subscriber]

  if not idx then
    for i, v in pairs(subscribers) do
      if v == subscriber then
        subscribers.map[subscriber] = i -- cached
        idx = i
        break
      end
    end
  end

  return idx
end

function M.dump_state(mod)
  local state = {
    items = mod.insert_line_items,
    mappings = mod.insert_line_items,
    subscribers = mod.insert_line_subscribers,
  }
  _G.__env_insert_line_state = state
end

function M.restore_state(mod)
  local state = _G.__env_insert_line_state
  _G.__env_insert_line_state = nil

  mod.insert_line_items = state.items
  mod.insert_line_items = state.mappings
  mod.insert_line_subscribers = state.subscribers
end

return M

--
-- Goal: Figureout how works lua, lua-gc and nvim, findout memory leaks
-- for manual testing via EnvLineExec or EnvCallFunc
--
-- 17-05-2023 @author Swarg
local M = {}

local ui = require("env.ui")
local su = require('env.sutil')
local uv = vim.loop

-- session specific local variables stored in module
-- play role RootGC for testing
local list_mem_consumer = {}
local tmp_bufnr = -1

---@param title string
---@param bar table? {string}
---@param foot table? {string}
function M.show_memtest_wnd(title, bar, foot)
  if tmp_bufnr ~= -1 then
    vim.cmd('silent! confirm bd ' .. tmp_bufnr)
  end
  local sf = string.format
  tmp_bufnr = ui.buf_new_named('MEM-TEST')
  ui.buf_append_lines(tmp_bufnr, {
    '-+----------------------------------------------------------+-',
    sf('%s [%d] %s', os.date(), uv.getpid(), title or 'MemTest')
  })
  if type(bar) == "table" then
    ui.buf_append_lines(tmp_bufnr, bar)
  end
  foot = foot or {
    "(Press 'a' - to allocate 1M tables with 16 digits)",
    "(Press 'f' - to free all allocated memory)",
    "(Press 's' - to show current memoty state)",
    "(Press 'g' - to start garbage collection)",
    "(Press 'q' - to close buffer)", -- default bindings for my own 'floating'
    ''
  }
  ui.buf_append_lines(tmp_bufnr, foot)
  local b = tmp_bufnr
  -- bind keylisteners to set of Actions
  local bk = vim.api.nvim_buf_set_keymap
  local o = { silent = true, noremap = true, nowait = true }
  local m = 'require("env.memtest")'
  bk(b, 'n', 'f', sf('<cmd>lua %s.%s()<CR>', m, 'manual_test_free_memory'), o)
  bk(b, 'n', 'a', sf('<cmd>lua %s.%s()<CR>', m, 'allocate_1Mt16d'), o)
  bk(b, 'n', 's', sf('<cmd>lua %s.%s()<CR>', m, 'manual_test_gc_state'), o)
  bk(b, 'n', 'g', sf('<cmd>lua %s.%s()<CR>', m, 'manual_test_call_gc'), o)
end

-- Measure MemUsage before, CallGC, wait1s, Measure MemUsage After GC show Free
-- https://www.lua.org/manual/5.1/manual.html#pdf-collectgarbage
function M.gc_state_and_call()
  local sf = string.format
  local lines = {}
  -- Returns the amount of memory currently used by the program in Kilobytes.
  -- in Lua 5.1 its double number
  local used_before = collectgarbage('count') -- double memory used in Kb
  table.insert(lines, sf('-- BeforeGC: %d Kb', used_before))
  -- Runs one complete cycle of garbage collection. x2
  local t1 = os.clock()
  -- whats meas this ret of collect ?
  local f, s = collectgarbage('collect'), collectgarbage('collect')
  local t2 = os.clock()
  -- give time for GC works -- is are realy nessessery?
  vim.wait(1000)
  table.insert(lines, sf('-- CallGC  : %d %d took: %d sec', f, s, (t2 - t1)))
  local used_after = collectgarbage('count') -- double memory used in Kb
  table.insert(lines, sf('-- AfterGC : %d Kb', used_after))
  local free_kb = used_before - used_after;
  local free_mb = free_kb / 1024
  table.insert(lines, sf('-- Free    : %d Kb (%d Mb)', free_kb, free_mb))
  --k, b = collectgarbage("count") in Lua > 5.1
  --assert(k*1024 == math.floor(k)*1024 + b)
  return lines
end

-- To Check is plugin reloading can triggers mem leak
-- While goes reloading in 100 times See VSZ RSS Memory of nvim process [pid]
function M.malual_test_is_reload_has_mem_leak()
  print(os.date(), '[', uv.getpid(), '] Stress-test Reload Strarting...')
  vim.schedule(function()
    for _ = 1, 100 do
      M.self_reload(nil)
      vim.wait(250)
    end
    print(os.date(), 'Done. ')
  end)
end

function M.gc_info()
  local c = collectgarbage('count')
  print("gc-count:", type(c), " ", vim.inspect(c))
end

function M.manual_test_open_wnd()
  M.show_memtest_wnd('MemTest', {
    '[INFO] Interactive Mode. Use KeyBindings to select actions',
  })
end

function M.allocate_1Mt16d()
  ui.buf_append_lines(tmp_bufnr, {
    'Memory Allocation. Allocate 1M tables with 16 digits (~198Mb)',
    '[WARNING] This will be hang nvim to some time.',
    '[WARNING] Next Step - call freeMem via func manual_test_free_memory.' })
  vim.schedule(function()
    local t0 = os.time()
    su.set_randomseed(t0)
    collectgarbage('collect')
    vim.wait(1000)
    local before = (collectgarbage('count')) -- double in Kb
    -- 1M x tables with 16 numbers consume --
    local list = {}
    for _ = 1, 1000000 do     -- 10.000 x 15 -- allocated 2086Kb
      local tab = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15 }
      table.insert(list, tab) --su.rand_str(1024 * 1024))
    end
    table.insert(list_mem_consumer, list)
    local after = (collectgarbage('count'))
    local allocated = after - before
    local alloc_mb = allocated / 1024

    local sf = string.format
    ui.buf_append_lines(tmp_bufnr, {
      sf('-- Before Alloc: %d Kb', before),
      sf('-- After  Alloc: %d Kb', after),
      sf('-- Allocated   : %d Kb (%d Mb), in %d seconds',
        allocated, alloc_mb, (os.time() - t0)),
      '',
    })
  end)
end

-- Allocate memory
function M.manual_test_mem_big_allocation()
  M.show_memtest_wnd('MemoryAlocation')
  M.allocate_1Mt16d()
end

local function print_b(bufnr, lines)
  if bufnr ~= -1 then
    ui.buf_append_lines(bufnr, lines)
  else
    if type(lines) == "table" then
      for _, line in pairs(lines) do
        print(line)
      end
    else
      print(lines)
    end
  end
end

function M.manual_test_free_memory()
  if not list_mem_consumer or not list_mem_consumer[1] then
    print_b(tmp_bufnr, 'list-mem-consumer empty!')
    return
  end
  local sf = string.format
  print_b(tmp_bufnr, { '', sf("%s [%d] Free:Dispose Stored data...",
    os.date(), uv.getpid()) -- OwnPid
  })
  vim.schedule(function()
    local cnt = 0;
    while #list_mem_consumer > 0 do
      list_mem_consumer[#list_mem_consumer] = nil
      cnt = cnt + 1
    end
    list_mem_consumer = nil -- say GC free it
    print_b(tmp_bufnr, M.gc_state_and_call())

    print_b(tmp_bufnr,
      { '', string.format('Done. Was Released ListItems: %d', cnt),
        '-+----------------------------------------------------------+-' })
    list_mem_consumer = {} -- reinit new obj
  end)
end

function M.manual_test_gc_state()
  local used = collectgarbage('count') -- double memory used in Kb
  local umb = used / 1024
  print_b(tmp_bufnr, string.format('-- Used: %d Kb (%d Mb)', used, umb))
end

function M.manual_test_call_gc()
  print_b(tmp_bufnr, '-- Call GC')
  print_b(tmp_bufnr, M.gc_state_and_call())
  local used = collectgarbage('count')  -- double memory used in Kb
  local umb = used / 1024
  print_b(tmp_bufnr, string.format('-- Used: %d Kb (%d Mb)', used, umb))
end

--
return M

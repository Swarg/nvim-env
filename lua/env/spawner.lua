-- Tools for interaction with external Processes
--
local M = {}

local api = (vim or {}).api
local wrap = (vim or {}).schedule_wrap
local ui = require("env.ui")
local su = require("env.sutil")
---@diagnostic disable-next-line: unused-local
local fs = require("env.files")
local log = require('alogger')

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
-- local uv = (vim or {}).loop
local uv = R.require("luv", vim, "loop")             -- vim.loop

---@diagnostic disable-next-line: unused-local
local live_processes = {} -- TODO
local E = {}

---@return table
function M.new_process_props(bufnr, cmd, args, cwd)
  return {
    bufnr = bufnr, -- buffer for redirect stdout and stderr from process
    cmd = cmd,
    args = args or {},
    cwd = cwd,
    pid = -1,
    -- redirect stdout of process to buffer
    handler_on_read = M.pull_output_to_buf,
    handler_on_close = M.def_on_close, -- callback to close process
    process = nil,                     -- { args, stdio = {stdin, stdout, stderr}, cwd }
    output = '',                       -- stdout of process
    output_off = 1,                    -- already added to buffer from output
    cmdline = nil,                     -- used in run()
  }
end

---@param callback function(output, pp, exit_ok, signal)
function M.run_async(cwd, cmd, args, envs, callback, userdata)
  local pp = M.new_process_props(-1, cmd, args, cwd)
  pp.envs = envs
  pp.userdata = userdata
  pp.hide_start = true
  if callback and type(callback) == 'function' then
    pp.handler_on_close = callback
  end
  M.run(pp)
  return pp
end

-- block mode has issues with nvim gui. the process terminated on press any key
---@private
function M.run_and_wait(cwd, cmd, args, envs)
  local pp = M.new_process_props(-1, cmd, args, cwd)
  pp.envs = envs
  pp.hide_start = true
  M.run(pp)
  uv.run() -- process.join() wait for done
  return pp.output
end

--
---@param pp table
local function is_usebuf(pp)
  return pp and pp.bufnr and pp.bufnr ~= -1
end

--@param pp table
function M.cmd2str(pp)
  local line = ""
  if pp and pp.cmd and type(pp.args) == "table" then
    if pp.envs then
      line = line .. table.concat(pp.envs, " ") .. " "
    end
    line = line .. pp.cmd .. " " .. table.concat(pp.args, " ")
  end
  return line
end

-- function M.add_cmd2buf(pp)
--   local line = M.cmd2str(pp)
--   if line then
--     vim.api.nvim_buf_set_lines(pp.bufnr, 0, 0, true, { line })
--   end
-- end

local close_handle = function(handle)
  if handle and not handle:is_closing() then
    handle:close()
  end
end


-- take new chunk of raw process output and add it to the nvim buffer
-- case: then last line from process output not finished by '\n' - then
-- this line will be output only in next step - after '\n'
-- pp.output_off - marks the position of processed output parts
-- if buffer is opened in window then auto scroll it down
-- [NOTE] nvim_buf_line_count must not be called in a lua loop callback
-- use vim.schedule
---@param pp table
---@return table<string>|nil - lines added to vim buffer
function M.pull_output_to_buf(pp)
  if not is_usebuf(pp) then return end
  local output_len = #pp.output
  local outpos = {} -- to pass the position of the last occurrence of '\n'
  local lines = su.split_range(pp.output, '\n', pp.output_off, output_len, outpos)
  --log.debug_wo('pull: output_len:%s, op_off: %s, lastNL: %s, lines_cnt: %s ',
  -- output_len, pp.output_off, outpos, #lines)
  if not lines then
    return nil
  end
  -- the last line is always either empty or unfinished -> not add it to buffer
  -- in this step. It will be added in the next step after receiving the '\n'
  table.remove(lines, #lines)
  pp.output_off = outpos.pos -- update processed output parts

  local cnt = api.nvim_buf_line_count(pp.bufnr)
  api.nvim_buf_set_lines(pp.bufnr, cnt, cnt, true, lines)

  if api.nvim_win_get_buf(0) == pp.bufnr and api.nvim_win_is_valid(0) then
    vim.cmd [[ normal! G ]] -- jump to the end -- auto scroll down
  end
  return lines
end

-- to fetch lines without "\n" in the end
-- (one-line-ask-message for ask value from input)
function M.get_latest_output(pp)
  -- output_off output_len
  if pp.output and pp.output_off then
    -- local output_len = #pp.output
    return pp.output:sub(pp.output_off)
  end
end

-- Default callback on close Process - if has buffer show pid
---@diagnostic disable-next-line: unused-local
function M.def_on_close(output, pp, exit_ok, signal)
  if is_usebuf(pp) then
    local cnt = api.nvim_buf_line_count(pp.bufnr)
    local lines = { '', string.format('[%s] Process Closed ', (pp.pid or 0)) }
    api.nvim_buf_set_lines(pp.bufnr, cnt, cnt, true, lines)
  end
  if pp then
    pp.state = 'closed'
  end
end

-- find full path to the given command
---@return string
---@param cmd string
function M.get_exepath(cmd)
  if vim and vim.fn and type(vim.fn.exepath) == 'function' then
    return vim.fn.exepath(cmd)
  else
    return cmd
  end
end

---@private
---@return boolean
function M._validate_run(pp)
  if pp == nil or pp.cmd == nil or pp.cmd == "" then
    error(log.error("No process_properties"))
    return false -- error
  end

  pp.cmdline = M.cmd2str(pp)

  if pp.hide_start then
    log.debug(pp.cmdline)
  else
    log.info(pp.cmdline)
  end

  pp.bufnr = pp.bufnr or -1 -- -1 -- works in background, no output to buf
  if type(pp.bufnr) ~= "number" then
    error(log.error("Illegal buffer number" .. vim.inspect(pp.bufnr)))
    return false
  end

  return true
end

-- print message(lines) to the nvim buffer
---@param pp table
---@param lines table
---@param ls number?
---@param le number?
function M.notify(pp, lines, ls, le)
  if is_usebuf(pp) then
    ls = ls or 0
    le = le or 0
    api.nvim_buf_set_lines(pp.bufnr, ls, le, true, lines)
  end
end

function M._on_done(pp, exit_ok, signal)
  if pp and pp.handler_on_close ~= nil then
    pp.handler_on_close(pp.output, pp, exit_ok, signal)
  end
  if pp and is_usebuf(pp) and not pp.editable then
    api.nvim_buf_set_option(pp.bufnr, 'modifiable', false)
  end
end

-- ref https://neovim.io/doc/user/luvref.html   uv.spawn()
-- Run external cmd with args and redirect stdout & stderr to buffer(pp.output)
--
---@param pp table  See: new_process_props(ctx)
function M.run(pp)
  M._validate_run(pp)

  M.notify(pp, { pp.cmdline }, 0, 0) -- add cmd to buffer

  local handle_stdout = function(err, chunk)
    --assert(not err, err)
    if not err and chunk then
      pp.output = pp.output .. chunk -- collect process output
      if pp and pp.handler_on_read then
        if vim and vim.schedule then
          vim.schedule(function()
            pp.handler_on_read(pp) -- todo remove chunk
          end)
        else
          pp.handler_on_read(pp)
        end
      end
    end
  end

  local done = wrap and wrap(M._on_done) or M._on_done

  local stdin = uv.new_pipe(false)
  local stdout = uv.new_pipe(false)
  local stderr = uv.new_pipe(false)

  local handle
  local on_close = function(code, signal)
    if handle then
      stdout:read_stop()
      stderr:read_stop()
      close_handle(stdin)
      close_handle(stdout)
      close_handle(stderr)
      close_handle(handle)

      pp.code = code
      local exit_ok = code == 0
      done(pp, exit_ok, signal)
      handle = nil
    end
  end

  local exepath = M.get_exepath(pp.cmd)
  local spawn_params = {
    args = pp.args,
    env = pp.envs, -- {}, vim.loop.env
    stdio = { stdin, stdout, stderr },
    cwd = pp.cwd or uv.cwd(),
  }
  handle, pp.pid = uv.spawn(exepath, spawn_params, on_close)
  if not handle then
    local message = pp.pid:match("ENOENT")
        and string.format("Command '%s' is not executable "
          .. "(check your $PATH)", pp.cmd)
        or string.format("Failed to run '%s': %s", pp.cmd, pp.pid)
    error(message)
  end
  pp.process = spawn_params
  log.debug("New Process %s", pp)
  -- add Pid of launched process to first line
  M.notify(pp, { '[' .. pp.pid .. '] ' .. pp.cmdline }, 0, 1)

  uv.read_start(stdout, handle_stdout)
  uv.read_start(stderr, handle_stdout) -- one handler for both
end

-- Force kill process by pp.pid with output ui.message
---@param pid number
function M.kill_process(pid)
  if pid and type(pid) == "number" then
    uv.kill(pid, uv.constants.SIGKILL) -- 9 for linux
    local msg = " Process " .. vim.inspect(pid) .. " Stopped. "
    ui.echohl(msg)
    log.debug(msg)
  end
end

-- return callback for stop the process by Ctrl-C(SIGINT)
-- Used:
--  for stop gradle with '--continuous' flag then build fails,
--  to detach from debugging jvm launched by gradle with --countinuous flag
---@param msg string|nil message to UI on Stop Process
---@return function (pp table)
function M.mk_cb_send_sigint(msg)
  return function(pp)
    log.debug("StopProcess pp:%s", pp)
    if pp and pp.pid then
      log.info('[%s] %s', pp.pid, (msg or "Stop ")) -- ui.echohl(
      -- Ctrl-D trigger eof event for soft stop
      -- close_handle(pp.process.stdio.stdin) -- force to stop process
      if type(pp.pid) == "number" then
        log.debug("Send SIGINT to %s", pp.pid)
        uv.kill(pp.pid, uv.constants.SIGINT) -- soft, force: SIGKILL
      else
        log.debug("NO SIGINT! pid:%s", pp.pid)
      end
    else
      ui.echohl("Illegal ProcessProperties " .. vim.inspect(pp))
    end
  end
end

-- write to stdin of the opened process
-- if input is '' - do nothing
--
---@param pp table
---@param input string
---@param close boolean?
function M.write_to_stdin(pp, input, close)
  assert(type(pp) == 'table', 'pp - process props')
  assert(type(input) == 'string', 'input should be stirng')

  -- assert(type(pp.process) == 'table', 'pp.process')
  -- assert(type(pp.process.stdio) == 'table', 'pp.process.stdio')
  -- assert(pp.process.stdio[1] ~= nil, 'pp.process.stdio[1] -- stdin')

  local stdin = ((pp.process or E).stdio or E)[1] -- stdin or write_pipe

  if stdin and input ~= '' then
    stdin:write(input) -- return uv_write_t userdata
    -- stdin:flush() -- how?
    if close then
      stdin:close()
    end
    return true
  end

  return false
end

return M

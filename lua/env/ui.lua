local M = {}
local su = require('env.sutil')
local fs = require("env.files")
---@diagnostic disable-next-line: unused-local
local log = require('alogger')

local E, v2s, fmt = {}, tostring, string.format

-- find by buffname in all buffers
-- [Note] Each Buffer have unique name. You cannot create new one with
-- already existed buffer name
---@param bname string
---@return number
function M.get_buf_with_name(bname)
  if bname and bname ~= "" then
    local list = vim.api.nvim_list_bufs()
    for _, bn in ipairs(list) do
      local loaded = vim.api.nvim_buf_is_loaded(bn)
      if loaded then
        local ok, name = pcall(vim.api.nvim_buf_get_name, bn)
        if ok and name and name ~= '' and su.ends_with(name, bname) then
          return bn
        end
      end
    end
  end
  return -1
end

-- Create new or clear created Buffer And prepare to output stdout
---@param ctx table{bufnr, out_bufnr, prev_bufnr}
---@param filetype string|nil
function M.create_buf_nofile(ctx, bname, filetype)
  local api = vim.api or {}
  local win = api.nvim_get_current_win()
  -- reuse already exists buf
  if bname then
    local oldbufnr = M.get_buf_with_name(bname)
    if oldbufnr and oldbufnr ~= -1 then
      ctx.out_bufnr = oldbufnr -- reuse
    end
  end

  if not ctx.bufnr or ctx.bufnr == 0 then
    ctx.bufnr = vim.api.nvim_get_current_buf()
  end
  -- if oldbufnr ~= -1 then
  --   vim.cmd(string.format('silent! confirm bd %d', oldbufnr))
  -- end
  if not ctx.out_bufnr or ctx.out_bufnr == -1 then
    ctx.out_bufnr = vim.api.nvim_create_buf(true, true)
  end
  api.nvim_win_set_buf(win, ctx.out_bufnr) -- mk foreground
  pcall(api.nvim_buf_set_name, ctx.out_bufnr, bname)

  api.nvim_buf_set_option(ctx.out_bufnr, 'modifiable', true) -- clear old
  api.nvim_buf_set_lines(ctx.out_bufnr, 0, -1, true, {})
  api.nvim_buf_set_option(ctx.out_bufnr, 'buftype', 'nofile')
  api.nvim_buf_set_option(ctx.out_bufnr, 'swapfile', false)
  api.nvim_buf_set_option(ctx.out_bufnr, 'filetype', 'shell')
  --api.nvim_win_set_option(win, 'wrap', true) -- FIXME passed to other windows
  api.nvim_win_set_option(win, 'cursorline', true)

  api.nvim_buf_set_var(ctx.out_bufnr, 'prev_buf', ctx.prev_bufnr or ctx.bufrn)

  api.nvim_buf_set_keymap(
  -- standart cmd bdelete - can close nvim
  -- Bdelete cmd from moll/vim-bbye
    ctx.out_bufnr, 'n', 'q', '<cmd>EnvBufClose<cr>', -- '<cmd>Bdelete<cr>',
    { silent = true, noremap = true, nowait = true }
  )
  if filetype and filetype ~= '' then
    api.nvim_buf_set_var(ctx.out_bufnr, 'custom_filetype', filetype)
  end
end

---@param bufnr number -- the source of new buffer (like test-file)
---@param out_bufnr number -- the new dyn created buffer (like runned test report)
---@param path string|nil  -- bufname of bufnr
function M.bind_bufs(bufnr, out_bufnr, path)
  assert(type(bufnr) == 'number')     -- i.e. the test-file
  assert(type(out_bufnr) == 'number') -- i.e. report for the test-file

  if bufnr ~= out_bufnr then
    path = path or vim.api.nvim_buf_get_name(bufnr)

    M.buf_set_back_src(out_bufnr, path)  -- for back jumps from repo->test
    M.set_attached_buf(bufnr, out_bufnr) -- for jumps from test->repo
  end
end

---@param bufnr number
---@param path string
function M.buf_set_back_src(bufnr, path)
  if bufnr and vim.api and bufnr then
    vim.api.nvim_buf_set_var(bufnr, 'back_src_path', path)
  end
end

---@param bufnr number
---@param varname string
function M.buf_get_valueof(bufnr, varname)
  if type(bufnr) == 'number' and varname then
    if not bufnr or bufnr == 0 then
      bufnr = vim.api.nvim_get_current_buf()
    end
    local ok, value = pcall(vim.api.nvim_buf_get_var, bufnr, varname)
    if ok then
      return value
    end
  end
  return nil
end

-- for quick jumps back from the report page of running tests
---@param ctx table  -- add back_src_path if found in curr buf
function M.has_back_src(ctx)
  if ctx and ctx.bufnr then
    local back = M.buf_get_valueof(ctx.bufnr, 'back_src_path')
    if back and fs.file_exists(back) then
      ctx.back_src_path = back
      return true
    end
  end
end

---@param ctx table
function M.open_back_src(ctx)
  if ctx and ctx.back_src_path then
    vim.api.nvim_exec(":e " .. ctx.back_src_path, true)
  end
end

--- set attached_bufnr into bufnr
---@param bufnr number - to which we attach another buf with attached_bufnr
---@param attached_bufnr number|nil
function M.set_attached_buf(bufnr, attached_bufnr)
  if bufnr ~= attached_bufnr then
    vim.api.nvim_buf_set_var(bufnr, 'attached_bufnr', attached_bufnr)
  end
end

function M.open_attached_buf(ctx)
  if ctx and ctx.attached_buf then
    if vim.api.nvim_buf_is_loaded(ctx.attached_buf) then
      vim.api.nvim_exec(":b " .. ctx.attached_buf, true)
    else
      print('The Buffer Not Loaded')
      vim.api.nvim_buf_set_var(ctx.bufnr, 'attached_bufnr', nil) --?
    end
  end
end

function M.has_attached_buf(ctx)
  local bn = M.buf_get_valueof((ctx or {}).bufnr, 'attached_bufnr')
  if bn then
    ctx.attached_buf = bn
    return true
  end
end

-- Create new buffer for tmp output with keybindings to quick close via q
---@param bufname string Name for new buffer
-- [WARN] delete already existed old buffer with this new bufname
---@return number buffer_nr
function M.buf_new_named(bufname)
  local api = vim.api
  ---@cast api table
  local win = vim.api.nvim_get_current_win()
  -- reuse already exists buf
  local oldbufnr = M.get_buf_with_name(bufname)
  if oldbufnr ~= -1 then
    vim.cmd(string.format('silent! confirm bd %d', oldbufnr))
  end
  local bufnr = vim.api.nvim_create_buf(true, true)
  api.nvim_win_set_buf(win, bufnr)

  api.nvim_buf_set_option(bufnr, 'modifiable', true)
  api.nvim_buf_set_lines(bufnr, 0, -1, true, {})
  api.nvim_buf_set_option(bufnr, 'buftype', 'nofile')
  api.nvim_buf_set_option(bufnr, 'swapfile', false)
  api.nvim_buf_set_option(bufnr, 'filetype', 'temp')
  api.nvim_win_set_option(win, 'cursorline', true)

  -- Standart cmd for close buffer is 'bdelete' - it can close nvim!
  -- Bdelete cmd from moll/vim-bbye stay vim alive then last buffer closed
  api.nvim_buf_set_keymap(bufnr, 'n', 'q', '<cmd>Bdelete<cr>',
    { silent = true, noremap = true, nowait = true })
  if bufname then pcall(api.nvim_buf_set_name, bufnr, bufname) end
  return bufnr
end

---@param bufnr number
---@param lines table|string
function M.buf_append_lines(bufnr, lines)
  if bufnr or lines then
    local cnt = vim.api.nvim_buf_line_count(bufnr)
    -- fs.str2file('/tmp/last', os.date() .. ' ' .. type(lines) .. '\n')
    if type(lines) == "string" then
      if lines:find('\n', 1, true) then
        lines = vim.split(lines, "\n\r?", { trimempty = false })
      else
        lines = { lines }
      end
    elseif type(lines) == "table" then
      -- TODO Case: All items in the replacement ar ray must be strings
    else
      lines = { tostring(lines) }
    end
    local nvim_buf_set_lines = vim.api.nvim_buf_set_lines
    local ok, err = pcall(nvim_buf_set_lines,
      bufnr, cnt, cnt, true, lines)
    if not ok then
      local errors
      if type(err) == 'string' then
        errors = vim.split(err, '\n\r?', { trimempty = false })
      else
        errors = { 'ERROR' }
      end
      nvim_buf_set_lines(bufnr, cnt, cnt, true, errors)
      nvim_buf_set_lines(bufnr, cnt, cnt, true, { vim.inspect(lines) })
    end
    vim.cmd [[ normal! G ]]
  end
end

-- Show message in "status" line
---@param msg string
---@param hlgroup string|nil
---@param keep_in_messages boolean? (:messages
function M.echohl(msg, hlgroup, keep_in_messages)
  -- log.debug("echohl", msg, hlgroup, async)
  hlgroup = hlgroup or "Function"
  if not msg then
    return
  end
  -- cook msg and highlight group
  local func_echohl = function(msg0)
    local suffix = keep_in_messages == true and "m" or "" -- echom
    hlgroup = hlgroup or "None"
    -- this way do not store messages in status line history
    local cmd = fmt(':redraw | :echohl %s | echo%s "%s" | echohl None',
      hlgroup, suffix, string.sub(v2s(msg0), 1, (vim.v or E).echospace or 120))

    local ok, err = pcall(vim.api.nvim_command, cmd)
    if not ok then
      log.debug('## Error on echo msg:%s error:%s ', msg, err)
      -- just as is to avoid errors in trying to interpret as variable
      if (vim.api.nvim_out_write) then
        -- to show given message in status and put to history (w/o hl-color)
        vim.api.nvim_out_write(v2s(msg0) .. '\n')
      end
    end
  end

  if not vim then -- testing without vim
    print(msg)
  elseif vim.in_fast_event() then
    -- solve err:  must not be called in a lua loop callback
    vim.schedule_wrap(function()
      func_echohl(msg) -- schedule_wrap fix E5560: in vim.loop
    end)
  else
    func_echohl(msg)
  end
end

function M.print(msg)
  if msg and msg ~= '' then
    if vim.in_fast_event() then
      -- solve err:  must not be called in a lua loop callback
      vim.schedule(function() print(msg) end)
    else
      print(msg)
    end
  end
end

function M.echo_err(msg)
  M.echohl(msg, "ErrorMsg")
end

--
-- Replacing of vim.fn.input to allow return index out of range (0)
-- to fix LineInsert with 0 index(named|shared items)
---@param variants table:string
---@return number|string
local function inputlist_ex(variants)
  local choice
  local prompt = table.concat(variants, "\n") ..
      "\nPick one item[1-" .. #(variants or E) .. "]:"
  vim.ui.input({ prompt = prompt }, function(input)
    choice = input
  end)
  if choice == 'q' or choice == '' or choice == nil then
    choice = -1
  end
  local n = tonumber(choice)
  return n ~= nil and n or choice
end

--
-- pick one item from list in interactive mode
-- if no space to show all items via vim.fn.inputlist ask by index only
-- with ability to show all list via type "ls"
--
---@param items table
---@param prompt string
---@param named table? extra mapping key-value
---@return string?
---@return number
---@return string|number? choice
function M.pick_one(items, prompt, label_fn, short, named)
  short = short == nil and false or short -- default is false
  local height = vim.api.nvim_win_get_height(0) or 16
  local choice = nil

  if #items < height or not short then
    local choices = { prompt }
    for i, item in ipairs(items) do
      table.insert(choices, string.format('%2d: %s', i, label_fn(item)))
    end
    -- choice = vim.fn.inputlist(choices) -- returns number only
    choice = inputlist_ex(choices) -- string|number
  else
    vim.ui.input({ prompt = 'index[1-' .. tostring(#items) .. '|list]: ' },
      function(input)
        choice = input
      end)
    if choice == 'l' or choice == 'ls' or choice == 'list' then
      return M.pick_one(items, prompt, label_fn, false)
    end
    if tonumber(choice) ~= nil then choice = tonumber(choice) end
  end
  -- extra for non number, string values "0" and so one
  local item = (named or E)[choice or false]
  if log.is_debug() then
    log.debug('for choice:(%s)"%s": named-item:"%s" named:%s',
      type(choice), choice, item, named)
  end
  if item and choice then
    return item, -1, choice -- named from shared
  end

  local nchoice = tonumber(choice) or -1
  return items[choice], nchoice, choice
end

M.openFileCallback = function(file)
  if file then
    vim.api.nvim_exec(":e " .. file, true)
  end
end

-- Return batch of lines from given line_number(n) from current nvim buffer
---@param n number line_number
---@param count string  lines count from line_nr
---@return  table<string>?
M.fetch_lines = function(n, count)
  if n and count then
    return vim.api.nvim_buf_get_lines(0, n, n + count, false)
  end
  return nil
end

-- Ask the user for confirmation before performing a given action
-- Ask comfirmation
---@param message string
---@param callback function? - callback with action
---@param userdata any      - data for callback
---@return any -- output from callback
function M.ask_confirm_and_do(message, callback, userdata)
  local output = nil
  message = message or "Do you agree"
  vim.ui.input({ prompt = message .. ' ? (y/N) ' },
    function(input)
      if su.is_yes(input) then
        output = true
        if type(callback) == 'function' then
          output = callback(userdata) or true
        end
      end
    end)
  return output
end

-- ask value and return input of default
---@param message string
---@param default string|nil
---@param completion string|nil
function M.ask_value(message, default, completion)
  local output = default
  message = message or "Value"
  vim.ui.input({ prompt = message, default = default, completion = completion },
    function(input)
      output = input
    end)
  return output
end

-- Check whether the focus is currently in the floating window and,
-- if so, close this window by going to the main editor window.
-- Usefull then you needs to jump into a some file from float(diagnostic-like)
-- window.
-- If not closed floating window jump not occurs - the file will not be opened.
function M.close_float_win_if_need()
  local wc = vim.api.nvim_win_get_config(0)
  -- is float
  if wc and wc.relative == "win" and wc.win then
    vim.api.nvim_exec(":hide ", true) -- close current(float) window
  end
  -- nvim_win_get_buf({window}) Gets the current buffer in a window
end

--
-- get interpreter from the first line of the current opened file(bufnr)
-- if interpreter will be not found return empty string
--
-- #!/bin/bash              -> bash
-- #!/usr/bin/bash          -> bash
-- #!/usr/bin/env bash      -> bash
-- #!/usr/bin/env /bin/bash -> bash
function M.get_interpreter(bufnr)
  bufnr = bufnr or 0
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, 1, false)
  if lines and lines[1] then
    if lines[1]:sub(1, 3) == "#!/" then
      local s = fs.extract_filename(lines[1]) or ''
      if s:sub(1, 4) == "env " then             -- #!/usr/bin/env bash  -> bash
        s = fs.extract_filename(s:sub(5)) or '' -- #!/usr/bin/env /bin/bash
      end
      s = s:gsub("^%s*(.-)%s*$", "%1")          -- trim
      return s
    end
  end
  return ''
end

return M

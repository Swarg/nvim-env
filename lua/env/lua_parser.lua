-- 01-10-2023 @author Swarg
local M = {}

-- Goal: GotoDefinition (EnvGotoSource) from inside a buf with lua code from:
--  - var definition of path to file
--    (resolve full path to file defined in luacode)
--
--  - from dynamic|static method of Class to Module with ClassFile
--    (from instance:method to method source code)

--

local fs = require('env.files')
local su = require('env.sutil')
local log = require('alogger')
local tu = require("env.util.tables")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local m_tokenizer = require("env.util.tokenizer.parser")
local m_token_tree = require("env.util.tokenizer.tree")

local D = require("dprint")
local dprint = D.mk_dprint_for(M)

local E = {}

--- check is given name starts with UpperCase letter and can be a ClassName
---@param varname string
---@return boolean
function M.is_class_name(varname)
  return varname and varname:match('^%u[%w_]+$') ~= nil
end

--
-- correct comment position in the line with skipping comments in quotes:
--  '--', --
--        ^use
---@param line string
---@param comment string? -- // /* #  two chars max
function M.find_comment_pos(line, comment)
  comment = comment or '--' -- lua comment
  if line then
    local prev, quote, sblock = nil, nil, nil
    local f = #comment == 2
    local wc1 = comment:sub(1, 1)
    local wc2 = f and comment:sub(2, 2) or nil

    for i = 1, #line do
      local c = line:sub(i, i)
      if prev ~= '\\' then
        if c == '"' or c == "'" then
          if not quote then
            quote = c
          elseif quote == c then
            quote = nil
          end
        elseif not quote and c == '[' and prev == '[' then
          sblock = true
        elseif not quote and c == ']' and prev == ']' then
          sblock = false
          -- elseif not quote and c == '-' and prev == '-' then
        elseif not quote and not sblock and (
              (f and c == wc2 and prev == wc1) or -- two chars comment
              (not f and c == wc1)                -- one char comment like #
            )
        then
          return i - 1
        end
      end
      prev = c
    end
  end
  return nil
end

--
---@param lines table
function M.format_comments(lines)
  assert(type(lines) == 'table', 'lines')

  local max, t = 0, {}
  for _, line in ipairs(lines) do
    local i = M.find_comment_pos(line)
    local entry = {}
    if i then
      if i > max then max = i - 1 end
      local code, comment = line:sub(1, i - 1), line:sub(i, #line)
      entry = { code, comment }
    else -- only code without comments
      if #line > max then max = #line end
      entry = { line }
    end
    t[#t + 1] = entry
  end

  local res = {}
  for _, v in ipairs(t) do
    local code, comment = v[1], v[2]
    if not comment then
      res[#res + 1] = code
    else
      res[#res + 1] = string.format("%-" .. max .. "s%s", code, comment)
    end
  end
  return res
end

---@param ctx table
---@param line string
---@param callback function
---@return boolean
function M.find_path_to_source_for(ctx, line, callback)
  -- ./lua/env/lang/oop/ClassGen.lua:106: attempt to concatenate a nil value
  if line:match("^[%s]*%.?/[^%s]+%:[%d]+%: ") then
    local path, line_nr = line:match("^[%s]*(%.?/[^%s]+)%:([%d]+)%: ")
    line_nr = tonumber(line_nr) or 1
    path = fs.build_path(vim.loop.cwd(), path)
    assert(type(callback) == 'function')
    return callback(path, line_nr)
  elseif M.find_path_from_code(ctx, line, callback) then
    -- elseif line:match("^local [%w]* = '?\"?[%w/]+'?\"?") then
  end
  return false
end

-- ---@return string?,string? varmodule module_namespace
-- function M.parse_line_require(line)
--   -- local M = require('ns')
--   local p = '^[%s]*local[%s]+([^%s]+)[%s]*=[%s]*require%s?%(?[\'"]([%w_%.]+)[\'"]%)?'
--   local vn, ns = line:match(p)
--   if not vn then
--     -- global
--     vn, ns = line:match('^%s*([^%s]+)[%s]*=[%s]*require%s?%(?[\'"]([%w_%.]+)[\'"]%)?')
--   end
--   return vn, ns
-- end

--
-- local mod = require("ns.module") -- comment -- > "mod" "ns.module" "-- comment"
--
-- local ok, mod = pcall(require, 'ns.module') > "mod" "ns.module"
--
-- 'module' "module" ('module') or ("module") --> module
---@param s string
function M.parse_line_require(s)
  local vn, mod, tail = nil, nil, nil
  if type(s) ~= 'string' or s == '' then
    return vn, mod, tail
  end
  local rest
  vn, rest = string.match(s, "^%s*local%s+([^%s]+)%s*=%s*require%s*(.*)%s*$")
  if vn and rest and rest ~= '' then
    mod, tail = string.match(rest, "([^'\"%(%);]+)(.*)")
    if tail and tail ~= '' then
      tail = string.match(tail, "^['\"]?%)?(.-)%s*$")
      if su.trim(tail) == ';' then tail = '' end
    end

    -- return vn, mod or '', tail or ''
  else
    -- M = require('ns.module')   --- without local
    local p = '^%s*([^%s]+)%s*=%s*require%s*%(?[\'"]([%w_%.]+)[\'"]%)?(.*)$'
    vn, mod, tail = s:match(p)

    -- local ok, mod = pcall(require, 'ns.mod') --x --> "mod" "ns.mod" "--x"
    if not vn then
      p = [[^%s*local%s+[%w_]+,%s*([^%s]+)%s*=%s*pcall%(require,%s*['"]([^%s]+)['"]%)%s*(.-)%s*$]]
      vn, mod, tail = s:match(p)
    end
  end

  return vn, mod or '', tail or ''
end

-- [[ClassGen:new({..]]  ---> 'ClassGen',  'new'
-- first - Word Starts with upper case (Class or Module name)
-- second - function of the module or method of class
---@return string?,string? -- class, method
function M.parse_line_expr_class_method(line)
  local pattern = "^%s*(%u[^:%s'\"%-]*)[%.:]([^:%s'\"%-%(%)]+)"
  --
  return line:match(pattern)
end

--
-- [pl:getLangGen()]  -->  pl, LangGen
--  ^       ^method
--  instance
--
-- variable name of instance and dynamic method (:)
-- TODO check chain call like: instance:call1():call2():call3()
--
---@return string?,string? -- instance(varname), (first!)method
function M.parse_line_call_dyn_method(line)
  local pattern = "^%s*(%l[^:%s'\"%-]*):([^:%s'\"%-%(%)]+)%("
  --
  return line:match(pattern)
end

--
-- [[local p = sub_dir .. 'rpath/to/file']]  -> p, 'subdir .. ', 'rpath/to/file'
-- [[local str2 = '/relative/path/to/file']] -> str2, '', 'relativepath/to/file'
-- not: local M = require('org.pkg.module')  -- not pass by ()
--
---@param line string
---@return string, string, string -- varname, expression, value
function M.parse_line_assign_string(line)
  -- local pattern = "local ([%w_%.]+) = ([^'\"%(%)]*)['\"]([%w%._/]+)['\"]"
  local pattern = "local ([%w_%.]+) = ([^'\"]*)['\"]([%w%._/%-]+)['\"]"
  -- return varname, expression, value
  local vn, exp, value = string.match(line, pattern)
  -- try global
  if not vn then
    local ptrn_g = "(M%.[%w_%.]+) = ([^'\"]*)['\"]([%w%._/%-]+)['\"]"
    vn, exp, value = string.match(line, ptrn_g)
  end

  return vn, exp, value
end

--
-- sub_dir .. 'rpath/to/file' -> 'sub_dir ..',  'rpath/to/file'
-- 'relative/path/to/file' -> '', relative/path/to/file
--
---@param line string
---@return string, string, string -- varname, expression, value
function M.parse_line_expr_append_string(line)
  local pattern = "([^'\"]*)['\"]([%w%._/]+)['\"]"
  -- return expression, value
  return string.match(line, pattern)
end

function M.is_commented(line)
  return line and line:match('^[%s]*%-%-') ~= nil
end

-- to jump into file from lua code definition
-- with resolving the string vars values (work only inside one lua-file)
--
-- local path = root .. 'relative/path/to/file.lua'
-- local path = uv.cwd() .. '/relative/path/to/file.lua'
function M.find_path_from_code(ctx, line, callback)
  local vn_path, expr, path = M.parse_line_assign_string(line)
  log.debug("find_path_from_code vn:%s expr:%s path:%s", vn_path, expr, path)
  if vn_path and path then
    local bufnr = ctx.bufnr or 0
    -- relative path -- find full path
    if expr and expr ~= '' then                     --not su.starts_with(path, files.path_sep) then
      -- local var = vn_ref .. 'value'
      local vn_ref = expr:match('^[%s]*([%w_%.]+)') -- get first word
      log.debug('vn-ref(first-word)', vn_ref)
      if not vn_ref or vn_ref == 'require' then
        return false
      end
      if vn_ref == 'uv.cwd' and expr:match("uv%.cwd%(%) %.%.") then
        path = fs.join_path(uv.cwd(), path)
      else
        local startln, curline, lines
        startln, curline = 0, (vim.api.nvim_win_get_cursor(0) or {})[1]
        lines = vim.api.nvim_buf_get_lines(bufnr, startln, curline - 1, false)
        path = M.resolve_strvar_value(lines, vn_ref, 0) .. path
        -- notify that path resolved but not existed in filesystem
        if path and path ~= '' and path:find(fs.path_sep) then
          if not fs.file_exists(path) then
            print('path resolved but not existed: ' .. tostring(path))
            return true -- to stop the subsequent searching using other templs
          end
        end
      end
    end
    log.debug('found path: "%s"', path)
    return callback(path, 1)
  else
    -- log.debug('Not Found var and path for', line)
  end
  return false
end

-- Resolve value of given varname in lua-source code
-- traverse from offset index from the end of lines up to first line in lines
--
---@param offset number|nil -- start find from this line number
function M.resolve_strvar_value(lines, varname, offset)
  offset = offset or 0
  local pref = varname:find('%.') ~= nil and '' or 'local '

  local p_find_value_of_var = "^[%s]*" .. pref .. varname .. ' = ([^=]+)$'
  -- dprint('find var ptrn:', p_find_value_of_var)

  local value = ''

  for i = #lines - offset, 1, -1 do
    local line = lines[i]
    if not M.is_commented(line) then -- skip comments
      -- check is searched varname
      local expr = string.match(line, p_find_value_of_var)
      -- dprint('expr:', expr, i, line)
      if expr then
        local val
        expr, val = M.parse_line_expr_append_string(expr)
        -- dprint('expr parsed:', expr, val)
        if expr and val then
          value = val .. value
          varname = expr:match('^[%s]*([%w_%.]+)') -- get first word
          -- dprint('varname:', varname)
          if varname then
            pref = varname:find('%.') ~= nil and '' or 'local '

            p_find_value_of_var = "^[%s]*" .. pref .. varname .. ' = ([^=]+)$'
            -- dprint('new varname ptrn:', p_find_value_of_var)
          else
            break -- cannot resolve the value
          end
        end
        if expr:match("uv%.cwd%(%) %.%.") then
          return uv.cwd() .. value -- consider it resolved
        end
      end
    end
  end
  return value
end

--
-- Find definition of the given local varname(vn)
--
-- The search occurs from bottom to top from line number "ln_e"
-- to line number ln_s.
--
-- First line with 'end)' considered an exit to another scope(stop searching)
-- TODO improve the scrope detection by count of spaces
--
-- WARN: Not supported negative offsets for ln_s, ln_e, use only actual indexes
--
-- Example for varname = 'mg':   -->  expr = "MethodGen:new({})"
--
--     local mg = MethodGen:new({})                                      <--_
--     mg:generate()                    <<  cursor on method `generate`      |
--                                          find and return 'MethodGen:new({})'
-- search in reverse order from the ln_e to ln_s
--
-- no_local - no local prefix in the closest matched assignmention
--
---@param vn string  varname to find
---@param bufnr number
---@param ln_s number - upper bound - the line number from zero
---@param ln_e number - lower bound - the point from which the search begins
---@param full_rvalue boolean|nil
---@return string?, number?, number?, boolean? -- expr, lnum, lnum_end, no_local
function M.find_var_definition(vn, bufnr, ln_s, ln_e, full_rvalue)
  assert(type(vn) == 'string', 'vn')
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(ln_s) == 'number', 'ln_s')
  assert(type(ln_e) == 'number', 'ln_e')

  if not vn or vn == '' then
    return
  end

  if ln_s < 0 then ln_s = 0 end
  if ln_e < 0 then ln_e = 0 end

  local count = vim.api.nvim_buf_line_count(bufnr)
  if ln_e > count then ln_e = count end

  -- local p_find_value_of_var = "^[%s]*local[%s]+" .. vn .. '[%s]*=[%s]*(.+)$'
  -- checks all variables - for looks to a multilines assigns along the way [[
  local pattern_local_assign = "^[%s]*local[%s]+([%w_%.]+)[%s]*=[%s]*(.+)$"
  local pattern_assign = "^[%s]*([%w_%.]+)[%s]*=[%s]*(.+)$" -- exp = ...
  local prev_multiline_close, lnum_end = nil, nil

  for i = ln_e, ln_s, -1 do
    local line = (vim.api.nvim_buf_get_lines(bufnr, i, i + 1, false) or E)[1]

    if not line then
      -- skip nil
    elseif not prev_multiline_close and line:match("^[%s]*end%).*") then
      -- out of scope
      -- if M.debug then print("[DEBUG] out of scope") end
      return nil, nil, nil, nil
      --
    elseif line == ']]' then -- multiline definition detected
      prev_multiline_close = i
      --
    elseif not prev_multiline_close and line:match("^[%s]*}.*") then
      prev_multiline_close = i
      --
    elseif not M.is_commented(line) then -- skip comments
      local no_local = nil
      -- check is "local varname = expression"
      local vn0, expr = string.match(line, pattern_local_assign)
      -- check is "varname = expression"
      if not vn0 then
        vn0, expr = string.match(line, pattern_assign)
        no_local = true
      end
      -- if M.debug then print("[DEBUG] find vn:", inspect(vn), 'current vn0:', inspect(vn0)) end
      if vn0 and expr then
        -- its searched var
        if vn0 == vn then
          -- resolve all expresstion (multilines definition)
          if expr == '[[' or expr == '{' then
            lnum_end = prev_multiline_close
          end
          -- -- build full value from multiple lines if needed
          if full_rvalue and lnum_end then
            local lines = vim.api.nvim_buf_get_lines(bufnr, i + 1, lnum_end + 1, true)
            expr = expr .. '\n' .. table.concat(lines or {}, '\n')
          end
          -- if M.debug then print("[DEBUG] FOUND", expr, i, lnum_end, "\n\n") end
          return expr, i, (lnum_end or i), no_local

          -- vn0 ~= vn
          -- its another var close multiline block, continue searching
        elseif expr == '[[' or line == '[[' then
          prev_multiline_close = nil
        end
      end
    end
  end

  -- if M.debug then print("[DEBUG] NOT_FOUND\n", vn) end
  return nil, nil, nil
end

-- find full namespace for luavariable modulename
-- local M = require('full.namespace.my_module')
-- find M -> res: full.namespace.my_module
---@param modulename string
---@param ls number  lnum start index from 0
---@param le number  lnum end ( if less 0 then take from end of lines)
function M.find_module_namespace(modulename, bn, ls, le)
  assert(type(modulename) == 'string', 'classname')
  assert(type(bn) == 'number', 'bn - bufnr has:' .. tostring(bn))
  assert(type(ls) == 'number', 'ls - lnum ')
  assert(type(le) == 'number', 'le - lnum_end')

  local pattern = '^[%s]*local[%s]+' .. modulename ..
      '[%s]*=[%s]*require%s?%(?[\'"]([%w_%.]+)[\'"]%)?'

  if le < 0 then
    local cnt = vim.api.nvim_buf_line_count(bn)
    le = cnt + le + 1
  end
  for i = ls, le, 1 do
    local line = (vim.api.nvim_buf_get_lines(bn, i, i + 1, true) or E)[1]
    if line and not M.is_commented(line) then -- skip comments
      local ns = string.match(line, pattern)
      if ns then
        return ns, i
      end
    end
  end
  return nil, nil
end

-- function M.func(..)
-- function MyClass:method(..)
---@return string?, string?  -- container|Class, method
function M.parse_function_definition(line)
  if line then
    local pattern = '^[%s]*function[%s]+([^:%.%s]+)[%.:]([^:%.%s]+)%('
    return string.match(line, pattern)
  end
end

--
-- function MyClass:methodDoSomething()
-- end
--
---@param classname string  -- the short class name
---@param methodname string
---@param ls number  lnum start
---@param le number  lnum end ( if less 0 then take from end of lines)
function M.find_in_class_method_definition(classname, methodname, bn, ls, le)
  assert(type(classname) == 'string', 'classname')
  assert(type(methodname) == 'string', 'methodname')
  assert(type(bn) == 'number', 'bn - bufnr')
  assert(type(ls) == 'number', 'ls - lnum ')
  assert(type(le) == 'number', 'le - lnum_end')

  local pattern = '^[%s]*function[%s]+' .. classname .. '.' .. methodname .. '%('
  --                   dot for catch both ':' and '.' ---^
  local cnt = vim.api.nvim_buf_line_count(bn)
  if le < 0 then
    le = cnt + le + 1
  end
  if le > cnt then
    le = cnt
  end
  -- log.debug("find_in_class_method_definition: cn:%s mn:%s ls:%s le:%s",
  --   classname, methodname, ls, le)
  for i = ls, le, 1 do
    local line = (vim.api.nvim_buf_get_lines(bn, i, i + 1, false) or E)[1]
    if line and not M.is_commented(line) then -- skip comments
      local definition = string.match(line, pattern)
      if definition then
        return definition, i
      end
    end
  end
  return nil, nil
end

--------------------------------------------------------------------------------
---                QT: helpers for quickly writing tests
--------------------------------------------------------------------------------

---@param message string
---@return string?,string? -- passedin, expected
function M.parse_failure_pass_exp(message)
  if message then
    local pattern = "^Failure:.*Passed in:(.*)Expected:(.*)stack traceback"
    local passed, expected = string.match(message, pattern)
    if passed and expected then
      return su.trim(passed), su.trim(expected)
    end
  end
  return nil, nil
end

-- TODO case:  for assert.same(arg) -- arg == nil
-- Passed in:
-- (nil)
-- Did not expect:
-- type nil

-- (string) ' abc''
-- Note: single quotes are not escaped
---@return string?,any? -- type, value
function M.parse_to_type_value(line)
  if line then
    local type, value = string.match(line, "^%(([%w]+)%) (.*)$")
    if not type then
      type, value = string.match(line, "^%(([%w]+):%s[%w]+%) (.*)$")
    elseif type == 'string' then
      local fc = value:sub(1, 1)
      if fc == '"' or fc == "'" then
        local len, lc = #value, value:sub(-1, -1)
        if lc == fc then -- ensure ends with the same quotes
          len = len - 1
        end
        value = value:sub(2, len) -- remove quotes
      end
    end

    return type, value
  end
  return nil, nil
end

--
function M.parse_test_assert(line)
  if line and line ~= '' then
    local func, arg1, arg2, comment
    -- local pattern_func = "^[%s]*assert%.([%w_%.]+)[%s]*%([%s]*(.*)[%s]*%)(-?-?.*)$"
    --                                  ^function^      ^expression^
    -- func, expr = string.match(line, pattern_func)

    -- start position of the expression
    local sp = string.find(line, "(", 1, true)
    if not sp then
      return nil
    end
    -- end position of the expression
    local ep = su.get_group_end(line, sp)
    if not ep then
      return nil
    end

    -- if M.debug then
    --   print("[DEBUG]                 10        20        30        40")
    --   print("[DEBUG]        1234567890123456789012345678901234567890")
    --   print("[DEBUG] line:", inspect(line), #line, 'sp:', sp, 'ep:', ep)
    -- end

    func = line:match("^[%s]*assert%.([%w_%.]+)[%s]*%(")

    if ep + 1 < #line then
      comment = line:sub(ep + 1, #line)
      -- if M.debug then print("[DEBUG] comment:", inspect(comment)) end
    end
    -- expr = line:sub(sp + 1, ep - 1)

    if func then
      -- if M.debug then print("[DEBUG] group:", inspect(line:sub(sp + 1, ep - 1))) end
      local args, _ = su.split_groups(line, ',', sp + 1, ep - 1, false)
      if args then
        arg1 = args[1] or false --- false to keep order fo comment
        arg2 = args[2] or false
      end
      -- if M.debug then
      --   print("[DEBUG] comment2:", inspect(comment))
      --   print("[DEBUG] args:", inspect(args))
      -- end
      return func, arg1, arg2, comment
    end
  end
  return nil
end

-- todo keywords
-- yes: a a1 abc  ABC a_b
--  no: a.b  0a   'a'  "ab"  {'a'} ['a']
function M.is_simple_varname(s)
  return s and s ~= 'true' and s ~= 'false' and s:match("^[%a_][%w_]*$") ~= nil
end

-- In the current line with some assertion (like assert.same(exp, res)
-- substitute the result from busted, showed in the nvim diagnostics for this
-- line, and update this passedin value into:
-- - either the value itself in the assert.* function body or
-- - its value in an external variable     (like: local exp = "abc")
--
-- That is, this method substitutes the value from the diagnostic into the test
-- code (into function as direct value, or into value of "exp" var befor assert
--
-- supports: multiline string definition like:
-- local exp = [[
--    some text
--    here
-- ]]
-- assert.same(exp, res)
--
-- Note: there should be nothing between the lines with the variable declaration
-- and the assertion.
--
---@param bufnr number
---@param lnum number    -- current line under cursor  where error (starts fr1)
---@param col number     -- cursor por in the line               (index from 1)
---@param diagnostic table with one diagnostic entry from busted
---@param namespace_id number
function M.update_line_from_passedin(bufnr, lnum, col, diagnostic, namespace_id)
  log.debug("update_line_from_passedin")
  assert(type(namespace_id) == 'number', 'namespace_id')

  if not diagnostic or not diagnostic.message then
    log.debug('No d or d.message')
    return
  end
  -- cursor_por starts from 1, the lines in the nvim_bufs from 0, fix it:
  lnum, col = lnum - 1, col - 1

  if not diagnostic.lnum or lnum ~= diagnostic.lnum then
    log.debug('diff ln', diagnostic.lnum, lnum) -- 445 446
    return
  end
  -- if col < diagnostic.col or col > diagnostic.end_col then
  --   log.debug('Not in col')
  --   return
  -- end
  if diagnostic.namespace ~= namespace_id then
    log.debug('Skip not busted namespace')
    return
  end
  -- pass is the passedin (actual value in test)
  -- exp is expected - the correct value we expect in this assertion
  local pass, exp = M.parse_failure_pass_exp(diagnostic.message)
  if not pass or not exp then
    log.debug('Cannot find pass:[%s] or exp:[%s]', pass, exp)
    return
  end
  local cline = vim.api.nvim_get_current_line()
  log.debug('get lines bufnr:%s lnum:%s line:', bufnr, lnum, cline)

  local func, arg1, arg2, comment = M.parse_test_assert(cline)
  log.debug("parse_test_assert give func:%s arg1:%s arg2:%s c:%s from line: [%s]",
    func, arg1, arg2, comment, cline)

  if not func or not arg1 then
    log.debug('Cannot parse assert line f:%s a1:%s a2:%s c:%s currline:%s',
      func, arg1, arg2, comment, cline)
    return
  end

  local tab, used_exp_variable, add_exp_variable, exp_value
  local p_type, p_value = M.parse_to_type_value(pass) -- (string) 'value'
  log.debug("parse_to_type_value give: p_type:[%s] p_value:[%s] pass:[%s]",
    p_type, p_value, pass)

  if not p_type or not p_value then
    if pass == '(nil)' then
      p_type, p_value = 'nil', 'nil'
    else
      log.debug('Cannot parse passedin to type and value', pass, p_type, p_value)
      return
    end
  end
  tab = cline:match('^[%s]*') or '  ' -- indentation from original
  used_exp_variable = arg2 and M.is_simple_varname(arg1)
  if p_type == 'string' then
    p_value, add_exp_variable = M.str2code(p_value, used_exp_variable)
    --
  elseif p_type == 'table' then
    p_value = M.parse_busted_readable_table(p_value)
    if not p_value then
      error('Cannot parse table from ' .. tostring(pass))
    end
    local subtab = tab .. '  '
    p_value, add_exp_variable = M.table2code(p_value, used_exp_variable, subtab)
  end

  -- lines with definition for exp variable
  -- exp_lns & exp_lne -- lnums there exp variable placed in code if it existsts
  local exp_patch, exp_lns, exp_lne, nl = nil, nil, nil, nil

  log.debug("used_exp: %s add_exp:%s %s", used_exp_variable, add_exp_variable)
  if used_exp_variable or add_exp_variable then
    exp_value = p_value
    p_value = arg1                  -- variable name
    if add_exp_variable then
      if not used_exp_variable then --  otherwise use an existing varname
        arg1 = 'exp'                -- varname
      end
      p_value = arg1
    end
    exp_patch = {}

    -- find the place in the code where the value of this variable is defined
    -- here arg1 - varname, nl = no_local prefix in nearest as
    -- nl - no_local in the closest matched assignmention
    _, exp_lns, exp_lne, nl = M.find_var_definition(arg1, bufnr, 0, lnum, false)
    local prefix = 'local '
    if nl then
      prefix = ''
    end
    local exp_line = tab .. prefix .. arg1 .. ' = ' .. exp_value
    if exp_line:find("\n", 1) then
      su.split_range(exp_line, "\n", 1, #exp_line, false, exp_patch)
    else
      exp_patch[#exp_patch + 1] = exp_line
    end
    log.debug("var[%s] defined at lines: %s %s", arg1, exp_lns, exp_lne)
  end

  -- assert.same(nil, res) --> a.is_nil(res)
  if p_type == 'nil' and p_value == 'nil' then
    arg1 = arg2
    arg2 = nil
    log.debug('passedin is nil type swap arg1 to arg2:', arg1)
    --
  elseif arg1 and not arg2 and func ~= 'same' then
    log.debug('func:%s arg1:[%s] p_type:%s p_value:%s NO arg2',
      func, arg1, p_type, p_value)
    --
    if func == 'is_nil' then
      if p_type == 'boolean' then
        func = 'is_' .. p_value   -- case: assert.is_nil(res) --> a.is_false(1)
      else
        func, arg2 = 'same', arg1 -- case: assert.is_nil(1) --> a.same(1, 1)
        arg2 = arg1
      end
    end
  end

  local fixed, update = nil, {}
  if arg2 then
    fixed = tab .. string.format('assert.%s(%s, %s)', func, p_value, arg2)
  elseif arg1 then
    if p_type == 'boolean' and p_value then
      func = 'is_' .. p_value
    elseif p_type == 'nil' then
      func = 'is_nil'
    end
    fixed = tab .. string.format('assert.%s(%s)', func, arg1)
  else
    log.debug('Cannot fix no arg1 and arg2')
    return nil --?
  end
  if comment then
    fixed = fixed .. comment
  end
  update[#update + 1] = fixed -- patch = M.fix_multiline(patch)

  log.debug('Update line bufnr:%s lnum:%s new:[%s]', bufnr, lnum, update)
  vim.api.nvim_buf_set_lines(bufnr, lnum, lnum + 1, false, update)

  -- update exp var definition from a passedin
  log.debug('Update exp var lnum:%s lnum_end:%s new:[%s]', exp_lns, exp_lne, exp_patch)
  if exp_patch then
    -- if exp var not exists - create before assert function
    if not exp_lns or not exp_lne then
      exp_lns, exp_lne = lnum, lnum - 1
    end
    vim.api.nvim_buf_set_lines(bufnr, exp_lns, exp_lne + 1, false, exp_patch)
  end

  return fixed
end

-- Find in the given buffer number a line with a variable declaration
-- and replace its old declaration with the given new one
-- find in given bufnr line with definition of varname (local)
-- and chage it definition to new
---@param bufnr number
---@param lnum number
---@param varname string
---@param replacement table
function M.change_var_definition(bufnr, lnum, varname, replacement)
  assert(type(replacement) == 'table', 'replacement')

  local expr, dln, dln_e = M.find_var_definition(varname, bufnr, 0, lnum, false)
  if expr and dln then
    local u_lnum = dln
    local u_lnum_end = dln_e or dln

    vim.api.nvim_buf_set_lines(bufnr, u_lnum, u_lnum_end + 1, false, replacement)
    u_lnum_end = u_lnum + #replacement
    return u_lnum, u_lnum_end
  end
  return false
end

---@param p_value string
---@param used_exp_variable boolean|nil
function M.str2code(p_value, used_exp_variable)
  local add_exp_variable = false
  local has_new_lines = p_value:find("\n", 1, true) ~= nil
  local has_squote = p_value:find('\'', 1, true)
  local has_dquote = p_value:find('"', 1, true)

  if #p_value >= 50 then
    if has_squote and not has_dquote and not has_new_lines then
      p_value = '"' .. p_value .. '"'
    elseif has_new_lines or (has_squote and has_dquote) then
      local nl0 = ''
      if has_new_lines then nl0 = "\n" end
      p_value = "[[" .. nl0 .. p_value .. ']]'
    else
      p_value = "'" .. p_value .. "'"
    end
    add_exp_variable = not used_exp_variable
    --
  else -- short string
    if not has_new_lines then
      if has_squote and not has_dquote then
        p_value = '"' .. p_value .. '"'
      elseif not has_squote and has_dquote then
        p_value = "'" .. p_value .. "'"
      else -- both
        local f = p_value:sub(1, 1)
        if f == "'" then
          p_value = '"' .. p_value:gsub('"', '\\"') .. '"'
        else
          p_value = "'" .. p_value:gsub("'", "\\'") .. "'"
        end
      end
      --
    elseif not has_dquote then
      p_value = '"' .. p_value:gsub("\n", '\\n') .. '"'
    else
      p_value = '"' .. p_value:gsub('"', '\\"'):gsub("\n", '\\n') .. '"'
    end
  end

  return p_value, add_exp_variable
end

-- fix busted error message passedin-expected
-- issue in that is all string always wrapped in single quotes without cheking
-- for nesteng another single quotes
-- and replace cjon userdata.nil to null (use cjson.null
function M.busted_message_fix_quotes(s)
  if s then
    local res = ''
    for line in string.gmatch(s, '([^\n]+)') do
      -- print("[DEBUG] line:", require"inspect"(line))
      local text = line:match("%s+%*?%[%d+%] = '(.*)'[}%s]*$")
      if text and #text > 2 and su.get_char_count(line, "'", 3) > 2 then
        -- res = res .. line .. " **" .. "\n" -- marck needs to fix
        local fixed = text
        if line:find('"') then
          fixed = text:gsub('"', '\\"')
        end
        local ps = line:find("'") - 1 -- text, 1, true) + 1
        local pe = ps + #text + 1 + 2 -- 2 is for skipped ''
        local q = '"'
        line = line:sub(1, ps) .. q .. fixed .. q .. line:sub(pe, #line)
      elseif line and #line > 16 then
        -- from cjson.null
        local i, j = string.find(line, "userdata: (nil)", 1, true)
        if i and j then
          line = line:sub(1, i - 1) .. 'null' .. line:sub(j + 1)
        end
      end
      res = res .. line .. "\n"
    end
    return res
  end
  return s
end

-- simple string array to lua-table
function M.parse_busted_readable_table(s)
  assert(type(s) == 'string', 'input must be a string has:' .. type(s))
  s = M.busted_message_fix_quotes(s)
  local res, _, tokenizer = m_tokenizer.parse_line(s)
  ---@diagnostic disable-next-line: cast-local-type
  res = res and res[1] or nil
  return res, tokenizer
end

-- table to string
---@param t table
---@param used_exp_variable boolean|nil
---@param tab string|nil
function M.table2code(t, used_exp_variable, tab)
  assert(type(t) == 'table', 't')
  assert(not used_exp_variable or type(used_exp_variable) == 'boolean',
    'used_exp_variable must be a boolean')
  assert(not tab or type(tab) == 'string', 'tab')
  tab = tab or '  '
  local s = tu.table2code(t, tab)
  local add_exp_variable = false

  if #s > 40 and not used_exp_variable then
    add_exp_variable = true
  end
  return s, add_exp_variable
end

function M.get_tab_len(line)
  if line and #line > 0 then
    local tab = line:match('^(%s*)')
    if tab then return #tab end
  end
  return 0
end

--
-- find the range of the block in lua source code starts from given lnum
-- Note: works based Tokenizer
--
-- case 1: function name() ..
-- case 2: function() ..
-- case 3: it(..., function() ...
-- skip:
--   local function
--
-- to get lines:
-- lines = vim.api.nvim_buf_get_lines(bufnr, l_start, l_end + 1, true)
--
---@param bufnr number
---@param lnum number -- the number of line from which go to find block range
---@return number?, number? - lnum_start lnum_end
function M.find_function_block_range(bufnr, lnum)
  dprint("\nget_function_context bufnr:", bufnr, 'lnum', lnum)
  if bufnr and lnum then
    local l_start, l_end = -1, -1
    -- space count in the start line(current with lnum) -- from which find ctx
    -- s - start, ln - line number
    local s_tab_ln, s_tab_n, sblock, sblock2end = lnum, nil, nil, nil
    -- the possible end of the context
    local pl_end, pl_end_tabn = nil, nil

    local tp, t = m_tokenizer.Tokenizer:new(), m_token_tree:new()


    -- go up to start
    -- find start of the function context
    local fbreak = false           -- to impelement a break from repeat-until block
    for i = lnum, 0, -1 do
      repeat                       -- one iteration block to support continue
        local line = (vim.api.nvim_buf_get_lines(bufnr, i, i + 1, true) or E)[1]
        if not line then break end -- continue (break from curr iteration)

        t:sync(tp:tokenize(line))
        local w1, ps, _ = t:cd(0, 1):cat() -- a first word in the line
        local tabn = math.max(ps - 1, 0)   -- spaces count before the first word


        dprint('i:', i, 'tabn:', tabn, 'line:', line, 'word:', w1,
          's_tab_n:', s_tab_n, 'pl_end_tabn:', pl_end_tabn, 'sblock:', sblock)

        -- if sblock then
        if t:cd(0):last():isGroup('[') then -- case: --[[
          local curr_sb = t:getLuaSBlock()
          if sblock and curr_sb == sblock then
            dprint('[[]] exit from sblock wait:', sblock)
            sblock = nil -- exi
          elseif curr_sb and not sblock then
            sblock2end = t.getLuaSBlockPair(curr_sb)
            dprint('[[ remember opened sblock for l_end:', sblock2end)
            if i ~= lnum then
              dprint('[[ reset tab - found opened sblock:', sblock)
              s_tab_ln, s_tab_n = i, tabn
              pl_end, pl_end_tabn = nil, nil --?
            end
          end
        end
        if sblock then
          dprint('skip - inside [[sblock]] i:', i)
          do break end                         -- continue
        elseif t:cd(0):last():isWord(']') then -- to simple support sblocks
          sblock = t.getLuaSBlockPair(t:getLuaSBlock())
          dprint(']] enter to sblock starts: ', sblock)
        end

        -- pick space count(tab) in the start line or prev one if start is empty
        if s_tab_n == nil and #line > 0 and not sblock then
          s_tab_ln, s_tab_n = i, tabn
          dprint('take tab from lnum: ', i, 'tab:', tabn, sblock)
        end

        if tabn == 0 and i ~= lnum and w1 == 'end' then
          dprint('cannot find the start of function block. stop on |end')
          return nil, nil
        end

        if i == lnum and w1 == 'end' then
          dprint('remember the possible pl_end')
          pl_end_tabn, pl_end = tabn, i
          --
          -- it("desc", function()
          --                    ^ 0 2 3 (token path) --------v
        elseif w1 == 'function' or w1 == 'it' and t:cd(0, 2, 3):isGroup('(') then
          --
          if tabn < s_tab_n or
              (i == s_tab_ln) or
              (tabn == 0 and s_tab_n == 0) or
              (pl_end and tabn == pl_end_tabn)
          then
            dprint('found l_start:', i, ' tab_n:', tabn, 'sblock:', sblock)
            s_tab_n, l_start = tabn, i

            if pl_end and tabn == pl_end_tabn then
              dprint('previously memorized pl_end is actualy l_end: ')
              l_end = pl_end
            end
            -- break
            do
              fbreak = true; break
            end -- "continue "goes to next iteration of for
          else
            dprint('has function but not passed as start.')
          end
          -- check possible end
        elseif pl_end and tabn == pl_end_tabn then
          -- keywords forming a pair with the word end
          if w1 == 'if' or w1 == 'else' or w1 == 'elseif' or
              w1 == 'for' or w1 == 'while' or w1 == 'then' or w1 == 'do' then
            dprint('forget pl_end its a part of the block:', w1)
            pl_end, pl_end_tabn = nil, nil
          end
        end
      until true -- one iteration repeat-until to support the continue inside
      if fbreak then break end
    end          -- loop (for)

    if l_start < 0 then
      return nil
    elseif l_end > -1 then
      return l_start, l_end
    end

    -- find the line_end of the function block
    local cnt = vim.api.nvim_buf_line_count(bufnr)
    dprint('==> find the end of the function from:', lnum + 1, 'to:', cnt)
    -- sblock, fbreak = sblock, false -- ?
    fbreak = false
    sblock = sblock2end
    --
    for i = lnum + 1, cnt, 1 do
      local line = (vim.api.nvim_buf_get_lines(bufnr, i, i + 1, false) or E)[1]
      repeat                                     -- one iteration block to support continue
        if not line or #line == 0 then break end -- continue

        t:sync(tp:tokenize(line))
        local w1, ps, _ = t:cd(0, 1):cat() -- a first word in the line
        local tabn = math.max(ps - 1, 0)   -- spaces count before the first word

        dprint('i ', i, 'tab:', tabn, 'line:', line)

        if sblock then
          if t:cd(0):last():isWord(']') then -- to simple support sblocks
            local curr = t:getLuaSBlock()
            dprint('check the sblock wait: ', sblock, 'curr:', curr)
            if curr == sblock then
              dprint('exit from [[]] check passed')
              sblock = nil
            end
          else
            dprint('skip [[sblock]]')
            do break end -- continue
          end
        else             --if not sblock then
          t:cd(0):last()
          if t:isGroup('[') then
            sblock = t.getLuaSBlockPair(t:getLuaSBlock())
            dprint('[[ found start of the sblock:', sblock)
          elseif t:isWord(']') then
            sblock = t:getLuaSBlock()
            dprint(']] found end of the sblock:', sblock)
          end
        end

        if w1 == 'end' and tabn == s_tab_n then
          dprint('found l_end! at i:', i)
          l_end = i
          do
            fbreak = true; break
          end -- break from repeat-until iteration
        end
        -- dprint('end iteration0')
      until true
      if fbreak then break end
      -- dprint('end iteration')
    end

    if l_start > -1 and l_end > -1 then
      dprint('l_start:', l_start, 'l_end:', l_end)
      return l_start, l_end
    end
  end
end

--
-- return the signature (class method params) of the method(function)
-- in which the cursor is located (lnum + col)
-- function Class:method(a, b)
--    <cursor here>              -->   "Class", "method" "a, b"
-- end
--
---@param bufnr number
---@param lnum number
---@param col number|nil
function M.get_signature_of_method(bufnr, lnum, col)
  col = col or 1
  local fbs, fbe = M.find_function_block_range(bufnr, lnum)
  dprint('function range is ', fbs, fbe)
  if fbs and fbe then
    local signature = (vim.api.nvim_buf_get_lines(bufnr, fbs, fbs + 1, false) or E)[1]
    if signature then -- function Class:method()
      local class, method, params =
          signature:match('function%s*([%w_]+)[:.]([%w_]+)%((.*)%)')
      return class, method, params
    end
    return signature
  end
end

return M

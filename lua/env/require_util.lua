local M = {}

-- Helper for testing without PlenaryBustedFile
-- For resolve module from table like vim.loop or from module-name
-- If from_table not nil and varargs has correct path - return value from
-- specified path in table. Overwise return require(module_name)
--
-- Example Usage case: R.require("luv", vim, "loop")
---@param module_name string  for load by full module name
---@param from_table table?   if not nil from_table take module from table
---@param ... string
---@return table of module
function M.require(module_name, from_table, ...)
  if from_table and ... then
    local args = { ... }
    local entry = from_table
    for i = 1, #args do
      local v = args[i]
      if v then
        entry = entry[v]
      end
    end
    if entry then
      return entry
    end
  end

  local ok, m = pcall(require, module_name)
  if not ok then
    m = {}
    -- local saved_path = package.path
    -- Try to change path for load module from $VIMRUNTIME ?
  end
  return m
  --_=k and l.add_context("busted","2.1.2-3")'
end

return M

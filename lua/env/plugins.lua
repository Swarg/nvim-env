-- Quick access to installed plugin, search, research, etc
local M = {}

local uv = vim.loop
local ui = require('env.ui')
local su = require('env.sutil')
local fs = require('env.files')
local cu = require('env.util.commands')
local cb = require('env.util.clipboard')
local telescope = require('env.bridges.telescope')

-- TODO Support not only packer plugin-manager, autodetect installed pm
local ACTIVE_PLUGINS_DIR = '/site/pack/packer/start'
local PLUGINS_DIR = vim.fn.stdpath("data") .. fs.fixpath4os(ACTIVE_PLUGINS_DIR)
local self_plugin_name = "nvim-env"
local self_package_name = 'env'

function M.get_self_plugin_name()
  return self_plugin_name
end

function M.get_self_package_name()
  return self_package_name
end

function M.plugins_dir()
  return PLUGINS_DIR
end

-- root path of this plugin
function M.self_root()
  return PLUGINS_DIR .. fs.path_sep .. self_plugin_name
end

-- List of key:value of plagins name:full-path
---@return table
function M.plugins_list()
  local stat = uv.fs_stat(PLUGINS_DIR)
  if not stat then
    error('Not found ' .. vim.inspect(PLUGINS_DIR))
    return {}
  end

  local items = {}
  local dirs = vim.fn.glob(PLUGINS_DIR .. '/*')
  local paths = vim.split(dirs, '\n', { trimempty = false })
  for _, file in pairs(paths) do
    local key = fs.basename(file)
    items[key] = file
    -- vim.cmd('source ' .. file)
  end
  return items
end

local plugins_list = {}
local plugins_names = {}


local plugins_cmds_list = {
  "path",       -- print|copy path to plugin
  "source",     -- open source file of plugin
  "grep",       -- grep in plugin directory
  "config",     -- open configuration file in nvim config for this plugin
  "readme",     -- find and open readme from plugin directory
  "github",     -- findout link to repository and open|copy
  "commands",   -- find list of commands that this plugin registered
  "keybindings" -- find and show all keybindings registered by this plugin
}

-- function builder for complete EnvPlugin command
local complete_plugin_name = function(arg, cmdline, cursor_pos)
  --print(vim.inspect(arg) .. ' ' .. vim.inspect(cmdline) .. ' ' .. vim.inspect(cursor_pos))
  local words = vim.split(cmdline, ' ', { trimempty = true })
  if not words then
    return {}
  end
  local n = #words;
  -- case: 'cmd arg |'
  if su.substr(cmdline, cursor_pos, cursor_pos) == " " then
    n = n + 1
  end
  local filter = cu.mk_completion_filter(arg)
  if n == 2 then     -- select plugin name
    return cu.completion_sort(vim.tbl_filter(filter, plugins_names))
  elseif n == 3 then -- selet sub-command
    return cu.completion_sort(vim.tbl_filter(filter, plugins_cmds_list))
  end
end

local ENV_PLUGIN_USAGE = [[
open, path, grep, config, site, readme, commands, keybindings
]]
-- Command for interact with installed plugin
-- EnvPlugin <plugin-name> <command>
function M.reg_cmd_EnvPlugin()
  plugins_list = M.plugins_list()
  plugins_names = vim.tbl_keys(plugins_list)
  cu.reg_cmd('EnvPlugin', function(opts)
      -- print(vim.inspect(opts.fargs)) --table, if nargs = '*' then args splited
      local key = opts.fargs[1]
      local cmd = opts.fargs[2]
      if not plugins_list or not cmd or not key then
        return
      end
      -- Action
      local path = plugins_list[key]
      if cmd == "help" or cmd == "h" or cmd == '--help' then
        ui.print(ENV_PLUGIN_USAGE)
      elseif cmd == "source" or cmd == "open" then
        M.plugin_open_source(key, path)
      elseif cmd == "inspect" or cmd == "i" then
        M.plugin_inspect(key, path, opts.fargs)
      elseif cmd == "path" then
        M.plugin_path(path, opts.fargs)
      elseif cmd == "grep" then
        M.plugin_grep(path, opts.fargs)
      elseif cmd == "config" then
        M.plugin_config(path, opts.fargs)
      elseif cmd == "site" then -- aka github
        M.plugin_site(path, opts.fargs)
      elseif cmd == "readme" then
        M.plugin_readme(path, opts.fargs)
      elseif cmd == "commands" then
        M.plugin_commands(path, opts.fargs)
      elseif cmd == "keybindings" then
        M.plugin_keybindings(path, opts.fargs)
      else
        ui.print("Unknown command: " .. vim.inspect(cmd) ..
          " expected: :EnvPlugin <name> <cmd>")
      end
    end,
    -- then nargs = '*' then opts.fargs will be 'cooked' - splited by ' '
    -- otherwise one arg with spaces
    { nargs = '*', complete = complete_plugin_name }
  )
end

---@param path string full path to plugin name
---@param name string plugin dir name
---@return string|nil full path to *.lua-source
function M.find_first_plugin_source(path, name)
  if path and name then
    local ps = fs.path_sep
    local file = vim.fn.glob(path .. ps .. 'lua' .. ps .. '*.lua')
    if not file or file == "" then
      -- path/to/plugin/lua/*/init.lua
      local fn = path .. ps .. 'lua' .. ps .. '*' .. ps .. 'init.lua'
      file = vim.fn.glob(fn)
    end
    return file
  end
  return nil
end

---@param name string plugin dir name
---@param path string full path to plugin name
function M.plugin_open_source(name, path)
  local file = M.find_first_plugin_source(path, name)
  if file and file ~= "" then
    vim.cmd("e " .. file)
    ui.print(file)
  else
    local i = vim.inspect
    ui.print('Not Found [' .. i(name) .. '] ' .. i(path))
  end
end

-- Show inner table structure for a specified plugin in a new buffer
-- with ability to open specified in plagin-table part
--
-- Usage Exapmle:
--   :EnvPlugin null-ls.nvim inspect                          -- show all
--   :EnvPlugin null-ls.nvim inspect builtins code_actions    -- only given
--
---@param name string -- the plugin name
---@param path string -- full path to main plagin lua file
---@param fargs table -- extra args for specify the path
function M.plugin_inspect(name, path, fargs)
  local file = M.find_first_plugin_source(path, name)
  if file and file ~= "" then
    fargs = fargs or {}
    local dir = fs.get_parent_dirname(fs.get_parent_dirpath(file))
    local ok, plugin = pcall(require, dir)
    if ok then
      local bufnr = ui.buf_new_named('Inspect ' .. dir)
      if not bufnr or bufnr == -1 then
        error('Cannot create new buffer to output')
      end
      local t = plugin
      local ns = dir
      ui.buf_append_lines(bufnr, vim.inspect(fargs))
      if #fargs > 2 then
        -- open keys in table hierarhy by given args
        for i = 3, #fargs do
          local k = fargs[i]
          local v = t[k]
          if not v then
            break
          end
          ns = ns .. '.' .. k
          t = v
        end
        ui.buf_append_lines(bufnr, ' namespace ' .. ns)
      end
      ui.buf_append_lines(bufnr, vim.inspect(t))
    else
      print('Cannot found ' .. tostring(dir))
    end
  else
    local i = vim.inspect
    ui.print('Not Found [' .. i(name) .. '] ' .. i(path))
  end
end

---@param path string full path to plugin name
function M.plugin_path(path, args)
  print(vim.inspect(args))
  if args and args[3] == "cb" then
    cb.copy_to_clipboard(path)
    print("Copied to cb: " .. vim.inspect(path))
  else
    print(path)
  end
end

-- grep via telescope.nvim in the sources of selected plugin
---@param path string full path to plugin name
---@param args table
function M.plugin_grep(path, args)
  if path then
    local find_str = ""
    if args and args[3] then
      find_str = args[3]
    end
    telescope.live_grep(path, find_str)
  end
end

---@diagnostic disable-next-line
function M.plugin_readme(path, args)
  print('Not Implements yet')
end

---@diagnostic disable-next-line
function M.plugin_config(path, args)
  print('Not Implements yet')
end

---@diagnostic disable-next-line
function M.plugin_site(path, args)
  print('Not Implements yet')
end

---@diagnostic disable-next-line
function M.plugin_commands(path, args)
  print('Not Implements yet')
end

---@diagnostic disable-next-line
function M.plugin_keybindings(path, args)
  print('Not Implements yet')
end

return M

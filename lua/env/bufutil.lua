-- Works with nvim buffers,
-- opened filenames  check-exist|list|count|info|save|reopen
-- Usage: Save|Reopen the bunch of files
local M = {}
local E = {}

-- local vim.api.= vim.api
local su = require 'env.sutil'
local fs = require 'env.files'

function M.current_bufname()
  return vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
end

-- dub of ctx in env.lua
function M.current_buf_info()
  local api = vim.api
  local t = {
    bufnr = api.nvim_get_current_buf(),
    bufname = api.nvim_buf_get_name(0),
    cursor_pos = api.nvim_win_get_cursor(0),
    --local row, col = unpack(vim.api.nvim_win_get_cursor(0))
    cursor_row = 0,
    cursor_col = 0,
    current_line = api.nvim_get_current_line(),
    can_edit = api.nvim_buf_get_option(0, 'modifiable')
  }
  if t.cursor_pos then
    t.cursor_row = t.cursor_pos[1]
    t.cursor_col = t.cursor_pos[2]
  end
  return t
end

function M.get_buf_extension(bufnr)
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local fname = vim.api.nvim_buf_get_name(bufnr)
  if fname then
    return fs.extract_extension(fname)
  end
  return nil
end

function M.save_current_buffer()
  local bufnr = vim.api.nvim_get_current_buf()
  if vim.o.buftype == '' then
    local uri = vim.uri_from_bufnr(bufnr)
    if not su.starts_with(uri, 'file://') then
      return
    end
    local fname = vim.api.nvim_buf_get_name(bufnr)
    if fname == '' then
      return
    end
    local stat = vim.loop.fs_stat(fname)
    if not stat then
      vim.fn.mkdir(vim.fn.expand('%:p:h'), 'p')
    end
    vim.cmd('w')
  end
end

-- in buffer with given number has opened file and this buffer loaded and
-- this file can be modifiable
---@param bufnr number
local function is_buf_contains_opened_file(bufnr)
  local n = bufnr
  if n and vim.api.nvim_buf_is_loaded(n) and
      vim.bo[n].buftype ~= "nofile" and
      vim.bo[n].buftype ~= "prompt" and -- used in DAP Watches, dap-repl
      vim.api.nvim_buf_get_name(bufnr) ~= "" and
      vim.api.nvim_buf_get_option(n, 'modifiable') then
    return true
  end
  return false
end

-- get only loaded modifiable filenames
---@return table list with filenames of all opened filenames
function M.get_opened_filenames()
  local list = {}
  for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
    if is_buf_contains_opened_file(bufnr) then
      local mod = vim.api.nvim_buf_get_option(bufnr, 'modifiable')
      if mod == true then
        local name = vim.api.nvim_buf_get_name(bufnr)
        table.insert(list, name)
      end
    end
  end
  return list
end

-- Count of only loaded and modifiable opened files
---@param ignore_fn string name of the one ignored-file-name
function M.get_opened_files_count(ignore_fn)
  local n = 0
  for _, bn in ipairs(vim.api.nvim_list_bufs()) do
    if is_buf_contains_opened_file(bn) then
      local name = vim.api.nvim_buf_get_name(bn)
      if name ~= ignore_fn then
        n = n + 1
      end
    end
  end
  return n
end

-- Save list of all currently opened filenames to given filename
-- Olf content of filename will be rewrited
---@param filename string
function M.save_opened_filenames(filename)
  local res = { name = filename, count = 0 }
  if filename and filename ~= "" then
    local list = M.get_opened_filenames()
    local file = fs.open_file(filename, "w")
    local cnt = 0
    if file then
      for _, el in pairs(list) do
        if el and el ~= "" and el ~= filename then
          file:write(el .. '\n')
          cnt = cnt + 1
        end
      end
      file:close()
      res.count = cnt
    end
  end
  return res
end

-- Check is given filename already opened in nvim buffer
---@param filename string
function M.is_filename_already_opened(filename)
  if filename and filename ~= "" then
    for _, bn in ipairs(vim.api.nvim_list_bufs()) do
      if vim.api.nvim_buf_is_loaded(bn) then
        local name0 = vim.api.nvim_buf_get_name(bn)
        if name0 and name0 == filename then
          return true
        end
      end
    end
  end
  return false
end

-- reopen files from file with filenames in each line in
-- if this filename not already opened
---@param filename string
function M.reopen_last_files(filename)
  if filename and fs.file_exists(filename) then
    local opened, skipped = 0, 0
    for name in io.lines(filename) do
      if not M.is_filename_already_opened(name) then
        vim.api.nvim_command(":e" .. name)
        opened = opened + 1
      else
        skipped = skipped + 1
      end
    end
    return { opened = opened, skipped = skipped }
  end
end

-- return current or prev selected text
---@return string?
function M.get_vtext()
  local a_orig = vim.fn.getreg('a')
  local mode = vim.fn.mode()
  if mode ~= 'v' and mode ~= 'V' then
    vim.cmd([[normal! gv]])
  end
  vim.cmd([[silent! normal! "aygv]])
  local text = vim.fn.getreg('a')
  vim.fn.setreg('a', a_orig)
  return text
end

---@class TextSelection
---@field bufnr number
---@field line_start number
---@field line_end number
---@field text string
---@class parent TextSelection|nil

-- Return current selection text an source of selection (buff, line_nr)
---@return TextSelection
function M.get_selected_text(opt)
  local vstart = vim.fn.getpos("'<")
  local vend = vim.fn.getpos("'>")
  local ret = {}
  ret.bufnr = vim.api.nvim_get_current_buf()
  ret.line_start = vstart[2]
  ret.line_end = vend[2]
  ret.text = M.get_vtext()
  if opt and opt.oneline then
    ret.text = ret.text:gsub('\n', ' ')
  end
  return ret
end

-- Return selected lines in current buffer
---@param opts table line1 line2
---@param strict_indexing boolean
---@return table<string>?  lines
function M.get_selected_lines(opts, strict_indexing)
  if opts and type(opts.line1) == 'number' and type(opts.line2) == 'number' then
    local bufn = vim.api.nvim_get_current_buf()
    -- add -1 because otherwise first selected line not in work!
    local ln1 = opts.line1 > 0 and (opts.line1 - 1) or opts.line1
    return vim.api.nvim_buf_get_lines(bufn, ln1, opts.line2, strict_indexing)
  end
end

---@param opts table line1 line2
---@param lines table <string> replacement
function M.set_selected_lines(opts, lines)
  if opts and type(opts.line1) == 'number' and type(opts.line2) == 'number' and
      type(lines) == 'table' then
    local bn = vim.api.nvim_get_current_buf()
    local ln1 = opts.line1 > 0 and (opts.line1 - 1) or opts.line1
    vim.api.nvim_buf_set_lines(bn, ln1, opts.line2, true, lines)
    return true
  end
end

---@return TextSelection
function M.get_current_line()
  local ret = {}
  ret.bufnr = vim.api.nvim_get_current_buf()
  ret.text = vim.api.nvim_get_current_line()
  ret.line_start = 0
  local cursor_pos = vim.api.nvim_win_get_cursor(0)
  if cursor_pos then
    ret.line_start = cursor_pos[1]
  end
  ret.line_end = ret.line_start
  return ret
end

-- insert a string to the given buffer before given lnum
-- if cbi not defined insertion will be occurs before current line
--
---@param line string can be multiline
---@param cbi table? by default get for current buffer and current lnum
function M.insert_line_to(line, cbi)
  local lines = {}
  cbi = cbi or M.current_buf_info()
  local row = cbi.cursor_row

  if line:find('\n') then
    for line0 in line:gmatch("([^\n]*)\n?") do
      if line0 and line0 ~= '' then
        table.insert(lines, line)
      end
    end
  else
    lines[#lines + 1] = line
  end

  vim.api.nvim_buf_set_lines(0, row - 1, row - 1, true, lines)
end

-- Get selected region, works in many modes.
--
---@param winnr number?
---@param opts table?{line1, line2} -- from vim command handler
--
---@return table{line1:number, line2:number}
function M.get_selection_range(winnr, opts)
  local api = vim.api
  local mode = api.nvim_get_mode().mode

  local cursor_pos = api.nvim_win_get_cursor(winnr or 0)
  local cursor_row = (cursor_pos or E)[1] or 0
  -- local cursor_col = (cursor_pos or E)[2] or 0
  local ln_start, ln_end = cursor_row, cursor_row

  if mode == "v" then
    local visual_pos = vim.fn.getpos("v")
    local start_row = visual_pos[2] - 1
    if start_row <= cursor_row then
      ln_start, ln_end = start_row, cursor_row
    else
      ln_start, ln_end = cursor_row, start_row
    end
    --
  elseif mode == "V" then
    local start_row = vim.fn.getpos("v")[2] - 1

    if start_row <= cursor_row then
      ln_start, ln_end = start_row, cursor_row
    else
      ln_start, ln_end = cursor_row, start_row
    end
    --
  end

  -- when the command is applied to the selected row range then
  -- vim.fn.getpos gives the wrong range
  if opts and opts.line1 and opts.line2 and ln_start == ln_end then
    ln_start, ln_end = opts.line1, opts.line2
  end

  return { line1 = ln_start, line2 = ln_end }
end

---@param bufnr number?
function M.get_all_range(bufnr)
  bufnr = bufnr or 0
  local cnt = vim.api.nvim_buf_line_count(bufnr)
  return { line1 = 0, line2 = cnt }
end

---@return boolean
function M.is_visual_mode()
  local m = vim.api.nvim_get_mode().mode
  return m == 'v' or m == 'V' or m == "\x16"
end

--
---@param line1 number
---@param line2 number
function M.select_range(bufnr, line1, line2)
  -- select range around wrapped insertion
  vim.fn.setpos("'<", { bufnr, line1 + 2, 1, 0 })
  vim.fn.setpos("'>", { bufnr, line2 + 1, 9999, 0 })
  vim.cmd([[exe "norm! gv"]])
  -- apply setpos v-block V-line(?)
  -- does not work with gV
end

---@return number, number, number, number -- lnum, col, lnum_end, col_end
function M.get_selection_range_ex()
  M.close_visual_mode()
  vim.cmd([[normal! gv]]) -- restore latest selection range

  -- show not current but previous one
  local vstart = (vim.fn.getpos("'<") or E)
  local vend = (vim.fn.getpos("'>") or E)
  M.close_visual_mode()

  local lnum, lnum_end = vstart[2], vend[2]
  local col, col_end = vstart[3], vend[3]

  return lnum, col, lnum_end, col_end
end

local nvim_replace_termcodes = ((vim or E).api or E).nvim_replace_termcodes or
    -- for testing outside nvim
    ---@diagnostic disable-next-line: unused-local
    function(key, f1, f2, f3) --[[stub]] end

local NVIM_KEY_ESC = nvim_replace_termcodes('<esc>', true, false, true) -- "\27"

function M.close_visual_mode()
  if M.is_visual_mode() then
    vim.api.nvim_feedkeys(NVIM_KEY_ESC, 'x', false)
  end
end

return M

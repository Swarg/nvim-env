-- 05-04-2024 @author Swarg
-- Goal: keep state of statefull modules on reload code on the fly(runtime)

local M = {}

--------------------------------------------------------------------------------
--                             Utils
--------------------------------------------------------------------------------

---@param name string
---@param modt table
---@return boolean
function M.add_package_to_loaded(name, modt)
  assert(type(name) == 'string', 'name')
  assert(type(modt) == 'table', 'modt')
  assert(package.loaded[name] == nil, 'ensure not already loaded')

  package.loaded[name] = modt
  return true
end

---@param name string
---@return boolean
function M.del_package_from_loaded(name)
  assert(type(name) == 'string', 'name')
  assert(package.loaded[name] ~= nil, 'ensure loaded')

  package.loaded[name] = nil
  return true
end

---@param mod table
function M.get_loaded_package_name(mod)
  if type(mod) and package.loaded then
    for name, m in pairs(package.loaded) do
      if m == mod then
        return name
      end
    end
  end
  return nil
end

--------------------------------------------------------------------------------

local get_loaded_package_name = M.get_loaded_package_name

-- modules with state that needs to be maintained after code reload
local registered_stateful_modnames = {}

-- aka interface checker
---@param t any? (expected module itself(table)
---@return table
function M.assert_IStatefullModule(t)
  assert(type(t) == 'table', 'expected module (table) got: ' .. type(t))
  assert(type(t.dump_state) == 'function',
    'expected function dump_state got: ' .. type(t.dump_state))
  assert(type(t.restore_state) == 'function',
    'expected function restore_state got: ' .. type(t.restore_state))
  return t
end

function M.is_statefull_module(t)
  return type(t) == 'table' and type(t.dump_state) == 'function' and
      type(t.restore_state) == 'function'
end

-- own state
local function dump_own_state()
  _G.__env_registered_stateful_modnames = registered_stateful_modnames
end

local function restore_own_state()
  registered_stateful_modnames = _G.__env_registered_stateful_modnames
  _G.__env_registered_stateful_modnames = nil
end

-- register module
---@param modt table
---@return boolean
function M.register(modt)
  local t = M.assert_IStatefullModule(modt)
  local name = assert(get_loaded_package_name(t), 'expected already loaded')
  registered_stateful_modnames[name] = true
  return true
end

---@param modt table
---@return boolean
function M.un_register(modt)
  local t = M.assert_IStatefullModule(modt)
  local name = assert(get_loaded_package_name(t), 'expected already loaded')
  registered_stateful_modnames[name] = nil
  return true
end

---@param modname string
---@return boolean
function M.is_registered(modname)
  return registered_stateful_modnames[modname or false]
end

--
function M.dump_all_state()
  local cnt = 0
  dump_own_state() -- self state with modules names

  for name, _ in pairs(registered_stateful_modnames) do
    local mod = require(name)
    mod.dump_state()
    cnt = cnt + 1
  end
  return cnt
end

function M.restore_all_state()
  local cnt = 0
  restore_own_state() -- self state with modules names

  for name, _ in pairs(registered_stateful_modnames) do
    local mod = require(name)
    mod.restore_state()
    cnt = cnt + 1
  end
  return cnt
end

--------------------------------------------------------------------------------

if _G.TEST then
  M.dump_state = dump_own_state
  M.restore_state = restore_own_state
  M.get_registry = function() return registered_stateful_modnames end
  M.clear_registry = function() registered_stateful_modnames = {} end
end

return M

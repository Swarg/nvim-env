-- 14-07-2024 @author Swarg
-- newStuff from env.langs.*.*Gen (classes, tests, etc)
-- dynamicaly generated for current Project lang(from opened file)

local c4lv = require 'env.util.cmd4lua_vim'
local cu = require 'env.util.commands'

local Lang = require 'env.lang.Lang'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- dynamicaly generated based on current opened file
function M.handle(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(':EnvNew')
      :about('Generate new Stuff depended of the current Project/Lang')
      :handlers(M);

  local lang = Lang.getLangByCtx(w, true, true, false)

  if not lang then
    w -- stub to show all register Lang classes if not found any Project
        :desc('show all supported Langs')
        :cmd('list', 'ls')
  else
    -- generate available commands for programming-language from current,
    -- resolved Project which register self class at the beginning of the
    -- plugin start (env.lua)
    w:clear_level_help()
    lang:newStuff(w)
  end

  w:run():exitcode()
end

local _commands = { "list" }
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

function M.get_current_buffname()
  local api = vim.api
  local bufnr = api.nvim_get_current_buf()
  return api.nvim_buf_get_name(bufnr) -- current opened filename (full)
end

--
-- show all supported Langs
--
--@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end
  w:say(table.concat(Lang.getRegisteredLangsAliases(), " "))
end

return M

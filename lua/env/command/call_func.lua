-- 24-10-2023 @author Swarg
local M = {}
--
local ui = require('env.ui')
local bu = require('env.bufutil')
local fs = require('env.files')
local mdu = require('env.util.markdown')
local ubash = require('env.util.bash')
local ubats = require('env.util.bats')
local uhttp = require('env.util.http')
local cu = require("env.util.commands")
local log = require('alogger')
local c4lv = require("env.util.cmd4lua_vim")
local parser = require("env.util.code_parser")
local spawner = require('env.spawner')
-- to perfome http requests used:
-- local http = require("socket.http") + luasec for https
-- local ltn12 = require("ltn12")

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
--[[
Call the lua function under the cursor in the vim
Usage:
 :EnvCallFunc <Command>
Commands:
    cl clean     forget   -  clear memorized(forget) callable context
   mem remember           -  remember(set) the function under the curstor, for future call
*      run                -  [Default] run function via nvim-build-in: lua require(module).func(arg)
    st status             -  show what is calling on each :EnvCallFunc
     v verbose            -  call func under the cursor and print output to buffer
]]
M.USAGE_EXAPLES = [[
]]

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(":EnvCallFunc")
      :about("Run the lua function from the current line under the cursor")
      :about("in the built-in vim lua interpreter")

      :handlers(M):default_cmd('run')

      :desc('run a func under the cursor as via-cmd: lua require(module).func(arg)')
      :cmd('run')

      :desc("call func under the cursor and print output to buffer")
      :cmd("verbose", "v")

      :desc('remember(set) the function under the curstor, for future call')
      :cmd("remember", "mem")

      :desc('clear memorized(forget) callable context')
      :cmd("forget", "clean")

      :desc("show what is calling on each :EnvCallFunc")
      :cmd("status", "st")

      :run()
end

-- options to register this command in vim
local commands = { 'current', 'remember', 'memorized', 'forget' }
M.opts = { nargs = '*', complete = cu.mk_complete(commands) }

--------------------------------------------------------------------------------
--                               SandBox
--------------------------------------------------------------------------------

-- :EnvCallFunc verbose or <leader>kw
-- lua require('env.command.call_func').callme
-- to check jump to the word of the funcion name and use ":EnvCallFunc status"
function M.call_me(arg)
  print('This text from print')
  print('arg:', arg)
  return 'value of the call_me, arg: ' .. tostring(arg)
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

-- Globaly for frequentry Self Reloading
-- _G.ENV_MEM_ctx_props;  _G.ENV_MEM_func2call

local ERR_NO_CALLABLE_CTX = 'Cannot Resolve Callable context'
local ERR_UNFINISHED_CMD = 'Cannot run unfinished command. No line after \\'

M.call_func_pattern_silent = 'lua require("%s").%s(%s)'
--M.call_func_pattern_verbose = 'lua print(vim.inspect(require("%s").%s()))'

local function get_ext(bufnr)
  local ext = bu.get_buf_extension(bufnr)
  if not ext or ext == '' then
    ext = ui.get_interpreter(bufnr) -- pick from first line #!/bin/bash
  end
  return ext
end
--
---@return string ext
---@return table work_ctx
local function get_ext_and_work_ctx(bufnr)
  local ext = get_ext(bufnr)
  local ctx = M.get_work_ctx(ext)
  return ext, ctx
end

-- create string with lua code to eval the required function (call_func_simple)
---@param w Cmd4Lua
function M.cmd_run(w)
  log.debug("cmd_run")
  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')

  local arg = w:desc('value to pass into a function as an argument')
      :optional():arg(0)
  if arg then
    arg = "'" .. arg .. "'"
  end

  if w:is_input_valid() then
    local ext, t = get_ext_and_work_ctx(0)

    if t.err then
      print(t.err)
      --
    elseif t and t.funcname then
      if ext == 'lua' then
        if not t.module then
          return print('Not found module for ' .. tostring(t.funcname))
        end
        local cmd_pattern = M.call_func_pattern_silent
        local cmd = string.format(cmd_pattern, t.module, t.funcname, arg)
        print(cmd)
        vim.cmd(cmd) -- run function as std nvim command
        --
      elseif ext == 'sh' or ext == 'bash' then
        local ok, bcmd = ubash.build_cmd_to_call_bash_func(t)
        if not ok then
          return print(bcmd)
        end
        local cmd, args = "/bin/bash", { "-c", bcmd } -- not needed "bcmd"
        t.ext = ext
        t.title = 'Run: ' .. t.file .. ':' .. t.funcname
        vim.cmd('w')
        M.run_os_command_in_buf(t, cmd, args, w:is_dry_run())
        --
      elseif ext == 'md' then
        print('Markdown not implements yet.')
        --
      else
        print('call function not supported for ', ext)
      end
      --
    elseif t.cmd then
      if t.not_finished_line then
        print(ERR_UNFINISHED_CMD)
      else
        vim.cmd('w')
        M.run_os_command_in_buf(t, t.cmd, t.args or {}, w:is_dry_run())
      end
      --
    elseif t.uri and t.method then
      M.send_http_request(t, w:is_verbose())
    else
      print(ERR_NO_CALLABLE_CTX)
    end
  end
end

--
-- EnvCallFunc verbose
--  - inside bash and sh files allows to run a function from script
--    in external x-terminal-emulator
--
-- call_func_verbose
---@param w Cmd4Lua
function M.cmd_verbose(w)
  local no_buf = w:desc('run function without a new buffer')
      :has_opt('--no-buff', '-b')

  local arg = w:desc('value to pass into a function as an argument')
      :optional():arg(0)
  if arg then
    arg = "'" .. arg .. "'"
  end

  if w:is_input_valid() then
    local ext, t, func

    if _G.ENV_MEM_func2call and _G.ENV_MEM_ctx_props then
      t = _G.ENV_MEM_ctx_props
      func = _G.ENV_MEM_func2call -- take from memory
      print('Call ' .. M.ctx_name(t))
    else
      ext, t = get_ext_and_work_ctx(0)
      _G.ENV_MEM_ctx_props = nil
      _G.ENV_MEM_func2call = nil

      if ext == 'lua' then
        if t then
          func = M.luaeval_func(t, arg) -- findout from cursor position in buff
        end
      end
    end

    if t and t.err then
      return print('Error:', t.err)
    end
    if not t then
      return print(ERR_NO_CALLABLE_CTX)
    end

    -- middleware
    if t.funcname and t.file then
      -- to call function from bash script
      if ext == 'sh' or ext == 'bash' then
        local ok, bcmd = ubash.build_cmd_to_call_bash_func(t, '\\"')
        if not ok then
          return print(bcmd)
        end
        -- it will be used delow for M.open_in_external_term
        t.cmd, t.args = "/bin/bash", { "-c", '"' .. bcmd .. '"' }
        --
      else
        print('Not supported for ' .. tostring(ext))
      end
    end

    if func then
      if no_buf then
        print(func())
      else
        M.call_func_and_print_to_buf(t, func)
      end
    elseif t.cmd then
      if t.not_finished_line then
        print(ERR_UNFINISHED_CMD)
      else
        vim.cmd('w')
        local pid = M.open_in_external_term(t.cmd, t.args, t.cwd, t.envs)
        print('New Process ' .. tostring(pid))
      end
    elseif t.uri and t.method then
      M.send_http_request(t, true)
    else
      print(ERR_NO_CALLABLE_CTX)
    end
  end
end

---@param w Cmd4Lua
function M.cmd_remember(w)
  if w:is_input_valid() then
    -- clean
    _G.ENV_MEM_ctx_props = nil
    _G.ENV_MEM_func2call = nil

    local ext = get_ext(0)
    local ctx = M.get_module_function_under_cursor(ext)
    if not ctx then
      return print(ERR_NO_CALLABLE_CTX)
    end
    if ctx and ctx.err then
      return print(ctx.err)
    end

    ctx.ext = ext
    _G.ENV_MEM_ctx_props = ctx
    local pref = ''

    if ext == 'lua' then
      _G.ENV_MEM_func2call = M.luaeval_func(_G.ENV_MEM_ctx_props)
      if not _G.ENV_MEM_func2call then
        _G.ENV_MEM_ctx_props = nil
        print(vim.inspect(ctx))
        return print(ERR_NO_CALLABLE_CTX)
      end
      pref = 'Lua'
    elseif ext == 'sh' or ext == 'bash' then
      pref = 'shell'
    end

    print('[' .. pref .. '] Set call to ' .. M.ctx_name(_G.ENV_MEM_ctx_props))
  end
end

---@param w Cmd4Lua
function M.cmd_status(w)
  if w:is_input_valid() then
    if _G.ENV_MEM_ctx_props then
      print('Always Call ' .. M.ctx_name(_G.ENV_MEM_ctx_props))
    else
      local t = M.get_module_function_under_cursor()
      print('In Mem: Clean. Under Cursor: ' .. M.ctx_name(t))
    end
  end
end

---@param w Cmd4Lua
function M.cmd_forget(w)
  if w:is_input_valid() then
    if (_G.ENV_MEM_ctx_props) then
      print('Forget ' .. M.ctx_name(_G.ENV_MEM_ctx_props) .. ". Now Clean")
    end
    _G.ENV_MEM_func2call = nil
    _G.ENV_MEM_ctx_props = nil
  end
end

-- Create code-chunk with calling given function from given module
-- for run inside nvim given public function from given module
-- function and module taked from buffer under cursor and stored in t-table
--
-- debug.traceback()  -- current traceback
-- require 'module'
-- dofile 'file-with-code'
-- loadfile 'file-with-code'
-- loadstring 'print("hellow")'
--
-- lua_ls.settings.Lua.workspace.library = vim.api.nvim_get_runtime_file('', true),
---@param ctx table (module, function, project_root)
---@param arg string|nil
function M.luaeval_func(ctx, arg)
  if ctx and ctx.module and ctx.funcname then
    local code_chunk = string.format(
      [[ local M = require'%s'; return M.%s(%s) ]],
      ctx.module, ctx.funcname, arg)
    local f = loadstring(code_chunk) --, 'dyn-loaded-chunk')
    return f
  end
end

--
---@param func function
---@param ctx table (module, function, project_root)
function M.call_func_and_print_to_buf(ctx, func)
  if func then
    local title = 'Run ' .. M.ctx_name(ctx)
    local output_lines = { '--+-------- Output --------+--' }
    local overrided_print = function(...)
      -- smart varargs handling. build many to one line-string
      local output = ''
      for i = 1, select('#', ...) do
        local val = select(i, ...)
        if type(val) == 'string' then
          output = output .. ' ' .. val
        else -- number, booleans, tables, etc
          output = output .. ' ' .. vim.inspect(val)
        end
      end
      if string.find(output, '\n', 1, true) then
        local lines = vim.split(output, "\n\r?", { trimempty = false })
        vim.list_extend(output_lines, lines, 1, #lines)
      else
        table.insert(output_lines, output)
      end
    end

    local orig_print = print
    print = overrided_print
    local call_ok, result = xpcall(func, debug.traceback)
    print = orig_print

    -- create buffer after calling func for case then called code
    -- must process the data from current buffer
    local bufnr = ui.buf_new_named(title)
    if not bufnr or bufnr == -1 then
      print(result)
      error('Cannot create new buffer to output')
    end
    ui.buf_append_lines(bufnr, { ' [+] ' .. M.ctx_name(ctx), '' })
    ui.buf_append_lines(bufnr, output_lines)
    if call_ok then -- returned value of function
      ui.buf_append_lines(bufnr, { '--+-------- Result --------+--' })
      result = vim.inspect(result)
    else -- stack trace of error in calling func
      ui.buf_append_lines(bufnr, { '--+-------- Error ---------+--' })
      result = M.fancy_traceback(ctx, result)
    end
    ui.buf_append_lines(bufnr, result)
  end
end

function M.build_to_run_full_md_block(cbi, res)
  res = res or {}
  if mdu.is_http_block(cbi) then
    res = mdu.get_http_request(cbi.bufnr, cbi.cursor_row, cbi.current_line)
    --
  elseif mdu.is_shell_block(cbi) then
    res.err = 'run all commands in the bash-block not implemented yet'
    --
  elseif mdu.get_block_syntax(cbi) == 'jq' then
    res.err = 'run jq expression not implemented yet'
    --
  else
    res.err = 'not supported md syntax block ' .. mdu.get_block_syntax(cbi)
  end
  return res
end

-- get function module project info of current opened buffer under cursor
-- cursor must be stay under the function name
---@return table (module, funcname, project)
function M.get_module_function_under_cursor(ext)
  log.debug("get_module_function_under_cursor", ext)
  local cbi = bu.current_buf_info()
  log.debug("cbi: %s, ext:%s", cbi, ext)

  local res = { project = nil, module = nil, funcname = nil }

  if ext == 'md' then
    if mdu.is_syntax_block_head(cbi) then -- cursor at the block head
      res = M.build_to_run_full_md_block(cbi, res)
      --
    elseif mdu.is_inside_block(cbi) and mdu.is_shell_block(cbi) then
      -- if cbi.current_line == '```sh' or cbi.current_line == '```bash' then
      res = mdu.get_full_bash_command(cbi.bufnr, cbi.cursor_row)
      --
      --  ```\n $ bash cmd ```\n
    elseif ubash.is_dollar_first(cbi.current_line) then
      res = ubash.get_bash_command_one_line(cbi.current_line)
    else
      res.err = 'To run a command from a markdown put cursor into command ' ..
          'inside a syntax block (```sh or ```bash)'
      --now at: "' .. cbi.current_line:sub(1, 12) .. '"'
    end
    return res
    --
  elseif ext == 'sh' or ext == 'bash' then
    -- take function name from bash script
    res.funcname = ubash.get_funcname(cbi.current_line)
    if res.funcname then
      res.file = cbi.bufname
      -- pick args and envs from docblock under the function  in current_line
      return ubash.pick_annotations_fr_docblock(cbi.bufnr, cbi.cursor_row, res)
    else
      if not ubash.get_command_from_cmd_route_cond(cbi, res) then
        res = ubash.get_full_bash_command(cbi.bufnr, cbi.cursor_row)
      end
    end
    --
  elseif ext == 'http' or ext == 'https' then
    res = mdu.get_http_request(cbi.bufnr, cbi.cursor_row, cbi.current_line)
    --
  elseif ext == 'jq' then
    res = M.get_cmd_to_run_jq_script(cbi.bufname)
    --
  elseif ext == 'bats' then
    res = ubats.get_cmd_to_run_single_test(cbi.bufname, cbi.current_line)
  end

  local funcname = parser.get_function_name(cbi.current_line)
  if funcname then
    res.funcname = funcname
    res.file = cbi.bufname
    res.project = parser.project_of_buf(cbi)
    local module = parser.get_module(res.project, cbi.bufname)
    if module then
      res.module = module
    end
  end
  return res
end

--
-- run jq program defined in separeted file with same name as file with json
--
-- jq -r -f given_data.jq given_data.json
-- where:
--   given_data.json - a data for processing
--   given_data.jq   - script with jq-filters(program) to process the json-data
--
-- -r -f see https://jqlang.github.io/jq/manual/v1.7/#invoking-jq
--
---@param bufname string
---@return table
function M.get_cmd_to_run_jq_script(bufname)
  local dir = fs.extract_path(bufname)
  local script = fs.extract_filename(bufname)
  local jsonfile = fs.join_path(dir, script .. '.json')
  local t = {}
  if not fs.file_exists(jsonfile) then
    -- without json-data file works with data produce in "jq-program" itself
    t.cmd, t.args = 'jq', { '--null-input', '--from-file', bufname }
  else
    t.cmd, t.args = 'jq', { '--raw-output', '--from-file', bufname, jsonfile }
  end
  return t
end

-- Context for call in the fly.
-- Under the cursor pos in the current buffer or already memorized
---@return table (module, function, project)
function M.get_work_ctx(ext)
  if _G.ENV_MEM_ctx_props and not _G.ENV_MEM_ctx_props.err then
    return _G.ENV_MEM_ctx_props
  else
    _G.ENV_MEM_ctx_props = nil
    _G.ENV_MEM_func2call = nil
    return M.get_module_function_under_cursor(ext) -- can give {} insted nil
  end
end

---@param ctx table (module function [project])
function M.ctx_name(ctx)
  if ctx and ctx.module and ctx.funcname then
    return ctx.module .. '.' .. ctx.funcname .. '()'
  elseif ctx.cmd and ctx.args then
    return ctx.cmd .. ' ' .. table.concat(ctx.args, ' ')
  else
    return '-'
  end
end

--
---@param ctx table {ext, env, cwd}
---@param cmd string
---@param args table
---@param dry_run boolean
function M.run_os_command_in_buf(ctx, cmd, args, dry_run)
  if dry_run then
    local cmd_line = tostring(cmd)
    if args then
      cmd_line = cmd_line .. ' ' .. table.concat(args, ' ')
    end
    print(cmd_line)
    return cmd_line
  end

  log.debug("run_os_command_in_buf: %s %s", cmd, args)
  ctx = ctx or {}
  local filetype = ctx.ext or 'lua'
  local bufname = ctx.title or ("Run: " .. cmd .. ' ' .. filetype)

  -- buffer to show stdout of command (fill ctx.out_bufnr)
  ui.create_buf_nofile(ctx, bufname, filetype)
  if ctx.bufnr then
    -- to jump back to the testfile from the buf with report
    ui.bind_bufs(ctx.bufnr, ctx.out_bufnr)
  end
  local cwd = ctx.project_root or ctx.cwd or os.getenv("$HOME")

  local pp = spawner.new_process_props(ctx.out_bufnr, cmd, args, cwd)
  pp.envs = ctx.envs
  pp.hide_start = true

  spawner.run(pp)
end

--
-- Open given system command in the external x-terminal-emulator
--
---@param cmd string
---@param args table
---@param cwd string?
---@param envs table? - system envs with DISPLAY SHELL and etc
function M.open_in_external_term(cmd, args, cwd, envs)
  cwd = cwd or fs.cwd()
  -- envs = envs or vim.loop.env -- pass EnvVars for lauch term
  -- without it give: "xfce4-terminal Gtk-WARNING **: cannot open display:"

  if not fs.is_os_windows() then                        -- unix
    local unix_term = (M.config or {}).unix_term or 'x' --'/usr/bin/x-terminal-emulator'

    if not fs.file_exists(unix_term) then
      return print('Cannot find unix_term: ' .. tostring(unix_term))
    end
    local sh = os.getenv('SHELL')

    local full_cmd = cmd .. ' ' .. ubash.join_args(args)
    local cd_n_envs = ''

    -- to print the full-command itself in the x-terminal-emulator
    local pref = cwd .. '$ ' .. string.gsub(full_cmd, '"', '\\"') -- cwd + input
    pref = string.gsub(pref, '\\\\"', '\\\\\\"')                  -- fix escapes
    -- change dir and pass envs
    if cwd ~= fs.cwd() then
      cd_n_envs = ' && cd ' .. cwd --+ .. ' && pwd'
    end
    if envs and next(envs) then
      for _, env in ipairs(envs) do
        cd_n_envs = cd_n_envs .. ' && export ' .. env
      end
    end

    local line = "sh -c 'echo \"" .. pref .. "\"" ..
        cd_n_envs .. " && " .. full_cmd .. ';' .. sh .. "'"
    -- /usr/bin/x-terminal-emulator -e 'bash -c "pwd; $SHELL"'
    -- --disable-factory  Do not register with the activation nameserver,
    --                    do not re-use an active terminal
    --  -e                Execute the argument to this option inside the terminal.
    cmd, args = unix_term, { '-H', '--disable-factory', '-e', line }
    -- Note: x-terminal-emulator is needed EnvVars (i.g. DISPLAY) to work
    --
  else -- win
    -- TODO test under win
    cmd, args = 'cmd', { '/c', 'start', 'cmd.exe /c ' .. table.concat(args, ' ') }
  end

  ---@diagnostic disable-next-line: unused-local
  local on_close = function(output, pp, exit_ok, signal)
    local msg = 'Process terminated ' .. tostring((pp or {}).pid)
    -- .. ' exit_ok:' .. tostring(exit_ok) .. ' signal:' .. tostring(signal)
    log.debug('%s%s', msg, output)
    print(msg)
  end

  local pp = spawner.run_async(cwd, cmd, args, vim.loop.env, on_close)
  log.debug('run term[%s] async cmd:%s args:%s', (pp or {}).pid, cmd, args)

  return pp and pp.pid or nil
end

--
---@param reqt table
---@return string
local function mk_buff_name(reqt)
  local n = tostring(reqt.host) .. ' '
  local i = string.find(reqt.uri, '?', 1, true)
  local path = i and reqt.uri:sub(1, i) or reqt.uri or ''
  local name = (n .. path):gsub('/', '.')
  return name
end

--
-- Helper to send http-request from markdown file
-- intended to work through `:EnvCallFunc run` and `:EnvCallFunc verbose`
--
-- Goal: send http[s]-request from HEADERS not from simple url
--
--  GET /v2/_catalog HTTP/1.1
--  PROXY: http://127.0.0.1:443
--  USER: username
--  PASSWORD: passwd
--  Host: registry-mirror.loc
--  User-Agent: custom
--  Accept: */*
--
-- TODO interactively request username and password on 401 Unauthorized
--
---@param t table{uri,host,scheme,method,headers [proxy]}
---@param verbose boolean?  - true - show request and response headers in ouput
function M.send_http_request(t, verbose)
  log.debug("send_http_request", t.method, t.uri)

  local http = require("socket.http")
  local ltn12 = require("ltn12")
  -- https -> luasec

  print('Send http request...')
  vim.schedule(function()
    local response_body = {}

    local reqt = {
      scheme = t.scheme, -- luasock supports https(TLS) via LuaSec package
      method = t.method,
      uri = t.uri,
      host = t.host or t.headers.host,
      headers = t.headers,
      proxy = t.proxy or nil, -- "http://127.0.0.1:80"
      sink = ltn12.sink.table(response_body),
      -- basic-auth:
      user = t.user,
      password = t.password,
    }
    -- to support POST requests with body
    if t.body then
      local payload = tostring(t.body)
      reqt.source = ltn12.source.string(payload);
      reqt['content-length'] = #payload
    end

    local host0, port0 = string.match(reqt.host, '^([^:]+):(%d+)$')
    if host0 and port0 then
      log.debug('host:%s port:%s', host0, port0)
      reqt.host, reqt.port = host0, tonumber(port0)
    end
    local r, code, headers, status = http.request(reqt) -- 1 200 {} "HTTP/1.1 200 OK"
    if not r then
      if verbose then print(vim.inspect(reqt)) end
      log.debug('error reqt:%s', reqt)
      print('[ERROR]: ', '"' .. tostring(code) .. '"')
      return
    end

    if code == 401 then -- 401 Unauthorized
      local ok, www_auth = pcall(require, "www-auth")
      if not ok then
        print('Cannot authenticate the luarock "www-auth" not installed ')
        return
      end
      if ui.ask_confirm_and_do("Authorize?", function()
            --
            if www_auth.auth(t, headers, response_body, ui.ask_value) then
              -- Resend Request with auth data...
              M.send_http_request(t, verbose)
            else
              print('Cannot authenticate: "' .. tostring(t.err) .. '"')
            end
            return true
          end)
      then
        return
      end
    end

    local ctx, filetype, bufname = {}, 'http', mk_buff_name(reqt)

    -- TODO filetype from content-type for non-verbose mode
    ui.create_buf_nofile(ctx, bufname, filetype)
    if ctx.bufnr then ui.bind_bufs(ctx.bufnr, ctx.out_bufnr) end

    local lines = {}

    if verbose then
      -- lines[#lines + 1] = log.format("reqt: %s", t):gsub("\n", ' ')
      uhttp.reqt_to_lines(reqt, t.proto, lines)
      uhttp.headers_to_lines(status, headers, lines)
    end

    uhttp.response_to_lines(response_body, lines)

    -- show code and status for response with empty body
    if #lines == 0 or (#lines == 1 and lines[1] == '') then
      lines[1] = tostring(code) .. ' ' .. tostring(status)
    end

    vim.api.nvim_buf_set_lines(ctx.out_bufnr, 0, 1, true, lines)

    print('Done.')
  end)
end

return M

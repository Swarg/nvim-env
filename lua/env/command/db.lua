-- 05-06-2024 @author Swarg
-- Goal:
--  - send SQL to dbms from opened buffer and show it in separate nvim buffer

local log = require 'alogger'
local su = require 'env.sutil'
local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'
local upsql = require 'env.util.psql'
local db_nvim = require 'db_env.nvim'
local db_env = require 'db_env.env'
local usql = require 'db_env.sql'
local uhibernate = require 'db_env.langs.java.hibernate'

-- doto view scheme via tbl_viewer
-- local tviewer = require 'env.util.tbl_viewer'


local M, T, S, SG = {}, {}, {}, {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvDB')
      :about('Simple DB client to send queries into Database')
      :handlers(M)

      :desc('connect to dbind a specified block type from source code')
      :cmd("connect", "c")

      :desc('show connection status')
      :cmd("status", "st")

      :desc('send SQL to already connected db and show response')
      :cmd("send", "s")

      :desc('interact with given table name in already connected db')
      :cmd("table", "t")

      :desc('disconnect from connected db')
      :cmd("disconnect", "d")

      :desc('interact with sql code')
      :cmd("sql")

      :desc('helpers to configure and monitor your dbms, settings and credentials')
      :cmd("helpers", 'h')

      :run()
      :exitcode()
end

--
-- dbms configurations helpers
--
---@param w Cmd4Lua
function M.cmd_helpers(w)
  w:handlers(M)

  -- configs
      :desc('create new .dbconf file used by this plugin and qdb cli tool')
      :cmd("create-new-dbconf", 'cndc')

      :desc('create configurations with the credentials used to connect to the dbms')
      :cmd("mk-credentials-config", 'mcc')

      :desc('change the owner of the database to the given user (role)')
      :cmd("set-database-owner", 'sdbo')

  -- TODO: create new db-user (for connection via username+password(md5-method)
  -- TODO: create new database for given db-user
  -- TODO: show logged-in users
  -- TODO: the info of local dbms instance (for postgres `pg_lsclusters`)
  -- TODO: postgres: add line with pair "user-password" into ~/.pgpass

      :run()
end

--
-- interact with given table name in already connected db
--
---@param w Cmd4Lua
function M.cmd_table(w)
  w:handlers(T)

      :desc('show all rows in given table')
      :cmd("show", "s")

      :run()
end

function M.cmd_sql(w)
  w:handlers(S)

      :desc('format sql code (make all keywords uppercase)')
      :cmd("format", "f")

      :desc('format log messages from the Hibernate log output')
      :cmd("format-hibernate-log", "fhl")

      :desc('convert multiple "insert into values" commands into one')
      :cmd("compact-insert-values", "civ")

      :desc('generate sql code')
      :cmd("generate", "g")

      :run()
end

--
-- generate sql-code
-- EnvDB sql generate ..
---@param w Cmd4Lua
function S.cmd_generate(w)
  w:handlers(SG)

      :desc('generate CREATE TABLE sql code for one or two [related] tables')
      :cmd("create-table", "ct")

      :run()
end

local _commands = { "connect", "send", "disconnect" }
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local DEF_DB_DRIVER = db_env.DEF_DBMS_DRIVER
local DEF_TABLE_ROWS_LIMIT = 1000
local DEF_SQL_TO_SHOW_TABLE = 'SELECT * FROM %s LIMIT %d;'

--                 Implementation of IStatefullModule

-- for reloading
function M.dump_state()
  _G.__env_db_state = db_env.get_state()
end

function M.restore_state()
  db_env.set_state(_G.__env_db_state)
  _G.__env_db_state = nil
end

-- this method populate lua-table "w.vars" with dbms, dbname, user, etc
---@param w Cmd4Lua
local function define_conn_opts(w)
  -- usage examples:
  -- --connection-string postgres:///mydbname
  -- --cs postgres://dbuser:secret@localhost/mydbname
  -- --cs postgres://dbuser@localhost/mydbname?application_name=myapp
  -- --dbms postgres --host localhost --user dbuser --dbname my_db
  -- dbms specific (postgres):
  -- --connection-string "service=my-service" --pg-service-conf path/to/conf

  w:desc('the name of the dbms driver'):def(DEF_DB_DRIVER):v_opt('--dbms', '-d')

  w:desc('db host'):v_opt('--host', '-h')
  w:desc('db port'):v_opt('--port', '-P')
  w:desc('db name'):v_opt('--dbname', '-D')
  w:desc('db username'):v_opt('--user', '-U')
  w:desc('db password'):v_opt('--password', '-W')

  -- alternative way to define the connection string(insted --dbname --user, etc)
  w:desc('the connection string like url or "dbname=db user=admin password=**"')
      :v_opt('--conn-string', '-cs') -- old sourcename (url or params)

  -- postgres specific
  w:desc('(postgres) service name from pg-service-conf'):v_opt('--service', '-s')
  w:desc('(postgres) config with connection settings'):v_opt('--pg-service-conf')
  w:desc('(postgres) config with credentials for connection'):v_opt('--pgpass')

  -- mysql specific
  -- TODO
end

--------------------------------------------------------------------------------
--                            Helpers
--------------------------------------------------------------------------------
--
-- create configurations with the credentials used to connect to the dbms
--
---@param w Cmd4Lua
function M.cmd_mk_credentials_config(w)
  w:v_opt_verbose('-v')
  define_conn_opts(w) -- populate w.vars with dbms, dbname, user, etc

  if not w:is_input_valid() then return end

  local ret = db_env.create_credentials_configs(w.vars)
  w:say(ret)
  --
end

--
-- change the owner of the database to the given user (role)
--
---@param w Cmd4Lua
function M.cmd_set_database_owner(w)
  w:v_opt_verbose('-v')
  define_conn_opts(w) -- populate w.vars with dbms, dbname, user, etc

  local dbname = w:tag('dbname'):desc('database name'):pop():arg()
  local user = w:tag('user'):desc('username to set database owner'):pop():arg()

  if not w:is_input_valid() then return end

  w:say(db_env.set_db_owner(dbname, user, w.vars))
end

--
-- create new .dbconf file used by this plugin and qdb cli tool
--
---@param w Cmd4Lua
function M.cmd_create_new_dbconf(w)
  if not w:is_input_valid() then return end
  w:say('Use `qdb mk-dbconf` or `qdb gen-db-props`')
end

--------------------------------------------------------------------------------

--
-- connect to dbind a specified block type from source code
--
---@param w Cmd4Lua
function M.cmd_connect(w)
  w:usage('EnvDB connect --source postgres:///mydbname')
  w:usage('EnvDB c --source postgres://dbuser:secret@localhost/mydbname')
  w:usage('EnvDB c --source postgres://dbuser@localhost/mydbname?application_name=myapp')
  w:usage('EnvDB c --host localhost --user dbuser --dbname my_db')
  w:v_opt_verbose('-v')
  define_conn_opts(w) -- user password host port, dbms specific config files

  if not w:is_input_valid() then return end

  -- from first line in the current buffer or from current line
  local source = w.vars.source or db_nvim.get_connstring_from_curent_buf()

  -- the point from which to start searching for the .dbconf file
  w.vars.lookup_from_fn = db_nvim.get_current_bufname()

  local params, errmsg = db_env.prepare_conn_params(source, w.vars)
  if not params or type(params) ~= 'table' then
    return w:error(errmsg)
  end
  w:say(db_env.connect(params))
end

--
-- show connection status
--
---@param w Cmd4Lua
function M.cmd_status(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end
  w:say(db_env.status(w:is_verbose()))
end

---@param sql string
---@param opts table{sql, verbose, dry_run}
local function execute_sql(sql, opts, lines)
  log.debug('execute SQL:', sql)
  local _, ret = db_env.send(sql, opts)

  lines = lines or {}
  local rows

  if type(ret) == 'table' then
    lines = upsql.fmt_table_obj_to_lines(ret, '|', opts, lines)
    rows = #ret - 1
    --
  elseif sql then -- build detailed error report
    su.split_range(sql, "\n", 1, #sql, nil, lines)
    lines[#lines + 1] = ''
    lines[#lines + 1] = ''
    ret = tostring(ret)
    if ret:find('LuaSQL: error executing statement.') then
      ret = ret:sub(36)
    end
    su.split_range(ret, "\n", 1, #ret, nil, lines)
  end

  local metainfo = '(rows: ' .. tostring(rows)
  if opts.name then
    metainfo = metainfo .. ', table: "' .. tostring(opts.name) .. '"'
  end
  if opts.limit then
    metainfo = metainfo .. ', limit: ' .. tostring(opts.limit)
  end
  lines[#lines + 1] = metainfo .. ')'

  local bufnr = db_nvim.create_buf({ sql = sql }, 'db_response', lines)

  return 'the response is output to the buf: ' .. tostring(bufnr)
end

--
-- send SQL to already connected db
--
---@param w Cmd4Lua
function M.cmd_send(w)
  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')

  local sql = w:desc('SQL command to send into already connected db')
      :opt('--sql-command', '-s')

  if not w:is_input_valid() then return end

  local opts = {
    dry_run = w:is_dry_run(),
    verbose = w:is_verbose()
  }

  log.debug('SQL sending...')
  sql = sql or db_nvim.get_current_query()

  w:say(execute_sql(sql, opts))
end

--
-- disconnect from connected db
--
---@param w Cmd4Lua
function M.cmd_disconnect(w)
  if not w:is_input_valid() then return end

  w:say(db_env.disconnect())
end

--------------------------------------------------------------------------------
--                         Sub cmd table
--------------------------------------------------------------------------------

--
-- show all rows in given table
--
---@param w Cmd4Lua
function T.cmd_show(w)
  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')
  local name = w:desc('table name'):opt('--name', '-n')
  local limit = w:desc('rows limit')
      :def(DEF_TABLE_ROWS_LIMIT):optn('--limit', '-l')
  local query = w:desc('SQL to show table')
      :def(DEF_SQL_TO_SHOW_TABLE):opt('--query', '-q')

  if not w:is_input_valid() then return end

  if not name or name == '' then
    local word, col, line = db_nvim.buf_word_under_cursor()
    name = usql.find_table_name_from_sql(line, col, word)
  end

  if not name or name == '' then
    return w:error('Expected name table. use --name opts or put cursor to it')
  end

  local sql = string.format(query, name, limit)

  if w:is_dry_run() then
    return w:say(sql)
  end

  local opts = {
    dry_run = w:is_dry_run(),
    verbose = w:is_verbose(),
    name = name,
    limit = limit,
  }

  w:say(execute_sql(sql, opts))
end

--------------------------------------------------------------------------------
--                         Sub cmd sql
--------------------------------------------------------------------------------

--
-- format sql command (make all keyword uppercase)
--
---@param w Cmd4Lua
function S.cmd_format(w)
  w:v_opt_verbose('-v')
  local no_semicolon = w:desc('not add the semicolon to each command')
      :has_opt('--no-semicolon-ends', '-S')

  if not w:is_input_valid() then return end

  local ctx = db_nvim.get_current_query_ctx(w.vars.vim_opts)
  if not ctx then
    return w:error('Selected lines expected')
  end

  if w:is_verbose() then w:say(vim.inspect(ctx)) end

  ctx.lines = usql.fmt_lines(ctx.lines, {
    semicolon_ends = no_semicolon ~= true
  })
  db_nvim.update_lines(ctx)
end

--
-- format log messages from the Hibernate log output
--
---@param w Cmd4Lua
function S.cmd_format_hibernate_log(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end

  local ctx = db_nvim.get_current_query_ctx(w.vars.vim_opts)
  if not ctx then
    return w:error('Selected lines expected')
  end

  if w:is_verbose() then w:say(vim.inspect(ctx)) end

  ctx.lines = uhibernate.format_log_with_sql(ctx.lines)
  db_nvim.update_lines(ctx)
end

--
-- convert multiple "insert into values" commands into one
--
---@param w Cmd4Lua
function S.cmd_compact_insert_values(w)
  w:v_opt_verbose('-v')

  if not w:is_input_valid() then return end

  local ctx = db_nvim.get_current_query_ctx(w.vars.vim_opts)
  if not ctx then
    return w:error('Selected lines expected')
  end

  local ok, lines = usql.mk_compact_insert_values(ctx.lines)
  if not ok or type(lines) ~= 'table' then
    return w:error(tostring(lines))
  end

  ctx.lines = lines
  db_nvim.update_lines(ctx)
end

-- cmd4lua issue with string to list parsing "a(bc)d" -> {"a" "(bc)" "d"}
-- round brackets
function M.fix_round_brackets_list(list)
  if type(list) == 'table' then
    local t = {}
    local j = -1
    for i, s in ipairs(list) do
      local idx = #t
      if idx > 0 and s and string.sub(s, 1, 1) == '(' then
        t[idx] = t[idx] .. s
        j = i
      else
        if j + 1 == i and s:sub(1, 1) == ':' then -- name:varchar(100):nn
          t[idx] = t[idx] .. s
        else
          t[idx + 1] = s
        end
      end
    end
    return t
  end
  return nil
end

--
-- generate CREATE TABLE sql code for one or two [related] tables
--
-- See also ./lua/env/draw/command/builtin_db.lua
--
---@param w Cmd4Lua
function SG.cmd_create_table(w)
  w:v_opt_verbose('-v')
  w:usage('EnvDB sql generate create-table User --columns [id:int:PK name] ')
  w:usage('EnvDB sql g ct Person -c [id+ name:varchar(55) age:int]')
  w:usage('EnvDB sql g ct Person -c [name:varchar(55) age:int]')

  w:usage('EnvDB sql generate create-table User -c [name:varchar age:int] ')
  w:usage('EnvDB sql g ct User -c [name:varchar age:int] ' ..
    '-t2 Order -c2 [item:varchar] --relationship 1-N') -- one User has many Order

  w:usage('EnvDB sql g ct Citizen -c [name:varchar age:int] ' ..
    '-t2 Passport -c2 [pass_no:int] -r 1-1') -- one Citizen has one Passport

  w:usage('EnvDB sql g ct Actor -c [name:varchar age:int] ' ..
    '-t2 Movie -c2 [name:int year:int] -r N-N') -- many Actor in many Movies

  --

  local tname = w:desc('table name'):pop():arg()
  local columns = w:desc('columns'):optl('--columns', '-c')

  w:desc('the name of the second table'):v_opt('--t2-name', '-t2')
  w:desc('the columns of the second table'):v_optl('--t2-columns', '-c2')

  -- 1 - none ; 1-N one-to-many;  1-1 one-to-one;  N-N many-to-many
  w:desc('relationship of two table [-;1-1;1-N;N-N]')
      :v_opt('--relationship', '-r')

  if not w:is_input_valid() then return end
  ---@cast tname string
  ---@cast columns table

  columns = M.fix_round_brackets_list(columns)
  w.vars.t2_columns = M.fix_round_brackets_list(w.vars.t2_columns)
  w.vars.tables = {}

  local sql, errmsg = usql.gen_create_table(tname, columns, w.vars)
  if not sql then return w:error(errmsg) end
  local ordered_names = { tname, w.vars.t2_name, --[[may be a joining table3]] }

  -- additional templates queries
  local dml
  dml, errmsg = usql.gen_templ_insert_into(w.vars.tables, ordered_names)
  if not dml then w:error(errmsg) else sql = sql .. "\n" .. dml end

  dml, errmsg = usql.gen_templ_delete(w.vars.tables, ordered_names)
  if not dml then w:error(errmsg) else sql = sql .. "\n" .. dml end

  dml, errmsg = usql.gen_templ_select(w.vars.tables, ordered_names)
  if not dml then w:error(errmsg) else sql = sql .. "\n" .. dml end

  if w:is_verbose() then -- show inner table representation
    sql = sql .. "\n" .. require('inspect')(w.vars.tables)
  end

  -- past into current nvim buffer
  local ctx = db_nvim.get_current_query_ctx(w.vars.vim_opts)
  ctx.lines = su.split_range(sql, "\n")
  db_nvim.update_lines(ctx)
end

--
return M

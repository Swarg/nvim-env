-- 01-03-2025 @author Swarg

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

local c4lv = require 'env.util.cmd4lua_vim'
local cu = require 'env.util.commands'

local M = {}

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvYaml')
      :about()
      :handlers(M)

      :desc('get full path in yaml file from element under the cursor')
      :cmd('get-full-path', 'gfp')

      :desc('jump to line with specified $ref (under the cursor or given as arg)')
      :cmd('jump-to-ref', 'j2r')

      :desc('determine the range of a key in the current line and select it body')
      :cmd('select-key-body', 'skb')

      :desc('get sibling(one-level) keys for the key in current line')
      :cmd('get-sibling-keys', 'gsk')

      :run()
      :exitcode()
end

local _commands = { "get-full-path", }
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

-- local log = require 'alogger'
local uyml = require 'env.util.yaml'
local ucb = require 'env.util.clipboard'
local Editor = require 'env.ui.Editor'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


--
-- get full path in yaml file from element under the cursor
--
---@param w Cmd4Lua
function M.cmd_get_full_path(w)
  local copy = w:desc('copy to system clipboard'):has_opt('--copy', '-c')

  if not w:is_input_valid() then return end

  local path, err = uyml.get_full_path()
  if not path then return w:error(err) end
  if copy then
    ucb.copy_to_clipboard(path)
    return w:say('copied: ' .. v2s(path))
  end
  w:say(path)
end

--
-- jump to line with specified $ref (under the cursor or given as arg)
--
---@param w Cmd4Lua
function M.cmd_jump_to_ref(w)
  local ref = w:desc('ref to jump'):optional():pop():arg()

  if not w:is_input_valid() then return end
  ---@cast ref string?
  if ref == 'cb' or ref == 'clipboard' then
    local err
    ref, err = ucb.get_from_clipboard('^[^%s]+$')
    if not ref then return w:error(err) end
  end

  local ok, err = uyml.jump_to_ref(ref)
  if not ok then return w:error(err) end
end

-- helper
local function show_or_copy_found_range(w, lnum, lnum_end, err)
  if type(lnum) ~= 'number' or type(lnum_end) ~= 'number' then
    return w:error(err or 'key not found')
  end
  if w.vars.copy then
    ---@diagnostic disable-next-line
    local lines = Editor.getLines(nil, 0, lnum - 1, lnum_end - 1)
    lines[#lines + 1] = ''
    ucb.copy_to_clipboard(table.concat(lines, "\n"))
    return w:say('lines copied ' .. v2s(lnum_end - lnum))
  end
  w:say(v2s(lnum) .. " " .. v2s(lnum_end) .. '  (lines:' .. v2s(lnum_end - lnum))
end

--
-- determine the range of a key in the current line and select it body
--
---@param w Cmd4Lua
function M.cmd_select_key_body(w)
  local key = w:desc('the full name of the key to be selected'):optional():pop():arg()
  w:desc('copy to system clipboard'):v_has_opt('--copy', '-c')

  if not w:is_input_valid() then return end ---@cast key string?

  local lnum, lnum_end, err = uyml.find_key_body_range(key)
  show_or_copy_found_range(w, lnum, lnum_end, err)
end

--
-- get sibling(one-level) keys for the key in current line
-- by default it copy only keys names without child nodes
-- if you need to copy all range with sublings use --full option
--
---@param w Cmd4Lua
function M.cmd_get_sibling_keys(w)
  local key = w:desc('the full name of the key'):optional():pop():arg()
  local full = w:desc('full range with all keys'):has_opt('--full', '-f')
  w:desc('copy to system clipboard'):v_has_opt('--copy', '-c')

  if not w:is_input_valid() then return end ---@cast key string?

  local lnum, lnum_end, err = uyml.find_sibling_keys_range(key)
  if full then
    return show_or_copy_found_range(w, lnum, lnum_end, err)
  end
  if err or not lnum or not lnum_end then return w:error(err) end
  -- show only keynames
  local keys, err2 = uyml.get_sibling_key_names(lnum + 1, lnum_end, nil)
  if not keys then return w:error(err2) end

  local s = table.concat(keys, "\n")
  if w.vars.copy then
    ucb.copy_to_clipboard(s)
    return w:say("copied keys: " .. v2s(#keys))
  end
  w:say(s)
end

--
return M

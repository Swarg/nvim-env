-- 02-10-2024 @author Swarg
--
-- Decompile compiled artifacts
-- Lang.cmdDecompile

local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'
local Lang = require 'env.lang.Lang'
local ucmd = require 'env.lang.utils.command'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- dynamicaly generated based on current opened file
-- -PL java
function M.handle(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(':EnvDecompile')
      :about('Decompile artifacts(executable) depending on the Project/Lang')
      :handlers(M);

  local lang = Lang.getLangByCtx(w, true, true, false)

  if not lang then
    return print('Not found Project/Lang for opened buffer. Try with -PL ext')
  end

  -- generate available commands for programming-language for current Lang
  w:clear_level_help()
  ucmd.clearSelectLangByExt(w)

  lang:cmdDecompile(w)

  -- w:run():exitcode()
end

local _commands = { "list" }
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

return M

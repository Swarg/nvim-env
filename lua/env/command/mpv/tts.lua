-- 02-12-2024 @author Swarg
--
-- text-to-speech integration
--

local fs = require 'env.files'
local bu = require 'env.bufutil'
local utts = require 'env.util.tts'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- text-to-speech
--
---@param w Cmd4Lua
function M.handle(w, config)
  M.config = config
  w:about('text-tp-speech')
      :handlers(M)

      :desc('set the host(url) of the REST API service')
      :cmd('set-host', 'sh')

      :desc('set the host(url) of the REST API service')
      :cmd('set-speaker', 'sp')

      :desc('set to support SSML (xml tags) by default')
      :cmd('set-ssml', 'ss')

      :desc('generate speech for text in selected lines')
      :cmd('generate-for-lines', 'g')

      :desc('generate speech for text in current vtt entry')
      :cmd('generate-for-vtt-entry', 'ge')

      :desc('check duration for current entry and for generated audio')
      :cmd('check-duration', 'cd')

      :desc('join all audio parts into one audio track')
      :cmd('join-audio', 'ja')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

-- passed from env.setup
-- M.config

--
---@return string?
local function get_api_url()
  return utts.get_tts_config().api_url
end

-- session only scope
---@param url string
local function set_api_url(url)
  utts.get_tts_config().api_url = url
end

--
-- set the host of the rest api server
--
---@param w Cmd4Lua
function M.cmd_set_host(w)
  local url = w:desc('url of the REST API service'):pop():arg()
  if not w:is_input_valid() then return end
  if type(url) == 'string' and url ~= '' and url ~= '.' then
    set_api_url(url)
  end
  w:say('current api_url is: ' .. v2s(get_api_url()))
end

--
-- set the host(url) of the REST API service
--
---@param w Cmd4Lua
function M.cmd_set_speaker(w)
  local speaker = w:desc('the speaker name'):pop():arg()

  if not w:is_input_valid() then return end

  local conf = utts.get_tts_config()
  if type(speaker) == 'string' then
    if speaker == 'ls' then
      return w:say()
    elseif speaker ~= '' and speaker ~= '.' and speaker ~= '?' then
      conf.speaker = speaker
    end
  end
  w:say('current api_url is: ' .. v2s(conf.speaker))
end

--
-- set to support SSML (xml tags) by default
--
---@param w Cmd4Lua
function M.cmd_set_ssml(w)
  local f = w:desc('use ssml flag'):pop():arg()

  if not w:is_input_valid() then return end

  local conf = utts.get_tts_config()
  if f ~= '?' and f ~= '.' and f ~= '' then
    f = string.lower(f)
    conf.ssml = (f == '1' or f == '+' or f == 't' or f == 'true')
  end
  w:say('SSML: ' .. v2s(conf.ssml))
end

--
-- populate w.vars (opts)
--
---@param w Cmd4Lua
local function define_speaker_and_opts(w)
  local conf = utts.get_tts_config()
  local def_speaker = conf.speaker or 'xenia'
  local def_sample_rate = tonumber(conf.sample_rate) or 48000
  local def_api_url = get_api_url()

  w:desc('the speaker name (voice to generate audio)')
      :def(def_speaker):v_opt('--speaker', '-s')

  w:desc 'URL address of REST API Service'
      :def(def_api_url):v_opt('--api-url', '-h')

  w:desc 'With SSML you can control pauses and prosody of synthesized speech.'
      :v_has_opt('--ssml', '-L')

  w:desc('sample_rate(8000,24000,48000)')
      :def(def_sample_rate):v_optn('--sample-rate', '-r')

  w:desc('the filename to save output audio')
      :v_opt('--output-filename', '-f')

  w:desc('the directory to save output audio')
      :def(conf.outdir or './audio'):v_opt('--outdir', '-d')

  w:desc('play generated audio'):v_has_opt('--play', '-p')
end

--
-- generate speech for selected text
--
---@param w Cmd4Lua
function M.cmd_generate_for_lines(w)
  define_speaker_and_opts(w)

  if not w:is_input_valid() then return end

  local vopts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(vopts, false) or {}
  if #(lines or E) == 0 then
    return w:say('empty selection')
  end

  local ok, errmsg, path = utts.generate(lines, w.vars)

  if not ok then return w:error(errmsg) end

  w:say(path)
end

--
-- generate speech(audio-file) for text in current vtt entry
-- Note: you must first connect to mpv via ips
--
---@param w Cmd4Lua
function M.cmd_generate_for_vtt_entry(w)
  define_speaker_and_opts(w)

  if not w:is_input_valid() then return end

  local ok, err = utts.do_generate_text_to_speech(w.vars)
  if not ok then w:error(err) end
end

--
-- check duration for current entry and for generated audio
--
---@param w Cmd4Lua
function M.cmd_check_duration(w)
  w:desc('play generated audio'):v_has_opt('--play', '-p')

  if not w:is_input_valid() then return end

  local ok, err, msg = utts.do_check_audio_duration(w.vars)
  if not ok then
    return w:error(err)
  end
  w:say(msg)
end

local function current_bufname()
  return vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
end
--
-- join all audio parts into one audio track
--
---@param w Cmd4Lua
function M.cmd_join_audio(w)
  w:desc('input audio filename'):def('silence.wav'):v_opt('--input', '-i')
  w:desc('output audio filename'):def('out.wav'):v_opt('--output', '-o')
  w:desc('sample_rate'):def(44100):v_optn('--sample-rate', '-r')
  w:desc('channels'):def(1):v_optn('--channels', '-c')
  -- a_001.mp3 [a_002.mp3] a_003.mp3 ...
  w:desc('ignore missing audio files(index-gaps)'):v_has_opt('--ignore-gaps', '-g')

  local dir = w:desc('dir'):pop():arg()
  local subst = w:desc('filename of the vtt subtitle with timestamps'):pop():arg()

  if not w:is_input_valid() then return end

  if dir == '.' then dir = os.getenv('PWD') end
  if subst == '.' then
    local fn = current_bufname()
    if fn and string.match(fn, '%.vtt$') then
      subst = fn
    else
      return w:error('supported only from vtt files')
    end
  end
  ---@cast subst string
  ---@cast dir string

  local ok, err, entries = utts.join_audio_parts(dir, subst, w.vars)
  if not ok then
    return w:error(err)
  end

  if type(entries) == 'table' then
    local ffmpeg_cmd = utts.gen_ffmpeg_cut_parts_cmd(entries)
    if ffmpeg_cmd ~= '' then
      local fn = fs.join_path(dir, 'cut-video-parts.sh')
      local saved = fs.str2file(fn, ffmpeg_cmd, 'wb')
      w:say('ffmpeg commad to cur parts: ' .. v2s(fn) .. ' saved:' .. v2s(saved))
    end
  end
  print(entries, ok)
end

return M

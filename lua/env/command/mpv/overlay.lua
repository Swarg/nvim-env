-- 06-12-2024 @author Swarg
--
-- Goal: work with adding images to the right place in the video
--

local fs = require 'env.files'
local uvo = require 'env.util.mpv.video_overlay'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- EnvMpv overlay
--
---@param w Cmd4Lua
function M.handle(w, config)
  M.config = config
  w:about('overlay')
      :handlers(M)

      :desc('make script to add overlay with all defined in given vtt file images')
      :cmd('gen-script', 'gs')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local function current_bufname()
  return vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
end

--
-- generate script with command for ffmpeg based on given vtt file
-- parse given vtt file
-- (subtitle-based defintion of the places and time for specified images)
--
-- the main feature of this mechanic is that it allows you to create a
-- image-container with slots inside which you can simply use the slot number
-- to place the desired image into specified place in the screen by its alias
--
---@param w Cmd4Lua
function M.cmd_gen_script(w)
  local dir = w:desc('dir'):pop():arg()
  local subt = w:desc('vtt file'):pop():arg()
  w:desc('input video filename'):v_opt('--input', '-i')
  w:desc('output vide filename'):v_opt('--output', '-o')
  w:desc('filename of the generated shell script')
      :def('overlay.sh'):v_opt('--script', '-s')

  if not w:is_input_valid() then return end

  if dir == '.' then dir = os.getenv('PWD') end
  if subt == '.' then
    local fn = current_bufname()
    if fn and string.match(fn, '%.vtt$') then
      subt = fn
    else
      return w:error('supported only from vtt files')
    end
  end
  ---@cast dir string
  ---@cast subt string

  local cmd, err, _ = uvo.gen_ffmpeg_cmd(dir, subt, w.vars)
  if not cmd then
    return w:error(err)
  end
  local fn = w.vars.script
  local saved = fs.write(fn, cmd, 'w')
  w:say('script: ' .. v2s(fn) .. ' saved:' .. v2s(saved))
end

--
return M

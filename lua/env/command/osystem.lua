-- 19-03-2024 @author Swarg
-- Goal:
--   - research os processes state
--   - tree of all process
--   - subtree of given process by pid
--   - all parents up from given pid
--   - door to experiments subcmd


-- local log = require('alogger')
local ui = require('env.ui')
local uos = require("env.util.os")
local cu = require("env.util.commands")
-- local su = require('env.sutil')
-- local fs = require('env.files')
local tviewer = require("env.util.tbl_viewer")
local c4lv = require("env.util.cmd4lua_vim")

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop")

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- Note: nvim vars opts.line1 opts.line2  passed via cmd4lua_vim.newCmd4Lua
--
---@param opts table from nvim with line1, line2 and fargs
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvOSystem')
      :about('Interact with Operation System')
      :handlers(M)

      :desc('Show status of given processes by pids')
      :cmd('status', 'st')

      :desc('Show pid of current nvim instance and its childs')
      :cmd('getpid', 'pid')

      :desc('Report about memory status')
      :cmd('memory-status', 'm')

      :desc('Show hierarchy tree of all running processes')
      :cmd('process-tree', 't')

      :desc('Show the hierarchy of all parent processes for a given pid')
      :cmd('parents-procs', 'pp')

      :desc('Show all child process for a given pid')
      :cmd('childs', 'c')

      :desc('experiments and reserch into the work of external programs')
      :cmd('proc', 'p')

      :run()
end

local commands = {
}
-- options to register this command in vim
M.opts = { nargs = "*", range = true, complete = cu.mk_complete(commands) }

--------------------------------------------------------------------------------
--                               SandBox
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule

M.ps_table = nil -- state

-- for reload
function M.dump_state()
  -- require("env.command.osystem.process").dump_state()
  _G.__env_osystem_ps_table = M.ps_table
end

function M.restore_state()
  -- require("env.command.osystem.process").restore_state()
  M.ps_table = _G.__env_osystem_ps_table
  _G.__evn_osystem_ps_table = nil
end

--------------------------------------------------------------------------------

function M.cmd_proc(w)
  require("env.command.osystem.proc").handle(w)
end

---@param w Cmd4Lua
local function define_save_reuse_opts(w)
  w:desc('save parsed ouput from ps to future reuse')
      :v_has_opt('--save-output', '-s')

  w:desc('reuse saved parsed output without ask ps againe')
      :v_has_opt('--use-output', '-u')
end

local own_pid = nil

---@return number
local function getpid()
  if own_pid == nil then
    own_pid = uv.getpid()
  end
  return own_pid
end

--
-- as cast to num from optn
--
---@param pid any
---@return number
---@param optional boolean?
local function own_pid_on_zero(w, pid, optional)
  if pid == 0 or pid == "0" or pid == 'own' or pid == 'self' then
    return getpid()
  end
  local npid = tonumber(pid)
  if not npid then
    if optional then
      return 0 -- nil
    end
    w:error('expected pid, got ' .. tostring(pid))
  end
  return npid or -1
end

--
---@param w Cmd4Lua
---@return table?
local function get_parsed_ps_output(w)
  local t = nil
  if w.vars.reuse_output then -- reuse old
    if not M.ps_table then
      return w:error('no saved ps output use --save before')
    end
    t = M.ps_table
  end

  if not t or w.vars.save_output then
    local output = uos.get_ps_output()
    t = uos.parse_ps_output(output)
    if w.vars.save_output then
      M.ps_table = t -- keep to reuse in next time
      print('parsed ps output saved for reuse.')
    end
  end

  return t
end

function M.init_new_buff(title, filetype)
  filetype = filetype or 'md'
  title = title or 'OSystem'
  local ctx = {}
  ui.create_buf_nofile(ctx, title, filetype)

  local lncnt = vim.api.nvim_buf_line_count(ctx.out_bufnr)
  local nvim_buf_set_lines = vim.api.nvim_buf_set_lines
  local bufnr = ctx.out_bufnr
  local tmp_lines = {}

  local append_line = function(line)
    tmp_lines[1] = line
    nvim_buf_set_lines(bufnr, lncnt, lncnt, true, tmp_lines)
    lncnt = lncnt + 1
  end

  return ctx, append_line
end

--
-- Show status of given processes by pids
-- for one process by its pid of for a list of pids
--
---@param w Cmd4Lua
function M.cmd_status(w)
  define_save_reuse_opts(w)
  local title = w:desc('prefix for title of a buffer'):opt('--title', '-t')
  local pids = w:desc('pids'):pop():argl() ---@cast pids table
  local view = w:desc('open all parsed data in the TableViewer'):has_opt('-V')

  if not w:is_input_valid() then return end

  local t = get_parsed_ps_output(w)
  if w:has_error() then return end ---@cast t table

  if not pids or next(pids) == nil then
    return w:error('pids not specified')
  end

  title = 'OSystem:Status' .. tostring(title or '')

  if view then
    local ctx = { state = tviewer.wrap2state(t, title) }
    ctx.state.node_name_cb = uos.mk_node_name
    tviewer.create_buf(ctx, ctx.state.src or 'table-viewer')
    return
  end

  local _, append_line = M.init_new_buff(title)

  append_line(uos.ps_state_title)
  -- body
  for _, pid in pairs(pids) do
    pid = own_pid_on_zero(w, pid, false)
    append_line(uos.ps_state2str(t[pid], pid))
  end
end

--
-- Show pid of current nvim instance and its childs
--
---@param w Cmd4Lua
function M.cmd_getpid(w)
  if not w:is_input_valid() then return end
  print(uv.getpid())
end

--
-- Report about memory status
--
---@param w Cmd4Lua
function M.cmd_memory_status(w)
  define_save_reuse_opts(w)
  local title = w:desc('prefix for title of a buffer'):opt('--title', '-t')
  local pid = w:desc('show data for given pid'):optn('--pid', '-p')

  local fcmd = w:desc('show state for processes filted by given part of command')
      :opt('--filterby-cmd', '-fc') -- use .* to pass all cmds

  local fsize = w:desc('show only processes with used memory size more than (Kb)')
      :opt('--filterby-mem', '-fm')

  local fcpu = w:desc('filter by consumed CPU %'):optn('--filterby-cpu', '-fu')
  local show_cwd = w:desc('show the cwd of the processes'):has_opt('--cwd', '-d')

  if not w:is_input_valid() then return end

  local t = get_parsed_ps_output(w)
  pid = own_pid_on_zero(w, pid, true)
  if w:has_error() then return end ---@cast t table

  local function sort_by_size(a, b)
    return a.rss < b.rss --size
  end

  if pid and pid > 0 then
    print(uos.ps_state_title)
    print(uos.ps_state2str(t[pid], pid))
    return
  end


  local tt = {} -- for showing
  local cnt, mem, trss, pcpu, maxpid = 0, 0, 0, 0, 0
  -- readable to Kb
  if fsize then fsize = uos.str_size2num(fsize, 1024) / 1024 end -- in Kb
  local filter_by_size = type(fsize) == 'number' and fsize > 0
  local filter_by_cpu = type(fcpu) == 'number' and fcpu > 0

  --  all or only filtered processes
  for _, v in pairs(t) do
    if v.size then --
      local passed, show = true, false

      if passed and filter_by_size then
        if v.size >= fsize then show = true else passed = false end
      end

      if passed and filter_by_cpu then
        if v.pcpu >= fcpu then show = true else passed = false end
      end

      if passed and fcmd then
        if v.cmd and string.find(v.cmd, fcmd) then
          show = true
        else
          passed = false
        end
      end

      if passed then
        if v.pid > maxpid then maxpid = v.pid end
        mem = mem + v.size
        trss = trss + v.rss
        pcpu = pcpu + v.pcpu
        cnt = cnt + 1
      end

      if passed and show then
        tt[#tt + 1] = v
      end
    end
  end

  local totals = string.format('processes:%s memory(SIZE:%s,RSS:%s) cpu:%s%% max-pid:%s',
    cnt, uos.kb2readable(mem), uos.kb2readable(trss), pcpu, maxpid)

  if not next(tt) then
    return w:say(totals)
  end

  table.sort(tt, sort_by_size)
  title = 'OSystem:Memory' .. tostring(title or '')
  local _, append_line = M.init_new_buff(title)
  local details = ''

  if show_cwd then uos.enrich_ps_state_with_cwd(tt) end

  if fcmd then details = details .. ' CMD with: "' .. tostring(fcmd) .. '"' end
  if fsize then details = details .. ' SIZE >= ' .. uos.kb2readable(fsize) end
  if fcpu then details = details .. ' CPU % >= ' .. tostring(fcpu) end

  if details ~= '' then
    append_line('Filtered by: ' .. details)
  end
  append_line('')

  append_line(uos.ps_state_title)
  -- body
  for _, v in pairs(tt) do append_line(uos.ps_state2str(v)) end

  append_line('')
  append_line(totals)
end

--
-- Show the hierarchy of all parent processes for given pid
--
---@param w Cmd4Lua
function M.cmd_parents(w)
  define_save_reuse_opts(w)
  local pid = w:desc('pid of a process for which show all its parents')
      :pop():argn()

  if not w:is_input_valid() then return end

  local t = get_parsed_ps_output(w)
  pid = own_pid_on_zero(w, pid)
  if w:has_error() then return end ---@cast t table


  local d = t[pid or false]
  if not d then
    return w:error('Not found process with pid: ' .. tostring(pid))
  end

  local tree = { d }
  while d do
    d = t[d.ppid or false] -- get parent
    tree[#tree + 1] = d
  end

  local title = 'Process Tree for ' .. tostring(pid)
  local _, append_line = M.init_new_buff(title)

  append_line(title)
  append_line(uos.ps_state_title)
  -- reverse
  for i = #tree, 1, -1 do
    append_line(uos.ps_state2str(tree[i]))
  end
end

--
-- Show all child process for given pid
--
---@param w Cmd4Lua
function M.cmd_childs(w)
  w:usage(':EnvOS childs 0  -- to show all own childs ')
  define_save_reuse_opts(w)
  local pid = w:desc('pid of the process for which show its childs'):pop():argn()
  local recursive = w:desc('build recursive tree'):has_opt('--recursive', '-R')

  if not w:is_input_valid() then return end

  local t = get_parsed_ps_output(w)
  pid = own_pid_on_zero(w, pid)
  if w:has_error() then return end ---@cast t table

  local parent = t[pid or false]
  if not parent then
    return w:error('Not found process with pid: ' .. tostring(pid))
  end

  local title = 'Childs Processes of pid: ' .. tostring(pid)
  if not recursive then
    local list = uos.build_child_list(t, pid, false)
    local _, append_line = M.init_new_buff(title)

    append_line(title)
    append_line(uos.ps_state_title)
    append_line(uos.ps_state2str(parent)) -- process itself

    for _, v in ipairs(list) do
      append_line(uos.ps_state2str(v))
    end
    --
  else -- recursive
    local tree = {
      [pid] = { parent, childs = uos.build_child_list(t, pid, recursive) }
    }
    local ctx = { state = tviewer.wrap2state(tree, title) }
    ctx.state.node_name_cb = uos.mk_node_name
    tviewer.create_buf(ctx, ctx.state.src or 'table-viewer')
  end
end

--
-- Show hierarchy tree of all running processes
-- title when you need to show multiples buffers with diff content
-- by default each new request will redraw in same buffer(with same title)
--
---@param w Cmd4Lua
function M.cmd_process_tree(w)
  define_save_reuse_opts(w)
  local title = w:desc('prefix for title of a buffer'):opt('--title', '-t')
  local flat = w:desc('show as flat list'):has_opt('--flat', '-f')

  if not w:is_input_valid() then return end

  local t = get_parsed_ps_output(w)
  if w:has_error() then return end ---@cast t table

  local tree
  if flat then
    tree = t
  else
    tree = uos.build_tree(t)
  end

  title = 'Processes Tree' .. (title or '')
  local ctx = { state = tviewer.wrap2state(tree, title) }
  ctx.state.node_name_cb = uos.mk_node_name
  tviewer.create_buf(ctx, ctx.state.src or 'table-viewer')
end

return M

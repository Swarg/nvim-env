-- 24-10-2023 @author Swarg
local M = {}

-- UseCase: runs functions via comments in a source file. see SandBox
--
-- Execute text in comment as vim command.
-- Pick the text in the comment at the current line under cursor
-- and run it as vim command.
-- Then you need to perform command in specific linenumber you can remember it
-- and call after placed cursor at the destination line

local c4lv = require("env.util.cmd4lua_vim")
local cu = require("env.util.commands")

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--[[
Execute line from buffer as a vim command
Usage:
 :EnvLineExec <Command>
Commands:
*      c current      -  [Default] execute the current line under the cursor
   clean forget       -  foget last memorized line
    last memorized    -  run already memorized line as vim command
     p2m remember     -  remember the current line for future use
]]
---@param opts table|nil opts from nvim
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(":EnvLineExec")
      :about('Execute line from the buffer as a vim command')

      :handlers(M)
      :default_cmd('current') -- default behavior

      :desc('execute the current line under the cursor')
      :cmd("current", 'c') -- M.cmd_current

      :desc('remember the current line for future use (put to mem)')
      :cmd("remember", "p2m") -- M.cmd_remember

      :desc('run already memorized line as vim command')
      :cmd("memorized", "last") -- M.cmd_memorized(w)

      :desc('foget last memorized line')
      :cmd("forget", "clean")

      :run()
end

-- for complete
local commands = { 'current', 'remember', 'memorized', 'forget' }
-- options to register this command in vim
M.opts = { nargs = '*', complete = cu.mk_complete(commands) }


--------------------------------------------------------------------------------
--                               SandBox
--------------------------------------------------------------------------------

-- you can try it via jump to next line and call ':EnvLineExec current'

-- :EnvInfo
-- lua require('env.cmds.call_func').call_me()

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local memorized_line

-- '--  some-content -- comment'  -->  'some-content --comment'
---@return string
local function get_current_line_for_run()
  local line = vim.api.nvim_get_current_line()
  line = line:gsub("^%-+", "") -- left trim all comments
  line = line:gsub("^%s+", "") -- left trim all spaces
  return line
end

-- run the current line
---@param w Cmd4Lua
function M.cmd_current(w)
  if w:is_input_valid() then
    local line = get_current_line_for_run()
    if line and line ~= "" then
      print(line) -- print to history to acess via ':messages'
      vim.cmd(line)
    end
  end
end

---@param w Cmd4Lua
function M.cmd_remember(w)
  if w:is_input_valid() then
    memorized_line = get_current_line_for_run()
    print('Memorized: ' .. vim.inspect(memorized_line))
  end
end

---@param w Cmd4Lua
function M.cmd_memorized(w)
  if w:is_input_valid() then
    local line = memorized_line
    if not line or line == "" then
      print('No Memorized line')
      return
    end
    print(line) -- print to history to acess via ':messages'
    vim.cmd(line)
  end
end

---@param w Cmd4Lua
function M.cmd_clean(w)
  if w:is_input_valid() then
    memorized_line = nil
  end
end

return M

-- 09-01-2024 @author Swarg

local M = {}

local dprint = require('dprint')
---@diagnostic disable-next-line
local c4lv = require("env.util.cmd4lua_vim")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

---@param opts table
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvLog')
      :about('Interact with dprint (DebugPrint)')
      :handlers(M)

      :desc('Show version of extended luarocks packege names "dprint"')
      :cmd('version', 'v')

      :desc('Status of the all modules mapping name -> enabled')
      :cmd('status', 'st')

      :desc('Enable|Disable debug print for given or all modules')
      :cmd('set-enabled', 'se')

      :desc('Set Source Root')
      :cmd('set-src-root', 'sr')

      :desc('Call dprint to issue debug print output')
      :cmd('message', 'm')

      :run()
end

M.opts = { desc = 'nvim-env dprint', nargs = '*' }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

---@param w Cmd4Lua
function M.cmd_version(w)
  if w:is_input_valid() then
    print('dprint._VERSION: ' .. tostring(dprint._VERSION))
    print('dprint._URL:     ' .. tostring(dprint._URL))
  end
end

---@param w Cmd4Lua
function M.cmd_status(w)
  local module = w:desc('Module Name'):optional():pop():arg()

  if w:is_input_valid() then
    local st
    if not module or module == 'all' or module == '*' then
      st = dprint.status()
    elseif type(module) == 'string' then
      st = dprint.status_of(module)
    else
      st = 'no module name'
    end

    print(vim.inspect(st))
  end
end

---@param w Cmd4Lua
function M.cmd_set_enabled(w)
  w:about('Define "dprint.on" to change global enabled of dprint')

  local module = w:desc('Set enabled for one module by full name or for ' ..
    '"dprint.on" | "all" '):pop():arg()
  local enable = w:desc('Enable dprint (true|false)'):pop():argb()
  local except = w:desc('Do not change state of this modules')
      :optl('--except', '-e')

  if w:is_input_valid() then
    local msg

    if module == '__dprint' or module == 'dprint.on' then
      dprint.on = enable
      msg = 'dprint.on=' .. tostring(dprint.on)
      --
    elseif module == 'all' or module == '*' then
      local c, t = dprint.enable_all_modules(enable, except)
      msg = ("Enabled=%s applyed to %s/%s modules"):format(tostring(enable), c, t)
      --
    elseif type(module) == 'string' then
      dprint.enable_module(module, enable)
      msg = vim.inspect(dprint.status_of(module))
    end

    print(msg)
  end
end

--
-- src_root is used to shorten the trace message
-- at the moment you can specify only one root, for example for a project
-- or for library like: "/usr/local/share/lua/5.1/"
--
---@param w Cmd4Lua
function M.cmd_set_src_root(w)
  local auto = w:desc('Auto search src-root')
      :group('A', 1):has_opt('--auto', '-a')
  local root = w:desc('src_root used in trace outputs')
      :group('A', 1):pop():arg()

  if w:is_input_valid() then
    if auto then
      print('auto search for src_root...')
      dprint.src_root = dprint.find_src_root(nil, 1)
    elseif root then
      dprint.src_root = root
    else
      print('Error --auto or src_root-value')
    end
    print('src_root: |' .. (dprint.src_root or '') .. '|')
    print('now trace is: ' .. tostring(dprint.get_trace(1)))
  end
end

---@param w Cmd4Lua
function M.cmd_message(w)
  local msg = w:desc('message for dprint'):def('Hellow %s'):optional():pop():arg()
  local params = w:desc('params for message'):def({ 'World' })
      :optional():optl('--params', '-p')

  if w:is_input_valid() then
    if not dprint.on then
      print('Warn: dprint is disabled. Sending a debug message will be ignored')
      -- use EnvDPrint se self true
    end
    dprint.dprint(msg, unpack(params or {}))
  end
end

return M

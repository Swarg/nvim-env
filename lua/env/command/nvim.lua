-- 10-04-2024 @author Swarg
local M = {}
--
local c4lv = require("env.util.cmd4lua_vim")
local cu = require("env.util.commands")
-- local tviewer = require("env.util.tbl_viewer")

local R = require 'env.require_util'
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect


--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvVim')
      :about('Nvim Research tool')
      :handlers(M)

      :desc("nvim_win_set_cursor")
      :cmd("set-cursor-pos", 'scp')

      :desc("nvim_replace_termcodes")
      :cmd("termcodes", 'tc')

      :cmd("termcodes-build-map", 'tcbm')

      :cmd("visual-modes", 'vm')

      :run()
      :exitcode()
end

local _commands = {
  "termcodes",
}
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------
--

local fmt = string.format

--
-- nvim_replace_termcodes
-- <esc> -> \27
---@param w Cmd4Lua
function M.cmd_termcodes(w)
  local keys = w:desc('string with keys'):pop():arg()
  local from_part = w:tag('from_part'):def(true):optb('--from_part')
  local do_lt = w:tag('do_lt'):def(false):optb('--do_lt')
  local special = w:tag('special'):def(true):optb('--special')

  if not w:is_input_valid() then return end ---@cast keys string

  local value = vim.api.nvim_replace_termcodes(keys, from_part, do_lt, special)
  local bytes = { string.byte(value, 1, #value) }
  w:say('(', keys, from_part, do_lt, special, ') -> ', inspect(value), inspect(bytes))
end

function M.cmd_termcodes_build_map(w)
  if not w:is_input_valid() then return end
  local abyte = string.byte('a', 1, 1)
  local s = ''
  for i = 0, 25 do
    local comba = fmt('<%s-%s>', 'c', string.char(abyte + i))
    local keys0 = vim.api.nvim_replace_termcodes(comba, true, false, true)
    s = s .. comba .. ' ' .. inspect { string.byte(keys0, 1, #keys0) } .. "\n"
  end

  for i = 0, 25 do
    local comba = fmt('<%s-%s>', 'a', string.char(abyte + i))
    local keys0 = vim.api.nvim_replace_termcodes(comba, true, false, true)
    s = s .. comba .. ' ' .. inspect { string.byte(keys0, 1, #keys0) } .. "\n"
  end

  for i = 0, 25 do
    local comba = fmt('<%s-%s-%s>', 'c', 'a', string.char(abyte + i))
    local keys0 = vim.api.nvim_replace_termcodes(comba, true, false, true)
    s = s .. comba .. ' ' .. inspect { string.byte(keys0, 1, #keys0) } .. "\n"
  end

  for i = 0, 12 do
    local comba = fmt('<f%d>', i)
    local keys0 = vim.api.nvim_replace_termcodes(comba, true, false, true)
    s = s .. comba .. ' ' .. inspect { string.byte(keys0, 1, #keys0) } .. "\n"
  end
  w:say(s)
end

--
--
--
---@param w Cmd4Lua
function M.cmd_set_cursor_pos(w)
  local lnum = w:tag('lnum'):desc('starts from 1'):pop():argn()
  local col = w:tag('col'):desc('starts from 0'):pop():argn()

  if not w:is_input_valid() then return end

  vim.api.nvim_win_set_cursor(0, { lnum, col })

  local i = vim.inspect
  print('win_get_pos:', i(vim.api.nvim_win_get_cursor(0)))
  print('getpos.:', i(vim.fn.getpos('.')))
end

--
-- experimental
--
---@param w Cmd4Lua
function M.cmd_visual_modes(w)
  if not w:is_input_valid() then return end
  -- to visual mode
  local keys = vim.api.nvim_replace_termcodes('<c-v>', true, false, true)
  vim.api.nvim_feedkeys(keys, 'v', false)
  vim.api.nvim_feedkeys('l', 'v', false)

  local i = vim.inspect
  print(i(vim.api.nvim_get_mode()))
  -- ?
end

return M

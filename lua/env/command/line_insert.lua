-- 08-11-2023 @author Swarg
local M = {}

-- to reload this module use: (put cursor to line and run :EnvLineExec <space>kl)
-- :EnvReload command EnvLineInsert

--
local ui = require('env.ui')
local fs = require('env.files')
local bu = require('env.bufutil')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local tviewer = require("env.util.tbl_viewer")
local utbl = require("env.util.tables")
local log = require('alogger')
local Context = require 'env.ui.Context'

local U = require("env.lang.utils.insertion")

-- state
M.insert_line_items = {}
M.insert_line_mappings = {}
M.insert_line_subscribers = {}
U.build_insertion(M, M.config)

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(":EnvLineInsert")

      :about('Like Code-action for current line and buff. based on file type')
      :about('Quick impl. simplest prototype code actions')

      :handlers(M)
      :default_cmd('insert')

      :desc('Insert item(pattern) to the current line')
      :cmd("insert", 'i')

      :desc('add own mappings for given lang (like type for @param)')
      :cmd("mappings", 'm')

      :desc('Edit items(patterns) for insertion into current line')
      :cmd("change-item", 'p')

      :desc('Swap indexes for two specified items')
      :cmd("swap-items", 's')

      :desc('Show list of all insetion snippets(items)')
      :cmd("list", 'ls')

      :desc('Show current inner state in new buffer')
      :cmd("state", 'S')

      :desc('rebuild insertion state (to update with runtime code reloading)')
      :cmd("rebuild")

      :desc('Insert words-samples by a given smaple name')
      :cmd('samples', 'sam')

      :run()
      :exitcode()
end

local _commands = {
  "insert", "mappings", "change-item", "swap-items", "state", "list", "rebuild",
}
M.opts = { nargs = "*", range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule

-- for reloading
function M.dump_state() U.dump_state(M) end

function M.restore_state() U.restore_state(M) end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

---@param index number
---@param ext string
---@return table
function M.get_context(index, ext, opts)
  local cbi = U.get_context(opts) -- bu.current_buf_info() ++

  cbi.ext = ext                   -- lang

  -- for dynamic substitution handler for this specific template(snippet)
  cbi.index = index
  cbi.mappings = M.insert_line_mappings[ext] or {}
  cbi.subscribers = M.insert_line_subscribers[ext] or {}
  cbi.errmsg = nil

  return cbi
end

-- extension from current buffer
--
---@return string
local function resolve_lang()
  local ext, cur_bufname = nil, vim.api.nvim_buf_get_name(0)
  if string.match(cur_bufname or '', 'Run Test: ') then
    ext = ui.buf_get_valueof(0, 'custom_filetype') or ''
  else
    ext = fs.extract_extension(cur_bufname)
    if not ext or ext == '' then
      ext = ui.get_interpreter() or ''
    end
  end
  return ext
end


--
-- :EvnLineInsert
-- Quick impl of interactive pickup and paste the code snippets
-- supports code snippet generation for non-modifiable buffer like Test Results
---@param w Cmd4Lua
function M.cmd_insert(w)
  local lang = w:desc('for given lang(ext)'):opt('--lang', '-l')
  local index = w:desc('index of the item in list'):optn('--index', '-i')
  local ls = w:desc('list of all available lang(file ext)'):has_opt('--list', '-L')

  if not w:is_input_valid() then return end

  -- show keys by lang (file extention e.g. langs)
  if ls then
    local line = ''
    for key, _ in pairs(M.insert_line_items) do
      line = line .. ' ' .. tostring(key)
    end
    print(line)
    return
  end

  local ext = lang or resolve_lang()
  local items = M.insert_line_items[ext] or M.insert_line_items.def
  local snippet, choise

  local label_fn = function(item)
    if #item > 50 then item = item:sub(1, 70) end
    return item:gsub('\n', '.')
  end

  local shared = U.shared_insertions

  if index then
    snippet = items[index]
  else -- interactive mode
    snippet, index, choise = ui.pick_one(items, "Select:", label_fn, true, shared)
    if log.is_debug() then
      log.debug("index:%s choise:%s snippet:'%s' shared_items:%s",
        index, choise, snippet, shared)
    end
  end

  if snippet and snippet ~= "" then
    local opts = w.vars.vim_opts or E
    local cbi = M.get_context(index, ext, opts)
    cbi.choise = choise

    -- log.debug('vmode:%s selection:%s, ln1:%s ln2:%s', visual_mode, cbi.selection,
    --   opts.line1, opts.line2)
    local n, hdl_name = nil, U.get_handler_name(snippet)

    if hdl_name then
      U.call_handler(cbi, snippet, hdl_name)
    else
      local cmd, argline = U.get_vim_command(snippet)
      if cmd and cmd ~= '' and argline then
        vim.cmd(':' .. cmd .. ' ' .. argline)
      else
        n = U.line_insert_str_to_cursor(cbi, snippet)
      end
    end

    if cbi.errmsg then
      return w:error(cbi.errmsg)
    end
    if cbi.visual_mode and type(n) == 'number' then
      bu.select_range(0, cbi.selection.line1, cbi.selection.line1 + n)
    end
  else
    log.debug('Not Found snippet for ', lang, index)
  end
end

--
-- Edit items(patterns) for insertion into current line
--
---@param w Cmd4Lua
function M.cmd_change_item(w)
  local lang = w:def(resolve_lang()):opt('--lang', '-l')
  local lnum = w:desc('the number of the item-pattern'):pop():argn()
  local add = w:desc('add new index'):has_opt('--add', '-a')
  local value = w:desc('string value to change a pattern'):opt('--value', '-v')

  if not w:is_input_valid() then return end
  ---@cast lnum number
  local ts = tostring

  local lang_items = M.insert_line_items[lang]
  if not lang_items then
    print('Not found items for lang: ' .. ts(lang))
    return
  end
  if not lang_items[lnum] then
    if add then
      if not value then
        return w:error('use --value to add new pattern for item ' .. ts(lnum))
      end
    else
      return w:error('Not found item idx: ' .. ts(lnum) .. ' lang:' .. ts(lang))
    end
  elseif not add then
    return w:error('Item already exists index: ' .. ts(lnum))
  end

  if not value then
    print(lang_items[lnum]) -- just show not edit
    --
  elseif type(value) == 'string' and value ~= '' then
    value = value:gsub('\\n', "\n")
    lang_items[lnum] = value
    print('changed index:', lnum, 'pattern:', value)
  else
    return w:error('expected value is not an empty string got: ' ..
      type(value) .. ' |' .. ts(value) .. '|')
  end
end

-- todo separate cmd to reload command handler
function M.hot_self_reload()
  vim.api.nvim_del_user_command('EnvLineInsert')
  package.loaded["env.command.line_insert"] = nil
  require("env.util.commands")
      .reg_handler('EnvLineInsert', "env.command.line_insert")
end

--
-- Swap indexes for two specified items
--
---@param w Cmd4Lua
function M.cmd_swap_items(w)
  local lang = w:desc('lang extension'):def(resolve_lang()):opt('--lang', '-l')
  local i1 = w:desc('index1'):pop():argn()
  local i2 = w:desc('index2'):pop():argn()

  w:v_opt_dry_run('-d')

  if not w:is_input_valid() then return end ---@cast i1 number
  ---@cast i2 number
  if i1 == i2 then
    return print('sure? swap ' .. tostring(i1) .. ' and ' .. tostring(i2))
  end

  -- staticaly part of the snippets(the main and essential part of mechanics)
  local m = M.insert_line_items[lang or false]
  if not m then
    return w:error('Nof Found Lang: ' .. tostring(lang))
  end

  -- handlers - a dynamic part of the snippet (optional).
  -- this is optional part. inserting snippets can work without it.
  local m2 = M.insert_line_mappings[lang] or {}
  local m3 = M.insert_line_subscribers[lang] or {}

  if not m[i1] then return print('Not Found item: ' .. tostring(i1)) end
  if not m[i2] then return print('Not Found item: ' .. tostring(i2)) end

  if w:is_dry_run() then
    print('Lang: ', lang, 'has:', m ~= nil)
    print('1:', i1, (m[i1] or ''):gsub('\n', '.'), 'subs:', m2[i1], m3[i1])
    print('2:', i1, (m[i2] or ''):gsub('\n', '.'), 'subs:', m2[i2], m3[i2])

    return
  end

  local function swap(map)
    local tmp = map[i1]
    map[i1] = map[i2]
    map[i2] = tmp
  end
  swap(m); swap(m2); swap(m3)

  print("swapped " .. tostring(i1) .. " with " .. tostring(i2))
end

--
-- Show list of all insetion snippets(items)
--
---@param w Cmd4Lua
function M.cmd_list(w)
  local lang = w:desc('lang extension'):def(resolve_lang()):opt('--lang', '-l')
  local s = w:desc('start index in range'):def(1):optn('--start-index', '-s')
  local e = w:desc('end index in range'):def(-1):optn('--end-index', '-e')

  if not w:is_input_valid() then return end

  local m = M.insert_line_items[lang or false]
  if not m then
    return w:error('Nof Found Lang: ' .. tostring(lang))
  end

  if e < 0 then e = #m + e + 1 end
  if s < 0 then s = #m + s + 1 end

  if s > e then
    return print('bad range to show', s, e)
  end

  local res = 'Lines Range: ' .. tostring(s) .. ' - ' .. tostring(e) .. "\n"
  for i = s, e do
    res = res .. string.format("%2d  %s\n", i, (m[i] or ''):gsub("\n", '.'))
  end

  print(res)
end

--
-- Show current inner state in new buffer
-- [Debugging]
--
---@param w Cmd4Lua
function M.cmd_state(w)
  if not w:is_input_valid() then return end

  local obj = {
    module = M, -- has M.config inside
    cmd4lua_vars = w.vars,
    vim_mode = vim.api.nvim_get_mode(),
    getpos_v = vim.fn.getpos("v"),
  }
  local ctx = { state = tviewer.wrap2state(obj, 'LineInsertState') }
  tviewer.create_buf(ctx, ctx.state.src or 'table-viewer')
end

--
-- rebuild insertion state (to update with runtime code reloading)
--
---@param w Cmd4Lua
function M.cmd_rebuild(w)
  w:v_opt_quiet('-v')
  if not w:is_input_valid() then return end

  U.build_insertion(M, M.config)
  w:say('insertion rebuilded.')
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

local MM = {}

--@param w Cmd4Lua
function M.cmd_mappings(w)
  w:handlers(MM)
      :desc('Inspect mappings as lua table')
      :cmd("inspect", "i")

      :desc('Save mappings to file')
      :cmd("save", "s")

      :desc('Save mappings from file')
      :cmd("load", "l")

      :desc('View insertion-item for specified lang and index')
      :cmd("get-item", "g")

      :desc('Set insertion-item')
      :cmd("set-item", "si")

      :desc('Edit specified item mappings as lua-table')
      :cmd("edit", "e")

      :desc('rebuild mappings from default + current config')
      :cmd("rebuild", "rb")

      :run()
end

---@param w Cmd4Lua
local function define_mappings_opts(w)
  local lang = w:def(resolve_lang()):opt('--lang', '-l')
  local lnum = w:tag('lnum'):pop():argn()
  return lang, lnum
end

local function get_mappings_or_error(w, lang, lnum)
  local lang_lines = M.insert_line_mappings[lang]
  if not lang_lines then
    return w:error('Not Found lines for lang:' .. v2s(lang))
  end
  local mappings = lang_lines[lnum]
  if not mappings then
    return w:error(fmt('Not Found mappings for n:%s lang:%s', v2s(lnum), v2s(lang)))
  end
  return lang_lines, mappings
end

local function key_value(mappings, key_word)
  local replace = tostring(U.map_word(mappings, key_word))
  return '[' .. tostring(key_word) .. ']: ' .. replace
end

--
-- View value for specified insert-item mappings
--
-- mappings line-number pattern
---@param w Cmd4Lua
function MM.cmd_get_item(w)
  local lang, lnum = define_mappings_opts(w)
  local key = w:opt('--key', '-k')

  if not w:is_input_valid() then return end

  local lang_lines, mappings = get_mappings_or_error(w, lang, lnum)
  if w:has_error() then return end

  if not key or not mappings then
    w:fsay('for lang %s has items: %s for idx:%s has mappings: %s',
      v2s(lang), #(lang_lines or E), lnum, mappings ~= nil)
  else
    w:say(key_value(mappings, key))
  end
end

-- change insertion entry for given lang and index
function MM.cmd_set_item(w)
  local lang, lnum = define_mappings_opts(w)
  local key = w:required():opt('--key', '-k')
  local value = w:required():opt('--value', '-v')

  if not w:is_input_valid() then return end

  local _, mappings = get_mappings_or_error(w, lang, lnum)
  if w:has_error() then return end

  assert(key, 'has key')
  assert(value, 'has value')

  ---@diagnostic disable-next-line: param-type-mismatch
  local f = U.update_mappings(mappings, key, value)
  print('updated: ' .. tostring(f) .. ', ' .. key_value())
end

MM.def_tmp_file = '/tmp/insert_mappings.lua'

--
-- Save mappings to file
--
---@param w Cmd4Lua
function MM.cmd_inspect(w)
  local lang, lnum = define_mappings_opts(w)
  if not w:is_input_valid() then return end

  local _, mappings = get_mappings_or_error(w, lang, lnum)
  if w:has_error() then return end

  print(require "inspect" (mappings))
end

---@param code string code
local function code2mappings(w, lang, lnum, code)
  local ok, t = utbl.luacode2table(code)
  if not ok or type(t) ~= 'table' then
    w:error(v2s(t))
    return nil
  end

  assert(type(lnum) == 'number', 'lnum')
  M.insert_line_mappings[lang][lnum] = t
  return t
end

---@return string?
local function mappings2code(w, lang, lnum)
  local _, mappings = get_mappings_or_error(w, lang, lnum)

  if mappings then
    local inspect = require "inspect"
    local code = inspect(mappings)
    local comment = fmt("-- %s %s\n", lang, lnum)
    return comment .. code
  end
end

--
-- Save mappings to file
--
---@param w Cmd4Lua
function MM.cmd_save(w)
  local lang, lnum = define_mappings_opts(w)
  local fn = w:desc('filename from which load')
      :def(MM.def_tmp_file):opt('--file', '-f')

  if not w:is_input_valid() then return end

  local code = mappings2code(w, lang, lnum)
  if w:has_error() then return end ---@cast code string

  if fs.file_exists(fn) then
    if not ui.ask_confirm_and_do('Sure rewrite file ' .. v2s(fn) .. '?') then
      return w:say('canceled')
    end
  end

  local saved = fs.str2file(fn, code, 'w')
  w:say('saved:', saved, fn)
end

--
-- Save mappings from file
--
---@param w Cmd4Lua
function MM.cmd_load(w)
  local lang, lnum = define_mappings_opts(w)
  local fn = w:desc('filename from which load')
      :def(MM.def_tmp_file):opt('--file', '-f')

  if not w:is_input_valid() then return end

  -- check is exists mappings for given lang+lnum
  get_mappings_or_error(w, lang, lnum)
  if w:has_error() then return end

  local code = fs.read_all_bytes_from(fn)
  if not code then
    return w:error('cannot load:' .. v2s(fn))
  end

  local t = code2mappings(w, lang, lnum, code)

  w:say('loaded mappings for ', lang, lnum, 'keys:', utbl.tbl_keys_count(t))
end

local function split(line, sep)
  sep = sep or "\n"
  local t = {}
  for str in string.gmatch(line, '([^' .. sep .. ']+)') do
    table.insert(t, str)
  end
  return t
end

--
-- Add id field to lua-table
--
---@param w Cmd4Lua
function MM.cmd_edit(w)
  local lang, lnum = define_mappings_opts(w)

  if not w:is_input_valid() then return end

  local code = mappings2code(w, lang, lnum)
  if w:has_error() then return end

  local StatefulBuf = require("env.draw.ui.StatefulBuf")
  local name = fmt('mappigns-%s-%s', v2s(lang), v2s(lnum))
  local lines = split(code, "\n")

  local dispatch = {
    ---@param t table{buf, event}
    on_write = function(t)
      local lines0 = vim.api.nvim_buf_get_lines(t.buf, 0, -1, false)
      local code0 = table.concat(lines0, "\n")
      code2mappings(w, lang, lnum, code0)
    end
  }
  StatefulBuf.create_buf(name, lines, dispatch)
end

--
-- rebuild mappings from default + current config
MM.cmd_rebuild = M.cmd_rebuild


--
-- Insert samples
-- an alternative way to force filler words(samples) to be inserted
-- :LineInsert 0
--
---@param w Cmd4Lua
function M.cmd_samples(w)
  local samples = require 'env.lang.utils.samples'

  local name = w:desc('sample name'):pop():arg()
  local format = w:desc('output format'):opt('--format', '-f')
  local items = w:desc('items'):opt('--items', '-i')
  local extra = w:desc('extra'):opt('--extra', '-e')


  if not w:is_input_valid() then return end
  ---@cast name string

  if name == '.' then
    local ok, err = samples.handler_add_sample() -- from current line
    if not ok then
      return w:error(err)
    end
    return
  end

  local lines, err = samples.do_add_sample(name, format, items, extra)
  if not lines then
    return w:error(tostring(err))
  end

  Context:new():insertBeforeCurrentLine(lines)
end

return M

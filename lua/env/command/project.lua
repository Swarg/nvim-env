-- 14-07-2024 @author Swarg
--

local log = require 'alogger'
local class = require 'oop.class'
local c4lv = require 'env.util.cmd4lua_vim'
local cu = require 'env.util.commands'
local fs = require 'env.files'
local utbl = require 'env.util.tables'

-- local env = require 'env' -- TODO remove !

local Lang = require 'env.lang.Lang'
local eclipse = require 'env.bridges.eclipse'
local pcache = require 'env.cache.projects'
local ucmd = require 'env.lang.utils.command'
local nvim_lsp = require("env.util.nvim_lsp")

local M, L, ELT = {}, {}, {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- dynamicly generated based on current opened file
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvProject')
      :about('Interact with Projects')
      :handlers(M)

      :desc('generate new project')
      :cmd('new', 'n')

      :desc('open already existed project')
      :cmd('open', 'o')

      :desc('close already opened project')
      :cmd('close', 'c')

      :desc('show list of all opened projects')
      :cmd('list', 'ls')

      :desc('show information about current project')
      :cmd('info', 'i')

      :desc('build current project')
      :cmd('build', 'b')

      :desc('run current project')
      :cmd('run', 'r')

      :desc('reload already opened project(the BuildSystem config file)')
      :cmd('reload', 'R')

      :desc('apply code changes in current file (for code worked inside vm)')
      :cmd('hotswap', 'hs')

      :desc('run test of the current project')
      :cmd('test', 't')

      :desc('debuggin current project')
      :cmd('debug', 'd')

      :desc('read per-dir settings')
      :cmd('dir-conf', 'D')

      :desc('interact with lsp-language server')
      :cmd('lsp', 'l')

      :desc('intefact with embeded lua tests "subproject"')
      :cmd('embeded-lua-tests', 'elt')

      :run():exitcode()
end

--
-- interact with lsp-language server
-- :EnvProject lsp
--
---@param w Cmd4Lua
function M.cmd_lsp(w)
  w:about('Language Server Protocol')
      :handlers(L)

      :desc('information about lsp-server for current bufname')
      :cmd('info', 'i')

      :desc('restart lsp-server')
      :cmd('restart', 'r')

      :desc('edit lsp settings for current project')
      :cmd('settings', 's')

      :run()
end

--
-- intefact with embeded lua tests "subproject"
--
---@param w Cmd4Lua
function M.cmd_embeded_lua_tests(w)
  w:handlers(ELT)

      :desc('resolve current opened _spec.lua file to Setup EmbededLuaTests')
      :cmd("open", 'o')

      :run()
end

local _commands = {
  "new", "info", "build", "run", "test", "debug", "lsp", "dir-conf",
  "open", "close"
}
M.opts = { nargs = '*', range = false, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule

-- for reloading
function M.dump_state()
  eclipse.dump_state()
  pcache.dump_state()
  Lang.dump_state()
end

function M.restore_state()
  eclipse.restore_state()
  pcache.restore_state()
  Lang.restore_state()
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local get_current_context = ucmd.get_current_context
local is_java_project = ucmd.is_java_project
local log_debug = log.debug
local className = class.name




--
-- generate a new project for given language
-- this function each time generate the list of languages that already
-- registered in env.lang.Lang class
-- actual handlers for each lang see in env.langs.<PL>.command.new_project
--
---@param w Cmd4Lua
function M.cmd_new(w)
  local map = Lang.getRegisteredLangs()
  if not next(map) then
    return w:say('No supported languages')
  end

  local busy, langs = {}, {}
  for name, lang_class in pairs(map) do
    local short
    short = string.sub(name, 1, 1)
    if busy[short] then short = nil else busy[short] = true end

    log_debug('lang_class', className(lang_class))
    local handler = assert(lang_class.cmdNewProject, 'cmdNewProject')

    if type(handler) == 'function' and not langs[lang_class] then
      w:desc('Create new Project for language: ' .. class.name(lang_class))
      w:cmd(name, short, function(w0) handler(w0, lang_class) end)
    end

    langs[lang_class] = true
  end

  w:run()
end

--
-- open already existed project
--
---@param w Cmd4Lua
function M.cmd_open(w)
  w:about('open a project for the current file')
  w:usage(':EnvProject open --markers [.git Makefile pom.xml mix.exs] --lsp')
  local cwd = ucmd.get_current_directory()

  local dir = w:desc('start lsp'):defOptVal(cwd):opt('--directory', '-d')
  local with_lsp = w:desc('start lsp'):has_opt('--lsp', '-l')
  local markers = w:desc('project root markers'):optl('--merkers', '-s')
  -- local

  if not w:is_input_valid() then return end

  local ctx = ucmd.get_current_context()
  local root_dir, proj_name

  if dir then
    if dir == '..' then
      dir = fs.get_parent_dirpath(cwd)
    end
    print("directory as project_root:", dir)
    root_dir = dir
  else
    if #(markers or E) < 0 then
      return w:error('cannot find directory without markers')
    end ---@cast markers table

    root_dir = pcache.find_root(markers, ctx.bufname)
    print("found by markers", root_dir)
  end

  if not with_lsp then
    -- temp workaround with issue: goto-definition and jdtls-settings
    --  my settings for nvim  and goto-definition jump over stdlib with error
    --  this code ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/settings/jdtls.lua
    --  uses pcache to determine projects roots
    --  - charset encoding issue
    if root_dir and root_dir ~= '' then
      local added = pcache.add_project_root(root_dir)
      w:fsay('add root_dir to cached project-roots %s %s', added, root_dir)
    end
  end

  if is_java_project(ctx) then
    w:say('Run jdtls for', ctx.bufname)
    proj_name = eclipse.run_jdtls(ctx.bufname)
  end

  w:say('Project Name: ' .. v2s(proj_name))
end

local function show_opened_projects_list(w)
  local dirs = utbl.sorted_keys(Lang.getActiveProjects(), nil)
  if #(dirs or E) == 0 then
    return w:say('No Opened Projects')
  end

  local s = ''
  for i, dir in ipairs(dirs) do
    s = s .. fmt("%2s  %s\n", i, dir)
  end
  if w:is_verbose() then
    s = s .. "\nCached:\n" .. table.concat(pcache.get_all_roots(), "\n")
  end
  return w:say(s)
end

--
-- close already opened project
--
---@param w Cmd4Lua
function M.cmd_close(w)
  w:usage(':EnvProject close --list')
  w:v_opt_verbose('-v')
  local a = w:desc('root_dir or index in the projects list'):optional():pop():arg()
  local ls = w:desc('show list of all opened projects'):has_opt('--list', '-ls')

  if not w:is_input_valid() then return end

  local project_root, n = a, tonumber(a)

  if n ~= nil and n > 0 then
    -- select project root via index in opened projects list
    project_root = (utbl.sorted_keys(Lang.getActiveProjects(), nil) or E)[n]
  end

  if ls or a == nil or (n ~= nil and n <= 0) or project_root == nil then
    return show_opened_projects_list(w)
  end

  if project_root == '.' then
    project_root = fs.cwd()
  elseif project_root == '..' then
    project_root = fs.get_parent_dirpath(fs.cwd())
  end

  if type(project_root) ~= 'string' or project_root == '' then
    return w:error('not found project for given arg:' .. v2s(a))
  end

  local closed = Lang.closeProject(project_root) -- also clear in pcache
  w:say('Project Closed ', closed, project_root)
end

--
-- show list of all opened projects
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:usage('to see via TableViewer use :EnvLang inspect')
  if not w:is_input_valid() then return end
  local t = Lang.getActiveProjects()
  local s = ''
  for dir, lang in pairs(t) do
    s = s .. fmt("%16s %s\n", className(lang), dir)
  end
  if s == '' then s = 'empty' end
  w:say(s)
end

--
-- show information about current project
--
---@param w Cmd4Lua
function M.cmd_info(w)
  w:v_opt_verbose('-v')
  w:def(false):v_has_opt('--resolve-on-cache', '-RNC')
  local lang = Lang.getLangByCtx(w, true, false, false)

  if not w:is_input_valid() then return end

  if not lang then return end -- ! exit with errmsg

  local builder = lang:getProjectBuilder()
  -- if the instance-type type is `temporary`, it means that
  -- there are no open projects for this context
  local itype = string.upper(Lang.getInstanceType(lang))
  local meaning = ''
  if itype == 'TEMPORARY' then
    meaning = '  -  No Any Opened Projects for this file'
  end
  local project_root = lang:getProjectRoot()
  local lsp_active = lang:isIgnoredByLsp()

  local info =
      fmt("Lang             : %s\n", className(lang)) ..
      fmt("    instance-type: %s%s\n", itype, meaning) ..
      fmt("    .src         : %s\n", lang.src) ..
      fmt("    .test        : %s\n", lang.test) ..
      fmt("    .project_root: %s\n", project_root) ..
      fmt("    alrd_existed : %s\n", lang.already_existed) ..
      fmt("       no-markers: %s\n", Lang.isProjectWithoutMarkers(project_root)) ..
      fmt("       lsp-active: %s\n", lsp_active) ..
      "\n" ..
      fmt("LangGen          : %s\n", className(lang:getLangGen())) ..
      fmt("Builder          : %s\n", className(builder))

  if builder then
    local apr = builder['actual_project_root']
    if apr then apr = '(actual:' .. apr .. ')' end -- for LuaTester
    info = info ..
        fmt("    flatStructure: %s\n", builder:isFlatStructure()) ..
        fmt("    rawSnippets  : %s\n", builder:isRawSnippets()) ..
        fmt("    .project_root: %s%s\n", builder:getProjectRoot(), apr) ..
        fmt("    .buildscript : %s\n", builder:getBuildScriptPath()) ..
        fmt("    .src         : %s\n", builder.src) ..
        fmt("    .test        : %s\n", builder.test) ..
        fmt("    .settings    : %s\n", type(builder.settings))
  end

  local dir = lang.project_root or ucmd.get_current_directory()
  local list = Lang.getProjectsWithoutMarkers(dir)
  info = info ..
      fmt("Projects without markers in %s (%s): \n", v2s(dir), #(list or E)) ..
      table.concat((list or E), " ")

  w:say(info)

  if w:is_verbose() then
    local inspect = require "inspect"
    w:say("Builder settings:")
    w:say(inspect((builder or E).settings))
    --
    w:say(builder:getErrLogger():getReadableReport("Builder Errors:"))
    w:say(lang:getLangGen():getErrLogger():getReadableReport("LangGen Errors:"))
  end

  -- todo java
  --[[
  --
  local eclipse = require 'env.bridges.eclipse'
  -- use case:  show jdtls_info
  if project_root then
    project_name = M.get_full_project_name(project_root)
    eclipse_files = eclipse.project_info(project_root)
  end

  local append = ui.buf_append_lines
  append(bufnr, { 'project_root: ' .. (project_root or '?') })
  append(bufnr, { 'project_name: ' .. (project_name or '?') })
  append(bufnr, { 'eclipse_files: ' .. (eclipse_files or '?') })
  ]]
end

local function define_clean_task_opt(w)
  w:desc('clean task for builder'):tag('clean_task'):v_has_opt('--clean', '-c')
end
--
-- build(compile) current project
--
---@param w Cmd4Lua
function M.cmd_build(w)
  define_clean_task_opt(w)
  local lang = Lang.getLangByCtx(w)

  if not w:is_input_valid() then return end

  if not lang then return end -- ! exit with errmsg

  -- env.build_project(w.vars.vim_opts)
  w:say(lang:buildProject(w.vars))
end

--
-- run current project
--
---@param w Cmd4Lua
function M.cmd_run(w)
  define_clean_task_opt(w)
  local lang = Lang.getLangByCtx(w)

  if not w:is_input_valid() then return end
  if not lang then return end -- ! exit with errmsg

  w:say(lang:runProject(w.vars))
end

--
-- reload already opened project(the BuildSystem config file)
--
---@param w Cmd4Lua
function M.cmd_reload(w)
  local lang = Lang.getLangByCtx(w)

  if not w:is_input_valid() then return end
  if not lang then return end

  eclipse.jdtls_update_project_config(lang.project_root)
  lang:reloadBuildSystemConfig()
end

--
-- apply code changes in current file (for code worked inside vm)
--
---@param w Cmd4Lua
function M.cmd_hotswap(w)
  local lang = Lang.getLangByCtx(w)

  if not w:is_input_valid() then return end
  if not lang then return end
  -- JdtUpdateHotcode
  local bufname = vim.api.nvim_buf_get_name(0)
  -- if eclipse.is_active_project(lang.project_root) then
  --   needs dap session
  --   print("call :JdtUpdateHotcode ...")
  --   local ok, ret = pcall (vim.cmd, "JdtUpdateHotcode")
  --   if not ok then
  --     print(ret)
  --   else
  --     return
  --   end
  -- end
  -- just compile and send current file into jvm or supported servlet-container
  lang:hotswap(bufname)
end

--
-- run test of the current project
--
---@param w Cmd4Lua
function M.cmd_test(w)
  if not w:is_input_valid() then return end
  error('Not implemented yet')
  -- env.project_test
end

--
-- debuggin current project
--
---@param w Cmd4Lua
function M.cmd_debug(w)
  if not w:is_input_valid() then return end
  -- env.debugging_project(w.vars.vim_opts) -- todo, now only gradle for java project
end

--
-- read per-dir settings
--
---@param w Cmd4Lua
function M.cmd_dir_conf(w)
  local bufname = ucmd.get_current_context().bufname
  local path = w:desc('path'):optional():def(bufname):pop():arg()
  local nocache = w:desc('read without caching'):has_opt('--no-cache', '-C')

  if not w:is_input_valid() then return end ---@cast path string

  if path == '.' then path = os.getenv('PWD') or '' end

  local conf = pcache.readDirConf(path, nocache)

  print(path, require "inspect" (conf))
end

--
--
--------------------------------------------------------------------------------
--                            LSP
--------------------------------------------------------------------------------


--
-- information about lsp-server for current bufname
--
---@param w Cmd4Lua
function L.cmd_info(w)
  if not w:is_input_valid() then return end

  error('Not implemented yet')
end

--
-- restart lsp-server
--
---@param w Cmd4Lua
function L.cmd_restart(w)
  w:usage(':EnvProject lsp restart')

  if not w:is_input_valid() then return end

  local ctx = get_current_context()

  if is_java_project(ctx) then
    if nvim_lsp.stop_lsp(ctx.bufnr, "jdtls") then
      nvim_lsp.echohl("Waiting to stop jdtls...")
      nvim_lsp.wait_to_stop(ctx.bufnr, "jdtls")
    end

    nvim_lsp.echohl('Running... jdtls for ' .. v2s(ctx.bufname))
    local proj_name = eclipse.run_jdtls(ctx.bufname)
    nvim_lsp.echohl('Project name: ' .. v2s(proj_name))
    --
  else
    w:error('not implemented for ' .. v2s(ctx.ext))
  end
end

--
-- edit lsp settings for current project
--
---@param w Cmd4Lua
function L.cmd_settings(w)
  w:usage(':EnvProject lsp settings')

  if not w:is_input_valid() then return end

  local ctx = get_current_context()
  local path, root_dir
  if is_java_project(ctx) then
    path, root_dir = eclipse.get_project_settings_path(ctx.bufname)
  end

  if path and path ~= '' then
    if fs.is_file(path) then
      vim.cmd(":e " .. path)
    else
      w:error('not found file: ' .. v2s(path))
    end
  elseif path == false then
    w:error('not found for project_root: ' .. v2s(root_dir))
  else
    w:error('not implemented for ' .. v2s(ctx.ext))
  end
end

--
-- Resolve current opened _spec.lua file to Setup Embeded LuaTests
-- To support the ability to write Lua tests for other programming languages
-- it was necessary until I made automatic recognition of the case of opening a
-- Lua file inside a project with a different programming language.
-- at the moment, the lua_ls configuration are updated automatically and
-- configured through the config and the function call
-- env.update_lsp_settings_on_attach (and checkEmbededLuaTestsCase)
--
---@param w Cmd4Lua
function ELT.cmd_open(w)
  w:usage(':EnvProject embeded-lua-tests open')
  if not w:is_input_valid() then return end

  local ctx = get_current_context()

  if not string.match(ctx.bufname, "%.lua$") then
    w:error('Expected test file with *.lua extension')
  end

  local project_root = pcache.find_root_in_cache(nil, ctx.bufname)
  if not project_root then
    return w:error('not found cached project_root')
  end
  local subproj_root = (pcache.projects_cache[project_root] or E).embeded
  if (subproj_root) then
    return w:error('already opened for: ' .. v2s(subproj_root))
  end

  local LuaLang = require 'env.langs.lua.LuaLang'
  local instance = LuaLang:new()
      :setProjectRoot(project_root)
      :findProjectBuilder(ctx.bufname) -- recache inside
  ---@cast instance env.langs.lua.LuaLang

  if not instance.builder or instance.project_root == project_root then
    -- todo or already opened
    return w:error('Not found Embeded LuaTests ' .. v2s(class.name(instance)))
  end

  local cached = (Lang.getActiveProjects() or E)[instance.project_root]
  w:say('cached: ', class.name(cached), instance.project_root)
end

return M

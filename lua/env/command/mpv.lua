-- 25-05-2024 @author Swarg
-- Goals:
--  - interact with mpv via input-ipc-server
--

local log = require 'alogger'
local fs = require 'env.files'
local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'
local ucb = require 'env.util.clipboard'
local vtt = require 'env.langs.vtt.snippets'
local mpv = require 'env.bridges.mpv'
local uvtt = require 'env.langs.vtt.utils'
local utts = require 'env.util.tts'
local Translator = require 'env.translate.Translator'

-- for keybindings TODO move to luarock-package
local ubuf = require 'env.draw.utils.buf'

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
--
-- subcmd mpv
-- builtin client to interact with the mpv through input-ipc-server
--
---@param opts table
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvMpv')
      :about('mpv input-ipc-server')
      :handlers(M)

      :desc('input-ipc-server to interact with mpv via socket')
      :cmd('ipc', 'i') -- connect disconnect status pull-msgs

      :desc('interact with subtitles, edit, jump, sync, etc')
      :cmd('subs', 's')

      :desc('remote control of a ipc-pre-connected mpv instance')
      :cmd('ctl', 'c')

  -- researching and debugging
      :desc('The list of current input key bindings inside mpv.')
      :cmd('input-bindings', 'ib')

      :desc('The list of current input key bindings inside nvim vtt buffer.')
      :cmd('key-bindings', 'kb')

      :desc('text-to-speech')
      :cmd('tts', 't')

      :desc('generate command to create overlay for video defined in vtt file')
      :cmd('overlay', 'o')

      :run()
      :exitcode()
end

local commands = {
}
-- options to register this command in vim
M.opts = { nargs = "*", range = true, complete = cu.mk_complete(commands) }

local I = {}
function M.cmd_ipc(w)
  w:about('mpv input-ipc-server')
      :handlers(I)

      :desc('connect to mpvctl socket')
      :cmd('connect', 'c')

      :desc('disconnect from mpvctl socket')
      :cmd('disconnect', 'd')

      :desc('status of mpvctl connection')
      :cmd('status', 'st')

      :desc('(experimental) manyaly pull-messages from mpv')
      :cmd('pull-messages', 'pm')

      :run()
end

local C = {}
function M.cmd_ctl(w)
  w:about('remote contol of mpv instance')
      :handlers(C)

      :desc('get current filename opened in the mpv ')
      :cmd('filename', 'fn')

      :desc('get mpv working directory')
      :cmd('working-directory', 'wd')

      :desc('get current time-pos of the video playing in the mpv')
      :cmd('timepos', 't')

      :desc('stop(pause=true) playing')
      :cmd('stop', 's')

      :desc('resume playing video from pause')
      :cmd('play', 'p')

      :desc('seek time-pos')
      :cmd('seek', 'sk')

      :desc('continut playing from pause')
      :cmd('toggle-pause', 'tp')

  -- advanced
      :desc('send mpv command get_property with given args')
      :cmd('get-property', 'gp')

      :desc('The list of top-level properties.')
      :cmd('property-list', 'pls')

      :desc('The list of top-level properties.')
      :cmd('command-list', 'cls')

      :desc('run mpv command with args')
      :cmd('run', 'r')

      :run()
end

local S = {}
function M.cmd_subs(w)
  w:about('subtitles')
      :handlers(S)

      :desc('Reload the given subtitle tracks or the current one')
      :cmd('reload', 'r')

      :desc('open subtitles of the currently playing video')
      :cmd('open', 'o')

      :desc('Sync play time-pos in mpv to subtitles entry under the cursor')
      :cmd('sync-timepos-to-lnum', 't2l')

      :desc('Sync play time-pos in mpv to subtitles entry under the cursor')
      :cmd('sync-lnum-to-timepos', 'l2t')

      :desc('Get current time-pos from mpv and put it into end-timestamp in line')
      :cmd('set-end-time-from-mpv-timepos', 'efm')

      :desc('Get current time-pos from mpv and put it into start-timestamp')
      :cmd('set-start-time-from-mpv-timepos', 'sfm')

  -- interact with already opened subtitles-file in nvim buffer
      :desc('Sync current and next timestamps in already opened subtitles-file')
      :cmd('sync-two-timestamps', 'stt')

      :desc('Join current entry with next in already opened subtitles-file')
      :cmd('join-two-to-one', 'j')

      :desc('move the right part from cursor from current line into next entry')
      :cmd('move-right-to-next', 'mr')

      :desc('move the left part from cursor from current line into prev entry')
      :cmd('move-left-to-prev', 'ml')

      :desc('translate sentence')
      :cmd('translate', 't')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------
local DEF_SEEK_STEP = 4
local SYNC_TIME_ON_MOVE = true

--                    Implementation of IStatefullModule
--                       for runtime code reloading

function M.dump_state() mpv.dump_state() end

function M.restore_state() mpv.restore_state() end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
-- text-to-speech
--
---@param w Cmd4Lua
function M.cmd_tts(w)
  require("env.command.mpv.tts").handle(w, M.config)
end

--
-- generate command to create overlay for video defined in vtt file
--
---@param w Cmd4Lua
function M.cmd_overlay(w)
  require 'env.command.mpv.overlay'.handle(w, M.config)
end

---@param w Cmd4Lua
local function define_shared_opts(w)
  w:v_opt_quiet('-q')
  w:v_opt_quiet('-v')

  w:desc('number or unix-socket-filename of mpv'):def('0')
      :v_opt('--socket-file-name', '-s')

  w:desc('reconnect to mvp socket')
      :v_has_opt('--reconnect', '-c')
end

---@param w Cmd4Lua
local function is_connected(w)
  if w.vars.reconnect then
    mpv.connect_to_mpv(w.vars.socket_file_name, true)
  end
  local connected = mpv.is_connected()
  if not connected then
    w:error('not connected to mpv.')
  end
  return connected
end

--
-- connect to mpvctl socket
--
---@param w Cmd4Lua
function I.cmd_connect(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end

  local ok, errmsg = mpv.connect_to_mpv(w.vars.socket_file_name)
  if not ok then w:error(errmsg) else w:say(errmsg) end
end

--
-- disconnect from mpvctl socket
--
---@param w Cmd4Lua
function I.cmd_disconnect(w)
  w:v_opt_quiet('-q')
  if not w:is_input_valid() then return end
  if mpv.is_connected() then
    local ok, fn = mpv.shutdown_connection()
    w:say('disconnected:', ok, fn)
  else
    w:say('not connected yet')
  end
end

--
-- status of mpvctl connection
--
---@param w Cmd4Lua
function I.cmd_status(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.status(w:is_verbose()))
  end
end

--
-- pull-messages from mpv ipc
--
---@param w Cmd4Lua
function I.cmd_pull_messages(w)
  define_shared_opts(w)
  local timeout = w:desc('timeout in ms'):def(1000):pop():argn()

  if not w:is_input_valid() then return end
  ---@cast timeout number
  timeout = timeout or 1000
  if is_connected(w) then
    local lines = mpv.read_socket_to_lines(timeout, {}, nil)
    w:say(vim.inspect(lines))
  end
end

--------------------------------------------------------------------------------
--                          Remote Control

--
-- stop(pause=true) playing
--
---@param w Cmd4Lua
function C.cmd_stop(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.mpv_pause())
  end
end

--
-- continut playing from pause
--
---@param w Cmd4Lua
function C.cmd_play(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.mpv_play())
  end
end

---@param w Cmd4Lua
function C.cmd_toggle_pause(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.mpv_toggle_pause())
  end
end

--
-- seek time-pos
--
---@param w Cmd4Lua
function C.cmd_seek(w)
  define_shared_opts(w)
  local step = w:desc('step to seek (negative values to seek back)')
      :def(-2):pop():argn()

  if not w:is_input_valid() then return end
  ---@cast step number

  if is_connected(w) then
    w:say(mpv.mpv_seek(step))
  end
end

--
-- get current filename opened in the mpv
-- -- notice -c used to reconnect, -C to copy
--
---@param w Cmd4Lua
function C.cmd_filename(w)
  define_shared_opts(w)
  local full = w:desc('full path or basename'):def('full'):pop():arg()
  local copy = w:desc('copy path to system clipboard'):has_opt('--copy', '-C')

  if not w:is_input_valid() then return end

  if is_connected(w) then
    local fn = mpv.cmd_get_payling_filename(full)
    if copy then
      ucb.copy_to_clipboard(v2s(fn))
      return w:say('copied: ' .. v2s(fn))
    end
    w:say(fn)
  end
end

--
-- get mpv working directory
--
---@param w Cmd4Lua
function C.cmd_working_directory(w)
  define_shared_opts(w)
  local copy = w:desc('copy path to system clipboard'):has_opt('--copy', '-C')

  local open = w:desc('open work-dir in the nvim'):has_opt('--open', '-o')

  if not w:is_input_valid() then return end

  if is_connected(w) then
    local wd = mpv.get_working_directory()
    if open and type(wd) == 'string' then
      if fs.dir_exists(wd) then
        vim.cmd(':e ' .. v2s(wd))
        return w:say('opened: ' .. v2s(wd))
      else
        return w:error('not found dir: ' .. v2s(wd))
      end
    end
    if copy then
      ucb.copy_to_clipboard(v2s(wd))
      return w:say('copied: ' .. v2s(wd))
    end
    w:say(wd)
  end
end

--
-- get current time-pos of the video playing in the mpv
--
---@param w Cmd4Lua
function C.cmd_timepos(w)
  define_shared_opts(w)
  local copy = w:desc('copy path to system clipboard'):has_opt('--copy', '-C')

  if not w:is_input_valid() then return end

  if is_connected(w) then
    local timepos = mpv.cmd_get_time_pos()
    local s = uvtt.fmt_sec(timepos or 0) .. ' ' .. v2s(timepos)
    if copy then
      ucb.copy_to_clipboard(s)
      return w:say('copied: ' .. s)
    end
    w:say(s)
  end
end

--------------------------------------------------------------------------------
--                             Subtitles

local function do_translate_sub(n, limit, from, to, copy)
  local text = uvtt.get_current_sentence(n, limit)
  log.debug('sentence: "%s"', text)
  if text and text ~= '' then
    ---@param resp table{original, translated}
    local callback = function(resp)
      log.debug('translated:', resp)
      local translated = resp.translated or vim.inspect(resp)
      if copy then
        ucb.copy_to_clipboard(translated)
        return print('copied: ' .. translated)
      end
      print(translated) -- w:say
    end

    from, to = Translator.recognize_langs(text, from, to)
    Translator.get_instance():translate(text, from, to, callback)
  end
end

local function mk_keymap_silent_callback(desc, callback)
  return {
    desc = desc,
    silent = true,
    noremap = true,
    nowait = true,
    callback = callback
  }
end

--
-- s          -- toggle play stop
-- <leader>lr -- send cmd to reload sybtitles by mpv
-- <leader>le -- sync the timepos in mpv with lnum in nvim
--               (line with timestamp in the current line in the nvim buffer)
-- <leader>lw -- sync the lnum in nvim with timepos in the mpv
-- <leader>lz -- translate recognized sentence from current line
--               (it can be joined multiple entries to translate w/o timestamps)
-- <leader>1  -- subs sync-two-timestamps
-- <leader>2  -- subs join-two-to-one
-- <leader>3  -- subs move-right-to-next
-- <leader>4  -- subs move-left-to-prev
-- <leader>5  -- subs set-end-time-from-mpv-timepos
-- <leader>6  -- subs set-start-time-from-mpv-timepos
-- <leader>7  -- sync nvim lnum with timepos in mvp
-- <leader>8  -- sync timepos in mvp with lnum in nvim
-- <leader>9  -- ctl seek -3
-- <leader>0  -- ctl seek 3
---@param bufnr number?
function M.setup_keybindings_for_buffer_with_subs(bufnr)
  bufnr = bufnr or vim.api.nvim_get_current_buf()

  -- s in normal mode to toggle pause|play
  local vtt_buf_key_bindings = {}
  local km = ubuf.mk_register_buf_key(vtt_buf_key_bindings)
  --
  -- nvim_buf_set_keymap({buffer}, {mode}, {lhs}, {rhs}, {*opts})
  local mo = function(desc, callback)
    desc = 'mpv-subs:' .. v2s(desc)
    return mk_keymap_silent_callback(desc, callback)
  end

  km(bufnr, 'n', 's', mo('toggle_pause', function()
    if mpv.is_connected() then mpv.mpv_toggle_pause() end
  end))
  km(bufnr, 'n', '<leader>z', mo('translate', function()
    if mpv.is_connected() then do_translate_sub(nil, 8) end
  end))
  km(bufnr, 'n', '<leader>1', mo('sync-two-timestamps', function()
    if mpv.is_connected() then vtt.handler_sync_two_timestamps() end
  end))
  km(bufnr, 'n', '<leader>2', mo('join-two-to-one', function()
    if mpv.is_connected() then vtt.handler_join_two_to_one() end
  end))
  km(bufnr, 'n', '<leader>3', mo('move-right-to-next', function()
    if mpv.is_connected() then vtt.handler_move_right_to_next(SYNC_TIME_ON_MOVE) end
  end))
  km(bufnr, 'n', '<leader>4', mo('move-left-to-prev', function()
    if mpv.is_connected() then vtt.handler_move_left_to_prev(SYNC_TIME_ON_MOVE) end
  end))
  km(bufnr, 'n', '<leader>5', mo('set-end-time-from-mpv-timepos', function()
    if mpv.is_connected() then vtt.handler_set_endtime_from_mpv_timepos() end
  end))
  km(bufnr, 'n', '<leader>6', mo('set-start-time-from-mpv-timepos', function()
    if mpv.is_connected() then vtt.handler_set_starttime_from_mpv_timepos() end
  end))
  -- old lw
  km(bufnr, 'n', '<leader>7', mo('sync-lnum-to-mpv-timepos', function()
    if mpv.is_connected() then mpv.sync_nvim_lnum_with_mpv_time_pos() end
  end))
  -- old le
  km(bufnr, 'n', '<leader>b', mo('sync-mpv-timepos-to-lnum', function()
    if mpv.is_connected() then mpv.sync_mpv_time_pos_with_nvim_lnum() end
  end))
  km(bufnr, 'n', '<leader>9', mo('seek -' .. v2s(DEF_SEEK_STEP), function()
    if mpv.is_connected() then mpv.mpv_seek(-DEF_SEEK_STEP) end
  end))
  km(bufnr, 'n', '<leader>0', mo('seek ' .. v2s(DEF_SEEK_STEP), function()
    if mpv.is_connected() then mpv.mpv_seek(DEF_SEEK_STEP) end
  end))
  km(bufnr, 'n', '<leader>-', mo('reload', function()
    if mpv.is_connected() then
      vim.cmd([[:w]])
      mpv.send_custom_cmd('sub-reload')
    end
  end))
  km(bufnr, 'n', '<leader>8', mo('text-to-speech', function()
    if mpv.is_connected() then
      print('generate text-to-speech...')
      local ok, err = utts.do_generate_text_to_speech(nil, true)
      if not ok then print(err) end
    end
  end))
  km(bufnr, 'n', '<leader>=', mo('play-generated-tts-audio', function()
    if mpv.is_connected() then
      print('play...')
      local ok, err = utts.do_check_audio_duration(nil, true)
      if not ok then print(err) end
    end
  end))

  return vtt_buf_key_bindings
end

--
-- Reload the given subtitle tracks. If the id argument is missing, reload
-- the current track. (Works on external subtitle files only.)
--
---@param w Cmd4Lua
function S.cmd_reload(w)
  define_shared_opts(w)
  local id = w:desc('track id'):optional():pop():arg()

  if not w:is_input_valid() then return end

  if is_connected(w) then
    vim.cmd([[:w]])
    w:say(mpv.send_custom_cmd('sub-reload', id))
  end
end

local vtt_buf_key_bindings = nil
--
--
-- open subtitles of the currently playing video
-- notice -c used to reconnect, -C to copy
--
---@param w Cmd4Lua
function S.cmd_open(w)
  define_shared_opts(w)
  local not_open = w:desc('only show, not open in new nvim buffer')
      :has_opt('--not_open', '-n')

  local copy = w:desc('copy path to system clipboard'):has_opt('--copy', '-C')

  if not w:is_input_valid() then return end
  if not is_connected(w) then
    return
  end

  local ok, subpath, subtype = mpv.cmd_get_subtitles()

  if copy then
    ucb.copy_to_clipboard(v2s(subpath))
    return w:say('copied: ' .. v2s(subpath))
  end

  if not not_open and type(subpath) == 'string' then
    if fs.file_exists(subpath) then
      vim.cmd(":e " .. subpath)
      vtt_buf_key_bindings = M.setup_keybindings_for_buffer_with_subs()
    else
      return w:error(fmt('not found sub file %s', v2s(subpath), v2s(subtype)))
    end
  end

  print(ok, subpath, subtype)
end

--
-- Sync play time-pos in mpv to subtitles entry under the cursor
-- move time-pos in mpv to subtitle in current line in nvim buff
--
---@param w Cmd4Lua
function S.cmd_sync_timepos_to_lnum(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.sync_mpv_time_pos_with_nvim_lnum())
  end
end

--
-- Sync play time-pos in mpv to subtitles entry under the cursor
-- jump to line number with subtitle playing in mpv
--
---@param w Cmd4Lua
function S.cmd_sync_lnum_to_timepos(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(mpv.sync_nvim_lnum_with_mpv_time_pos())
  end
end

--
-- Get current time-pos from mpv and put it into end-timestamp in line
--
---@param w Cmd4Lua
function S.cmd_set_end_time_from_mpv_timepos(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_set_endtime_from_mpv_timepos())
  end
end

--
-- Get current time-pos from mpv and put it into start-timestamp
--
---@param w Cmd4Lua
function S.cmd_set_start_time_from_mpv_timepos(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_set_starttime_from_mpv_timepos())
  end
end

--
-- Sync current and next timestamps in already opened subtitles-file
--
---@param w Cmd4Lua
function S.cmd_sync_two_timestamps(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_sync_two_timestamps())
  end
end

--
-- Join current entry with next in already opened subtitles-file
--
---@param w Cmd4Lua
function S.cmd_join_two_to_one(w)
  define_shared_opts(w)
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_join_two_to_one())
  end
end

--
-- move the right part from cursor from current line into next entry
--
---@param w Cmd4Lua
function S.cmd_move_right_to_next(w)
  define_shared_opts(w)
  local sync_time = w:desc('update time on sentence end')
      :has_opt('--sync-time', '-t')
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_move_right_to_next(sync_time))
  end
end

--
-- move the left part from cursor from current line into prev entry
--
---@param w Cmd4Lua
function S.cmd_move_left_to_prev(w)
  define_shared_opts(w)
  local sync_time = w:desc('update time on sentence end')
      :has_opt('--sync-time', '-t')
  if not w:is_input_valid() then return end
  if is_connected(w) then
    w:say(vtt.handler_move_left_to_prev(sync_time))
  end
end

--
-- translate subtitles entry
-- current sentence
--
---@param w Cmd4Lua
function S.cmd_translate(w)
  local n = w:desc('lines to translate'):opt('--lines', '-l')
  local limit = w:desc('limit'):def(8):optn('--limit', '-L')
  local from = w:desc('source language(from)'):def('auto'):opt('--src-lang', '-s')
  local to = w:desc('destination language(to)'):def('ru'):opt('--dst-lang', '-d')

  local copy = w:desc('copy path to system clipboard'):has_opt('--copy', '-C')

  if not w:is_input_valid() then return end

  do_translate_sub(n, limit, from, to, copy)
end

--------------------------------------------------------------------------------


--
-- run mpv command with args
--
---@param w Cmd4Lua
function C.cmd_run(w)
  w:usage('mpv run get_property time-pos')
  w:usage('mpv run cmd [arg1 arg2]')

  local cmd = w:desc('the mpv command'):pop():arg()
  local args = w:desc('the arguments of command'):def({}):pop():argl()

  if not w:is_input_valid() then return end
  ---@cast cmd string
  ---@cast args table
  if args[1] == 'help!' then return w:say(vim.inspect(mpv.DOCS)) end

  print(mpv.send_custom_cmd(cmd, args[1], args[2], args[3], args[4], args[5]))
end

--
-- send mpv command get_property with given args
--
---@param w Cmd4Lua
function C.cmd_get_property(w)
  w:usage('mpv get_property video-params')
  w:usage('mpv gp [arg1 arg2]')

  local args = w:desc('the arguments for get_property'):def({}):pop():argl()

  if not w:is_input_valid() then return end
  ---@cast args table
  if args[1] == 'help!' then return w:say(vim.inspect(mpv.DOCS)) end

  w:say(mpv.send_custom_cmd('get_property', args[1], args[2], args[3], args[4]))
end

local function show_list(w, list)
  if type(list) == 'table' then
    local s = ''
    for _, line in ipairs(list) do
      s = s .. v2s(line) .. "\n"
    end
    print(s)
  else
    w:error('error')
  end
end

-- The list of top-level properties.
function C.cmd_property_list(w)
  -- TODO open in new buf
  if not w:is_input_valid() then return end

  local _, list = mpv.send_custom_cmd('get_property', 'property-list')
  show_list(w, list)
end

-- The list of input commands.
-- This returns an array of maps, where each map node represents a command.
-- This map currently only has a single entry: name for the name of the command.
-- (This property is supposed to be a replacement for --input-cmdlist.
-- The option dumps some more information, but it's a valid feature request to
-- extend this property if needed.)
---@param w Cmd4Lua
function C.cmd_command_list(w)
  if not w:is_input_valid() then return end
  local _, list = mpv.send_custom_cmd('get_property', 'command-list')
  show_list(w, list)
end

---@return string
local function build_keybindings_line(t)
  if type(t) ~= 'table' then
    return v2s(t)
  end
  local is_week = t.is_weak == true and 'W' or ' '
  local s = fmt('%-18s %3s %s %s %s',
    v2s(t.key), v2s(t.priority), is_week, v2s(t.cmd), t.comment or '')
  return s
end


local function show_key_bindings(w, map)
  if type(map) == 'table' then
    local s = ''
    for _, v in ipairs(map) do
      s = s .. ' ' .. build_keybindings_line(v) .. "\n"
    end
    -- todo opts to open in new buff
    print(s)
  else
    w:error('error: expected table')
  end
end

-- The list of current input key bindings. This returns an array of maps,
-- where each map node represents a binding for a single key/command.
-- This map has the following entries:
--   key cmd is_weak owner section priority comment
-- https://mpv.io/manual/master/#command-interface-input-bindings
---@param w Cmd4Lua
function M.cmd_input_bindings(w)
  if not w:is_input_valid() then return end
  local _, key_bindings = mpv.send_custom_cmd('get_property', 'input-bindings')
  show_key_bindings(w, key_bindings)
end

--
-- The list of current input key bindings inside nvim vtt buffer.
--
---@param w Cmd4Lua
function M.cmd_key_bindings(w)
  if not w:is_input_valid() then return end
  if type(vtt_buf_key_bindings) ~= 'table' then
    return w:error('buffer for vtt not created')
  end

  local s = ''
  for _, e in ipairs(vtt_buf_key_bindings) do
    s = s .. fmt("%2s  %12s  -  %s\n", v2s(e.mode), v2s(e.key), v2s(e.desc))
  end
  w:say(s)
end

--
-- experimental
-- its works! manual testing. recv messages from mpv ipc via posix.poll
--
---@param w Cmd4Lua
function M.cmd_drain_in(w)
  if not w:is_input_valid() then return end
  local reqid1 = mpv.cast_cmd('get_property', 'time-pos')
  local reqid2 = mpv.cast_cmd('get_property', 'time-pos')
  local lines, idx = mpv.read_socket_to_lines(250, {}, reqid2)
  print(vim.inspect(lines), reqid1, reqid2, idx)
end

return M

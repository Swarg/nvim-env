-- 03-10-2024 @author Swarg
--
-- Wrapper around Project Management tools for specified language
-- Lang.cmdProjectManagement

local c4lv = require 'env.util.cmd4lua_vim'
local ucmd = require 'env.lang.utils.command'
local Lang = require 'env.lang.Lang'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- dynamicaly generated based on current opened file
-- -PL java
function M.handle(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(':EnvPM')
      :about('Project Management Project/Lang')

  local lang = Lang.getLangByCtx(w, true, true, false)

  if not lang then
    return print('Not found Project/Lang for opened buffer. Try with -PL ext')
  end

  -- generate available commands for programming-language for current Lang
  w:clear_level_help()
  ucmd.clearSelectLangByExt(w)

  lang:cmdProjectManagement(w)
end

M.opts = { nargs = '*', range = true }

return M

-- 15-03-2024 @author Swarg
--
local log = require('alogger')
local fs = require('env.files')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local utbl = require 'env.util.tables'

local Lang = require('env.lang.Lang')
-- local base = require 'env.lang.utils.base'
local tviewer = require("env.util.tbl_viewer")
-- local pcache = require('env.cache.projects')
-- local lcache = require('env.cache.library')

local M, P = {}, {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvLang')
      :about('Interact and Configure Programm Languages and Projects')
      :handlers(M)

      :desc('Interact with Project')
      :cmd("project", 'p')

      :desc('Init New Flat Project (src+test in one dir)')
      :cmd("flat-project", 'fp')

      :desc('show all Opened Projects (cached)')
      :cmd("inspect", 'i')

      :desc('close opened project (remove from cached))')
      :cmd("close")

      :desc('Link two files together for quick switching (test and source)')
      :cmd("link", 'l')

      :run()
      :exitcode()
end

--
-- Interact with Project
-- `:EnvLang project` just redirect to :EnvProject
--
---@param w Cmd4Lua
function M.cmd_project(w)
  w:handlers(P)

      :desc('Open Project in the current directory')
      :cmd('open', 'o')

      :desc('Close current Opened Project(in cwd)')
      :cmd('close', 'c')

      :desc('Inspect Current Project')
      :cmd('info', 'i')

      :desc('Create New Project')
      :cmd('new', 'n')

      :run()
end

local _commands = {
  "inspect", "flat-project"
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

log = log
local E, v2s, fmt = {}, tostring, string.format


-- local function get_bufdir(bufnr)
--   local bufname = vim.api.nvim_buf_get_name(bufnr or 0)
--   return bufname and bufname:match("(.*[/\\])") or nil
-- end



--
-- Open Project in the current directory
-- this cache used in my nvim-settings:
-- see ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/settings/jdtls.lua
---@param w Cmd4Lua
function P.cmd_open(w)
  if not w:is_input_valid() then return end
  w:say('use :EnvProject open')
end

--
-- Close current Opened Project(in cwd)
--
---@param w Cmd4Lua
function P.cmd_close(w)
  w:usage("EnvLang project close <index>")
  w:usage("EnvLang project close -1        # to list all projects")
  local n = w:desc('index'):def(-1):pop():argn()

  if not w:is_input_valid() then return end

  w:say('To Close Lsp use :EnvLsp close or :LspStop')

  local map = Lang.getActiveProjects()
  local dirs = utbl.sorted_keys(map, nil)

  local project_root = dirs[n]

  if n < 0 or not project_root then -- ls
    local s = ''
    for i, dir in ipairs(dirs) do
      s = s .. fmt("%2s  %s\n", i, v2s(dir))
    end
    return w:say(s)
  end

  w:say('Project Closed ', Lang.closeProject(project_root), project_root)
end

--
-- Inspect Current Project
--
---@param w Cmd4Lua
function P.cmd_info(w)
  if not w:is_input_valid() then return end
  w:say('use :EnvProject info')
end

--
-- Create New Project
--
---@param w Cmd4Lua
function P.cmd_new(w)
  if not w:is_input_valid() then return end
  w:say('use :EnvProject new <language> <buildsystem>')
end

--
-- Flat Project all files in same dir with src+test
-- Goal: rapidly testing and check suggestions
-- create a new project in memory only(by default) to
-- in order to start working for toggling between sources and tests and so on
--
-- @Deprecated TODO MOVE to env.langs.*.command.new_project
--
---@param w Cmd4Lua
function M.cmd_flat_project(w)
  local fn = w:desc('file in the project to be created')
      :def(vim.api.nvim_buf_get_name(0)):opt('--file', '-f')

  local mkconf = w:desc('create project config file (like pom.xml, mix.exs)')
      :has_opt('--config-file', '-c')

  if not w:is_input_valid() then return end

  local lang = Lang.getLang(fn) -- find Lang for ext of the current buffer
  if not lang then
    return w:error('Cannot resolve Lang for ' .. tostring(fn))
  end

  local project_root = fs.ensure_dir(fs.extract_path(fn))
  local lang0 = Lang.getOpenedProject(project_root)
  if lang0 then
    -- local ok, lang0 = pcall(Lang.findProjectRoot, lang, fn)
    -- if ok or type(lang0) ~= 'string' then -- Project root not found.
    return w:error('Project already exists for dir: ' ..
      tostring(lang0:getProjectRoot()))
  end

  lang:newProject(project_root, { flat = true, inmem = not mkconf })
  local typ = mkconf == true and 'OnDisk' or 'InMemoryOnly'
  local cnt, details = nil, ''

  if mkconf then -- create build script + additional project config files
    w.vars.flat_project = true
    cnt = lang:createProjectConfigs(w.vars)
    details = lang:getProjectBuilder():getFileWriter():getReadableReport()
  end

  print('New Flat Project Initialized.', typ, cnt, details)
end

--
-- close opened project (remove from cached))
--
---@param w Cmd4Lua
function M.cmd_close(w)
  local fn = w:desc('file in the project to be created')
      :def(vim.api.nvim_buf_get_name(0)):opt('--file', '-f')

  if not w:is_input_valid() then return end

  local lang = Lang.getLang(fn) -- find Lang for ext of the current buffer
  if not lang then
    return w:error('Cannot resolve Lang for ' .. tostring(fn))
  end

  local ok, lang0 = pcall(Lang.findProjectRoot, lang, fn)
  if ok then
    local projdir = lang0:getProjectRoot()
    local closed = Lang.closeProject(projdir)
    print('Project ', projdir, (closed == true and 'closed' or 'Not Found'))
  else
    print('Not found project for', fn)
  end
end

--
-- show all cached project
--
---@param w Cmd4Lua
function M.cmd_inspect(w)
  if not w:is_input_valid() then return end

  local projects = Lang.getActiveProjects()

  if type(projects) == 'table' then
    if not next(projects) then
      print("empty - No Opened Projects")
      return
    end

    local ctx = { state = tviewer.wrap2state(projects, 'Lang-Projects') }
    tviewer.create_buf(ctx, ctx.state.src or 'table-viewer')
  end
end

--
-- Link two files together for quick switching (test and source)
-- Goal: to link a custom(not standatr linked) test file to a source file that
-- already has another test file (autofounded by toggleing src<->test).
--
---@param w Cmd4Lua
function M.cmd_link(w)
  w:usage(':EnvLang link list -- input". 1" to link current file to 1 in list')

  local fn = w:desc('file in the project to be linked')
      :def(vim.api.nvim_buf_get_name(0)):opt('--file', '-f')
  local act = w:desc():pop():optional():arg()

  if not w:is_input_valid() then return end

  local lang = Lang.getLang(fn) -- find Lang for ext of the current buffer
  if not lang then
    return w:error('Cannot resolve Lang for ' .. tostring(fn))
  end

  local ok, lang0 = pcall(Lang.findProjectRoot, lang, fn)
  if not ok or type(lang0) == 'string' then -- Project root not found.
    print('Not Found Project for this file ' .. tostring(fn))
    return
  end
  lang = lang0
  -- list all sources files
  local map = (lang:getCIMap() or E).cn2ci or {}
  if not next(map) then
    print('empty')
    return
  end

  -- if not act or act == '' or act == 'list' or act == 'ls' then
  local names, max = {}, 0
  for ciname, _ in pairs(map) do
    names[#names + 1] = ciname
    if #ciname > max then max = #ciname end
  end

  table.sort(names)

  local name2n = {}
  for i, name in pairs(names) do name2n[name] = i end
  local curr_ci = nil

  if not act or act == '' or act == 'list' then
    local listing = ''
    for i, name in ipairs(names) do
      local m, sign, name2, n2 = ' ', '  ', '', ''
      local ci = assert(map[name], 'has ClassInfo') ---@cast ci env.lang.ClassInfo
      if ci:getPath() == fn then m, curr_ci = '@', ci end
      local pair = ci:getPair()
      if pair then ---@cast pair env.lang.ClassInfo
        sign, name2 = '->', pair:getClassName()
        n2 = '(' .. tostring(name2n[pair:getClassName()]) .. ')'
      end
      listing = listing .. string.format('%s%2d %-' .. max .. 's %s %s %s\n',
        m, i, name, sign, n2, name2)
    end

    print(listing) -- listing
    local msg = 'Enter the indices of the files that need to be linked together: '
    act = vim.fn.input(msg)
    print("\n")
  end

  if act == 'q' or act == 'Q' or act == '' then
    return print("Cancel")
  end

  local index_pairs = {} -- of numbers
  for s in string.gmatch(act .. ' ', '([^ ]+)') do
    local index = nil
    if s == '.' or s == 'current' or s == fn then
      curr_ci = curr_ci or lang:findCIbyPath(fn)
      if not curr_ci then
        return print('Cannot find ClassInfor for ' .. tostring(fn))
      end
      -- add to map and list to be able to refer to it using a new number
      names[#names + 1] = curr_ci:getClassName()
      name2n[curr_ci:getClassName()] = #names
      index = #names
    else
      index = assert(tonumber(s), 'number expected got: ' .. tostring(s))
    end
    table.insert(index_pairs, index)
  end

  -- link together all the specified pairs
  local i = 1

  while i < #index_pairs do
    local ni1, ni2 = index_pairs[i], index_pairs[i + 1]
    assert(ni1 and ni2, 'Error 1:' .. tostring(ni1) .. ' 2:' .. tostring(ni2))

    local name1, name2 = names[ni1], names[ni2] -- classnames
    if name1 and name2 and name1 ~= name2 then
      local ci1 = assert(map[name1], 'has CI1') ---@cast ci1 env.lang.ClassInfo
      local ci2 = assert(map[name2], 'has CI2') ---@cast ci2 env.lang.ClassInfo
      ci1:bindPair(ci2)
      print(fmt('(%s) %s <-> (%s) %s', ni1, v2s(name1), ni2, v2s(name2)))
    else
      print(fmt('(%s) %s xXx (%s) %s', ni1, v2s(name1), ni2, v2s(name2)))
    end
    i = i + 2
  end
end

return M

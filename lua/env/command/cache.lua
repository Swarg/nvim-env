-- 12-03-2024 @author Swarg
--
local log = require('alogger')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local Object = require 'oop.Object'

local Lang = require('env.lang.Lang')
local pcache = require('env.cache.projects')
local lcache = require('env.cache.library')
local eclipse = require 'env.bridges.eclipse' --jdtls

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvCache')
      :about('Projects Cache and workarounds')
      :handlers(M)

      :desc('show all state of projects cache')
      :cmd("inspect", 'i')

      :desc('projects cache statistics')
      :cmd("statistics", 's')

      :desc('clean all cached date')
      :cmd("clean-all", 'ca')

      :run()
end

local _commands = {
  "inspect"
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local E = {}
log = log

--
-- show all state of cache
--
---@param w Cmd4Lua
function M.cmd_inspect(w)
  if not w:is_input_valid() then return end

  print('pcache-stats:', vim.inspect(pcache.statistics))
  print("env.cache.projects:", vim.inspect(pcache.projects_cache))
  print("env.cache.projects.dirsettings:", vim.inspect(pcache.getDirConfings()))
  print("env.cache.libraries:", vim.inspect(lcache.get_cache()))

  print('env.bridges.eclipse:', vim.inspect(eclipse.get_projects_cache()))
  print('env.bridges.eclipse workspace:', eclipse.get_workspace_path())

  -- normalize from Objects to raw Arrays
  local projects = {}
  for k, lang in pairs(Lang.getActiveProjects() or E) do
    projects[k] = Object.toArray(lang)
  end
  print('env.Lang.prjects:', vim.inspect(projects))

  local jdtls_mem = vim.g.java_asked_project_names
  if jdtls_mem then
    print('user.lsp.settings.jdtls(nvim-settings):', vim.inspect(jdtls_mem))
  end
end

---@param w Cmd4Lua
function M.cmd_statistics(w)
  if not w:is_input_valid() then return end

  print("cache:projects statistics:")
  print(vim.inspect((pcache or E).statistics))
end

---@param w Cmd4Lua
function M.cmd_clean_all(w)
  if not w:is_input_valid() then return end

  pcache.full_cleanup() -- clear known paths already checked for Projects
  eclipse.clear_cache()

  Lang.clearCache() -- close all Opened Projects

  print('cleaned')
end

return M

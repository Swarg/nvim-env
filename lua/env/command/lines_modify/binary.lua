-- 26-08-2024 @author Swarg
--
-- Goals:
--  - hex 2 string, string to hex
--  base64 decode encode
--

local Editor = require 'env.ui.Editor'
local U = require("env.util.convert");

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvLinesModify binary
function M.handle(w)
  w:about('convert to binary format (like hex) and back')
      :handlers(M)

      :desc('convert string with hex to readable (hex_decode)')
      :cmd('hex-to-str', 'h2s')

      :desc('convert readable string to hex (hex_encode)')
      :cmd('str-to-hex', 's2h')

      :desc('encode given string into base64')
      :cmd('base64_encode', 'be')

      :desc('decode given base64-string to readable')
      :cmd('base64_decode', 'bd')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


--
-- convert string with hex to readable
--
---@param w Cmd4Lua
function M.cmd_hex_to_str(w)
  if not w:is_input_valid() then return end

  local ctx = Editor:new():getContext()
  local line = ctx:getCurrentLine()
  -- local offset = 1
  -- if line and line:sub(1, 2) == '0x' then offset = 2 end

  local updated_line = U.hex_decode(line)

  ctx:setCurrentLine(updated_line)
end

--
-- convert readable string to hex
--
---@param w Cmd4Lua
function M.cmd_str_to_hex(w)
  if not w:is_input_valid() then return end

  local ctx = Editor:new():getContext()
  local line = ctx:getCurrentLine()

  local updated_line = U.hex_encode(line)

  ctx:setCurrentLine(updated_line)
end

local ok_base64, base64 = pcall(require, 'base64')

--
-- encode given string into base64
--
---@param w Cmd4Lua
function M.cmd_base64_encode(w)
  if not w:is_input_valid() then return end

  if not ok_base64 then error(base64) end

  local ctx = Editor:new():getContext()
  local line = ctx:getCurrentLine()

  ctx:setCurrentLine(base64.encode(line))
end

--
-- decode given base64-string to readable
--
---@param w Cmd4Lua
function M.cmd_base64_decode(w)
  if not w:is_input_valid() then return end

  if not ok_base64 then error(base64) end

  local ctx = Editor:new():getContext()
  local line = ctx:getCurrentLine()

  ctx:setCurrentLine(base64.decode(line))
end

return M

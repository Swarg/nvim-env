-- 02-02-2024 @author Swarg
local M = {}
--
-- Goals:
--  - minify the output of the `tree`-command
--  - build full path from tree leaf (under the cursor in current buffer),
--  - provide actions for obtained path: open, file, stat, head, jq, and so on
--

local log = require('alogger')
local ui = require('env.ui')
local fs = require("env.files")
local bu = require('env.bufutil')
local utables = require("env.util.tables")
local utree = require("env.util.unix_tree_cmd");
local clipboard = require("env.util.clipboard")


--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvLinesModify shell-command
function M.handle(w)
  log.debug("shell-command handle")

  w:about('Modify the Selected Lines in shell output')
      :handlers(M)

      :desc('modify output of the tree command (selected lines)')
      :cmd('tree', 't')

      :desc('build full path from the leaf of the tree (and run cmd)')
      :cmd('path-from-tree', 'pft')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


--
-- modify output of a tree cmd
--
---@param w Cmd4Lua
function M.cmd_tree(w)
  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}

  if opts and opts.line1 and opts.line2 then
    local lines = bu.get_selected_lines(opts, false)
    if lines then
      local res = utree.parse_output(lines)
      if res then
        bu.set_selected_lines(opts, res)
      end
    end
  end
end

local function join_args(cmd)
  if type(cmd) == 'table' then
    return table.concat(cmd, ' ') -- todo quote
  else
    return tostring(cmd)
  end
end

---@param cmd0 string
---@param hl boolean?
---@param out table
local function exec(cmd0, out, hl)
  log.debug(cmd0)
  assert(type(out) == 'table', 'res')

  if hl then out[#out + 1] = '' end
  fs.execrl(cmd0, out)
  if hl then out[#out + 1] = '' end
  return out
end

---@param path string
local function open_in_buf(path)
  log.debug("open in buffer", path)
  ui.close_float_win_if_need()
  vim.api.nvim_exec(":e " .. path, true)
end

--
-- fetch path from the current leaf of the tree
--
---@param w Cmd4Lua
function M.cmd_path_from_tree(w)
  log.debug("cmd_path_from_tree")
  w:usage('EnvLM shell-command path-from-tree --exec [ head -n 4 ] -b')
  w:usage('EnvLM shell-command pft --jq [ .Name | .[] ] --new-buffer')

  local copy = w:desc('copy to system clibboard'):has_opt('--copy', '-C')
  local cmd = w:desc('execute given sys command for path'):optl('--exec', '-e')
  local file = w:desc('run sys cmd `file` for this path'):has_opt('--file', '-f')
  local stat = w:desc('run sys cmd `stat` for this path'):has_opt('--stat', '-s')
  local open = w:desc('open path in the new nvim buffer'):has_opt('--open', '-o')
  local head = w:desc('show first n lines'):defOptVal('1'):optn('--head', '-h')
  local tail = w:desc('show last  n lines'):defOptVal('1'):optn('--tail', '-t')
  local jq = w:desc('show jq output for json file and given filter')
      :defOptVal('.'):optl('--jq', '-j')
  local in_new_buffer = w:desc('open output in the new nvim buffer')
      :has_opt('--new-buffer', '-b')
  local max_tree_lines = w:desc('limit of the tree lines in buffer')
      :def(128):optn('--limit', '-L')

  if not w:is_input_valid() then return end

  local lnum = (vim.api.nvim_win_get_cursor(0) or {})[1] -- by win id
  local s = math.max(0, lnum - max_tree_lines)           -- ~start lnum
  local lines = vim.api.nvim_buf_get_lines(0, s, lnum, false)
  log.debug('find start "', s, lnum)

  local start_pos
  for i = #lines, 1, -1 do -- find start pos
    if not utree.has_tree_nodes(lines[i]) then
      local root = lines[i]
      start_pos = s + i -- - 1
      log.debug('find start-pos at: %s line:"%s"', start_pos, root)
      -- add directory from current buf
      if root and root:sub(1, 1) ~= '/' then
        local dir = fs.extract_path(vim.api.nvim_buf_get_name(0))
        if dir then
          root = fs.join_path(dir, root)
          lines[i] = root
          log.debug('full root:"%s"', root)
        end
      end
      break
    end
  end

  if not start_pos then
    return print('Cannot find the root of the tree.' ..
      ' Started from lnum:' .. tostring(lnum) .. '.' ..
      ' Try increasing the limit with `--limit N` more than ' ..
      tostring(max_tree_lines))
  end

  local path, res = nil, {}

  -- lines = vim.api.nvim_buf_get_lines(0, start_pos, lnum, false)
  lines = utables.flat_copy(lines, start_pos, lnum) or {}

  path = utree.build_path(lines, lnum) or ''

  if (open or file or stat or head or tail or cmd or jq) and
      not fs.file_exists(path) then
    return print('File not exists: "' .. tostring(path) .. '"')
  end

  if copy then
    if clipboard.copy_to_clipboard(path) then
      return print('copied')
    end
    --
  elseif open then
    return open_in_buf(path)
    --
  elseif file then
    exec('file ' .. path, res)
    local i = (res[1] or ''):find(':')
    if i then res[1] = '# ' .. res[1]:sub(i + 1) end -- onty type
  elseif stat then
    exec('stat ' .. path, res, not in_new_buffer)
  elseif head then
    exec('head -n ' .. tostring(head) .. ' ' .. path, res, head > 1)
  elseif tail then
    exec('tail -n ' .. tostring(tail) .. ' ' .. path, res, tail > 1)
  elseif jq then
    exec('jq ' .. join_args(jq) .. ' ' .. path, res, not in_new_buffer)
    --
  elseif cmd then
    exec(join_args(cmd) .. ' ' .. path, res, not in_new_buffer)
  else
    res = { path }
  end

  if in_new_buffer then
    log.debug("open output in the new nvim buffer")
    local bufnr = ui.buf_new_named('/tmp/' .. path)
    ui.buf_append_lines(bufnr, res)
    if jq then
      vim.api.nvim_buf_set_option(bufnr, 'filetype', 'json')
    end
    vim.cmd([[exe "norm! gg"]])
    --
  else -- just past into the current buffer
    local bufnr = vim.api.nvim_get_current_buf() or 0
    vim.api.nvim_buf_set_lines(bufnr, lnum, lnum, false, res)

    -- type gv to select inserted lines
    vim.fn.setpos("'<", { bufnr, lnum + 1, 1, 0 })
    vim.fn.setpos("'>", { bufnr, lnum + #res, 9999, 0 })
    -- vim.cmd([[exe "norm! gv"]])
  end
end

return M

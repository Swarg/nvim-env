-- 05-11-2023 @author Swarg
local M = {}
-- SubCommands for EnvLinesModify cmd4lua

---@diagnostic disable-next-line
local su = require('env.sutil')
local bu = require('env.bufutil')
local cu = require("env.util.commands")
---@diagnostic disable-next-line: unused-local
local log = require('alogger')
local c4l_base = require("cmd4lua.base")
local c4lv = require("env.util.cmd4lua_vim")

local Context = require("env.ui.Context")

--------------------------------------------------------------------------------

-- :EnvLinesModify cmd4lua
function M.handle(w)
  w:handlers(M)
      :desc('Create function for :cmd(name,sn)')
      :cmd('gen-cmd-func', 'gcf')

      :desc('Convert line with command and description into lua code :desc :cmd')
      :cmd('line-to-desc-cmd', 'ldc')

      :desc('Convert code of the command handler with old if-style to the' ..
        ' framework style')
      :cmd('convert-to-new', 'ctn')

      :desc('Generate _commands table for selected range')
      :cmd('build-cmd-completion', 'bcc')

      :run()
end

--------------------------------------------------------------------------------

local _commands = {
  "line-to-desc-cmd", "convert-to-new", "build-cmd-completion"
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
---@param w Cmd4Lua
function M.cmd_gen_cmd_func(w)
  local node = w:desc('Create command node with a given subcommands')
      :optl('--node', '-n') -- table|string

  if not w:is_input_valid() then return end
  local opts = w.vars.vim_opts or {}

  local lines = bu.get_selected_lines(opts, false)
  if not lines then
    error('Empty lines')
    return
  end
  local res, cnt, desc = {}, 0, ''
  -- TODO local handlers_name = c4l.extract_handlers_table_name()

  for _, line in pairs(lines) do
    local name = c4lv.find_cmdnames(line)
    if name then
      local func_name, code = c4l_base.get_cmdhandler_funcname(name), nil
      if func_name then
        cnt = cnt + 1
        if node then
          ---@diagnostic disable-next-line: param-type-mismatch
          code = c4lv.gen_code_cmd_node(func_name, node)
        else
          code = c4lv.gen_code_cmd_handler(func_name, '', desc)
          desc = ''
        end
        res[#res + 1] = ''
        su.split_range(code, '\n', 1, -1, false, res)
      end
    else
      desc = c4lv.find_desc(line, '') -- pick comment before cmd
    end
  end

  if cnt > 0 then
    local insert_pos = { line1 = opts.line2 + 1, line2 = opts.line2 + 1 }
    bu.set_selected_lines(insert_pos, res)
    Context:new():selectLines(opts.line2 + 1, opts.line2 + #res - 1)
  else
    print('Not Found cmd names in the selected lines')
  end
end

--
-- Convert code of the command handler with old if-style to a framework style
--
function M.cmd_convert_to_new(w)
  if not w:is_input_valid() then return end
  local opts = w.vars.vim_opts

  local lines = bu.get_selected_lines(opts, false)
  local res = {}

  local ptn_names = ":is_cmd%(['\"](.-)['\"],%s*['\"](.-)['\"],?.*%) then"

  if lines then
    local impls, comment = { '' }, ''
    for i = 1, #lines do
      local line = lines[i]
      -- print('[DEBUG] i:', i,line)
      local parsed = false

      local desc = line:match('if w:(desc%(.-%):?)')
      if desc then
        res[#res + 1] = ''
        line = '    :' .. desc
        res[#res + 1] = line
        parsed = true
        comment = desc:match("desc%(['\"](.-)['\"]%)") or ''
      end
      --
      local is_cmd, ifbody = line:match(':?is_cmd(%(.-%))%s+then(.*)$')
      if is_cmd then
        local name, sn = line:match(ptn_names)
        line = '    :cmd' .. is_cmd
        res[#res + 1] = line
        parsed = true

        name, sn = c4l_base.fullname_first(name, sn)
        name = c4l_base.get_cmdhandler_funcname(name)
        if not name then
          error('cannot parse name from lnum:' .. i .. ' line:' .. line)
        end
        local body = ''
        if ifbody and ifbody ~= '' then
          -- print('##[DEBUG]## Ifbody |' .. tostring(ifbody) .. '|')
          body = ifbody
          -- end in same line?
        else
          -- find end of the ifbody
          while i < #lines do
            i = i + 1
            local line0 = lines[i]
            -- print("[DEBUG] in while i:", (i), line0)
            if line0:match('^%s+end') or line0:match('^%s+elseif') then
              -- print("[DEBUG] matched!")
              break
            elseif not line0:match('^%s+%-%-$') then -- skip comment --
              body = body .. line0 .. "\n"
            end
          end
        end
        local code = c4lv.gen_code_cmd_handler(name, body, comment)
        su.split_range(code, '\n', 1, -1, false, impls)
        comment = ''
      end
      if not parsed and not line:match('^%s+%-%-$') then -- skip comment --
        table.insert(res, '--' .. line)
      end
    end

    res[#res + 1] = '    :run()'
    res[#res + 1] = 'end'
    res[#res + 1] = ''

    for _, line0 in pairs(impls) do
      table.insert(res, line0)
    end

    bu.set_selected_lines(opts, res)
  end
end

--
-- Convert line with command and description into lua code :desc :cmd
--
---@param w Cmd4Lua
function M.cmd_line_to_desc_cmd(w)
  if not w:is_input_valid() then return end
  local opts = w.vars.vim_opts

  local lines = bu.get_selected_lines(opts, false)
  if not lines then return end

  local t = {}
  for _, line in ipairs(lines) do
    if line and line ~= '' then
      local cmd, desc = string.match(line, '^([^%s]+)%s%s+(.*)$')
      t[#t + 1] = ''
      t[#t + 1] = '      :desc("' .. tostring(desc) .. '")'
      t[#t + 1] = '      :cmd("' .. tostring(cmd) .. '")'
      t[#t + 1] = ''
    end
  end
  bu.set_selected_lines(opts, t)
end

--
-- Generate _commands table for selected range
--
---@param w Cmd4Lua
function M.cmd_build_cmd_completion(w)
  if not w:is_input_valid() then return end
  local opts = w.vars.vim_opts

  local lines = bu.get_selected_lines(opts, false)
  if not lines then return end

  local cmds = {}
  for _, line in ipairs(lines) do
    local cmd = string.match(line or '', "^%s*:cmd%(['\"]([%w%-]+)['\"]")
    if cmd and cmd ~= '' then
      cmds[#cmds + 1] = cmd
    end
  end
  if #cmds < 1 then
    return w:error('Not found any commands')
  end
  local s = "" -- local _commands = { "
  for _, cmd in ipairs(cmds) do
    if s ~= '' then s = s .. ", " end
    s = s .. fmt('"%s"', cmd)
  end

  bu.insert_line_to(s, nil)
end

return M

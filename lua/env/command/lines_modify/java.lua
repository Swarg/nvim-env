-- 05-11-2023 @author Swarg
local M = {}
--

---@diagnostic disable-next-line
local su = require('env.sutil')
local bu = require('env.bufutil')
local ucb = require 'env.util.clipboard'

local mvn_gen = require 'env.langs.java.util.maven.gen'
local mvn_parser = require 'env.langs.java.util.maven.parser'
local refactor = require 'env.langs.java.util.refactor'


-- :EnvLinesModify java
-- change selected lines in java source file(buffer)
function M.handle(w)
  w:about('Modify the Selected Lines in java source file')
      :handlers(M)

      :desc('Modify the test expression')
      :cmd("assert-equals", "ae")

      :desc('Parser selected lines in pom.xml')
      :cmd("mvn-parse-deps", 'mpd')

      :desc('wrap selected lines to string literal')
      :cmd("wrap-lines-to-string", 'wl2s')

      :desc('split one long line of code with String x=".."; into several lines')
      :cmd("one-to-many", 'o2m')

      :desc('format toString output of the Objects state from one line to multiple')
      :cmd("beautify-tostring-output", 'bto')

      :run()
end

--------------------------------------------------------------------------------


--
-- Modify the test expression
-- select lines -> :<,>EnvLinesModify [java] assert-equals
--
---@param w Cmd4Lua
function M.cmd_assert_equals(w)
  if not w:is_input_valid() then return end

  local pattern = "%sassertEquals(%s, %s);"

  local opts = w.vars.vim_opts or {}

  if opts.line1 and opts.line2 and pattern then
    local lines = bu.get_selected_lines(opts, false)
    if not lines then
      return
    end
    local changes = 0
    for i, line in pairs(lines) do
      -- '  left = right;'
      local ident, left, right = line:match("(%s+)([^%s]+)%s*=%s*([^%s]+)%s*;")
      if left and right then
        ident = ident or ''
        lines[i] = string.format(pattern, ident, right, left)
        changes = changes + 1
      end
    end
    if changes > 0 then
      bu.set_selected_lines(opts, lines)
    end
  end
end

--
-- Parser selected lines in pom.xml
--
---@param w Cmd4Lua
function M.cmd_mvn_parse_deps(w)
  local copy = w:desc('copy to clipboard'):has_opt('--copy', '-c')
  local togradle = w:desc('convert to gradle deps'):has_opt('--to-gradle', '-g')
  local toluamap = w:desc('convert to lua map'):has_opt('--to-lua-map', '-l')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false)
  local t = mvn_parser.parse_dependencies_lines(lines)

  local output = ''

  if toluamap then -- for inner use
    for _, gav in pairs(t) do
      output = output .. "\n" .. mvn_parser.dep_to_luamap(gav)
    end
    bu.set_selected_lines(opts, su.split(output, "\n"))
    return w:say('replaced')
  end

  if togradle then
    for _, gav in pairs(t) do
      output = output .. "\n" .. mvn_parser.dep_to_gralde(gav)
    end
  else
    output = vim.inspect(t)
  end

  if copy then
    ucb.copy_to_clipboard(output)
    print('copied to clipboard')
  else
    print(output)
  end
end

--
-- wrap selected lines to string literal
--
---@param w Cmd4Lua
function M.cmd_wrap_lines_to_string(w)
  local opts = w.vars.vim_opts or {}

  if not w:is_input_valid() then return end


  if not opts.line1 or not opts.line2 then
    return w:error('not selected lines')
  end

  local lines = bu.get_selected_lines(opts, false)
  if not lines then
    return w:error("no lines")
  end

  local res = { "String exp = " }
  local suff = ' +'
  for i = 1, #lines do
    local line = lines[i]
    if string.find(line, '"', 1, true) then
      line = line:gsub('"', '\\"')
    end
    if i == #lines then suff = ';' end
    res[#res + 1] = '"' .. line .. '\\n"' .. suff
  end

  bu.set_selected_lines(opts, res)
end

--
-- split one long line of code with String x=".."; into several lines
--
---@param w Cmd4Lua
function M.cmd_one_to_many(w)
  local max_len = w:desc("max length"):def(80):optn("--max-length", "-m")

  if not w:is_input_valid() then return end

  local line = vim.api.nvim_get_current_line()
  local lnum = (vim.api.nvim_win_get_cursor(0) or {})[1]

  local lines = refactor.fold_one_string_to_many(line, max_len)

  vim.api.nvim_buf_set_lines(0, lnum - 1, lnum, true, lines)
end


--
-- format toString output of the Objects state from one line to multiple
--
---@param w Cmd4Lua
function M.cmd_beautify_tostring_output(w)
  local max_len = w:desc("max length"):def(80):optn("--max-length", "-m")

  if not w:is_input_valid() then return end

  local line = vim.api.nvim_get_current_line()
  local lnum = (vim.api.nvim_win_get_cursor(0) or {})[1]

  local lines = refactor.beautify_tostring_output(line, max_len)

  vim.api.nvim_buf_set_lines(0, lnum - 1, lnum, true, lines)
end

return M

-- 05-11-2023 @author Swarg
local M = {}
--

---@diagnostic disable-next-line
local su = require 'env.sutil'
local bu = require 'env.bufutil'
---@diagnostic disable-next-line: unused-local
local log = require 'alogger'
---@diagnostic disable-next-line: unused-local
local Cmd4Lua = require 'cmd4lua'

local tu = require 'env.util.tables'
local Editor = require 'env.ui.Editor'
local lua_parser = require 'env.lua_parser'
local tviewer = require 'env.util.tbl_viewer'
local lapi_consts = require 'env.lang.api.constants'
local uclipboard = require 'env.util.clipboard'

local R = require 'env.require_util'
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

--------------------------------------------------------------------------------
--                       LUA SOURCE CODE
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :desc('Add the one line local var definition')
      :cmd("add-local-definition", "ald")

      :desc('Add id field to lua-table')
      :cmd("add-id2table", "id2t")

      :desc('sort keys in the table in the given order')
      :cmd("sort-table-keys", "stk")

      :desc('reorder table keys for formated multiline table with a given order')
      :cmd("reorder-multiline-keys", "rmk")

      :desc('extract table keys from selected lines')
      :cmd("extract-table-keys", "etk")

      :desc('Add id field to lua-table')
      :cmd("gen-method-body", "gmb")

      :desc('source code (module) mappings')
      :cmd("mappings", "m")

      :desc("minimalize require import")
      :cmd("format-require", "fr")

      :run()
end

--------------------------------------------------------------------------------
local mappings = {
  classes = {},
  modules = {},
}
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
-- local LEXEME_NAMES = lapi_consts.LEXEME_NAMES

--
--
--
---@param w Cmd4Lua
---@return table? lines
---@return table opts{line1, line2}
local function get_lines_and_opts(w)
  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false)
  if not lines then
    if not opts.line1 or not opts.line2 then
      log.debug('no selected range(line1 or line2) in opts')
    end
    w:error('you should select a range of the lines')
    return nil, E
  end
  return lines, opts
end

--
--  --order .   +   "-- order: key_a key_x key_w" in first line
--
---@param order table
---@param lines table
---@param res table
---@param i number
---@return table
---@return number
local function pickup_order_from_first_line(order, lines, res, i)
  -- pickup order from the first line with lines to sort
  if order == '.' or type(order) == 'table' and order[1] == '.' then
    local line_with_order = string.match(lines[i] or '', 'order:%s*(.-)%s*$')
    if not line_with_order or line_with_order == nil then
      error('expected first line with something like "order: desc name type"')
    end
    res[i] = lines[i]
    i = i + 1
    order = {}
    for key in string.gmatch(line_with_order, "([^%s]*)%s?") do
      if key and key ~= '' then
        order[#order + 1] = key
      end
    end
  end
  return order, i
end


-- function('1+2', 3)  -->  local x = '1+2'; function(x, 3)
--@param w Cmd4Lua
function M.cmd_add_local_definition(w)
  if w:is_input_valid() then
    error('TODO')
  end
end

--
-- local t = {
--   { k = v, },  --> { k = v, id = 1}
--   { k2 = v3, },  --> { k = v, id = 2}
-- }
--@param w Cmd4Lua
function M.cmd_add_id2table(w)
  local opts = w.vars.vim_opts or {}
  local next_id = w:def(0):optn('--next-id', '-i')

  if w:is_input_valid() then
    local lines = bu.get_selected_lines(opts, false)
    if not lines then
      if not opts.line1 or not opts.line2 then
        log.debug('no selected range(line1 or line2) in opts')
      end
      return false
    end
    local res = {}
    local prev_open
    for n, line in pairs(lines) do
      local i = su.last_indexof(line, '}')
      if not i then
        if prev_open == n - 1 then
          i = su.last_indexof(line, ',')
        else
          if line:sub(-1, -1) == '{' then
            prev_open = n
          end
        end
      end

      if i then
        while i > 0 and line:sub(i - 1, i - 1) == ' ' do
          i = i - 1
        end
        if i > 1 then -- skip end of table
          next_id = next_id + 1
          local sep = ','
          if line:sub(i - 1, i - 1) == ',' then
            sep = ''
          end
          local l, r = line:sub(1, i - 1), line:sub(i)
          line = l .. sep .. ' id = ' .. tonumber(next_id) .. r
        end
      end
      table.insert(res, line)
    end
    bu.set_selected_lines(opts, res)
    return true
  end
end

local function get_next_sub_table(i, lines)
  local line = lines[i]
  local has_start = string.match(line, "^%s*{")
  if has_start == nil or has_start == '' then
    error('expected lines with table got: i:' .. v2s(i) ..
      ' line:"' .. v2s(line) .. '" prev:' .. v2s(lines[i - 1]))
  end

  for j = i, #lines do
    line = lines[j]
    if string.match(line, "^%s*}") then
      return table.concat(lines, " ", i, j), j + 1
    end
  end
  return nil, -1
end

--
-- sort keys in the table in the given order
-- table must be onliner per each selected lines
--
---@param w Cmd4Lua
function M.cmd_sort_table_keys(w)
  w:usage('--order [id name props]')
  w:v_opt_verbose('-v')
  local order = w:desc('desired order of keys'):optl('--order', '-o')
  local multiline = w:desc('multiline table'):has_opt('--multiline', '-m')
  local sub = w:desc('subtables with multiline'):has_opt('--subtables', '-s')

  if not w:is_input_valid() then return end
  local lines, opts = get_lines_and_opts(w)
  if not lines then return end -- with errmsg

  local res = {}

  local parse = lua_parser.parse_busted_readable_table

  if w:is_verbose() then
    print('desired keys order', inspect(order))
  end


  if multiline then
    if sub then
      local i, limit = 1, 1000
      while i < #lines and limit > 0 do
        local body, j = get_next_sub_table(i, lines)

        local t = parse(body or '')
        if t then
          local s = (tu.table2code(t, '', nil, order) or E):gsub("\n", ' ')
          res[#res + 1] = s .. ','
        end
        if j < 0 or j >= #lines or limit == 0 then
          break
        end
        i = j
        limit = limit - 1
      end
    else
      local table_body = table.concat(lines, " ")
      local t = parse(table_body)
      if type(t) ~= 'table' then
        t = parse("{" .. table_body .. "}") -- try with {}
      end
      if type(t) ~= 'table' then
        return w:error('cannot parse lua-table from selected lines')
      else
        local s = (tu.table2code(t, '', nil, order) or E):gsub("\n", ' ')
        res = { s .. ',' }
      end
    end

    bu.set_selected_lines(opts, res)
    return
  end

  -- to format one-line tables like {k=1, b=2} (not support multiline tables)
  local c = 0
  for _, line in pairs(lines) do
    local tab, body, comma = string.match(line, '^(%s*)({.*})%s*(,?)%s*$')
    if body then
      local t = parse(body)
      if t then
        local s = (tu.table2code(t, '', nil, order) or E):gsub("\n", ' ')
        if s and s ~= '' then
          line = tab .. s .. comma
          c = c + 1
        end
      end
    end
    res[#res + 1] = line
  end

  if c == 0 then
    w:error("cannot detect lua table. try -m optkey if your table is multiline")
    return
  end

  bu.set_selected_lines(opts, res)
  return true
end

--
-- reorder table keys for formated multiline table with a given order
-- if the order of sorting the keys is not set, then sort by alphabetical order
--
-- {                                          {
--   desc = ...                                 desc = ... ,
--   default = ... + [desc,name,type]           name = ...,
--   exampl = ...                   -->         type = ...,
--   type = ...                                 exampl = ...,
--   name = ...                                 default = ...
-- }                                          }
--
---@param w Cmd4Lua
function M.cmd_reorder_multiline_keys(w)
  w:usage('--order [id name props]')
  w:v_opt_verbose('-v')
  local order = w:desc('desired order of the keys'):optl('--order', '-o')

  if not w:is_input_valid() then return end
  ---@cast order table

  local lines, opts = get_lines_and_opts(w)
  if not lines then return end -- with errmsg


  local function flush_to_res(res, buflines)
    if not buflines or not res then
      return
    end
    local priority_map = {}
    for _, key in ipairs(order or E) do
      priority_map[key] = true
    end

    local cnt = #buflines
    local map = {}
    -- Keys for which are not set as an order - which will be sorted in alphabetical order
    local ramains_keys = {}
    for i = 1, cnt do
      local line0 = buflines[i]
      local k, v = string.match(line0, '^%s+([^%s]+)%s*=%s*(.-)%s*$')
      if not k or not v then
        error('cannot parse line to kv: ' .. v2s(line0))
      end
      map[k] = line0
      if not priority_map[k] then
        ramains_keys[#ramains_keys + 1] = k
      end
    end
    table.sort(ramains_keys)

    for _, key in ipairs(order or E) do
      local line = map[key]
      if line ~= nil then
        res[#res + 1] = line
      end
    end

    for _, key in ipairs(ramains_keys) do
      local line = map[key]
      res[#res + 1] = line
    end
  end

  local res = {}
  local i, parent_ind, buflines = 1, nil, nil
  order, i = pickup_order_from_first_line(order, lines, res, i)

  while i <= #lines do
    local line = lines[i]
    local ind = string.match(line, '^(%s+).+$')
    if ind then
      if parent_ind == nil then
        parent_ind = ind
        buflines = {}
        res[#res + 1] = line
      elseif #ind == #parent_ind then
        flush_to_res(res, buflines)
        res[#res + 1] = line
        parent_ind = nil
      elseif #ind > #parent_ind then
        buflines[#buflines + 1] = line
      end
    end
    i = i + 1
  end

  bu.set_selected_lines(opts, res)
  return true
end

--
-- extract table keys from selected lines
--
---@param w Cmd4Lua
function M.cmd_extract_table_keys(w)
  local to_cb = w:desc('copy to clipboard only'):has_opt('--to-clipboard', '-c')
  local qs = w:desc('wrap to single quotes'):has_opt('--single-quoted', '-s')
  local qd = w:desc('wrap to double quotes'):has_opt('--double-quoted', '-d')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false)
  local keys = ''
  local q = ''
  if qs then q = "'" elseif qd then q = '"' end
  local sep = q == '' and " " or ", "

  if lines and #lines > 0 then
    if #lines == 1 then
      error('Not implemented yet for single line table ')
    else
      for _, line in ipairs(lines) do
        local key, _ = string.match(line, "^%s*([^%s]+)%s*=%s*(.*)$")
        if key then
          if keys ~= '' then
            keys = keys .. sep
          end
          keys = keys .. q .. key .. q
        end
      end
    end
  end
  if to_cb then
    uclipboard.copy_to_clipboard(keys)
    w:say('copied!')
  else
    bu.insert_line_to(keys, { cursor_row = opts.line1 })
  end
end

--
-- for a word under the cursor in current line generate method body
-- self.obj:method(params)   ->  function Obj:method(params) .. end
-- if word is a method name of instance of same class and has mappings for
-- this class -> paste generated code to the end of this class-file
-- class->file mappings setups manualy via :EnvLinesModify lua mappings
-- for fast setup set cursor to the class name in source file of this class
-- and run :EnvLinesModify lua mappings --class . --file .
--
---@param w Cmd4Lua
function M.cmd_gen_method_body(w)
  if not w:is_input_valid() then return end
  local editor, ctx, method, code

  editor = Editor:new()
  ctx = editor:resolveWords().ctx
  method = ctx.word_element

  if not method or LEXEME_TYPE.METHOD ~= ctx.lexeme_type then
    print('Not Found method')
    return
  end

  -- fbs- function-block-{start,end}
  local classname, method_body, fbs, fbe = 'C', '', nil, nil
  local params = ctx.current_line:match('%((.+)%)$')

  -- self.instance:method(params)
  local instance = ctx.word_container:match('%.([%w]+)$')
  if instance then
    classname = instance:sub(1, 1):upper() .. instance:sub(2)
  end

  -- return self.instance:method(param) --> add "return self.parent" to method body
  if ctx.current_line:match('^%s*return ') then
    fbs, fbe = lua_parser.find_function_block_range(ctx.bufnr, ctx.cursor_row)
    if not fbs or not fbe then
      print('Cannot find range of the function block')
      -- return
    end
    local lines = ctx:getLines(ctx.cursor_row + 1, fbe)
    if instance and lines and #lines > 0 then
      local last = lines[#lines]
      if last:match('^%s*return self') then
        lines[#lines] = last:match('^(%s*)') .. 'return self.parent'
      end
    end
    method_body = table.concat(lines, "\n")
  end

  if instance and ctx.word_container:match('^self') then
    method_body = method_body:gsub('self%.' .. instance, 'self')
  end

  code = string.format("function %s:%s(%s)\n%s\nend\n",
    classname, method, params, method_body
  )
  local lines = su.split_range(code, "\n")
  ---@cast lines table

  if classname and mappings.classes[classname] and type(lines) == 'table' then
    local file = mappings.classes[classname]
    -- past generated code to the end of the class file
    if editor:insertLinesToFile(file, lines, -1) then
      if fbe then
        -- remove replaced lines
        editor:setLines(ctx.bufnr, ctx.cursor_row, fbe, {})
      end
      return true
    end
  end
  ---@cast fbe number

  -- past the code into the current buffer
  ctx:setLines(ctx.cursor_row + 1, fbe, lines)
  ctx:selectLines(ctx.cursor_row + 1, ctx.cursor_row + #lines)

  return true
end

--------------------------------------------------------------------------------

local function define_opt_view(w)
  local view = w:desc('open result in table-viewer')
      :has_opt('--view', '-v')
  local open_node = w:desc('open a node path(cd)')
      :optl('--open-node', '-o')

  return view, open_node
end

--
-- mappings of sources Class -> file
-- used to past generated code into specified in this mappings buffer
--
---@param w Cmd4Lua
function M.cmd_mappings(w)
  local view, open_node = define_opt_view(w)
  local class = w:desc('mappings for class'):opt('--class', '-c')
  local file = w:desc('full file name'):opt('--file', '-f')
  -- actions
  local show = w:desc('mappings for class'):has_opt('--show', '-s')
  local remove = w:desc('mappings for class'):has_opt('--remove', '-r')
  local dry_run = w:has_opt('--dry-run', '-d', 'only show without saving')

  if file == '.' or file == 'current' then
    file = vim.api.nvim_buf_get_name(0)
  end

  if class == '.' or class == 'cursor' then
    class = Editor:new():resolveWords().ctx.word_element
  end

  if not w:is_input_valid() then return end

  if view then
    tviewer.open(mappings, 'lua-mappings', open_node)
    return
  end

  local pref = ''
  if dry_run then
    pref = '[dry-run] Will be '
    print('class:', class, 'file:', file)
  end

  if class then
    if show then
      print(class .. ': ' .. tostring(mappings.classes[class]))
    elseif remove then
      if mappings.classes[class] then
        if not dry_run then
          mappings.classes[class] = nil
        end
        print(pref .. 'removed class:' .. tostring(class))
      else
        print('not found in classes' .. tostring(class))
      end
    end
  elseif show then
    print(inspect(mappings))
  end

  if class and file then
    local value = nil
    if not dry_run then
      mappings.classes[class] = file
      value = mappings.classes[class]
    else
      value = file
    end
    print(pref .. class .. ': ' .. tostring(value))
  end
end

--
-- minimalize require import for all selected lines
-- local x = require("pkg.x")  --> local x = require 'pkg.x'
--
---@param w Cmd4Lua
function M.cmd_format_require(w)
  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}

  if not opts.line1 or not opts.line2 then
    return w:error('no selected range(line1 or line2) in opts')
  end
  local lines = bu.get_selected_lines(opts, false)
  if not lines then
    return w:error('no lines')
  end

  local res = {}
  for _, line in ipairs(lines) do
    local vn, mod, tail = lua_parser.parse_line_require(line)
    if vn and mod then
      line = fmt("local %s = require '%s'%s", vn, mod, tail or '')
    end
    res[#res + 1] = line
  end

  bu.set_selected_lines(opts, res)
  return true
end

-- assert.same('self.collector', ctx.word_container)
-- assert.same('annotation_root', ctx.word_element)

return M

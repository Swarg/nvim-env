-- 23-05-2024 @author Swarg
--
-- Goals:
--  - interact with vtt-subtitles
--

local vtt = require 'env.langs.vtt.snippets'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvLinesModify vtt
function M.handle(w)
  w:about('Modify the Selected Lines in vtt subtitles')
      :handlers(M)

      :desc('join selected two vvt-entry into one')
      :cmd('join', 'j')

      :desc('sync end timestamp of the current entry with next')
      :cmd('sync', 's')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


--
-- join selected parts into one
--
---@param w Cmd4Lua
function M.cmd_join(w)
  if not w:is_input_valid() then return false end

  local ok, errmsg = vtt.handler_join_two_to_one()

  if not ok then w:error(tostring(errmsg)) end
end

--
-- sync end timestamp of the current entry with next
--
---@param w Cmd4Lua
function M.cmd_sync(w)
  if not w:is_input_valid() then return end

  local ok, errmsg = vtt.handler_sync_two_timestamps()
  if not ok then w:error(tostring(errmsg)) end
end

return M

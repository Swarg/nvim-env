-- 14-01-2024 @author Swarg
local M = {}
--
local bu = require('env.bufutil')
local u8 = require("env.util.utf8")
local upsql = require 'env.util.psql'
local utbl = require 'env.util.tables'
local umarkdown = require 'env.util.markdown'


-- :EnvLinesModify md
-- change selected lines in md file(buffer)
function M.handle(w)
  w:about('Modify the Selected Lines in markdown file')
      :handlers(M)

      :desc('Convert selected lines to List of elements')
      :cmd("to-list", "tl")

      :desc('Build the content from titles in the current buffer')
      :cmd("build-content", "bc")

      :desc('Extract all http(s)-urls from the current buffer')
      :cmd("extract-urls", "eu")

      :desc('Extract url and title from html code with some article')
      :cmd("extract-url-and-title", "eut")

      :desc('Align the anchor in the title bar to the right')
      :cmd("align-anchor", "aa")

      :desc('interact with tables')
      :cmd("table", "t")

      :run()
end

local T = {}
M.table = T -- testing

--
-- interact with tables
-- EnvLM md table
--
---@param w Cmd4Lua
function M.cmd_table(w)
  w:about('table in markdown')
      :handlers(T)

      :desc('format sql-like table into markdown syntax')
      :cmd("format-sql", "fs")

      :desc('create markdown table from formated text')
      :cmd("from-formated-text", "fft")

      :desc('create markdown table from raw text')
      :cmd("from-raw-text", "frt")

      :desc('format compressed markdown table with spaces')
      :cmd("reformat", "rf")

      :run()
end

local sql_lines_to_table_obj = upsql.lines_to_table_obj
local fmt_table_obj_to_lines = upsql.fmt_table_obj_to_lines

--------------------------------------------------------------------------------

local DEF_WIDTH = 80
local fmt = string.format

-- html <ul> - the Unordered List element (not --ordered)
---@param w Cmd4Lua
function M.cmd_to_list(w)
  local ordered = w:desc('Create ordered list with numbers')
      :has_opt('--ordered', '-o')

  local opts = w.vars.vim_opts or {}

  if w:is_input_valid() then
    if opts.line1 and opts.line2 then
      local lines = bu.get_selected_lines(opts, false) or {}
      local res = {}
      local root_tabn = #(string.match(lines[1] or '', '^(%s+)') or '')
      local indexes = { root_tabn = 0 }
      local prev_tabn = root_tabn

      for _, line in ipairs(lines) do
        local tabn = #(string.match(line, '^(%s+)') or '')
        local n = indexes[tabn]
        if n == nil then
          indexes[tabn], n = 1, 1
        elseif tabn < prev_tabn then
          indexes[prev_tabn] = 1
        end
        indexes[tabn] = indexes[tabn] + 1

        local pref
        if ordered then
          pref = tostring(n) .. '. '
          n = n + 1
        else
          pref = '- '
        end

        if tabn > root_tabn then
          pref = fmt('%' .. tostring(tabn - root_tabn) .. 's', ' ') .. pref
        end

        res[#res + 1] = string.gsub(line, '^%s*', pref)
        prev_tabn = tabn
      end

      bu.set_selected_lines(opts, res)
      return true
    else
      print('No selected lines')
    end
  end
end

local function get_lines_range(w)
  return w.vars.selection and bu.get_selection_range(0, w.vars.vim_opts) or
      bu.get_all_range(0)
end

--
-- Build the content from titles in the current buffer
-- by default for all current buffer
--
---@param w Cmd4Lua
function M.cmd_build_content(w)
  w:desc('apply only to selected lines'):v_has_opt('--selection', '-s')

  local urls = w:desc('collect the urls to build referenses list')
      :has_opt('--urls', '-u')

  if not w:is_input_valid() then return end

  local opts = get_lines_range(w)

  local lines = bu.get_selected_lines(opts, false) or {}

  -- starts from last buf line to keep it on set_selected_lines
  -- if this is not done, it will be lost when inserted
  local content, refs = { lines[#lines], '' }, {}

  for _, line in ipairs(lines) do
    local tab, title = string.match(line, '^##([#]*)%s+(.*)$')
    if title and title ~= '' then
      tab = string.gsub(tab, '#', '  ')
      content[#content + 1] = tab .. '- ' .. title
    end
    if urls then
      local url = string.match(line, '(https?://[^%s%)%(]+)')
      if url and url ~= '' then
        refs[#refs + 1] = url
      end
    end
  end

  if #refs > 0 then
    content[#content + 1] = ''
    for _, url in ipairs(refs) do
      content[#content + 1] = url
    end
  end

  bu.set_selected_lines({ line1 = opts.line2, line2 = opts.line2 }, content)
  vim.cmd [[ normal! G ]] -- jump to the end
end

--
-- Extract all http(s)-urls from the current buffer
--
---@param w Cmd4Lua
function M.cmd_extract_urls(w)
  w:v_opt_quiet('-q')
  w:desc('apply only to selected lines'):v_has_opt('--selection', '-s')

  if not w:is_input_valid() then return end

  local opts = get_lines_range(w)

  local lines = bu.get_selected_lines(opts, false) or {}

  local refs = { lines[#lines], '' }

  for _, line in ipairs(lines) do
    local url = string.match(line, '(https?://[^%s%)%(]+)')
    if url and url ~= '' then
      refs[#refs + 1] = url
    end
  end

  if #refs > 2 then
    bu.set_selected_lines({ line1 = opts.line2, line2 = opts.line2 }, refs)
    w:say('found urls: ', #refs)
    vim.cmd [[ normal! G ]]
  else
    print('not found any http(s) urls')
  end
end

--
-- Extract url and title from html code with some article
--
---@param w Cmd4Lua
function M.cmd_extract_url_and_title(w)
  if not w:is_input_valid() then return end
  local opts = bu.get_selection_range(0, w.vars.vim_opts)
  local lines = bu.get_selected_lines(opts, false) or {}
  -- data-link="/en/time-lua/"
  local link, title = nil, nil
  for _, line in pairs(lines) do
    if not link then
      link = string.match(line, '<div data%-link="([^"]+)"')
    else
      title = string.match(line, '^%s*<h1 class="[^"]+">([^<]+)</h1>')
      if title ~= nil then
        break
      end
    end
  end
  if not link or not title then
    w:error("not found link and title from the html code of the article")
  end
  bu.set_selected_lines({ line1 = opts.line1, line2 = opts.line2 }, { title, link })
end

--
-- Align the anchor in the title bar to the right
--
--   ## some title          anchor
--
-- used to build title-contents
--
---@param w Cmd4Lua
function M.cmd_align_anchor(w)
  -- w:desc('apply to all lines in the current buffer'):v_has_opt('--all', '-a')
  local width = w:desc('line width'):def(DEF_WIDTH):optn('--width', '-w')
  local anchor = w:desc('anchor'):optional():pop():arg()

  local title_deep = w:desc('markdown title deep')
      :def(2):optn('--title-deep', '-d')

  if not w:is_input_valid() then return end
  w.vars.selection = true
  local opts = get_lines_range(w)

  local lines = bu.get_selected_lines(opts, false) or {}
  local res = {}
  local slen = u8.utf8_slen

  --
  local function build_new_line(left, _width, _anchor, td)
    local prefix = ''
    _width = _width or DEF_WIDTH
    if td and td > 0 then
      local actualtd = #(string.match(left, '^([#]+)%s') or '')
      if actualtd < td then
        prefix = string.rep('#', td - actualtd, '') .. ' '
      end
    end
    local remains = _width - (slen(left) + slen(_anchor) + #prefix) - 1
    local pad = string.format("%" .. tostring(remains) .. "s", ' ')

    return prefix .. left .. pad .. anchor
  end

  -- for one current line with given anchor byargument
  if anchor and anchor ~= '' then
    local line = lines[1]
    res = lines
    res[1] = build_new_line(line, width, anchor, title_deep)
  else
    -- with search of anchor in selected lines
    for _, line in ipairs(lines) do
      -- local anchor0 = string.match(line, '^###?%s+.*(%s+[^%s]+)%s*$')
      local anchor0 = string.match(line, '(%s+[^%s]+)%s*$')
      if anchor0 and anchor0 ~= '' then
        local i = string.find(line, anchor0, 1, true)
        if i then
          anchor = string.match(anchor0, '^%s+([^%s]+)%s*$')
          if anchor and anchor ~= '' then
            local left = line:sub(1, i - 1)
            line = build_new_line(left, width, anchor, title_deep)
          end
        end
      end
      res[#res + 1] = line
    end
  end

  bu.set_selected_lines(opts, res)
end

--------------------------------------------------------------------------------

--
-- interact with sql-tables
--
---@param w Cmd4Lua
function T.cmd_format_sql(w)
  w:usage("EnvLM markdown table format -s '\t'")
  w:usage("EnvLM markdown table format -s '  ' -v '|'")
  local sep = w:desc('cell separator'):def('|'):opt('--separator', '-s')
  local vsep = w:desc('visual cell separator'):def('|'):opt('--visual', '-v')
  w:desc('to markdown table'):v_has_opt('--markdown', '-m')
  local raw = w:desc('show the raw table (debugging)'):has_opt('--raw', '-r')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}

  local lines = bu.get_selected_lines(opts, false) or {}

  if sep == '\\t' then sep = "\t" end
  if sep == '\\n' then sep = "\n" end
  if sep == '\\r' then sep = "\r" end

  local tbl = sql_lines_to_table_obj(lines, sep, w.vars)
  if raw then return w:say(vim.inspect(tbl)) end -- debugging

  local res = fmt_table_obj_to_lines(tbl, vsep, opts)
  bu.set_selected_lines(opts, res)
end

--
-- create markdown table format formated text
--
---@param w Cmd4Lua
function T.cmd_from_formated_text(w)
  local raw = w:desc('show the raw table (debugging)'):has_opt('--raw', '-r')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false) or {}

  local tbl = utbl.lines_to_table_obj(lines, w.vars)
  if raw then return w:say(vim.inspect(tbl)) end -- debugging

  w.vars.markdown = true
  local res = fmt_table_obj_to_lines(tbl, '|', w.vars)
  bu.set_selected_lines(opts, res)
end

--
-- reformat markdown table
-- col1 | col2 |                  col1 | col2          |
-- ---- | ---- |            -->   ---- | ------------- |
-- cell | short |                 cell | short         |
-- cell | something big |         cell | something big |
--
---@param w Cmd4Lua
function T.cmd_reformat(w)
  local raw = w:desc('show the raw table (debugging)'):has_opt('--raw', '-r')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false) or {}
  local tbl = umarkdown.lines_to_table_obj(lines)

  if raw then return w:say(vim.inspect(tbl)) end -- debugging

  w.vars.markdown = true
  local res = fmt_table_obj_to_lines(tbl, '|', w.vars)
  bu.set_selected_lines(opts, res)
end

--
-- format raw text with tab separated columns into markdown table
--
--  "col1\tcol2\tcol3"          col1 | col2 | col3
--  "a\tb\tc"            -->    ---- | ---- | -----
--  "d\te\tf"                   a    |  b   | c
--                              d    |  e   | f
--
---@param w Cmd4Lua
function T.cmd_from_raw_text(w)
  local raw = w:desc('show the raw table (debugging)'):has_opt('--raw', '-r')
  local sep = w:desc('column separator'):def('\t'):opt('--sep', '-s')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  if sep == '\t' then sep = "\t" end

  local lines = bu.get_selected_lines(opts, false) or {}
  local tbl = utbl.parse_raw_lines_to_table_obj(lines, sep)

  if raw then return w:say(require 'inspect' (tbl)) end -- debugging

  w.vars.markdown = true
  local res = fmt_table_obj_to_lines(tbl, '|', w.vars)
  bu.set_selected_lines(opts, res)
end

return M

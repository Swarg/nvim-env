-- 13-03-2024 @author Swarg
-- Goals:
--  - make logs more readable(by replace datetime and log level
--

local bu = require('env.bufutil')
---@diagnostic disable-next-line: unused-local
local log = require('alogger')
---@diagnostic disable-next-line: unused-local
local Cmd4Lua = require("cmd4lua")

local M = {}


--------------------------------------------------------------------------------
--                       LUA SOURCE CODE
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('make logs more readable by removing datatime and log-level')
      :cmd("mk-readable", "r")

      :run()
end

--------------------------------------------------------------------------------


--
-- make logs more readable by removing datatime and log-level
--
---@param w Cmd4Lua
function M.cmd_mk_readable(w)
  local all = w:desc('apply to all file'):has_opt('--all', '-a')

  if not w:is_input_valid() then return end

  local lines, opts = nil, w.vars.vim_opts or {}
  -- [date time] [loglvl] ...
  local pattern = '^%[%d...%-..%-.. ..:..:.%d%] %[[^%s]+%] '
  local function apply(input, ptrn)
    local t = {}
    for _, line in ipairs(input) do
      t[#t + 1] = string.gsub(line, ptrn, '', 1)
    end

    return t
  end

  if all then opts.line1, opts.line2 = 0, -1 end

  lines = bu.get_selected_lines(opts, false) or {}
  lines = apply(lines, pattern)
  bu.set_selected_lines(opts, lines)
end

return M

-- 27-02-2025 @author Swarg
-- Goals:
--  - compare two sets of lines picked from specifed nvim buffer's to:
--    - find out unique lines in two sets of lines (ful)
--      e.g. to compare two file sets to to find the differences

-- local log = require 'alogger'
local tu = require 'env.util.tables'
local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvCompare')
      :about('Comparison of several data to search for differences')
      :handlers(M)

      :desc('pick lines from current opened buffer to compare')
      :cmd("lines-pick", 'lp')

      :desc('clear selected lines to compare')
      :cmd("lines-clear", 'lc')

      :desc('show selected lines to compare')
      :cmd("lines-show-selected", 'lss')

      :desc('compare two sets of lines to find unique lines')
      :cmd("find-unique-lines", 'ful')

      :run()
end

local _commands = {
  'diff'
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local selected_lines1, selected_lines1_name = nil, nil
local selected_lines2, selected_lines2_name = nil, nil

local function create_new_buffer()
  local curbufnr = vim.api.nvim_get_current_buf()
  vim.cmd(":enew")
  --  newbufnr = vimv.api.nvim_create_buf(true, true)
  local newbufnr = vim.api.nvim_get_current_buf()
  assert(curbufnr ~= newbufnr, 'ensure new buffer created')
  return newbufnr
end
--
-- fork from `EnvRefactor stacktrace compare`
-- define:  bufnr1, lnum1, lnum_end1, bufnr2, lnum2, lnum_end2
---@param w Cmd4Lua
local function define_lnum_and_buf(w)
  w:desc("bufnr with the first lines"):def(0):v_optn("--bufnr1", "-b1")
  w:desc("bufnr with the second lines"):def(0):v_optn("--bufnr2", "-b2")

  w:desc('lnum of the first sections of lines'):def(0):v_optn("--lnum1", "-l1")
  w:desc('lnum of the second sections of lines'):def(0):v_optn("--lnum2", "-l2")
  w:desc('lnum-end of the first sections of lines'):def(-1):v_optn("--lnum-end1", "-e1")
  w:desc('lnum-end of the second sections of lines'):def(-1):v_optn("--lnum-end2", "-e2")
end

-- pick lines from
---@param bufnr number?
---@param ln number?
---@return table? <string> of lines
---@return number the lnum with first StackTraceEntry
local function get_lines_from_buf(bufnr, ln, lnum_end)
  bufnr = bufnr or 0
  ln = ln or 0
  local end_is_defined = type(lnum_end) == 'number' and lnum_end > 0
  lnum_end = lnum_end or vim.api.nvim_buf_line_count(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, ln, lnum_end, true)
  if (lines == nil or #lines < 1) then
    return nil, -1
  end
  local t = {}
  for i = 1, #lines do
    local line = lines[i]
    if not end_is_defined and (line == nil or line == '') then
      break
    end
    t[#t + 1] = lines[i]
  end

  return t, ln
end

--
--
--
---@param bufnr number?
---@param lnum number?
---@param lnum_end number?
---@return table?
---@return string? name
local function pick_lines(bufnr, lnum, lnum_end)
  bufnr = bufnr or 0
  local is_lnum_end_defined = type(lnum_end) == 'number' and lnum_end > 0
  lnum = lnum or vim.api.nvim_win_get_cursor(0)[1]
  lnum_end = lnum_end or vim.api.nvim_buf_line_count(bufnr)
  local offset = 0
  if (lnum > 0) then
    lnum = lnum - 1
    offset = 1
  end

  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum_end, true)
  if (lines == nil or #lines < 1) then
    return nil, nil
  end

  local t, name = {}, nil
  for i = 1 + offset, #lines do
    local line = lines[i]
    if not is_lnum_end_defined and (line == nil or line == '') then
      break
    end
    t[#t + 1] = lines[i]
  end
  if offset == 1 then
    name = lines[1]
  end
  return t, name
end

local function add_all(t, src)
  for _, line in ipairs(src) do
    t[#t + 1] = line
  end
end
local function add(t, line)
  t[#t + 1] = line
end

----

--
-- pick lines from current opened buffer to compare
--
---@param w Cmd4Lua
function M.cmd_lines_pick(w)
  local bn = w:desc('the bufnr with lines to pick'):def(0):optn("--bufnr", "-b")
  local ln = w:desc('the lnum of begining of the lines to pick'):optn("--lnum", "-l")
  local lne = w:desc('the lnum of ends of the lines to pick'):optn("--lnum-end", "-e")

  if not w:is_input_valid() then return end

  if selected_lines1 == nil then
    selected_lines1, selected_lines1_name = pick_lines(bn, ln, lne)
    w:say('selected lines1: ' .. v2s(#(selected_lines1 or E)) .. " " .. selected_lines1_name)
  elseif selected_lines2 == nil then
    selected_lines2, selected_lines2_name = pick_lines(bn, ln, lne)
    w:say('selected lines2: ' .. v2s(#(selected_lines2 or E)) .. " " .. selected_lines2_name)
  else
    return w:error("two set of lines is already selected.(clear before)")
  end
end

--
-- clear selected lines to compare
--
---@param w Cmd4Lua
function M.cmd_lines_clear(w)
  local n = w:desc('set of lines to clear'):optn("--lines-set", "-s")
  if not w:is_input_valid() then return end
  if n == 1 then
    selected_lines1 = nil
    w:say('cleaned lines1 ' .. v2s(selected_lines1_name))
    selected_lines1_name = nil
  elseif n == 2 then
    selected_lines2 = nil
    w:say('cleaned lines2 ' .. v2s(selected_lines2_name))
    selected_lines2_name = nil
  else
    selected_lines1, selected_lines1_name = nil, nil
    selected_lines2, selected_lines2_name = nil, nil
    w:fsay('cleaned lines1(%s) & lines2(%s)', selected_lines1_name, selected_lines2_name)
  end
end

--
-- show selected lines to compare
--
---@param w Cmd4Lua
function M.cmd_lines_show_selected(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end

  if w:is_verbose() and (selected_lines1 or selected_lines2) then
    local t = {}
    if selected_lines1 then
      add(t, (selected_lines1_name or 'lines1') .. " " .. #selected_lines1)
      add_all(t, selected_lines1)
    end
    if selected_lines2 then
      add(t, (selected_lines2_name or 'lines2') .. " " .. #selected_lines2)
      add_all(t, selected_lines2)
    end
    local bufnr = create_new_buffer()
    vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, t)
    return
  end

  if selected_lines1 == nil then
    w:say('no selected_lines1')
  else
    w:say('lines1: ' .. v2s(#selected_lines1) .. ' ' .. v2s(selected_lines1_name))
  end

  if selected_lines2 == nil then
    w:say('no selected_lines2')
  else
    w:say('lines1: ' .. v2s(#selected_lines2) .. ' ' .. v2s(selected_lines2_name))
  end
end

---@return table?
---@return table?
local function get_lines_sets_by_opts(w)
  local v = w.vars
  if v.bufnr1 == v.bufnr2 and v.lnum1 == v.lnum2 and v.lnum_end1 == v.lnum_end2 then
    return w:error("same sections with a lines. nothing to compare!")
  end

  local lines1 = get_lines_from_buf(v.bufnr1, v.lnum1, v.lnum_end1)
  local lines2 = get_lines_from_buf(v.bufnr2, v.lnum2, v.lnum_end2)
  if lines1 == nil or #lines1 < 1 then
    return w:error('not found the first lines');
  end
  if lines2 == nil or #lines1 < 1 then
    return w:error('not found the second lines');
  end
  return lines1, lines2
end


--
-- compare two section of lines in one or multiple buffers
-- Find unique elements in the given lines by considering lines as a set of values
--
---@param w Cmd4Lua
function M.cmd_find_unique_lines(w)
  define_lnum_and_buf(w)
  if not w:is_input_valid() then return end

  local lines1, lines2 = selected_lines1, selected_lines2
  local name1, name2 = selected_lines1_name, selected_lines2_name

  if not lines1 or not lines2 then
    lines1, lines2 = get_lines_sets_by_opts(w)
    name1, name2 = 'lines1', 'lines2'
    if w:has_error() then return end -- with errmsg
  else
    w:say('compare already selected (via lines-pick) sets of lines')
  end
  if not lines1 then return w:error("no lines1") end
  if not lines2 then return w:error("no lines2") end

  -- compare
  local no_in_line1, no_in_line2, has_in_both = {}, {}, {}

  local lines1_map = tu.list_to_map(lines1, true)
  local lines2_map = tu.list_to_map(lines2, true)

  for line1, _ in pairs(lines1_map) do
    if not lines2_map[line1] then
      no_in_line2[#no_in_line2 + 1] = line1
    else
      has_in_both[#has_in_both + 1] = line1
    end
  end

  for line2, _ in pairs(lines2_map) do
    if not lines1_map[line2] then
      no_in_line1[#no_in_line1 + 1] = line2
    end
  end

  -- build reporting

  local report_lines = {}

  if #no_in_line2 > 0 then
    add(report_lines, "# Lines from lines1 which are not in lines2:")
    add(report_lines, "# (lines which are only in " .. v2s(name1) .. ')')
    add_all(report_lines, no_in_line2)
    add(report_lines, '')
  end
  if #no_in_line1 > 0 then
    add(report_lines, "# Lines from lines2 which are not in lines1:")
    add(report_lines, "# (lines which are only in " .. v2s(name2) .. ')')
    add_all(report_lines, no_in_line1)
    add(report_lines, '')
  end
  if #has_in_both > 0 then
    add(report_lines, "# Lines that are in both " .. v2s(name1) .. ' & ' .. v2s(name2))
    add_all(report_lines, has_in_both)
    add(report_lines, '')
  end

  local bufnr = create_new_buffer()
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, report_lines)
end

return M

-- 05-11-2023 @author Swarg
local M = {}

--
-- sources:
-- /usr/share/nvim/runtime/lua/vim/lsp.lua
-- /usr/share/nvim/runtime/lua/vim/lsp/buf.lua
--
local fs = require('env.files')
local su = require 'env.sutil'
---@diagnostic disable-next-line
local log = require('alogger')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local nvim_lsp = require("env.util.nvim_lsp")
local tviewer = require("env.util.tbl_viewer")
local projects_cache = require 'env.cache.projects'
local ObjEditor = require 'env.ui.ObjEditor'
local Lang = require 'env.lang.Lang'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect


--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvLsp')
      :about('Nvim-Lsp Research tool')
      :handlers(M)

      :desc('the list of current lsp clients')
      :cmd("clients", "c")

      :desc('launch lsp server for current buffer')
      :cmd("start", "run")

      :desc('stop already running lsp server for current lsp-client')
      :cmd("stop", "S")

      :desc('checks the projects configuration and updates the dependencies')
      :cmd("refresh-project", "rp")

      :desc('Get list of a server capabilities available to lsp-clients')
      :cmd("server-capabilities", "sc")

      :desc('Executes an LSP server command')
      :cmd("execute_command", "ec")

      :desc('Get the lsp symbols of the all current buffer')
      :cmd("doc-symbols", "ds")

      :desc('Lists all the references to the symbol under the cursor')
      :cmd("references", "r")

      :desc('Get the Infos about symbol under the cursor in current buffer')
      :cmd('symbol-info', 'si')

      :desc('Position info about nvim_cursor and lsp position in the buffer')
      :cmd("position", "p")

      :desc('resolve word-type under the cursor in the current line (debugging)')
      :cmd("resolve-words", "rw")

      :desc('find a specified block type from source code')
      :cmd("find-block-range", "fbr")

      :run()
      :exitcode()
end

local _commands = { "doc-symbols", "server-capabilities", "clients",
  "find-block-range", "position", "symbol-info", "references" }
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- helpers

-- same options for frequent use
local function define_opt_view(w)
  local view = w:desc('open result in table-viewer')
      :has_opt('--view', '-v')
  local open_node = w:desc('open a node path(cd)')
      :optl('--open-node', '-o')

  return view, open_node
end

local function open_in_tviewer(obj, bufnr, name, open_node)
  name = name or ''
  name = 'nvim-lsp-' .. tostring(name) .. '-' .. tostring(bufnr)
  tviewer.open_in_buf(obj, name, open_node, nvim_lsp.mk_node_name)
end

---@param dir string
---@param ext string
local function get_first_opened_file(dir, ext)
  if not dir or not ext or dir == '' then
    return nil
  end
  local found = nil
  for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
    if vim.api.nvim_buf_is_loaded(bufnr) then
      local mod = vim.api.nvim_buf_get_option(bufnr, 'modifiable')
      if mod == true then
        local name = vim.api.nvim_buf_get_name(bufnr)
        if name and su.ends_with(name, ext) and su.starts_with(name, dir) then
          return name
        end
      end
    end
  end
  log.debug("get_first_opened_file found:%s for ext:%s in: %s", found, ext, dir)
  return found
end

-------------------------------------------------------------------------------

---@param w Cmd4Lua
local function update_java_project_for_curr_file(w)
  local ok, jdtls = pcall(require, 'jdtls')
  if ok and type(jdtls) == 'table' then
    jdtls.update_project_config()
  else
    w:error('Not found jdtls')
  end
end

---@return boolean
local function refresh_java_project(w, bufname, ext)
  if ext == 'java' then
    update_java_project_for_curr_file(w)
    return true
  end

  if ext == 'gradle' or ext == 'pom' then
    local lang = Lang.findOpenedProjectByPath(bufname)
    if not lang then
      w:error("not found opened java project for a current opened file")
      return true
    end ---@cast lang env.lang.Lang
    local fn = get_first_opened_file(lang:getProjectRoot(), 'java')
    if not fn then
      w:error('to update jdtls you must have opened at least one source file')
      return true
    end

    -- change current file to any java file in same project
    vim.cmd(':e ' .. v2s(fn))
    update_java_project_for_curr_file(w)
    vim.cmd(':e ' .. v2s(bufname)) -- back to original file
    return true
  end

  return false
end

--
-- checks the projects configuration and updates the dependencies
--
---@param w Cmd4Lua
function M.cmd_refresh_project(w)
  if not w:is_input_valid() then return end

  local bufname = vim.api.nvim_buf_get_name(0)
  -- local clients = vim.lsp.get_active_clients()
  local ext = string.match(bufname or '', '%.([^%.]+)$')

  if refresh_java_project(w, bufname, ext) then return end

  w:error('unsupported file: ' .. tostring(ext))
end

local root_markers = {
  ".git", "Makefile", "mvnw", "gradlew", "pom.xml", "build.gradle"
}
--
-- from ~/.config/nvim/lua/user/lsp/settings/jdtls.lua
---@return string? root
---@return string? ext
local function get_project_root()
  local bufname = vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
  local ext = fs.extract_extension(bufname)
  -- jdt
  local project_root = projects_cache.find_root(root_markers, bufname)
  -- project_root = require("jdtls.setup").find_root(root_markers)
  return project_root, ext
end

local function update_asked_java_proj(dir, value)
  if type(vim.g.java_asked_project_names) == 'table' then
    local java_asked_project_names = vim.g.java_asked_project_names
    java_asked_project_names[dir] = value
    vim.g.java_asked_project_names = java_asked_project_names
  end
end

--
-- launch lsp server for current buffer
--
---@param w Cmd4Lua
function M.cmd_start(w)
  w:v_opt_verbose('-v')

  if not w:is_input_valid() then return end

  local dir, ext = get_project_root()
  if not dir then
    return w:error('not found project root')
  end

  -- reset already asked per-session settings
  if ext == 'java' then
    update_asked_java_proj(dir, nil)
    if w:is_verbose() then
      print(inspect(vim.g.java_asked_project_names))
    end
  end

  vim.cmd(':LspStart')
end

--
-- stop already running lsp server for current lsp-client
-- :LspStop
--
---@param w Cmd4Lua
function M.cmd_stop(w)
  w:v_opt_verbose('-v')

  local index = w:desc('id of the client'):pop():argn()

  if not w:is_input_valid() then return end

  local ok, clients = pcall(vim.lsp.get_active_clients)
  if not ok then
    return w:error('cannot get acive lsp.clients')
  end

  for i, e in ipairs(clients) do
    if type(e) == 'table' then
      local buffs = e.attached_buffers or E
      local ws = (e.workspaceFolders or E)[1] or E

      print(i, e.name, ws.name, ws.uri, log.format("buffs: %s", buffs))
      if w:is_verbose() then print(inspect(e)) end

      if i == index then
        if type(e.stop) == 'function' then
          e.stop()
          return w:say 'stopped'
        end
      end
    end
  end
end

---@param w Cmd4Lua
function M.cmd_clients(w)
  w:usage(':EnvLsp clients --client-id 1 --view  # to see in the TableViewer')
  local view, open_node = define_opt_view(w)
  local vim_lsp = w:desc('vim.lsp')
      :group('A', 1):has_opt('--vim-lsp', '-p')

  local client_id = w:desc('lsp client-id')
      :group('A', 1):optn('--client-id', '-c')

  local list = w:desc('list of the lsp client-ids')
      :group('A', 1):has_opt('--list', '-ls')

  if w:is_input_valid() then
    local obj, clients, name = nil, nil, 'active-clients'

    if vim_lsp then
      name = 'vim.lsp'
      obj = (vim or {}).lsp
    else
      local ok
      ok, clients = pcall(vim.lsp.get_active_clients)
      if not ok then
        print('Error: ' .. tostring(clients))
        return
      end
      if client_id ~= nil then
        name = 'active-client-' .. tostring(client_id)
        obj = clients[client_id]
      end
    end

    if view then
      open_in_tviewer(obj, 0, name, open_node)
    elseif vim_lsp then
      print('only with --view')
      --
      -- show active-clients id
    elseif list and type(clients) == 'table' then
      local ids = ''
      ---@diagnostic disable-next-line: unused-local
      for id, client in pairs(clients) do
        ids = ids .. ' ' .. id
      end
      print('active-client-ids: ' .. tostring(ids))
    end
  end
end

-- :lua =vim.lsp.get_active_clients()[1].server_capabilities
---@param w Cmd4Lua
function M.cmd_server_capabilities(w)
  local client_id = w:desc('lsp client-id'):def(1):optn('--client-id', '-c')
  local view, open_node = define_opt_view(w)
  local inbuf = w:desc('show in new buffer'):has_opt('--show-in-buffer', '-b')

  if w:is_input_valid() then
    local ok, clients = pcall(vim.lsp.get_active_clients)
    if not ok then
      print(clients)
      return
    end

    local client = clients[client_id]
    if client then
      local response = client.server_capabilities
      local name = 'lsp-client-' .. tostring(client_id) .. '-capabilities'
      if view then
        open_in_tviewer(response, 0, name, open_node)
      elseif inbuf then
        local on_write = function() end
        local on_cmd = function() end
        local t = {
          _client_id = client_id,
          _client_name = client.name,
          a_server_capabilities = client.server_capabilities,
          b_client_all = client,
        }
        -- to show lua-table in new buffer
        ObjEditor.create(t, 'LspClientInfo', on_write, on_cmd)
      else
        print(inspect(response))
      end
    end
  end
end

-- :EnvLsp ec
--  workspace/executeCommand
-- /usr/share/nvim/runtime/lua/vim/lsp/buf.lua
-- https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#workspace_executeCommand
--
-- to see available commands:
--
---@param w Cmd4Lua
function M.cmd_execute_command(w)
  local list = w:desc('list of the all available commands')
      :group('A', 1):has_opt('--list', '-l')

  local cmd = w:desc('the command to execute by lsp server')
      :group('A', 1):pop():arg(0)

  local _ = w:desc('arguments'):many():optional():arg(0)

  if w:is_input_valid() then
    -- list of all
    if list then
      local clients = vim.lsp.get_active_clients()
      local msg = ''
      if type(clients) == 'table' then
        local caps = clients[1].server_capabilities
        local provider = (caps or {}).executeCommandProvider

        if type(provider) == 'table' then
          for _, c in pairs(provider.commands or {}) do
            msg = msg .. ' ' .. tostring(c)
          end
        else
          msg = 'executeCommandProvider is ' .. type(provider) .. "\n"
          for key, _ in pairs(clients[1]) do
            msg = msg .. ' ' .. key
          end
        end
      else
        msg = 'Not found active-clients'
      end
      print(msg)
      return
    end
    ----
    local params = {
      command = cmd,
      arguments = {},
    }
    while w:has_args() do
      params.arguments[#params.arguments + 1] = w:pop():arg(0)
    end
    vim.lsp.buf.execute_command(params)
    -- *vim.lsp.buf.execute_command()*
  end
end

---@param w Cmd4Lua
function M.cmd_doc_symbols(w)
  local view, open_node = define_opt_view(w)

  local output_fn = w:desc('save to file')
      :def('/tmp/lsp-raw-doc-symbols'):opt('--output', '-o')
  local edit = w:desc('open in editor'):has_opt('--edit', '-e')

  if w:is_input_valid() then
    local bufnr = vim.api.nvim_get_current_buf()

    nvim_lsp.request_doc_symbols(bufnr, function(response)
      if view then
        open_in_tviewer(response, bufnr, 'doc-symbols', open_node)
        -- if not view then save to file
      elseif type(output_fn) == 'string' then
        if fs.str2file(output_fn, vim.inspect(response), 'w') then
          print('saved to', output_fn)
          if edit then
            vim.cmd(':e ' .. output_fn)
          end
        end
      end
    end)
  end
end

---@param w Cmd4Lua
function M.cmd_references(w)
  local lnum = w:desc('The line number'):optn('--line', '-l')
  local character = w:desc('The line number'):optn('--character', '-c')
  local view, open_node = define_opt_view(w)

  if w:is_input_valid() then
    local bufnr = vim.api.nvim_get_current_buf()
    local pos = { line = lnum, character = character }

    local response = nvim_lsp.request_references(bufnr, pos)

    if view then
      open_in_tviewer(response, bufnr, 'references', open_node)
    else
      print(inspect(response))
    end
  end
end

-- Info about current position in the buffer
---@param w Cmd4Lua
function M.cmd_position(w)
  local view, open_node = define_opt_view(w)

  if w:is_input_valid() then
    local cursor = vim.api.nvim_win_get_cursor(0)
    -- make sure lnum = cursor[1] starts from 1 not from 0
    local obj = { nvim_win_cursor = cursor }

    if vim and vim.lsp then
      obj.position_params = vim.lsp.util.make_position_params()
      obj.text_document_params = vim.lsp.util.make_text_document_params()
    end

    if view then
      open_in_tviewer(obj, 0, 'position-info', open_node)
    else
      print(inspect(obj))
      -- line starts from 0 not from 1 like in the cursor[1]
      -- simple url to the file opened in the current buffer
    end
  end
end

-- debugging
---@param w Cmd4Lua
function M.cmd_resolve_words(w)
  local verbose = w:has_opt('--verbose', '-v')

  if w:is_input_valid() then
    -- local Object = require("oop.Object")
    local Context = require("env.ui.Context")
    local code_parser = require("env.util.code_parser")
    local c, lexeme
    c = Context:new():resolveWords()
    ---@cast c env.ui.Context
    lexeme = code_parser.lexeme_name(c.lexeme_type)
    print(c.word_container, c.word_element, lexeme, c.lexeme_type)
    if verbose then
      print(inspect(c:toArray()))
    end
  end
end

-- symbol under the cursor
---@param w Cmd4Lua
function M.cmd_symbol_info(w)
  local view = w:desc('open result in tviewer'):has_opt('--view', '-v')

  if w:is_input_valid() then
    local cursor = vim.api.nvim_win_get_cursor(0)
    local bufnr = vim.api.nvim_get_current_buf()

    nvim_lsp.symbol_under_cursor(bufnr, cursor, function(response)
      if view then
        open_in_tviewer(response, bufnr, 'cursor')
      else
        print(inspect(response))
      end
    end)
  end
end

--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.cmd_find_block_range(w)
  local view, open_node = define_opt_view(w)
  local kind = w:desc('kind of a block to find')
      :def('function'):opt('--kind', '-k')
  local text = w:desc('show as a text'):has_opt('--text', '-t')

  if not w:is_input_valid() then return end

  local bufnr = vim.api.nvim_get_current_buf()
  local cursor = vim.api.nvim_win_get_cursor(0)

  nvim_lsp.find_block_range(bufnr, cursor, kind, function(res)
    if view then
      open_in_tviewer(res, bufnr, 'block-in-cursor', open_node)
    else -- old
      local ls, le = res.lnum, res.lnum_end
      if ls == nil or le == nil then
        print('Not Found for lnum:', cursor[1])
        return
      end

      if text then
        local lines = vim.api.nvim_buf_get_lines(0, ls, le + 1, false)
        local res0 = table.concat(lines, "\n")
        print(res0)
      else
        print(ls + 1, le + 1)
      end
    end
  end)
end

return M

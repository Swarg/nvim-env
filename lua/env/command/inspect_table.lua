-- 04-11-2023 @author Swarg
local M = {}
--
-- Goal:
--   a simple and convenient way to inspect and research large Lua tables:
--   inspect table with FileSystem-style commands
--   load and view table in the nvim buffer

M.tools = {}

local fs = require('env.files')
local su = require('env.sutil')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local log = require('alogger')
local tviewer = require("env.util.tbl_viewer")
local ucb = require 'env.util.clipboard'

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop



--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(":EnvInspectTable")
      :about("Inspect Lua Table in a FileSystem-style")
  M.handle0(w)
end

function M.handle0(w)
  w:handlers(M)
      :desc('load lua-table from file (mount)')
      :cmd('load')

      :desc('clear lua-table from the current state (umount)')
      :cmd('clear')

  -- :desc('save cwd to file'):cmd('save') -- todo

      :cmd('cd')
      :cmd('ls')
      :cmd('pwd')
      :cmd('stat')
      :cmd('tree')
      :cmd('grep')

      :desc('Show content of a cwd (the current subtable - cursor)')
      :cmd('cat')

  --   tviewr
      :desc('tree view of a lua-table in the nvim buffer with folding a nodes')
      :cmd('view')

      :desc('open given path of the tree nodes in the tviewer')
      :cmd('open')

      :desc('attach to viewer state by cuffent buff')
      :cmd('attach')

      :desc('detach from viewer and back to prev state')
      :cmd('detach')

      :desc('Debug Inspect the inner state with out table object itself')
      :cmd('dinspect')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local commands = { "ls", "open", "tree", "attach", "dinspect", "cat", "cd",
  "load", "view", "clear", "stat", "pwd", "detach" }
M.opts = { nargs = '*', complete = cu.mk_complete(commands) }

-- root is a inspect object itself, cwd -cursor to subtable on it
M.state = {
  src = nil, -- source of table file or global invim lua ref
  root = nil,
  cwd = nil,
  path = nil,
}

function M.tools.set_object(obj, src)
  M.state = tviewer.wrap2state(obj, src)
  return M.state
end

function M.tools.clear_object()
  M.state = {}
end

function M.get_path()
  return M.state.path
end

function M.do_attach(o, bufnr)
  M.prev_state = M.state
  M.state = o
  M.state.bufnr = bufnr

  local name = o.src or ''
  print('Attached to tviewer at buf: ' .. tostring(bufnr) .. ' ' .. name)
end

--------------------------------------------------------------------------------

-- load
---@param w Cmd4Lua
function M.cmd_load(w)
  local fn = w:desc('file name with lua-table to load'):pop():arg()

  if w:is_input_valid() then
    if fn == 'buf' then
      -- todo take from current tbl-viewer nvim-buff
    elseif fs.file_exists(fn) then
      local _, bytes = tviewer.load(M.state, fn)
      print('loaded: ', bytes, ' bytes')
    else
      print('Not found file' .. tostring(fn))
    end
  end
end

---@param w Cmd4Lua
function M.cmd_clear(w)
  if w:is_input_valid() then
    M.tools.clear_object()
  end
end

local cd = tviewer.cd
local ls = tviewer.ls
local pwd = tviewer.pwd
local tree = tviewer.tree

---@param w Cmd4Lua
function M.cmd_cd(w)
  local key = w:desc('key-name'):pop():many():arg(0)

  w:pre_work(type(M.root) == 'table', 'no table')
  w:pre_work(type(M.cwd) == 'table', 'no cursor')

  if w:is_input_valid() then
    cd(M.state, key)
    while w:has_args() do
      key = w:pop():arg(0)
      cd(M.state, key)
    end
  end
end

---@param w Cmd4Lua
function M.cmd_pwd(w)
  if w:is_input_valid() then
    print(pwd(M.state))
  end
end

---@param w Cmd4Lua
function M.cmd_ls(w)
  local list = w:has_opt('--list', '-l')

  if w:is_input_valid() then
    print(ls(M.state, list))
  end
end

---@param w Cmd4Lua
function M.cmd_tree(w)
  local max_depth = w:desc('depth', 'subtrees count to display'):pop():argn(0)

  if w:is_input_valid() then
    print(tree(M.state, max_depth))
  end
end

---@param w Cmd4Lua
function M.cmd_cat(w)
  if w:is_input_valid() then
    print(inspect(M.state.cwd))
  end
end

---@param w Cmd4Lua
function M.cmd_stat(w)
  if w:is_input_valid() then
    if type(M.state.cwd) == 'table' then
      if M.state.src then
        print(M.state.src)
      end
      print(type(M.state.cwd) .. pwd(M.state))
    else
      print('cwd:' .. tostring(M.state.cwd) ..
        ' root:' .. tostring(M.state.root))
    end
  end
end

-- attach to viewer
function M.cmd_attach(w)
  if w:is_input_valid() then
    local o, bufnr = tviewer.get_current_state()
    if o ~= nil and bufnr then
      M.do_attach(o, bufnr)
    else
      print('Not Found tviewer in the current buffer')
    end
  end
end

function M.cmd_detach(w)
  if w:is_input_valid() then
    M.state = M.prev_state
    M.prev_state = nil
    local bufnr = (M.prev_state or {}).bufnr or ''
    print('Detached from buf: ' .. bufnr .. ' ' .. tostring(M.prev_state))
  end
end

-- self debugging
-- debug-inspect inner state fields without root and cwd of table itself
function M.cmd_dinspect(w)
  local view = w:desc('view inspect in the new buffer'):has_opt('--view', '-v')

  if w:is_input_valid() then
    if M.state then
      local t = {}
      -- refs to inner state fields without object-table itself(root, cwd)
      for k, v in pairs(M.state) do
        if k ~= 'root' and k ~= 'cwd' then
          t[k] = v
        end
      end
      if view then
        local o = tviewer.wrap2state(t)
        tviewer.create_buf({ state = o }, 'inspect')
      else
        print(inspect(t))
      end
    end
  end
end

--------------------------------------------------------------------------------

-- open table in new buf - tbl-viewer
---@param w Cmd4Lua
function M.cmd_view(w)
  local fn = w:desc('load from given file'):opt('--file', '-f')
  local global = w:desc('runtime _G'):has_opt('--global', '-G')
  local mem = w:desc('in runtime from code'):opt('--mem', '-m')
  local require0 = w:desc('module via require'):opt('--require', '-r')
  local list_bufs = w:desc('api.nvim_list_bufs'):has_opt('--list_bufs', '-B')
  local package_luv = w:desc('package luv'):has_opt('--luv', '-u')

  local plang = w:desc('the current opened Projects')
      :has_opt('--project-lang', '-P')
  local lcache = w:desc('the cache of libraries used in the opened projects')
      :has_opt('--library-cache', '-C')

  local logger = w:desc('the logger state'):has_opt('--logger', '-L')
  --
  local open = w:desc('open path(cd)'):optl('--open-node', '-o')
  local attach = w:desc('attach cli to viewer'):has_opt('--attach', '-a')

  if w:is_input_valid() then
    local state = nil
    if fn then
      if fs.file_exists(fn) then
        state = {}
        local _, bytes = tviewer.load(state, fn)
        print('loaded bytes:' .. tostring(bytes) .. ' root:', state.root)
      else
        print('Not Found ' .. tostring(fn))
        return
      end
      -- -mem '_G'
    elseif global then
      state = tviewer.wrap2state(_G, '_G')
      --
    elseif list_bufs then
      local res = ((_G.vim or {}).api or {}).nvim_list_bufs()
      state = tviewer.wrap2state(res, 'nvim_list_bufs')
      --
    elseif package_luv then
      state = tviewer.wrap2state(uv, 'package luv')
      --
    elseif plang then
      local projects = require('env.lang.Lang').getActiveProjects()
      state = tviewer.wrap2state(projects, 'Lang-Projects')
      --
    elseif lcache then
      local libs_cache = require('env.cache.library').get_cache()
      state = tviewer.wrap2state(libs_cache, 'LibraryCache')
      --
    elseif logger then
      state = tviewer.wrap2state(log.raw_state(), 'alogger')
      --
    elseif type(require0) == 'string' then
      local ok, m = pcall(require, require0)
      if ok then
        state = tviewer.wrap2state(m, require0)
      else
        print('Error on require: ' .. tostring(m))
        return
      end
      --
    elseif type(mem) == 'string' then
      local getter = loadstring(mem)
      if type(getter) == 'function' then
        state = tviewer.wrap2state(getter(), 'code')
      else
        print(type(getter))
      end
    else
      state = M.state
    end

    if not state then
      log.debug('No State')
      return
    end

    -- deep copy ?
    local ctx = { state = state }
    local _, bufnr = tviewer.create_buf(ctx, state.src or 'table-viewer')
    -- after load and draw
    if type(open) == 'table' then
      tviewer.open_path(state, bufnr, open)
    end
    if attach then
      M.do_attach(state, bufnr)
    end
  end
end

--
-- tviewer
-- open given path of the tree nodes in the tviewer
-- this is not a cd operation that changes the value of cwd i.g.
-- just open specified node path in the tviewer(buffer)
---@param w Cmd4Lua
function M.cmd_open(w)
  w:desc('key-name (one or many)'):many():arg(0)

  if w:is_input_valid() then
    local path = {}
    -- build path to opened node
    while w:has_args() do
      local key = w:pop():arg(0) ---@cast key string
      if key then
        if not su.is_quote(key:sub(1, 1)) then
          key = tonumber(key) or key
          -- if n then key = n end
        end
        path[#path + 1] = key
      end
    end

    local o, bufnr = tviewer.get_current_state()
    if o then
      -- local bufnr = M.state.bufnr
      tviewer.open_path(o, bufnr, path)
    else
      log.debug('Not Found state of viewed table for current buff')
    end
  end
end

--
-- to find paths inside the table to string values containing the
-- specified string
--
---@param w Cmd4Lua
function M.cmd_grep(w)
  local str = w:desc("grep values in all sub tables - find path"):pop():arg()
  local copy = w:desc("copy to system clipboard"):has_opt('--copy', '-c')

  if not w:is_input_valid() then return end

  local ret, err = tviewer.grep(M.state, str)
  if type(ret) ~= 'table' then
    return w:error(err)
  end
  ---@cast ret table
  local c = 0
  local s = ''
  for _, line in ipairs(ret) do
    s = s .. line .. "\n"
    c = c + 1
  end
  if copy then
    ucb.copy_to_clipboard(s)
    return w:say('copied bytes: ' .. #s)
  end

  if s == '' then s = 'nothing found' end
  w:say(s)
end

return M

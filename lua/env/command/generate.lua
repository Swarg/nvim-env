-- 14-07-2024 @author Swarg
-- Generate simple stuff such as passwords

local c4lv = require 'env.util.cmd4lua_vim'
local cu = require 'env.util.commands'
local su = require 'env.sutil'
local bu = require 'env.bufutil'
local ujson = require 'env.util.json'
local udt = require 'env.util.datetime'
local Editor = require 'env.ui.Editor'
local log = require 'alogger'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvGen')
      :handlers(M)

      :desc('Return current timestamp')
      :cmd('timestamp', 't')

      :desc('Generate random password')
      :cmd('password', 'p')

      :desc('Generate random identificator')
      :cmd('id', 'i')

      :desc('Generate random date')
      :cmd('date', 'd')

      :desc('masking specified fileds of the opened json file with random value')
      :cmd('masking-json', 'mj')

      :run()
      :exitcode()
end

local _commands = {
  'password'
}
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------
--

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

---@param w Cmd4Lua
local function define_seed_and_copy_opt(w)
  w:desc('seed for randomize default is os.time()')
      :def(os.time()):v_optn('--seed', '-s')

  w:desc('copy to clipboard'):v_has_opt('--copy', '-c')
end

---@param w Cmd4Lua
local function define_inline_to_json_kvpair(w)
  w:desc('insert into value of json kvpair in current line')
      :v_has_opt('--json-value', '-j')
end

---@param w Cmd4Lua
---@return string? value to replace
local function check_inline_to_json_value(w)
  log_debug("check_inline_to_json_value", w.vars.json_value)
  if w.vars.json_value then
    w.vars.current_line = Editor.getCurrentLine()
    local k, v = ujson.parse_kvpair(w.vars.current_line)
    if not k or not v then
      return w:error('not found json key-value pair in current line')
    end
    if type(v) ~= 'string' then
      return w:error('expected string value got: ' .. type(v))
    end
    w.vars.length = #v
    w.vars.old_value = v
    log_debug("found in line: k:%s v:%s", k, v)
    return v
  end
  return nil
end

--
--
--
local function do_inline_to_json_value(w, new_value)
  local line = w.vars.current_line
  local old_value = w.vars.old_value
  log_debug("do_inline_to_json_value old:%s new:%s %s", old_value, new_value, line)

  if line and old_value and new_value then
    local updated_line = su.replace_first(line, old_value, new_value)
    log_debug("updated_line:", updated_line)
    Editor:new():setCurrentLine(updated_line)
  end
end

local function need_copy_to_clipboard(w, value)
  if w.vars.copy then
    require('env.util.clipboard').copy_to_clipboard(value)
    print('Copied to clipboard')
    return true;
  end
  return false
end

--
-- Call dprint to issue debug print output
--
---@param w Cmd4Lua
function M.cmd_password(w)
  w:about("Generate the password with given string length")

  define_seed_and_copy_opt(w)
  define_inline_to_json_kvpair(w)
  local length = w:desc("length of password def 16")
      :def(16):optn("--length", "-l")

  local special = w:desc('use special characters - default is false')
      :has_opt('--use-special', '-u')

  if not w:is_input_valid() then return end

  check_inline_to_json_value(w)
  if w.vars.old_value then length = #w.vars.old_value end

  local passwd = su.rand_rasswd(length, special, w.vars.seed)

  if not need_copy_to_clipboard(w, passwd) then
    if w.vars.old_value then
      do_inline_to_json_value(w, passwd)
    else
      bu.insert_line_to(passwd)
    end
  end
end

--
-- Return current timestamp
--
---@param w Cmd4Lua
function M.cmd_timestamp(w)
  define_seed_and_copy_opt(w)
  define_inline_to_json_kvpair(w)

  if not w:is_input_valid() then return end

  check_inline_to_json_value(w)

  local time = tostring(os.time())

  if not need_copy_to_clipboard(w, time) then
    if w.vars.old_value then
      do_inline_to_json_value(w, time)
    else
      bu.insert_line_to(time)
    end
  end
end

--
-- Generate random identificator
--
---@param w Cmd4Lua
function M.cmd_id(w)
  w:about("Generate random identificator with given string length or selected area")

  define_seed_and_copy_opt(w)
  define_inline_to_json_kvpair(w)
  w:desc("length of the id"):def(32):v_optn("--length", "-l")

  if not w:is_input_valid() then return end

  check_inline_to_json_value(w)

  local id = su.rand_id(w.vars.length, w.vars.seed)
  if not need_copy_to_clipboard(w, id) then
    if w.vars.old_value then
      do_inline_to_json_value(w, id)
    else
      bu.insert_line_to(id)
    end
  end
end

--
-- Generate random date
--
---@param w Cmd4Lua
function M.cmd_date(w)
  define_seed_and_copy_opt(w)
  define_inline_to_json_kvpair(w)
  w:desc('timestamp format'):v_opt('--format', '-f')

  if not w:is_input_valid() then return end

  local min, max = os.time() - 60 * 60 * 24 * 365, os.time()

  -- "2025-02-21T12:08:59.658990Z",
  local old_date = check_inline_to_json_value(w)

  if not w.vars.format and old_date then
    w:say('pick format from sample is not implemented yet')
  end

  local newdate = udt.gen_random_timestamp(w.vars.seed, w.vars.format, min, max)
  log_debug('new date:', newdate)

  if not need_copy_to_clipboard(w, newdate) then
    if w.vars.old_value then
      do_inline_to_json_value(w, newdate)
    else
      bu.insert_line_to(newdate)
    end
  end
end

--
--
-- masking specified fileds of the opened json file with random value
--
---@param w Cmd4Lua
function M.cmd_masking_json(w)
  w:usage("EnvGenerate masking-json [id name created_at modified_on] " ..
    "-m '[mydomain.org$=masked.com nickname=hidden]'")
  local key_names = w:desc('the key names to be masked'):pop():argl()
  local value_mappings = w:desc('value mapping lua-pattern=mask')
      :optl("--values-mapping", "-m")

  w:desc('seed for randomize default is os.time()'):v_optn('--seed', '-s')
  w:desc('timestamp format'):v_opt('--time-format', '-t')

  if not w:is_input_valid() then return end
  ---@cast key_names table
  ---@cast value_mappings table

  if #(key_names or E) == 0 then return w:error('no keys to be masked') end

  local fn = Editor.getCurrentFile()
  if match(fn, '%.json$') == nil then
    return w:error('supported only json files')
  end

  local kmap = {}; for _, key in ipairs(key_names) do kmap[key] = true; end
  local vmap = {};
  for _, mapping in ipairs(value_mappings) do
    local patt, new_val = match(mapping, '^(.-)=(.-)$')
    vmap[patt] = new_val;
  end

  local lines = Editor.getLines0(0, 1, -2)
  local min, max = os.time() - 60 * 60 * 24 * 365, os.time()
  local cnt = 0
  local seed = w.vars.seed or os.time()
  math.randomseed(seed)

  for i = 1, #lines do
    local line = lines[i]
    local ind, k, v, comma = match(line, '^(%s*)"([%w_]+)"%s*:%s*"(.-)"%s*(,?)%s*$')
    if ind and k and v and comma then
      local new_v = nil
      if kmap[k] then
        if k == 'id' then
          ---@diagnostic disable-next-line: param-type-mismatch
          new_v = su.rand_id(#v, "old")
        elseif k == 'created_on' or k == 'modified_on' then
          new_v = udt.gen_random_timestamp("old", w.vars.time_format, min, max)
        else
          -- ...
        end

        if not new_v then
          for patt, subst in pairs(vmap) do
            local old = match(v, patt)
            -- print('patt:', patt, 'val:', v, 'match:', old)
            if old and old ~= '' then
              new_v = v:gsub(patt, subst)
              -- print('subst', k, '=', v, 'new:', new_v, 'subst=', subst)
            end
          end
        end

        if new_v then
          line = fmt('%s"%s": "%s"%s', ind, k, new_v, comma)
          -- print(line, k, 'old:', v, 'new', new_v)
          lines[i] = line
          cnt = cnt + 1
        end
      end
    end
  end

  Editor.setLines0(0, 1, -2, lines)
  w:say('updated ' .. v2s(cnt))
end

return M

-- 30-09-2024 @author Swarg
--
-- Refactor source code
-- Lang.cmdRefactor + LangGen.cmdRefactor

local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'
local Lang = require 'env.lang.Lang'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- dynamicaly generated based on current opened file
function M.handle(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(':EnvRefactor')
      :about('Refactor the code depended of the current Project/Lang')
      :handlers(M);

  local lang = Lang.getLangByCtx(w, true, true, false)

  if not lang then
    w -- stub to show all register Lang classes if not found any Project
        :desc('show all supported Langs')
        :cmd('list', 'ls')
  else
    -- generate available commands for programming-language from current,
    -- resolved Project
    w:clear_level_help()
    lang:cmdRefactor(w)
  end

  w:run():exitcode()
end

local _commands = { "list" }
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--
-- show all supported Langs
--
--@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end
  w:say(table.concat(Lang.getRegisteredLangsAliases(), " "))
end

return M

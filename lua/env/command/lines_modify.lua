-- 24-10-2023 @author Swarg
local M = {}
--

---@diagnostic disable-next-line
local su = require('env.sutil')
local bu = require('env.bufutil')
local u8 = require("env.util.utf8")
local cu = require("env.util.commands")
local tables = require("env.util.tables")
---@diagnostic disable-next-line: unused-local
local log = require('alogger')
---@diagnostic disable-next-line: unused-local
local Cmd4Lua = require("cmd4lua")
local c4lv = require("env.util.cmd4lua_vim")
local sourcecode = require("env.sourcecode")
local uclipboard = require("env.util.clipboard")

local R = require 'env.require_util'
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

--[[
Modify Selected Lines
Usage:
 :EnvLinesModify <Command>
Commands:
   c4l cmd4lua                 -  Cmd4Lua code converter
   ebt extract-between-tags    -  .... name="value", ...  -->  value
     l lua
     j java                    -  modify java source code
    jl join-lines              -  join multiple lines together
    sp swap-pair               -  key = value --> value = key
    wq wrap-quotes             -  wrap selected text to quotes
]]
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- Note: nvim vars opts.line1 opts.line2  passed via cmd4lua_vim.newCmd4Lua
--
---@param opts table from nvim with line1, line2 and fargs
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvLinesModify')
      :about('Modify Selected Lines')
      :handlers(M)
      :set_var('ext', bu.get_buf_extension(nil))

      :desc('Cmd4Lua helper to gen code and navigate')
      :cmd("cmd4lua", "c4l")

      :desc('modify lua source code')
      :cmd('markdown', 'md')

      :desc('modify lua source code')
      :cmd('lua', 'l')

      :desc('modify java source code')
      :cmd('java', 'j')

      :desc('interact with vtt-subtitles')
      :cmd("vtt", "v")

      :desc('modify log output in nvim buffer')
      :cmd('logs', 'lg')

      :desc('edit output of a shell commands')
      :cmd('shell-command', 'sc')

      :desc("wrap selected text to quotes")
      :cmd("wrap-quotes", "wq")

      :desc("wrap selected text to given pattern")
      :cmd("wrap", "w")

      :desc('join multiple lines together')
      :cmd("join-lines", 'jl')

      :desc("key = value --> value = key")
      :cmd("sp", "swap-pair")

      :desc('extract from the selected lines a substr that matches given pattern')
      :cmd("es", "extract-subs")

      :desc('.... name="value", ...  -->  value')
      :cmd("ebt", "extract-between-tags")

      :desc('extract lines from specified column in selected lines')
      :cmd("ec", "extract-column")

      :desc('Fix the sequence of numbers in the comments')
      :cmd("fix-comments-nums", "fcn")

      :desc('format the columns to make a table from selected text')
      :cmd("columns", "c")

      :desc('join selected lines(rows) into a table ')
      :cmd("rows-to-table", "t")

      :desc('sort selected lines')
      :cmd("sort", "s")

      :desc('replace utf-8 punctuation by ascii chars')
      :cmd("simple-punctuation", "pu")

      :desc('replace tabs by spaces')
      :cmd("replace-tab", "rt")

      :desc('replace all lines matching a given patterns')
      :cmd("replace-lines", "rl")

      :desc('paste text/html from system clipboard')
      :cmd("paste-text-html", "pth")

      :desc('convert to binary format (like hex) and back')
      :cmd('binary', 'b')

      :run()
      :exitcode()
end

local commands = { "wrap-quotes", "join-lines", "java", "extract-between-tags",
  "swap-pair", "lua", "logs", "shell-command", "fix-comments-nums", "sort" }
-- options to register this command in vim
M.opts = { nargs = "*", range = true, complete = cu.mk_complete(commands) }

--------------------------------------------------------------------------------
--                               SandBox
--------------------------------------------------------------------------------

--[[

extract-between-tags
    .... name="value", ...  -->  value

cmd4lua gcf
      :cmd('lua', 'l')

lua add-id2table
    local t = {
      { k = v, },
      { k2 = v3, },
    }

-- lua gen-method-body( cursor under "method"-word:      lua m -c Obj -f .
function M.cmd_gen_method_body(w)
  return self.obj:method(params)
  self.obj.field = 1
  return self
end

]]

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

function M.cmd_cmd4lua(w)
  require("env.command.lines_modify.h_cmd4lua").handle(w)
end

function M.cmd_markdown(w)
  require("env.command.lines_modify.markdown").handle(w)
end

function M.cmd_lua(w)
  require("env.command.lines_modify.lua").handle(w)
end

function M.cmd_java(w)
  require("env.command.lines_modify.java").handle(w)
end

function M.cmd_logs(w)
  require("env.command.lines_modify.logs").handle(w)
end

function M.cmd_shell_command(w)
  require("env.command.lines_modify.shell").handle(w)
end

function M.cmd_vtt(w)
  require("env.command.lines_modify.vtt").handle(w)
end

function M.cmd_binary(w)
  require("env.command.lines_modify.binary").handle(w)
end

--                 Implementation of IStatefullModule

-- for reloading
function M.dump_state()
end

function M.restore_state()
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--------------------------------------------------------------------------------

-- :EnvLinesModify fix-comments-nums
--@param w Cmd4Lua
function M.cmd_fix_comments_nums(w)
  local start_i = w:def(0):optn('--start-num', '-n')
  local lang = w:opt('--lang', '-l')
  local opts = w.vars.vim_opts or {}

  if w:is_input_valid() then
    local lua_parser = require("env.lua_parser")
    local ext = w.vars.ext or lang
    local comment = sourcecode.get_oneline_comment(ext) -- lua c sh othe is //

    if opts.line1 and opts.line2 then
      local lines = bu.get_selected_lines(opts, false) or {}
      local res, n = {}, start_i
      for _, line in pairs(lines) do
        if line then
          local i = lua_parser.find_comment_pos(line) --line:find(comment, 1, true)
          if i and i > 0 then
            local l = line:sub(1, i - 1)
            local r = line:sub(i + #comment, #line):match("^%s*%d+(.*)$") or ''
            line = l .. string.format(" %s %2s%s", comment, n, r)
          else
            line = line .. string.format(" %s %2s", comment, n)
          end
          res[#res + 1] = line
          n = n + 1
        end
      end
      res = lua_parser.format_comments(res)
      bu.set_selected_lines(opts, res)
    else
      print('No selected lines')
    end
  end
end

-- :EnvLinesModify wrap-quotes
---@param w Cmd4Lua
function M.cmd_wrap_quotes(w)
  w:about('Wrap each line to quotes')
  w:usage('EnvLM wq -s " " -c          # to wrap worlds to quotes with commas')

  local opts = w.vars.vim_opts or {}

  opts.double = w:has_opt("--double", "-d", 'wrap lines into "')
  opts.with_nums = w:has_opt("--numbers", "-n", 'add line numbers in comments')
  opts.lua_table = w:defOptVal(''):opt("--lua-table", "-t", 'wrap to lua-table')

  opts.comma = w:desc("add ',' to the ends of line"):has_opt("--comma", "-c")
  opts.append = w:desc("add '..' to the ends of line"):has_opt("--append", "-a")

  w:desc("split line by given separator before wrap"):v_opt("--sep", "-s")
  w:desc("trim each line before wrap"):v_opt("--trim", "-T")

  if w:is_input_valid() then
    local comment = sourcecode.get_oneline_comment(w.vars.ext)
    if opts.lua_table then opts.comma = true end

    if opts.line1 and opts.line2 then
      local lines = bu.get_selected_lines(opts, false)

      if lines and #lines > 0 then
        if w.vars.sep and w.vars.sep ~= '' then
          local t = {}
          for _, line in ipairs(lines) do
            local foreach = nil
            if w.vars.trim then
              foreach = su.trim
            end
            su.split(line, w.vars.sep, t, foreach)
          end
          lines = t
        end

        -- todo escape already exists quotes
        local ends0, nl0 = '', ''
        if opts.append then
          ends0 = ' ..'
          if opts.double then
            nl0 = '\\n'
          end
        elseif opts.comma then
          ends0 = ','
        end
        for i, line in pairs(lines) do
          if w.vars.trim then
            line = su.trim(line)
          end
          local ends
          if opts.with_nums then
            ends = ends0 .. ' ' .. comment .. ' ' .. tostring(i) -- todo /* num */
          else
            ends = ends0
          end

          if opts.double then
            line = '"' .. line .. nl0 .. '"'
          else
            line = "'" .. line .. "'"
          end
          if i < #lines then
            line = line .. ends
          end
          lines[i] = line
        end
        -- wrap to the lua-table
        if opts.lua_table then
          local t = {}
          if opts.lua_table ~= '' then
            t[#t + 1] = 'local ' .. tostring(opts.lua_table) .. ' = {'
          else
            t[#t + 1] = '{'
          end
          tables.flat_copy(lines, nil, nil, t)
          t[#t + 1] = '}'
          lines = t
        end
        bu.set_selected_lines(opts, lines)
      end
    end
  end
end

--
-- wrap selected text to given pattern
--
---@param w Cmd4Lua
function M.cmd_wrap(w)
  w:about('Wrap each line to given pattern')
  local opts = w.vars.vim_opts or {}
  local n = w:desc('number used in autoinctement'):def(1):optn('--number', '-n')
  local pattern = w:desc('pattern to apply for each line')
      :def("(%d, '%s'),"):opt("--pattern", "-p")

  if not w:is_input_valid() then return end

  if opts.line1 and opts.line2 then
    local lines = bu.get_selected_lines(opts, false)
    if not lines then return end

    local has_num = string.find(pattern, '%d', 1, true) ~= nil
    local new_lines = {}
    for _, line in pairs(lines) do
      if has_num then
        line = string.format(pattern, n, line)
        n = n + 1
      else
        line = string.format(pattern, line)
      end
      new_lines[#new_lines + 1] = line
    end

    bu.set_selected_lines(opts, new_lines)
  end
end

-- EnvLinesModify join-lines ..
--@param w Cmd4Lua
function M.cmd_join_lines(w)
  w:about('Join lines')
      :desc('join multiple lines with bash cmds into one')
      :cmd("bash-cmd", "bc", M.do_join_lines_bash_cmd)
      :run()
end

-- Useful then you need to join multiple lines like
-- cmd 1 \
-- cmd 2 \
-- into one long line
-- EnvLinesModify join-lines bc
function M.do_join_lines_bash_cmd(w)
  local opts = w.vars.vim_opts or {}
  if w:is_input_valid() then
    if opts and opts.line1 and opts.line2 then
      local lines = bu.get_selected_lines(opts, false)
      if lines and #lines > 0 then
        local joined = ''
        for _, line in pairs(lines) do
          joined = joined .. ' ' .. su.trim(string.gsub(line, ' \\', '', 1))
        end
        bu.set_selected_lines(opts, { joined })
      end
    end
  end
end

-- :EnvLinesModify swap-pair ...
local MODIFY_SWAP_PAIR_USAGE = [[
  input              result                        opts
   key = value,  --> value = key,                |  -s =      or without opts
   key = value,  --> value key,                  |  -s = -h   or -h
   KEY = 1,      --> [1] = 'KEY',                |  -s = -k
   key = value,  --> assert.same(value, key)     |  -la
   key = value,  --> assert.same(value, tbl.key) |  -la -R tbl.
Usage:
]]
--[[
Usage:
 :EnvLinesModify swap-pair [Options]
Options:
   -L, --prefix-left  (string)       -  append a prefix to the left of the results
   -R, --prefix-right  (string)      -  append a prefix to the right of the results
   -s, --separator  (string) def: =  -  a string to divide the line into pairs
   -h, --separator-hide              -  remove separator from the results
   -p, --pattern  (string)           -  if defined then use pattern not simple swapping
   -la, --lua-assert                 -  set pattern to assert.same(%s, %s) [to write test]
   -lc, --lua-const                  -  swap key-value in lua table definition (like enum names)
]]
-- key = value,  --> assert.same(value, tbl.key) |  -la -R tbl.
--
--@param w Cmd4Lua
function M.cmd_swap_pair(w)
  local opts = w.vars.vim_opts or {}
  w:usage(MODIFY_SWAP_PAIR_USAGE)

  if not opts.line1 or not opts.line2 then
    return print('Works only with a selected lines')
  end

  local sep = w:desc('a string to divide the line into pairs')
      :def('='):opt('--separator', '-s')

  local nosep = w:desc('remove separator from the results')
      :has_opt('--separator-hide', '-h')

  local pattern = w:desc('if defined then use pattern not simple swapping')
      :opt('--pattern', '-p')
  local rprefix = w:desc('append a prefix to the right of the results')
      :opt('--prefix-right', '-R')
  local lprefix = w:desc('append a prefix to the left of the results')
      :opt('--prefix-left', '-L')

  local wrap_strs = w:desc('wrap parst from four points(1,2,3,4) (4-list<str>)')
      :optl('--wrap', '-w')

  local lua_const =
      w:desc('swap key-value in lua table definition (like enum names)')
      :has_opt('--lua-const', '-lc')

  -- lua table line to assert.same
  -- key = value, --> assert.same(value, key)
  if not pattern and w:desc('set pattern to assert.same(%s, %s) [to write test]')
      :has_opt('--lua-assert', '-la') then
    pattern = 'assert.same(%s, %s)'
  end
  -- key = value

  if not w:is_input_valid() then return false end

  -- actual work
  local lines = bu.get_selected_lines(opts, false)
  if lines and #lines > 0 then
    local res = {}
    for _, line in pairs(lines) do
      local i = string.find(line, sep)
      if i then
        local l = su.trim(line:sub(1, i - 1))
        local r = su.trim(line:sub(i + #sep))
        local middle, ends = sep, ''
        if nosep then middle = '' end

        local e = r:sub(-1, -1)
        -- case key = value,   --> value = key,
        if e == ',' or e == ';' or e == '.' then
          r = r:sub(1, #r - 1)
          ends = e
        end
        if rprefix then l = rprefix .. l end
        if lprefix then r = lprefix .. r end

        if type(wrap_strs) == 'table' then
          local t = wrap_strs
          local w1, w2, w3, w4 = t[1] or '', t[2] or '', t[3] or '', t[4] or ''
          --
          line = w1 .. r .. w2 .. middle .. w3 .. l .. w4 .. ends
          --
        elseif pattern then
          line = string.format(pattern, r, l)
        else
          -- KEY = 1, --> [1] = 'KEY',
          if lua_const then
            r = sourcecode.mk_tbl_key(r)
            l = sourcecode.mk_tbl_value(l, "'")
          end
          line = r .. middle .. l .. ends
        end
      end
      table.insert(res, line)
    end
    bu.set_selected_lines(opts, res)
  end
end

-- :EnvLinesModify extract-between-tags
local EXTRACT_BETWEEN_TAGS_USAGE = [[

arg 1 - open tag   -- required
arg 2 - close tag  -- reqiured
-o|--offset        -- start find open tag from line character
-p|--prefix        -- append between result (found substring)
-s|--suffix        -- append after result
-P|--pattern       -- pattern ("%s", result)

Example:
   'name="value",..'  open='name"' close='"' -->  value')
   'name="value",..'  open='name"' close='"' -->  value')
]]

--  'name="value",..'  open='name"' close='"' -->  value')
--  'name="value",..'  open='name"' close='"' -->  value')
--@param w Cmd4Lua
function M.cmd_extract_between_tags(w)
  w:about('Extract substring between open and close tags')
  w:usage(EXTRACT_BETWEEN_TAGS_USAGE)


  local opts = w.vars.vim_opts or {}
  local open_tag = w:tag('open-tag'):pop():arg()
  local close_tag = w:tag('close-tag'):pop():arg()

  local offset = w:def(1):optn('--offset', '-o')
  local prefix = w:def(''):opt('--prefix', '-p')
  local suffix = w:def(''):opt('--suffix', '-s')
  local pattern = w:def(''):opt('--pattern', '-P')

  -- if not open_tag or open_tag == '' or not close_tag or close_tag == '' then
  --   print('Not specified open and close tags')
  --   return
  -- end

  -- ensure all input correct overwise show help
  if not w:is_input_valid() then
    return
  end

  local lines = bu.get_selected_lines(opts, false)
  if lines then
    local res = {}
    for _, line in pairs(lines) do
      local s = string.find(line, open_tag, offset, true)
      if s then
        local e = string.find(line, close_tag, s + #open_tag + 1, true)
        if e then
          line = su.trim(line:sub(s + #open_tag, e - 1))
          if pattern and pattern ~= '' then
            line = string.format(pattern, line)
          else
            line = prefix .. line .. suffix
          end
        end
      end
      table.insert(res, line)
    end
    bu.set_selected_lines(opts, res)
  end
end

-- build map from list of raw_wrapper
-- private method (exposed for testing)
--
-- used to wrap specified column of the table via cli command
-- ['1','`','2','<','>']  --> {[1]={'`'}, [2]=={'<','>'}}
function M.build_colums_wrapper(t)
  if type(t) == 'table' then
    local i, max, obj = 0, #t, {}

    -- only for single char
    -- \' --> '   \( --> (    \[ --> [
    local function _unescape(v)
      if v:sub(1, 1) == '\\' then
        return v:sub(2)
      end
      return v
    end

    while i < max do
      i = i + 1
      local idx = tonumber(t[i])
      local v = t[i + 1]
      if idx and v and not tonumber(v) then
        obj[idx] = { _unescape(v) }
        if i + 2 <= max and not tonumber(t[i + 2]) then
          table.insert(obj[idx], _unescape(t[i + 2]))
          i = i + 1
        end
        i = i + 1
      end
    end

    return obj
  end
  return t
end

--
--
-- format the columns to make a table from selected text
---@param w Cmd4Lua
function M.cmd_columns(w)
  local opts = w.vars.vim_opts or {}

  if not opts.line1 or not opts.line2 then
    return print('Works only with a selected lines')
  end

  local sep = w:desc('a string to divide the line into columns')
      :def("\t"):opt('--separator', '-s')

  local tsep = w:desc('sepatator in the output table')
      :def("  "):opt('--tsep', '-t')

  local wrapper = w:desc('wrap one or multiple specified columns into strings')
      :optl('--wrapper', '-w')

  local sort_n = w:desc('sort by the number of column'):optn('--sort', '-S')

  w:desc('show verbose info'):v_opt_verbose('-v')


  if not w:is_input_valid() then return false end

  if w:is_verbose() then print('raw_wrapper:', inspect(wrapper)) end

  wrapper = M.build_colums_wrapper(wrapper)

  if w:is_verbose() then
    print('separator:', sep)
    print('table-sep:', tsep)
    print('wrapper  :', inspect(wrapper))
  end

  -- actual work
  local lines = bu.get_selected_lines(opts, false)
  opts.lines = lines

  if lines and #lines > 0 then
    local rows, maxs, res = {}, {}, {}
    -- split to rows
    for _, line in pairs(lines) do
      local columns = su.split(line, sep)

      if columns then
        for i, cell in pairs(columns) do
          maxs[i] = maxs[i] or 0
          if #cell > maxs[i] then maxs[i] = #cell end
        end
      end

      table.insert(rows, columns)
    end


    if sort_n then
      -- by string
      table.sort(rows, function(a, b)
        local a0, b0 = a[sort_n] or '', b[sort_n] or ''
        return string.upper(a0) < string.upper(b0)
      end)
    end

    -- build table
    for _, columns in pairs(rows) do
      local s = ''
      for i, cell in pairs(columns) do
        local max = maxs[i]
        -- wrap specified columns into strings
        if wrapper and wrapper[i] then
          local wtbl = wrapper[i]
          cell = wtbl[1] .. cell .. (wtbl[2] or wtbl[1])
        end
        if max < 99 then
          s = s .. string.format("%-" .. max .. 's%s', cell, tsep)
        else
          s = s .. string.format("%-99s%s", cell, tsep)
        end
      end
      s = s
      res[#res + 1] = s
    end

    -- opts.rows, opts.maxs = rows, maxs

    bu.set_selected_lines(opts, res)

    if w:is_verbose() then
      print('maxs width:', inspect(maxs))
    end
  end
  return wrapper
end

--
--
-- Usage Example:
--   code-line1()           code-line1()   -- comment1
--   code-line2()           code-line2()   -- comment2
--   code-line3()      -->  code-line3()   -- comment3
--   -- comment1
--   -- comment2
--   -- comment3
--
---@param w Cmd4Lua
function M.cmd_rows_to_table(w)
  local columns = w:desc('the columns number'):def(2):optn('--columns', '-c')
  local sep = w:desc('colums separator'):def('  '):opt('--sep', '-s')

  if not w:is_input_valid() then return end

  local opts = bu.get_selection_range(0, w.vars.vim_opts)
  -- print('vim_opts:', vim.inspect(w.vars.vim_opts))
  -- print('opts:', vim.inspect(opts))

  if opts.line2 - opts.line1 < 2 then
    return print('select lines to build table, got: ' ..
      'ln1:' .. tostring(opts.line1) .. " ln2: " .. tostring(opts.line2))
  end

  opts.line1 = opts.line1 + 1 -- ?? issue

  if sep == '\t' then sep = "\t" end

  local lines = bu.get_selected_lines(opts, false)
  assert(lines, 'lines from given range')
  local t, r, h, max, max_next_col = {}, 1, (#lines / columns), 0, 0
  -- r - a row number in current specified column
  -- h - a rows count in the one column
  -- max used to format cell

  local coln = 1

  for i = 1, #lines do
    local line = lines[i]
    local s = t[r]
    -- dprint('[#] i:', i, 'j:', j, 'row:', s, 'line:', line)
    if not s then
      s = line
    else
      local n = max - #s + 2
      s = s .. string.format("%" .. n .. "s", "") .. sep .. line
    end
    t[r] = s
    if #s > max_next_col then max_next_col = #s end

    r = r + 1
    if r > h then -- jump to next column
      r = 1
      coln = coln + 1
      max = max_next_col
      max_next_col = 0
    end
  end

  bu.select_range(0, opts.line1 - 2, opts.line1 + h - 2)

  bu.set_selected_lines(opts, t)
  return t
end

---@param w Cmd4Lua
function M.cmd_simple_punctuation(w)
  local all = w:desc('apply to all file'):has_opt('--all', '-a')

  if w:is_input_valid() then
    if all then
      pcall(vim.cmd, '%s/[“”]/"/g')
      pcall(vim.cmd, "%s/[‘’]/'/g")
      pcall(vim.cmd, "%s/[–]/-/g")
      pcall(vim.cmd, "%s/[—]/-/g")
      pcall(vim.cmd, "%s/[…]/.../g")
      pcall(vim.cmd, "%s/[•]/*/g")
      pcall(vim.cmd, '%s/[«»]/"/g')
    else
      local opts = bu.get_selection_range(0, w.vars.vim_opts)
      local lines = bu.get_selected_lines(opts, false)
      local t = {}
      for _, line in ipairs(lines or {}) do
        t[#t + 1] = u8.mk_simple_punctuation(line)
      end
      bu.set_selected_lines(opts, t)
    end
  end
end

---@param w Cmd4Lua
function M.cmd_sort(w)
  local opts = w.vars.vim_opts or {}
  local du_out = w:desc('sort output of the du -hcd 1 (linux)'):has_opt('--du')

  if not opts.line1 or not opts.line2 then
    return print('Works only with a selected lines')
  end
  if not w:is_input_valid() then return end

  local lines = bu.get_selected_lines(opts, false)
  if lines and #lines > 0 then
    if du_out then
      lines = M.do_sort_du_hcd1_output(lines)
    else
      table.sort(lines, function(a, b)
        return string.upper(a) < string.upper(b)
      end)
    end
    bu.set_selected_lines(opts, lines)
  end
end

---@return number
local function readable_size_to_bytes(s)
  local d, decimal, mod = string.match(s, '^(%d+)%.?(%d*)(%a)')
  if not d then
    if tonumber(s) == nil then
      error('cannot parse size:"' .. v2s(s) .. '"')
    end
    d = s -- size without K, M, G
  end
  local num = tonumber(d)
  if decimal and decimal ~= '' then
    local dnum = tonumber(decimal) or 0
    if #decimal == 1 then
      num = num + (dnum / 10)
    elseif #decimal == 2 then
      num = num + (dnum / 100)
    end
    -- print("[DEBUG] dnum:", num, dnum/10)
  end
  local f = 1
  if mod then
    if mod == 'K' or mod == 'k' then
      f = 1024
    elseif mod == 'M' or mod == 'm' then
      f = 1024 * 1024
    elseif mod == 'G' or mod == 'g' then
      f = 1024 * 1024 * 1024
    end
  end
  return num * f
end

function M.do_sort_du_hcd1_output(lines)
  local t, res = {}, {}
  for i = 1, #lines do
    local size, path = string.match(lines[i], '^([^%s]+)%s+(.-)$')
    if size and path then
      t[#t + 1] = { size, path }
    end
  end

  table.sort(t, function(a, b)
    local a_sz = readable_size_to_bytes(a[1])
    local b_sz = readable_size_to_bytes(b[1])
    return a_sz < b_sz
  end)

  for i = 1, #t, 1 do
    local e = t[i]
    res[#res + 1] = fmt('%-8s  %s', e[1], e[2])
  end

  return res
end

--
-- replace tabs by spaces
--
---@param w Cmd4Lua
function M.cmd_replace_tab(w)
  local opts = w.vars.vim_opts or {}
  if not w:is_input_valid() then return end

  local lines = bu.get_selected_lines(opts, false)
  if not lines or #lines == 0 then return end

  for i = 1, #lines do
    local line = lines[i]
    if line then
      lines[i] = line:gsub("\t", '    ')
    end
  end

  bu.set_selected_lines(opts, lines)
end

--
-- replace lines with given patterns
--
---@param w Cmd4Lua
function M.cmd_replace_lines(w)
  w:usage('EnvLM replace-lines [ "^start-with", "ends-with$" ]')
  local patterns = w:desc('one pattern or list of the patterns to replace')
      :pop():argl()

  local opts = w.vars.vim_opts or {}
  if not w:is_input_valid() then return end
  ---@cast patterns table

  local lines = bu.get_selected_lines(opts, false)
  if not lines or #lines == 0 then return end

  local res = {}
  local replaced = {}
  for i = 1, #lines do
    local line = lines[i]
    if line then
      local matched = false
      for _, p in pairs(patterns) do
        if string.match(line, p) then
          matched = true
          break
        end
      end
      if not matched then
        res[#res + 1] = line
      else
        replaced[#replaced + 1] = line
      end
    end
  end

  bu.set_selected_lines(opts, res)
end

--
-- paste text/html from system clipboard
--
---@param w Cmd4Lua
function M.cmd_paste_text_html(w)
  if not w:is_input_valid() then return end

  local lines = uclipboard.get_text_html_unix()

  if type(lines) ~= 'table' or #lines == 0 or
      (#lines == 1 and (lines[1] == '' or lines[1] == ' ')) then
    return w:error('clipboard is empty')
  end

  local opts = bu.get_selection_range(0, w.vars.vim_opts)

  if not opts.line1 or not opts.line2 then
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    local cursor_row = (cursor_pos or E)[1] or 0
    opts.line1, opts.line2 = cursor_row, cursor_row
  end

  bu.set_selected_lines(opts, lines)
end

--
-- extract lines from specified column in selected lines
-- the beginning of the column is determined by the current cursor position
-- e.g. by cur_col
--
---@param w Cmd4Lua
function M.cmd_extract_column(w)
  w:v_opt_quiet('-q')

  local as_list = w:desc('show as list(lua table)'):has_opt('--list', '-l')
  local sep = w:desc('columns separator'):def(' '):opt('--separator', '-s')

  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false)
  if not lines or #lines == 0 then
    return w:error('no selected lines')
  end

  local cur_col = (vim.api.nvim_win_get_cursor(0) or E)[2] + 1
  local max_pe = cur_col
  local cells = {}
  for _, line in ipairs(lines) do
    local pe = string.find(line, sep, cur_col, true) or #line
    local cell = string.sub(line, cur_col, pe)
    -- case: right align
    if string.match(cell, '^%s*$') and pe < max_pe then
      cell = string.sub(line, cur_col, max_pe)
    end
    cells[#cells + 1] = su.trim(cell)
    if pe > max_pe then max_pe = pe end
  end

  local output
  if as_list then
    output = require "inspect" (cells)
  else
    output = table.concat(cells, " ")
  end
  uclipboard.copy_to_clipboard(output)

  w:say('copied to clipboard')
  return cells
end

--
-- extract from the selected lines a substr that matches given pattern
--
---@param w Cmd4Lua
function M.cmd_extract_subs(w)
  w:v_opt_quiet('-q')
  w:usage(":EnvLM extract-subs '%${([%w_]+)}'")
  w:usage(":EnvLM es '%${(_[%w_]+)}'")

  local patt = w:desc('pattern'):pop():arg()

  if not w:is_input_valid() then return end ---@cast patt string

  local opts = w.vars.vim_opts or {}
  local lines = bu.get_selected_lines(opts, false)
  if not lines or #lines == 0 then
    return w:error('no selected lines')
  end

  local t = {}

  for _, line in ipairs(lines) do
    local sa = { string.match(line, patt) }
    local ns = ''
    for _, s in ipairs(sa) do
      ns = ns .. ' ' .. s
    end
    if ns and ns ~= '' then
      t[#t + 1] = ns
    end
  end
  uclipboard.copy_to_clipboard(table.concat(t, '\n'))
  w:say('copied to clipboard')
end

if _G.TEST then
  M.test = {
    readable_size_to_bytes = readable_size_to_bytes,
  }
end

return M

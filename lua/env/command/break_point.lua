-- 09-10-2024 @author Swarg
--
-- Integration with nvim-dap to configure breakpoints conditional
--
-- How to create conditional breakpoints
-- https://github.com/mfussenegger/nvim-dap/discussions/670
-- :help dap.toggle_breakpoint
-- todo fix floating window to edit one breakpoint props

local log = require 'alogger'
local fs = require 'env.files'
local c4lv = require 'env.util.cmd4lua_vim'
local udap = require 'env.bridges.dap'
local ObjEditor = require 'env.ui.ObjEditor'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvBreakPoint')
      :about('nvim-dap integration')
      :handlers(M)

      :desc("information about current line breakpoint")
      :cmd("info", "i")

      :desc("jump to the next breakpoint in the current buffer")
      :cmd("next", "n")

      :desc("jump to the prev breakpoint in the current buffer")
      :cmd("prev", "p")

      :desc("edit breakpoint properties (e.g. to set a condition)")
      :cmd("edit", "e")

      :desc("edit all breakpoints at one (for the current opened project)")
      :cmd("edit-all", "ea")

      :desc("list of all brackpoints in the current buffer")
      :cmd("list", "ls")

      :desc("save brackpoints")
      :cmd("save", "s")

      :desc("load brackpoints")
      :cmd("load", "l")

      :run()
      :exitcode()
end

M.opts = { nargs = '*', range = true }

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local str_to_bool = ObjEditor.str_to_bool
local bp_fn_and_proj_root_of_def

---@param w Cmd4Lua
---@return number?
---@return number?
local function define_bufnr_lnum_opts(w)
  local bufnr = w:desc():optn("--bufnr", "-b")
  local lnum = w:desc():optn("--lnum", '-l')

  return bufnr, lnum
end

local function current_bufnr_lnum_if_not_defined(bufnr, lnum)
  local win = vim.api.nvim_get_current_win()
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  lnum = lnum or (vim.api.nvim_win_get_cursor(win) or E)[1]
  return bufnr, lnum
end

---@param w Cmd4Lua
local function define_project_root_opt(w)
  return w:desc("directory of the project root"):opt("--project-root", "-d")
end

local function def_proj_root_if_not_defined(project_root)
  if not project_root or project_root == '' or project_root == '.' then
    project_root = vim.fn.getcwd()
  end
  project_root = fs.ensure_dir(project_root)
  return project_root
end


--
-- list of all brackpoints in the current buffer
--
---@param w Cmd4Lua
function M.cmd_list(w)
  local bufnr = w:desc():optn("--bufnr", "-b")

  if not w:is_input_valid() then return end

  local t, err = udap.get_breakpoints_lines(bufnr)
  if not t then
    return w:error(err)
  end
  w:say('[' .. table.concat(t, " ") .. ']')
end

--
-- information about current line breakpoint
--
---@param w Cmd4Lua
function M.cmd_info(w)
  local bufnr, lnum = define_bufnr_lnum_opts(w)

  -- to get as json to manually add to a breakpoints.json file
  local as_json = w:desc("show as json entry for file"):has_opt('--json', '-j')
  local project_root = define_project_root_opt(w)

  if not w:is_input_valid() then return end

  project_root = def_proj_root_if_not_defined(project_root)

  local ok, err = udap.breakpoint_info(bufnr, lnum, as_json, project_root)
  if not ok then return w:error(err) end
  w:say(ok)
end

--
-- jump to the next breakpoint in the current buffer
--
---@param w Cmd4Lua
function M.cmd_next(w)
  if not w:is_input_valid() then return end

  local lnum, err = udap.get_next_breakpoint_lnum()
  if type(lnum) ~= 'number' or (lnum <= 0 or lnum >= math.huge) then
    return w:say(err or 'no')
  end
  vim.api.nvim_win_set_cursor(0, { lnum, 0 })
end

--
-- jump to the prev breakpoint in the current buffer
--
---@param w Cmd4Lua
function M.cmd_prev(w)
  if not w:is_input_valid() then return end

  local lnum, err = udap.get_prev_breakpoint_lnum()
  if type(lnum) ~= 'number' or (lnum <= 0 or lnum >= math.huge) then
    return w:say(err or 'no')
  end
  vim.api.nvim_win_set_cursor(0, { lnum, 0 })
end

--
-- convert empty strings to nil
-- (empty strings is used to show keys in ObjEditor)
--
---@return string?
local function not_empty_or_nil(s)
  if s == '' or string.match(s, '^s+$') then
    return nil
  end
  return s
end
--
-- callback for ObjEditor
--
-- to apply udapted breakpoint
---@param t table{bufnr, lnu, breakpoint{line, condition, hit_condition, log_message}}
local function update_breakpoint(t)
  log_debug("update_breakpoint", t)
  if type(t) ~= 'table' then
    return false
  end
  t.UI_ERRORS = nil
  if type(t.bufnr) ~= 'number' or t.bufnr < 0 then
    log_debug('no bufnr')
    t.UI_ERRORS = "no bufnr"
    return false
  end
  if type(t.lnum) ~= 'number' or t.lnum < 0 then
    log_debug('no lnum')
    t.UI_ERRORS = "no lnum"
    return false
  end
  local condition = not_empty_or_nil(t.condition)
  local hit_condition = not_empty_or_nil(t.hit_condition)
  local log_message = not_empty_or_nil(t.log_message)

  udap.set_breakpoint(t.bufnr, t.lnum, condition, hit_condition, log_message)
  return true
end

--
-- edit breakpoint properties (e.g. to set a condition)
--
---@param w Cmd4Lua
function M.cmd_edit(w)
  w:v_opt_verbose('-v')
  local bufnr, lnum = define_bufnr_lnum_opts(w)

  if not w:is_input_valid() then return end

  bufnr, lnum = current_bufnr_lnum_if_not_defined(bufnr, lnum)

  local bp, err = udap.get_breakpoint(bufnr, lnum)
  if not bp then
    return w:error(err)
  end
  local bufname = "BreakPoint:" .. v2s(bufnr) .. ':' .. v2s(lnum)
  local wbp = {
    bufnr = bufnr,
    lnum = lnum,
    -- line = bp.line,
    condition = bp.condition or "",
    hit_condition = bp.hitCondition or "",
    log_message = bp.logMessage or "",
  }
  if w:is_verbose() then
    w:say("original bp:" .. require 'inspect' (bp))
  end

  local keyorder = { 'condition', 'hit_condition', 'log_message', 'bufnr', 'lnum' }
  ObjEditor.create(wbp, bufname, update_breakpoint, nil, keyorder, false)
end

local apply_breakpoints

--
-- edit all breakpoints for the current project at one
--
---@param w Cmd4Lua
function M.cmd_edit_all(w)
  w:v_opt_verbose('-v')
  local project_root = define_project_root_opt(w)

  if not w:is_input_valid() then return end

  project_root = def_proj_root_if_not_defined(project_root)

  local bpdata, err = udap.get_all_brackpoints(project_root)
  if not bpdata then
    return w:error(err)
  end

  -- convert dap representation to short
  local t = { { 'path-to-source-file', 'line-number', 'enabled' } }
  local detailed = {} -- with condition, hit_condition, or log_messages
  local enabled = udap.is_bp_enabled
  for i = 1, #bpdata do
    local b = bpdata[i]
    local file, bps = b.file, b.bps
    for _, bp in ipairs(bps) do
      -- filename lnum 0 to disable via condition= '0==1'
      if udap.is_breakpoin_with_details(bp) then
        bp.file = file
        detailed[#detailed + 1] = bp
      else
        t[#t + 1] = { file, bp.line, enabled(bp) }
      end
    end
  end

  -- local keyorder = { 'condition', 'hit_condition', 'log_message', 'bufnr', 'lnum' }
  local korder = { 'project_root', 'simple', 'extra' }
  local data = { project_root = project_root, simple = t, detailed = detailed }
  ObjEditor.create(data, 'breakpoints', apply_breakpoints, nil, korder, false)
end

local function fail_with_ui_msg(t, msg)
  t.UI_ERRORS = msg
  return false
end
--
-- callback for ObjEditor
--
-- to apply udapted breakpoint
---@param t table{bufnr, lnu, breakpoint{line, condition, hit_condition, log_message}}
function apply_breakpoints(t)
  log_debug("apply_breakpoints", t)
  if type(t) ~= 'table' then return false end

  t.UI_ERRORS = nil
  if type(t.project_root) ~= 'string' then
    return fail_with_ui_msg(t, 'no project_root')
  end
  if type(t.simple) ~= 'table' then
    return fail_with_ui_msg(t, "no simple breakpoints definition")
  end
  if type(t.detailed) ~= 'table' then
    return fail_with_ui_msg(t, "no detailed breakpoints definition")
  end

  local bpdata = {}
  local map = {}

  local function add_bps(file, e)
    map[file] = map[file] or {
      file = file,
      bps = {}
    }
    local bps = map[file].bps
    bps[#bps + 1] = e
  end

  for i, bp in ipairs(t.simple) do
    local file, lnum, enabled = bp[1], bp[2], str_to_bool(bp[3])

    if not file then
      return fail_with_ui_msg(t, 'no file for breakpoint#' .. v2s(i))
    end
    if not lnum then
      return fail_with_ui_msg(t, 'no lnum for breakpoint#' .. v2s(i))
    end
    if file ~= 'path-to-source-file' then -- ignore example
      add_bps(file, {
        line = lnum,
        condition = (enabled == false) and '0==1' or nil
      })
    end
  end

  for i, bp in ipairs(t.detailed) do
    local file = assert(bp.file, 'bp.file')
    if not bp.line then
      return fail_with_ui_msg('no line-number for detailed bp#' .. v2s(i))
    end
    add_bps(file, {
      line = bp.line,
      condition = not_empty_or_nil(bp.condition),
      hit_condition = not_empty_or_nil(bp.hit_condition),
      log_message = not_empty_or_nil(bp.log_message)
    })
  end

  for _, e in pairs(map) do bpdata[#bpdata + 1] = e end -- flatten

  udap.apply_all_breakpoints(bpdata, t.project_root)

  return true
end

-------------------------------------------------------------------------------
--                          persistens
--


function bp_fn_and_proj_root_of_def(filename, project_root)
  project_root = def_proj_root_if_not_defined(project_root)

  if not filename or filename == '' then
    filename = fs.join_path(project_root, 'breakpoints.json')
  end
  return filename, project_root
end

--
-- save brackpoints
--
---@param w Cmd4Lua
function M.cmd_save(w)
  local filename = w:desc("name of the file in which save")
      :opt("--filename", "-f")
  local project_root = define_project_root_opt(w)

  local overwrite = w:desc("to overwrite already existed file")
      :has_opt("--overwrite", "-w")
  local add_new_bp = w:desc("to add new breakpoints to already existed file")
      :has_opt("--add", "-a")

  if not w:is_input_valid() then return end

  filename, project_root = bp_fn_and_proj_root_of_def(filename, project_root)

  if fs.file_exists(filename) then
    if add_new_bp then
      error('Not implemented yet')
    elseif not overwrite then
      return w:error('the file alread exists (use --overwrite|--add or options)')
    end
  end

  local ok, err = udap.save_brackpoints(filename, project_root)
  if not ok then return w:error(err) end
  w:say('saved: ' .. v2s(ok) .. ' ' .. v2s(filename))
end

--
-- load brackpoints
--
---@param w Cmd4Lua
function M.cmd_load(w)
  local filename = w:desc("name of the file in which save")
      :opt("--filename", "-f")
  local project_root = define_project_root_opt(w)

  if not w:is_input_valid() then return end

  filename, project_root = bp_fn_and_proj_root_of_def(filename, project_root)

  local cnt, err = udap.load_breakpoints(filename, project_root)
  if not cnt then return w:error(err) end
  w:say('restored breakpoints: ' .. v2s(cnt))
end

return M

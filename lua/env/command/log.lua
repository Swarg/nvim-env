-- 25-10-2023 @author Swarg
-- Goals:
--   - configure on fly
--   - open logfile
--   - inspect actual current settings
--   - jump to location from log-message by in-log linenumber
--
local M = {}

---@diagnostic disable-next-line
local log = require('alogger')
local c4lv = require("env.util.cmd4lua_vim")
local fs = require('env.files')

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--[[
Interact and change the logger settings
Usage:
 :EnvLog <Command>
Commands:
     c clear      -  Clear the current latest log file
     i inspect    -  Inspect the log config
     m message    -  Issue the log message with given params
     o open       -  Open the current latest log file
   dir path       -  Show the directory with a log file
     s set        -  Change the logger settings
    st status     -  The Logger status
--]]
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvLog')
      :about('Interact and change the logger settings')
      :handlers(M)

      :desc('The Logger status')
      :cmd('status', 'st')

      :desc('Show directory with a log file')
      :cmd('dir', 'path')

      :desc('Open the current latest log file')
      :cmd('open', 'o')

      :desc('Clear the current latest log file')
      :cmd('clear', 'c')

      :desc('Change the logger settings')
      :cmd('set', 's')

      :desc('Enable or Disable Debug log level')
      :cmd('debug', 'd')

      :desc('Enable or Disable Trace log level')
      :cmd('trace')

      :desc('Inspect the log config')
      :cmd('inspect', 'i')

      :desc('Issue the log message with given params')
      :cmd('message', 'm')

      :desc('open file at linenumber from specified message in log by lnmun')
      :cmd('jump', 'j')

      :run()
end

-- --@param w Cmd4Lua
-- function M.cmd_message(w)
--   if w:is_input_valid() then
--     print(M.cmd_logger_issue_message(w))
--   end
-- end

M.opts = { desc = 'nvim-env log-file', nargs = '*' }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.cmd_status(w)
  if w:is_input_valid() then
    print(log.status())
  end
end

--@param w Cmd4Lua
function M.cmd_path(w)
  if w:is_input_valid() then
    print(log.get_logdir())
  end
end

local function get_logfile()
  local outfile = log.get_outfile()
  if not outfile then
    outfile = log.get_logdir() .. '/latest.log'
  end
  return outfile
end

--@param w Cmd4Lua
function M.cmd_open(w)
  if w:is_input_valid() then
    local outfile = get_logfile()
    vim.cmd(':e ' .. outfile)
  end
end

--@param w Cmd4Lua
function M.cmd_clear(w)
  if w:is_input_valid() then
    print(log.purge())
  end
end

--@param w Cmd4Lua
function M.cmd_inspect(w)
  local view = w:desc('open in tviewer'):has_opt('--view', '-v')

  if w:is_input_valid() then
    if view then
      local tviewer = require("env.util.tbl_viewer")
      tviewer.open_in_buf(log.raw_state(), 'alogger')
    else
      print(inspect(log.__get_config()))
    end
  end
end

local function validate_log_level(val)
  local lvl = val and log.level_str2i(val) or nil
  if not log.is_valid_lvl(lvl) then
    local msg = 'Expected One of [' .. tostring(log.get_supported_levels()) .. ']'
    return false, msg
  end
  return true
end

--
-- logger set
--
---@param w Cmd4Lua
function M.cmd_set(w)
  w
      :desc('set saving to file')
      :cmd('save', 's', function(w0)
        local desc = 'enable/disable saving a log messages to a log file'
        local saving = w0:desc('saving', desc):pop():argb()
        log.set_save(saving)
        print(log.status())
      end)

      :desc('set the log level')
      :cmd('level', 'l', function(w0)
        local alvl = w0:desc('the log level'):pop(validate_log_level):arg()
        if w:is_input_valid() then
          log.set_level(log.level_str2i(alvl))
          print(log.status())
        end
      end)

      :run()
end

---@param w Cmd4Lua
function M.cmd_debug(w)
  local enable = w:desc('enable or disable debug log level (1/0)'):pop():argb(0)

  if w:is_input_valid() then
    if enable then
      if log.is_debug() then
        print('DEBUG log-level already enabled')
      else
        log.set_level(log.levels.DEBUG)
        local msg = ''
        if not log.is_saving() then
          log.set_save(true)
          msg = 'and set saving = true'
        end
        print('DEBUG log-level enabled', msg)
      end
    else
      if log.is_debug() then
        log.set_level(log.levels.INFO)
        print('log-level set to INFO')
      else
        print('log-level already is ', log.get_level_name())
      end
    end
  end
end

--
-- Enable or Disable Trace log level
--
---@param w Cmd4Lua
function M.cmd_trace(w)
  local enable = w:desc('enable or disable trace log level (1/0)'):pop():argb(0)
  if not w:is_input_valid() then return end
  if enable then
    if log.is_trace() then
      print('TRACE log-level already enabled')
    else
      log.set_level(log.levels.TRACE)
      local msg = ''
      if not log.is_saving() then
        log.set_save(true)
        msg = 'and set saving = true'
      end
      print('TRACE log-level enabled', msg)
    end
  else
    if log.is_trace() then
      log.set_level(log.levels.INFO)
      print('log-level set to INFO')
    else
      print('log-level already is ', log.get_level_name())
    end
  end
end

-- Create a new log message with given log level and text
---@param w Cmd4Lua
function M.cmd_message(w)
  local alvl = w:desc('level', 'the log level'):pop(validate_log_level):arg()
  -- build a one line from a multiple words
  local msg = w:desc('message', 'the log message'):pop():arg()
  local verbose = w:desc('duplicate output to StdOut'):has_opt('--verbose', '-v')

  -- print(msg) -- echo input to check
  if w:is_input_valid() then
    ---@cast alvl string
    ---@cast msg string
    local lvl = log.level_str2i(alvl)
    if verbose then
      print(lvl, msg)
    end
    log.log(lvl, msg)
  end
end

--
-- open file at linenumber from specified message in log by lnmun
--
-- to quickly jump to a specific location from the log file,
-- which is specified by the line number in the log
--
--   - fetch line from log by specified index
--   - parse fetched message log line and extract target-filename and lnum
--   - jump to this location
--
---@param w Cmd4Lua
function M.cmd_jump(w)
  local n_validator = function(n)
    return tonumber(n) ~= nil and tonumber(n) > 0
  end
  local fn_validator = function(fn)
    return fn ~= nil and fs.file_exists(fn)
  end

  w:v_opt_dry_run('-D')
  w:v_opt_verbose('-v')

  local ln = w:desc('line number in the file from which take the jump path:ln')
      :pop(n_validator):argn()
  local fn = w:desc('log-file from which take path:ln')
      :def(get_logfile()):validate(fn_validator):opt('--file', '-f')
  local dir = w:desc('parent dir for search target file')
      :def('lua'):opt('--dir', '-d')

  if not w:is_input_valid() then return end
  ---@cast ln number

  local line = (fs.read_lines_in_range(fn, ln, ln) or {})[1]
  local target_fn, target_ln = nil, nil

  if w:is_verbose() or w:is_dry_run() then
    print('fn:', fn, 'lnum in log:', ln, "\nLINE:", line)
  end

  if not line then
    return w:error('cannot read line ' .. tostring(ln) .. ' in ' .. tostring(fn))
  end

  if line:sub(1, 1) == '[' then
    local s = string.gsub(line, '^%[%d...%-..%-.. ..:..:.%d%] %[[^%s]+%] ', '', 1)
    if not s then
      return w:error('cannot extract filename and linenum from', line)
    end
    line = s
  end

  -- path/to/some/source.ext:88:
  target_fn, target_ln = string.match(line, '([^%:]+)%:(%d+)%:')

  local has = fs.file_exists(target_fn)
  -- try find in specified source dir
  if not has then
    target_fn = fs.join_path(dir, target_fn)
    has = fs.file_exists(target_fn)
  end

  if w:is_verbose() or w:is_dry_run() then
    print('target_fn:', target_fn, 'target_ln:', target_ln, 'exists:', has)
  end

  if not has then
    return w:error('Cannot found file' .. tostring(target_fn))
  end

  if not w:is_dry_run() then
    vim.api.nvim_exec(":e " .. target_fn, true) -- open the file
    vim.api.nvim_exec(":" .. target_ln, true)   -- jump to the line number
  end
end

return M

-- 31-12-2024 @author Swarg
--
-- Goal:
--   open jar(war) files
--   integration with NvimTree plugin to open jar file under the cursor


local log = require 'alogger'
local cu = require 'env.util.commands'
local c4lv = require 'env.util.cmd4lua_vim'
local JarViewer = require 'env.langs.java.ui.JarViewer'
local eclipse = require 'env.bridges.eclipse'


local M = {}


--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  return c4lv.newCmd4Lua(opts)
      :root(":EnvJarViewer")
      :about("Open and inspect Jar-files (archives) in a Tree(FileSystem)-style")
      :handlers(M)
      :default_cmd('open')

      :desc('open jar(war) file in new buffer')
      :cmd('open', 'o')

      :desc('open jar file from path of jdlts buffer ')
      :cmd('open-from-jdtls', 'ofj')

      :desc('show all opened jar-viewers')
      :cmd('list', 'ls')

      :desc('close opened jar-viewer for given bufnr')
      :cmd('close', 'c')

      :desc('open decompiled class-file by given classname')
      :cmd('open-class', 'oc')

      :desc('open directory with specified package')
      :cmd('open-package', 'op')

      :desc('decompile selected file from opened jar file')
      :cmd('decompile', 'd')

      :desc('set decompiler by given name(use nil to reset)')
      :cmd('set-decompiler', 'sd')

      :desc('show current selected decompiler and its configured jar file)')
      :cmd('get-decompiler', 'gd')

      :desc('change or show output directory for decompiled and extracted files')
      :cmd('outdir', 'od')

      :desc('find inner path by given class or package')
      :cmd('find', 'f')

      :desc('expand all sub-nodes in the inner file tree')
      :cmd('expand-all', 'ea')

      :desc('collapse all sub-nodes in the inner file tree')
      :cmd('collapse-all', 'ca')

      -- set java_home ?

      :run()
      :exitcode()
end

local _commands = {
  "open", "decompile", "set-decompiler", "get-decompiler"
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

log = log
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local build_path = JarViewer.build_path
local pick_classname_from_line_in_buffer = JarViewer.code.pick_classname_from_line_in_buffer
local get_absolute_path_from_current_line, get_jar_viewer, get_jar_viewer_of
local get_list_jar_viewer

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule
-- for reload
function M.dump_state()
  _G.__env_jar_viewer_map = JarViewer.getBuffers()
end

function M.restore_state()
  JarViewer.setBuffers(_G.__env_jar_viewer_map)
  _G.__env_jar_viewer_map = nil
end

local function setJarViewerProps(w, viewer)
  if w.vars.reverse_enginering then
    w.vars.output_dir = w.vars.output_dir or build_path(nil, 'src/main/java/')
    w.vars.tmp_dir = w.vars.tmp_dir or build_path(nil, 'target/classes')
  end

  if w.vars.output_dir then
    viewer:setDecompOutDir(w.vars.output_dir)
  end
  if w.vars.tmp_dir then
    viewer:setTmpDir(w.vars.tmp_dir)
  end
end

---@param w Cmd4Lua
---@param viewer env.langs.java.ui.JarViewer
local function openFileInJar(w, viewer, inner_path)
  if viewer and w then
    inner_path = inner_path or w.vars.inner_path
    w:verbose('jar:', viewer.jar_file)
    w:verbose('inner_path:', inner_path)

    if inner_path then
      viewer:doOpenFile(inner_path, {
        decompiler = w.vars.decompiler,
        output_dir = w.vars.output_dir,
        verbose = w.vars.verbose,
        java_home = nil,
      })
    end
  end
end

--
-- open jar(war) file in new buffer
--
---@param w Cmd4Lua
function M.cmd_open(w)
  -- to open from NvimTree window (cursor under the jar-file)
  w:usage('EnvJarViewer open -i path/to/Main.class -d jb_fernflower -o ./decompiled')
  w:usage('EnvJarViewer open -o src/main/java/ -t build/classes/')

  w:v_opt_verbose('-v')
  local path2jar = w:desc('path to the jar file to open'):optional():pop():arg()
  w:desc('inner path to class file in the jar file'):v_opt('-i', '--inner-path')
  w:desc('decompiler name to decompile class-files'):v_opt('-d', '--decompiler')
  w:desc('output directory for decompiled files'):v_opt('-o', '--output-dir')
  w:desc('output directory for decompiled files'):v_opt('-t', '--tmp-dir')
  w:desc('reverse-enginering mode set directories to src/main/java & build/classes')
      :v_has_opt('-R', '--reverse-enginering')

  if not w:is_input_valid() then return end

  local err

  if not path2jar then
    path2jar, err = get_absolute_path_from_current_line()
    if not path2jar then
      return w:error(err or 'cannot pick the path to a jar-file')
    end
  end
  if path2jar == '.' then path2jar = vim.api.nvim_buf_get_name(0) end
  if path2jar == 'cl' then path2jar = vim.api.nvim_get_current_line() end
  if not path2jar then w:error('not found jar file to open.') end
  ---@cast path2jar string

  if path2jar:sub(1, 2) == '~/' then
    path2jar = v2s(os.getenv('HOME')) .. '/' .. path2jar:sub(2, #path2jar)
  end

  local viewer = JarViewer:new(nil, path2jar)

  setJarViewerProps(w, viewer)
  local ok, err0 = viewer:openJarFile()
  if not ok then return w:error(err0) end

  openFileInJar(w, viewer)
end

--
-- open jar file in new buffer
--
---@param w Cmd4Lua
function M.cmd_open_from_jdtls(w)
  local only_jar = w:desc('do not open a class-file, only jar')
      :has_opt('--only-jar', '-C')

  if not w:is_input_valid() then return end

  local t, err = eclipse.parse_jdt_path(vim.api.nvim_buf_get_name(0))
  if not t then
    return w:error(err)
  end

  local path2jar = assert(t.path, 'path')
  local clazz = ''
  if t.tail then
    clazz = t.tail:gsub('%.class', ''):gsub('%.', '/'):gsub('%(', '/') .. '.class'
  end
  local viewer, bufnr = JarViewer.findAlreadyOpened(path2jar)
  if not viewer then
    viewer = JarViewer:new(nil, path2jar)
    setJarViewerProps(w, viewer)
    local ok, err0 = viewer:openJarFile()
    if not ok then return w:error(err0) end
  elseif bufnr then
    w:say('found already opened jar in bufnr:' .. v2s(bufnr))
    vim.cmd(":b " .. v2s(bufnr))
  end

  if clazz ~= '' and clazz and not only_jar then
    openFileInJar(w, viewer, clazz)
  end
end

--
-- close opened jarviewer for given bufnr
--
---@param w Cmd4Lua
function M.cmd_close(w)
  local bufnr = w:desc('bufnr with jarviewer to be closed'):pop():argn()

  if not w:is_input_valid() then return end
  ---@cast bufnr number

  if bufnr == 0 then
    bufnr = JarViewer.getMinBufnr() -- to close first opened
  end
  local map = JarViewer.getBuffers()
  if map[bufnr or false] == nil then
    return w:error('not found jar-viewer for buffer ' .. v2s(bufnr))
  end
  local jarviewer = map[bufnr]
  map[bufnr] = nil
  w:say('closed ' .. v2s(jarviewer.jar_file))
  if jarviewer.bufnr and jarviewer.bufnr ~= 0 then
    vim.api.nvim_buf_delete(jarviewer.bufnr, { force = true })
  end
end

--
-- show all opened jar-viewers
-- way to see the bufnr to close an alredy opened jar-file
--
---@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end
  local list = get_list_jar_viewer()
  w:say(#list == 0 and 'empty' or list)
end

--
-- decompile selected file from opened jar file
--
---@param w Cmd4Lua
function M.cmd_decompile(w)
  w:usage('EnvJarViewer open -i path/to/Main.class -d jb_fernflower -o ./decompiled')

  w:v_opt_verbose('-v')
  w:desc('inner path to class file in the jar file'):v_opt('-i', '--inner-path')
  w:desc('decompiler name to decompile class-files'):v_opt('-d', '--decompiler')
  w:desc('output directory for decompiled files'):v_opt('-o', '--output-dir')

  if not w:is_input_valid() then return end

  local jar_viewer = get_jar_viewer(w)
  if not jar_viewer then return end;

  local inner_path = w.vars.inner_path

  if not inner_path then
    local is_dir
    inner_path, is_dir = jar_viewer:getInnerPathForSelectedArchiveEntry()
    if is_dir then
      return w:error('cannot decompile directory ' .. v2s(inner_path))
    end
    if not inner_path then
      return w:error('not found inner path')
    end
  end

  openFileInJar(w, jar_viewer, inner_path)
end

--
-- set decompile by given name
--
-- when you first ask to decompile a class file, you are asked which decompiler
-- to use, and then it is remembered and will be automatically taken for each
-- subsequent class-file.
-- And if you need to select another decompiler for this oy can use this command
--
---@param w Cmd4Lua
function M.cmd_set_decompiler(w)
  local decompiler_name = w:desc('active decompiler name to be set'):pop():arg()

  if not w:is_input_valid() then return end

  local jar_viewer = get_jar_viewer(w)
  if not jar_viewer then return end
  ---@cast decompiler_name string

  local success = jar_viewer:setDecompiler(decompiler_name)

  w:say('changed to non empty:' .. v2s(success))
end

--
-- show current selected decompiler and its configured jar file)
--
---@param w Cmd4Lua
function M.cmd_get_decompiler(w)
  if not w:is_input_valid() then return end

  local jar_viewer = get_jar_viewer(w)
  if not jar_viewer then return end
  local name, jar, handler = jar_viewer:getDecompiler()
  if not name then
    return w:say('no selected decompiler');
  end
  local def_decom_opts = type(handler) == 'table' and handler.DEFAULT_OPTS or ''

  w:fsay('%s %s hasModule:%s def_opts:%s',
    name, jar, type(handler) == 'table', def_decom_opts)
end

--
-- change or show output directory for decompiled files)
--
---@param w Cmd4Lua
function M.cmd_outdir(w)
  local dir = w:desc('decompiler output directory'):optional():pop():arg()
  local td = w:desc('temporary dir for extracted files'):opt('-t', '--tmp-dir')

  if not w:is_input_valid() then return end

  local jar_viewer = get_jar_viewer(w)
  if not jar_viewer then return end
  ---@cast dir string

  if not dir or dir == '?' then
    dir = jar_viewer:getDecompOutDir()
  elseif dir and dir ~= '' then
    jar_viewer:setDecompOutDir(dir)
  end

  if not td or td == '?' or td == '' then
    td = jar_viewer:getTmpDir()
  else
    jar_viewer:setTmpDir(td)
  end

  w:say('decompiler output directory: ' .. v2s(dir))
  w:say('temporary directory: ' .. v2s(td))
end

--
-- open decompiled class-file by given classname
--
-- for example, so that you can quickly go to decompiled source code while
-- standing on the line with the import of a class
--
---@param w Cmd4Lua
function M.cmd_open_class(w)
  w:usage(':EnvJarViewer open-class . -d jb_fernflower -o ./decomplied')

  w:v_opt_verbose('-v')
  local cn = w:desc('inner path or classname or . to pick from import'):pop():arg()
  w:desc('resolve classname as inner of the current'):v_has_opt('-i', '--inner-class')

  w:desc('decompiler name to decompile class-files'):v_opt('-d', '--decompiler')
  w:desc('output directory for decompiled files'):v_opt('-o', '--output-dir')
  w:desc('bufnr of jar-viewer (to support multiple jars'):v_opt('-b', '--bufnr')

  if not w:is_input_valid() then return end

  if cn == '.' then
    local err
    local inner = w.vars.inner_class
    cn, err = pick_classname_from_line_in_buffer(inner)
    if not cn then return w:error(err) end
  end

  if not cn or #cn < 1 then
    return w:error('no classname')
  end
  local ext = match(cn, '%.(%l%w+)$')
  if ext == nil then
    cn = string.gsub(cn, '%.', '/') .. '.class'
  end

  local jar_viewer = get_jar_viewer_of(w, tonumber(w.vars.bufnr) or 0) -- first
  if not jar_viewer then return end;

  local inner_path = cn

  openFileInJar(w, jar_viewer, inner_path)
end

--
-- open directory with specified package (and class) to show in JarViewer
-- to quick jump from source code into JarViewer with auto moving to right place
--
---@param w Cmd4Lua
function M.cmd_open_package(w)
  w:v_opt_verbose('-v')
  local dir = w:desc('inner directory to show'):optional():pop():arg()
  w:desc('bufnr of jar-viewer (to support multiple jars'):v_opt('-b', '--bufnr')

  if not w:is_input_valid() then return end
  local jar_viewer = get_jar_viewer_of(w, tonumber(w.vars.bufnr) or 0) -- first
  if not jar_viewer then return end;

  w:verbose('inner_path:', dir)
  ---@cast dir string
  local cls, err
  -- pick package from import or a package deinition line
  if not dir or dir == '.' then
    dir, cls, err = JarViewer.code.pick_package_from_line_in_buffer()
    if not dir then return w:error(err) end
  end
  _, err = jar_viewer:openFileTreeElm(dir, cls)
  if err then return w:error(err) end
end

--
-- helpers

--
-- helper
--
---@param w Cmd4Lua
---@return env.langs.java.ui.JarViewer?
function get_jar_viewer(w)
  local bufnr = vim.api.nvim_get_current_buf()
  local jar_viewer = (JarViewer.getBuffers() or E)[bufnr]
  if not jar_viewer then
    w:error('Not Found Alredy Opened JarViewer for current buffer')
    return nil
  end
  return jar_viewer
end

---@param w Cmd4Lua
---@return env.langs.java.ui.JarViewer?
---@param bufnr number
function get_jar_viewer_of(w, bufnr)
  local jar_viewer
  if not bufnr or bufnr == 'first' or bufnr == 0 then
    jar_viewer = JarViewer.getFirstViewer()
    if not jar_viewer then
      w:error('There is no any opened JarViewer')
      return nil
    end
  else
    jar_viewer = JarViewer.getByBufnr(bufnr)
  end

  if not jar_viewer then
    w:error('Not found JarViewer for bufnr: ' .. v2s(bufnr))
  end

  return jar_viewer
end

--
-- show mapping bufnr -> jar
--
function get_list_jar_viewer()
  local map = JarViewer.getBuffers() or E
  local bufs = {}
  for k, _ in pairs(map) do
    bufs[#bufs + 1] = k
  end
  table.sort(bufs)
  local t = {}
  for i = 1, #bufs do
    local bufnr = bufs[i]
    local jar = (map[bufnr] or E).jar_file
    t[#t + 1] = fmt('%2d: %4d - %s', i, v2s(bufnr), v2s(jar))
  end

  return table.concat(t, "\n")
end

--
-- if installed NvimTree and current "window"(buffer) is NvimTree - pick from here
-- or pick from plain text in the current buffer(todo)
--
---@return string? path to jar
---@return string? errmsg
function get_absolute_path_from_current_line()
  local bufname = vim.api.nvim_buf_get_name(0)
  if not bufname or bufname == '' then
    return nil, 'cannot get bufname'
  end

  if match(bufname, "NvimTree_%d$") then
    local node = require 'nvim-tree.lib'.get_node_at_cursor()
    return (node or E).absolute_path;
  else
    local ext = match(bufname, '%.(%w+)$')
    if ext == 'java' or ext == 'class' then
      -- todo via package? to search dep-artifact library?
      return nil, 'expected cursor in FileTree, but not at the source code'
    end
  end

  return nil, 'unsupported buffer source to pick path to the jar-file'
end

--

--
-- find inner path by given class or package
--
---@param w Cmd4Lua
function M.cmd_find(w)
  local name = w:desc('class or package name to search'):pop():arg()
  local is_lua_pattern = w:desc('is lua pattern'):has_opt('--pattern', '-p')

  if not w:is_input_valid() then return end

  local jar_viewer = get_jar_viewer_of(w, tonumber(w.vars.bufnr) or 0) -- first
  if not jar_viewer then return end;

  local ok, err = jar_viewer:findEntry(name, is_lua_pattern)
  if not ok then
    return w:error(err)
  end
  w:say(ok)
end

--
-- expand all subdirs
--
---@param w Cmd4Lua
function M.cmd_expand_all(w)
  if not w:is_input_valid() then return end
  local jar_viewer = get_jar_viewer_of(w, tonumber(w.vars.bufnr) or 0) -- first
  if not jar_viewer then return end;
  jar_viewer:expandAll()
end

--
-- collapse all archive entries(subdirs)
--
---@param w Cmd4Lua
function M.cmd_collapse_all(w)
  if not w:is_input_valid() then return end
  local jar_viewer = get_jar_viewer_of(w, tonumber(w.vars.bufnr) or 0) -- first
  if not jar_viewer then return end;
  jar_viewer:collapseAll()
end

return M

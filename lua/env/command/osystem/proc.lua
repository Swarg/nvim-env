-- 02-12-2024 @author Swarg
-- EnvOS proc

local ok_linux_util, u_proc_statm = pcall(require, 'linux_util.proc_statm')


local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
---@param w Cmd4Lua
function M.handle(w)
  w
      :about('Sysem process management and monitoring')
      :handlers(M)

      :desc('send the signal to the given process')
      :cmd('kill', 'k')

      :desc('parse low-level process info')
      :cmd('parse', 'p')

      :run()
end

local P = {}
--
-- parse low-level process info
--
---@param w Cmd4Lua
function M.cmd_parse(w)
  w
      :about('input-ipc-server')
      :handlers(P)

      :desc('parse /proc/PID/statm to readable')
      :cmd('statm', 'sm')

      :run()
end

--
-- parse /proc/PID/statm to readable
--
---@param w Cmd4Lua
function P.cmd_statm(w)
  local input = w:desc('content of the /proc/pid/statm'):pop():arg()
  local u = w:desc('output units bytes, Kb, Mb'):def('Mb'):opt('--units', '-u')

  if not w:is_input_valid() then return end

  if not ok_linux_util then
    return w:error('not found linux_util rock')
  end ---@cast u_proc_statm table

  if input == '.' then
    input = vim.api.nvim_get_current_line()
  end
  local factor, readable = 1, ''
  u = string.lower(u)
  if u == 'mb' or u == 'm' then
    factor = 1024 * 1024
    readable = 'Mb'
  elseif u == 'kb' or u == 'k' then
    factor = 1024
    readable = 'Kb'
  end

  local t = u_proc_statm.parse(input, factor)
  if type(t) ~= 'table' then
    return w:error('cannot parse input')
  end

  w:say(u_proc_statm.fmt_as_kv(t, factor, readable))
end

return M

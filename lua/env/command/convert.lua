-- 03-02-2024 @author Swarg
--
local log = require('alogger')
local cu = require("env.util.commands")
local c4lv = require("env.util.cmd4lua_vim")
local convert = require("env.util.convert");
local clipboard = require("env.util.clipboard")

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvConvert')
      :about('Convert values')
      :handlers(M)

      :desc('Convert given string(char) to bytes')
      :cmd('string-to-bytes', 's2b')

      :desc('Convert given sequense of bytes into string')
      :cmd('bytes-to-string', 'b2s')

      :cmd("hex-to-decimal", 'h2d')
      :cmd("decimal-to-hex", 'd2h')

      :desc('seconds from epoch to readable datatime')
      :cmd("millist-to-date", 'm2d')
      :cmd("date-to-millis", 'd2m')

      :run()
end

local _commands = {
  'string-to-bytes', 'bytes-to-string',
  'hex_to_decimal', 'decimal_to_hex',
}
M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local function insert_to_buff(lines)
  local lnum = (vim.api.nvim_win_get_cursor(0) or {})[1] or 0
  vim.api.nvim_buf_set_lines(0, lnum, lnum, false, lines)
end

local function output(w, ret)
  if w.vars.copy and clipboard.copy_to_clipboard(ret) then
    print('copied')
  elseif w.vars.insert then
    insert_to_buff({ ret })
  else
    print(ret)
  end
end

---@param w Cmd4Lua
local function define_shared_opts(w)
  w:desc('copy to system clibboard'):v_has_opt('--copy', '-C')
  w:desc('return as lua-code'):defOptVal(''):opt('--lua-code', '-l')
  w:desc('insert result into current line'):has_opt('--insert', '-i')
end

--
-- Convert given string(char) to bytes
--
---@param w Cmd4Lua
function M.cmd_string_to_bytes(w)
  define_shared_opts(w)
  local str = w:desc('string to convert'):pop():arg()

  if not w:is_input_valid() then return end ---@cast str string

  local ret = ''
  local as_tbl = w.vars.lua_code
  for i = 1, #str do
    local b = string.byte(str, i, i)
    if #ret > 0 then
      if as_tbl then ret = ret .. ', ' else ret = ret .. ' ' end
    end
    ret = ret .. tostring(b)
  end

  if as_tbl then
    ret = '{ ' .. ret .. ' }'
    if as_tbl ~= '' then
      ret = 'local ' .. as_tbl .. ' = ' .. ret
    end
  end

  output(w, ret)
end

--
-- Convert given sequense of bytes into string
--
---@param w Cmd4Lua
function M.cmd_bytes_to_string(w)
  w:usage(':EnvConvert b2s [226,148,128]')
  w:usage(':EnvConvert b2s "226,148,128"')
  w:usage(':EnvConvert b2s 35') -- #

  define_shared_opts(w)
  local raw_bytes = w:desc('bytes to convert'):pop():argl()
  if not w:is_input_valid() then return end

  if raw_bytes and #raw_bytes == 1 and type(raw_bytes[1]) == 'string' then
    raw_bytes = raw_bytes[1]
  end
  local bytes
  if raw_bytes and #raw_bytes > 0 and type(raw_bytes[1]) == 'number' then
    bytes = raw_bytes
  else
    bytes = convert.srt2numarr(raw_bytes)
  end

  if type(bytes) == 'table' then
    output(w, string.char(unpack(bytes)))
  else
    log.debug('error raw:bytes:%s, bytes:%s', raw_bytes, bytes)
  end
end

--
---@param w Cmd4Lua
function M.cmd_hex_to_decimal(w)
  define_shared_opts(w)
  local hex = w:desc('hex'):pop():arg()
  if not w:is_input_valid() then return end ---@cast hex string
  output(w, tostring(convert.hex_to_decimal(hex)))
end

--
---@param w Cmd4Lua
function M.cmd_decimal_to_hex(w)
  define_shared_opts(w)
  local decimal = w:desc('decimal'):pop():arg()
  if not w:is_input_valid() then return end ---@cast decimal string
  output(w, tostring(convert.decimal_to_hex(tonumber(decimal) or 0)))
end

--
-- local temp = os.date("*t", 906000490) -- produce the table
-- { year = 1998, month = 9, day = 16, yday = 259, wday = 4,
-- hour = 23, min = 48, sec = 10, isdst = false}

--
-- Seconds from Epoch to readable datatime
--
---@param w Cmd4Lua
function M.cmd_millist_to_date(w)
  define_shared_opts(w)
  local sec = w:desc('milliseconds from epoch'):pop():argn()
  if not w:is_input_valid() then return end
  output(w, tostring(os.date("%Y-%m-%d %H:%M:%S", tonumber(sec))))
end

--
--
--
---@param w Cmd4Lua
function M.cmd_date_to_millis(w)
  w:usage('date-to-millis "2024-02-04 12:12:12"')
  define_shared_opts(w)
  local dt = w:desc('datetime to seconds from epoch'):pop():arg()
  local time = w:optional():pop():arg()
  local pattern = w:desc('pattern to parsing date from string')
      :def("(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)"):opt('--pattern', '-p')

  if not w:is_input_valid() then return end

  if time then dt = dt .. ' ' .. time end -- case two args: date + time

  -- Assuming a date pattern like: yyyy-mm-dd hh:mm:ss
  assert(type(dt) == 'string', 'dt')
  -- local pattern = "(%d+)-(%d+)-(%d+) (%d+):(%d+):(%d+)"
  local y, m, d, h, min, s = dt:match(pattern)

  local nsec = os.time({
    year = y, month = m, day = d, hour = h, min = min, sec = s
  })
  output(w, tostring(nsec))
end

return M

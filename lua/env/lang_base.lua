--
-- Base functionality to any proglang platrorm.
-- Abstract class for one like c,java,lua,...
--
-- 23-05-2023 @author Swarg
local M = {}
local fs = require('env.files')


-- Deprecated
--
---@param path string path to new file
---@param callback function before open new file
---@param userdata any?
function M.confirm_create_new_file(path, callback, userdata)
  assert(type(path) == 'string', 'Expected String - full path name')
  local confirm = false
  vim.ui.input({ prompt = "Create New Test File: " .. path .. ' ? (y/N)' },
    function(input)
      if input and input == 'y' or input == 'Y' or input == 'yes' then
        confirm = true
        local dir = vim.fs.dirname(path)
        vim.cmd(':silent !mkdir -p ' .. dir)
        if type(callback) == 'function' then
          local body = callback(path, userdata)
          if type(body) == 'string' then
            if not fs.file_exists(path) then
              fs.str2file(path, body, 'w')
            else
              error('File Already Exists' .. path)
            end
          end
        end
        vim.api.nvim_exec(":e " .. path, true)
      end
    end)
  return confirm
end

-- interactive table editing and ask confirmation to continue
---@param t table
---@param message string|function
function M.ask_confirmation_and_edit(t, message)
  if t and message then
    local res = false
    local message0 = message
    if type(message) == 'function' then
      message0 = message(t)
    end

    vim.ui.input({ prompt = message0 .. ' -- Continue? ([Y]es|[E]dit|[Q]uit) ' },
      function(input)
        if (input == "e" or input == "E" or input == 'edit') then
          for k, v in pairs(t) do
            M.interactive_edit_filed(t, k, v)
          end
          -- after edit before continue ask confirmation of updated values
          res = M.ask_confirmation_and_edit(t, message)
        elseif (input == "y" or input == "Y" or input == "yes") then
          res = true
        end
      end
    )
    return res
  end
end

function M.interactive_edit_filed(t, k, v)
  if t and k then
    vim.ui.input({ prompt = k .. ' [' .. v .. ']:' },
      function(input)
        if input and input ~= "" then
          t[k] = input
        end
      end)
  end
end

-- function M.check_edit()
--   local t = { key1 = "value1", key2 = 'value2', key3 = 'value3' }
--   M.ask_confirmation_and_edit(t, 'message')
--   print(vim.inspect(t))
-- end

return M

-- 25-09-2023 @author Swarg
local M = {}
--
local fs = require('env.files')
local su = require('env.sutil')
local ui = require('env.ui')
local log = require('alogger')
local spawner = require('env.spawner')

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

function M.run(opts)
  -- print(inspect(opts))
  local ctx = {}
  ui.create_buf_nofile(ctx, "Make: " .. tostring(opts.args))
  local pp = spawner.new_process_props( -- create cmd to run test
    ctx.out_bufnr,                      -- output of process show only in status
    'make',
    opts.fargs or {},
    uv.cwd()
  )
  pp.wait_attach = true
  -- pp.handler_on_read =
  spawner.run(pp)
end

return M

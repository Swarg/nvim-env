-- 02-08-2024 @author Swarg
-- Goals:
--  - improve standart jdtls.ui.pick_many
--  - make it possible to call constructor generation using a hotkey

local jdtls = require 'jdtls'
local ui = require 'jdtls.ui'


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local function index_of(xs, term)
  for i, x in pairs(xs) do
    if x == term then
      return i
    end
  end
  return -1
end

-- improved version of original nvim-jdtls.pick_many
-- makes it possible to specify all the necessary elements with one input
-- simply indicate the numbers separated by a space and 0 at the end for
-- confirmation
function M.pick_many(items, prompt, label_f, opts)
  if not items or #items == 0 then
    return {}
  end

  label_f = label_f or function(item)
    return item
  end
  opts = opts or {}

  local choices = {}
  local selected = {}
  local is_selected = opts.is_selected or function(_)
    return false
  end
  for i, item in pairs(items) do
    local label = label_f(item)
    local choice = string.format("%d. %s", i, label)
    if is_selected(item) then
      choice = choice .. " *"
      table.insert(selected, item)
    end
    table.insert(choices, choice)
  end

  while true do
    local msg = fmt("\n%s\n%s (Esc to finish): ", table.concat(choices, "\n"), prompt)
    local answer = vim.fn.input(msg)
    if answer == "" then
      break
    end
    -- 1 2 3 0 -- selec needed in one line
    local indexes = M.split(answer, tonumber)
    for _, index in ipairs(indexes) do
      -- local index = tonumber(index)
      if type(index) == 'number' then -- ~= nil then
        if index <= 0 then            -- end of input
          return selected
        end
        local choice = choices[index]
        local item = items[index]
        if string.find(choice, "*") == nil then
          table.insert(selected, item)
          choices[index] = choice .. " *"
        else
          choices[index] = string.gsub(choice, " %*$", "")
          local idx = index_of(selected, item)
          table.remove(selected, idx)
        end
      end
    end
  end
  return selected
end

function M.split(line, for_each)
  local t = {}
  local has_for_each = type(for_each) == 'function'
  for s in string.gmatch(line, '([^%s]+)') do
    table.insert(t, has_for_each == true and for_each(s) or s)
  end
  return t
end

-- hack to improve
ui.pick_many = M.pick_many

local function make_code_action_params(bufnr, from_selection)
  local params
  if from_selection then
    params = vim.lsp.util.make_given_range_params()
  else
    params = vim.lsp.util.make_range_params()
  end
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  params.context = {
    diagnostics = vim.lsp.diagnostic.get_line_diagnostics(bufnr),
  }
  return params
end

function M.genContructorsPrompt()
  local fn = (jdtls.commands or E)['java.action.generateConstructorsPrompt']
  if type(fn) == 'function' then
    local bufnr = vim.api.nvim_get_current_buf()
    fn(nil, { params = make_code_action_params(bufnr, false), bufnr = bufnr })
  else
    print('not Found')
  end
end

return M

-- 02-08-2024 @author Swarg
-- :EnvPlugin nvim-tree.lua open
-- ~/.local/share/nvim/site/pack/packer/start/nvim-tree.lua/doc/nvim-tree-lua.txt
--
-- show keybindings in NvimTree_1 window: g?
-- how to get path from file-entry inside NvimTree_1 see JarViewer
--

local log = require 'alogger'
local fs = require 'env.files'

local class = require 'oop.class'
local Lang = require 'env.lang.Lang'

-- local ok_nvim_tree,  api = require 'nvim-tree.api'
local ok, nvim_tree = pcall(require, 'nvim-tree')
local api, Event

if ok and type(nvim_tree) == 'table' then
  api = require 'nvim-tree.api'
  Event = api.events.Event
else -- stub
  local e = {}
  api, Event = e, e
end


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug

---@return env.lang.Lang?
local function getLangForFile(fname)
  if not fname or fname == '' then
    return nil
  end
  local ext = fs.extract_extension(fname)
  if ext and ext ~= '' then
    local lang = Lang.getLang(fname)
    if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
      return lang:resolve(fname)
    end
  end
  return nil
end


---@param t table
local function onNewFile(t)
  local lang = getLangForFile((t or E).fname)
  if lang then lang:onNewFile(t.fname) end
end

-- same for files and dirs
---@param t table
local function onNodeRenamed(t)
  local lang = getLangForFile((t or E).fname)
  if lang then lang:onFileRenamed(t.old_name, t.new_name) end
end

-- ---@param t table
-- local function onFileRemove(t)
--   return t.fname
-- end
--
-- ---@param t table
-- local function onDirRemove(t)
--   return t.folder_name
-- end


function M.setup_integration()
  log_debug("setup_integration")

  api.events.subscribe(Event.FileCreated, onNewFile)
  api.events.subscribe(Event.NodeRenamed, onNodeRenamed)
  -- api.events.subscribe(Event.FileRemoved, onFileRemove)
  -- api.events.subscribe(Event.FolderRemoved, onDirRemove)
end

return M

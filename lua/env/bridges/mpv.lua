-- 24-05-2024 @author Swarg
-- Goal:
--  - client for input-ipc-server of mpv
--
-- Dependencies:
--  - luaposix
--  - cjson
--
-- Docs:
--   https://mpv.io/manual/master/#properties
--   https://mpv.io/manual/master/#command-interface-sub-ass-extradata
--   https://mpv.io/manual/master/#command-interface-track-list
--   https://mpv.io/manual/master/#command-interface-property-list
--
--   https://luaposix.github.io/luaposix/modules/posix.sys.socket.html#send
--   https://pubs.opengroup.org/onlinepubs/9699919799/functions/send.html
--   https://pubs.opengroup.org/onlinepubs/9699919799/functions/recv.html
--

local log = require 'alogger'
local posix = require 'posix'
local socket = require 'posix.sys.socket'
local unistd = require 'posix.unistd'
local cjson = require 'cjson'

local fs = require 'env.files'
local uvtt = require 'env.langs.vtt.utils'
-- local inspect = require 'inspect'


-- Consts
local SOCKET_NAME = 'mpvctl'
local UID = tostring(os.getenv("UID") or unistd.getuid())
local RUNTIME_DIR = os.getenv("XDG_RUNTIME_DIR") or ("/run/user/" .. UID)
local SUCCESS_FLAG = '"error":"success"'
local next_reqid = 100
local RTIMEOUT = 350 -- to read from socket after "cast"
local NULL = cjson.null

local M = {}
M.DOCS = {
  "https://mpv.io/manual/master/",
  "https://mpv.io/manual/master/#properties",
  "https://mpv.io/manual/master/#lua-scripting",
  "https://github.com/mpv-player/mpv/blob/master/DOCS/man/input.rst",
}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local is_log_debug, log_debug = log.is_debug, log.debug


--------------------------------------------------------------------------------

-- singleton todo keep connection state on hot code reload
local mpvcon = {
  ok = nil,
  fn = nil, -- unix socket filename
  fd = nil, -- socket filedescriptor
  errcode = nil,
  errmsg = nil,
  --
  wd = nil,
  sub = nil,
  subtype = nil,
}

-- for hot code reloading to keep connection
function M.dump_state()
  local state = {
    mpvcon = mpvcon
  }
  _G.__env_mpv_bridge = state
end

function M.restore_state()
  local state = _G.__env_mpv_bridge
  _G.__env_mpv_bridge = nil
  mpvcon = state.mpvcon
end

-- socket_dir = os.getenv("MPV_SOCKET_DIR") or (os.getenv("XDG_RUNTIME_DIR") or "/run/user/"..posix.unistd.getuid()).."/mpvsockets"

---@param offset any -- number?|string?
function M.get_socket_name(offset)
  local n = tonumber(offset) or 0
  return RUNTIME_DIR .. '/' .. SOCKET_NAME .. '.' .. n
end

---@param offset number?
---@return string?
function M.get_opened_socket_name(offset)
  assert(offset ~= nil, 'offset')

  local filename = nil

  if type(offset) == 'number' or tonumber(offset) ~= nil then
    filename = M.get_socket_name(offset)
  else
    filename = v2s(offset)
  end

  local file, _, err = io.open(filename)
  log_debug('check socket filename:', filename, file, err)

  if file == nil and err == 6 then -- 6 == socket (maybe not portable?)
    return filename
  elseif file then
    io.close(file)
  end

  log_debug('not found opened socket for %s', filename)

  return nil
end

--------------------------------------------------------------------------------


-- connect to unix socket for given filename
---@return table?
function M.connect_to_socket(filename)
  local alive_socket_fn = M.get_opened_socket_name(filename)
  if not alive_socket_fn or alive_socket_fn == '' then
    return nil
  end

  local fd = assert(socket.socket(socket.AF_UNIX, socket.SOCK_STREAM, 0))

  local ok, errmsg, errcode = socket.connect(fd, {
    family = socket.AF_UNIX,
    path = alive_socket_fn,
  })

  return {
    fn = alive_socket_fn,
    fd = fd,
    ok = ok,
    errmsg = errmsg,
    errcode = errcode,
  }
end

---@return boolean
function M.is_connected()
  return type(mpvcon) == 'table' and mpvcon.fd ~= nil
end

local is_connected = M.is_connected

-------------------------------------------------------------------------------
--       tools to get data from the json response without decoding

-- -1 if not found
---@return number
function M.get_requestid(line)
  -- "request_id":789,"
  if line then
    local reqid = string.match(line, '"request_id":%s*(%d+)[,}]')
    return tonumber(reqid) or -1
  end
  return -1
end

-- local SUCCESS_FLAG = '"error":"success"'
---@param line string
---@return boolean
function M.is_success_response(line)
  -- { ... }
  return type(line) == 'string' and line:find(SUCCESS_FLAG, 1, true) ~= nil
end

--
---@return boolean
---@param lines table?
---@param resp_idx number?
---@param req_id number?
function M.is_success_for(lines, resp_idx, req_id, hint)
  if type(lines) == 'table' and resp_idx and req_id then
    local json = lines[resp_idx]
    if is_log_debug() then
      log_debug('[%s] ret[%s/%s]: "%s"', hint or '', resp_idx, #lines, json)
    end
    return M.is_success_response(json) and M.get_requestid(json) == req_id
  end
  return false
end

---@param str_json string?
---@param hint string?
---@return table?
function M.decode_json(str_json, hint)
  local ok_json, decoded = pcall(cjson.decode, str_json)

  if not ok_json or type(decoded) ~= 'table' then
    local err_msg = decoded
    hint = hint or ''
    log_debug('Cannot parse json response[%s]: %s', hint, err_msg)

    return nil -- 'cannot parse json response: ' .. v2s(str_json)
  end

  return decoded
end

--------------------------------------------------------------------------------

-- split by "\n" return remains chars in ends of buf without "\n"
---@param t table
---@param buf string
---@param readed string
---@param wait_reqid number?
---@return string remains in buf
---@return number index in t with needed req_id
local function append_buf_to_lines(t, buf, readed, wait_reqid)
  local req_id_at_idx = -1
  if t and readed and readed ~= '' then
    local p, len = 1, #readed
    while p do
      local i = string.find(readed, "\n", p, true)
      if i then
        local line = readed:sub(p, i - 1)
        log_debug('add line:', line)
        if buf ~= '' then
          line = buf .. line
          buf = ''
        end
        t[#t + 1] = line
        p = i + 1

        -- ched on req_id
        if wait_reqid and req_id_at_idx == -1 then
          if M.get_requestid(line) == wait_reqid then
            req_id_at_idx = #t
          end
        end
      else
        break
      end
    end
    local remains = len >= p and (len + 1 - p) or 0
    return (remains > 0) and (buf .. readed:sub(p)) or buf, req_id_at_idx
  else
    log_debug('append: hast:%s buf:%s', t ~= nil, readed)
  end

  return buf, req_id_at_idx
end

---@param flag boolean
---@param step number
---@param revents table?
local function debug_show_revent_state(flag, step, revents)
  if flag then
    local s = ''
    for k, v in pairs(revents or E) do
      s = s .. ' ' .. v2s(k) .. ':' .. (v == true and '1' or '0')
    end
    log_debug('step:%s revents: "%s"', step, s)
  end
end

--
-- "Drain incomming data from socket"
-- based on posix.poll - it allows reading without blocking the current process
--
-- Read ready-to-read data from the socket by placing each json line in a
-- separate table element(as string, without json-decoding into lua-table)
--
-- If you only need to check whether there is any data ready for reading, then
-- set timeout = 0 in this case there will be no waiting for a new portion of
-- incoming data, but only a check to see if there is data already ready to read.
--
-- If you need to read the response after a command sent to the socket, set an
-- acceptable timeout for reading or -1 for infinite waiting.
--
---@param timeout number?
---@param lines table? output to collect incomming lines
---@return table lines
---@return number index of response with waiting req_id
function M.read_socket_to_lines(timeout, lines, waiting_reqid)
  assert((mpvcon or E).fd, 'expected already connected to mpv via ipc')

  log_debug('read from socket fd:%s wreq_id:%s', mpvcon.fd, waiting_reqid)
  lines = lines or {}

  local fds = {
    [mpvcon.fd] = { events = { IN = true } },
  }

  --  0 - to check that there is already previously received data ready for reading
  -- -1 - for 'infinite' waiting the event occurs
  timeout = timeout or 0

  local ret, buf, resp_idx = 1, '', -1
  local step = 0 -- debugging

  local is_debug = is_log_debug()

  while ret ~= 0 do
    step = step + 1
    -- if step > 32 then log_debug('anti infinity loop: stop', step) break end

    -- posix.poll check is any of given fds ready to read (POLLIN)
    -- its return:
    --   - 0 - on timeout, without any events
    --   - 1 - on ready to read
    ret = posix.poll(fds, timeout)

    log_debug('step:%s poll-ret:%s lines:%s bufsz:%s timeout:%s wrid:%s idx:%s',
      step, ret, #lines, #buf, timeout, waiting_reqid, resp_idx)

    if ret == -1 then
      log_debug("poll() failed")
      break
    end

    if ret == 0 then
      -- wait_attempt - an attempt to fix the case of early exit from the
      -- reading loop when the response to the desired Request_ID has not yet
      -- been received.
      -- if (not waiting_reqid or resp_idx > 0) or wait_attempt > 3 then
      -- log_debug("timeout expired attempt: %s", wait_attempt)
      log_debug("timeout expired")
      break
      -- end
      -- wait_attempt = wait_attempt + 1
    end


    for fd, _ in pairs(fds) do
      -- revents: IN:1 PRI:0 OUT:0 ERR:0 NVAL:0 HUP:0
      local revents = (fds[fd] or E).revents

      if not revents then
        log_debug('no revents for fd: %s', fd)
      else
        debug_show_revent_state(is_debug, step, revents)

        -- POLLIN: ready to read, must ret == 1
        if revents.IN and ret == 1 then
          log_debug('step:%s revent.IN reading...', step)
          local res = posix.read(fd, 1024)

          if is_debug then
            log_debug('step:%s recived: %s %s', step,
              (res ~= nil and #res or -1), (res or ''):gsub("\n", ""))
          end
          local resp_idx0
          buf, resp_idx0 = append_buf_to_lines(lines, buf, res, waiting_reqid)
          if resp_idx == -1 then resp_idx = resp_idx0 end
          if buf == '' then
            timeout = RTIMEOUT -- or 0?
            -- check is recived response to needed req_id if so - work is done
            if waiting_reqid then
              if resp_idx > -1 then
                break
              else
                timeout = 1000 -- give more time to fetch needed req_id
              end
            end
          else
            timeout = 1000
          end

          -- if has any incomming data switch to notmal recving mode i.g
          -- set next_timeout to 1sec to waite next portion of the data
          timeout = (buf == '') and 0 or 1000 -- no timeout or 1 sec
        end

        -- A device has been disconnected, or a pipe or FIFO has been closed by
        -- the last process that had it open for writing
        if revents.HUP then
          log_debug('close on hup', fd)
          fds[fd] = nil
          M.shutdown_connection() -- posix.close(fd)
          if not next(fds) then
            break
          end
        end
        -- OUT  - Normal data may be written without blocking.
        -- PRI  - High-priority data may be read without blocking.
        -- ERR  - An error has occurred on the device or stream.
        -- NVAL - The specified fd value is invalid.
      end
    end
  end

  log_debug('reading done. lines read:%s resp_idx:%s', #lines, resp_idx)
  return lines, resp_idx
end

--
-- send command to mpv via ipc and read response, then
-- if the response is received successfully, return the data or nil on fail
--
---@return table?, table? -- data, decoded-json
function M.call_cmd(...)
  log_debug('call_cmd...')

  -- send message
  local req_id = M.cast_cmd(...)

  -- recv response
  local msg_queue = {}
  local lines, resp_idx = M.read_socket_to_lines(RTIMEOUT, msg_queue, req_id)
  if resp_idx < 0 then
    log_debug('try one more time')
    lines, resp_idx = M.read_socket_to_lines(RTIMEOUT, msg_queue, req_id)
  end
  -- TODO perfome events from msg_queue

  local success = M.is_success_for(lines, resp_idx, req_id)

  local decoded, data = nil, nil
  if success then
    decoded = M.decode_json(lines[resp_idx or false])
    data = (decoded or E).data
  end

  if is_log_debug() then
    log_debug('call_cmd: req_id:%s success:%s idx:%s data:%s decoded:%s',
      req_id, success, resp_idx, type(data), type(decoded))
  end

  return data, decoded -- fail
end

--
--
--
---@param verbose boolean?
function M.status(verbose)
  if mpvcon then
    local msg = fmt('socket(%s):%s wdir:%s',
      v2s(mpvcon.fd), v2s(mpvcon.fn), v2s(mpvcon.wd))

    if verbose then
      msg = msg .. fmt(" errcode:%s errmsg:%s",
        v2s(mpvcon.errcode), v2s(mpvcon.errmsg))
    end

    return msg
  end
  return 'not connected'
end

--
---@return boolean
---@return string?
function M.shutdown_connection()
  if mpvcon and mpvcon.fd then
    log_debug("shutdown_connection fd: %s (%s)", mpvcon.fd, mpvcon.fn)
    socket.shutdown(mpvcon.fd, socket.SHUT_RDWR)
    local fn = mpvcon.fn
    mpvcon = nil -- clear
    return true, fn
  end
  return false, nil
end

---@param req_id number
---@param cmd string
function M.build_cmd_json(req_id, cmd, ...)
  local args = ''
  for i = 1, select('#', ...) do
    local arg = select(i, ...)
    if arg ~= nil then
      local q = type(arg) == 'string' and '"' or ''
      args = args .. ',' .. q .. v2s(arg) .. q
    end
  end
  return fmt('{"command":["%s"%s],"request_id":%d}%s', cmd, args, req_id, "\n")
end

--
---@param fn string?
---@return boolean
---@return string
function M.connect_to_mpv(fn, force)
  log_debug("connect_to_mpv fn:%s hascon:%s force:%s", fn, mpvcon ~= nil, force)

  if is_connected() and force then
    M.shutdown_connection()
  end

  if not is_connected() then
    fn = fn or M.get_opened_socket_name(0)
    local con = M.connect_to_socket(fn)
    if not con then
      mpvcon = nil
      -- mpv file.mkv --input-ipc-server=/run/user/1000/mpvctl.0
      return false, fmt("Not Foun socket: '%s' for connect to the mpv\n%s\n%s%s",
        v2s(fn),
        "ensure that the inpus-ipc-server is activated in the mpv.",
        'mpv file.mkv --input-ipc-server=',
        M.get_socket_name(fn)
      )
    end

    if con.errcode then
      local errmsg = fmt('Error "%s": %s %s',
        v2s(con.fn), v2s(con.errcode), v2s(con.errmsg))
      -- os.remove(con.fn)
      return false, errmsg
    end

    mpvcon = con
    M.get_working_directory()

    return true, 'connected to ' .. v2s(mpvcon.fn)
  end

  return true, fmt('already connected to socket: %s', mpvcon.fn)
end

--
---@param cmd string
---@return boolean, any?  -- ok, response
function M.send_custom_cmd(cmd, ...)
  assert(type(cmd) == 'string', 'cmd')
  local data, t = M.call_cmd(cmd, ...)
  if data == NULL and t then
    return t.error == 'success', nil
  end
  return data ~= nil, data
end

---@return number? reqid
function M.cast_cmd(cmd, ...)
  if mpvcon and mpvcon.fd then
    local req_id = next_reqid + 1
    next_reqid = next_reqid + 1
    local request_json = M.build_cmd_json(req_id, cmd, ...)
    log_debug('cast_cmd: send request[%s]: "%s"', req_id, request_json)
    socket.send(mpvcon.fd, request_json)
    return req_id
  end
  return nil
end

--
---@return string?
function M.cmd_get_time_pos()
  local data, _ = M.call_cmd('get_property', 'time-pos')
  ---@diagnostic disable-next-line: return-type-mismatch
  return data
end

--
-- filenam - Currently played file, with path stripped.
-- If this is an URL, try to undo percent encoding as well.
-- See also:
-- https://mpv.io/manual/master/#command-interface-filename
-- https://mpv.io/manual/master/#command-interface-path
-- https://mpv.io/manual/master/#command-interface-stream-open-filename
---@return string
function M.cmd_get_payling_filename(noext)
  local arg = (noext == true or noext == 'noext') and 'filename/no-ext' or 'filename'
  if noext == 'full' then
    arg = 'path'
  end

  local data, _ = M.call_cmd('get_property', arg)
  if type(data) == 'string' then
    local fn = data
    if noext == 'full' and not fn:find('[/\\]', 1, false) then
      fn = fs.build_path((mpvcon.wd or ''), fn)
    end
    return fn
  end
  return v2s(data)
end

-- ask forking directory
---@param force boolean?
---@return string?
function M.get_working_directory(force)
  local wd = (mpvcon or E).wd
  if force then wd = nil end -- refetch

  if not wd and is_connected() then
    local data, _ = M.call_cmd('get_property', 'working-directory')
    if type(data) == 'string' then
      mpvcon.wd = data
      wd = mpvcon.wd
    else
      log_debug('error on getting working-directory data:%s', data)
      wd = nil
    end
  end

  return wd
end

--
---@param force_update boolean?
---@return boolean, string?, string?  -- ok, subfilename, subtype
function M.cmd_get_subtitles(force_update)
  log_debug("cmd_get_subtitles force: ", force_update)
  assert(mpvcon and mpvcon.fd, 'expected already connected to mpv ipc')

  if not force_update and mpvcon.sub then
    return true, mpvcon.sub, mpvcon.subtype
  end

  assert(mpvcon.wd, 'expected already has working-directory')

  local data, _ = M.call_cmd('get_property', 'track-list')

  if type(data) == 'table' then
    for _, v in ipairs(data) do
      if type(v) == 'table' then
        if v.type == 'sub' and v.selected and v.external then
          local fn = v["external-filename"]
          if type(fn) == 'string' then
            local path = fs.build_path(mpvcon.wd, fn)
            mpvcon.sub = path
            mpvcon.subtype = v.codec -- 'webvtt'
            return path ~= nil, mpvcon.sub, mpvcon.subtype
          end
        end
      end
    end
  end

  return false, 'fail'
end

--
--
function M.mpv_pause()
  -- local reqid = M.cast_cmd('set_property', 'pause', true)
  -- local lines, resp_idx = M.read_socket_to_lines(RTIMEOUT, {}, reqid)
  -- return M.is_success_for(lines, resp_idx, reqid, 'pause')
  local data, _ = M.call_cmd('set_property', 'pause', true)
  return data ~= nil
end

--
--
function M.mpv_play()
  local data, _ = M.call_cmd('set_property', 'pause', false)
  return data ~= nil
end

---@return string current state
function M.mpv_toggle_pause()
  local data, _ = M.call_cmd('get_property', 'pause')
  if data ~= nil and type(data) == 'boolean' then
    local prev_pause_st = data
    local _, t = M.call_cmd('set_property', 'pause', not prev_pause_st)
    log_debug('toggled: ', (t or E).error) -- t.error == 'success'
    return prev_pause_st == true and 'play' or 'stop'
  end
  return 'error'
end

---@param value number
function M.mpv_seek(value)
  local _, t = M.call_cmd('seek', value, 'exact')
  return (t or E).error
end

-- move play position in mpv to value of subs timestamp under the curson in nvim
-- to move video to vtt subtitles from nvim
--
function M.sync_mpv_time_pos_with_nvim_lnum()
  local sts, _ --[[ste]] = uvtt.get_current_sub_timestamps()
  local ts = uvtt.str_timestamp_to_num(sts)
  -- local te = uvtt.str_timestamp_to_num(ste)
  if ts then
    local _, t = M.call_cmd('set_property', 'time-pos', ts)
    return (t or E).error
  end
end

--
--
function M.sync_nvim_lnum_with_mpv_time_pos()
  local data, t = M.call_cmd('get_property', 'time-pos')
  local timepos = tonumber(data)
  if not timepos then
    return false, 'cannot get time-pos from mpv', data, t
  end
  -- find lnum in nvim buff
  local ok, ret = uvtt.find_lnum_for_timepos(nil, timepos)
  if not ok or type(ret) ~= 'number' then
    return false, v2s(ret) -- errmsg
  end
  local lnum = ret
  vim.api.nvim_win_set_cursor(0, { lnum, 0 })

  return lnum, timepos
end

--
-- set offset of the current playin media file
--
---@param time_pos number in sec
function M.mpv_set_time_pos(time_pos)
  if type(time_pos) == 'number' then
    local _, t = M.call_cmd('set_property', 'time-pos', time_pos)
    return (t or E).error
  end
end

--
-- interact with user-defined custom callbacks(lua-functions
-- at ~/.config/mpv/scripts)
--
-- register_script_message
-- script_audio_tracks
function M.mpv_script_message(...)
  local data, _ = M.call_cmd('script-message', ...)
  return data ~= nil
end


--

if _G.TEST then
  M.append_buf_to_lines = append_buf_to_lines
end


return M
--[[
Notes:

on close mpv:

{"event":"playback-restart"}
{"event":"audio-reconfig"}
{"event":"video-reconfig"}
{"event":"tracks-changed"}
{"event":"end-file"}

on set_property time-pos 123.567

{"request_id":789,"error":"success"}
{"event":"seek"}
{"event":"playback-restart"}
{"event":"pause"}
{"event":"pause"}


observe_property - Watch a property for changes.
https://mpv.io/manual/master/#json-ipc-observe-property
If the given property is changed, then an event of type property-change will be
generated:

{ "command": ["observe_property", 1, "volume"] }
{ "error": "success" }
{ "event": "property-change", "id": 1, "data": 52.0, "name": "volume" }
]]

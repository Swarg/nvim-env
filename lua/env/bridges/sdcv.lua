-- 14-01-2024 @author Swarg
--  Goal: parse output of sdcv - console version of StarDict program
local M = {}
--
local su = require('env.sutil')
local ui = require('env.ui')
local u8 = require("env.util.utf8")
local spawner = require('env.spawner')



M.WIDTH = 80
M.FOUND_N_ITEMS = "^Found %d+ items, similar to"
M.CHOICE = "Your choice%[%-1 to abort%]"

---@param text string
---@param src_lang string
---@param dst_lang string
---@diagnostic disable-next-line: unused-local
function M.build_command(text, src_lang, dst_lang)
  return 'sdcv', { text }
end

--
-- Goal: check whether the sdcv output contains a prompt for data input (stdin)
-- if has - ask value from user and or continue work or terminate
function M.mk_output_handler()
  return function(pp)
    if pp and pp.output and not pp.found_checked then
      if string.match(pp.output, M.CHOICE, pp.output_off) then
        pp.found_checked = true

        -- .. "Your choice[-1 to abort]: " -- line without "\n"
        local v = ui.ask_value(pp.output, '0')
        local cancel = not v or v == '' or v == 'q' or v == 'Q' or v == '-1'

        if cancel or not spawner.write_to_stdin(pp, v .. "\n", false) then
          spawner.mk_cb_send_sigint("Cancell")(pp)
          return
        end
      end
    end
  end
end

--
-- Goal: format sdcv output ad pass to given callback (to show in nvim buffer)
---@param callback function  - to show formated lines in the editor
function M.mk_on_close_handler(callback)
  assert(type(callback) == 'function', 'callback')
  ---@diagnostic disable-next-line: unused-local
  return function(output, pp, exit_ok, signal)
    local callback0 = callback
    -- flush output to buffer
    output = output or pp.output
    if not output then
      return false
    end

    local lines = M.format_output(output)

    lines[#lines + 1] = ''
    lines[#lines + 1] = string.format('[%s] Process Closed ', (pp.pid or 0))

    if callback0 then
      callback0(pp.bufnr, lines)
    end

    return lines
  end
end

--
--
---@param o table
---@param i number
---@param skip_empty boolean?
---@param prefix string?
local function add_line(o, i, skip_empty, prefix)
  -- local len = i - 1 - o.prev_nl
  local line = o.line:sub(o.prev_nl, i - 1)
  local len = u8.utf8_slen(line)

  if not skip_empty or len > 0 then
    if prefix and prefix ~= '' then
      line = prefix .. line
    end
    if o.next_tab then
      line = o.next_tab .. line
      o.next_tab = nil -- for "  Syn : "
    end
    -- local line = prefix .. o.line:sub(o.prev_nl, i - 1)

    -- debug
    local j = string.find(line, "\n", 1, true)
    if j then
      local lines = su.split_range(line, "\n")
      for _, line0 in ipairs(lines or {}) do
        -- print(line0)
        -- o.t[#o.t + 1] = line0
        error('Found newline at |' .. tostring(line0) .. "|" .. tostring(j))
      end
    else
      if len <= M.WIDTH then
        o.t[#o.t + 1] = line
      else
        --
        local limit = 64
        while true and limit > 0 do
          local k = string.find(line, '≈', 2, true)
          if k then
            local line0 = line:sub(1, k - 1)
            if u8.utf8_slen(line0) > M.WIDTH then
              local p = string.find(line0, ')', 1, true)
              if p then
                local line00 = line0:sub(1, p)
                line0 = ' ' .. line0:sub(p + 1) -- will be added below
                o.t[#o.t + 1] = line00
              end
            end
            o.t[#o.t + 1] = line0
            line = line:sub(k)
          else
            break
          end
          limit = limit - 1
        end

        if u8.utf8_slen(line) <= M.WIDTH then
          o.t[#o.t + 1] = line
        else
          local lines = su.split_range(line, ";", 1, #line, false)
          for _, ln in ipairs(lines or {}) do
            if u8.utf8_slen(ln) <= M.WIDTH then
              o.t[#o.t + 1] = ln
            else
              local sublines = u8.utf8_fold(ln, M.WIDTH)
              for _, l in ipairs(sublines) do
                o.t[#o.t + 1] = l
              end
            end
          end
        end
      end
    end
  end
  o.prev_nl = i + 1
end

--
---@return table
function M.format_output(output)
  local t = {}

  local state = {
    handler = M.fmt_handler_head,
    line = output,
    prev_nl = 1,
    t = t,
  }

  local i = 1
  -- for i = 1, #output do
  while i <= #output do
    local c = output:sub(i, i)
    state.handler(state, c, i)
    state.pc4 = state.pc3
    state.pc3 = state.pc2
    state.pc2 = state.pc1
    state.pc1 = c
    i = i + 1
  end

  add_line(state, i, true)

  return t
end

---@param o table
---@param c string
---@param i number
function M.fmt_handler_head(o, c, i)
  if c == "\n" then
    local t = o.t
    if o.pc1 == "\n" then
      o.prev_nl = i + 1
      t[#t + 1] = ''
      if o.pc2 == "\n" then -- \n\n\n
        o.handler = M.fmt_handler_trascript
      end
    else
      add_line(o, i)
    end
  end
end

--- sdcv values
-- after head
function M.fmt_handler_trascript(o, c, i)
  if c == ']' then
    -- add_line(o, i)
    o.t[#o.t + 1] = o.line:sub(o.prev_nl, i)
    o.prev_nl = i + 1
    o.handler = M.fmt_handler_root
  elseif c == "\n" then
    add_line(o, i, true)
  end
end

-- switch between two different languages
--  ru-word En-word
--          ^c
-- crossroads of two languages
local function check_crossroads_of_two_languages(o, c, i)
  -- a	97	1 z	122	1 A	65 Z	90
  local b = string.byte(c)
  -- A-Z or a-z
  if (b >= 65 and b <= 90) or (b >= 97 and b <= 122) then
    -- print('DEBUG', i, c)
    -- print(string.byte(o.pc3), string.byte(o.pc2))
    -- print(string.byte(o.pc4), string.byte(o.pc3))

    if u8.is_utf8_ru(o.pc3, o.pc2) or u8.is_utf8_ru(o.pc4, o.pc3) then
      add_line(o, i - 1, true, '  ')
    end
  end
end

function M.fmt_handler_root(o, c, i)
  if c == ' ' then
    if o.pc1 == '.' then
      -- ' 1. noun' part of speech: noun verb adverb adjective
      --     ^
      --     c
      if o.pc3 == ' ' and o.pc2 and string.match(o.pc2, '%d') then
        add_line(o, i - 3, true) -- prev if has
        o.t[#o.t + 1] = '----------------'
        o.prev_handler = M.fmt_handler_sections
        o.handler = M.fmt_handler_add_word
      end
    end

    -- switch between two different languages
    --  ru-word En-word
    --          ^c
  elseif o.pc1 == ' ' then
    check_crossroads_of_two_languages(o, c, i)
    --
  elseif c == "\n" then
    add_line(o, i, true)
  end
end

function M.fmt_handler_add_word(o, c, i)
  if c == ' ' then
    add_line(o, i)
    o.handler = o.prev_handler
    o.prev_handler = nil
    --
  elseif c == "\n" then
    add_line(o, i, true)
  end
end

--
--  1) a) .. b) .. 2) a) .. b) ..
function M.fmt_handler_sections(o, c, i)
  if c == ' ' then
    if o.pc1 == ')' then
      -- ' 1) noun' part of speech: noun verb adverb adjective
      --     ^
      --   21c
      if o.pc3 == ' ' and o.pc2 then
        local code = string.byte(o.pc2)
        if code >= 48 and code <= 57 then -- '%d' is number 0-9
          add_line(o, i - 3, true)
          add_line(o, i)
          --
          -- a:97 -:- z:122
        elseif code >= 97 and code <= 122 then -- " a) " ascii en char
          add_line(o, i - 4, true)             -- prev line
          add_line(o, i, true, ' ')
        end
        --
      elseif o.pc4 == ' ' and o.pc3 then          -- " a) " utf8 ru char
        --  " a) " unicode ru char  а-п
        if u8.is_utf8_ru(o.pc3, o.pc2, true) then -- lowercase only
          add_line(o, i - 4, true)                -- prev line
          add_line(o, i, true, ' ')
        end
      end
      -- " 1. noun"
    elseif o.pc1 == '.' and o.pc3 == ' ' and string.match(o.pc2, '%d') then
      add_line(o, i - 3, true)
      o.t[#o.t + 1] = '----------------'
      add_line(o, i)
    end
    --
  elseif c == ':' then
    -- ' Syn : '
    if o.pc1 == ' ' and o.pc2 == 'n' and o.pc3 == 'y' and o.pc4 == 'S' then
      add_line(o, i - 5, true, '  ')
      o.next_tab = '  '
    end
    --
  elseif o.pc1 == ' ' then
    check_crossroads_of_two_languages(o, c, i)
    --
  elseif c == "\n" then
    add_line(o, i, true)

    -- Sync : ... ∙         226 136 153
    --            ^ utf8 is ^   ^   ^
  elseif string.byte(c) == 153 and string.byte(o.pc1) == 136 and
      string.byte(o.pc2) == 226 then
    add_line(o, i - 3, true)
  end
end

return M

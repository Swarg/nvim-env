--
-- Bridge with eclipse projects and nvim-jdtls
-- Goal: Determine from the .classpath file value of JRE_CONTAINER
-- and update jdtls config to set the corresponds jre as default
-- for run java projects in correct version of jdk/jre
--
-- 28-05-2023 @author Swarg
--
local fs = require('env.files')
local tu = require('env.util.tables')
local log = require('alogger')
local pcache = require 'env.cache.projects'

local U = require 'env.lang.utils.base'

local JC = require 'env.langs.java.util.consts'

local M = {}


local E, log_debug = {}, log.debug
local get_value_from_args = U.get_value_from_args
local join_path = fs.join_path
local match = string.match


M.jdtls = false -- will be passed from my nvim-settings via M.bind_jdtls()
local workspace_path = nil


---class env.bridges.eclipse.ProjectEntry
---field parent                      ProjectEntry|nil
---field type                        string?  eclipse|maven|gradle|ant?|flat?
---field has_sub_projects            boolean? (true in MultiProjects)
---field jre                         string?
---field classpath_mtime             number?
---field invisible_project_name      string?
---field invisible_project_classpath table? (settings)
--
-- elemts: project_root = {jre:string, classpath_mtime:number}
---@private
local projects_cache = {}

function M.clear_cache()
  projects_cache = {}
end

---@return table
function M.get_projects_cache()
  return projects_cache
end

--------------------------------------------------------------------------------
--           To keep state on lua-code runtime reloading (hotswap)

function M.dump_state()
  _G.__env_eclipse_bridge_state = {
    jdtls = M.jdtls,
    workspace_path = workspace_path,
    projects_cache = projects_cache,
  }
end

function M.restore_state()
  local state = _G.__env_eclipse_bridge_state
  _G.__env_eclipse_bridge_state = nil
  M.jdtls = state.jdtls
  workspace_path = state.workspace_path
  projects_cache = state.projects_cache
end

--------------------------------------------------------------------------------

---@param dir string
---@param settings table  project settings
---@private
function M.cache_put(dir, settings)
  assert(type(dir) == 'string', 'dir')
  log_debug("cache_settings %s: %s", dir, settings)
  if dir then -- ensuredirname?
    projects_cache[dir] = settings
  end
end

---@param root_dir string  project_root
---@return table?
function M.cache_get(root_dir)
  return projects_cache[root_dir or false]
end

local cache_get = M.cache_get
local cache_put = M.cache_put


--
-- function from my jdtls module:
--
-- jdtls.confirm_start_for_project
-- jdtls.get_project_root
-- jdtls.get_project_name
-- jdtls.setup_on_attach
-- jdtls.mk_config
-- jdtls.cmd_jdtls
--
---@param modt table
function M.bind_jdtls(modt, workspace)
  log_debug("bind_jdtls module:%s workspace:%s", type(modt), workspace)
  M.jdtls = modt
  -- show exposed functions
  if log.is_debug() then
    if type(modt) == 'table' then
      for k, v in pairs(modt) do
        if type(v) == 'function' then
          log_debug('jdtls.%s', k)
        end
      end
    end
  end
  workspace_path = fs.ensure_dir(workspace)
end

function M.get_workspace_path()
  if not workspace_path then
    return fs.ensure_dir(JC.DEFAULT_WORKSPACE)
  end
  return workspace_path
end

-- trigger to run jdtls for current bufname (opened java file)
---@param fn string?
function M.run_jdtls(fn)
  log_debug("run_jdtls", fn)
  if M.jdtls then
    local func = M.jdtls.confirm_start_for_project
    assert(type(func) == 'function', 'jdtls.confirm_start_for_project')
    return func({ force = true })
  end
  return 'no jdtls module'
end

---@param project_root string?
---@return boolean
function M.is_active_project(project_root)
  if type(M.jdtls) ~= 'table' then return false end
  local active = M.jdtls.is_active_project(project_root)
  return active == true or active == 'yes' -- ?
end

--
-- wrapper around nvim-jdtls plugin and command JdtUpdateConfig
-- syn with my jdtls sessings (active|ignored project)
--
---@param project_root string?
function M.jdtls_update_project_config(project_root)
  local active = M.is_active_project(project_root)
  log_debug("jdtls_update_project_config active:%s %s", active, project_root)
  if not active then
    return
  end

  local ok, nvim_jdtls = pcall(require, 'jdtls') -- nvim-jdtls
  if ok and type(nvim_jdtls) == 'table' then
    log_debug("project is active run nvim-jdtls.update_project_config")
    -- trigger: "java/projectConfigurationUpdate"
    nvim_jdtls.update_project_config()
    return true
  end
  return false
end

--
-- configured by user input as not-active(ignored) project
-- for which do not need to launch a lsp server instance
--
---@param root_dir string
---@return boolean|string
function M.isIgnoredByLsp(root_dir)
  if not M.jdtls or not root_dir then return 'no-jdtls' end

  local active = M.jdtls.is_active_project(root_dir)
  return active -- true - yes, false - no, nil - unknown(not asked from user)
end

-- Get JRE_CONTAINER defined in root_dir/.classpath and setup the default jre
-- runtime version on which the project will be launched
-- Project roots are cached with check last_modified of .classpath
-- in order not to parse the .classpath file every time for already known
-- projects
-- jre is a default jdk
--
-- used in my settings for jdtls 'user.lsp.settings.jdtls'
--
---@param config table (already built for jtls)
function M.setup_jre_runtime(config)
  local root_dir = (config or E).root_dir
  log_debug("setup_jre_runtime condif.root_dir: %s", root_dir)
  if root_dir then
    local project_settings = cache_get(root_dir) or {}

    M.update_eclipse_settings(config, project_settings)
    cache_put(root_dir, project_settings)

    M.set_default_jre_container(config, project_settings.jre)
  end
end

-- .classpath
--	<classpathentry kind="con" path="org.eclipse.jdt.launching.JRE_CONTAINER/
--	org.eclipse.jdt.internal.debug.ui.launcher.StandardVMType/JavaSE-17/"/>
--	Note: The Gradle eclipse plugin generate the jre name like: 'JavaSE-N/'
--	Eclipse IDE as 'JavaSE-N'
M.jre_container_pattern = '<classpathentry.+kind="con".+' ..
    'path="org%.eclipse%.jdt%.launching%.JRE_CONTAINER.*StandardVMType/' ..
    '([^/]+)/?"/>'


-- todo
-- idea: fix  src = "_/pkg" -> "_/" for FlatProjects without builders
-- Note: seems that "_" in src means path in workspace/data for invisible project
--
---@return table?
---@return number?
function M.parse_classpath_conf(project_root)
  local path = fs.join_path(project_root, '.classpath')
  local file_exists, lm = fs.is_file(path)
  if not file_exists then
    log_debug("not found", path)
    return nil
  end
  local t, err = U.parse_xml_body(fs.read_all_bytes_from(path))
  if type(t) ~= 'table' then
    log_debug("error on load xml", path, err)
    return nil
  end
  return t, lm
end

--
-- Parse project_root/.classpath and return JRE_CONTAINER
---@param project_root string
---@return string|nil
function M.get_jre_container(project_root)
  local path = join_path(project_root, '.classpath')
  local file = io.open(path, "rb")
  if file then
    local jre
    while true do
      local line = file:read("*l")
      if not line then
        break
      end
      jre = line:match(M.jre_container_pattern)
      if jre and jre ~= "" then
        break
      end
    end
    file:close()
    return jre
  end

  -- for line in io.lines(path) do
  --   local jre = line:match(M.jre_container_pattern)
  --   if jre and jre ~= "" then
  --     return jre
  --   end
  -- end
  return nil
end

-- return list of config.settings.java.configuration.runtimes or nil
---@param config table config for jdtls
---@return table?nil list of path config.settings.java.configuration.runtimes
function M.get_java_configuration_runtimes(config)
  return tu.tbl_value_at_path(config, "settings", "java", "configuration",
    "runtimes")
end

-- Find runtime definition by given jre name and set it as default
-- Runtimes list placed at 'config.settings.java.configuration.runtimes'
-- Remove default=true for all entries with 'name'-field not equals given jre
-- and add default=true for the entry with a 'name' equal to the given jre
-- Set the JRE_CONTAINER from eclipse project file '.classpth' as default rt
---@param config table
---@param jre string
function M.set_default_jre_container(config, jre)
  if jre and jre ~= "" and type(config) == 'table' then
    local runtimes = M.get_java_configuration_runtimes(config)
    if type(runtimes) == 'table' then
      for _, t in pairs(runtimes) do
        if type(t) == 'table' then
          if (t.name == jre) then
            log.debug('Default JRE: %s', jre)
            t.default = true
          else
            t.default = nil
          end
        end
      end
    end
  end
end

-- testing
function M.get_cached_classpath_mtime_for(root_dir)
  return (M.cache_get(root_dir) or E).classpath_mtime
end

function M.mtime_of_classpath_file(project_root)
  return fs.last_modified(fs.join_path(project_root, '.classpath'))
end

-- directory in WORKSPACE for this project what  used by jdtls to storing this
-- settings and templ files
---@param t table
function M.get_project_workspace_from_jdtlsconf(t)
  return get_value_from_args(t.cmd, '-data')
end

-- read per-project settings for given directory(project_root)
---@param jdtls_config table
---@param p_entry table - per-project settings to caching (cache-entry)
function M.update_eclipse_settings(jdtls_config, p_entry)
  local root_dir = assert(jdtls_config.root_dir, 'root from jdtls config')

  local now_mtime = M.mtime_of_classpath_file(root_dir)
  local b = not p_entry.classpath_mtime or p_entry.classpath_mtime ~= now_mtime
  if b then
    p_entry.classpath_mtime = now_mtime
    p_entry.jre = M.get_jre_container(root_dir)
    p_entry.datadir = M.get_project_workspace_from_jdtlsconf(jdtls_config)
    M.read_invisible_project_settings(root_dir, p_entry)
  end
  return p_entry
end

--[[ log from jdtls in ~/workspace(configured via nvim):
!ENTRY org.eclipse.jdt.ls.core 1 0 data time
!MESSAGE Try to create an invisible project for the workspace ../vtbgb19/hw04
!MESSAGE Creating the Java project hw04_fa5e2e7f
!MESSAGE Finished creating the Java project hw04_fa5e2e7f
!MESSAGE Successfully created a workspace invisible project hw04_fa5e2e7f
]]
--
---@param root_dir string
---@param t table - cached project settings
function M.read_invisible_project_settings(root_dir, t)
  log_debug("read_invisible_project_settings", root_dir)
  if not root_dir or (not t or E).datadir then
    return
  end
  local nomarkers = pcache.isProjectWithoutMarkers(root_dir)
  local name = M.find_invisible_project_name(root_dir, t.datadir)
  log_debug("read_invisible_project_settings %s %s", nomarkers, name, root_dir)
  if not name then
    return -- not found
  end
  t.invisible_project_name = name
  t.invisible_project_path = join_path(t.datadir, name)
  -- + .classpath | + .project
  -- t.invisible_project = M.parse_classpath_conf(invpath) -- + .classpath

  -- TODO next to impl!
end

---@param root_dir string
---@param datadir string
function M.find_invisible_project_name(root_dir, datadir)
  local dir = (fs.get_parent_dirname(root_dir) or ''):gsub("-", "%-")
  local pattern = "^" .. dir .. '_'
  local name, err = fs.find_first_in(datadir, pattern, 'directory')
  if not name and err then
    log_debug('has error on search dirname in ', datadir, pattern, err)
  end

  log_debug("find_invisible_project_name", dir, name)
  return name
end

-- JRE_CONTAINER and .classpath mtime
---@param root_dir string
function M.project_info(root_dir)
  if root_dir and M.projects_cache[root_dir] then
    local proj_entry = M.projects_cache[root_dir]
    local s = 'JRE: ' .. (proj_entry.jre or '?')
    if type(proj_entry.classpath_mtime) == 'number' then
      s = s .. ' Modified: ' .. os.date("%c", proj_entry.classpath_mtime)
          .. ' (' .. proj_entry.classpath_mtime .. ')'
    end
    return s
  end
end

--
-- ~/workspace/parent/subproj/.classpath
-- ~/workspace/.classpath
--
---@return false|string?
---@return string?
---@param conf_fn string?
function M.get_project_settings_path(path, conf_fn)
  conf_fn = conf_fn or '.classpath'

  local project_root = pcache.get_cached_root_dir_for(path)
  if not project_root or project_root == '' then
    return false, 'not found cached project_root for ' .. tostring(path)
  end
  -- todo check in root_dir .. '.classpath' for not an invisible_projects
  local t = M.cache_get(project_root or '')
  local root_dir -- [invisible] project root dir
  if not t or not t.invisible_project_name then
    local project_name = pcache.get_full_project_name(project_root)
    root_dir = join_path(M.get_workspace_path(), project_name)
    local invisname = M.find_invisible_project_name(project_root, root_dir)
    root_dir = join_path(root_dir, invisname)
    -- put to cache?
  else
    root_dir = join_path(t.datadir, t.invisible_project_name) -- cached
  end

  log_debug("get_project_settings_path root_dir:%s t:%s", project_root, t)
  return join_path(root_dir, conf_fn)
end

-- to jar
---@return table?{head, project_name, path, props, tail}
---@return string? errmsg
function M.parse_jdt_path(s)
  local head, body, tail = match(s or '', '^jdt://([^%?]+)%?=(.-)=/%%3C(.-)$')
  if not head or head == '' then
    return nil, 'not a jdt uri'
  end
  local proj_name, path, props = match(body, '^(.-)/%%5C(.-)=/(.*)$')

  if path then
    path = string.gsub(path, "%%5C", "") -- \/ -> /
  end

  local t = {}
  if props then
    local pp = 1
    local limit = 100
    while pp ~= nil and pp <= #props do
      limit = limit - 1
      if limit < 0 then error('!') end
      local pos = string.find(props, '=/=/', pp, true)
      if not pos then
        break
      end
      local s0 = string.sub(props, pp, pos - 1)
      local k, v = match(s0, '^(.-)=/(.*)$')
      if k and v then
        t[k] = string.gsub(v, "%%5C", "") -- \/ -> /
      end
      pp = pos + 4
    end
  end

  return {
    head = head, project_name = proj_name, path = path, props = t, tail = tail
  }
end

-- org.logging.core(LoggerContext.class  -->  org.logging.core.LoggerContext
function M.get_classname_from_parsed_jdt(t)
  if type(t) ~= 'table' or type(t.tail) ~= 'string' then
    return nil
  end
  return string.gsub(t.tail, '%.class$', ''):gsub('%(', '.')
end

-- jdt:// .. "%3Cjava.lang(System.class" -> java.lang.System
---@param path string
function M.get_class_from_jdt(path)
  local pattern = '^jdt://.-%%3C([^%%%(]+)%((%w+)%.(%w+)$'
  local pkg, bn, ext = match(path or '', pattern)
  if pkg and bn then
    return pkg .. '.' .. bn, ext
  end
  return nil
end

-- find in the list of all open buffers a buffer with the given buffer name,
-- what containing the given class name
--
-- java.lang.System --> jdt:// .. "%3Cjava.lang(System.class"
--
---@param clazz string
---@return false|number bufnr
---@return string?
function M.get_opened_bufnr_for_jdt_class(clazz)
  log_debug("get_opened_bufnr_for_jdt_class", clazz)
  if type(clazz) ~= 'string' or clazz == '' then
    return false, 'no classname'
  end
  for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
    local loaded = vim.api.nvim_buf_is_loaded(bufnr)
    if loaded then
      local ok, name = pcall(vim.api.nvim_buf_get_name, bufnr)
      if ok and name:sub(1, 6) == 'jdt://' then
        local clazz0 = M.get_class_from_jdt(name)
        if clazz0 ~= nil and clazz0 == clazz then
          return bufnr, nil
        end
      end
    end
  end
  return false, 'not found jdt buffer with ' .. tostring(clazz)
end

--
--
--
function M.get_clients(bufnr)
  bufnr = bufnr or 0
  local clients = {}
  local candidates = vim.lsp.get_active_clients({ bufnr = bufnr })
  for _, c in pairs(candidates) do
    local command_provider = c.server_capabilities.executeCommandProvider
    if command_provider.commands then
      table.insert(clients, c)
    end
  end

  return clients
end

--
-- when setting such a setting, it is written to a file in the .settings
-- directory to the file: .settings/org.eclipse.jdt.core.prefs
--
-- TODO view current settings java.project.getSettings
--
---@param tags string
function M.set_compiler_taskTags(tags)
  local util = require("jdtls.util")
  local settings = {
    ['org.eclipse.jdt.core.compiler.taskTags'] = tags or ''
  }
  local params = {
    command = "java.project.updateSettings",
    arguments = {
      vim.uri_from_bufnr(0),
      settings
    },
  }
  util.execute_command(params)
end

return M

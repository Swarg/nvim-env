--
-- Bridje for nvim-dup aimed to check the DAP state before run external builder
--
local M = {}
local log = require('alogger')
local fs = require 'env.files'
local su = require 'env.sutil'
local eclipse = require 'env.bridges.eclipse'

local dap_status_ok, dap = pcall(require, "dap")

-- from dap to edit breakpoints
local DAP_QUICKFIX_TITLE = "DAP Breakpoints"
local DAP_QUICKFIX_CONTEXT = DAP_QUICKFIX_TITLE

local MSG_DAP_NOT_FOUND = 'Not found dap. It looks like the nvim-dap plugin is not installed'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

-- lazy import other modules to have a lower startup footprint
local lazy = setmetatable({}, {
  __index = function(_, key)
    return require('dap.' .. key)
  end
})

---@return boolean
---@return string? -- errmsg
local function has_nvim_dap()
  if not dap_status_ok then
    log.error(MSG_DAP_NOT_FOUND)
    return false
  end
  return true
end

-- nvim-dap/lua/dap.lua
function M.has_valid_adapter(launch_settings)
  if not launch_settings or type(launch_settings) ~= "table" then
    log.error('Invalid config type %s', type(launch_settings))
    return false
  end
  if not has_nvim_dap() then return end

  local adapter = dap.adapters[launch_settings.type]
  if type(adapter) == 'table' then
    return true
  elseif type(adapter) == 'function' then
    return true
  elseif adapter == nil then
    log.error('The selected configuration references adapter `%s`, ',
      'but dap.adapters.%s is undefined', launch_settings.type, launch_settings.type)
  else
    log.error(
      'Invalid adapter `%s` for config `%s`. Expected a table or function. '
      .. 'Read :help dap-adapter and define a valid adapter.',
      adapter, launch_settings.type)
  end
  return false
end

function M.run(launch_settings, opts)
  log.debug('Run DAP launch-settings: %s opts:%s', launch_settings, opts)
  dap.run(launch_settings, opts)
end

-- Experimental before events driven
local original_dap_terminate_func
-- Goal: Has way to stop gradle daemon runned with --continuation flag(hotswap)
-- Wrap original function to proxy to call kill process
---@param callback function
---@param userdata table
function M.set_terminate_proxy(callback, userdata)
  -- restore original function
  if dap.terminate ~= M.set_terminate_proxy then
    -- remember original func (terminate_opts, disconnect_opts, cb)
    original_dap_terminate_func = dap.terminate
    -- set proxy func with own callback for userdate
    ---@diagnostic disable-next-line unused-local
    dap.terminate = function(terminate_opts, disconnect_opts, cb)
      if callback then callback(userdata) end
      dap.terminate = original_dap_terminate_func
    end
  end
end

--
---@param out table|function|nil
function M.subscribe_initalized(out)
  if not has_nvim_dap() then
    return
  end
  dap.listeners.after['event_initialized']['env'] = function(session, body)
    log.debug('Initialized: session: %s, body: %s', session, body)
    dap.listeners.before['event_initialized']['env'] = nil
    if out and type(out) == 'table' then
      out.session = session
      out.body = body
    elseif type(out) == 'function' then
      out(session, body)
    end
    -- log.debug('dap-listeners before', vim.inspect(dap.listeners.before))
    -- log.debug('dap-listeners after', vim.inspect(dap.listeners.after))
  end
end

-- -- stopped in breakpoint
-- dap.listeners.after['event_stopped']['env'] = function(session, body)
--   log.debug('Stopped: session: %s, body: %s') --, session, body)
--   dap.listeners.before['event_stopped']['env'] = nil
-- end

-- Goal: Send Sigint to self runned external debugger or another callback
-- called on close debuggind session
function M.subscribe_onclose_debug_session(callback, userdata)
  if not has_nvim_dap() then
    return
  end
  ---@diagnostic disable-next-line: unused-local
  dap.listeners.before['event_exited']['env'] = function(session, body)
    log.debug('Exited: session: %s, body: %s') --, session, body)
    dap.listeners.before['event_exited']['env'] = nil
    if callback then
      callback(userdata)
    end
  end

  ---@diagnostic disable-next-line: unused-local
  dap.listeners.after['event_terminated']['env'] = function(session, body)
    log.debug('Terminated: session: %s, body: %s') --, session, body)
    dap.listeners.before['event_terminated']['env'] = nil
    if callback then
      callback(userdata)
    end
  end

  ---@diagnostic disable-next-line: unused-local
  dap.listeners.after['disconnect']['env'] = function(session, body)
    log.debug('Disconect: session: %s, body: %s') --, session, body)
    dap.listeners.before['disconnect']['env'] = nil
    if callback then
      callback(userdata)
    end
  end
end

--------------------------------------------------------------------------------
--                           Breakpoints
--------------------------------------------------------------------------------


--
-- return a list of line numbers with breakpoints
---@param bufnr number?
---@return false|table
---@return string?
function M.get_breakpoints_lines(bufnr)
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local bps = lazy.breakpoints.get(bufnr)
  if type(bps) ~= 'table' then
    return false, 'not found for buf:' .. v2s(bufnr)
  end
  local t = {}
  for _, buf_bps in pairs(bps) do
    if type(buf_bps) == 'table' then
      for _, e in ipairs(buf_bps) do
        if type(e) == 'table' and e.line then
          t[#t + 1] = e.line
        end
      end
    end
  end
  return t, nil
end

---@param bufnr number?
---@param lnum number?
---@return false|table
---@return string? err
---@return number? bufnr
local function get_breakpoint0(bufnr, lnum)
  local api = vim.api
  local win = vim.api.nvim_get_current_win()
  bufnr = bufnr or api.nvim_get_current_buf()
  lnum = lnum or (api.nvim_win_get_cursor(win) or E)[1]

  local bps = lazy.breakpoints.get(bufnr)
  if type(bps) ~= 'table' then
    return false, 'not found breakpoint for bufnr: ' .. v2s(bufnr)
  end
  for _, buf_bps in pairs(bps) do
    if type(buf_bps) == 'table' then
      for _, e in ipairs(buf_bps) do
        if type(e) == 'table' and e.line == lnum then
          return e, nil, bufnr
        end
      end
    end
  end
  return false, "not found for buf:" .. v2s(bufnr) .. 'lnum:' .. v2s(lnum), bufnr
end

function M.get_breakpoint(bufnr, lnum)
  if not has_nvim_dap() then
    return false, MSG_DAP_NOT_FOUND
  end
  local bp, err = get_breakpoint0(bufnr, lnum)
  if not bp then
    return false, err
  end
  return bp
end

---@param bufnr number?
---@param lnum number?
---@param project_root string?
---@param as_json boolean?
---@return false|string
---@return string? errmsg
function M.breakpoint_info(bufnr, lnum, as_json, project_root)
  if not has_nvim_dap() then
    return false, MSG_DAP_NOT_FOUND
  end
  local b, err, bufnr0 = get_breakpoint0(bufnr, lnum)
  if not b then
    return false, err
  end ---@cast bufnr0 number

  if as_json then
    if type(project_root) ~= 'string' then
      return false, 'no project_root'
    end
    local e = M.create_per_file_bps_entry(bufnr0, { b }, project_root)
    local ok, json = pcall(vim.fn.json_encode, e)
    if not ok then return false, err end
    return json
  end

  return require "inspect" (b)
end

-- from dap
---param lsessions table<integer, Session>
---param fn fun(lsession: Session)
local function broadcast(lsessions, fn)
  for _, lsession in pairs(lsessions) do
    fn(lsession)
    broadcast(lsession.children, fn)
  end
end

-- wrapper around oridianl function
-- used to edit(update) existed breakpoint
-- (the copy of the original nvim-dap code - goal: to pass the bufnr and the lnum)
--
---@param bufnr number
---@param lnum number
function M.set_breakpoint(bufnr, lnum, condition, hit_condition, log_message)
  log_debug("set_breakpoint bufn:%s lnum:%s cond:'%s' hit_cond:%s log_msg:%s",
    bufnr, lnum, condition, hit_condition, log_message)

  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(lnum) == 'number', 'lnum')

  lazy.breakpoints.toggle({
    condition = condition,
    hit_condition = hit_condition,
    log_message = log_message,
    replace = true -- replace_old
  }, bufnr, lnum)

  local bps = lazy.breakpoints.get(bufnr)
  log_debug("bps", bps)

  broadcast(dap.sessions(), function(s)
    log_debug('broadcast session:', s)
    s:set_breakpoints(bps)
  end)

  if vim.fn.getqflist({ context = DAP_QUICKFIX_CONTEXT }).context == DAP_QUICKFIX_CONTEXT then
    dap.list_breakpoints(false)
  end
end

--
-- create the entry with possible several breakpoints for one file
--
---@param bufnr number -- to get absolute path to file with breakpoints
---@param bps table{{line, condition, hitCondition, logMessage}}
---@param project_root string
---@return nil|table
---@return string? errmsg
function M.create_per_file_bps_entry(bufnr, bps, project_root)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(project_root) == 'string', 'project_root')
  assert(type(bps) == 'table', 'bps')

  -- path is a some file with one or more breakpoints
  local path = vim.api.nvim_buf_get_name(bufnr)
  if not path then
    return nil, 'cannot get path for bufnr:' .. v2s(bufnr)
  end

  local per_file_entry = {
    file = nil,
    bps = {},
  }
  if path:sub(1, 6) == "jdt://" then
    per_file_entry.file = (assert(eclipse.get_class_from_jdt(path), 'class'))
    per_file_entry.is_class = true;
  else
    per_file_entry.file = fs.get_inner_path_or_full(project_root, path)
  end

  for _, bp in pairs(bps) do
    per_file_entry.bps[#per_file_entry.bps + 1] = {
      line = bp.line,
      condition = bp.condition,
      hitCondition = bp.hitCondition,
      logMessage = bp.logMessage
    }
  end

  return per_file_entry
end

--
-- save all breakpoints into given file
-- warn this code overwrite already existed file
--
---@diagnostic disable-next-line: unused-local
function M.get_all_brackpoints(project_root)
  project_root = fs.ensure_dir(project_root or vim.fn.getcwd())

  local allbps = lazy.breakpoints.get()
  if type(allbps) ~= 'table' then
    return false, 'not found breakpoints'
  end
  local t = {}

  for bufnr, bps in pairs(allbps) do
    t[#t + 1] = M.create_per_file_bps_entry(bufnr, bps, project_root)
  end

  return t
end

--
-- save all breakpoints into given file
-- warn this code overwrite already existed file
--
---@param filename string?
---@diagnostic disable-next-line: unused-local
function M.save_brackpoints(filename, project_root)
  assert(type(filename) == 'string' and filename ~= '', 'filename')

  local t, err0 = M.get_all_brackpoints(project_root)
  if not t then
    return false, err0
  end

  local bin = vim.fn.json_encode(t)
  -- override already exists
  local h, err = io.open(filename, 'wb')
  if not h then
    return false, err
  end
  h:write(bin)
  h:close()

  return true
end

---@diagnostic disable-next-line: unused-local
---@param filename string
---@return false|table
---@return string? errmsg
function M.load_brackpoints0(filename)
  assert(type(filename) == 'string' and filename ~= '', 'filename')

  local h, err = io.open(filename, 'rb')
  if not h then
    return false, err
  end
  local bin = h:read("*a")
  h:close()

  log_debug("bin:", filename, bin)
  local ok, data = pcall(vim.fn.json_decode, bin)
  log_debug("data", ok, data)
  if not ok then
    return false, data -- err
  end
  return data, nil
end

--
---@param fentry table{file, is_class, bps:table}
---@return number enabled breakpoints
---@return number disabled
local function get_enabled_breakpoints_count(fentry)
  if type(fentry) ~= 'table' or type(fentry.bps) ~= 'table' then
    return -1, -1
  end
  local ce, cd = 0, 0
  for _, bp in ipairs(fentry.bps) do
    if type(bp) == 'table' then
      if not bp.disabled then
        ce = ce + 1
      else
        cd = cd + 1
      end
    end
  end
  return ce, cd
end

---@return boolean
---@return string?
function M.open_jdt_buffer_for_class(clazz)
  log_debug("open_jdt_buffer_for_class", clazz)
  local bufnr, err = eclipse.get_opened_bufnr_for_jdt_class(clazz)
  if type(bufnr) ~= 'number' or bufnr < 1 then
    log_debug("not found the opened jdt buffer with class: %s %s", clazz, err)
    return false, err
  end

  -- todo ask jdtls to open new buff for given classname

  local ok, err2 = pcall(vim.cmd, ':b ' .. v2s(bufnr))
  if not ok then
    log_debug('cannot open buffer:%s %s', v2s(bufnr), v2s(err2))
    return false, err2
  end
  return true, nil
end

--
-- for file by given file_entry
--
---@param fentry table{file, is_class, bps:table}
---@param project_root string
---@param n number?
---@return false|number count of restored breakpoints
---@return string?
local function restore_breakpoints(fentry, project_root, n)
  log_debug("restore_breakpoints", n, (fentry or E).file)
  if not fentry.file then
    return false, 'bad file_entry - no file #' .. v2s(n)
  elseif not fentry.bps then
    return false, 'bad file_entry - no bps #' .. v2s(n)
  end
  local enabled_bps, _ = get_enabled_breakpoints_count(fentry)
  if enabled_bps < 1 then
    return false, 'no enabled breakpoints for ' .. v2s(fentry.file)
  end

  -- open saved file before restore breakpoints
  if fentry.is_class then -- regular file not jdts
    local ok, err = M.open_jdt_buffer_for_class(fentry.file)
    if not ok then
      return false, err
    end
  else -- open source file from the project

    local path
    if su.starts_with(fentry.file, project_root) then
      path = fentry.file
    else
      path = fs.join_path(project_root, fentry.file)
    end

    if not fs.file_exists(path) then
      return false, 'not found path: ' .. v2s(path)
    end
    log.debug('open ', path)
    vim.cmd(':e ' .. path)
  end

  local bufnr = vim.api.nvim_get_current_buf()
  local c = 0
  -- restore breakpoints
  for _, bp in ipairs(fentry.bps) do
    if bp.line and not bp.disabled then
      pcall(vim.api.nvim_win_set_cursor, 0, { bp.line, 1 })
      M.set_breakpoint(bufnr, bp.line, bp.condition, bp.hitCondition, bp.logMessage)
      c = c + 1
    else
      log_debug('disabled or broken breakpoint file:%s entry:%s', fentry.file, bp)
    end
  end
  return c
end

--
-- load saved breakpoints and restore in current session
--
---@param filename string
---@param project_root string
function M.load_breakpoints(filename, project_root)
  log_debug("load_breakpoints", filename, project_root)
  assert(type(filename) == 'string', 'filename')
  assert(type(project_root) == 'string', 'project_root')

  local data, err = M.load_brackpoints0(filename)
  if type(data) ~= 'table' then
    return false, 'error on load:' .. v2s(err)
  end

  log_debug('restoring breakpoints...')
  return M.apply_all_breakpoints(data, project_root)
end

--
-- restore breakpoints settings
-- e.g. to apply changed via UI
--
---@return number?
---@return string? errmsg
function M.apply_all_breakpoints(bpdata, project_root)
  if type(bpdata) ~= 'table' then
    return nil, 'breakpoints data'
  end

  local c = 0 -- a number of restored breakpoints

  for n, fentry in ipairs(bpdata) do
    local cnt, err2 = restore_breakpoints(fentry, project_root, n)
    if not cnt and type(err2) == 'string' then
      print(err2)
    end
    if type(cnt) == 'number' and cnt > 0 then
      c = c + 1
    end
  end

  return c, nil
end

---@param bufnr number
---@param lnum number
---@return false|number min
---@return string? errmsg
---@return number? max
function M.get_nearest_breakpoint_lnums(bufnr, lnum)
  local allbps = lazy.breakpoints.get(bufnr)
  if type(allbps) ~= 'table' then
    return false, 'not found breakpoint for bufnr: ' .. v2s(bufnr)
  end
  local bps = allbps[bufnr]
  if type(bps) ~= 'table' then
    return false, 'not found breakpoints for bufnr: ' .. v2s(bufnr)
  end

  local prev, next = 0, math.huge
  for _, e in ipairs(bps) do
    if type(e) == 'table' and type(e.line) == 'number' then
      if e.line < next and e.line > lnum then
        next = e.line
      end
      if e.line > prev and e.line < lnum then
        prev = e.line
      end
    end
  end
  return prev, nil, next
end

--
function M.get_next_breakpoint_lnum()
  local bufnr = vim.api.nvim_get_current_buf()
  local lnum = (vim.api.nvim_win_get_cursor(0) or E)[1]
  local prev, err, next = M.get_nearest_breakpoint_lnums(bufnr, lnum)
  if not prev then
    return false, err
  end
  if next < lnum or next == math.huge then
    return false, 'no next breakpoint'
  end

  return next
end

function M.get_prev_breakpoint_lnum()
  local bufnr = vim.api.nvim_get_current_buf()
  local lnum = (vim.api.nvim_win_get_cursor(0) or E)[1]
  local prev, err, _ = M.get_nearest_breakpoint_lnums(bufnr, lnum)
  if not prev then
    return false, err
  end
  if prev > lnum or prev <= 0 then
    return false, 'no prev breakpoint'
  end
  vim.api.nvim_win_set_cursor(0, { prev, 0 })
  return true
end

--

-- check is breakpoints has custom details such as
-- condition, hit_condition or log_messages fields
--
---@return boolean
function M.is_breakpoin_with_details(bp)
  assert(type(bp) == 'table', 'breakpoint')
  local disabled = bp.condition == '0==1' -- to emulate "on|off toggle"
  return (bp.condition ~= nil and not disabled)
      or bp.hit_condition ~= nil
      or bp.log_messages ~= nil
end

-- '0==1' - disabled flag
function M.is_bp_enabled(bp)
  return bp ~= nil and (bp.condition == nil or bp.condition == '')
end

return M

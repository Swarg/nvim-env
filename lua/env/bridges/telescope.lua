-- Extension to work with telescope.nvim
local M = {}

---@diagnostic disable-next-line
local telesope_ok, telescope = pcall(require, 'telescope')
local t_builtin_ok, telescope_builtin = pcall(require, 'telescope.builtin')

local E = {}

local nvim_replace_termcodes = ((vim or E).api or E).nvim_replace_termcodes or
    function() --[[key, f1, f2, f3) stub]] end -- for testing outside nvim


local NVIM_KEY_ESC = nvim_replace_termcodes('<esc>', true, false, true) -- "\27"

function M.has_telescope()
  return t_builtin_ok ~= nil and type(telescope_builtin) == 'table'
end

-- Run live_grep picker-finder-previewer from telescope.nvim
-- in given path
--
-- [Notes] https://github.com/BurntSushi/ripgrep/issues/2505
-- See: telescope.nvim/lua/telescope/config.lua
-- append( "vimgrep_arguments",
-- 'rg' is the ripgrep: https://github.com/BurntSushi/ripgrep/
-- [WARN] flag '--vimgrep' consumes a lot of memory,
-- especially with defaul search mode for each input
-- https://github.com/nvim-telescope/telescope.nvim/pull/2488
--
---@param path string
---@param find_str string|nil todo predefine input to search
function M.live_grep(path, find_str)
  if path and t_builtin_ok then
    local opts = {}
    -- opts.prompt_title = title or "Live Grep"
    opts.search_dirs = { path }
    --local grep_open_files = opts.grep_open_files
    opts.cwd = path --opts.cwd and vim.fn.expand(opts.cwd) or vim.loop.cwd()
    -- opt for ripgrep to work with in mode this one thread
    -- see https://github.com/BurntSushi/ripgrep/issues/2505
    --should --vimgrep run in single threaded mode by default?
    -- slower but but consumes almost no memory, because in cuncurrency mode
    -- need to store results in memory befor output it to the stdout
    opts.additional_args = { "-j1" } --  --threads count (passed+
    local has_find_str = type(find_str) == 'string' and #find_str > 2
    if has_find_str then
      opts.default_text = find_str
      local telescope_fix = require 'env.bridges.telescope-fix'
      telescope_fix.set_can_start_grep(true)
      -- tx.send()
    end

    telescope_builtin.live_grep(opts)

    if has_find_str then
      vim.api.nvim_feedkeys(NVIM_KEY_ESC, 'n', false)
    end
    -- how to check is additional_args passed to process
    -- rename /usr/bin/rg, create bash script with:
    --    #!/bin/bash
    --    echo "$@" > /tmp/args run grep
    -- Result:
    -- --color=never --no-heading --with-filename --line-number --column \
    --   --smart-case -j1 \
    --   -- findme ~/.local/share/nvim/site/pack/packer/start/telescope.nvim
  end
end

return M

--
-- [Telescope Fix: Switch Live Grep to Grep by fully entered search query]
-- Goal: turn off search for each entered character "Life Grep" in Telescope
-- Starts grep searching only by defined keybilding
-- For solving the issue of excessive resource consumption
--
-- To fix behavior of telescope plugin you need to add this lines
-- to the source code in this file:
-- telescope.nvim/lua/telescope/pickers.lua
--
--  ...
--  local fix_ok, FIX = pcall(require, 'env.bridges.telescope-fix') -- FixAddLine
--  -- print("FixFound: ".. vim.inspect(fix_ok))                  -- FixAddLine
--
--  function Picker:find()
--   ...
--   FIX.check_is_live_input_needed(self, tx, prompt_bufnr)       -- FixAddLine
--   local main_loop = async.void(function()
--     ...
--
--     rx.last()
--     if FIX.need_to_skip_live_input(self) then goto continue end -- FixAddLine
--
--     ...
--     local ok, msg = pcall(function()
--       self.finder(prompt, process_result, process_complete)
--     end)
--     FIX.notify_search_done(self)                               -- FixAddLine
--     ...
--
--       ::continue::  -- FIX --                                  -- FixAddLine
--     end
--   end)  -- end of main_loop = asunc.void
--

local M = {}

-- print the search status to the status line not to the message history
function M.notify(msg)
  if msg and msg ~= "" then
    local s = "echo " .. vim.inspect(msg)
    if vim.in_fast_event() then
      -- solve err:  must not be called in a lua loop callback
      vim.schedule(function() vim.api.nvim_command(s) end)
    else
      vim.api.nvim_command(s)
    end
  end
end

---@param can boolean
function M.set_can_start_grep(can)
  local ok, picker = pcall(require, 'telescope.pickers')
  if ok then
    picker._flag_can_start_grep = can
    return true
  end
  return false
end

-- function Picker:check_is_live_input_needed(tx, prompt_bufnr)
---@param prompt_bufnr number
function M.check_is_live_input_needed(picker, tx, prompt_bufnr)
  local lgrep = "Grep"
  local s = picker.prompt_title
  local is_grep = s and s:sub(- #lgrep) == lgrep -- ends with
  M.notify("Type request and Press <ESC><Space> or <F2> on insert mode")
  if is_grep then
    local cb = function()
      M.notify('Searching...')
      picker._flag_can_start_grep = true
      tx.send()
    end
    local bkeymap = vim.api.nvim_buf_set_keymap
    -- press space in normal mode to trigger search
    bkeymap(prompt_bufnr, 'n', '<Space>', "", { callback = cb })
    bkeymap(prompt_bufnr, 'i', '<F2>', "", { callback = cb })
  end
  picker._flag_no_live_input_for_grep = is_grep and picker.default_text == nil
end

--function Picker:need_to_skip_live_input()
---@return boolean true - skip searching for new input
function M.need_to_skip_live_input(picker)
  if picker._flag_no_live_input_for_grep then
    if picker._flag_can_start_grep ~= true then
      return true                       --goto continue -- skip search while not be presses n:' ' or i:Ctrl-j
    end
    picker._flag_can_start_grep = false -- reset to the next one
  end
  return false
end

-- function Picker:notify_search_done()
function M.notify_search_done(picker)
  if picker._flag_no_live_input_for_grep then
    M.notify('Search Done.')
  end
end

return M

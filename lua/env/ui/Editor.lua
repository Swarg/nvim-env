-- 20-09-2023 @author Swarg
--
local log = require 'alogger'
local class = require 'oop.class'
local ui = require 'env.ui'
local su = require 'env.sutil'
local fs = require 'env.files'
local spawner = require 'env.spawner'

local Report = require 'env.lang.Report'
local Context = require 'env.ui.Context'
local ExecParams = require 'env.lang.ExecParams'
local BufIterator = require 'env.ui.nvim.BufIterator'



class.package 'env.ui'
---@class env.ui.Editor: oop.Object
---@field new fun(self): env.ui.Editor
---@field ctx env.ui.Context
---@field item any
---@field searchWord string?
local Editor = class.new_class(nil, 'Editor', {
})

-- constructor used in Editor:new()
function Editor:_init()
end

local log_debug = log.debug


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


---@return boolean
function Editor.hasUI()
  local bufnr = ((vim or E).api or E).nvim_get_current_buf()
  return type(bufnr) == 'number' and bufnr > -1
end

--
---@param update boolean?
---@param parser olua.util.lang.AParser?
---@return env.ui.Context
function Editor:getContext(update, parser)
  if not self.ctx then
    self.ctx = Context:new(nil, parser)
  elseif update then
    self.ctx:update()
    self.ctx.parser = parser or self.ctx.parser
  end

  return self.ctx
end

-- return new instance of BuffIterator
--
-- to traverse lines in the specified buffer
-- Note: use Editor.iterator() insted of Editor:iterator
--
---@return olua.util.Iterator
function Editor.iterator(bufnr, lnum)
  if bufnr and type(bufnr) ~= 'number' then
    error('arg#1: expected number? bufnr, got: ' .. v2s(bufnr))
  end
  log_debug("new BufIterator for", bufnr, lnum)
  return BufIterator:new(nil, bufnr, lnum)
end

-- return current opened filename
-- ? getContext
---@return string
function Editor.getCurrentFile()
  return vim.api.nvim_buf_get_name(0)
end

function Editor.getCurrentBufnr()
  return vim.api.nvim_get_current_buf()
end

function Editor.getBufLineCount(bufnr)
  return vim.api.nvim_buf_line_count(bufnr or 0) or 0
end

---@param path string
---@return number?
function Editor.getBufnrForPath(path)
  local list = vim.api.nvim_list_bufs()
  for _, bn in ipairs(list) do
    local loaded = vim.api.nvim_buf_is_loaded(bn)
    if loaded then
      local ok, name = pcall(vim.api.nvim_buf_get_name, bn)
      if ok and name and name ~= '' and path == name then
        return bn
      end
    end
  end
  return nil
end

--
-- ? getContext
---@return string
function Editor.getCurrentLine()
  return vim.api.nvim_get_current_line()
end

---@param line_or_lines string|table
function Editor:setCurrentLine(line_or_lines)
  return self:getContext():setCurrentLine(line_or_lines)
end

---@param line_or_lines string|table
---@return boolean
function Editor:insertAfterCurrentLine(line_or_lines)
  return self:getContext(true):insertAfterCurrentLine(line_or_lines)
end

---@param line_or_lines string|table
---@return boolean
function Editor:insertBeforeCurrentLine(line_or_lines)
  return self:getContext(true):insertBeforeCurrentLine(line_or_lines)
end

function Editor:showErr(message)
  ui.echo_err(message)
end

---@param title string
---@return env.lang.Report
function Editor:newReport(title)
  if vim.schedule_wrap then
    local ctx = {}
    ui.create_buf_nofile(ctx, title)
    local out_bufnr = ctx.out_bufnr

    return Report:new({
      -- wrap for launch from inside uv.loop
      writer = vim.schedule_wrap(function(line)
        self:addLine(out_bufnr, line)
      end)
    })
    -- for testing without nvim ui
  else
    self.reports = self.reports or {}
    self.reports[title] = ''
    return Report:new({
      writer = function(line)
        self.reports[title] = self.reports[title] .. line .. '\n'
      end
    })
  end
end

-- Append line to the end of given bufnr
---@param bufnr number
---@param line string
function Editor:addLine(bufnr, line)
  if bufnr and bufnr > -1 and line then
    if not vim then
      print('Add', bufnr, line)
      return
    end

    local outpos = {} -- to pass the position of the last occurrence of '\n'
    local lines = su.split_range(line, '\n', 1, #line, outpos)
    local cnt = vim.api.nvim_buf_line_count(bufnr)
    ---@cast cnt number
    vim.api.nvim_buf_set_lines(bufnr, cnt, cnt, true, lines)

    if vim.api.nvim_win_get_buf(0) == bufnr and vim.api.nvim_win_is_valid(0) then
      vim.cmd [[ normal! G ]] -- jump to the end -- auto scroll down
    end
  end
end

function Editor:getLinesCount(bufnr)
  return vim.api.nvim_buf_line_count(bufnr)
end

function Editor:getLines(bufnr, lnum, lnum_end)
  return vim.api.nvim_buf_get_lines(bufnr, lnum, lnum_end + 1, false)
end

function Editor.getLines0(bufnr, lnum, lnum_end)
  return vim.api.nvim_buf_get_lines(bufnr, lnum, lnum_end + 1, false)
end

---@param bufnr number? if null take from selt.ctx
---@param lnum number
---@param lnum_end number
---@param lines table
function Editor:setLines(bufnr, lnum, lnum_end, lines)
  bufnr = bufnr or (self.ctx or E).bufnr
  vim.api.nvim_buf_set_lines(bufnr, lnum, lnum_end + 1, true, lines)
end

function Editor.setLines0(bufnr, lnum, lnum_end, lines)
  vim.api.nvim_buf_set_lines(bufnr or 0, lnum, lnum_end + 1, true, lines)
end

--
-- insert given lines into file to given line-number
-- if file exists but not opened in editor - open before insert
--
---@param file string
---@param lines table
---@param lnum number -- to insert to the end of file pass -1
---@param jump boolean|nil -- jump to the place of insertion
function Editor:insertLinesToFile(file, lines, lnum, jump)
  assert(type(file) == 'string', 'file')
  assert(type(lines) == 'table', 'lines')
  assert(type(lnum) == 'number', 'lnum')
  if jump == nil then jump = true end


  local bufnr = ui.get_buf_with_name(file)
  if not bufnr then
    if not fs.file_exists(file) then
      return false
    end
    self:open(file)
    bufnr = ui.get_buf_with_name(file)
    if not bufnr then
      return false
    end
  end

  if bufnr then
    if lnum < 0 then
      local lcnt = vim.api.nvim_buf_line_count(bufnr)
      -- if lnum > 1 then lnum = lnum - 1 end
      lnum = lcnt + lnum -- + 1
      -- TODO find first from back line free from comment
    end

    vim.api.nvim_buf_set_lines(bufnr, lnum - 1, lnum, true, lines)
    if jump then
      -- open and move cursor to pasted lines
      vim.api.nvim_win_set_buf(0, bufnr)
      vim.api.nvim_win_set_cursor(0, { math.max(0, lnum), 1 })
    end
    return true
  end
end

-- resolveWords or Lexeme in the current line under cursor
-- get words under cursor and add it into self.ctx:
-- word_container[.:]word_element
-- if word_element is function or method has after self '(' add method_name
--
-- word_container is the word that comes before the word under cursors
-- followed by a dot or colon (can be nil)
--
-- word_element - the word uder cursor in current line
--
---@return self
function Editor:resolveWords()
  self:getContext():resolveWords()
  return self
end

---@param exp_lexeme_type number
function Editor:getResolvedWord(exp_lexeme_type)
  return self:getContext():getWordElement(exp_lexeme_type)
end

---@param wordchars_pattern string? default is '[%a_%d]+' same as "[%w_]+"
function Editor:resolveWordSimple(wordchars_pattern)
  return self:getContext(true):resolveWordSimple(wordchars_pattern)
end

function Editor:resolveMethodOrFunction()
  return self:getContext(true):resolveWords():getResolvedMethodOrFunction()
end

---@param s string
function Editor:replaceCurrentWord(s)
  return self.ctx ~= nil and self.ctx:replaceCurrentWord(s)
end

---@return string? path (buffname)
---@return number? bufrn
---@param by_full_path boolean?
function Editor.findOpenedFile(fn, by_full_path)
  log_debug("findOpenedFile", fn)
  local path, bufnr
  if fn and fn ~= '' then
    if fn:sub(1, 1) ~= fs.path_sep then
      fn = fs.path_sep .. v2s(fn)
    end
    for _, bn in ipairs(vim.api.nvim_list_bufs()) do
      local loaded = vim.api.nvim_buf_is_loaded(bn)
      if loaded then
        local ok, name = pcall(vim.api.nvim_buf_get_name, bn)
        log_debug("buffer: ", bn, name)
        if ok and name and name ~= '' then
          if (by_full_path and name == fn or su.ends_with(name, fn)) then
            path, bufnr = name, bn
            break
          end
        end
      end
    end
  end

  log_debug("findOpenedFile %s ret: %s %s", fn, path, bufnr)

  return path, bufnr
end

-- open file by the path in the editor
---@param path string
---@param jumpto false|string|number? -- lnum or word to find
function Editor:open(path, jumpto)
  log.debug("open %s", path)
  if path and path ~= '' and vim and vim.api and vim.api.nvim_exec then
    ui.close_float_win_if_need()

    -- only select if already opened
    local bufnr = ui.get_buf_with_name(path)
    if bufnr and bufnr > 0 then
      -- open and move cursor to pasted lines
      vim.api.nvim_win_set_buf(0, bufnr)
      -- vim.api.nvim_win_set_cursor(0, { math.max(0, lnum), 1 })
    else -- open from file
      vim.api.nvim_exec(":e " .. path, true)
      -- vim.cmd(":e " .. path)
      -- trigger to load configs defined via ftplugin (highlieght and lsp)
      local ext = fs.extract_extension(path)
      if ext and ext ~= '' then
        vim.cmd(":set filetype=" .. ext)
      end
    end

    -- jumpt to lnum or to given word via search
    if type(jumpto) == 'number' then
      pcall(vim.api.nvim_exec, ":" .. jumpto, true) -- jump to the line number
    elseif type(jumpto) == 'string' and jumpto ~= '' and jumpto ~= ' ' then
      -- find word in opened file
      local ok, _ = pcall(vim.api.nvim_exec, ("/" .. jumpto), true)
      if ok then vim.api.nvim_exec(":nohlsearch", true) end
    end
    jumpto = nil
    --
  end
end

function Editor:setSearchWord(word)
  self.searchWord = word
end

-- UseCases:
--  - auto jump to a new generated test method for a new test-file
--
function Editor:findWord()
  if type(self.searchWord) == 'string' and self.searchWord ~= '' then
    log.debug('search Word %s', self.searchWord) -- method name
    local _, _ = pcall(vim.api.nvim_exec, "/" .. self.searchWord, true)
  end
end

-- function Editor.ask()
--   print('|'..tostring(Editor.askValue({}, 'msg:'))..'|')
-- end

--
-- ask value and return input or default
-- on empty input it returns a nil value not ''
--
---@param message string
---@param default string|nil
---@param completion string|nil
function Editor.askValue(message, default, completion)
  local output = nil
  message = message or "Value"
  vim.ui.input({ prompt = message, default = default, completion = completion },
    function(input)
      output = input
    end)
  -- for stub.vim take default only by "pressed" enter by user
  if output == "\n" then
    output = default
  end
  log.debug('askValue %s answer: |%s| default:', message, output, default)
  return output
end

--
---@param items table
---@param prompt string
function Editor:askItems(items, prompt)
  local mod = require 'env.bridges.nvim-jdtls'
  return mod.pick_many(items, prompt)
end

-- one
---@param items table
---@return number
function Editor.pickItemIndex(items, prompt)
  local s = ''
  for n, item in ipairs(items) do
    s = s .. fmt("%2d %s\n", n, v2s(item))
  end
  prompt = prompt or 'Choose Index: '
  local output
  vim.ui.input({ prompt = s .. prompt }, function(input)
    output = tonumber(input)
  end)

  return output
end

---@param ask_msg string?     default is "Do you agree?"
---@param callback function?  action on confirm
---@return any? output from callback or true on confirm Yes or nil
function Editor:askToAction(ask_msg, callback, userdata, extradata)
  local output = nil
  ask_msg = ask_msg or "Do you agree"
  vim.ui.input({ prompt = ask_msg .. ' ? (y/N) ' },
    function(input)
      if su.is_yes(input) then
        output = true
        if type(callback) == 'function' then
          output = callback(userdata, extradata)
        end
      end
    end)
  return output
end

---@param bname string
---@param params env.lang.ExecParams {cwd, cmd, args, env, opts}
---@return number of vim buff with output (-1 - errors, -2 no out_bufnr)
function Editor:runCmdInBuffer(bname, params)
  ExecParams.validate(params, bname)
  bname = bname or params.desc or 'NoName'

  local cwd = params.cwd or fs.cwd()

  log_debug("runCmdInBuffer", params)
  if cwd and params.cmd and params.args then
    local ctx = {}
    ui.create_buf_nofile(ctx, bname)       -- buffer to show stdout of command
    ui.bind_bufs(ctx.bufnr, ctx.out_bufnr) -- to jump back to source buffer
    local opts = params.opts or {}

    local t = params
    local pp = spawner.new_process_props(ctx.out_bufnr, t.cmd, t.args, cwd)
    pp.envs = params.envs
    pp.hide_start = opts.hide_start ~= nil and opts.hide_start or true
    pp.src_bufnr = opts.src_bufnr
    if opts.handler_on_read then pp.handler_on_read = opts.handler_on_read end
    if opts.handler_on_close then pp.handler_on_close = opts.handler_on_close end
    spawner.run(pp)

    return ctx.out_bufnr or -2
  end
  return -1
end

-- Show message with given highlieght group(color) in the status bar
---@param msg string?
---@param hlgroup string?
---@param keep_in_messages boolean? (:messages)
function Editor:echoInStatus(msg, hlgroup, keep_in_messages)
  if msg then
    ui.echohl(msg, hlgroup, keep_in_messages)
    -- vim.schedule(function() vim.notify(line) end)
  end
end

---@param async boolean?
function Editor:errInStatus(msg, async)
  if msg then
    ui.echohl(msg, 'Error', async)
  end
end

---@param line string
---@param linenr number
---@param bufnr number? def 0 - current
---@return boolean
function Editor:insertLineBefore(line, linenr, bufnr)
  bufnr = bufnr or 0
  if vim.api and type(linenr) == 'number' then
    local lines = vim.api.nvim_buf_get_lines(bufnr, linenr - 1, linenr, true)
    if lines and lines[1] then
      lines = { line, lines[1] }
      vim.api.nvim_buf_set_lines(bufnr, linenr - 1, linenr, true, lines)
      return true
    end
  end
  return false
end

class.build(Editor)
return Editor

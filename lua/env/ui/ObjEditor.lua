-- 15-07-2024 @author Swarg
--
-- Goal: edit given object(lua-table) as code in new separated nvim buffer
-- with hooks on :e!(read) and :w (write)
--

local log = require 'alogger'
local su = require 'env.sutil'
local tu = require 'env.util.tables'

local StatefulBuf = require 'env.draw.ui.StatefulBuf'

local M = {}


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug


-- ask confirmation and edit
--
---@param t table
---@param message string|function?
---@param title string? name for Buffer with table to edit
---@param do_callback function
---@param cb_on_cmd function?  - will be called on `:e!` - can be used to
--                               validate or update user input
---@param key_order table?     - to keep predefined order of the keys in t
function M.interactive(t, message, title, do_callback, cb_on_cmd, key_order)
  assert(type(t) == 'table', 't')
  assert(type(do_callback) == 'function', 'do_callback')

  local msg = message or ''
  if type(message) == 'function' then
    msg = message(t) -- build message dynamically
  end

  vim.ui.input({ prompt = msg .. ' -- Continue? ([Y]es|[E]dit|[Q]uit) ' },
    function(input)
      if (input == "e" or input == "E" or input == 'edit') then
        M.create(t, title, do_callback, cb_on_cmd, key_order)
      elseif (input == "y" or input == "Y" or input == "yes") then
        do_callback(t)
      end
    end
  )
end

--
-- create new Buffet to edit given table in nvim buffer
--
---@param t table - the table to edit
---@param title string?
---@param on_write_cb function
---@param on_cmd_cb function?
---@param key_order table?
---@param float boolean?
---@return false|number bufnr
---@return string? errmsg
---@return string? bufname
function M.create(t, title, on_write_cb, on_cmd_cb, key_order, float)
  assert(type(t) == 'table', 't')
  title = title or 'EditTableInBuf'

  local dispatcher = {
    on_cmd = M.handler_on_cmd, -- to perform :e!
    on_write = M.handler_on_write,
    on_read = M.handler_on_read,
  }
  local vars = {
    data = {
      original = t,          -- original table
      key_order = key_order,
      on_cmd = on_cmd_cb,    -- to validate user input via `:e!`
      on_save = on_write_cb, -- apply updated user input
      parent_bufnr = vim.api.nvim_get_current_buf(),
    }
  }
  local lines = {
    float and ("-- " .. v2s(title)) or '', -- only for float buffer
    "-- Edit a given state, and call tha command `:w` to apply the changes",
  }
  if on_cmd_cb ~= nil then
    lines[#lines + 1] =
    '-- To validate input before applying it, call the command `:e!`'
  end
  if float then
    lines[#lines + 1] =
    "-- You can close this floating window by pressing the q key"
  end
  M.tbl2lines(t, key_order, lines)

  return StatefulBuf.create_buf(title, lines, dispatcher, vars, float)
end

--
-- create buffer in the floating window
--
function M.create_float(t, title, on_write_cb, on_cmd_cb, key_order)
  return M.create(t, title, on_write_cb, on_cmd_cb, key_order, true)
end

---@param bufnr number
---@param lines table of lines
local function set_lines(bufnr, lines)
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, lines)
end
---@return table of string
---@param key_order table?
---@param out table?
function M.tbl2lines(t, key_order, out)
  out = out or {}
  return su.split(tu.table2code(t, '  ', nil, key_order, true), "\n", out)
end

-- lines2tbl
---@param bufnr number
---@return false|table
---@return string? errmsg
function M.tbl_from_lines_in_buf(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, false)
  if not lines or #lines < 1 then
    return false, 'empty lines bufnr:' .. v2s(bufnr)
  end
  local ok, data = StatefulBuf.get_buf_var(bufnr, 'data')
  if not ok then
    return false, data -- errmsg
  end
  local ok2, updated = tu.luacode2table(table.concat(lines, "\n"))
  if not ok2 then return false, v2s(updated) end -- errmsg
  if type(updated) ~= 'table' then return false, 'cannot convert to table' end

  return updated
end

-- debugging
local function lcnt(t)
  if (t or E).buf then
    local cnt = t.buf and vim.api.nvim_buf_line_count(t.buf) or -1
    return 'lines:' .. cnt
  end
  return '?'
end

--
-- triggers on `:w` nvim command
--
---@param t table{buf, event, file, id, match}
function M.handler_on_write(t)
  log_debug("tbl_editor_handler_on_read:", t.event, lcnt(t))

  local data, err = StatefulBuf.get_buf_var(t.buf, 'data')
  if not data then return print(err) end
  if type(data.on_save) == 'function' then
    local current, err2 = M.tbl_from_lines_in_buf(t.buf) -- update data.current
    if not current then print(err2) end
    data.on_save(current, data.original)
    StatefulBuf.close(t.buf, data.parent_bufnr)
  end
end

--
-- just refill the "EditTableInBuf"-buffer from data.current or data.original
--
-- on :e! first triggers BufReadCmd then BufWinEnter
--
---@param t table{buf, event, file, id, match}
function M.handler_on_read(t)
  log_debug("tbl_editor_handler_on_read:", t.event, lcnt(t))
  local data, err = StatefulBuf.get_buf_var(t.buf, 'data')
  if not data then return print(err) end

  if type(data.current) == 'table' then
    set_lines(t.buf, M.tbl2lines(data.current, data.key_order))
  elseif type(data.original) == 'table' then
    set_lines(t.buf, M.tbl2lines(data.original, data.key_order))
  else
    print('no data.current and data.original')
  end
end

--
--
---@param t table
---@param cmd string cmdline from vim.fn.getcmdline()
function M.handler_on_cmd(t, cmd)
  log_debug("tbl_editor_handler_on_cmd:", t.event, lcnt(t), cmd)

  if cmd == 'e!' or cmd == 'edit' or cmd == 'e!' or cmd == 'edit!' then
    local data, err = StatefulBuf.get_buf_var(t.buf, 'data')
    if not data then return print(err) end

    if type(data.on_cmd) == 'function' then
      local current, err2 = M.tbl_from_lines_in_buf(t.buf) -- update data.current
      if not current then print(err2) end

      data.on_cmd(current, cmd, data.original)
      data.current = current
      StatefulBuf.set_buf_var(t.buf, 'data', data)
    end
  end
end

function M.str_to_bool(s)
  return s == true or s == 'true' or s == 't' or s == '1' or s == 'yes' or s == 'y'
end

return M

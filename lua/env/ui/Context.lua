-- 29-09-2023 @author Swarg
local class = require 'oop.class'

local log = require 'alogger'
local su = require 'env.sutil'
local lapi_consts = require 'env.lang.api.constants'
local Selection = require 'env.ui.Selection'
local BufIterator = require 'env.ui.nvim.BufIterator'

local cp = require 'env.util.code_parser' -- deprecated todo remove


class.package 'env.ui'
---@class env.ui.Context: oop.Object
---@field new fun(self, o:table?, parser:olua.util.lang.AParser?): env.ui.Context
---@field parser olua.util.lang.AParser?
---@field bufnr number
---@field bufname string
---@field can_edit boolean
---@field cursor_pos table?
---@field current_line string?
---@field lines_count number?
---@field cursor_row number
---@field cursor_col number
--                                  resolved context: words under the cursor:
---@field word_element string?   -- current element(lexeme) under the the cursor
---@field word_container string? -- lexeme before the current word
---@field word_elm_start number? -- the start position of the word in current_line
---@field word_elm_end number?   -- the end position of the word in current_line
---@field lexeme_type number?    -- of word_element
local Context = class.new_class(nil, 'Context', {
})

Context.LEXEME_TYPE = lapi_consts.LEXEME_TYPE
Context.LEXEME_NAMES = lapi_consts.LEXEME_NAMES
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug, log_trace = log.debug, log.trace

-- constructor used in Editor:new()
---@param parser olua.util.lang.AParser
function Context:_init(parser)
  self:update()
  self.parser = parser
  -- self.bufnr = -1
  -- self.can_edit = false
  -- self.cursor_row = 0 -- lnum
  -- self.cursor_col = 0 -- character
end

--
-- static factory
--
---@param bufnr number?
---@param lnum number?
---@param col number?
---@param parser olua.util.lang.AParser?
---@return env.ui.Context
function Context.of(bufnr, lnum, col, parser)
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  lnum = lnum or 1
  col = col or 1
  local obj = {
    bufnr = bufnr,
    bufname = vim.api.nvim_buf_get_name(bufnr),
    can_edit = true,
    cursor_pos = { lnum, col },
    cursor_row = lnum,
    cursor_col = col,
    parser = parser,
    current_line = (vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum, true) or E)[1],
  }
  ---@diagnostic disable-next-line: return-type-mismatch
  return setmetatable(obj, Context)
end

-- factory: return info about current buffer
---@return self
function Context:update()
  local api = vim.api
  self.bufnr = api.nvim_get_current_buf()
  assert(type(self.bufnr) == 'number' and self.bufnr > 0,
    'expected correct bufnr, got:' .. tostring(self.bufnr))

  self.bufname = api.nvim_buf_get_name(self.bufnr)
  self.can_edit = api.nvim_buf_get_option(self.bufnr, 'modifiable')
  self.current_line = api.nvim_get_current_line()
  self.cursor_pos = nil
  self:getCurrentLineNumber()
  return self
end

---@return boolean
function Context:isModifiable()
  return self.can_edit == true
end

---@param enable boolean
function Context:setModifiable(enable)
  assert(type(enable) == 'boolean', 'f')
  vim.api.nvim_buf_set_option(self.bufnr, 'modifiable', enable)
end

---@return number
function Context.getLinesCount(self)
  local count = vim.api.nvim_buf_line_count(0)
  if type(self) == 'table' then
    self.lines_count = count
  end
  return tonumber(count) or -1
end

---@return string
function Context:getCurrentBufname()
  if not self.bufname then
    self:update()
  end
  return self.bufname
end

function Context:getCurrentLine()
  if not self.current_line then
    self:update()
  end
  return self.current_line -- or vim.api.nvim_get_current_line()
end

-- cursor coords(Position) in buffer
function Context:getCurrentLineNumber()
  self.cursor_pos = self.cursor_pos or vim.api.nvim_win_get_cursor(0)

  if self.cursor_pos then
    self.cursor_row, self.cursor_col = self.cursor_pos[1], self.cursor_pos[2]
  else
    self.cursor_row, self.cursor_col = 0, 0
  end

  return self.cursor_row, self.cursor_col
end

---@return string current line
---@return number cursor_row
---@return number cursor_col
local function validated_curr_line_and_cursor_pos(self)
  local cline = self.current_line
  assert(type(cline) == 'string', 'current_line got:' .. tostring(cline))
  assert(type(self.cursor_row) == 'number', 'cursor row (lime number)')
  assert(type(self.cursor_col) == 'number', 'cursor col (character in line)')
  return cline, self.cursor_row, self.cursor_col
end

-- lexem under the cursor
---@return self
function Context:selectLexem()
  log_debug("resolveWordsWithParser", self.parser ~= nil)
  assert(self.parser, 'parser')

  local lexems = self.parser.splitLineToLexemes(self.current_line)
  self.selection = Selection:new({
    bufnr = self.bufnr,
    lnum = self.cursor_row,
    line = self.current_line,
    col = self.cursor_col + 1, -- convert from nvim column coords
    lexems = lexems,
  })
  return self
end

-- of current word under the cursor in buffer
---@return number?
---@return string?
---@return number? index of word for given pos
function Context:getLexemeType()
  if not self.selection then
    error('expected resolvedLexem')
  end
  local lexems = assert(self.selection.lexems, 'selection.lexems')
  local col = assert(self.selection.col, 'selection.col')
  local lexer = self.parser.lexer()
  local i, word = lexer.indexOfByPos(lexems, col)
  local lt = lexer.getTypeOfPos(lexems, col)

  return lt, word, i
end

--
---@param predicate function? predicate to stop parsing at the desired place
---@param lnum_start number?
---@return olua.util.lang.AParser
function Context:parse(predicate, lnum_start)
  assert(self.bufnr, 'bufnr')
  assert(self.parser, 'parser')
  lnum_start = lnum_start or 0

  return self.parser
      :setIterator(BufIterator:new(nil, self.bufnr, lnum_start), lnum_start)
      :prepare(predicate)
end

---@param scn string
---@return table {found, variants}
function Context:parseAndFindFullClassName(scn, res)
  assert(self.parser, 'parser')
  assert(type(scn) == 'string' and scn ~= '', 'expected string short classname')

  res = res or {}
  self:parse(self.parser:factory().newPredicateFindClassName(scn, res))

  return res
end

--
-- !deprecated
--
-- resolveWords or Lexeme in the current line under cursor
-- get words under cursor and add it into self.ctx:
-- word_container[.:]word_element
-- if word_element is function or method has after self '(' add method_name
--
-- word_container is the word that comes before the word under cursors
-- followed by a dot or colon (can be nil)
--
-- word_element - the word uder cursor in current line
--
--- param line string? if nil then auto fetch current line from buffer
--- param cursor_pos table? if nil then auto fetch from api.nvim_win_get_cursor
--                          {line_nr, column_nr}
---@return self
function Context:resolveWords()
  log.debug("resolveWords")
  validated_curr_line_and_cursor_pos(self)
  local s, e

  -- self.ctx.current_line = line
  -- word(lexeme|token) under cursor (current)
  self.word_element, s, e = cp.get_word_at(self.current_line, self.cursor_col + 1)
  -- word before(left) current word
  self.word_container = cp.get_container_name(self.current_line, s)

  -- is method|function?
  -- TODO deligate lexeme type definition to the specific Lang Module(Class)
  self.lexeme_type = cp.get_lexeme_type(self.current_line, self.word_element, s, e)

  log.debug('found: [%s][%s] lexeme_type %s ',
    self.word_container, self.word_element, self.lexeme_type
  )
  self.word_elm_start, self.word_elm_end = s, e

  -- if lexeme_type == Context.LEXEME_TYPE.FUNCTION then
  --   self.method_name = self.word_element
  -- end
  return self
end

---@param chars_pattern string?
function Context:resolveWordSimple(chars_pattern)
  local line, _, col = validated_curr_line_and_cursor_pos(self)
  local word, s, e = cp.get_word_at(line, col + 1, nil, chars_pattern)
  self.word_element, self.word_elm_start, self.word_elm_end = word, s, e
  self.lexeme_type = Context.LEXEME_TYPE.UNKNOWN
  log_debug("resolveWordSimple word:%s(%s:%s) currline:%s", word, s, e, line)
  return self
end

---@param self env.ui.Context?
function Context.hasCurrentLine(self)
  return self ~= nil and self.bufnr ~= nil and
      self.current_line ~= nil and type(self.cursor_row) == 'number'
end

local function hasWordElm0(self)
  return self ~= nil and
      type(self.word_element) == 'string' and
      type(self.word_elm_start) == 'number' and
      type(self.word_elm_end) == 'number'
end

---@param s string
function Context:replaceCurrentWord(s)
  s = s or ''
  if not Context.hasCurrentLine(self) or not hasWordElm0(self) then
    return false
  end
  local cl = self.current_line or ''
  local left = string.sub(cl, 1, self.word_elm_start - 1)
  local right = string.sub(cl, self.word_elm_end + 1, #cl)
  local newline = left .. s .. right
  self:updateLine(newline, self.cursor_row)
  return true
end

---@param lines table
---@return boolean
function Context:replaceCurrentWordByLines(lines)
  if not Context.hasCurrentLine(self) or not hasWordElm0(self) then
    return false
  end
  if type(lines) ~= 'table' then
    -- todo check "\n"
    return self:replaceCurrentWord(tostring(lines))
  end
  local cl = self.current_line or ''
  local left = string.sub(cl, 1, self.word_elm_start - 1)
  local right = string.sub(cl, self.word_elm_end + 1, #cl)
  local newline = left .. lines[1]
  if #lines == 1 then
    newline = newline .. right
  end
  lines[1] = newline
  -- replace current line
  self:setLines(self.cursor_row, self.cursor_row, lines)
  return true
end

--
-- designed to be called when the current_line has already been received and
-- has not been modified
--
---@param comment_patt string?
---@return boolean
function Context:isCurrLineEmptyOrComment(comment_patt)
  log_debug("isCurrLineEmptyOrComment", self, comment_patt)
  -- local comment_patt = comment_patt or "^%s*//"
  local l = (self or E).current_line
  local empty = l == nil or l == '' or string.match(l, "^%s*$") ~= nil
  local comment = false
  if not empty and comment_patt and comment_patt ~= '' then ---@cast l string
    comment = string.match(l, comment_patt) ~= nil
  end
  return empty or comment
end

--
-- returns the previously received current line and removes it from the buffer
--
-- designed to be called when the current_line has already been received and
-- has not been modified
--
---@return string
function Context:pullCurrLine()
  local curr_lnum = assert(self.cursor_row, 'sure called after resolved ctx')
  self:getCurrentLineNumber()
  assert(self.cursor_row == curr_lnum, 'expected unchanged ctx state')
  local line = assert(self.current_line, 'current_line')

  -- line = match(line, "^%s*//%s*(.-)%s*$") or line -- extract from comment
  if line and line ~= '' then               -- pull
    self:setLines(curr_lnum, curr_lnum, {}) -- remove from buffer
    self:update()
  end
  log_debug("pullCurrLine lnum:%s line:|%s|", curr_lnum, line)
  return line
end

---@param ctx env.ui.Context?
---@param expectex_lexeme_type number  -- pass -1 to any lextype
---@return false|string
---@return string?
---@return number lexeme type
function Context.getWordElement(ctx, expectex_lexeme_type)
  assert(type(expectex_lexeme_type) == 'number', 'expectex_lexeme_type')
  if not ctx then
    return false, 'no context', -1
  end
  if not Context.hasCurrentLine(ctx) then
    return false, 'no current line', -1
  end
  if not hasWordElm0(ctx) then
    return false, 'no resolved word (word under the cursor)', -1
  end

  -- local varname, lextyp = ctx.word_element, ctx.lexeme_type
  if ctx.lexeme_type ~= expectex_lexeme_type or expectex_lexeme_type == -1 then
    local exp_lex = Context.LEXEME_NAMES[expectex_lexeme_type or 0] or '?'
    local res_lex = Context.LEXEME_NAMES[ctx.lexeme_type or 0] or '?'
    return false, fmt('expected lexeme_type %s(%s) got: %s(%s)',
      exp_lex, v2s(expectex_lexeme_type),
      res_lex, v2s(ctx.lexeme_type)
    ), -1
  end
  return ctx.word_element, nil, ctx.lexeme_type
end

--
-- return already resolved function or method name
-- (from curent position of the cursor in current buffer)
--
---@param ctx self?
---@return string?
function Context.getResolvedMethodOrFunction(ctx)
  local t, method_name = (ctx or E).lexeme_type, nil
  if t == Context.LEXEME_TYPE.METHOD or t == Context.LEXEME_TYPE.FUNCTION then
    method_name = (ctx or E).word_element
  end
  log_debug("getResolvedMethodOrFunction mn:%s lexeme:%s", method_name, t)
  return method_name
end

-- return a curret lexeme(word_element) value only if
-- it matched given lexeme_type or lexeme_type is not defined(nil)
-- otherwise return nil even if the element exists
--
-- UseCases:
--  - is Cursor Undex given lememe type (function|method etc)
--  - get current value of word_element (no params)
--
---@param lexeme_type number|nil
function Context:element(lexeme_type)
  local element = nil
  if not lexeme_type or self.lexeme_type == lexeme_type then
    element = self.word_element
  end
  log.debug("element has lexeme_type:%s ask:%s return: %s (actual:%s)",
    self.lexeme_type, lexeme_type, element, self.word_element
  )
  return element
end

-- lnum and lnum_end statrts from 1 not from 0
function Context:getLines(lnum, lnum_end)
  return vim.api.nvim_buf_get_lines(self.bufnr, lnum - 1, lnum_end, false)
end

---@param open_line string
---@param close_line string
---@return false|table{lines}
---@return number lnum_end (stop point)
function Context:getLinesBetween(open_line, close_line, usetrim)
  assert(type(open_line) == 'string' and open_line ~= '', 'open_line')

  ---@diagnostic disable-next-line: unused-local
  local handler = function(o, lines, lnum, total_lines)
    for i, line in ipairs(lines) do
      local line0 = line
      if usetrim and line ~= '' then
        line0 = su.trim(line)
      end
      if line0 == open_line then
        log_trace("OPEN")
        o.opened = true
      elseif line0 == close_line then
        log_trace("CLOSE state:%s", o)
        o.opened = nil
        o.lnum_end = lnum + i
        return false -- stop
      elseif o.opened then
        log_trace("ADD line:|%s|", line)
        o.lines = o.lines or {}
        o.lines[#o.lines + 1] = line
      end
    end
    return true
  end
  local ud, _ = self:processLines(handler, {})
  return ud.lines or false, ud.lnum_end or -1
end

--
---@return number starts from 1 not from 0 as in nvim_buf_get_lines
---@return number
local function get_selection_range(winnr)
  local mode = vim.api.nvim_get_mode().mode
  local cursor_pos = vim.api.nvim_win_get_cursor(winnr or 0)
  local cursor_row = (cursor_pos or E)[1] or 0 -- starts from 1 not from 0

  local ln_start, ln_end = cursor_row, cursor_row

  if mode == "V" or mode == 'v' then
    local start_row = vim.fn.getpos("v")[2] -- starts from 1 not from 0

    if start_row <= cursor_row then
      ln_start, ln_end = start_row, cursor_row
    else
      ln_start, ln_end = cursor_row, start_row
    end
  end

  return ln_start, ln_end
end

--
-- returns lnum and lnum_end starts from 1 not from 0
--
---@return table? lines
---@return number lnum
---@return number lnum_end
function Context:getSelectedLines()
  local lnum, lnum_end = get_selection_range(0)

  local ok, lines = pcall(vim.api.nvim_buf_get_lines, 0, lnum - 1, lnum_end, false)
  if not ok then
    return nil, -1, -1
  end
  return lines, lnum, lnum_end
end

-- lines are processed in portions along a given number of lines
--
---@param handler function
---@param userdata table?
---@param opts table?{batch:number, limit:number, lnum, lnum_end}
function Context:processLines(handler, userdata, opts)
  assert(type(handler) == 'function', 'expected function handler')
  opts = opts or {}
  local batch = opts.batch ~= nil and opts.batch or 32
  local limit = opts.limit ~= nil and opts.limit or 1000000
  assert(limit > 0, 'has limit')

  local lstart, lend = opts.lnum or 0, batch
  local total_lines_count = self:getLinesCount() or 0
  local stop_lnum = total_lines_count
  if type(opts.lnum_end) == 'number' and opts.lnum_end < stop_lnum then
    stop_lnum = opts.lnum_end
  end

  if total_lines_count == 0 then
    return userdata, 0
  end

  if batch > stop_lnum then
    batch = stop_lnum
  end

  while lstart < stop_lnum and limit > 0 do
    limit = limit - 1
    local lines = vim.api.nvim_buf_get_lines(self.bufnr, lstart, lend, false);
    if not lines or not handler(userdata, lines, lstart, total_lines_count) then
      break
    end

    lstart, lend = lend, lend + batch
    if lend > total_lines_count then
      lend = total_lines_count
    end
  end

  return userdata, lend
end

--
-- Note: define lnum and lnum_end starts from 1 not from 0!
--
---@param lnum number
---@param lnum_end number
---@param lines table
function Context:setLines(lnum, lnum_end, lines)
  log_debug("setLines s:% e:% lines:%s", lnum, lnum_end, lines)
  vim.api.nvim_buf_set_lines(self.bufnr, lnum - 1, lnum_end, true, lines)
  return self
end

-- set line in given range
---@param lnum number
---@param s string|table<string>
---@return boolean
function Context:updateLine(s, lnum)
  lnum = lnum or self.cursor_row
  local lines
  if type(s) == 'string' then
    if Context.is_string_with_new_lines(s) then
      error('Not implemented yet TODO: split newlines to table')
    else
      lines = { s }
    end
  elseif type(s) == 'table' then
    lines = s
  else
    error('expected string line or lines:table<string>')
  end

  if self.cursor_row and self.bufnr and lnum then
    local set_lines = vim.api.nvim_buf_set_lines
    local ok, err = pcall(set_lines, self.bufnr, lnum - 1, lnum, true, lines)
    if ok then
      return true
    end
    log.debug('error on buf_set_lines: %s', err, lines)
  end
  return false
end

function Context.is_string_with_new_lines(s)
  return type(s) == 'string' and string.find(s, "\n", 1, true) ~= nil
end

---@param line_or_lines string|table
function Context:setCurrentLine(line_or_lines)
  if line_or_lines and self.cursor_row then
    return self:updateLine(line_or_lines, self.cursor_row)
  end
  return false
end

local function prepare_to_insert(self, line_or_lines)
  if not self.cursor_row then
    self:update()
  end
  local lines
  if type(line_or_lines) == 'table' then
    lines = line_or_lines
  elseif Context.is_string_with_new_lines(line_or_lines) then
    lines = su.split_range(line_or_lines, "\n")
  else
    lines = { line_or_lines }
  end
  assert(type(lines) == 'table', 'expected table lines')

  return lines
end

---@param line_or_lines string|table
function Context:insertAfterCurrentLine(line_or_lines)
  local lines = prepare_to_insert(self, line_or_lines)
  self:setLines(self.cursor_row + 1, self.cursor_row, lines)
  return true
end

---@param line_or_lines string|table
function Context:insertBeforeCurrentLine(line_or_lines)
  local lines = prepare_to_insert(self, line_or_lines)
  self:setLines(self.cursor_row, self.cursor_row - 1, lines)
  return true
end

---@param lnum number
---@param lnum_end number
function Context:selectLines(lnum, lnum_end)
  if vim and vim.fn and vim.cmd then
    -- [bufnum, lnum, col, off]
    vim.fn.setpos("'<", { self.bufnr, lnum, 1, 0 })
    vim.fn.setpos("'>", { self.bufnr, lnum_end, 9999, 0 })
    vim.cmd([[exe "norm! gv"]])
  end
  return self
end

--
---@param lnum number?
---@param col number?
function Context:setCursorPos(lnum, col)
  local bufnr = self.bufnr or 0
  lnum = lnum or 1
  col = col or 1
  local max = vim.api.nvim_buf_line_count(bufnr)
  if lnum < 0 then
    lnum = max + lnum
  end
  if lnum > max then lnum = max end
  vim.api.nvim_win_set_buf(0, bufnr)
  vim.api.nvim_win_set_cursor(0, { math.max(lnum, 0), math.max(col, 1) })
end

-- function Context.check()
--   local context = Context:new()
--   print(vim.inspect(context))
-- end

--
-- simple current buffer only
--
---@param parser table{find_var_definition:function}
---@param varname string
---@param up number?
---@return false|table?
---@return string? errmsg
function Context:find_var_definition0(parser, varname, up)
  assert(type(parser) == 'table', 'parser')
  assert(type(parser.find_var_definition) == 'function', 'parser.find_var_definition')
  assert(type(varname) == 'string', 'varname')

  up = up or 16

  local startpos = math.min(self.cursor_row - up, 0)
  local endpos = self.cursor_row

  local expr, s, e, noloc = parser.find_var_definition(
    varname, self.bufnr, startpos, endpos, false)

  if not expr then
    return false, 'not found variable assigment(definition) '
  end

  return { expr = expr, lnum = s, lnum_end = e, nolocal = noloc }
end

---@param self self
---@return string
function Context.__tostring(self)
  if type(self) ~= 'table' then
    return type(self)
  end
  local line = self.current_line
  if line and #(line) > 128 then line = string.sub(line, 1, 128) end
  local lnum, col, lines = self.cursor_row, self.cursor_col, self.lines_count

  local s = fmt("bufnr:%s Editable:%s cursor(lnum:%s col:%s) lines: %s\n",
        v2s(self.bufnr), v2s(self.can_edit), v2s(lnum), v2s(col), v2s(lines or '?')) ..
      -- "current_line:[[" ..
      v2s(line)

  if self.word_element then
    local lexeme_name = Context.LEXEME_NAMES[self.lexeme_type or false] or '?'
    s = s .. "\n" ..
        fmt('word_element(%s:%s): "%s" [%s:%s]',
          v2s(self.word_elm_start), v2s(self.word_elm_end),
          v2s(self.word_element),
          v2s(self.lexeme_type), v2s(lexeme_name))
  end
  if self.word_container then
    s = s .. fmt(' word_container: "%s"', v2s(self.word_container))
  end
  return s
end

---@return table
function Context:getCurrentLineDiagnostic()
  local opts = { lnum = self.cursor_row - 1, }
  return vim.diagnostic.get(self.bufnr, opts)
end

---@return table
function Context:getAllDiagnosticsSameAs(t)
  local opts = {
    namespace = t.namespace,
    severity = t.severity,
    bufnr = t.bufnr,
    source = t.source,
    message = t.message,
    code = t.code,
  }
  return vim.diagnostic.get(self.bufnr, opts)
end

-- TODO find way to fix lua-ls for works withjl  return class.build(Context)
class.build(Context)
return Context

-- 14-08-2024 @author Swarg

local log = require 'alogger'
local class = require 'oop.class'

local Iterator = require 'olua.util.Iterator'

local log_debug = log.debug

--
-- obstruction wrapper for getting lines from a buffer opened in the editor
--
-- "inner" class
class.package 'env.ui.nvim'
---@class env.ui.nvim.BuffIterator: olua.util.Iterator
---@field new fun(self, o:table?, bufnr:number?, lnum:number?): env.ui.nvim.BuffIterator
---@field o table
local C = class.BuffIterator:implements(Iterator) {
  -- constructor
  ---@param bufnr number?
  ---@param lnum number?
  _init = function(self, bufnr, lnum)
    log_debug("Iter._init bufnr:%s lnum:%s", bufnr, lnum)
    bufnr = bufnr or vim.api.nvim_get_current_buf()
    assert(type(bufnr) == 'number', 'expected number bufnr got:' .. type(bufnr))
    self.o = { -- state
      bufnr = bufnr,
      line_count = vim.api.nvim_buf_line_count(bufnr or 0) or 0,
      lnum = lnum or 0,
      current_line = nil,
    }
  end,

  ---@return boolean
  hasNext = function(self)
    return self.o.lnum < self.o.line_count
  end,

  next = function(self)
    local o, ok, t = self.o, nil, nil
    ok, t = pcall(vim.api.nvim_buf_get_lines, o.bufnr, o.lnum, o.lnum + 1, false)
    if not ok then error(t) end -- devmode
    o.lnum = o.lnum + 1
    o.current_line = t[1]
    return o.current_line
  end,
}

class.build(C) -- check interface impl
return C

-- 14-08-2024 @author Swarg
-- Iterator over simple table

local class = require 'oop.class'

class.package 'env.ui.Selection'
---@class env.ui.Selection: oop.Object
---@field new fun(self, o:table?): env.ui.Selection
---@field bufnr number
---@field lnum number
---@field col number
---@field line string
---@field lexems table? parsed by AParser impl
---@field index number?
local C = class.new_class(nil, 'Selection', {})



class.build(C)
return C

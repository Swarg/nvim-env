--  27-05-2024  @author Swarg
-- Goal:
--  - keep translated text(dictionary) in persistent cache in disk
--  - keep track to multiple input types:
--      - words, phrases, sentences, paragraphs, dumps
--  - reload from disk to mem
--  - split cache to two parts: loaded from disk(pmap) and new in-mem only(mmap)
--  - before translate search text in cache(mmap, pmap) and return it if found
--  call cache:save() to save entries into disk

local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'
local U = require 'env.translate.utils'

class.package 'env.translate'
---@class env.translate.Cache : oop.Object
---@field new fun(self, o:table?, dir:string?, persistent:boolean?): env.translate.Cache
---@field dir string
---@field mmap table -- in_mem (not saved to disk)
---@field pmap table -- persists data (loaded to mem from disk)
---@field persistent boolean? (true - save to disk, false - in mem only)
---@field updated boolean (true - has not saved data)
local Cache = class.new_class(nil, 'Cache', {
})

local DEV_MODE = true -- true - throws errors, false - write to logs only

local DEF_CACHE_DIR = fs.join_path(os.getenv('HOME'), '.local/share/env-translator/')

---@diagnostic disable-next-line: unused-local
local E, fmt, v2s, log_debug = {}, string.format, tostring, log.debug
local T_EMPTY = U.IT.EMPTY
local T_WORDS = U.IT.WORDS
local T_PHRASES = U.IT.PHRASES
local T_SENTENCES = U.IT.SENTENCES
local T_PARAGRAPHS = U.IT.PARAGRAPHS
local T_DUMPS = U.IT.DUMPS -- not formated text without punctuations(ig. subtitles)

local ENTRY_SEPS = {
  [T_EMPTY] = nil,
  [T_WORDS] = { '|', "\n" },
  [T_PHRASES] = { '|', "\n" },
  [T_SENTENCES] = { "\n", "\n\n" },
  [T_PARAGRAPHS] = { "\n", "\n\n" },
  [T_DUMPS] = { "\n", "\n\n" },
}

--[[ dictionary structure:
local map = {
  en = {
    ru = {
      words = {
        hellow = 'привет',
      },
      phrases = {
        ['hellow world'] = 'привет мир',
      },
      ... sentences, paragraphs, dumps
    }
  }
}
]]


-- constructor used in Cache:new()
--
---@param dir string?
---@param persistent boolean? default is true (load and save to disk)
function Cache:_init(dir, persistent)
  self.mmap = {} -- in_mem
  self.pmap = {} -- persiste data (loaded from (or loaded to) disk)

  self.persistent = persistent == nil and true or persistent
  self.dir = dir or DEF_CACHE_DIR

  self.updated = false

  if self.persistent then
    log.debug('prepare persistent')
    self:load()
  end
end

---@param fmtmsg string
local function critical(fmtmsg, ...)
  if DEV_MODE then
    error(log.format(fmtmsg, ...))
  end
  log.error(fmtmsg, ...)
end

-- from mmap in-mem
---@param slang string
---@param dlang string
---@param itype string input type
---@return table
function Cache:get_inmem_section(slang, dlang, itype)
  self.mmap = self.mmap or {}
  local t = self.mmap
  t[slang] = t[slang] or {}
  t = t[slang]
  t[dlang] = t[dlang] or {}
  t = t[dlang]
  t[itype] = t[itype] or {}
  return t[itype] -- self.mmap[slang][dlang][itype]
end

-- from pmap (loaded from disk)
---@param slang string
---@param dlang string
---@param itype string input type
---@return table
function Cache:get_persists_section(slang, dlang, itype)
  self.pmap = self.pmap or {}
  local t = self.pmap
  t[slang] = t[slang] or {}
  t = t[slang]
  t[dlang] = t[dlang] or {}
  t = t[dlang]
  t[itype] = t[itype] or {}
  return t[itype] -- self.pmap[slang][dlang][itype]
end

---@param resp env.translate.ParsedResponse
---@param src_lang string
---@param dst_lang string
---@param itype string
function Cache:put(resp, src_lang, dst_lang, itype)
  if type(resp) == 'table' and resp.translated and resp.original then
    local t = self:get_inmem_section(src_lang, dst_lang, itype)

    if not t[resp.original] then
      t[resp.original] = resp.translated

      self.updated = true
      return true
    end
  end

  return false
end

---@param text string
---@param sl string from(source lang)
---@param dl string to(destination lang)
---@param itype string
---@return string?
function Cache:find(text, sl, dl, itype)
  assert(type(itype) == 'string', 'itype')
  -- in-mem map
  local s = (((self.mmap[sl or false] or E)[dl or false] or E)[itype] or E)[text]
  -- persists map
  if not s then
    s = (((self.pmap[sl or false] or E)[dl or false] or E)[itype] or E)[text]
  end
  return s
end

---@param dir string
---@return boolean
local function ensure_dir_exists(dir)
  assert(type(dir) == 'string' and dir ~= '', 'dir')
  if not fs.dir_exists(dir) then
    if not fs.mkdir(dir) then
      log.error('cannot create cache directory "%s"', dir)
      return false
    end
  end
  return true
end

---@param dir string
---@return table?
local function get_files_in(dir)
  if dir and fs.is_directory(dir) then
    local ok, errmsg = fs.list_of_files(dir)
    if not ok or type(ok) ~= 'table' then
      log.error(errmsg)
    else
      return ok -- list of files
    end
  end
  return nil
end

--
-- load all from cache to pmap(persistent data)
-- CACHE_DIR/slang/dlang/itype/file
-- pmap
---@return number (loaded files)
function Cache:load()
  log_debug("load from: '%s'", self.dir)
  self.pmap = {} -- clean

  if not fs.dir_exists(self.dir) then
    log_debug("cache-dir not exists", self.dir)
    return 0
  end

  local c = 0
  for _, slang in ipairs(fs.list_of_files(self.dir) or E) do
    local dir0 = fs.join_path(self.dir, slang)
    local files0 = get_files_in(dir0)
    log.debug('dir0:', dir0, files0)

    if files0 then
      self.pmap[slang] = {}
      local map0 = self.pmap[slang]

      for _, dlang in ipairs(files0) do
        local dir00 = fs.join_path(dir0, dlang)
        local files00 = get_files_in(dir00)
        log.debug('dir00:', dir00, files00)

        if files00 then
          map0[dlang] = {}
          local map00 = map0[dlang]

          for _, itype in ipairs(files00) do
            local dir000 = fs.join_path(dir00, itype)
            local files000 = get_files_in(dir000)
            log.debug('dir000:', dir000, files000)

            if files000 then
              map00[itype] = {}
              local map000 = map00[itype]

              for _, fn in ipairs(files000) do
                local path = fs.join_path(dir000, fn)
                log.debug('path:', path)
                if Cache.load_file_to(map000, path) then
                  c = c + 1
                end
              end
            end
          end
        end
      end
    end
  end

  return c
end

--


---@param t table
---@param itype string
local function build_content_of(itype, t)
  local seps = ENTRY_SEPS[itype]
  assert(type(seps) == 'table', 'expected known seps')
  local sep, nl = (seps or E)[1], (seps or E)[2]
  local s = ''
  for orig, transl in pairs(t) do
    s = s .. orig .. sep .. transl .. nl
  end
  return s
end

---@param map000 table {orig = transl}
---@param dir string CACHE_DIR/slang/dlang/itype/
---@param itype string
function Cache.save_entries_to(map000, dir, itype)
  if itype and next(map000) and ensure_dir_exists(dir) then
    local name = '1'
    local fn = fs.join_path(dir, name)
    log_debug('saving to', fn)
    local content = build_content_of(itype, map000)
    local f = io.open(fn, 'ab') -- append to already existed file
    if f then
      f:write(content)
      f:close()
    end

    return true
  end
  return false
end

-- map000 from mmap
function Cache:move_to_pmap(map000, sl, dl, itype)
  local t = self:get_persists_section(sl, dl, itype)
  for orig, transl in pairs(map000) do
    if t[orig] ~= nil then
      critical('entry: %s/%s/%s "%s" already in pmap', sl, dl, itype, orig)
    end
    t[orig] = transl
  end
  return true
end

-- save all in-mem state to disk
function Cache:save()
  local c = 0
  for src_lang, map0 in pairs(self.mmap) do
    for dst_lang, map00 in pairs(map0) do
      local itc, clear_queue = 0, {}

      for itype, map000 in pairs(map00) do
        local dir = fs.join_path(self.dir, src_lang, dst_lang, itype)
        if Cache.save_entries_to(map000, dir, itype) then
          if self:move_to_pmap(map000, src_lang, dst_lang, itype) then
            clear_queue[#clear_queue + 1] = itype
          end
          c = c + 1
        end
        itc = itc + 1
      end

      -- clear saved(flush from mem to disk i.g. mmap > pmap
      for _, itype in ipairs(clear_queue) do
        map00[itype] = {}
      end

      log_debug('%s/%s cleaned:%s/%s', src_lang, dst_lang, #clear_queue, itc)
    end
  end
  return c
end

local function parse_entry(line)
  local original, translated = string.match(line, '^(.+)|(.+)$')
  return original, translated
end

-- load entries from given path into map(table)
---@param map table
---@param path string
function Cache.load_file_to(map, path)
  local f = io.open(path, 'rb')
  if f then
    while true do
      local line = f:read('*l')
      if not line then break end
      local original, translated = parse_entry(line)
      if original and translated then
        map[original] = translated
      end
    end
    f:close()
    return true
  else
    log.error('cannot open file: "%s"', path)
  end
  return false
end

--
---@param map table
---@return table
local function dict_status(map)
  local t = {}
  if type(map) == 'table' then
    for slang, map0 in pairs(map) do
      for dlang, map00 in pairs(map0) do
        for itype, map000 in pairs(map00) do
          local k = fmt("%s-%s-%s", slang, dlang, itype)
          local c, szo, szt = 0, 0, 0
          for original, translated in pairs(map000) do
            c = c + 1
            szo = szo + #original
            szt = szt + #translated
          end
          t[k] = { cnt = c, szo = szo, szt = szt }
        end
      end
    end
  end
  return t
end

---@param tag string
---@param state table from status_of(
---@return string
local function fmt_dict_state(tag, state)
  local s = tag
  local c, tszo, tszt = 0, 0, 0
  if not next(state) then
    return s .. " empty"
  else
    s = s .. "\n"
    for k, t in pairs(state) do
      s = s .. fmt("  %s: %s %s %s\n", v2s(k), v2s(t.cnt), v2s(t.szo), v2s(t.szt))
      c = c + 1
      tszo = tszo + t.szo
      tszt = tszt + t.szt
    end
    return s .. fmt("  total: cnt:%s szo:%s szt:%s", c, tszo, tszt)
  end
end

--
---@param verbose boolean?
---@return string
function Cache:status(verbose)
  verbose = verbose

  local mst = dict_status(self.mmap)
  local pst = dict_status(self.pmap)
  local fds = fmt_dict_state
  return
      fds('In-Memory    :', mst) .. "\n" ..
      fds('In-Disk      :', pst) .. "\n" ..
      fmt("Cache-Dir    : %s\n", v2s(self.dir)) ..
      fmt("Def-Cache-Dir: %s\n", v2s(DEF_CACHE_DIR)) ..
      fmt("persistent   : %s\n", v2s(self.persistent)) ..
      fmt("updated      : %s\n", v2s(self.updated)) ..
      fmt("DevMode      : %s\n", v2s(DEV_MODE))
end

-- ctl

---@return string
function Cache:clear_in_mem()
  local mst = dict_status(self.mmap)
  self.mmap = {}
  return fmt_dict_state('In-Memory:', mst)
end

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule
--                       for runtime code reloading

function Cache:dump_state()
  _G.__evn_translator_cache = {
    dir = self.dir,
    persistent = self.persistent,
    updated = self.updated,
    mmap = self.mmap,
    pmap = self.pmap,
  }
end

function Cache:restore_state()
  local t = _G.__evn_translator_cache
  _G.__evn_translator_cache = nil

  self.dir = t.dir
  self.persistent = t.persistent
  self.updated = t.updated
  self.mmap = t.mmap
  self.pmap = t.pmap
end

--

if _G.TEST then
  Cache.build_content_of = build_content_of
end

class.build(Cache)
return Cache

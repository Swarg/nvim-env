-- 27-05-2024 @author Swarg
--

local log = require 'alogger'
local class = require 'oop.class'

local U = require 'env.translate.utils'
local Cache = require 'env.translate.Cache'
local utm = require "env.util.http.transport_manager"
-- api
local googleapis = require "env.translate.googleapis"


class.package 'env.translate'
---@class env.translate.Translator : oop.Object
---@field new fun(self, o:table?, api:env.translate.API?, transport:env.util.http.Transport?): env.translate.Translator
---@field api env.translate.API
---@field transport env.util.http.Transport
---@field callback function
---@field cache env.translate.Cache
local C = class.new_class(nil, 'Translator', {
  api = nil,       -- mk-request + parse_response
  transport = nil, -- send + decode
  callback = nil,  -- response-consumer
  cache = nil,
})


local translator = nil

-- TODO many translators api
local def_translate_api = googleapis


-- constructor
--
---@param api env.translate.API:
---@param transport env.util.http.Transport
---@param callback function?
function C:_init(api, transport, callback)
  log.debug("Translator:_init %s %s", api ~= nil, transport ~= nil)
  self.api = api or self.api or def_translate_api
  self.transport = transport or self.transport or utm.choose_transport()
  self.callback = callback or self.callback

  assert(type(self.api) == 'table', 'api')
  assert(type(self.transport) == 'table', 'transport got:' .. type(self.transport))
  assert(not self.callback or type(self.callback) == 'function', 'callback')
end

-- recognize text language
-- this is necessary so that you can check the presence in the cache
function C.recognize_langs(text, src_lang, dst_lang)
  if not src_lang or src_lang == 'auto' then
    src_lang = U.detect_lang(text)
  end
  dst_lang = dst_lang or U.opposite_lang(src_lang)
  log.debug('from lang:%s to_lang:%s', src_lang, dst_lang)
  return src_lang, dst_lang
end

---@param text string
---@param src_lang string from
---@param dst_lang string to
---@param callback function? -- transtaltion render callback
function C:translate(text, src_lang, dst_lang, callback)
  log.debug('translate')
  text, src_lang, dst_lang = self.api.validated(text, src_lang, dst_lang)
  local itype = U.get_input_type(text)

  local found = self.cache and self.cache:find(text, src_lang, dst_lang, itype)
  if found then
    return self:return_from_cache(text, found, callback)
  end

  local req = assert(self.api.mk_request(text, src_lang, dst_lang), 'request')

  log.debug('request to send: %s', req)

  local wrapped_callback = self:mk_middleware(callback, src_lang, dst_lang, itype)
  local opts = self.api.mk_opts(wrapped_callback)

  return self.transport.send(req, opts)
end

--
---@return env.translate.Translator
---@param opts table?
function C.get_instance(opts)
  if translator == nil then
    opts = opts or {}
    translator = C:new(nil,
      opts.api or def_translate_api,
      opts.transport or utm.choose_transport()
    )
    translator.cache = opts.cache or Cache:new(nil,
      opts.cache_dir,
      opts.cache_persistent
    )
  end
  return translator
end

-- goal: save translated into cache
---@param callback function?
---@param src_lang string
---@param dst_lang string
---@param itype string -- input type: [words|phrases|sentences|...]
---@return function
function C:mk_middleware(callback, src_lang, dst_lang, itype)
  return function(response, opts, uri)
    if self.cache then
      self.cache:put(response, src_lang, dst_lang, itype)
    end

    if type(callback) == 'function' then
      return callback(response, opts, uri)
    else
      return response
    end
  end
end

---@param text string
---@param found string
---@param callback function?
---@return any|env.translate.ParsedResponse
function C:return_from_cache(text, found, callback)
  if found then
    local response = {
      original = text,
      translated = found
    }
    if type(callback) == 'function' then
      return callback(response)
    end
    return response
  end
end

-- save in-mem cache to disk
function C:flush_cache()
  return self.cache:save()
end

--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule
--                       for runtime code reloading

function C.dump_state()
  local t = C.get_instance()
  t.cache:dump_state()
end

function C.restore_state()
  C.get_instance().cache:restore_state()
end

class.build(C)
return C

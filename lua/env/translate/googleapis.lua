-- 27-05-2024 @author Swarg
--
local u8 = require "env.util.utf8"
local url = require "env.util.url"
local middleware = require "env.util.http.middleware"


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local API_ENDPOINT = 'translate.googleapis.com'
local API_PATH = '/translate_a/single'
local API_QUERY = "?client=gtx&sl=%s&tl=%s&dt=t&q=" -- src_lang, dst_lang

---@class env.translate.API
local M = {}

---@param text string?
---@param src_lang string?
---@param dst_lang string?
---@return string, string, string -- text, src_lang, dst_lang
function M.validated(text, src_lang, dst_lang)
  assert(type(text) == 'string' and text ~= '', 'expected string text')
  text = text
  src_lang = src_lang or 'auto'
  dst_lang = dst_lang or 'en'

  return text, src_lang, dst_lang
end

-- create request to api to translate given text
-- for socket.html
--
---@param text string
---@param src_lang string?
---@param dst_lang string?
---@return env.util.http.Request
function M.mk_request(text, src_lang, dst_lang)
  text, src_lang, dst_lang = M.validated(text, src_lang, dst_lang)
  local encoded = url.url_encode(text)

  local uri = API_PATH .. fmt(API_QUERY, src_lang, dst_lang) .. encoded

  return {
    scheme = 'http', -- luasock supports https(TLS) via LuaSec package
    method = 'GET',
    host = API_ENDPOINT,
    uri = uri
  }
end

function M.mk_opts(render_callback)
  return {
    is_json = true,

    -- decoded json ->  env.translate.ParsedResponse
    ---@diagnostic disable-next-line: unused-local
    callback = function(raw_response, opts, uri)
      -- opts.is_json
      --
      local response = middleware.decode(raw_response, opts, uri) -- json -> luatable
      if type(response) ~= 'table' then
        error('error on middleware to parse json for' .. v2s(uri))
      end
      ---@cast response table

      ---@diagnostic disable-next-line: param-type-mismatch
      local parsed_resp = M.parse_response(response)
      if render_callback then
        return render_callback(parsed_resp)
      end
      return parsed_resp
    end,
  }
end

---@param response table
---@return env.translate.ParsedResponse?
function M.parse_response(response)
  assert(type(response) == 'table', 'expected already decoded json response')
  local t = response
  local messages = t[1]

  -- join all sentences into one whole
  local translated = ''
  local original = ''
  for _, message in ipairs(messages) do
    -- if #translated > 1 then translated = translated .. "\n" original = original .. "\n" end
    translated = translated .. tostring(message[1])
    original = original .. tostring(message[2])
  end

  -- if requests[requested_url] and #requests[requested_url] == 0 then
  --   requests[requested_url] = nil
  -- end
  return {
    translated = u8.mk_simple_punctuation(translated):gsub("\n", ' '),
    original = original
  }
end

return M

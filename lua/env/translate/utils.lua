-- 27-05-2024 @author Swarg
--

---@class env.translate.ParsedResponse -- TranslatedAnswer
---@field original string
---@field translated string
---class parent TranslatedAnswer|nil

---@class env.translate.API
---@field validated fun(text:string?, src_lang:string?, dst_lang:string?): string, string, string
---@field mk_request fun(text:string, src_lang:string?, dst_lang:string?): env.util.http.Request
---@field mk_opts fun(callback:function?): table
---@field parse_response fun(response:string): env.translate.ParsedResponse

---@class env.translate.Transport
---@field send fun(request: env.util.http.Request)
---@field decode fun(raw_response:string): string -- string to lua-table(decode json)

local u8 = require("env.util.utf8")

local M = {}
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local MAX_SENTENCE_LENGTH = 120

-- input type
M.IT = {
  EMPTY = 'empty',
  WORDS = 'words',
  PHRASES = 'phrases',
  SENTENCES = 'sentences',
  PARAGRAPHS = 'paragraphs',
  DUMPS = 'dumps',
}

M.END_OF_SENTENCE = { ['.'] = true, ['!'] = true, ['?'] = true }
M.IN_SENTENCE_SEP = { [','] = true, [':'] = true, [';'] = true, ['-'] = true }


--
-- smart way by checking utf8 codepoints(bytes)
-- fix isseue for lua5.1 string.match(s, RU_CHARS) -- this doesn't work properly
--
---@param text string
-- function M.detect_lang_utf8(text)
---@param deflang string? (auto)
function M.detect_lang(text, deflang)
  deflang = deflang or 'auto' -- en?

  if text and text ~= '' then
    local i, en, ru = 0, 0, 0

    while i <= #text do
      i = i + 1
      local b1, b2 = string.byte(text, i, i + 1)

      if u8.is_ascii_letter(b1) then
        en = en + 1
        if u8.is_ascii_letter(b2) then
          i  = i + 1
          en = en + 1
        end
      elseif u8.utf8_is_ru_bytes(b1, b2) then
        i = i + 1
        ru = ru + 1
        if ru > 1 then
          return 'ru'
        end
      end
    end

    if en > 0 then
      return 'en'
    end
  end

  return deflang
end

--
-- it works, but it's not always correct
-- recognize the string as Russian language with any(?) UTF-8 characters
--
function M.detect_lang_old(text)
  if text then
    local m = text:match("[А-Яа-яЁё]+")
    if m and m ~= '' then
      return 'ru'
    else
      return 'en'
    end
  end
  return 'auto'
end

function M.opposite_lang(lang)
  if lang == 'en' then
    return 'ru'
  elseif lang == 'ru' then
    return 'en'
  end
  return lang
end

function M.check_can_transtale(text, src_lang, dst_lang)
  if not text or text == '' then
    print('No text to translate')
    return false
  end
  if not src_lang then
    print('Source Languge not specified')
    return false
  end
  if not dst_lang then
    print('Target Languge not specified')
    return false
  end
  if src_lang == dst_lang then
    print(fmt('Specified Same locale for source and destionation'
      .. ' language "%s" "%s"', src_lang, dst_lang))
    return false
  end

  return true
end

----

function M.is_lowercase(s)
  return s ~= nil and string.lower(s) == s
end

function M.is_uppercase(s)
  return s ~= nil and string.upper(s) == s
end

function M.indexof_sentence_sep(text, offset)
  offset = offset or 1
  local i = string.find(text, '[%.!?]', offset, false)
  return i ~= nil and i or -1
end

-- not a words
local DIGITS_AND_PUNCTUATIONS_PTRN =
'^[%d%s%.%-%+%/\\|:=,;!%?@%#%$%%%^%&%*%(%)%{%}%[%]%<%>~`\'"]+$'

--
-- word, phrases, sentences, paragraphs, dumps
--
-- dumps - is a set of sentences without punctuation marks.
--
---@param text string original to translate
---@return string
function M.get_input_type(text)
  local typ, text_len = M.IT.WORDS, #(text or '')
  if text_len == 0 or string.match(text, DIGITS_AND_PUNCTUATIONS_PTRN) then
    return M.IT.EMPTY
  end

  if text_len > 3 and text:find('%s') then
    local idx = M.indexof_sentence_sep(text)
    if idx > 0 and idx < text_len then
      typ = M.IT.PARAGRAPHS -- multiple
      -- no punctuation marks and too long
    elseif idx < 0 and text_len > MAX_SENTENCE_LENGTH then
      typ = M.IT.DUMPS
    else
      -- local fc = text:sub(1, 1) local upper = U.is_uppercase(fc)
      local has_sentence_end = idx == text_len -- U.end_of_sentence[lc]

      if --[[upper and]] has_sentence_end then
        typ = M.IT.SENTENCES
      else
        typ = M.IT.PHRASES
      end
    end
  end

  return typ
end

return M

--
-- Goal: Check diffs from builtin lua-libs and installed in system
-- (to explore how NVim and Lua works under the hood)
--
local M = {}

local api = vim.api
local R = require('env.require_util')
local ui = require('env.ui')
local su = require('env.sutil')
local cu = require("env.util.commands")
local du = require 'env.util.diagnostics'
local log = require('alogger')
local c4lv = require("env.util.cmd4lua_vim")

-- for compare version with installed in system scope
local builtin_libs_kv = {
  luv = 'vim.loop',
  inspect = 'vim.inspect',
  luassert = "assert",
  cjson = "vim.json"
}

local function select_one_lib()
  local items = vim.tbl_keys(builtin_libs_kv)
  local label_fn = function(item)
    return string.format('%-12s  -- (%s)', item, builtin_libs_kv[item])
  end
  local libname = ui.pick_one(items, "Select Lib", label_fn)
  return libname
end

-- to research and check how its works
-- show same version for both builtin and Path inside the instance of nvim
function M.luv_version()
  local i = vim.inspect
  local uv0 = vim.loop
  print("NVim builtin libuv version: "
    .. i(uv0.version_string()) .. ' (' .. uv0.version() .. ')')

  local ok, uv = pcall(require, 'luv')
  if ok then
    print("PATH scope libuv version:   "
      .. i(uv.version_string()) .. ' (' .. i(uv.version()) .. ')')
  end
end

-- for manual check is the R.require works as expected ++
function M.R_luv_version()
  -- Then it run in NVim instance vim not null and this code pick module
  -- value from vim.loop but not require("luv").
  -- NOTE: require("luv") inside NVim instance pick lib from builtin not
  -- from system scope
  local m = R.require("luv", vim, "loop")
  local v = "( 'luv', vim.loop) "
  local i = vim.inspect
  if m and m.version_string and m.version then
    v = v .. " ver: " .. i(m.version_string()) .. ' ' .. i(m.version())
  else
    v = v .. " nil"
  end
  print("R.require " .. v)
end

-- Try to pull the version and description of the library
-- Check _VERSION _DESCRIPTION and func: version() and version_string()
-- output [libname] version <found-version> [\nDescrioption: <Descriptions>]
---@param tbl_lib table module
---@param libname string?
function M.get_lib_info(tbl_lib, libname)
  if tbl_lib then
    local i = vim.inspect
    local res = ''
    if libname then
      res = '[' .. tostring(libname) .. '] version: '
    end
    if tbl_lib._VERSION then
      res = res .. tostring(tbl_lib._VERSION)
    end
    if type(tbl_lib.version_string) == "function" then
      res = res .. ' ' .. tbl_lib.version_string()
    end
    if type(tbl_lib.version) == "function" then
      res = res .. ' ' .. i(tbl_lib.version())
    end
    if tbl_lib._DESCRIPTION then
      res = res .. '\n Description:' .. tbl_lib._DESCRIPTION
    end
    return res
  end
end

-- evaluate lua-code(text) to lua-value. Example text "vim.loop" --> into tbl
---@param tbl_name string
---@return any (table)
local function get_table_by_name(tbl_name)
  local func = loadstring('return ' .. tbl_name)
  if func then
    return func()
  end
end

-- Show both: built-in and in-paths(require) lib-infos
-- to compare versions
-- by module(lib) name get version and description
---@param libname string
---@param builtin_name string?
function M.get_lib_version(libname, builtin_name)
  local ok, module = pcall(require, libname)
  local i, res = vim.inspect, ''
  res = string.format("require(%s)         -- ", i(libname))
  if ok then
    res = res .. M.get_lib_info(module, libname)
  else
    res = res .. string.format("Module Not Found")
    -- print(module) show all searching paths
  end
  print(res)
  if builtin_name then
    res = string.format("NVim built-in %s  -- ", i(builtin_name))
    local blib = get_table_by_name(builtin_name)
    if type(blib) == 'table' then
      res = res .. M.get_lib_info(blib, builtin_name)
    else
      res = res .. type(blib)
    end
    print(res)
  end
end

-- Select one of known lib from list and show its info
function M.lib_version()
  local libname = select_one_lib()
  if libname then
    print(' ')
    print('Selected ' .. libname)
    -- local funcname = name .. '_version';  M[funcname]();
    M.get_lib_version(libname, builtin_libs_kv[libname])
  end
end

-- inspect lib-module as table to see functions and inner structure
function M.lib_show_as_table()
  local libname = select_one_lib()
  if libname then
    print(' ')
    print('Selected ' .. libname)
    local ok, m = pcall(require, libname)
    if ok then
      print(libname .. ': ' .. vim.inspect(m))
    else
      print(string.format("require(%s) -- Module Not Found", libname))
      local builtin_name = builtin_libs_kv[libname]
      if builtin_name then
        print(string.format("NVim Builtin %s:", builtin_name))
        local blib = get_table_by_name(builtin_name)
        print(vim.inspect(blib))
      end
    end
  end
end

-- Show all NVim-Paths from which modules are loaded
function M.runtimepath()
  local paths = vim.api.nvim_list_runtime_paths()
  if type(paths) == 'table' then
    table.sort(paths)
    for _, v in pairs(paths) do
      print(v)
    end
  else
    print(vim.inspect(paths))
  end
  -- vim.api.nvim_command("echo $VIMRUNTIME") -- /usr/share/nvim/runtime/
  -- vim.opt.runtimepath:append(',~/.config/nvim/lua')
  -- vim.o.path
end

function M.get_os_time()
  local t = os.time()
  local c = os.clock()
  local c5 = os.clock() ^ 5
  print("os.time (" .. type(t) .. ') ' .. vim.inspect(t) ..
    " os.clock: (" .. type(c) .. ') ' .. vim.inspect(c) ..
    " os.clock ^ 5: (" .. type(c5) .. ') ' .. vim.inspect(c5))
  -- number millis
end

function M.random_string()
  local t = su.rand_str(16)
  print(t)
  -- number millis
end

-- for manual check how echohl works
function M.call_echohl()
  local prompt = 'Input message for echo: '
  local msg = vim.fn.input(prompt)
  local i = su.last_indexof(msg, " ")
  local hl = su.substr(msg, i + 1)
  print(vim.inspect(msg), ' HLGroup: ', vim.inspect(hl))
  ui.echohl(msg, hl)
end

function M.call_save_log_message()
  local prompt = 'Input message for log: '
  local msg = vim.fn.input(prompt)
  local i = su.last_indexof(msg, " ")
  local lword = '2'
  if i then
    lword = su.substr(msg, i + 1) or "2"
  end
  print(vim.inspect(msg), ' Level:', vim.inspect(lword))
  ---@diagnostic disable-next-line
  local ok, level = pcall(tonumber(lword))
  if not ok then level = 2 end -- INFO
  if not log.is_initialized() then
    print("[ERROR] Plugin not initialized! use env.setup({})")
    return
  end
  log.log(level, msg)
end

-- Show details about all currently opened buffers
function M.show_buffers_info()
  local loaded_cnt, skipped = 0, 0
  local list = vim.api.nvim_list_bufs()
  print("BufNr, [+] LOADED | [-] UNLOADED, fn, btype, ftype, modifiable: ")
  for _, bn in ipairs(list) do
    local loaded = api.nvim_buf_is_loaded(bn)
    local mod = api.nvim_buf_get_option(bn, 'modifiable')
    local name = api.nvim_buf_get_name(bn)
    local state
    if loaded == true then state = "[+]" else state = "[-]" end
    print(bn .. ' ' .. state .. ' ' .. vim.inspect(name) ..
      ' bt:"' .. vim.bo[bn].buftype .. '" ' ..
      ' ft:"' .. vim.bo[bn].filetype .. '" m:' .. vim.inspect(mod))
    if loaded then
      loaded_cnt = loaded_cnt + 1
    else
      skipped = skipped + 1
    end
  end
  print(string.format("Passed: %s Skipped: %s", loaded_cnt, skipped))
end

-- Registry experimental commands to explore nvim buffer structure
function M.reg_cmds_EnvBufInfos()
  cu.reg_cmd("EnvBufInfo", M.buffer_info, { nargs = '*' })

  -- to check how its work -- you can attach to buffer any you own variable
  cu.reg_cmd("EnvBufVarSet", function(opts)
    if opts and #opts.fargs >= 2 then
      local bufnr = vim.api.nvim_get_current_buf()
      local varname = opts.fargs[1] or "__evn_temp_var_name"
      local value = opts.fargs[2]
      api.nvim_buf_set_var(bufnr, varname, value)
      local i = vim.inspect
      ui.echohl(bufnr .. ' ' .. i(varname) .. ' == ' .. i(value), '')
    end
  end, { nargs = '*' })

  cu.reg_cmd("EnvBufVarGet", function(opts)
    if opts and #opts.fargs >= 1 then
      local varname = opts.fargs[1] or "__env_temp_var_name"
      -- local bufnr = 0 -- current
      local bufnr = vim.api.nvim_get_current_buf()
      if #opts.fargs >= 2 then
        ---@diagnostic disable-next-line
        bufnr = tonumber(opts.fargs[2])
      end
      local i = vim.inspect
      local ok, value = pcall(api.nvim_buf_get_var, bufnr, varname)
      if ok then
        ui.echohl(bufnr .. ' ' .. i(varname) .. ' == ' .. i(value))
      else
        ui.echohl('Not Found var ' .. i(varname) .. ' in buff:' .. i(bufnr), '')
      end
    end
  end, { nargs = '*' })

  ---@diagnostic disable-next-line: unused-local
  cu.reg_cmd("EnvBufDiagnostics", M.cmd_buf_diagnostics, { nargs = '*' })
end

--: EnvBufDiagnostics
function M.cmd_buf_diagnostics(opts)
  log.debug("cmd_buf_diagnostics")
  local dopts, bufnr, desc = {}, nil, nil

  local w = c4lv.newCmd4Lua(opts)
  w:root(':EnvBufDiagnostics')
  w:about('To Research the internal structure of the nvim diagnostincs')

  desc = 'a buffer number. 0 is default for the current buffer'
  bufnr = w:def(0):optn('--bufnr', '-b', desc)

  w:default_cmd('info')

  if w:desc("remove diagnostic from current line under the cursor")
      :is_cmd('remove-current', 'rc') then
    local dlist = vim.diagnostic.get(bufnr, dopts)
    local cursor_pos = api.nvim_win_get_cursor(0)
    if dlist and cursor_pos then
      -- pick real bufnr from firs record
      if bufnr == 0 then
        bufnr = (dlist[1] or {}).bufnr or 0
      end
      local ln = cursor_pos[1]
      desc = 'Exactly by the cursor position. By default all for current line'
      local col = w:has_opt('--check-column', '-c', desc) and cursor_pos[2] or nil

      if w:is_input_valid() then
        -- work:
        ---@cast bufnr number
        local to_remove = du.filter_by_position(dlist, bufnr, ln, col)
        du.remove_all(bufnr, dlist, to_remove)
      end
    end
  elseif w:desc("remove all diagnostics from current buffer")
      :is_cmd('remove-all', 'ra') then
    local dlist = vim.diagnostic.get(bufnr or 0, dopts)
    if not bufnr or bufnr == 0 then bufnr = (dlist[1] or {}).bufnr or 0 end
    du.remove_all(bufnr, dlist, dlist)
    --
  elseif w:desc("inspect [all] diagnostics in the current buffer")
      :is_cmd('info', 'i') then
    -- all diagnostics for given bufnr
    -- dopts = { severity = { min = vim.diagnostic.severity.HINT } }
    local dlist = vim.diagnostic.get(bufnr, dopts)
    desc = 'Info Only about the current line (By Default all for the buffer)'
    if w:has_opt('--current-line', '-cl', desc) then
      local cursor_pos = api.nvim_win_get_cursor(0)
      local ln = cursor_pos[1]
      local col = w:has_opt('--check-column', '-c') and cursor_pos[2] or nil
      -- only for current line
      if bufnr == 0 then
        bufnr = (dlist[1] or {}).bufnr or 0
      end
      ---@diagnostic disable-next-line: param-type-mismatch
      dlist = du.filter_by_position(dlist, bufnr, ln, col)
    end

    if w:is_input_valid() then
      -- work:
      if dlist then
        local out_bufnr = ui.buf_new_named('Raw Diagnostics')
        if not out_bufnr or out_bufnr == -1 then
          print(vim.inspect(dlist))
          error('Cannot create new buffer to output')
        end
        ui.buf_append_lines(out_bufnr, vim.inspect(dlist))
      else
        print('Not Found for buf:', bufnr)
      end
    end
  end

  w:show_help_or_errors()
end

-- EnvBufInfo
---@param opts table
function M.buffer_info(opts)
  local w = c4lv.newCmd4Lua(opts)
      :root(':EnvBufInfo')
      :about('Show internal nvim buf infos')

  local bufnr = w:desc('bufnr'):def(0):argn(0) -- buff number
  if bufnr == 0 then
    bufnr = api.nvim_get_current_buf()
  end

  if not w:is_input_valid() then return end ---@cast bufnr number

  local i = vim.inspect
  local curr_win = api.nvim_get_current_win()
  print("bufnr:", bufnr,
    '  cursor:', i(api.nvim_win_get_cursor(curr_win)),
    '  current_win:', curr_win,
    '  tabpage_list_wins:', i(api.nvim_tabpage_list_wins(0)),
    '  win_number:', api.nvim_win_get_number(0),
    '  win_tabpage:', api.nvim_win_get_tabpage(0),
    '  mode:', i(api.nvim_get_mode()),
    '  wim_config:', i(api.nvim_win_get_config(curr_win))
  )
  print("buf_options:")
  local opt_names = { 'filetype', 'modifiable', 'buftype', 'swapfile',
    'syntax', 'readonly' }
  for _, optname in ipairs(opt_names) do
    print(optname, i(api.nvim_buf_get_option(bufnr, optname)))
  end

  -- set cursor position
  -- nvim_win_set_cursor({window}, {pos})

  -- print(i(vim.api.nvim_get_mode()))
  -- api.nvim_buf_set_var(bufnr, "my_var_name", 888)
  --vim.api.nvim_buf_get_all_options_info(bufnr)
  print('bufname:' .. i(api.nvim_buf_get_name(bufnr)))
  -- print('text:' .. i(api.nvim_buf_get_text(bufnr, 0, 0, -1, -1, {})))
  local buf = vim.bo[bufnr] -- metatable {}
  if buf then
    print(' vim.bo[n]:', i(buf))
    -- vim.lsp.diagnostic  --get_namespace(ctx.client_id)
  else
    print("Not Found in vim.bo for bufnr:", bufnr)
  end
end

function M.edit_table_in_buf()
  local ObjEditor = require 'env.ui.ObjEditor'
  -- local Editor = require 'env.ui.Editor'
  -- local editor = Editor:new()
  local t = {
    a = "abc",
    keyb = "",
    keyc = 42,
    key4 = true,
    b = false,
    pkg = 'pkg',
    clazz = 'pkg.Main',
  }
  local key_order = { 'pkg', 'clazz', 'a', 'b', 'keyb' }

  local on_write = function(current, original)
    -- print('callback on write', require "inspect" (t))
    print("[DEBUG] current:", require "inspect" (current))
    print("[DEBUG] original:", require "inspect" (original))
  end
  ---@diagnostic disable-next-line: unused-local
  local on_cmd = function(current, cmdline, original) -- :e!
    print('callback on_cmd', cmdline)
    local cn = string.match(current.clazz, "([^%.]+)$")
    current.clazz = tostring(current.pkg) .. '.' .. cn
  end
  vim.cmd [[:messages clear]]
  local title = 'EditObjectAsCode'
  -- ObjEditor.create(t, title, on_write, on_cmd) -- simple
  -- ask confirmation to continue or edit
  ObjEditor.create(t, title, on_write, on_cmd, key_order)
end

-- Select and call one of the research action
---@diagnostic disable-next-line: unused-local
function M.pick_one_action(opts)
  local i = nil
  if opts.fargs and opts.fargs[1] then
    i = tonumber(opts.fargs[1])
  end
  local items = { -- TODO how fill table by module functions names ?
    "show_buffers_info",
    "lib_version",
    "lib_show_as_table", -- lib_inspect
    "runtimepath",
    "R_luv_version",
    "get_os_time",
    "random_string",
    "call_echohl",
    "call_save_log_message",
    "edit_table_in_buf",
  }
  if i then
    M[items[i]]();
    return
  end
  local prompt = "Select Action:"
  local label_fn = function(item) return item end
  local res = ui.pick_one(items, prompt, label_fn)
  if res then
    print(' ')
    M[res]();
  end
end

return M

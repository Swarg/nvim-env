-- Research tool to explore how lua and vim works inside
-- 16-05-2023 @author Swarg
local M = {}

local su = require('env.sutil')
local cu = require("env.util.commands")

-- lua print(vim.inspect(package.loaded))
-- lua print(vim.inspect(collectgarbage("count")))
-- lua print(vim.inspect(gcinfo()))

---@param name string?
function M.reload_package(name)
  if not name then
    name = vim.fn.input('PackageName: ')
    print('\n')
  end
  if name then
    package.loaded[name] = nil
    return require(name)
  end
end

-- http://www.lua.org/manual/5.1/manual.html#pdf-package.loaded
-- A table used by require to control which modules are already loaded.
function M.loaded_packages(pattern)
  local i = 0
  local grep = type(pattern) == 'string'
  if grep then
    print('Grep: ' .. pattern)
  end
  for name in pairs(package.loaded) do
    local skip = false
    if grep then
      if not name:match(pattern) then
        skip = true
        -- goto continue
      end
    end
    if not skip then
      print(name)
      i = i + 1
    end
    -- ::continue::
  end
  print('[Total]: ' .. tostring(i))
end

---@param name string?
function M.package_info(name)
  if not name then
    name = vim.fn.input('PackageName: ')
    print('\n')
  end
  if name then
    local t = package.loaded[name]
    print('Package:', name)
    print(vim.inspect(t))
  end
end

---@param pattern string
---@return table, number  list of matched packages, total packages count
function M.grep_packages(pattern)
  local list = {}
  local cnt = 0
  if pattern and package.loaded then
    for name in pairs(package.loaded) do
      if name:match(pattern) then
        table.insert(list, name)
      end
      cnt = cnt + 1
    end
  end
  return list, cnt
end

function M.loaded_packages_count()
  local cnt = 0
  if package.loaded then
    for _ in pairs(package.loaded) do
      cnt = cnt + 1
    end
  end
  return cnt
end

-- For Research and Explore loaded Packages (via require)
-- http://www.lua.org/manual/5.1/manual.html#pdf-package.loaded
-- package.loaded - A table used by require to control which modules are
--   already loaded.
-- package.path
-- package.cpath
-- package.preload
-- package.seeall (module)
-- package.loadlib (libname, funcname) -
--   Dynamically links the host program with the C library libname.
function M.reg_cmd_EnvPackages()
  cu.reg_cmd("EnvPackages", function(opts)
    M.cmd_handler_pakages(cu.fargs_safely(opts))
  end, { nargs = '*', complete = M.cmd_complete_env_packages })
end

-- EnvPackages
local cmd_packages_subcmds = { 'loaded', 'info', 'reload', 'grep' }
local cmd_pakages_usage = [[
Usage: EnvPackages <cmd> [package-name]
  loaded           -  Show names of all loaded packages
  info   <name>    -  Table of package by given package name
  reload <name>    -  Reload package for given package name
  grep   <pattern> -  Show only packages matched the given pattern
]]

function M.cmd_handler_pakages(args)
  local cmd, arg;
  if args then cmd = args[1] end
  if args then arg = args[2] end

  if cmd == "help" or cmd == "h" then
    print(cmd_pakages_usage)
    return
  end
  if cmd == "info" or cmd == "i" then -- put to memorized
    M.package_info(arg)
  elseif cmd == "loaded" or cmd == "ls" then
    M.loaded_packages(arg)
  elseif cmd == "reload" or cmd == "r" then
    M.reload_package(arg)
  elseif cmd == "grep" or cmd == "g" then
    M.loaded_packages(arg)
  else
    print("UNKNOWN Command: " .. vim.insert(cmd))
    return
  end
end

function M.cmd_complete_env_packages(arg, cmdline, cursor_pos)
  local words = vim.split(cmdline, ' ', { trimempty = true })
  if not words then
    return {}
  end
  local n = #words;
  -- case: 'cmd arg |'
  if su.substr(cmdline, cursor_pos, cursor_pos) == " " then
    n = n + 1
  end
  local filter = cu.mk_completion_filter(arg)
  if n == 2 then -- selet sub-command
    return cu.completion_sort(vim.tbl_filter(filter, cmd_packages_subcmds))
    -- else if n = 3 complete to loaded packages?
  end
end

return M

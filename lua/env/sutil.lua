local M = {}

local D = require("dprint")
local default_debug_enabled = false
local dprint = D.mk_dprint_for(M, default_debug_enabled)
local dvisualize = D.mk_dvisualize_for(M, default_debug_enabled)

---@param str string
---@param start string ends
function M.starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

---@param str string?
---@param e string ends
function M.ends_with(str, e)
  return str ~= nil and e ~= nil and (e == "" or str:sub(- #e) == e)
end

-- Get substr from given range without errors
-- [start:endi] including both indices
---@param start number
---@param endi number|nil if not defined - to lineends
---@return string|nil
function M.substr(str, start, endi)
  if str and start then
    endi = endi or (#str + 1)
    if start < 1 then
      start = 1
    elseif start > #str then
      start = #str + 1
    end
    if endi > #str then
      endi = #str + 1
    elseif endi < 0 then
      endi = #str + 1 + endi
    end
    if start <= endi and start >= 1 then
      return str:sub(start, endi)
    end
  end
  return nil
end

---@param haystack string
---@param needle string
---@return number index
function M.last_indexof(haystack, needle)
  if haystack and needle and #needle <= #haystack and needle ~= "" then
    local i, j
    local k = 0

    repeat
      i = j
      ---@diagnostic disable-next-line: cast-local-type
      j, k = string.find(haystack, needle, k + 1, true)
    until j == nil
    return i
  end
  return -1
end

---@param str string
function M.get_last_line(str)
  if str then
    local pos = 0
    local start = 0
    while true do
      local nl = string.find(str, "\n", pos, true)
      if not nl then break end
      start = pos
      pos = nl + 1
    end
    if pos == 0 then
      return str
    end
    local len = string.len(str)
    local e
    if pos - 1 >= len then
      e = len - 1
    else
      start = pos
      e = len
    end
    -- print("["..str.."] len:"..len.." pos:"..pos.." prev:"..prev.." s:"..start.." e:"..e)
    return string.sub(str, start, e)
  end
  return nil
end

-- for init random seed. By default set to 0
-- Usage example set_randomseed(os.clock())
---@param seed number? for random
function M.set_randomseed(seed)
  if type(seed) == "number" then
    math.randomseed(seed)
  end
end

local chars_en_and_digits = {}
do -- [0-9a-zA-Z]
  for c = 48, 57 do table.insert(chars_en_and_digits, string.char(c)) end
  for c = 65, 90 do table.insert(chars_en_and_digits, string.char(c)) end
  for c = 97, 122 do table.insert(chars_en_and_digits, string.char(c)) end
end

-- ensume call the math.randomseed before call this func
-- or the result always be the same
---@param length number? length of generated string
function M.rand_str(length)
  if length and length > 0 then
    --math.randomseed(os.clock() ^ 5)
    --local array = {}
    local line = ""
    for _ = 1, length do
      --array[i] = chars_en_and_digits[math.random(1, #chars_en_and_digits)]
      line = line .. chars_en_and_digits[math.random(1, #chars_en_and_digits)]
    end
    return line --table.concat(array)
  end
  return ""
end

---@param src string
---@param sub string
---@param show_error any if not null - print_error
---@return boolean
function M.contanis(src, sub, show_error)
  if src and sub and sub ~= "" then
    local s = string.find(src, sub)
    if s and s >= 0 then
      return true
    end
    if not s or s == "number" then
      -- print (inspect(s))
      if show_error and show_error ~= "no" then
        print("Not Contains [" .. sub .. "] in [" .. src .. "]")
      end
    end
  end
  return false
end

-- usefull then need replace plain string via string.gsub(src, plain)
-- by default gsub  considers the second parameter as an regex_escape
---@param str string
function M.regex_escape(str)
  return str:gsub("[%(%)%.%%%+%-%*%?%[%^%$%]]", "%%%1")
end

-- replace newlines to escaped '\n'
function M.escape_nl(s)
  if s and s:find("\n", 1, true) then
    return s:gsub("\n", '\\n')
  end
  return s
end

-- split one big line to the several lines with given max length
---@param srcline string
---@param maxlen number?
---@return table<string>
function M.fold_line(srcline, maxlen)
  maxlen = maxlen or 80
  if srcline then
    local lines = {}
    local start, prev_end = 1, 1
    local is_comment = false
    local f = srcline:sub(1, 2)
    if f == '--' or f == '//' or f == '# ' then
      start = 3
      is_comment = true
      maxlen = maxlen - 3
      if f == '# ' then f = '#' end
    end
    -- vim.fn.strdisplaywidth  str2list
    for i = 1, #srcline do
      if (i - start) >= maxlen then
        if start < prev_end then
          local line0 = string.sub(srcline, start, prev_end - 1)
          if is_comment then
            line0 = f .. ' ' .. line0
          end
          table.insert(lines, line0)
          start = prev_end + 1
        end
      end
      local c = string.sub(srcline, i, i)
      if c == ' ' then
        prev_end = i
      end
    end

    if start <= #srcline then
      local line0 = string.sub(srcline, start, #srcline)
      if is_comment then
        line0 = f .. ' ' .. line0
      end
      table.insert(lines, line0)
    end
    return lines
  end
  return {}
end

-- split given src_line by '\n'
-- \n to new lines
-- \t to tab
function M.apply_escapes(src_line)
  if src_line and src_line ~= '' then
    local lines = M.split_range(src_line, "\\n")
    local res = {}
    if lines then
      for _, line in pairs(lines) do
        line = line:gsub('\\t', "\t")
        table.insert(res, line)
      end
    end
    --for line in src_line:gmatch('([^\n]+)\\n') do

    -- for line in (src_line..'\\n'):gmatch('([^\\]+)\\n') do
    --   table.insert(lines, line)
    -- end
    return res --lines
  end
  return {}
end

--- Splits a string at each instance of a separator.
---
--- Examples:
--- <pre>
---   split(":aa::b:", ':'))   -->   { 'aa', 'b' },
---   split("axaby", 'ab?'))   -->   { 'x', 'y' },
---   split("x*yz*o", '%*'))   -->   { 'x', 'yz', 'o' }
---   split("|x|y|z|", '|'))   -->   { 'x', 'y', 'z' }
--- </pre>
---
---@param s string String to split
---@param sep string? Separator pattern by default sep is space
---@return table List of split components
---@param foreach function?
function M.split(s, sep, t, foreach)
  assert(not t or type(t) == 'table')
  if type(sep) ~= 'string' then sep = nil end -- case t moved to sep
  if sep == nil or sep == ' ' then
    sep = "%s"
  end
  t = t or {}
  local has_cb = type(foreach) == 'function' ---@cast foreach function
  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    if has_cb then str = foreach(str) end
    table.insert(t, str)
  end
  return t
end

-- 'abc\n\def\ng' -->  {'abc', 'def', 'g'}
-- 'abc\n\def\n' -->  {'abc', 'def', ''}
-- not remove empty lines, emply line in the end of list - the sign that
-- there was '\n' at the end of the string (used in process stdout)
---@param str string
---@param sep string? plain text Default is '\n'
---@param p_start number?
---@param p_end number?  if not defined or less than 0 - take str length
---@param out_last table|false|nil  {pos} -- return char position in the last el
---@param lines table<string>|nil
function M.split_range(str, sep, p_start, p_end, out_last, lines)
  assert(not out_last or out_last == false or type(out_last) == 'table', 'out_last')
  assert(not lines or type(lines) == 'table', 'lines')

  if str then
    p_start = p_start or 1
    if not p_end or p_end < 0 then
      p_end = string.len(str) --#str
    end
    sep = sep or '\n'
    local pos = p_start
    local line_start = 0
    lines = lines or {}
    while true do
      local p = string.find(str, sep, pos, true)
      if not p or p > p_end then
        break
      end
      -- if (p > p_end) then return lines end
      line_start = pos
      pos = p + #sep --1
      p = p - 1
      if str:sub(p, p) == '\r' then
        p = p - 1
      end
      table.insert(lines, string.sub(str, line_start, p))
    end
    if out_last then
      out_last.pos = pos
    end
    table.insert(lines, string.sub(str, pos, p_end))
    return lines
  end
  return nil
end

-- Return the index of line contains the find-str in lines (list-table)
-- lines - only list-table with numeric indexes
-- if you need to search in specific range of lines - define this range
-- through [start:end] (By default search goes in all lines)
-- if find-str not found - return -1
--
---@param lines table<string> stack
---@param find_str string needle
---@param start number?
---@param ends number?
---@param plain boolean? (Default true)
---@return number index
function M.indexof(lines, find_str, plain, start, ends)
  if (lines and find_str) then
    plain = plain == nil or plain
    start = start or 1
    ends = ends or #lines
    for i, line in pairs(lines) do
      if i >= start then
        local p = string.find(line, find_str, 1, plain)
        if p then
          ---@diagnostic disable-next-line: return-type-mismatch
          return i
        end
      end
      if i > ends then
        break
      end
    end
  end
  return -1;
end

-- Remove all empty lines from list<string>
-- Warn! Modify the given list-table
---@param list table
function M.rm_empty_elm(list)
  if type(list) == 'table' then
    for i = #list, 1, -1 do
      if list[i] == '' then
        table.remove(list, i)
      end
    end
  end
  return list
end

-- dump object value to string
-- tables prints in simple one-line style without newlines
function M.inspect0(obj)
  if type(obj) == 'table' then
    local s = '{'
    local first = true
    for k, v in pairs(obj) do
      if not first then
        s = s .. ', '
      end
      if type(k) == 'number' then
        k = '[' .. k .. ']'
      elseif type(k) ~= 'string' then
        -- Note inspect shown srting keys starts with digit as ["N*"]
        k = tostring(k)
      end
      s = s .. k .. ' = ' .. M.inspect0(v)
      first = false
    end
    return s .. '}'
  elseif type(obj) == 'string' then
    return '"' .. obj .. '"'
  else
    return tostring(obj)
  end
end

-- improved string formatting aimed at increased stability
-- extended string formating aimed to
-- and the maximum possible display of all passed arguments
-- ignore broken %patterns if no valid places defined via %s then
-- remains varargs appends to end of msg
-- tables shown in one-line style
---@param fmsg string
---@param ... any
function M.format(fmsg, ...)
  fmsg = fmsg or ''
  local off = 1
  local no_places = false
  for i = 1, select('#', ...) do
    local val = select(i, ...)
    local t = type(val)
    if t == 'table' then
      val = M.inspect0(val)
    elseif t ~= 'string' then
      val = tostring(val)
    end

    if no_places then
      fmsg = fmsg .. ' ' .. val -- append to end of line of no more  '%s'
    else
      --local p = string.find(fmsg, '%%%a', off, false)
      local p, ch
      local search = true
      -- find next %X replace '%%' to '%'
      while search do
        search = false
        p = string.find(fmsg, '%', off, true)
        if p then
          ch = string.sub(fmsg, p + 1, p + 1) -- std ch is: 's' 'd' 'f'...
          if ch == '%' then                   -- %% -> %
            fmsg = string.sub(fmsg, 1, p - 1) .. string.sub(fmsg, p + 1)
            off = p + 1
            search = true
          end
        end
      end
      -- replace %? by value
      if p then
        local left = string.sub(fmsg, 1, p - 1)
        local right = string.sub(fmsg, p + 2)
        fmsg = left .. val .. right
        off = #left + #val
      else
        no_places = true
        fmsg = fmsg .. ' ' .. val
      end
    end
  end
  return fmsg
end

function M.trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

-- check is string contains y|yes w
---@param s string?
---@return boolean
function M.is_yes(s)
  if s and s ~= "" then
    s = string.lower(s)
    return (s == "y" or s == "yes" or s == "+" or s == "1") == true
  end
  return false
end

function M.table_size(t)
  if t and type(t) == 'table' then
    local i = 0
    for _, _ in pairs(t) do i = i + 1 end
    return i
  end
  return -1
end

-- Checking the correctness of the substitution into the template of values
-- for the given key-value pairs.
-- In Output Keys replaced by specific values from the substitutions table.
--
-- This check should ensure that
-- no one substitution key from the table must remain in the result lines
--
-- to Create output based templete and substitutions use:
-- local output = string.gsub(template, "[%w._]+", substitutions)
-- See test for examples
--
---@param lines table generated output based template + substitutions
---@param templ_lines table
---@param substitutions table
function M.check_templ_substitution(lines, templ_lines, substitutions)
  if type(lines) ~= 'table' then
    error('lines must be a table with strings')
  end
  if type(templ_lines) ~= 'table' then
    error('templ_lines must be a table with strings')
  end
  if type(substitutions) ~= 'table' then
    error('substitutions must be a key-value table')
  end

  local function has_subs_key_in(line0)
    for key, _ in pairs(substitutions) do
      local s, _ = string.find(line0, key, 1, true)
      if s then
        return key
      end
    end
    return nil
  end

  for i, line in pairs(lines) do
    local tline = templ_lines[i]
    if not tline then
      error('Not Found templ_line ' .. tostring(i))
    end
    if #line > 0 and has_subs_key_in(tline) then
      local key = has_subs_key_in(line)
      if key then
        error('Found Not Replaced Key: [' .. tostring(key) .. ']')
      end
    end
  end
end

-- generate password
---@param length number?         default 12
---@param use_specials boolean?  default - false
---@param seed number?|string?   default is os.time() old - to use prev randomseed
function M.rand_rasswd(length, use_specials, seed)
  local normals = {
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "abcdefghijklmnopqrstuvwxyz",
    "0123456789",
  }
  local specials = "!#$%&()*+,-./:;<=>?@[]^_{|}~"

  length = length or 12
  if seed ~= "old" then
    seed = seed or os.time()
    math.randomseed(seed)
  end

  local chars = normals
  if use_specials then
    table.insert(chars, specials)
    print('with special')
  end
  local index, pw = 0, ""
  repeat
    -- index = index + 1
    index = math.random(#chars)
    local rnd = math.random(chars[index]:len())
    local ch = chars[index]:sub(rnd, rnd)
    if math.random(2) == 1 then
      pw = pw .. ch
    else
      pw = ch .. pw
    end
    -- index = index % #chars
  until pw:len() >= length
  return pw
end

--
-- generate random id
---@param length number?
---@param seed number?
---@return string
function M.rand_id(length, seed)
  local normals = {
    "abcdef",
    "0123456789",
  }

  length = length or 32
  if seed ~= "old" then
    seed = seed or os.time()
    math.randomseed(seed)
  end

  local chars = normals
  local index, pw = 0, ""
  repeat
    -- index = index + 1
    index = math.random(#chars)
    local rnd = math.random(chars[index]:len())
    local ch = chars[index]:sub(rnd, rnd)
    if math.random(2) == 1 then
      pw = pw .. ch
    else
      pw = ch .. pw
    end
    -- index = index % #chars
  until pw:len() >= length
  return pw
end

function M.dup(s, n)
  if s and n then
    local res = ''
    for _ = 1, n do res = res .. s end
    return res
  end
  return s
end

-- substitute specific values for the start and end positions and
-- prepare for correct operation if possible
--
-- if p_start and p_end not specified then take from [1 to line end]
-- if given with negative indixes then calculate specific positions from
-- the end of the sring
--
---@param line string
---@param p_start number|nil
---@param p_end number|nil
---@return number?, number?
function M.normalize_range(line, p_start, p_end)
  p_start = p_start or 1
  p_end = p_end or #line
  if p_end < 0 then p_end = #line + p_end + 1 end       -- from end of line
  if p_start < 0 then p_start = #line + p_start + 1 end -- from end of line
  if p_start > #line then
    return nil, nil                                     -- correct work not possible!
  end
  if p_end > #line then
    p_end = #line
  end
  return p_start, p_end
end

-- return index of the given character (fc) in line or -1 if not found
---@param line string
---@param fc string    -- char to find
---@param p_start number|nil  default is 1
---@param p_end number|nil    default is #line
function M.back_indexof(line, fc, p_start, p_end)
  if line and fc and #fc == 1 then
    p_start, p_end = M.normalize_range(line, p_start, p_end)
    if not p_start then
      return -1
    end
    if p_start < p_end then
      for i = p_end, p_start, -1 do
        if line:sub(i, i) == fc then
          return i
        end
      end
    end
  end
  return -1
end

function M.get_char_count(line, fc, limit)
  if line and fc then
    local cnt = 0
    for i = 1, #line do
      local c = line:sub(i, i)
      if c == fc then
        cnt = cnt + 1
        if limit and cnt > limit then break end
      end
    end
    return cnt
  end
  return 0
end

--
local pairs_group = {
  ["'"] = '\'',
  ['"'] = '"',
  ["("] = ')',
  ["{"] = '}',
  ["["] = ']',
  [")"] = '(',
  ["}"] = '{',
  ["]"] = '[',
}

---@param c string
---@return string|nil
function M.get_pair(c)
  if c then
    return pairs_group[c]
  end
end

function M.is_paired(c)
  return c == '\'' or c == '"' or
      c == '(' or c == ')' or
      c == '{' or c == ']' or
      c == '[' or c == ']'
end

---@param c string
---@return boolean
function M.is_opening_pair(c)
  return c == '\'' or c == '"' or c == '(' or c == '{' or c == '['
end

function M.is_quote(c)
  return c == '\'' or c == '"'
end

--
-- find the end pos for current opened char(group)
-- supports groups: {} [] () '' "" and escape char \\
-- all in '' and "" groups consider as the simple text and a group
---@param line string
---@param offset number|nil
---@return number  index of the end of given group
function M.get_group_end(line, offset)
  dprint("get_group_end")
  offset = offset or 1
  if not line or offset >= #line then
    return -1
  end

  local fc = line:sub(offset, offset)
  if not M.is_opening_pair(fc) then
    -- dprint('offset must be at opening pair char, got: ', fc)
    return #line
  end

  local open, close, stack, top = fc, M.get_pair(fc), {}, {}
  top = { cnt = 1, open = open, close = close }
  stack[#stack + 1] = top

  local DEBUG = D.is_enabled(M)

  local prev, escape_n, not_escaped = fc, 0, false
  for i = offset + 1, #line do
    local c = line:sub(i, i)
    if DEBUG then dvisualize(line, i, i, 'prev:', prev) end

    if c == '\\' then
      escape_n = escape_n + 1
      -- self escaped by prev slash \\]
      not_escaped = prev == '\\' and escape_n % 2 == 0
    else
      escape_n = 0 -- \\x  break the chain of
    end

    if DEBUG then
      dprint('i:', i, 'open:', open, 'close:', close, ' cnt:', top.cnt,
        'escn:', escape_n, 'not_escaped:', not_escaped)
    end

    if prev ~= '\\' or (c ~= '\\' and not_escaped) then
      not_escaped = false
      if c == close then
        top.cnt = top.cnt - 1
        if top.cnt == 0 then
          -- pop
          if #stack > 1 then
            -- print('Leave from Group ', top.open, top.close, ' was groups:', #stack)
            top = stack[#stack - 1]
            stack[#stack] = nil
            open = top.open
            close = top.close
            dprint('Now Current Group is ', top.open, top.close, ' now groups:', #stack)
          else
            dprint("Finish ", i, "\n")
            return i
          end
        end
      elseif c == open then
        top.cnt = top.cnt + 1
        if DEBUG then
          dprint('Inc Deep in Current Group ', top.open, c, ' deep:', top.cnt)
        end
        --
      elseif M.is_opening_pair(c) then
        if open ~= '"' and open ~= "'" then
          -- open new group
          local po, pc = open, close -- DEBUG
          top = { cnt = 1, open = c, close = M.get_pair(c) }
          open = top.open
          close = top.close
          stack[#stack + 1] = top
          if DEBUG then
            dprint('Enter to the New Group ', top.open, top.close,
              ' deep:', top.cnt, ' groups:', #stack, ' PrevGroup:', po, pc)
            -- if not top.close then error('[DEBUG] Not Found Close Char for: ' .. c) end
          end
        else
          -- dprint('Simple Text not a new Group ', c)
        end
      end
    end
    prev = c
  end
  dprint('# Ends with Error! no close: ', close, ' for:', open, 'stack:', #stack)
  return -1
end

M.debug = false
--
-- "func_a(x,y),func_b(z,u)"   -->  "func_a(x,y)"  &  "func_b(z,u)"
-- "a(x,y) ,   b(z,u)"         -->     "a(x,y)"    &  "b(z,u)" (not keep_spaces)
--        ^ ^^^
---@param line string
---@param sep string|nil|false     --  default is ','
---@param p_start number|nil
---@param p_end number|nil
---@param keep_spaces boolean|nil  -- default false to make trim
---@return table?, number?
---@param decorator function?     -- element decorator befor add to list
function M.split_groups(line, sep, p_start, p_end, keep_spaces, decorator)
  assert(not decorator or type(decorator) == 'function', 'decorator')
  dprint("split_groups")
  sep = sep or ','
  if line then
    p_start, p_end = M.normalize_range(line, p_start, p_end)
    if not p_start or not p_end then
      dprint('cannot normalize_range')
      return nil, nil
    end
    local prev, i, t = p_start, p_start, {}

    -- if M.debug then
    --   print('-------------------------------------------------------------------')
    --   print('         10        20        30        40        50        60')
    --   print('123456789012345678901234567890123456789012345678901234567890')
    --   print(line, 'p_start:', p_start, 'p_end:', p_end)
    -- end
    dvisualize(line, p_start, p_end)

    local function _add()
      local sub = line:sub(prev, i - 1)
      -- if M.debug then print('[DEBUG] add [' .. sub .. '] prev:', prev, ' i:', i - 1) end
      if not keep_spaces then
        sub = sub:gsub("^%s*(.-)%s*$", "%1") -- trim spaces from startd and end
      end
      if decorator then
        sub = decorator(sub)
      end
      t[#t + 1] = sub
      prev = i + 1
    end

    while i < p_end do
      local c = line:sub(i, i)
      dvisualize(line, i, i)
      if c == sep then
        _add()
      elseif M.is_paired(c) then --and i < p_end then
        local ge = M.get_group_end(line, i)
        dprint('found paired c', c, ' at ', i, 'group_end', ge)
        if not ge or ge == -1 then
          _add()
          -- error('Cannot find end of gproup for ' .. c)
          -- return what was able to recognize and the reached position
          return t, i --
        end
        i = ge
        dprint('paired i := ', i, 'c:', c, ' end:', line:sub(i, i))
      end
      i = i + 1
    end
    -- dprint('latest: prev:', prev, ' i:', i, ' ge:', ge)
    i = p_end + 1
    _add()
    dprint('end of group at pos:', i)
    return t, i
  end
  return nil, nil
end

---@param line string
---@return string
function M.unescape0(line)
  line = string.gsub(line, '\\a', "\a")
  line = string.gsub(line, '\\b', "\b")
  line = string.gsub(line, '\\f', "\f")
  line = string.gsub(line, '\\n', "\n")
  line = string.gsub(line, '\\r', "\r")
  line = string.gsub(line, '\\t', "\t")
  line = string.gsub(line, '\\v', "\v")
  line = string.gsub(line, '\\"', '"')
  line = string.gsub(line, "\\'", "'")
  line = string.gsub(line, '\\\\', '\\')
  return line
end

function M.unwrap_quotes(s)
  if s then
    local fc = s:sub(1, 1)
    local lc = s:sub(-1, -1)
    if fc == lc and (fc == '"' or fc == "'") then
      s = s:sub(2, -2)
    end
  end
  return s
end

---@param s string
---@param len number
---@param at string one of center, left, right  or  ' - ', '- ', ' -'
function M.align_str_to(s, len, at)
  at = at or ' - ' -- center
  local slen = #s
  if slen > len then
    return s:sub(1, len)
  else
    local rems = len - slen
    local l = math.floor(rems / 2)
    local r = rems - l
    if at == ' - ' or at == 'center' then
      return string.rep(' ', l, '') .. s .. string.rep(' ', r, '')
    elseif at == '- ' or at == 'left' then
      return s .. string.rep(' ', rems, '')
    elseif at == ' -' or at == 'right' then
      return string.rep(' ', rems, '') .. s
    end
  end
end

function M.replace_first(line, old_sub, new_sub)
  if line and old_sub then
    local i, j = string.find(line, old_sub, 1, true)
    if i and j then
      return line:sub(1, i - 1) .. new_sub .. line:sub(j + 1)
    end
  end
  return line
end

---@param lines table
---@param chars table
---@param repl string?
function M.replace_chars_in_lines(lines, chars, repl)
  repl = repl or ''
  local t = {}
  for i, line in ipairs(lines) do
    for _, char in ipairs(chars) do
      line = string.gsub(line, char, repl)
    end
    t[i] = line
  end
  return t
end

---@param line string
---@param s number
---@param e number
---@param replacement string
function M.replace_in_range(line, s, e, replacement)
  if line and #line >= e and replacement then
    return line:sub(1, s - 1) .. replacement .. line:sub(e + 1, #line)
  end
  return line
end

function M.fromhex(s)
  return (s:gsub('..', function(cc)
    return string.char(tonumber(cc, 16))
  end))
end

function M.tohex(s)
  return (s:gsub('.', function(c)
    return string.format('%02x', string.byte(c))
  end))
end

return M

-- 27-04-2024 @author Swarg
-- snippets for EnvLineInsert for clang
--
--
--
--

local M = {}

local insert_items = {
  "",
  "",
  "",
  "\nvoid ${WORD}(void)\n{\n}\n",
  '${TAB} fprintf(stderr, "\n")',
  "",
  "",
  "",
  "",
  "\n  test TODO", -- 10
  "",
  "",
  "",
  "${MODULE}",
  "",
  "",
  ",${WORD}:",
  "",
  "",
  "",
  "assert ${LINE} ==",
  '    assert ${WORD} == \n',
  '    res = ${WORD}\n  assert res == \n',
  "",
  "",
  "",
  "",
  "",
  "",
  '\n-- ${DATE} @author ${AUTHOR} \ndefmodule ${SWORD} do\nend', -- 30
  "${TOGGLE_BOOL}",
  "'${WORD}'",
  "'${SWORD}'",
  '{${SWORD}}',
  "  fprintf(stderr,${WORD})\n",
}

local insert_mappings = {
  -- TODO
}
local insert_handlers = {
  -- TODO
}

---@param reg function
function M.add(reg)
  reg({ 'c', 'h' }, insert_items, insert_mappings, insert_handlers)
end

return M

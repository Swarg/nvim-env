-- 07-11-2023 @author Swarg
local M = {}

-- Goal:
-- NewStuff Command handler for PhpGen

local log = require 'alogger'
local ucmd = require 'env.lang.utils.command'

local PhpGen = require 'env.langs.php.PhpGen'
---@diagnostic disable-next-line: unused-local
local Editor = require("env.ui.Editor")
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")
local FieldGen = require("env.lang.oop.FieldGen")

-- :EnvNew <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('New Stuff for php')
      :handlers(M)

      :desc('Generate new php class')
      :cmd("class", "c")

      :desc('Add new covers to a class docblock')
      :cmd("add-covers")

      :desc('Create new test-case file. (many-tests-for-one-source)')
      :cmd("test-case", 'tc')

      :run()
end

--------------------------------------------------------------------------------

local defineStdGenOpts = ucmd.defineStdGenOpts

--- :EnvNew class
---@param w Cmd4Lua
-- function PhpGen:cmdNewClass(w)
---@return string|false  -- path
---@return string?       -- errmsg
function M.cmd_class(w)
  local gen = w:var('gen') ---@type env.langs.php.PhpGen
  defineStdGenOpts(w) --dry-run --quiet --autho --no-date --not-open

  local classname, pkg
  pkg = w:opt('--package', '-p', 'by default its taken from a current file')

  w:group('A', 1)
      :desc('If a shortname - generate with a package(ns) of current opened')
      :tag('classname'):pop():v_arg()
  w:group('A', 1)
      :desc('Take a ClassName from a current line with use(import)')
      :tag('classname')
      :v_optp('--from-use', '-u', PhpGen.getClassNameFromLine, gen)

  w:v_opt('--imports', '-i')
  w:v_opt('--extends', '-e') -- inheritance
  w:v_opt('--implements', '-m')

  w:desc('Class of Object-Value with specified fields, getters, isEqualTo')
  w:v_has_opt('--object-value', '-o') -- entity-class

  w:desc('public if without a constructor, and private if with. ' ..
    'Example: --fields [field1 private-Id-id mod-Type-name Type]')
  local fields_cli = w:optl('--fields', '-f')

  -- if not defined in opt fields check in --class-value
  if not fields_cli and w:var('object_value') then
    fields_cli = w:optl('--object-value', '-o')
  end

  -- params
  w:v_has_opt('--constructor', '-c') -- ??

  -- todo
  -- w:desc('apply mb_strtolower for string params in constructor')
  -- opts.str_lower = w:opt('--strto-lower', '-sl', desc)

  w:tag('test_class'):has_opt('--test', '-t', 'generate a test file')
  -- methodname to autogen pick from self:getContext().element(LEX_TYPE.FUNC)


  ----------------------------------- actually work (runs if all input correct)
  if not w:is_input_valid() then return false end


  -- convert from cli defintion to Field objects
  if fields_cli then -- list of strings from cli input to parse
    w:set_var('fields', FieldGen.parseFieldsFromStrings(fields_cli))
  end

  classname = w.vars.classname -- autovalidate in is_input_valid

  -- if package(namespace) not defined when take from current opened class
  if not classname:find('\\', 1) then
    if not pkg then
      local ctx = gen:getContext()
      local from_ci = gen.lang
          :resolveClass(ctx.bufname, ClassInfo.CT.CURRENT)
          :getClassInfo()
      if from_ci then
        pkg = from_ci:getPackage()
        log.debug('Resolve from current: %s pkg:', from_ci:toString(), pkg)
      end
    end
  end

  if pkg then
    classname = pkg .. '\\' .. classname
  end

  if not w:is_quiet() then
    classname = Editor.askValue('Full ClassName: ', classname)
    if not classname or classname == '' then
      gen:getEditor():echoInStatus('Canceled')
      return false
    end
  end
  log.debug('New ClassName to Generate: %s', classname)
  local new_ci = ClassResolver.of(gen.lang):lookupByClassName(classname)

  return gen:createClassFile(new_ci, w:get_vars())
end

--------------------------------------------------------------------------------

--
--
---@param w Cmd4Lua
-- function PhpGen:cmdAddCovers(w)
function M.cmd_add_covers(w)
  local phpgen = w:get_var('gen')
  local classname

  w:group('A', 1):tag('classname'):pop():v_arg()
  w:group('A', 1):desc('Take a ClassName from a current line with use(import)')
      :tag('classname')
      :v_optp('--from-use', '-u', PhpGen.getClassNameFromLine, phpgen)

  classname = w.vars.classname -- autovalidate in is_input_valid

  if w:is_input_valid() then   --  ensure_all_parsed
    -- add covers
    phpgen:addCoversToDocblock(classname)
  end
end

--
-- Create new test-case file. (many-tests-for-one-source)
--
-- new test-case - in subdir with source-file name
-- dy default test-case will be named from the method name under the cursor
--
---@param w Cmd4Lua
function M.cmd_test_case(w)
  local gen = w:var('gen') ---@type env.langs.php.PhpGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  w:desc('test-case name i.g. method name')
      :v_opt('--name', '-n') -- support name with subdirs:  sub1/sub2/

  if not w:is_input_valid() then return end

  return gen:createNewTestCaseFile(w.vars)
end

return M

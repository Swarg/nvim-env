---@diagnostic disable: duplicate-set-field
local class = require 'oop.class'

local fs = require('env.files')
---@diagnostic disable-next-line: unused-local
local log = require('alogger')

local R = require 'env.require_util'
local json = R.require("cjson", vim, "json") -- vim.json

local Builder = require('env.lang.Builder')

class.package 'env.langs.php'
---@class env.langs.php.ComposerBuilder : env.lang.Builder
---@field new fun(self, o:table?, project_root:string?): env.langs.php.ComposerBuilder
---@field project_root string
---@field buildscript string
---@field src string
---@field test string
---@field phpunit env.langs.php.PhpUnit
---@field namespaces table
---@field modules_test_namespaces table
---@field config table?  -- parsed composer.json
local ComposerBuilder = class.new_class(Builder, 'ComposerBuilder', {
  name = 'composer', -- php ?
  buildscript = 'composer.json',
  project_props = nil,
  -- namespaces = nil,              getNamespaceMap()
  -- modules_test_namespaces = nil, getModulesTestNamespaceMap() based phpunit
})


---@param args table
---@param opts table
---@return table args
local function setup_opts(args, opts)
  if args and opts then
    if opts.clean_task == true then
      table.insert(args, 1, "clean") -- cleanTest?
    end
  end
  return args
end

-- public static
-- check is the given directory is Root of Source Code (has empty .git directory)
---@param path string
---@return string?  path to script file or nil
---@diagnostic disable-next-line: duplicate-set-field
function ComposerBuilder.isProjectRoot(path)
  if path then
    local script_file = fs.join_path(path, 'composer.json') --".git")
    if fs.file_exists(script_file) then
      return script_file
    end
  end
  return nil
end

function ComposerBuilder:getConfig()
  if not self.config then
    self.config = self:parseJson()
  end
  return self.config
end

---@private
function ComposerBuilder:parseJson()
  local path = fs.join_path(self.project_root, self.buildscript)
  local jsonstr = fs.read_all_bytes_from(path)
  local config = assert(json.decode(jsonstr), 'must contain a JSON object')
  return config
end

-- todo take from composer.json
-- autoload & autoload-dev
function ComposerBuilder:getNamespaceMap()
  if not self.namespaces then
    -- TODO  parse from compose.json
    self.namespaces = {
      -- path into first part of namespace
      ['src/'] = 'App\\',
      ['tests/'] = 'Test\\'
    }
  end
  return self.namespaces
end

function ComposerBuilder:setPhpUnit(phpunit)
  self.phpunit = phpunit
  self:getModulesTestNamespaceMap()
end

--
function ComposerBuilder:getModulesTestNamespaceMap()
  -- if already created - reuse
  if self.modules_test_namespaces then
    return self.modules_test_namespaces
  end

  -- create a new one
  self.modules_test_namespaces = {}
  local tdirs = nil -- test modules paths
  if self.phpunit then
    tdirs = self.phpunit:getTestDirs('src/')
    -- src/Auth/Test/Unit
  end

  -- create mapping module namespace into test path
  local nsmap = self:getNamespaceMap()
  if tdirs then
    local pattern = '([%w]+).([%w]+).([%w]+)' -- detect modular structure
    for _, dir in pairs(tdirs) do
      local src, module, test = dir:match(pattern)
      if test == 'Test' and src ~= nil and module ~= nil then
        local module_ns = (nsmap[src .. '/'] or 'X') .. module .. '\\'
        self.modules_test_namespaces[module_ns] = dir
      end
    end
  end
  return self.modules_test_namespaces
end

--
-- TODO
function ComposerBuilder:argsTestSingleClass(class_name, opts, method_name)
  if class_name then
    local test = class_name
    -- if defined method name then run test only for this method
    if method_name and method_name ~= '' then
      test = test .. '.' .. method_name
    end
    local args = { "test", "--tests", test, "-i" } -- args
    return setup_opts(args, opts)
  end
  return {}
end

class.build(ComposerBuilder)
return ComposerBuilder

-- 19-09-2023 @author Swarg
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')

local Lang = require('env.lang.Lang') -- Abstract class Lang (Base)
---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")

local PhpGen = require 'env.langs.php.PhpGen'
local PhpUnit = require 'env.langs.php.PhpUnit'
local PhpDebug = require("env.langs.php.PhpDebug")
local ComposerBuilder = require('env.langs.php.ComposerBuilder')
-- local PhpConfSyncer = require('env.langs.php.PhpConfSyncer')
local Psalm = require('env.langs.php.Psalm')

-- Note: there must be a slash at the end of the path

class.package 'env.langs.php'
---@class env.langs.php.PhpLang : env.lang.Lang   @define class PhpLang extends Lang
---@field getLangGen fun(self): env.langs.php.PhpGen
---@field executable      string          bin of the lang interprater
---@field namespace_map   table           created by ComposerBuilder
---@field modules_test_namespaces table - created by ComposerBuilder based on
--                                       PhpUnit conf after findTestFramework()
local PhpLang = class.new_class(Lang, 'PhpLang', {
  ext = 'php',    -- extension of source files
  pkg_sep = '\\', -- separator used in namespace of classname
  executable = 'php',
  root_markers = { '.git', 'composer.json' },
  -- src = 'src/' -- take from Lang
  test = 'tests' .. fs.path_sep -- override test/
})

local DEFAULT_NAMESPACE_MAP = {
  [PhpLang.src] = 'App\\', [PhpLang.test] = 'Test\\'
}

Lang.register(PhpLang)


--------------------------------------------------------------------------------
--                                 INIT
--------------------------------------------------------------------------------

-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework

---@param fn string filename
---@return boolean
function PhpLang.isLang(fn)
  if fn then
    return fn:match('%.php$') ~= nil or
        fn:match("[/\\]composer%.json") ~= nil or
        fn:match("[/\\]phpunit%.xml") ~= nil or
        fn:match("[/\\]phpcs%.xml") ~= nil
  end
  return false
end

-- used at Lang:resolve
function PhpLang:findProjectBuilder()
  self.builder = nil
  if ComposerBuilder.isProjectRoot(self.project_root) then
    self.builder = ComposerBuilder:new({
      project_root = self.project_root,
      src = self.src,
      test = self.test,
    })
    -- self.builder:bindContext({ src = SRC, test = TEST, }) deprecated
    self:getNamespaceMap() -- bind self.namespace_map
    -- TODO take src and test paths from composer mappings
    -- self:syncSrcAndTestPathsWith(self.builder)
  else
    log.debug('[DEBUG] NO COMPOER! for %s', self.project_root)
  end
  return self
end

-- used at Lang:resolve
function PhpLang:findTestFramework()
  self.testframework = nil
  if self.project_root then
    local phpunit_xml = fs.join_path(self.project_root, 'phpunit.xml')
    if fs.file_exists(phpunit_xml) then
      local phpunit = PhpUnit:new():parse(fs.read_all_bytes_from(phpunit_xml))
      self.testframework = phpunit
      if self.builder then
        self.builder:setPhpUnit(phpunit) -- self.builder.phpunit = phpunit
        self.modules_test_namespaces = self.builder:getModulesTestNamespaceMap()
      end
    end
  end
end

-- Used to display file paths in the namespace path and vice versa
-- src/ -> App\\
---@return table self.namespace_map
function PhpLang:getNamespaceMap()
  if not self.namespace_map and self.builder and self.builder.getNamespaceMap then
    -- src/ -> App\\
    self.namespace_map = self.builder:getNamespaceMap() or {}
  end
  return self.namespace_map
end

-- for testing, instead of a constructor
function PhpLang:setup(opts)
  opts = opts or {}
  self.namespace_map = opts.namespace_map or DEFAULT_NAMESPACE_MAP
  return self
end

--------------------------------------------------------------------------------
--                        TEST FRAMEWORK
--------------------------------------------------------------------------------

---@return string?, string?  -  testsuite_name, test_path
function PhpLang:getTestSuiteName(namespace_path)
  local tfw = self:getTestFramework()
  if tfw and class.instanceof(tfw, PhpUnit) then
    ---@cast tfw env.langs.php.PhpUnit
    return tfw:findTestSuiteNameFor(namespace_path)
  end
end

-- find for given classname test directory settings from phpunit.xml config
---@return string?, string?  -- module_namespace, test_path
function PhpLang:getTestModuleMapping(classname)
  if classname and self.modules_test_namespaces then
    -- ['App\\Auth\\'] = 'src/Auth/Test/Unit',
    for module_ns, tpath in pairs(self.modules_test_namespaces) do
      if su.starts_with(classname, module_ns) then
        return module_ns, tpath
      end
    end
  end
  return nil
end

--------------------------------------------------------------------------------

-- inner_path is path inside project without project_root and extension
-- in modular project it src + modulename + namespace
-- in php src dir maps into 'App' word first part of namespace
--
-- use PSR-4 from composer.json (holds in self.namespace_map)
--
---@param ns string? -- the namespace or innerpath (without root and extendsion)
--- override
function PhpLang:getClassNameForInnerPath(ns)
  if ns and self.namespace_map then
    for path, ns_v in pairs(self.namespace_map) do
      if su.starts_with(ns, path) then
        ns = ns_v .. string.sub(ns, string.len(path) + 1, string.len(ns))
        local res = fs.path_to_classname(ns, self.pkg_sep) -- '\\' class_name
        return res
      end
    end
  end
  return ns
end

--- override
---@param cn string -- classname
function PhpLang:getInnerPathForClassName(cn)
  if cn then
    if self.namespace_map then
      for path, ns_v in pairs(self.namespace_map) do
        if su.starts_with(cn, ns_v) then
          cn = path .. string.sub(cn, string.len(ns_v) + 1, string.len(cn))
          break
        end
      end
    else
      log.debug('[DEBUG] No namespace_map for root: %s', self.project_root)
    end

    local res = fs.classname_to_path(cn, self.pkg_sep)
    return res -- relative path in the project
  end
  return cn
end

--- override
---@param filename string
---@return boolean
function PhpLang:isSrcPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getSrcPath()) and not self:getTestSuiteName(ns)
  end
  return false
end

--- override
---@param filename string
---@return boolean|nil
function PhpLang:isTestPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    if self:hasTestFramework() then
      return self:getTestSuiteName(ns) ~= nil
    else
      return su.starts_with(ns, self:getTestPath())
    end
  end
end

--
-- check is a class are test class from some configured testsuite path
-- if so then pass testsuite path and name into classinfo
--
-- Lookup goes through InnerPath
--
-- findout test suites for given inner-path(namespace)
-- check is classinfo from some testsuite namespace (test-class)
-- then addd testsuite_name and testsuite_path into classinfo
---@param classinfo env.lang.ClassInfo
---@return boolean
function PhpLang:lookupTestSuite(classinfo)
  local ns = classinfo:getInnerPath()
  if ns then
    if self:hasTestFramework() then
      local testsuite_name, testsuite_path = self:getTestSuiteName(ns)
      if testsuite_name and testsuite_path then
        classinfo:setTestSuite(testsuite_name, testsuite_path)
        return true
      end
    else
      if su.starts_with(ns, self:getTestPath()) then
        classinfo:setTestSuite('unit', self:getTestPath())
        return true
      end
    end
  end
  return false
end

--
-- a simple and straightforward classname mappings: one source to one test
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- for module configured at phpunit:
-- source-class: App/Auth/Entity/User/Id
-- return      : App/Auth/Test/Unit/Entity/User/Id
--- override
function PhpLang:getTestClassForSrcClass(src_classname)
  local module_ns, tpath = self:getTestModuleMapping(src_classname)

  -- if here no modules configured in phpunit use default mapping:
  if not module_ns and not tpath then
    module_ns = 'Test'
    tpath = self:getTestPath()
  end

  if module_ns and tpath then
    local test_ns = self:getClassNameForInnerPath(tpath)
    local relative_cn = src_classname:sub(#module_ns + 1)
    return Lang.joinNs(test_ns, relative_cn, self.pkg_sep) .. self.test_suffix
  end
  return nil
end

--
-- a simple and straightforward classname mappings: one test to one source
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- for module configured at phpunit:
-- test-class: App/Auth/Test/Unit/Entity/User/Id
-- return    : App/Auth/Entity/User/Id
--- override
function PhpLang:getSrcClassForTestClass(test_classname)
  local module_ns, tpath = self:getTestModuleMapping(test_classname)
  if module_ns and tpath then
    local test_ns = self:getClassNameForInnerPath(tpath)
    local relative_cn = test_classname:sub(#test_ns + 1)
    local scn = Lang.joinNs(module_ns, relative_cn, self.pkg_sep)
    if scn and su.ends_with(scn, self.test_suffix) then
      scn = scn:sub(1, - #self.test_suffix - 1);
    end
    return scn
  else
    -- TODO without modules
  end
  return nil
end

-- ---@deprecated
-- function PhpLang:generateSourceFile(classinfo, opts)
--   log.debug("generateSourceFile")
--   if classinfo then
--     self:getLangGen():createSourceFile(classinfo, opts)
--   end
-- end

---@return string?
function PhpLang:getDockerComposeServiceName()
  local dc = self:getDockerCompose()
  if dc then
    -- current submodule(dirname) as begining of the "service"-name
    local prefix = fs.get_parent_dirname(self.project_root) or ''

    local service = dc:findServiceName(prefix, 'php', 'cli')
        -- if here no *-php-cli image for console check *-php-fpm
        or dc:findServiceName(prefix, 'php', 'fpm')
    log.debug('Service name: %s', service)
    return service
  end
end

--
-- wrap to run cmd through a docker container if it configured in project
--
-- check is current project configured to development via docker-compose
-- and if so then prepare docker command to execute a given command and args
-- via docker-container
--
--  How its works:
--   - lookup docker-compose.yml file
--   - get dirname of the current project_root (as subproject name)
--   - search "service" name with <subproject-dirname> <php> <cli|fpm>
--   - if the service name is found then assume that the project is
--     configured to run through a docker container
--
---@param cmd string
---@param args table
---@param envs table?
---@return string, table, table?
function PhpLang:wrapToDockerIfConfigured(cmd, args, envs)
  if cmd then
    local service_name = self:getDockerComposeServiceName()

    if service_name then
      local dc = self:getDockerCompose()
      ---@cast dc env.lang.DockerCompose
      args = dc:getArgsToRunService(service_name, cmd, args, envs)
      cmd = dc:getExecutable() -- 'docker'
      return cmd, args, nil    -- envs passed into docker-container
    end
  end
  return cmd, args, envs
end

--------------------------------------------------------------------------------
--                            USE CASES
--------------------------------------------------------------------------------
-- See env.lang.Lang.lua

-- sync actual project configuration with nvim plugins
function PhpLang:doSyncProjectSettings(ci)
  log.debug("doSyncProjectSettings")
  self:ensurePathExists(ci)
  error('Not implemented yet')
  -- local syncer = PhpConfSyncer:new({ lang = self, classinfo = ci })
  -- syncer:sync()
end

---@param ci string
---@param w Cmd4Lua
function PhpLang:doCodeCheck(ci, w)
  log.debug("doCodeCheck")
  w:about('Code Check')

  if w:desc("A static analysis tool for php")
      :is_cmd('psalm', 'p') then
    Psalm:new({ lang = self, classinfo = ci }):handle(w)
  end

  w:show_help_or_errors()
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function PhpLang:getArgsToRunSingleTestFile(ci)
  log.debug("getArgsToRunSingleTestFile")
  self:ensurePathExists(ci)

  -- test framework bin
  local tfw_bin = self:getTestFrameworkExecutable() or 'php'
  local path = ci:getInnerPath() .. '.' .. self.ext
  local cmd, args = self:wrapToDockerIfConfigured(tfw_bin, { path })

  return self:newExecParams(cmd, args)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function PhpLang:getArgsToRunSingleSrcFile(ci)
  log.debug("getArgsToRunSingleSrcFile")
  self:ensurePathExists(ci)

  -- interpreter bin
  local php_bin = self.executable or 'php'
  local path = ci:getInnerPath() .. '.' .. self.ext
  local cmd, args = self:wrapToDockerIfConfigured(php_bin, { path })

  return self:newExecParams(cmd, args)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return string?     -- report of validation
function PhpLang:doValidateDebugger(ci)
  log.debug("doValidateDebugger")
  self:ensurePathExists(ci)

  return PhpDebug:new({
    lang = self,
    report = self:newReport('Validate Debugger')
  }):validateConfig(ci)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function PhpLang:getArgsToDebugSingleTestFile(ci)
  log.debug("getArgsToDebugSingleTestFile")
  self:ensurePathExists(ci)

  local php_bin = self.executable or 'php'
  local path = ci:getInnerPath() .. '.' .. self.ext
  local envs = { "XDEBUG_TRIGGER=1" }
  local cmd, args = self:wrapToDockerIfConfigured(php_bin, { path }, envs)

  return self:newExecParams(cmd, args, envs)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function PhpLang:getArgsToDebugSingleSrcFile(ci)
  log.debug("getArgsToDebugSingleSrcFile")

  return self:getArgsToDebugSingleTestFile(ci)
end

---@return env.langs.php.PhpGen
function PhpLang:newLangGen()
  self.gen = PhpGen:new({ lang = self })
  return self.gen
end

class.build(PhpLang)
return PhpLang

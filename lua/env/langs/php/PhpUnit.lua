-- 17-09-2023 @author Swarg
-- Dependencies:
-- luarocks install xml2lua
-- See https://github.com/manoelcampos/xml2lua
-- Goal: parse phpunit.xml interface to settings
-- TODO remove
local class = require 'oop.class'

local fs = require('env.files')
local ok_xml2lua, xml2lua = pcall(require, "xml2lua")
local ok_xmlhandler, xmlhandler = pcall(require, "xmlhandler.tree")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local DEBUG = false


class.package 'env.langs.php'
---@class env.langs.php.PhpUnit : oop.Object
---@field new fun(self, o:table?): env.langs.php.PhpUnit  -- Constructor(_init)
---@field attr ?table
---@field testsuites ?table
---@field source ?table
---@field php ?table
local PhpUnit = class.new_class(nil, 'PhpUnit', {
  executable = "vendor/bin/phpunit"
  -- attr = {},       -- from <phpunit *>
  -- testsuites = {}, -- unit = {}, functional = {}
  -- source = {},     -- [attr] includes(directories)
  -- php = {},        -- envs
})


-- Class of current instance ( to instantiate new objects)
function PhpUnit:getClass()
  return getmetatable(self)
end

local function check_dependencies()
  if not ok_xml2lua or not ok_xmlhandler then
    error('Not Found pacakge: xml2lua use luarocks install xml2lua')
  end
end

local function xml_node_to_list(dst, src)
  if src and type(src[1]) == 'string' then -- single objet
    table.insert(dst, src[1])
    -- list of objects (directories)
  elseif src and type(src[1]) == 'table' then
    for _, el in pairs(src) do
      table.insert(dst, el[1])
    end
  end
end

---@param xml string? -- lines table<string>
---@return env.langs.php.PhpUnit
function PhpUnit:parse(xml)
  assert(type(xml) == 'string', 'xml')
  check_dependencies()

  --Instantiates the XML parser
  local handler = xmlhandler:new()
  local parser = xml2lua.parser(handler)
  parser:parse(xml)

  local tpu = handler.root.phpunit
  if not tpu then
    error('Illegal xml: Not Found <phpunit>')
  end
  if DEBUG then print('[DEBUG] PhpUnit:Start Parsing') end

  if tpu._attr then
    if DEBUG then print('[DEBUG] PhpUnit: _attr') end
    self.attr = {}
    -- attributes whithin phpblock
    for k, v in pairs(tpu._attr) do
      self.attr[k] = v
    end
  end

  -- test directories
  if tpu.testsuites and tpu.testsuites.testsuite then
    if DEBUG then print('[DEBUG] PhpUnit: testsuites') end
    self.testsuites = {}
    for _, v in pairs(tpu.testsuites.testsuite) do
      if v and v._attr and v._attr.name and v.directory then
        local suite_name = v._attr.name
        local t = self.testsuites[suite_name] or {}
        xml_node_to_list(t, v.directory)
        self.testsuites[v._attr.name] = t
      end
      -- print(inspect(v))
    end
  end

  -- source directories
  if tpu.source then
    self.source = {}
    if tpu.source._attr then
      self.source.attr = {}
      for k0, v0 in pairs(tpu.source._attr) do
        self.source.attr[k0] = v0;
      end
    end
    if tpu.source.include and tpu.source.include.directory then
      self.source.includes = {}
      xml_node_to_list(self.source.includes, tpu.source.include.directory)
    end
  end

  -- php
  if tpu.php then
    self.php = {}
    if tpu.php.env then
      self.php.envs = {}
      for _, v in pairs(tpu.php.env) do
        if v._attr and v._attr.name then
          self.php.envs[v._attr.name] = v._attr.value -- skip force attr
        end
      end
    end
  end

  if DEBUG then print('[DEBUG] PhpUnit: Parsed Self:', inspect(self)) end
  return self
end

--------------------------------------------------------------------------------


-- is given path in test path
---@param path string
---@param suite_name string?
---@return boolean
function PhpUnit:is_test_path(path, suite_name)
  suite_name = suite_name or 'unit'
  if not path then -- or not path:match("Test.php$") then
    return false
  end
  if self.testsuites and self.testsuites[suite_name] then
    for _, v in pairs(self.testsuites[suite_name]) do
      local i = string.find(path, v, 1, true)
      if i then
        return true
      end
    end
  end
  return false
end

-- find testsuite name for namespacepath in project
---@param path string        -- can be full path to filename or namespacepath
---@return string?, string?  -- testsuitename and testpath
function PhpUnit:findTestSuiteNameFor(path)
  if self.testsuites and path then
    for name, testsuite in pairs(self.testsuites) do
      for _, v in pairs(testsuite) do
        local i = string.find(path, v, 1, true) -- todo start with?
        if i then
          return name, v
        end
      end
    end
  end
  return nil
end

-- start - filter default is src/ for test dir inside modules
function PhpUnit:getTestDirs(start)
  start = start or 'src/'
  if self.testsuites then
    local t = {}
    for _, testsuite in pairs(self.testsuites) do
      for _, tpath in pairs(testsuite) do
        local s, e = string.find(tpath, start, 1, true)
        if s == 1 and e == #start then -- startwith
          table.insert(t, tpath)
        end
      end
    end
    return t
  end
  return nil
end

--------------------------------------------------------------------------------
--                              DEPRECATED

--
---@param test_path string
---@return string?
---@deprecated
function PhpUnit:get_src_path_for_test(test_path)
  -- is already with Test.php suffix
  if not test_path:match("Test.php$") then
    return test_path
  end

  local pattern = '([%w]+).([%w]+).([%w]+)' -- detect modular structure

  if test_path and self.testsuites then
    for _, testsuite in pairs(self.testsuites) do
      for _, path in pairs(testsuite) do
        local i = string.find(test_path, path, 1, true)
        if i then
          local src, module, test = (path or ''):match(pattern)
          if test == 'Test' and src ~= nil and module ~= nil then
            -- with dir slash  to prevent rename like '/a/PartMod/a' -> /a/Part
            local psrc = fs.ensure_dir(fs.join_path(src, module))
            path = fs.ensure_dir(path)
            local src_path = test_path:gsub(path, psrc):gsub("Test%.php", "%.php");
            return src_path
          end
        end
      end
    end
    -- TODO return default mappint tests/ -> src/
  end
  return nil; --not found
end

--   App\Auth\Test\Unit\Service\TokinizerTest -- true (Module Auth)
--   Test\Unit\Entity\TokinTest -- false
--
--   now used namespace_map from composer.json parsed by ComposerBuilder
--
---xdeprecated
function PhpUnit:is_module_test_class(class_name)
  local pattern = '([%w]+)\\([%w]+)\\([%w]+)\\[%w\\]*(Test)$'
  local root, module, test, suffix = (class_name or ''):match(pattern)
  return root == 'App' and test == 'Test' and module ~= nil and suffix ~= nil
end

class.build(PhpUnit)
return PhpUnit

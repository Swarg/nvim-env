-- 24-09-2023 @author Swarg
--
-- Goal: parse output from phpinfo
--
local class = require 'oop.class'

-- local su = require('env.sutil')
-- local fs = require('env.files')
-- local log = require('alogger')


class.package 'env.langs.php'
---@class env.langs.php.PhpInfoParser : oop.Object
---@filed handler function
---@filed prev_handler function?
---@filed section string?
local PhpInfoParser = class.new_class(nil, 'PhpInfoParser', {
  section = nil,
  prev_empty = nil,
  handler = nil,
  prev_handler = nil,
})

-- RequiredSections:
-- parse data from phpinfo output for this configuration sections:
local configuration_keys = {
  -- Core
  -- ctype
  -- curl
  -- Features
  -- date
  -- dom
  -- fileinfo
  -- filter
  -- ftp
  -- hash
  -- iconv
  -- json
  -- libxml
  -- mbstring
  -- mysqlnd
  -- openssl
  -- pcre
  -- PDO
  -- pdo_sqlite
  -- Phar
  -- posix
  -- readline
  -- Reflection
  -- session
  -- SimpleXML
  -- sodium
  -- SPL
  -- sqlite3
  -- standard
  -- tokenizer
  ['xdebug'] = true,
  -- xml
  -- xmlreader
  -- xmlwriter
  -- zlib
  -- Additional Modules
  -- Module Name
  ['Environment'] = true,
  ['PHP Variables'] = true,
  -- PHP License
}

function PhpInfoParser:isRequiredSection()
  return self.section and configuration_keys[self.section]
end

--------------------------------------------------------------------------------
--                            Parsing
--------------------------------------------------------------------------------

--
---@param phpinfo_output string - raw output
---@param handler function? default is phpinfo_handler(start point to parse)
---@return table with parsed PhpInfo
---@param section string? default is nill
function PhpInfoParser:parse(phpinfo_output, handler, section)
  self.handler = handler or PhpInfoParser.phpinfo_handler -- 'phpinfo()'
  self.section = section
  self.t = { configs = {}, phpinfo = {} }

  for line in phpinfo_output:gmatch('([^\n]+)') do
    -- line == nil or line == '' not possible here! see tests
    if not self:handle(line) then break end
  end

  return self.t
end

function PhpInfoParser:handle(line)
  if not self.handler then
    return false
  end
  self:handler(line)
  return true;
end

local ADDITIONAL_INI_FILES_PARSED = 'Additional .ini files parsed'

-- mappings for short names from phpinfo ouput
local phpinfo_keys = {
  ['PHP Version'] = 'php_ver',
  ['Configuration File (php.ini) Path'] = 'conf',
  ['Loaded Configuration File'] = 'loaded_conf',
  [ADDITIONAL_INI_FILES_PARSED] = 'ini_files',
  ['PHP API'] = 'php_api',
  ['Debug Build'] = 'debug_build',
}

-- common: split line to key-value
-- "Debugger => enabled" --> key:"Debugger", value: "enabled"
function PhpInfoParser.splitKeyValue(line, sep)
  sep = sep or ' =>'
  if line and sep then
    local i = string.find(line, sep, 1, true)
    if i then
      return line:sub(1, i - 1), line:sub(i + #sep + 1)
    end
  end
  return line, nil
end

-- for Section 'PHP Variables'
-- $_SERVER['XDEBUG_TRIGGER'] => 1
---@return string|false, string, string?  -- array_name|false, key, value
function PhpInfoParser.parsePhpVariable(line, sep)
  sep = sep or ' =>'
  if line and sep then
    local i = string.find(line, sep, 1, true)
    if i then
      -- in array key
      local j = string.find(line, "['", 1, true)
      local s, ak, array_name = 1, 0, false

      if j then
        array_name = line:sub(1, j - 1)
        ak = 2
        s = j + ak -- ['
      end
      -- array_name, in_array_key|key, value
      return array_name, line:sub(s, i - 1 - ak), line:sub(i + #sep + 1)
    end
  end
  return false, line, nil
end

-- Directive => Local Value => Master Value
-- "Master Value" (from php.ini) could be overridden with "Local Value"
--
-- master is either the value compiled into PHP, or set via a main php.ini
-- directive. I.e., the value that's in effect when PHP fires up,
-- before it executes any of your code.

-- local is the value that's currently in effect at the moment you call phpinfo()
-- This local value is the end result of any overrides that have taken place
-- via ini_set() calls, php_value directives in httpd.conf/.htaccess, etc.
--
---@param line string
---@return string, string?, string?
function PhpInfoParser.parseDirectiveLocalMasterValue(line, sep)
  sep = sep or ' =>'
  if line and sep then
    local i = string.find(line, sep, 1, true)
    if i then
      local j = line:find(sep, i + #sep, true)
      if j then
        local directive = line:sub(1, i - 1)
        local loc = line:sub(i + #sep + 1, j - 1)
        local master = line:sub(j + #sep + 1)
        return directive, loc, master
      else
        return line:sub(1, i - 1), line:sub(i + #sep + 1), nil
      end
    end
  end
  return line, nil, nil
end

-- grep only keys specified at the phpinfo_keys table
--
--  - collect data into t.phpinfo
--  - switch to configuration_hdlr
--
---@param line string
function PhpInfoParser:phpinfo_handler(line)
  local t = self.t
  local k, v = PhpInfoParser.splitKeyValue(line)
  if k and v then
    local sk = phpinfo_keys[k]
    if sk then
      if k == ADDITIONAL_INI_FILES_PARSED and v:sub(-1, -1) == ',' then
        v = { v:sub(1, #v - 1) }
        self.multiline = v
        self:switchHandlerTo(PhpInfoParser.multiline_handler)
      end
      t.phpinfo[sk] = v
      -- else print('[\'' .. k .. '\'] = \'\',') -- DEBUG
    end
  elseif k == 'Configuration' then
    -- switch to an another handler
    self:switchHandlerTo(PhpInfoParser.configuration_handler)
  end
end

function PhpInfoParser:switchSection(name)
  if name and configuration_keys[name] then -- isRequiredSection
    if not self.t.configs[name] then
      self.t.configs[name] = {}             -- init
    end
  end
  self.section = name
  self.subtable = nil
end

function PhpInfoParser:switchHandlerTo(handler)
  self.prev_handler = self.handler
  self.handler = handler
end

function PhpInfoParser:switchHandlerBack()
  self.handler = self.prev_handler
  self.prev_handler = nil
end

-- parse multilines into one value-array
-- for "Additional .ini files parsed => ..,\n..,\n..\n"
function PhpInfoParser:multiline_handler(line)
  if line:sub(-1, -1) == ',' then
    line = line:sub(1, #line - 1)
  else
    self:switchHandlerBack()
  end
  table.insert(self.multiline, line)
end

-- Collect all key-value pair from Configuration section for names specified
-- at the configuration_keys table
--
-- collect data into t.configs[srction_name]
--
---@param line string
function PhpInfoParser:configuration_handler(line)
  local k, v = PhpInfoParser.splitKeyValue(line)
  if k and v then
    if self:isRequiredSection() then
      self:getSectionTable()[k] = v
    end
  elseif k and k ~= '' and #k < 32 then
    -- print(k) -- DEBUG for print all sections names
    self:switchSection(k)

    if self.section == 'PHP Variables' then
      self:switchHandlerTo(PhpInfoParser.php_variables_handler)
    elseif self.section == 'xdebug' then
      self:switchHandlerTo(PhpInfoParser.xdebug_handler)
    end
  end
end

function PhpInfoParser:xdebug_handler(line)
  local start = line:sub(1, 2)
  -- colored logo -- ignore except version
  if start == '\27[' then
    if self:isRequiredSection() then
      --["\27[0mVersion"] = "3.2.2", k = k:match('\27%[%d%a(.+)') or '?'
      local i = string.find(line, 'Version =>', 1, true)
      if i then
        local _, v = PhpInfoParser.splitKeyValue(line)
        self:getSectionTable()['Version'] = v
      end
    end
    return
  end

  local k, lv, mv = PhpInfoParser.parseDirectiveLocalMasterValue(line)
  if k and lv then
    if self:isRequiredSection() then
      if k == 'Feature' and lv == 'Enabled/Disabled' then
        self.subtable = 'features'
        return
      elseif k == 'Directive' and lv == 'Local Value' and mv == 'Master Value' then
        self.subtable = nil
        return
      end
      self:getSectionTable()[k] = lv
    end
  elseif k and k ~= '' and #k < 32 and #k > 2 then
    self:switchSection(k)
    self:switchHandlerBack()
  end
end

function PhpInfoParser:getSectionTable()
  local section_name = self.section or 'unnamed'
  if not self.t.configs[section_name] then
    self.t.configs[section_name] = {}
  end
  local sec = self.t.configs[section_name]
  if self.subtable then
    if not sec[self.subtable] then
      sec[self.subtable] = {}
    end
    sec = sec[self.subtable]
  end
  return sec
end

-- for Section 'PHP Variables'
function PhpInfoParser:php_variables_handler(line)
  local array_name, k, v = PhpInfoParser.parsePhpVariable(line)
  if k and v then
    if self:isRequiredSection() then
      local sec = self:getSectionTable()
      -- case:  assign-to-array-entry  i.e $_SERVER['PWD'] => value
      -- array_name - name of array i.e. $_SERVER
      -- k - in-array-element-name  i.e. PWD
      -- v - value to assign
      if array_name then
        if not sec[array_name] then
          sec[array_name] = {}
        end
        sec = sec[array_name]
      end
      -- assigned value is array (multiline string)
      if v == 'Array' then
        self.cur_array = {} -- fill in the next parsed lines
        sec[k] = self.cur_array
        self:switchHandlerTo(PhpInfoParser.php_array_handler)
      else
        sec[k] = v
      end
    end
  elseif k and k ~= '' and #k < 32 and #k > 2 then
    -- print('@@@@ Section name:', k) DEBUG to print all sections names
    self:switchSection(k)
    self:switchHandlerBack()
  end
end

-- used for section "PHP Variables" $_SERVER.argv
-- parse multiline php-array definition into one table(array)
--
-- self.cur_array - body of array
-- self.cur_array_state - helper for multiline handling
--
function PhpInfoParser:php_array_handler(line)
  if self.cur_array then
    if self.cur_array_state then
      if self.cur_array_state == 1 then
        if line == ')' then
          self.cur_array = nil
          self.cur_array_state = nil
          self:switchHandlerBack()
        else
          table.insert(self.cur_array, line)
        end
      end
    elseif line == '(' then
      self.cur_array_state = 1 -- opened
    end
  end
end

class.build(PhpInfoParser)
return PhpInfoParser

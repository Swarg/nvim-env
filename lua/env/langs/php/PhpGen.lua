--- diagnostic disable: unused-local
-- 21-09-2023 @author Swarg
local log = require('alogger')
local class = require 'oop.class'

local lapi_consts = require 'env.lang.api.constants'
local AccessModifiers = require("env.lang.oop.AccessModifiers")
local LangGen = require("env.lang.oop.LangGen")
local ComponentGen = require("env.lang.oop.ComponentGen")
local Docblock = require("env.lang.oop.Docblock")
local ClassInfo = require("env.lang.ClassInfo")


class.package 'env.langs.php'
---@class env.langs.php.PhpGen : env.lang.oop.LangGen   @define class PhpGen extends LangGen
---@field new fun(self, o:table): env.langs.php.PhpGen
---@field lang env.langs.php.PhpLang?
---@field ext string
---@field tab string
---@field default_field_type string
---@field default_field_amod number  -- access modifier (private|public, etc)
local PhpGen = class.new_class(LangGen, 'PhpGen', {
  lang = nil,
  ext = 'php', -- extension of source files
  test = 'tests',
  default_field_type = 'string',
  default_field_amod = AccessModifiers.ID.PRIVATE,

  cmdhandler_new_stuff = "env.langs.php.command.new_stuff", -- :EnvNew ..
})

local LEXEME_TYPE = lapi_consts.LEXEME_TYPE


--
-- take classname from current line with use(import)(from sourcefile via editor)
function PhpGen:getClassNameFromLine()
  local line = self:getContext().current_line
  if line then
    -- use(import)
    local classname = line:match('^s*use ([^;]+);')

    return classname
  end
end

--
---@param classname string
function PhpGen:addCoversToDocblock(classname)
  log.debug("addCoversToDocBlock %s", classname)
  if not classname then
    return
  end
  local ctx, ci = self:getContext(true), nil
  ci = self.lang:resolveClass(ctx.bufname, ClassInfo.CT.CURRENT):getClassInfo()

  -- full namespace
  local nssep = self:getPkgDot()                                 -- in normal langs is a simple dot char: '.'
  if classname:sub(1, 1) ~= nssep and classname:find(nssep) then --
    classname = nssep .. classname
  end
  if ci then
    local line = '@covers ' .. tostring(classname)
    self:getDocblock(ci, Docblock.ID.CLASSS, ci:getShortClassName())
        :addLine(line)
  end
end

-- local template_docsblock = [[
-- /**
--  *
--  */
-- ]]

local template_method = [[
    ${ACCESSMOD} function ${NAME}(${PARAMS})${RETURN_PREF}${RETURN_TYPE}
    {
${BODY}
    }
]]

local template_constructor = [[
    ${ACCESSMOD} function __construct(${PARAMS})
    {
${BODY}
    }
]]

local template_field = [[    ${ACCESSMOD} ${TYPE} ${NAME}${DEFAULT_VALUE};]]
-- local template_asign_to_self_field = [[${THIS} ${EQUALS} ${VALUE};]]

local def_phpunit_test_imports = [[
use PHPUnit\Framework\TestCase;
]]


local template_class = [[
<?php

declare(strict_types=1);

namespace ${PACKAGE};

${IMPORTS}
/**
 *${COVERS}
 *${DATE}
 *${AUTHOR}
 */
class ${CLASS}${EXTENDS}
{
${FIELDS}
${METHODS}
}
]]

-- override
function PhpGen:getClassTemplate() return template_class end

function PhpGen:getMethodTemplate() return template_method end

---@diagnostic disable-next-line: unused-local
function PhpGen:getConstructorName(classinfo) return '__construct' end

---@return string
function PhpGen:getFieldTemplate() return template_field end

---@return string, string template + separator
function PhpGen:getParamTemplate() return '${TYPE} ${NAME}', ',' end

function PhpGen:getKWordThis() return '$this' end

function PhpGen:getKWordBoolean() return 'bool' end

---@return string
function PhpGen:getVarPrefix() return '$' end

---@return string
function PhpGen:getReturnPrefix() return ': ' end

---@return string
function PhpGen:getAttrDot() return '->' end

function PhpGen:getPkgDot() return '\\' end

-- https://www.php.net/manual/en/language.operators.comparison.php
---@return string
function PhpGen:getOpCompareIdentical() return '===' end

---@return string
function PhpGen:getConstructorTemplate() return template_constructor end

---@return string
function PhpGen:getGetterTemplate() return '' end

---@return string
function PhpGen:getSetterTemplate() return '' end

---@return string
function PhpGen:getEqualsTemplate() return '' end

---@return string
function PhpGen:getDefaultFieldType() return self.default_field_type end

---@return number
function PhpGen:getDefaultFieldAMod() return self.default_field_amod end

--------------------------------------------------------------------------------



function PhpGen:genOptsForTestFile(tci, opts)
  log.debug("createOptsForTestFile")
  assert(tci ~= nil)

  local sci       = tci:getPair()

  opts            = opts or {}
  opts.test_class = true
  opts.extends    = "TestCase" -- phpunit

  opts.imports    = 'use ' .. sci:getClassName() .. ";\n"
      .. def_phpunit_test_imports

  local tcn_short = sci:getShortClassName() or ''
  opts.covers     = tcn_short

  -- method name in source file under the cursor
  local context   = self:getContext(true)
  local method    = context:element(LEXEME_TYPE.FUNCTION)
  if method then
    log.debug("has method under the cursor:", method)
  end
  local tmn = ComponentGen.getNameWithPreffix('test', method or 'Success')

  local obj_vn = ClassInfo.getVarNameForClassName(tcn_short, '$')

  local t = self:getTab()
  local body = string.format("%s%s%s = new %s();\n" ..
    "%s%sself::assertEquals(1, %s);",
    t, t, obj_vn, tcn_short,
    t, t, obj_vn)
  self:getEditor():setSearchWord(tmn) -- to jump to a generated test method name

  local params = false
  local methodGen = self:getMethodGen(tci)
  opts.methods = { methodGen:genTest(tmn, params, body) }
  return opts
end

-- TODO
--     if opts.constructor then
--       local param = '$' .. f
--       if opts.str_lower and type == 'string' then
--         param = 'mb_strtolower(' .. param .. ')'
--       end

--------------------------------------------------------------------------------
-- simple test without any testframework based on one file test/ATestTool.php
local test_file_pattern = [[
<?php

require_once 'test/ATestTool.php';
require_once '$MODULE';

function testX() {
  echo __FUNCTION__;
  assertEquals(1, 2, 'msg');
  fin();
}

testX();
]]

-- Generate body of test file for given context and test file(path)
---@param tci env.lang.ClassInfo
function PhpGen.genSimpleATestToolBody(tci)
  if tci and tci:getPath() then
    local module = tci:getInnerPath() .. ".php"; -- path for require
    if module then
      module = module:gsub('Test$', '')          -- replace Test-Suffix
    end
    local body = test_file_pattern:gsub('$MODULE', module)
    return body
  end
end

class.build(PhpGen)
return PhpGen

-- 15-03-2024 @author Swarg
-- snippets for EnvLineInsert for Php
--
--
--
--

local M = {}

local insert_items = {
  '${DATE} ${TIME}',
  '\n        /** @var string|false|null $${WORD} */\n',
  '\n/**\n *\n */\n', -- comment
  '\n/**\n *\n * ${DATE}\n * @author ${AUTHOR} \n */\n',
  '\nrequire_once "${MODULE}";',
  '\nclass ${WORD}\n{\n    public function __construct()\n    {\n    }\n}',
  '\n    private ${PREV_WORD} $${WORD};\n        $this->${WORD} = ${WORD};\n',
  '\n    public function get${WORD}(): ${PREV_WORD}\n    {\n        return $this->${WORD};\n    }\n',
  'echo \'[DEBUG] ${WORD}: \'; print_r($${WORD}); echo "\\n";\n',
  'echo "[DEBUG] Try to call [${WORD}]\\n";\n',
  '\nfunction ${WORD}() {\n\n}\n\n',
  '\nfunction test_${WORD}() {\n\n}\n\n',
  '\n${WORD}();\n',
  '\n        $this->expectException(InvalidArgumentException::class);\n',
  '\n        $this->expectExceptionMessage(\'\');\n',
  'dd($${WORD});\n',
  'var_dump($${WORD});\n',
  'Assert::notEmpty($${WORD});\n',
  'mb_strtolower(${WORD})',
  "${TOGGLE_BOOL}",
}

local insert_mappings = {
}

local insert_handlers = {
}

---@param reg function
function M.add(reg)
  reg('php', insert_items, insert_mappings, insert_handlers)
end

return M

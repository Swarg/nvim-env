-- 25-09-2023 @author Swarg
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local spawner = require('env.spawner')
local log = require('alogger')

local E = {}
local R = require 'env.require_util'
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local cjson = R.require("cjson", vim, "json")        -- vim.json

-- local Lang = require('env.lang.Lang')
-- local ComposerBuilder = require('env.langs.php.ComposerBuilder')
-- local DockerCompose = require('env.lang.DockerCompose')

local NAMESPACE = 1 -- default for using in tests
if vim and vim.api and vim.api.nvim_create_namespace then
  NAMESPACE = vim.api.nvim_create_namespace("NVIM_ENV_PSALM_N")
end

-- TODO configurable
local attributes = {
  severity = "severity",
  lnum     = "line_from", -- was 'row' now not works
  end_lnum = "line_to",   -- end_row
  col      = "column_from",
  end_col  = "column_to",
  code     = "shortcode",
}

class.package 'env.langs.php'
---@class env.langs.php.Psalm: oop.Object
---@field lang env.langs.php.PhpLang
---@field executable string
---@field args table
---@field envs table?
---@field output_raw string?
---@field output_json table?
---@field rawDiagnostics table?     raw json from psalm
---@field diagnostics table?        cooked for nvim by bufnr
local Psalm = class.new_class(nil, 'Psalm', {
  lang = nil, -- required
  executable = 'vendor/bin/psalm',
  args = { "--output-format=json", "--no-progress" },
})


---@param w Cmd4Lua
function Psalm:handle(w)
  log.debug('Psalm action')
  w:about("Psalm - a static analysis tool for PHP")
      :handlers(Psalm, self)

      :desc('Check all files and show diagnostics')
      :cmd('check-all', 'ch')

      :desc('redrawOldDiagnostics')
      :cmd('redraw', 'rd')
      :desc('add suppress annotation for psalm warning error in the current line')
      :cmd('suppress', 'su')

      :desc('clear all diagnostics')
      :cmd('clear-all', 'ca')

      :desc('clear diagnostics in the current buffer')
      :cmd('clear')

      :run()
end

-- checkAllFilesAndShowDiagnostics
function Psalm:cmdCheckAll()
  log.debug('checkAllFilesAndShowDiagnostics')
  assert(self.lang)

  self:cmdClearAll()
  self:bindConfig()
  self:fetchAllFromTool(function(obj)
    Psalm.parseOutput(obj)
    Psalm.cookDiagnosticsForVimByBufnr(obj)
    Psalm.showDiagnostics(obj)
  end)
end

function Psalm:bindConfig()
  self.lang:getStorage().psalm = self
  local dc = self.lang:getDockerCompose()
  if dc then
    self.serviceName = self.lang:getDockerComposeServiceName()
    self.docker_volumes = dc:getVolumesPathMappings(self.serviceName)
    self.useDocker = self.serviceName ~= nil
    log.debug('Use Docker: %s, volumes: %s',
      self.useDocker, su.table_size(self.docker_volumes))
  end
end

--
---@param callback function (self)
function Psalm:fetchAllFromTool(callback)
  log.debug("fetchAllFromTool")
  assert((self.lang or E).project_root, 'The ProjectRoot must be for specified')
  assert(type(callback) == 'function', 'the callback must be a function')

  local cmd, args, envs = self.executable, self.args, nil
  cmd, args, envs = self.lang:wrapToDockerIfConfigured(cmd, args, self.envs)

  -- closure
  ---@diagnostic disable-next-line: unused-local
  local ondone_callback = function(output, pp, exit_ok, signal)
    log.debug("ondone_callback output len: ", #(output or ''))
    local obj = self -- pass value into closure
    obj.output_raw = output
    callback(obj)
  end
  args, envs = args or {}, envs or {}
  local pp =
      spawner.run_async(self.lang.project_root, cmd, args, envs, ondone_callback)
  self:nofityIfSlow(pp.cmdline)
end

function Psalm:nofityIfSlow(msg, hlgroup)
  if self.useDocker then
    hlgroup = hlgroup or 'Function'
    local editor = self.lang.editor
    if editor and msg then
      editor:echoInStatus(msg, hlgroup)
    end
  end
end

-- parse all psalm output into one heap
-- For multiple files it will need to separate them by numbers
function Psalm:parseOutput()
  log.debug("parseOutput")
  assert(self.output_raw, 'the raw output from psalm reguired')
  local ok, json = pcall(cjson.decode, self.output_raw)
  if not ok then
    print('Cannot parse json responce: ' .. tostring(json))
    return
  end

  self.rawDiagnostics = json -- for later reuse via cmds

  -- apply PathMappings docker->machine
  local diagnostics, cnt, no_buf = {}, 0, 0
  for _, json_diagnostic in pairs(json) do
    local file_path = json_diagnostic.file_path
    if self.useDocker then
      file_path = self:applyPathMappings(file_path)
    end
    local bufnr = self.findBufNum(file_path)
    -- print("[DEBUG] file_path:", file_path, bufnr)
    if bufnr then
      local diagnostic = Psalm:cookDiagnosticEntryForVim(json_diagnostic)
      diagnostic.bufnr = bufnr
      table.insert(diagnostics, diagnostic)
      cnt = cnt + 1
    else
      no_buf = no_buf + 1
    end
  end

  self.diagnostics = diagnostics
  log.debug('diagnostics cooked: %s no-bufs:%s', cnt, no_buf)
  return self.diagnostics -- multiples-files in one big table
end

-- split diagnostic of multiple-files by bufnr and clear from the old buffs
---@return table
function Psalm:cookDiagnosticsForVimByBufnr()
  log.debug("cookDiagnosticsForVimByBufnr")
  local by_bufnr = {}

  if self.diagnostics then
    for _, diagnostic in ipairs(self.diagnostics) do
      if not diagnostic.bufnr then
        log.debug("received multiple-file diagnostic without bufnr: %s", diagnostic)
      else
        by_bufnr[diagnostic.bufnr] = by_bufnr[diagnostic.bufnr] or {}
        table.insert(by_bufnr[diagnostic.bufnr], diagnostic)
      end
    end
  end

  -- clear stale diagnostics
  for _, old_diagnostic in ipairs(Psalm.getDiagnostics(NAMESPACE)) do
    by_bufnr[old_diagnostic.bufnr] = by_bufnr[old_diagnostic.bufnr] or {}
  end

  self.diagnostics = by_bufnr
  self:nofityIfSlow('Diagnostics Updated.')
  return self.diagnostics -- many tables with keys:bufnr -> values:diagnostics
end

function Psalm:showDiagnostics()
  log.debug("showDiagnostics")
  if self.diagnostics then
    local i = 0
    for bufnr, bufnr_diagnostics in pairs(self.diagnostics) do
      vim.diagnostic.set(NAMESPACE, bufnr, bufnr_diagnostics)
      i = i + 1
    end
    log.debug('a number of diagnostics passed to nvim %s', i)
  end
end

function Psalm.getDiagnostics(anamespace)
  if vim and vim.diagnostic then
    return vim.diagnostic.get(nil, { namespace = anamespace }) or {}
  end
  return {}
end

--

---@param file_path string
---@return string?
function Psalm:applyPathMappings(file_path)
  assert(file_path, 'A file_path must specified')

  if self.docker_volumes then
    for cp, mp in pairs(self.docker_volumes) do
      if file_path:find(cp, 1, true) == 1 and
          file_path:sub(#cp + 1, #cp + 1) == '/' then -- check '/'
        return fs.join_path(mp .. file_path:sub(#cp + 1))
      end
    end
  end
  return nil
end

function Psalm.findBufNum(file_path)
  if vim and vim.fn then
    assert(fs.file_exists(file_path), file_path)
    return vim.fn.bufadd(file_path)
  end
  return 1
end

-- convert json-diagnostic from psalm into nvim format
---@return table nvim-diagnostic
function Psalm:cookDiagnosticEntryForVim(json_diagnostic)
  local e = {}
  e.namespace = NAMESPACE
  -- e.source = 'psalm'
  for attr, json_key in pairs(attributes) do
    if json_diagnostic[json_key] ~= cjson.null then --vimNIL then
      e[attr] = json_diagnostic[json_key]
    end
  end
  if e.lnum then e.lnum = e.lnum - 1 end
  if e.end_lnum then e.end_lnum = e.end_lnum - 1 end
  if e.col then e.col = e.col - 1 end
  if e.end_col then e.end_col = e.end_col - 1 end

  e.severity = 1 -- error todo
  e.message = self:buildDiagnosticMessage(json_diagnostic)
  return e
end

function Psalm.foldMessage(message)
  if message and #message > 80 then
    local i = string.find(message, ', but', 60, true)
    i = i or string.find(message, ', expecting', 60, true)
    if i then
      message = message:sub(1, i) .. "\n" .. message:sub(i + 2)
    end
  end
  return message
end

---@param jd table -- json_diagnostic
function Psalm:buildDiagnosticMessage(jd)
  local message, link, details = Psalm.foldMessage(jd.message), '', ''
  if jd.link then link = jd.link end

  if jd.snippet and jd.snippet ~= cjson.null then
    details = "\n" .. jd.snippet
    if jd.selected_text then
      local i = jd.snippet:find(jd.selected_text, 1, true)
      local visual = ''
      if i then
        visual = su.dup('-', i - 1) .. su.dup('^', #jd.selected_text)
      else
        visual = jd.selected_text
      end
      details = details .. "\n" .. visual
    end
  end

  if jd.other_references and jd.other_references ~= cjson.null then
    -- if jd.other_references and type(jd.other_references) == table then
    local refs, s = jd.other_references, ''
    for _, ref in pairs(refs) do
      s = s ..
          "\n" .. (ref.label or '') ..
          "\n" .. (ref.file_name or '?') .. ':' .. (ref.line_from or '?') ..
          "\n" .. (ref.snippet or '?')
    end
    details = details .. "\nOther References:" .. s
  end

  return string.format("[%s:%s] %s\n\n%s\n%s",
    jd.shortcode, jd.type, link, -- "shortcode": 30, "link": "https://psalm.dev/030",
    message,
    details                      -- snippet of wrong code + other_references
  )
end

-- function Psalm:redrawOldDiagnostics()
function Psalm:cmdRedraw()
  log.debug("redrawOldDiagnostics")
  local prev = (self.lang:getStorage().psalm or {})
  if prev then
    self.rawDiagnostics = prev.rawDiagnostics -- raw Json from psalm
    self.diagnostics = prev.diagnostics       -- cooked
    -- todo pass configh
    self.lang:getStorage().psalm = self
    self:showDiagnostics()
  else
    log.debug('Not Found a prev psalm instance in lang')
  end
end

-- function Psalm:clearDiagnosticsAll()
function Psalm:cmdClearAll()
  log.debug("clearDiagnosticsAll")
  for _, old_diagnostic in ipairs(Psalm.getDiagnostics(NAMESPACE)) do
    vim.diagnostic.set(NAMESPACE, old_diagnostic.bufnr, {})
  end
end

-- function Psalm.clearDiagnosticsCurrBuf()
function Psalm.cmdClear()
  log.debug("clearDiagnosticsCurrBuf")
  vim.diagnostic.set(NAMESPACE, 0, {})
end

-- from list with all diagnostics for own namespace filter only with given bufnr
---@param bufnr number
---@return table?
function Psalm:getDiagnosticsForBuf(bufnr)
  if type(bufnr) == 'number' then
    local list = self.getDiagnostics(NAMESPACE) -- all in one table as list
    local t = {}
    for _, d in ipairs(list) do
      if d.bufnr == bufnr then
        table.insert(t, d)
      end
    end
    return t
  end
  return nil
end

-- find all diagnostics for given line number in a list of all diagnostics for
-- one bufnr (filtered by getDiagnosticsForBuf)
---@param linenr number
---@param col number?
---@return table?
function Psalm:getDiagnosticForLine(diagnostics, linenr, col)
  if diagnostics and type(linenr) == 'number' then
    linenr = linenr - 1 -- in list bufnr starts from 0 not from 1
    local msg = nil
    for _, d in ipairs(diagnostics) do
      if linenr >= d.lnum and linenr <= d.end_lnum then
        log.debug('Line number matched')
        if col >= d.col and col <= d.end_col then
          return d
        else
          msg = 'Cannot find the error for this place. ' ..
              'Place the cursor on the error.'
        end
      end
    end
    if msg then print(msg) end
  end
  return nil
end

-- add suppress annotation for psalm warning error in the current line
-- function Psalm:addSuppressAnnotation()
function Psalm:cmdSuppress()
  log.debug("addSuppressAnnotation")
  local c = self.lang:getEditor():getContext()
  if c then
    local dlist = self:getDiagnosticsForBuf(c.bufnr)
    local d = self:getDiagnosticForLine(dlist, c.cursor_row, c.cursor_col)
    if d then
      local err = string.match(d.message, '%[[%d]+:([%w]+)%]', 1)
      if err then
        local tab = (c.current_line or ''):match('^[%s]+') or '    '
        local line = tab .. '/** @psalm-suppress ' .. err .. ' */'
        if self.lang.editor:insertLineBefore(line, c.cursor_row, c.bufnr) then
          ---@cast dlist table
          self:updateDiagnosticsAndRedraw(c.bufnr, dlist, c.cursor_row)
        end
      end
    else
      log.debug("Not Found %s", d)
    end
  end
end

-- update lnum fow all diagnostics for given bufnr that placed below linenr
---@param diagnostics table
function Psalm:updateDiagnosticsAndRedraw(bufnr, diagnostics, linenr)
  local updated = false
  linenr = linenr - 1
  for _, d in ipairs(diagnostics) do
    if linenr <= d.lnum then
      d.lnum = d.lnum + 1
      d.end_lnum = d.end_lnum + 1
      updated = true
    end
  end
  if updated then
    vim.diagnostic.set(NAMESPACE, bufnr, diagnostics)
  end
end

class.build(Psalm)
return Psalm

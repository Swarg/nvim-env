---@diagnostic disable: unused-local
-- 23-09-2023 @author Swarg
-- Goal:
--  - Validate a Debugger Configuration for a given project.
--    Supports:
--     - Xdebug
--
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')
local uos = require('env.util.os')
local spawner = require('env.spawner')

local R = require 'env.require_util'
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local Object = require("oop.Object")
local Lang = require('env.lang.Lang')
local ClassInfo = require("env.lang.ClassInfo")
local PhpInfoParser = require 'env.langs.php.PhpInfoParser'
local DockerCompose = require('env.lang.DockerCompose')

class.package 'env.langs.php'
---@class env.langs.php.PhpDebug : oop.Object
---@field lang env.langs.php.PhpLang
---@field classinfo env.lang.ClassInfo?
---@field project_root string?
---@field report env.lang.Report
---@field verbose_lvl number
---@field errors table
---@field phpinfo string?      - raw ouput from 'php -i' (phpinfo)
---@field info table?          - parsed date from 'php -i' (phpinfo)
---@field dockerCompose env.lang.DockerCompose?
---@field debugFromDocker boolean
---@field service_name string? - from docker compose conf
---@field docker_volumes table? - from docker compose conf
--                                (in_container_path -> realpath)
local PhpDebug = class.new_class(nil, 'PhpDebug', {
  lang = nil,      -- required to be passed through the constructor
  report = nil,    -- it is also required
  verbose_lvl = 0, -- debug
  errors = {},
  debugFromDocker = false,
})

local E = {}

PhpDebug.EV_TRIGGER_K = "XDEBUG_TRIGGER"
PhpDebug.EV_TRIGGER_V = "1"
PhpDebug.EV_CHECKER_K = "NVIM_ENV"
PhpDebug.EV_CHECKER_V = 'VALIDATE_DEBUGGER'

local WARN_NO_RELEVANT_DAP_LAUNCH_SETTINGS = [[
--------------------------------------------------------------------------------
[WARN] This means that there is no working dap setting to launch a debug session
[WARN] for the current project. In order to be able to debug the project,
[WARN] you need to configure the nvim-dap plugin
--------------------------------------------------------------------------------
]]

-- Lookup and Validate Debugger Configuration
---@param classinfo env.lang.ClassInfo
function PhpDebug:validateConfig(classinfo)
  assert(self.lang)
  assert(self.report)

  self.classinfo = classinfo
  -- in fact, the lang-object can describe a subproject
  self.project_root = self.lang:getProjectRoot()
  self:validateDockerConfig()
  self:fetchAndParsePhpInfo(function(obj)
    PhpDebug.validatePhpInfo(obj)
    PhpDebug.validateNvimPlugins(obj)
    -- TODO
    -- ping host from within docker
    -- self:validatePathMappingsInDocker()
    -- showErrors
  end)
end

--
function PhpDebug:validatePhpInfo()
  self:verbose(1, inspect(self.info)) -- parserd
  self:add('PhpInfo Validation...')
  -- pass EnvVar
  self:chk('Environmen Variable Passed: ', self:checkEnvVarPassed())
  self:validateXdebugInstallation()
  -- TODO
  -- check connection DAP->debug-listener->Xdebug
  --  - ping local host from docker-container if used
  -- check is listener already runned if no - open 9003 port and try trigger
  -- debugging for any line from given classinfo
  self:validatePhpState()
end

function PhpDebug:validatePhpState()
  if not self:ok('PHP Configuration', self:hasPhpInfoConfiguration()) then
    return -- fail no parsed phpinfo output
  end
  local vars = self:getPhpInfoConfiguration()["PHP Variables"]
  if self:ok('PHP Variables', vars) then
    local server = vars['$_SERVER']
    if self:ok('$_SERVER', server) then
      local M = PhpDebug
      local envvars_passed_to_php =
          self:chk(M.EV_TRIGGER_K, server[M.EV_TRIGGER_K] == M.EV_TRIGGER_V)
          and
          self:chk(M.EV_CHECKER_K, server[M.EV_CHECKER_K] == M.EV_CHECKER_V)

      self:chk('EnvVar Passed to php-script', envvars_passed_to_php)
    end
  end
end

--
function PhpDebug:validateNvimPlugins()
  self:add('Validate Nvim Plugins')
  self:validateNvimDap()
  -- Check PathMappings
  self:add('vscode-php-debug') -- DAPServer for nvim-dap
end

function PhpDebug:validateNvimDap()
  -- Check Configuration for dap
  local dap_status_ok, dap = pcall(require, "dap")
  if self:chk('nvim-dap istalled', dap_status_ok) then
    self:validateNvimDapAdapter(dap)
    self:validateNvimDapConfiguration(dap)
  end
end

-- the adapter define the external program like vscode-php-debug extension
-- This program is a listener (on default 9003) port to which Xdebug itself
-- will connect. For nvim-dap(DapClient for nvim) this program is a DapServer
--
-- check is external program configured and available
--
---@param dap table
function PhpDebug:validateNvimDapAdapter(dap)
  local adapter = (dap.adapters or E).php

  if self:chk('nvim-dap php-adapter', adapter ~= nil) then
    local cmd = adapter.command
    local args = adapter.args
    self:chk('nvim-dap adapter cmd', uos.get_executable(cmd))
    -- todo extract to validateNodeInstallation
    if cmd == 'node' then
      local js = (args or E)[1] -- node path-to-js
      if self:chk('External DebugAdapter Path: ', js ~= nil) then
        self:chk(tostring(js), fs.file_exists(js))
      end
    end
  end
end

-- Validate NvimDapConfiguration for php
-- This Config define how to launch debugger
-- It is a list of all available configurations that used in chain
-- nvim-dap(DAP client) <-> vscode-php-debug(DapServer)
-- DapServer - is a listener (on default 9003 port) to which Xdebug itself
-- will connect
--
-- this settings can be loaded by :DapLoadLaunchJSON (.vscode/launch.json)
--
-- TODO autocreate this settings for current project based inproject configs
--
---@param dap table
function PhpDebug:validateNvimDapConfiguration(dap)
  local configuration = (dap.configurations or E).php
  if self:ok('nvim-dap: dap.configurations.php', configuration ~= nil) then
    if not self.xdebug_port then
      self:error('Cannot check dap-settings: uknown xdebug_port')
      return
    end
    self.dapValidLaunchSettings = {}
    self.dapBrokenLaunchSettings = {}
    self:hl()
    -- each setting is a description of how to launch the debug adapter
    for _, settings in pairs(configuration) do
      self:dapCheckLaunchSettings(settings)
    end
    self:hl()
    self:dapValidateTotalHealthy()
  end
end

-- setting is a description of how to launch the debug adapter (nvim-dap)
---@param settings table -- dap_launch_settings
function PhpDebug:dapCheckLaunchSettings(settings)
  assert(settings)
  local name = settings.name or 'unnamed'
  -- used in dap to get adaper: like dap.adapters[settings.type]
  if settings.type ~= 'php' then
    self:dapAddWrongLaunchSettings(name, 'Type', settings.type)
    return
  end
  -- first, if the ports are different, don't check further, discard and move on
  if settings.port ~= self.xdebug_port then
    self:dapAddWrongLaunchSettings(name, 'Port', settings.port)
    return
  end
  --
  local validPathMappings = {} -- than sure will be work correctly

  if self:dapValidateLaunchPathMappings(settings, validPathMappings) then
    self:dapReportLaunchSettings(settings, validPathMappings)
    self.dapValidLaunchSettings[name] = settings
  else
    self:dapAddWrongLaunchSettings(name, 'PathMappings', settings.pathMappings)
  end
end

function PhpDebug:dapAddWrongLaunchSettings(name, msg, value)
  self:add(' == [%s] Wrong %s %s', name, msg, value)
  table.insert(self.dapBrokenLaunchSettings, name)
end

function PhpDebug:dapReportLaunchSettings(settings, validPathMappings)
  self:add(' == [%s] ==', settings.name or 'unnamed')

  self:add('  port: %s ', settings.port) -- already checked

  if settings.localSourceRoot then
    self:add('  localSourceRoot: %s ', settings.localSourceRoot)
  end
  -- pathMappings is used when debugging is doing from inside the docker container
  if settings.pathMappings then
    local pathMappings = inspect(settings.pathMappings):gsub("\n", '')
    self:add('  PathMappingsDap: %s', pathMappings)
    self:add('  PathMappings Matched to docker-compose config: ')
    for container_path, dc_map in pairs(validPathMappings) do
      self:add('  %s -> %s', container_path, dc_map)
    end
  end
  if settings.cwd then self:add('  cwd: %s ', settings.cwd) end
  if settings.program then self:add('  program: %s ', settings.program) end
end

-- Path mappings used:
--  -- then debuggin app from inside docker container
--  if for current project found docker compose configuration and no app
-- and there is no corresponding configuration in the dap-conf for php - errror
--
-- this code used in cycle for check each settings in dap.configuration.php
--
-- This is the place where the self.pathMappingsDap is filled in
--
---@param settings table from a one dap config entry
function PhpDebug:dapValidateLaunchPathMappings(settings, outValidPathMappings)
  assert(settings)
  self:verbose(2, 'dapValidateLaunchPathMappings [%s]', settings.name)

  -- actual only for debug from inside docker-container
  if self.debugFromDocker and self.dockerCompose then
    -- case 1: used docker (needs mappings) - dap config no - error
    if not settings.pathMappings then -- dapLauncherSettings
      -- self:add('No PathMappings in dap-settings for debugging inside docker')
      return false
    end
    local matched_cnt = 0 -- count of matched pathMappings dap<->docker
    -- volumes: inside container -> realpath(machin)
    if self.docker_volumes then
      -- dap_mp is a mahcine path in-dap-settings-entry
      for container_path, dap_mp in pairs(settings.pathMappings) do
        container_path = fs.ensure_no_dir_slash(container_path)
        -- dc_mp is machinve path in docker-compose conf
        local dc_mp = self.docker_volumes[container_path]
        if self:matchPaths(dc_mp, dap_mp) then
          -- build a path matching "dap <-> docker-compose"
          outValidPathMappings[container_path] = dc_mp
          matched_cnt = matched_cnt + 1
        end
      end
      -- case 2: has dockercompose and no volumes is has configs in dap?
      -- check is missed docker-compose config and already corrent nvim-dap conf
    else
      -- Direct via COPY in Dockerfile
      -- TODO
    end

    return matched_cnt > 0 --?
  end
  -- another cases without docker?
  return true
end

-- summary about recognized relevant correct settings to launch debug sessions
-- for a current project
-- based on:
--  - xdebug port
--  - docker + pathMappings (when development goes via Docker)
--
function PhpDebug:dapValidateTotalHealthy()
  self:add('nvim-dap: dap.configurations.php : Summary:')
  self:add('Dap Launch Settings for php: ')

  local worked_sz = su.table_size(self.dapValidLaunchSettings)
  if self:chk('Correct Dap Launch Settings', worked_sz) then
    for name, setting in pairs(self.dapValidLaunchSettings) do
      self:add('  [OK] "%s"', name)
    end
  end

  local wrong_sz = su.table_size(self.dapBrokenLaunchSettings)
  self:add('Not Relevant Dap Launch Settings(for this project) [%s]: ', wrong_sz)
  for _, name in pairs(self.dapBrokenLaunchSettings) do
    self:add('  [--] "%s"', name)
  end

  if worked_sz <= 0 then
    self:add(WARN_NO_RELEVANT_DAP_LAUNCH_SETTINGS)
  end
end

--
-- if debugging goes from inside docker container then:
--
-- - check if the real path inside the container($_SERVER['pwd'])
--   matches to what is configured in the dockerfile or docker-compose.yml
--
-- function PhpDebug:validatePathMappingsInDocker()
--   if self.debugFromDocker then
--     local vars         = self:getPhpInfoConfiguration()["PHP Variables"]
--     local server       = vars['$_SERVER']
--     local pwd          = fs.ensure_no_dir_slash(server['PWD'])
--     local realpath_dc  = (self.docker_volumes or E)[pwd]
--     local realpath_dap = (self.dapValidPathMappings or E)[pwd]
--     local dc_ok        = self:chk('realpath dc', realpath_dc)
--     local dap_ok       = self:chk('realpath dap', realpath_dap)
--
--     if not dc_ok or not dap_ok then
--       self:add('actual docker volumes: ', self.docker_volumes)
--       self:add('valid nvim-dap settings: ', self.dapValidPathMappings)
--     end
--   end
-- end

--
-- auto replace ${workspaceFolder} to real path
--
---@param dc_path string   - path from dockercompose conf-file
---@param dap_path string  - path from dap.configuration.php for vscode-php-debug
---@return boolean
function PhpDebug:matchPaths(dc_path, dap_path)
  if dc_path and dap_path then
    -- is self.dockerCompose.workdir and self.dockerCompose.yml in proj_root?
    dap_path = dap_path:gsub('${workspaceFolder}', self.project_root)
    self:verbose(2, '  docker   path: %s', dc_path)
    self:verbose(2, '  nvim-dap path: %s', dap_path)
    return dc_path == dap_path
  end
  return false
end

-- check is docker used for development
-- check occurs by searching service-name for the current [sub]project
--
-- - parse a docker-compose settings for current self.service_name
--   - volumes  -- self.docker_volumes
--
function PhpDebug:validateDockerConfig()
  self:add('Checking is a docker being used...')
  self.debugFromDocker = false
  self.dockerCompose = self.lang:getDockerCompose()
  if not self.dockerCompose then
    return
  end
  self:add('Found Docker Compose configuration.')
  self.service_name = self.lang:getDockerComposeServiceName()
  if not self:chk('DockerComposeServiceName', self.service_name) then
    -- if the service name is not found, then we think that this project
    -- was not intended to be launched through a docker container.
    -- development without docker
    return
  end
  -- development with docker-compose
  -- debugging will take place from inside the docker container
  self.debugFromDocker = true
  self:updateProjectRootByDocker()

  -- for PathMappings checks
  -- determine how code maps within container and real mathine
  -- a first way - via volumes configured in docker-compose.yml (Always fresh)
  -- in this appropriate you doesn't need rebuild your docker-image to update
  -- a code changes
  self.docker_volumes = self.dockerCompose:getVolumesPathMappings(self.service_name)
  if not self.docker_volumes then
    self:add('[WARN] Not Found Volumes for service %s', self.service_name)
  end
  -- a second way - via COPY within Dockerfile (Rebuild for each code changes)
  -- TODO build mappings based COPY in Dockerfile
end

-- case: dir with docker-compose.yml in project root and prev self.project_root
-- taked from ClassInfo from subproject dir which is a root for php-composer
-- update self.project_root
function PhpDebug:updateProjectRootByDocker()
  local wd = self.dockerCompose.workdir
  if wd and wd ~= self.project_root and #wd < #self.project_root then
    self:add('Sync ProjectRoot to %s', wd)
    self.project_root = wd
  end
end

function PhpDebug:fetchAndParsePhpInfo(job_callback)
  self:add('Fetching a phpinfo...')
  local cmd, args, envs = self.lang.executable or 'php', { '-i' }, nil
  -- local repl_args = { '-r', 'print_r($_SERVER); xdebug_info();' }
  envs = {
    PhpDebug.EV_TRIGGER_K .. '=' .. PhpDebug.EV_TRIGGER_V,
    PhpDebug.EV_CHECKER_K .. '=' .. PhpDebug.EV_CHECKER_V
  }
  cmd, args, envs = self.lang:wrapToDockerIfConfigured(cmd, args, envs)

  -- closure
  local ondone_callback = function(output, pp, exit_ok, signal)
    local _self = self -- pass value into closure
    _self.phpinfo = output
    _self.info = PhpInfoParser:new():parse(output)
    _self:chk('phpinfo parsed', self.info ~= nil)
    job_callback(_self)
  end
  args = args or {}
  envs = envs or {}
  spawner.run_async(self.project_root, cmd, args, envs, ondone_callback)
end

-- is EnvVar(System Scoupe) passed into system with php(Into Docker Container)
function PhpDebug:checkEnvVarPassed()
  local envs = self:getPhpInfoConfiguration().Environment or E
  -- this EnvVar only for checking is EnvVars passed to the php
  return envs[PhpDebug.EV_CHECKER_K] == PhpDebug.EV_CHECKER_V and
      -- this EnvVar Trigger Xdebug for stepdebugging
      envs[PhpDebug.EV_TRIGGER_K] == PhpDebug.EV_TRIGGER_V
end

local xdebug_filtered_keys = {
  'Version',

  'xdebug.mode',
  'xdebug.client_port', -- host.docker.internal
  'xdebug.client_host', -- default 9003
  'xdebug.log',         -- /tmp/dir
  'xdebug.log_level',
  'IDE Key',            -- xdebug.idekey
}
function PhpDebug:validateXdebugInstallation()
  local xdebug = self:getXdebugSettings() --(i.configs or E).xdebug
  if self:chk('Xdebug installed', xdebug ~= nil) then
    -- Enabled and Optional Features
    if xdebug.features then
      for k, v in pairs(xdebug.features) do
        self:add('  %s => %s', k, v)
      end
    end
    for _, k in pairs(xdebug_filtered_keys) do
      if xdebug[k] then
        self:add('  %s => %s', k, xdebug[k])
      end
    end
    --
    self.xdebug_port = tonumber(self:getXdebugDirective('client_port'))
    self.xdebug_host = self:getXdebugDirective('client_host')
    self:chk('  Client Port', self.xdebug_port)
    self:chk('  Client Host', self.xdebug_host)

    local step_debugger = (xdebug.features or {})['Step Debugger']
    self:chk('  Step Debugger', (step_debugger or ''):match('enabled'))
  else
    -- self:add(self.phpinfo) -- DEBUG
  end
end

--------------------------------------------------------------------------------
--         The Data from phpinfo available after  fetchAndParsePhpInfo()
--------------------------------------------------------------------------------

function PhpDebug:hasPhpInfoConfiguration()
  local c = (self.info or E).configs
  return c and c ~= E and su.table_size(c) > 0
end

function PhpDebug:getPhpInfoConfiguration()
  return ((self.info or E).configs or E)
end

-- from PhpInfo Configuration
-- .features
-- xdebug. ...
function PhpDebug:getXdebugSettings()
  return self:getPhpInfoConfiguration().xdebug or E
end

-- Directive => Local Value => Master Value
function PhpDebug:getXdebugDirective(name)
  local xdebug = self:getPhpInfoConfiguration().xdebug
  if xdebug and name then
    if not name:match('^xdebug%.') then
      name = 'xdebug.' .. name
    end
    return xdebug[name]
  end
  return nil
end

--------------------------------------------------------------------------------
--                            Tools
--------------------------------------------------------------------------------
--

---@private
---@param line string
function PhpDebug:add(line, ...)
  if self.report and line then
    self.report:add(line, ...)
  end
end

function PhpDebug:hl()
  if self.report then
    self.report:add('--------------------------------------------------------------------------------')
  end
end

function PhpDebug:verbose(lvl, line, ...)
  if lvl <= self.verbose_lvl and self.report and line then
    self.report:add(line, ...)
  end
end

function PhpDebug:error(line, ...)
  if self.report and line then
    local s = self.report:add('[ERROR] ' .. line, ...)
    if s then
      table.insert(self.errors, s)
    end
  end
end

-- add to Report with check if ok is true or not nil or false
-- if nil or false then add into self.errors
---@param title string
---@param ok any
---@return boolean
function PhpDebug:chk(title, ok)
  if self.report and title then
    local success = true
    if ok == true then
      ok = 'OK'
    end
    if not ok or ok == 0 then
      success = false
      ok = 'FAIL'
      table.insert(self.errors, title)
    end
    self.report:add('%s: [%s]', title, ok)

    return success
  end
  return false
end

-- Add to the report title with suffix:
-- on success then with "..."  (aka open node)
-- on nil,false or 0 - "[FAIL]"
--
-- Here is assumed that we are entering the section,
-- but it is not yet known how it will work.
-- Therefore, we cannot immediately display [OK] here
---@param title string
---@param success any
function PhpDebug:ok(title, success)
  if self.report and title then
    local st = '...'
    if not success or success == 0 or success == false then
      st = ' [FAIL]'
      table.insert(self.errors, title)
    end
    self.report:add('%s', title, st)

    return success
  end
end

class.build(PhpDebug)
return PhpDebug

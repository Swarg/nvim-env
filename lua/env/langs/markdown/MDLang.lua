-- 30-09-2024 @author Swarg

local class = require 'oop.class'

local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'

local Lang = require 'env.lang.Lang'



class.package 'env.langs.markdown'
---@class env.langs.markdown.MDLang : env.lang.Lang
---@field new fun(self, o:table?, project_root:string? ): env.langs.markdown.MDLang
--
local C = class.new_class(Lang, 'MDLang', {
  ext = 'markdown',
  root_markers = { '.git', 'Makefile' },
  -- Note: there must be a slash at the end of the path
})

Lang.register(C)



---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
-- local join_path, file_exists = fs.join_path, fs.file_exists
-- local get_parent_dirpath = fs.get_parent_dirpath



-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework
---@param fn string filename
---@return boolean
function C.isLang(fn)
  return type(fn) == 'string' and fn:match('%.md$') ~= nil
end

function C:newLangGen()
  return nil
end

-- stub
---@param marker string?
function C:findProjectBuilder(marker)
  log_debug("findProjectBuilder %s marker:%s", (self or E).project_root, marker)
  self.builder = nil
  return self
end

-- stub
---@override
---@param project_root string
---@param opts table?{project_root, flat, builderClass}
---@return table properties to synck
function C:createBuilder(project_root, opts)
  log_debug("createBuilder")
  return { project_root = project_root, (opts or E).flat } -- stub
end

--
--
-- stub
---@return string?
function C:getClassNameForInnerPath(innerpath)
  return innerpath
end

--
-- jumpt to another file with defintion of word under the cursor
--
-- TODO jump from ToC to header
--
function C:gotoDefinition()
  log_debug("gotoDefinition")
  -- local ctx = self:getEditor():getContext(true, nil) -- SParser:new())
  -- local cl = su.trim(ctx:getCurrentLine())

  local path, jumpto = nil, 1

  error('Not implemented yet')

  return Lang.openExistedFileWithJumpTo(self:getEditor(), path, jumpto)
end

--  to interact with output from os tree command
local function is_tree_entry(s)
  return s ~= nil and (su.starts_with(s, '├─') or su.starts_with(s, '└─'))
end


-- todo with nested sub dirs
--
---@param editor env.ui.Editor
---@param line string
---@return string? path
local function tree_entry_get_full_path(editor, line)
  local _, k = string.find(line, '── ', 1, true)
  if not k then
    return nil
  end
  local name = su.trim(string.sub(line, k + 1, #line))
  local ctx = editor:getContext()
  local lnum = ctx:getCurrentLineNumber()
  local i = lnum
  local sw = su.starts_with

  while i > 0 do
    i = i - 1
    local line0 = (ctx:getLines(i, i) or E)[1]
    if not line0 or line0:sub(1, 3) == '```' then
      break
    end

    if not (sw(line0, '└') or sw(line0, '│') or sw(line0, '├')) then
      local path = fs.join_path(su.trim(line0), name)
      log_debug("path from tree node:", path)
      return path
    end
  end
  return nil
end


--
-- from some source code
-- main goal: jump to source file from stacktrace with errors
-- for example, from errors in tests
--
---@param editor env.ui.Editor
---@return boolean
function C.gotoDefinitionFromNonSource(editor, line)
  line = line or editor:getContext():getCurrentLine()
  log_debug("gotoDefinitionFromNonSource |%s|", line)

  local path, jumpto = nil, 1
  line = su.trim(line)

  if is_tree_entry(line) then
    path = tree_entry_get_full_path(editor, line)
  end

  if not path then
    return false
  end
  return Lang.openExistedFileWithJumpTo(editor, path, jumpto)
end

class.build(C)
return C

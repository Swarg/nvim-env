-- 17-03-2024 @author Swarg
-- snippets for EnvLineInsert for Elixir Lang
--
--
local ubuf = require("env.bufutil")

local M = {}
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local insert_items = {
  '`${SWORD}`',
  '```\n${LINE}\n```',
  '```sh\n${LINE}\n```',
  '```json\n${LINE}\n```',
  '```lua\n${LINE}\n```', -- 5
  '[${SWORD}]()',         -- url
  '![${WORD}]()',         -- image http://url/a.png
  '- ${LINE}',
  '"${SWORD}"',
  '## ${LINE}',                        -- 10
  '### ${LINE}',                       -- 11
  '`${LINE}`',
  '_${WORD}_',                         -- italic
  '__${WORD}__',                       -- bold  same as **${WORD}**
  '```php\n<?\n```',
  '<!-- ${DATE} ${TIME} ${SWORD} -->', -- 16
  "## My Solution\n",
  "## Community Solutions\n",
  '${HANDLER_MK_FILE_REF}', -- 19
  "${HANDLER_EMIT_HTML}",   -- "\n<${WORD}>\n</${WORD}>", -- 20
  "<${WORD}></${WORD}>",    -- 21
  '```java\n${LINE}\n```',  -- 22
  '```sql\n${LINE}\n```',   -- 23
  '${MD_TITLE}',
  '[0]: ${LINE}',           -- http://host.org...  ->  [1]: http://host.org...
  '',
  '',
  '',
  '',
  '${WORD_UTF8}',              -- 30 translate: you2we
  '```Makefile\n${LINE}\n```', -- 31
  '${HANDLER_ADD_REFERENCE_STYLE_LINK}',
  '```html\n${LINE}\n```',     -- 33
  '```svelte\n${LINE}\n```',   -- 34
  '```elixir\n${LINE}\n```',   -- 35
  '```js\n${LINE}\n```',
  '',
  '```groovy\n${LINE}\n```',
  '```Dockerfile\n${LINE}\n```',
  '',
  '${HANDLER_CONVERT_TIMESTAMP}', -- 41
  '',
  '',
  '${HANDLER_INLINE_FILE}',              -- 44
  '${HANDLER_NEW_FILE}',
  '${HANDLER_GEN_HTML_FOR_CSS_CLASSES}', -- for styles.css for learning purposes
  '${HANDLER_GEN_MULTIPLE_CSS_CLASSES}',
}

local insert_mappings = {
  [30] = {
    ['вы'] = {
      ['будете'] = 'будем',
      ['хотите'] = 'нужно',
      ['должны'] = 'нужно',
      ['можете'] = 'можно',
      ['сможете'] = 'возможно',
      ['сделаете'] = 'сделаем',
      ['предоставляете'] = 'предоставляем',
      ['получили'] = 'получим',
      ['получите'] = 'получим',
      ['получаете'] = 'получаем',
      ['вернете'] = 'вернём',
      ['выдаете'] = 'выдаётся',
      ['вызываете'] = 'вызываем',
      ['решите'] = 'решить',
      ['запустите'] = 'запустим',
      ['запускаете'] = 'запускается',
      ['вводите'] = 'вводим',
      ['создаете'] = 'создаётся',
      ['создадите'] = 'создадим',
      ['используете'] = 'используется',
      ['выполняете'] = 'выполняется',
      ['обращаетесь'] = 'обращаемся',
      ['открываете'] = 'открываем',
      ['отправляете'] = 'отправляется',
      ['указываете'] = 'указывается',
      ['начнете'] = 'начнем',
      ['начинаете'] = 'начинают',
      ['знаете'] = 'известно',
    },
    ['вам'] = {
      ['необходимо'] = 'нужно',
      ['следует'] = 'нужно',
      ['придется'] = 'нужно',
    }
  }
}


---@param cbi table{words, word_idx, word_s, word_e}
---@diagnostic disable-next-line: unused-local
function M.handler_you2we(key, cbi, str, word)
  assert(type(cbi) == 'table', 'cbi')
  local mappings = cbi.mappings[cbi.index or false] or E
  local new_str
  local next_word = (cbi.words or E)[cbi.word_idx + 1]

  if word == 'вы' or word == 'Вы' then
    new_str = (mappings['вы'] or E)[next_word]
  elseif word == 'вам' or word == 'Вам' then
    new_str = (mappings['вам'] or E)[next_word]
  end

  if new_str then
    cbi.word_s = cbi.words.brange[cbi.word_idx][1]
    cbi.word_e = cbi.words.brange[cbi.word_idx + 1][2]
    return new_str
  end
  return str
end

---@param bufnr number
---@return table
function M.get_references_style_links(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, true)
  local t = {}
  for _, line in ipairs(lines) do
    local index, url = string.match(line, '^%[([%w]+)%]:%s*([^%s]+)')
    if index and url then
      local key = tonumber(index) or index
      t[key] = url
    end
  end
  return t
end

-- wrap selected text as reference-style link
---@param cbi table
---@param snippet string
---@return boolean success
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function M.handler_add_url_reference(cbi, snippet)
  -- print(vim.inspect(cbi))

  -- take url from clipboard
  local url = vim.fn.getreg("+")
  if not url or string.match(url, '^https?://[%w_]+') == nil then
    return false, 'expected url in the system clipboard'
  end

  local lnum, col, lnume, cole = ubuf.get_selection_range_ex()

  if lnum ~= lnume then
    error('selected multilines not supported.')
  end

  if col == cole then
    -- if cbi.visual_mode then end
    return false, 'Select the word before the action'
  end

  local refs = M.get_references_style_links(cbi.bufnr)
  local next_index = #refs + 1

  local line = cbi.current_line
  -- trimed
  local selection = line:sub(col, cole):gsub("^%s*(.-)%s*$", "%1")

  local left, right = line:sub(1, col - 1), line:sub(cole + 1, #line)

  line = fmt('%s[%s][%s]%s', left, selection, next_index, right)

  vim.api.nvim_buf_set_lines(0, lnum - 1, lnum, true, { line })

  local new_ref = fmt('[%s]: %s', next_index, v2s(url))
  local cnt = vim.api.nvim_buf_line_count(cbi.bufnr)
  vim.api.nvim_buf_set_lines(cbi.bufnr, cnt, cnt, true, { new_ref })

  return true, nil
end

local log = require 'alogger'
local Editor = require 'env.ui.Editor'
local udt = require 'env.util.datetime'
local su = require 'env.sutil'
local fs = require 'env.files'
local common = require 'env.lang.utils.common'
local hdgen = require 'hdgen'
local uhtml = require 'env.util.html'

-- see also EnvLine convert d2t
function M.handler_convert_timestamp()
  local words = su.split(Editor.getCurrentLine(), " ")
  local w1 = words[1]
  local lines = nil
  if w1 == 'now' or w1 == 'timestamp' then
    local ts = os.time()
    lines = { tostring(ts) .. ' -> ' .. udt.timestamp2readable(tonumber(w1) or 0) }
    --
  elseif tonumber(w1) then -- timestamp to readable
    lines = { w1 .. ' -> ' .. udt.timestamp2readable(tonumber(w1) or 0) }
  else                     -- current readable datetime to timestamp
    if words[2] then w1 = w1 .. ' ' .. words[2] end
    lines = { w1 .. ' -> ' .. (udt.datetime2timestamp(w1) or '?') }
  end
  if lines then
    Editor:new():setCurrentLine(lines)
  end
  return true
end

function M.handler_mk_file_ref()
  local editor = Editor:new()
  local fn = editor:getCurrentFile()
  local cwd = os.getenv('PWD')
  if cwd and fn and su.starts_with(fn, cwd) then
    fn = './' .. fn:sub(#cwd + 2, #fn)
  end
  editor:getContext():insertAfterCurrentLine('/* ' .. v2s(fn) .. ' */')
  return true
end

local function is_empty(s)
  return s == nil or s == '' or su.trim(s) == ''
end

---@return false|env.ui.Editor
---@return string path|errmsg
---@return number?
---@return number?
local function get_editor_n_filename()
  local editor = Editor:new()
  local cl = su.trim(editor:getCurrentLine())
  if is_empty(cl) then
    return false, 'current line is empty'
  end
  if #cl > 255 then
    return false, 'current line is too long got: ' .. #cl
  end
  local fn, lnum, lnum_end = match(cl, '^%./([^%s]+):(%d+):(%d+)$')
  if not fn then
    fn, lnum = match(cl, '^%./([^%s]+):(%d+)$')
    if not fn then
      fn = match(cl, '^%./([^%s]+)$')
    end
  end
  lnum = tonumber(lnum) or 1
  lnum_end = tonumber(lnum_end) or -1 -- whole file

  if not fn then
    return false, 'expected file name got: ' .. v2s(cl)
  end
  local cwd = fs.cwd()
  local path = fs.build_path(cwd, fn)

  return editor, path, lnum, lnum_end
end

--
-- inline content of the given defined in the current line file into md file
--
function M.handler_inline_file()
  local editor, path, lnum, lnum_end = get_editor_n_filename()
  if not editor then
    return false, path -- errmsg
  end
  if not fs.file_exists(path) then
    return false, 'not found file ' .. v2s(path)
  end
  local lines = {}
  lines[1] = '```' .. (fs.extract_extension(path) or '')
  if lnum == 1 and lnum_end == -1 then
    fs.read_lines(path, lines)
  elseif lnum > 0 then -- and lnum_end > lnum then
    local f = io.open(path, 'rb')
    if f then
      local ln = 0
      local has_lnum_end = lnum_end ~= -1
      while true do
        ln = ln + 1
        local line = f:read("*l")
        if line == nil or (has_lnum_end and ln > lnum_end) then
          break -- eof or limit
        end
        if ln >= lnum then
          lines[#lines + 1] = line
        end
      end
      f:close()
    end
  end

  while lines[#lines] == '' do lines[#lines] = nil end -- trim empty lines

  lines[#lines + 1] = '```'

  editor:insertAfterCurrentLine(lines)

  return true
end

-- create new empty file from name in the current line
--
function M.handler_new_file()
  local editor, path = get_editor_n_filename()
  if not editor then
    return false, path -- errmsg
  end
  -- todo: write content from syntax block if has
  if not fs.file_exists(path) then
    fs.write(path, '')
  end
  editor:open(path, 1)
  return true
end

--
-- to support shortcut like div.box-1{p'Text'} -> div{class="box-1",p'Text'}
-- this will be work only for root element
---@param lines table
local function apply_shortcuts(lines)
  if #lines == 1 then
    local tag, t, s, rest = match(lines[1], '^%s*(%w+)([%.#])([%w%-_]+)(.*)$')
    if rest ~= '' and rest ~= nil then
      if rest:sub(1, 1) == '{' and rest:sub(-1, -1) == '}' then
        rest = rest:sub(2, -2)
      end
      rest = ',' .. rest
    end
    if tag and s then
      local attr = t == '.' and 'class' or 'id'
      lines[1] = tag .. '{' .. attr .. '="' .. s .. '"' .. rest .. '}'
    end
    if not tag and string.match(lines[1], '^%s*([%w%-%_]+)$') then
      lines[1] = lines[1] .. "()" -- br --> b('') tag is a function -needs call
    end
  end
end

-- x3 br  ->  <br/><br/><br/>
---@return number
local function extract_multiply_factor(lines)
  if #lines == 1 then
    local tab, n, line = string.match(lines[1], '^(%s*)x(%d+)%s+(.*)%s*$')
    if n and line and line ~= '' then
      lines[1] = tab .. line
      return tonumber(n) or 1
    end
  end
  return 1
end

-- dup lines in given factor
---@param lines table
---@param n number
---@return table
local function apply_multiply_factor(lines, n)
  if type(n) ~= 'number' or n <= 1 or #lines == 0 then
    return lines
  end
  local t = {}
  if #lines == 1 then -- in one line
    local line = lines[1]
    local s = string.match(line, '^(%s*)') or ''
    for _ = 1, n, 1 do
      s = s .. string.match(line, '^%s*(.-)%s*$') -- trimed
    end
    t[#t + 1] = s
  else
    for _ = 1, n do
      for _, line in ipairs(lines) do
        t[#t + 1] = line
      end
    end
  end
  return t
end

--
-- convert lua-code of hdgen into html code
-- lua-code is a text from selected lines or current line
--
-- Usage Example:
--
-- `html{body{p'text'}}` ->  <!doctype html><html><body><p>text</p></body></html>
--
function M.handler_emit_html()
  local editor = Editor:new()
  -- lnum starts from 1 not from 0
  local lines, lnum, lnum_end = editor:getContext():getSelectedLines()
  if type(lines) ~= 'table' then
    return false, 'no lines'
  end
  if #lines == 0 then
    lines = { editor.getCurrentLine() }
    lnum = editor:getContext():getCurrentLineNumber()
    lnum_end = lnum
  end
  local indent = match(lines[1] or '', '^(%s*)')
  local depth = #(indent or '') / 2 -- one tab 2spaces

  local n = extract_multiply_factor(lines)
  log.debug('multiply factor: ', n, lines)
  apply_shortcuts(lines)

  local opts = { format = true, tab = '  ', depth = depth, tag_sep = "\n" }

  local ok, err, res = hdgen.mk_html_code(lines, true, opts)
  if not ok or type(res) ~= 'table' then
    return false, v2s(err)
  end
  if res[#res] == '' then res[#res] = nil end

  res = apply_multiply_factor(res, n)

  editor:getContext():setLines(lnum, lnum_end, res)
  return true
end

-- align-items{start, end, center, stretch}  -->
-- .align-items-start { align-items: start; }
-- .align-items-end { align-items: end; } ...
function M.handler_gen_multiple_css_classes()
  local editor = Editor:new()
  local line = editor:getCurrentLine()
  local prefix, rest = match(line, '^([%w_%-]+){(.-)}$')
  if not prefix then
    return false, 'expected some-prefix{val1, val2, .. valN}'
  end
  local values = su.split(rest, ",", nil, su.trim)
  local t = {}
  for _, v in ipairs(values) do
    v = v2s(v)
    t[#t + 1] = fmt('.%s-%s { %s: %s; }', v2s(prefix), v, prefix, v)
  end

  local lnum = editor:getContext():getCurrentLineNumber()
  editor:getContext():setLines(lnum, lnum, t)

  return true
end

---@param add function
---@param styles table
---@param opts table
local function generate_html_elms_for_css(add, styles, opts)
  local tag = opts.tag or 'div'
  for _, e in ipairs(styles.classes) do
    local css_class = e[1] -- from css-selector
    local rules = e[2]     -- rule body table

    if type(rules) == 'table' then
      if #rules <= 1 then
        add(fmt('  <%s class="%s">%s</%s>', tag, css_class, rules[1] or '', tag))
      else
        add(fmt('  <%s class="%s">', tag, css_class))
        for _, prop in ipairs(rules) do
          add("    " .. prop)
        end
        add(fmt('  </%s>', tag, css_class))
        add('')
      end
    elseif rules ~= nil then
      add(fmt('  <%s class="%s">%s</%s>', tag, css_class, v2s(rules), tag))
    end
  end
end

M.SUBSTITUTE_PATTERN = "%${([%w_]+)}" -- ${KEY} ${THE_KEY}

---@param editor env.ui.Editor
---@param add function
---@param styles table
---@param opts table
local function generate_html_code_for_template(editor, add, styles, opts)
  if opts.lns == nil or opts.lne == nil then
    return false, 'expected lns and lne with already existed html-code template'
  end
  local lines = editor:getContext():getLines(opts.lns, opts.lne)
  if not lines or #lines == 0 then
    return false, 'template with html-code not found'
  end

  for _, e in ipairs(styles.classes) do
    local css_class = e[1] -- from css-selector
    local rules = e[2]     -- rule body table
    if type(rules) == 'table' then
      rules = table.concat(rules, ' ')
    elseif type(rules) ~= 'string' then
      rules = v2s(rules)
    end
    local subst_kv_map = {
      CSS_CLASS = css_class,
      RULES = rules
    }
    add('  <div>' .. subst_kv_map.RULES .. '</div>') -- desc
    for _, line in ipairs(lines) do
      string.gsub(line, '${CSS_CLASS}', css_class)
      add(string.gsub(line, M.SUBSTITUTE_PATTERN, subst_kv_map))
    end
  end

  return true
end


--
-- parse sleected lines from styles.css and
-- generate html p-tags with parsed css classes
-- for learning purposes
function M.handler_gen_html_for_css_classes()
  local editor = Editor:new()
  local lines, _, lnum_end = editor:getContext():getSelectedLines()
  if type(lines) ~= 'table' then return false, 'no lines' end
  local off = 0
  local opts = uhtml.parse_kvpairs(uhtml.get_comment_content(lines[1]))
  if next(opts) ~= nil then
    off = 1
  end

  local styles = uhtml.parse_css(lines, off, opts)

  local res = {} -- html code
  local function add(line) res[#res + 1] = line end

  if opts.act == 'use' then -- use given template from the buffer
    local ok, err = generate_html_code_for_template(editor, add, styles, opts)
    if not ok then return false, err end
  else
    -- default - generate new template
    generate_html_elms_for_css(add, styles, opts)
  end

  if res[#res] == '' then res[#res] = nil end

  editor:getContext():setLines(lnum_end + 1, lnum_end, res)
  return true
end

--
local insert_handlers = {
  [30] = M.handler_you2we,
  ADD_REFERENCE_STYLE_LINK = M.handler_add_url_reference,
  CONVERT_TIMESTAMP = M.handler_convert_timestamp,
  INLINE_FILE = M.handler_inline_file,
  NEW_FILE = M.handler_new_file,
  MK_FILE_REF = common.handler_mk_file_ref,
  EMIT_HTML = M.handler_emit_html,
  GEN_HTML_FOR_CSS_CLASSES = M.handler_gen_html_for_css_classes,
  GEN_MULTIPLE_CSS_CLASSES = M.handler_gen_multiple_css_classes,
}

---@param reg function
function M.add(reg)
  reg('md', insert_items, insert_mappings, insert_handlers)
end

return M

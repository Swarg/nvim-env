-- 23-05-2024 @author Swarg
-- Goal:
--   - work with vtt-subtitles
--

local mpv = require 'env.bridges.mpv'
local uvtt = require "env.langs.vtt.utils"
local M = {}

local insert_items = {
  '${HANDLER_LINE_TO_SENTENCE}',
  '${CAPITALIZE_WORD}',
  '"${WORD}"',
  '"${SWORD}"',
  '<!b05>', -- for tts break time="500ms"
  '',
  '',
  '"${CAPITALIZE_WORD_WITH_UNDERSCORE}"',
  '"${CAPITALIZE_WORD_WITH_SPACE}"',                         -- order item --> "Order Item"
  '',
  '${HANDLER_VTT_SYNC_TIMESTAMPS_CURR_END_WITH_NEXT_START}', -- 11
  '${HANDLER_VTT_JOIN_TWO_TO_ONE}',                          -- 12
  '${HANDLER_VTT_MOVE_RIGHT_TO_NEXT}',
  '${HANDLER_VTT_MOVE_LEFT_TO_PREV}',
  '${HANDLER_VTT_SET_ENDTIME_FROM_MPV_TIMEPOS}',
  '${HANDLER_VTT_SET_STARTTIME_FROM_MPV_TIMEPOS}',
  '',
  '',
  '',
  '',
  '${HANDLER_SUBS_SYNC_MPV_WITH_NVIM}',
  '${HANDLER_SUBS_SYNC_NVIM_WITH_MPV}',
  '',
  '${HANDLER_MPVCTL_CONNECT}',
  '${HANDLER_MPVCTL_DISCONNECT}',
  '${HANDLER_MPV_STOP}',
  '${HANDLER_MPV_PLAY}',
  '',
  '${HANDLER_MAP_WORD}',
}

local Editor = require 'env.ui.Editor'
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local get_bufnr_lnum_col = uvtt.get_bufnr_lnum_col
-- bunch size for fetched from nvim lines on find next entry
local BSZ = 8

local insert_mappings = {
}

-- "this is a sentence" --> "This is a sentence."
function M.handler_line_to_sentence()
  local bufnr, lnum, _ = get_bufnr_lnum_col()
  local line = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum, false)[1]
  line = uvtt.trim(line)
  line = uvtt.mk_upper_first_letter(line)
  local lc = line:sub(-1, -1)
  if not uvtt.is_punctuation_mark(lc) then
    line = line .. '.'
  end
  vim.api.nvim_buf_set_lines(bufnr, lnum - 1, lnum, false, { line })
  return true
end

local words_map = {
  spel = 'Svelte',
  spelt = 'Svelte',
  ['spelt kit'] = 'SvelteKit',
  ['speltkit'] = 'SvelteKit',
  ['spelkit'] = 'SvelteKit',
  ['dotspelt'] = '.svelte',
  ['dotspel'] = '.svelte',
}

function M.handler_map_word()
  local ctx = Editor:new():getContext(true)
  local w = ctx:resolveWordSimple().word_element
  local new_word = words_map[string.lower(w or '')]
  if new_word then
    ctx:replaceCurrentWord(new_word)
    return true
  end
  return false, 'not found known word to replace'
end

--
--
-- VTT_SYNC_TIMESTAMPS_CURR_END_WITH_NEXT_START
-- sync(in nvim buffer) timestamp of current entry with next entry
-- next_entry.starttime = current_entry.endtime
--
---param cbi table
---param snippet string
---@return boolean success
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function M.handler_sync_two_timestamps() -- cbi, snippet)
  local bufnr, lnum, _ = get_bufnr_lnum_col()

  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum + BSZ, false)

  local res, errmsg = uvtt.sync_timestamp_end_with_next_entry(lines)

  if type(res) == 'table' and #res > 0 then
    vim.api.nvim_buf_set_lines(bufnr, lnum - 1, lnum + #res - 1, true, res)
    if type(errmsg) == 'number' then
      local new_lnum = lnum - 1 + errmsg
      vim.api.nvim_win_set_cursor(0, { math.max(0, new_lnum), 1 })
    end
    return true, nil
  end

  return false, errmsg
end

--
function M.handler_join_two_to_one()
  local bufn, lnum, _ = get_bufnr_lnum_col()

  local lnum_tp = uvtt.find_prev_timepos_lnum(bufn, lnum)
  if lnum_tp < 0 then
    return false, 'not found line with time-range'
  end
  vim.api.nvim_win_set_cursor(0, { lnum_tp, 0 })

  lnum = lnum_tp
  local lines = vim.api.nvim_buf_get_lines(bufn, lnum - 1, lnum + BSZ, false)

  local t, n = uvtt.join_two_entry_to_one(lines)

  if type(t) == 'table' and #t > 0 and type(n) == 'number' then
    vim.api.nvim_buf_set_lines(bufn, lnum - 1, lnum - 1 + n, true, t)
    return true, nil
  end

  return false, 'error'
end

-- update timestamp in two entries by current time-pos in mpv
---@param value any
local function sync_two_entries_for(value)
  if type(value) == 'number' and value > -1 then
    local lnum = value
    vim.api.nvim_win_set_cursor(0, { lnum, 0 })
    local ok, errmsg = M.handler_set_endtime_from_mpv_timepos()
    if ok then
      return M.handler_sync_two_timestamps()
    end
    return ok, errmsg
  end
  return false, 'expected number lnum got ' .. v2s(value)
end

---@param sync_time boolean?
function M.handler_move_right_to_next(sync_time)
  local ok, errmsg = uvtt.move_right_substr_to_next_entry(sync_time)
  if ok and sync_time then
    return sync_two_entries_for(errmsg)
  end
  return ok, errmsg
end

---@param sync_time boolean?
function M.handler_move_left_to_prev(sync_time)
  local ok, errmsg = uvtt.move_left_substr_to_prev_entry(sync_time)
  if ok and sync_time then
    print(errmsg)
    return sync_two_entries_for(errmsg)
  end
  return ok, errmsg
end

-- set end-timestamp in the nvim buf to current time-pos in mpv
function M.handler_set_endtime_from_mpv_timepos()
  local timepos = mpv.cmd_get_time_pos()
  if type(timepos) ~= 'number' then
    return false, 'cannot get current timepos from mpv got: ' .. v2s(timepos)
  end
  return uvtt.update_timestamp_in_current_line(timepos, false)
end

-- set start-timestamp in the nvim buf to current time-pos in mpv
function M.handler_set_starttime_from_mpv_timepos()
  local timepos = mpv.cmd_get_time_pos()
  if type(timepos) ~= 'number' then
    return false, 'cannot get current timepos from mpv got: ' .. v2s(timepos)
  end
  return uvtt.update_timestamp_in_current_line(timepos, true)
end

-- :EnvLM vtt mpv connect
---@return boolean, string -- ok, errmsg
function M.handler_mpvctl_connect()
  return mpv.connect_to_mpv()
end

function M.handler_mpvctl_disconnect()
  return mpv.shutdown_connection()
end

function M.handler_mpv_stop()
  local ok = mpv.mpv_pause()
  return ok, not ok and 'fail' or nil
end

function M.handler_mpv_play()
  local ok = mpv.mpv_play()
  return ok, not ok and 'fail' or nil
end

function M.handler_sync_mpv_with_nvim()
  local ok = mpv.sync_mpv_time_pos_with_nvim_lnum()
  return ok, not ok and 'fail' or nil
end

function M.handler_sync_nvim_with_mpv()
  local ok = mpv.sync_nvim_lnum_with_mpv_time_pos()
  return ok, not ok and 'fail' or nil
end

--
local insert_handlers = {
  LINE_TO_SENTENCE = M.handler_line_to_sentence,
  VTT_SYNC_TIMESTAMPS_CURR_END_WITH_NEXT_START = M.handler_sync_two_timestamps,
  VTT_JOIN_TWO_TO_ONE = M.handler_join_two_to_one,

  VTT_MOVE_RIGHT_TO_NEXT = M.handler_move_right_to_next,
  VTT_MOVE_LEFT_TO_PREV = M.handler_move_left_to_prev,
  VTT_SET_ENDTIME_FROM_MPV_TIMEPOS = M.handler_set_endtime_from_mpv_timepos,
  VTT_SET_STARTTIME_FROM_MPV_TIMEPOS = M.handler_set_starttime_from_mpv_timepos,

  MPVCTL_CONNECT = M.handler_mpvctl_connect,
  MPVCTL_DISCONNECT = M.handler_mpvctl_disconnect,
  -- vtt subs editor
  SUBS_SYNC_MPV_WITH_NVIM = M.handler_sync_mpv_with_nvim,
  SUBS_SYNC_NVIM_WITH_MPV = M.handler_sync_nvim_with_mpv,

  -- remote control from nvim
  MPV_STOP = M.handler_mpv_stop,
  MPV_PLAY = M.handler_mpv_play,

  MAP_WORD = M.handler_map_word,
}

---@param reg function
function M.add(reg)
  reg('vtt', insert_items, insert_mappings, insert_handlers)
end

return M

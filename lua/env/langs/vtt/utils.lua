-- 25-05-2024 @author Swarg
local M = {}

local log = require 'alogger'
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

-- M.vvt_pattern = "^(%d%d:%d%d:%d%d.%d%d%d) %-%-> (%d%d:%d%d:%d%d.%d%d%d)"
M.vvt_pattern = "^(%d%d:%d%d:%d%d.%d+) %-%-> (%d%d:%d%d:%d%d.%d+)"
M.ptn_timestamp_h_m_s_ms = "^(%d%d):(%d%d):(%d%d).(%d%d%d)$"

local MAX_WIDTH_FOR_JOIN = 90
local BSZ = 8
-- Punctuation marks to complete a sentence
local end_of_sentence = { ['.'] = true, ['!'] = true, ['?'] = true }
local in_sentence_sep = { [','] = true, [':'] = true, [';'] = true, ['-'] = true }

---@return table|false
---@return string? errmsg
function M.join_two_entry_to_one(lines)
  assert(type(lines) == 'table', 'lines')
  local t = { '' }
  local prevts, prevte = nil, nil
  local p = M.vvt_pattern
  local n = 0

  for _, line in ipairs(lines) do
    n = n + 1
    local empty_line = string.match(line, "^%s*$") ~= nil

    if empty_line and prevte then -- and prevts then
      -- finished
      break
    else
      local ts, te = string.match(line, p)
      if ts and te then
        if prevts == nil then
          prevts = ts
        else
          prevte = te
        end
      elseif not empty_line then
        -- text
        local prev = t[#t]
        if prev and prev ~= '' and #prev + #line < MAX_WIDTH_FOR_JOIN then
          t[#t] = prev .. ' ' .. line
        else
          t[#t + 1] = line
        end
      end
    end
  end

  if prevte then
    t[1] = v2s(prevts) .. ' --> ' .. v2s(prevte)
    t[#t + 1] = ''
  end

  return t, n
end

---@param lines table
---@return table?
---@return string? error
function M.sync_timestamp_end_with_next_entry(lines)
  assert(type(lines) == 'table', 'lines')
  if #lines < 3 then
    return nil, 'minimal is 3 lines got: ' .. v2s(#lines)
  end
  local ts, te = string.match(lines[1], M.vvt_pattern)
  if not ts or not te then
    return nil, 'expeted under the timestamp of entry got: ' .. v2s(lines[1])
  end

  local t = {
    lines[1], -- timestamp
    lines[2], -- text
  }
  local ts2, te2, idx

  for i = 3, #lines do
    local line = lines[i]
    ts2, te2 = M.parse_timestamp(line)
    if ts2 and te2 then
      t[#t + 1] = te .. ' --> ' .. te2
      idx = #t
      break
    else
      t[#t + 1] = line
    end
  end

  if not ts2 then
    return nil, 'not found timestamp of the next entry'
  end

  return t, idx
end

--
-- from the line under the cursor in nvim buff
--
---@return string?, string? -- start end
function M.get_current_sub_timestamps()
  -- ret.bufnr = vim.api.nvim_get_current_buf()
  local bufnr, lnum, _ = M.get_bufnr_lnum_col()
  local lnum_start = math.max(0, lnum - BSZ)
  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum_start, lnum, false)
  if lines then
    for i = #lines, 1, -1 do
      local line = lines[i]
      local sts, ste = M.parse_timestamp(line)
      if sts and ste then
        return sts, ste
      end
    end
  end
  return nil, nil
end

---@return number
function M.str_timestamp_to_num(s)
  local h, m, sec, ms = string.match(s or '', M.ptn_timestamp_h_m_s_ms)
  if h and m and s and ms then
    return h * 3600 + m * 60 + sec + ms / 1000
  end
  return -1
end

---@param line string?
---@return string?, string?
function M.parse_timestamp(line)
  local sts, ste = string.match(line or '', M.vvt_pattern)
  return sts, ste
end

--
-- 22.8 -> '00:00:22.800'
--
---@param sec number|string of secords 22.8  or already formated string
---@return string
function M.fmt_sec(sec)
  local time
  local typ = type(sec)
  if typ == 'number' then
    local s = math.floor(sec)
    local ms = (sec - s) * 1000
    time = fmt("%.2d:%.2d:%.2d.%.3d", s / (60 * 60), s / 60 % 60, s % 60, ms)
    -- already formated
  elseif typ == 'string' and string.match(sec, M.ptn_timestamp_h_m_s_ms) then
    time = sec
  else
    error('expected number of formated string got:' .. v2s(sec) .. ' ' .. type(sec))
  end
  return time
end

---@param ts string|number
---@param te string|number
function M.build_timestamp_line(ts, te)
  return M.fmt_sec(ts) .. ' --> ' .. M.fmt_sec(te)
end

---@param winnr number?
---@return number, number, number -- bufnr, lnum, col
function M.get_bufnr_lnum_col(winnr)
  local bufnr = vim.api.nvim_get_current_buf()
  local cp = (vim.api.nvim_win_get_cursor(winnr or 0) or {})
  return bufnr, cp[1], cp[2]
end

---@param bufnr number?
---@param timepos number
---@return boolean
---@return number|string
function M.find_lnum_for_timepos(bufnr, timepos)
  assert(type(timepos) == 'number', 'timepos')
  local a = vim.api
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  local cnt = a.nvim_buf_line_count(bufnr)
  if not cnt or cnt < 1 then
    return false, 'current buffer is empty'
  end

  local first = (a.nvim_buf_get_lines(bufnr, 0, 1, true) or E)[1]

  if not string.find(first or '', 'WEBVTT') then
    return false, 'expected WEBVTT got:' .. v2s(first:sub(1, 64))
  end

  local i, bunch = 1, 32
  local c = 0 -- checked line count

  while cnt - i > 0 do
    local lines = a.nvim_buf_get_lines(bufnr, i, math.min(i + bunch, cnt), false)
    if not lines then
      break
    end
    for j, line in ipairs(lines) do
      local sts, ste = string.match(line, M.vvt_pattern)
      if sts and ste then
        local ts = M.str_timestamp_to_num(sts)
        local te = M.str_timestamp_to_num(ste)
        if timepos >= ts and timepos <= te or timepos < ts then
          return true, i + j
        end
      end
      c = c + 1
    end
    i = i + bunch + 1
  end

  return false, 'not found lnum for ' .. v2s(timepos) .. ' checked: ' .. v2s(c)
end

---@param lines table
---@param offset number
function M.indexof_prev_timepos(lines, offset)
  assert(type(lines) == 'table', 'lines')
  assert(type(offset) == 'number' and offset <= #lines, 'offset')
  log_debug("indexof_prev_timepos offset:", offset)

  for i = offset --[[#lines]], 1, -1 do
    local line = lines[i]
    local ts = M.parse_timestamp(line)
    if ts ~= nil then
      return i
    end
  end
  return -1
end

function M.find_prev_timepos_lnum(bufnr, lnum)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(lnum) == 'number', 'lnum')
  log_debug("indexof_prev_timepos offset:", lnum)

  local lnum_start = math.max(0, lnum - BSZ)
  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum_start, lnum, false)
  for i = #lines, 1, -1 do
    local ts = M.parse_timestamp(lines[i])
    if ts ~= nil then
      return lnum_start + i
    end
  end
  return -1
end

function M.trim(s)
  return string.gsub(s, "^%s*(.-)%s*$", "%1")
end

---@param s string
---@return boolean
function M.is_upper(s)
  return s ~= '' and s == string.upper(s)
end

-- todo utf8
function M.mk_upper_first_letter(s)
  return s:sub(1, 1):upper() .. s:sub(2)
end

function M.is_punctuation_mark(s)
  s = s or false
  return end_of_sentence[s] or in_sentence_sep[s] ~= nil
end

function M.is_entry_metaline(line)
  return line ~= nil and (line == '' or line:match(M.vvt_pattern) ~= nil)
end

---@param lines table
---@param i number
function M.recognize_sentence_start(lines, i)
  if lines and i and i > 1 then
    local line = lines[i]
    if line and line ~= '' and not M.is_upper(line:sub(1, 1)) then
      for j = i - 1, 1, -1 do
        local line0 = lines[j]
        if not M.is_entry_metaline(line0) then
          local lc = line0:sub(-1, -1)
          if end_of_sentence[lc] then
            lines[i] = M.mk_upper_first_letter(lines[i])
          end
          break
        end
      end
    end
  end
  return lines
end

-- for move_right_substr_to_next_entry
-- ask nvim for prev lines
---@param bufnr number
---@param lines table
function M.recognize_sentence_start_ex(bufnr, lnum, lines, i)
  if lines and i and lines[i] then
    local line = lines[i]
    if line and line ~= '' and not M.is_upper(line:sub(1, 1)) then
      local lnum_s = math.max(0, lnum - BSZ)
      local get_lines = vim.api.nvim_buf_get_lines

      local ok, prev_lines = pcall(get_lines, bufnr, lnum_s, lnum, false)
      if not ok or type(prev_lines) ~= 'table' then
        return lines
      end

      for j = #(prev_lines or E), 1, -1 do
        local line0 = prev_lines[j]
        if not M.is_entry_metaline(line0) then
          local lc = line0:sub(-1, -1)
          if end_of_sentence[lc] then
            lines[i] = M.mk_upper_first_letter(lines[i])
          end
          break
        end
      end
    end
  end
  return lines
end

--
-- move part of the line from the right of the cursor to the next entry
--
---@return boolean, string|number? -- ok, errmsg
---@param sync_time boolean?
function M.move_right_substr_to_next_entry(sync_time)
  local bufnr, lnum, col = M.get_bufnr_lnum_col()
  if not lnum or not col then
    return false, 'cannot find cursor_pos: lnum, col'
  end

  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum + BSZ, false)
  if not lines or #lines == 0 then
    return false, 'no lines'
  end

  local right = M.trim((lines[1] or ''):sub(col + 1))

  if right == '' or #right == 1 and #lines[1] == col + 1 then
    return false, 'nothing to move from right'
  end

  -- find next entry
  local next_entry_i = -1
  for i, line in ipairs(lines) do
    local sts, _ = M.parse_timestamp(line)
    if sts then
      next_entry_i = i + 1
      break
    end
  end

  -- update buffer
  if next_entry_i > 0 then
    local remains = M.trim(lines[1]:sub(1, col))
    local lc = remains:sub(-1, -1)
    local sentence_end = end_of_sentence[lc]
    -- make capital letter in new sentence
    if sentence_end and not M.is_upper(right:sub(1, 1)) then
      right = M.mk_upper_first_letter(right)
    end

    lines[1] = remains
    lines[next_entry_i] = right .. ' ' .. lines[next_entry_i]

    M.recognize_sentence_start_ex(bufnr, lnum - 1, lines, 1)

    vim.api.nvim_buf_set_lines(bufnr, lnum - 1, lnum + BSZ, false, lines)

    local prev_timepos_lnum = -1
    if sync_time then
      if (M.is_upper(right:sub(1, 1)) or sentence_end or in_sentence_sep[lc]) then
        prev_timepos_lnum = M.find_prev_timepos_lnum(bufnr, lnum)
      end
    end

    return true, prev_timepos_lnum
  end

  return false, 'not found next entry'
end

--
-- move part of the line from the left of the cursor to the prev entry
--
---@param sync_time boolean?
---@return boolean, string|number? -- ok, errmsg|prev_timepos_lnum
function M.move_left_substr_to_prev_entry(sync_time)
  local bufnr, lnum, col = M.get_bufnr_lnum_col()
  if not lnum or not col then
    return false, 'cannot find cursor_pos: lnum, col'
  end

  if lnum < 3 then
    return false, 'too few lines to the current one'
  end
  local lnum_start = math.max(0, lnum - BSZ)
  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum_start, lnum, false)
  if not lines or #lines == 0 then
    return false, 'no lines'
  end

  local curr_line = lines[#lines]
  local left = M.trim((curr_line or ''):sub(1, col))

  if col <= 1 or left == '' then
    return false, 'nothing to move from left'
  end

  -- find prev entry from the current one
  local prev_entry_i = -1

  for i = #lines, 1, -1 do
    local line = lines[i]
    local sts, _ = M.parse_timestamp(line)
    if sts then
      i = i - 1
      while line == '' and i > 0 do
        line = lines[i]
        i = i - 1
      end

      prev_entry_i = i - 1 -- asume one empty line between entries
      break
    end
  end

  -- update buffer
  if prev_entry_i > 0 and prev_entry_i <= #lines then
    assert(type(left) == 'string', 'expected string left')

    local remains = M.trim(lines[#lines]:sub(col + 1))
    lines[#lines] = remains
    lines[prev_entry_i] = lines[prev_entry_i] .. ' ' .. left
    local lc_left = left:sub(-1, -1)
    local sentence_end = end_of_sentence[lc_left] ~= nil
    if sentence_end then
      lines[#lines] = M.mk_upper_first_letter(remains)
    end
    M.recognize_sentence_start(lines, prev_entry_i)

    local prev_timepos_lnum = -1
    if sync_time and (sentence_end or in_sentence_sep[lc_left]) then
      log_debug('detected punctuation mark in sentence')
      local prev_tp_i = M.indexof_prev_timepos(lines, prev_entry_i)
      if prev_tp_i > -1 then
        prev_timepos_lnum = lnum_start + prev_tp_i
      end
    end

    vim.api.nvim_buf_set_lines(bufnr, lnum_start, lnum, true, lines)

    return true, prev_timepos_lnum
  end

  return false, 'not found prev entry ' .. v2s(prev_entry_i)
end

--
---@param lines table
function M.set_endtimestamp_for(lines)
  assert(type(lines) == 'table', 'lines')
  if #lines < 3 then
    return nil, 'minimal is 3 lines got: ' .. v2s(#lines)
  end
  local ts, te = string.match(lines[1], M.vvt_pattern)
  if not ts or not te then
    return nil, 'expeted under the timestamp of entry got: ' .. v2s(lines[1])
  end

  local t = {
    lines[1], -- timestamp
    lines[2], -- text
  }
  local ts2, te2

  for i = 3, #lines do
    local line = lines[i]
    ts2, te2 = M.parse_timestamp(line)
    if ts2 and te2 then
      t[#t + 1] = te .. ' --> ' .. te2
      break
    else
      t[#t + 1] = line
    end
  end

  if not ts2 then
    return nil, 'not found timestamp of the next entry'
  end

  return t
end

---@param timepos number
---@param for_start boolean?
function M.update_timestamp_in_current_line(timepos, for_start)
  assert(type(timepos) == 'number', 'timepos')

  local bufnr, lnum, _ = M.get_bufnr_lnum_col()
  if not bufnr or not lnum then
    return false, 'cannot get current cursor_pos in the nvim'
  end

  local line = vim.api.nvim_get_current_line()
  local sts, ste = M.parse_timestamp(line)
  if not sts or not ste then
    return false, 'you need the cursor to be on the line with the timestamp'
  end
  local ts, te
  if for_start then
    ts, te = timepos, ste
  else
    -- for end
    ts, te = sts, timepos
  end
  line = M.build_timestamp_line(ts, te)

  vim.api.nvim_buf_set_lines(bufnr, lnum - 1, lnum, true, { line })

  return true, line
end

-- from the cursor in the nvim buffer
---@param n number? lines to translate
---@return string
---@param limit number?
function M.get_current_sentence(n, limit)
  local bufnr, lnum, _ = M.get_bufnr_lnum_col()

  local ln, s = lnum, ''
  local c = 0

  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum + BSZ, false)
  local cnt = vim.api.nvim_buf_line_count(bufnr)

  while ln < cnt do
    local has = false
    for _, line in ipairs(lines) do
      local timestamp, _ = M.parse_timestamp(line)
      if not timestamp and line ~= '' then
        if s ~= '' then s = s .. ' ' end
        s = s .. line
        c = c + 1
        if (n and c >= n) or
            (not n and end_of_sentence[s:sub(-1, -1)]) or
            (limit and c >= limit) then
          has = true
          break
        end
      end
    end
    ln = lnum + BSZ
    if has then
      break
    end
    --  next portion
    lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum + BSZ, false)
  end

  return s
end

--
-- to get the sub entry with order number, start, end, text
--
---@return table?{index, lnum}
---@return string? errmsg
function M.get_current_subt_entry()
  local bufnr, curr_lnum, _ = M.get_bufnr_lnum_col()

  --  "00:19:11.160 --> 00:19:14.420",
  local max_lnum = math.min(curr_lnum + 32, vim.api.nvim_buf_line_count(bufnr))
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, max_lnum, false)

  if not lines then
    return nil, 'cannot get the lines of buffer'
  end
  -- local entry, prev = {}, nil
  local time_patt = M.vvt_pattern
  local idx = 0
  local lnum, prev_sts, prev_ets

  for i = 0, curr_lnum do -- #lines
    local line = lines[i] or ''
    if line and line ~= '' then
      local sts, ets = match(line, time_patt) -- M.parse_timestamp(line)
      if sts and ets then
        prev_sts, prev_ets = sts, ets
        idx = idx + 1
        lnum = i
      end
    end
  end
  if not prev_sts or not prev_ets then
    return nil, 'cannot find time range of subs entry'
  end

  local text, lnum_end = {}, lnum
  for i = lnum + 1, max_lnum do
    local line = lines[i]

    -- is a time-range with next entry?
    if match(line, time_patt) ~= nil then
      break
    end
    text[#text + 1] = line
    lnum_end = i
  end
  -- trim
  while text[#text] == '' do
    text[#text] = nil
    lnum_end = lnum_end - 1
  end

  local entry = {
    index = idx,
    tstart = prev_sts,
    tend = prev_ets,
    text = text,
    lnum = lnum, -- with time range of the entry
    lnum_end = lnum_end,
  }
  return entry, nil
end

--
-- get duration of the subtitle entry beetwen time range
--
---@param subt_entry table?{index, tstart, tend}
---@return number
---@return number start timestamp
---@return number end timestamp
function M.entry_get_duration(subt_entry)
  assert(type(subt_entry) == 'table', 'e')
  local sts = M.str_timestamp_to_num(subt_entry.tstart) or 0
  local ets = M.str_timestamp_to_num(subt_entry.tend) or 0
  return ets - sts, sts, ets
end

---@return table?
---@return string? errmsg
function M.parse_file(subtfn)
  local h, err = io.open(subtfn, 'rb')
  if not h then
    return nil, err
  end
  local t = {}

  local time_patt = M.vvt_pattern
  local idx, i = 0, 0
  local entry = nil -- current entry

  while true do
    i = i + 1
    local line = h:read("*l")
    if not line then
      break
    end

    if line ~= '' then
      local sts, ets = match(line, time_patt) -- M.parse_timestamp(line)
      -- head with time range of the subtitle
      if sts and ets then
        idx = idx + 1
        if entry ~= nil then
          entry.lnum_end = i - 1
          t[#t + 1] = entry
          entry = nil
        end
        entry = {
          index = idx,
          tstart = sts, -- to seconds
          tend = ets,
          text = {},
          lnum = i,       -- with time range of the entry
          lnum_end = nil, -- not yet known
        }
      elseif entry then   -- current line is a text
        entry.text[#entry.text + 1] = line
      end
    end
  end
  if entry and t[#t] ~= entry then
    t[#t + 1] = entry
  end

  return t
end

return M

-- 12-03-2024 @author Swarg
-- Goal:
-- NewStuff Command handler for ExGen
--  - generate new module by fullname

local log = require('alogger')
local base = require("env.lang.utils.base")
local ucmd = require 'env.lang.utils.command'
local ClassResolver = require("env.lang.ClassResolver")
local ExUnit = require 'env.langs.elixir.ExUnit'

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvNew <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('New Stuff for elixir-lang')
      :handlers(M)

      :desc('Generate new module')
      :cmd("module", "m")

      :desc('Create new test-case file. (many-tests-for-one-source)')
      :cmd("test-case", 'tc')

      :desc('Create new mix.exs with Project config')
      :cmd("mix-exs", 'me')

      :desc('Create new test_helper.exs with ExUnit sturtup config')
      :cmd("test-helper", 'th')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local defineStdGenOpts = ucmd.defineStdGenOpts

-- aka class in OOP
---@param w Cmd4Lua
function M.cmd_module(w)
  local gen = w:var('gen') ---@type env.langs.elixir.ExGen
  defineStdGenOpts(w) --dry-run --quiet --autho --no-date --not-open --force-rewrite


  local modulename = w:desc('module name'):pop():arg() -- aka classname

  w:tag('class_doc'):v_opt('--module-doc', '-D')
  w:v_opt('--imports', '-i')
  w:v_opt('--extends', '-e') -- inheritance
  w:v_opt('--implements', '-m')

  w:tag('test_class'):has_opt('--test', '-t', 'generate a test file')

  if not w:is_input_valid() then return end
  ---@cast modulename string

  log.debug('New ModuleName to Generate: %s', modulename)
  local new_ci = ClassResolver.of(gen.lang):lookupByClassName(modulename)

  gen:createClassFile(new_ci, w:get_vars())
end

--
-- Create new test-case file. (many-tests-for-one-source)
--
-- new test-case - in subdir with source-file name
-- dy default test-case will be named from the method name under the cursor
--
---@param w Cmd4Lua
function M.cmd_test_case(w)
  local gen = w:var('gen') ---@type env.langs.elixir.ExGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  w:desc('test-case name i.g. method name')
      :v_opt('--name', '-n') -- support name with subdirs:  sub1/sub2/

  if not w:is_input_valid() then return end

  return gen:createNewTestCaseFile(w.vars)
end

--
-- Create new mix.exs with Project config and another configs(test,formatter)
--
---@param w Cmd4Lua
function M.cmd_mix_exs(w)
  local gen = w:var('gen') ---@type env.langs.elixir.ExGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  if not w:is_input_valid() then return end

  gen.lang:createProjectConfigs(w.vars)

  w:say(gen:getReadableSaveReport("Configs:"))
end

--
-- Create new test_helper.exs with ExUnit sturtup config
--
---@param w Cmd4Lua
function M.cmd_test_helper(w)
  local gen = w:var('gen') ---@type env.langs.elixir.ExGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  if not w:is_input_valid() then return end

  local code, path = ExUnit.createExUnitConfig(gen:getFileWriter(), w.vars)
  print(base.save_file_codes[code], path)
end

return M

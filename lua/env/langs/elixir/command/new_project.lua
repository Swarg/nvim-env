-- 16-07-2024 @author Swarg
--  Just a stub
--  use build system or framework to generate new project
--  TODO minimalistic elixir&mix.exs project

local fs = require 'env.files'
local ucmd = require 'env.lang.utils.command'
local Lang = require 'env.lang.Lang'
local ExLang = require 'env.langs.elixir.ExLang'

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvProject new <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('generate mix project with mix-exs')
      :cmd('mix-exs', 'm')

  -- :cmd('phoenix', 'ph')

      :desc('Generate Flat "Project" (RawSnippets in one directory)')
      :cmd('flat', 'f')

      :run()
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

local defineStdGenOpts = ucmd.defineStdGenOpts
--
-- generate mix project with mix-exs
--
---@param w Cmd4Lua
function M.cmd_mix_exs(w)
  defineStdGenOpts(w)
  if not w:is_input_valid() then return end

  error('Not implemented yet')
end

--
-- Dummy, in-memory project(flat)
-- TODO with makefile?
--
-- Generate Flat "Project" (RawSnippets in one directory)
-- for learning purposes
-- Goal: provide jumping from the sources to the tests files, back and so on.
-- In Memory Only
--
---@param w Cmd4Lua
function M.cmd_flat(w)
  w:v_opt_quiet('-Q')
  local cwd = ucmd.get_current_directory()
  local dir = w:desc('the project root directory'):def(cwd):opt('--dir')

  if not w:is_input_valid() then return end

  local project_root = fs.ensure_dir(dir) --fs.extract_path(fn))
  local lang0 = Lang.getOpenedProject(project_root)
  if lang0 then
    return w:error('Project already exists for dir: ' ..
      tostring(lang0:getProjectRoot()))
  end

  -- Lang.getLang(fn) -- find Lang for ext of the current buffer
  local opts = { flat = true, inmem = true }
  local lang = ExLang:new():newProject(project_root, opts)

  w:say('New Flat Elixir Project Initialized (InMemoryOnly): ', lang.project_root)
end

return M

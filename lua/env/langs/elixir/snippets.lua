-- 15-03-2024 @author Swarg
-- snippets for EnvLineInsert for Elixir Lang
--
--
--
--

local M = {}

local insert_items = {
  "  @moduledoc \"\"\"\n  \"\"\"\n", --1
  "  @doc \"\"\"\n  \"\"\"\n",
  "\n  @spec ${PREV_WORD}(${WORD} :: String.t()) :: String.t()",
  "\n  def ${WORD} do\n  \n  end", -- 4
  "${TAB} dbg(${WORD})\n",           -- 5  IO.inspect(${WORD})
  "",
  "",
  "",
  "",
  "\n  test \"${WORD}\" do\n    assert x == ${WORD}()\n  end\n", -- 10
  "",
  "",
  "",
  "${MODULE}", -- 14
  "",
  "",
  ",${WORD}:",
  "",
  '${HANDLER_MK_FILE_REF}',                -- 19
  "",                                      -- 20
  "assert ${LINE} ==",                     -- 21
  '    assert ${WORD} == \n',
  '    res = ${WORD}\n  assert res == \n', -- 23
  "",
  "",
  "",
  "",
  "",
  "",
  '\n-- ${DATE} @author ${AUTHOR} \ndefmodule ${SWORD} do\nend', -- 30
  "${TOGGLE_BOOL}",                                              -- 31
  "'${WORD}'",                                                   -- 32
  "'${SWORD}'",                                                  -- 33
  '{${SWORD}}',                                                  -- 34
  "  dbg(${WORD})\n",                                            -- 35
}

local common = require 'env.lang.utils.common'

local insert_mappings = {
  -- TODO
}
local insert_handlers = {
  MK_FILE_REF = common.handler_mk_file_ref,
}

---@param reg function
function M.add(reg)
  reg({ 'ex', 'exs' }, insert_items, insert_mappings, insert_handlers)
end

return M

-- 12-03-2024 @author Swarg

local class = require 'oop.class'

-- local fs = require('env.files')
-- local R = require 'env.require_util'


class.package 'env.langs.elixir'
---@class env.langs.elixir.ExUnit : env.lang.Lang
---@field new fun(self, o:table?): env.langs.elixir.ExUnit -- Constructor(_init)
---@field attr ?table
---@field testsuites ?table
---@field source ?table
local ExUnit = class.new_class(nil, 'ExUnit', {
  executable = "mix test"
})

-- cmd: elixir -r my_module.ex -r test_helper.exs my_module_test.exs
local template_test_helper = [[
ExUnit.start()
ExUnit.configure(exclude: :pending, trace: true, seed: 0)
]]

--
-- for ExLang.createProjectConfigs
--
-- config for test framework
---@param fw env.lang.FileWriter
---@param opts table
function ExUnit.createExUnitConfig(fw, opts)
  opts.fn = opts.fn or 'test_helper'
  local test_dir = opts.test or '' -- lang.test or builder.test
  local fn = test_dir .. opts.fn   -- lang.test
  return fw:createFile(fn, 'exs', template_test_helper, opts)
end

class.build(ExUnit)
return ExUnit

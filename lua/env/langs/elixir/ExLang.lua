-- 12-03-2024 @author Swarg
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')
local ExGen = require("env.langs.elixir.ExGen")
local ExUnit = require 'env.langs.elixir.ExUnit'
local MixBuilder = require("env.langs.elixir.MixBuilder")
local ClassResolver = require('env.lang.ClassResolver')

local Lang = require('env.lang.Lang')
local base = require("env.lang.utils.base")


class.package 'env.langs.elixir'
--- define class ExLang extends Lang
---@class env.langs.elixir.ExLang : env.lang.Lang
---@field new fun(self, o:table?, project_root:string?, builder:env.lang.Builder?): env.langs.elixir.ExLang
---@field getLangGen fun(self): env.langs.elixir.ExGen
---@field getProjectBuilder fun(self): env.langs.elixir.MixBuilder
---@field executable      string   bin of the lang interprater
---@field namespace_map   table    created by MixBuilder TODO
---@field modules_test_namespaces table - created after findTestFramework() TODO
---@field src string
---@field test string
---@field last_lsp_update number   time of a setup settings (os.time())
--                                 used to update already exists settings only
--                                 when rockspec file was changed
local ExLang = class.new_class(Lang, 'ExLang', {
  ext = 'ex', -- extension of source files  how about exs?
  ext2 = 'exs',
  executable = 'elixir',
  root_markers = { '.git', 'mix.exs' }, --, 'Makefile' }, -- test? lib?
  -- Note: there must be a slash at the end of the path
  src = 'lib' .. fs.path_sep,
  test = 'test' .. fs.path_sep,
  test_suffix = '_test',
})

Lang.register(ExLang)

local E, log_debug = {}, log.debug

--------------------------------------------------------------------------------
--                                 INIT
--------------------------------------------------------------------------------

-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework
---@param fn string filename
---@return boolean
function ExLang.isLang(fn)
  if fn then
    return fn:match('%.exs?$') ~= nil -- .ex or .exs
  end
  return false
end

--
-- createBuilder for new generate Project
--
-- only instance in memory
-- use cases:
--  first: intended to create a flat project manualy via cli
--  second generate new project for cwd via cli
--
---@param project_root string
---@param opts table?
---@return table properties to synck
function ExLang:createBuilder(project_root, opts)
  log.debug("createBuilder", project_root)
  opts = opts or {}

  local props = {
    project_root = project_root,
    buildscript = opts.buildscript or 'mix.exs',
    src = 'lib' .. fs.path_sep,
    test = 'test' .. fs.path_sep,
  }

  self.builder = MixBuilder:new(props)

  -- flat project
  if opts.flat then
    props.src = opts.src or './'
    props.test = opts.test or './'

    -- if opts.RAW_SNIPPETS then
    self.builder:setRawSnippets()
  end

  -- itself:  minimal sync for testing
  self.project_root = project_root
  self.src, self.test = props.src, props.test

  -- test framework

  return props
end

--
-- createMixExsConfig + ExUnit test_helper + Formatter + .gitignore
--
---@param pp table?{fn} the kvmap(properties) of the new project
---@return self
function ExLang:createProjectConfigs(pp)
  log_debug("createProjectConfigs")
  pp = pp or {}
  pp.buildscript = pp.buildscript or 'mix.exs' -- fn
  pp.project_name = pp.project_name or 'Project'
  pp.app_name = pp.app_name or 'app'
  pp.version = pp.version or '0.1.0'
  pp.elixir_version = pp.elixir_version or '~> 1.16'
  pp.deps = pp.deps or ''
  pp.application = pp.application or ''
  pp.extra_apps = pp.extra_apps or ''

  -- lines what will be added to project props
  pp.project_props = pp.project_props or ''

  -- can be used to make flat project structure
  pp.elixirc_paths = pp.elixir_paths or '"./lib"'
  pp.test_paths = pp.test_paths or '"./test"'

  if pp.flat_project then
    pp.elixirc_paths, pp.test_paths = '"./"', '"./"'
  end
  local builder = self:getProjectBuilder()
  local fw = builder:getFileWriter()

  if builder:createBuildScript(pp, nil) > 0 then   -- mix.exs
    ExUnit.createExUnitConfig(fw, pp.exunit or {}) -- testframework
    ExGen.createFormatterConfig(fw, pp)
    fw:createFile('.gitignore', '', ExGen.template_gitignore, pp)
    -- self:createMakefile(opts)
    -- README.md ?
  end

  return self -- self:getSavedStateCount()
end

--
--
-- used at Lang:resolve()
function ExLang:findProjectBuilder()
  log.debug("findProjectBuilder %s", (self or E).project_root)
  self.builder = nil
  local userdata = {} -- to pass src, test, project_root

  -- if MakeBuilder.isProjectRoot(self.project_root, userdata) then -- raw
  --   self.builder = MakeBuilder:new(userdata)
  --   self.builder:getSettings()
  --   --
  if MixBuilder.isProjectRoot(self.project_root, userdata) then
    self.builder = MixBuilder:new(userdata)
    self.builder:getSettings()
    --
  else
    log.debug('[WARN] No MixProject for', self.project_root)
    return self
  end

  return self:syncSrcAndTestPathsWithBuilder() -- sync Lang and Builder
end

-- used at Lang:resolve
function ExLang:findTestFramework()
  self.testframework = nil

  -- TODO
  -- if self.project_root then
  --   local script = fs.join_path(self.project_root, 'mix.exs')
  --   if fs.file_exists(script) then
  --     self.testframework = Mix:new():setProjectRoot(self.project_root)
  --     self.testframework:getConfig()
  --   end
  -- end
  return self
end

--------------------------------------------------------------------------------
--                        TEST FRAMEWORK
--------------------------------------------------------------------------------

---@param ns string
---@return string?, string?  -  testsuite_name, test_path
---@diagnostic disable-next-line: unused-local
function ExLang:getTestSuiteName(ns)
  -- TODO
  -- local tfw = self:getTestFramework()
  -- if tfw and class.instanceof(tfw, Mix|Raw) then
  --   return tfw:findTestSuiteNameFor(ns)
  -- end
end

--
-- usecase: many sources with diff iterations for one test file
--
-- module_i1.ex -> module_i1_test.exs
-- module_i2.ex -> module_i1_test.exs
--
---@param self self
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo
local function findExistedFileForRawSnippet(self, ci, ctype)
  local path = ci ~= nil and ci:getPath() or nil
  local is_test = path and ci:isTestClass()
  local has_pair = path and ci:getPair() ~= nil
  log.debug("findPairedFileForRawSnippet", path, is_test, ctype, has_pair)

  if path and is_test then
    local fn, itern = string.match(path, '^(.+)_i(%d+)_test.exs$')
    log.trace('match give fn:%s itern:%s', fn, itern)
    itern = tonumber(itern)
    ctype = ctype -- todo

    while fn and itern and itern >= 0 do
      itern = itern - 1
      local suff = itern > 0 and ('_i' .. itern .. '_test.exs') or '_test.exs'

      local path0 = path:gsub('_i(%d+)_test.exs$', suff)
      local exists = fs.file_exists(path0)
      log.trace('file %s exists: %s', path0, exists)

      if exists then
        log.debug('File Found: %s', path0)
        local ci0 = ClassResolver.of(self):lookupByPath(path0)
        ci0 = assert(ci0, 'new ClassInfo should exists')
        ci0:bindPair(ci:getPair())
        return ci0
      end
    end
  end

  return ci
end

--
-- fired from ClassResolver when not found existed file for given paired CI
--
---@param ci env.lang.ClassInfo
---@param ftype number? ClassInfo.CT(CT_SOURCE, CT_TEST, CT_OPPOSITE)
---@return env.lang.ClassInfo
function ExLang:onResolveCINoExistedPath(ci, ftype)
  log.trace("onResolveCINoExistedPath", ftype)

  if ci and MixBuilder.isRawSnippets(self.builder) then
    return findExistedFileForRawSnippet(self, ci, ftype)
  end

  return ci
end

--------------------------------------------------------------------------------

--
--  lib/some_module_name -->  lib/SomeModuleName
--
-- inner_path is a path inside the project without project_root and extension
-- in modular project it src + modulename + namespace
-- in lua src dir does not maps to namespace
--
--- override
---@param ns string? -- the namespace or innerpath (without root and extendsion)
---@return string?
function ExLang:getClassNameForInnerPath(ns)
  if ns then
    local cn, pref = nil, ''
    local src, test = self:getSrcPath(), self:getTestPath()

    if self.isFlatDirPart(ns, src, test) then
      -- self:isTestInnerPath(ns)
      cn = ns -- no prefix path
    else
      if su.starts_with(ns, src) then
        pref = self:getSrcPath()
      elseif su.starts_with(ns, test) then
        pref = self:getTestPath()
      end

      cn = ns:sub(#pref + 1)
    end
    cn = base.toPascalCase(cn)
    return fs.path_to_classname(cn, '%.') -- class_name
  end

  return ns
end

--
--  lib/SomeModuleName -> lib/some_module_name
--  test/SomeModuleTest -> test/some_module_test
--
--- override
---@param cn string -- classname
---@return string
function ExLang:getInnerPathForClassName(cn)
  if cn then
    if su.ends_with(cn, self.test_suffix) then
      local fn = base.pascalCaseToSnake(fs.classname_to_path(cn, '%.') or '')
      return fs.join_path(self.test, fn)
      --
    elseif self.builder then
      local cn0 = self.builder:findInnerPathForModule(cn)
      if cn0 then
        return base.pascalCaseToSnake(cn0)
      end
    end

    local path0 = fs.classname_to_path(cn, '%.') or ''
    local fn = base.pascalCaseToSnake(path0)
    local innerpath = fs.join_path(self:getSrcPath(), fn)
    return innerpath -- relative path in the project
  end

  return cn
end

--- override
---@param filename string
---@return boolean
function ExLang:isSrcPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getSrcPath())
  end
  return false
end

--- override
---@param filename string
---@return boolean|nil
function ExLang:isTestPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    if self:hasTestFramework() then
      return self:getTestSuiteName(ns) ~= nil
    else
      return su.starts_with(ns, self:getTestPath())
    end
  end
end

-- check is a class are test class from some configured testsuite path
-- if so then pass testsuite path and name into classinfo
--
-- Lookup goes through InnerPath
--
-- findout test suites for given inner-path(namespace)
-- check is classinfo from some testsuite namespace (test-class)
-- then addd testsuite_name and testsuite_path into classinfo
---@param classinfo env.lang.ClassInfo
---@return boolean
function ExLang:lookupTestSuite(classinfo)
  local ns = classinfo:getInnerPath()
  log.debug("lookupTestSuite inner:", ns)
  if ns then
    local testsuite_name, testsuite_path

    if self:hasTestFramework() then
      log.trace("take testsuite from TestFramework")
      testsuite_name, testsuite_path = self:getTestSuiteName(ns)
      --
    elseif su.starts_with(ns, self.test) then
      log.trace("set default testsuite")
      testsuite_name, testsuite_path = 'unit', self.test -- ?
    end

    if testsuite_name and testsuite_path then
      classinfo:setTestSuite(testsuite_name, testsuite_path)
      return true
    end
  end

  log.trace("classinfo is not a test class")
  return false
end

--
-- a simple and straightforward classname mappings: one source to one test
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- source-class: app.module.init
-- return      : app.module.init_test
--
-- Note: self.test_suffix used for filename and here is "_test"  but!
-- for ModuleNames used "Test" suffix
--
--- override
---@param src_classname string
function ExLang:getTestClassForSrcClass(src_classname)
  local tcn = Lang.getWithSuffix(src_classname, "Test") -- not selt.test_suffix
  log.debug("getTestClassForSrcClass scn:", src_classname, self.src, tcn)
  return tcn
end

--
-- a simple and straightforward classname mappings: one test to one source
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- test-class: app.module.init_spec       (class ie module)
-- return    : app.module.init
--
-- Note: self.test_suffix used for filename and here is "_test"  but!
-- for ModuleNames used "Test" suffix
--
--- override
---@param test_classname string
function ExLang:getSrcClassForTestClass(test_classname)
  -- here don't use self.test_suffix because here, for elixir its used for
  -- file name not for Module name:
  -- module(class)name has "Test" but filename has "_test"
  local scn = Lang.getPathWithoutSuffix(test_classname, "Test")
  log.debug('getSrcClassForTestClass', test_classname, scn)
  return scn
end

--------------------------------------------------------------------------------
--                            USE CASES
--------------------------------------------------------------------------------
-- See env.lang.Lang.lua


---@param tci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function ExLang:getArgsToRunSingleTestFile(tci)
  log.debug("getArgsToRunSingleTestFile")
  self:ensurePathExists(tci)

  -- test framework bin
  local cmd, args = nil, nil
  local path = tci:getPath() -- :getInnerPath() .. '.' .. self.ext

  -- Note:
  --  FlatProject can be simple project where sources related together
  --  (iex -S mix and one namespace for module names) -> can mix test ...
  --  RawSnippets - is a set of independed sources and testfiles
  --  (Can content multiple files of same code (iterations) the goal
  --  provide way to run test directly via elixir without `mix test`
  if MixBuilder.isRawSnippets(self.builder) then -- can used without mix.exs
    cmd = 'elixir'
    local sci = tci:getPair() or ClassResolver.of(self):getOppositeClass(tci)
    if not self:isPathExists(sci) then
      error('Not Found paired src. If you run a raw-snippets run it from src-file')
    end

    assert(sci, 'should find a source file for test file')
    local optkey, test_helper = '-r', self.builder:getTestHelperPath()
    assert(test_helper, 'test_helper.exs should exists')

    -- if not test_helper then -- seems it doesnt work properly
    --   optkey, test_helper = '--eval', 'ExUnit.start()'
    -- end -- try run test without test_helper.exs

    -- elixir -r my_module.ex -r test_helper.exs my_module_test.ex
    args = { '-r', sci:getPath(), optkey, test_helper, path }
    -- ExUnit autocheck file: test/test_helper.exs
  else
    cmd = self:getTestFrameworkExecutable() or 'mix' -- + test
    args = { 'test', path }
  end

  return self:newExecParams(cmd, args)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function ExLang:getArgsToRunSingleSrcFile(ci)
  log.debug("getArgsToRunSingleSrcFile")
  self:ensurePathExists(ci)

  -- interpreter bin
  local bin = self.executable or 'lua'
  local ext = self.ext

  -- to support multiple extensions ext2. asume path is to ensure existed file
  if ci:getPath() then ext = fs.extract_extension(ci:getPath()) or ext end

  local path = ci:getInnerPath() .. '.' .. ext
  local cmd, args = bin, { path }

  return self:newExecParams(cmd, args)
end

---@return env.langs.elixir.ExGen
function ExLang:newLangGen()
  self.gen = ExGen:new({ lang = self })
  return self.gen
end

---@param innerpath string
---@param ext string?
function ExLang:getFullPathForInnerPath(innerpath, ext)
  ext = ext or self.ext
  if self.ext2 and su.ends_with(innerpath, self.test_suffix or '_test') then
    ext = self.ext2
  end
  return fs.join_path(self.project_root, innerpath) ..
      (ext == '' and '' or '.' .. ext)
end

class.build(ExLang)
return ExLang

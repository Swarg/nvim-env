-- 12-03-2024 @author Swarg
local class = require 'oop.class'

local log = require('alogger')

local lapi_consts = require 'env.lang.api.constants'
local AccessModifiers = require("env.lang.oop.AccessModifiers")
local LangGen = require("env.lang.oop.LangGen")


class.package 'env.langs.elixir'
---@class env.langs.elixir.ExGen : env.lang.oop.LangGen
---@field lang env.langs.elixir.ExLang?
---@field ext string
---@field tab string
---@field default_field_type string
---@field default_field_amod number  -- access modifier (private|public, etc)
local ExGen = class.new_class(LangGen, 'ExGen', {
  lang = nil,
  ext = 'ex', -- extension of source files
  src = 'lib',
  test = 'test',
  tab = '  ',
  default_field_type = 'String.t',
  default_field_amod = AccessModifiers.ID.PRIVATE,

  cmdhandler_new_stuff = "env.langs.elixir.command.new_stuff", -- :EnvNew ..
})

local LT, E = lapi_consts.LEXEME_TYPE, {}


--
-- take classname from current line with use(import)(from sourcefile via editor)
function ExGen:getClassNameFromLine()
  local line = self:getContext().current_line
  if line then
    -- use ModuleName
    local classname = line:match('^%s*use ([^%s]+)$')

    return classname
  end
end

local template_method = [[
  ---@spec ${RETURN_TYPE}
  def${ACCESSMOD} ${NAME}(${PARAMS}) do
${BODY}
  end
]]

local template_constructor = [[
TODO
]]

local template_field = [[    ${ACCESSMOD} ${TYPE} ${NAME}${DEFAULT_VALUE};]]

local def_exunit_test_imports = [[
  use ExUnit.Case
]]


local template_test_file = [[
#${DATE}${AUTHOR}
defmodule ${FULL_CLASSNAME} do
${IMPORTS}

  describe "${PAIR_CLASSNAME}" do

    # @tag :pending
    test "success" do
      assert 1 + 1 == 2
    end
${FIELDS}
${METHODS}
  end
end
]]

-- as "method" of the test-class in oop
local template_test_block = [[
  test "${NAME}" do
${BODY}
  end
]]



local template_class = [[
#${DATE}${AUTHOR}

defmodule ${FULL_CLASSNAME} do
  @moduledoc """
${CLASS_DOC}
  """
${FIELDS}
${METHODS}
end
]]

---  see template_mix_exs  in MixBuilder

-- from `mix new`
ExGen.template_gitignore = [[
# The directory Mix will write compiled artifacts to.
/_build/

# If you run "mix test --cover", coverage assets end up here.
/cover/

# The directory Mix downloads your dependencies sources to.
/deps/

# Where third-party dependencies like ExDoc output generated docs.
/doc/

# Ignore .fetch files in case you like to edit your project deps locally.
/.fetch

# If the VM crashes, it generates a dump, let's ignore it too.
erl_crash.dump

# Also ignore archive artifacts (built via "mix archive.build").
*.ez

# Ignore package tarball (built via "mix hex.build").
*-*.tar

# Temporary files, for example, from tests.
/tmp/
]]


local template_formatter = [==[
# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,${SRC},${TEST}}/**/*.{ex,exs}"]
]
]==]

local template_makefile = [==[
]==]
--[[ Example of Makefile for raw src+test in same dir without mix.exs
init:
	mkdir _build/

help:
	echo init clean solution

solution:
	elixirc solution.ex -o ./_build && elixir -pa ./_build solution_test.exs

clean:
	rm _build/*.beam
]]


-- override
-- class aka module
---@param ci env.lang.ClassInfo?
function ExGen:getClassTemplate(ci)
  if ci and ci:isTestClass() then return template_test_file end
  return template_class
end

---@param ci env.lang.ClassInfo?
function ExGen:getMethodTemplate(ci)
  if ci and ci:isTestClass() then return template_test_block end
  return template_method
end

---@diagnostic disable-next-line: unused-local
function ExGen:getConstructorName(classinfo) return '' end

---@return string
function ExGen:getFieldTemplate() return template_field end

---@return string, string template + separator
function ExGen:getParamTemplate() return '${NAME}', ',' end

function ExGen:getKWordThis() return 'self' end -- ?

function ExGen:getKWordBoolean() return 'boolean' end

---@return string
function ExGen:getVarPrefix() return '' end

---@return string
function ExGen:getReturnPrefix() return '' end

---@return string
function ExGen:getAttrDot() return '.' end

---@return string
function ExGen:getOpCompareIdentical() return '==' end

---@return string
function ExGen:getConstructorTemplate() return template_constructor end

---@return string
function ExGen:getGetterTemplate() return '' end

---@return string
function ExGen:getSetterTemplate() return '' end

---@return string
function ExGen:getEqualsTemplate() return '' end

---@return string
function ExGen:getDefaultFieldType() return self.default_field_type end

---@return number
function ExGen:getDefaultFieldAMod() return self.default_field_amod end

--------------------------------------------------------------------------------



---@param tci env.lang.ClassInfo
---@param opts table?
function ExGen:genOptsForTestFile(tci, opts)
  log.debug("createOptsForTestFile")
  assert(tci ~= nil)

  local sci, c, tmn, t, body, exp, res
  sci = tci:getPair()
  assert(sci, 'source classinfo')

  opts = opts or {}
  opts.test_class = true

  opts.imports = def_exunit_test_imports ..
      '  import ' .. sci:getClassName() -- TODO

  -- method|function name in source file under the cursor
  tmn, t, body = 'success', self:getTab(), ''
  c = self:getContext(true):resolveWords()
  log.debug('ctx', c)

  if c.lexeme_type == LT.FUNCTION or c.lexeme_type == LT.METHOD then
    tmn = c.word_element
    log.debug("has method under the cursor:", tmn, c.lexeme_type)

    if c.lexeme_type == LT.FUNCTION then
      body = ("%s%slocal res = M.%s()\n"):format(t, t, tmn)
    else
      body = ("%s%slocal res = %s:%s()\n"):format(t, t, c.word_container, tmn)
    end

    body = body .. ("%s%slocal exp = 1\n"):format(t, t)
    exp, res = "exp", "res"
    self:getEditor():setSearchWord(tmn) -- to jump to a generated test method name
    -- local obj_vn = ClassInfo.getVarNameForClassName(tcn_short)
  else
    exp, res = '1', '0'
  end

  body = body .. string.format("%s%sassert.same(%s, %s)", t, t, exp, res)

  opts.methods = { self:getMethodGen(tci):genTest(tmn, false, body) }
  return opts
end

--
-- for createProjectConfigs
--
---@param opts table{flat_project}
function ExGen.createFormatterConfig(fw, opts)
  local o = {
    src = 'lib',   -- opts.elixirc_paths,
    test = 'test', -- opts.test_paths,
  }
  if (opts or E).flat_project then
    o.src, o.test = '.', '.'
  end
  local fn = '.formatter.exs'
  return fw:createFileFrTempl(fn, template_formatter, o, true)
end

--
-- for flat project
---@param opts table
function ExGen:createMakefile(opts)
  opts = opts or {}
  if opts.flat_project then
    -- no longer so relevant when it was possible to create mix.exs for
    -- project with flat structure (all source and tests in same projroot dir)
    template_makefile = template_makefile
  end
end

class.build(ExGen)
return ExGen

-- 12-03-2024 @author Swarg
-- Goal:
--  - support for mix.exs - a elixir-project configuration file
--  - provide way to run test files for "FlatProject or RawSnippets"
--
local class = require 'oop.class'

local log = require('alogger')
local fs = require('env.files')
local su = require('env.sutil')

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") -- vim.loop

local base = require("env.lang.utils.base")
local Builder = require('env.lang.Builder')

local parser = require 'ex_parser.grammar'

class.package 'env.langs.elixir'
---@class env.langs.elixir.MixBuilder : env.lang.Builder
---@field new fun(self, o:table?, project_root:string?, src:string?, test:string?): env.langs.elixir.MixBuilder
---@field project_root    string
---@field buildscript     string   mix.exs (config-file with a settings)
---@field buildscript_lm  number?  last modify time of the mix.exs-file
--                                 todo: checks changes and reload config
--- field settings        table?   parsed a mix.exs file (buildscript)(config)
---@field workspace_libs  table?   module -> absolute path (deps-mappings)
--                                 mappings for all dependencies(packages)
--                                 specified in the mix.exs-file in secrion
--                                 dependencies. TODO
local MixBuilder = class.new_class(Builder, 'MixBuilder', {
  buildscript = 'mix.exs',
  name = 'mix', -- executable?
  project_props = nil,
})


local E = {}
local log_debug, log_trace = log.debug, log.trace

--
-- public static
-- check is the given directory(!) is Root of Source Code with mix.exs
-- (table)userdata used to pass infos need to build instance of MixBuilder
--
---@param path string path to dir not a file(!)
---@param userdata table|nil output data to build instance
---@return string?  full path to script file or nil
function MixBuilder.isProjectRoot(path, userdata)
  log.debug("MixBuilder.isProjectRoot %s", path)
  if type(path) == 'string' then
    if path:match('mix%.exs$') then
      return path
    end

    local dir = fs.ensure_dir(path)
    local handle, errmsg = uv.fs_scandir(dir)
    log.debug('find at dir %s handle %s', dir, type(handle))
    -- local exists = (stat and stat.type == "directory") == true
    if type(handle) == 'string' or not handle then
      log.debug('Scandir Error %s', errmsg)
      return nil
    end

    local mix = nil
    while true do
      local name, _ = uv.fs_scandir_next(handle)
      if not name then break end

      if name == 'mix.exs' and fs.is_file(fs.join_path(dir, name)) then
        mix = name
        if not userdata then break end

        userdata.buildscript = mix
        userdata.project_root = path
        --
      elseif userdata and (name == 'lib' or name == 'src') then
        userdata.src = userdata.src or (name .. fs.path_sep)
        --
      elseif userdata and (name == 'test') then
        userdata.test = userdata.test or (name .. fs.path_sep)
      end
    end

    if mix then
      return fs.join_path(path, mix)
    end
  end
  return nil
end

-- @Override
--
-- Settings from buildscript (rockspec)
---@return table
function MixBuilder:getSettings()
  if not self.settings then
    Builder.getSettings(self) -- updetes self.settings

    local elixirc_paths = ((self.settings or E).project or E).elixirc_paths
    log_debug('project.elixirc_paths: %s', elixirc_paths)

    if type(elixirc_paths) == 'table' then
      local mainsrc = elixirc_paths[1]
      if mainsrc then
        self.src = mainsrc
        if mainsrc == './' then -- to support "flat" src+test in same dir
          log.debug('Flat Project detected src==test==./')
          self.test = mainsrc
        end
      end
    end
  end
  return self.settings
end

--
-- @Override
--
-- src+dir in same directory
--
---@return boolean
function MixBuilder:isFlatStructure()
  local project = ((self or E).settings or E).project
  return project ~= nil and (project.elixirc_paths or E)[1] == './' and
      (project.test_paths or E)[1] == './'
end


--
-- return alredy lookuped or make lookup of test_helper.exs
--
---@param max_deep number?
function MixBuilder:getTestHelperPath(max_deep)
  local path = self.settings.test_helper
  if not path then
    path = 'test_helper.exs'
    local dir = self:getProjectRoot()

    -- lookup test_helper file
    local fn = fs.join_path(dir, path)
    -- in the root of the project
    if fs.file_exists(fn) then
      self.settings.test_helper = path
    else
      -- in ./test/test_helper.exs
      fn = fs.join_path(dir, self.test, path)
      if fs.file_exists(fn) then
        self.settings.test_helper = fn
      else
        -- in ../test_helper.exs
        -- in ../../test_helper.exs
        local deep, tfn, parent_dir = 0, nil, nil
        max_deep = max_deep or 4
        parent_dir = dir

        while deep < max_deep and parent_dir do
          deep = deep + 1
          parent_dir = fs.get_parent_dirpath(parent_dir)
          if parent_dir then
            tfn = fs.join_path(parent_dir, path)
            if fs.file_exists(tfn) then
              break
            end
            tfn = nil
          end
        end

        if tfn then
          self.settings.test_helper = tfn
        else
          log.debug('Not Found test_helper: %s', path)
        end
      end
    end
  end

  return self.settings.test_helper
end

--
-- Something like a FlatProject but with an even more flexible and simpler
-- structure, for running tests without `mix test some_test.exs`.
--
-- Treat the project as a directory of code snippets created for learning purposes.
-- this is used to run tests for sources without mix.exs (ExLang InMemOnly)
--
function MixBuilder:setRawSnippets()
  self.settings           = self.settings or {}
  -- raw snippets
  local t                 = self.settings
  t.project_name          = t.project_name or 'RawSnippets.MixProject'
  t.project               = t.project or {}
  t.project.app           = t.project.app or ':raw_snippets'
  t.project.elixirc_paths = t.project.elixirc_paths or { './' }
  t.project.test_paths    = t.project.test_paths or { './' }
  t.project.raw_snippets  = true -- used by isRawSnippets and goto src-test
  return self
end

-- parsing mix.exs --> luatable
--[[ stub
  local config = {
    project = {
      app = 'todo',
      version = '0.1.0',
      elixirc_paths = { "./lib" }, -- sources
      deps = {
        -- # {:dep_from_hexpm, "~> 0.3.0"},
        -- # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
      }
    },
    application = {
      extra_applications = { ':logger' }
    }
  }
]]

-- @Override
-- parse content of mix.exs file into a luatable object
-- parse loaded mix.exs file (buildscript)
-- called from Builder.loadBuildScript()
--
---@param path string -- self:getBuildScriptPath()
---@param bin string
---@param prev_lm number
---@diagnostic disable-next-line: unused-local
---@return false|table
---@return string? errmsg
function MixBuilder:parseBuildScript(path, bin, prev_lm)
  log_debug('parseBuildScript %s size:%s prevlm:%s', path, #(bin or ''), prev_lm)

  local ast = parser.parse(bin)
  if not ast then
    error('Cannot parse file: ' .. tostring(path) ..
      " ast:" .. tostring(ast) .. " input:\n" .. tostring(bin))
  end
  log_trace('AST: %s', ast)

  local ctx = parser.mk_ctx(ast, true)
  local ok, obj = pcall(parser.to_obj, ast, ctx)
  if not ok then
    error('Cannot convert AST to object ' .. tostring(obj))
  end

  local config = parser.mix_exs_as_config(obj)
  log.trace('Mix.exs as config: %s', config)

  return config
end

-- config.build.modules
---@return table (readonly)
function MixBuilder:getModulesMap()
  return ((self.settings or E).build or E).modules or {}
end

-- find innner path by module name
-- find module( for cn - classname
--
---@param cn string - classname
---@return string? inner_path without extension
function MixBuilder:findInnerPathForModule(cn)
  log.debug("findInnerPathForModule", cn)
  if cn then
    local fn = base.pascalCaseToSnake(fs.classname_to_path(cn, '%.') or '')
    if su.ends_with(cn, 'Test') then
      return fs.join_path(self.test, fn)
    else
      return fs.join_path(self.src, fn)
    end
  end
end

--
-- TODO
---@diagnostic disable-next-line: unused-local
function MixBuilder:argsTestSingleClass(class_name, opts, method_name)
  if class_name then
    local test = class_name
    -- if defined method name then run test only for this method
    if method_name and method_name ~= '' then
      test = test .. '.' .. method_name
    end
    local args = { "test", "--tests", test, "-i" } -- args
    -- return setup_opts(args, opts)
    return args
  end
  return {}
end

--------------------------------------------------------------------------------
--                          Templates
--------------------------------------------------------------------------------

-- Mix source code here
-- .. /elixir/lang_elixir/lib/mix/lib/mix/tasks/test.ex
--
local template_mix_exs = [==[
defmodule ${project_name}.MixProject do
  use Mix.Project

  def project do
    [
      app: :${app_name},
      version: "${version}",
      elixir: "${elixir_version}",
      elixirc_paths: [${elixirc_paths}],
      test_paths: [${test_paths}],
      ${project_props}
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      ${application}
      extra_applications: [${extra_apps}]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      ${deps}
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/repo/lib.git", tag: "0.1.0"}
    ]
  end
end
]==]

-- APP_NAME:           :my_app
-- VERSION             0.1.0
-- ELIXIR_VERSION      ~> 1.16
-- extra_applications:[:logger]
function MixBuilder:getBuildScriptTemplate()
  return template_mix_exs
end

class.build(MixBuilder)
return MixBuilder

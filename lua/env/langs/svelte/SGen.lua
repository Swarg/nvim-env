-- 29-08-2024 @author Swarg
-- Generate Stuff for Svelte

local log = require 'alogger'
local class = require 'oop.class'

local U = require 'env.langs.svelte.util.gen'

local base = require 'env.lang.utils.base'
local Editor = require 'env.ui.Editor'
local LangGen = require 'env.lang.oop.LangGen'
local ClassInfo = require 'env.lang.ClassInfo'
local AccessModifiers = require 'env.lang.oop.AccessModifiers'
local lapi_consts = require 'env.lang.api.constants'

local ugen = require 'env.langs.svelte.util.gen'
local lexer = require 'env.langs.svelte.util.lexer'
local routing_util = require 'env.langs.svelte.util.routing'
local SParser = require 'env.langs.svelte.SParser'



class.package 'env.langs.svelte'
---@class env.langs.svelte.SGen : env.lang.oop.LangGen
---@field lang env.langs.svelte.SLang?
---@field ext string
---@field tab string
local C = class.new_class(LangGen, 'SGen', {
  lang = nil,
  ext = 'svelte', -- extension of source files
  tab = '  ',
  default_field_type = 'String',
  default_field_amod = AccessModifiers.ID.PRIVATE,

  cmdhandler_new_stuff = "env.langs.svelte.command.new_stuff", -- :EnvNew ..
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug
---@diagnostic disable-next-line: unused-local
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
local LT = LEXEME_TYPE
-- local PAGE_FN = U.PAGE_FN
local find_lnum_of_last_import = ugen.find_lnum_of_last_import


-- todo  update


-- treat Svelte Component like class in other oop lang
-- override
---@param ci env.lang.ClassInfo?
---@diagnostic disable-next-line: unused-local
function C:getClassTemplate(ci) return 'todo' end

function C:getKWordExtands() return ' extends ' end

---@return string
function C:getFieldTemplate() return 'let ${NAME};' end

---@return string, string template + separator
function C:getParamTemplate() return '${NAME}', ',' end

function C:getKWordThis() return 'this' end

function C:getKWordBoolean() return 'boolean' end

---@return string
function C:getVarPrefix() return '' end

---@return string
function C:getReturnPrefix() return '' end

---@return string
function C:getAttrDot() return '.' end

---@return string
function C:getOpCompareIdentical() return '==' end

---@return string
function C:getConstructorTemplate() return '' end

---@return string
function C:getGetterTemplate() return '' end

---@return string
function C:getSetterTemplate() return '' end

---@return string
function C:getEqualsTemplate() return '' end

---@return string
function C:getDefaultFieldType() return '' end

---@return number
function C:getDefaultFieldAMod() return AccessModifiers.ID.PRIVATE end

--------------------------------------------------------------------------------

-- is Component Exists
---@param classname string
---@param ctype number?
function C:isClassExists(classname, ctype)
  log_debug("isClassExists cn:%s ctype:%s", classname, ctype)
  ctype = ctype or ClassInfo.CT.SOURCE
  local ci = self.lang:getClassResolver(ctype, nil, classname)
      :run():getClassInfo(false)

  local b = self.lang:isPathExists(ci)
  return b, ci
end

--------------------------------------------------------------------------------


--
-- create new Component file either for given Componebt or for Compoment under
-- the cursor position in the opened svelte source code
--
---@param self self
function C.newComponent(self, compo_name, opts)
  local editor = self and self:getEditor() or Editor:new()
  local ctx = editor:getContext(true, SParser:new())

  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'empty line expected route'
  end
  opts = opts or {}
  opts.fn = opts.fn or ctx:getCurrentBufname()
  opts.simple = opts.simple == nil and true or opts.simple

  if not compo_name then
    local lt, word, i = ctx:selectLexem():getLexemeType()
    if not lexer.isComponentLexemType(lt) then
      return false, 'expected Component under the cursor, got: ' ..
          v2s(LT[lt or false])
    end
    if type(word) ~= 'string' then
      return false, 'no Component name'
    end
    local lexems = (ctx.selection or E).lexems
    opts.attrs = lexer.getAttribures(lexems, i)
    opts.module = lexer.getImportDefinition(lexems) -- import from
    compo_name = word
  end

  local path, err = ugen.gen_new_compoment_file(compo_name, opts)
  if not path then
    return false, err
  end
  if #(opts.import or E) > 0 then
    local imported = {}
    local lnum = find_lnum_of_last_import(ctx, imported)
    if lnum > 0 and not imported[compo_name] then
      ctx:setLines(lnum + 1, lnum, opts.import)
    end
  end
  editor:open(path, 2)


  return true
end

---@param route string
---@param opts table?
function C:newRoute(route, opts)
  log_debug("newRoute", route)
  local ctx = self:getEditor():getContext(true)
  local fn = ctx:getCurrentBufname()

  local new_route_path = U.mk_route_path(fn, route)

  local body = U.gen_component_body()
  local st = self:getFileWriter():saveFile(false, new_route_path, body, opts)
  if st == base.OK_SAVED then
    return new_route_path
  end
  return false, st
end

---
---@return false|table{lines, routes}
---@return string? errmsg
function C:getRoutesFromSelectedLines()
  local ctx = self:getEditor():getContext(true)
  local lines, ls, le = ctx:getSelectedLines()

  log_debug("getRoutesFromSelectedLines lines:%s %s-%s", #(lines or E), ls, le)

  if not lines or #(lines or E) == 0 then
    return false, 'no selected lines'
  end
  return {
    links = routing_util.get_routes_from_selected_lines(lines),
    lines = lines,
    lnum = ls,
    lnum_end = le
  }
end

--
--  /tutor/page  -->   /tutor/event/page
--
---@param data table{links:table, lines:table, lnum,lnum_end}
---@param subdir string
---@param n number
---@param callback function?(new_href, link_entry{href, lnum}, data:table)
function C:refactorRoutes(data, subdir, n, callback)
  assert(type(subdir) == 'string' and subdir ~= '', 'subdir')
  assert(type(data) == 'table', 'routes_data')
  assert(type(data.lines) == 'table' and #data.lines > 0, 'lines')
  assert(type(data.links) == 'table' and #data.links > 0, 'routes.links')

  local lines = data.lines
  local has_handler = type(callback) == 'function'

  for i, e in ipairs(data.links) do
    log_debug("link #%s: %s", i, e)
    local href, line, lnum = routing_util.refactor_route(lines, e, subdir, n)
    lines[lnum] = line
    if has_handler then ---@cast callback function
      callback(href, e, data)
    end
  end

  return lines
end

class.build(C)
return C

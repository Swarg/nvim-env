-- 29-08-2024 @author Swarg
-- Goal:
-- NewStuff Command handler for SvelteGen

local log = require 'alogger'

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvNew <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('New Stuff for SvelteCode')
      :handlers(M)

      :desc('Generate new svelte component')
      :cmd("component", "c")

      :desc('Generate new route page')
      :cmd("route", "r")

      :desc('change routes(a-href) in the selected lines (with refactoring)')
      :cmd("refactor-routes", "rr")

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------
log.debug("init")

local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug


--
-- Generate new svelte component
--
---@param w Cmd4Lua
function M.cmd_component(w)
  local name = w:desc('name of the component being created'):opt('--name', '-n')
  local attrs = w:desc('names of the attributes'):opt('--attributes', '-a')

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@type env.langs.svelte.SGen
  ---@cast name string
  local opts = {
    attrs = attrs,
    simple = nil,
    fn = nil,
  }
  local ok, err = gen:newComponent(name, opts)
  if not ok then
    return w:error(v2s(err))
  end
  -- open new file if success created
end

--
-- Generate new route page
--
---@param w Cmd4Lua
function M.cmd_route(w)
  local route = w:desc('the relative route to create'):pop():arg()
  local add_url = w:desc('add a-href to new route'):has_opt('--add-url', '-u')

  if not w:is_input_valid() then return end
  ---@cast route string

  local gen = w:var('gen') ---@type env.langs.svelte.SGen

  local path, err = gen:newRoute(route, w.vars)
  if not path then
    return 'Error code:' .. v2s(err)
  end
  if add_url then
    error('Not implemented yet')
    -- local url = fmt("")
    -- local cl = gen:getEditor():getCurrentLine()
    -- gen:getEditor():insertAfterCurrentLine(url)
  end

  gen:getEditor():open(path)
end

---@param new_href string
---@param link table{href, lnum}
---@param data table{lines, links, lnum, lnum_end, gen}
local function refactor_route_callback(new_href, link, data)
  log_debug("refactor_route_callback new:%s link:%s", new_href, link)
  local gen = assert(data.gen, 'data.gen') ---@type env.langs.svelte.SGen

  if new_href and link.href and new_href ~= link.href then
    local new_dir = gen.lang:refactorRoute(new_href, link.href)
    if new_dir then
      data.moved = (data.moved or 0) + 1
    end
  end
end

--
-- refactor routes(a-href) in the selected lines (with refactoring dirs in fs)
-- a href="/parent/page" --> ahref="/parent/something/new/page"
--
-- replace href in selected lines and rename(move) dirs if exists
-- goal:
--  - ability to move all childs in given directory into the new one
--    in filesystem(in src/routes) and in href in the selected lines
--
---@param w Cmd4Lua
function M.cmd_refactor_routes(w)
  local subdir = w:desc("the subdir name to insert"):pop():arg()
  local n = w:desc("the depth in url in add subdir"):type('number'):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast subdir string
  ---@cast n number

  local gen = w:var('gen') ---@type env.langs.svelte.SGen

  n = tonumber(n) or 1

  local t, err = gen:getRoutesFromSelectedLines()
  if not t then
    return w:error(err)
  end
  assert(type(t.lnum) == 'number', 'routes_data.lnum')
  assert(type(t.lnum_end) == 'number', 'routes_data.lnum_end')
  t.gen = gen

  local lines = gen:refactorRoutes(t, subdir, n, refactor_route_callback)
  -- -2 is vim.fn.setpos issue
  gen:getEditor():getContext():setLines(t.lnum, t.lnum_end, lines)

  gen:getEditor():echoInStatus(fmt('a-href refactored links:%s moved-dirs:%s',
    #(t.links or E), v2s(t.moved)))

  return t
end

return M

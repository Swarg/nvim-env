-- 28-08-2024 @author Swarg
-- Goal:
--

local log = require 'alogger'
local class = require 'oop.class'
local G = require 'env.langs.svelte.util.grammar'
local AParser = require 'olua.util.lang.AParser'
local lexer = require 'env.langs.svelte.util.lexer'

class.package 'env.langs.svelte.SParser'
---@class env.langs.svelte.SParser : olua.util.lang.AParser
---@field new fun(self, o:table?, iter:olua.util.Iterator?, offset:number?): env.langs.svelte.SParser
---@field setIterator fun(self, iter:olua.util.Iterator, offset:number?): env.langs.svelte.SParser
---@field setStoppedAt fun(self, line:string) : env.langs.svelte.SParser
---@field comp table{name, script, template, style}
local C = class.new_class(AParser, 'JParser', {
})

--
-- constructor
-- local parser = SParser:new(nil, Context:new())
--
---@param iter olua.util.Iterator?
---@param offset number?
function C:_init(iter, offset)
  log.debug("_init iter:%s offset:%s", iter, offset)
  self:super(iter, offset) -- call constructor of superclass

  self.compo = {           -- current parsed compoment(module) file
    name = nil, script = nil, template = nil, style = nil,
  }
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug


function C.lexer()
  return lexer
end

--
---@param line string
---@return table?
function C.splitLineToLexemes(line)
  if not line or line == '' then
    return nil
  end
  return G.offhand.cpLexemsWithPos:match(line)
end

class.build(C)
return C

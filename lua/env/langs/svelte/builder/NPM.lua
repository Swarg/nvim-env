-- 29-08-2024 @author Swarg
--
-- Node-Package-Manager Builder
--

local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'
local cjson = require 'cjson'

local Builder = require 'env.lang.Builder'


local BUILDSCRIPT = 'package.json'


class.package 'env.langs.svelte.builder'
---@class env.langs.svelte.builder.NPM : env.lang.Builder
---@field new fun(self, o:table?, project_root:string?, src:string?, test:string?): env.langs.svelte.builder.NPM
---@field of fun(self, project_root:string, userdata:table?): env.langs.svelte.builder.NPM
---@field src string
---@field test string
---@field settings table?
local C = class.new_class(Builder, 'NPM', {
  src = 'src',
  test = 'test',
  buildscript = BUILDSCRIPT
})


local log_debug = log.debug
-- local join_path = fs.join_path

--
-- public static
-- check is the given directory is Root of Source Code with .rockspec
-- userdata used to pass infos need to build instance of C
--
---@param path string
---@param userdata table|nil output data to build instance
---@return string?  full path to script file or nil
function C.isProjectRoot(path, userdata)
  userdata = userdata

  local fn, buildscript
  if path then
    fn = fs.join_path(path, BUILDSCRIPT)
    if fs.file_exists(fn) then
      buildscript = fn
    end
  end
  log_debug("isProjectRoot %s ret buildscript:%s", path, buildscript)
  return buildscript
end

-- to use default behaviour in the env.lang.Builder
---@return table?
function C:getSettings()
  log_debug("getSettings", type(self.settings))
  if not self.settings then
    self.settings = Builder.getSettings(self)
  end
  return self.settings
end

-- @Override
-- called from Builder.getSettings() -> Builder.loadBuildScript()
---@param fn string -- self:getBuildScriptPath()
---@param json string
---@param prev_lm number
---@return false|table
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function C:parseBuildScript(fn, json, prev_lm)
  log_debug("parseBuildScript", fn)
  local ok, decoded = pcall(cjson.decode, json)
  if not ok then
    error(decoded)
  end

  return decoded
end

class.build(C)
return C

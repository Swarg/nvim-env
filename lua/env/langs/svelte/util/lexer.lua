-- 28-08-2024 @author Swarg
local M = {}

local constants = require 'env.lang.api.constants'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local LT = constants.LEXEME_TYPE
M.LT = LT


---@param t any
---@return boolean
function M.isComponentLexemType(t)
  return t == LT.TAG_SINGLE or t == LT.TAG_PAIRED or t == LT.COMPONENT
end

---@param s string?
---@return boolean
function M.isFirstUpper(s)
  if type(s) == 'string' and #s > 0 then
    local fc = s:sub(1, 1)
    return fc == string.upper(fc)
  end
  return false
end

--
-- index of the word in parsed words from one line
--
---@param lexems table
---@param pos number
---@return number
---@return string?
function M.indexOfByPos(lexems, pos)
  for i, lexem in ipairs(lexems) do
    local s, e = lexem[2], lexem[3]
    if pos >= s and pos <= e then
      return i, lexem[1]
    end
  end
  return -1
end

---@param lexems table
---@param off number
---@return boolean
function M.isSingleTag(lexems, off)
  off = off or 2
  for i = off, #lexems do
    if (lexems[i] or E)[1] == '>' then
      return lexems[i - 1][1] == '/'
    end
  end
  return false
end

---@param lexems table
---@param i number
---@param unwrap_string boolean?
local function get_token_at(lexems, i, unwrap_string)
  local s = ((lexems or E)[i] or E)[1]
  if unwrap_string and type(s) == 'string' and
      s:sub(1, 1) == "'" and s:sub(-1, -1) == "'" then
    s = s:sub(2, -2)
  end
  return s
end

local function is_string_token(s)
  if type(s) == 'string' and #s >= 1 then
    local fc, lc = s:sub(1, 1), s:sub(-1, -1)
    return fc == lc and (fc == '"' or fc == "'")
  end
  return false
end

---@param lexems table
---@param token string
function M.indexof_token(lexems, token)
  for i, lexem in ipairs(lexems) do
    if lexem[1] == token then
      return i
    end
  end
  return -1
end

--
---@param lexems table
---@param col number
---@return number
---@return string?
function M.getTypeOfPos(lexems, col)
  local i, curr = M.indexOfByPos(lexems, col)
  if not i or i < 0 or not curr then
    return -1, curr -- out of range
  end

  -- lexems

  if #(curr or '') == 0 then
    return LT.EMPTY
  end

  local desctruct = false
  local nexti = i + 1
  local prev, next0 = get_token_at(lexems, i - 1), get_token_at(lexems, nexti)
  if prev == '<' then
    -- <tag /> or <tag></tag>
    return M.isSingleTag(lexems, i) and LT.TAG_SINGLE or LT.TAG_PAIRED
  end
  if prev == '{' and next0 == '}' then
    nexti = nexti + 1
    prev, next0 = get_token_at(lexems, i - 2), get_token_at(lexems, nexti)
    desctruct = true
  end

  if prev == 'import' and next0 == 'from' then
    if desctruct then
      -- TODO What does the capitalized word mean in js import - a CLASS?
      return M.isFirstUpper() and LT.IDENTIFIER or LT.FUNCTION
    end

    local source = get_token_at(lexems, nexti + 1)
    if is_string_token(source) and match(source, '%.svelte[\'"]$') ~= nil then
      return LT.COMPONENT -- CLASS?
    end
    -- todo function import { something } from
    return LT.IDENTIFIER
  end


  return LT.UNKNOWN
end

---@param lexems table?
---@param off number?
function M.getAttribures(lexems, off)
  if not lexems or not off then
    return nil -- ?
  end

  assert(type(off) == 'number', 'expected number offset')
  assert(lexems[off - 1] ~= '<', 'expected index of the compoment')
  local t, prev_lexem = {}, nil

  for i = off, #lexems do
    local lexem = lexems[i][1]
    if lexem == '>' then
      return t
    elseif lexem == '=' then
      t[#t + 1] = prev_lexem
    end
    prev_lexem = lexem
  end
  return t
end

---@param lexems table?
---@return table?{path:string}
function M.getImportDefinition(lexems)
  if lexems and M.indexof_token(lexems, 'import') > -1 then
    local i = M.indexof_token(lexems, 'from')
    if i > -1 then
      local path = get_token_at(lexems, i + 1, true) -- unwrap from ''
      return {
        path = path
      }
    end
  end
  return nil
end

return M

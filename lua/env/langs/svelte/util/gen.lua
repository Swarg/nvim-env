-- 28-08-2024 @author Swarg
-- integration with SvelteKit


local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'

local utempl = require 'env.lang.utils.templater'

local M = {}

M.PAGE_FN = '+page.svelte'
local PAGE_FN = M.PAGE_FN

local TAB = '  '
local TEMPL_COMPOMENT_CODE = [[
<script>${script}
</script>

${template}
<style>${style}
</style>
]]

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--
-- /* content */     -->  content
-- <!-- content -->  -->  content
--
---@param line string?
function M.get_comment_content(line)
  local comment = nil
  if line ~= nil and line ~= '' then
    comment = match(line, '^%s*<!%-%-%s*(.-)%s*%-%->%s*$') or
        match(line, '^%s*/%*%s*(.-)%s*%*/%s*$') or match(line, "^%s*//%s*(.*)$")
  end
  return comment
end

---@param fn string current page path
---@param route string
---@param curr_page_route string?
function M.mk_route_path(fn, route, curr_page_route)
  assert(type(fn) == 'string', 'fn')
  assert(type(route) == 'string' and route ~= '', 'route')
  local dir = fn
  if su.ends_with(fn, PAGE_FN) then
    dir = fs.get_parent_dirpath(fn) or fn
  end
  local subdir = curr_page_route
  if route:sub(1, 1) == '/' then
    subdir = nil -- from the webapp root
  end

  return fs.join_path(dir, subdir, route, PAGE_FN)
end

--
--
---@return boolean
---@return string? errmgs
function M.create_code(path, lines, force)
  if fs.file_exists(path) and not force then
    return false, 'file already exists ' .. v2s(path)
  end
  local dir = fs.get_parent_dirpath(path)
  if type(dir) ~= 'string' or dir == '' then
    return false, 'no parent directory'
  end
  fs.mkdir(dir)
  if not fs.write(path, lines, "wb") then
    return false, 'cannot save to ' .. v2s(path)
  end
  return path
end

--
--
---@param name string component name
---@param opts table {fn:string, attrs:table?, simple: boolean?}
---       opts.fn string absolute path in parent component (curret file)
---@return false|string path
---@return string code errmsg
function M.gen_new_compoment(name, opts)
  assert(type(opts) == 'table', 'opts')
  local parent_dir = fs.get_parent_dirpath(opts.fn)
  if not parent_dir or not fs.dir_exists(parent_dir) then
    return false, 'expected existed parent dir ' .. v2s(parent_dir)
  end

  local path, relpath
  if opts.simple == false then
    relpath = name -- dirname
    -- NestedCompoment/NestedCompoment.svelte
    -- NestedCompoment/NestedCompoment.scss
  end

  -- pick path from import defintion (todo check given name with basename fr path)
  if opts.module and opts.module.path then -- import X from './path'
    relpath = opts.module.path
    if relpath:sub(1, 1) == '/' then       -- from webapp root
      local _, i = string.find(parent_dir, 'src')
      local _, i2 = string.find(parent_dir, 'routes', i, true)
      parent_dir = parent_dir:sub(1, i2 or i or #parent_dir)
      log_debug("parent_dir:%s relpath:%s", parent_dir, relpath)
    elseif relpath:sub(1, 2) == './' then -- relative path
      relpath = relpath:sub(3, #relpath)
    end
  else
    relpath = fs.join_path(relpath, name .. '.svelte')
  end

  path = fs.join_path(parent_dir, relpath)

  local script, template = '', ''

  if #(opts.attrs or E) > 0 then
    for _, attr in ipairs(opts.attrs) do
      script = script .. "\n  export let " .. v2s(attr) .. ';'
      template = template .. fmt("<p>The %s is: {%s}</p>\n", v2s(attr), v2s(attr))
    end
  end

  opts.import = opts.import or {}
  opts.import[#opts.import + 1] = fmt("%simport %s from './%s'", TAB, name, relpath)

  local code = utempl.substitute(TEMPL_COMPOMENT_CODE, {
    script = script,
    template = template,
    style = '',
  })

  return path, code
end

---@param script string?
---@param template string?
---@param style string?
function M.gen_component_body(script, template, style)
  return utempl.substitute(TEMPL_COMPOMENT_CODE, {
    script = script or '',
    template = template or '',
    style = style or '',
  })
end

--
--
---@param name string component name
---@param opts table {fn:string, attrs:table?, simple: boolean?}
---       opts.fn string absolute path in parent component (curret file)
---@return false|string path
---@return string? errmsg
function M.gen_new_compoment_file(name, opts)
  local path, code = M.gen_new_compoment(name, opts)
  if not path or not code then
    return false, code -- errmsg
  end
  return M.create_code(path, code)
end

--
-- Create +page.svelte foa each link in a-href
--
---@param urls table{[1]-url, [2]title}
---@param routes_root_dir string current page path
---@param opts table?{add_desc, curr_page_route}
function M.gen_new_routes0(urls, routes_root_dir, opts)
  assert(type(routes_root_dir) == 'string' and routes_root_dir ~= '', 'expected string cdir')
  opts = opts or {}

  local t = {}

  for i, e in ipairs(urls) do
    local url = e[1]
    local title = e[2] or ('link_' .. v2s(i))
    if url then
      local template = opts.template or ''
      if opts.add_desc then
        if template ~= '' then template = template .. "\n" end
        template = template .. fmt("<h1>%s</h1>\n", title)
      end

      t[#t + 1] = {
        path = M.mk_route_path(routes_root_dir, url, opts.curr_page_route),
        body = M.gen_component_body(opts.script, template, opts.style),
      }
    end
  end
  return t
end

--
-- todo move to SParser
---@param ctx env.ui.Context
---@param imported table? output with already imported compoments
function M.find_lnum_of_last_import(ctx, imported)
  local lines = ctx:getLines(1, 16)
  if type(lines) ~= 'table' then
    return -1
  end
  local lnum = 0
  local has_import_map = type(imported) == 'table'

  for i, line in ipairs(lines) do
    if string.match(line, '^%s*</script>') then
      break
    elseif string.match(line, '^%s*<script>') then
      lnum = i
    else
      local compo = string.match(line, "^%s*import%s+([%w_]+)%s+from")
      if compo then
        lnum = i
        if has_import_map then
          imported[compo] = i
        end
      end
    end
  end
  return lnum
end

return M

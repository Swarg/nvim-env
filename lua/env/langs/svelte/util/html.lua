-- 30-08-2024 @author Swarg
local M = {}
--


function M.extract_ahref(line)
  local href = string.match(line or '', '<a%s+href%s*=%s*"([^"]+)"')
  return href
end

---@param s string?
function M.has_html_tag(s)
  return s ~= nil and s ~= '' and string.match(s, "<(.-)>") ~= nil
end

return M

-- 28-08-2024 @author Swarg
--

local lpeg = require 'lpeg'


local M = {}

---@diagnostic disable-next-line: unused-local
local P, R, B, C, V, lpegS = lpeg.P, lpeg.R, lpeg.B, lpeg.C, lpeg.V,
    lpeg.S ---@diagnostic disable-next-line: unused-local
local Ct, Cc, Cg, Cp, Cf = lpeg.Ct, lpeg.Cc, lpeg.Cg, lpeg.Cp, lpeg.Cf


local EOF = P(-1)
local S = lpegS ' \t\v\n\f' -- whitespace (the set of chars)
local S1 = S ^ 1            -- one or more spaces

local pString = P '"' * (P(1) - P '"') ^ 0 * P '"'
local pString2 = P "'" * (P(1) - P "'") ^ 0 * P "'"
local pCommentML = P '/*' * (P(1) - P '*/') ^ 0 * P '*/' -- multiline
local pCommentOL = P '//' * (P(1) - P '\n') ^ 0          -- oneline

local pOperator = P('!=') + '!' + '==' + '=' + '+=' + '-=' +
    ';' + '{' + '}' + '(' + ')' + '[' + ']' + '@' +
    '<<' + '<=' + '<-' + '<' + '>>' + '>=' + '>' + '->' +
    '--' + '-' + '++' + '+' + '&&' + '&' + '||' + '|' +
    '^' + '*' + '/' + '%' + '?' -- + '.'



--------------------------------------------------------------------------------
--                        for offhand parsing
--------------------------------------------------------------------------------
local oh = {}


---@param t table
---@param s number     Cp()
---@param word string  C(patt)
---@param e number     Cp()
local function foldWithPos(t, s, word, e)
  t[#t + 1] = { word, s, e - 1 }
  return t
end

-- split current line by operators and brackets
oh.sep = pOperator + lpegS('",()[]{}:/@')

-- split the entire string into lexemes, taking into account:
-- "simple strings", comments
oh.cpLexemsWithPos = Ct(
  (S1
    + Cp() * C(pString) * Cp() % foldWithPos
    + Cp() * C(pString2) * Cp() % foldWithPos
    + Cp() * C(pCommentML) * Cp() % foldWithPos
    + Cp() * C(pCommentOL) * Cp() % foldWithPos
    + Cp() * C(P '<>' + P '()' + P '[]' + P 'this.') * Cp() % foldWithPos
    + ((Cp() * C(oh.sep) * Cp()) % foldWithPos)
    + ((Cp() * C((P(1) - (S1 + oh.sep)) ^ 1) * Cp()) % foldWithPos)
  --            ^^^^^^^^^^^^^^^^^^^^^ any chars except space and seps
  ) ^ 0
  * EOF)

M.offhand = oh
--------------------------------------------------------------------------------
return M

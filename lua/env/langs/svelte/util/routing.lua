-- 28-08-2024 @author Swarg
--
-- Helper to worck with webapp routes
--

local su = require 'env.sutil'
local fs = require 'env.files'
local uhtml = require 'env.langs.svelte.util.html'

local M = {}



M.PAGE_FN = '+page.svelte'
local PAGE_FN = M.PAGE_FN

local SRC_ROUTES = fs.ensure_dir(fs.join_path('src', 'routes'))
M.SRC_ROUTES = SRC_ROUTES

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


--
--
function M.get_new_routes_page(filename, route, name)
  name = name or PAGE_FN
  local _, j = string.find(filename, SRC_ROUTES, 1, true)
  if not j then
    return false, 'expected path inside routes'
  end
  return filename:sub(1, j) .. route .. '/' .. name
end

-- /tmp/project/src/routes/path/to/ -> /path/to/
---@param path string
---@return string?
function M.get_app_route(path)
  local _, j = string.find(path, SRC_ROUTES, 1, true)
  if not j then
    return nil
  end

  local s = path:sub(j)
  if #s > 1 and s:sub(-1, -1) == '/' then
    s = s:sub(1, -2)
  end
  return s
end

-- /tmp/project/src/routes/path/to/  ->  /tmp/project/src/routes
---@return string
function M.get_routes_root(path)
  local _, j = string.find(path, SRC_ROUTES, 1, true)
  assert(type(j) == 'number', 'expected path with ' .. SRC_ROUTES)
  return path:sub(1, j)
end

--
---@param lines table
function M.get_routes_from_selected_lines(lines)
  local t = {}
  for i, line in ipairs(lines) do
    local link = uhtml.extract_ahref(line)
    if link then
      t[#t + 1] = {
        href = link,
        lnum = i,
      }
    end
  end
  return t
end

---@param lines table
---@param entry table{href:string, lnum:number}
---@param subdir string
---@param n number
---@return string updated href
---@return string updated line
---@return number lnum of line with given href
function M.refactor_route(lines, entry, subdir, n)
  assert(type(entry) == 'table', 'expected table entry of route')
  assert(type(entry.href) == 'string', 'href')
  assert(type(entry.lnum) == 'number', 'href')
  assert(type(lines[entry.lnum or false]) == 'string', 'line with href')

  local line, href = M.refactor_route0(lines[entry.lnum], entry.href, subdir, n)
  return href, line, entry.lnum
end

---@return string new_line
---@return string new_href
function M.refactor_route0(line, href, subdir, insert_idx)
  assert(type(insert_idx) == 'number', 'insert_idx got:' .. v2s(insert_idx))

  local s, e = string.find(line, href, 1, true)
  if not s then
    error(fmt('not found given href in href:|%s| line:', v2s(line), v2s(href)))
  end
  local dirs = su.split(href, "/")
  table.insert(dirs, insert_idx, subdir)
  local pref = href:sub(1, 1) == '/' and '/' or ''
  local new_href = pref .. table.concat(dirs, "/")
  local new_line = line:sub(1, s - 1) .. new_href .. line:sub(e + 1, #line)

  return new_line, new_href
end

return M

-- 29-08-2024 @author Swarg

local class = require 'oop.class'

local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'

local Lang = require 'env.lang.Lang'
-- local ClassInfo = require 'env.lang.ClassInfo'
-- local ClassResolver = require 'env.lang.ClassResolver'
local api_constants = require 'env.lang.api.constants'
local SGen = require 'env.langs.svelte.SGen'
local gen_util = require 'env.langs.svelte.util.gen'
local urouting = require 'env.langs.svelte.util.routing'
local uhtml = require 'env.langs.svelte.util.html'
-- local SParser = require 'env.langs.svelte.SParser'
local NPMBuilder = require 'env.langs.svelte.builder.NPM'



class.package 'env.langs.svelte'
---@class env.langs.svelte.SLang : env.lang.Lang
---@field new fun(self, o:table?, project_root:string?, builder:env.lang.Builder?): env.langs.svelte.SLang
---@field getLangGen fun(self): env.langs.svelte.SGen
---@field executable      string   bin of the lang interprater
---@field namespace_map   table    created by MixBuilder TODO
---@field modules_test_namespaces table - created after findTestFramework() TODO
---@field src string
---@field test string
---@field builder env.lang.Builder
---@field last_lsp_update number   time of a setup settings (os.time())
--                                 used to update already exists settings only
--                                 when rockspec file was changed
--
local C = class.new_class(Lang, 'SLang', {
  ext = 'svelte',
  -- executable = 'svelte', -- svelte SingleClassApp.svelte
  root_markers = {
    '.git', 'package.json', 'svelte.config.js', 'vite.config.js'
  },
  -- Note: there must be a slash at the end of the path
  src = 'src',
  test = 'test',
  test_suffix = 'Test', -- ??
})

Lang.register(C)



---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local join_path, file_exists = fs.join_path, fs.file_exists
local get_parent_dirpath = fs.get_parent_dirpath
local get_comment_content = gen_util.get_comment_content
local extract_ahref = uhtml.extract_ahref
local has_html_tag = uhtml.has_html_tag
-- local instanceof = class.instanceof
---@diagnostic disable-next-line: unused-local
local LT = api_constants.LEXEME_TYPE



-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework
---@param fn string filename
---@return boolean
function C.isLang(fn)
  return type(fn) == 'string' and fn:match('%.svelte$') ~= nil
end

---@return env.langs.java.JGen -- the instance of a LangGen class (ready to work)
function C:newLangGen()
  self.gen = SGen:new({ lang = self })
  return self.gen
end

---@param marker string?
function C:findProjectBuilder(marker)
  log_debug("findProjectBuilder %s marker:%s", (self or E).project_root, marker)
  self.builder = nil

  local project_root = assert(self.project_root, 'project_root')

  local userdata = {} -- to pass src, test, project_root
  if NPMBuilder.isProjectRoot(project_root, userdata) then
    self.builder = NPMBuilder:of(project_root, userdata)
    if self.builder ~= nil then
      log_debug('found Builder for given project_root:', project_root)
      return self:syncSrcAndTestPathsWithBuilder() -- sync Lang and Builder
    end
  end

  log_debug('Not Found any known BuildSystem in %s', self.project_root)
  return self
end

---@override
---@param project_root string
---@param opts table?{project_root, flat, builderClass}
---@return table properties to synck
function C:createBuilder(project_root, opts)
  log_debug("createBuilder", opts)
  opts = opts or {}

  project_root = project_root or opts.project_root or self.project_root

  return NPMBuilder:new(nil, project_root)
end

--
--
--
---@return string?
function C:getClassNameForInnerPath(innerpath)
  if not innerpath or innerpath == '' then
    return innerpath
  end

  local cn, pref = nil, ''
  local src, test = self:getSrcPath(), self:getTestPath()

  if su.starts_with(innerpath, src) then
    pref = self:getSrcPath()
  elseif su.starts_with(innerpath, test) then
    pref = self:getTestPath()
  end

  cn = innerpath:sub(#pref + 1)
  cn = fs.path_to_classname(cn, '%.') -- class_name

  log_debug("getClassNameForInnerPath %s > cn:%s (src:%s test:%s)",
    innerpath, cn, src, test)

  return cn
end

--
-- check is given (relative uri) path is exists in the project_dir/src/routes/
--
---@param self self
---@return string?
---@param path string?
---@param resolved string?
---@param subdir string?
local function check_route_path(self, path, resolved, subdir)
  local project_root = (self or E).project_root or '.'
  log_debug("check_route_path check:%s resolved:%s", path, resolved)
  if not resolved and path and #path > 2 then
    if path:sub(1, 2) == './' then
      path = path:sub(3)
    end
    resolved = join_path(project_root, subdir, path)
    if not fs.file_exists(resolved) then
      log_debug('file does not exists %s', resolved)
      resolved = join_path(get_parent_dirpath(project_root), subdir, path)
      if not file_exists(resolved) then
        log_debug('file does not exists %s', resolved)
        resolved = nil
      end
    end
  end
  return resolved
end

--
-- jumpt to another file with defintion of word under the cursor
--
function C:gotoDefinition()
  log_debug("gotoDefinition")
  local ctx = self:getEditor():getContext(true, nil) -- SParser:new())
  local cl = su.trim(ctx:getCurrentLine())

  local path, jumpto = nil, 1

  -- case-1: jump by relative path in the comment from poject root or parent dir
  local comment = get_comment_content(cl)
  path = check_route_path(self, comment, path)

  -- case-2: whole line is path to existed file
  if not path and not has_html_tag(cl) then
    path = check_route_path(self, cl, path)
  end

  -- case-3: a-href="/path"
  if not path then
    local url = extract_ahref(cl)
    if url and url ~= '' and string.find(url, '://', 1, true) == nil then
      path = check_route_path(self, url, nil, urouting.SRC_ROUTES)
    end
  end

  log_debug("comment: |%s| path:|%s|", comment, path)

  if type(path) ~= 'string' then
    log.debug('Not found defintion for line:|%s|', cl)
    return
  end

  -- auto convert to +page.svelte
  if fs.is_directory(path) then
    local p = join_path(path, urouting.PAGE_FN)
    if fs.file_exists(p) then
      path = p
    end
  end

  return Lang.openExistedFileWithJumpTo(self:getEditor(), path, jumpto)
end

--
-- from some source code
-- main goal: jump to source file from stacktrace with errors
-- for example, from errors in tests
--
---@param editor env.ui.Editor
---@return boolean
function C.gotoDefinitionFromNonSource(editor, line)
  line = line or editor:getContext():getCurrentLine()
  log_debug("gotoDefinitionFromNonSource |%s|", line)

  local path, jumpto = nil, 1
  path = su.trim(line)
  if not uhtml.has_html_tag(path) then
    path = check_route_path({project_root = os.getenv('PWD')}, path, nil)
  end
  -- todo stacktraces output

  if not path then
    return false
  end
  return Lang.openExistedFileWithJumpTo(editor, path, jumpto)
end

--
-- check is given old_href exists in src/routes
-- and if no conflict with new_href then move old dir to the new subdirectory
--
---@param new_href string
---@param old_href string
---@return false|string
---@return string?
function C:refactorRoute(new_href, old_href)
  local dir = join_path(self.project_root, urouting.SRC_ROUTES)
  log_debug("refactorRoute old_href:%s new:%s (%s)", old_href, new_href, dir)

  local old_name = join_path(dir, old_href)
  local dst = join_path(dir, new_href)

  -- /tutor/page        -- old
  -- /tutor/event/page  -- new
  local i = 1
  while i < #new_href and new_href:sub(i, i) == old_href:sub(i, i) do
    i = i + 1
  end
  local shared_dir = new_href:sub(1, i - 2) -- /tutor
  if shared_dir == '' then
    log_debug('no shared dir ')
    return false, 'no shared dir'
  end

  local new_dir_end = string.find(new_href, '/', i, true)
  local new_dir = new_href:sub(i, new_dir_end - 1) -- event
  if new_dir == '' then
    return false, 'no new dir'
  end
  local new_dir_path = fs.join_path(dir, shared_dir, new_dir)
  local subdir_to_move = string.sub(new_href, new_dir_end + 1, #new_href)
  if subdir_to_move == '' then
    return false, 'no new_dst_subdir' -- page
  end
  local new_name = join_path(new_dir_path, subdir_to_move)
  --  '/tmp/proj/src/routes/tutor/event/section1'

  if file_exists(dst) then
    log_debug('cannot refactor to already existed route: ' .. v2s(dst))
    return false
  end

  local src_typ = fs.file_type(old_name)
  if src_typ ~= 'file' and src_typ ~= 'directory' then
    log_debug('cannot refactor to already existed route: ' .. v2s(dst))
    return false
  end

  if not fs.mkdir(new_dir_path) then
    return false, 'cannot create new dir: ' .. new_dir_path
  end

  os.rename(old_name, new_name) -- dirs
  log_debug("moved %s -> %s", old_name, new_name)

  return new_name, old_name
end

class.build(C)
return C

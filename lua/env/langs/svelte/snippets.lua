-- 28-08-2024 @author Swarg
-- snippets for EnvLineInsert for Svelte
--
--
--
--

local M = {}

local insert_items = {
  "\n<script>\n</script>",
  "<${WORD}></${WORD}>",
  "<p>${LINE}</p>",
  "\n<style>\n</style>",
  "<pre>${IDENTIFIER}:{'('+typeof ${IDENTIFIER}+') '+JSON.stringify(${IDENTIFIER})}</pre>\n",
  '',
  "${TAB} console.log('${IDENTIFIER}:('+typeof(${IDENTIFIER})+')', ${IDENTIFIER});\n",
  "${HANDLER_NEW_COMPONENT}",    -- 8 create file with component from curr word
  "${HANDLER_EXTRACT_TO_ROUTE}", -- 9 extract all code from current +page.svelte to new route
  '',                            -- 10 new test
  '',
  '',
  '',
  '',
  '',
  '',
  "<h3> My Solution</h3>\n",
  "<h3> Community Solutions</h3>\n",
  '${HANDLER_MK_FILE_REF}', -- 19 -- inner path in the current project
  '',
  '',
  '${HANDLER_WRAPTO_A_HREF}', -- 21
  '${HANDLER_WRAPTO_UL_LI}',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  "${HANDLER_BUILD_URLS}",        -- 31
  '${HANDLER_GEN_NEW_ROUTES}',    -- 32
  '${HANDLER_REMOVE_ATTR_CLASS}', -- 33
  '',
  '',
  '',
  '',
  '',
  '',
}

local insert_mappings = {
}


local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'
local Editor = require 'env.ui.Editor'
local SGen = require 'env.langs.svelte.SGen'
local routing_util = require 'env.langs.svelte.util.routing'
local gen_util = require 'env.langs.svelte.util.gen'
local common = require 'env.lang.utils.common'



local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug
local get_app_route = routing_util.get_app_route


local function get_editor_n_ctx(parser)
  local editor = Editor:new()
  local ctx = editor:getContext(true, parser)
  return editor, ctx
end

--
-- <div class="abcde" style="">  -->  <div style="">
--
function M.handler_remove_attr_class()
  local _, ctx = get_editor_n_ctx()
  local lines, lnum, lnum_end = ctx:getSelectedLines()
  if not lines or #lines == 0 then
    return false, 'no selected lines'
  end
  local res = {}
  for _, line in ipairs(lines) do
    local ps, pe = string.find(line, '%s+class="[%w%-_%s]+"')
    if ps then
      line = line:sub(1, ps - 1) .. line:sub(pe + 1, #line)
    end
    res[#res + 1] = line
  end

  ctx:setLines(lnum, lnum_end, res)
  return true
end

--
-- for given single route or for list of urls in a-href
--
function M.handler_extract_to_route()
  local editor, ctx = get_editor_n_ctx()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'expected not empty line with route'
  end
  local filename = ctx:getCurrentBufname()
  local words = su.split(ctx:pullCurrLine(), ' ')

  local path2page = routing_util.get_new_routes_page(filename, words[1])
  if not path2page then
    return false, 'cannot gen path routes page'
  end

  local e, err = gen_util.create_code(path2page, ctx:getLines(1, -1))
  if not e then
    return false, err
  end
  if not fs.file_exists(path2page) then
    return false, 'not created ' .. v2s(path2page)
  end

  ctx:setLines(1, -1, {}) -- clear current opened file
  editor:open(path2page)  -- open the new one

  return true
end

--
--
--
function M.handler_new_component()
  return SGen.newComponent(nil, nil, nil)
end

--
--  title1
--  title2          <a href="/url-1">title-1</a>
--            -->   <a href="/url-2">title-2</a>
--  url1
--  url2
--
function M.handler_build_urls()
  local _, ctx = get_editor_n_ctx()
  local lines, s, e = ctx:getSelectedLines()
  if not lines then
    return false, 'no selected lines'
  end

  local pref, suff, cont_open, cont_close = '', '', nil, nil
  local titles, href = {}, {}
  local t = titles

  for i, line in ipairs(lines) do
    if i == 1 and string.match(line, "^s*:") then
      -- command
      local cmd = string.match(line, "^s*:%s*([^%s]+)")
      if cmd == 'help' then
        return false, 'wrap prefix|suffix, list'
      elseif cmd == 'wrap' then
        local a1, a2 = string.match(line, "^s*:%s*[^%s]+%s+([^|]+)|([^|]+)")
        pref, suff = a1, a2
      elseif cmd == 'list' then -- <lo> <li>A</li> <li>B</li> </lo>
        pref, suff = '  <li>', '</li>'
        cont_open, cont_close = "<lo>", "</lo>"
      end
    else
      if line == '' or string.match(line, '^%s*$') then
        t = href
      else
        t[#t + 1] = line
      end
    end
  end
  t = {}
  t[#t + 1] = cont_open -- <lo>
  for i, title in ipairs(titles) do
    t[#t + 1] = fmt('%s<a href="/%s">%s</a>%s',
      pref, v2s(href[i]), v2s(title), suff)
  end
  t[#t + 1] = cont_close

  ctx:setLines(s, e, t)
  return true
end

--
-- https://example.com/path/  --> <a href="https://example.com/path">path</a>
-- For current line only
function M.handler_wrapto_a_href()
  local _, ctx = get_editor_n_ctx()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'expected not empty line'
  end
  local tab, cl = string.match(ctx:getCurrentLine(), '^(%s*)(.-)%s*$')
  log_debug("handler_wrapto_a_href tab:|%s| curr_line:|%s|", tab, cl)
  -- <cmd> url
  -- cmd|url|title
  local cmd, url, title = string.match(cl, '^%s*([^|]+)%s*|%s*([^|%s]+)%s*|?%s*(.*)$')
  log_debug("### cmd:%s url:%s title:%s", cmd, url, title)
  local pref, suff = '', ''
  if title == '' then title = nil end

  if cmd and cmd ~= '' then
    if cmd == 'li' then
      pref, suff = '<li>', '</li>'
    elseif cmd == 'div' then
      pref, suff = '<div>', '</div>'
    elseif cmd == 'span' then
      pref, suff = '<span>', '</span>'
    else
      if string.find(cmd, "/") and string.find(url, "/") == nil then
        title = url -- /path/to/section|The Section
        url = cmd
      else
        title = cmd -- The Section|/path/to/section
      end
    end
  else
    url = cl -- case: "/path/to/section" or http://host.com/relpath
  end

  title = title or string.match(url, '^.-://[^/]+/(.*)$')
  log_debug("### url:%s", url, title, cl)
  if not title or title == '' then
    title = 'link'
    log_debug("### set title to link!")
  end

  log.debug("url:%s title:%s pref:%s suff:%s tab:%s", url, title, pref, suff, tab)

  local line = fmt('%s%s<a href="%s">%s</a>%s', tab, pref, url, title, suff)

  ctx:setCurrentLine(line)
  return true
end

--
--                         <ul>
--  - item 1                 <li>item 1</li>
--  - item 2     -->         <li>item 2</li>
--                         <ul>
--
function M.handler_wrapto_ul_li()
  local _, ctx = get_editor_n_ctx()
  local lines, s, e = ctx:getSelectedLines()
  if not lines then
    return false, 'no selected lines'
  end
  local t = { '<ul>' }
  for _, line in ipairs(lines) do
    line = string.match(line, '^%s*%-%s*(.*)%s*$') or su.trim(line)
    if line and line ~= '' then
      t[#t + 1] = fmt("  <li>%s</li>", v2s(line))
    end
  end
  t[#t + 1] = '</ul>'
  ctx:setLines(s, e, t)

  return true
end

---@param ctx env.ui.Context
local function get_layout_code(ctx)
  log_debug("get_layout_code")
  local script, templ = '', ''

  local lines, _ = ctx:getLinesBetween("<script>", "/* layout end */", true)
  log_debug("layout ", #(lines or E))

  if lines then
    -- check Navigation Bar Component import
    for _, line in ipairs(lines) do
      local compo = string.match(line, "^%s*import%s+([^%s]+)%s+from")
      log_debug("code line:|%s| compo:|%s|", line, compo)
      if compo == 'Nav' or compo == 'NavBar' then
        templ = fmt("<%s></%s>\n", compo, compo)
      end
      script = script .. "\n" .. line
    end
  end

  return script, templ
end

--
-- Create +page.svelte foa each link in a-href
-- <li><a href="/tutorial/part-1-basic/welcome-to-svelte">Welcome to Svelte</a> </li>
--
function M.handler_gen_new_routes()
  log_debug("handler_gen_new_routes")
  local editor, ctx = get_editor_n_ctx()
  local lines = ctx:getSelectedLines()
  if not lines or #lines == 0 then
    return false, 'no selected lines'
  end

  local page_path = ctx:getCurrentBufname()
  local routes_root_dir = routing_util.get_routes_root(page_path)

  local script, templ = get_layout_code(ctx)
  local opts = {
    add_desc = true,
    script = script,
    template = templ,
    style = '',
    curr_page_route = get_app_route(fs.get_parent_dirpath(page_path) or '')
  }

  log_debug("parse selected lines:%s curr_page_route:%s full:(%s)",
    #(lines or E), opts.curr_page_route, page_path)

  local t = {}

  for _, line in ipairs(lines) do
    local url = string.match(line, 'a href="([^"]+)"')
    local title = string.match(line, '>([^<]+)</a>')
    log_debug("url:%s title:%s from line:|%s|", url, title, line)
    if url then
      t[#t + 1] = { url, title }
    end
  end

  assert(type(routes_root_dir) == 'string', 'has route_root_dir')

  local files = gen_util.gen_new_routes0(t, routes_root_dir, opts)
  log_debug("generated page files:", #(files or E))
  local checked_dirs, cnt = {}, 0

  for _, fe in ipairs(files) do
    local path, body = fe.path, fe.body
    local exists = fs.file_exists(path)
    log.debug("check path exists", exists, path)
    if not exists then
      local dir0 = fs.get_parent_dirpath(path) ---@cast dir0 string
      if dir0 and not checked_dirs[dir0] then
        fs.mkdir(dir0)
        checked_dirs[dir0] = true
      end
      if fs.write(path, body, 'wb') then
        cnt = cnt + 1
      end
    end
  end

  editor:echoInStatus("Created pages: " .. v2s(cnt) .. "/" .. #files)

  return true
end

--
local insert_handlers = {
  EXTRACT_TO_ROUTE = M.handler_extract_to_route,
  NEW_COMPONENT = M.handler_new_component,
  BUILD_URLS = M.handler_build_urls,
  WRAPTO_A_HREF = M.handler_wrapto_a_href,
  WRAPTO_UL_LI = M.handler_wrapto_ul_li,
  GEN_NEW_ROUTES = M.handler_gen_new_routes,
  REMOVE_ATTR_CLASS = M.handler_remove_attr_class,
  MK_FILE_REF = common.handler_mk_file_ref,
}

---@param reg function
function M.add(reg)
  reg('svelte', insert_items, insert_mappings, insert_handlers)
  reg('ts', insert_items, insert_mappings, insert_handlers)
end

return M

-- 17-03-2024 @author Swarg
-- snippets for EnvLineInsert for Bash Scripting
--
--
--
--

local M = {}

local insert_items = {
  'local ${LINE}',
  '"${SWORD}"',
  "$${WORD}",
  "\nfunction ${WORD}() {\n}\n\n",   -- 4
  "${TAB} echo \"$${WORD}\" >&2\n",    -- 5
  "${TAB} (( $# == 0 )) && echo '' && exit 1\n",
  "${TAB} for (( i = 0; i <= ${WORD}; i++ )); do\n${TAB} done\n",
  '${TAB} if [ -z "$${WORD}" ]; then\n ${TAB} fi',
  '${TAB} if [ "$1" == "$${WORD}" ]; then \n ${TAB} fi',
  '${TAB} elif [ "$1" == "" -o "$1" == "" ]; then' .. "\n",
  'if [[ -z "$1" || "$1" == "h" || "$1" == "help" ]]; then' .. "\n",
  '\n# ${DATE} @author ${AUTHOR}\n# Goal:\n#  -\n# Dependencies:\n# -\n',
  '#!/bin/bash\n\n',
  "#!/usr/bin/env bash\n\nmain () {\n}\n\nmain \"$@\"\n"
}

local insert_mappings = {
}

local insert_handlers = {
}

---@param reg function
function M.add(reg)
  reg({'sh', 'bash'}, insert_items, insert_mappings, insert_handlers)
end


return M

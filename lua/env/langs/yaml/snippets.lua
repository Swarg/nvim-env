-- 01-03-2025 @author Swarg
--
--
--
--
--

local M = {}

local insert_items = {
  ':EnvYaml get-full-path --copy<CR>',
  ':EnvYaml jump-to-ref<CR>',
  ':EnvYaml select-key-body --copy<CR>',
  ':EnvYaml get-sibling-keys --copy<CR>',
  '',
  '',
  '',
  '',
  '',
}

local insert_mappings = {
}

local insert_handlers = {
}

---@param reg function
function M.add(reg)
  reg({ 'yml', 'yaml' }, insert_items, insert_mappings, insert_handlers)
end

return M

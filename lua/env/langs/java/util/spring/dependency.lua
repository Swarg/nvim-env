-- 05-09-2024 @author Swarg
--
-- to insert new dependencies to pom.xml or build.gralde via
--  :EnvNew dependency spring-boot ..
--
-- taken from start.spring.io
--

local M = {}
--
local m = {}
local springframework_boot = "org.springframework.boot"

local E = {}

-- spring-boot-parent <parent></parent> (pom-parent)
M.parent = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-parent',
  version = '3.3.3',
  relativePath = '',
}

-- for dependencies block

-- dev tools
m['spring-boot-devtools'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-devtools",
  scope = "runtime",
  optional = "true",
}
m['lombok'] = {
  groupId = 'org.projectlombok',
  artifactId = 'lombok',
  optional = 'true',
}

m['spring-boot-starter-data-jpa'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-data-jpa",
}
m['spring-boot-starter-data-rest'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-data-rest",
}

m['spring-boot-starter-validation'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-validation",
}
m['spring-boot-starter-web'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-web",
}

-- testing
m['spring-boot-starter-test'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-test",
  scope = "test"
}
m['spring-security-test'] = {
  groupId = "org.springframework.security",
  artifactId = "spring-security-test",
  scope = "test"
}

-- sql db
m['spring-boot-starter-jdbc'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-jdbc',
}
m['h2'] = { -- in-memory-db
  groupId = "com.h2database",
  artifactId = "h2",
  scope = "runtime"
}
m['postgresql'] = {
  groupId = "org.postgresql",
  artifactId = "postgresql",
  scope = "runtime"
}
m['mysql-connector-j'] = {
  groupId = 'com.mysql',
  artifactId = 'mysql-connector-j',
  scope = 'runtime',
}
m['mariadb-java-client'] = {
  groupId = 'org.mariadb.jdbc',
  artifactId = 'mariadb-java-client',
  scope = 'runtime',
}

m['flyway-database-postgresql'] = {
  groupId = "org.flywaydb",
  artifactId = "flyway-database-postgresql",
}

m['liquibase-core'] = {
  groupId = 'org.liquibase',
  artifactId = 'liquibase-core',
}


-- web
m['vaadin-spring-boot-starter'] = {
  groupId = 'com.vaadin',
  artifactId = 'vaadin-spring-boot-starter',
}

-- templates
m['spring-boot-starter-thymeleaf'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-thymeleaf",
}
m['spring-boot-starter-freemarker'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-freemarker',
}
m['spring-boot-starter-mustache'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-mustache',
}

-- security
m['spring-boot-starter-security'] = {
  groupId = springframework_boot,
  artifactId = "spring-boot-starter-security",
}

m['thymeleaf-extras-springsecurity5'] = {
  groupId = "org.thymeleaf.extras",
  artifactId = "thymeleaf-extras-springsecurity5",
}

-- i/o
m['spring-boot-starter-mail'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-mail',
}

-- ops
m['spring-boot-starter-actuator'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-actuator',
}

M.map = m


---@param t table?
---@return boolean
function M.is_spring_boot_project(t)
  if type(t) ~= 'table' then
    return false
  end
  -- maven:pom.xml
  if ((t.project or E).parent or E).artifactId == "spring-boot-starter-parent" then
    return true
  end
  -- todo for gradle
  return false
end

return M

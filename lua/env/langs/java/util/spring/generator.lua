-- 20-08-2024 @author Swarg
--

local log = require 'alogger'
local fs = require 'env.files'
local utempl = require 'env.lang.utils.templater'
local resolver = require 'env.langs.java.util.spring.resolver'

local M = {}

local get_templ_dir = resolver.get_templ_dir_from

local TAB = '    '

M.TEMPL_SPRINGBOOT_MAINCLASS = [[
package ${PACKAGE};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *${DATE}
 * @author ${AUTHOR}
 */
@SpringBootApplication
public class ${MAINCLASS} {

	public static void main(String[] args) {
		SpringApplication.run(${MAINCLASS}.class, args);
	}

}
]]

M.TEMPL_MVN_POM_PARENT = [[
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.3.3</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
]]

local CONTROLLER_ENDPOINT_TEMPL = [[
    @${http_method}Mapping${mapping_params}
    public String ${method_name}(${method_params}) {
${method_body}
        return "${templ_name}";
    }
]]

-- org.springframework.ui.Model
-- org.springframework.stereotype.Controller
-- org.springframework.web.bind.annotation.GetMapping
-- org.springframework.web.bind.annotation.PostMapping
-- org.springframework.web.bind.annotation.PathVariable
-- org.springframework.web.bind.annotation.RequestMapping
-- org.springframework.web.bind.annotation.RequestParam

local http_methods = {
  connect = 1,
  head = 1,
  get = 1,
  post = 1,
  put = 1,
  delete = 1,
  patch = 1,
  options = 1,
  trace = 1,
}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug


local function split(s)
  local t = {}
  for str in string.gmatch(s, '([^\n]+)') do
    table.insert(t, str)
  end
  return t
end

-- "/user/{user}"  -> {"user"}
---@param s string
---@return table
function M.get_path_variables(s)
  local t = {}
  for vn in string.gmatch(s, '{([^{}]+)}') do
    table.insert(t, vn)
  end
  return t
end

-- get -> Get, GET -> Get
---@param s string
local function firstUpper(s)
  return string.upper(s:sub(1, 1)) .. string.lower(s:sub(2))
end

local function varname_to_class(s)
  return string.upper(s:sub(1, 1)) .. s:sub(2)
end

local function class_to_varname(s)
  return string.lower(s:sub(1, 1)) .. s:sub(2)
end

local TEMPLATE_BEAN = [[
    @Bean
    public ${bean_class} ${bean_name}(${bean_params}) {
        ${code}
    }
]]
local TEMPLATE_VALUE_PROPERTY_FILED = [[
    @Value("${property}")
    private String ${field_name};
]]

--
---@param bean_class string
---@param opts table?{filename:string}
---@return table{code:table<string>}
function M.new_bean(bean_class, opts)
  assert(type(bean_class) == 'string' and bean_class ~= "", 'beanClass')
  opts = opts or E
  local params = opts.params or E
  local code, bean_params = '', ''
  if params[1] == "builder" or params[1] == "b" then
    bean_params = bean_class .. 'Builder builder'
    code = "return builder.build();"
  else
    code = fmt("return new %s(%s);", bean_class, bean_params)
  end
  local lines = utempl.substitute(TEMPLATE_BEAN, {
    bean_name = class_to_varname(bean_class),
    bean_class = bean_class,
    bean_params = bean_params,
    code = code,
  })

  return {
    code = split(lines)
  }
end

function M.new_value_property_field(property)
  assert(type(property) == 'string' and property ~= "", 'property name')
  local field_name = ''

  for part in string.gmatch(property, '([^%.]+)') do
    if field_name ~= '' then
      part = firstUpper(part)
    end
    field_name = field_name .. part
  end
  return split(utempl.substitute(TEMPLATE_VALUE_PROPERTY_FILED, {
    property = "${" .. property .. "}",
    field_name = field_name,
  }))
end

--
-- Generate code for new method of Controller with given http-method,
-- endpoint addr and template name
--
---@param http_method string
---@param endpoint string    - use "." for empty endpoint (inherited from
--                             per-class annotation)
---@param templ_name string
---@param fn string absolute path to source file(of controller)
---@return false|table
---@return string? errmsg
function M.new_endpoint(http_method, endpoint, templ_name, fn)
  log_debug("new_endpoint", http_methods, endpoint, templ_name, fn)
  if http_method == "help" then
    return false, "<http_method> <endpoint> <templ_name>"
  end
  if not http_method or http_methods[string.lower(http_method)] == nil then
    return false, 'arg1: expected valid http_method got: ' .. v2s(http_method)
  end
  if not endpoint or endpoint == '' then -- use "." for empty endpoint
    return false, 'arg2: expected endpoint'
  end
  if not templ_name or templ_name == '' then
    return false, 'no template name'
  end
  if not fn or fn == '' then
    return false, 'no bufname'
  end
  local templ_dir, err = get_templ_dir(fn)
  if not templ_dir then
    return false, err
  end

  local tab2 = TAB .. TAB
  local mapping_params, method_params, method_body = '', '', ''

  if endpoint ~= '.' then
    mapping_params = fmt('("%s")', endpoint)
    local path_vars = M.get_path_variables(endpoint)
    if #(path_vars or E) > 0 then
      local mp, mb = '', ''
      for _, vn in ipairs(path_vars) do
        if mp ~= '' then mp = mp .. ", " end
        mp = mp .. fmt('@PathVariable %s %s', varname_to_class(vn), vn)
        mb = mb .. fmt("%smodel.addAttribute(\"%s\", %s);\n", tab2, vn, vn)
      end
      method_params, method_body = mp, mb
    end
  end

  -- add stub for empty
  if method_params ~= '' then method_params = method_params .. ", " end
  method_params = method_params .. 'Model model'
  if method_body == '' then
    method_body = fmt('%smodel.addAttribute("", );', tab2)
  end

  local subs = {
    http_method = firstUpper(http_method),
    mapping_params = mapping_params,
    method_name = templ_name,
    method_params = method_params,
    method_body = method_body,
    templ_name = templ_name
  }
  local code = utempl.substitute(CONTROLLER_ENDPOINT_TEMPL, subs)

  return {
    code = split(code),
    templ_path = fs.join_path(templ_dir, templ_name .. '.html'),
    templ_body = M.get_templ_body(),
  }
end

--
-- todo more flexible
-- based on pom.xml and spring config
function M.get_templ_body()
  return [[
<!DOCTYPE html>
<html th:replace="~{ layout :: layout(_, ~{::section}) }">
<body>
<section>

  <!-- page content -->

</section>
</body>
</html>
]]
end

-- todo more flexible (libs versions and params substitutions)
function M.get_layout_templ_body_bootstrap()
  return [[
<!DOCTYPE html>
<html th:fragment="layout (title, content)" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8" />
    <title th:replace="${title}">Title</title>
    <link rel="stylesheet" href="/static/style.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/static/bootstrap-5.3.3.min.css" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
</head>
<body>
  <nav th:replace="~{ parts/navbar :: navbar }" />

  <div class="container mt-5">
    <div th:replace="${content}"> Page Content </div>
  </div>

  <footer> <hr> &copy; ProjectName </footer>

  <!-- Bootstrap js -->
  <script src="/static/popper-2.11.8.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
  <script src="/static/bootstrap-5.3.3.min.js" integrity="sha384-0pUGZvbkm6XF6gxjEnlmuGrJXVbNuzT9qBBavbLwCsOGabYfZo0T0to5eqruptLy" crossorigin="anonymous"></script>

  <!-- LiveReload (spring-boot-devtools) -->
  <script th:if="${@environment.acceptsProfiles('dev','local')}" src="http://localhost:35729/livereload.js"></script>
</body>
</html>
]]
end

return M

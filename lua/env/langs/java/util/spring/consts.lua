-- 15-08-2024 @author Swarg
--

local syntax = require 'env.langs.java.util.syntax'

local M = {}

-- What is the difference between @component and @bean annotation?
-- - @Bean is used at the method level to explicitly declare individual beans
--   with custom instantiation logic, while
-- - @Component is used at the class level to mark classes as Spring-managed
--   components that are automatically discovered during component scanning.
M.compoment_aliases = {
  --'Bean',
  'Component', 'Configuration',
  'Repository', 'Service', 'Controller', 'RestController',
}

M.stereotype_package = 'org.springframework.stereotype'

-- for @Autowired and @Autoscan
local map_layer2annotation = {
  [0] = '@Compoment',
  service = '@Service',
  services = '@Service',
  controller = '@Controller',  -- @RestController
  controllers = '@Controller', -- @RestController
  repository = '@Repository',
  repositories = '@Repository',
  config = '@Configuration',
  model = '@Entity',
  models = '@Entity',
}
--

--
-- check whether this class has annotations for Autowired and DI operation
-- and if not, add the appropriate one based on the subpackage it is in
--
---@param clazz table{ln, annotations}
---@return table? - the lines of code with new needed annotations
function M.get_required_component_annotations(clazz)
  if not clazz or not clazz.ln then
    return nil
  end

  if syntax.find_annotation_any(clazz, M.compoment_aliases) then
    return nil -- already has
  end

  local layer = string.match(clazz.pkg, '%.([%w_]+)$')
  local annotation = map_layer2annotation[layer or 0] or '@Compoment'

  -- TODO add to import

  return { annotation }
end

--
return M

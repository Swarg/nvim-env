-- 05-09-2024 @author Swarg
-- Goals:
-- - resolve paths in the project for used libraries
--  implemented:
--    - thymeleaf: jump into template file from error in the log

local log = require 'alogger'
local fs = require 'env.files'

--
local M = {}

-- src/main/resources/templates
local DEF_VIEWS_DIR = "templates"
local TEMPL_EXT = 'html'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--
-- pull abs path to templates directory from full path to some source file
---@return false|string
---@return string?  errmsg
function M.get_templ_dir_from(fn, views_dir)
  log_debug("get_templ_dir", fn, views_dir)
  views_dir = views_dir or DEF_VIEWS_DIR
  local _, i = string.find(fn, "/src/main/")
  if not i then
    return false, 'cannot find src/main/ in given:' .. v2s(fn)
  end
  local dir0 = fn:sub(1, i)
  return fs.join_path(dir0, "resources", views_dir)
end

---@param template_name string
---@param project_root string
---@param views_dir string?
---@return string
function M.build_template_path(template_name, project_root, views_dir)
  views_dir = views_dir or DEF_VIEWS_DIR
  return fs.join_path(project_root, views_dir, template_name .. '.' .. TEMPL_EXT)
end

--
--
-- to jump into template file from the error in log
--
---@param line string
---@param project_root string
---@return string?
---@return string?
function M.resolveSourceFrom(line, project_root)
  log_debug("resolveSourceFrom %s |%s|", project_root, line)
  if not line then
    return nil, nil
  end
  local path, jumpto
  local templ_err = match(line, '%[THYMELEAF%].-%(template: ([^%)]+)%)')
  if templ_err then
    --'"auth/login" - line 15, col 28'
    local templ_name, lnum = match(templ_err, '"([^"%s]+)"%s+%-%s+line%s+(%d+)')
    log_debug("templ_name:%s lnum:%s", templ_name, lnum)
    if templ_name then
      path = M.build_template_path(templ_name, project_root)
      jumpto = lnum or 1
    end
  end

  log_debug("say: path:%s lnum:%s", path, jumpto)
  return path, jumpto
end

return M

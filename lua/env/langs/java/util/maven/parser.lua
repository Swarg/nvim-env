-- 28-07-2024 @author Swarg
-- Goal: parse mvn output

-- local su = require 'env.sutil'
local log = require 'alogger'

local M = {}
local log_debug = log.debug


function M.parse_log_line(line)
  local log_level, msg = string.match(line, "^%[([%w]+)%]%s+(.-)%s*$")
  return log_level, msg
end

-- "<id>content</id>" -> "id" "content" "id"
---@param s string
function M.unwrapp_in_tag(s)
  local otag, content, ctag = string.match(s, "<([%w_%-]+)>%s*([^<>]+)%s*</([%w_%-]+)>")
  return otag, content, ctag
end

--
-- Parse global xml config to find server by given id in the servers block
--
-- t.configuration.server
---@param server_id string
---@return table?{user, password}
function M.get_server_props(fn, server_id)
  log_debug("get_server_props", fn)
  local file, errmsg = io.open(fn, "rb")
  local inside_servers, inside_server, active = false, false, false
  local username, password
  if not file then
    log_debug("cannot read file %s", errmsg)
    return nil
  end

  while true do -- read file
    local line = file:read("*l")
    if not line then
      break
    end
    local s = string.match(line, "^%s*(.-)%s*$")
    -- log_debug("line: ", s, inside_servers)

    if not inside_servers then
      if s == '<servers>' then
        -- log_debug("## Entrer to servers block", s)
        inside_servers = true
      end
    else -- inside_servers block
      if not inside_server then
        if s == '</servers>' then
          inside_servers = false
          break
        elseif s == '<server>' then
          inside_server = true
        end
      else -- inside_server block
        if s == '</server>' then
          inside_server = false
        else
          local otag, content, ctag = M.unwrapp_in_tag(s)
          if active then
            if otag == ctag and otag == 'username' then
              username = content
            elseif otag == 'password' then
              password = content
              active = false
              break
            end
          end
          if otag == ctag and otag == 'id' and content == server_id then
            active = true
          end
        end
      end
    end
  end

  file:close()
  return { user = username, password = password }
end

--------------------------------------------------------------------------------
--                     Compile Output parser
--------------------------------------------------------------------------------

---@param lines table
function M.parse_compile_output(lines)
  assert(type(lines) == 'table', 'lines')
  local t = { src_roots = {} }
  local lines_cnt = #lines
  t.handler = M.handler_co_common

  local i = 0
  while i < lines_cnt and t.handler ~= nil do
    i = i + 1
    local line = lines[i]
    if not line then
      break
    end
    t.handler(t, line)
  end
  return t
end

--
function M.handler_co_common(st, line)
  local _, msg = M.parse_log_line(line)
  if msg == "Source roots:" then
    st.handler = M.handler_co_source_root
  end
end

function M.handler_co_source_root(st, line)
  local _, msg = M.parse_log_line(line)
  if msg == "Command line options:" then
    st.handler = M.handler_co_command_line_options
  else
    st.src_roots[#st.src_roots + 1] = msg
  end
end

function M.handler_co_command_line_options(st, line)
  local _, msg = M.parse_log_line(line)
  st.javac_opts = msg
  st.handler = M.handler_co_build_success
end

function M.handler_co_build_success(st, line)
  local ll, msg = M.parse_log_line(line)
  if ll == 'INFO' and msg == "BUILD SUCCESS" then
    st.handler = nil -- finish
  end
end

--------------------------------------------------------------------------------
--[[

[DEBUG] Using compiler 'javac'.
[DEBUG] Adding ~/java/webapp/target/generated-sources/annotations to compile source roots:
  ~/java/webapp/src/main/java
[DEBUG] New compile source roots:
  ~/java/webapp/src/main/java
  ~/java/webapp/target/generated-sources/annotations
[DEBUG] CompilerReuseStrategy: reuseCreated
[DEBUG] useIncrementalCompilation enabled
[INFO] Nothing to compile - all classes are up to date
]]


-- todo with classifier
---@param gav string?
---@return table?{groupId, artifactId, version}
function M.parse_artifact_coords(gav)
  local pattern_gav = "^([^:%s]+):([^:%s]+):([^:%s]+)$"
  local groupId, artifactId, version = string.match(gav or '', pattern_gav)
  if gav and not groupId then
    groupId, artifactId = string.match(gav, "^([^:%s]+):([^:%s]+)$")
    version = nil
  end
  if groupId and artifactId then -- and version then
    return {
      groupId = groupId,
      artifactId = artifactId,
      version = version,
    }
  end
  return nil
end

--
-- parse a one-line string containing the name of the artifact with a classifier
--
---@param gavc string
---@return table?{groupId, artifactId, version, classifier}
function M.parse_artifact_with_classifier(gavc)
  local pattern = "^([^:]+):([^:]+):([^:]+):([^:]+)$"
  local g, a, v, classifier = string.match(gavc or '', pattern)
  if g and a and v and classifier then
    return {
      groupId = g,
      artifactId = a,
      version = v,
      classifier = classifier
    }
  end
  return nil
end

--
-- parse lines with pom.xml dependencies into object representation
--
---@return table
function M.parse_dependencies_lines(lines)
  local t, e = {}, {}
  local match = string.match
  for _, line in pairs(lines or {}) do
    line = match(line, '^%s*(.-)%s*$')
    if line == '</dependencies>' then
      break
    elseif line == '<dependency>' then
      e = {}
    elseif line == '</dependency>' then
      t[#t + 1] = e
    else
      -- <artifactId>artname</artifactId>
      local tag, value = string.match(line, '^<([^%<%>]+)>(.+)</([^%<%>]+)>$')
      if tag then
        e[tag] = value
      end
    end
  end
  if next(e) ~= nil and t[#t] ~= e then
    t[#t + 1] = e
  end

  return t
end

return M

-- 03-10-2024 @author Swarg
-- wrapper around mvn cli tool

local fs = require 'env.files'
local parser = require 'env.langs.java.util.maven.parser'


local M = {}

local HOME = os.getenv('HOME')
local EXECUTABLE = 'mvn'
local LOCAL_MVN_REPO = fs.join_path(HOME, '.m2', 'repository')

local DEPENDENCY_PLAGIN_NAME = "org.apache.maven.plugins:maven-dependency-plugin"
local DEPENDENCY_PLAGIN_VERSION = "3.8.0"

-- https://maven.apache.org/plugins/maven-install-plugin/index.html
-- local INSTALL_PLUGIN_NAME = "org.apache.maven.plugins:maven-install-plugin"
-- local INSTALL_PLUGIN_VERSION = "3.1.3"
-- :install-file



---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local parse_artifact_coords = parser.parse_artifact_coords
local join_path = fs.join_path

---@param gav table?
---@param version_requeired boolean?
function M.is_valid_artifact(gav, version_requeired)
  local ne = function(str)
    return type(str) == 'string' and str ~= ''
  end
  return type(gav) == 'table' and ne(gav.groupId) and ne(gav.artifactId) and
      (ne(gav.version) or not version_requeired)
end

--
--
---@param s string?
---@param version_requeired boolean?
---@return boolean
function M.is_valid_artifact_name(s, version_requeired)
  return M.is_valid_artifact(parse_artifact_coords(s), version_requeired)
end

---@param gav table{groupId,artifactId,version}
function M.get_artifact_relative_path(gav)
  local group = assert(gav.groupId, 'groupId')
  local artifact = assert(gav.artifactId, 'artifactId')
  -- local version = assert(gav.version, 'version')
  local path = join_path(string.gsub(group, '%.', '/'), artifact)
  if gav.version and gav.version ~= '' then
    path = join_path(path, gav.version)
  end
  return path
end

-- 'org.slf4j:slf4j-simple:2.0.16' --> ~/.m2/repository/org/slf4j/slf4j-simple/2.0.16/
---@return false|table:string files
---@return string? err
---@return string? path
function M.get_artifact_files(gav)
  local path = join_path(LOCAL_MVN_REPO, M.get_artifact_relative_path(gav))
  local files, err = fs.list_of_files(path)
  if type(files) ~= 'table' then
    return false, v2s(err), path
  end
  return files, nil, path
end

---@return false|table
---@return string? errmsg
function M.get_artifact_versions(gav)
  local path = join_path(LOCAL_MVN_REPO, M.get_artifact_relative_path(gav))
  local ver_dirs, err = fs.list_of_dirs(path)
  return ver_dirs, err
end

--
-- parse the list of files of one specific version of the artifact
-- from the Maven repository into a convenient structured form
-- with keys equal to classifiers
--
---@param gav table{groupId,artifactId,version}
---@return false|table?{pom, jar, sources, javadoc}
---@return string?
function M.parse_artifact_files(gav, files)
  if not M.is_valid_artifact(gav) then
    return false, 'invalid groupId:artifactId:version'
  end ---@cast gav table

  if type(files) ~= 'table' or #files == 0 then
    return nil, 'no files'
  end

  local t = { pom = nil, jar = nil, sources = nil, javadoc = nil }

  local pref_len = #gav.artifactId + #gav.version + 2
  local psep = fs.path_sep

  for _, name in ipairs(files) do
    local ext = fs.extract_extension(name)
    if ext == "pom" then
      t.pom = name
    elseif ext == "jar" then
      local pref = ''
      if string.find(name, psep, 1, true) then -- is a full path not basename
        pref = fs.ensure_dir(fs.get_parent_dirpath(name) or '')
        name = fs.basename(name)
      end

      -- org.slf4j:slf4j-simple:2.0.16      onle-line artifact name(g:a:v)
      -- slf4j-simple-2.0.16-sources.jar    in maven repo filename
      --                     ^^^^^^^classifier
      local rest = string.sub(name, pref_len, #name)
      if rest == '.jar' then
        t.jar = pref .. name
      else
        local classifier = string.match(rest, "^-(.-)%.jar$")
        t[classifier] = pref .. name
      end
    end
  end

  return t
end

--
-- show the existed files and versions in the local repo for given artifact
--
---@param name string
---@return false|string
---@return string? err
function M.artifact_info(name, opts)
  opts = opts or E
  local gav = parse_artifact_coords(name)
  if not M.is_valid_artifact(gav) then
    return false, 'not a valid artifact name: ' .. v2s(name)
  end
  local s = name .. "\n"
  local files, _, path = M.get_artifact_files(gav)
  if type(files) == 'table' then
    s = s .. v2s(path or '?') .. "\n"
    for _, fn in ipairs(files) do
      local ext = match(fn, '%.([^.]+)$')
      if ext ~= 'sha1' and ext ~= 'repositories' then
        s = s .. v2s(fn) .. "\n"
      end
    end
  end
  -- show existed versions
  local dir = fs.get_parent_dirpath(path or '')
  if dir and fs.dir_exists(dir) then
    local versions = fs.list_of_dirs(dir)
    s = s .. 'Installed versions: ' .. table.concat(versions or E, ', ')
  else
    s = s .. "No installed versions"
  end

  -- check latest version
  -- https://mvnrepository.com/artifact/groupId/artifactId
  -- https://repo1.maven.org/maven2/groupId/artifactId/maven-metadata.xml
  if opts.latest_version then
    local latest = "TODO"
    s = s .. "\nLatest version: " .. latest
  end

  return s
end

--
-- install the given artifact to the local maven repo
--
---@param name string gav
---@param opts table?{sources, javadoc,repository}
---@param lang env.lang.Lang
function M.dependency_get(lang, name, opts)
  if not name or name == '' then
    return false, 'no artifact name (groupId:artifactId:version)'
  end
  local gav = parse_artifact_coords(name)
  if not M.is_valid_artifact(gav) then
    return false, 'invalid groupId:artifactId:version'
  end ---@cast gav table

  opts = opts or E
  -- local cwd = lang:getProjectRoot()

  local pversion = opts.dependency_plugin_version or DEPENDENCY_PLAGIN_VERSION
  local args = {
    DEPENDENCY_PLAGIN_NAME .. ':' .. pversion .. ':get', -- goal
    '-Dartifact=' .. v2s(name)
  }

  if opts.sources then
    args[#args + 1] = '-Dclassifier=sources'
  end
  if opts.javadoc then
    args[#args + 1] = '-Dclassifier=javadoc'
  end

  if opts.repository then
    args[#args + 1] = '-DrepoUrl=' .. v2s(opts.repository)
  end

  if opts.dest then -- "-Ddest=path/to/my.jar";
    args[#args + 1] = "-Ddest=" .. v2s(opts.dest)
  end

  local envs, desc = nil, nil
  local params = lang:newExecParams(EXECUTABLE, args, envs, desc)

  lang:runSysCmd('dependency:get', params)
  return true
end

---@param gav table{groupId,artifactId,version}
---@param args table
function M.append_gav_opts(gav, args)
  args[#args + 1] = '-DgroupId=' .. assert(gav.groupId, 'groupId')
  args[#args + 1] = '-DartifactId=' .. assert(gav.artifactId, 'artifactId')
  args[#args + 1] = '-Dversion=' .. assert(gav.version, 'version')
end

--
---@param lines table?
---@return false|string
---@return string?
function M.lines_to_short_artifact_name(lines)
  if type(lines) ~= 'table' or #lines == 0 then
    return false, 'no lines'
  end
  local t = (parser.parse_dependencies_lines(lines) or E)[1]
  if (t.groupId and t.artifactId) then
    return v2s(t.groupId) .. ':' .. v2s(t.artifactId) .. ':' .. v2s(t.version)
  end
  return false, 'expected lines from pom.xml with <groupId><artifactId>...'
end

return M

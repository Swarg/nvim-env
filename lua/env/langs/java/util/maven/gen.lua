-- 15-07-2024 @author Swarg
-- templates
local M = {}

-- local utempl = require 'env.lang.utils.templater'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

M.POM_HEAD = [[
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

]]
M.POM_FOOTER = [[
</project>
]]


M.TEST_FRAMEWORK_JUNIT_4 = [[
    <dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.13.2</version>
			<scope>test</scope>
		</dependency>
]]
M.DEFAULT_TEST_FRAMEWORK = M.TEST_FRAMEWORK_JUNIT_4

-- see keys  in JBuilder
M.TEMPL_POM_XML = M.POM_HEAD .. [==[
${POM_PARENT}
  <groupId>${GROUP_ID}</groupId>
  <artifactId>${ARTIFACT_ID}</artifactId>
  <version>${VERSION}</version>
  <packaging>${PACKAGING}</packaging>

  <name>${PROJECT_NAME}</name>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>${JAVA_VERSION}</maven.compiler.source>
    <maven.compiler.target>${JAVA_VERSION}</maven.compiler.target>
    <exec.mainClass>${MAINCLASS}</exec.mainClass>
  </properties>

  <dependencies>
${DEPENDENCIES}
${DEPENDENCY_TEST_FRAMEWORK}
  </dependencies>

  <build>
    <pluginManagement>
    </pluginManagement>

    <plugins>

${PLUGINS}

    </plugins>
  </build>

]==] .. M.POM_FOOTER

M.TEMPL_PLUGIN_JAR_WITH_MAINCLASS = [[
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.4.2</version>
        <configuration>
          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <mainClass>${MAINCLASS}</mainClass>
            </manifest>
          </archive>
        </configuration>
      </plugin>
]]
-- <!-- Here come other details .. -->

-- mvn clean compile assembly:single
M.TEMPL_PLUGIN_ASSEMBLY_JAR_WITH_DEPENDENCIES = [[
      <plugin>
        <artifactId>maven-assembly-plugin</artifactId>
        <configuration>
          <archive>
            <manifest>
              <mainClass>fully.qualified.MainClass</mainClass>
            </manifest>
          </archive>
          <descriptorRefs>
            <descriptorRef>jar-with-dependencies</descriptorRef>
          </descriptorRefs>
        </configuration>
      </plugin>
]]

-- https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html
-- mvn clean package shade:shade
M.TEMPL_PLUGIN_SHADE_JAR_WITH_DEPENDENCIES = [[
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>
        <version>3.6.0</version>
        <executions>
          <execution>
            <phase>package</phase>
            <goals>
              <goal>shade</goal>
            </goals>
            <configuration>
              <transformers>
                <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                  <mainClass>${MAINCLASS}</mainClass>
                </transformer>
              </transformers>
            </configuration>
          </execution>
        </executions>
      </plugin>
]]


M.TEMPL_MIN_POM_XML = M.POM_HEAD .. [==[
  <groupId>${GROUP_ID}</groupId>
  <artifactId>${ARTIFACT_ID}</artifactId>
  <version>${VERSION}</version>
]==] .. M.POM_FOOTER


---@param gav table?{groupId, artifactId, version}
function M.gen_min_pom_xml(gav)
  gav = gav or E
  return M.POM_HEAD ..
      '  <groupId>' .. (gav.groupId or 'group') .. "</groupId>\n" ..
      '  <artifactId>' .. (gav.artifactId or 'artifact') .. "</artifactId>\n" ..
      '  <version>' .. (gav.version or '1.0') .. "</version>\n" ..
      "  <packaging>jar</packaging>\n" ..
      M.POM_FOOTER
end

--
---@param gav table?{groupId, artifactId, version, scope, optional}
---@param scope string?
---@return string?
function M.generate_dependency(gav, scope)
  if type(gav) ~= 'table' then
    return nil
  end
  local ex = ''
  scope = scope or gav.scope
  if gav.version then
    ex = ex .. "\n      <version>" .. v2s(gav.version) .. "</version>"
  end
  if scope and scope ~= '' then
    ex = ex .. "\n      <scope>" .. v2s(scope) .. "</scope>"
  end
  if gav.optional then
    ex = ex .. "\n      <optional>" .. v2s(gav.optional) .. "</optional>"
  end

  return fmt([[
    <dependency>
			<groupId>%s</groupId>
			<artifactId>%s</artifactId>%s
		</dependency>]], gav.groupId, gav.artifactId, ex)
end

---@param gav table{groupId, artifactId, version}
function M.dep_to_gralde(gav, prefix)
  if type(gav) == 'table' and gav.groupId and gav.artifactId then
    prefix = prefix or 'implementation'
    local s = prefix .. ' ' .. v2s(gav.groupId) .. ':' .. v2s(gav.artifactId)
    if gav.version then
      s = s .. ':' .. v2s(gav.version)
    end
    return s
  end
  -- implementation 'dev.samstevens.totp:totp:1.7.1'
  return ''
end

--
-- for inner use at langs.java.spring.dependency
--
---@param gav table
---@return string
function M.dep_to_luamap(gav)
  if type(gav) == 'table' and gav.groupId and gav.artifactId then
    local s = "m['" .. v2s(gav.artifactId) .. "'] = {\n" ..
        "  groupId = '" .. v2s(gav.groupId) .. "',\n" ..
        "  artifactId = '" .. v2s(gav.artifactId) .. "',\n"
    local tags = {}
    for tag, _ in pairs(gav) do
      if tag ~= 'groupId' and tag ~= 'artifactId' then
        tags[#tags + 1] = tag
      end
    end
    table.sort(tags)
    for _, tag in ipairs(tags) do
      local v = gav[tag]
      s = s .. "  " .. tag .. " = '" .. v2s(v) .. "',\n"
    end
    return s .. "}\n"
  end
  return ''
end

return M

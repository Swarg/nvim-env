-- 15-07-2024 @author Swarg
-- Generate
--
local log = require 'alogger'
local tu = require 'env.util.tables'

local M = {}

local tbl_merge_flat = tu.tbl_merge_flat

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- mvn archetype:generate
-- -X for debug output
-- -DarchetypeCatalog=http://afbwt03:8081/nexus/content/groups/JavaRepo
-- to speedup project generation
-- https://maven.apache.org/archetype/maven-archetype-plugin/examples/generate-alternative-catalog.html
local templ_cmd_mvn_gen_project = [[
  archetype:generate
  -X
  -DarchetypeCatalog=${ARCHETYPE_CATALOG}
  -DarchetypeGroupId=${ARCHETYPE_GROUP}
  -DarchetypeArtifactId=${ARCHETYPE_ARTIFACT}
  -DarchetypeVersion=${ARCHETYPE_VERSION}

  -DgroupId=${GROUP_ID}
  -DartifactId=${ARTIFACT_ID}
  -Dversion=${VERSION}
  -DinteractiveMode=false

]]
-- to speedup generation without connection to remote catalog
local ARCHETYPE_CATALOG = 'local' -- remote

M.PARAMS_ORDER = {
  'ARCHETYPE_CATALOG',
  'ARCHETYPE_GROUP',
  'ARCHETYPE_ARTIFACT',
  'ARCHETYPE_VERSION'
}


function M.get_archetype_quickstart_props()
  return {
    ARCHETYPE_CATALOG = ARCHETYPE_CATALOG,
    ARCHETYPE_GROUP = "org.apache.maven.archetypes",
    ARCHETYPE_ARTIFACT = "maven-archetype-quickstart",
    ARCHETYPE_VERSION = "1.4",
  }
end

-- https://maven.apache.org/archetypes/maven-archetype-webapp/
--  mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes \
--          -DarchetypeArtifactId=maven-archetype-webapp -DarchetypeVersion=1.4
function M.get_archetype_webapp_props()
  return {
    ARCHETYPE_CATALOG = ARCHETYPE_CATALOG,
    ARCHETYPE_GROUP = "org.apache.maven.archetypes",
    ARCHETYPE_ARTIFACT = "maven-archetype-webapp",
    ARCHETYPE_VERSION = "1.4",
  }
end

---@param name string? quickstart|webapp
function M.get_archetype_default_props(name)
  if not name or name == 'quickstart' then
    return M.get_archetype_quickstart_props()
  elseif name == 'webapp' then
    return M.get_archetype_webapp_props()
  end
end

-- function M.mk_builder_info(t)
--   log.debug("mk_builder_info", t)
--   if type(t) == 'table' and t.ARCHETYPE_GROUP then
--     return fmt("Maven: archetype:generate [%s %s %s %s]\n",
--       v2s(t.ARCHETYPE_CATALOG), v2s(t.ARCHETYPE_GROUP),
--       v2s(t.ARCHETYPE_ARTIFACT or ''), v2s(t.ARCHETYPE_VERSION or ''))
--   end
--   return ''
-- end

---@param t table
---@param key string
---@param def string
local function not_empty_or_def(t, key, def)
  local s = t[key]
  if (s == nil or s == '') then
    t[key] = assert(def, 'default value')
  end
  return t[key]
end

---@param s string
---@return table
local function split_args(s)
  local t = {}
  for arg in string.gmatch(s, '([^%s]+)') do table.insert(t, arg) end
  return t
end

---@param t table
---@return table args for mvn-system-command
function M.gen_args_archetype_generate(t)
  log.debug("gen_args_archetype_generate", t)
  local sub_ptrn = "%${([%w_]+)}" -- ${KEY} ${THE_KEY}
  local def = M.get_archetype_default_props()

  -- shorthands for defining in UI
  if t.ARCHETYPE_ARTIFACT == 'webapp' then
    t = tbl_merge_flat(t, M.get_archetype_webapp_props())
  elseif t.ARCHETYPE_ARTIFACT == 'quickstart' then
    t = tbl_merge_flat(t, M.get_archetype_quickstart_props())
  end

  not_empty_or_def(t, 'ARCHETYPE_CATALOG', ARCHETYPE_CATALOG or 'remote')
  not_empty_or_def(t, 'ARCHETYPE_GROUP', def.ARCHETYPE_GROUP)
  not_empty_or_def(t, 'ARCHETYPE_ARTIFACT', def.ARCHETYPE_ARTIFACT)
  not_empty_or_def(t, 'ARCHETYPE_VERSION', def.ARCHETYPE_VERSION)
  not_empty_or_def(t, 'GROUP_ID', 'pkg')
  not_empty_or_def(t, 'ARTIFACT_ID', 'app')
  not_empty_or_def(t, 'VERSION', '1.0-SNAPSHOT')

  return split_args(string.gsub(templ_cmd_mvn_gen_project, sub_ptrn, t))
end

--

local PROJ_FROM_ARCHETYPE_PTRN = "%[INFO%]%s+Project created from Archetype in dir:%s*([^\n]+)%s*"
function M.find_generated_proj_dir(output)
  if type(output) == 'string' then
    local path = string.match(output, PROJ_FROM_ARCHETYPE_PTRN)
    if path and path ~= '' then
      return path
    end
  end
  return nil
end

return M

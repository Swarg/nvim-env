-- 20-07-2024 @author Swarg
local M = {}
--

local home = os.getenv("HOME") or ''

M.SRC_MAIN_JAVA = "src/main/java/"
M.SRC_TEST_JAVA = "src/test/java/"

M.SRC_MAIN_WEBAPP = "src/main/webapp/"

M.DEFAULT_WORKSPACE = home .. "/workspace/"

M.GLOBAL_MAVEN_CONF_SETTINGS = '/opt/maven/conf/settings.xml'

-- Extended type for ClassInfo
M.CT_WEBTEMPLATE = 10 -- files in the src/main/webapp

M.TOMCAT_WEBAPPS_DIR = home .. "/tomcat/webapps"


M.JAVA_KEYWORD_LIST = {
  'abstract', 'assert', 'boolean', 'break', 'byte', 'case', 'catch', 'char',
  'class', 'const', 'continue', 'default', 'do', 'double', 'else', 'enum',
  'extends', 'final', 'finally', 'float', 'for', 'goto', 'if', 'implements',
  'import', 'instanceof', 'int', 'interface', 'long', 'native', 'new',
  'package', 'private', 'protected', 'public', 'record', 'return',
  'short', 'static', 'strictfp', 'super', 'swith', 'synchronized',
  'this', 'throw', 'throws', 'transient', 'try',
  'var', 'void', 'volatile', 'while',
  -- ,'exports', 'requires',  'module',
}

M.JAVA_OPETERATOR_LIST = {
  '!=', '!', '==', '=', ',=', '-=', '+=',
  '<<', '<=', '<-', '<', '>>', '>=', '>', '->',
  '--', '-', ',,', ',', '&&', '&', '||', '|',
  '^', '*', '/', '%', '?'
}

M.JAVA_PRIMITIVE_TYPE_LIST = {
  'byte', 'short', 'char', 'int', 'long', 'float', 'double', 'boolean'
}
M.JAVA_PRIM_VALUES_MAP = {
  ['true'] = 1,
  ['false'] = 1,
  ['null'] = 1,
}

---@return table
local function list2map(t)
  local m = {}; for _, elm in ipairs(t) do m[elm] = true end; return m
end

M.JAVA_KEYWORD_MAP = list2map(M.JAVA_KEYWORD_LIST)
M.JAVA_OPETERATOR_MAP = list2map(M.JAVA_OPETERATOR_LIST)
M.JAVA_PRIMITIVE_TYPE_MAP = list2map(M.JAVA_PRIMITIVE_TYPE_LIST)

--

-- diagnostic from jdtls
M.ERROR = {
  WRONG_METHOD_ARGS = 'WRONG_METHOD_ARGS'
}


return M

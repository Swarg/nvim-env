-- 30-09-2024 @author Swarg
--

local log = require 'alogger'
local fs = require 'env.files'
local tu = require 'env.util.tables'
local JParser = require 'env.langs.java.JParser'
local CONST = require 'env.langs.java.util.consts'
local FileIterator = require 'olua.util.FileIterator'

local KEYWORDS = tu.list_to_map(CONST.JAVA_KEYWORD_LIST, true)

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local join_path, dir_exists = fs.join_path, fs.dir_exists
local log_debug, log_trace, log_is_trace = log.debug, log.trace, log.is_trace

local M = {}

local function vprint(flag, ...)
  if flag then
    print(...)
  end
end

-- local mapping, err, namescnt = M.load_csv_mappings(mapping_dir)
-- if not mapping then
--   return false, err
-- end
-- vprint(verbose, 'Names in mapping:', namescnt)

---@param mappings table?
---@return table{fields, methods, params}
local function validate_mappings(mappings)
  assert(type(mappings) == 'table', 'mappings')
  assert(type(mappings.fields) == 'table', 'mappings.fields')
  assert(type(mappings.methods) == 'table', 'mappings.methods')
  assert(type(mappings.params) == 'table', 'mappings.params')
  return mappings
end


---@param dir string directory with sources or source file
---@param mappings table{fields, methods, params}
---@param opts table?{recursive, verbose}
---@return false|table report
---@return string? err
function M.apply_name_mapping_to_dir(dir, mappings, opts)
  local recursive = (opts or E).recursive
  local verbose = (opts or E).verbose

  validate_mappings(mappings)

  local files = nil
  if fs.is_file(dir) then
    files = { dir }
  else
    if not dir_exists(dir) then
      return false, 'directory not found ' .. v2s(dir)
    end
    files = (recursive == true) and fs.list_of_files(dir) or
        fs.list_of_files_deep(dir, '%.java$')
  end

  if not files or #files == 0 then
    return false, 'not found any java file in dir: ' .. v2s(dir)
  end


  local cnt = #files
  local report = {
    files = {},
    errors = 0
  }

  for n, fn in ipairs(files) do
    vprint(verbose, n, '/', cnt, fn)
    local file_entry = { path = fn, success = false, error = nil }

    local lines, errmsg = M.apply_name_mapping_to_file(fn, mappings, report)
    if not lines then
      vprint(verbose, errmsg)
      report.errors = report.errors + 1
      file_entry.error = errmsg
    else
      if fs.write(fn, lines, 'wb') then
        file_entry.success = true
      else
        vprint(verbose, 'error on save file:' .. v2s(fn))
        report.errors = report.errors + 1
        file_entry.error = 'not saved'
      end
    end
    report.files[#report.files + 1] = file_entry
  end

  return report, nil
end

--
--
--
---@return false|table
---@return string?
---@return number parsed lines
function M.load_csv_file(fn, out)
  out = out or {}
  local h, err = io.open(fn, 'rb')
  if not h then
    return false, 'cannot read file:' .. v2s(fn) .. ': ' .. v2s(err), 0
  end
  local c = 0
  while true do
    local line = h:read('*l')
    if not line then
      break
    end
    -- searge,name,side,desc
    -- field_100013_f,isPotionDurationMax,0,"True if potion effect duration is at maximum, false otherwise."
    local deob, name, _ = match(line, '^([%w_]+),([%w_]+),(%d)')
    if deob and name then
      out[deob] = name
      c = c + 1
    end
  end
  return out, nil, c
end

--
--
---@param dir string
---@return false|table
---@return string? err
---@return number? parsed lines with name mapping
function M.load_csv_mappings(dir)
  if not dir_exists(dir) then
    return false, 'directory not found ' .. v2s(dir)
  end

  local t = {
    fields = {},
    methods = {},
    params = {},
  }
  local ok, err, c1, c2, c3

  ok, err, c1 = M.load_csv_file(join_path(dir, 'fields.csv'), t.fields)
  if not ok then return false, err end

  ok, err, c2 = M.load_csv_file(join_path(dir, 'methods.csv'), t.methods)
  if not ok then return false, err end

  ok, err, c3 = M.load_csv_file(join_path(dir, 'params.csv'), t.params)
  if not ok then return false, err end

  return t, nil, (c1 + c2 + c3)
end

---@param report table
---@param section string
local function inc_report_section(report, section)
  if report and section then
    report[section] = (report[section] or 0) + 1
  end
end

---@param fn string
---@param mapping table{fields, methods, params}
---@return false|table - lines
---@return string? errmsg
---@param report table
function M.apply_name_mapping_to_file(fn, mapping, report)
  log_debug("apply_name_mapping_to_file", fn)
  assert(type(mapping) == 'table', 'mapping')
  local lines = {}
  local parser = JParser:new(nil, FileIterator:new(nil, fn, lines))
  parser:prepare(nil) --parseClassBody()

  if parser:hasErrors() then
    return false, parser:getErrLogger():getReadableReport()
  end

  -- build plan for mapping
  local clazz = parser:result() -- parsed clazz

  ---@param ln number
  ---@param oldname string
  ---@param newname string
  local function replace_old_by_new_name(ln, oldname, newname, section)
    assert(type(ln) == 'number' and ln > 0, 'expected number lnum')

    local orig_line = lines[ln]
    if not orig_line then
      error(fmt('no line with lnum:%s at:%s oldname:%s',
        v2s(ln), v2s(section), v2s(oldname)))
    end
    if string.find(orig_line, oldname, 1, true) then
      local new_line = string.gsub(orig_line, oldname, newname)
      lines[ln] = new_line
      inc_report_section(report, section)
    end
  end

  for _, e in ipairs(clazz.fields or E) do
    local aname = mapping.fields[e.name or false]
    if aname then
      replace_old_by_new_name(e.ln, e.name, aname, 'fields')
    end
  end

  -- signature and body of methods and constructors
  for _, me in ipairs(clazz.methods or E) do
    local aname = mapping.methods[me.name or false]
    if aname then
      replace_old_by_new_name(me.ln, me.name, aname, 'methods')
    end
    for _, pe in ipairs(me.params or E) do
      local param_name = pe[2]
      aname = mapping.params[param_name or false]
      if aname then
        for ln = me.ln, me.lne, 1 do
          if ln == me.lne and not lines[ln] then break end
          replace_old_by_new_name(ln, param_name, aname, 'params')
        end
      end
    end
    M.apply_name_mapping_to_lines(lines, me.ln, me.lne, mapping, report)
  end

  return lines, nil
end

--
--
---@param lines table
---@param lnum_start number
---@param lnum_end number
---@param rep table - report
function M.apply_name_mapping_to_lines(lines, lnum_start, lnum_end, mapping, rep)
  local in_comment = false
  for ln = lnum_start, lnum_end, 1 do
    local line = lines[ln]
    if not line and ln == lnum_end then
      break
    end
    if not line then
      error('no line for lnum:' .. v2s(ln))
    end
    if line ~= '' and match(line, '^%s*//') == nil then
      if in_comment and match(line, '^%s*%*/') then
        in_comment = false
      elseif match(line, '^%s*/%*') then
        in_comment = true
      elseif not in_comment then
        M.apply_name_mapping_to_line(lines, ln, mapping, rep)
      end
    end
  end
end

--
-- todo comments
--
---@param lines table
---@param ln number
---@param mapping table{fields, methods, params}
---@param report table
function M.apply_name_mapping_to_line(lines, ln, mapping, report)
  local line = lines[ln]
  log_trace("apply_name_mapping_to_line %s |%s|", ln, line)
  local lexemes = JParser.splitLineToLexemes(line, true)
  local has_changes = false

  for i = #(lexemes or E), 1, -1 do
    local entry = (lexemes or E)[i]
    local oldname, col_s, col_e = entry[1], entry[2], entry[3]
    local must_be_checked = col_e - col_s > 3 and KEYWORDS[oldname] == nil

    if must_be_checked then
      local new_name, section
      new_name, section = mapping.params[oldname], 'params'
      if not new_name then
        new_name, section = mapping.methods[oldname], 'methods'
        if not new_name then
          new_name, section = mapping.fields[oldname], 'fields'
        end
      end

      if new_name then
        line = line:sub(1, col_s - 1) .. new_name .. line:sub(col_e + 1)
        has_changes = true
        inc_report_section(report, section)
      end
      log_trace('old:|%s| new:|%s| updated-line:%s', oldname, new_name, line)
      --
    elseif log_is_trace() then
      log_trace('oldname: %s skiped', oldname)
    end
  end

  if has_changes then
    lines[ln] = line
  end
  return line
end

return M

-- 09-08-2024 @author Swarg
local M = {}

local CONST = require 'env.langs.java.util.consts'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local KW = CONST.JAVA_KEYWORD_MAP

function M.isKeyWord(s)
  return KW[s or 0] ~= nil
end

function M.isOperator(s)
  return CONST.JAVA_OPETERATOR_MAP[s or 0] ~= nil
end

-- given string is empty or contains only one line comment
---@return boolean
function M.isEmptyOrComment(s)
  return match(s or '', '^%s*$') ~= nil      -- empty
      or match(s, '^%s*//') ~= nil           -- one line comment
      or match(s, '^%s*/%*.-%*/%s*$') ~= nil -- one line comment
  -- ignored cases:
  -- or match(s, '^%s*%*') ~= nil      -- * part of multiline comment
  -- or match(s, '^%s*/%*') ~= nil     -- /** or /*
  -- ?? */ here the code
end

function M.isComment(s)
  return s and (match(s, '^%s*//') ~= nil or match(s, '^%s*/%*.-%*/%s*$') ~= nil)
end

-- Short?? MyClass
function M.isClassName(s)
  return type(s) == 'string' and match(s, '^%u[%w_]*%$?[%w_]*$') ~= nil
end

-- check is given word can be the class name without package
-- ClassName - yes
-- CONSTANT  - no
-- instance  - no
-- 0123      - no
---@param s string?
---@return boolean
function M.isShortClassName(s)
  if type(s) == 'string' and #s > 0 then
    if #s > 4 then -- CamelCase, Ca
      return (string.match(s, "^%u[%a_%d%$]+[%l]+[%a_%d]*$") ~= nil)
    else           -- URI OS etc
      return (string.match(s, "^%u[%a]+$") ~= nil)
    end
  end
  return false
end

---@param s any
---@return boolean
function M.isFullClassNameWithPackage(s)
  if type(s) == 'string' and #s > 0 then
    local p = "^%l[%l%.]+%.[%w%.]*%u[%a_%d%$]+[%l]+[%a_%d]*$"
    return string.match(s, p) ~= nil and string.match(s, '%.%.') == nil
  end
  return false
end

-- check is given string starts with some package like "org.comp.Class"
---@param s string?
function M.isStartsWithPackage(s)
  return s and #s > 0 and (s:match('^[%l]+%.[%l]+') ~= nil)
end

function M.getPackage(s)
  return match(s, '^%s*(.-)%.%u[%w%$_]*%s*$')
end

-- for variables and methods not a Classnames
function M.isVariableName(s)
  return type(s) == 'string' and match(s, '^%l[%w_]*$') ~= nil and KW[s] == nil
end

function M.isConstantName(s)
  return type(s) == 'string' and match(s, '^[%u_][%u%d_]*$') ~= nil and KW[s] == nil
end

-- todo check keywords
---@param s string
---@param token table?
---@param msg string?
function M.validatedVariableName(s, token, msg)
  if not M.isVariableName(s) then
    local details = ''
    if type(token) == 'table' then
      details = ' token: ' .. require "inspect" (token)
    end
    error(fmt('expected valid variable name for "%s" got:"%s"%s',
      v2s(msg or '?'), v2s(s), v2s(details)))
  end
  return s
end

function M.validatedClassFieldName(s, token, msg)
  if not M.isVariableName(s) and not M.isConstantName(s) then
    local details = ''
    if type(token) == 'table' then
      details = ' token: ' .. require "inspect" (token)
    end
    error(fmt('expected valid field name for "%s" got:"%s"%s',
      v2s(msg or '?'), v2s(s), v2s(details)))
  end
  return s
end

---@param s string
---@param token table?
---@return string
function M.validatedClassName(s, token, msg)
  if not M.isClassName(s) then
    local details = ''
    if type(token) == 'table' then
      details = ' token: ' .. require "inspect" (token)
    end
    error(fmt('expected valid class name for "%s" got:"%s"%s',
      v2s(msg or '?'), v2s(s), v2s(details)))
  end
  return s
end

function M.validatedMethodName(s, token, msg)
  return M.validatedVariableName(s, token, msg)
end

--
-- some.comp.handlers { some.comp = some.comp.subpkg } -- > some.comp.subpkg
--
---@param s string
---@param map table
function M.find_mapping_starts_with(s, map, sep)
  sep = sep or '.'
  for key, _ in pairs(map) do -- key is a package or a full class name
    if s == key or
        -- starts with
        #s > #key and s:sub(1, #key) == key and s:sub(#key + 1, #key + 1) == sep
    then
      return key
    end
  end
  return nil
end

local IMPORT_INLINE_MAPPING_PATTS = {
  -- import pkg.old.ClassA; // pkg.new.ClassA
  "^%s*import%s+([^%s/;]+)%s*;%s*//%s*([^%s/;]+)%s*$",
  -- import pkg.old.ClassA; // import pkg.new.ClassA;
  "^%s*import%s+([^%s/;]+)%s*;%s*//%s*import%s+([^%s/;]+)%s*;?%s*$",

  -- import static pkg.old.ClassA.method; // pkg.new.ClassA
  "^%s*import%s+static%s+([^%s/;]+)%.[%w_]+%s*;%s*//%s*([^%s/;]+)%s*$",
  -- import static pkg.old.ClassA; // import static pkg.new.ClassA;
  "^%s*import%s+static%s+([^%s/;]+)%.[%w_]+%s*;%s*"
  .. "//%s*import%s+static%s+([^%s/;]+)%.[%w_]+%s*;%s*$",
}
--
-- to pick mapping from the current line in the editor
--
---@param line string
function M.parse_import_mapping(line)
  local from, to
  for _, patt in ipairs(IMPORT_INLINE_MAPPING_PATTS) do
    from, to = string.match(line, patt)
    if from and from ~= '' then
      break
    end
  end
  return from, to
end

return M

-- 07-03-2025 @author Swarg
-- generate code to Gson

local log = require 'alogger'
local class = require 'oop.class'
-- local uyaml = require 'env.util.yaml'
local ucore = require 'env.langs.java.util.core'
local ClassInfo = require("env.lang.ClassInfo")
-- local AccessModifiers = require 'env.lang.oop.AccessModifiers'
--
-- local Field = require 'env.lang.oop.Field'
-- local Docblock = require 'env.lang.oop.Docblock'
-- local FieldGen = require 'env.lang.oop.FieldGen'
--
-- local upojo = require 'env.langs.java.util.openapi.pojo'


class.package 'env.langs.java.util.openapi'
---@class env.langs.java.util.openapi.GsonCodeGen : oop.Object
---@field new fun(self, o:table?, codegen: env.langs.java.util.openapi.CodeGen): env.langs.java.util.openapi.GsonCodeGen
---@field codegen env.langs.java.util.openapi.CodeGen
---@field tab string '    '
---@field output table (lines of the generated source code)
local C = class.new_class(nil, 'GsonCodeGen', {
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug, log_trace = log.debug, log.trace
local getVarNameForClassName = ClassInfo.getVarNameForClassName
local getShortClassNameOf = ClassInfo.getShortClassNameOf

local gson_adapter_imports = {
  'com.google.gson.Gson',
  'com.google.gson.JsonElement',
  'com.google.gson.JsonObject',
  'com.google.gson.JsonParser',
  'com.google.gson.TypeAdapter',
  'com.google.gson.stream.JsonReader',
  'com.google.gson.stream.JsonWriter'
}

--
-- constructor
--
function C:_init(codegen, tab)
  self.codegen = codegen
  self.tab = tab or (codegen or E).tag or '    '
end

---@param n number
function C:mk_ind(n)
  local tab = self.tab
  local ind = ''; for _ = 1, n, 1 do ind = ind .. tab end; return ind
end

---@param ind number
function C:add(ind, s)
  self.output = self.output or {}
  self.output[#self.output + 1] = self:mk_ind(ind) .. s
end

function C:addf(ind, s, ...)
  self.output = self.output or {}
  self.output[#self.output + 1] = self:mk_ind(ind) .. string.format(s, ...)
end

--[[
public class DnsRecordAdapter extends TypeAdapter<DnsRecord> {

    private final Gson gson = new Gson();

    @Override
    public void write(JsonWriter out, DnsRecord value) throws IOException {
        gson.toJson(value, value.getClass(), out);
    }

    @Override
    public DnsRecord read(JsonReader in) throws IOException {
        JsonElement jsonElement = JsonParser.parseReader(in);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String recordType = jsonObject.get("type").getAsString();
        DnsRecordType dnsRecordType = DnsRecordType.valueOf(recordType);

        switch (dnsRecordType) {
            case A:
                return gson.fromJson(jsonElement, ARecord.class);
            case AAAA:
                return gson.fromJson(jsonElement, AAAARecord.class);
            ...

            default:
                return gson.fromJson(jsonElement, DnsRecordSharedFields.class);
        }
    }
}
]]

-- for cases when in one field you hold information of the specified instance
-- e.g. in samples DnsRecord(interface) with field type that actually is an enum
---@param enum_values table
---@param interface_fqcn string
---@param enum_field_name string (like type in DnsRecord)
function C:genTypeAdapterForEnum(enum_values, interface_fqcn, enum_field_name)
  log_debug("genTypeAdapterForEnum", interface_fqcn, enum_values, enum_field_name)
  self.output = {}
  enum_field_name = enum_field_name or 'type'

  local pkg = ucore.getPackage(interface_fqcn)
  local scn = getShortClassNameOf(interface_fqcn) or '?'
  local vn = getVarNameForClassName(scn) .. 'Type'

  local imports_map = {}
  local enum_impls_map = {}
  local impls = self.codegen:get_implementations_of(interface_fqcn)
  log_debug("found:%s implementations of %s ", #(impls or E), interface_fqcn)

  local default_impl_scn = nil

  for _, fqcn in ipairs(impls) do
    local clazz = self.codegen:get_clazz(fqcn)
    if clazz then
      local field = self.codegen.get_elm_by_name(clazz.fields, enum_field_name)
      if field and type(field.enum) == 'table' then
        local enum_cnt = #(field.enum or E)
        if enum_cnt == 1 then
          local enum = (field.enum or E)[1]
          log_debug('enum: %s -> %s', enum, fqcn)
          enum_impls_map[enum] = clazz.name
          -- todo add clazz.fqcn to import
          imports_map[fqcn] = 1
        elseif enum_cnt > 1 then
          default_impl_scn = getShortClassNameOf(fqcn)
          imports_map[fqcn] = 1
        else
          error('expected list-1 enum got 0 in ' .. v2s(fqcn))
        end
      end
    else
      error('not found fqcn' .. v2s(fqcn))
    end
  end
  local imports = {}
  for import, _ in pairs(imports_map) do imports[#imports + 1] = import end
  table.sort(imports)

  self:add(0, 'package ' .. v2s(pkg) .. '.adapter' .. ';')
  self:add(0, '')
  self:add_imports(gson_adapter_imports)
  self:add(0, '')
  self:add_imports(imports)
  --
  self:add(0, '')
  self:add(0, 'public class ' .. scn .. 'Adapter extends TypeAdapter<' .. scn .. '> {')

  self:add(1, '@Override')
  self:addf(1, 'public %s read(JsonReader in) throws IOException {', scn)
  self:add(2, 'JsonElement jsonElement = JsonParser.parseReader(in);')
  self:add(2, 'JsonObject jsonObject = jsonElement.getAsJsonObject();')
  self:addf(2, 'String %s = jsonObject.get("%s").getAsString();', enum_field_name, enum_field_name)
  -- self:addf(2, 'DnsRecordType dnsRecordType = DnsRecordType.valueOf(recordType);')
  self:addf(2, '%sType %s = %sType.valueOf(%s); // Str2Enum', scn, vn, scn, enum_field_name)
  self:addf(0, '')

  self:add(2, 'switch (' .. vn .. ') {')

  for _, enum in ipairs(enum_values) do
    local impl_fqcn = enum_impls_map[enum]
    local impl = getShortClassNameOf(impl_fqcn) or '?'
    self:add(3, 'case ' .. v2s(enum) .. ':')
    self:add(4, 'return gson.fromJson(jsonElement, ' .. v2s(impl) .. '.class);')
  end

  self:add(3, 'default:')
  self:add(4, 'return gson.fromJson(jsonElement, ' .. v2s(default_impl_scn) .. '.class);')

  self:add(2, '}') -- switch
  self:add(1, '}') -- method

  self:add(0, '}')
  return self.output
end

---@param imports table
function C:add_imports(imports)
  assert(type(imports) == 'table', 'imports')
  for _, import in ipairs(imports) do
    self:add(0, 'import ' .. import .. ';')
  end
end

class.build(C)
return C

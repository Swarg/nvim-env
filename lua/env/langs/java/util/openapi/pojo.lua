-- 01-03-2025 @author Swarg
-- OpenAPI scheme converter to java classes
--  - support oneOf-relation with an interface and abstract class generation
--  - support complex logic with allOf + ref-to-oneOf + properties
--  - support direct generation of the "interface" class (defined as oneOf)
--  - correct way to evaluate common fields for interface-like relationships
--  - show required(fields) in docblock of the class
--  - use required(fields) to keep same field order in output classes
--
--  TODO
--   - fields order in impl classes. first all from interface then its own
--   - way to specify desired order of the fields
--

local log = require 'alogger'
local uyaml = require 'env.util.yaml'
local utbl = require 'env.util.tables'
local ubase = require 'env.lang.utils.base'
local AccessModifiers = require 'env.lang.oop.AccessModifiers'


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local log_trace = log.trace

local CLASS = nil
local INTERFACE = 'interface'
local ABSTRACT = 'abstract'
-- local RECORD = 'record' -- java16+
local LIST_IMPORT = 'java.util.List'
local MAP_IMPORT = 'java.util.Map'

M.COMMON_FIELDS = 'common_fields'

-- helper
local function inspect(v)
  return require "inspect" (v)
end

local function add_err(state, err)
  assert(type(state) == 'table', 'state')
  if state and err then
    log_debug('add_err:', err)
    state.errors = state.errors or {}
    state.errors[#state.errors + 1] = err
  end
  return err
end

--
-- #/compoment/schemas/name -> {"compoment", "schemas", "name"}
---@return table
function M.split_key_to_parts(ref)
  local path = {}
  for key in ref:gmatch("([^/]*)/?") do -- split to path
    if key and key ~= '' and key ~= '#' then path[#path + 1] = key end
  end
  return path
end

-- ClassName --> className
local function to_varname(s)
  return string.sub(s, 1, 1):lower() .. string.sub(s, 2)
end

--
-- stage 1: assembly specfied api-compoment-schemas(keys) as an classes to be
-- generated as the POJO's
--
-- generate POJO classes to the current opened project
-- generate only for specified keys inside given yaml file with contains OpenAPI
-- api-schemas.
--
---@param yml table parsed yml
---@param keys table of strings   like  #/component/schemas/...
---@param opts table?{as_records, required_only}
---@return table? report
---@return string? errsg
function M.generate_classes(yml, keys, pkg, opts)
  log_debug("generate_classes keys:[%s] opts:%s", keys, opts)
  assert(type(keys) == 'table', 'keys of schemas to generate')
  local state = {
    pkg = pkg,                  -- aka org.app.api.model
    opts = opts or {
      as_records = nil,         -- generate records(java16+) instead of regular classes
      with_inner_classes = nil, -- generate inner classes for nested objects
    }
  }
  state.yml = yml
  state.errors = state.errors or {}
  state.report = state.report or {}

  -- key is a full path inside api-compoments schemes
  -- (an entity of the OpenAPI to be generated as POJO)
  -- inside OpenAPI key can represent as $ref so another name for this is "ref"
  for _, key in ipairs(keys) do
    local clazz, err = M.generate_pojo_class(state, key, pkg)
    if not clazz or not clazz.name then
      log_debug('has fatal error: {}', err)
      return nil, err
    end
  end

  return state, nil
end

--
--
--
---@param state table{classes,chached_refs, yml, lang}
---@param key string the full key path of the api-compoment scheme to generate (ref)
---@param pkg string root package for fqcn of generated class
---@return table? clazz struct of POJO to save as javacode
---@return string? errmsg
function M.generate_pojo_class(state, key, pkg)
  log_debug("generate_pojo_class %s pkg:%s", key, pkg)
  if type(key) ~= 'string' or #key < 2 then
    return nil, add_err(state, 'expected key of a compoment schema, got: ' ..
      type(key))
  end
  if not pkg then
    return nil, add_err(state, 'no root java package for generated class files')
  end

  M.enshure_cache_initialized(state)

  local clazz = M.get_already_created_clazz(state, nil, key)
  if clazz then return clazz end

  local node, err0, path = uyaml.open_node(state.yml, key)
  if type(node) ~= 'table' or not path then
    return nil, add_err(state, err0 or ('cannot open yml-node ' .. v2s(key)))
  end

  local ntype = node['type']
  if not ntype and node['allOf'] then
    ntype = node.allOf.type or 'object' -- ?
  end
  if not ntype then return nil, 'no type in ' .. key end
  if ntype ~= 'object' then
    return nil, add_err(state, 'expected node with type:object got: ' .. v2s(ntype))
  end

  -- mark the key of the generated api-component
  state.keypath = key -- #/component/schemas/... -- ?? improve, stack??
  local err
  local pkg0, classname = M.gen_pkg_and_class_name(state, path, pkg)

  clazz, err = M.build_pojo_class(state, pkg0, classname, node, key, nil, nil)

  log_debug('generate_pojo_class %s ret:%s %s', key, (clazz or E).fqcn, err or 'no err')

  return clazz, err
end

--
-- with same package checking
---@param clazz table
---@param import string|table{fqcn}
local function add_import(clazz, import)
  clazz.imports = clazz.imports or {}
  local fqcn = nil
  if type(import) == 'string' then
    local pkg0 = match(import, '^(.-)%.[^%.]+$')
    if clazz.pkg ~= pkg0 then
      fqcn = import
    end
  elseif type(import) == 'table' and import.fqcn then
    if import.pkg ~= clazz.pkg then
      fqcn = import.fqcn
    end
  end

  if fqcn then
    clazz.imports[fqcn] = true
  end
end

---@param state table{classes}
---@param clazz table{fields, imports}
function M.organize_imports(state, clazz)
  log_debug("organize_imports", (clazz or E).fqcn)
  state = state
  local cnt = #((clazz or E).fields or E)
  local function add(s)
    clazz.imports = clazz.imports or {}
    clazz.imports[s] = true
  end
  for i = 1, cnt do
    local field = clazz.fields[i]
    local container = match(field.type or '', '^([^<>]+)<')
    if container == 'List' then
      add(LIST_IMPORT)
    elseif container == 'Map' then
      add(MAP_IMPORT)
    end
  end
end

--
-- Assembly all parts of the api-component-schmema in one whole (assembled schema)
-- (in another words resolve all parts of the object defined in the parsed yml)
--
-- It grew out of a simple resolving for "properties", then from walking through
-- such subsidies as allOf and oneOf, etc...
--
-- pick content of the "properties" key in node or
-- assembly properties from allOf or oneOf
--
---@param state table
---@param node table
---@param key string aka $ref (or classname)
---@return table? prepared props to build class from schmema
---@return string? errmsg?
---@return string? parent_key
---@return table? common_fields (for abstract class or interface)
function M.assembly_object_parts(state, node, key)
  local props, allOf, oneOf = node['properties'], node['allOf'], node['oneOf']

  log_debug("assembly_object_parts key:%s node:%s props:%s allOf:%s oneOf:%s",
    key, node ~= nil, props ~= nil, allOf ~= nil, oneOf ~= nil)

  local parent_key, err, cf
  -- key of the parent api compoment (to support extends for java classes)

  if not props then
    if type(allOf) == 'table' then
      props, err, parent_key = M.assembly_obj_parts_from_allOf(state, allOf, key)
    elseif type(oneOf) == 'table' then
      props, err, cf = M.assembly_obj_parts_from_oneOf(state, oneOf, key)
    elseif string.find(node.description or '', 'Extra', 1, true) ~= nil then
      log_debug("anonim object desc:%s", node.description)
      -- description = "Extra X-specific information about the Y."
      -- props = {} -- use Object type ??
    end

    if not props then
      err = 'no properties in ' .. key .. " node:\n" .. M.get_tbl_keys_s(node)
      props = nil
    end
  elseif type(props) == 'table' then
    props = M.assembly_refs_to_non_object(state, props, key)
  end

  return props, err, parent_key, cf
end

---@param node table{type}
---@return boolean
function M.is_primitive_type(node)
  return node ~= nil and node.type ~= 'object' and node.type ~= 'array'
end

--
-- copying properties to another table here I did to avoid affecting
-- properties elsewhere.
-- as started attempt to fix issue with A AAAA
--
---@param state table
---@param props table
---@param key string
function M.assembly_refs_to_non_object(state, props, key)
  log_trace("assembly_refs_to_non_object", key)
  local props0 = {}

  for prop_name, prop_body in pairs(props) do
    local ref = prop_body['$ref']
    log_trace('prop_name:%s ref:%s', prop_name, ref)

    if ref then
      local node, err0 = M.resolve_ref(state, ref, key .. '/' .. prop_name)
      if type(node) ~= 'table' then
        error(v2s(err0 or ('cannot resolve_ref:' .. v2s(ref))))
      end
      -- it here pass objects refs too then it broke classnaming
      -- e.g. case: List<Error> instead of List<Messages> in
      -- test example with dns-records_api-response-collection
      if M.is_primitive_type(node) then
        prop_body = node
        -- log_trace("anode:", inspect(anode))
      else
        -- kep prop_body with $ref to object or array for class naming
        -- by this $ref will be generated fqcn and new class
      end
    end
    props0[prop_name] = prop_body
  end
  return props0
end

--
-- collect entrie prop from multiple elements
-- case: allOff: { $ref: .. + properties: .. }
--
---@return table? props
---@return string? errmsg
---@return string? parent_key is a ref to the parent api_component
function M.assembly_obj_parts_from_allOf(state, allOf, key)
  log_debug("assembly_obj_parts_from_allOf", key)
  local props, parent_key = {}, nil

  for n, elm in ipairs(allOf) do
    local ref = elm['$ref']
    if ref then
      log_trace('%s process $ref: %s', n, ref)
      local raw_node0, err0 = M.resolve_ref(state, ref, 'allOff')
      if not raw_node0 then
        return nil, err0 or 'err on recolve_ref allOff'
      end
      local node, err, pk, cf = M.assembly_object_parts(state, raw_node0, ref)
      if not node then
        return nil, err or ('error on build_object_properties for ' .. v2s(ref))
      end
      if cf ~= nil then
        M.add_common_fields_to_property(state, props, cf, ref, key)
      end
      -- merge with overriding existed keys
      for k, v in pairs(node) do props[k] = v end

      -- to track the compoment schema name(keypath|ref) superclass
      if n == 1 then
        parent_key = pk or ref
      end
      --
    elseif elm['properties'] ~= nil then
      local props0 = elm['properties']
      log_trace('%s process properties', n, props0)
      -- merge with overriding existed keys
      for k, v in pairs(props0) do
        log_trace("props0.key:%s = v:%s  props[k]:%s", k, type(v), type(props[k]))
        if type(v) == 'table' then
          if type(props[k]) == 'table' then
            v = M.merge_with_flat_copy(props[k], v) -- child node inherit parent
          else
            v = utbl.deep_copy(v)
          end
        end
        props[k] = v
      end
    else
      log_trace('%s elm with unknown fields', n, M.get_tbl_keys_s(elm))
    end
  end
  return props, nil, parent_key
end

--
-- aka inline a set of the commons fields as an sepeareted object of the pojo class
--
---@param state table
---@param props table
---@param cf table
---@param ref string of the generated interface clazz as an part of assembled class
---@param key string (api-compoment-schema name) of the assembled class
function M.add_common_fields_to_property(state, props, cf, ref, key)
  log_debug("add_common_fields_to_property from %s to %s", ref, key)
  local clazz = M.get_already_created_clazz(state, nil, ref)
  log_debug("")
  if clazz and clazz.typ == INTERFACE then
    local syntetic_field_name = to_varname(assert(clazz.name, 'class.name'))
    props[syntetic_field_name] = {
      -- 'syntetic inlined field as a container for common_fields ' ..
      description = 'agregation of a common fields from ' .. v2s(ref),
      ['$ref'] = ref, -- will be used in build_pojo_class_field to pick ClassName
      syntetic = M.COMMON_FIELDS,
    }
    log_debug('added prop:', inspect(props))
    return
  end

  -- notification for the dev mode
  local msg = '[WARNING] unexpected not empty common_fields for ref:' .. v2s(ref) ..
      '\nin assembling props allOf for ' .. v2s(key) ..
      "\ncommon_fields: " .. M.get_tbl_keys_s(cf, " ")
  error(msg)
end

--
-- agregate all fields from all varianst of the oneOf
-- build interface of abstract class
--
---@param state table
---@param oneOf table (list of nodes|schemas)
---@param key string aka ref to compoment schema
---@return table? props
---@return string? errmsg
---@return table? common_fields (of interface or abstrace class to extends)
function M.assembly_obj_parts_from_oneOf(state, oneOf, key)
  log_debug("assembly_obj_parts_from_oneOf", key)
  assert(type(oneOf) == 'table' and #oneOf > 0, 'oneOf')

  local commons_key = nil         -- of the api scheme with common_fields(abstract in oop)
  local common_fields = {}        -- to findout fields existed in all variants of oneOf
  local used_interfaces_keys = {} -- to ensure that used one interface_class_key
  local generated_classes = {}    -- list of classes to add common interface impl

  -- oneOf and list of classes nested from one parent class
  for i = 1, #oneOf do
    local node = oneOf[i]
    local ref = node['$ref']

    if ref then -- ref is a api compoment scheme name
      local clazz, err = M.generate_pojo_class(state, ref, state.pkg)
      if not clazz then
        error('cannot resolve class for ' .. v2s(key) .. ': ' .. v2s(err))
      end
      if not clazz.parent_key then
        error('no parent key for ' .. v2s(key) .. ' at ' .. v2s(i) .. ' ' .. v2s(ref))
      end
      generated_classes[#generated_classes + 1] = clazz
      used_interfaces_keys[ref or i] = clazz.parent_key

      if i == 1 then -- track common_fields to collect used by all oneOf variants
        commons_key = clazz.parent_key
        M.add_all_names(common_fields, clazz.fields)
      else
        M.remove_all_unshared_names(common_fields, clazz.fields)
      end
    else
      error('TODO: non $ref #' .. v2s(i) .. " node-keys:" .. M.get_tbl_keys_s(node))
    end
  end

  M.validate_oneOf_interface(key, commons_key, used_interfaces_keys);
  ---@cast commons_key string
  local iclazz = M.create_java_interface(state, key, common_fields, nil)
  -- aka base or default impl for created interface
  M.create_def_impl_pojo(state, commons_key, common_fields, key)

  for _, clazz in ipairs(generated_classes) do
    clazz.implements = iclazz.fqcn
    -- clazz.extends = aclazz.fqcn -- ??
    -- todo ensure clazz has all commons_fields (add if not)
  end

  return {}, nil, common_fields
end

--
--
-- validate that all oneOf classes used the same(one) interface
--
---@param key string?
---@param interface_key string?
---@param used_interfaces_keys table
function M.validate_oneOf_interface(key, interface_key, used_interfaces_keys)
  if not interface_key then
    error('inferface not find out for ' .. v2s(key))
  end
  -- validate what used one interface for all variants of oneOf
  local diffs = {}
  for impl_key0, interface_key0 in pairs(used_interfaces_keys) do
    if interface_key0 ~= interface_key then
      diffs = { v2s(impl_key0) .. ' use: ' .. v2s(interface_key0) }
    end
  end

  if #diffs > 0 then
    error('found multiple interfaces for oneOf: ' .. v2s(key) .. "\n" ..
      table.concat(diffs, "\n"))
  end
end

--
-- create "glue" for oneOf to bind abstract class to extended childs
--
-- eshure interface has all shared fields
--
---@param state table
---@param key string? of api component for this generated interface
---@param interface_key string
---@param common_fields table map of the fields exists in all oneOf elements
---@return table clazz(interface)
function M.create_java_interface(state, interface_key, common_fields, key)
  log_debug("create_java_interface %s for impl:%s", interface_key, key)

  local clazz = M.get_already_created_clazz(state, nil, interface_key)
  if clazz ~= nil then
    if clazz.typ ~= INTERFACE then
      error(v2s(clazz.fqcn or interface_key) ..
        ' expected interface clazz got: ' .. v2s(clazz.typ))
    end
    return clazz
  end -- reuse if already created

  local path = M.split_key_to_parts(interface_key)
  local pkg, classname = M.gen_pkg_and_class_name(state, path, state.pkg)

  clazz = M.create_class(state, interface_key, pkg, classname, {}, {
    typ = INTERFACE,
    common_fields = common_fields,
  })

  local fqin = clazz.fqcn or '?' -- full qualified interface name
  assert(state.classes[fqin], 'ensure interface is cached ' .. v2s(fqin))

  M.organize_imports(state, clazz)

  return clazz
end

--
-- create "glue" for oneOf to bind abstract class to extended childs
--
-- eshure interface has all shared fields
--
---@param state table
---@param key string of api component for this generated interface
---@param commons_key string
---@param common_fields table map of the fields exists in all oneOf elements
---@return table clazz(interface)
function M.create_abstract_pojo(state, commons_key, common_fields, key)
  log_debug("create_abstract_pojo %s for :%s", commons_key, key)

  local clazz = M.get_already_created_clazz(state, nil, commons_key)
  if clazz ~= nil then
    if clazz.typ ~= ABSTRACT then
      error(v2s(clazz.fqcn or commons_key) ..
        ' expected abstract clazz got: ' .. v2s(clazz.typ))
    end
    return clazz
  end -- reuse if already created

  local path = M.split_key_to_parts(commons_key)
  local pkg, cn = M.gen_pkg_and_class_name(state, path, state.pkg)

  clazz = M.create_class(state, commons_key, pkg, cn, {}, {
    typ = ABSTRACT,
    common_fields = common_fields,
  })

  local fqin = clazz.fqcn or '?' -- full qualified interface name
  assert(state.classes[fqin], 'ensure abstract class is cached ' .. v2s(fqin))

  M.organize_imports(state, clazz)

  return clazz
end

--
-- create a pojo class with a default implementation of the given interface
-- interface_key - is a api key for java interface what is already generated
-- (has in the state.classes)
-- see sample examples with
--   DnsRecord(interface) and DnsRecordSharedFields(defImpl) used for
--   DnsRecordResponse
--
---@param interface_key string the api key of the interface that this class is
-- implements
function M.create_def_impl_pojo(state, commons_key, common_fields, interface_key)
  log_debug("create_def_impl_pojo %s for :%s", commons_key, interface_key)

  local clazz = M.get_already_created_clazz(state, nil, commons_key)
  if clazz ~= nil then
    return clazz
  end -- reuse if already created

  local iclazz = M.get_already_created_clazz(state, nil, interface_key)
  if not iclazz then
    error('expected interface class is already exists ' .. v2s(interface_key))
  end
  local fqin = iclazz.fqcn
  assert(iclazz.typ == INTERFACE, 'expected interface ' .. v2s(fqin))

  local keypath = M.split_key_to_parts(commons_key)
  local pkg, cn = M.gen_pkg_and_class_name(state, keypath, state.pkg)

  clazz = M.create_class(state, commons_key, pkg, cn, {}, {
    typ = CLASS,
    common_fields = common_fields,
    implements = fqin,
  })
  clazz.description = "default implementation of " .. v2s(fqin)

  M.organize_imports(state, clazz)

  return clazz
end

--
--
-- cache of the classes prepared to the generation
---@param state table
function M.enshure_cache_initialized(state)
  state.classes = state.classes or {}                 -- map fqcn to class
  state.ref2inner_class = state.ref2inner_class or {} -- map
  -- cache ref(api-compoment yml path) to fully qualified class name(java)
  state.ref2fqcn = state.ref2fqcn or {}
end

--
-- search by fqcn or by key(api-compoment-schema )
--
-- check in cache of already created classes
--
-- fqcn==nil and keypath~=nil to find in the cache of already generated classes
-- from OpenAPI schemas
--
---@param state table
---@param fqcn string?
---@param key string?
function M.get_already_created_clazz(state, fqcn, key)
  log_debug("get_already_created_clazz fqcn: %s key: %s", fqcn, key)
  if fqcn == nil and key ~= nil then
    fqcn = state.ref2fqcn[key]
    log_debug('found fqcn: %s for key: %s', fqcn, key)
  end

  local clazz = state.classes[fqcn]
  if clazz ~= nil then -- already created
    log_debug('found in cleated classes: %s (%s)', fqcn, (clazz.typ or 'class'))
    return clazz
  end
  clazz = (state.ref2inner_class or E)[key or false]
  if clazz ~= nil then
    log_debug('found in inner classes by key: %s (%s)', key, (clazz.typ or 'class'))
    return clazz
  end

  log_debug("not found created clazz for fqcn:%s key:%s", fqcn, key)
  return nil
end

--
-- parse structure of the openapi-yml into inner structure (lua-tables) to
-- generate the POJO classes
--
---@param state table{cached_refs, classes}
---@param pkg string
---@param classname string
---@param node table yml OpenAPI definition of the scheme (POJO to gen)
---@param key string? api_component name(key in the yml scheme)
---@param container_fqcn string? to build inner class with class-container
---@return table? clazz struct
---@return string? errmsg
---@param extra table? {desc, title} e.g. to pass description from field to inner class
function M.build_pojo_class(state, pkg, classname, node, key, container_fqcn, extra)
  log_debug("build_pojo_class", pkg, classname, type(node))
  assert(type(node) == 'table', 'node')

  local fqcn = v2s(pkg) .. '.' .. v2s(classname)
  local clazz = M.get_already_created_clazz(state, fqcn, key)
  if clazz ~= nil then return clazz end -- to reuse existed clazz

  local props, err, pk, cf = M.assembly_object_parts(state, node, key or fqcn)
  if not props then
    return nil, add_err(state, err)
  end

  clazz = M.create_class(state, key, pkg, classname, props, {
    typ = CLASS,
    common_fields = cf,
    parent_key = pk,
    container_fqcn = container_fqcn,
    extra = extra,
    required = node['required'],
  })

  log_debug('build_pojo_class ret: %s is_inner:%s (%s) ',
    clazz.fqcn, clazz.parent ~= nil, clazz.typ or 'class')

  return clazz, nil
end

--
-- create clazz for a given key (api-compoment-schema) with a given
-- package and classname.
--
-- with caching. if in already created classes has the clazz with same fqcn then
-- it will be returned (this is a solution for a complex interface creation)
--
-- if this class is a type for a field in another class and you want to pass
-- description from a field to this new class (no in api-compoment-schema)
-- then pass a table in opts.extra with description and table keys)
--
---@param key string? full key of api-compoment-schema
---@return table
---@param pkg string
---@param classname string
---@param props table
---@param opts table {
--   ctyp,
--   common_fields:table,  - for abstract|interface inheritance (map)
--   container_fqcn:string?, - not nil if this is is an inner class
--   parent_key:string?, - is an api-key of compoment like superclass(experimental
---  extra:table?{desc, title, enum} -- to pickup comments from the field(4 iclasses)
--                                      for which this class is generated
-- }
function M.create_class(state, key, pkg, classname, props, opts)
  log_debug("create_class k:%s pkg:%s cn:%s opts:%s", key, pkg, classname, opts)
  local fqcn = v2s(pkg) .. '.' .. v2s(classname)
  opts = opts or E
  local extra = opts.extra or E -- {desc|description, title, enum} (from field or yml-node)

  local existed_clazz = (state.classes or E)[fqcn]
  if existed_clazz ~= nil then
    if existed_clazz.typ == INTERFACE or existed_clazz.typ == ABSTRACT then
      assert(existed_clazz.name == classname, 'classname')
      assert(existed_clazz.pkg == pkg, 'classname')
      log_debug('reuse already existed clazz: ', fqcn, existed_clazz.typ)
      return existed_clazz
    end
    error('attempt to override already existed clazz with new clazz:'
      .. v2s(fqcn) .. ' ' .. v2s(key) .. ' (' .. v2s((opts.ctyp or 'class')) .. ')')
  end

  local clazz = {
    typ = opts.typ,               -- CLASS(nil) INTERFACE ABSTRACT
    api_component = key,          -- like #/component/schemas/zones_zone
    parent_key = opts.parent_key, -- ref to parent api-compoment schema (superclass)
    extends = opts.extends,       -- superclass name
    implements = opts.implements, -- interface name(fqcn) of table of the names
    parent = opts.container_fqcn, -- class container if this class is inner
    pkg = assert(pkg, 'package'),
    name = assert(classname, 'classname'),
    fqcn = fqcn,
    description = props['description'] or extra.description or opts.desc,
    title = props['title'] or extra.title, -- for comment
    enum = props['enum'] or extra.enum,    -- for comment
    imports = nil,                         -- set(map) not list!
    fields = {},
    -- methods = nil, -- for getters and setters
    required = opts.required -- required fields
  }
  -- I have to immediately caching this newly created workpiece of clazz,
  -- but not yet completely filled with data, in order to immediately add
  -- inner classes to this class
  M.add_class_to_cache(state, clazz, key)

  --[[
  if opts.parent_key then
    -- (not finished experemental stuff to support inheritance for oneOf case)
    -- clazz.extends = state.ref2fqcn[pk] -- ??
    -- At the moment, the building full schema from the parts of sub-schemes
    -- through "allOf: $ref + props" is done through the aggregation of all
    -- fields of all without inheritance - to support Java16+ records
    -- but "oneOf: $ref, $ref, ..." is considered as inheritance from one common
    -- superclass. (and command subclass generated in the build_object_properties)
    --
    -- if will need to work without records it can be used with oop-inheritance
    -- the main idea is generate common abstract class that agregate all common
    -- fields what used in all implementations of the oneOf what considered as
    -- interface(e.q. DnsRecord in samples example with java-records)
    --
    -- then ARecord -> DnsRecordSharedFields(can be base or abstrace class and
    -- no needs DnsRecord as interface (this is only a workaround for the records)
    if not clazz.extends then
      local errmsg = 'no fqcn for parent key: ' .. v2s(opts.parent_key) ..
          ' for class ' .. clazz.fqcn .. " known api-keys(refs):\n" ..
          table.concat(M.get_tbl_keys(state.ref2fqcn), '\n')
      log_debug('[WARNING] %s', errmsg)
    end
  end
  --]]

  local fields_map = {}

  if opts.common_fields ~= nil then
    for fname, field in pairs(opts.common_fields) do
      fields_map[fname] = field
    end
  end

  for fname, prop in pairs(props or E) do
    fields_map[fname] = M.build_pojo_class_field(state, clazz, fname, prop)
  end

  local fieldnames = M.get_sorted_field_names(state, clazz.required, fields_map)

  for _, fieldname in ipairs(fieldnames) do
    local field = fields_map[fieldname]
    clazz.fields[#clazz.fields + 1] = field
  end

  return clazz
end

-- Priority names of fields for sorting
local MAP_PRIORITY_FIELD_NAMES = {
  id = true,
  name = true,
  ['type'] = true,
}
local LIST_PRIORITY_FIELD_NAMES = {
  'id', 'type', 'name',
}

--
-- keep id always as a first field name
--
---@param state table
---@param fields_map table{field_name=field_obj}
function M.get_sorted_field_names(state, order, fields_map)
  state = state -- to
  local fieldnames = {}
  local priority_map = MAP_PRIORITY_FIELD_NAMES
  local priority_list = LIST_PRIORITY_FIELD_NAMES
  -- The names of the fields that will be added after priority with a given order
  local remain_keys = {}
  local syntetic = {}

  if type(order) == 'table' then
    priority_list = order
    priority_map = {}
    for _, fname in ipairs(priority_list) do -- create priority_map
      priority_map[fname] = true
    end
  end

  for fieldname, field in pairs(fields_map) do
    if type(field) == 'table' and field.syntetic then
      syntetic[#syntetic + 1] = fieldname
    elseif not priority_map[fieldname] then
      remain_keys[#remain_keys + 1] = fieldname
    end
  end

  -- building sorted fieldname list
  --
  -- agregation of the one-level json fields into separated java pojo class
  table.sort(syntetic)
  for _, key in ipairs(syntetic) do
    fieldnames[#fieldnames + 1] = key
  end

  for _, key in ipairs(priority_list) do
    if fields_map[key] ~= nil then
      fieldnames[#fieldnames + 1] = key
    end
  end

  table.sort(remain_keys)

  for _, key in ipairs(remain_keys) do
    fieldnames[#fieldnames + 1] = key
  end

  return fieldnames
end

--
---@param state table
---@param clazz table{fqcn, api_component, parent}
---@param key string? api-compoment schema name (like #/components/schema/name...
---@return table clazz
function M.add_class_to_cache(state, clazz, key)
  local c = clazz or E
  local inner = c.parent ~= nil -- parent is a container_fqcn
  log_debug("add_class_to_cache fqcn:%s key:%s inner:%s (%s) fields:%s",
    c.fqcn, key, inner, (c.typ or 'class'), #(c.fields or E))

  local ref = key or clazz.api_component or ('IGNORE_' .. v2s(clazz.fqcn))
  state.classes = state.classes or {}
  state.ref2fqcn = state.ref2fqcn or {}

  if not inner then -- class container with possible inner classes
    if state.classes[clazz.fqcn] ~= nil then
      -- for interfaces and abstract classes in complex nested logic of generation
      log_debug('use alredy existed clazz for ' .. v2s(clazz.fqcn))
      return state.classes[clazz.fqcn]
      -- error('attempt to override already exited class: ' .. v2s(clazz.fqcn))
    end
    state.classes[clazz.fqcn] = clazz
    --
  elseif inner then
    -- like state.classes but only for inner classes which holds inside another
    -- classes (records in records to reduce amount of a small files count)
    state.ref2inner_class = state.ref2inner_class or {}
    if state.ref2inner_class[ref] ~= nil then
      log_debug('use alredy existed inner clazz:%s for %s', c.fqcn, ref)
      local existed_clazz = state.ref2inner_class[ref]
      assert(existed_clazz.fqcn == clazz.fqcn, 'sync inner fqcn')
      clazz = existed_clazz
    else
      log_trace('add ref:%s as inner_clazz:%s', ref, c.fqcn)
      state.ref2inner_class[ref] = clazz
    end

    if state.classes[clazz.parent or false] ~= nil then
      log_debug('add inner class:%s to clazz-container:%s', clazz.fqcn, clazz.parent)
      local clazz_container = state.classes[clazz.parent]

      clazz_container.classes = clazz_container.classes or {}
      clazz_container.classes[#clazz_container.classes + 1] = clazz
    else
      log_trace('cannot add inner class to class-container - container is not cached!')
    end
  end

  state.ref2fqcn[ref] = clazz.fqcn
  -- assert(clazz.api_component == keypath, 'sync keypath and api_component')

  return clazz
end

--
--
local function is_with_inner_classes(state)
  local opts = ((state or E).opts) or E
  local records = opts.as_records == true
  local with_inner = opts.with_inner_classes == true
  log_trace("is_with_inner_classes records:%s with-inner:%s", records, with_inner)
  return records or with_inner
end

--
-- to add meta information into field for comment in the generated source code
--
---@param field table
---@param node table{title,description, default,...}
---@return table
function M.add_meta_info(field, node)
  field.type = node['type'] or field['type']
  -- for comment under the field or method or class
  field.title = node['title'] or field['title']
  field.description = node['description'] or field['description']
  -- enum for comment(available field values)
  field.enum = node['enum'] or field['enum']
  field.nullable = node['nullable'] or field['nullable']
  field.readOnly = node['readOnly'] or field['readOnly'] --if true no settter
  field.format = node['format'] or field['format']
  field.pattern = node['pattern'] or field['pattern']
  field.maxLength = node['maxLength'] or field['maxLength']
  field.min = node['min'] or field['min']
  field.max = node['max'] or field['max']
  field.syntetic = node.syntetic or field.syntetic

  if field['example'] == nil then
    field['example'] = node['example']
  end
  if field['default'] == nil then -- can be false
    field.default = node['default']
  end

  return field
end

--
--
--
---@param state table
---@param fieldname string  (prop name from properties(map))
---@param node table node of the field (prop_body from properties(map)
---@return table? field{name, type, desc, title, ...}
---@return string? errmsg
function M.build_pojo_class_field(state, clazz, fieldname, node)
  log_debug("build_pojo_class_field: %s class:%s(%s)",
    fieldname, (clazz or E).name, (clazz or E).api_component)

  assert(type(state.classes) == 'table', 'state')
  assert(type(clazz) == 'table', 'clazz')
  if type(node) ~= 'table' then
    local msg = 'expected table field got: ' .. type(node) ..
        ' for: ' .. v2s(fieldname) .. ' ' .. v2s((clazz or E).fqcn)
    log_debug(msg)
    return nil, msg
  end

  local field = {
    amod = AccessModifiers.PRIVATE, -- for CodeGen
    name = fieldname,
    type = nil,                     -- not yet known, but will be evaluated here
  }
  M.add_meta_info(field, node)

  local ref = node['$ref']

  if ref then
    local node0, _ = M.resolve_ref(state, ref, fieldname)
    if node0 then
      M.add_meta_info(field, node0) -- inherite metainfo from ref
      log_trace('use resolved ref: %s node0: %s', ref, node0)
      node = node0
    end
  end

  local field_type = field.type or node['type']

  -- required is a list of required fieldnames
  if field_type == nil and (node.properties ~= nil or node.required ~= nil) then
    field.type = 'object'
    field_type = 'object'
  end

  if field_type == 'object' then
    field_type = M.resolve_object_field_type(state, clazz, field, node, ref)
  elseif field_type == 'array' then
    field_type = M.resolve_array_field_type(state, clazz, field, node, ref)
  else
    field_type = M.resolve_primitive_field_type(state, clazz, field, node, ref)
  end

  field['type'] = field_type

  return field
end

--

local mapping_open_api_types_to_java = {
  ['array'] = false,
  ['string'] = 'String',
  ['boolean'] = 'boolean',
  ['byte'] = 'byte',
  ['short'] = 'short',
  ['integer'] = 'int',
  ['number'] = 'int', -- or double?
  -- todo another types
}

local function error_in_field(msg, fqcn, fieldname)
  error(v2s(msg) .. ' field: ' .. v2s(fieldname) .. ' in class:' .. v2s(fqcn))
end


--
-- reuse already existed clazz or create a new one for component key
--
-- generate class(or reuse existed) and pick it ClassName as a Type
--
---@param state table
---@param clazz table
---@param field table{name, type, desc, title} to be used in new class docblock
---@param node table
---@param ref string?
---@return string field_type
function M.resolve_object_field_type(state, clazz, field, node, ref)
  local field_type, field_name = (field or E).type, (field or E).name
  log_trace("resolve_object_field_type:%s fieldname:%s", field_type, field_name)
  assert(field_type == 'object')
  local type_clazz, err = nil, nil

  if ref then
    assert(node.type == 'object', 'the ref must refer to the type:object')
    type_clazz, err = M.generate_pojo_class(state, ref, state.pkg)
    if not type_clazz then
      error(err)
    end
    add_import(clazz, type_clazz)
    M.add_meta_info(field, clazz)
    return type_clazz.name -- for field type
  end

  -- object defined inside api-schema without $ref - pick classname from fieldname
  assert(ref == nil, 'anonim object definition without $ref')

  local inner = is_with_inner_classes(state)
  local pkg, classname, fqcn, container_fqcn = clazz.pkg, nil, nil, nil

  -- create pkg & classname
  if inner then
    classname = ubase.toPascalCase(field_name)
    fqcn = pkg .. '.' .. clazz.name .. '.' .. classname
    container_fqcn = clazz.fqcn -- for inner classes|records
    -- for inner actual scn after compile will be Parent$Child
  else
    classname = clazz.name .. ubase.toPascalCase(field_name)
    fqcn = pkg .. '.' .. classname
    container_fqcn = nil
  end
  log_trace("inner:%s box:%s ", inner, container_fqcn, pkg, classname, fqcn)

  field_type = classname

  type_clazz = M.get_already_created_clazz(state, fqcn, nil)
  if type_clazz then
    M.add_meta_info(field, type_clazz)
    return field_type
  end

  -- parent_key / properties|allOf|oneOf / field_key
  local key = v2s(clazz.api_component) .. '/properties/' .. v2s(field_name)

  type_clazz, err = M.build_pojo_class(state, pkg, classname, node, key, container_fqcn, field)
  if not type_clazz then
    if field.description ~= nil then -- like Extra X-specific information about Y.
      field_type = 'Object'          -- anonim objects without fields
    else
      error(v2s(err))                -- todo fix for no properties Meta Extra
    end
  end

  if inner and type_clazz then
    M.validate_inner_class_caching(state, clazz, type_clazz)
  end

  return field_type
end

--
--
--
---@param o table state
---@param clazz_container table
---@param inner_clazz table
function M.validate_inner_class_caching(o, clazz_container, inner_clazz)
  local c_fqcn = v2s((clazz_container or E).fqcn)
  local i_fqcn = v2s((inner_clazz or E).fqcn)
  log_debug("validate_inner_class_caching", i_fqcn, c_fqcn)

  if type(inner_clazz) ~= 'table' then
    error('no inner clazz for container-class:' .. c_fqcn)
    return
  end
  if type(clazz_container) ~= 'table' then
    error('no clazz-container for inner-class:' .. i_fqcn)
    return
  end
  local key = assert(inner_clazz.api_component, 'inner class api key')
  if o.ref2inner_class[key] == nil then
    error('no inner clazz in (map)ref2inner_class: ' .. key .. ' ' .. i_fqcn)
  end
  if #((clazz_container or E).classes or E) == 0 then
    error('clazz-container has no classes!')
  end
  local idx, found_iclazz = -1, nil
  for i, iclazz in ipairs(clazz_container.classes) do
    if iclazz.fqcn == inner_clazz.fqcn then
      idx, found_iclazz = i, iclazz
      break
    end
  end
  if not idx or idx < 1 then
    error('inner clazz ' .. i_fqcn .. ' not found in class-container: ' .. c_fqcn)
  end
  if found_iclazz ~= inner_clazz then
    error('found another inner clazz with same fqcn:' .. i_fqcn ..
      ' in class-container: ' .. c_fqcn .. ' (diff instance)')
  end
end

--
-- todo implements all cases
--
-- string   ->   String
-- integer  ->   int       integer + nullable  -->  Integer
--
---@param state table
---@param clazz table
---@param field table{name, type, desc, title} to be used in new class docblock
---@param node table
---@param ref string?
---@return string field_type
function M.resolve_primitive_field_type(state, clazz, field, node, ref)
  local ftype, fname = (field or E).type, (field or E).name
  log_trace("resolve_primitive_field_type:%s fieldname:%s %s", ftype, fname, ref)
  state = state
  -- ?? to fix issue with dns-records lat_direction (no type is given)
  if ftype == nil and field.enum ~= nil then
    ftype = 'string'
  end

  local javatype = mapping_open_api_types_to_java[ftype or false]

  if javatype then
    ftype = javatype
    if node.nullable or node.default == 'null' then -- like int --> Integer
      ftype = ftype:sub(1, 1):upper() .. ftype:sub(2)
    end
  else
    error(
      "in build field:" .. (fname) .. " for clazz: " .. (clazz or E).fqcn ..
      " no primitive javatype: " .. v2s(javatype) .. ' field_type:' .. v2s(ftype) ..
      "\n field: " .. inspect(field) ..
      "\n node: " .. inspect(node) ..
      "\nref: " .. v2s(ref))
  end
  log_trace('resolve_primitive_field_type ret:%s for %s', ftype, fname)
  return ftype
end

---@param state table
---@param clazz table
---@param field table{name, type, desc, title} to be used in new class docblock
---@param node table
---@param ref string?
---@return string field_type
function M.resolve_array_field_type(state, clazz, field, node, ref)
  local field_type, fieldname = (field or E).type, (field or E).name
  local fqcn = (clazz or E).fqcn
  log_trace("resolve_array_field_type:%s fieldname:%s %s", field_type, fieldname, fqcn)
  assert(field_type == 'array')

  -- items:
  --     type: string
  -- type: array
  local items = node.items
  if not items and node['$ref'] then
    node = assert(M.resolve_ref(state, node['$ref'], fieldname))
    log_trace("node by ref", inspect(node))
    items = node.items
  end

  if not items then
    local details = inspect(node)
    error_in_field('no items details' .. details, fqcn, fieldname)
  elseif items.type == nil then
    -- case instead of primitive defined ref to object?
    if items['$ref'] ~= nil then
      local elm_type, ref0 = nil, items['$ref']
      local clazz0, err = M.generate_pojo_class(state, ref0, state.pkg)
      if not clazz0 then
        print('[DEBUG]', err) -- + warning
        elm_type = 'Object'
      else
        -- add to import?
        elm_type = clazz0.name
      end
      add_import(clazz, LIST_IMPORT)
      field_type = 'List<' .. elm_type .. '>'
    else
      error_in_field('not found a type of the array', fqcn, fieldname)
    end
  elseif items.type == 'object' then -- array of objects
    field_type = M.gen_type_for_anon_item_object(state, fieldname, node, clazz, ref)
  else
    local javatype = mapping_open_api_types_to_java[items.type]
    if not javatype then
      error_in_field('unknown type' .. v2s(items.type), fqcn, fieldname)
    end

    add_import(clazz, LIST_IMPORT)

    field_type = 'List<' .. javatype .. '>'
  end

  log_trace('resolve_array_field_type ret:%s for %s', field_type, fieldname)
  return field_type
end

--
-- type:array -> {items: property:... type:object}
--
-- step 1: generate ClassName for an anonim object defined in properties
-- step 2: if this Class is not already generated - generate a new one
--
---@param state table
---@param fieldname string
---@param node table{items:{properties}}
---@param clazz table{fqcn, classes}
---@param ref string?
---@return string
function M.gen_type_for_anon_item_object(state, fieldname, node, clazz, ref)
  local pa_fqcn = (clazz or E).fqcn
  log_debug("gen_type_for_anon_object", fieldname, pa_fqcn)
  local items = assert(node.items, 'items')
  -- uniqueItems (Set)
  if not items.properties and not items['$ref'] and not items.allOf then
    error_in_field('object of items without properties|$ref|allOf', pa_fqcn, fieldname)
  end

  local path = nil
  if ref == nil or #ref < 3 then
    log_debug('pick class name templ from the fieldname:%s ref:%s node:%s', fieldname, ref, items)
    path = { fieldname }
  elseif ref ~= nil then
    path = M.split_key_to_parts(ref)
  end

  local pkg, cn = M.gen_pkg_and_class_name(state, path, state.pkg)
  if cn:sub(-1, -1) == 's' then
    cn = cn:sub(1, -2)
  end
  local keypath = nil
  -- example?
  if #path == 1 then -- no ref
    pkg = clazz.pkg
  end

  M.build_pojo_class(state, pkg, cn, items, keypath, nil, node)
  add_import(clazz, LIST_IMPORT)

  return 'List<' .. cn .. '>' -- or Object on fail
end

--
--
--
---@param state table{yml, keypath, cache}
---@param ref string
---@param keyname string -- only for err report
---@return table?
---@return string? errmsg
function M.resolve_ref(state, ref, keyname)
  log_debug("resolve_ref:%s keyname:%s", ref, keyname)
  state.cached_refs = state.cached_refs or {}
  local cache = state.cached_refs

  local err, node = nil, cache[ref]
  if not node then
    cache[ref], err, _ = uyaml.open_node(state.yml, ref)
    if err then
      local errmsg = 'cannot resolve ref: ' .. v2s(ref) ..
          ' for ' .. keyname .. ' in ' .. v2s(state.keypath) ..
          "\nopen-node-details: " .. v2s(err)
      return nil, add_err(state, errmsg)
    end
    node = cache[ref]
  end
  log_trace('resolve_ref ret: %s', node)
  return node
end

--
--  generate package and short Class name for given key-path (POJO to gen)
--
--
---@param state table parsed to keys(words) key_path
---@return string pkg
---@return string cn
function M.gen_pkg_and_class_name(state, path, pkg)
  log_debug("gen_pkg_and_class_name st:%s path:%s pkg:%s", type(state), path, pkg)
  local scheme_name = assert(path[#path], 'scheme_name from (table)keypath')
  -- local subpkg, classname = '', nil

  -- zones_zone --> pkg.zones.Zone
  local words, p, prev_sep, sepn = {}, 1, nil, 0
  local pkg_edge = 0
  local function add(s)
    log.trace('add word: ', s)
    words[#words + 1] = s
  end

  while true do
    local i = string.find(scheme_name, '[_%-]', p, false)
    if not i then
      add(scheme_name:sub(p))
      break
    end
    local sep = scheme_name:sub(i, i)
    sepn = sepn + 1
    if pkg_edge == 0 and prev_sep ~= nil and prev_sep ~= sep then
      pkg_edge = #words + 1
      -- case zones_api-response-common --> zones.APIResponseCommon
      local next_sep = string.match(scheme_name, '([_%-])', i + 1)
      log_trace('i:%s sep:%s next_sep:%s sepn:%s', i, sep, next_sep, sepn)
      if not (prev_sep == '-' and sep ~= prev_sep) then
        pkg_edge = pkg_edge - 1
      end
      -- if next_sep == sep or (not next_sep and sepn < 2) then
      --   pkg_edge = pkg_edge - 1
      -- end
    end
    add(scheme_name:sub(p, i - 1))
    prev_sep = sep
    p = i + 1
  end

  local cnt = #words
  local pkg_end = pkg_edge
  local class_start = pkg_edge + 1

  if cnt == 2 and pkg_edge == 0 then
    pkg_end = 1
    class_start = 2
  end
  local words_in_classname = cnt - class_start
  --
  -- if cnt == 1 then
  --   pkg_end = 0
  --   class_start = 1
  -- elseif class_start > 1 and cnt - class_start == 0 then
  --   -- aaa_alert-types one edge word use as sub pkg and as ClassName
  --   if not M.is_plural(words[pkg_end], words[class_start]) then -- zones zone
  --     log_trace("use word from package in classname")
  --     class_start = class_start - 1
  --   end
  -- elseif pkg_edge == 0 and pkg_end == 0 then
  --   pkg_end = 1
  --   class_start = 2
  -- end
  log_trace('%s words:%s edge:%s pkg_e:%s cls_s:%s words_in_classname: %s',
    scheme_name, cnt, pkg_edge, pkg_end, class_start, words_in_classname)

  for i = 1, pkg_end, 1 do
    pkg = pkg .. '.' .. words[i]
  end

  local cn = ''

  for i = class_start, #words, 1 do
    local word = words[i]
    if word then -- to PascalCase
      cn = cn .. (word:sub(1, 1):upper()) .. word:sub(2)
    end
  end

  log_trace('generated pkg:%s cn:%s', pkg, cn)

  return pkg, cn
end

-------------------
------ tooling
--

---@param common table
---@param clazz_fields table
function M.add_all_names(common, clazz_fields)
  for _, t in ipairs(clazz_fields or E) do
    assert(type(t) == 'table' and t.name ~= nil, 'tbl with name')
    common[t.name] = utbl.deep_copy(t)
  end
end

---@param list table{elm1.name, elm2.name}
function M.index_of_tbl_with_name(list, name)
  for i, t in ipairs(list or E) do
    assert(type(t) == 'table' and t.name ~= nil, 'tbl with name')
    if t.name == name then
      return i
    end
  end
  return -1
end

--
-- keep only those fields that are in both comon and in clazz_fields and
-- remove all the rest(uncommon fields)
--
---@param common table
---@param clazz_fields table
function M.remove_all_unshared_names(common, clazz_fields)
  local to_remove = {}
  for name, commonField in pairs(common or E) do
    local i = M.index_of_tbl_with_name(clazz_fields, name)
    if i < 1 then -- not shared
      to_remove[#to_remove + 1] = name
    else
      local field = clazz_fields[i]
      if field.enum then
        commonField.enum = M.merge_enum_elms(commonField.enum or {}, field.enum)
      end
    end
  end

  for _, name in ipairs(to_remove) do
    common[name] = nil -- mark as uncommon
  end
end

local function is_clazz_implements(clazz, fqin)
  local impl = clazz.implements
  local ityp = type(impl)
  if ityp == 'string' then
    if impl == fqin then
      return true
    end
  elseif ityp == 'table' then
    error('Not implemented yet for multiple implements')
  end
  return false
end

---@param state table
---@param fqin string fully qualified interface name
function M.get_implementations_of(state, fqin)
  local t = {}
  for fqcn, clazz in pairs(state.classes or E) do
    if is_clazz_implements(clazz, fqin) then
      t[#t + 1] = fqcn
    end
    if #(clazz.classes or E) > 0 then
      for _, inner_clazz in ipairs(clazz.classes) do
        if is_clazz_implements(inner_clazz, fqin) then
          t[#t + 1] = inner_clazz.fqcn
        end
      end
    end
  end

  table.sort(t)

  return t
end

-------------------
----- helpers

-- zones, zone -> true
function M.is_plural(plural, singular)
  if plural and singular then
    if #plural - 1 == #singular and plural:sub(-1, -1) == 's' and
        plural:sub(1, -2) == singular then
      return true
    end
  end
  return false
end

-- helper
-- child overrided a parent props but inherit it
function M.merge_with_flat_copy(parent_node, child_node)
  local t = {}
  if parent_node then for k, v in pairs(parent_node) do t[k] = v end end
  if child_node then for k, v in pairs(child_node) do t[k] = v end end
  return t
end

function M.flat_copy(parent_node, child_node)
  local t = {}
  if parent_node then for k, v in pairs(parent_node) do t[k] = v end end
  if child_node then for k, v in pairs(child_node) do t[k] = v end end
  return t
end

-- debugging tool
---@param t table?
function M.get_tbl_keys(t)
  local keys = {}; for k, _ in pairs(t or E) do keys[#keys + 1] = k end;
  return keys
end

---@param t table?
function M.get_tbl_keys_s(t, sep)
  if type(t) ~= 'table' then return tostring(t) end
  local keys = {}; for k, _ in pairs(t or E) do keys[#keys + 1] = k end;
  return table.concat(keys, sep or "\n")
end

-- altered dst!
---@return table (dst)
---@param dst table
---@param src table
function M.merge_enum_elms(dst, src)
  local t = {}
  for _, elm in ipairs(dst) do
    t[#t + 1] = elm
  end

  for _, src_elm in ipairs(src) do
    local has = false
    for _, elm in ipairs(t) do
      if elm == src_elm then
        has = true
        break
      end
    end
    if not has then
      t[#t + 1] = src_elm
    end
  end

  return t
end

return M

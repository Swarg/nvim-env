-- 05-03-2025 @author Swarg
--
-- java class generator from assembled api-componebt scheme
--

local log = require 'alogger'
local class = require 'oop.class'
-- local uyaml = require 'env.util.yaml'
-- local ubase = require 'env.lang.utils.base'
local ClassInfo = require("env.lang.ClassInfo")
-- local AccessModifiers = require 'env.lang.oop.AccessModifiers'
--
local Field = require 'env.lang.oop.Field'
local Docblock = require 'env.lang.oop.Docblock'
local FieldGen = require 'env.lang.oop.FieldGen'

local upojo = require 'env.langs.java.util.openapi.pojo'


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug, log_trace = log.debug, log.trace
local getVarNameForClassName = ClassInfo.getVarNameForClassName
local getShortClassNameOf = ClassInfo.getShortClassNameOf

local INTERFACE = 'interface'
local ABSTRACT = 'abstract'



class.package 'env.langs.java.util.openapi'
---@class env.langs.java.util.openapi.CodeGen : oop.Object
---@field new fun(self, o:table?, lang:env.langs.java.JLang, state:table): env.langs.java.util.openapi.CodeGen
---@field lang env.langs.java.JLang
---@field opts table{as_records}
---@field state table
---@field clazz table
---@field doc env.lang.oop.Docblock
---@field tab string '    '
---@field output table (lines of the generated source code)
local C = class.new_class(nil, 'CodeGen', {
})

--
-- constructor
--
function C:_init(lang, state, tab)
  self.lang = lang
  self.state = state
  self.opts = (state or E).opts
  self.tab = tab or '    '
end

--
-- entry point to class generation from yml
-- keys - is a specified api-component-schemas to be generated as POJO classes
--
---@return table?
---@return string? errmsg
function C.generate_classes(yml, keys, pkg, lang, opts)
  log_debug("generate_classes keys:[%s] opts:%s", keys, opts)
  assert(type(keys) == 'table', 'keys of schemas to generate')

  -- stage 1: generate structure of the classes(POJOs) to save as java code
  local state, err = upojo.generate_classes(yml, keys, pkg, opts)
  if not state then
    return nil, err
  end

  -- state 2: save generated structure to files in current opened project
  local code_gen = C:new(nil, lang, state)

  for _, clazz in pairs(state.classes) do
    code_gen:save_pojo_class_as_code(clazz)
  end

  return state, nil
end

--

function C:initDocblock(tab)
  self.doc = Docblock:new({ tab = tab or '' })
  return self.doc
end

--
-- validate state, yml, lang, pkg
--
---@return string? err
function C:validate_state()
  local state = (self or E).state
  if type(state) ~= 'table' then return 'no state container' end
  if type(state.yml) ~= 'table' then return 'no yml' end
  -- validate
  if self.lang == nil then return 'no JLang' end
  local lang = self.lang
  if not lang.getProjectRoot or not lang:getProjectRoot() then
    return 'no project root'
  end
  -- if not pkg then
  --   return 'no root java package for generated class files'
  -- end
  return nil
end

--
--
-- clazz.api_component -(key) string the full name of geneated compoment in yml file
--
---@param clazz table
function C:save_pojo_class_as_code(clazz)
  assert(type(clazz) == 'table', 'clazz to generate java code')
  local state = self.state
  log_debug("save_pojo_class_as_code", clazz.api_component, clazz.fqcn, state.opts)

  local gen = self.lang:getLangGen() ---@cast gen env.langs.java.JGen
  local ci = gen:getLang():lookupByClassName(clazz.fqcn, ClassInfo.CT.SOURCE)
  log_trace("ci: ", ci)
  if not ci then
    return false, 'cannot resolve classinfo for ' .. v2s(clazz.fqcn)
  end

  if not gen:canCreateNewClass(ci, nil, (state.opts or E).force_rewrite) then
    local errmsg = 'cannot create new class for ' .. v2s(clazz.api_component)
    log_debug(errmsg)
    self:add_err(errmsg)
    return false, errmsg
  end
  local sourcecode, params, path, err

  if state.with_inner_classes then
    error('generation java code with inner classes is not implemented yet')
    --
  elseif state.opts.as_records then -- java16+
    -- all inner objects represents as an inner record in file of parent class
    sourcecode, err = self:gen_source_code_with_java_records(clazz)

    if sourcecode then
      local content = table.concat(sourcecode, "\n")
      log_debug('creatre file...', (clazz or E).fqcn)
      if gen:createFile(true, ci:getPath(), content, state.opts) then
        ci:setPathExists(true)
        path = ci:getPath()
      end
    end
  else
    -- each class in a separate java file
    params = C.convert_for_component_gen(gen, clazz, ci)
    local opts = C.merge_with_flat_copy(state.opts, params)
    path, err = gen:createClassFile(ci, opts)
  end

  if not path then
    self:add_err(err)
    error(err) -- debuging
  else
    state.report[# state.report + 1] = { clazz.fqcn, path }
  end
  return true
end

--
--
--
---@param clazz table{classes}
---@return table? source code
---@return string? errmsg
function C:gen_source_code_with_java_records(clazz)
  log_debug("gen_source_code_with_java_records", (clazz or E).fqcn)
  self.output = {} -- lines of the generated source code

  self:add(0, 'package ' .. v2s(clazz.pkg) .. ';')
  self:add(0, '')

  self:add_imports(clazz)
  self:initDocblock()

  self:gen_source_code(0, clazz)

  self:add(0, '')

  local lines = self.output
  -- cleanup
  self.output = nil
  self.doc.lines = nil

  return lines, nil
end

---@param fc table field or class
function C:addDocBlockComment(depth, fc)
  if FieldGen.hasDocBlockElements(fc) or fc.api_component then
    self.doc:newMultiline(self.output, self:mk_ind(depth))
    FieldGen.buildDocBlockComment0(self.doc, fc)

    if fc.api_component then
      self.doc:add(fc.api_component)
    end
    if fc.parent_key then
      self.doc:add('parent scheme: ' .. v2s(fc.parent_key)) -- todo inheritance
    end
    if fc.required then
      local required = ''
      if type(fc.required) == 'table' then
        for _, name in ipairs(fc.required) do
          required = required .. ' ' .. v2s(name)
        end
      else
        required = tostring(fc.required)
      end
      self.doc:add('required:' .. required)
    end

    if depth > 0 then -- compress docblocks to oneliner only for class members
      self.doc:zip()
    end
  end
end

---@param n number
function C:mk_ind(n)
  local tab = self.tab
  local ind = ''; for _ = 1, n, 1 do ind = ind .. tab end; return ind
end

---@param ind number
function C:add(ind, s)
  self.output = self.output or {}
  self.output[#self.output + 1] = self:mk_ind(ind) .. s
end

function C:add_err(err)
  assert(type((self or E).state) == 'table', 'state')
  if self.state and err then
    log_debug('add_err:', err)
    self.state.errors = self.state.errors or {}
    self.state.errors[#self.state.errors + 1] = err
  end
  return err
end

function C:add_imports(clazz)
  if clazz.imports and next(clazz.imports) ~= nil then
    local pkgs, javapkgs = {}, {}
    for import, _ in pairs(clazz.imports) do
      if import:sub(1, 4) == 'java' then
        javapkgs[#javapkgs + 1] = import
      else
        pkgs[#pkgs + 1] = import
      end
    end
    table.sort(pkgs)
    table.sort(javapkgs)
    for _, import in ipairs(javapkgs) do
      self:add(0, 'import ' .. import .. ';')
    end
    for _, import in ipairs(pkgs) do
      self:add(0, 'import ' .. import .. ';')
    end
    self:add(0, '')
  end
end

--
--
--
---@param depth number
---@param clazz table
function C:gen_source_code_of_record(depth, clazz)
  self:addDocBlockComment(depth, clazz)

  self:add(depth, 'public record ' .. v2s(clazz.name) .. '(')

  -- add fields
  local fields_cnd = #clazz.fields
  local close_def = ') {'
  if clazz.implements then
    local scn = getShortClassNameOf(clazz.implements)
    close_def = ') implements ' .. v2s(scn) .. ' {'
    -- todo if another package add to import
  end
  -- to track the required fields of which are not in the fields of this class
  local required_fields = C.list_to_keys_map(clazz.required)
  -- delegate
  local common_fields_holder = nil


  for n, field in ipairs(clazz.fields) do
    self:addDocBlockComment(depth + 2, field)
    local field_type = self:get_field_type(field, clazz)
    assert(field.name, 'field.name')
    local comma = n == fields_cnd and close_def or ','
    self:add(depth + 2, v2s(field_type) .. ' ' .. v2s(field.name) .. comma)
    required_fields[field.name] = nil
    if not common_fields_holder and field.syntetic == upojo.COMMON_FIELDS then
      common_fields_holder = {
        field = field, -- to pass name and type
        clazz = assert(self:resolve_class_by_type(clazz, field.type), 'clazz'),
      }
    end
  end

  -- add method-delegates into nested pojo to emulate flat structure described
  -- with required(fileds) in the api-component-schemas
  if common_fields_holder then
    local cfh_name = (common_fields_holder.clazz or E).name or
        common_fields_holder.field.type or ''
    for _, fieldname in pairs(clazz.required or E) do
      if required_fields[fieldname] then
        local fields = (common_fields_holder.clazz or E).fields
        local holder_field_name = common_fields_holder.field.name

        local field = C.get_elm_by_name(fields, fieldname)
        if not field then
          field = { -- stub
            description = 'this field is not found in the ' .. cfh_name ..
                ' (a class with common_fields)',
            name = fieldname,
          }
          self:warning('not found required field: ' .. v2s(fieldname) ..
            ' in ' .. v2s(holder_field_name) .. ' for ' .. v2s((clazz or E).fqcn))
        end
        field.type = field.type or 'Object'
        -- public String name() { return this.dnsRecord.name(); }
        local line = 'public ' .. field.type .. ' ' .. fieldname .. '() {' ..
            ' return this.' .. holder_field_name .. '.' .. fieldname .. '(); }'
        self:add(0, '')
        self:addDocBlockComment(depth + 1, field)
        self:add(depth + 1, line)
      end
    end

    -- extra method to get access to common_fields_holder
    local field = common_fields_holder.field
    local line = 'public ' .. field.type ..
        ' get' .. common_fields_holder.clazz.name .. '() {' ..
        ' return this.' .. field.name .. '; }'
    self:add(0, '')
    self:add(depth + 1, line)
  end

  -- add inner records
  for _, iclazz in ipairs(clazz.classes or E) do
    self:add(0, '')
    self:gen_source_code_of_record(depth + 1, iclazz)
  end
  self:add(depth, '}')
end

--
--
--
---@param depth number
---@param clazz table
---@return table output
function C:gen_source_code_of_interface(depth, clazz)
  self:addDocBlockComment(depth, clazz)
  local opts = self.opts or E

  local extends = self:build_extends_definition(clazz)
  self:add(depth, 'public interface ' .. v2s(clazz.name) .. extends .. ' {')

  if clazz.implements then
    error('interface cannot implements another interface but can only extends it')
  end
  local only_records_impl = opts.as_records and self:is_all_impls_are_records(clazz.fqcn)

  for _, field in ipairs(clazz.fields) do
    local field_type = self:get_field_type(field, clazz)
    self:addDocBlockComment(depth + 1, field)
    if only_records_impl then
      self:add(depth + 1, field_type .. ' ' .. field.name .. '();')
    else
      local fname = C.mk_upper_first_char(field.name)
      self:add(depth + 1, field.type .. ' get' .. fname .. '();')
      if not field.read_only then
        local vn = self:get_varname_from_field(field)
        local param = field_type .. ' ' .. vn
        self:add(depth + 1, 'void set' .. fname .. '(' .. param .. ');')
      end
    end
    self:add(0, '');
  end

  -- add inner records
  if #(clazz.classes or E) > 0 then
    error('interface cannot content inner classes')
  end

  self:add(depth, '}')

  return self.output
end

--
--
--
---@param depth number
---@param clazz table
function C:gen_source_code_of_abstract_clazz(depth, clazz)
  self:addDocBlockComment(depth, clazz)
  -- local opts = self.opts or E
  -- local only_records_impl = opts.as_records and self:is_all_impls_are_records(clazz.fqcn)

  local extends = self:build_extends_definition(clazz)
  local impls = self:build_implements_definition(clazz)
  self:add(depth, 'public abstract class ' .. v2s(clazz.name) .. extends .. impls .. ' {')

  -- generate fields
  for _, field in ipairs(clazz.fields) do
    local fieldtype = self:get_field_type(field, clazz)
    self:addDocBlockComment(depth + 1, field)
    self:add(depth + 1, 'protected ' .. fieldtype .. ' ' .. field.name .. ';')
  end

  -- generate getters and setters
  for _, field in ipairs(clazz.fields) do
    local fname_part = C.mk_upper_first_char(field.name)
    local field_type = self:get_field_type(field, clazz)
    self:add(0, '')
    self:add(depth + 1, 'public ' .. field_type .. ' get' .. fname_part .. '() {')
    self:add(depth + 2, --[[]] 'return this.' .. field.name .. ';')
    self:add(depth + 1, '}')

    if not field.read_only then
      local param = field_type .. ' ' .. field.name
      self:add(0, '')
      self:add(depth + 1, 'public void set' .. fname_part .. '(' .. param .. ') {')
      self:add(depth + 2, --[[]] 'this.' .. field.name .. ' = ' .. field.name .. ';')
      self:add(depth + 1, '}')
    end
  end

  -- inner classes ?? is ok inner classes for abstrac class?
  for _, iclazz in ipairs(clazz.classes or E) do
    self:add(0, '')
    self:gen_source_code(depth + 1, iclazz)
  end

  self:add(depth, '}')

  return self.output
end

--
---@param clazz table
function C:build_extends_definition(clazz)
  local s = ''
  if clazz.extends then
    -- todo if another package add to import
    if type(clazz.extends) == 'string' then
      s = ' extends ' .. getShortClassNameOf(clazz.extends)
    elseif type(clazz.extends) == 'table' then
      -- todo scn
      s = ' extends ' .. table.concat(clazz.extends, ', ')
    end
  end
  return s
end

---@param clazz table
function C:build_implements_definition(clazz)
  local s = ''
  if clazz.implements then
    if type(clazz.implements) == 'string' then
      s = ' implements ' .. getShortClassNameOf(clazz.implements)
    elseif type(clazz.extends) == 'table' then
      -- todo scn
      s = ' implements ' .. table.concat(clazz.implements, ', ')
    end
  end
  -- todo
  return s
end

---@return table (list of fqcn)
function C:get_implementations_of(fqin)
  return upojo.get_implementations_of(self.state, fqin)
end

---@param fqin string
function C:is_all_impls_are_records(fqin)
  fqin = fqin
  local impls = self:get_implementations_of(fqin)
  if #(impls or E) > 0 then
    for _, clazz in ipairs(impls) do
      -- todo way to detect records
      if clazz.extends ~= nil or #(clazz.methods or E) > 0 then
        return false
      end
    end
    return true
  end
  return false
end

function C.mk_upper_first_char(s)
  return s:sub(1, 1):upper() .. s:sub(2)
end

--
---@param depth number
---@param clazz table
---@return table output lines
function C:gen_source_code(depth, clazz)
  if clazz.typ == INTERFACE then
    self:gen_source_code_of_interface(depth, clazz)
  elseif clazz.typ == ABSTRACT then
    self:gen_source_code_of_abstract_clazz(depth, clazz)
  else
    self:gen_source_code_of_record(depth, clazz)
  end
  return self.output
end

function C:warning(msg)
  local quiet = ((self.state or E).opts or E).quiet
  if not quiet then
    print('[WARNING]', msg)
  end
end

--------------------------------------------------------------------------------

-- helpers

--
--
--
---@param clazz table{imports, pkg, name, fqcn}
---@param scn string short class name(type of the field)
function C:resolve_class_by_type(clazz, scn)
  log_debug("resolve_class_by_type %s in %s", scn, clazz.qfcn)
  -- todo implement full logic to resolve fqcn based on import and class-container name
  local fqcn = clazz.pkg .. '.' .. scn
  local found_clazz = upojo.get_already_created_clazz(self.state, fqcn, nil)
  -- todo find in inner classes
  return found_clazz
end

--
-- to track fields without types
---@param named_elms_list table{name}
function C.get_elm_by_name(named_elms_list, name)
  for _, elm in ipairs(named_elms_list or E) do
    if elm.name == name then
      return elm
    end
  end
  return nil
end

--
-- to track fields without types
---@param field table
---@param clazz table{fqcn}
function C:get_field_type(field, clazz)
  local ftype = field.type or 'Object'
  if not field.type then
    if not field.type then
      local fqcn = (clazz or E).fqcn or (clazz or E).name or '?'
      self:add_err('field without type in ' .. v2s(fqcn) .. ' ' .. v2s(field.name))
    end
  end
  return ftype
end

---@return string
function C:get_varname_from_field(field)
  local field_type = field.type or 'Object'
  if field.name then
    return field.name
  end
  local vn = getVarNameForClassName(field_type)
  -- todo boolean -> b, int - i
  return vn or 'variable'
end

--
--
---@param fqcn string?
---@return table?
function C:get_clazz(fqcn)
  return ((self.state or E).classes or E)[fqcn or false]
end

--------------------------------------------------------------------------------


---@param gen env.langs.java.JGen
function C.convert_for_component_gen(gen, clazz, ci)
  local t = {
    -- imports
    fields = {},
    methods = {}
  }
  local methodGen = gen:getMethodGen(ci)

  t.methods[1] = methodGen:genConstructor({ params = {} })
  t.methods[2] = nil -- for allParamsConstructor

  for _, raw_field in ipairs(clazz.fields) do
    local field = Field:new(raw_field)
    t.fields[#t.fields + 1] = field
    -- gen getters and setters
    t.methods[#t.methods + 1] = methodGen:genGetter(field, nil)
    t.methods[#t.methods + 1] = methodGen:genSetter(field, nil)
  end
  t.methods[#t.methods + 1] = methodGen:genIsEqualTo(t.fields, 'equals')
  -- all params contructor
  t.methods[2] = methodGen:genConstructor({ params = t.fields })
  -- todo: hashCode toString etc

  return t
end

--------------------------------------------------------------------------------

-- helper
-- child overrided a parent props but inherit it
function C.merge_with_flat_copy(parent_node, child_node)
  local t = {}
  for k, v in pairs(parent_node) do t[k] = v end
  for k, v in pairs(child_node) do t[k] = v end
  return t
end

-- aka copy keys from the list(of keys) into map(hashset)
function C.list_to_keys_map(list, value)
  local map = {}
  for _, key in ipairs(list or E) do
    map[key] = value or true
  end
  return map
end

class.build(C)
return C

-- 14-01-2025 @author Swarg
-- to refactor java source code

local log = require 'alogger'
local syntax = require 'env.langs.java.util.syntax'
local jgrammar = require 'env.langs.java.util.grammar'
local JParser = require 'env.langs.java.JParser'
local consts = require 'env.langs.java.util.consts'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local PRIM_VALUES = consts.JAVA_PRIM_VALUES_MAP
local TAGS = jgrammar.tags
local log_debug = log.debug
local lfmt = log.format
-- local dvisualize = require 'dprint'.visualize

local find = string.find

local OPPOSITE_BOOL_OP = {
  ['&&'] = '||',
  ['||'] = '&&',
  ['!='] = '==',
  ['=='] = '!=',
  ['>'] = '<=',
  ['<'] = '>=',
  ['<='] = '>',
  ['>='] = '<',
  -- how about "x".equals(y)  ?? or isTrue(x) ?  or !isTrue(
}

-- "> " -> ">"  or ">x" -> ">"
---@param s string
---@return string
---@return number
local function fix_bool_op(s)
  if s == '&&' or s == '||' then return s, 2 end

  local fc = string.sub(s, 1, 1)
  if fc == '>' or fc == '<' or '!' then
    local sc = string.sub(s, 2, 2)
    if sc ~= '=' then
      return fc, 1 -- "> " --> ">"   "!i" --> "!"
    end
  end

  return s, 2 -- by default the bool op - two chars long
end

local function trim(s)
  return match(s or '', '^%s*(.-)%s*$')
end

--
-- if (logger != null && bin != null && bin.length > 0) {
-- if (logger == null || bin == null || bin.length <= 0) {
--
-- WARN! not supported cases:
-- if (s != null && !s.isEmpty()) {
--
---@param line string
function M.invert_condition(line)
  if not line or line == '' then return line end

  local p, t = 1, {}
  local sub = string.sub
  local limit = 100
  local ind = match(line, '^(%s*)')
  if ind and #ind > 0 then t[#t + 1] = ind:sub(1, #ind - 1) end

  while p < #line do
    local i = find(line, '[%!%=%>%<%&%|]+', p, false)
    if not i then
      t[#t + 1] = trim(sub(line, p, #line))
      break
    end
    local op, opn, oplen
    op = sub(line, i, i + 1) -- == != && || >= <= >? <?
    op, oplen = fix_bool_op(op)

    if (op == '!') then -- ?? !isEmpty()
      opn = ''
    else
      opn = OPPOSITE_BOOL_OP[op]
    end

    if opn then
      local s = trim(sub(line, p, i - 1))
      t[#t + 1] = s
      t[#t + 1] = opn
    else
      t[#t + 1] = sub(line, p, i + 1)
    end

    limit = limit - 1; if limit < 0 then error('stop!') end

    p = i + oplen
  end

  return table.concat(t, ' ')
end

--

--
-- change the order of arguments in the line of source code with method call
--
-- method(a,b,c) --> method(c,b,a)
--
---@param line string
---@param p1 number
---@param p2 number
---@return string?
---@return string? errmsg
function M.swap_method_args(line, mname, mparams, p1, p2)
  if not mname or not mparams or not p1 or not p2 then
    return nil, 'no data to swap the method params'
  end
  if p1 == p2 then return nil, 'nothing to do' end

  local mn, args = jgrammar.pMethodCall:match(line)
  if mn ~= mname then
    return nil, 'expected method ' .. v2s(mname) .. ' got: ' .. v2s(mn)
  end
  if not args or #args == 0 then
    return nil, 'not found arguments ' .. v2s(line)
  end
  if p1 > #args or p2 > #args then
    return nil, fmt('no few args:%s p1:%s p2:%s ', v2s(#args), v2s(p1), v2s(p2))
  end
  -- swap
  local tmp = args[p2]
  args[p2] = args[p1]
  args[p1] = tmp

  local ind = match(line, '^(%s*)')
  local remains = match(line, '%);(.-)$') or ''

  return ind .. mname .. '(' .. table.concat(args, ', ') .. ');' .. remains
end

-- signature
function M.parse_signature_param_types(s)
  return { jgrammar.pSignatureParamTypes:match(s) }
end

--
-- change the order of arguments in the line of source code with method call
--
-- method(a,b,c) --> method(b,c,a)
--
---@param line string
---@param from number
---@param to number
---@return string?
---@return string? errmsg
function M.move_method_arg(line, mname, mparams, from, to)
  log_debug("move_method_arg", mname, mparams, from, to)
  if not mname or not mparams or not from or not to then
    return nil, 'no data to move method arg'
  end
  if from == to then return nil, 'nothing to do' end

  local mn, args = jgrammar.pMethodCall:match(line)
  if mn ~= mname then
    return nil, 'expected method ' .. v2s(mname) .. ' got: ' .. v2s(mn)
        .. ' in line:' .. v2s(line)
  end
  if not args or #args == 0 then
    return nil, 'not found arguments ' .. v2s(line)
  end
  if from > #args or to > #args then
    return nil, fmt('no few args:%s p1:%s p2:%s ', v2s(#args), v2s(from), v2s(to))
  end
  local new_args = {}
  if to == #args then -- to the end
    for n, arg in ipairs(args) do
      if n ~= from then
        local pos = #new_args + 1
        if pos == to then
          new_args[pos] = false -- place for arg_to_move
          pos = pos + 1
        end
        new_args[pos] = arg
      end
    end
  end
  new_args[to] = args[from]

  local ind = match(line, '^(%s*)')
  local remains = match(line, '%);(.-)$') or ''

  return ind .. mname .. '(' .. table.concat(new_args, ', ') .. ');' .. remains
end

--
---@param line string
---@param ast table   sub-ast with one method entry
---@param argn number
---@param value string
---@return string?
---@return string? errmsg
function M.set_method_arg(line, ast, argn, value)
  log_debug("set_method_arg", argn, line)
  assert(type(line) == 'string', 'line')
  assert(type(ast) == 'table', 'ast with method entry')
  assert(type(argn) == 'number', 'argn')
  assert(type(value) == 'string', 'value')

  local argvalue, ps, pe = syntax.get_method_arg(ast, argn)

  if type(argvalue) ~= 'string' or not ps or not pe then
    return nil, 'no arg#' .. v2s(argn) .. ' args:' .. v2s(#ast - 1)
  end

  value, ps, pe = M.prepare_value_to_insert(line, ps, pe, value)
  return line:sub(1, ps - 1) .. v2s(value) .. line:sub(pe + 1, #line)
end

---@param line string
---@param ast table
---@param new_name string
function M.set_method_name(line, ast, new_name)
  log_debug("set_method_name", new_name)
  assert(type(line) == 'string', 'line')
  assert(type(ast) == 'table', 'ast with method entry')
  assert(type(new_name) == 'string' and new_name ~= '', 'new_name')
  local name, ps, pe = syntax.get_method_name_from_subast(ast)

  if type(name) ~= 'string' or not ps or not pe then
    return nil, lfmt('no method name in %s for new_name:%s', ast, new_name)
  end
  return line:sub(1, ps - 1) .. v2s(new_name) .. line:sub(pe + 1, #line)
end

--
-- find space or some separator to do not split line in middle of the word
---@return number
---@param str string
---@param pstart number
---@param pend number
local function back_index_of_sep(str, pstart, pend, max_len)
  local e = pstart + max_len
  if e > pend or e >= #str then
    return pend
  end

  if e < pend then
    local limit = max_len
    while e >= pstart and limit > 0 do
      local c = str:sub(e, e)
      local is_sep = c == ' ' or c == '\\n' or c == ',' or c == ';'
      -- dvisualize(str, e, e, is_sep)
      if is_sep then
        break
      end
      e = e - 1
      limit = limit - 1
    end
  end

  -- if separator char is not found -
  if e <= pstart then
    e = math.min(pend, pstart + max_len)
  end

  return e
end

--
-- 'String line = "line-1\nline-2\nline-3\n";' -->
--
-- 'String line = "line-1\n" +'
--         "line-2\n" +
--         "line-3\n";
--
---@param line string
---@param max_len number?
---@return table
function M.fold_one_string_to_many(line, max_len)
  max_len = max_len or 80
  if max_len < 8 then max_len = 8 end

  -- tab = tab or '        '
  local sub = string.sub
  local ind, vn = '        ', 's'
  ind, vn, line = match(line, '^(%s*)String ([%w_]+)%s*=%s*"(.-)"%s*;?%s*$')
  assert(line, 'line')

  local lines = {}
  local function append0(str, s, e)
    local line0 = ind .. ind .. '"' .. sub(str, s, e) .. '" +'
    lines[#lines + 1] = line0
  end

  -- dvisualize(line, 1, #line, 'length')

  ---@param str string
  ---@param s number
  ---@param e number
  local function append(str, s, e)
    if #str <= max_len then
      append0(str, s, e)
    else
      local p0 = s
      while p0 <= e do
        local e0 = back_index_of_sep(str, p0, e, max_len)
        append0(str, p0, e0);
        p0 = e0 + 1
      end
    end
  end
  lines[#lines + 1] = ind .. "String " .. vn .. ' ='

  local p = 1
  while p <= #line do
    local _, i = find(line, "\\n", p, true)
    if not i then
      append(line, p, #line)
      break
    end
    append(line, p, i)
    p = i + 1
  end

  local last = lines[#lines]
  last = last:sub(1, #last - 2) .. ';'
  lines[#lines] = last

  return lines
end

--
-- kind of one to many
--
-- String exp = "MyClass[id=123, name=abc, active=true]" -->
--
-- String exp = "MyClass["+
--         "id=123,"+
--         " name=abc,"+
--         " active=true"+
--         "]"
--
---@param line string
---@param max_len number?
---@return table
function M.beautify_tostring_output(line, max_len)
  max_len = max_len or 80
  if max_len < 8 then max_len = 8 end

  -- tab = tab or '        '
  local sub = string.sub
  local ind, vn = '        ', 's'
  ind, vn, line = match(line, '^(%s*)String ([%w_]+)%s*=%s*"(.-)"%s*;?%s*$')
  assert(line, 'line')

  local lines = {}
  local function append(str, s, e)
    local line0 = ind .. ind .. '"' .. sub(str, s, e) .. '" +'
    lines[#lines + 1] = line0
  end

  local p = 1
  while p <= #line do
    local _, i = find(line, "[,%[%]{}]", p, false)
    if not i then
      append(line, p, #line)
      break
    end
    local next_char = line:sub(i + 1, i + 1)
    if next_char == ']' or next_char == '}' then
      local c = line:sub(i, i)
      if c == '[' and next_char == ']' or c == '{' and next_char == '}' then
        i = i + 1
      end
      c = line:sub(i + 1, i + 1)
      if c == ',' then
        i = i + 1
      end
    elseif next_char == ',' then -- ] \n ,  -> ],
      i = i + 1
    end
    append(line, p, i)
    p = i + 1
  end
  lines[1] = ind .. "String " .. vn .. ' = ' .. (match(lines[1], '^%s*(.-)$') or '')

  local last = lines[#lines]
  last = last:sub(1, #last - 2) .. ';'
  lines[#lines] = last

  return lines
end

--------------------------------------------------------------------------------

--
-- Create a table of offsets according to the sizes of the lines, so that
-- it could, according to it, can, according to the absolute position of the
-- offsets from the line joined in one big-line, calculate the line number and
-- the position from the beginning of each separated line
--
---@param lines table
---@return table
local function build_lnums_offsets(lines)
  local lnum_offsets = {}
  local prev = 0
  for _, line in ipairs(lines) do
    local off_s = prev
    local off_e = #line + prev + 1
    lnum_offsets[#lnum_offsets + 1] = { off_s, off_e }
    prev = off_e
  end
  return lnum_offsets
end

--
-- pair for build_lnums_offsets
-- It is used in order to get line-number and col-positions for the nvim buffer
-- in which each line separately, based on
--
---@param lnum_offsets table - table from build_lnums_offsets()
---@param offset_s number is a start position in big-line from joined lines
---@param offset_e number is a end position in big-line from joined lines
---@return number? lnum  - line number calculated from lnum_offsets
---@return number? pos_s - start position in particular line(not whole big-line)
---@return number? pos_e - end position
local function find_lnum_and_pos(lnum_offsets, offset_s, offset_e)
  for lnum0 = #lnum_offsets, 1, -1 do
    local eoff = lnum_offsets[lnum0]
    local off_s0, off_e0 = eoff[1], eoff[2]
    if off_s0 < offset_s and offset_s < off_e0 then
      local vlnum = --[[method.ln-1 +]] lnum0
      local pos_s = offset_s - off_s0 - 1
      local pos_e = pos_s + (offset_e - offset_s)
      return vlnum, pos_s, pos_e
    end
  end
  return nil, nil, nil
end

--
-- by the number of the line of the code in which the local variable with a given
-- name is used, to find the class method and a specific place where
-- the assignment of the value of a given local variable is going on,
-- in order to change this value to substitute another text(new value).
--
---@param ctx env.ui.Context
---@param varname string
---@param old_value string
---@return table?
---@return string? errmsg
function M.find_variable_assigment(ctx, varname, old_value)
  log_debug("find_variable_assigment", varname, old_value)
  local lnum = ctx.cursor_row
  ctx.parser = ctx.parser or JParser:new()

  ctx:parse(function(parser0, tag)
    return parser0.lnum > lnum and tag == TAGS.METHOD
  end)

  local parser = ctx.parser
  ---@cast parser env.langs.java.JParser
  local method = parser.findParsedMethodByLnum(lnum, ctx.parser:getResult())
  local m = method or E
  log_debug('found parsed method:%s ln:%s lne:%s', m.name, m.ln, m.lne)
  if not method then
    return nil, 'not found method for lnum: ' .. v2s(lnum)
  end
  if not method.ln or not method.lne then
    return nil, 'method without lnum positions'
  end
  local mb_lines = ctx:getLines(method.ln, method.lne)

  local place = nil
  local str = table.concat(mb_lines, "\n")
  local methodbody = match(str, "{\n*(.-)%s*}%s*$")
  local off0 = str.find(str, methodbody, 1, true)

  -- parse to find lines with assignment of the value of the variable
  local ast = jgrammar.offhand.pLocalVarAssignmentsWithPos:match(methodbody)
  if not ast or #ast == 0 then
    log_debug('not found assignments for var:%s in :%s\n', varname, methodbody)
    return nil, 'cannot parse method body'
  end

  local lnum_offsets = build_lnums_offsets(mb_lines)

  -- down to top: find first assignment from a button of the code
  -- todo by lnum with assert*-method
  for i = #ast, 1, -1 do
    local sub_ast = ast[i] -- with variable definition, assignment, def+assignment
    local varname0 = sub_ast.varname[1]
    if varname0 == varname then
      log_debug('found variable with same name:', varname)
      -- local value0 = unwrap_value((sub_ast.value or E)[1])
      local value0 = (sub_ast.value or E)[1]
      -- '""' - to allow replace old value only for empty like: String exp = "";
      if syntax.is_same_value(value0, old_value) or value0 == '""' then
        local off_s = off0 + assert(sub_ast.value[2], 'offset_s')
        local off_e = off0 + assert(sub_ast.value[3], 'offset_e')
        local vlnum, pos_s, pos_e = find_lnum_and_pos(lnum_offsets, off_s, off_e)
        if vlnum and pos_s and pos_e then
          place = {
            varname = varname,
            old_value = old_value,
            lnum = method.ln + vlnum - 1,
            lnum_end = method.ln + vlnum - 1,
            col = pos_s,
            col_end = pos_e,
          }
          break
        end
      else
        log_debug('value mismatch v:|%s| ov:|%s|', value0, old_value)
      end
    end
  end

  log_debug('place found: ', place ~= nil)
  return place, nil
end

--
-- change the contents of the text in the line(s) of the nvim buffer
-- in a position according to a given plan
--
---@param ctx env.ui.Context
---@param plan table
---@param new_value string
---@return boolean?
---@return string? err
function M.substitute_new_value(ctx, plan, new_value)
  log_debug("substitute_new_value plan:%s new_value:|%s|", plan, new_value)
  local lnum = assert(plan.lnum, 'lnum')
  local lnum_end = assert(plan.lnum_end, 'lnum_end')
  local col = assert(plan.col, 'col')
  local col_end = assert(plan.col_end, 'col_end')

  local line = ctx:getLines(lnum, lnum_end)[1]
  if not line then
    return false, 'not found line'
  end
  -- TODO
  -- support for multiline values like:   "abc\n" + "de\n"
  local value, ps, pe = M.prepare_value_to_insert(line, col, col_end, new_value)
  local uline = line:sub(1, ps - 1) .. v2s(value) .. line:sub(pe + 1, #line)
  ctx:setLines(lnum, lnum_end, { uline })

  return true
end

-- change col and col_end for digits, bools, null, etc
---@return string
---@return number
---@return number
function M.prepare_value_to_insert(line, col, col_end, new_value)
  local fc = line:sub(col, col)
  if new_value == '' and fc ~= '"' then
    return '""', col, col_end
  end

  -- todo "line1\n"+"line2\n"+"line\3" for big strings
  local old_value = nil -- to check strings substitution
  new_value = new_value:gsub("\n", "\\n")

  local fcnv = new_value:sub(1, 1)

  if fc == '"' and fcnv ~= fc then
    col = col + 1
    col_end = col_end - 1
  elseif fc ~= '"' and fcnv ~= '"' then
    -- case then your substitute string value to not a sting(wo "") place
    old_value = line:sub(col, col_end)
    if not PRIM_VALUES[old_value] and not tonumber(old_value)
        or not tonumber(new_value) and not PRIM_VALUES[new_value]
    then
      new_value = '"' .. new_value .. '"'
    end
  end
  log_debug('prepared new_value:|%s| old_value:|%s|', new_value, old_value)
  return new_value, col, col_end
end

return M

-- 01-01-2025 @author Swarg
--

local log = require 'alogger'

local M = {}

M.DEFAULT_OPTS = ''

local log_debug = log.debug

--
--
--
---@param classfile string - absolute path to class file
---@param params table {java_home, decompiler_jar, outdir, libs, verbose}
---@return false|string
---@return string? errmsg
function M.decompile_file(classfile, params)
  log_debug("decompile_file", classfile, params)
  classfile = classfile
  params = params
  return false, 'not implemented yet'
end

return M

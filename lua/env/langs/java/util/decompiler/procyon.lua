-- 31-12-2024 @author Swarg
-- experimental
--
-- integration with procyon decompiler
--

local log = require 'alogger'
local fs = require 'env.files'
local common = require 'env.langs.java.util.decompiler.common'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local prepare_dependencies_libs = common.prepare_dependencies_libs
local get_java_executable = common.get_java_executable
local log_debug = log.debug

M.DEFAULT_OPTS = '--debug-line-numbers --stretch-lines'

--
--
-- fqcn (fully qualifed classname can be usefull then classfile has diff name)
--
---@param classfile string - absolute path to class file
---@param params table {java_home, decompiler_jar, outdir, libs, verbose, fqcn}
---@return string?
---@return string? errmsg
function M.decompile_file(classfile, params)
  log_debug("decompile_file", classfile, params)

  local fqcn, inner_path = common.get_validated_fqcn_and_inner_path(params)

  local java = get_java_executable(params)
  local decomp_opts = params.decomp_opts or M.DEFAULT_OPTS
  local verbose = params.verbose
  local decompiler_jar = assert(params.decompiler_jar, 'decompiler_jar')
  local outdir = assert(params.outdir, 'outdir')

  decomp_opts = decomp_opts .. ' --output-directory ' .. outdir

  local libs = prepare_dependencies_libs(params.libs)

  local cmd = java .. fmt(" -jar %s %s %s '%s' 2>&1",
    decompiler_jar, decomp_opts, libs, classfile)

  log_debug("command: ", cmd)
  local lines = fs.execrl(cmd)

  if verbose then for _, line in ipairs(lines) do print(line) end end

  for _, line in ipairs(lines or E) do
    -- Here the problem is that the decompilator does not show the full path
    -- where it saved the Java file. And there is no option to display the full
    -- path to the saved Java file, but it can be obtained if you read the real
    -- full name of the class from the inside of the bytcode or
    -- if the fully qualifed classname is known in advance.
    local path2classfile = match(line, 'Decompiling%s+(.-)%.class%.%.%.$')
    if path2classfile then
      log_debug("found decompiling: ", path2classfile)
      local path2java = common.get_path2java(outdir, fqcn, inner_path)

      if not fs.file_exists(path2java) then
        return nil, 'Not Found output:' .. v2s(path2java) .. "\n" ..
            table.concat(lines, "\n");
      end

      return path2java
    end
  end

  return nil, table.concat(lines, "\n")
end

return M

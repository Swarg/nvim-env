-- 02-10-2024 @author Swarg
-- DecompilerManager:
-- - wrapper around multiple decompilers
-- -
--
-- intregration with my own research-agent
-- - find place from with class was loaded(class-info)
-- - get bytecode of a given class from running jvm
-- - and other research tools
--
-- org.eclipse.jdt.ls.core.internal.decompiler.FernFlowerDecompiler
--
-- jb_fernflower
-- org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences
-- org.jetbrains.java.decompiler.main.decompiler.ConsoleDecompiler
--



local log = require 'alogger'
local fs = require 'env.files'

local M = {}

local home = os.getenv('HOME')
local java_home = os.getenv('JAVA_HOME')

M.DECOMP_DIR = home .. "/tmp-agent/decomp/"

M.DEFAULT_DECOMPILTER = 'jb_fernflower'

M.DECOMPILER_PROVIDERS = {
  javap = java_home .. '/bin/javap', -- standart decompiller from JDK

  jb_fernflower = home .. "/Decomp/jb-fernflower/fernflower.jar",
  -- home .. "/Decomp/jb-fernflower/decompiler-engine.jar", -- taken from jdtls
  vineflower = home .. "/Decomp/vineflower/vineflower.jar",       -- jvm > 11
  -- Quiltflower 1.9.0 required jvm11(29-10-2022)
  quiltflower = home .. "/Decomp/vineflower/quiltflower.jar",     -- jvm < 11

  procyon = home .. "/Decomp/procyon/procyon-decompiler-1.0.jar", -- old
  fernflower = home .. "/Decomp/fernflower/fernflower.jar",       -- old
  -- todo cfr
}

local decompiler_ordered_names = {
  'javap',         -- standard from jdk
  'jb_fernflower', -- mainained by JetBrains (my fork with bsm+dol+utm)
  'vineflower',    -- new jvm > 11
  'quiltflower',   -- old jmv < 11
  'fernflower',    -- old suitable for java8
  'procyon',       -- old cannot work with java8+?
  -- cfr
}




---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local file_exists = fs.file_exists

--
-- readable list of decompilers names
--
---@return table
function M.get_decompilers_list()
  local names = decompiler_ordered_names
  local t = {}
  for _, name in ipairs(names) do
    t[#t + 1] = name
  end
  return t
end

--
---@return boolean
function M.is_supported_decompiler(name)
  return M.DECOMPILER_PROVIDERS[name or false] ~= nil
  -- and file_exists ?
end

--
---@param decompiler_name string?
---@return false|string
---@return string? errmsg
function M.get_decompiler_jar(decompiler_name)
  local name = decompiler_name or M.DEFAULT_DECOMPILTER
  local path = M.DECOMPILER_PROVIDERS[name or false]
  if not path then
    return false, 'unknown decompiler: ' .. v2s(name)
  end
  if not file_exists(path) then
    return false, 'not found path: ' .. v2s(path) .. ' for: ' .. v2s(name)
  end
  return path, nil
end

--
-- dynamic loading of the lua-module for a given decompiler name
--
---@param decompiler_name string
---@return table? module
---@return string? errmsg
function M.get_decompiler_module(decompiler_name)
  if not decompiler_name or #decompiler_name < 2 then
    return nil, 'bad decompiler name: ' .. v2s(decompiler_name)
  end
  local modname = 'env.langs.java.util.decompiler.' .. v2s(decompiler_name)

  local ok, mod = pcall(require, modname)
  if not ok or type(mod) ~= 'table' then
    return nil, 'cannot find module: ' .. v2s(modname)
  end

  return mod, nil
end

--
--
-- decompile the one specified classfile
--
---@param path_to_classfile string absolute path to the classfile
---@param opts table?{libs, decompiler_name, inner_path, decompiler_jar, output_dir}
---@return string? path to decompiled java source file
---@return string? errmsg
function M.decompile_classfile(path_to_classfile, opts)
  log_debug("decompile_classfile", path_to_classfile, opts)
  opts = opts or E
  local decompiler_jar, decompiler_name, decompiler_module, err

  decompiler_jar = opts.decomp_jar
  decompiler_name = opts.decompiler

  if not decompiler_jar then
    if not decompiler_name then
      return nil, 'decompiler_name is not specified'
    end
    decompiler_jar, err = M.get_decompiler_jar(decompiler_name)
    if not decompiler_jar then
      return nil, err
    end
  elseif not file_exists(decompiler_jar) then
    return nil, 'Not found: ' .. v2s(decompiler_jar)
  end

  decompiler_module, err = M.get_decompiler_module(decompiler_name)
  if not decompiler_module then
    return nil, err
  end

  local aclassfile = fs.mk_abspath(path_to_classfile)
  if not file_exists(aclassfile) then
    return nil, 'not found classfile: ' .. v2s(path_to_classfile)
  end

  local outdir = opts.output_dir or M.DECOMP_DIR
  log_debug('outdir:', outdir)

  if not fs.dir_exists(outdir) then -- ?? ./
    log_debug('mkdir:', outdir)
    fs.mkdir(outdir)
  end

  local path, errd = decompiler_module.decompile_file(aclassfile, {
    java_home = opts.java_home or java_home,
    decompiler_jar = decompiler_jar,
    outdir = outdir,
    verbose = opts.verbose,
    inner_path = opts.inner_path,
    fqcn = opts.classname, -- helper to get (fqcn) fully qualified class name
  })

  if not path and errd then
    -- case: current java is 8(v52) but required 17(v61)+
    -- Exception in thread "main" java.lang.UnsupportedClassVersionError:
    -- <ClassName> has been compiled by a more recent version of
    -- the Java Runtime (class file version 61.0), this version of the
    -- Java Runtime only recognizes class file versions up to 52.0
    local patt = '\nException in thread "main"%s*([^:]+)%s*:%s*([^\n]+)%s*\n'
    local exception, msg = match(errd, patt)
    local pref = ''
    if exception then
      if exception == 'java.lang.UnsupportedClassVersionError' then
        pref = "(Seems you have installed a too old java version)\n"
      end
      error(pref .. v2s(exception) .. ': ' .. v2s(msg))
    end
  end
  return path, errd
end

--
return M

-- 01-01-2025 @author Swarg
-- integration with standart javap from JDK
--
-- NOTE:
-- javap supported:
--  jar:file:///path/to/MyJar.jar!/mypkg/MyClass.class
--

local log = require 'alogger'
local fs = require 'env.files'
local common = require 'env.langs.java.util.decompiler.common'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local join_path, file_exists = fs.join_path, fs.file_exists
local log_debug = log.debug
local prepare_dependencies_libs = common.prepare_dependencies_libs

-- -l - to show LineNumberTable
-- -c - to show bytecode
-- -private - to show all members of the class
M.DEFAULT_OPTS = '-l -private -c'

--
--
--
---@param classfile string - absolute path to class file
---@param params table {java_home, decompiler_jar, outdir, libs, verbose}
---@return false|string
---@return string? errmsg
function M.decompile_file(classfile, params)
  log_debug("decompile_file", classfile, params)
  local javap = assert(params.decompiler_jar, 'decompiler_jar')
  local inner_path = assert(params.inner_path, 'inner_path')
  local outdir = assert(params.outdir, 'outdir')
  local decomp_opts = params.decomp_opts or M.DEFAULT_OPTS
  local cp_sep = fs.is_os_windows() and ';' or ':'
  local libs = prepare_dependencies_libs(params.libs, cp_sep)
  if libs ~= '' then
    decomp_opts = decomp_opts .. ' -classpath '
  end

  -- /tmp/decompiled/pkg/some/Main.class.javap
  local outfile = join_path(outdir, inner_path .. '.javap')

  local cmd = fmt("%s %s '%s' '%s' > %s",
    javap, decomp_opts, libs, classfile, outfile)

  log_debug('command: ', cmd)
  os.execute(cmd)

  return outfile, nil
end

--[[
table of the class-file bytecode versions (52 -> java8,  61 -> java17)

version           code    Release     End of public    End of extended
                           date       updates (free)   support (paid)

JDK  1.0            45   23-01-1996   xx-03-1996       -
JDK  1.1            45   18-02-1997   xx-10-2002       -
J2SE 1.2            46   04-12-1998   xx-11-2003       -
J2SE 1.3            47   08-05-2000   xx-04-2006       -
J2SE 1.4            48   13-02-2002   xx-10-2008       -
J2SE 5.0 (1.5)      49   30-09-2004   xx-10-2009       -
JavaSE 6 (1.6)      50   11-12-2006   xx-04-2013       xx-10-2018
JavaSE 7 (1.7)      51   28-07-2011   xx-07-2015       -
JavaSE 8 (1.8) LTS  52   18-03-2014   xx-04-2019       xx-12-2030
JavaSE 9 (1.9)      53   21-09-2017   xx-03-2018       -
JavaSE 10(1.10)     54   20-03-2018   xx-09-2018       -
JavaSE 11      LTS  55   25-09-2018   xx-04-2019       xx-01-2032
JavaSE 12           56   19-03-2019   xx-09-2019       -
JavaSE 13           57   17-09-2019   xx-03-2020       -
JavaSE 14           58   17-03-2020   xx-09-2020       -
JavaSE 15           59   16-09-2020   xx-03-2021       -
JavaSE 16           60   16-03-2021   xx-09-2021       -
JavaSE 17      LTS  61   14-09-2021   xx-09-2024       xx-09-2029
JavaSE 18           62   22-03-2022   xx-09-2022       -
JavaSE 19           63   20-09-2022   xx-03-2023       -
JavaSE 20           64   21-03-2023   xx-09-2023       -
JavaSE 21      LTS  65   19-09-2023   xx-09-2028       xx-09-2031
JavaSE 22           66   19-03-2024   xx-09-2024       -
JavaSE 23           67   17-09-2024   xx-03-2025       -
JavaSE 24           68   ??-03-2025   xx-09-2025       -
JavaSE 25      LTS  69   ??-09-2025   xx-09-2030       xx-09-2033

The acquisition of Sun Microsystems by Oracle Corp. was completed on 27-01-2010.
]]

--[[
1   January   31
2   February  28 (29 in leap years)
3   March     31
4   April     30
5   May       31
6   June      30
7   July      31
8   August    31
9   September 30
10  October   31
11  November  30
12  December  31
]]

return M

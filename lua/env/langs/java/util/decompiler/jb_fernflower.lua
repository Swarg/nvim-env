-- 01-01-2025 @author Swarg
--
-- integration with jb_fernflower (used in jdtls and Inteleji IDEA)
--
-- Sources of fernflower maintained by JetBrains:
--   https://github.com/JetBrains/intellij-community/tree/master/plugins/java-decompiler/engine
--
-- under the hood this artifact use fernflower decompiler
-- and its standard options are described here:
--   org.jetbrains.java.decompiler.main.extern.IFernflowerPreferences
--
-- My fork with fixed cli-arg-parset to supports dump-original-lines and ident
--   https://gitlab.com/Swarg/fernflower
--
--

local log = require 'alogger'
local fs = require 'env.files'
local common = require 'env.langs.java.util.decompiler.common'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local join_path = fs.join_path
local log_debug = log.debug
local get_java_executable = common.get_java_executable
local prepare_dependencies_libs = common.prepare_dependencies_libs

--
-- DUMP_ORIGINAL_LINES works only with BYTECODE_SOURCE_MAPPING
-- See src/org/jetbrains/java/decompiler/main/ClassesProcessor.java:464
--
-- HIDE_DEFAULT_CONSTRUCTOR default is 1 - 0 to get linenumber with class def
M.DEFAULT_OPTS = '-ind=4  -bsm=1 -dol=1  -hdc=0 -dgs=1'
-- Note:
-- -dol is a dump-original-linenumbers (my improvement in my fork)
-- -dgs - DECOMPILE_GENERIC_SIGNATURES
--
-- utm - UNIT_TEST_MODE  add bytecodeMapping, LiesMapping, NotMapped the end of
-- the decompiled file. (Used in test)
--   class 'pkg/some/model/Config' {
--   Lines mapping:
--     9 <-> 118         - original-line <- current-line
--   Not mapped:
--     14                - e.g. assignment to static field of the class

--
--
--
---@param classfile string - absolute path to class file
---@param params table {java_home, decompiler_jar, outdir, libs, verbose}
---@return string? path to decompiled file
---@return string? errmsg
function M.decompile_file(classfile, params)
  log_debug("decompile_file", classfile, params)

  local java = get_java_executable(params)

  local decomp_opts = params.decomp_opts or M.DEFAULT_OPTS

  local decompiler_jar = assert(params.decompiler_jar, 'decompiler_jar')
  local inner_path = assert(params.inner_path, 'inner_path')
  local outdir = assert(params.outdir, 'outdir')
  local verbose = params.verbose

  local libs = prepare_dependencies_libs(params.libs)

  local cmd = fmt("%s -jar '%s' %s %s '%s' '%s' 2>&1",
    java, decompiler_jar, decomp_opts, libs, classfile, outdir)

  log_debug('command:', cmd)
  if verbose then print('[VERBOSE]', cmd) end
  local lines = fs.execrl(cmd)

  if verbose then for _, line in ipairs(lines) do print(line) end end

  local classfile0 = nil
  for _, line in ipairs(lines or E) do
    if not classfile0 then
      classfile0 = match(line, 'Decompiling class%s+(.-)$')
    elseif match(line, "%.%.%. done$") then
      -- jb_fernflower save output file without package subdirs
      local classname = fs.basename(classfile0)
      local decompiled_fn = join_path(outdir, v2s(classname) .. '.java')

      return common.validated_path_to_decompiled_file(
        classfile, inner_path, outdir, decompiled_fn, lines)
    end
  end

  local errmsg = table.concat(lines, "\n")
  return nil, errmsg
end

return M

--[[
INFO:  Decompiling class by/gdev/Main
INFO:  ... done

Usage: java -jar fernflower.jar [-<option>=<value>]* [<source>]+ <destination>
Example: java -jar fernflower.jar -dgs=true c:\my\source\ c:\my.jar d:\decompiled\
]]

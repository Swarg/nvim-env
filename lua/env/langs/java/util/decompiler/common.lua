-- 01-01-2025 @author Swarg
--
-- shared logic for decompilers
--

local log = require 'alogger'
local fs = require 'env.files'


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local join_path, file_exists = fs.join_path, fs.file_exists
local log_debug = log.debug

---@param params table
function M.get_java_executable(params)
  local java = 'java'
  if type(params) == 'table' then
    if params.java then
      java = params.java
    elseif params.java_home then
      java = join_path(params.java_home, 'bin', 'java')
    end
  end
  return java
end

--
-- way to define 3th party dependencies for a decompiler (aka classpath)
--
---@param libs table?
---@return string
function M.prepare_dependencies_libs(libs, sep)
  sep = sep or ' '
  if type(libs) == 'table' then
    return table.concat(libs, sep)
  elseif libs then
    return v2s(libs)
  end
  return ''
end

--
-- restore package subdirs for decompiled class-file
--
-- Main.java -> pkg/some/Main.java
--
---@param decompiled_fn string absolute path to decompiled file
---@param inner_path string - inner path to classfile in jar (pkg/some/Main.class
---@param outdir string - outputdir for decompiled java-file
---@return string
function M.restore_package_subdir(decompiled_fn, inner_path, outdir)
  local inner_path_java = inner_path:gsub('class$', 'java')
  if outdir and outdir:sub(1, 1) ~= '/' then
    outdir = join_path(os.getenv('PWD'), outdir) -- mk abs path
  end
  local abs_path_with_pkg = join_path(outdir, inner_path_java)
  local pkgdir = fs.get_parent_dirpath(abs_path_with_pkg)
  log_debug('restore_package_subdir ' ..
    'decompiled_fn:%s ipath:%s ipath_java:%s abs_path_with_pkg:%s pkgdir:%s',
    decompiled_fn, inner_path, inner_path_java, abs_path_with_pkg, pkgdir)

  if pkgdir and (fs.dir_exists(pkgdir) or fs.mkdir(pkgdir)) then
    os.rename(decompiled_fn, abs_path_with_pkg)
    decompiled_fn = abs_path_with_pkg
  else
    log_debug('cannot create directory %s', pkgdir)
  end

  return decompiled_fn
end

--
-- for decompilers wich save decompiled source file without package
-- and when you cannot get the path of the produces output file from the output
-- of the decompiler itself
--
-- check is decompiled_fn exists and try to search by basename in classfile
-- usecase: classname with md5 (research-agent)
--
---@return string? path to decompiled fn
---@return string? errmsg
function M.validated_path_to_decompiled_file(
    classfile, inner_path, outdir, decompiled_fn, lines)
  if not file_exists(decompiled_fn) then
    -- support for case when classfile and inner_path without real package
    -- only short-class name (research-agent)
    if classfile == inner_path then -- without package
      local dfn2 = join_path(outdir, fs.extract_filename(classfile) .. '.java')
      if file_exists(dfn2) then
        -- return without restore_package_subdir (because no package in classfile)
        return dfn2, nil
      end
    end

    return nil, "error: output file is Not Found: " .. decompiled_fn .. "\n"
        .. table.concat(lines, "\n");
  end

  decompiled_fn = M.restore_package_subdir(decompiled_fn, inner_path, outdir)

  return decompiled_fn, nil
end

--

--
---@param params table{fqcn, inner_path}
function M.get_validated_fqcn_and_inner_path(params)
  local fqcn = params.fqcn -- helper to find full path to decompiled java file
  local inner_path = params.inner_path
  if not fqcn and not inner_path then
    error('expected either fqcn either inner_path (to-classfile), but both is nil')
  end
  return fqcn, inner_path
end

--
--
--
---@param outdir string
---@param fqcn string? fully qualifed classname
---@param inner_path? string inside project
function M.get_path2java(outdir, fqcn, inner_path)
  if fqcn then
    local ipath0 = fqcn:gsub("%.class", ''):gsub('%.', fs.path_sep) .. '.java'
    local path2java = fs.join_path(outdir, ipath0)
    local exists = fs.file_exists(path2java)
    log_debug("path based on fqcn: %s %s", exists, path2java)
    if exists then
      return path2java;
    end
  end
  if inner_path then
    local ipath0 = inner_path:gsub('%.class$', '.java')
    local path2java = fs.join_path(outdir, ipath0)
    log_debug("path based on inner_path: ", path2java)
    return path2java
  end

  return nil
end

return M

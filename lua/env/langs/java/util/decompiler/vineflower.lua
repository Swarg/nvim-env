-- 31-12-2024 @author Swarg
--
-- integration with Vineflower Decompiler
-- 1.10.1 (release 16 Apr 2024)
-- https://github.com/Vineflower/vineflower
-- (this is a renamed successor to the decompiler named quiltflower)
--
-- this decompiler require jdk11+
--
--

local log = require 'alogger'
local fs = require 'env.files'
local common = require 'env.langs.java.util.decompiler.common'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local join_path = fs.join_path
local log_debug = log.debug
local prepare_dependencies_libs = common.prepare_dependencies_libs
local get_java_executable = common.get_java_executable

-- bcm=1 - bytecode-source-mapper to work dump-code-lines(dcl)
M.DEFAULT_OPTS = '-dgs=1 -bsm=1 --__dump_original_lines__=1 -dcl=1'

--
--
--
---@param classfile string - absolute path to class file
---@param params table {java_home, decompiler_jar, outdir, libs, verbose, inner_path}
--
---@return string?
---@return string? errmsg
function M.decompile_file(classfile, params)
  log_debug("decompile_file", classfile, params)

  local java = get_java_executable(params)

  local decomp_opts = params.decomp_opts or M.DEFAULT_OPTS
  local decompiler_jar = assert(params.decompiler_jar, 'decompiler_jar')
  local inner_path = assert(params.inner_path, 'inner_path')
  local outdir = assert(params.outdir, 'outdir')
  local verbose = params.verbose

  local libs = prepare_dependencies_libs(params.libs)

  local cmd = fmt("%s -jar %s %s %s '%s' '%s' 2>&1",
    java, decompiler_jar, decomp_opts, libs, classfile, outdir)

  log_debug('command:', cmd)
  if verbose then print('[VERBOSE]', cmd) end
  local lines = fs.execrl(cmd)

  if verbose then for _, line in ipairs(lines) do print(line) end end
  -- debugging

  local classfile0 = nil
  for _, line in ipairs(lines or E) do
    if not classfile0 then
      classfile0 = match(line, 'Decompiling class%s+(.-)$')
    elseif match(line, "%.%.%. done$") then
      -- vineflower save output file without package subdirs
      local classname = fs.basename(classfile0)
      local decompiled_fn = join_path(outdir, v2s(classname) .. '.java')
      if verbose then
        print("[VERBOSE] has done! decompiled", decompiled_fn, classfile0)
      end

      return common.validated_path_to_decompiled_file(
        classfile, inner_path, outdir, decompiled_fn, lines)
    end
  end

  return nil, table.concat(lines, "\n")
end

-- todo figure out what is --file option. is for jar arvhive?
-- local cmd = fmt("%s -jar %s %s %s %s --file %s 2>&1",
--   java, decompiler_jar, decomp_opts, libs, classfile, outjavafile)

--[[
  -- pkg/some/Main.class -> pkg/some/Main.java
  local javafile = inner_path:gsub('%.class$', '.java')
  local outjavafile = join_path(outdir, javafile)
  local outdir_with_pkg = fs.get_parent_dirpath(outjavafile)
  if outdir_with_pkg then
    fs.mkdir(outdir_with_pkg)
  end

      local decompiled_file = outjavafile --join_path(outdir, v2s(classname) .. '.java')
]]


-- local name = nil
-- if classes[1] then
--   name = classes[1]
-- else
--   name = match(classfile, "[/\\]([^/\\]+).class$") -- aclassfile
-- end
--
--[[
Success:

INFO:  Loaded 3 plugins
INFO:  JVM info: VendorName, Inc. - 17.0.13 - 17.0.13+11-LTS
INFO:  Scanning classes from Java runtime current
INFO:  Scanning classes from module java.base@17.0.13
INFO:  Scanning classes from module java.compiler@17.0.13
....
INFO:  Scanning classes from module jdk.zipfs@17.0.13
INFO:  Scanning classes from file /tmp/jarviewer/pkg/some/Main.class
INFO:  Loading Class: pkg/some/Main from file /tmp/jarviewer/pkg/some/Main.class
INFO:  Loading Class: java/lang/invoke/MethodHandles$Lookup from module java.base@17.0.13
INFO:  Loading Class: java/lang/invoke/MethodHandles from module java.base@17.0.13
INFO:  Preprocessing class pkg/some/Main
INFO:     Loading Class: java/lang/Object from module java.base@17.0.13
INFO:  ... done
INFO:  Decompiling class pkg/some/Main
INFO:        Loading Class: java/nio/charset/StandardCharsets from module java.base@17.0.13
INFO:        Loading Class: java/lang/System from module java.base@17.0.13
INFO:        Loading Class: java/lang/String from module java.base@17.0.13
...
INFO:  ... done
]]


--[[
Example of the Error when use jdk8:

Error: A JNI error has occurred, please check your installation and try again
Exception in thread "main" java.lang.UnsupportedClassVersionError:
org/jetbrains/java/decompiler/main/decompiler/ConsoleDecompiler
has been compiled by a more recent version of the Java Runtime
(class file version 55.0),
this version of the Java Runtime only recognizes class file versions up to 52.0
]]

--[[


--show-hidden-statements=bool, -shs=bool
  Display hidden code blocks for debugging purposes.
  default: 0 (OFF)
]]
return M

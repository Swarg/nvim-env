-- 20-07-2024 @author Swarg
-- Goal: Run the Junit Tests in independed way

-- javac -d /absolute/path/for/compiled/classes \
-- -cp /absolute/path/to/junit-4.12.jar \
--    /absolute/path/to/TestClassName.java
--
-- Then run your test cases. For example:
--
-- java -cp /absolute/path/for/compiled/classes:\
-- /absolute/path/to/junit-4.12.jar:\
-- /absolute/path/to/hamcrest-core-1.3.jar \
-- org.junit.runner.JUnitCore \
-- your.package.TestClassName

local log = require 'alogger'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local find, gsub = string.find, string.gsub

local parse_open_xml_tag, add_child_to_node, add_text_to_node
local log_trace = log.trace

local xml_esq_map = {
  ['&lt;'] = '<',
  ['&gt;'] = '>',
  ['&amp;'] = '&',
  ['&apos;'] = "'",
  ['&quot;'] = '"',
  ['&#10;'] = "\n",
  ['&#9;'] = "\t",
}

--
---@param s string
---@return string
function M.normalized_xml_str(s)
  if not s or s == '' then return s end

  local res = gsub(s, "%&%l%l%l?%l?;", xml_esq_map)
  res = gsub(res, "%&%#%d%d?%d?;", xml_esq_map) -- #10 -> \n
  return res
end

-- <testsuite name="pkg.SomeTest" tests="2" skipped="0" failures="2" errors=""
--            timestamp="" hostnmae time=
--   <properties/>
--   <testcase name="test_2" classname="pkg.SomeTest" time=>
--     <failure messages>
--   </testcase>


---@param path string
---@return table?
---@return string?
function M.parse_xml_test_report(path)
  local h, err = io.open(path, 'rb')
  if not h then
    return nil, err
  end

  local lnum = 0
  local nested = {}
  local root_node = nil
  local c_node = nil -- current opened node
  -- local p_ind = ''
  local depth = 0
  local in_cdata = false -- <![CDATA[...]]>

  local function close_paired_tag()
    log_trace('close paired tag')
    nested[depth] = nil
    depth = depth - 1
    local parent_node = (c_node or E).parent
    if c_node then
      c_node.parent = nil
    end
    c_node = parent_node -- back to parent level
    in_cdata = false
  end


  while true do
    lnum = lnum + 1
    local line = h:read("*l")
    if not line then
      break
    end

    if in_cdata and c_node then
      local pline = line -- debugging
      local cde = find(line, ']]>')
      if cde then
        add_text_to_node(c_node, line:sub(1, cde - 1))
        in_cdata = false -- end of cdata block
        line = line:sub(cde + 3, #line)
      else
        add_text_to_node(c_node, line)
        line = ''
      end
      log_trace("after CDATA:|%s|", line, pline)
    end

    local node = parse_open_xml_tag(line)
    if node and node.tag then
      log_trace("depth:%s tag:%s %s ctx:%s | %s %s",
        depth, node.tag, (node.close and 'c' or 'o'), (c_node or E).tag, lnum, line)

      if node.close then                          -- case </tag> or <tag/>
        if c_node and node.tag == c_node.tag then -- pair case </tag>
          close_paired_tag()
        elseif c_node then                        -- case <tag/>
          add_child_to_node(c_node, node)         -- add as brother
          node.close = nil                        -- so as not to keep too much (e.g. <tag/>)
          node.parent = nil
        end
        log_trace('close tag', node.tag, ' by node.close')
        --
      else -- open new node
        depth = depth + 1
        nested[depth] = node.tag
        if c_node then
          add_child_to_node(c_node, node) -- add new opened node as nested child
        end

        if not root_node then
          root_node = node
        end

        c_node = node
        if c_node.text and c_node.text ~= '' then
          local text = c_node.text
          if #text >= 9 and text:sub(1, 9) == '<![CDATA[' then
            local ce = find(text, ']]>', 9, true) -- or #text
            in_cdata = true
            log_trace("found CDATA: ", c_node.tag, 'ce:', ce)
            if not ce then
              c_node.text = text:sub(10, #text)
            else
              c_node.text = text:sub(10, ce - 1)
              in_cdata = false
              text = text:sub(ce + 3)

              local j = find(text, '</' .. c_node.tag .. '>', 1, true) -- paired close tag
              if j then
                close_paired_tag()
              end
            end
          end
        end
      end
    elseif c_node then
      if #line > 0 then
        -- case <system-err><![CDATA[]]></system-err>
        local close = false
        local j = find(line, '</' .. c_node.tag .. '>') -- paired close tag
        if j then
          close = true
          local text = line:sub(1, j)
          if text == '<![CDATA[]]>' then
            text = ""
          end
          line = text
        end
        add_text_to_node(c_node, line)
        if close then
          close_paired_tag() -- c_node
        end
      end
    end
  end

  h:close()
  nested = nested
  return root_node
end

--

---@param parent table
---@param child table
function add_child_to_node(parent, child)
  parent.childs = parent.childs or {}
  parent.childs[#parent.childs + 1] = child
  child.parent = parent
end

---@param node table{text:table}
---@param text string
function add_text_to_node(node, text)
  if text then
    local typ = type(node.text)
    if typ == 'string' then
      node.text = { node.text }
    end
    node.text = node.text or {}
    table.insert(node.text, text)
  end
end

-- simplified parser,
-- it is considered that there can only be one tag on one line
---@return table?{tag, ind, attrs}
function parse_open_xml_tag(s)
  local patt = '^(%s*)<%s*([%w%-_]+)%s*(.-)%s*(/?)%s*>%s*(.-)%s*$'
  local ind, tag, s_attrs, close, inner_text = match(s, patt)
  if not tag or tag == '' then
    ind, tag = match(s, '^(%s*)</%s*([%w%-_]+)%s*>%s*$') -- </tag>
    if tag then
      return { tag = tag, ind = ind, close = true }      -- paired close tag
    end
    return nil
  end

  local node = {
    tag = tag,
    -- ind = (ind ~= '') and ind or nil,
    attrs = nil,
    text = nil,
    close = (close == '/') and true or nil,
  }
  if inner_text ~= '' and not node.close then
    node.text = inner_text
  end

  -- testsuite name="org.common.SomeTest" tests="2" message="...&lt;1&gt;"
  if s_attrs and s_attrs ~= '' then
    local limit = 1024
    local p = 1
    local sub = string.sub
    local key = nil -- open
    local val_s = nil

    while p < #s_attrs do
      local i = find(s_attrs, '["%s]', p, false)
      if not i then
        break
      end
      local c = sub(s_attrs, i, i)
      if c == '"' then
        if not key then                -- open
          key = sub(s_attrs, p, i - 2) -- ="
          val_s = i + 1
        elseif val_s then              -- close
          -- todo escape  \"
          local val = sub(s_attrs, val_s, i - 1)
          node.attrs = node.attrs or {}
          node.attrs[key] = val
          key, val_s = nil, nil
        end
      end

      limit = limit - 1; if limit < 0 then break end
      p = i + 1
    end
  end

  return node
end

---param s string?
---return boolean
-- local function is_trace_line(s)
--   return s ~= nil and match(s, '^%s*at .*$') ~= nil
--     -- find(s, '%gt;', 1, true) == nil
--     --   and find(s, '&quot;', 1, true) == nil
-- end

-- to skip message with escaped multiline-possible values
--
-- in this point lines has xml escaped < > chars: &lt; &gt;
---- expected: <...> but was: <..>
function M.get_lt_gt_balance(s, balance)
  balance = balance or 0
  if s and #s > 0 then
    local p = 1
    while p < #s do
      local i, j, val = find(s, '&(%l%l);', p, false) -- to find &lt; &gt;
      if val == 'lt' then
        balance = balance + 1
      elseif val == 'gt' then
        balance = balance - 1
      end
      if not i then
        break
      end
      p = j + 1
    end
  end
  return balance
end

--
-- shorten the stacktrace so that only what is important for a quick jump remains
-- make full stacktrace from (testsuite.testcase.failure) more readable
--
-- remove from the beginning and from the end traces that
-- are not important, leaving only those where the location of the error is shown
--
---@param failure_node table{text}
---@param testcase_node table{attrs}
---@return table
---@return number lnum
---@return string? test_file
function M.get_meaningful_stacktrace(failure_node, testcase_node)
  log_trace("get_meaningful_stacktrace")
  if type(failure_node.text) ~= 'table' then
    return { v2s(failure_node.text) }, -1, nil
  end
  local failure_lines = failure_node.text

  local attrs = assert(testcase_node.attrs, 'testcase_node.attrs')

  local failure_lnum = nil -- line number where the test failed
  local test_file = nil    -- SomeClassTest.java

  -- <testcase name="test_2" classname="org.common.SomeTest" time="0.007">
  local interest_line = v2s(attrs.classname) .. '.' -- .. v2s(attrs.name)
  -- attrs.name is a display test name or the method name of this test method
  local lnum_start, lnum_end = nil, 0
  local org_junit_Assert_top_lnum = nil

  -- to skip message line with multiline strings with possible familiar stacktrace
  local ltgt_balance = M.get_lt_gt_balance(failure_lines[1], 0)
  local first_trace_line = 2
  if ltgt_balance ~= 0 then
    while first_trace_line < #failure_lines do
      if M.get_lt_gt_balance(failure_lines[first_trace_line], ltgt_balance) == 0
      then
        first_trace_line = first_trace_line + 1
        break
      end
      first_trace_line = first_trace_line + 1
    end
  end

  for n = first_trace_line, #failure_lines do
    local line = failure_lines[n]
    if not lnum_start then
      if line and find(line, interest_line, 1, true) then -- and is_trace_line(line) then
        local filename, s_lnum = match(line, '%s*at.-%s*%(([^:]+):(%d+)%)%s*$')
        failure_lnum = tonumber(s_lnum)
        test_file = filename
        lnum_end = n + 1
        org_junit_Assert_top_lnum = org_junit_Assert_top_lnum or 2 -- n - 1
        log_trace("failure_lnum:%s fn:%s:%s", lnum_end, filename, failure_lnum)
        break
      else
        -- org.junit.Assert. or org.junit.jupiter.api.Assertions.a
        local p = find(line, 'org%.junit%..-Assert', 1, false)
        if p then
          org_junit_Assert_top_lnum = n
        end
      end
    end
  end
  lnum_start = org_junit_Assert_top_lnum or math.huge

  local t = {}
  if org_junit_Assert_top_lnum then
    t[#t + 1] = "..." -- mean reduce org.junit.Assert lines in top of the trace
  end

  for i = lnum_start, lnum_end do
    t[#t + 1] = failure_lines[i]
  end
  t[#t + 1] = "..."

  -- add causes from botton of the trace
  for i = lnum_end, #failure_lines do
    local line = failure_lines[i]
    if line and match(line, '^Caused by: ') then
      for k = i, #failure_lines do
        t[#t + 1] = failure_lines[k]
      end
      break
    end
  end

  return t, (failure_lnum or 0), test_file
end

--

if _G.TEST then
  M.test = {
    parse_open_xml_tag = parse_open_xml_tag
  }
end

return M

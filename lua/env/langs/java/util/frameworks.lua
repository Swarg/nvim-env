-- 05-09-2024 @author Swarg
-- Goals:
--  - resolve patters for GotoDefinition from the logs

local log = require 'alogger'
local spring_resolver = require 'env.langs.java.util.spring.resolver'

local M = {}

M.GROUP = {
  JUNIT_4 = 'junit',
  JUNIT_5 = 'org.junit.jupiter',
}


---@param line string
---@param project_root string
---@return string?
---@return string?
function M.resolveSourceFrom(line, project_root)
  log.debug("resolveSourceFrom |%s|", line)
  local path, jumpto = spring_resolver.resolveSourceFrom(line, project_root)
  -- todo another fw

  return path, jumpto
end

return M

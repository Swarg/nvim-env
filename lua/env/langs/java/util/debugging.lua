-- 27-07-2024 @author Swarg
local M = {}

local log = require 'alogger'
local Editor = require 'env.ui.Editor'
local udap = require 'env.bridges.dap'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local log_debug = log.debug

local function notify(msg)
  ---@diagnostic disable-next-line: param-type-mismatch
  Editor.echoInStatus(nil, msg, nil)
end

function M.is_valid_port(port)
  return type(port) == "number" and port > 0 and port < 65535
end

---@param host string?
---@param port number?
---@return table
function M.get_dap_launch_settings(host, port)
  return { -- from dap.configuration.java
    name = "Attach",
    type = 'java',
    request = 'attach',
    hostName = host or "127.0.0.1",
    port = port or 5005,
    -- cwd
  }
end

---@param dap_settings table - dap_launch_settings
function M.get_readable_dap_host_port(dap_settings)
  return fmt("%s:%s", v2s(dap_settings.hostName), v2s(dap_settings.port))
end

-- parse user input to update host and port in dap_launch_settings
---@param dap_launch_settings table
---@param user_input string host:port
---@param default string  host:port
function M.update_dap_settings(dap_launch_settings, user_input, default)
  if user_input ~= default then
    local host, port = string.match(user_input, "^([^:]+):(%d+)$")
    if host == nil and port == nil and tonumber(user_input) ~= nil then
      host = dap_launch_settings.hostName
      port = user_input
    end
    if not host or tonumber(port) == nil then
      return false, 'No cannot parse input to host and port: "' .. user_input .. '"'
    end
    port = tonumber(port)
    if not M.is_valid_port(port) then
      return false, "Invalid port: " .. v2s(port)
    end
    dap_launch_settings.hostName = host
    dap_launch_settings.port = port
  end
  return true
end

-- Just attach to already started external debugger
-- TODO ability to define hostName and port
-- Behavior:
--  - Work only with already runned debugger, not try to start new one.
--  - After debugging session termination - the external debugger
--    continues its work and the application remains running
function M.attach_to_debugger(ci, opts)
  log_debug("attach_to_debugger", ci)
  opts = opts or {}
  local dap_opts = {}
  local dap_launch_settings = M.get_dap_launch_settings(opts.host, opts.port)
  local output, defhp = nil, M.get_readable_dap_host_port(dap_launch_settings)

  -- ask value from user
  vim.ui.input({ prompt = "Attach to Debugger: ", default = defhp }, function(input)
    output = input
  end)
  -- process user output
  if output == nil or output == 'q' or output == '' then
    notify("canceled")
    return
  end

  local ok, errmsg = M.update_dap_settings(dap_launch_settings, output, defhp)
  if not ok then
    return false, errmsg
  end

  if not udap.has_valid_adapter(dap_launch_settings) then
    return false, 'No Valid Adapter for java'
  end
  local addr = M.get_readable_dap_host_port(dap_launch_settings)

  udap.subscribe_initalized(function(sess, _) -- session, body
    notify(fmt('Attached to Debugger (%s)', addr, v2s(sess.initialized)))
  end)

  udap.subscribe_onclose_debug_session(function()
    -- in Attach mode only show in status, without closing the program being debugged
    notify(fmt('Deattached from Debugger (%s)', addr))
  end, {})

  notify('Attach to Debugger...')
  udap.run(dap_launch_settings, dap_opts)
end

return M

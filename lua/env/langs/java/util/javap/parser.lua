-- 02-01-2025 @author Swarg
--
-- parse output from javap -l path/to/MyClass.class
--
-- This can be used, for example, to clarify the mapping of source files
-- decompiled by other decompilers.


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
--
--
local function split(s, out)
  out = out or {}

  local function add(pstart, pend)
    local sub = s:sub(pstart, pend)
    if #sub > 0 then
      out[#out + 1] = sub
    end
  end

  local p = 1
  while true do
    local i = string.find(s, '[%s%(%),]', p)
    if not i then
      i = #s
      break
    end
    local c = s:sub(i, i)
    add(p, i - 1)
    if c == '(' or c == ')' then
      out[#out + 1] = c
    end
    p = i + 1
  end
  add(p, #s + 1)
  return out
end

local function tbl_clear(t)
  local sz = #t
  for i = 1, sz, 1 do
    t[i] = nil
  end
end

--
M.VISIBILITY_MOD = {
  public = true,
  private = true,
  protected = true,
  -- package? default
}
local is_constructor_def_line
local get_constructor_visibility
local get_constructor_params
local parse_constructor_def
local is_method_def_line
local parse_method_def
local parse_static_block_def

--
-- parse output from `javap -l my.class`
--
---@param lines table
---@return table?
---@return string? errmsg
function M.parseLineNumberTables(lines)
  if type(lines) ~= 'table' or #lines < 4 then
    return nil, 'no input'
  end

  local classname = nil
  local tmp = {}
  split(lines[2], tmp)
  local sz = #tmp
  if sz >= 3 and tmp[sz] == '{' then
    classname = tmp[sz - 1]
  end
  if not classname then
    return nil, 'not found classname'
  end

  local t = {
    compiled = match(lines[1], '^%s*Compiled from "(.-)"%s*$'),
    classname = classname,
    constructors = {},
    methods = {},
    static = nil, -- static block to init class statick fields
  }

  -- for i = 3, #lines, 1 do
  local i = 2;
  while i < #lines do
    i = i + 1
    local line = lines[i]
    if line == '}' then -- end of classdefinition
      break
    end
    -- print('[DEBUG]', i, line)
    tbl_clear(tmp)
    split(line, tmp)

    if is_constructor_def_line(classname, tmp) then
      i = parse_constructor_def(lines, i, t, tmp)
    elseif is_method_def_line(tmp) then
      i = parse_method_def(lines, i, t, tmp)
    elseif tmp and tmp[1] == 'static' then
      i = parse_static_block_def(lines, i, t)
    end
  end

  return t
end

-- line 6: 0
---@return number?
---@return number?
function M.parse_line_table_entry(s)
  if s then
    local d1, d2 = match(s, '^%s*line%s+(%d+):%s+(%d+)%s*')
    if d1 and d2 then
      return tonumber(d1), tonumber(d2)
    end
  end
  return nil, nil
end

---@param classname string
---@param t table
---@return boolean
function is_constructor_def_line(classname, t)
  return type(t) == 'table' and classname ~= nil and
      (t[1] == classname and t[2] == '(' or
        t[2] == classname and t[3] == '(' and M.VISIBILITY_MOD[t[1]])
end

--
-- public private protected (package)
-- nil is default (package) visibility modif
--
---@return string?
function get_constructor_visibility(classname, t)
  return t[1] == classname and nil or t[1]
end

-- (int, int)
function get_constructor_params(classname, t)
  local offset = nil
  if t[1] == classname and t[2] == '(' then
    offset = 3
  elseif t[2] == classname and t[3] == '(' then
    offset = 4
  else
    error('expected constructor definition got:' .. table.concat(t, ' '))
  end
  local params = {}
  for i = offset, #t do
    local param = t[i]
    if param == ')' then
      break
    end
    params[#params + 1] = param
  end
  return params
end

---@return number
local function get_first_linenum(lines, i)
  local next_line = lines[i + 1]
  if not match(next_line, '%s*LineNumberTable:%s*') then
    error('expected LineNumberTable got:' .. v2s(next_line))
  end
  next_line = lines[i + 2]
  local lnum_start, _ = M.parse_line_table_entry(next_line)
  if not lnum_start then
    error('expected linenumber in line:' .. tostring(i + 2) ..
      ' got:' .. v2s(next_line))
  end
  return lnum_start
end

local function process_bytecode_line_mapping(lines, i, out)
  local lnum, bytecode_lnum = nil, nil
  local sz = #lines
  while i < sz and lines[i + 1] ~= '' do
    lnum, bytecode_lnum = M.parse_line_table_entry(lines[i])
    if out and lnum then
      out[lnum] = bytecode_lnum
    end
    i = i + 1
  end
  return i, lnum
end

--
-- processing lines with the body of the constructor
--
---@return number
function parse_constructor_def(lines, i, t, tmp)
  local classname = t.classname
  local lnum, lnum_start = nil, get_first_linenum(lines, i)
  i, lnum = process_bytecode_line_mapping(lines, i + 2, nil)
  lnum = lnum or lnum_start

  t.constructors[#t.constructors + 1] = {
    visibility = get_constructor_visibility(classname, tmp),
    params = get_constructor_params(classname, tmp),
    lnum = lnum_start,
    lnum_end = lnum,
  }

  return i
end

--

-- public static int[] getSizes();
---@param t table
function is_method_def_line(t)
  local sz = #(t or E)
  if sz > 4 and t[sz] == ';' and t[sz - 1] == ')' then
    return true
  end
  return false
end

local function indexof(t, find)
  for i = 1, #t do
    if t[i] == find then
      return i
    end
  end
  return -1
end

---@return number
function parse_method_def(lines, i, t, tmp)
  -- local classname = t.classname
  local visibility = tmp[1]

  if not M.VISIBILITY_MOD[visibility] then
    visibility = nil -- default(package visibility)
  end
  local idx_open_bracket = indexof(tmp, '(')
  if idx_open_bracket < 2 then
    error('expected valid method definition got:' .. table.concat(tmp, ' '))
  end

  local params = {}
  local name = tmp[idx_open_bracket - 1]
  for n = idx_open_bracket + 1, #tmp do
    local param = tmp[n]
    if param == ')' then
      break
    end
    params[#params + 1] = param
  end


  local lnum, lnum_start = nil, get_first_linenum(lines, i)
  i, lnum = process_bytecode_line_mapping(lines, i + 2, nil)
  lnum = lnum or lnum_start

  t.methods[#t.methods + 1] = {
    visibility = visibility,
    ret_value = nil,
    mods = nil,  -- static
    name = name, -- method name
    params = params,
    lnum = lnum_start,
    lnum_end = lnum,
  }
  return i
end

---@return number
function parse_static_block_def(lines, i, t)
  local lnum, lnum_start = nil, get_first_linenum(lines, i)
  i, lnum = process_bytecode_line_mapping(lines, i + 2, nil)

  t.static = {
    lnum = lnum_start,
    lnum_end = lnum or lnum_start,
  }
  return i
end

if _G.TEST then
  M.split = split
  M.is_constructor_def_line = is_constructor_def_line
  M.get_constructor_params = get_constructor_params
  M.is_method_def_line = is_method_def_line
end

return M

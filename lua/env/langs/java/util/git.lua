-- 05-10-2024 @author Swarg
-- https://github.com/github/gitignore/blob/main/Java.gitignore
-- https://github.com/github/gitignore/blob/main/Maven.gitignore
-- https://github.com/github/gitignore/blob/main/Global/Vim.gitignore
--
local M = {}

local templ_gitignore = [[
## Java
.mtj.tmp/
*.class
*.jar
*.war
*.ear
*.nar

# virtual machine crash logs, see http://www.java.com/en/download/help/error_hotspot.xml
hs_err_pid*
replay_pid*

## Maven
target/
!**/src/main/**/target/
!**/src/test/**/target/
pom.xml.tag
pom.xml.releaseBackup
pom.xml.versionsBackup
pom.xml.next
release.properties
dependency-reduced-pom.xml
buildNumber.properties
.mvn/timing.properties
# https://github.com/takari/maven-wrapper#usage-without-binary-jar
.mvn/wrapper/maven-wrapper.jar

# Eclipse m2e generated files
# Eclipse Core
.project
# JDT-specific (Eclipse Java Development Tools)
.classpath


## Gradle
bin/
build/
.gradle
.gradletasknamecache
gradle-app.setting
!gradle-wrapper.jar


## IntelliJ IDEA
out/
.idea/
.idea_modules/
*.iml
*.ipr
*.iws


## STS
.apt_generated
.classpath
.factorypath
.project
.settings
.springBeans
.sts4-cache


## Eclipse
.settings/
bin/
tmp/
.metadata
.classpath
.project
*.tmp
*.bak
*.swp
*~.nib
local.properties
.loadpath
.factorypath


## NetBeans
/nbproject/private/
/nbbuild/
/dist/
/nbdist/
/.nb-gradle/
build/
!**/src/main/**/build/
!**/src/test/**/build/
nbactions.xml
nb-configuration.xml


## VS Code
.vscode/
uploads/
.code-workspace


## Operating System Files
*.DS_Store
Thumbs.db
*.sw?
]]


-- # Secrets .env


function M.gen_gitignore_body(opts)
  opts = opts or {}
  local s = templ_gitignore
  return s
end

return M

-- 01-01-2025 @author Swarg
--
-- jvm researcher(javaagent)
--  - get bytecode from running jvm
--
--
-- WARNING from jdk21:
--
-- A Java agent has been loaded dynamically (path/to/agent.jar)
-- If a serviceability tool is in use, please run with -XX:+EnableDynamicAgentLoading to hide this warning
-- If a serviceability tool is not in use, please run with -Djdk.instrument.traceUsage for more information
-- Dynamic loading of agents will be disallowed by default in a future release
--

local log = require 'alogger'
local fs = require 'env.files'
local jvm = require 'env.langs.java.util.jvm'
local core = require 'env.langs.java.util.core'

local M = {}

local home = os.getenv('HOME')
-- local java_home = os.getenv('JAVA_HOME')

M.JAVA = "java"
M.DEFAULT_JVM_VERSION = 21

M.DECOMP_DIR = home .. "/tmp-agent/decomp/"

M.AGENTS = {
  [8] = {
    agent = home .. "/tmp-agent/ragent.jar",
  },
  [21] = {
    agent = home .. "/tmp-agent/ragent.jar", -- research-agent
  }
}

--
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local file_exists = fs.file_exists
local isFullClassNameWithPackage = core.isFullClassNameWithPackage

-- testing helper
---@param prefix string?
function M.change_home(prefix)
  prefix = prefix or '~'
  for _, e in pairs(M.AGENTS) do
    e.agent = fs.join_path(prefix, string.sub(v2s(e.agent), #home + 2))
  end
end

--
--
--
---@param agentJarLoader string
---@param agentJarToLoad string? self
---@param agentCmd string
---@param pid number
---@param opts table?{binjava}
---@return table?
function M.run_agent(agentJarLoader, agentJarToLoad, agentCmd, pid, opts)
  log_debug("run_agent", agentJarLoader, agentJarToLoad, agentCmd, pid)
  local java = (opts or E).binjava or M.JAVA
  agentJarToLoad = agentJarToLoad or 'self'

  local cmd = fmt("%s -jar %s attach-javaagent %s %s -c '%s'",
    java, agentJarLoader, v2s(pid), agentJarToLoad, agentCmd)

  log_debug("run_agent cmd:", cmd)
  if (opts or E).dry_run then return { cmd } end

  return fs.execrl(cmd) -- table
end

--
-- same as get_agent_for_jvm but with pid validate
--
---@param pid number         only to validate for caller
---@return string? path to javaagent
---@return string? err
---@return number? jvm_version
function M.get_agent_for_jvm_instance(binjava, pid)
  log_debug("get_agent_for_jvm_instance bin:%s pid:%s", binjava, pid)
  if type(pid) ~= 'number' or pid < 0 then
    return nil, 'expected valid pid of jvm got: ' .. v2s(pid)
  end

  local agentjar, err, jvm_version = M.get_agent_for_jvm(binjava)
  if not file_exists(agentjar) then
    return nil, 'not found agentjar file: ' .. v2s(agentjar)
  end

  return agentjar, err, jvm_version
end

--
-- get full path to javaagent based on version of the java(jdk,jre) determined
-- from the binjava
-- with pid of jvm validation
--
---@param binjava string
---@return string? path to javaagent
---@return string? err
---@return number? jvm_version
function M.get_agent_for_jvm(binjava)
  local jvm_version = jvm.get_java_version(binjava)
  log_debug("path:%s -> version:%s", binjava, jvm_version)
  if not jvm_version then
    return nil, 'cannot determine jvm version for executable: ' .. v2s(binjava)
  end

  local agents = M.AGENTS[jvm_version] or M.AGENTS[M.DEFAULT_JVM_VERSION]
  local agentjar = agents.agent
  log_debug('jvm_version:%s agentjar:%s', jvm_version, agentjar)
  if not agentjar then
    return nil, 'not found agentjar for jvm_version: ' .. v2s(jvm_version)
  end

  return agentjar, nil, jvm_version
end

--
-- find path to the agent logfile (with report)
--
---@param lines table?
---@return string? path to agent report file
---@return string? error e.q. case "no such process"
function M.find_agent_logfile(lines)
  local logfile
  for _, line in ipairs(lines or E) do
    logfile = match(line, "^%s*See output results in the file:%s*(.-)%s*$")
    if logfile then
      return logfile
    end
    if string.find(line, "java.io.IOException: No such process") ~= nil then
      return nil, "No such process"
    end
  end
  return nil
end

--
--
--
---@param binjava string
---@param pid number
---@param opts table
---@return string? path to report
---@return string? err
---@return table? message(lines)
function M.get_all_loaded_classes_from_jvm(binjava, pid, opts)
  log_debug("get_all_loaded_classes_from_jvm")
  opts = opts or {}
  local agentjar, err, _ = M.get_agent_for_jvm_instance(binjava, pid)
  if not agentjar then return nil, err end

  local agentCmd = "class all-loaded"
  local pkg = opts['package']
  if pkg and pkg ~= '' then
    agentCmd = agentCmd .. " --package " .. pkg
  end
  opts.binjava = binjava
  local lines = M.run_agent(agentjar, 'self', agentCmd, pid, opts)
  local logfile, err0 = M.find_agent_logfile(lines)

  return logfile, err0, lines
end

--
-- Location: path/to/some.jar!/pkg/to/some.class
--
-- useful when you need to find out where a class is loaded from which external
-- Jar file.
--
---@param binjava string
---@param pid number
---@param classname string
---@return string? path
---@return string? errmsg
---@return table?  lines with message from the AgentLoader
function M.get_jvm_classinfo(binjava, pid, classname, opts)
  if not isFullClassNameWithPackage(classname) then
    return nil, 'invalid classname: ' .. v2s(classname)
  end
  opts = opts or {}
  local agentjar, err, jvm_version = M.get_agent_for_jvm_instance(binjava, pid)
  if not agentjar then
    return nil, err
  end
  log_debug('jvm_version:', jvm_version)
  local agentCmd = "class info " .. classname
  opts.binjava = binjava
  local lines = M.run_agent(agentjar, 'self', agentCmd, pid)
  local logfile, err0 = M.find_agent_logfile(lines)

  return logfile, err0, lines
end

--
-- TODO: Not implemented yet!
--
-- fetch the class bytecode from live jvm and save to file on the disk
--
---@param binjava string
---@param pid number
---@param classname string
---@param opts table
---@return string?  path to agent logfile
---@return string?  errmsg
---@return table?   response lines from AgentLoader
function M.get_bytecode_from_jvm(binjava, pid, classname, opts)
  log_debug("get_bytecode_from_jvm", binjava, pid, classname)
  if not isFullClassNameWithPackage(classname) then
    return nil, 'invalid classname: ' .. v2s(classname)
  end
  local agentjar, err, jvm_version = M.get_agent_for_jvm_instance(binjava, pid)
  if not agentjar then
    return nil, err
  end
  jvm_version = jvm_version -- todo
  opts = opts or {}
  opts.binjava = binjava

  local agentCmd = "bytecode " .. v2s(classname)
  local lines = M.run_agent(agentjar, 'self', agentCmd, pid, opts)
  local logfile, err0 = M.find_agent_logfile(lines)
  return logfile, err0, lines
end

--
-- parse ResearchAgent output(report logfile)
-- to fetch path to saved decompiled file
--
---@param logfile string
---@return string? path to output(decompiled file)
---@return string? errmsg
function M.report_get_saved_class_file(logfile)
  local lines = fs.read_lines(logfile)
  for _, line in ipairs(lines) do
    local errmsg = match(line, '^%[ERROR%]%s*(.-)$')
    if errmsg then
      return nil, errmsg
    end
    local output = match(line, "^Saved:%s*(.-%.class)%s*$")
    if output then
      return output
    end
  end
  return nil, 'not found output file in the agent report'
end

--
-- transform already existed or define new class in already runned jvm
--
---@param binjava string
---@param pid number
---@param path string
---@param opts table?
function M.update_bytecode_in_jvm(binjava, pid, classname, path, opts)
  log_debug("update_bytecode_in_jvm", binjava, pid, classname, path)
  if not isFullClassNameWithPackage(classname) then
    return nil, 'invalid classname: ' .. v2s(classname)
  end
  local agentjar, err, jvm_version = M.get_agent_for_jvm_instance(binjava, pid)
  if not agentjar then
    return nil, err
  end
  jvm_version = jvm_version -- todo
  opts = opts or {}
  opts.binjava = binjava

  local agentCmd = "hotswap " .. v2s(classname) .. ' ' .. v2s(path)
  local lines = M.run_agent(agentjar, 'self', agentCmd, pid, opts)
  local logfile, err0 = M.find_agent_logfile(lines)
  return logfile, err0, lines
end

return M

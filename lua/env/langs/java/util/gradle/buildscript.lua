-- 24-01-2025 @author Swarg
-- build.gradle parser manager
-- https://docs.gradle.org/current/userguide/writing_build_scripts.html#N10B52
-- https://docs.groovy-lang.org/docs/latest/html/documentation/core-domain-specific-languages.html

local log = require 'alogger'
local su = require 'env.sutil'

local frameworks = require 'env.langs.java.util.frameworks'
local ugroovy_dsl = require 'env.langs.java.util.gradle.groovy_dsl_syntax'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

local JUNIT_4_GROUP = frameworks.GROUP.JUNIT_4
local JUNIT_5_GROUP = frameworks.GROUP.JUNIT_5

---@param body string
---@param dsl string? groovy is default
---@return false|table
---@return string? errmsg
function M.parse(body, dsl)
  log_debug("parse", dsl)
  if not dsl or dsl == 'groovy' then
    return M.parse_build_gradle(body)
  elseif dsl == 'kotlin' then
    return M.parse_build_gradle_kts(body)
  end
  return false, 'unknown dsl: ' .. tostring(dsl)
end

local detect_test_framework

--
-- groovy dsl
--
---@return false|table
---@return string? errmsg
function M.parse_build_gradle(body)
  local t = ugroovy_dsl.parse_dsl(body)

  local deps = (t.dependencies or E) -- .testImplementation or E
  local test_deps = deps.testImplementation or E
  local cdeps = deps.implementation or E
  local tfn = detect_test_framework(test_deps) or detect_test_framework(cdeps)
  -- todo checks assertj, mockito, etc

  t.testframework = {
    executable = 'gradle',
    name = tfn
  }

  log_debug("parse_build_gradle testfw: %s", t.testframework.name)
  return t
end

--
-- kotlin dsl
--
---@return false|table
---@return string? errmsg
function M.parse_build_gradle_kts(body)
  print('[WARN] parsing build.gradle.kts - Not implemented yet')
  body = body

  local t = {
    plugins = {},
    dependencies = {},

    testframework = {
      executable = 'gradle',
      name = 'junit5', -- defualt
    }
  }

  return t
end

function detect_test_framework(test_deps)
  for _, line in ipairs(test_deps) do
    if su.starts_with(line, JUNIT_5_GROUP) then
      return 'junit5'
    elseif su.starts_with(line, JUNIT_4_GROUP) then
      return 'junit4'
    end
  end
  return nil
end

return M

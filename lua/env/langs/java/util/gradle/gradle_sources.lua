-- 23-02-2025 @author Swarg

local log = require 'alogger'
local fs = require 'env.files'

local DISTS_DIR = fs.join_path(os.getenv('HOME'), '.gradle', 'wrapper', 'dists')

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local join_path, file_exists = fs.join_path, fs.file_exists


local M = {}

-- cache of mappings inner-pkg-path (without ClassName) to plugin-dir-name
-- like:
-- org/gradle/api/tasks/testing/ ->
--              {'testing-base', 'testing-base-infrastructure', 'testing-jvm' }
-- Note: one package can be in several plugins-dirs
local plugin_dirs_cache = {}

M.cached_version_of_gradle_wrapper_distr = nil
M.cached_root_dir_of_distr_with_sources = nil


--

-- ~/.gradle/wrapper/dists/gradle-8.9-all/6m0mbzute7p0zdleavqlib88a/gradle-8.9/src/...
---@return string? version
---@return string? src-root-dir
---@return string? plugin-dir
function M.parse_path_to_gradle_source_file(path)
  local ver = match(path or '', "%.gradle.wrapper.dists.gradle%-(%d+%.%d+)%-")
  log_debug("wrapper distr version: ", ver)
  if not ver then
    return nil, nil, nil
  end

  local i = string.find(path, fs.path_sep .. "src" .. fs.path_sep)
  if not i then
    return nil
  end
  local src_root = path:sub(1, i + 4)
  local j = string.find(path, fs.path_sep, i + 6, true)
  if not j then
    return nil
  end

  local plugin_dir = path:sub(i + 5, j - 1)
  return ver, src_root, plugin_dir
end

---@param jump_path string absolute path for current opened file (bufname)
---@param fqcn string
---@return string? absolute path to given fqcn
---@return string? errmsg
function M.findPathToSourceOfClass(jump_path, fqcn)
  log_debug("findPathToSourceOfClass", fqcn, jump_path)
  local _, src_root, plugin_dir = M.parse_path_to_gradle_source_file(jump_path)
  if not src_root or not plugin_dir then
    return nil, 'expected path to the source file of gradle'
  end
  return M.findPathToSourceOfClass0(fqcn, src_root, plugin_dir)
end

--
-- find source dir of the installed into system a gradle
-- expected what you have installed "all" distr (not a "bin") into your os
-- into ~/.gradle/wrapper/dists/gradle-{VERSION}-all
--
---@return string? path to src root of the dir with gradle sources
---@return string? errmsg
function M.find_root_dir_of_gradle_sources(version)
  if not version then
    return nil, 'no version'
  end
  local gradle_dist = join_path(DISTS_DIR, 'gradle-' .. v2s(version) .. '-all')
  local dirs = fs.list_of_dirs(gradle_dist)
  if not dirs then
    return nil, 'not found "all"-distr for gradle version: "' .. v2s(version) .. '"'
  end
  local srcdir = join_path(gradle_dist, dirs[1], 'gradle-' .. v2s(version), 'src')
  return srcdir .. fs.path_sep
end

--
---@param versions table?
---@return string? version
---@return string? errmsg
function M.find_gradle_version_of_distr_with_sources(versions)
  log_debug("find_gradle_version_of_distr_with_sources", DISTS_DIR)
  local dirs = fs.list_of_dirs(DISTS_DIR)
  if not dirs then
    return nil, 'not found any wrapper distrs in' .. v2s(DISTS_DIR)
  end
  local max_major, max_minor, max_patch = 0, 0, 0
  local max_ver

  for _, dir in ipairs(dirs) do
    local version = match(dir, '^gradle%-([%d%.]+)%-all$')
    local major, minor, patch = M.parse_gradle_version(version)
    if major and minor then
      if not patch and major >= max_major and minor > max_minor then
        max_major, max_minor, max_patch, max_ver = major, minor, 0, version
      elseif patch ~= nil and major == max_major and minor == max_minor and
          patch > max_patch then
        max_major, max_minor, max_patch, max_ver = major, minor, patch, version
      end
      if versions then
        versions[#versions + 1] = version
      end
    end
  end
  if max_major == 0 then
    return nil, 'not found wrapper "all"-distr'
  end

  return max_ver
end

---@param ver string "8.9" or "8.12.1"
---@return number? major
---@return number? minor
---@return number? patch
function M.parse_gradle_version(ver)
  if not ver or ver == '' then
    return nil
  end
  local vmajor, vminor, vpatch = nil, nil, nil

  vmajor, vminor = match(ver, '^(%d+)%.(%d+)$')
  if not vmajor or not vminor then
    vmajor, vminor, vpatch = match(ver, '^(%d+)%.(%d+)%.(%d+)$')
  end

  return tonumber(vmajor), tonumber(vminor), tonumber(vpatch)
end

--
--
---@param fqcn string
---@param src_root string
---@param plugin_dir string?
function M.findPathToSourceOfClass0(fqcn, src_root, plugin_dir)
  log_debug("findPathToSourceOfClass0", fqcn, src_root, plugin_dir)
  if not src_root or not fqcn then
    return nil
  end

  local inner_path = fqcn:gsub('%.', fs.path_sep) .. '.java'

  local definition_path = M.find_path_in_cache(src_root, inner_path)
  if definition_path then -- ^ plugin_dir is found in the cache
    return definition_path
  end

  if plugin_dir then
    definition_path = join_path(src_root, plugin_dir, inner_path)
    if fs.file_exists(definition_path) then
      log_debug("found in same plugin_dir")
      M.put_plugin_dir_to_cache(inner_path, plugin_dir)
      return definition_path
    end
  end

  -- find in all another plugins subdirs
  local dirs = fs.list_of_dirs(src_root)
  if not dirs or #dirs == 0 then
    return nil
  end

  log_debug("find plugin_dir in fs dirs:", #dirs)

  for _, plugin_dir0 in ipairs(dirs) do
    -- print("## debug dir: ", dir)
    if plugin_dir0 ~= plugin_dir then -- to skip already checked
      definition_path = join_path(src_root, plugin_dir0, inner_path)
      if fs.file_exists(definition_path) then
        -- caching
        M.put_plugin_dir_to_cache(inner_path, plugin_dir0)
        return definition_path
      end
    end
  end

  return nil -- not found
end

--
--
---@param inner_path string
---@param plugin_dir string
function M.put_plugin_dir_to_cache(inner_path, plugin_dir)
  if not inner_path or not plugin_dir then
    return nil
  end
  local inner_pkg_path = fs.get_parent_dirpath(inner_path)
  if not inner_pkg_path then
    return
  end
  plugin_dirs_cache[inner_pkg_path] = plugin_dirs_cache[inner_pkg_path] or {}
  local entry = plugin_dirs_cache[inner_pkg_path]
  entry[#entry + 1] = plugin_dir
  log_debug("put to plugin-dir cache", plugin_dir, inner_path)
end

--
--
--
---@param gradle_src_root string
---@param inner_path string pkg/some/Class.java
---@return string? path to existed java-file
function M.find_path_in_cache(gradle_src_root, inner_path)
  local inner_pkg_path = fs.get_parent_dirpath(inner_path)
  local known_plugin_dirs = plugin_dirs_cache[inner_pkg_path]
  local found_path, found_plugin_dir
  if known_plugin_dirs then
    for _, plugin_dir in ipairs(known_plugin_dirs) do
      local path = join_path(gradle_src_root, plugin_dir, inner_path)
      if file_exists(path) then
        found_path = path
        found_plugin_dir = plugin_dir
        break
      end
    end
  end
  log_debug('found in cache', found_plugin_dir, inner_path)
  return found_path
end

function M.clear_plugin_dir_cache()
  plugin_dirs_cache = {}
end

---@return table
function M.get_plugin_dir_cache()
  return plugin_dirs_cache
end

--

--
-- experimental only!
--
---@param src_root_dir string
---@param inner_pkg_path string
function M.find_all_plugin_dirs_for_pkg_path(src_root_dir, inner_pkg_path)
  if not src_root_dir or not inner_pkg_path then
    return nil
  end
  local dirs = fs.list_of_dirs(src_root_dir)
  if not dirs then
    return nil
  end
  local t = {}
  for _, dir in ipairs(dirs) do
    local path = join_path(src_root_dir, dir, inner_pkg_path)
    if fs.file_exists(path) then
      t[#t + 1] = dir
    end
  end
  return t
end

--

return M

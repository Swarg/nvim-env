-- 26-01-2025 @author Swarg
--
-- AST to DSL transformer
-- Goal: cooke raw ast-like structore of groovy dsl to simple lua-table
--
-- Refs
-- ~/.gradle/dists/gradle-8.9-all/6m0mbzute7p0zdleavqlib88a/gradle-8.9/
--  src/platform-jvm/org/gradle/api/java/archives/Manifest.java
--  src/core-api/org/gradle/api/tasks/TaskContainer.java
--

local log = require 'alogger'
local groovy_dsl_parser = require 'env.langs.java.util.gradle.groovy_dsl_parser'
local LT = groovy_dsl_parser.LEXEME_TYPE

local M = {}

---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
--
--
---@return table
function M.parse_dsl(bin)
  log_debug("parse_dsl body sz:", #(bin or ''))
  local ast = groovy_dsl_parser.parse_to_ast(bin)
  return M.ast_to_dsl(ast)
end

--
--
---@param ast table
function M.ast_to_dsl(ast)
  local t = {}
  for _, node in ipairs(ast) do
    M.handle_node(t, node)
  end
  return t
end

---@param parent table
---@param n any
---@return table?
function M.handle_node(parent, n)
  log_trace("handle_node ctx-method:")
  assert(type(parent) == 'table', 'parent')
  local nt = type(n)
  if nt ~= 'table' then
    return nil
  end
  local lt = n[1]
  log_trace('lexeme:', lt)

  if lt == LT.ASSIGNMENT then
    M.handle_node_assignment(parent, n)
  elseif lt == LT.METHODCALL or lt == LT.CHAINEDCALL then
    M.handle_node_methodcall(parent, n)
  elseif lt == LT.CLOSURE then
    M.handle_node_closure(parent, n)
  elseif lt == LT.KVPAIR_LIST then
    M.handle_node_kvlist(parent, n)
  elseif lt == LT.NEW_ID then
    M.handle_node_new_id(parent, n)
  elseif lt == LT.TERNARY_OP then
    M.handle_node_ternary_op(parent, n)
  end

  return parent
end

local function unwrap_quotes(s)
  if type(s) == 'string' then
    local q = s:sub(1, 1)
    if q == '"' or q == "'" and s:sub(-1, -1) == q then
      return s:sub(2, -2)
    end
  end
  return s
end

---@param parent table
---@param key string
---@param value any
function M.assign(parent, key, value)
  key = unwrap_quotes(key)
  if type(value) == 'table' then
    local calculated_value = {}
    M.handle_node(calculated_value, value)
    parent[key] = calculated_value
  else
    parent[key] = unwrap_quotes(value)
  end
  return parent
end

function M.handle_node_assignment(parent, n)
  assert(n[1] == LT.ASSIGNMENT, 'assignment')
  local key = assert(n[2], 'key')
  local value = assert(n[3], 'value')

  M.assign(parent, key, value)

  return parent
end

--
function M.handle_node_methodcall(parent, n)
  assert(n[1] == LT.METHODCALL or n[1] == LT.CHAINEDCALL, '[chained] method call')

  local method_name = n[2] or 'unknown'
  log_trace("handle_node_methodcall mn:", method_name)

  parent[method_name] = parent[method_name] or {}
  local ctx = parent[method_name]

  local i = 2
  while i < #n do
    i = i + 1
    local param = n[i]
    log_trace("param", i, param)
    if param == nil then break end

    local pt = type(param)
    if pt == 'table' then
      M.handle_node(ctx, param)
    elseif pt == 'string' then
      log_trace('append string', param)
      ctx[#ctx + 1] = unwrap_quotes(param)
    else
      error('todo for param with type: ' .. pt)
    end
  end

  return parent
end

---@param parent table
---@param n table
function M.handle_node_closure(parent, n)
  log_trace("handle_node_closure n:%s parent:%s", n, parent)
  assert(n[1] == LT.CLOSURE, 'closure')
  local statements = n[2]
  if type(statements) == 'table' then
    for _, statement in ipairs(statements) do
      if type(statement) == 'table' then
        -- log_trace('statement: %s', statement)
        M.handle_node(parent, statement)
      end
    end
  else
    log_trace('empty closure')
  end
  return parent
end

---@param parent table
---@param n table
function M.handle_node_kvlist(parent, n)
  log_trace("handle_node_kvlist n:%s parent:%s", n, parent)
  assert(n[1] == LT.KVPAIR_LIST, 'closure')
  for i = 2, #n do
    local e = n[i]
    if type(e) == 'table' then
      assert(e[1] == LT.KVPAIR, 'list elm must be kvpair')
      local key = assert(e[2], 'key')
      local value = assert(e[3], 'value')
      M.assign(parent, key, value)
    end
  end
  return parent
end

--[[
http://docs.gradle.org/current/dsl/org.gradle.api.artifacts.ConfigurationContainer.html

configurations {
  myConfiguration
}


configurations.create('myConfiguration')
configurations.myConfiguration.transitive = false

configurations.create('myConfiguration')
configurations.myConfiguration {
    transitive = false
}
]]
function M.handle_node_new_id(parent, n)
  log_trace("handle_node_new_id n:%s parent:%s", n, parent)
  assert(n[1] == LT.NEW_ID, 'new_id')
  local id = n[2]
  parent[id] = parent[id] or {}
  return parent
end

-- condition ? branch-true : branch-false
-- it.isDirectory() ? it : zipTree(it)
function M.handle_node_ternary_op(parent, n)
  log_trace("handle_node_ternary_op n:%s parent:%s", n, parent)
  assert(n[1] == LT.TERNARY_OP, 'ternary_op')
  parent[#parent + 1] = {
    condition = n[2],
    btrue = n[3],
    bfalse = n[4]
  }
  return parent
end

return M

-- 24-01-2025 @author Swarg
--
-- a simple groovy dsl parser
--

local lpeg = require 'lpeg'
local loc = lpeg.locale()

local jgrammar = require 'env.langs.java.util.grammar'
local CONST = require 'env.langs.java.util.consts'

---@diagnostic disable-next-line: unused-local
local P, R, B, C, V, lpegS = lpeg.P, lpeg.R, lpeg.B, lpeg.C, lpeg.V, lpeg.S
---@diagnostic disable-next-line: unused-local
local Ct, Cc, Cg, Cp, Cf = lpeg.Ct, lpeg.Cc, lpeg.Cg, lpeg.Cp, lpeg.Cf

-- lpeg.B For making sure that a match is not preceded by a given pattern

local M = {}
-- M.debug = false
M.debug = true

-- luarocks/dprint: optional use if installed (or automatically create a stub)
local ok_dprint, D = pcall(require, 'dprint') -- debugging print
if not ok_dprint then
  D = {
    _VERSION = 'stub-of-dprint',
    mk_dprint_for = function(_, _) return function(...) end end,
    mk_dvisualize_for = function(_, _) return function(...) end end,
    on = false,
  }
end
local enabled_by_default = M.debug -- false
-- local dprint = D.mk_dprint_for(M, enabled_by_default)
local dvisualize = D.mk_dvisualize_for(M, enabled_by_default)

local path, prev_p, depth = {}, 0, 0
M.debug_clear = function() path, depth, prev_p = {}, 0, 0 end

local I = function(tag)
  if M.debug then -- dprint is on globaly
    return lpeg.P(function(matchedLine, curr_pos)
      if tag:sub(-4, -1) == '-End' then
        path[depth] = nil
        depth = math.max(0, depth - 1)
      else
        if curr_pos > prev_p then
          depth = depth + 1
          prev_p = curr_pos
        end
        path[depth] = tag
      end
      dvisualize(matchedLine, curr_pos, curr_pos, 'TAG:', tag, table.concat(path,'.'))
      return true
    end)
  end

  return lpeg.P(true) -- pattern what always pass, not affeted current pos
end

local EOF = P(-1)
local S = lpegS' \t\v\n\f\r' -- whitespace (the set of chars)
local NL = P'\r'^-1 * P'\n'  -- line end \r\n
local S0 = S^0               -- any count of spaces
-- local S1 = S^1               -- one or more spaces
local SPACES = lpegS' \t\v\f'^0
local SPACES1 = lpegS' \t\v\f'^1

local digit = R '09'
-- local uppderLetter = R('AZ')
local letter = R('az', 'AZ') + P'_'
local alphanum = letter + digit
local dot = P'.'
local hex = R('af', 'AF', '09')
local exp = lpegS'eE' * lpegS'+-' ^ -1 * digit^1
local fs = lpegS'fFlL'
local is = lpegS'uUlL'^0

local hexnum = P'0' * lpegS'xX' * hex^1 * is^-1
local octnum = P'0' * digit^1 * is ^ -1
local decnum = digit^1 * is ^ -1
local floatnum = digit^1 * exp * fs ^ -1 +
    digit^0 * dot * digit^1 * exp ^ -1 * fs ^ -1 +
    digit^1 * dot * digit^0 * exp ^ -1 * fs ^ -1
local pNumLit = hexnum + octnum + floatnum + decnum

local pCharLit =
    P "'" * (P'\\' * P(1) + (1 - lpegS "\\'"))^1 * P "'"

local pStringLit =
    P'"' * (P'\\' * P(1) + (1 - lpegS'\\"'))^0 * P'"'

local pCommentML = P'/*' * (P(1) - P'*/')^0 * P'*/' -- multiline
local pCommentOL = P'//' * (P(1) - P'\n')^0         -- oneline
local pCommentMLInline = P'/*' * (P(1) - (P'*/'+ NL))^0 * P'*/' -- multiline
-- local pComment = Cc(COMMENT) * (pCommentML + pCommentOL)

-- local ccomment_open = P'/*' * (1 - P'*/')^0 * -P(1)                   -- /* to end of the line
-- local ccomment_close = (P(1) - P'*/' - P'/*')^0 * P'*/' * S0 * -P(1) -- /* to end of the line

--[[
local pOperator = P('!=') + '!' + '==' + '=' + '+=' + '-=' +
    ';' + '{' + '}' + '(' + ')' + '[' + ']' + '@' +
    '<<' + '<=' + '<-' + '<' + '>>' + '>=' + '>' + '->' +
    '--' + '-' + '++' + '+' + '&&' + '&' + '||' + '|' +
    '^' + '*' + '/' + '%' + '?' -- + '.'
]]
local pLogicOp = P('||') + '!' + '&&' + '||'
local pCompOp = P('==') + '!=' +
    '<<' + '<=' + '<' + '>>' + '>=' + '>'


local pLiteral = (pNumLit + pCharLit + pStringLit)

local pString = P'"' * (P(1) - P'"')^0 * P'"'
local pDocString = P'"""' * (P(1) - P'"""')^0 * P'"""'

local pComment0 = pCommentML + pCommentOL

-- skip everything that can escape parentheses
local pSkipEscaped = pDocString + pString + pComment0


-- recursive grammar
-- () "("  """\n ( """   "*" and """*""" `/* ( */ `  and `// ) \n`
local pBalancedParentheses = lpeg.P { "RuleBP",
  RuleBP = P "(" * ((P(1) - lpeg.S'()"/') + V'RuleBP' + pSkipEscaped)^0 * P ")"
}

local pBalancedCurlyBraces = lpeg.P { "RuleBCB",
  RuleBCB = P "{" * ((P(1) - lpeg.S'{}"/') + V'RuleBCB' + pSkipEscaped)^0 * P "}"
}


local function buildKWPatt(list)
  local patt = lpeg.P(false) -- just empty pattern for appending
  for _, w in ipairs(list) do patt = patt + w end
  return patt * -loc.alnum
end
local pKeyword = buildKWPatt(CONST.JAVA_KEYWORD_LIST)

local pIdentifier = -- I'IdentifierOpen'*
    (letter * alphanum^0 - pKeyword * (-alphanum))

-- [[som.pkg.]Class.]methodName
-- [[som.pkg.]Class.]fieldName
-- varname
local pGlobalIdentifier =
    (pIdentifier * '.')^0 * pIdentifier

local pIdentifierWithWildCard =
    pIdentifier + P"'*'" + pIdentifier*P'*'

local pGIdentifierWithWildCard =
    (pIdentifierWithWildCard * '.')^0 * pIdentifierWithWildCard


-- local Statements = lpeg.V'Statements'
local Statement = V'Statement'
local MethodCall = V'MethodCall' -- JavaLanguageVersion.of(21)
local ConstructorCall = V'ConstructorCall'
local MethodParam = V'MethodParam'
local MethodParams = V'MethodParams'
local MethodLastParam = V'MethodLastParam'
local MapBody = V'MapBody'
local MapExpl = V'MapExpl'
local MapKVPair = V'MapKVPair'
local Closure = V'Closure'
local ChainedMethodCall = V'ChainedMethodCall'
local Condition = V'Condition'
local BoolExpression = V'BoolExpression'

local pTrue = P'true'
local pFalse = P'false'
local pNull = P'null'
local pEmptyString = P"''"

local SC = S0 * (pComment0 * S0)^0
local SCL = SPACES * (pCommentMLInline * SPACES)^0
-- line end
local LINE_END = SPACES * pCommentMLInline^0 * SPACES * (NL + pCommentOL + EOF)

-- local UNKNOWN = 0
local ASSIGNMENT = '='
local METHODCALL = 'MC'
local CONSTRUCTORCALL = 'NEW'
local CHAINEDCALL = 'CMC'    -- given({}).when({}).then({})
local KVPAIR_LIST = 'KVL' -- aka ordered map
local KVPAIR = 'KV'
local CLOSURE = 'C'
local CONDITION = 'IF'
local BOOL_EXPR = 'BE'
local TERNARY_OP = 'TO'
local NEW_ID = 'NI'

M.LEXEME_TYPE = {
  ASSIGNMENT = ASSIGNMENT,
  METHODCALL = METHODCALL,
  CHAINEDCALL = CHAINEDCALL,
  KVPAIR_LIST = KVPAIR_LIST,
  KVPAIR = KVPAIR,
  CLOSURE = CLOSURE,
  CONDITION = CONDITION,
  TERNARY_OP = TERNARY_OP,
  NEW_ID = NEW_ID,
}

local function token(id, patt) return Ct(Cc(id) * patt) end

local pType = jgrammar.pType
local pValue = I'pValue' *
  pLiteral + pFalse + pTrue + pNull + pEmptyString + P'-'*pNumLit
  -- varname | pkg.Class.field | System.properties['user.home']
  + pGlobalIdentifier * P"["*(pLiteral+ pGlobalIdentifier)*P']'
  + pGlobalIdentifier
  *I'pValue-End'
local cpVal = C(pValue)

-- for "configuration {myConfiguration}"
local pIdInClosure = I'pNewIdentifierInClosure'
  * SC * token(NEW_ID, C(pIdentifier)) * LINE_END -- (NL+ P';')
  * I'pNewIdentifierInClosure-End'


local Assignment = V'Assignment'
local TernaryOp = V'TernaryOp'

-- local Term = V'Term'
local grammar = lpeg.P { 'Statement',
  Statement = SC * I'Statement'*
    (Assignment + Condition + BoolExpression + ConstructorCall + MethodCall) * SC
  *I'Statement-End',

  -- key = "value" [/* comment */]\n
  Assignment = I'Assignment' * token(ASSIGNMENT, (
      C(pGlobalIdentifier) * SCL * P'=' * SCL
      * (Statement + C(pValue * (SCL * P'+' * SCL * (pValue))^0))
      +
      (pType * SPACES1) * C(pGlobalIdentifier) * SCL * P'=' * SCL
      * (Statement + C(pValue * (SCL * P'+' * SCL * (pValue))^0))
    )
    * (LINE_END + P';'^0) -- limitation: {k=v}  Will not work correctly! need \n
  ) *I'Assignment-End',

  Condition = I'Condition' * token(CONDITION,
    SC* P'if' *SC* C(pBalancedParentheses) *SC * C(pBalancedCurlyBraces) *
    (P'else' *SC* C(pBalancedCurlyBraces))^0
  )*I'Condition-End',

  BoolExpression = I'BoolExpression' * token(BOOL_EXPR,
    SC * (MethodCall + C(pValue))
       * SCL * C(pCompOp + pLogicOp) *I'Op-End'
       * SCL * (MethodCall + C(pValue))
  )*I'BoolExpression-End',

  TernaryOp = I'TernaryOp'* token(TERNARY_OP,
    (Statement+cpVal)*SCL*P'?'*SCL*(Statement+cpVal)*SCL*P':'*SCL*(Statement+cpVal)
  )*I'TernaryOp-End',

  MapKVPair = token(KVPAIR,
    -- key: value
    I'Key'*C(pIdentifier+pLiteral)*I'Key-End'
    *SCL*P':'*SCL*
    C(pGlobalIdentifier + pLiteral + pTrue + pFalse)
  ),
  MapBody = I'MapBody' * token(KVPAIR_LIST, -- without []
    MapKVPair * (SCL*P','*SC*MapKVPair)^0
  )*I'MapBody-End',
  MapExpl = P'['*SCL* MapBody *SCL*P']',

  Closure = I'Closure'* token(CLOSURE,
    SCL* Ct((P'{'* ((TernaryOp+Statement+pIdInClosure) *S0)^0 * SC * P'}'))
  )*I'Closure-End',

  MethodParam = I'MethodParam'*
    -- {(  not key: 'value' (Map)
    (C(pGlobalIdentifier + pLiteral+ pTrue + pFalse) + MapExpl) *SCL*-lpegS':{('
    + MethodCall
  *I'MethodParam-End',

  MethodLastParam = I'MethodLastParam'
    *SCL* (MapBody + Closure * ChainedMethodCall^0) * SCL
  *I'MethodLastParam-End',

  MethodParams =  I'MethodParams' *
     ( -- params of the method
      -- MethodParam * SCL       -- from sourceSets.main.output
      -- inputs.property "version", project.version
      MethodParam * SCL * (SCL * P',' * SCL * MethodParams)^0
      +
      P'(' * SC               -- mavenLocal()
        * (MethodParam * (SCL* P',' * SC * MethodParam)^0)^0
        * MethodLastParam^0
        * SC * (P','*SC)^-1 *
      P')'
        * Closure^-1          -- method(params) {/* closure */ }
        * ChainedMethodCall^0 -- method(params) {closure} .chainedMethod(params)
      +
      --MethodLastParam * I'Lne'*LINE_END *I'Lne-End'-- apply plugin: 'java'
      MethodLastParam * -pIdentifier
  ) * I'MethodParams',

  MethodCall = I'MethodCall' * token(METHODCALL,
    -- 'method name', but not "apply plugin: 'java'"
    C(pGlobalIdentifier+pGIdentifierWithWildCard) * SCL*-lpegS':;,' * MethodParams
  ) * I'MethodCall-End',

  ConstructorCall = I'ConstructorCall' * token(CONSTRUCTORCALL,
    -- 'method name', but not "apply plugin: 'java'"
    P'new'*SPACES1* C(pGlobalIdentifier) * SCL*-lpegS':;,' * MethodParams
  ) * I'ConstructorCall-End',

  -- given({}).when({}).then({})  ->  given{}.when{}.then{}
  --                              ->  given {} when {} then {}  (without dots)
  ChainedMethodCall = I'ChainedMethodCall'* token(CHAINEDCALL,
    (dot+SPACES1) * C(pIdentifier) *SCL* MethodParams * S0
  )*I'ChainedMethodCall-End',
}

local G = I'Start' * SC * grammar^0 * SC * I'Finish'

--
-- raw parsed ast like structure
--
---@param s string
---@return table
function M.parse_to_ast(s)
  return { lpeg.match(G, s) }
end

--

if _G.TEST then
  M.p = {
    CommentMLInline = pCommentMLInline,
    GIdentifierWithWildCard = pGIdentifierWithWildCard,
    grammar = grammar,
  }
end

return M


-- 18-07-2024 @author Swarg
local M = {}
--

-- https://docs.gradle.org/current/userguide/java_testing.html

M.WAIT_ATTACH = "Listening for transport dt_socket at address: "
M.BUILD_FAILED = "BUILD FAILED"
M.WAITING_FOR_CHANGES = 'Waiting for changes to input files of tasks...'

---@param args table
---@param opts table
---@return table args
function M.setup_opts(args, opts)
  if args and opts then
    if opts.clean_task == true then
      table.insert(args, 1, "clean") -- cleanTest?
    end
    if opts.hotswap == true then
      table.insert(args, "--continuous")
    end
    if opts.no_daemon == true then
      table.insert(args, "--no-daemon")
    end
    if opts.test_debug == true or opts.debug_jvm == true then
      table.insert(args, '--debug-jvm')
    end
    if opts.test_html_report ~= false then
      -- works via "reports.html.required = false" inside "tasks.withType(Test)"
      -- "Test report disabled, omitting generation of the HTML test report."
      table.insert(args, '-PnoHtmlReports')
    end
  end
  return args
end

-- -i - Set log level to info. same as
-- -Dorg.gradle.logging.level=info   "(quiet,warn,lifecycle,info,debug)"

--
-- https://docs.gradle.org/current/userguide/command_line_interface.html
-- https://docs.gradle.org/current/userguide/build_environment.html
-- https://docs.gradle.org/current/userguide/java_testing.html

return M

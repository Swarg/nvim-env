-- 18-01-2025 @author Swarg
--
-- simple dependency manager (cached in ~/.gradle/modules-2/files-2.1/...
--

local fs = require 'env.files'

local M = {}
local join_path = fs.join_path
local HOME = os.getenv('HOME')
local GRADLE_CACHE = join_path(HOME, '.gradle', 'caches', 'modules-2', 'files-2.1')

--
---@param gav table{groupId,artifactId,version}
function M.get_artifact_relative_path(gav)
  local group = assert(gav.groupId, 'groupId')
  local artifact = assert(gav.artifactId, 'artifactId')
  local path = join_path(group, artifact)
  if gav.version and gav.version ~= '' then
    path = join_path(path, gav.version)
  end
  return path
end

--
-- org.slf4j:slf4j-simple:2.0.16   -->
--       ~/.gradle/caches/modules-2/files-2.1/org.slf4j/slf4j-simple/2.0.16/...
--
---@return false|table:string files
---@return string? err
---@return string? path
function M.get_artifact_files(gav)
  local path = join_path(GRADLE_CACHE, M.get_artifact_relative_path(gav))
  local files, err = fs.list_of_files_deep(path)
  if type(files) ~= 'table' then
    return false, err, path
  end
  return files, nil, path
end

return M

-- 06-09-2024 @author Swarg
-- Goal:
-- - generate simple tempate for LuaTester

local log = require 'alogger'
local fs = require 'env.files'
local base = require("env.lang.utils.base")
local utempl = require 'env.lang.utils.templater'


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug

M.SRC_TEST_LUA = "/src/test/_lua/" -- for testing jvm app with lua

-- example of how to write tests with db-env and lhttp REST-API
local TEMPLATE_WEBAPP_N_DB = [[
-- ${DATE} @author ${AUTHOR}
-- Start the webapp before run this test
--
-- dependencies:
--   - luarocks: busted, lhttp, db-env${LUAROCKS_DEPS}

require "busted.runner" ()
local assert = require 'luassert'

local H = require 'lhttp.html.helper'
local DB = require 'db_env.helper'

local hostaddr = H.localhost8080
local dbconf = ${DBCONF}

-- webapp endpoints
local ep = {
${ENDPOINTS}
}

H.check_webapp_health(hostaddr, ${CHECK_HEALTH_ENDPOINT}, {
  msg = ${FAIL_HEALTH_MSG}
})

local client ---@cast client lhttp.Client

describe("rest-api", function()
  setup(function()
    client = H.newClient(hostaddr, H.UserAgent.Linux.Firefox, H.CT.APP_JSON)
    client:addResponseFilter(H.jsonFilterMaskTimestamp)
    -- db connection
    DB.connect_by_config(dbconf)
    DB.refresh_db_state(${REFRESH_DB_STATE_PARAMS})
  end)

  before_each(function()
    client:newRequest()
  end)

  teardown(function()
    DB.disconnect()
  end)

${TEST}
end)
]]



-- webapp endpoints
local example_endpoints = [[
  people = '/people',
  people_id = '/people/{id}',]]

local example_test = [[
  it("/people/{id}", function()
    local exp = { username = 'Jonny', email = 'jonny@mail.com' }
    assert.same(exp, client:getJsonO(ep.people_id, { id = 4 }))
  end)
]]

---@return table
local function get_def_subs()
  return {
    DATE = os.date(' %d-%m-%Y'),
    AUTHOR = base.getDeveloperName(),
    FAIL_HEALTH_MSG = "use 'make clean-build run' before run this test",
    DBCONF = './src/main/resources/application.properties',
    -- todo support the flyway
    REFRESH_DB_STATE_PARAMS = '{ __scripts_dir = "./sql/" }',
    LUAROCKS_DEPS = '',
    ENDPOINTS = example_endpoints,
    TEST = example_test,
  }
end

---@return string
---@return string
---@param project_root string
function M.add_webapp_n_db_template(project_root, opts)
  log_debug("add_webapp_n_db_template", project_root)
  local path = fs.join_path(project_root, M.SRC_TEST_LUA, 'webapp_spec.lua')
  local subs = get_def_subs()

  if opts.no_date then subs.DATE = '' end
  subs.AUTHOR = opts.author or subs.AUTHOR

  local body = utempl.substitute(TEMPLATE_WEBAPP_N_DB, subs)
  return path, body
end

return M

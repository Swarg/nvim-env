-- 14-08-2024 @author Swarg
local M = {}

local U = require 'env.langs.java.util.core'
local constants = require 'env.lang.api.constants'

local LT = constants.LEXEME_TYPE
M.LT = LT

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
-- index of the word in parsed words from one line
--
---@param lexems table
---@param pos number
---@return number
---@return string?
function M.indexOfByPos(lexems, pos)
  for i, lexem in ipairs(lexems) do
    local s, e = lexem[2], lexem[3]
    if pos >= s and pos <= e then
      return i, lexem[1]
    end
  end
  return -1
end

local function is_class(s)
  return U.isClassName(s) or U.isFullClassNameWithPackage(s)
end

local function is_type(s)
  return match(s, '^%u[%w_%.]*[%w%<%?%s%,>]*$') ~= nil or
      match(s, '^<[%w,%s]+>$') ~= nil -- <T> <K,V>
end

local lexem2typ = {
  [';'] = LT.OPSEP,
  ['{'] = LT.BLOCK_OPEN,
  ['}'] = LT.BLOCK_CLOSE,
  ['('] = LT.BRACKET,
  [')'] = LT.BRACKET,
  ['['] = LT.BRACKET,
  [']'] = LT.BRACKET,
}


-- todo add check to SPACE lexeme (between lexemes)

--
---@param lexems table
---@param col number
---@return number
---@return string?
function M.getTypeOfPos(lexems, col)
  local i, curr = M.indexOfByPos(lexems, col)
  if not i or i < 0 or not curr then
    return -1, curr -- out of range
  end

  if #(curr or '') == 0 then
    return LT.EMPTY
  end

  if U.isKeyWord(curr) then return LT.KEYWORD end
  if U.isOperator(curr) then return LT.OPERATOR end

  local prev = (lexems[i - 1] or E)[1]
  local next = (lexems[i + 1] or E)[1]

  local typ = lexem2typ[curr] -- ; { } ()[]
  if typ then
    return typ
  end

  if curr == '()' then
    typ = LT.OPERATOR -- method call, part of method signature,
  elseif curr == '<>' then
    typ = LT.OPERATOR -- diamond empty subtype
  elseif (next == '<>') then
    typ = (is_class(curr)) and LT.CLASS or LT.UNKNOWN
  elseif next == '[' or next == '[]' then
    typ = (is_class(curr)) and LT.CLASS or LT.ARRAY
  elseif next == '(' or next == '()' then
    if prev == 'new' then
      typ = LT.CLASS
    elseif prev == '@' then
      typ = LT.ANNOTATION
    else
      typ = LT.METHOD
    end
  elseif prev == '@' and is_class(curr) then
    typ = LT.ANNOTATION
  elseif prev == 'this.' then
    typ = (next ~= '(' and next ~= '()') and LT.CLASSFIELD or LT.METHOD
  elseif next == '=' then
    if is_class(prev) then
      typ = LT.VARNAME -- varname
    else
      typ = LT.IDENTIFIER
    end
  else
    local fc = string.sub(curr, 1, 1)
    if fc == '"' then
      typ = LT.STRING
    elseif tonumber(curr) then
      typ = LT.NUMBER
    elseif U.isComment() then
      typ = LT.COMMENT
    elseif U.isVariableName(curr) then
      typ = LT.IDENTIFIER
    elseif is_type(curr) then
      typ = LT.TYPE
    else
      typ = LT.UNKNOWN
    end
  end

  return typ, curr
end

M.t = {
  is_type = is_type
}

return M

-- 24-01-2025 @author Swarg
-- interface between implementation and callers

local Editor = require 'env.ui.Editor'
local JParser = require 'env.langs.java.JParser'
local codegen = require 'env.langs.java.util.codegen.generator'

local M = {}
local E = {}

--
-- lombok like boilerplate code generation
--
-- generate code as @lombok.Value
--
-- given - only fields in class - generate class-Value same as @Value from the
-- lombok library
--
-- https://projectlombok.org/features/Value
---@param opts table{bufnr}
---@return boolean
---@return string? errmsg
function M.gen_value_class(opts)
  local bufnr = (opts or E).bufnr or 0
  local parser = JParser:new(nil, Editor.iterator(bufnr)):prepare();
  local lines, err = codegen.gen_value_class(parser.clazz)
  if type(lines) ~= 'table' then
    return false, err
  end
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, lines)
  return true
end

--
-- lombok like boilerplate code generation
--
-- generate code as @lombok.Data
--
-- given - only fields in class - generate class-Value same as @Value from the
-- lombok library
--
-- https://projectlombok.org/features/Data
--
---@param opts table{bufnr}
---@return boolean
---@return string? errmsg
function M.gen_data_class(opts)
  local bufnr = (opts or E).bufnr or 0
  local parser = JParser:new(nil, Editor.iterator(bufnr)):prepare();
  local lines, err = codegen.gen_data_class(parser.clazz)
  if type(lines) ~= 'table' then
    return false, err
  end
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, true, lines)
  return true
end

return M

-- 28-01-2025 @author Swarg
-- migration helper


local log = require 'alogger'
local consts = require 'env.langs.java.util.consts'
local refactor = require 'env.langs.java.util.refactor'
local JParser = require 'env.langs.java.JParser'
local FileIterator = require 'olua.util.FileIterator'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local move_method_arg = refactor.move_method_arg
local parse_signature_param_types = refactor.parse_signature_param_types
local log_debug = log.debug

local M = {}

-- migration from junit4 to junit5 imports mappings
M.mapping_update_from_4 = {
  ['org.junit.Test'] = 'org.junit.jupiter.api.Test',
  ['org.junit.Assert'] = 'org.junit.jupiter.api.Assertions',
  ['org.junit.Before'] = 'org.junit.jupiter.api.BeforeEach',
  ['org.junit.After'] = 'org.junit.jupiter.api.AfterEach',
  ['org.junit.BeforeClass'] = 'org.junit.jupiter.api.BeforeAll',
  ['org.junit.AfterClass'] = 'org.junit.jupiter.api.AfterAll',
  ['org.junit.Ignore'] = 'org.junit.jupiter.api.Disabled',
  ['static org.junit.Assert.*'] = 'static org.junit.jupiter.api.Assertions.*',
  -- ?
  ['static org.junit.Assert.assertTrue'] = 'static org.junit.jupiter.api.Assertions.assertTrue',
  ['static org.junit.Assert.assertFalse'] = 'static org.junit.jupiter.api.Assertions.assertFalse',
  ['static org.junit.Assert.assertEquals'] = 'static org.junit.jupiter.api.Assertions.assertEquals',
  ['static org.junit.Assert.assertNull'] = 'static org.junit.jupiter.api.Assertions.assertNull',
  ['static org.junit.Assert.assertNotNull'] = 'static org.junit.jupiter.api.Assertions.assertNotNull',
}
-- https://junit.org/junit4/javadoc/4.13/org/junit/Assert.html

--
-- junit4
-- assertEquals("Pull value from box", exp, unBox(ibox, 0));
-- Java: The method assertEquals(int, int, String) in the type Assertions is not applicable for the arguments (String, int, int)
-- junit5:
-- assertEquals(exp, unBox(ibox, 0), "Pull value from box");

local Assertions = {}
local classes = {
  Assertions = Assertions,
}

--
-- corrects the arguments ordering in the method
-- based on a diagnostic error from (jdtls)
--
---@param line string
---@param errinfo table?
---@return string? updatedLine
---@return string? errmsg
function M.fix_method_args(line, errinfo)
  if not line or line == '' then
    return nil, 'no line to fix method args'
  end
  if type(errinfo) ~= 'table' then
    return nil, 'no errinfo to fix method args'
  end
  local cn, mn = errinfo.class_name, errinfo.method_name

  if not mn or not cn then
    return nil, 'no methodname or classname'
  end
  if not errinfo.errtype then
    return nil, 'no errtype for ' .. v2s(cn) .. '.' .. v2s(mn)
  end

  local handler = (classes[cn] or E)[mn]
  if not handler then
    return nil, 'unsupported ' .. v2s(cn) .. '.' .. v2s(mn)
  end

  return handler(line, errinfo)
end

-----
--
-- handlers to fix error shown by diagnostics:
--

--
-- fix params order in the Assertions.assertEquals() method
--
-- class_name = 'Assertions',
-- method_name = 'assertEquals',
-- method_params = 'int, int, String',
-- bad_args = 'String, int, int',
-- errtype = 'WRONG_METHOD_ARGS',
--
---@param line string
---@param ei table -- error-info
function Assertions.assertEquals(line, ei)
  local et = ei.errtype
  if et == consts.ERROR.WRONG_METHOD_ARGS then
    -- expected 'int, int, String' got 'String, int, int'
    local ept = parse_signature_param_types(ei.method_params) or E
    local apt = parse_signature_param_types(ei.bad_args) or E
    log_debug("exp pt:%s actual pt:%s", ept, apt)
    if #ept == 3 and #apt == 3 and apt[1] == 'String' and ept[3] == 'String' then
      return move_method_arg(line, ei.method_name, ei.method_params, 1, 3)
    end
  end
  return nil, 'unsupported errtype: ' .. v2s(et) .. ': ' .. v2s(ei.method_params)
end

Assertions.assertArrayEquals = Assertions.assertEquals

--
---@param line string
---@param ei table -- error-info
function Assertions.assertNotNull(line, ei)
  local et = ei.errtype
  if et == consts.ERROR.WRONG_METHOD_ARGS then
    -- expected 'type, String' got 'String, type'
    local ept = parse_signature_param_types(ei.method_params) or E
    local apt = parse_signature_param_types(ei.bad_args) or E
    if #ept == 2 and #apt == 2
        and (apt[1] == 'String' or apt[1] == 'Supplier<String>')
        and (ept[2] == 'String' or ept[2] == 'Supplier<String>')
    then
      return move_method_arg(line, ei.method_name, ei.method_params, 1, 2)
    end
  end
  return nil, 'unsupported errtype: ' .. v2s(et) .. ': ' .. v2s(ei.method_params)
end

Assertions.assertNull = Assertions.assertNotNull
Assertions.assertTrue = Assertions.assertNotNull
Assertions.assertFalse = Assertions.assertNotNull


----
--
--------------------------------------------------------------------------------

--
-- to update the test source file from junit-4 to junit-5
--
--  fix:
--   - fix imports ( org.junit -> org.junit.jupiter.api)
--   - remove System.out.pringln(test-name) lines from test methods
--   - remove public modifier from test methods
--  TODO:
--   - change annotations: Before -> BeforeEach, BeforeClass -> BeforeAll
--
---@return number
---@return string? errmsg
---@param opts table
function M.update_test_source(path, opts)
  log_debug("update_test_source_to_junit5", path)
  opts = opts or {}

  local lines = {}
  local parser = JParser:new(nil, FileIterator:new(nil, path, lines))
  parser:prepare(nil)

  if parser:hasErrors() then
    return 0, parser:getErrLogger():getReadableReport()
  end

  -- build plan for mapping
  local clazz = parser:result() or E -- parsed clazz
  local methods = clazz.methods or E
  local imports = clazz.imports or E
  local import_lns = clazz.import_lns or E

  -- if #(methods) < 1 then return 0, 'no methods' end

  local c = 0
  local function is_test_method(m)
    return type(m) == 'table'
        and ((m.annotations or E)[1] or E).name == 'Test'
        and type(m.ln) == 'number' and type(m.lne) == 'number'
        and m.name
  end

  local lines_cnt = #lines

  local import_mappings = M.mapping_update_from_4
  for n, import in ipairs(imports) do
    if import_mappings[import] then
      local ln = import_lns[n]
      -- expected "static"-pefix in mapping value itself
      lines[ln] = 'import ' .. v2s(import_mappings[import]) .. ';'
      c = c + 1
    end
  end

  -- todo change per-class Annotations RunWith -> ExtendWith

  for _, m in ipairs(methods) do
    if is_test_method(m) then
      local fline = lines[m.ln]
      local ind = match(fline, '^(%s+)public void ')
      if ind then
        lines[m.ln] = string.gsub(fline, 'public void', 'void')
        c = c + 1
      end

      -- remove sout with test name
      ind = match(lines[m.ln + 1] or '', '^(%s+)System.out.println%(')
      if ind then
        lines[m.ln + 1] = nil -- ind .. '// given'
        c = c + 1
      end

      -- todo change method Annotations (Before -> BeforeEach)
    end
  end

  -- prepare result to save and save to the same file
  if c > 0 then
    local updated_lines = {}
    for i = 1, lines_cnt do
      local line = lines[i]
      if line ~= nil then -- skip gapes
        updated_lines[#updated_lines + 1] = line
      end
    end
    local h, err = io.open(path, 'wb')
    if not h then
      return 0, err
    end
    updated_lines[#updated_lines + 1] = '' -- to ends with "\n"

    assert(h:write(table.concat(updated_lines, "\n")))
    assert(h:close())
  end

  return c
end

return M

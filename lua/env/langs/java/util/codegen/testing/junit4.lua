-- 27-01-2025 @author Swarg
--

local ClassInfo = require 'env.lang.ClassInfo'
local ComponentGen = require 'env.lang.oop.ComponentGen'

local M = {}

M.template_import = [[
import org.junit.Test;
import static org.junit.Assert.*;
]]

M.template_method = [[
    @Test
    ${ACCESSMOD} ${RETURN_TYPE} ${NAME}(${PARAMS}) {
        System.out.println("${NAME}");
${BODY}
    }
]]

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local getNameWithPreffix = ComponentGen.getNameWithPreffix
--
--
---@param sci env.lang.ClassInfo
---@param opts table{name}
---@return string body
---@return string tmn
---@return number offset lnum
---@return number col
function M.gen_testmethod_body(sci, opts)
  local tab = opts.tab or '  '

  local tcn_short = sci ~= nil and sci:getShortClassName() or ''

  local tmn = getNameWithPreffix('test', opts.method_name or 'Success')

  local obj_vn = ClassInfo.getVarNameForClassName(tcn_short, '')
  local t2 = tab .. tab

  local given, when, then0 = '', '', ''
  local use_bdd = false

  given = fmt("%s%s %s = new %s();\n", t2, tcn_short, obj_vn, tcn_short)
  then0 = fmt("%sassertEquals(1, %s);", t2, obj_vn)

  if use_bdd then
    given = t2 .. "// given\n" .. given
    when = t2 .. "// when\n" .. when
    then0 = t2 .. "// then\n" .. then0
  end

  local offset_lnum, col = 0, #'public void'

  local body = given .. when .. then0
  return body, tmn, offset_lnum, col
end

return M

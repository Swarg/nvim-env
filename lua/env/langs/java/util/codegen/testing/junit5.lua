-- 27-01-2025 @author Swarg
--
local log = require 'alogger'
local ClassInfo = require 'env.lang.ClassInfo'
local Modifier = require 'env.langs.java.util.reflect.Modifier'
local JParser = require 'env.langs.java.JParser'
local consts = require 'env.langs.java.util.consts'
-- local refactor = require 'env.langs.java.JParser'

local M = {}
M.template_import = [[
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
]]

--[[
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
]]

M.template_method = [[
    @Test
    @DisplayName("Test functionality")
    ${RETURN_TYPE} ${NAME}(${PARAMS}) {
${BODY}
    }
]]
-- ${ACCESSMOD} - package default

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--
-- call callback to get env.ui.Context from opts from LangGen.createNewTestMethod
-- parse current context to gen info about current method
--
---@return table? method
---@return table? clazz
function M.parse_testing_method(opts)
  local ctx = type(opts.get_ctx) == 'function' and opts.get_ctx() or nil
  log_debug("parse_testing_method has-ctx:", ctx ~= nil)
  if not ctx then
    return nil, nil
  end
  local lnum = ctx.cursor_row or 1
  ctx.parser = JParser:new()

  local parser = ctx:parse(JParser.predicate_stop_at_lnum(lnum), 0)
  local clazz = parser:getResult()
  local method = JParser.findParsedMethodByLnum(lnum, clazz)

  log_debug('found clazz:%s method:%s', (clazz or E).name, (method or E).name)
  if clazz and not method and log.is_debug() then
    local t = {}
    for _, method0 in ipairs(clazz.methods or E) do
      t[#t + 1] = fmt("%04d: %s", method0.ln or 0, v2s(method0.name))
    end
    log_debug("parsed methods in class:%s\n%s", clazz.name, table.concat(t, "\n"))
  end
  return method, clazz
end

--
--
---@param sci env.lang.ClassInfo
---@param opts table{tab, name}
---@return string body
---@return string tmn
---@return number offset lnum
---@return number col
function M.gen_testmethod_body(sci, opts)
  log_debug("gen_testmethod_body")
  local tab = opts.tab or '  '

  local method, clazz = M.parse_testing_method(opts)

  local tcn_short = sci ~= nil and sci:getShortClassName() or ''

  local tmn = opts.method_name

  local obj_vn = ClassInfo.getVarNameForClassName(tcn_short, '')
  local t2 = tab .. tab


  local given, when, then0 = '', '', ''
  local use_bdd = true

  if method and clazz then -- by the parsed source code of the testing method
    given, when, then0 = M.gen_given_when_then_blocks(clazz, method, opts)
  else
    -- simple
    given = fmt("%s%s %s = new %s();", t2, tcn_short, obj_vn, tcn_short)
    -- todo integration with mock?
    -- then0 = fmt("%sassertThat(%s).isEqualTo(0);", t2, obj_vn)
    then0 = fmt("%sassertEquals(null, %s);", t2, obj_vn)
  end

  if use_bdd then
    local mn = opts.method_name or 'Test'
    mn = string.upper(mn:sub(1, 1)) .. mn:sub(2)
    tmn = 'given_when' .. v2s(mn) .. '_then'
    given = t2 .. "// given\n" .. given
    when = t2 .. "// when\n" .. when
    then0 = t2 .. "// then\n" .. then0
    if given:sub(-1, -1) ~= "\n" then given = given .. "\n" end
    if when:sub(-1, -1) ~= "\n" then when = when .. "\n" end
  end
  local offset_lnum, col = 2, 5

  local body = given .. when .. then0
  return body, tmn, offset_lnum, col
end

---@param clazz table result from JParser
---@param method table
---@param opts table{tab}
---@return string given
---@return string when
---@return string then
function M.gen_given_when_then_blocks(clazz, method, opts)
  local tab = (opts or E).tab or '  '
  local t2 = tab .. tab

  local scn = v2s(clazz.name)
  local is_static = Modifier.isStatic(method.mods or 0)
  local rettyp = method.typ
  local res_varname = nil
  local method_args = ''
  if not rettyp or rettyp == 'void' then
    res_varname = nil -- given = fmt("%s%s(%s);\n", t2, method.name, method_args)
  elseif consts.JAVA_PRIMITIVE_TYPE_MAP[rettyp] then
    res_varname = 'res'
  else
    res_varname = ClassInfo.getVarNameForClassName(rettyp)
  end

  local retvaldef = ''
  local method_name = method.name

  if res_varname ~= nil then
    retvaldef = v2s(rettyp) .. ' ' .. v2s(res_varname) .. ' = '
  end
  --

  local given, when, then0 = '', '', ''

  if #(method.params or E) > 0 then
    for n, param in ipairs(method.params) do
      local argname = param[2] or ('param' .. v2s(n))
      if #given > 0 then given = given .. "\n" end
      given = given .. fmt("%s%s %s = null;", t2, param[1] or 'Object', argname)
      if #method_args > 0 then method_args = method_args .. ', ' end
      method_args = method_args .. argname
    end
  end

  if is_static then
    method_name = scn .. '.' .. method_name
    when = fmt("%s%s%s(%s);", t2, retvaldef, method_name, method_args)
  else -- dynamic method of the obj
    local objName = ClassInfo.getVarNameForClassName(scn)
    local newInstanceArgs = ''
    given = given ..
        fmt("%s%s %s = new %s(%s);", t2, scn, objName, scn, newInstanceArgs)
    when = fmt("%s%s%s(%s);", t2, retvaldef, method_name, method_args)
  end

  then0 = fmt('%sassertEquals(%s, %s);', t2, 'null', res_varname or '1')

  return given, when, then0
end

return M

--[[
Examples
@Test
void junit_extension_cannot_be_loaded() {
    assertThatExceptionOfType(ClassNotFoundException.class)
        .isThrownBy(() -> Class.forName("org.junit.jupiter.api.extension.Extension"));
}
]]


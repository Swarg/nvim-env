-- 23-01-2025 @author Swarg
--
-- lombok like the boilerplate|source code generation
--

local log = require 'alogger'
local Modifier = require 'env.langs.java.util.reflect.Modifier'

local M = {}

local INDENTATION = '    '
local MAX_LINE_LEN = 80

local log_debug = log.debug
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


---@return table lines with generated code
---@param clazz table
function M.gen_value_class(clazz, opts)
  log_debug("gen_value_class", (clazz or E).name)
  assert(type(clazz) == 'table', 'clazz')
  local params = {
    ind = (opts or E).ind or INDENTATION,
    max_line_len = (opts or E).max_line_len or MAX_LINE_LEN,
  }

  local t = {} -- result lines

  local function add(s) t[#t + 1] = s or '' end

  add('package ' .. v2s(clazz.pkg) .. ';')
  add()
  for n = 1, #(clazz.imports or E) do
    add('import ' .. v2s(clazz.imports[n]) .. ';')
  end
  add('import java.beans.ConstructorProperties;') -- for Constructor
  add('import java.util.Objects;')                -- for hashCode
  add()
  add('public final class ' .. v2s(clazz.name) .. ' {')
  M.gen_fields(clazz, t, 'private final', params)
  add()
  M.gen_all_args_constructor(clazz, t, params)
  add()
  M.gen_getters(clazz, t, params)
  M.gen_hash_code(clazz, t, params)
  add()
  M.gen_equals(clazz, t, params)
  add()
  M.gen_tostring(clazz, t, params)

  add('}') -- end of the class

  return t
end

-- @Data:
--   @ToString, @EqualsAndHashCode,
--   @Getter on all fields,
--   @Setter on all non-final fields, and
-- todo  @RequiredArgsConstructor
function M.gen_data_class(clazz, opts)
  log_debug("gen_data_class", (clazz or E).name)
  assert(type(clazz) == 'table', 'clazz')
  local params = {
    ind = (opts or E).ind or INDENTATION,
    max_line_len = (opts or E).max_line_len or MAX_LINE_LEN,
  }

  local t = {} -- result lines

  local function add(s) t[#t + 1] = s or '' end

  add('package ' .. v2s(clazz.pkg) .. ';')
  add()
  for n = 1, #(clazz.imports or E) do
    add('import ' .. v2s(clazz.imports[n]) .. ';')
  end
  add('import java.util.Objects;') -- for hash_code
  add()
  add('public final class ' .. clazz.name .. ' {')

  M.gen_fields(clazz, t, nil, params)
  add()
  M.gen_required_args_constructor(clazz, t, params)
  add()
  M.gen_getters(clazz, t, params)
  M.gen_setters(clazz, t, params)
  M.gen_hash_code(clazz, t, params)
  add()
  M.gen_equals(clazz, t, params)
  add()
  M.gen_tostring(clazz, t, params)

  add('}') -- end of the class

  return t
end

--
-- generate java code  of the in-class fields definition
--
---@param clazz table{fields}
---@param mod string?
function M.gen_fields(clazz, t, mod, params)
  local fields = clazz.fields
  local ind = params.ind or INDENTATION

  for i = 1, #fields do
    local field = fields[i]
    local ftyp = assert(field.typ, 'field.typ')
    local fname = assert(field.name, 'field.name')
    mod = mod or Modifier.mods2str(field.mods)
    t[#t + 1] = fmt('%s%s %s %s;', ind, mod, ftyp, fname)
  end
end

--
---@param clazz table{name, fields}
---@param t table output
---@param params table{ind, max_line_len}
function M.gen_all_args_constructor(clazz, t, params)
  local ind = params.ind or INDENTATION
  local max_line_len = params.max_line_len or MAX_LINE_LEN
  local function add(s) t[#t + 1] = s or '' end

  local classname = v2s(clazz.name)
  local fields = clazz.fields
  local constructor_params = {}

  local sing_param_len = 0
  for i = 1, #fields do
    local field = fields[i]
    local ftyp = assert(field.typ, 'field.typ')
    local fname = assert(field.name, 'field.name')
    constructor_params[#constructor_params + 1] = '"' .. fname .. '"'
    sing_param_len = sing_param_len + #ftyp + #fname
  end
  sing_param_len = sing_param_len + 2 * (#fields - 1)

  -- AllArgsConstructor
  --   annotation
  local constructor_param_names = table.concat(constructor_params, ", ")
  if #constructor_param_names + 26 + #ind <= max_line_len then
    add(ind .. '@ConstructorProperties({' .. constructor_param_names .. '})')
  else
    add(ind .. '@ConstructorProperties({')
    add(ind .. ind .. constructor_param_names)
    add(ind .. '})')
  end

  --   constructor
  if (sing_param_len + 12 + #ind + #classname <= max_line_len) then
    local sign_params = ''
    for _, field in ipairs(fields) do
      if sign_params ~= '' then sign_params = sign_params .. ', ' end
      sign_params = sign_params .. field.typ .. ' ' .. field.name
    end
    add(ind .. 'public ' .. classname .. ' (' .. sign_params .. ') {')
  else
    local line = ind .. 'public ' .. classname .. ' ('
    for n, field in ipairs(fields) do
      if #line + #field.typ + #field.name < max_line_len then
        line = line .. field.typ .. ' ' .. field.name
      else
        if line:sub(-1, -1) == ' ' then line = line:sub(1, #line - 1) end
        add(line)
        line = ind .. ind .. ind .. field.typ .. ' ' .. field.name
      end
      if n < #fields then line = line .. ', ' end
    end
    add(line .. ') {')
  end

  for _, field in ipairs(fields) do
    add(fmt('%s%sthis.%s = %s;', ind, ind, field.name, field.name))
  end
  add(ind .. '}') -- end of the constructor
end

--
---@param clazz table{name, fields}
---@param t table output
---@param params table{ind, max_line_len}
function M.gen_required_args_constructor(clazz, t, params)
  -- todo implement via final Modifier in the field.mods
  return M.gen_all_args_constructor(clazz, t, params)
end

local function upCamelCaseName(s)
  return string.upper(s:sub(1, 1)) .. s:sub(2)
end

--
-- getters
--
---@param clazz table{fields}
---@param params table{ind, max_line_len}
function M.gen_getters(clazz, t, params)
  local ind = params.ind or INDENTATION
  local function add(s) t[#t + 1] = s or '' end

  for _, field in ipairs(clazz.fields) do
    local pref = 'get'
    if field.typ == 'boolean' or field.typ == 'Boolean' then pref = 'is' end
    add(fmt('%spublic %s %s%s() {', ind, field.typ, pref, upCamelCaseName(field.name)))
    add(ind .. ind .. 'return this.' .. field.name .. ';')
    add(ind .. '}')
    add()
  end
end

--
-- do not generate setters for final fields
--
---@param clazz table{fields}
---@param params table{ind, max_line_len}
function M.gen_setters(clazz, t, params)
  local ind = params.ind or INDENTATION
  local function add(s) t[#t + 1] = s or '' end

  for _, field in ipairs(clazz.fields) do
    if not Modifier.isFinal(field.mods or 0) then
      add(fmt('%spublic void set%s(%s %s) {',
        ind, upCamelCaseName(field.name), field.typ, field.name))
      add(ind .. ind .. 'this.' .. field.name .. ' = ' .. field.name .. ';')
      add(ind .. '}')
      add()
    end
  end
end

-- generate hash_code
---@param clazz table{fields}
---@param params table{ind, max_line_len}
function M.gen_hash_code(clazz, t, params)
  local ind = params.ind or INDENTATION
  local function add(s) t[#t + 1] = s or '' end

  add(ind .. '@Override')
  add(ind .. 'public int hashCode() {')
  add(ind .. ind .. 'final int PRIME = 31;')
  add(ind .. ind .. 'int result = 17;')

  for _, field in ipairs(clazz.fields) do
    -- <java8
    -- add('result = PRIME * result + ((name == null) ? 0 : name.hashCode());'
    -- java8+:
    add(fmt('%s%sresult = PRIME * result + Objects.hashCode(this.%s);',
      ind, ind, v2s(field.name)))
  end
  add(ind .. ind .. 'return result;')
  add(ind .. '}')
end

--
-- todo support fields with primitive types (fieldName.equals)
--
---@param clazz table{name, fields}
---@param params table{ind, max_line_len}
function M.gen_equals(clazz, t, params)
  local ind = params.ind or INDENTATION
  local function add(s) t[#t + 1] = s or '' end
  local ind2 = ind .. ind
  local classname = clazz.name

  add(ind .. '@Override')
  add(ind .. 'public boolean equals(Object obj) {')
  add(ind2 .. 'if (this == obj) return true;')
  add(ind2 .. 'if (obj == null || getClass() != obj.getClass()) return false;')
  add(ind2 .. classname .. ' other = (' .. classname .. ') obj;')

  for _, field in ipairs(clazz.fields) do
    local fn = upCamelCaseName(field.name)
    local fn1 = 't' .. fn
    local fn2 = 'o' .. fn
    add(ind2 .. field.typ .. ' ' .. fn1 .. ' = this.get' .. fn .. '();')
    add(ind2 .. field.typ .. ' ' .. fn2 .. ' = other.get' .. fn .. '();')
    -- if (tName == null ? oName != null : !tName.equals(oName)) return false;
    add(ind2 .. 'if (' .. fn1 .. ' == null ? ' .. fn2 .. '!= null : !'
      .. fn1 .. '.equals(' .. fn2 .. ')) return false;')
  end

  add(ind2 .. 'return true;')
  add(ind .. '}')
end

--
--
---@param clazz table{name, fields}
---@param params table{ind, max_line_len}
function M.gen_tostring(clazz, t, params)
  local ind = params.ind or INDENTATION
  local max_line_len = params.max_line_len or MAX_LINE_LEN
  local function add(s) t[#t + 1] = s or '' end
  local ind2 = ind .. ind
  local classname = clazz.name
  local fields = clazz.fields

  add(ind .. '@Override')
  add(ind .. 'public String toString() {')

  local line = ind2 .. 'return "' .. classname .. '('

  for n, field in ipairs(fields) do
    local fn = upCamelCaseName(field.name)
    local s = field.name .. '=" + get' .. fn .. '()'
    if n > 1 then
      s = ' + ", ' .. s
    end
    if #line + #s < max_line_len then
      line = line .. s
    else
      add(line)
      line = ind2 .. ind2
    end
  end
  add(line .. ' + ")";')
  add(ind .. '}')
end

return M

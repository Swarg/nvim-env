-- 08-08-2024 @author Swarg
-- java.lang.reflect.Modifier
--
-- https://hg.openjdk.org/jdk8u/jdk8u/jdk/file/774f11d707e0/src/share/native/java/lang/Class.c
-- https://openjdk.org/projects/jdk/17/
-- https://github.com/openjdk/jdk17
--
local bit = require 'bit32'

local M = {}


local MAP_CLASS_TYPES = {
  SYNTHETIC      = 0x00001000,
  ANNOTATION     = 0x00002000,
  ['@interface'] = 0x00002000,
  ENUM           = 0x00004000,
  enum           = 0x00004000,
  -- the exact values are not known; I have provided approximate
  -- values for now todo check for possible conflicts:
  class          = 0x00100000,
  interface      = 0x00200000,
  record         = 0x00400000,
  CLASS          = 0x00100000,
  INTERFACE      = 0x00200000,
  RECORD         = 0x00400000,
}

local CONST           = {
  PUBLIC       = 0x00000001,
  PRIVATE      = 0x00000002,
  PROTECTED    = 0x00000004,
  STATIC       = 0x00000008,
  FINAL        = 0x00000010,
  SYNCHRONIZED = 0x00000020,
  VOLATILE     = 0x00000040,
  TRANSIENT    = 0x00000080,
  NATIVE       = 0x00000100,
  INTERFACE    = 0x00000200,
  ABSTRACT     = 0x00000400,
  STRICT       = 0x00000800,
  --
  public       = 0x00000001,
  private      = 0x00000002,
  protected    = 0x00000004,
  static       = 0x00000008,
  final        = 0x00000010,
  synchronized = 0x00000020,
  volatile     = 0x00000040,
  transient    = 0x00000080,
  native       = 0x00000100,
  interface    = 0x00000200,
  abstract     = 0x00000400,
  strict       = 0x00000800,
}

M.MAP_CLASS_TYPES     = MAP_CLASS_TYPES
M.CONST               = CONST

M.PUBLIC              = 'public'
M.PRIVATE             = 'private'
M.PROTECTED           = 'protected'
M.STATIC              = 'static'
M.FINAL               = 'final'
M.SYNCHRONIZED        = 'synchronized'
M.VOLATILE            = 'volatile'
M.TRANSIENT           = 'transient'
M.NATIVE              = 'native'
M.INTERFACE           = 'interface'
M.ABSTRACT            = 'abstract'
M.STRICT              = 'strictfp' -- for classes only floating ops



local Modifier = M

M.ACCESS_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE,
}
M.CLASS_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE,
  Modifier.ABSTRACT, Modifier.STRICT, Modifier.FINAL,
  -- Modifier.STATIC, -- caannot be in mainclass,  only  can be in innner class
}
M.INTERFACE_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE,
  Modifier.ABSTRACT, Modifier.STATIC, Modifier.STRICT
}
M.CONSTRUCTOR_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE
}
M.METHOD_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE,
  Modifier.ABSTRACT, Modifier.STATIC, Modifier.FINAL,
  Modifier.SYNCHRONIZED, Modifier.NATIVE, Modifier.STRICT,
}
M.FIELD_MODIFIERS = {
  Modifier.PUBLIC, Modifier.PROTECTED, Modifier.PRIVATE,
  Modifier.STATIC, Modifier.FINAL, Modifier.TRANSIENT,
  Modifier.VOLATILE
}

---@param mod number
-- return mod & constants.PUBLIC ~= 0
function M.isAbstract(mod) return bit.btest(mod, CONST.ABSTRACT) end

---@param mod number
-- return mod & constants.PUBLIC ~= 0
function M.isPublic(mod) return bit.btest(mod, CONST.PUBLIC) end

---@param mod number
function M.isPrivate(mod) return bit.btest(mod, CONST.PRIVATE) end

---@param mod number
function M.isProtected(mod) return bit.btest(mod, CONST.PROTECTED) end

---@param mod number
function M.isStatic(mod) return bit.btest(mod, CONST.STATIC) end

---@param mod number
function M.isFinal(mod) return bit.btest(mod, CONST.FINAL) end

---@param mod number
---@param modmask number
function M.isMod(mod, modmask) return bit.btest(mod, modmask) end

-- todo gen lua-code for all methods


---@return string
---@param mods number
function M.mods2str(mods)
  local s = ''
  local function add_sep()
    if s ~= '' then s = s .. ' ' end
  end
  -- mods = mods or 0
  if M.isPublic(mods) then
    s = s .. M.PUBLIC
  elseif M.isPrivate(mods) then
    s = s .. M.PRIVATE
  elseif M.isProtected(mods) then
    s = s .. M.PROTECTED
  end
  if M.isStatic(mods) then
    add_sep()
    s = s .. M.STATIC
  end
  if M.isFinal(mods) then
    add_sep()
    s = s .. M.FINAL
  end
  -- todo all
  return s
end

return M

-- 13-08-2024 @author Swarg
-- Convert token from grammar into element of syntax tree
-- used by JParser

local log = require 'alogger'
local bit = require 'bit32'
local su = require 'env.sutil'
local U = require 'env.langs.java.util.core'
local G = require 'env.langs.java.util.grammar'
local Modifier = require 'env.langs.java.util.reflect.Modifier'
local CONST = require 'env.langs.java.util.consts'

local ClassInfo = require 'env.lang.ClassInfo'

local M = {}

M.Modifier = Modifier
M.tags = G.tags
local MODIFIER_CONST = Modifier.CONST
local CLASS_TYPES = Modifier.MAP_CLASS_TYPES
local T = G.tags

local E, v2s, fmt = {}, tostring, string.format
local log_trace = log.trace
local lfmt = log.format

---@return table
local function validated_token(token, expected)
  assert(type(token) == 'table', 'expected table token')
  local kind = (token or E)[1]
  if kind ~= expected then
    error(fmt("expected token kind: %s got:%s", v2s(expected), v2s(kind)))
  end
  return token
end

-- last token if the number, contains the character position on the current line
-- at which the pattern for that element was stopped
--
-- For Example: "public MyClass (Object params1, \n String pa2) {\n ..."
--
---@param token table
---@return number?
local function add_continue(token, elm)
  elm.continue = tonumber(token[#token]) -- .CONTINUE
  return elm.continue
end

-- with side effect: add new annotation into clazz.free_annotations
---@param token table{'@', annoName}
---@param lnum number
---@return table{ln, name, params?}
---@return number -- of next annotation if has
function M.token_to_annotation(token, lnum, off)
  token = validated_token(token, T.ANNOTATION)
  off = off or 0

  local annotation = {
    ln = lnum,
    name = token[off + 2],
    params = nil, -- -- for @Anno("sliteral")  will be value="sliteral"
    -- for @Anno(k="val") will be a table { {"k", "val"} }
  }
  add_continue(token, annotation)

  assert(annotation.name, 'on convert token to annotation')

  local tparams = token[off + 3]
  if tparams and tparams ~= T.ANNOTATION and tparams ~= '(' then
    M.tokens_to_annotation_params(tparams, annotation)
    off = off + 3 -- @ name params
    -- error(log.format('cannot build annotation from: %s token:(%s)', t, token))
  else
    off = off + 2 -- @ name
  end

  return annotation, off
end

-- clazz - container for given annotation to put it into clazz.free_annotations
---@param clazz table{free_annotations}
function M.add_free_annotation(clazz, annotation)
  clazz.free_annotations = clazz.free_annotations or {}
  clazz.free_annotations[#clazz.free_annotations + 1] = annotation
  return annotation
end

---@param tokens table?
---@param annotation table{name}
---@return table
function M.tokens_to_annotation_params(tokens, annotation)
  if type(tokens) == 'number' then -- empty annot body like @Anno()
    return annotation
  end
  if (type(tokens) ~= 'table') then
    error('expected table tokens got (' .. type(tokens) .. '): ' .. v2s(tokens))
  end
  annotation.params = annotation.params or {}
  if type(tokens) == 'table' then                        -- can be number in case: @Anno(\n)
    for _, kvpair in ipairs(tokens) do
      annotation.params[#annotation.params + 1] = kvpair -- {"k", "v"}
    end
  end
  return annotation
end

-- move(attach) free_annotations to element
-- modify container (remove free_annotations
---@param container table
---@param elm table
---@return table elm
function M.attach_annotations_to_elm(container, elm)
  assert(type(elm) == 'table', 'elm to be annotated')

  if (container or E).free_annotations then
    assert(#(elm.annotations or E) == 0, 'ensure does not overwrite existing')

    elm.annotations = container.free_annotations
    container.free_annotations = nil -- clear
  end
  return elm
end

---@param name string annotation name without @
---@param elm table{annotations:table?}
---@return table?
---@return number index in list
function M.find_annotation(elm, name)
  if type(elm) ~= 'table' or #(name or '') < 2 or not elm.annotations then
    return nil, -1
  end
  for i, e in ipairs(elm.annotations) do
    if e.name == name then
      return e, i
    end
  end
  return nil, -1
end

-- first from given names
---@param elm table
---@param names table
function M.find_annotation_any(elm, names)
  if type(elm) ~= 'table' or #(names or E) < 1 or not elm.annotations then
    return nil, -1
  end
  for i, e in ipairs(elm.annotations) do
    for _, fname in ipairs(names) do
      if e.name == fname then
        return e, i
      end
    end
  end
  return nil, -1
end

-- todo extends implements so on ..
---@param token table?
---@param clazz table conversion result
---@param lnum number
function M.token_to_class_entry(token, lnum, clazz)
  token = validated_token(token, T.CLASSDEF)

  clazz.ln = lnum
  local mods = 0
  for j = 2, #token do
    local s = token[j]
    local mod0 = MODIFIER_CONST[s or 0]
    if mod0 and not CLASS_TYPES[s or false] then
      mods = bit.bor(mod0, mods)
    else
      local typ0 = CLASS_TYPES[s]
      if typ0 then
        clazz.typ = s -- just a string or num? todo num
        clazz.name = assert(token[j + 1], 'has-name')
        for i = j + 2, #token do
          local next_tok = token[i]
          if type(next_tok) == 'table' then
            if next_tok[1] == T.EXTENDS then
              clazz.extends = next_tok[2]    -- string
            elseif next_tok[1] == T.IMPLEMENTS then
              clazz.implements = next_tok[2] -- table - list of interfaces
            end
          end
        end
        break -- todo extends, implements
      else
        error('unsupported class type: ' .. v2s(s))
      end
    end
  end
  clazz.mods = mods
  add_continue(token, clazz)
  if clazz.typ == nil or clazz.name == nil then
    error(lfmt("cannot convert token to class. got token:%s clazz t:%s n:%s",
      token, clazz.typ, clazz.name))
  end

  return M.attach_annotations_to_elm(clazz, clazz)
end

--
-- handle continue and defualt value of the field
--
---@param token table?
---@param clazz table{free_annotations}
function M.token_to_class_field_entry(token, lnum, clazz)
  token = validated_token(token, T.FIELD)

  local mods = 0
  local e = { ln = lnum, lne = nil, continue = nil, value = nil }
  for j = 2, #token do
    local s = token[j]
    local mod0 = MODIFIER_CONST[s or 0]
    if mod0 then
      mods = bit.bor(mod0, mods)
    else
      local typ0 = s
      if typ0 then
        e.typ = s -- just a string or num? todo num
        e.name = U.validatedClassFieldName(token[j + 1], token, 'fieldName')
        break
      end
    end
  end
  e.mods = mods
  add_continue(token, e)

  assert(e.typ ~= nil and e.name ~= nil, 'on convert token to field')

  -- default value of the field like `public String name = "athena";`
  if token[#token - 2] == '=' then     -- with continue
    e.value = token[#token - 1]
  elseif token[#token - 1] == '=' then -- without continue
    e.value = token[#token]
  end
  log_trace("token_to_class_field_entry:", e)

  return M.attach_annotations_to_elm(clazz, e)
end

--
-- todo throws
--
---@param token table?
---@param lnum number
---@param clazz table{free_annotations}
---@return table
function M.token_to_constructor_entry(token, lnum, clazz)
  token = validated_token(token, T.CONSTRUCTOR)

  local e = { ln = lnum, lne = nil, continue = nil }

  local mods = 0
  for j = 2, #token do -- 1 is LEXEME ID: "CONSTRUCTOR"
    local s = token[j]
    local mod0 = MODIFIER_CONST[s or 0]
    if mod0 then
      mods = bit.bor(mod0, mods)
    else
      e.name = U.validatedClassName(token[j], token, 'Constructor')
      local next_tok = token[j + 1]
      if type(next_tok) == 'table' then
        M.token_with_params_to_method_entry(next_tok, e)
        next_tok = token[j + 2]
      end
      if type(token[j + 2]) == 'table' then
        M.token_with_throws_to_method_entry(token[j + 2], e)
      end
      break
    end
  end
  e.mods = mods
  add_continue(token, e)

  assert(e.name ~= nil and e.mods ~= nil, 'convert token to constructor')

  return M.attach_annotations_to_elm(clazz, e)
end

local function is_tbl(v)
  return type(v) == 'table'
end

--
---@param token table?
---@param method_entry table
function M.token_with_params_to_method_entry(token, method_entry)
  token = validated_token(token, T.SIGNPARAMS)

  method_entry.params = method_entry.params or {}
  for i = 2, #token do -- 1 is LEXEME ID: "SIGNPARAMS"
    local t = token[i]
    log_trace("token[%s]", i, t)
    local off, annotations = 1, nil

    -- process annotations of method parameters
    if is_tbl(t) and is_tbl(t[1]) then
      local ann_off = 0
      local limit = 32
      while (t[1][ann_off + 1] == T.ANNOTATION) and limit > 0 do
        limit = limit - 1
        local annotation, next_off = M.token_to_annotation(t[1], 0, ann_off)
        ann_off = next_off

        annotations = annotations or {}
        annotation.ln = nil -- unknown
        annotations[#annotations + 1] = annotation
      end
      if annotations ~= nil then
        off = off + 1
      end
    end

    assert(type(t[off]) == 'string', 'type of param')
    local is_final = false
    if t[off] == 'final' then -- todo another param mods
      off      = off + 1
      is_final = true
    end
    assert(U.validatedVariableName(t[off + 1], t, 'param name'))

    method_entry.params[#method_entry.params + 1] = {
      annotations = annotations,
      [1] = t[off],     -- type of param
      [2] = t[off + 1], -- name of param
      [3] = is_final and 'FINAL' or nil
    }
  end
  return method_entry
end

---@param token table
---@param method_entry table
function M.token_with_throws_to_method_entry(token, method_entry)
  token = validated_token(token, T.THROWS)

  method_entry.throws = method_entry.throws or {}
  for i = 2, #token do -- 1 is LEXEME
    local t = token[i]
    assert(type(t[1]) == 'string', 'type of thrown')
    assert(U.validatedClassName(t[1], t, 'throwable name'))

    method_entry.throws[#method_entry.throws + 1] = t
  end
  return method_entry
end

-- todo throws
function M.token_to_method_entry(token, lnum, clazz)
  token = validated_token(token, T.METHOD)

  local e = { ln = lnum, lne = nil, continue = nil }

  local mods = 0
  for j = 2, #token do
    local s = token[j]
    local mod0 = MODIFIER_CONST[s or 0]
    if mod0 then
      mods = bit.bor(mod0, mods)
    else
      e.typ = token[j]
      e.name = U.validatedVariableName(token[j + 1], token, 'Method')
      local next_tok = token[j + 2]
      if type(next_tok) == 'table' then
        M.token_with_params_to_method_entry(next_tok, e)
      end
      if type(token[j + 3]) == 'table' then
        M.token_with_throws_to_method_entry(token[j + 3], e)
      end
      break
    end
  end
  e.mods = mods
  add_continue(token, e)

  assert(e.name ~= nil and e.mods ~= nil, 'convert token to method')

  return M.attach_annotations_to_elm(clazz, e)
end

--------------------------------------------------------------------------------

---@param method table{params}
---@param params table{{"Type", "paramName1"}, {"Type", "paramName2"}}
---@return table
function M.add_params_to_method(method, params)
  method.params = method.params or {}
  for _, p in ipairs(params) do
    method.params[#method.params + 1] = p
  end
  return method
end

---@return table
function M.new_di_constructor(classname, params, lnum)
  return {
    mods = Modifier.CONST.PUBLIC,
    name = U.validatedClassName(classname),
    params = params,
    annotations = { { name = "Autowired", ln = lnum, } },
    ln = lnum + 1,
    di = true, -- ?
  }
end

--------------------------------------------------------------------------------
M.DEF_TAB = '    '
M.MAX_WIDTH = 80

function M.get_params_as_str_len(params)
  local len = 0
  for i, p in ipairs(params) do
    local typ, pname = p[1] or '', p[2] or ''
    len = len + #typ + #pname + ((i > 1) and 2 or 0) -- ', '
  end
  return len
end

---@param e table{mods:number, name:string, params:table?}
function M.get_code_of_constructor_singature(e, tab)
  tab = tab or M.DEF_TAB
  local s = tab .. Modifier.mods2str(e.mods) .. ' ' .. v2s(e.name) .. '('
  local indent = string.rep(' ', #s)
  if e.params then
    local singlen = #s + M.get_params_as_str_len(e.params)
    local sep = (singlen > M.MAX_WIDTH) and (",\n" .. indent) or ', '
    for i, p in ipairs((e.params or E)) do
      local typ, vn, sep0 = p[1], p[2], (i > 1 and sep or '')
      s = s .. fmt("%s%s %s", sep0, typ, vn)
    end
  end
  return s .. ") {"
end

---@param params table
---@param tab string?
---@param lines table?
function M.get_code_of_assign_params_to_private_fields(params, tab, lines)
  lines = lines or {}
  tab = tab or M.DEF_TAB
  for _, p in ipairs((params or E)) do
    local vn = p[2]
    lines[#lines + 1] = fmt("%s%sthis.%s = %s;", tab, tab, vn, vn)
  end
  return lines
end

---@param annotations table
---@param lines table?
---@param tab string?
function M.get_code_of_annotations(annotations, lines, tab)
  tab = tab or M.DEF_TAB
  lines = lines or {}
  if type(annotations) == 'table' and #annotations > 0 then
    for _, e in ipairs(annotations) do
      if (e.name) then
        local s = tab .. '@' .. e.name
        if #(e.params or E) > 0 then
          s = s .. '('
          -- todo multiline for too long annotation params
          local sep = ', '
          for i, kv in ipairs(e.params) do
            assert(type(kv) == 'table', 'kv')
            local key, value, sep0 = kv[1], kv[2], (i > 1 and sep or '')
            if i == 1 and key == 'value' and #e.params == 1 then
              s = s .. value -- literal as is
            elseif key and value then
              s = s .. string.format("%s%s = %s", sep0, key, value)
            end
          end
          s = s .. ')'
        end
        lines[#lines + 1] = s
      end
    end
  end
  return lines
end

-- todo move to JGen
---@param e table
---@param kind string
---@return table(lines)
---@param lines table?
---@param tab string?
function M.syntax_elm_to_code(e, kind, lines, tab)
  tab = tab or M.DEF_TAB
  lines = lines or {}
  if kind == T.CONSTRUCTOR then
    M.get_code_of_annotations(e.annotations, lines)
    su.split(M.get_code_of_constructor_singature(e, tab), "\n", lines)
    M.get_code_of_assign_params_to_private_fields(e.params, tab, lines)
    lines[#lines + 1] = tab .. "}"
  else
    error('Not implemented yet')
  end

  return lines
end

---@param classnames table of short classnames
---@return table
---@return table
function M.get_code_fields_from_classnames(classnames, mods, tab)
  tab = tab or M.DEF_TAB
  local fields, signparams = {}, {}
  mods = mods or (Modifier.CONST.PRIVATE + Modifier.CONST.FINAL)
  local smods = Modifier.mods2str(mods)
  for _, classname in ipairs(classnames) do
    local field_name = ClassInfo.getVarNameForClassName(classname)
    fields[#fields + 1] = fmt('%s%s %s %s;', tab, smods, classname, field_name)
    signparams[#signparams + 1] = { classname, field_name }
  end
  return fields, signparams
end

--------------------------------------------------------------------------------
--
--          to parse the source code inside methods by grammar(java)
--

-- todo L double hex
---@return boolean
function M.is_value(s)
  return tonumber(s) ~= nil
      or s == 'null' or s == 'true' or s == 'false'
      or type(s) == 'string' and (
        s:match('^".*"$') ~= nil or s:match("^'.'$") ~= nil or
        s:match('^""".*"""$') ~= nil
      )
end

-- unwrap from quotes if s is a String value
function M.unwrap_value(s)
  if s then
    local fc = s:sub(1, 1)
    if fc == '"' or fc == "'" and fc == s:sub(-1, -1) then
      return string.sub(s, 2, -2)
    end
  end
  return s
end

--
-- arg can be quoted('") string what needs to be unwrapped to compare with exp
-- solve issue with quoted strings and escaped chararters like "\n"
--
---@param arg string value(or variablename?) from method argument
---@param exp string value from diagnostic message
---@return boolean
function M.is_same_value(arg, exp)
  if arg ~= nil and arg == exp then --or tonumber(arg) == tonumber(exp))
    return true
  end

  local fc = arg:sub(1, 1)
  if fc == '"' or fc == "'" and arg:sub(-1, -1) == fc then
    arg = string.sub(arg, 2, -2)
  end
  if arg == exp then
    return true
  end
  if string.find(arg, '\\n', 1, true) then
    arg = string.gsub(arg, '\\n', "\n")
  end
  if string.find(arg, '\\t', 1, true) then
    arg = string.gsub(arg, '\\t', "\t")
  end
  return arg == exp
end

--
-- check is given string is a simple local variable name
-- xyz            - ok
-- variableName   - ok
-- this.fieldName - no
-- this.method()  - no
-- obj.prop       - no
--
---@return boolean
function M.is_local_variable_name(s)
  return s ~= nil and tonumber(s) == nil and CONST.JAVA_KEYWORD_MAP[s] == nil
      and CONST.JAVA_PRIM_VALUES_MAP[s] == nil
      and string.match(s, '^[%a%$][%w_%$]+$') ~= nil
end

-- common
local function is_lexeme(e, tag)
  return type(e) == 'table' and tag ~= nil and e[1] == tag and e[2] ~= nil
end

local function is_str(v) return type(v) == 'string' end
local function is_num(v) return type(v) == 'number' end

--
-- parse line with java code to a flatten abstract syntax tree
---@return table?{{METHOD, methodName, pos_s, pos_e}, {ARGUMENT, value, s, e}}
---@return string? errmsg
---@return number?
function M.parse_method_call_to_ast(line)
  local ast, epos = G.offhand.pMethodCallChainedWithPos:match(line)
  if type(ast) ~= 'table' then
    return nil, 'cannot parse line: |' .. v2s(line) .. '|'
  end
  epos = epos
  if not is_lexeme(ast[1], G.tags.METHOD) then
    return nil, log.format('expected method got: %s', v2s(ast[1]))
  end
  local method_entry = ast[1]
  local method_name = method_entry[2]
  local pos_s = method_entry[3]
  local pos_e = method_entry[4]
  if not is_str(method_name) or not is_num(pos_s) or not is_num(pos_e) then
    return nil, log.format('expected method with name got:%s', method_entry)
  end

  return ast, nil, epos
end

--
-- get the name of the method from ast of method call
-- (given ast can be hold chained methods with opsep in the end)
--
---@param ast table
---@param n number?
function M.get_method_name(ast, n)
  n = n or 1
  local amn = 0
  for i = 1, #ast do
    local elm = ast[i]
    if is_lexeme(elm, G.tags.METHOD) then
      amn = amn + 1
      if n == amn then
        return ast[i][2]
      end
    end
  end
  return nil
end

---@return table? sub_ast with given method (name+args+positions)
function M.get_method_entry(ast, n)
  n = n or 1
  local amn = 0
  for i = 1, #ast do
    local elm = ast[i]
    if is_lexeme(elm, G.tags.METHOD) then
      amn = amn + 1
      if n == amn then
        local sub_ast = { elm }
        for k = i + 1, #ast, 1 do
          elm = ast[k]
          if is_lexeme(elm, G.tags.ARGUMENT) then
            sub_ast[#sub_ast + 1] = elm
          else
            break
          end
        end
        return sub_ast --ast[i][2]
      end
    end
  end
  return nil
end

--
-- get the name of the method from sub-ast of method entry
--
---@return string name
---@return number pos start
---@return number pos end
function M.get_method_name_from_subast(sub_ast)
  local methodname_entry = sub_ast[1] or E
  assert(methodname_entry[1] == G.tags.METHOD, 'method')
  local name = methodname_entry[2]
  local pos_s = methodname_entry[3]
  local pos_e = methodname_entry[4]
  return name, pos_s, pos_e
end

--
---@param sub_ast table
---@param n number
function M.get_method_arg(sub_ast, n)
  local arg_elm = sub_ast[n + 1] or E
  local value = arg_elm[2]
  local pos_s = arg_elm[3]
  local pos_e = arg_elm[4]
  return value, pos_s, pos_e
end

return M

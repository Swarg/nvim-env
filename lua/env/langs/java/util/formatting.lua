-- 13-01-2025 @author Swarg
--

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local find = string.find


--
-- simple source code formatting to fix major styling issues.
--
--   - comments
--   - if(, for(
--
---@param lines table
function M.format(lines)
  if not lines or #lines == 0 then return end

  for n, s in ipairs(lines) do
    -- to remove empty lines with trailing-whitespaces
    if match(s, '^%s+$') ~= nil then
      s = ''
      -- continue
    end
    -- "  code  " --> "  code"
    local line0 = match(s, '^(.-)%s+$')
    if line0 and line0 ~= '' then
      s = line0
    end

    -- //comment  -->  // comment
    local i = find(s, '//[^%s]', 1, false)
    if i and i > 0 and s:sub(i - 1, i - 1) ~= ':' then
      s = s:sub(1, i - 1) .. '// ' .. s:sub(i + 2, #s)
    end
    -- "/*c" --> "/ *c"  but "/**"  --> "/**"
    i = find(s, '/%*[^%s%*]', 1, false)
    if i and i > 0 then s = s:sub(1, i - 1) .. '/* ' .. s:sub(i + 2, #s) end
    -- "c*/" --> "/ *c"
    i = find(s, '[^%s%*]%*/', 1, false)
    if i and i > 0 then s = s:sub(1, i) .. ' */' .. s:sub(i + 3, #s) end

    i = find(s, 'if(', 1, true)
    if i and i > 0 then s = s:sub(1, i - 1) .. 'if (' .. s:sub(i + 3, #s) end

    i = find(s, 'for(', 1, true)
    if i and i > 0 then s = s:sub(1, i - 1) .. 'for (' .. s:sub(i + 4, #s) end

    i = find(s, '){', 1, true)
    if i and i > 0 then s = s:sub(1, i - 1) .. ') {' .. s:sub(i + 2, #s) end

    lines[n] = s
  end
end

---@return table
function M.fmt_method_signature(s, max_len)
  max_len = max_len or 90
  if not s or #s < max_len then return s end
  local i = find(s, '(', 1, true)
  if not i then return { s } end
  local k = find(s, ')', i, true)
  if not k then return { s } end

  local ind = match(s, '^(%s*)') or ''


  local t = {}
  t[#t + 1] = s:sub(1, i)
  t[#t + 1] = ind .. '    ' .. s:sub(i + 1, k - 1)
  t[#t + 1] = ind .. s:sub(k, #s)
  return t
end

--     Class        -->  Class<?>
-- new ArrayList()  -->  new ArrayList<>()
---@param s string line
function M.fix_raw_types(s)
  if not s or s == '' then return s end

  local p = 1
  local pattlen = 6
  while p < #s do
    -- todo except ".. instanceof Class", "Class.forName"
    local i = find(s, '[^%w_]Class[^%w%<%(%.]', p, false)
    if not i then break end

    s = s:sub(1, i) .. 'Class<?>' .. s:sub(i + pattlen, #s)
    p = i + pattlen + 4 -- '<?>'
  end

  local i = find(s, 'new ArrayList(', 1, true);
  if i and i > 0 then s = s:sub(1, i - 1) .. 'new ArrayList<>(' .. s:sub(i + 14, #s) end

  i = find(s, 'new HashMap(', 1, true);
  if i and i > 0 then s = s:sub(1, i - 1) .. 'new HashMap<>(' .. s:sub(i + 12, #s) end

  return s
end

return M

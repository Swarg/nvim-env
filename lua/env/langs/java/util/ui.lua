-- 23-02-2025 @author Swarg
local M = {}
--

-- local log = require 'alogger'
local fs = require 'env.files'
local core = require 'env.langs.java.util.core'
local isFullClassNameWithPackage = core.isFullClassNameWithPackage
-- local log_debug, lfmt = log.debug, log.format

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
--
---@param w Cmd4Lua
---@return string?
function M.get_classname_from_clipboard(w, q)
  q = q or 'classname'
  local cn = vim.fn.getreg("+")
  if not isFullClassNameWithPackage(cn) then
    cn = vim.fn.getreg("*")
  end
  if not isFullClassNameWithPackage(cn) then
    return w:error('cannot get ' .. v2s(q) .. ' from clipboard')
  end
  if q == 'package' then
    return core.getPackage(cn) or cn
  end
  return cn
end

function M.normalize_path(path)
  if not path then
    return path
  end
  if path and path:sub(1, 2) == '~/' then
    path = os.getenv('HOME') .. path:sub(2)
  end
  if string.find(path, "\n", 1) ~= nil then
    path = path:gsub("\n", "")
  end
  return path
end

---@param w Cmd4Lua
function M.get_path_from_clipboard(w)
  local path = M.normalize_path(vim.fn.getreg("+"))
  if not fs.file_exists(path) then
    path = M.normalize_path(vim.fn.getreg("*"))
    if not fs.file_exists(path) then
      w:error('not found file ' .. v2s(path))
      return nil
    end
  end
  return path
end

return M

-- 29-07-2024 @author Swarg

local log = require 'alogger'
local fs = require 'env.files'
local JC = require 'env.langs.java.util.consts'
local Editor = require 'env.ui.Editor'

-- to send http query to tomcat
local mime = require("mime")
local socket_url = require("socket.url")


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug

local function notify(msg)
  ---@diagnostic disable-next-line: param-type-mismatch
  Editor.echoInStatus(nil, msg, nil)
end

--
---@param t table?
---@param get_creds_callback function - callback to fetch creds(user,password)
--                           from some global configuration file
function M.is_valid_conf_entry(t, get_creds_callback)
  if type(t) ~= 'table' or type(t.configuration) ~= 'table' then
    return false
  end
  local conf = t.configuration
  if conf.server and not conf.creds then
    assert(type(get_creds_callback) == 'function')
    conf.creds = get_creds_callback(conf.server)
  end
  local f = conf.server and conf.url and conf.path and conf.creds
  log_debug('is_valid_conf_entry: %s user:%s', f, (conf.creds or E).username)
  return f
end

---@param classfile string
---@param classname string
---@param opts table?
function M.redeploy_class(t, classname, classfile, opts)
  log_debug("redeploy_class", classname, classfile)
  if M.copy_classfile_to_webapps(t, classname, classfile) then
    return M.reload_webapp(t, opts)
  end
  return false
end

-- webtemplate or jsp(todo)
---@return boolean
---@param innerpath string?
---@param viewfile string  absolute path to viewfile in the project
---@param opts table?
function M.redeploy_view(t, innerpath, viewfile, opts)
  log_debug("redeploy_view", innerpath)
  if M.copy_viewfile_to_webapps(t, innerpath, viewfile) then
    return M.reload_webapp(t, opts)
  end
  return false
end

local ok_tomcat_webapps_dir = nil

---@return string
local function validated_tomcat_webapps_dir()
  if ok_tomcat_webapps_dir == true then
    return JC.TOMCAT_WEBAPPS_DIR
  end
  if ok_tomcat_webapps_dir == false or not fs.dir_exists(JC.TOMCAT_WEBAPPS_DIR)
  then
    ok_tomcat_webapps_dir = false
    error('not found tomcat/webapps: ' .. v2s(JC.TOMCAT_WEBAPPS_DIR))
  end
  ok_tomcat_webapps_dir = true
  return JC.TOMCAT_WEBAPPS_DIR
end

---@param conf table{configuration={path,url,server}}
---@param classname string --ci env.lang.ClassInfo
---@param classfile string -- absolute path to already compiled classfile
function M.copy_classfile_to_webapps(conf, classname, classfile)
  -- local path = absolute_path -- get_absolute_path_to_classfile(self, ci)
  if not fs.file_exists(classfile) then
    error('not found compiled classfile: ' .. v2s(classfile))
  end

  -- url = 'http://localhost:8080/manager/text'
  local webapp_classes_dir = fs.join_path(validated_tomcat_webapps_dir(),
    conf.configuration.path, "WEB-INF", "classes")

  if not fs.dir_exists(webapp_classes_dir) then
    error("not found webapp classes dir: " .. v2s(webapp_classes_dir))
  end
  local relpath = fs.classname_to_path(classname) .. '.class'
  local dst_path = fs.join_path(webapp_classes_dir, relpath)

  -- to ensure will copied
  if fs.file_exists(dst_path) then os.remove(dst_path) end

  local ok, err = fs.copy(classfile, dst_path)
  if not ok then
    error('on copy ' .. v2s(classfile) .. ' error: ' .. v2s(err))
  end
  return ok
end

---@param t table
---@param innerpath string? relative path in project_root from src/main/webapp/
---@param viewfile string absolute path
function M.copy_viewfile_to_webapps(t, innerpath, viewfile)
  assert(type(innerpath) == 'string', 'innerpath')
  if not fs.file_exists(viewfile) then
    error('not found view file' .. v2s(viewfile))
  end

  local conf = t.configuration
  local webapp_dir = fs.join_path(validated_tomcat_webapps_dir(), conf.path)
  local dst_path = fs.join_path(webapp_dir, innerpath)

  -- to ensure will copied
  if fs.file_exists(dst_path) then os.remove(dst_path) end

  local ok, err = fs.copy(viewfile, dst_path)
  if not ok then
    error('on copy ' .. v2s(viewfile) .. ' error: ' .. v2s(err))
  end
  notify('update ' .. v2s(innerpath))
  return ok
end

-- reload webapp deployed in tomcat
---@param tomcat_plugin_entry table{configuration={}} from maven pom.xml
function M.reload_webapp(tomcat_plugin_entry, opts)
  log_debug("reload")
  if (opts or E).reload == false then -- without reload tomcat
    notify('without tomcat reloading.')
    return true
  end

  assert(type(tomcat_plugin_entry) == 'table', 'tomcat_plugin_entry')
  local conf = tomcat_plugin_entry.configuration

  local sid = assert(conf.server, 'tomcat server id')
  assert(type(conf.creds) == 'table', 'has creds (user,password)')

  if type(conf.creds) ~= 'table' then
    notify('Not found creds in ' .. v2s(JC.GLOBAL_MAVEN_CONF_SETTINGS) ..
      " for server id: " .. v2s(conf.server))
    return false
  end

  local query = conf.url .. "/reload?path=" .. conf.path
  ---@param response_body table
  ---@param headers table?
  ---@param reqt table
  ---@diagnostic disable-next-line: unused-local
  local on_read = function(response_body, headers, reqt)
    notify(table.concat(response_body, "\n"))
  end

  notify(query .. " (" .. v2s(sid) .. ")") -- reloading...
  -- local ret = fs.execr(fmt("curl -v --user %s:%s %s/reload?path=%s 2>&1",
  --   conf.creds.user, conf.creds.password, conf.url, conf.path))
  M.send_query(tomcat_plugin_entry, query, on_read)

  return true
end

--[[
* Connected to localhost (::1) port 8080 (#0)
* Server auth using Basic with user 'war-deployer'
> GET /manager/text/reload?path=/mywebapp HTTP/1.1
> Host: localhost:8080
> Authorization: Basic <hashhere-encoded user+password>
> User-Agent: curl/7.74.0
> Accept: */*
>
< HTTP/1.1 200
< Cache-Control: private
< X-Frame-Options: DENY
< X-Content-Type-Options: nosniff
< Content-Type: text/plain;charset=utf-8
< Transfer-Encoding: chunked
< Date: Mon, 29 Jul 2024 06:40:21 GMT
<
OK - Reloaded application at context path [/mywebapp]
]]
---@param t table {configuration={url, path, creds{user, password}}}
---@param url string
---@param handler function?
function M.send_query(t, url, handler)
  local http = require("socket.http")
  local ltn12 = require("ltn12")

  local response_body = {}

  local turl = assert(socket_url.parse(url), 'parsed url')

  local reqt = {
    scheme = turl.scheme or 'http',
    method = 'GET',
    headers = { ['user-agent'] = 'nvim-env' },
    -- uri,
    sink = ltn12.sink.table(response_body),
    host = turl.host or 'localhost',
    port = turl.port or 8080,
    uri = turl.path .. '?' .. (turl.query or '')
  }

  local conf = assert((t.configuration), 'configuration')
  local creds = assert(conf.creds, 'configuration.creds')

  reqt.headers["authorization"] = "Basic " ..
      (mime.b64(creds.user .. ":" .. socket_url.unescape(creds.password)))

  local function send()
    local r, code, headers, status = http.request(reqt)
    if not r then
      log_debug('error code:%s(%s) req:%s h:%s', code, status, reqt, headers)
    end
    if type(handler) == 'function' then
      handler(response_body, headers, reqt)
    end
  end

  if type((vim or E).schedule) == 'function' then
    vim.schedule(send)
  else
    send()
  end
end

return M

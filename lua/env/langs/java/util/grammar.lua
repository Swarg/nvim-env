-- 08-08-2024 @author Swarg
-- Goal: parsing java sources in a streaming style when the entire file is not
-- immediately available and parsing occurs line by line.
--
-- https://www.inf.puc-rio.br/~roberto/lpeg/
--

local lpeg = require 'lpeg'
local loc = lpeg.locale()

local CONST = require 'env.langs.java.util.consts'
local Modifier = require 'env.langs.java.util.reflect.Modifier'

---@diagnostic disable-next-line: unused-local
local P, R, B, C, V, lpegS = lpeg.P, lpeg.R, lpeg.B, lpeg.C, lpeg.V, lpeg.S
---@diagnostic disable-next-line: unused-local
local Ct, Cc, Cg, Cp, Cf = lpeg.Ct, lpeg.Cc, lpeg.Cg, lpeg.Cp, lpeg.Cf

-- lpeg.B For making sure that a match is not preceded by a given pattern

local M = {}
M.debug = false


-- luarocks/dprint: optional use if installed (or automatically create a stub)
local ok_dprint, D = pcall(require, 'dprint') -- debugging print
if not ok_dprint then
  D = {
    _VERSION = 'stub-of-dprint',
    mk_dprint_for = function(_, _) return function(...) end end,
    mk_dvisualize_for = function(_, _) return function(...) end end,
    on = false,
  }
end
local enabled_by_default = false
local dprint = D.mk_dprint_for(M, enabled_by_default)
local dvisualize = D.mk_dvisualize_for(M, enabled_by_default)
--

local ID = 'ID'
local OP = 'OP'
local OPSEP = 'OPSEP' -- ;
local KEYWORD = 'KEYWORD'
local LITERAL = 'LITERAL'
local PACKAGE = 'PACKAGE'
local IMPORT = 'IMPORT'
local CLASSDEF = 'CLASSDEF'
local INNERCLASSDEF = 'INNERCLASSDEF'
local COMMENT = 'COMMENT'
local COMMENT_OPEN = 'COMMENT_OPEN'
local COMMENT_CLOSE = 'COMMENT_CLOSE'
local COMMENT_ML = 'COMMENT_ML' -- multiline comment  /* .. */
local ANNOTATION = '@'
local FIELD = 'FIELD'
local METHOD = 'METHOD'
local CONSTRUCTOR = 'CONSTRUCTOR'
-- part of constructor or method signature Type:argname,
local SIGNPARAMS = 'SIGNPARAMS'
local ARGUMENT = 'ARGUMENT' -- e.g.  used in method call
local TYPE = 'TYPE' -- (String) id
local THROWS = 'THROWS'
local EXTENDS = 'EXTENDS'
local IMPLEMENTS = 'IMPLEMENTS'
local STATIC_BLOCK = 'STATIC_BLOCK'

--
--------------------------------------------------------------------------------
--                              Debugging
--------------------------------------------------------------------------------
--
-- factory to build debug pattern
-- which, when debugging mode is enabled, will display the current position
-- while parsing input.
-- This can help in cases where it is not clear why the matching does not work
-- to find out where the process of working complex templates goes.
-- Note:
-- to reduce costs in normal mode, not in debug mode this factory creates an
-- empty pattern-stub that always passes and does not do any checks for debugging
-- Therefore, in order to enable debugging mode and see where template parsing
-- goes, you need to set require('dprint').on = true before loading this module
--
-- Example:
--   local D = require 'dprint'
--   D.on = true                                 -- before load this module
--   local parser = require 'jparser.grammar'
--   ..
--   D.enable_module(parser, true)
--   parser.parse(some_input)
--   ..
--   I'A' * lpeg.P("word1") * I'B' * lpeg.P("second") * I'C'
--
---@param tag string
local I = function(tag)
  if M.debug then -- dprint is on globaly
    return lpeg.P(function(matchedLine, curr_pos)
      dvisualize(matchedLine, curr_pos, curr_pos, 'TAG:', tag)
      return true
    end)
  end

  return lpeg.P(true) -- pattern what always pass, not affeted current pos
end

--------------------------------------------------------------------------------

local EOF = P(-1)
local S = lpegS' \t\v\n\f\r' -- whitespace (the set of chars)
local S0 = S^0 -- any count of spaces
local S1 = S^1 -- one or more spaces
local SPACES = lpegS' \t\v\f' -- whitespace without new-nline

local digit = R'09'
local uppderLetter = R('AZ')
local letter = R('az', 'AZ') + P'_'
local alphanum = letter + digit
local dot = P'.'
local hex = R('af', 'AF', '09')
local exp = lpegS'eE' * lpegS'+-'^-1 * digit^1
local fs = lpegS'fFlL'
local is = lpegS'uUlL'^0

local hexnum = P'0' * lpegS'xX' * hex^1 * is^-1
local octnum = P'0' * digit^1 * is^-1
local decnum = digit^1 * is^-1
local floatnum = digit^1 * exp * fs^-1 +
                 digit^0 * P'.' * digit^1 * exp^-1 * fs^-1 +
                 digit^1 * P'.' * digit^0 * exp^-1 * fs^-1
local pNumLit = hexnum + octnum + floatnum + decnum

local pCharLit =
  P'L'^-1 * P"'" * (P'\\' * P(1) + (1 - lpegS"\\'"))^1 * P"'"

local pStringLit =
  P'L'^-1 * P'"' * (P'\\' * P(1) + (1 - lpegS'\\"'))^0 * P'"'

local pCommentML = P'/*' * (P(1) - P'*/')^0 * P'*/' -- multiline
local pCommentOL = P'//' * (P(1) - P'\n')^0 -- oneline
local pComment = Cc(COMMENT) *  (pCommentML + pCommentOL)

local ccomment_open = P'/*' * (1 - P'*/')^0 * - P(1) -- /* to end of the line
local ccomment_close = (P(1) - P'*/' - P'/*')^0 * P'*/' * S0 * -P(1) -- /* to end of the line

local pLiteral = -- I'LiteralO'*
(pNumLit + pCharLit + pStringLit)
-- I'LiteralC'

local cpLiteral = Cc(LITERAL) * pLiteral

--
local pString = P'"' * (P(1) - P'"')^0 * P'"'
local pDocString = P'"""' * (P(1) - P'"""')^0 * P'"""'

local pComment0 = pCommentML + pCommentOL

-- skip everything that can escape parentheses
local pSkipEscaped = pDocString + pString + pComment0

-- recursive grammar
-- () "("  """\n ( """   "*" and """*""" `/* ( */ `  and `// ) \n`
local pBalancedParentheses = lpeg.P { "RuleBP",
  RuleBP = P"(" * ((P(1) - lpeg.S'()"/') + V'RuleBP' + pSkipEscaped)^0 * P")"
}

local pBalancedCurlyBraces = lpeg.P { "RuleBCB",
  RuleBCB = P"{" * ((P(1) - lpeg.S'{}"/') + V'RuleBCB' + pSkipEscaped)^0 * P"}"
}

local pUntilOpenCurlyBraces =
  ((P(1) - lpeg.S'{"/') + pSkipEscaped)^0

-- find the next command separator(;, taking into account "string" and comments
local pFindOpSep =
  ((P(1) - lpeg.S';{}"/') + pSkipEscaped + pBalancedCurlyBraces)^0 * P";"
--

local pFindEndOfMultilineString =
  (P(1) - P'"""')^0 * P'"""'

-- pattern to add current position (end-of-line) to indicate need next line
local pIsEOFP = EOF * function(_, pos)
  return pos, pos -- not closed
end

-- build pattern to find given substring in any source line
--
-- Usage Example:
--
--   local col = lpeg.match(line, patt)
--   local start_pos = col - #str_to_find
--   local end_por = col -1
--   sub = line:sub(start_pos, end_pos) -- assert(sub == str_to_find)
--
---@return userdata pattern
function M.build_patt_find_outside_string_and_comments(str_to_find)
  return ((P(1) - (lpeg.S'"/'+P(str_to_find))) + pSkipEscaped)^0 *
    P(str_to_find)
    -- Cp() * P(str_to_find) * Cp()
  -- ^ start                 ^end pos
end


-- local kwAccessModifiersList = { 'private', 'protected', 'public', }
local kwClassType = {
  'class', 'interface', 'enum', 'record',
  '@interface' -- annotation
}

local pOperator = P('!=') + '!' + '==' + '=' + '+='+'-='+
    ';' + '{' + '}' + '(' + ')' + '[' + ']' + '@' +
    '<<' + '<=' + '<-' + '<' + '>>' + '>=' + '>' + '->' +
    '--' + '-' + '++' + '+' + '&&' + '&' + '||' + '|' +
    '^' + '*' + '/' + '%' + '?' -- + '.'

local pCompOp = P('==') + '!=' +
    '<<' + '<=' + '<' + '>>' + '>=' + '>'

local pLogicOp = P('||') + '!' + '&&' + '||'

-- or elm1 or elm2 ... for keywords
local function buildKWPatt(list)
  local patt = lpeg.P(false) -- just empty pattern for appending
  for _, w in ipairs(list) do patt = patt + w end
  return patt * -loc.alnum
end

local pKeyword = buildKWPatt(CONST.JAVA_KEYWORD_LIST)
-- local pKeyword = lpeg.P(false) -- ReservedWords
-- for _, w in ipairs(keyword_list) do pKeyword = pKeyword + w end
-- pKeyword = pKeyword * -loc.alnum
local cpKeyword = --[[Cc(KEYWORD) * ]] C(pKeyword)

local pIdentifier = -- I'IdentifierOpen'*
(letter * alphanum^0 - pKeyword * (-alphanum))
-- * I'IdentifierClose'

-- org.comp.MainClass.InnerClass
-- org.comp.MainClass$AnnonimInnerClass
local pGlobalIdentifier = pIdentifier * ((P'.' + P'$') * pIdentifier)^0
local pGlobalIdentifierClass = pGlobalIdentifier * P'.class'

-- byte int short long float double boolean
local pPrimitiveType = buildKWPatt(CONST.JAVA_PRIMITIVE_TYPE_LIST)

local pClassType = buildKWPatt(kwClassType)
local pAccessModifier = buildKWPatt(Modifier.ACCESS_MODIFIERS)
local pClassModifier = buildKWPatt(Modifier.CLASS_MODIFIERS)
local pFieldModifier = buildKWPatt(Modifier.FIELD_MODIFIERS)
local pMethodModifier = buildKWPatt(Modifier.METHOD_MODIFIERS)
local pConstructorModifier = buildKWPatt(Modifier.CONSTRUCTOR_MODIFIERS)


-- c- capture p-pattern
local cpOpSep = S0 * C(P';')
local cpCBO = C(P'{') -- curly brace open
local cpCBC = C(P'}') -- curly brace close

local cpCommentOpen = Ct(Cc(COMMENT_OPEN) * ccomment_open * Cp())

local pShortClassName =
  (uppderLetter * alphanum^0 - pKeyword *(-alphanum))

local pShortInnerClassName = -- Class$Inner
  (uppderLetter * alphanum^0 * (P'$' * alphanum)^-1 - pKeyword *(-alphanum))

local pMethodName =
  ((R('az')+P'_') * alphanum^0 - pKeyword *(-alphanum)) * -P'.'

-- full class name aka pkg.comp.ClassName pkgClass
local pPackageId = (letter * (alphanum + dot) ^0 - cpKeyword * (-alphanum))

local cpPackage = Ct(Cc(PACKAGE) *
  P'package' * S1 * C(pPackageId) * S0 * cpOpSep)

local cpImport =  Ct(Cc(IMPORT) *
  P'import' * S1 * C((P'static'*S1)^-1 * pGlobalIdentifier * P'.*'^-1) * S0 * cpOpSep)

-- (public|protected|private|-) [static]
local cpClassModifier = C(pClassModifier)
-- "static"-keyword can use only for inner class not a main per-file class!
local cpInnerClassModifier = C(pClassModifier + P'static')

-- java.lang.reflect.Modifier.class: 392
local cpConstructorModifier = C(pConstructorModifier)
local cpMethodModifier = C(pMethodModifier)

-- TODO add all possible cases
-- (public|protected|private|-) [static] [native] volatile


local cpClassType = C(pClassType) -- class,interface, enum, record


-- recursive grammar rule to match Classes and Parametrized Classes(Types)
-- for exmaple: Map<Enty<String, Object>>
-- Object...
-- todo proper naming for OneObjType
--
local pType = lpeg.P{"Type",
  ParamTypeDetails = S1 * (P'extends' + P'super') * S1 * V'ObjType',
  OneObjType = (P'?' + V'ObjType') * V('ParamTypeDetails')^-1,
  ParametrizedType = P'<'* V'OneObjType' * (P','*S0 * V'OneObjType')^0 * P'>',
  ObjType = pGlobalIdentifier * V('ParametrizedType')^-1, --* (P'...')^-1,
  Type = (V'ObjType' + pPrimitiveType) * P'[]'^0,
}
-- only for type of the lastMethodParam
local pTypeVarArgs = pType * P'...'
M.pType = pType;


-- not empty list of (parametrized) Classes
local pTypeList = Ct(C(pType) * (P',' * S0 * C(pType) )^0)

local pClassExtends =
  S0 * Ct(Cc(EXTENDS) * P'extends' * S1 * C(pType)) * -P','

local pClassImplements =
  S0 * Ct(Cc(IMPLEMENTS)* P'implements' * S1 * pTypeList)

local pClassImplementsAndExtends = S0*(pClassImplements + pClassExtends)^-2


-- can has issue with final
local cpClassDefinition = Ct(Cc(CLASSDEF) *
  (cpClassModifier * S1)^0 * cpClassType * S1 * C(pShortClassName) * S0 *
  pClassImplementsAndExtends * S0 *
  (pIsEOFP + cpCBO)
)

local cpInnerClassDefinition = Ct(Cc(INNERCLASSDEF) *
  (cpInnerClassModifier * S1)^0 * cpClassType * S1 * C(pShortClassName) * S0 *
  (pIsEOFP + cpCBO)
)

local pLiteralListNotEmpty =
P'{' * S0 * Ct((P','^0 * S0 * C(pLiteral))^1) * S0 * P'}'

local pGlobalIdentifierListNotEmpty =
P'{' * S0 * Ct((P','^0 * S0 * C(pGlobalIdentifier))^1) * S0 * P'}'

local pGlobalIdentifierClassListNotEmpty =
P'{' * S0 * Ct((P','^0 * S0 * C(pGlobalIdentifierClass))^1) * S0 * P'}'


-- for @Ann(key=ANN_VALUE)  and  for @Ann(ANN_VALUE)
local pAnnotationParamValue =
   pGlobalIdentifierClass +                   -- @Ann(MyClass.class)
   pGlobalIdentifier +                        -- @Ann(Cls.LifeCycle.PER_METHOD)
   pLiteral                                   -- @Ann("literal")
   +
   pGlobalIdentifierClassListNotEmpty +       -- @Ann({Some.class, B.class})
   pGlobalIdentifierListNotEmpty +            -- @Ann({pkg.Some, pkg.util.A})
   pLiteralListNotEmpty                       -- @Ann({"abc", "de"})


local cpKWPair = -- I'KWPairOpen' *
  Cg(I'KO'* S0 * C(pIdentifier) * I'KC'* -- left side   aka key =
      S0 * P'=' *                        -- assign k=v
      I'VO'* S0 * C(pAnnotationParamValue) *I'VC' -- right side "= value"
  )
  -- * I'KWPairClose'

local cpKWPairListNotEmpty = -- I'KWPairListNEOpen' *
Ct((P','^0 * S0 * Ct(cpKWPair))^1)
-- *I'KWPairListNEClose'

local cpAnnotationParam =  S0 * (
  cpKWPairListNotEmpty                   -- @Ann(k=1,b="str")   ! no {}
  +
  pAnnotationParamValue                  -- @A("liter"), @A(MyCls.class), ...
  / function(v) return { "value", v } end
) * S0


-- NOTE:
-- seems what "{}" is used only as list of some elements inside the ann. body
local cpAnnotationBody = -- I'AnnotationBodyOpen' *
S0 * P'(' * S0 * cpAnnotationParam^0 * S0 * P')'
-- *I'AnnotationBodyClose'


-- consume '(' but return its position in the matched line
local pBOpenPos = S0 * P'(' * lpeg.Cp() / function(pos, ...) return pos - 1, ... end


local cpAnnotation = S0 * Ct(Cc(ANNOTATION) *
  P'@' * C(pGlobalIdentifier) * S0 * (
    cpAnnotationBody  -- all annotation body in one line (can be empty)
    + pBOpenPos       -- opened ( without closing ) in the same line (continue)
    --^ sign what this annotation-body has multiline definition case: "(\n ...)"
    + -P'('           -- for case when @Annotation does not has`()` at all
  )
)

local cpAnnotationFull = S0 * Ct(Cc(ANNOTATION) *
  P'@'*C(pGlobalIdentifier) * S0 * cpAnnotationBody^-1)


-- elements that can appear in the class header before class signature
-- to parse java source file from package to line with class definition
-- TODO Add parsing for extends & implement for classes
M.cpClassHeaderParts = -- I'ClassHeaderParts' *
(I'HeaderPart' * (cpCommentOpen + Ct(pComment)
  + cpPackage
  + cpImport
  + cpAnnotation
  + cpClassDefinition + cpCBO + cpCBC + S) )^0

M.fpClassHeader = Ct(M.cpClassHeaderParts)


local pOpSep = S0 * P';'
local pArithmeticAction = S0 * (lpegS'+-*/%') * S0
local pGId = pGlobalIdentifier

local cpAssigmentRightPart = -- P(false) -- todo  filed = ""; * cpOpSep
S0 * C(P'=') * S0 * C(
  pIsEOFP -- onli continue
  + P'"""' * pIsEOFP                  -- MultiLine String with continue
  + pGlobalIdentifierClass * pOpSep   -- MyClass.class
  + P'new ' * pType * pBalancedParentheses * pOpSep -- new A(); or new A(...);

  -- String a = PREFIX + "abc";  or int x = 0;
  + (pGId + pLiteral + P'-'^0*pNumLit) * (pArithmeticAction * (pGId + pLiteral))^0 * pOpSep
);

local cpClassFieldDefinition = Ct(Cc(FIELD) * S0 *
  (C(pFieldModifier) * S1)^0 * C(pType) * S1 * C(pIdentifier) * S0 *
  (pIsEOFP + cpOpSep + cpAssigmentRightPart)
)
--  ^
--   `check for CONTINUE (multiline definition)


M.fpClassField = cpClassFieldDefinition

-- for constructors and methods:  public void doStuff() throws Exception, B {
local pThrows = S0 * Ct(Cc(THROWS) * P'throws' * S1 * pTypeList)
local pOptThrowsAndCp = pThrows^0 * S0 * lpeg.Cp()

 -- modifiers + classname without "("
local pConstructorSignatureBegin = -- I'ConstructorSignatureBeginOpen' *
  S0 * (cpConstructorModifier * S1)^-1 * C(pShortClassName)
-- * I'ConstructorSignatureBeginClose'

local pMethodSignatureBegin =
  S0 * (cpMethodModifier * S1)^0 * C(pType + P'void') * S1 * C(pMethodName)

local pSignatureMethodParam =
  -- Ct(S0 * (cpAnnotationFull * S0)^0 * C(pType + pTypeVarArgs *-(S0*P',')) * S1 * C(pIdentifier))
  Ct(S0 * (cpAnnotationFull * S0)^0 * (C(P'final') * S1)^-1 *
            (C(pType) * S1 * C(pIdentifier))+
            (C(pTypeVarArgs) * S0 * C(pIdentifier) * -(S0*P',')))

local pSignatureMethodParamsFull = Ct(Cc(SIGNPARAMS) *
  S0 * P'('
    -- list of params pairs(Type, name) can be empty
  * S0 * (pSignatureMethodParam * (P','*S0 * pSignatureMethodParam)^0)^-1
  * S0 * P')'
)
* pThrows^-1
* S0 * Cp() -- each parameter can be on a separate line - end position of it

-- the case when the definition is split into several lines
local pSignatureMethodParamsOnlyOpen = pBOpenPos -- position of opened (

---- to parse source code inside methods

-- (String, int, int, Object[])
M.pSignatureParamTypes =
  S0 * C(pType) * (S0*P','*S0* C(pType))^0

local pThisOrSuper = P'this.'+P'super.'

local pMethodCallRaw =
  pThisOrSuper^-1 * (pIdentifier * (P'.'+P'$'))^0
  * pMethodName * S0 * pBalancedParentheses

local pValue =
  pMethodCallRaw
  + P'new' * S1 * pType * S0 * pBalancedParentheses --pShortClassName
  + pLiteral + pDocString + P'-'*pNumLit
  + pThisOrSuper^-1 * pGlobalIdentifier -- field or constant
  + pGlobalIdentifierClass
-- lambda () -> {}

local pArrayIndex = S0* P'['* S0 * pValue *S0* P']'

local pValueEx =
  pValue * pArrayIndex^0

local pExp =
  pValueEx * (S0 * (lpegS('-+*/%') + pCompOp + pLogicOp) * S0 * pValueEx)^0

-- to split arguments of the method call into string parts without depth parsing
M.pMethodCall =
  S0*C(pMethodName)*S0* Ct(P'('*S0*C(pExp) *(S0*P','*S0*C(pExp))^0 *S0*P')')

----


M.fpClassConstructorBegin = Ct(Cc(CONSTRUCTOR) * -- I'ConstructorO' *
  pConstructorSignatureBegin *                   -- public MyClass + spaces
  (pSignatureMethodParamsFull + pSignatureMethodParamsOnlyOpen)
  -- * I'ConstructorC'
)

M.fpClassMethodBegin = Ct(Cc(METHOD) *
  pMethodSignatureBegin *                   -- public <type> methodName + spaces
  (pSignatureMethodParamsFull + pSignatureMethodParamsOnlyOpen)
)


M.cpClassBodyParts = (cpCommentOpen + pComment +
  cpAnnotation +
  cpInnerClassDefinition +
  M.fpClassField +
  M.fpClassConstructorBegin + -- signature of the cunstructor
  M.fpClassMethodBegin +      -- signature of the method
  cpCBO + cpCBC -- + S
)^0

M.fpClassBody = S0 * Ct(M.cpClassBodyParts)

--------------------------------------------------------------------------------
--                        for offhand parsing
--------------------------------------------------------------------------------
local oh = {}


---@param t table
---@param s number     Cp()
---@param word string  C(patt)
---@param e number     Cp()
local function foldWithPos(t, s, word, e)
  t[#t + 1] = { word, s, e - 1}
  return t
end

-- split current line by operators and brackets
oh.sep = pOperator + lpegS('",()[]{}:/@')

-- split the entire string into lexemes, taking into account:
-- "simple strings", comments, Types
oh.cpLexemsWithPos = Ct(
  (S1
  + Cp() * C(pString) * Cp() % foldWithPos
  + Cp() * C(pCommentML) * Cp() % foldWithPos
  + Cp() * C(pCommentOL) * Cp() % foldWithPos
  + Cp() * C(P'<>' + P'()' + P'[]' + P'this.') * Cp() % foldWithPos
  + Cp() * C(pType) * Cp() % foldWithPos
  + ((Cp() * C(oh.sep) * Cp()) % foldWithPos)
  + ((Cp() * C( (P(1) -(S1+oh.sep))^1 ) * Cp()) % foldWithPos)
  --            ^^^^^^^^^^^^^^^^^^^^^ any chars except space and seps
)^0
* EOF)


-- goal: split raw worlds without any container
oh.sep2 = pOperator + lpegS('",()[]{}:./@')
oh.cpRawLexemsWithPos = Ct(
  (S1
  + Cp() * C(pString) * Cp() % foldWithPos
  + Cp() * C(pCommentML) * Cp() % foldWithPos
  + Cp() * C(pCommentOL) * Cp() % foldWithPos
  + Cp() * C(P'<>' + P'()' + P'[]' + P'this.') * Cp() % foldWithPos
  + Cp() * C(pIdentifier) * Cp() % foldWithPos
  + ((Cp() * C(oh.sep2) * Cp()) % foldWithPos)
  + ((Cp() * C( (P(1) -(S1+oh.sep2))^1 ) * Cp()) % foldWithPos)
  --            ^^^^^^^^^^^^^^^^^^^^^ any chars except space and seps
)^0
* EOF)

---@param t table
---@param s number     Cp()
---@param word string  C(patt)
---@param e number     Cp()
local function foldTagWithPos(t, tag, s, word, e)
  t[#t + 1] = { tag, word, s, e - 1}
  return t
end
-- to parse method call with arguments or without
local pMethodCall0 =
  S0* Cc(METHOD) * Cp() * C(pMethodName) * Cp() % foldTagWithPos
  * S0*P'('
      * (S0* Cc(ARGUMENT)*Cp()*C(pExp)*Cp() % foldTagWithPos -- argument1
      * (S0*P','*S0* Cc(ARGUMENT)*Cp()*C(pExp)*Cp() % foldTagWithPos)^0 -- rest args
      )^-1
  *S0*P')'

oh.pMethodCallWithPos = Ct(pMethodCall0) * Cp()

-- method().with().chained();
oh.pMethodCallChainedWithPos =
 Ct(pMethodCall0 * (dot * pMethodCall0)^0
    *(S0* Cp()*P';' % function(t, s) t[#t+1] = { OPSEP, s }; return t end)^-1
   ) * Cp()

----

local pCountNewLine = P"\n" % function(t)
  t.lnum = (t.lnum or 0) + 1
end

local pSimpleValueWithTagPos =
  S0 *Cc(LITERAL) *Cp()*C(pLiteral + pDocString + P'-'*pNumLit)*Cp() % foldTagWithPos

local pLocVarDefinitionWithPos  =
   SPACES^0 *Cc(TYPE)*Cp()* C(pType) *Cp() % foldTagWithPos
  *SPACES^1 *Cc(ID)  *Cp()* C(pIdentifier) *Cp() % foldTagWithPos
  *S0 *P';'

-- like String varname = "value";
local pLocVarDefinitionWithAssignmentWithPos  =
   SPACES^0 *Cc(TYPE)*Cp()* C(pType) *Cp() % foldTagWithPos
  *SPACES^1 *Cc(ID)  *Cp()* C(pIdentifier) *Cp() % foldTagWithPos
  *S0 *P'='    *pSimpleValueWithTagPos
  *S0 *P';'

local pLocVarAssignmentWithPos  =
   SPACES^0 *Cc(ID) *Cp()* C(pIdentifier) *Cp() % foldTagWithPos
  *S0 *P'='   *pSimpleValueWithTagPos
  *S0 *P';'

local function normalize_var_lexeme(t, t0)
  if not t0 then return t end
  local entry = {}
  for i = 1, #t0 do
    local lexeme = t0[i][1]
    local pos_s = t0[i][3]
    local pos_e = t0[i][4]
    if lexeme == TYPE then entry.typ = { t0[i][2], pos_s, pos_e }
    elseif lexeme == ID then entry.varname = { t0[i][2], pos_s, pos_e }
    elseif lexeme == LITERAL then entry.value = { t0[i][2], pos_s, pos_e }
    end
  end
  t = t or {} -- ?
  t[#t + 1] = entry
  return t
end

oh.pLocalVarAssignmentsWithPos = -- P(false)
  Ct((
      Ct(pLocVarDefinitionWithPos) % normalize_var_lexeme
    + Ct(pLocVarDefinitionWithAssignmentWithPos) % normalize_var_lexeme
    + Ct(pLocVarAssignmentWithPos) % normalize_var_lexeme
    + (P(1) - P"\n" * pCountNewLine) -- skip another lines
    + (P"\n" * pCountNewLine) -- skip empty lines
  )^0)



M.offhand = oh
--------------------------------------------------------------------------------

M.tags = {
  ID = ID,
  OP = OP,
  OPSEP = OPSEP, -- ;
  KEYWORD = KEYWORD,
  LITERAL = LITERAL,
  PACKAGE = PACKAGE,
  IMPORT = IMPORT,
  CLASSDEF = CLASSDEF,
  COMMENT = COMMENT,
  COMMENT_OPEN = COMMENT_OPEN,
  COMMENT_CLOSE = COMMENT_CLOSE,
  ANNOTATION = ANNOTATION,
  FIELD = FIELD,
  METHOD = METHOD,
  CONSTRUCTOR = CONSTRUCTOR,
  SIGNPARAMS = SIGNPARAMS,
  ARGUMENT = ARGUMENT,
  TYPE = TYPE,
  STATIC_BLOCK = STATIC_BLOCK,
  COMMENT_ML = COMMENT_ML,
  THROWS = THROWS,
  EXTENDS = EXTENDS,
  IMPLEMENTS = IMPLEMENTS,
}

M.p = {
  EOF = EOF,
  whitespace = S,
  cpLiteral = cpLiteral,
  pOperator = pOperator,
  pPackageId = pPackageId,
  pShortInnerClassName = pShortInnerClassName,
  pIdentifier = pIdentifier,
  pGlobalIdentifier = pGlobalIdentifier,
  pGlobalIdentifierClass = pGlobalIdentifierClass,
  pCommentOL = pCommentOL, --  // \n
  pCommentML = pCommentML, --  /* */
  ccomment_open = ccomment_open,
  ccomment_close = ccomment_close,

  pAccessModifier = pAccessModifier,
  pClassModifier = pClassModifier,
  cpClassModifier = cpClassModifier,
  cpClassDefinition = cpClassDefinition,
  cpInnerClassDefinition = cpInnerClassDefinition,
  pTypeList = pTypeList,
  pClassImplements = pClassImplements,
  pClassExtends = pClassExtends,
  pClassImplementsAndExtends = pClassImplementsAndExtends,

  pMethodName = pMethodName,
  pMethodSignatureBegin = pMethodSignatureBegin,
  cpMethodModifier = cpMethodModifier,
  cpConstructorModifier = cpConstructorModifier,

  cpClassFieldDefinition = cpClassFieldDefinition,
  cpAssigmentRightPart = cpAssigmentRightPart,
  cpInnerClassModifiers = cpInnerClassModifier,

  cpAnnotation = cpAnnotation,
  cpAnnotationBody = cpAnnotationBody,
  cpAnnotationParam = cpAnnotationParam,
  cpAnnotationFull = cpAnnotationFull,
  cpKWPairListNotEmpty = cpKWPairListNotEmpty,
  cpImport = cpImport,

  pSignatureMethodParam = pSignatureMethodParam,
  pSignatureMethodParamsFull = pSignatureMethodParamsFull,
  pSignatureMethodParamsOnlyOpen = pSignatureMethodParamsOnlyOpen,
  pType = pType,
  pTypeVarArgs = pTypeVarArgs,
  pThrows = pThrows,
  pOptThrowsAndCp = pOptThrowsAndCp,

  D = D,
  dprint = dprint,
  dvisualize = dvisualize,
  --
  --
 pMethodCallRaw = pMethodCallRaw
}

M.lpeg = lpeg
M.pBalancedParentheses = pBalancedParentheses
M.pBalancedCurlyBraces = pBalancedCurlyBraces
M.pUntilOpenCurlyBraces = pUntilOpenCurlyBraces
M.pFindOpSep = pFindOpSep
M.pFindEndOfMultilineString = pFindEndOfMultilineString


--[[
Noteds:

static+abstract only for nested class:

public class EnclosingClass {
  public static abstract class LocationResult{
    public abstract void gotLocation(Location location);
  }
}
]]

return M

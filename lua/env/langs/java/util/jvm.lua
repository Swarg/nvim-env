-- 02-10-2024 @author Swarg
-- Goals:
--  - build list of all running jvm in the current system (via Unix ps command)
--  - make jvm-list readable
--  - find jvm-entry (parsed ps output with jvm's) to get the pid of the jvm
--  - keep cache(mapping) with installed jdk in the current OS
--    (path2java -> version + version -> path2java)
--  - get java version(by specified system path to executable java) with caching
--

local log = require 'alogger'
local fs = require 'env.files'
local su = require 'env.sutil'

local M = {}
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--
-- unix
--
---@param filter string?
---@return table{pid,cpu,mem,etime,cmd,args}
function M.get_jvm_list(filter)
  -- UID          PID    PPID  C STIME TTY          TIME CMD
  -- local lines = fs.execrl('ps --no-headers -exo "pid,ppid,etime,%cpu,%mem,cmd"')
  local lines = fs.execrl('ps --no-headers -eo "pid,%cpu,%mem,etime,cmd"')
  local t = {}
  for _, line in ipairs(lines) do
    if string.find(line, 'java', 1, true) then
      local e = M.parse_ps_entry(line)
      if e and e.cmd and string.find(e.cmd, 'java', 1, true) then
        if not filter or filter == '' or match(e.cmd, filter) then
          t[#t + 1] = e
        end
      end
    end
  end
  return t
end

--
-- create reabadle from get_jvm_list output
--
---@param home string?
---@param list table? for testing
---@return table:string
---@return table{pid, cmd}
function M.get_readable_jvm_list(home, list)
  list = list or M.get_jvm_list(nil)
  local t = {}
  for _, entry in ipairs(list) do
    t[#t + 1] = M.jvm_entry_to_short_string(entry, home)
  end
  return t, list
end

--
-- to find jvm-entry from get_jvm_list
--
---@param elist table
---@param name string the name or pattern of jvm cmd|args to be found
---@param plain boolean? true - to use name as lua-regex-pattern
function M.find_jvm_idx_by_name(elist, name, plain)
  log_debug("find_jvm_idx_by_name:%s plain:%s", name, plain)
  if name ~= nil and #name > 2 then
    for idx, e in ipairs(elist) do
      -- e is pid, cpu, mem, etime, cmd, args
      if type(e) == 'table' and type(e.args) == 'table' then
        if M.match_jvm_name(e.cmd, e.args, name, plain) then
          log_debug('found index:%s %s', idx, e.cmd, e.args[#e.args])
          return idx
        end
      end
    end
  end
  return nil
end

--
-- Unix
--
---@param s string
function M.parse_ps_entry(s)
  if s and s ~= '' then
    local words = su.split(s, ' ')
    local e = {
      pid = tonumber(words[1]),
      cpu = words[2],
      mem = words[3],
      etime = words[4],
      cmd = words[5],
      args = {}
    }

    local i = 5 -- starts from 6th word (1th arg)
    while i <= #words do
      i = i + 1
      local arg = words[i]
      if arg and arg:sub(1, 1) == "'" then
        arg = arg:sub(2);
        if arg:sub(-1, -1) == "'" then
          arg = arg:sub(#arg - 1)
        else
          while i <= #words do
            i = i + 1
            local arg0 = words[i]
            if arg0 then
              if arg0:sub(-1, -1) == "'" then
                arg = arg .. ' ' .. arg0:sub(1, #arg0 - 1)
                break
              end
              arg = arg .. ' ' .. arg0
            end
          end
        end
      else
      end
      e.args[#e.args + 1] = arg
    end
    return e
  end
  return nil
end

--
-- reduce as much as possible the entry about jvm while maintaining meaning data
-- that can be used to determine what is running in this jvm.
--
---@param t table
---@param home string?
---@return string
function M.jvm_entry_to_short_string(t, home)
  local has_home = type(home) == 'string' and home ~= ''
  local s = fmt('%-8s %-4s %-4s %-8s %s',
    v2s(t.pid), v2s(t.cpu), v2s(t.mem), v2s(t.etime), v2s(t.cmd))

  if #(t.args or E) > 0 then
    local n = 0
    while n < #t.args do
      n = n + 1
      local arg = t.args[n]
      if type(arg) == 'string' then
        if arg == '-cp' or arg == '--classpath' then
          n = n + 1
        elseif arg == '-jar' then
          s = s .. ' -jar'
        elseif arg:sub(1, 1) ~= '-' then
          if has_home and arg:sub(1, #home) == home then
            arg = '~' .. arg:sub(#home + 1, #arg)
          end
          s = s .. " " .. arg
          -- hide value of the optkey
        elseif arg:sub(1, 1) == '-' and (t.args[n + 1] or ''):sub(1, 1) ~= '-' then
          if string.find(arg, '[=:]', 1, false) == nil then
            n = n + 1
          end
        end
      end
    end
  end

  return s
end

--
-- used to find jvm by some given substring(name) like "Gradle Test"
---@param cmd string
---@param args table
---@param patt string - the name(substring) to match jvm from other in ps list
---@param plain boolean?
---@return boolean
function M.match_jvm_name(cmd, args, patt, plain)
  if not patt or #patt == 0 then
    return false
  end
  cmd = cmd
  if plain == nil then plain = true end

  if type(args) == 'table' then
    for i = #args, 1, -1 do
      local arg = args[i]
      if arg == '-cp' or arg == '--classpath' then
        break
      end
      if string.find(arg, patt, 1, plain) then
        return true
      end
    end
  end
  return false
end

--

-- cache to hold a paths to java-executable - associated with it versions
local version_mapping_cache = {
  -- abs path = version
  -- (string)binjava -> (number)version
  -- (number)version -> (string)binjava
}

--
--
function M.clear_version_mapping()
  version_mapping_cache = {}
end

function M.get_version_mapping()
  return version_mapping_cache
end

--
--
-- get java version from the path to java
-- (with caching via M.version_mapping_cache
--
-- 1. try to figure out the version from the cached path like /opt/java17/bin/java
-- 2. ask via system command `java -version`
--
---@param binjava string - path to executable java
---@return number? major version (8,11,17,21...)
function M.get_java_version(binjava)
  log_debug("get_java_version", binjava)
  if not binjava or binjava == "" or not string.find(binjava, "java") then
    return nil
  end

  local version = version_mapping_cache[binjava or false]
  if not version and binjava then
    version = match(binjava, "java(%d+)") -- aka /opt/java/java17/bin/java
    if not version or version == "" then
      version = M.ask_java_version(binjava, true)
    end

    if version then
      version = tonumber(version) -- major number like 8,11,17,21
      assert(version, 'expected number version got: ' .. v2s(version))

      version_mapping_cache[binjava] = version
      version_mapping_cache[version] = binjava
    end
  end

  return version
end

--
-- based on version_mapping_cache
--
---@param javabin string
---@param major_only boolean
---@return string?
function M.ask_java_version(javabin, major_only)
  log_debug("ask_java_version", javabin)
  assert(type(javabin) == 'string', 'path/to/java')

  local lines = fs.execrl(javabin .. " -version 2>&1")
  return M.parse_java_version(lines, major_only)
end

--
--
--
---@param lines table output from java -version
---@param major_only boolean
---@return string?
function M.parse_java_version(lines, major_only)
  local fline = (lines or E)[1]
  if not fline then
    return nil
  end
  -- output from `java -version`
  local version = string.match(fline, '%sversion%s"([^"]+)"') -- 8 - 17
  -- output from `java --version`
  if not version then
    version = string.match(fline, '^[^%s]+%s(%d+%.%d+%.%d+)%s') -- 21+
  end
  log_debug('version:%s from output:%s', version, fline)

  if version and version ~= "" and major_only then
    local major, minor = string.match(version, "^(%d+).(%d+)")
    if major and minor then
      if major == "1" then
        version = minor -- 1.8. ..
      else
        version = major -- 17.0. ..
      end
    end
  end

  return version
end

--

return M

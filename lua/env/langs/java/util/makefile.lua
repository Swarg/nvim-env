-- 06-09-2024 @author Swarg
--

local log = require 'alogger'
local utempl = require 'env.lang.utils.templater'

local M = {}

local substitute = utempl.substitute
local append = utempl.append
local set_if_empty = utempl.set_if_empty


local TEMPL_SPRING_BOOT_WEBAPP_MVN = [[
ARTIFACT=${_ARTIFACT}
VERSION=${_VERSION}

#rellative path in servlet-container and url
WEBAPP_ROOT=${_WEBAPP_ROOT}
PORT=${_PORT}

# for debugging
JPDA_TRANSPORT=dt_socket
JPDA_ADDRESS=127.0.0.1:5005
JPDA_SUSPEND=n
JDWP_OPTS=transport=$(JPDA_TRANSPORT),address=$(JPDA_ADDRESS),server=y,suspend=$(JPDA_SUSPEND)
JPDA_ARG=-agentlib:jdwp=$(JDWP_OPTS)

${_DEV_DEPENDENCIES}

.PHONY: build clean-build run run-jar debug debug-jar test ${_PHONY}


${_DEV_DEPENDENCIES_INSTALL}

${_DB_INIT_CMDS}


# Command Interface

build:
	mvn package

clean-build:
	mvn clean package

run:
	mvn ${_MVN_RUN_CMD}

run-jar:
	java -jar ./target/$(ARTIFACT)-$(VERSION).jar

${_TARGET_DEBUG}

debug-jar:
	java $(JPDA_ARG) -jar ./target/$(ARTIFACT)-$(VERSION).jar

${_RUN_TEST}

${_EX_COMMANDS}
]]

--------------------------------------------------------------------------------


-- dev_dependencies
local qdb_repo = [[
# this script used qdb cli tool what can be downloaded from (see install-qdb)
QDB_URL=https://gitlab.com/Swarg/dotfiles/-/raw/main/tools/tools/qdb?ref_type=heads
QDB_DST=${HOME}/.local/bin/qdb
]]

local qdb_install = [[
install-qdb:
	curl -o $(QDB_DST) $(QDB_URL)
]]

local lua_deps_install = [[
install-lua-deps:
	sudo apt update && sudo apt install luarocks
	sudo luarocks install busted
	sudo luarocks install luasql-postgres
	echo todo db-env lhttp
]]


local db_init_cmds = [[
# configure permission to connect to db from java-app
create-dbuser-and-db:
	qdb create-user-and-db

# drop existed tables and refill with test datas
# based on ./sql/scheme-ddl.sql & ./sql/insert-dml.sql
create-test-tables:
	qdb create-tables populate-tables
]]

-- for EX_COMMANDS(targets)
local cmd_luatests = [[
# Before running the tests, you need to start the application itself
test:
	busted src/test/_lua/
]]

local cmd_run_test_stub = [[
test:
	echo todo test
]]

-- fro MVN_RUN_CMD
local mvn_run_cmd_spring_boot = "spring-boot:run"

local spring_boot_debug_target = [[
debug:
	mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:$(JDWP_OPTS)"
]]

-- todo check it
-- mvn exec:java -Dexec.args="-classpath %classpath -Xdebug -Xrunjdwp:$(JDWP_OPTS) MainClass"


-- # shorthand to create permissions,db and test data to test JavaApp
-- init-test-db: create-dbuser-and-db create-test-tables


---@param artifact string
---@param version string
function M.gen_Makefile_body(artifact, version, opts)
  log.debug("gen_Makefile_body", artifact, version)
  opts = opts or {}
  local subs = {
    _ARTIFACT = artifact or '',
    _VERSION = version or '',
    _WEBAPP_ROOT = opts.webapp_root or '',
    _PORT = opts.port or '8080',
    _DEV_DEPENDENCIES = opts.dev_deps or '',
    _DEV_DEPENDENCIES_INSTALL = opts.dev_deps_install_cmds or '',
    _DB_INIT_CMDS = db_init_cmds,
    _MVN_RUN_CMD = 'exec:java',
    _TARGET_DEBUG = '',
    _RUN_TEST = '',
    _EX_COMMANDS = opts.ex_cmds or '',
    _PHONY = '', --'install-qdb install-lua-deps',
  }
  if opts.is_springboot then
    log.debug('spring-boot project')
    subs._MVN_RUN_CMD = mvn_run_cmd_spring_boot
    subs._TARGET_DEBUG = spring_boot_debug_target
  end
  if opts.luatest then
    log.debug('with luatest')
    -- command(targets) for installing dev dependencies
    append(subs, '_DEV_DEPENDENCIES_INSTALL', lua_deps_install)
    append(subs, '_RUN_TEST', cmd_luatests)
  end

  if opts.use_qdb then
    log.debug('with qdb')
    -- where to download from
    append(subs, '_DEV_DEPENDENCIES', qdb_repo)
    append(subs, '_DEV_DEPENDENCIES_INSTALL', qdb_install)
  end
  set_if_empty(subs, '_RUN_TEST', cmd_run_test_stub)

  return substitute(TEMPL_SPRING_BOOT_WEBAPP_MVN, subs)
end

return M

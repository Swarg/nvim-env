-- 16-02-2025 @author Swarg
--

local log = require 'alogger'
local log_debug = log.debug

local M = {}
local BUTTOM_TO_TOP = 1
local TOP_TO_BUTTOM = 2

M.DIRECTION_CODE_BUTTOM_TO_TOP = BUTTOM_TO_TOP
M.DIRECTION_CODE_TOP_TO_BUTTOM = TOP_TO_BUTTOM

local DIRECTIONS_CODE = {
  bt = BUTTOM_TO_TOP,
  b2t = BUTTOM_TO_TOP,
  ['buttom-to-top'] = BUTTOM_TO_TOP,
  ['buttom_to_top'] = BUTTOM_TO_TOP,
  du = BUTTOM_TO_TOP,
  d2u = BUTTOM_TO_TOP,
  ['down-to-up'] = BUTTOM_TO_TOP,
  ['down_to_up'] = BUTTOM_TO_TOP,

  tb = TOP_TO_BUTTOM,
  t2b = TOP_TO_BUTTOM,
  ['top-to-buttom'] = TOP_TO_BUTTOM,
  ['top_to_buttom'] = TOP_TO_BUTTOM,
  ud = TOP_TO_BUTTOM,
  u2d = TOP_TO_BUTTOM,
  ['up-to-down'] = TOP_TO_BUTTOM,
  ['up_to_down'] = TOP_TO_BUTTOM,
}

function M.get_compare_direction_code(direction)
  return DIRECTIONS_CODE[direction or false]
end

--
-- compare two stacktraces until the first differences
--
--
---@param stacktrace1 table?
---@param stacktrace2 table?
---@param direction string b2t t2b The direction of comparison :]
--        b2t - from the bottom to the top, or t2b - from the top to the bottom
---@return table?
---@return string? errmsg
function M.compare_ufd(stacktrace1, stacktrace2, direction)
  local dir = M.get_compare_direction_code(direction or "b2t")
  if not dir then
    return nil, 'unkwnon compare direction ' .. tostring(direction)
  end
  if (type(stacktrace1) ~= 'table' or #stacktrace1 == 0) then
    return nil, 'no first stacktrace'
  end
  if (type(stacktrace2) ~= 'table' or #stacktrace2 == 0) then
    return nil, 'no second stacktrace'
  end

  local st1len, st2len = #stacktrace1, #stacktrace2

  local function mk_result(fi1, fi2, ti1, ti2, has_diff)
    log_debug("mk_result f1:%s-t1:%s f2:%s-t2:%s has_diff:%s", fi1, ti1, fi2, ti2, has_diff)
    if not has_diff then
      return { equals = true }
    end
    return {
      same_at = {
        from1 = fi1,
        from2 = fi2,
        to1 = ti1,
        to2 = ti2,
      }
    }
  end

  local has_diff = false

  if dir == BUTTOM_TO_TOP then
    local i1, i2 = st1len, st2len
    while i1 > 0 and i2 > 0 do
      local line1 = stacktrace1[i1]
      local line2 = stacktrace2[i2]
      if line1 ~= line2 then
        has_diff = true
        break
      end
      i1 = i1 - 1
      i2 = i2 - 1
    end
    return mk_result(st1len, st2len, i1, i2, has_diff)
    --
  elseif dir == TOP_TO_BUTTOM then
    local i1, i2 = 1, 1
    while i1 <= st1len and i2 <= st2len do
      local line1 = stacktrace1[i1]
      local line2 = stacktrace2[i2]
      if line1 ~= line2 then
        has_diff = true
        break
      end
      i1 = i1 + 1
      i2 = i2 + 1
    end
    return mk_result(1, 1, i1, i2, has_diff)
  end
end

return M

-- 27-08-2023 @author Swarg
-- 14-07-2024 refactored
--
-- Abstract class for the Java Project Build Systems like ant, maven and gradle
--

local log = require 'alogger'
local class = require 'oop.class'
local su = require 'env.sutil'
local fs = require 'env.files'
local base = require 'env.lang.utils.base'

local Builder = require 'env.lang.Builder'
local ClassInfo = require 'env.lang.ClassInfo'
local JC = require 'env.langs.java.util.consts'

local Lang = package.loaded['env.lang.Lang'] ---@cast Lang env.lang.Lang



class.package 'env.langs.java'
---@class env.langs.java.JBuilder : env.lang.Builder
---@field new fun(self, o:table?, project_root:string?, src:string?, test:string?): env.langs.java.JBuilder
---@field of fun(self, project_root:string, userdata:table?): env.langs.java.JBuilder
---@field src string
---@field test string
---@field settings table?
---field project_props table? GROUP_ID, ARTIFACT_ID, VERSION, PROJECT_NAME,...
local C = class.new_class(Builder, 'JBuilder', {
  src = JC.SRC_MAIN_JAVA,
  test = JC.SRC_TEST_JAVA,
  -- project_props = nil,
  buildscript = '' -- no buildscript | Makefile
})

local UP_TO_DATE = 'up-to-date'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local is_empty_str = base.is_empty_str
local log_debug = log.debug
local join_path = fs.join_path


-- only for Projects with FlatStructure (without any build system)
-- marker is a .git, or a config file of some buildsystem
-- e.g. no marker - no buildsystem
--
---@param project_root string
---@param userdata table
function C.isProjectRoot(project_root, userdata)
  local no_markers = Lang.isProjectWithoutMarkers(project_root)
  if no_markers then
    userdata.project_root = project_root
    userdata.src, userdata.test = './', './'
    -- return true
    -- userdata.settings = { project = { flat = true }} ??
    -- self.builder = JBuilder:new(nil, self.project_root, './', './') -- FlatDir
    -- log_debug("set ProjectWithoutMarkers as FlatDir dir:%s",
    --   self.builder.project_root)
  end
  log_debug("isProjectRoot(Flat) dir:%s no_markers:%s", project_root, no_markers)
  return no_markers
end

function C:getSettings()
  log_debug("getSettings...")
  local settings
  if self:isFlatStructure() then
    settings = {}
  end
  return settings
end

-- parse full class name to package and name
-- org.comp.Main -> "org.comp", "Main"
---@return string? package
---@return string? simple classname
function C.getPackageAndClassName(cn)
  if type(cn) == 'string' then
    return string.match(cn, "^(.*)[%.](%u[%w%$_]+)$")
  end
  return nil, nil
end

---@param cn string
---@diagnostic disable-next-line: unused-local
function C:findInnerPathForModule(cn)
  log.debug("findInnerPathForModule", cn)
  return nil
end

-- -- todo remove all templs into JGen
-- local def_method = [[
--     public static void main(String[] args) {
--
--     }
-- ]]
--
-- local template_test_method = [[
--     @Test
--     public void ${METHOD_NAME}() {
--         System.out.println("${METHOD_NAME}");
-- BODY}
--     }
-- ]]
--
-- local def_junit_test_imports = [[
-- import org.junit.Test;
-- import static org.junit.Assert.*;
-- ]]


-- ---@return table DATE AUTHOR PACKAGE CLASS
-- function C.get_default_class_props()
--   return {
--     DATE = os.date('%d-%m-%Y'),
--     AUTHOR = "",
--     PACKAGE = "",
--     IMPORTS = "",
--     CLASS = "",
--     METHODS = def_method,
--   }
-- end
--
local default_package = 'pkg' -- cannot be empty!

function C.getDefaultProjectProperties()
  return {
    UI_TITLE = "New Java Project", -- for ObjEditor
    UI_ERRORS = nil,               -- for ObjEditor
    POM_PARENT = "",
    -- CREATE_PROJECT = "no",
    GROUP_ID = default_package,
    ARTIFACT_ID = "app",
    VERSION = "0.1.0",
    PROJECT_NAME = "app",
    MAINCLASS = "pkg.MainClass",
    DEPENDENCIES = "",
    TEST_FRAMEWORK = "junit:junit:4.13.2",
    JAVA_VERSION = "1.8", -- 21
    PACKAGING = "jar"     -- | war
  }
end

local PROJECT_PROPS_KEY_ORDER = {
  'UI_TITLE',       -- for ObjEditor
  'UI_ERRORS',      -- for ObjEditor
  'CREATE_PROJECT', -- used then needs to generate buildscript only
  'RAW_SNIPPETS',   -- only for FlatProject
  'GROUP_ID',
  'ARTIFACT_ID',
  'VERSION',
  'MAINCLASS',
  'PACKAGING',
  'PROJECT_NAME',
  'DEPENDENCIES',
  'TEST_FRAMEWORK',
  'JAVA_VERSION'
}

function C.getProjPropsKeyOrder()
  return PROJECT_PROPS_KEY_ORDER
end

local function validate_not_empty(t, key)
  local s = t[key]
  if s == nil or s == '' or string.match(s, "^%s*$") then
    t.UI_ERRORS = t.UI_ERRORS or ''
    if t.UI_ERRORS ~= '' then
      t.UI_ERRORS = t.UI_ERRORS .. ', '
    end
    t.UI_ERRORS = t.UI_ERRORS .. 'no ' .. key
  end
end

--
-- apply fix for MainClass based on GROUP_ID
-- used in ObjEditor
-- idea: audo apply on leaving from insert mode
--
---@param t table
function C.update_project_properties(t)
  log.debug("update_project_properties", t)
  if not type(t) == 'table' then
    return false
  end
  t.UI_ERRORS = nil
  validate_not_empty(t, 'MAINCLASS')
  validate_not_empty(t, 'GROUP_ID')
  validate_not_empty(t, 'VERSION')
  validate_not_empty(t, 'PROJECT_NAME')
  validate_not_empty(t, 'JAVA_VERSION') -- todo "last lts" --> "21"
  validate_not_empty(t, 'project_root')
  if is_empty_str(t.PACKAGING) then t.PACKAGING = 'jar' end


  local defpkg = default_package .. "."
  if su.starts_with(t.MAINCLASS, defpkg) then
    t.MAINCLASS = t.GROUP_ID .. '.' .. t.MAINCLASS:sub(#defpkg + 1, #t.MAINCLASS)
    -- MainClass without pakage is not supported
  elseif not t.MAINCLASS:match('%.') then
    t.MAINCLASS = t.GROUP_ID .. '.' .. t.MAINCLASS
  end
end

-- ---@param extra function
-- function C.mk_builder_info(t, extra)
--   -- M.update_project_properties(t)
--   return
--       extra(t) ..
--       tostring(t.GROUP_ID) .. ':' .. tostring(t.ARTIFACT_ID) .. ':' ..
--       tostring(t.VERSION) .. ' (' .. tostring(t.MAINCLASS) .. ') jvm: ' ..
--       tostring(t.JAVA_VERSION) ..
--       ' ProjectName: ' .. tostring(t.PROJECT_NAME) ..
--       ' CreateProject: ' .. tostring(t.CREATE_PROJECT)
-- end

-- local class_template = [[
-- package ${PACKAGE}
--
-- ${IMPORTS}
-- /*
--  * ${DATE}
--  * @author ${AUTHOR}
--  */
-- public class ${CLASS} {
--
-- ${METHODS}
-- }
-- ]]

--[==[


--
--
-- Create new class file for given ClassName
-- ClassName must be full this pakage (via dots)
-- File will be created only it here no file with same name!
--
---@param class_name string
---@return false|string? full path to new classfile or errflag
---@return string? errmsg
function C:createClassFile(class_name, ext, opts)
  if not class_name or class_name == '' then
    return false, 'no class_name'
  end

  if not fs.dir_exists(self.project_root) then
    return false, 'project directory is not exists ' .. v2s(self.project_root)
  end

  local cpath0 = class_name:gsub('%.', '/') .. '.' .. ext
  local subdir = self.src
  if opts and opts.test_class then
    subdir = self.test
  end
  local class_file = fs.join_path(self.project_root, subdir, cpath0)

  if fs.file_exists(class_file) then
    return false, 'file already exists ' .. class_file
  end


  local dir = fs.extract_path(class_file)
  if not fs.dir_exists(dir) then
    if not fs.mkdir(dir) then
      return false, 'cannot create directory: ' .. tostring(dir)
    end
  end

  local t = C.get_default_class_props()
  -- some.pkg.Class -> some.pkg
  t["PACKAGE"] = (C.getPackage(class_name) or '') .. ';'
  t["CLASS"] = fs.extract_filename(class_file)
  t["AUTHOR"] = self.getDeveloperName() or ''
  if opts then
    t["IMPORTS"] = opts.imports or ""
    t["METHODS"] = opts.methods or def_method
  end
  Builder.createFile(class_file, class_template, t)
  return class_file -- next any way open the file if it exists(new or old)
end

function C.getPackage(cn)
  return cn ~= nil and string.match(cn, "^(.*)[%.][%w]+") or nil
end

---@param ctx table { ext, method_name}
function C:createTestFile(class_name, ctx)
  local opts = {}
  opts.test_class = true
  opts.imports = def_junit_test_imports

  local mn = ctx.method_name
  -- generate one test-method
  if mn and #mn > 1 then
    local substitutions = {
      METHOD_NAME = 'test' .. string.upper(mn:sub(1, 1)) .. mn:sub(2, -1),
      BODY = "        assertEquals(1, 2);"
    }
    -- generate code
    opts.methods = string.gsub(template_test_method, "[%w_]+", substitutions)
  end
  -- generate all methods
  -- TODO

  return self:createClassFile(class_name, ctx.ext or 'java', opts), mn
end

---@param class_name string full-class-name with package
---@param ext string file extension like 'java'
---@return string|nil path to existing found file or nil
function C:lookupPathToClassInSelf(class_name, ext)
  local path = class_name:gsub('%.', '/') .. '.' .. ext
  -- check src/main/java
  local class_file = fs.join_path(self.project_root, self.src, path)
  if fs.file_exists(class_file) then
    return class_file
  end
  -- check src/test/java
  class_file = fs.join_path(self.project_root, self.test, path)
  if fs.file_exists(class_file) then
    return class_file
  end
end

--- find full path to specified class
-- Lookup in own src, parent and his childs
-- TODO find class in dependencies
---@param class_name nil|string|table full-class-name with package or variants
--                                 variants - list of full-class-names to check
---@param ext string file extension like 'java'
---@param passed table the project_roots where the search has already passed
function C:lookupPathToClass(class_name, ext, passed)
  if class_name and ext and fs.dir_exists(self.project_root) then
    passed[self:getProjectDirName()] = true

    -- here I am searching in the owned src and test
    if type(class_name) == 'table' then
      for _, class_name0 in pairs(class_name) do
        local class_file = self:lookupPathToClassInSelf(class_name0, ext)
        if class_file then return class_file end
      end
    else
      local class_file = self:lookupPathToClassInSelf(class_name, ext)
      if class_file then return class_file end
    end

    -- next search in the parent project and all it subprojects (if has parent)
    local parent = self:getParentBuilder()
    if parent then
      local dirname = parent:getProjectDirName()
      -- parent:getClass() instanceof JavaBuilder
      ---@diagnostic disable-next-line: undefined-field
      if not passed[dirname] and parent:instanceof(C) then
        passed[dirname] = true
        ---@diagnostic disable-next-line: undefined-field
        return parent:lookupPathToClass(class_name, ext, passed)
      end
    else
      -- root project has no parent!
      -- if self is a root project - check in the all own subprojects
      local subdir_names = self:getSubProjects()
      if type(subdir_names) == 'table' then
        for _, dirname in pairs(subdir_names) do
          if not passed[dirname] then
            local subproject = self:getSubProject(dirname)
            if subproject and subproject:instanceof(C) then
              ---@diagnostic disable-next-line: undefined-field
              local path = subproject:lookupPathToClass(class_name, ext, passed)
              if path then
                return path
              end
            end
          end
        end
      end
    end
  end
end
-- abstract
---@diagnostic disable-next-line: unused-local
function C:argsTestSingleClass(class_name, opts, method_name)
  error('Not implement yet! This method must be overridden')
end
]==]

--
--
--------------------------------------------------------------------------------
--         Flat Project (All sources and test-files in same dir)
--------------------------------------------------------------------------------

-- to support Flat Project Structure without any pom.xml(configured by cmd)
-- first uses in the MixBuilder
-- UseCase: playground for minimalistic education "project" code in one dir
-- REPL like style to run single java-code-snippets
-- TODO adopt to Java (First Implemented for Elixir Lang)

--
--
-- Something like a FlatProject but with an even more flexible and simpler
-- structure, for running tests without any frameworks `java some_test.java`.
--
-- Treat the project as a directory of code snippets created for learning purposes.
-- this is used to run tests for sources without pom.xml|grandle.build|etc
-- (JLang InMemOnly)
--
-- RawSnippets are stored in a flat structure so isFlatStructure = true
-- but isFlatStructure may be not only for disparate source-test pairs, but
-- also for simple flat projects where the sources are interconnected
--
function C:setRawSnippets()
  -- raw snippets
  self.settings = self.settings or {}
  local t = self.settings or {}

  t.project = t.project or {}
  t.project.raw_snippets = true -- used by isRawSnippets and goto src-test

  -- maven
  -- t.project.build = t.project.build or {}
  -- t.project.build.sourceDirectory = './' -- from maven pom.xml

  self.src, self.test = './', './' -- FlatStructure

  return self
end

-- function C:isFlatStructure()
-- also can be self.settings.project.build.sourceDirectory = './' for maven
-- end

--------------------------------------------------------------------------------


--
--
---@param ci env.lang.ClassInfo
---@return env.lang.ExecParams
function C:getArgsToRunSingleSrcFileFlatDir(ci)
  local t = {
    builder = self,
    lang = nil,
    classname = ci:getClassName(),
    javac = nil,             -- compiler
    java = nil,              -- jvm runtime
    build_dirname = 'build', -- build/classes, target/  etc
    build_path = nil,        -- project_root .. build_dirname
    compile_flags = nil,
    dependencies = nil,
    classpath = nil, -- will be populated by factoryBuildCompileCallback
    cp_sep = ':',    -- unix
    -- here I specify the next task that should be launched after
    -- compilation is complete.
    -- Goal: asynchronous launch exactly after the completion of the first task,
    -- and not in parallel. Task1:Compilation -> Task2:RunTestFile
    build_next_task = C.factoryBuildRunTestClassCallback,
  }

  local callback, errmsg = C.factoryBuildCompileCallback(t)
  -- here a callback to run tests can be sent immediately (without Task1:Compile)
  -- if everything is already compiled.
  if type(callback) ~= 'function' then
    -- if errmsg ~= UP_TO_DATE then
    error(errmsg)
    -- end
    -- case: all sources already compiled
    -- remove compile stage if here not updated sources and all already compiled
  end

  return self:newExecParams(nil, {}):withCallback(callback)
end

--
---@param ci env.lang.ClassInfo
---@return env.lang.ExecParams
function C:getArgsToRunSingleSrcFile(ci)
  log_debug("getArgsToRunSingleSrcFile %s", ci)
  if not ClassInfo.isValidFile(ci) then
    error('expected valid ci got: ' .. v2s(ci))
  end

  if self:isFlatStructure() then
    return self:getArgsToRunSingleSrcFileFlatDir(ci)
  end
  error('Not implemented yet')
end

---
---
---@param tci env.lang.ClassInfo
---@param opts table?
---@return env.lang.ExecParams
function C:getArgsToRunSingleTestFile(tci, opts)
  opts = opts or E
  -- Note:
  --  FlatProject can be simple project where sources related together
  --  (iex -S mix and one namespace for module names) -> can mix test ...
  --  RawSnippets - is a set of independed sources and testfiles
  --  (Can content multiple files of same code (iterations) the goal
  --  provide way to run test directly via /usr/lib/jvm/java without `mvn test`
  if self:isFlatStructure() then
    -- todo for RawSnippets
    return self:getArgsToRunSingleTestFileInFlatProject(tci)
  end
  error('Not implemented yet')
end

---@param tci env.lang.ClassInfo
---@return env.lang.ExecParams
---@diagnostic disable-next-line: unused-local
function C:getArgsToDebugSingleTestFile(tci)
  error('Not implemented yet')
end

--[[
Compile-and-launch single file source code
With this new feature in Java 11 and later, that java program switches
to source code mode if you pass the name of a .java source code file.
If that file contains a main method, that class is compiled.
The resulting bytecode is cached in memory.
Lastly, that bytecode’s main method executes
https://openjdk.org/jeps/330

This idea come from Elixir:
exlirc -r my_module.ex -r test_helper.exs my_module_test.ex
]]
---@param tci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function C:getArgsToRunSingleTestFileInFlatProject(tci)
  -- local sci = tci:getPair() or self:getOppositeClassInfo(tci)
  -- if not self:isPathExists(sci) then
  --   error('Not Found paired src. If you run a raw-snippets run it from src-file')
  -- end

  -- assert(sci, 'should find a source file for test file')

  -- in Java-11 introduce
  -- https://openjdk.org/jeps/330 Launch Single-File Source-Code Programs (J11)
  -- in Java-22 introduce way to lauch multiple java-files without compilation:
  -- https://openjdk.org/jeps/458
  -- https://openjdk.org/projects/amber/design-notes/on-ramp
  -- https://docs.oracle.com/en/java/javase/21/docs/specs/man/java.html
  return self:getArgsToRunSingleSrcFileFlatDir(tci)
  -- todo add support to use junit annotations? or put lib to classpath?
end

--------------------------------------------------------------------------------
--                              Low Level
--------------------------------------------------------------------------------

-- Notes: cmd, args for editor:runCmdInBuffer
--
-- local cmd, args = 'java', { '-version' } -- ok
-- local cmd, args = 'sh', { '-c', '"date"' } -- ok
-- local cmd, args = 'sh', { '-c', 'java -version' } -- ok
-- local cmd, args = 'sh', { '-c', s }

--
-- build the list of *.java files to be compiled  (Flat Project)
--
-- Find all *.java files in a given directory and check whether there are pairs
-- of them copied and, if so, check whether these sources have changed since the
-- last compilation and if not, exclude them from the list of sources for
-- compilation
--
-- local sources = "`find -type f -name *.java`"
--
---@return false|table
---@return string|number? errmsg or number of all founded sources
function C.get_updated_sources(dir, build_dir)
  local files, errmsg = fs.list_of_files_deep(dir, ".*%.java")
  if type(files) ~= 'table' then
    return files, errmsg
  end

  -- check by the last modified time which sources need to be recompiled
  -- and exclude already compiled and not changed
  local updated_list = {} -- updated_list

  for _, fn in ipairs(files) do
    local updated = true
    local fn0 = fn:gsub("%.java$", ".class")
    local classfile = join_path(build_dir, fn0)
    local cf_mtime = fs.last_modified(classfile)
    if cf_mtime then
      local sf_mtime = fs.last_modified(join_path(dir, fn))
      if sf_mtime ~= nil and sf_mtime <= cf_mtime then
        updated = false
      end
    end
    if updated then
      updated_list[#updated_list + 1] = fn
    end
  end
  return updated_list, #files
end

--
-- factory to build callback for Task:1 CompileToRunTests
--
---@param t table{lang, build_dirname:string, flags:flags, cp_sep, dependencies:table?}
---@return false|function|string
---@return string?
function C.factoryBuildCompileCallback(t)
  assert(type(t) == 'table', 't')
  local builder = assert(t.builder, 'builder')
  local root_dir = builder.project_root

  if not root_dir or not fs.dir_exists(root_dir) then
    return false, 'dir not exists ' .. v2s(root_dir)
  end

  local build_dirname = t.build_dirname or 'build'
  local javac = t.javac or 'javac'
  local cp_sep = t.cp_sep or ':' -- in windows used `;` insted of ':'(unix)

  local cflags = t.compile_flags or '-g -Xlint:unchecked -Xdiags:verbose'
  local build_path = t.build_path or fs.join_path(root_dir, build_dirname)

  local sources, errmsg = C.get_updated_sources(root_dir, build_path)
  if not sources then
    return false, v2s(errmsg)
  end
  if #sources == 0 then
    -- the case when nothing needs to be compiled - no changes to the code
    if type(errmsg) == 'number' and errmsg > 0 then
      -- 'all sources already compiled', v2s(errmsg) -- up-to-date
      -- TODO return next task(callback)
      return false, UP_TO_DATE
    end
    return false, 'nothing to complie' -- stop
  end

  local classpath = './' .. build_dirname -- .. ':'
  for _, dep in ipairs(t.dependencies or E) do
    classpath = classpath .. cp_sep .. dep
  end

  t.classpath = classpath -- to pass back to caller, or next task

  local args = su.split(cflags, " ")
  args[#args + 1] = "-d"
  args[#args + 1] = "./" .. build_dirname
  args[#args + 1] = "-cp"
  args[#args + 1] = classpath

  for _, src in ipairs(sources) do
    args[#args + 1] = src
  end

  local title = "Compile-" .. fs.get_parent_dirname(root_dir) -- aka project name

  -- this callback will be runned in env.lang.Lang:runSysCmd()
  ---@param lang env.lang.Lang
  return function(lang)
    log_debug('callback: Compile', root_dir, javac, args)
    t.lang = lang
    -- directory structure for the build
    if not fs.is_directory(build_path) then fs.mkdir(build_path) end

    -- schedule next task(run test from compiled sources)
    local opts2 = {}
    if type(t.build_next_task) == 'function' then
      -- handler_on_close will be run after this task is completed
      opts2.handler_on_close = t.build_next_task(t) -- factoryBuildRunTest..
      t.build_next_task = nil                       -- free
    end

    local params = lang:newExecParams(javac, args):withOpts(opts2)

    lang:getEditor():runCmdInBuffer(title, params)

    return 'Compile...'
    -- will be shown via Editor:echoInStatus
  end
end

-- factory to build callback for Task:2 RunTestFile
--
-- factory to build callback for runnign the Tests
-- Goal: run Test only after compilation is complete.
-- because commands are called asynchronously and without this the next one
-- will not start without waiting for this one to be completed
--
---@param params table{lang, classname, classpath,}
function C.factoryBuildRunTestClassCallback(params)
  assert(type(params) == 'table', 'expected table params to build callback')
  assert(params.lang, 'lang from prev task')
  assert(type(params.classpath) == 'string', 'expected string classpath')
  assert(type(params.classname) == 'string', 'expected class name to run')
  local t = params -- closure

  -- will be launched only after Compile Task -- via handler_on_close
  ---@param output string
  return function(output)
    log_debug('callback: RunTestFile')
    output = output               -- to check for compilation errors
    local java = t.java or 'java' -- executable to run compiled code
    local lang = t.lang

    if output and string.match(output, "\n?error:%s*([^\n]+)") then
      lang:getEditor():echoInStatus("Compilation error", "Error")
      return ""
    end
    --
    print('Compilation is complete now running tests...')

    local rflags = "-enableassertions" -- -ea for `assert 0 == 1 : "errmsg";`

    local args = su.split(rflags, " ")
    args[#args + 1] = '-classpath'
    args[#args + 1] = t.classpath
    args[#args + 1] = t.classname -- 'class name from tci

    local title = 'Run Test ' .. v2s(t.classname)
    local cwd = lang.project_root
    local opts = {           -- for next task
      handler_on_close = nil -- next task in chain (on done this one)
    }
    lang:getEditor():runCmdInBuffer(title, cwd, java, args, t.envs, opts)

    return 'RunTest...'
  end
end

---@param ci env.lang.ClassInfo
---@diagnostic disable-next-line: unused-local
function C:compile(ci) error('abstract') end

---@param ci env.lang.ClassInfo
---@diagnostic disable-next-line: unused-local
function C:hotswap(ci) error('abstract') end

---@return boolean
function C:isWebApp() return false end

--
-- checking that the specified file is a web resource,
-- based on src/main/webapp subdirs
--
-- for example an html page template is so set ci.type to CT_WEBTEMPLATE
-- and fix inner_path
--
---@param ci env.lang.ClassInfo
function C.isWebAppResource(ci)
  local fn = ci ~= nil and ci:getPath() or nil
  if fn ~= nil then
    local _, j = string.find(fn, JC.SRC_MAIN_WEBAPP)
    if not j then
      return false
    end
    ci.type = JC.CT_WEBTEMPLATE
    ci.inner_path = string.sub(fn, j + 1, #fn)
    return true
  end
  return false
end

class.build(C) -- class|module
return C

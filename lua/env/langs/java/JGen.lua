-- 14-07-2024 @author Swarg
-- Generate Stuff for JavaCode

local log = require 'alogger'
local class = require 'oop.class'


local lapi_consts = require 'env.lang.api.constants'
local LangGen = require 'env.lang.oop.LangGen'
local ClassInfo = require("env.lang.ClassInfo")
local AccessModifiers = require 'env.lang.oop.AccessModifiers'
-- local ClassInfo = require 'env.lang.ClassInfo'

local CODEGEN_TESING_PKG = 'env.langs.java.util.codegen.testing.'
local DEFAULT_TEST_FRAMEWORK = 'junit4'


class.package 'env.langs.java'
---@class env.langs.java.JGen : env.lang.oop.LangGen
---@field lang env.langs.java.JLang?
---@field ext string
---@field tab string
---@field default_field_type string
---@field default_field_amod number  -- access modifier (private|public, etc)
local C = class.new_class(LangGen, 'JGen', {
  lang = nil,
  ext = 'java', -- extension of source files
  test = 'src/main/test',
  tab = '    ',
  default_field_type = 'String',
  default_field_amod = AccessModifiers.ID.PRIVATE,

  cmdhandler_new_stuff = "env.langs.java.command.new_stuff", -- :EnvNew ..
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug
---@diagnostic disable-next-line: unused-local
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

--
-- take classname from current line with use(import)(from sourcefile via editor)
---@param line string?
function C:getClassNameFromLine(line)
  line = line or self:getContext().current_line
  if line then
    -- import org.ClassName;
    local cn = line:match('^%s*import%s+([^;]+)%s*;?')
    if not cn then
      -- MyClass var = new MyClass()
      cn = line:match('%a[%w_]+%s*=%s*new%s+(%u[%w_]+)%s*%(')
      if not cn then
        -- MyClass var = new org.comp.MyClass()
        cn = line:match('%a[%w_]+%s*=%s*new%s+([%w_%.]+)%s*%(')
      end
    end

    return cn
  end
end

-- local template_docsblock = [[
-- /**
--  *
--  */
-- ]]

---@diagnostic disable-next-line: unused-local
local def_method = [[
    public static void main(String[] args) {

    }
]]

local template_method = [[
    ${ACCESSMOD} ${RETURN_TYPE} ${NAME}(${PARAMS}) {
${BODY}
    }
]]


local template_constructor = [[
    ${ACCESSMOD} ${NAME}(${PARAMS}) {
${BODY}
    }
]]

local template_field = [[    ${ACCESSMOD} ${TYPE} ${NAME}${DEFAULT_VALUE};]]
-- local template_asign_to_self_field = [[${THIS} ${EQUALS} ${VALUE};]]

-- todo @Annotations
local template_class = [[
package ${PACKAGE};

${IMPORTS}
/**
 *${DATE}
 *${AUTHOR}
 */
public class ${CLASS}${EXTENDS} {
${FIELDS}
${METHODS}
}
]]


-- public class AppTest {
--     //@Test
--     public void appHasAGreeting() {
--     }
-- }
--

-- override
---@param ci env.lang.ClassInfo?
function C:getClassTemplate(ci)
  ci = ci
  -- if ci and ci:isTestClass() then return test_file_pattern end
  return template_class;
end

---@param ci env.lang.ClassInfo?
function C:getMethodTemplate(ci)
  if ci and ci:isTestClass() then
    return self:getTestCodeGenMod().template_method or template_method
  end
  return template_method
end

---@diagnostic disable-next-line: unused-local
---@param classinfo env.lang.ClassInfo
function C:getConstructorName(classinfo)
  return classinfo:getShortClassName()
end

function C:getKWordExtands() return ' extends ' end

---@return string
function C:getFieldTemplate() return template_field end

---@return string, string template + separator
function C:getParamTemplate() return '${TYPE} ${NAME}', ',' end

function C:getKWordThis() return 'this' end

function C:getKWordBoolean() return 'boolean' end

---@return string
function C:getVarPrefix() return '' end

---@return string
function C:getReturnPrefix() return '' end

---@return string
function C:getAttrDot() return '.' end

---@return string
function C:getOpCompareIdentical() return '==' end

---@return string
function C:getConstructorTemplate() return template_constructor end

---@return string
function C:getGetterTemplate() return '' end

---@return string
function C:getSetterTemplate() return '' end

---@return string
function C:getEqualsTemplate() return '' end

---@return string
function C:getDefaultFieldType() return self.default_field_type end

---@return number
function C:getDefaultFieldAMod() return self.default_field_amod end

--------------------------------------------------------------------------------


---@param classname string
---@param ctype number?
function C:isClassExists(classname, ctype)
  log_debug("isClassExists cn:%s ctype:%s", classname, ctype)
  ctype = ctype or ClassInfo.CT.SOURCE
  local ci = self.lang:getClassResolver(ctype, nil, classname)
      :run():getClassInfo(false)

  local b = self.lang:isPathExists(ci)
  -- print("[DEBUG] not:", classname, ci, b)
  return b, ci
end

--
-- generate projects file (structure for given project properties
--
---@param pp table{dry_run, MAINCLASS, openInEditor}
---@return number
function C:createProjectFiles(pp)
  local mainclass = (pp or E)["MAINCLASS"]
  log_debug("createProjectFiles mainclass:%s lang.project_root:%s",
    mainclass, (self.lang or E).project_root)

  local cnt = 0
  -- if mainclass present in props but not in the disk - generate a new one
  if mainclass then
    local exists, ci = self:isClassExists(mainclass, nil)
    log_debug('mainclass:%s exists:%s ci:', mainclass, exists, ci)
    if not exists then
      if self:createClassFile(ci, pp) then
        cnt = cnt + 1
        local tci = self.lang:getOppositeClassInfo(ci)
        if self.lang:generateTestFile(tci, pp) then
          cnt = cnt + 1
        end
      end
    end
  end

  return cnt -- created files
end

--
--
---@param tci env.lang.ClassInfo
---@param opts table?
---@diagnostic disable-next-line: unused-local
function C:genOptsForTestFile(tci, opts, def)
  local testgen_mod, tfn = self:getTestCodeGenMod()
  log_debug("genOptsForTestFile testfwname:%s", tfn)
  assert(tci ~= nil)
  local sci = tci:getPair()

  opts = opts or {}
  opts.test_class = true
  opts.extends = opts.extends
  opts.imports = testgen_mod.template_import or ''

  if not opts.name then
    -- self:getContext():resolveWords() -- (broke tests) alredy done by caller
    opts.name = self:getMethodUnderUICursor()
  end
  local p = {
    method_name = opts.name,
    tab = self:getTab(),
  }
  local body, tmn = testgen_mod.gen_testmethod_body(sci, p)
  self:getEditor():setSearchWord(tmn) -- to jump to a generated test method name

  local params = false
  local methodGen = self:getMethodGen(tci)
  opts.methods = { methodGen:genTest(tmn, params, body) }
  return opts
end

--
---@return string?
function C:getTestFrameworkName()
  local tf = self:getLang():getTestFramework()
  local name = (tf or E).name
  log_debug("get_test_fw_name:%s %s", name, tf)
  return name
end

--
-- require module for a given framework
-- see env.langs.java.util.codegen.testing.junit5
--
---@return table
---@return string test framework name
function C:getTestCodeGenMod()
  local tfn = self:getTestFrameworkName() or 'unknown'
  local modname = CODEGEN_TESING_PKG .. v2s(tfn)

  local ok, mod = pcall(require, modname)
  if not ok or type(mod) ~= 'table' then
    -- The second attempt to use default test framework
    if tfn ~= DEFAULT_TEST_FRAMEWORK then
      ok, mod = pcall(require, CODEGEN_TESING_PKG .. DEFAULT_TEST_FRAMEWORK)
      if ok and type(mod) == 'table' then
        return mod, DEFAULT_TEST_FRAMEWORK
      end
    end
    print('[WARN] err on loading module for ' .. v2s(tfn) .. ":\n" .. v2s(mod))
    return {}, tfn
  end

  return mod, tfn
end

-------------------------------------------------------------------------------

--
-- META-INF/services/..
-- append mapping for current opened class file as a Service Implementation
-- for the interface that this class is implemened.
--
---@param interface string? full interface name of the Service to be implemented
---@param impl string? - full class name of the Service Implementation
---@return string? abs path
---@return string? errmsg
function C:genServiceLoaderImpl(interface, impl, dry_run)
  local clazz, err = nil, nil

  if not impl or impl == '' or impl == '.' then
    clazz, err = self.lang:getClassDefintionInOpenedBuff()
    if not clazz then
      return nil, err
    end
    local pkg, cn = clazz.pkg, clazz.name
    if not pkg then
      return nil, 'Not Found package name in the current opened class'
    end
    if not cn then
      return nil, 'Not Found classname in the current opened class'
    end
    impl = v2s(pkg) .. '.' .. v2s(cn)
  end

  if not interface or interface == '' or interface == '.' then
    if not clazz then
      clazz, err = self.lang:getClassDefintionInOpenedBuff()
    end
    if not clazz then
      return nil, err
    end

    interface = (clazz.implements or E)[1]
    if not interface then
      return nil, 'Not Found interface in current opened class'
    end
    -- find full interface name
    local patt = '%.' .. v2s(interface) .. '$'
    for _, import in ipairs(clazz.imports or E) do
      if string.match(import, patt) then
        interface = import
        break
      end
    end
  end
  local dir = "resources/META-INF/services/"
  local path = self.lang:getProjectRoot() .. "/src/main/" .. dir .. v2s(interface)

  -- only show without writing
  if dry_run then
    return (v2s(path) .. "\n" .. impl), nil
  end

  local lineToAppend = impl .. "\n"
  local saved = self:createFile(false, path, lineToAppend, { append = true })
  if not saved then
    err = self:getFileWriter():getReadableReport()
    return nil, err
  end
  self:getFileWriter():clearEntries()

  return path, nil
end

class.build(C)
return C

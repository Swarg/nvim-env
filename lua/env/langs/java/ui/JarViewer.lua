-- 08-10-2024 @author Swarg
-- extension for nvim to interact with the content of Jar file
-- to view the jar(zip) content in tree-like explorer
--
-- :EnvJarViewer

local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'
local su = require 'env.sutil'
local decomp_manager = require 'env.langs.java.util.decompiler.decomp_manager'


class.package 'env.langs.java.ui'
---@class env.langs.java.ui.JarViewer : oop.Object
---@field new fun(self, o:table?, jarFile:string, lang:env.langs.java.JLang?) : env.langs.java.ui.JarViewer
---@field jar_file string absolute path to the jar file
---@field lang env.langs.java.JLang?
---@field bufnr number
---@field tree table
---@field lines table  -- visible lines
---@field lnum2path table -- mappings lnum to path in jar
---@field dirs_state table
---@field tmpdir string
---@field decompiler string?
---@field decomp_outdir string? directory to output decompiled sources
local C = class.new_class(nil, 'JarViewer', {
})

local augroup
local buffers = {} -- bufnr -> JarViewer
local CLOSED_DIR = "[+]"
local OPENED_DIR = "[-]"
local TOGGLED_DIR_STATE = {
  [CLOSED_DIR] = OPENED_DIR,
  [OPENED_DIR] = CLOSED_DIR
}

local EXT_TO_SYNTAX = {
  java = "java",
  gradle = "groovy",
  xml = "xml",
}


--
-- Constructor
--
-- instead of deprecated bindContext
---@param jar_file string
---@param lang env.langs.java.JLang?
function C:_init(jar_file, lang)
  log.debug("_init", jar_file, lang ~= nil)
  self.jar_file = jar_file
  self.lang = lang
  self.tmpdir = fs.is_win and 'c:\\jarviewer\\' or '/tmp/jarviewer/'
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug, log_trace = log.debug, log.trace
local file_exists, dir_exists, mkdir = fs.file_exists, fs.dir_exists, fs.mkdir
local join_path = fs.join_path
local is_supported_decompiler = decomp_manager.is_supported_decompiler

--
-- set decompiler name with validation
--
---@param decompiler_name string?
function C:setDecompiler(decompiler_name)
  if decompiler_name == 'nil' or decompiler_name == 'null' then
    self.decompiler = nil
  end
  if not is_supported_decompiler(decompiler_name) then
    decompiler_name = nil
  end
  self.decompiler = decompiler_name
  return self.decompiler ~= nil
end

---@param dir string?
function C:setDecompOutDir(dir)
  self.decomp_outdir = dir
end

---@return string
function C:getDecompOutDir()
  return self.decomp_outdir or decomp_manager.DECOMP_DIR
end

-- temporary dir for extracted files
---@return string?
function C:getTmpDir()
  return self.tmpdir
end

function C:setTmpDir(dir)
  assert(dir ~= nil and dir ~= '', 'expected non empty directory name')
  self.tmpdir = dir
end

--
--
--
---@return string? name
---@return string? path ot jar
---@return table? handler
function C:getDecompiler()
  if not self.decompiler then
    return nil, nil, nil
  end
  if not is_supported_decompiler(self.decompiler) then
    self.decompiler = nil
    return nil, nil, nil
  end
  local path2jar = decomp_manager.DECOMPILER_PROVIDERS[self.decompiler]
  local handler = decomp_manager.get_decompiler_module(self.decompiler)

  return self.decompiler, path2jar, handler
end

--
-- create new buffer with keybindings
--
---@return false|number bufnr
---@return string?
function C:openJarFile()
  if type(self.jar_file) ~= 'string' then
    error('self.jar_file == ' .. type(self.jar_file) ..
      ' at JarViewer#' .. v2s(self.bufnr))
  end
  local path = self.jar_file

  if not fs.file_size(path) then
    return false, "Not Found file: " .. v2s(path)
  end

  local raw_lines = fs.execrl("unzip -Z1 '" .. v2s(path) .. "' 2>&1")
  if not raw_lines then
    return false, "error on open " .. v2s(path)
  end

  self.tree = self.parseLinesToTree(raw_lines)
  self.dirs_state = {
    lnum2path = {}
  }

  -- initial render
  local lines = {}
  local lnum2path = {}
  self.renderTreeToLines(lines, lnum2path, self.dirs_state, self.tree, 0, "")
  self.lnum2path = lnum2path

  local bufnr, err = C.create_tree_viewer_buff(lines, "jar://" .. v2s(path))
  if not bufnr then
    return false, err
  end
  self.bufnr = bufnr

  buffers[bufnr] = self
  return bufnr
end

-- Check is given filename already opened in nvim buffer
---@param bufname string
---@return boolean
---@return number
function C.is_bufname_already_in_use(bufname)
  local is_buf_loaded = vim.api.nvim_buf_is_loaded
  if bufname and bufname ~= "" then
    for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
      if is_buf_loaded(bufnr) then
        local name0 = vim.api.nvim_buf_get_name(bufnr)
        if name0 and name0 == bufname then
          return true, bufnr
        end
      end
    end
  end
  return false, -1
end

--
-- create new Buffet to edit given table in nvim buffer
--
---@param lines table?
---@param bufname string?
---@return false|number bufnr
---@return string? err
function C.create_tree_viewer_buff(lines, bufname)
  assert(type(lines) == 'table', 'lines')
  bufname = bufname or 'JarViewer'

  if C.is_bufname_already_in_use(bufname) then
    return false, 'such bufname already in use'
  end

  local api = vim.api
  local win = api.nvim_get_current_win()

  local bufnr = api.nvim_create_buf(true, true)
  api.nvim_win_set_buf(win, bufnr) -- mk foreground

  local ok, _ = pcall(vim.api.nvim_buf_set_name, bufnr, bufname)
  if not ok then -- name alredy in use
    return false, 'jar file already opened'
  end

  api.nvim_buf_set_option(bufnr, 'modifiable', true)
  api.nvim_buf_set_lines(bufnr, 0, -1, true, lines or {})
  api.nvim_buf_set_option(bufnr, 'buftype', 'nofile')
  api.nvim_buf_set_option(bufnr, 'swapfile', false)
  api.nvim_buf_set_option(bufnr, 'filetype', "JarViewer")
  api.nvim_buf_set_option(bufnr, 'modifiable', false)

  -- api.nvim_buf_set_var(bufnr, M.BUF_VN_DISPATCHER, dispatcher)

  -- keybindings
  local opts = { silent = true, buffer = bufnr }
  -- vim.keymap.del("n", "<LeftMouse>", opts)
  local keymap = vim.keymap.set

  keymap('n', "<CR>", function() -- open
    C.viewer(bufnr):doToggleOpenClose()
  end, opts)

  keymap('n', "o", function() -- open file or toggle open|close the dir
    C.viewer(bufnr):doToggleOpenClose()
  end, opts)

  keymap('n', "h", function() C.viewer(bufnr):doClose() end, opts)
  keymap('n', "l", function() C.viewer(bufnr):doOpen() end, opts)

  keymap('n', "<C-h>", function() C.viewer(bufnr):collapseAll() end, opts)
  keymap('n', "<C-l>", function() C.viewer(bufnr):expandAll() end, opts)

  -- to copy full inner and basename of the file to the System Clipboard
  keymap('n', "y", function() C.viewer(bufnr):doCopyPath('basename') end, opts)
  keymap('n', "Y", function() C.viewer(bufnr):doCopyPath('inner') end, opts)
  keymap('n', "<C-y>", function() C.viewer(bufnr):doCopyPath('full') end, opts)

  keymap('n', "m", function() C.viewer(bufnr):doOpenElmByClipboard() end, opts)

  -- todo autoremove from JarViewer.buffers on close
  C.register_autocmd_for(bufnr)

  return bufnr
end

--
--
---@param lines table
---@param bufname string
---@param syntax string?
---@param filetype string?
function C.create_content_buff(lines, bufname, syntax, filetype)
  assert(type(lines) == 'table', 'lines')
  assert(type(bufname) == 'string' and bufname ~= "", 'bufname')

  if C.is_bufname_already_in_use(bufname) then
    return false, 'such bufname already in use'
  end

  local api = vim.api
  local win = api.nvim_get_current_win()

  local bufnr = api.nvim_create_buf(true, true)
  api.nvim_win_set_buf(win, bufnr) -- mk foreground

  local ok, _ = pcall(vim.api.nvim_buf_set_name, bufnr, bufname)
  if not ok then -- name alredy in use
    return false, 'such file already opened'
  end

  api.nvim_buf_set_option(bufnr, 'modifiable', true)
  api.nvim_buf_set_lines(bufnr, 0, -1, true, lines or {})
  api.nvim_buf_set_option(bufnr, 'buftype', 'nofile')
  api.nvim_buf_set_option(bufnr, 'swapfile', false)
  api.nvim_buf_set_option(bufnr, 'modifiable', false)
  api.nvim_buf_set_option(bufnr, 'readonly', true)

  if filetype and filetype ~= "" then
    api.nvim_buf_set_option(bufnr, 'filetype', filetype)
    --
  elseif syntax and syntax ~= '' then
    api.nvim_buf_set_option(bufnr, 'syntax', 'enable')
    api.nvim_buf_set_option(bufnr, 'syntax', syntax)
  end
end

---@return table
---@return table
local function sorted_keys0(t)
  assert(type(t) == 'table', 'table expected')
  local files = {}
  local dirs = {}
  for k, v in pairs(t) do
    if type(v) == 'table' then
      dirs[#dirs + 1] = k
    else
      files[#files + 1] = k
    end
  end

  table.sort(dirs)
  table.sort(files)

  return dirs, files
end


---@param lines table
---@param lnum2path table
---@param dirs_state table
---@param tree table
---@param depth number
---@param path string?
function C.renderTreeToLines(lines, lnum2path, dirs_state, tree, depth, path)
  depth = depth or 0
  path = path or ""
  local dirs, files = sorted_keys0(tree)
  local ident = string.rep("  ", depth)

  for _, dir in ipairs(dirs) do
    local dirpath = path .. dir .. "/"
    local dir_state = dirs_state[dirpath]
    if not dir_state then
      dir_state = CLOSED_DIR
      dirs_state[dirpath] = dir_state
    end
    lines[#lines + 1] = ident .. v2s(dir_state) .. " " .. v2s(dir) .. '/'
    lnum2path[#lines] = dirpath
    if dir_state == OPENED_DIR then
      local subdir = tree[dir]
      C.renderTreeToLines(lines, lnum2path, dirs_state, subdir, depth + 1, dirpath)
    end
  end

  for _, file in ipairs(files) do
    lines[#lines + 1] = ident .. v2s(file)
    lnum2path[#lines] = path .. file
  end
end

---@return table
function C.parseLinesToTree(lines)
  local tree_root = {}
  for _, line in ipairs(lines) do
    local is_dir = line:sub(-1, -1) == '/'
    local path_nodes = su.split(line, "/")

    local root = tree_root
    for i = 1, #path_nodes - 1 do
      local node = path_nodes[i]
      root[node] = root[node] or {}
      root = root[node]
    end
    local node = path_nodes[#path_nodes]
    if node ~= nil then
      root[node] = is_dir and {} or true
    end
  end
  return tree_root
end

--
-- register autocommand for given buffer
-- This callback will be called automatically on BufDelete
-- to auto clear instance from inner in-memeory buffer
--
---@param bufnr number
---@return number
function C.register_autocmd_for(bufnr)
  assert(type(bufnr) == 'number' and bufnr > 0, 'bufnr')
  local api = vim.api

  if augroup then
    augroup = api.nvim_create_augroup("JarViewer", { clear = true })
  end

  return api.nvim_create_autocmd("BufDelete", {
    group = augroup,
    buffer = bufnr,
    callback = function()
      buffers[bufnr] = nil -- clear
    end
  })
end

function C.getMinBufnr()
  local min = math.huge
  for n, _ in pairs(buffers) do
    if n < min then min = n end
  end
  return min
end

function C.getByBufnr(bufnr)
  if not bufnr or bufnr == 0 then bufnr = C.getMinBufnr() end
  return buffers[bufnr or false]
end

function C.getBuffers()
  return buffers
end

-- for hotswap reloading
function C.setBuffers(map)
  buffers = map or {}
end

--
---@param jar_file string
---@return env.langs.java.ui.JarViewer?
---@return number? bufnr
function C.findAlreadyOpened(jar_file)
  local found, bufnr = nil, nil
  if jar_file ~= nil and #jar_file > 4 then
    for bufnr0, viewer in pairs(buffers) do
      if type(viewer) == 'table' then
        if viewer.jar_file == jar_file then
          found = viewer
          bufnr = bufnr0
          break
        end
      end
    end
  end
  log_debug("findAlreadyOpened %s for %s", found ~= nil, jar_file)
  return found, bufnr
end

--
-- allows you to get an object by UI buffer number
-- to get first JarViewer use JarViewer.viewer(nil)
--
---@param bufnr number
---@return env.langs.java.ui.JarViewer
function C.viewer(bufnr)
  local viewer = buffers[bufnr or false]
  if not viewer then
    error('Not found JarViewer for buffer ' .. v2s(bufnr))
  end ---@cast viewer env.langs.java.ui.JarViewer
  assert(bufnr == viewer.bufnr)
  return viewer
end

-- to get first opened JarViewer
---@return env.langs.java.ui.JarViewer?
function C.getFirstViewer()
  return buffers[C.getMinBufnr() or false]
end

--
--
--
function C:render()
  local lines = {}
  local lnum2path = {}
  self.renderTreeToLines(lines, lnum2path, self.dirs_state, self.tree, 0, nil)
  self.lnum2path = lnum2path

  local api = vim.api
  local bufnr = assert(self.bufnr, 'bufnr')

  api.nvim_buf_set_option(bufnr, 'modifiable', true)
  api.nvim_buf_set_lines(bufnr, 0, -1, true, lines or {})
  api.nvim_buf_set_option(bufnr, 'modifiable', false)
end

---@param lnum number
function C.moveCursorTo(lnum)
  if type(lnum) == 'number' and lnum > 0 then
    vim.api.nvim_win_set_cursor(0, { lnum, 1 })
  end
end

local function isDir(state)
  return state == CLOSED_DIR or state == OPENED_DIR
end

---@param path string
function C.get_parent_dir(path)
  if type(path) == 'string' and path ~= '' then
    return match(path, "^(.-/)[^/]+/$") or match(path, "^(.-/)[^/]+$")
  end
  return nil
end

local function get_lnum_for_path(lnum2path, path)
  for lnum, path0 in pairs(lnum2path) do
    if path0 == path then
      return lnum
    end
  end
  return -1;
end


---@param dir string
---@param file string? file inside given directory
---@return boolean
function C:moveCursorToElement(dir, file)
  if dir:sub(-1, -1) ~= '/' then dir = dir .. '/' end
  local lnum = get_lnum_for_path(self.lnum2path, dir)
  if not lnum or lnum < 0 then
    return false
  end

  C.moveCursorTo(lnum)
  local success = true

  if file and #file > 0 then -- find file
    success = false
    local lines = vim.api.nvim_buf_get_lines(0, lnum - 1, -1, false)
    if lines and #lines > 1 then
      local ind = match(lines[1] or '', '^(%s*)')
      for n = 2, #lines do
        local line = lines[n] or ''
        local ind0 = match(line, '^(%s*)')
        if ind0 == ind then
          break -- end of files in dir
        end
        if string.find(line, file, 1, true) then
          C.moveCursorTo(lnum + n - 1)
          success = true
          break
        end
      end
    end
  end

  return success
end

--------------------------------------------------------------------------------
--                           Actions
--------------------------------------------------------------------------------

local function get_path_by_lnum(self)
  local win = vim.api.nvim_get_current_win()
  local lnum = (vim.api.nvim_win_get_cursor(win) or E)[1] -- lnums starts from 1
  assert(type(lnum) == 'number', 'lnum')

  local path0 = self.lnum2path[lnum or false]
  assert(type(path0) == 'string', 'Not Found path for lnum ' .. v2s(lnum))

  return path0, lnum
end

---@param path string
---@param prefix string?
---@return string
function C:get_full_path(path, prefix)
  prefix = prefix or ""
  return prefix .. v2s(self.jar_file) .. '!/' .. v2s(path)
end

-- jdt://contents/netty-all-4.0.10.Final.jar/io.netty.util.internal/SystemPropertyUtil.class?=research-agent/%5C/home%5C/swarg%5C/.m2%5C/repository%5C/io%5C/netty%5C/netty-all%5C/4.0.10.Final%5C/netty-all-4.0.10.Final.jar=/gradle_used_by_scope=/main,test=/%3Cio.netty.util.internal(SystemPropertyUtil.class

-- 'jdt://contents/slf4j-simple-2.0.16-sources.jar/org.slf4j.simple/OutputChoice.java
-- ?=dir/
-- org%5C/slf4j%5C/simple%5C/OutputChoice.java=/gradle_used_by_scope=/main,test=/%3Corg.slf4j.simple(OutputChoice.java'
--
-- "jdt://contents/cmd4j-0.1.0.jar/org.swarg.cli.cmd4j/CliFlow.class
-- ?=research-agent/
-- %5C/home%5C/swarg%5C/.m2%5C/repository%5C/org%5C/swarg%5C/cli%5C/cmd4j%5C/cmd4j%5C/0.1.0%5C/cmd4j-0.1.0.jar
-- =/gradle_used_by_scope=/main,test
-- =/%3Corg.swarg.cli.cmd4j(CliFlow.class"
---@param project_name string
function C:get_jdt_path(path, project_name, builder_scope)
  local jarFileBasename = fs.basename(self.jar_file)
  local classBaseName = fs.basename(path)
  local pkg = path:sub(1, -(#classBaseName + 2)):gsub('/', '.')
  project_name = project_name or 'dir'
  local escaped_full_path_to_jar = self.jar_file:gsub("/", "%%5C/")
  builder_scope = builder_scope or 'gradle_used_by_scope=/main,test'

  return fmt("jdt://contents/%s/%s/%s?=%s/" .. "%s=/%s=/%%3C%s(%s",
    jarFileBasename, pkg, classBaseName, project_name,
    escaped_full_path_to_jar, builder_scope, pkg, classBaseName)
end

---@param lines table
local function normalized_line_ends(lines)
  if (lines[1] or ''):sub(-1, -1) ~= "\r" then
    return lines
  end
  local t = {}
  for n, line in ipairs(lines) do
    t[n] = line:sub(-1, -1) == "\r" and line:sub(1, -2) or line
  end
  return t
end
--
-- extract file to the new buffer
--
---@param path string
---@param opts table?
function C:doOpenFile(path, opts)
  log_debug('doOpenFile', path)
  local ext = fs.extract_extension(path)
  if ext == 'class' then
    local path2javafile, err = self:doUnzipDecompileClassFile(path, opts)
    if not path2javafile then
      return print(err)
    end
    -- escaping the '$'-symbol, if this is not done it will resolve as EnvVar
    vim.cmd(":e " .. v2s(path2javafile):gsub('%$', '\\$'))
    return
  end

  local cmd = fmt("unzip -p '%s' '%s' 2>&1", self.jar_file, path)
  local lines = fs.execrl(cmd)
  if not lines then
    return print('error on execute ' .. v2s(cmd))
  end

  local syntax = EXT_TO_SYNTAX[ext or false]
  local bufname = self:get_full_path(path)

  local opened, bufnr = C.is_bufname_already_in_use(bufname)

  if opened and bufnr > 0 then
    vim.cmd(":b " .. v2s(bufnr)) -- to open already opened
  else
    C.create_content_buff(normalized_line_ends(lines), bufname, syntax, ext)
  end
end

function C:doToggleOpenClose()
  local path0, _ = get_path_by_lnum(self)
  local dir_state = self.dirs_state[path0]

  if isDir(dir_state) then
    self.dirs_state[path0] = TOGGLED_DIR_STATE[dir_state]
    self:render()
    --
  elseif dir_state == nil then -- is a file not a dir
    self:doOpenFile(path0)
  end
end

--
-- open current, selected file(archive entry|node) in opened archive (Jar-file)
--
function C:doOpen()
  local path0, _ = get_path_by_lnum(self)
  local dir_state = self.dirs_state[path0]

  if isDir(dir_state) then
    self.dirs_state[path0] = TOGGLED_DIR_STATE[dir_state]

    self:render()
    --
  elseif dir_state == nil then -- is a file not a dir
    self:doOpenFile(path0)
  end
end

--
-- close current node
--
function C:doClose()
  local path0, _ = get_path_by_lnum(self)
  local dir_state = self.dirs_state[path0]

  if dir_state == OPENED_DIR then
    self.dirs_state[path0] = CLOSED_DIR
  else
    local parent_dir = C.get_parent_dir(path0)
    if parent_dir and parent_dir ~= '' then
      self.dirs_state[parent_dir] = CLOSED_DIR
      self.moveCursorTo(get_lnum_for_path(self.lnum2path, parent_dir))
    end
  end

  self:render()
end

---@param typ string
function C:doCopyPath(typ)
  local path, _ = get_path_by_lnum(self)
  if not path then
    return print('Not Found path for current lnum')
  end

  if typ == 'basename' then
    if path:sub(-1, -1) == '/' then
      path = path:sub(1, #path - 1)
    end
    path = match(path, '([^/]+)$') or path
  elseif typ == 'full' then
    path = self:get_full_path(path)
  end
  -- copy to clipboard
  vim.fn.setreg("+", path)
  vim.fn.setreg('"', path)
  vim.fn.setreg("*", path) -- to primary?
  vim.fn.setreg('"', path)

  print('Copied ' .. v2s(path) .. ' to system clipboard.')
end

--
-- get full inner path inside the current opened archive(jar-file) to
-- the entry(node or file) under the cursor (by line-number)
--
---@return string?
---@return boolean isDirectory
function C:getInnerPathForSelectedArchiveEntry()
  local path, _ = get_path_by_lnum(self)
  return path, isDir(self.dirs_state[path])
end

---@param inner_path string path to class file inside the jar
---@param opts table?
---@return string? absolute path to file
---@return string? errmsg
function C:doUnzipDecompileClassFile(inner_path, opts)
  log_debug("doUnzipDecompileAndOpenClassFile", inner_path)
  local path2classfile, path2javafile, err

  -- check alredy exists
  if self.jar_file and inner_path and self.decomp_outdir then
    path2javafile = join_path(self.decomp_outdir, inner_path)
    path2javafile = path2javafile:gsub('%.class', '.java')

    local mtime = fs.last_modified(path2javafile)
    log_debug('check_to_alredy_exists mtime:%s %s', mtime, path2javafile)
    if mtime and mtime > (fs.last_modified(self.jar_file) or 0) then
      log_debug('alredy decompiled: %s -> %s', inner_path, path2javafile)
      return path2javafile
    end
  end

  path2classfile, err = self:extract_file(inner_path, nil)
  if not path2classfile then
    return nil, err
  end

  path2javafile, err = self:decompile_classfile(path2classfile, inner_path, opts)
  if not path2javafile then
    return nil, err or ('cannot decompile ' .. v2s(path2javafile))
  end
  if not file_exists(path2javafile) then
    return nil, 'Not Found ' .. v2s(path2javafile) .. v2s(err)
  end

  log_debug('path2javafile:', path2javafile)

  return path2javafile
end

-- find absolute path to extracted class-file
-- unzip 'artifact.jar' 'path/to/some.class' -d '/tmp/outputdir/'
--
-- Output will be like:
-- {
--   "Archive:  /home/user/dev/Launcher.jar",
--   "  inflating: /tmp/jarviewer/org/dev/Main.class  "
-- }
--
---@param inner_path string path to file inside jar(war) archive file
---@param outdir string?
---@return string? absolute path to extracted file
---@return string? errmsg
function C:extract_file(inner_path, outdir)
  outdir = outdir or self.tmpdir
  log_debug("extract_file %s to %s", inner_path, outdir)
  if not dir_exists(outdir) then mkdir(outdir) end

  local cmd = fmt("unzip -o '%s' '%s' -d '%s' 2>&1",
    self.jar_file, inner_path, outdir)

  -- -o  overwrite files WITHOUT prompting
  log_debug('cmd:', cmd)
  local lines = fs.execrl(cmd)
  if not lines or type(lines) ~= 'table' then
    return nil, 'error on execute ' .. v2s(cmd)
  end

  -- find absolute path from the unzip output
  for _, line in ipairs(lines) do
    local act, path = string.match(line, "^%s+(.-):%s+(.-)%s+")
    if path ~= nil and (act == 'inflating' or act == 'extracting') then
      return path;
    end
  end
  -- caution: filename not matched: ...

  return nil, 'cannot extract ' .. v2s(inner_path) .. ' from '
      .. v2s(self.jar_file)
      .. "\n" .. table.concat(lines, "\n")
end

--
-- to findout jvm version of classfile:
-- javap -v path/to/Some.class | head
-- major version:
-- - 52 -- is java 8
-- - 61 -- is java 17
---@return string?
---@return string? errmsg
---@param opts table?
function C:decompile_classfile(path_to_classfile, inner_path, opts)
  opts = opts or E
  if opts.decompiler then
    if not is_supported_decompiler(opts.decompiler) then
      return nil, 'unsupported decompiler: ' .. v2s(opts.decompiler)
    end
    self.decompiler = opts.decompiler
  end
  if opts.output_dir then
    self:setDecompOutDir(opts.output_dir)
  end

  local decompiler_name, err = self.decompiler, nil
  if not decompiler_name then
    decompiler_name, err = self:chooseDecompilerInteractively()
    if not decompiler_name then
      return nil, err
    end
    self.decompiler = decompiler_name;
  end

  log_debug('Decompiler to use: ', decompiler_name)

  local params = {
    inner_path = inner_path,
    decompiler = decompiler_name,
    verbose = opts.verbose,
    output_dir = self.decomp_outdir,
  }
  -- path_to_decompiled_javafile
  return decomp_manager.decompile_classfile(path_to_classfile, params)
end

--
-- ask decompiler name
-- ask the user which of the available decompilers should be used
--
---@return string?
---@return string? errmsg
function C:chooseDecompilerInteractively()
  local decompilers_list = decomp_manager.get_decompilers_list()
  local i = C.pickItemIndex(decompilers_list, 'Choose Decompiler: ')
  if not i or i < 1 or i > #decompilers_list then
    return nil, 'canceled' -- or out of bounds
  end
  local decompiler_name = decompilers_list[i]
  return decompiler_name, nil
end

--
--
--
--
---@param items table
---@param prompt string
function C.pickItemIndex(items, prompt)
  local s = ''
  for n, item in ipairs(items) do
    s = s .. fmt("%2d %s\n", n, v2s(item))
  end
  prompt = prompt or 'Choose Index:'
  local output
  vim.ui.input({ prompt = s .. prompt }, function(input)
    output = tonumber(input)
  end)

  return output
end

--
-- join the given cwd with a given path if it a relative one
-- if cwd is not defined take from envvar
--
---@param cwd string?
---@param path string
---@return string
function C.build_path(cwd, path)
  if fs.is_os_windows() and string.find(path, '/') then
    string.gsub(path, '/', '\\')
  end
  cwd = cwd or os.getenv('PWD')
  return fs.build_path(cwd, path)
end

--
-------------------------------------------------------------------------------
--                        content searcher helper
-------------------------------------------------------------------------------

-- to quick open element in jar by a inner path or a full class or package name
function C:doOpenElmByClipboard()
  local text0 = vim.fn.getreg("+")
  local text = match(text0 or '', '^%s*([^%s:]+)')
  log_debug("doOpenElmByClipboard: |%s|", text, text0)
  if not text or #text < 0 then return end
  local pkg, cls = match(text, '^(.-)[%./]?(%u[%w%_%$]*)$')
  if not pkg then return end
  local ok, err
  ok, err = self:openFileTreeElm(pkg, cls)
  return ok, err
end

-- makeDirOpened to show as FileTree
---@return boolean
function C:openFileTreeDir(dir)
  local node, tpath = self:getNodeAt(dir, '/')
  if not node or not tpath then
    return false
  end

  -- mark path of subdirs as opened
  local path0 = ''
  for _, subdir in ipairs(tpath) do
    path0 = path0 .. subdir .. '/'
    self.dirs_state[path0] = OPENED_DIR
  end
  return true
end

--
--
--
---@param dir string
---@param cls string?
---@return boolean
---@return string? errmsg
function C:openFileTreeElm(dir, cls)
  if not string.find(dir or '', '/', 1, true) then
    dir = dir:gsub('%.', '/') -- package to dir
  end
  if not self:openFileTreeDir(dir) then
    return false, 'not found dir:' .. v2s(dir) .. ' in ' .. v2s(self.jar_file)
  end
  vim.cmd(':b ' .. v2s(self.bufnr))
  self:render()

  return self:moveCursorToElement(dir, cls)
end

--
-- get a filetree node (map of file and dirs names) in given path
-- from "opened" jar file
--
-- can be used to get all files in given package
--
---@param path string
---@return table? files in the current node
---@return table? tpath
function C:getNodeAt(path, sep)
  log_trace("getNodeAt", path, sep)
  if not self.tree or not path or path == '' then
    return nil
  end
  sep = sep or '/' -- to treat path as package use '.'
  local tpath = su.split(path, sep)
  log_trace("getFilesAt path-to-open:%s sep:%s", tpath, sep)
  local dirnode = self.tree

  for i = 1, #tpath do
    local subdirname = tpath[i]
    dirnode = dirnode[subdirname or false]
    if dirnode == nil then
      log_trace('not found subdir:%s depth:%s', subdirname, i)
      return nil;
    end
  end
  return dirnode, tpath
end

--
-- get a list of file and dirs names in given path from "opened" jar file
--
function C:getFilesAt(path, sep, files_only)
  local dirnode = self:getNodeAt(path, sep)
  local t = {}
  for fname, node0 in pairs(dirnode or E) do
    if not files_only or type(node0) ~= 'table' then -- is dir
      t[#t + 1] = fname
    end
  end
  log_debug('found files:%s', #t)
  return t
end

--
-- check is current opened jar file has given file (with full inner path)
--
---@param inner_path string?
---@return boolean
function C:hasFile(inner_path)
  local has = false
  if inner_path and #inner_path > 0 then
    local dir = fs.get_parent_dirpath(inner_path)
    local fn = fs.basename(inner_path)
    if dir then
      local t = self:getNodeAt(dir, '/')
      return type(t) == 'table' and t[fn] ~= nil
    end
  end
  log_debug("hasFile %s %s in jar:%s", inner_path, has, self.jar_file)
  return has
end

--
-- check is opened jar has class-file
--
---@param pkg string the package name
---@param scn string short classname
---@return string?
function C:findClass(pkg, scn)
  local path = nil
  local t = self:getNodeAt(pkg, '.') -- files and dirs
  if t and scn and scn ~= '' then
    local suff = nil
    local file = true
    if t[scn .. '.class'] == file then
      suff = '.class'
    elseif t[scn .. '.java'] == file then
      suff = '.java'
    elseif t[scn] == file then
      suff = ''
    end
    if suff ~= nil then
      path = pkg:gsub('%.', '/') .. '/' .. v2s(scn) .. suff
    end
  end
  log_debug('findClass %s pkg:%s scn:%s (%s)', path, pkg, scn, self.bufnr)
  return path
end

--
-------------------------------------------------------------------------------
--                       java source code helper
-------------------------------------------------------------------------------

C.code = {}

local function is_dot_char(s, i)
  return s ~= nil and s:sub(i, i) == '.'
end

--
-- the simplest search for the full class name by import content
--
---@param bufnr number?
---@return string?
---@param inner boolean?
function C.code.find_package_for_short_classname(bufnr, short_classname, inner)
  bufnr = bufnr or 0
  local max = vim.api.nvim_buf_line_count(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, max, false)
  local scn_len = #(short_classname or '')

  local pkg = nil

  for _, line in ipairs(lines) do
    local ind, w1, w2, w3 = match(line, '^(%s*)(%w+)%s+([%w_%.]+)%s*([%w_%.;]*)')
    if ind == '' then
      if not pkg and w1 == 'package' then
        pkg = w2
      elseif w1 == 'import' then
        if scn_len > 0 and is_dot_char(w2, #w2 - scn_len)
            and su.ends_with(w2, short_classname) then
          return w2 -- fullpackage name found from import line
        end

        -- "public class", or protected final class
      elseif w2 == 'class' or w3 == 'class' then -- stop searching
        if inner then
          local curr_class_name = match(line, 'class%s+([%w_]+)')
          if curr_class_name then
            short_classname = v2s(curr_class_name) .. '$' .. v2s(short_classname)
          end
        end
        break;
      end
    end
  end
  if pkg == nil then return nil end -- not found package

  return (scn_len > 0) and (pkg .. '.' .. short_classname) or pkg
end

---@return string? pkg
---@return string? cls
---@return string? errmsg
function C.code.pick_package_from_line_in_buffer()
  local line = vim.api.nvim_get_current_line()
  if not line then
    return nil, nil, 'not found line under the cursor'
  end
  local cls = nil
  local pkg = match(line, 'package%s+([^%s]+)%s*;')
  if not pkg then
    pkg = match(line, '^%s*import%s+([^%s]+)%s*;')
    pkg, cls = match(pkg or '', '^(.-)%.([%w%_%$]+)$')
  end
  if not pkg then
    return nil, nil, 'not found package from current line: ' .. v2s(line:sub(1, 80))
  end
  return pkg:gsub('%.', '/'), cls, nil
end

--
-- java-source-code helper
--
---@return string? full classname
---@return string? errmsg
---@param as_inner boolean
function C.code.pick_classname_from_line_in_buffer(as_inner)
  local Editor = require 'env.ui.Editor'

  local bufname = vim.api.nvim_buf_get_name(0)
  if not bufname then
    return nil, 'no opened buffer'
  end

  local line = vim.api.nvim_get_current_line()
  if not line or #line < 4 then
    return nil, 'no current line or line is empty'
  end

  local ext = match(bufname, '%.(%w+)$')
  if ext == 'java' or ext == 'class' then
    local kw, word = match(line, '^(%w+)%s+([%w%.%_]+)%s*;%s*$')
    if kw == 'import' and word then
      return word, nil
    end

    -- from type in field to pick from same package
    local ctx = Editor:new():resolveWordSimple('[%w_]+')
    local short_cn = ctx.word_element
    if not short_cn then
      return nil, 'not found any class name under cursor in current line'
    end
    local cn = C.code.find_package_for_short_classname(0, short_cn, as_inner)
    if not cn then
      return nil, 'cannot find package for class:' .. v2s(short_cn)
    end
    return cn -- "pkg.short_cn"
  end

  return nil, 'unsupported file ext ' .. v2s(ext)
end

function C:expandInnerPathTo(path)
  if path and path ~= '' then
    local limit = 1000
    while #path > 0 and limit > 0 do
      self.dirs_state[path] = OPENED_DIR
      path = fs.get_parent_dirpath(path) or ''
      limit = limit - 1
    end
    self:render()
    -- todo set cursor to line with class if given
  end
end

-- to find Class by name or pattern
---@param tree table?
---@param path string?
function C:findEntry(name, is_patt, tree, path)
  path = path or ""
  tree = tree or self.tree
  local dirs, files = sorted_keys0(tree)

  for _, file in ipairs(files) do
    if (not is_patt and name == file) or (is_patt and match(file, name)) then
      if #path > 0 and path:sub(-1, -1) ~= '/' then path = path .. '/' end
      self:expandInnerPathTo(path)
      return path .. v2s(file)
    end
  end

  for _, dir in ipairs(dirs) do
    local dirpath = path .. dir .. "/"
    if not is_patt and name == dirpath then
      return dirpath
    end
    local subdir = tree[dir]
    local found = self:findEntry(name, is_patt, subdir, dirpath)
    if found then
      self:expandInnerPathTo(found)
      return found
    end
  end
  return nil -- not found
end

function C:expandAll(path, tree)
  tree = tree or self.tree
  path = path or ""
  local dirs_state = self.dirs_state
  local dirs, _ = sorted_keys0(tree)

  for _, dir in ipairs(dirs) do
    local dirpath = path .. dir .. "/"
    local subdir = tree[dir]
    dirs_state[dirpath] = OPENED_DIR
    self:expandAll(dirpath, subdir)
  end

  if tree == self.tree then
    self:render();
  end
end

function C:collapseAll()
  self.dirs_state = {}
  self:render();
end

--

class.build(C)
return C

-- 15-03-2024 @author Swarg
-- snippets for EnvLineInsert for Java
--
--
--

local M = {}

local insert_items = {
  -- shared: "${HANDLER_ADD_SAMPLE}",
  '${HANDLER_NEW_INSTANCE}', -- Object object = new Object();
  '',
  '${HANDLER_FIX_IF_PARENTHESESS}',
  '\n    public void ${WORD}() {\n        ;\n    }\n',
  '${TAB} System.out.println("${IDENTIFIER}:" + ${IDENTIFIER});\n',
  ':EnvRefactor class full-name<CR>', -- copy the fullname of the current opened class
  ':EnvRefactor add loggin-message debug<CR>',
  '${DATE} $TIME',
  "Optional<${WORD}>",
  ':EnvNew test-method<CR>',
  ':EnvRefactor fix junit method-args-order<CR>', -- junit4 -> junt5 fix
  ':EnvRefactor fix raw-types<CR>', -- to fix legacy code
  '', -- extract exp
  '${TAB} StringBuilder sb = new StringBuilder();\n',
  '',
  '',
  '\n    /**\n     *\n     */\n',    -- doc-comment
  "'${IDENTIFIER}:', ${IDENTIFIER}", -- 18
  '${HANDLER_MK_FILE_REF}',
  '',                    --
  "assertEquals(1, ${LINE})",        -- 21
  '        assertEquals("X", ${WORD});\n',
  '        String res = ${WORD};\n        assertEquals("", res);\n', -- 23
  '',
  '', -- 25
  '',
  '',
  '',
  '${HANDLER_MK_REF_TO_METHOD}',
  '\n/**\n *\n * ${DATE}\n * @author ${AUTHOR} \n */\n',
  "${TOGGLE_BOOL}",
  '',
  '',
  '',
  '',
  '',
  '',
  'private static final Logger log = LoggerFactory.getLogger(${Class}.class);', -- 38
  '',
  '',
  '',
  "${TAB} throw new IllegalStateException(\"Not implemented yet\");\n",
  '',
  '',
  '',
  '',
  '',
  '',
  '${HANDLER_RE_SHOW_PKG_CLS_IN_JAR}', -- 49
  '${HANDLER_REFACTOR_REALIGN_LINES}', -- 50
  '${HANDLER_GEN_VALUE_CLASS}',        -- boilerplate code gen like @lombok.Value
  '${HANDLER_GEN_DATA_CLASS}',         -- ToString,EqualsAndHashCode, Getter,Setter
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',
  '',  -- Spring:
  '${HANDLER_NEW_BEAN}', -- 71
  '${HANDLER_INJECT_DEPENDENCIES}',
  '${HANDLER_NEW_ENDPOINT}',
  '${HANDLER_NEW_VALUE_PROPERTY_FIELD}',
}
--insert_items[10] = '\n    @Test\n    public void test_${WORD}() {\n'
--    .. '        System.out.println("test_${WORD}");\n'
--    .. '        assertEquals(${WORD}());\n    }\n'

local insert_mappings = {
}

-- for [0]: "${HANDLER_ADD_SAMPLE}",
local samples = {
  mvn_dep = {
    "		<dependency>",
    "			<groupId></groupId>",
    "			<artifactId></artifactId>",
    "     <version></version>",
    "			<scope></scope>",
    "		</dependency>"
  }
}

local Editor = require 'env.ui.Editor'
local StatefulBuf = require 'env.draw.ui.StatefulBuf'
local su = require 'env.sutil'
local fs = require 'env.files'
local log = require 'alogger'
local ucb = require 'env.util.clipboard'
local lapi_consts = require 'env.lang.api.constants'
local JParser = require 'env.langs.java.JParser'
local syntax = require 'env.langs.java.util.syntax'
local uspring = require 'env.langs.java.util.spring.consts'
local spring_gen = require 'env.langs.java.util.spring.generator'
local grammar = require 'env.langs.java.util.grammar'
local eclipse = require 'env.bridges.eclipse'
local common = require 'env.lang.utils.common'
local codegen_handler = require 'env.langs.java.util.codegen.handler'

local E, v2s, fmt, match = {}, tostring, string.format, string.match
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
local tags = grammar.tags
local log_debug = log.debug


function M.handler_new_instance()
  local editor = Editor:new()
  local vn, errmsg = editor:resolveWords():getResolvedWord(LEXEME_TYPE.UNKNOWN)
  if not vn then
    return false, errmsg
  end
  -- -- lua_parser.find_var_definition
  -- local t, err = editor.ctx:find_var_definition0(lua_parser, vn, 16)
  -- if not t or not t.expr then
  --   return false, err
  -- end
  local is_klass, expr, klass
  local args, fch = '', vn:sub(1, 1)

  if fch == string.upper(fch) then
    is_klass = true
  end
  if is_klass then
    klass = vn
    vn = string.lower(fch) .. vn:sub(2) -- Object -> object
    expr = fmt('%s %s = new %s(%s);', klass, vn, klass, args)
  end

  if editor:replaceCurrentWord(expr) then
    return true
  end
  return false, 'cannot replace current word'
end

function M.handler_fix_if_parenthesess()
  local editor = Editor:new()
  local ctx = editor:getContext()
  if not ctx:hasCurrentLine() then
    return false, 'no current line'
  end
  local line
  line = ctx.current_line ---@cast line string
  local if_sp, if_ep = string.find(line, 'if', 1, true)
  if not if_sp or not if_ep then
    return false, 'not found "if"'
  end
  -- if string.find(line, '(', if_sp, true) then
  if string.match(line, 'if%s*%(', if_sp) then
    return true, 'already with ()'
  end
  if line:sub(if_ep + 1, if_ep + 1) == ' ' then
    if_ep = if_ep + 1
  end
  line = su.replace_in_range(line, if_sp, if_ep, 'if (') .. ')'
  if line:sub(-1, -1) == ')' then
    line = line .. ' {'
    local tab = match(line, "^(%s+)")
    line = { line, tab .. "    ;", tab .. "}" }
  end
  if ctx:setCurrentLine(line) then
    return true
  end
  return false, 'cannot update current line'
  -- if response == "OK")  -- > if (response == OK) {\n   $ }\n
end

-- todo with Autowired or nearest for cursor
local predicate_stop_on_first_not_empty_constructor = function(parser0, tag)
  if tag == tags.CONSTRUCTOR then
    local e = (parser0.last_elm or E)[2] -- [1] is TAGTYPE
    -- todo with @Autowired
    return #((e or E).params or E) > 0
  end
  return false
end

--
-- create readable link to method under the cursor
-- package + classname + method + lnum
--
function M.handler_mk_ref_to_method()
  local ctx = Editor:new():getContext()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'empty line (expected line with method signature definition)'
  end
  local method_name = ctx:resolveWords():getResolvedMethodOrFunction()
  log_debug("method_name under cursor", method_name)
  if not method_name then
    return false, 'not found method name under cursor'
  end

  local parser = JParser:new(nil, Editor.iterator(0))
      :prepareConstructors(predicate_stop_on_first_not_empty_constructor)

  if parser:hasErrors() then
    return false, parser:getErrLogger():getReadableReport()
  end

  local classname = parser:getMainClassName(true)
  local line = v2s(ctx.current_line)
  ucb.copy_to_clipboard(v2s(classname) .. ' ' .. v2s(method_name)
    .. ' // ' .. v2s(ctx.cursor_row or '?') .. "\n"       -- current line number
    .. (match(line, '%s*([^{]+)%s*{?%s*') or line) .. ';' -- signature of method
  )
  print("copied to system clipboard.")
  return true
end

----
-- source code refactoring

-- realign lines of decompiled sources code provided by decompiler
-- (by linenum mapping in the ends of the each line of source-code)
function M.handler_refactor_realign_lines()
  local refactor = require 'env.langs.java.command.refactor'
  refactor.do_realign_lines(0)
  return true
end

-----
--
-- reverse_enginering integration with JarViewer
--

--
function M.handler_re_show_pkg_cls_in_jar()
  local jar_viewer = require 'env.command.jar_viewer'
  local Cmd4Lua = require 'cmd4lua'
  jar_viewer.cmd_open_package(Cmd4Lua.of('.'))
end

--
--
-- By given ShortClassNames create private class fields and add it into first
-- not empty constructor (aka DI for @Autowired constructor)
--
-- todo: grab class names: in visual mode when user select fields
--
---@return boolean
---@return string? errmsg
function M.handler_inject_dependencies()
  local ctx = Editor:new():getContext()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'nothing to inject'
  end
  local beans = su.split(ctx:pullCurrLine(), ' ') -- ShortClassNames to inject
  log_debug("beans to inject", beans)

  local fields, params = syntax.get_code_fields_from_classnames(beans)

  local parser = JParser:new(nil, Editor.iterator(0))
      :prepareConstructors(predicate_stop_on_first_not_empty_constructor)

  if parser:hasErrors() then
    return false, parser:getErrLogger():getReadableReport()
  end

  local classname = parser:getMainClassName(false)
  local lnum4fields = parser:getLnumToAddNewField()
  local e = parser:getFirstNotEmptyConstructor()
  log_debug('parsed source lnum:%s constructor:%s', lnum4fields, e)

  if type(e) == 'table' then -- update existing not empty constructor
    local asglines = syntax.get_code_of_assign_params_to_private_fields(params)

    e = syntax.add_params_to_method(e, params)
    local signature = syntax.get_code_of_constructor_singature(e)
    local signature_lne = ((e.params or E).lne or e.ln)
    ctx:setLines(e.lne, e.lne - 1, asglines)
    ctx:setLines(e.ln, signature_lne, su.split(signature, "\n"))

    log_debug("signature(%s:%s):|%s|\nassigments(%s):|%s|",
      e.ln, signature_lne, signature, e.lne, asglines)
  else
    -- create new constructor
    e = syntax.new_di_constructor(classname, params, lnum4fields)
    local lines = syntax.syntax_elm_to_code(e, syntax.tags.CONSTRUCTOR, { '' })
    lines[#lines + 1] = ''
    ctx:setLines(lnum4fields, lnum4fields - 1, lines)
  end

  -- add fields of injected beans
  ctx:setLines(lnum4fields, lnum4fields - 1, fields)

  local annotations = uspring.get_required_component_annotations(parser.clazz)
  if annotations then
    -- add lines before class definition
    ctx:setLines(parser.clazz.ln, parser.clazz.ln - 1, annotations)
    -- todo import
  end

  return true
end

function M.handler_new_endpoint()
  local ctx = Editor:new():getContext()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'empty line expected <get|post> "endpoint" templName'
  end
  local fn = ctx:getCurrentBufname()
  local words = su.split(ctx:getCurrentLine(), ' ')
  local http_method, endpoint, templname = words[1], words[2], words[3]
  local e, err = spring_gen.new_endpoint(http_method, endpoint, templname, fn)
  if not e or not e.code then
    return false, err
  end
  ctx:setCurrentLine(e.code)

  if not fs.file_exists(e.templ_path) then
    local saved = fs.write(e.templ_path, e.templ_body)
    log_debug("saved", saved, e.templ_path)
    if saved then
      ---@diagnostic disable-next-line: param-type-mismatch
      Editor.echoInStatus(nil, 'Created new template ' .. v2s(e.templ_path))
    end
  end

  return true
end

-- "RestTemplate" -->  @Bean RestTemplate getRestTemplate() { return new Rest..}
function M.handler_new_bean()
  local ctx = Editor:new():getContext()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'empty line expected Bean ShortClassName [props]'
  end
  local words = su.split(ctx:getCurrentLine(), ' ')
  local beanClass = words[1]
  table.remove(words, 1)
  local opts = {
    filename = ctx:getCurrentBufname(),
    params = words,
  }
  local e, err = spring_gen.new_bean(beanClass, opts)
  if not e or not e.code then
    return false, err
  end
  ctx:setCurrentLine(e.code)
  return true
end

-- "app.url" --> @Value("${app.url}")\nprivate String appUrl
function M.handler_new_value_property_field()
  local ctx = Editor:new():getContext()
  if ctx:isCurrLineEmptyOrComment('^%s*//') then
    return false, 'empty line, expected property name'
  end
  local property = su.split(ctx:getCurrentLine(), ' ')[1]
  local lines, err = spring_gen.new_value_property_field(property)
  if not lines then
    return false, err
  end
  ctx:setCurrentLine(lines)
  return true
end

local function get_jdt_for_curr_buf()
  local bufname = Editor.getCurrentFile()
  local t = eclipse.parse_jdt_path(bufname) -- {path}
  if type(t) ~= 'table' then                -- or not t.path then
    return false, 'cannot parse curret buf name: ' .. v2s(bufname)
  end
  return t, nil
end

function M.handler_jdt_path()
  local t, err = get_jdt_for_curr_buf()
  if not t then return false, err end
  if not t.path then return false, 'not found path in jdt://' end

  ucb.copy_to_clipboard(t.path)
  print(t.path)

  return true
end

function M.handler_jdt_path_open_jar()
  local t, err = get_jdt_for_curr_buf()
  if not t then return err end
  local path = t.path
  if not path then return false, 'not found path in jdt://' end
  if not fs.file_exists(path) then
    return false, 'not found file: ' .. v2s(path)
  end
  local list = fs.execrl('jar tf ' .. v2s(path))

  local dispatcher = {
    on_write = function() end,
    on_read = function() end,
    on_delete = function() end,
  }
  StatefulBuf.create_buf(path .. '~', list, dispatcher, { jdt = t })
  return true
end

--
local insert_handlers = {
  NEW_INSTANCE = M.handler_new_instance,
  FIX_IF_PARENTHESESS = M.handler_fix_if_parenthesess,
  -- spring
  INJECT_DEPENDENCIES = M.handler_inject_dependencies,
  NEW_ENDPOINT = M.handler_new_endpoint,
  NEW_BEAN = M.handler_new_bean,
  NEW_VALUE_PROPERTY_FIELD = M.handler_new_value_property_field,
  --
  MK_FILE_REF = common.handler_mk_file_ref,
  MK_REF_TO_METHOD = M.handler_mk_ref_to_method,
  REFACTOR_REALIGN_LINES = M.handler_refactor_realign_lines,
  RE_SHOW_PKG_CLS_IN_JAR = M.handler_re_show_pkg_cls_in_jar,
  GEN_VALUE_CLASS = codegen_handler.gen_value_class,
  GEN_DATA_CLASS = codegen_handler.gen_data_class,
}

local class_insert_items = {
  '${HANDLER_JDT_PATH}',          -- 1 parse jdt:// path to clipboard
  '${HANDLER_JDT_PATH_OPEN_JAR}', -- 2
  '',
  '',
  '',
  ':EnvRefactor -PL java class full-name<CR>', -- 6
}

local class_insert_handlers = {
  JDT_PATH = M.handler_jdt_path,
  JDT_PATH_OPEN_JAR = M.handler_jdt_path_open_jar,
  MK_FILE_REF = common.handler_mk_file_ref,
}

---@param reg function
function M.add(reg)
  reg('java', insert_items, insert_mappings, insert_handlers, samples)
  reg('class', class_insert_items, {}, class_insert_handlers, nil)
end

return M

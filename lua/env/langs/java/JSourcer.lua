-- 10-01-2025 @author Swarg
--
-- Source Manager
-- The task of this class is to manage and interact with the ClassDefinition -
-- the output of the JParser (parsed source code of java)
--

local log = require 'alogger'
local class = require 'oop.class'

local fs = require 'env.files'
local core = require 'env.langs.java.util.core'
local Sourcer = require 'env.lang.Sourcer'
local ClassInfo = require 'env.lang.ClassInfo'
local JParser = require 'env.langs.java.JParser'
-- local G = require 'env.langs.java.util.grammar'

class.package 'env.langs.java'
---@class env.langs.java.JSourcer : env.lang.Sourcer
---@field new fun(self, o:table?, lang:env.langs.java.JLang): env.langs.java.JSourcer
---@field map table cn->clazz(from parser)
---@field lang env.langs.java.JLang
local C = class.new_class(Sourcer, 'JSourcer', {
})

local log_debug, log_trace = log.debug, log.trace


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local isFullClassNameWithPackage = core.isFullClassNameWithPackage
local find_mapping_starts_with = core.find_mapping_starts_with


--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

local STOP_PREDICATE = JParser.predicate_stop_on


---@return olua.util.lang.AParser
function C:getParserClass()
  return JParser
end

---@param classname string
---@param path string?
---@param stop_predicate function
---@return env.lang.oop.ClassDefinition?
---@return string?
function C:getClassDef(classname, path, stop_predicate)
  local cdef = self.map[classname or false]

  if not cdef then
    cdef = self:loadClassDef(path, stop_predicate)
    self.map[classname] = cdef
  end
  return cdef
end

--
-- parse given ci to extract classname from "A extends B"
--
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo?
---@return boolean? has_parent
---@return string? errmsg
function C:getParentClass(ci)
  log_debug("getParentClass for %s", ClassInfo.getClassName((ci or E)))
  if not ci or not ci:getClassName() then
    return nil, nil, 'empty ClassInfo'
  end

  local cn = ci:getClassName()
  local cdef = self:getClassDef(cn, ci:getPath(), STOP_PREDICATE.classdef)
  if not cdef then return nil, nil, 'error cannot parse ' .. v2s(cn) end

  local pcn = cdef:getParentName()
  if not pcn then return nil, false end -- no parent (no extends ...)

  if not isFullClassNameWithPackage(pcn) then
    local pcn0 = cdef:findClassInImport(pcn)
    -- if there is no such class in the import, check in the same package as
    -- the current one
    if not pcn0 and self.lang then
      pcn = v2s(cdef:getPackage()) .. '.' .. v2s(pcn) -- guess
    end
  end

  local pci, err = self.lang:findExistedClassByClassName(pcn) -- with caching
  if not pci and err and err ~= '' then log_debug('pcn:%s error:', pcn, err) end

  if pci ~= nil and pci:getClassName() ~= nil then
    cdef:setParentName(pci:getClassName())
    return pci, true, nil
  end

  return nil, false
end

--
--   abcde, cd, CD  -->  abCDe
--
---@return string?
local function replace_once(line, oldsub, newsub)
  local i, j = string.find(line, oldsub)
  if not i or not j then
    return nil
  end
  local updated_line = line:sub(1, i - 1) .. newsub .. line:sub(j + 1)
  return updated_line
end


---@param dir string path to file or directory
---@return boolean
---@return string? errmsg
function C:fixPackageInClassFiles(dir)
  if not fs.is_directory(dir) then
    return self:fixPackageInClassFile(dir)
  end

  local list = {}
  fs.list_of_files_deep(dir, '%.java$', list)
  for n, path in ipairs(list) do
    local ok, err = self:fixPackageInClassFile(path)
    if not ok then
      return false, 'error on [' .. v2s(n) .. ']: ' .. v2s(err)
    end
  end
  return true
end

--
-- fix package name by real path in given source file
-- with loading give file into the nvim buffer
-- (lines are updated inside the nvim buffer and not in the file itself)
--
---@param path string path to source file
---@return boolean
---@return string? errmsg
function C:fixPackageInClassFile(path)
  log_debug("fixPackageInClassFile", path)

  local lines = {}
  local cd, err = self:loadClassDef(path, STOP_PREDICATE.classdef, lines)
  if not cd or not cd.stop_info then return false, err end

  local old_pkg, lnum = cd.pkg, cd.pkg_ln
  if not old_pkg then
    return false, 'package def line not found'
  end
  if not lnum or lnum < 0 then
    return false, 'no lnum with original line of the package definition'
  end
  local original_line_with_pkg = lines[lnum] -- cd.stop_info.line
  if not original_line_with_pkg then
    return false, 'no original line from parser'
  end
  local inner_path, err2, _ = self:getInnerPath(path)
  if not inner_path then
    return false, "on getInnerPath:" .. v2s(err2)
  end
  local cn = self.lang:getClassNameForInnerPath(inner_path)
  local scn = ClassInfo.getShortClassNameOf(cn)
  if not cn or not scn then
    return false, 'no class or shortclass name'
  end
  local pkg = cn:sub(1, #cn - #scn - 1)
  if pkg == '' or pkg == '.' then
    return false, 'no package'
  end
  if old_pkg == pkg then
    return true, 'the package is already the same'
  end

  local updated_line = replace_once(original_line_with_pkg, old_pkg, pkg)
  if not updated_line then
    return false, 'not found old package name in the original line'
  end
  local update_lines_mapping = {
    [lnum] = updated_line,
  }
  log_debug("lnum:%s\noriginal: %s\nupdated:  %s",
    lnum, original_line_with_pkg, updated_line)

  local extra_pkg_mapping = {
    [old_pkg] = pkg
  }
  self:fixPackagesInImports(cd, update_lines_mapping, extra_pkg_mapping)

  local ok, err3 = self:updateFileContent(path, update_lines_mapping)

  return ok, err3
end

--
---@param cd env.lang.oop.ClassDefinition
---@param update_lines_mapping table
---@param pkg_mapping2 table? (from fixPackageInClassFile)
---@return boolean has_changes
function C:fixPackagesInImports(cd, update_lines_mapping, pkg_mapping2)
  log_debug("fixPackagesInImports")
  assert(type(update_lines_mapping) == 'table', 'update_lines_mapping')

  local pkg_mapping = self:getRenamePackageMapping()
  local has_mapping = type(pkg_mapping) == 'table' and next(pkg_mapping) ~= nil
  local has_extra = type(pkg_mapping2) == 'table' and next(pkg_mapping2) ~= nil

  local classname = v2s((cd or E).pkg) .. '.' .. v2s((cd or E).name)

  if not cd or #(cd.imports or E) < 0 or (not has_mapping and not has_extra) then
    log_debug("nothing to fix for", classname)
    return false
  end
  local imports = cd.imports
  local import_lns = cd.import_lns
  local c = 0
  assert(#imports == #import_lns, 'imports must be synchronized')

  for n, import in ipairs(imports) do
    local static = ''
    if match(import, '^static ') then
      static, import = 'static ', import:sub(8)
    end
    local old_pkg = find_mapping_starts_with(import, pkg_mapping, '.')
    local new_pkg = pkg_mapping[old_pkg or false]
    if not old_pkg and has_extra then ---@cast pkg_mapping2 table
      old_pkg = find_mapping_starts_with(import, pkg_mapping2, '.')
      new_pkg = pkg_mapping[old_pkg or false]
    end

    if old_pkg and new_pkg then
      local updated_import = new_pkg .. import:sub(#old_pkg + 1, #import)

      imports[n] = updated_import
      local import_lnum = import_lns[n] or -1
      log_trace("%s->%s = %s [%s]", old_pkg, new_pkg, import, v2s(import_lnum))

      update_lines_mapping[import_lnum] =
          'import ' .. static .. v2s(updated_import) .. ';'
      c = c + 1
    end
  end
  log_debug("fixed lines: %s for %s", c, classname)
  return c > 0
end

---@return boolean
---@return string? errmsg
function C:fixImportsInClassFile(path)
  log_debug("fixImportsInClassFile", path)
  local lines = {}
  local cd, err = self:loadClassDef(path, STOP_PREDICATE.classdef, lines)
  if not cd or not cd.stop_info then return false, err end

  local update_lines_mapping = {}
  if self:fixPackagesInImports(cd, update_lines_mapping, nil) then
    return self:updateFileContent(path, update_lines_mapping)
  end

  return false
end

--
-- to update a package in all classes (all files with source code)
--
-- for example, to switch to a new version of the library junit4 -> junit5
---@param opts table{all_test, all_main}
---@return number?
---@return string? errmsg
function C:updateAllImports(opts)
  log_debug("updateAllImports", opts)
  opts = opts or E
  local pkg_mapping = self:getRenamePackageMapping()
  local has_mapping = type(pkg_mapping) == 'table' and next(pkg_mapping) ~= nil
  if not has_mapping then
    return nil, 'no packages mapping to update'
  end
  if not self.lang then
    return nil, 'no JLang'
  end
  local fix_all_test, fix_all_main = opts.all_test, opts.all_main

  local project_root = self.lang:getProjectRoot()
  local cnt, dir, files = 0, nil, {}

  if fix_all_test then
    dir = fs.join_path(project_root, self.lang:getTestPath())
    fs.list_of_files_deep(dir, '%.java$', files)
    log_debug("found sources in test-dir: ", dir, #files)
  end

  if fix_all_main then
    local before = #files
    dir = fs.join_path(project_root, self.lang:getSrcPath())
    fs.list_of_files_deep(dir, '%.java$', files)
    log_debug("found sources in main-dir: ", dir, #files - before)
  end

  for _, path in ipairs(files) do
    if self:fixImportsInClassFile(path) then
      cnt = cnt + 1
    else
      log_debug('unchanged', path)
    end
  end

  return cnt, nil
end

--
--
--
---@param path string
---@return string? path
---@return string? errmsg
function C:getInnerPathFromSource(path)
  if not path or not fs.file_exists(path) then
    return nil, 'not found ' .. v2s(path)
  end
  local cd, err = self:loadClassDef(path, JParser.predicate_stop_on.classdef)
  if type(cd) ~= 'table' then
    return nil, err
  end
  local path_sep = fs.path_sep

  -- package + classname
  return (cd.pkg or ''):gsub('%.', path_sep) .. path_sep .. v2s(cd.name)
end

--

class.build(C)
return C

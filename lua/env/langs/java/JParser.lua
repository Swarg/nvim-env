-- 07-08-2024 @author Swarg
-- Goal:
-- source code parser with the ability to parse only the required part of code.
-- for example, the beginning of a class file or before the first non-empty
-- constructor. End of parsing can be configured via predicate function
--
-- TODO:
--  - parse method
--  - parse static block of code
--  - parse multiline comments
--

local log = require 'alogger'
local class = require 'oop.class'
local G = require 'env.langs.java.util.grammar'
local AParser = require 'olua.util.lang.AParser'
local syntax = require 'env.langs.java.util.syntax'
local lexer = require 'env.langs.java.util.lexer'


class.package 'env.langs.java.JParser'
---@class env.langs.java.JParser : olua.util.lang.AParser
---@field new fun(self, o:table?, iter:olua.util.Iterator?, offset:number?): env.langs.java.JParser
---@field setIterator fun(self, iter:olua.util.Iterator, offset:number?): env.langs.java.JParser
---@field setStoppedAt fun(self, line:string) : env.langs.java.JParser
---@field clazz table{pkg, imports, classtype, classname, class_def_lnum}
local C = class.new_class(AParser, 'JParser', {
})

local log_debug, log_trace = log.debug, log.trace

--
-- constructor
-- local parser = JParser:new(nil, Context:new())
--
---@param iter olua.util.Iterator?
---@param offset number?
function C:_init(iter, offset)
  log_debug("_init iter:%s offset:%s", iter, offset)
  self:super(iter, offset) -- call constructor of superclass

  self.clazz = {           -- current parsed clazz
    pkg = nil,
    pkg_ln = nil,
    imports = nil,
    import_lns = nil, -- line nums with imports
    fields = nil,
    constructors = nil,
    methods = nil,
    ln = nil,  -- line number with class definition (header) 'public class A {'
    lne = nil, -- end of the class '}'
  }
end

local E, v2s, fmt, match = {}, tostring, string.format, string.match

local T = G.tags
local isAbstract = syntax.Modifier.isAbstract

--
--------------------------------------------------------------------------------
--                             INTERFACE
--------------------------------------------------------------------------------

--
-- parsing the entire file or up to the point at which the predicate return true
-- override
---@param predicate function?
---@return self
function C:prepare(predicate)
  log_debug('prepare', C.predicate_stop_on_names[predicate] or type(predicate))
  return self:prepareMethods(predicate)
end

local FACTORY = {}

--- override
---@return table
function C:factory()
  return FACTORY
end

---@return table{pkg, imports, fields, constructors, methods, pkg_ln, import_lns}
function C:result()
  return self.clazz
end

---@return table
function C.lexer() return lexer end

-- ClassSignature - fileds, methods, constructors,

-- has package, imaport and parsed class defintion (without fields and methods)
function C:hasClassHeader()
  local clazz = self.clazz or E
  return clazz.name ~= nil and clazz.typ ~= nil and type(clazz.mods) == 'number'
end

-- from package up to the class definition (package, import, lnum of classdef)
--
-- package, imports, class_definition{typ, cname, lnum}
--
---@param predicate function?
function C:prepareClassHeader(predicate)
  if not self:hasClassHeader() then
    assert(self.iter, 'expected existing Iterator')
    self.lnum = 0 -- todo reset iterator if has state?
    self:parseClassHeader(predicate)
    local ln = v2s(self.lnum)

    -- validate
    if type((self.clazz or E).ln) ~= 'number' then  -- stop_lnum
      self:error('Cannot Parse the java class: ' .. -- public class ...
        'line with class definition not found ln:%s lnum:%s', ln, self.lnum)
    end
  end
  return self:hasClassHeader()
end

local function predicate_stop_on_package_defintion_line(self, tag)
  self = self
  -- log_debug("predicate_stop_on_package_defintion_line", tag, T.PACKAGE)
  return tag == T.PACKAGE
end

-- when you only need a package, import and class definition line
-- without fields and methods of the class
local function predicate_stop_on_class_defintion_header(self, tag)
  self = self
  return tag == T.CLASSDEF
end

---@param tag string
---@param self self {clazz, lnum, last_elm, curr_line, }
local function predicate_stop_on_first_constructor(self, tag)
  self = self
  return tag == T.CONSTRUCTOR
end

---@param tag string
---@param self self {clazz, lnum, last_elm, curr_line, }
local function predicate_stop_on_first_method(self, tag)
  self = self
  return tag == T.METHOD
end

local function predicate_stop_never(_, _)
  return false
end

--
-- to find class name in imports, and self class defintion name
-- parse only up to the beginning of the class defintion line
-- without parsing the whole class body.
--
---@param scn string
function FACTORY.newPredicateFindClassName(scn, out)
  ---@param tag string
  ---@param self self {clazz, lnum, last_elm, curr_line, }
  return function(self, tag)
    if tag == T.IMPORT and self.last_elm and self.last_elm[2] then
      local fcn = self.last_elm[2]
      -- extract short classname
      local scn0 = string.match(fcn, '^[%w%._]+%.([^%.]+)$')
      if scn0 == scn then
        out.found = fcn
        out.lnum = self.lnum -- self.last_elm[3] or self.lnum
        return true          -- stop
      elseif not out.found and scn0 == '*' then
        out.variants = out.variants or {}
        out.variants[#out.variants + 1] = fcn --- org.comp.util.*
      end
    elseif tag == T.CLASSDEF then
      if scn == self.clazz.name then
        local prefix = self.clazz.pkg ~= nil and v2s(self.clazz.pkg) .. '.' or ''
        out.found = prefix .. self.clazz.name -- same
      end
      return true                             -- stop parsing on class defintion
    end
    return false
  end
end

--
-- for example, in order to get the boundaries of the method inside which
-- the cursor is located
function C.predicate_stop_at_lnum(lnum)
  return function(parser, tag)
    return parser.lnum > lnum and (tag == T.METHOD or tag == T.FIELD)
  end
end

-- to parse to the end(EOF) of the source file
function FACTORY.newPredicateAlwaysFalse()
  return C.predicate_stop_on.never
end

C.predicate_stop_on = {
  package_def = predicate_stop_on_package_defintion_line,
  classdef = predicate_stop_on_class_defintion_header,
  fist_constructor = predicate_stop_on_first_constructor,
  fist_method = predicate_stop_on_first_method,
  never = predicate_stop_never,
}

C.predicate_stop_on_names = {
  [predicate_stop_on_package_defintion_line] = 'package_def',
  [predicate_stop_on_class_defintion_header] = 'classdef',
  [predicate_stop_on_first_constructor] = 'fist_constructor',
  [predicate_stop_on_first_method] = 'fist_method',
  [predicate_stop_never] = 'never',
}

--
-- all fields starting from the line with the class definition and
-- up to the first method or constructor
--
---@return self
---@param predicate function?
function C:prepareClassFields(predicate)
  if not self:prepareClassHeader(predicate) then return self end
  if predicate == C.predicate_stop_on.classdef then return self end -- stop

  if not self.clazz.fields then
    self:parseClassBody(predicate or predicate_stop_on_first_constructor)
  end
  return self
end

--
-- parse all constructors starting from the last previously discovered field
-- and up to the first method
--
---@return self
---@param predicate function?
function C:prepareConstructors(predicate)
  self:prepareClassFields(predicate)
  if not self.clazz.constructors then
    self:parseClassBody(predicate or predicate_stop_on_first_method)
  end
  return self
end

---@param predicate function?
function C:prepareMethods(predicate)
  predicate = (predicate ~= nil) and predicate or C.predicate_stop_on.never
  self:prepareConstructors(predicate)
  if not self.clazz.methods then
    self:parseClassBody(predicate) -- entire source
  end
  return self
end

---@param full boolean?
function C:getMainClassName(full)
  local cn = nil
  if self.clazz and self.clazz.name then
    cn = v2s(self.clazz.name)
    if full and self.clazz.pkg then
      cn = v2s(self.clazz.pkg) .. '.' .. cn
    end
  end
  log_trace("getMainClassName ret", cn)
  return cn
end

--
-- return the line number and field name of the latest defined filed in code
-- can be user to insert a new one
--
-- interact with already parsed data
--
---@return table?{lnum, name, t}
function C:getLastField()
  local t = (self.clazz or E).fields or E
  return t[#t]
end

---@return number
function C:getLnumToAddNewField()
  local e = self:getLastField()
  local lnum = nil
  if e then
    lnum = e.lne or e.ln or 0
  else
    lnum = self.clazz.ln -- todo case: "public class A \n extends B {"
  end
  log_trace("getLnumToAddNewField %s field:%s", lnum, e ~= nil)
  return (lnum or 0) + 1
end

--
-- interact with already parsed data
--
---@return table?
function C:getFirstNotEmptyConstructor()
  local t = ((self.clazz or E).constructors or E)
  if t then
    for _, e in ipairs(t) do
      if type(e) == 'table' and type(e.params) == 'table' and #e.params > 0 then
        return e
      end
    end
  end
  return nil -- not found
end

--------------------------------------------------------------------------------
--                    Offhand without full parsing
--                  quick approximate parsing offhand
--------------------------------------------------------------------------------

---@param line string
---@return table?
function C.splitLineToLexemes(line, raw)
  if not line or line == '' then
    return nil
  end
  -- "container.method()" -> "container", ".", "method", "()"
  if raw then
    return G.offhand.cpRawLexemsWithPos:match(line)
  end
  -- "container.method()" -> "container.method", "()"
  return G.offhand.cpLexemsWithPos:match(line)
end

---@return string?
function C.parseImportLineToFQCN(line)
  local fqcn = match(line, '^%s*import%s+static%s+(.-)%.[^%.]+%s*;%s*$') or
      match(line, '^%s*import%s+(.-)%s*;%s*$')
  return fqcn
end

--
-- todo add [jar]
--
---@return table?{classname, inner, method, lnum, jar}
function C.parseLineStacktrace(line)
  if C.isEmpty(line) or C.isCommented(line) then return nil end

  -- path = pkg + class [+ $subclass]
  local path, cname, n = string.match(line, ".*at (.+)%((.+)%.java:(%d+)%)")
  if not path or not cname then
    return nil
  end

  local res = { method = nil, classname = nil, inner = nil, lnum = nil }
  res.method = path:match("[^%.]+$")
  if cname then
    local s, e = string.find(path, cname, 1, true)
    if s and s > 0 then
      -- res.classfull = path:match("(.*[%.])")            -- Some$Inner
      res.classname = string.sub(path, 1, s - 1) .. cname -- SomeClass
      if e and e < #path and string.sub(path, e + 1, e + 1) == "$" then
        res.inner = string.sub(path, e + 1, #path):match("(.*)%.")
      end
    end
  end

  res.lnum = tonumber(n) or 0
  return res
end

-- for GotoDefintion from javac output
---@return string?
---@return number
function C.parseLineCompilerError(line)
  if not line or line == '' then return nil, 1 end
  local _, ep = string.find(line, '[ERROR] ', 1, true)
  local off = ep or 1

  local pattern = '^%s*(/[%w/%.%-_%$]+%.java):%[?(%d+)%]?'
  local path, n = string.match(line, pattern, off)

  if not path or path == '' then
    return nil, 1
  end
  return path, tonumber(n) or 1
end

--
-- parse output from Gradle test-task with failed test
--
-- SomeTest > test_something FAILED
--     org.junit.ComparisonFailure at SomeTest.java:35
--
function C.parseFaildedTestLine(line)
  local cause, fn, ln = match(line or '', '^%s*([^%s]+)%s*at%s*([^%s]+):(%d+)$')
  local t = nil
  if fn and tonumber(ln) then
    t = { cause = cause, fn = fn, lnum = tonumber(ln) }
  end
  log_debug('parseFaildedTestLine', t)
  return t
end

--------------------------------------------------------------------------------


local bracketsPair = {
  ['{'] = '}', ['('] = ')', ['['] = ']', ['<'] = '>',
}


--------------------------------------------------------------------------------
--                         Parser Parts
--------------------------------------------------------------------------------


-- override
---@return boolean
---@param line string?
function C.isCommented(line)
  return line ~= nil and (
    string.match(line, '^%s*//') ~= nil or         -- one line comment
    string.match(line, '^%s*/%*.-%*/%s*$') ~= nil) -- one line comment
end

-- override
---@param find string
---@return userdata
function C:getPatternFor(find)
  local patt
  if find == ')' then
    patt = G.pBalancedParentheses
  elseif find == '}' then
    patt = G.pBalancedCurlyBraces
  elseif find == '{' then
    patt = G.pUntilOpenCurlyBraces
  elseif find == ';' then
    patt = G.pFindOpSep
  elseif find == '"""' then
    patt = G.pFindEndOfMultilineString
  else
    log_trace('build parrent for stirng:|%s|', find)
    -- todo cache to map to reuse
    patt = G.build_patt_find_outside_string_and_comments(find)
  end
  return patt
end

local function add_package_def(self, token)
  assert((token or E)[1] == T.PACKAGE, 'tag package')
  self.clazz.pkg = assert(token[2], 'package-name')
  self.clazz.pkg_ln = self.lnum
  -- lnum of package? from self.lnum
  self.last_elm = { T.PACKAGE, self.clazz.pkg, self.lnum }
end

local function add_import_def(self, token)
  assert((token or E)[1] == T.IMPORT, 'tag import')
  local n = #self.clazz.imports + 1
  self.clazz.imports[n] = token[2]
  self.clazz.import_lns[n] = self.lnum

  self.last_elm = { T.IMPORT, token[2] or '', self.lnum }
end

--
--
---@param self self
---@return table
local function add_class_def(self, token)
  local clazz, lnum = (self or E).clazz, (self or E).lnum
  log_trace("add_class_def", lnum, token)

  syntax.token_to_class_entry(token, lnum, clazz)
  local continue_col = self:_pull_continue(clazz)

  log_trace("add_class_def continue_col:", continue_col, token)

  if continue_col then
    local line, lne = self:_next_line_up_to('{', continue_col)
    clazz.lne = lne
    -- todo parsing extends implements and so on
    if string.match(line or '', '^%s*{%s*$') then
      log_trace('CLASSDEF: found "{" on the next line', self.lnum)
    else
      error("something unexpected that could break parsing:|" .. v2s(line) .. "|")
    end
  end

  self.last_elm = { T.CLASSDEF, clazz }
  return clazz
end

--
--
local function add_annotation_def(self, token)
  local clazz, lnum = (self or E).clazz, (self or E).lnum
  log_trace("add_annotation_def lnum:%s token:%s", lnum, token)

  local annotation = syntax.token_to_annotation(token, lnum)
  syntax.add_free_annotation(clazz, annotation)
  local continue_col = self:_pull_continue(annotation)

  if not continue_col then
    annotation.params = token[3] -- {"value", "literal"}
  else
    local line, lne = self:_next_line_up_to(')', continue_col)
    annotation.lne = lne
    local tokens = G.p.cpAnnotationBody:match(line)
    if not tokens then
      error(fmt('cannot parse annotations from remainder_line:|%s|', v2s(line)))
    end
    syntax.tokens_to_annotation_params(tokens, annotation)
  end

  self.last_elm = { T.ANNOTATION, annotation }
  return annotation
end

local function is_start_of_str_multiline(self, continue_col)
  local n = continue_col
  return (self.curr_line or ''):sub(n - 3, n) == '"""'
end
--
--
---@param self self
---@param token table?
---@return table
local function add_field_def(self, token)
  local clazz, lnum = (self or E).clazz, (self or E).lnum
  log_trace("add_field_def", lnum, token)

  local field = syntax.token_to_class_field_entry(token, lnum, clazz)
  self.clazz.fields = self.clazz.fields or {}
  clazz.fields[#clazz.fields + 1] = field

  local continue_col = self:_pull_continue(field)
  if continue_col then
    local line, lne = '', nil
    -- findout a default value of the field
    if is_start_of_str_multiline(self, continue_col) then -- """ ... """
      line, lne = self:_next_line_up_to('"""')
    else
      line, lne = self:_next_line_up_to(';') -- continue_pos
    end
    assert(line, 'found the end of the class field assigment (defualt value)')
    field.value = (field.value or '') .. v2s(line or '')
    field.lne = lne
  end
  self.last_elm = { T.FIELD, field }
  return field
end

-- resolve_method_sign_with_throwns_in_sep_line_and_body_def
local function parse_method_throwns_and_body_def(self, continue_col, entry)
  local line, lne = self:_next_line_up_to("{", continue_col)
  assert(line, 'line with { of method body')
  local token0, continue = (G.p.pOptThrowsAndCp):match(line)
  if type(token0) == 'number' then
    continue = token0
    token0 = nil
  end
  if token0 then
    syntax.token_with_throws_to_method_entry(token0, entry)
  end
  self:_set_remainder(continue)
  line, lne = self:_next_line_up_to("}", continue)
  entry.lne = lne
  return true
end

-- for constructors and methods
--
---@param self self
---@param entry table constructor | method
---@param classtyp string?
local function resolve_method_signature_and_body_def(self, entry, classtyp)
  log_trace("resolve_method_signature_and_body_def")
  local continue_col = self:_pull_continue(entry)
  if not continue_col then
    -- one line definition
    error('todo without continue_col')
  end

  -- add params into constructor signature
  local first_char = self:_get_remainder_sub(1, 1) -- "{") and '}' or ')'

  -- case throwns or { in the new line
  if (first_char == nil or first_char == '') then
    local last_char = self:_get_curr_line_sub(-1, -1)
    if last_char == "\r" then
      last_char = self:_get_curr_line_sub(-2, -2)
    end
    if last_char == ')' then
      return parse_method_throwns_and_body_def(self, continue_col, entry)
    else
      error('last-char is:|' .. v2s(last_char) .. '|' .. v2s(string.byte(last_char)))
    end
  end

  -- abstract method without method body
  if first_char == ';' and (entry.mods and isAbstract(entry.mods)
        or classtyp == 'interface') then
    entry.lne = assert(entry.ln, 'lnum for abstract method')
    return -- no method body
  end

  local find_char = bracketsPair[first_char]
  if not find_char then
    error(fmt('unknown find_char of:|%s| rems-col:%s rems:|%s| lnum: %s ctyp:%s',
      v2s(first_char), v2s(self.remainder_line_col), v2s(self.remainder_line),
      v2s(entry.ln), v2s(classtyp)))
  end

  local line, lne = self:_next_line_up_to(find_char, continue_col) -- - 1)
  log_trace("next_line_up_to:|%s| give line:|%s| lne:%s",
    find_char, line, lne)

  if find_char == ')' or find_char == '{' then
    log_trace("parse method signature |%s|", line)

    local params_tokens, continue = G.p.pSignatureMethodParamsFull:match(line)
    if type(continue) ~= 'number' then
      error(fmt("no continue in line:|%s| lnum:%s", v2s(line), v2s(entry.ln)))
    end
    syntax.token_with_params_to_method_entry(params_tokens, entry)
    entry.params.lne = lne -- lnum with last param
    -- find end of the constructor body
    find_char = '}'
    line, lne = self:_next_line_up_to(find_char, continue) -- - 1)
  end

  if find_char == '}' then
    -- body of constructor is skipped
    entry.lne = lne
  end
end
--
--
--
---@param self self
---@param token table?
---@return table
local function add_constructor_def(self, token)
  local clazz, lnum = (self or E).clazz, (self or E).lnum
  log_trace("add_constructor_def", lnum, token)

  local constructor = syntax.token_to_constructor_entry(token, lnum, clazz)
  clazz.constructors = clazz.constructors or {}
  self.clazz.constructors[#self.clazz.constructors + 1] = constructor

  resolve_method_signature_and_body_def(self, constructor)

  self.last_elm = { T.CONSTRUCTOR, constructor }
  return constructor
end



--
--
--
---@param self self
---@param token table?
local function add_method_def(self, token)
  local clazz, lnum = (self or E).clazz, (self or E).lnum
  log_trace("add_method_def", lnum, token)
  local method = syntax.token_to_method_entry(token, lnum, clazz)
  clazz.methods = clazz.methods or {}
  self.clazz.methods[#self.clazz.methods + 1] = method

  resolve_method_signature_and_body_def(self, method, self.clazz.typ)

  self.last_elm = { T.METHOD, method }
  return method
end

local function add_static_code_def(self, token)
  error('Not implemented yet ' .. v2s(self) .. v2s(token))
end

---@param self self
local function add_multi_line_comment(self, token)
  if token[1] == T.COMMENT_OPEN then
    local col = token[2] + 1
    local line, lne = self:_next_line_up_to('*/', col)
    log_trace('found comment up to lnum:%s |%s|', lne, line)
    -- TODO attach javadoc to method and fields?
  else
    error('add_multi_line_comment: Not implemented yet for T.COMMENT_ML'
      .. v2s(self) .. require "inspect" (token))
  end
end

-- todo static block of code...

--


local has_next_token = C.protected.has_next_token
local next_token = C.protected.next_token

--
-- parseClassHead up to the class(enum|interface|etc) definition
-- collects: package, import, classdef + annotations for class
--
---@param stop_predicate function?
---@return self
function C:parseClassHeader(stop_predicate)
  log_trace("parseClassHeader ln:%s stop:%s", self.lnum, type(stop_predicate))
  self.clazz = self.clazz or {}

  local clazz = self.clazz
  clazz.imports = clazz.imports or {}
  clazz.import_lns = clazz.import_lns or {}
  -- if self:isStopped() then return self end

  while (self:_has_next()) do
    local line = self:_next_line()
    if line == nil then return self end -- ? EOF unexpected end of file
    local tokens = G.fpClassHeader:match(line)
    log_trace("parseClassHeader tokens:%s line:%s", tokens, line)

    while (has_next_token(tokens)) do
      local tag, token = next_token(tokens)

      if tag == T.PACKAGE then
        add_package_def(self, token)
      elseif tag == T.IMPORT then
        add_import_def(self, token)
      elseif tag == T.ANNOTATION then
        add_annotation_def(self, token)
      elseif tag == T.CLASSDEF then
        add_class_def(self, token)
        if stop_predicate and stop_predicate(self, tag) == true then
          self:setStoppedAt(line)
        end
        return self
      elseif tag == T.CONSTRUCTOR or tag == T.METHOD or tag == T.STATIC_BLOCK then
        error('expected parting only to class defintion, got:' .. v2s(tag))
      end

      if stop_predicate and stop_predicate(self, tag) == true then
        return self:setStoppedAt(line)
      end
    end
  end

  return self
end

-- mod [final] Type varname;
--
-- stop parsing when the first signature of a constructor or method appears
--
---@param stop_predicate function?
function C:parseClassBody(stop_predicate)
  log_debug("parseClassBody", self.lnum)
  if self:isStopped() then return self end

  while (self:_has_next()) do
    local line = self:_next_line()
    if line == nil then return true end -- EOF

    local tokens = G.fpClassBody:match(line)
    log_trace("parseClassBody tokens:%s line:%s", tokens, line)

    while (has_next_token(tokens)) do
      local tag, token = next_token(tokens)

      if tag == T.ANNOTATION then
        add_annotation_def(self, token)
      elseif tag == T.FIELD then
        add_field_def(self, token)
      elseif tag == T.CONSTRUCTOR then
        add_constructor_def(self, token)
      elseif tag == T.METHOD then
        add_method_def(self, token)
      elseif tag == T.STATIC_BLOCK then
        add_static_code_def(self, token)
      elseif tag == T.COMMENT_ML then
        add_multi_line_comment(self, token)
      elseif tag == T.COMMENT_OPEN then
        add_multi_line_comment(self, token)
      elseif tag == '}' then
        return self:setStoppedAt(line) -- end of class
      end

      if stop_predicate and stop_predicate(self, tag) == true then
        return self:setStoppedAt(line)
      end
    end
  end

  self.clazz.lne = self.lnum
  -- mark that the end of the file has been reached (_next_line == nil)
  return self
end

--
-- the result of the parsing  --  the parsed class structure
--
---@return table
function C:getResult()
  return self.clazz
end

----

--
-- from already parsed result(clazz)
--
---@param lnum number
---@return table? {annotations, mods, typ, name, params, ln, lne}
---@return number
function C.findParsedMethodByLnum(lnum, clazz)
  local methods = (clazz or E).methods or E
  for i, method in ipairs(methods) do
    if method.ln <= lnum and method.lne >= lnum then
      return method, i
    end
  end
  return nil, -1
end

-- testing
C.G = G -- grammar

class.build(C)
return C

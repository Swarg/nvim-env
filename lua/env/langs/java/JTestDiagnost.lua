-- 14-01-2025 @author Swarg
--
-- junit test reports handler
--

local log = require 'alogger'
local class = require 'oop.class'

local fs = require 'env.files'
local du = require 'env.util.diagnostics'
local ujunit = require 'env.langs.java.util.junit'
local consts = require 'env.langs.java.util.consts'
local Editor = require 'env.ui.Editor'
local Context = require 'env.ui.Context'
local opentest4j = require 'env.langs.java.util.testing.opentest4j'
local refactor = require 'env.langs.java.util.refactor'

local TestDiagnost = require 'env.lang.TestDiagnost'

class.package 'env.langs.java'
---@class env.langs.java.JTestDiagnost : env.lang.TestDiagnost
---@field new fun(self, o:table?, lang:env.lang.Lang, namespace:string): env.langs.java.JTestDiagnost
---@field lang env.lang.Lang
---@field namespace_id number
local C = class.new_class(TestDiagnost, 'JTestDiagnost', {
})


---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local lfmt = log.format
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local file_exists, join_path = fs.file_exists, fs.join_path

local get_meaningful_stacktrace = ujunit.get_meaningful_stacktrace

local find_variable_assigment = refactor.find_variable_assigment
local substitute_new_value = refactor.substitute_new_value

-- for diagnostics: assume there is only one assert per line
-- used to quickfix via cmd EnvHUsePassedIn from the line with diagnostic
C.IN_ROW_COL_START = 1
C.IN_ROW_COL_END = 80

local get_build_state, get_reports_dir_and_failed_tests, mk_test_report_files_map
local parse_xml_report, build_diagnostics_from_report
local build_diagnostics_from_compile_fail
local mkDiaFailureFromXmlNode

--
-- process output and showDiagnostics
-- handle only sucess case, handling the pp.code ~= 0
-- the erros handling in the lua via proxy func
--
---@param output string
---@param pp table
-- [DEBUG] props passed via pp:
--   save_output=true - to save raw decoded json into tmp file
--   add_original_json=true - to add into nvim diagnostic the decoded json
---@param exit_ok boolean
---@param signal number
---@diagnostic disable-next-line: unused-local
function C:processSingleTestOutput(output, pp, exit_ok, signal)
  local bufnr, src_bufnr = (pp or E).bufnr, (pp or E).src_bufnr
  log_debug("processSingleTestOutput buf:%s src_buf:%s out:%s pp:%s exit:%s sig:%s",
    bufnr, src_bufnr, type(output), type(pp), exit_ok, signal)

  local build_state, failedtask = get_build_state(output)
  log_debug('build state: ', build_state, failedtask)

  if build_state ~= 'FAILED' then
    return false -- nothing to do
  end
  if failedtask ~= nil and string.find(failedtask, 'compile') ~= nil then
    -- compileTestJava
    local diagnostics = build_diagnostics_from_compile_fail(output, failedtask)
    self:showAllDiagnostics(diagnostics)
    -- syntax errors can show by jdtls -- ( is also nothing to do??)
    return true
  end

  local reports_dir, fails = get_reports_dir_and_failed_tests(output)
  if not reports_dir or not fails then
    log_debug('not found dir or fails', reports_dir, fails)
    return false
  end

  -- build/test-results/test/
  -- binary  TEST-org.common.SomeTest.xml
  local files = fs.list_of_files(reports_dir, { ext = 'xml' })
  if not files or #files == 0 then
    log_debug('not found reports xml files')
    return false
  end

  self:showAllDiagnostics(self:buildDiagnosticsFromRepotFiles(
    src_bufnr, reports_dir, files, fails))
end

--
-- build diagnostics from
--
---@param src_bufnr number
---@param reports_dir string
---@param files table
---@param fails table
function C:buildDiagnosticsFromRepotFiles(src_bufnr, reports_dir, files, fails)
  local diagnostics = {}
  -- to avoid duplication and re-parsing of a report that has already been parsed
  local passed_report_files = {}

  local files_map = mk_test_report_files_map(files)

  -- one failed test can be in one test class file or in different ones;
  -- usually, one report file is created for one test class and can contain data
  -- about several methods in this test;
  -- here fails is a set of specific individual failed tests.
  for _, failed_test in ipairs(fails) do
    local tname = failed_test.name
    local fn = files_map[tname or false]
    if passed_report_files[tname] then
    elseif not fn then
      print('[WARN] not found filename for testname', tname)
    else
      local report_path = join_path(reports_dir, fn)
      local report = parse_xml_report(report_path)
      build_diagnostics_from_report(src_bufnr, report, diagnostics, report_path)
      passed_report_files[tname] = true
      failed_test.path = report_path -- ?
    end
  end

  return diagnostics
end

--
--
---@param path string
---@return table?
function parse_xml_report(path)
  if not file_exists(path) then
    log_debug('not found report: ' .. v2s(path))
    return nil
  end
  local report = ujunit.parse_xml_test_report(path)
  log_debug("parse_xml_report", (report or E).tag)
  return report
end

--
-- buildDiagnosticsFromDecoded
-- per one test class scope
--
---@param report table?{tag, attrs, childs} - parsed xml tree
---@param dlist table? out with result
---@param report_path string?
---@return table? diagnostics
function build_diagnostics_from_report(bufnr, report, dlist, report_path)
  log_debug("add_report_entries_to_nvim_diagnostics", (report or E).tag)
  if not report or not report.childs then
    log_debug('no report data to convert into nvim diagnostics')
    return nil
  end

  dlist = dlist or {}

  local function _add(d)
    if d then
      local ls = dlist[bufnr] or {}
      ls[#ls + 1] = d
      dlist[bufnr] = ls
    end
  end

  -- properties, testcase, system-out system-err
  for _, node in ipairs(report.childs) do
    if node and node.tag == 'testcase' and node.childs then
      local testcase = node
      for _, node0 in ipairs(testcase.childs) do
        -- <failure
        --   message="java.lang.AssertionError: expected: &lt;1&gt; but was:&lt;0&gt;"
        --   type="java.lang.AssertionError">
        --     java.lang.AssertionError: expected:&lt;1&gt; but was:&lt;0&gt; ..
        if node0 and node0.tag == 'failure' and node0.attrs then
          local failure = node0
          _add(mkDiaFailureFromXmlNode(failure, testcase, report_path))
        end
      end
    end
  end

  return dlist
end

---@param severity number
---@param message string
---@param lnum number
---param original_json table|nil
local function mkDEntry(severity, message, lnum, fn) --, original_json)
  log_debug("mkDEntry lnum:%s msg:%s", lnum, type(message))
  assert(type(severity) == 'number', 'severity')
  assert(type(message) == 'string', 'message')
  assert(type(lnum) == 'number', 'lnum')

  local col, end_col = C.IN_ROW_COL_START, C.IN_ROW_COL_END
  local d = du.mkDiagnosticEntry(severity, message, lnum, col, end_col)
  -- if original_json then
  --   d.user_data = d.user_data or {}
  --   d.user_data.original_json = original_json
  -- end
  d.fn = fn
  return d
end

--
-- parse a diagnostic message and reformat to make more readable
--
-- (dia-msg with test failure)
--
---@param dia_msg string
local function normalize_failure_message(dia_msg)
  local exception, expected, actual = C.parseRawTestFailMessage(dia_msg)
  if exception and expected then
    return exception .. ":\n" ..
        "Expected: |" .. v2s(expected) .. "|\n" ..
        "Actual:   |" .. v2s(actual) .. "|"
  end
  return dia_msg
end

---@return string? exception
---@return string? excepted value
---@return string? actual value
function C.parseRawTestFailMessage(dia_msg)
  log_debug("parseRawTestFailMessage")
  local exception, expected, actual
  exception = match(dia_msg or '', '^([^:]+):')
  if not exception then
    return nil, nil, nil
  end

  local pkg = match(exception, '^(.-)%.([^%.]+)$')
  log_debug("Exception: %s package:%s", exception, pkg)
  if pkg == 'org.opentest4j' then --.AssertionFailedError' then
    expected, actual = opentest4j.parse_failed_test_message(dia_msg, exception)
    --
  elseif exception == 'java.lang.AssertionError' then
    -- (assertj) assertThat(list).hasSize(3);
    local patt = "Expected size: (%d+) but was: (%d+) in:"
    expected, actual = match(dia_msg or '', patt)

    if not expected and not actual then
      actual = match(dia_msg, '(No value at JSON path "[%w_%.%$]+")')
      if actual then
        expected = 'value-at-given-JSON-path'
      end
    end
    -- java.lang.AssertionError: No value at JSON path "$.id"
  end

  if not expected then
    -- generic
    -- local patt = '^([^:]+):%s*expected:%s*<(.-)>%s*but was:%s*<(.-)>'
    -- exception, expected, actual = match(dia_msg or '', patt)
    local patt = '^[^:]+:%s*expected:%s*<(.-)>%s*but was:%s*<(.-)>'
    expected, actual = match(dia_msg or '', patt)
  end

  if not expected then
    -- generic
    -- local patt = '^([^:]+):%s*expected:%s*<(.-)>%s*but was:%s*<(.-)>'
    -- exception, expected, actual = match(dia_msg or '', patt)
    local patt = '^[^:]+:%s*(.-)%s+==>%s+expected:%s*<(.-)>%s*but was:%s*<(.-)>'
    _, expected, actual = match(dia_msg or '', patt)
  end

  log_debug('ret exception:%s expected:%s actual:%s', exception, expected, actual)

  if not expected then
    print("cannot parse diagnostic message:\n" .. v2s(dia_msg))
  end
  return exception, expected, actual
end

---@return string exception
---@return string excepted value
---@return string actual value
function C.parseNormalisedTestFailMessage(s)
  local patt = "^(.-):\nExpected:%s+|(.-)|\nActual:%s+|(.-)|\n"
  local exception, expected, actual = match(s, patt)
  log_debug("update_line_from_passedin exp:%s bufnr:%s", exception)

  -- org.opentest4j.AssertionFailedError: expected: not <null>
  -- org.junit.ComparisonFailure: expected:<[]> but was:<[ABCDE]>
  return exception, expected, actual
end

--
---@param testcase_node table
---@param failure_node table
function mkDiaFailureFromXmlNode(failure_node, testcase_node, report_path)
  log_debug("mkDiaFailureFromXmlNode")
  local e = nil
  if failure_node then
    local testcase_attrs = testcase_node.attrs or E
    local trace, lnum, fn = get_meaningful_stacktrace(failure_node, testcase_node)
    local message = ujunit.normalized_xml_str((failure_node.attrs or E).message)
    message = normalize_failure_message(message)

    local msg = fmt("%s\n%s\n%s %s %s\n%s\n",
      message,
      table.concat(trace, "\n"),
      v2s(testcase_attrs.classname), v2s(testcase_attrs.name), v2s(lnum),
      v2s(report_path or '')
    )
    e = mkDEntry(du.severity.WARN, msg, lnum, fn) --, add_original_data and failure_node)
  end

  return e
end

---@param list table
function mk_test_report_files_map(list)
  local t = {}
  for _, filename in ipairs(list) do
    local test_class = match(filename, '%.([^%.]+)%.xml')
    if test_class then
      t[test_class] = filename
    else
      print('WARN!', test_class, filename)
    end
  end
  return t
end

--
--
--
---@return string? dir
---@return table? failed tests
function get_reports_dir_and_failed_tests(output)
  local dir = match(output,
    "There were failing tests. See the results at: file://(.-)\n")

  if not dir then
    dir = match(output, -- when html reports is enabled
      "There were failing tests. See the report at: file://(.-)\n")
  end
  if not dir or dir == '' then
    log_debug('cannot find results dir')
    return nil, nil
  end

  -- case1:
  -- > Task :test FAILED
  -- SomeTest > test_MessageFormat FAILED
  --     org.junit.ComparisonFailure at SomeTest.java:35
  -- 1 test completed, 1 failed
  -- FAILURE: Build failed with an exception.
  --
  -- case2:
  -- > Task :test
  -- DeveloperRestControllerV1Test > Test create developer functionality FAILED
  --     java.lang.AssertionError at DeveloperRestControllerV1Test.java:76
  --         Caused by: com.jayway.jsonpath.PathNotFoundException
  -- 1 test completed, 1 failed
  -- FAILURE: Build failed with an exception.
  -- local sub = match(output, 'Task :test FAILED(.-)FAILURE:')
  local sub = match(output, 'Task :test(.-)FAILURE:')
  if not sub then
    log_debug('cannot find failed test')
    return nil, nil
  end

  local t = {}
  local testClassName, testMethodName

  for line in string.gmatch(sub, '([^\n]+)') do
    if testClassName then
      local cause, test, lnum = match(line, '%s*(.-)%s+at%s+([%w_]+)%.java:(%d+)%s*$')
      if cause and test and lnum then
        t[#t + 1] = {
          name = testClassName,
          method = testMethodName,
          lnum = tonumber(lnum),
          cause = cause,
        }
        testClassName, testMethodName = nil, nil
      end
    else
      -- local test_cls, test_m = match(line, '^([%w_]+)%s+>%s+([%w_]+) FAILED%s*$')
      -- @DisplayName("Test name with spaces functionality")
      local test_cls, test_m = match(line, '^([%w_]+)%s+>%s+(.-)%s*FAILED%s*$')
      if test_cls then
        testClassName = test_cls
        testMethodName = test_m
      end
    end
  end

  return dir, t
end

---@param s string
---@return string [FAILED SUCCESSFUL]
---@return string? [test compileTestJava]
function get_build_state(s)
  if not s or #s == '' then
    return 'ERROR', nil
  end
  local st = match(s, "\nBUILD (%u+) in")
  if not st then
    st = match(s, "\nBUILD (%u+)")
  end
  local task = nil
  if st == 'FAILED' then
    task = match(s, 'Task [%w_]*:(%w+) FAILED')
  end
  return st, task
end

---@param path string
---@param lnum number if given and no buffer with specified file - open new buff
function C.getBufnrWithOpen(path, lnum)
  if not path or #path < 1 then
    return nil
  end

  local _, bufnr = Editor.findOpenedFile(path, true)
  if not bufnr then
    Editor:new():open(path, lnum)
    _, bufnr = Editor.findOpenedFile(path, true)
  end
  return bufnr
end

--
-- compileTestJava (compilation errors)
--
---@param output string
---@param failedtask string
---@return table diagnostics
function build_diagnostics_from_compile_fail(output, failedtask)
  log_debug("build_diagnostics_from_compile_fail taks:'%s'", failedtask)

  local patt = '\n> Task :' .. v2s(failedtask) .. ' [FAILED]*(.-)' .. '\n%d+ errors?'
  local sub = match(output, patt)
  if (sub == nil) then
    -- case: compile_java_fatal_error_in_dependency
    sub = match(output, '\n> Task : [%w%-_]+ FAILED%s*\n(.-)%s*$') or output
  end
  local diagnostics, c = {}, 0

  local function add_dia_entry(t)
    local msg = v2s(t.msg) .. "\n" .. table.concat(t.lines or {}, "\n") .. "\n"
    -- open new buffer if not opened yet
    local bufnr = C.getBufnrWithOpen(t.path, t.lnum)
    if bufnr then
      local ls = diagnostics[bufnr] or {}
      ls[#ls + 1] = mkDEntry(du.severity.WARN, msg, t.lnum, t.path)
      diagnostics[bufnr] = ls
      c = c + 1
    else
      log_debug('cannot open ' .. v2s(t.path))
    end
  end

  local t = nil

  for line in string.gmatch(sub, '([^\n]+)') do
    if line then
      if line:sub(1, 1) == '/' then
        if t then
          add_dia_entry(t)
          t = nil
        end
        local path, lnum, msg = match(line, '^([^:]+):(%d+):%s*(.-)%s*$')
        if path and lnum and msg then
          t = {
            path = path,
            lnum = tonumber(lnum),
            msg = msg,
          }
        end
      elseif t then
        t.lines = t.lines or {}
        t.lines[#t.lines + 1] = line
      end
    end
  end
  if t then
    add_dia_entry(t)
  end

  log_debug('add diagnostic entries ', c)
  return diagnostics
end

--------------------------------------------------------------------------------

--
-- filter the diagnostics entries by a given opts
--
---@param dlist table
---@param opts table?
---@return table
function C.filter(dlist, opts)
  local t = {}
  opts = opts or E
  local has_opts = type(opts) == 'table' and next(opts) ~= nil
  local source = opts.source or 'Java'

  local function filter_by_opts(e)
    -- todo
    return type(e) == 'table'
  end

  if type(dlist) == 'table' then
    for _, e in ipairs(dlist) do
      if e.source == source and e.message then
        if not has_opts or filter_by_opts(e) then
          t[#t + 1] = e
        end
      end
    end
  end
  return t
end

--
-- parse give the readable message from diagnostic(jdtls) to the object
--
---@param diagnostic_message string
---@return table?{errtype, method_name, method_params, class_name, bad_args}
---@return string? errmsg
function C.get_error_info(diagnostic_message)
  log_debug("get_error_info", diagnostic_message)
  if not diagnostic_message then
    return nil, 'no diagnostic message'
  end

  local p = '^The method ([%w%_]+)%(([^%)]+)%) in the type (%u[%w_%$]+)%s+(.-)$'
  local mn, mp, cls, rest = match(diagnostic_message, p)
  if not cls or not mn then
    return nil
  end

  local errtype, bad_args = nil, nil
  if rest and rest ~= '' then
    -- Java: The method assertEquals(int, int, String) in the type Assertions
    -- is not applicable for the arguments (String, int, int)
    bad_args = match(rest, 'is not applicable for the arguments %((.-)%)$')
    errtype = consts.ERROR.WRONG_METHOD_ARGS
  end

  local t = {
    errtype = errtype,
    method_name = mn,
    method_params = mp,
    class_name = cls,
    bad_args = bad_args,
  }
  return t
end

--------------------------------------------------------------------------------

--
-- enrich params to run single test file for specific test framework
--
-- add a callback that will start parsing the output of the test results
-- and displaying them in nvim diagnostics
--
---@param params env.lang.ExecParams
---@return env.lang.ExecParams
function C:enrichedArgsRunSingleTest(params)
  log_debug("enrichedArgsRunSingleTest", params)
  -- local path2test = (params.args or E)[1]

  return params.args or {}
end

---@return boolean
---@return string? errmsg
function C:usePassedIn()
  local dlist, bufnr, cur_pos = self:getBufDiagnostic()
  log_debug("usePassedIn dlist:%s bur:%s cur:%s", type(dlist), bufnr, cur_pos)

  if dlist and cur_pos then
    -- pick real bufnr from firs record
    bufnr = (dlist[1] or {}).bufnr or 0
    -- Note cur_pos starts from 1, buf in nimbuf lines from 0
    local ln = cur_pos[1]
    local col = cur_pos[2]
    local curr_da = du.filter_by_position(dlist, bufnr, ln, col)
    if curr_da then
      -- local ns = self.namespace_id
      -- todo pick one if many
      local message = curr_da[1].message
      local ok, err = self:update_line_from_passedin(bufnr, ln, col, message)
      if not ok then
        return false, err
      end

      du.remove_all(bufnr, dlist, { curr_da[1] })
    end
  end
  return false
end

---@param w Cmd4Lua
---@return boolean
---@return string?
function C:updateTestDiagnostic(w)
  -- todo based on builded(ant|mvn|gradle)
  -- local reports_dir = "build/test-results/test/"
  local test_result_dir = "build/test-results/" -- common is "test", but itest ..
  local dirs = fs.list_of_dirs(test_result_dir)
  local diagnostics = {}

  local bufnr = assert(w.vars.bufnr, 'w.vars.bufnr')
  local bufname = assert(w.vars.bufname, 'w.vars.bufname')
  local scn = fs.extract_filename(bufname)

  for _, dir in ipairs(dirs or E) do
    local reports_dir = join_path(test_result_dir, dir)
    self:loadDiagnosticFrom(reports_dir, bufnr, scn, diagnostics)
  end

  self:showAllDiagnostics(diagnostics)
  return true
end

--
-- for one source file in bufnr
--
---@param reports_dir string
---@param bufnr number
---@param scn string
---@param diagnostics table outputResult
function C:loadDiagnosticFrom(reports_dir, bufnr, scn, diagnostics)
  -- binary  TEST-org.common.SomeTest.xml
  local files = fs.list_of_files(reports_dir, { ext = 'xml' })
  log_debug("loadDiagnosticFrom %s files:%s", reports_dir, #(files or E))

  if not files or #files < 1 then
    return false, 'not found reports at ' .. reports_dir
  end

  for _, fn in ipairs(files) do
    local i, j = string.find(fn, scn, 1, true)
    if i and j and fn:sub(i - 1, i - 1) == '.' and fn:sub(j + 1, j + 1) == '.'
    then
      local report_path = join_path(reports_dir, fn)
      local report = parse_xml_report(report_path)
      build_diagnostics_from_report(bufnr, report, diagnostics, report_path)
    end
  end
  return diagnostics
end

---@param self self
---@param bufnr number
---@param lnum number
---@param col number
---@param message string
---@return boolean
---@return string? errmsg
function C:update_line_from_passedin(bufnr, lnum, col, message)
  local exception, expected, actual = C.parseNormalisedTestFailMessage(message)
  if not expected or not actual then
    return false, lfmt('cannot parse diagnostic bufnr:%s ln:%s message:%s ns:%s',
      bufnr, lnum, message)
  end

  if exception == 'org.opentest4j.AssertionFailedError' then
    local ctx = Context.of(bufnr, lnum, col, nil)
    local line = ctx:getCurrentLine()
    local update_result, err = opentest4j.use_passedin_AssertionFailedError(
      line, expected, actual)

    if type(update_result) == 'string' then
      if update_result ~= line then
        ctx:updateLine(update_result, ctx.cursor_row)
      end
      return true
    elseif type(update_result) == 'table' then
      return self:update_variable_assignment_value(ctx, update_result)
    else
      return false, err or 'error on apply passedin'
    end
  else
    error('unsupported exception:' .. v2s(exception))
  end
end

--
-- find a line in which in the code is assigned the value of the variable
-- and change this value to the given actual
--
-- 1. find line with variable definition with value assignment
-- 2. substitute actual value into line with assignment code
--
---@param update_plan table{varname, new_value, old_value, lnum}
---@return boolean?
---@return string? errmsg
function C:update_variable_assignment_value(ctx, update_plan)
  log_debug("update_variable_assignment_value", update_plan)
  local varname = assert(update_plan.varname, 'varname')
  local new_value = assert(update_plan.new_value, 'new_value')
  local old_value = assert(update_plan.old_value, 'old_value')

  local place, err = find_variable_assigment(ctx, varname, old_value)
  if not place then
    return nil, 'not found assignment for variable:'
        .. v2s(varname) .. '= ' .. v2s(old_value) .. ':' .. v2s(err)
  end
  return substitute_new_value(ctx, place, new_value)
end

--

if _G.TEST then
  C.test = {
    get_build_state = get_build_state,
    get_reports_dir_and_failed_tests = get_reports_dir_and_failed_tests,
    mk_test_report_files_map = mk_test_report_files_map,
    parse_xml_report = parse_xml_report,
    build_diagnostics_from_report = build_diagnostics_from_report,
    mkDiaFailureFromXmlNode = mkDiaFailureFromXmlNode,
    build_diagnostics_from_compile_fail = build_diagnostics_from_compile_fail,
  }
end

class.build(C)
return C

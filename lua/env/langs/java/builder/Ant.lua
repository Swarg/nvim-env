-- rewrited 18-07-2024 @author Swarg
--
-- https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html
-- Goal: run the tests from legacy project

local class = require 'oop.class'
local log = require 'alogger'
local fs = require 'env.files'

local base = require 'env.lang.utils.base'
local JavaBuilder = require 'env.langs.java.JBuilder'


class.package 'env.langs.java.builder'
---@class env.langs.java.builder.Ant: env.langs.java.JBuilder
local C = class.new_class(JavaBuilder, 'Ant', {
  -- public fields of this Class:
  name = 'ant',
  buildscript = 'build.xml',
  project_root = nil,
  settings = nil,
  src = '/SRC/',
  test = '/TEST/',
})


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace



-- public static method
-- check is the given directory is Root of Ant Project
-- if so return founded build-script
---@param path string
---@return string?  path to script file or nil
function C.isProjectRoot(path, userdata)
  userdata = userdata

  local fn, buildscript
  if path then
    fn = fs.join_path(path, "build.xml")
    if fs.file_exists(fn) then
      buildscript = fn
    end
  end

  log_debug("isProjectRoot check:%s ret buildscript:%s", fn or path, buildscript)
  return buildscript
end

--
-- check is given absolute path to the file is a ant build script(build.xml)
--
---@param path string
---@return boolean
function C.isBuildScript(path)
  return type(path) == 'string' and fs.basename(path) == 'build.xml'
end

-- simple parsing the file settings.gradle in the root of the project
-- parse include - a list of the subprojects names (dirs)
---@diagnostic disable-next-line: unused-local
function C:parseBuildScript(fn, xml, prev_lm)
  local t, errmsg = base.parse_xml_body(xml)
  if not t then
    error(errmsg)
  end
  --[[
      project = {
        _attr = { basedir = ".", default = "main", name = "app" },

        property = {
          { _attr = { name = "src.dir", value = "src" } },
          { _attr = { name = "build.dir", value = "build" } },
          { _attr = { name = "classes.dir", value = "${build.dir}/classes" } },
          { _attr = { name = "jar.dir", value = "${build.dir}/jar" } },
          { _attr = { name = "main-class", value = "pkg.MainClass" } }
        }
        targets = {...}
  ]]
  local settings = {
    project = {
      name = ((t.project or E)._attr or E).name,
      property = {}
    }
  }
  for _, e in ipairs((t.project or E).property or E) do
    local name = (e._attr or E).name
    if name then
      settings.project.property[name] = e._attr.value
    end
  end
  -- TODO targets

  return settings
end

---@return table? list of include (SubProjects Names)
function C:getSubProjects()
  return self:getSettings().include or nil
end

--
-- See Enhance the build file
-- https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html
-- build.xml
local buildscript_template = [==[
<project name="${PROJECT_NAME}" basedir="." default="main">

    <property name="src.dir"     value="src"/>

    <property name="build.dir"   value="build"/>
    <property name="classes.dir" value="${build.dir}/classes"/>
    <property name="jar.dir"     value="${build.dir}/jar"/>

    <property name="main-class"  value="${MAINCLASS}"/>


    <target name="clean">
        <delete dir="${build.dir}"/>
    </target>

    <target name="compile">
        <mkdir dir="${classes.dir}"/>
        <javac srcdir="${src.dir}" destdir="${classes.dir}"/>
    </target>

    <target name="jar" depends="compile">
        <mkdir dir="${jar.dir}"/>
        <jar destfile="${jar.dir}/${ant.project.name}.jar" basedir="${classes.dir}">
            <manifest>
                <attribute name="Main-Class" value="${main-class}"/>
            </manifest>
        </jar>
    </target>

    <target name="run" depends="jar">
        <java jar="${jar.dir}/${ant.project.name}.jar" fork="true"/>
    </target>

    <target name="clean-build" depends="clean,jar"/>

    <target name="main" depends="clean,run"/>

</project>
]==]

--- override
function C:getBuildScriptTemplate()
  return buildscript_template -- return templ_build_xml
end

--------------------------------------------------------------------------------

---@param args table
---@param opts table
---@return table args
local function setup_opts(args, opts)
  if args and opts then
    if opts.clean_task == true then
      table.insert(args, 1, "clean") -- cleanTest?
    end
  end
  return args
end

function C:argsBuildProject(opts)
  return setup_opts({ "jar" }, opts)
end

-- clean compile run
function C:argsRunProject(opts)
  return setup_opts({ "run" }, opts)
end

-- deprecated todo move to getArgsToRunSingleTestFile
function C:argsTestSingleClass(class_name, opts, method_name)
  if class_name then
    -- class must be pkg/comp/SomeClassTest.java
    local clazz = class_name:gsub('%.', '/') .. '.java'
    local args = {
      "test-single-method",
      "-Djavac.includes=" .. clazz,
      "-Dtest.includes=" .. clazz,
      "-Dtest.class=" .. clazz,
    }
    if method_name and method_name ~= '' then
      table.insert(args, "-Dtest.method=" .. method_name)
    end
    setup_opts(args, opts)
    return args
  end
  return {}
end

class.build(C)
return C

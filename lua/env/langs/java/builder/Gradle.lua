-- refactored 18-07-2024 @author Swarg
--

local log = require 'alogger'
local class = require 'oop.class'
local fs = require 'env.files'

local Builder = require 'env.lang.Builder'
local JavaBuilder = require 'env.langs.java.JBuilder'
local ugd = require 'env.langs.java.util.gradle.debugging'
local ubuildscript = require 'env.langs.java.util.gradle.buildscript'
local gradle_sources = require 'env.langs.java.util.gradle.gradle_sources'


class.package 'env.langs.java.builder'
---@class env.langs.java.builder.Gradle: env.langs.java.JBuilder
---@field new fun(self, o:table?, project_root:string?): env.langs.java.builder.Gradle
local C = class.new_class(JavaBuilder, 'Gradle', {
  -- public fields of Class GradleBuilder:
  name = 'gradle',
  buildscript = 'build.gradle',
  project_root = nil,
  settings = nil, -- { include, }
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local setup_opts = ugd.setup_opts

local gradle_project_root_markers = {
  'build.gradle', 'settings.gradle',
  'build.gradle.kts', 'settings.gradle.kts',
}

-- public static
-- check is the given directory is Root of Gradle Project
-- if so return founded build-scritp (groovy or kotlin)
---@param path string
---@return string?  path to script file or nil
function C.isProjectRoot(path)
  local buildscript
  if path then
    for _, name in pairs(gradle_project_root_markers) do
      local script_file = fs.join_path(path, name)
      if fs.file_exists(script_file) then
        buildscript = script_file
        break
      end
    end
  end
  log_debug("isProjectRoot %s ret buildscript:%s", path, buildscript)
  return buildscript
end

---@param path string
---@return boolean
function C.isBuildScript(path)
  if type(path) == 'string' then
    local fn = fs.basename(path)
    if fn and fn ~= "" then
      for _, buildscript in ipairs(gradle_project_root_markers) do
        if fn == buildscript then
          return true
        end
      end
    end
  end
  return false
end

-- simple parsing the file settings.gradle in the root of the project
-- parse include - a list of the subprojects names (dirs)
function C:parseSettingsGradle()
  log_debug("parseSettingsGradle", self.project_root)
  if not self.project_root then
    return
  end

  local file = fs.join_path(self.project_root, 'settings.gradle')
  local exists = fs.file_exists(file)
  if not exists then
    log_debug("not found settings.gradle")
    return
  end

  local include = {} -- names of subprojects
  local lines = fs.read_lines(file)
  for _, line in pairs(lines) do
    if string.match(line, '^include') then
      for item in line:gmatch("'([^']+)'") do
        table.insert(include, item)
      end
    end
  end
  self.settings = {
    include = include
    -- todo other settings
  }
end

-- to override behavior from parent JavaBuilder
-- to use default behaviour in the env.lang.Builder
---@return table?
function C:getSettings()
  log_debug("getSettings", type(self.settings))
  if not self.settings then
    self.settings = Builder.getSettings(self)
  end
  return self.settings
end

-- @Override
-- called from Builder.getSettings() -> Builder.loadBuildScript()
---@param fn string -- self:getBuildScriptPath()
---@param body string
---@param prev_lm number
---@return false|table
---@return string? errmsg
function C:parseBuildScript(fn, body, prev_lm)
  log_debug("parseBuildScript", fn)
  prev_lm = prev_lm
  local ext = fs.extract_extension(fn)
  local dsl = ext == 'kts' and 'kotlin' or 'groovy'

  return ubuildscript.parse(body, dsl)
end

---@return table? list of include (SubProjects Names)
function C:getSubProjects()
  log_debug("getSubProjects has-settings: %s", self.settings ~= nil)
  if not self.settings then
    self:parseSettingsGradle()
  end
  return (self.settings or E).include
end

function C:argsBuildProject(opts)
  return setup_opts({ "build" }, opts)
end

function C:argsRunProject(opts)
  return setup_opts({ "run" }, opts)
end

---@param tci env.lang.ClassInfo
---@param opts table?
---@return env.lang.ExecParams
function C:getArgsToRunSingleTestFile(tci, opts)
  log_debug("getArgsToRunSingleTestFile", tci)
  opts = opts or E
  local test = tci:getClassName() --class_name

  -- if defined method name then run test only for this method
  if opts.method_name and opts.method_name ~= '' then
    if opts.method_name:sub(1, 6) ~= 'assert' then -- ignore assertTrue.. etc
      test = v2s(test) .. '.' .. v2s(opts.method_name)
    end
  end
  local args = { "test", "--tests", test } -- -i
  -- optkey '-i' to run Gradle with INFO logging level.
  -- It'll show you the result of each test while they are running.
  -- Downside is that you will get far more output for other tasks also then
  setup_opts(args, opts)
  return self:newExecParams(assert(self.name, 'mvn'), args)
end

---@param tci env.lang.ClassInfo
---@return env.lang.ExecParams
function C:getArgsToDebugSingleTestFile(tci)
  return self:getArgsToRunSingleTestFile(tci, { test_debug = true })
end

-------------------------------------------------------------------------------
--                      Generate  build.gradle


---@diagnostic disable-next-line: unused-local
local default_test_framework = [[
    testImplementation 'junit:junit:4.13.2'
]]
-- to launch old junit4 tests with junit5 add this deps:
-- org.junit.vintage:junit-vintage-engine:5.11.4

-- settings.gradle
local templ_settings_gradle = [[
rootProject.name = '${ARTIFACT_ID}'
]]

-- gradle.properties
-- The artifact ID defaults to the project name configured in settings.gradle
local templ_gradle_properties = [[
# properties
group=${GROUP_ID}
version=${VERSION}
]]
-- https://docs.gradle.org/current/userguide/build_environment.html#sec:gradle_configuration_properties

-- https://docs.gradle.org/current/userguide/java_library_plugin.html
local templ_build_gradle = [[
plugins {
    id 'java'
    id 'application'
    id 'maven-publish'
    // id 'java-library'
    // id 'eclipse'
}

version = project.version
group = project.group

sourceCompatibility = ${JAVA_VERSION}
targetCompatibility = ${JAVA_VERSION}

// java {
//   toolchain {
//     languageVersion.set(JavaLanguageVersion.of(${JAVA_VERSION}))
//   }
// }

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    testImplementation 'org.assertj:assertj-core:3.27.3',
    testImplementation 'org.junit.jupiter:junit-jupiter-engine:5.11.4'
    testImplementation 'org.mockito:mockito-junit-jupiter:5.15.2'

    // implementation 'com.google.guava:guava:33.4.0-jre'
}

application {
    mainClass = '${MAINCLASS}'
}

tasks.withType(JavaCompile) {
    options.encoding = "UTF-8"
}

// Custom Task to create all-dependencies-in-one-jar
task fatJar(type: Jar) {
    manifest {
        attributes "Main-Class": application.mainClass
    }
    archiveBaseName = 'all-in-one-jar'
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    from {
        configurations.runtimeClasspath.collect {
            it.isDirectory() ? it : zipTree(it)
        }
    } {
        // fix java.lang.SecurityException:
        // Invalid signature file digest for Manifest main attributes
        exclude("META-INF/*.RSA", "META-INF/*.SF", "META-INF/*.DSA")
    }
    with jar
}

// gralde test -PnoHtmlReports
tasks.named('test') {
    if (project.hasProperty("noHtmlReports")) {
        reports.html.required = false
    }
    useJUnitPlatform()
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            artifact(jar) {
                builtBy build
            }
        }
    }

    // This block selects the repositories you want to publish to.
    repositories {
        // Add the repositories you want to publish to here.
    }
}
]]

-- testImplementation 'junit:junit:4.13.2'

-- Set compile options for Java compilation tasks:
-- tasks.withType(JavaCompile).configureEach {
--     options.compilerArgs += ['-Xdoclint:none', '-Xlint:none', '-nowarn']
-- }
--
--[[ TODO
testing {
    suites {
        test { // Configure the built-in test suite
            useJUnit('4.13.2') // Use JUnit4 test framework
        }
    }
}
--]]

-- reports/tests/test/classes/some.package.MainClassTest.html

-- gradle test --tests ru.swarg.utils.TokensTest --debug-jvm

-- > Task :utilities:test
-- Listening for transport dt_socket at address: 5005

-- Gradle stores logs here: ~/.gradle/daemon/<GrandleVersion>/<pid>.out.log

-- ERROR: transport error 202: bind failed: Address already in use

-- NOTES gradle 7.4.2 on cleanTest test
-- Execution failed for task ':cleanTest'.
-- > java.io.IOException: Unable to delete directory
-- '.../build/test-results/test/binary'
-- Seems like in this directory gradle-daemon keep tmp file with name:
-- .fuse_hidden000005c700000006
-- How to solve: gradle --stop (Or kill gradle-daemon)

-- settings.gradle buildSrc
-- https://docs.gradle.org/current/userguide/organizing_gradle_projects.html


--- override
function C:getBuildScriptTemplate()
  return {
    ['build.gradle'] = templ_build_gradle,
    ['settings.gradle'] = templ_settings_gradle,
    ['gradle.properties'] = templ_gradle_properties,
  }
end

function C:genNoHtmlReportDefinition()
  return {
    "tasks.withType(Test) {",
    "    useJUnitPlatform()",
    "    if (project.hasProperty(\"noHtmlReports\")) {",
    "        reports.html.required = false",
    "    }",
    "}"
  }
end

function C.findPathToSourceOfClass(path, qfcn)
  return gradle_sources.findPathToSourceOfClass(path, qfcn)
end

class.build(C)
return C -- class

-- 13-07-2024 @author Swarg
-- https://maven.apache.org/guides/introduction/introduction-to-the-pom.html

local class = require 'oop.class'

local log = require 'alogger'
local fs = require 'env.files'

local base = require 'env.lang.utils.base'
local utables = require 'env.util.tables'
local Builder = require 'env.lang.Builder'
local Editor = require 'env.ui.Editor'
local JavaBuilder = require 'env.langs.java.JBuilder'
local JC = require 'env.langs.java.util.consts'
local mvn_gen = require 'env.langs.java.util.maven.gen'
local parser = require 'env.langs.java.util.maven.parser'
local utomcat = require 'env.langs.java.util.tomcat'

local init_project = require 'env.langs.java.util.maven.init_project'
-- local library_cache = require("env.cache.library")


class.package 'env.langs.java.builder'
---@class env.langs.java.builder.Maven: env.langs.java.JBuilder
---@field new fun(self, o:table?, project_root:string?, src:string?, test:string?): env.langs.java.builder.Maven
---@field project_root    string
local C = class.new_class(JavaBuilder, 'Maven', {
  name = 'mvn', -- executable command of this build system
  buildscript = 'pom.xml',
  -- project_props = nil,
  -- modules_map = nil,
  -- settings { project - from pom.xml, compile_opts}
})


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace

---@param keep_in_messages boolean?
local function notify(msg, keep_in_messages)
  ---@diagnostic disable-next-line: param-type-mismatch
  Editor.echoInStatus(nil, msg, nil, keep_in_messages)
end

--
-- public static
-- check is the given directory is Root of Source Code with .rockspec
-- userdata used to pass infos need to build instance of C
--
---@param path string
---@param userdata table|nil output data to build instance
---@return string?  full path to script file or nil
function C.isProjectRoot(path, userdata)
  userdata = userdata

  local fn, buildscript
  if path then
    fn = fs.join_path(path, "pom.xml")
    if fs.file_exists(fn) then
      buildscript = fn
    end
  end
  log_debug("isProjectRoot %s ret buildscript:%s", path, buildscript)
  return buildscript
end

--
-- check is given absolute path to the file is a maven build script(pom.xml)
--
---@param path string
---@return boolean
function C.isBuildScript(path)
  return type(path) == 'string' and fs.basename(path) == 'pom.xml'
end

-- to override behavior from parent JavaBuilder
-- to use default behaviour in the env.lang.Builder
---@return table?
function C:getSettings()
  log_debug("getSettings", type(self.settings))
  if not self.settings then
    self.settings = Builder.getSettings(self)
  end
  return self.settings
end

-- @Override
-- called from Builder.getSettings() -> Builder.loadBuildScript()
---@param fn string -- self:getBuildScriptPath()
---@param xml string
---@param prev_lm number
---@return false|table
---@return string? errmsg
---@diagnostic disable-next-line: unused-local
function C:parseBuildScript(fn, xml, prev_lm)
  log_debug("parseBuildScript", fn)
  local t, errmsg = base.parse_xml_body(xml)
  if not t then
    error(errmsg)
  end

  -- hide std attrs
  -- _attr = {
  --   xmlns = "http://maven.apache.org/POM/4.0.0",
  --   ["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance",
  --   ["xsi:schemaLocation"] = "http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
  -- },
  if (t.project or E)._attr then
    t.project._attr = nil
  end
  if (t.project.modelVersion) then
    t.project.modelVersion = nil
  end
  return t
end

---@return string
---@param pp table - project properties{GROUP_ID, ARTIFACR_ID, MAINCLASS, ..}
function C:getBuildScriptTemplate(pp)
  log_debug("getBuildScriptTemplate", pp)
  if not base.is_empty_str(pp.MAINCLASS) then
    pp.PLUGINS = mvn_gen.TEMPL_PLUGIN_JAR_WITH_MAINCLASS
  else
    pp.PLUGINS = ''
  end
  if pp.TEST_FRAMEWORK and pp.TEST_FRAMEWORK ~= '' then
    -- convert short group:artifact:version into dependency block
    local gav = parser.parse_artifact_coords(pp.TEST_FRAMEWORK)
    pp.DEPENDENCY_TEST_FRAMEWORK = mvn_gen.generate_dependency(gav, 'test')
    pp.TEST_FRAMEWORK = nil -- clear
  end
  return mvn_gen.TEMPL_POM_XML
end

--
-- add new dependencies into pom.xml
---@param deps table{groupId, artifactId, version,...}
function C:addDependencies(deps)
  local fn = self:getBuildScriptPath()
  if not fs.file_exists(fn) then
    return false, 'not found: ' .. v2s(fn)
  end

  local cfn = Editor.getCurrentFile()
  if cfn ~= fn then
    ---@diagnostic disable-next-line: param-type-mismatch
    Editor.open(nil, fn, '\\<\\/dependencies\\>')
  end
  local s = ''
  -- sort
  local deps_list = utables.map_to_list(deps)
  table.sort(deps_list, function(a, b)
    -- a: {k, v}
    return a[1] < b[1]
  end)

  for _, kvpair in ipairs(deps_list) do
    local dep_gav = kvpair[2]
    s = s .. "\n" .. mvn_gen.generate_dependency(dep_gav)
  end
  Editor:new():insertBeforeCurrentLine(s)
end

--------------------------------------------------------------------------------

---@param args table
---@param opts table?
---@return table args
local function setup_opts(args, opts)
  if type(args) == 'table' and type(opts) == 'table' then
    if opts.clean_task == true or opts.clean == true then
      table.insert(args, 1, "clean") -- cleanTest?
    end

    -- with this flag it will be possible to connect to port 5005 with a debugger
    if opts.test_debug == true then
      args[#args + 1] = '-Dmaven.surefire.debug'
    end
  end
  return args
end

---@param ci env.lang.ClassInfo
---@return env.lang.ExecParams
function C:getArgsToRunSingleSrcFile(ci)
  log_debug("getArgsToRunSingleSrcFile %s", ci)
  if not ci or not ci:isValidFile() then
    error('expected valid sci got: ' .. v2s(ci))
  end

  local classname = ci:getClassName()
  -- mvn exec:java -Dexec.mainClass="org.comp.App"
  local args = { "exec:java", '-Dexec.mainClass=' .. classname }

  return self:newExecParams(assert(self.name, 'mvn'), args)
end

---@param tci env.lang.ClassInfo
---@param opts table?
---@return env.lang.ExecParams
function C:getArgsToRunSingleTestFile(tci, opts)
  log_debug("getArgsToRunSingleTestFile", tci)
  opts = opts or E

  local test = tci:getClassName() -- class_name

  -- mvn test -Dtest="ClassUnitTest#test_method"
  if opts.method_name and opts.method_name ~= '' then
    test = test .. "#" .. v2s(opts.method_name)
  end

  local args = { "test", "-Dtest=" .. test } -- args
  setup_opts(args, opts)

  return self:newExecParams(assert(self.name, 'mvn'), args)
end

---@param tci env.lang.ClassInfo
---@return env.lang.ExecParams
function C:getArgsToDebugSingleTestFile(tci)
  return self:getArgsToRunSingleTestFile(tci, { test_debug = true })
end

-- $ mvn package &&
-- Building jar: full/path/to/app-0.1-jar
-- $ mvn exec:java -Dexec.mainClass="pkg.MainClass"
--                      ^^^ use it if you don't specify this in the properties

---@return env.lang.ExecParams
function C:argsBuildProject(opts)
  local args = setup_opts({ "package" }, opts)
  return self:newExecParams(assert(self.name, 'mvn'), args)
end

--[[ Details: Command to Run maven project:
1. POM.xml Must Contains ref to MainClass with main method
<properties>
    <exec.mainClass>fully-qualified-class-name</exec.mainClass>
</properties>

2. Run Command:
mvn clean compile exec:java
[-Dexec.args="xxx"]
--]]
-- mvn exec:java -Dexec.mainClass="com.example.Main" [-Dexec.args="argument1"]
-- If exec.mainClass not defined mvn lookup it in pom.xml at
-- proprties -> exec.mainClass
-- compile or package
---@return env.lang.ExecParams
function C:argsRunProject(opts)
  local args
  if self:isWebApp() then
    args = C:argsDeployWebApp(opts)
  end
  if not args then
    args = { "compile", "exec:java" }
  end
  setup_opts(args, opts)
  log_debug("argsRunProject args:%s opts:%s", args, opts)
  return self:newExecParams(assert(self.name, 'mvn'), args)
end

--
--
---@param opts table?
function C:argsDeployWebApp(opts)
  log_debug("argsDeployWebApp")
  opts = opts or {}

  local t = self:getTomcatMavenPlugin()
  if type(t) == 'table' then
    return { "package", "tomcat7:redeploy" }
  end

  log_debug('unkwnown deployment environment')
  return false
end

--------------------------------------------------------------------------------

function C:isWebApp()
  local t = self:getSettings()

  local project = ((t or E).project or E) -- [1]
  local packaging = (project or E).packaging
  local webapp = packaging == 'war'
  log_debug("isWebApp: %s Packaging:%s (%s)", webapp, packaging, type(project))
  local keys = {}
  for key, _ in pairs(project) do keys[#keys + 1] = key end
  log_debug("keys:", keys)

  return webapp
end

---@return table? {configuration={path:string, server:string, url:string}}
function C:getTomcatMavenPlugin()
  return self:getPlugin("org.apache.tomcat.maven", "tomcat7-maven-plugin")
end

--
-- find a ginen plugin settins(from parsed pom.xml) in xml-path:
--
--   project.build.pluginManagement.plugins
--
---@return table?
function C:getPlugin(groupId, artifactId)
  log_debug("getPlugin", groupId, artifactId)
  local project = ((self:getSettings() or E).project or E) -- [1]

  local t = (((((project or E).build or E).pluginManagement or E)
    .plugins or E).plugin or E)

  for _, e in ipairs(t or E) do
    if type(e) == 'table' then
      if e.artifactId == artifactId and e.groupId == groupId then
        return e
      end
    end
  end
  return nil
end

-- return absolute path in target/classes for given ClassInfo
---@param self self
---@param ci env.lang.ClassInfo
local function get_target_classfile(self, ci)
  local ip = fs.classname_to_path(ci:getClassName())
  return fs.join_path(self.project_root, 'target/classes/', ip) .. '.class'
end

--
-- to trigger compile to compile:
--
-- Goal: trigger maven to compile sources to get full options to recompile
--       given source file directly via javac without mvn itself
--
---@param self self
---@param ci env.lang.ClassInfo
local function trigger_compile_and_fetch_opts(self, ci)
  local path = get_target_classfile(self, ci)
  if fs.file_exists(path) then
    log_debug('remove compiled classfile: ' .. path)
    os.remove(path)
  else
    error('not found ' .. path)
  end
  notify('mvn -X compile')

  local lines = fs.execrl("cd " .. self.project_root .. " && mvn -X compile")
  if not type(lines) == 'table' then
    error('cannot run mvn -X compile')
  end

  local t = parser.parse_compile_output(lines)
  if #((t or E).src_roots or E) == 0 or not (t or E).javac_opts then
    notify('no compilation options found')
    return nil
  else
    notify('got compilation options!')
  end

  return t
end

--
-- compile given source file(java) based on given maven compilation options
-- if there is no compilation option, run maven and get them from it
--
---@param ci env.lang.ClassInfo
---@diagnostic disable-next-line: unused-local
function C:compile(ci)
  assert(ci:isValidFile(), 'expected valid ci')
  local settings = assert(self:getSettings(), 'has settings')

  settings.compile_opts = settings.compile_opts or
      trigger_compile_and_fetch_opts(self, ci)

  -- compile source via javac
  if settings.compile_opts then
    notify('compile... ' .. v2s(ci:getClassName()), true)

    local ip = ci:getInnerPath()
    local lines = fs.execrl(
      fmt("cd %s && javac %s %s.java",
        self.project_root, settings.compile_opts.javac_opts, ip))

    local path = get_target_classfile(self, ci)
    local compiled = fs.file_exists(path)
    notify('compiled: ' .. v2s(compiled) .. ' ' .. v2s(path))
    if not compiled then
      log.info("javac output:", lines)
    end
    return compiled
  end

  return false
end

-- from global maven config
---@param id string the name of the server(id)
local function get_creds_by_server_id(id)
  if type(id) == 'string' and id ~= '' then
    return parser.get_server_props(JC.GLOBAL_MAVEN_CONF_SETTINGS, id)
  end
  return nil
end

--
-- See: nvim-jdtls hotcodereplace
--
---@param ci env.lang.ClassInfo
---@diagnostic disable-next-line: unused-local
function C:hotswap(ci)
  assert(ci:isValidFile(JC.CT_WEBTEMPLATE), 'expected valid ci')

  if self:isWebApp() then
    local t = self:getTomcatMavenPlugin() -- tomcat plugin conf in the pom.xml
    if utomcat.is_valid_conf_entry(t, get_creds_by_server_id) then ---@cast t table
      if ci.type == JC.CT_WEBTEMPLATE then
        utomcat.redeploy_view(t, ci:getInnerPath(), ci:getPath(), { reload = false })
      else
        utomcat.redeploy_class(t, ci:getClassName(), get_target_classfile(self, ci))
      end
    end
  end
end

-- aka dependencies or libraries
--
---@return table (readonly)
function C:getModulesMap()
  error('Not implemented yet getModulesMap')
end

-- -- find innner path by module name iek
-- -- find module( for cn - classname
-- --
-- ---@param cn string - classname
-- ---@return string? inner_path without extension
-- function C:findInnerPathForModule(cn)
--   log_debug("findInnerPathForModule", cn)
--   -- TODO: custom (test) framework mappings based on buildscript config
--   return nil
-- end

--------------------------------------------------------------------------------
--                       bridge with mvn
--------------------------------------------------------------------------------

---@param name string?
function C.getArchetypeDefaultProps(name)
  return init_project.get_archetype_default_props(name)
end

-- used to generate new project
---@param pp table
function C.getArgsArchetypeGenerate(pp)
  return init_project.gen_args_archetype_generate(pp)
end

function C.findGeneratedProjectDir(output)
  return init_project.find_generated_proj_dir(output)
end

class.build(C)
return C

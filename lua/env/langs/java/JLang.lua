-- 13-07-2024 @author Swarg
--
local class = require 'oop.class'

local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'

local Lang = require 'env.lang.Lang'
local ClassInfo = require 'env.lang.ClassInfo'
local JavaGen = require 'env.langs.java.JGen'
local ClassResolver = require 'env.lang.ClassResolver'
local JBuilder = require 'env.langs.java.JBuilder'
local AntBuilder = require 'env.langs.java.builder.Ant'
local MavenBuilder = require 'env.langs.java.builder.Maven'
local GradleBuilder = require 'env.langs.java.builder.Gradle'
local JParser = require 'env.langs.java.JParser'
local JSourcer = require 'env.langs.java.JSourcer'
local JC = require 'env.langs.java.util.consts'
local api_constants = require 'env.lang.api.constants'
local udebugging = require 'env.langs.java.util.debugging'
local uframeworks = require 'env.langs.java.util.frameworks'
local JTestDiagnost = require 'env.langs.java.JTestDiagnost'

local BUILDER_CLASSES = { MavenBuilder, AntBuilder, GradleBuilder, JBuilder }


class.package 'env.langs.java'
---@class env.langs.java.JLang : env.lang.Lang
---@field new fun(self, o:table?, project_root:string?, builder:env.langs.java.JBuilder?): env.langs.java.JLang
---@field getLangGen fun(self): env.langs.java.JGen
---@field executable      string   bin of the lang interprater
---@field namespace_map   table    created by MixBuilder TODO
---@field modules_test_namespaces table - created after findTestFramework() TODO
---@field src string
---@field test string
---@field builder env.langs.java.JBuilder
---@field sourcer env.langs.java.JSourcer?
---@field last_lsp_update number   time of a setup settings (os.time())
--                                 used to update already exists settings only
--                                 when rockspec file was changed
--
local C = class.new_class(Lang, 'JLang', {
  ext = 'java',
  executable = 'java', -- java SingleClassApp.java
  root_markers = {
    '.git', 'pom.xml', 'build.gradle', 'build.gradle.kts', 'build.xml',
    'Makefile'
  },
  -- Note: there must be a slash at the end of the path
  src = JC.SRC_MAIN_JAVA,
  test = JC.SRC_TEST_JAVA,
  test_suffix = 'Test',
})

Lang.register(C)

local E, v2s, fmt = {}, tostring, string.format
local log_debug, log_trace = log.debug, log.trace
local instanceof = class.instanceof
local isWebAppResource = JBuilder.isWebAppResource
local LT = api_constants.LEXEME_TYPE
local file_exists = fs.file_exists

--------------------------------------------------------------------------------
--                                 INIT
--------------------------------------------------------------------------------

-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework
---@param fn string filename
---@return boolean
function C.isLang(fn)
  return type(fn) == 'string' and fn:match('%.java$') ~= nil
end

---@override
---@param project_root string
---@param opts table?{project_root, flat, builderClass}
---@return table properties to synck
function C:createBuilder(project_root, opts)
  log_debug("createBuilder", opts)
  opts = opts or {}
  local builder

  project_root = project_root or opts.project_root or self.project_root

  -- flat project
  if opts.flat or (opts.src == './' and opts.test == './') then
    builder = JBuilder:new(nil, project_root, './', './')
    -- builder.buildscript = '' -- no buildscript

    if opts.RAW_SNIPPETS then
      builder:setRawSnippets()
    end
    --
    return builder
  end

  -- Regular defualt Project Buidler(Maven|Gradle|Ant|etc)
  return MavenBuilder:new(nil, project_root)
  -- used only in syncSrcAndTestPathsWith to sync **2 in Lang
end

--
-- create project build script files and another config files (.gitignore)
-- see also JGen:createProjectFiles - sources of Java lang
--
---@param pp table - project properties
function C:createProjectConfigs(pp)
  if type(pp) ~= 'table' then return false, 'no properties' end
  -- another project files such as .gitirnore README.md and so on
end

---@return env.lang.Builder?
function C.getBuilderClassForBuildScript(path)
  for _, builder_klass in ipairs(BUILDER_CLASSES) do
    if builder_klass.isBuildScript(path) then
      return builder_klass
    end
  end
  return nil;
end

--
-- used at Lang:resolve
---@param marker string?
function C:findProjectBuilder(marker)
  log_debug("findProjectBuilder %s marker:%s", (self or E).project_root, marker)
  self.builder = nil

  local project_root = assert(self.project_root, 'project_root')

  for _, builder_class in ipairs(BUILDER_CLASSES) do
    local userdata = {} -- to pass src, test, project_root
    if builder_class.isProjectRoot(project_root, userdata) then
      self.builder = builder_class:of(project_root, userdata)
      if self.builder ~= nil then
        log_debug('found Builder for given project_root:', project_root)
        return self:syncSrcAndTestPathsWithBuilder() -- sync Lang and Builder
      end
    end
  end

  log_debug('Not Found any known BuildSystem in %s', self.project_root)
  return self
end

-- function Lang:doSyncProjectSettings(classinfo) end
-- used at Lang:resolve
function C:findTestFramework()
  log_debug("findTestFramework")
  self.testframework = nil
  if self.builder then
    local getSettings = self.builder.getSettings or JBuilder.getSettings
    -- old way to avoid infin-loop and use function from JBuilder not Builder
    -- local settings = JBuilder.getSettings(self.builder)
    -- (smthg for FlatProject)
    local settings = getSettings(self.builder)
    self.testframework = (settings or E).testframework
    log_debug("findTestFramework:%s", self.testframework)
  end
  return self
end

--
-- source manager
---@return env.langs.java.JSourcer
function C:getSourcer()
  if self.sourcer == nil then
    self.sourcer = JSourcer:new(nil, self)
  end
  return self.sourcer
end

--------------------------------------------------------------------------------
--                        TEST FRAMEWORK
--------------------------------------------------------------------------------

---@param ns string
---@return string?, string?  -  testsuite_name, test_path
---@diagnostic disable-next-line: unused-local
function C:getTestSuiteName(ns)
  -- TODO
  -- local tfw = self:getTestFramework()
  -- if tfw and class.instanceof(tfw, Mix|Raw) then
  --   return tfw:findTestSuiteNameFor(ns)
  -- end
end

function C:getTestDiagnost()
  log_debug("getTestDiagnost has:%s", self.test_diagnost ~= nil)
  if not self.test_diagnost then
    self.test_diagnost = JTestDiagnost:new(nil, self, 'JAVA')
  end
  return self.test_diagnost
end

--
-- TODO Adopt from elixit to Java
-- usecase: many sources with diff iterations for one test file
--
-- MyClass_i1.java -> MyClass_i1Test.java
-- MyClass_i2.java -> MyClass_i1Test.java
--
---@param self self
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo
local function findExistedFileForRawSnippet(self, ci, ctype)
  local path = ci ~= nil and ci:getPath() or nil
  local is_test = path and ci:isTestClass()
  local has_pair = path and ci:getPair() ~= nil
  log_debug("findPairedFileForRawSnippet", path, is_test, ctype, has_pair)

  if path and is_test then
    local fn, itern = string.match(path, '^(.+)_i(%d+)Test.java$')
    log_trace('match give fn:%s itern:%s', fn, itern)
    itern = tonumber(itern)
    ctype = ctype -- todo

    while fn and itern and itern >= 0 do
      itern = itern - 1
      local suff = itern > 0 and ('_i' .. itern .. 'Test.java') or 'Test.java'

      local path0 = path:gsub('_i(%d+)Test.java$', suff)
      local exists = file_exists(path0)
      log_trace('file %s exists: %s', path0, exists)

      if exists then
        log_debug('File Found: %s', path0)
        local ci0 = ClassResolver.of(self):lookupByPath(path0)
        ci0 = assert(ci0, 'new ClassInfo should exists')
        ci0:bindPair(ci:getPair())
        return ci0
      end
    end
  end

  return ci
end

--
-- fired from ClassResolver when not found existed file for given paired CI
--
---@param ci env.lang.ClassInfo
---@param ftype number? ClassInfo.CT(CT_SOURCE, CT_TEST, CT_OPPOSITE)
---@return env.lang.ClassInfo
function C:onResolveCINoExistedPath(ci, ftype)
  log_trace("onResolveCINoExistedPath", ftype)

  if ci and JBuilder.isRawSnippets(self.builder) then
    return findExistedFileForRawSnippet(self, ci, ftype)
  end

  return ci
end

--------------------------------------------------------------------------------

--
--  lib/some_module_name -->  lib/SomeModuleName
--
-- inner_path is a path inside the project without project_root and extension
-- in modular project it src + modulename + namespace
-- in lua src dir does not maps to namespace
--
--- override
---@param ns string? -- the namespace or innerpath (without root and extendsion)
---@return string?
function C:getClassNameForInnerPath(ns)
  if ns then
    local cn, pref = nil, ''
    local src, test = self:getSrcPath(), self:getTestPath()
    local flatStruct = self.isFlatDirPart(ns, src, test)

    if flatStruct then
      -- self:isTestInnerPath(ns)
      cn = ns -- no prefix path
    else
      if su.starts_with(ns, src) then
        pref = self:getSrcPath()
      elseif su.starts_with(ns, test) then
        pref = self:getTestPath()
      end

      cn = ns:sub(#pref + 1)
    end

    cn = fs.path_to_classname(cn, '%.') -- class_name

    log_debug("getClassNameForInnerPath %s > cn:%s (src:%s test:%s flat:%s)",
      ns, cn, src, test, flatStruct)

    return cn
  end

  return ns
end

--
--  src/main/java/SomeClassName -> lib/some_module_name
--  test/SomeModuleTest -> test/some_module_test
--
--- override
---@param cn string -- classname
---@return string
function C:getInnerPathForClassName(cn)
  if cn then
    if su.ends_with(cn, self.test_suffix) then
      return fs.join_path(self.test, fs.classname_to_path(cn, '%.'))
      --
    elseif self.builder then
      local cn0 = self.builder:findInnerPathForModule(cn)
      if cn0 then
        return cn0
      end
    end

    -- relative(inner) path in the project
    return fs.join_path(self:getSrcPath(), fs.classname_to_path(cn, '%.'))
  end

  return cn
end

--- override
---@param filename string
---@return boolean
function C:isSrcPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getSrcPath())
  end
  return false
end

--- override
---@param filename string
---@return boolean|nil
function C:isTestPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    if self:hasTestFramework() then
      return self:getTestSuiteName(ns) ~= nil
    else
      return su.starts_with(ns, self:getTestPath())
    end
  end
end

-- check is a class are test class from some configured testsuite path
-- if so then pass testsuite path and name into classinfo
--
-- Lookup goes through InnerPath
--
-- findout test suites for given inner-path(namespace)
-- check is classinfo from some testsuite namespace (test-class)
-- then addd testsuite_name and testsuite_path into classinfo
---@param classinfo env.lang.ClassInfo
---@return boolean
function C:lookupTestSuite(classinfo)
  local ns = classinfo:getInnerPath()
  log_debug("lookupTestSuite inner:%s cn:%s", ns, classinfo.classname)
  if ns then
    local testsuite_name, testsuite_path

    if self:hasTestFramework() then
      log_trace("take testsuite from TestFramework")
      testsuite_name, testsuite_path = self:getTestSuiteName(ns)
      --
    elseif su.starts_with(ns, self.test) then
      log_trace("set default testsuite")
      testsuite_name, testsuite_path = 'unit', self.test -- ?
    end

    if testsuite_name and testsuite_path then
      classinfo:setTestSuite(testsuite_name, testsuite_path)
      return true
    end
  end

  log_trace("classinfo is not a test class")
  return false
end

--
-- a simple and straightforward classname mappings: one source to one test
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- source-class: app.module.MyClass
-- return      : app.module.MyClassTest
--
--- override
---@param src_classname string
function C:getTestClassForSrcClass(src_classname)
  local tcn = Lang.getWithSuffix(src_classname, self.test_suffix)
  log_debug("getTestClassForSrcClass scn:", src_classname, self.src, tcn)
  return tcn
end

--
-- a simple and straightforward classname mappings: one test to one source
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- test-class: app.module.init_spec       (class ie module)
-- return    : app.module.init
--
--- override
---@param test_classname string
function C:getSrcClassForTestClass(test_classname)
  local scn = Lang.getPathWithoutSuffix(test_classname, self.test_suffix)
  log_debug('getSrcClassForTestClass', test_classname, scn)
  return scn
end

--------------------------------------------------------------------------------
--                            USE CASES
--------------------------------------------------------------------------------
-- See env.lang.Lang.lua

---@param filename string
function C:onNewFile(filename)
  log_debug("onNewFile", filename)
  assert(type(filename) == 'string', 'filename')
  local ext = fs.extract_extension(filename)
  if ext == 'java' then
    local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo(false)
    ---@cast ci env.lang.ClassInfo
    local path, errmsg = self:getLangGen():createClassFile(ci, {})
    if not path and errmsg then
      self:getEditor():echoInStatus('error:' .. tostring(error), 'ErrorMsg', false)
    end
  end
end

-- triggers for files and directories
---@param oldname string
---@param newname string
function C:onFileRenamed(oldname, newname)
  log_debug("onFileRenamed", oldname, newname)
  -- TODO rename source classfile
end

---@param tci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function C:getArgsToRunSingleTestFile(tci, opts)
  log_debug("getArgsToRunSingleTestFile")
  self:ensurePathExists(tci)

  local builder = self:getProjectBuilder()
  if instanceof(builder, JBuilder) then ---@cast builder env.langs.java.JBuilder
    return builder:getArgsToRunSingleTestFile(tci, opts)
  end

  -- test framework bin
  local cmd, args = nil, nil
  local path = tci:getPath()                       -- :getInnerPath() .. '.' .. self.ext

  cmd = self:getTestFrameworkExecutable() or 'mvn' -- + test
  args = { 'test', path }

  return self:newExecParams(cmd, args)
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function C:getArgsToRunSingleSrcFile(ci)
  log_debug("getArgsToRunSingleSrcFile")
  self:ensurePathExists(ci)

  local builder = self:getProjectBuilder()
  if instanceof(builder, JBuilder) then ---@cast builder env.langs.java.JBuilder
    return builder:getArgsToRunSingleSrcFile(ci)
  end

  -- interpreter bin
  local bin = self.executable or 'java'
  local ext = self.ext

  -- to support multiple extensions ext2. asume path is to ensure existed file
  if ci:getPath() then ext = fs.extract_extension(ci:getPath()) or ext end

  local path = ci:getInnerPath() .. '.' .. ext
  local cmd, args = bin, { path }

  return self:newExecParams(cmd, args)
end

---@param tci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function C:getArgsToDebugSingleTestFile(tci)
  log_debug("getArgsToDebugSingleTestFile")
  self:ensurePathExists(tci)

  local builder = self:getProjectBuilder()
  if instanceof(builder, JBuilder) then ---@cast builder env.langs.java.JBuilder
    return builder:getArgsToDebugSingleTestFile(tci)
  end

  -- test framework bin
  local cmd, args = nil, nil
  local path = tci:getPath()                       -- :getInnerPath() .. '.' .. self.ext

  cmd = self:getTestFrameworkExecutable() or 'mvn' -- + test
  args = { 'test', path }

  return self:newExecParams(cmd, args)
end

---@return env.langs.java.JGen -- the instance of a LangGen class (ready to work)
function C:newLangGen()
  self.gen = JavaGen:new({ lang = self })
  return self.gen
end

---@return boolean|string
function C:isIgnoredByLsp()
  local eclipse = require 'env.bridges.eclipse'
  return eclipse.isIgnoredByLsp(self.project_root)
end

---@param filename string
function C:hotswap(filename)
  if not self.builder then
    log_debug('no builder')
    return false
  end
  local ci = self:resolveClass(filename, ClassInfo.CT.CURRENT):getClassInfo()
  if not ci then
    log_debug("No ClassInfo for: %s", filename)
    return false
  end
  -- compile file
  if isWebAppResource(ci) or self.builder:compile(ci) then
    self.builder:hotswap(ci)
  end
end

--
-- checking the case when the user makes the GotoDefinition from
-- the place of the definition itself
--
-- call contenx: after parsing the content of the current opened buffer(java-code)
--
---@param ctx env.ui.Context
---@param scn string
local function checkCursorAlreadyInClassDefinition(ctx, scn)
  log_debug("checkCursorAlreadyInClassDefinition scn:", scn)
  local parser = assert(ctx.parser, 'has parser')
  ---@cast parser env.langs.java.JParser
  local selection = ((ctx or E).selection or E) ---@cast selection env.ui.Selection

  -- check if current line is a line with class definition(alredy in place)
  local lnum, col = selection.lnum, selection.col
  local classdef_ln = (parser.clazz or E).ln

  log_debug('current lnum:%s col:%s classdef_ln:%s', lnum, col, classdef_ln)
  if not lnum or not col or classdef_ln ~= lnum then
    return --
  end

  local line = assert(selection.line, 'selection.line')

  local extens_pos = string.find(line or '', ' extends ') or math.huge
  local implements_pos = string.find(line or '', ' implements ') or math.huge

  -- public class SCN extends ...
  local at_def = col < extens_pos and col < implements_pos
  local found = nil
  if at_def then
    found = v2s(parser.clazz.pkg) .. '.' .. v2s(parser.clazz.name)
  end
  log_debug('GotoDefintion from Definition itself: %s fcn:%s', at_def, found)
  return found
end

--
-- find from current opened buffer full path and linenum(or word-to-jump) to
-- given short class name (From word under the cursor)
--
---@param ctx env.ui.Context
---@param scn string
---@param lt number
---@return string?
---@return number|string?
local function resolveDefinitionOfClass(self, ctx, scn, lt)
  assert(type(scn) == 'string' and scn ~= '', 'short classname')
  local res = ctx:parseAndFindFullClassName(scn, {})
  log_debug("resolved from %s %s to %s", lt, scn, res)

  if not res.found then
    res.found = checkCursorAlreadyInClassDefinition(ctx, scn)
  end

  local fcn = res.found
  if not fcn then
    if res.variants then
      error('Not implemented yet: to search in variants of import org.comp.*')
    end
    return nil, nil
  end

  local ip = self:getInnerPathForClassName(fcn)
  -- todo for non in project classes from external 3th party dependencies
  return self:getFullPathForInnerPath(ip), (scn or 1)
end

---@param self self
---@param ctx env.ui.Context
---@param mn string
---@param lt number
---@diagnostic disable-next-line: unused-local
local function resolveDefinitionOfMethod(self, ctx, mn, lt)
  error('Not implemented yet for method')
end

---@param self self
---@param ctx env.ui.Context
---@param vn string
---@param lt number
---@diagnostic disable-next-line: unused-local
local function resolveDefinitionOfVariable(self, ctx, vn, lt)
  error('Not implemented yet for variable')
end

--
-- checking that the specified inner-path(to class) exists in the open jar viewer
-- if such a class file exists in the jar archive,
-- it will return the internal path to it without src/main/java
--
-- use case: reverse engineering a jar file when not all code is decompiled
-- from the open jar file, but it is considered that if there is such a class
-- in the jar file, then use it as a source after decompilation.
--
---@param jar_viewer env.langs.java.ui.JarViewer
---@param inner_path string
---@return string?
local function resolveInnerPathInJarViewer(self, jar_viewer, inner_path)
  if not jar_viewer or not inner_path then
    return nil
  end

  local src_main_java = self:getSrcPath()
  if su.starts_with(inner_path, src_main_java) then
    inner_path = inner_path:sub(#src_main_java + 1, #inner_path)
  end
  inner_path = inner_path .. '.class'
  local has_file = jar_viewer:hasFile(inner_path)

  log_debug('resolveInnerPathInJarViewer ret: %s', inner_path, has_file)
  return has_file == true and inner_path or nil
end

--
--
-- for reverse engineering mode, when you have a jar file with "source" code
-- that needs to be decompiled before opened in nvim
--
---@param self self
---@param ctx env.ui.Context?
---@param path string?
---@param classname string?
local function resolvePathByJarViewer(self, ctx, path, classname)
  local JarViewer = require 'env.langs.java.ui.JarViewer'
  local jviewer = JarViewer.getFirstViewer()
  log_debug("resolvePathByJarViewer given: p:%s cn:%s jv:%s",
    path, classname, jviewer ~= nil)
  -- todo add support for multiple opened jar viewers (binding bufnr-java>jv)

  if jviewer then
    local inner_path = nil

    if path ~= nil and path ~= '' then
      local inner_path0 = self:getInnerPath(path) -- in project
      inner_path = resolveInnerPathInJarViewer(self, jviewer, inner_path0)
      -- ^ in jar file (without src/main/java/)
    else
      -- path to class not generated by JLang - check by package + short classname
      local parser = (ctx or E).parser ---@cast parser env.langs.java.JParser
      local pkg = ((parser or E).clazz or E).pkg
      log_debug('search class %s.%s in JarViewer', pkg, classname)
      -- just based on a guess when the type (class) is taken from under the cursor
      -- and the package is the same as in the current class.
      if pkg and classname then
        inner_path = jviewer:findClass(pkg, classname)
      end
    end

    if inner_path then
      path, _ = jviewer:doUnzipDecompileClassFile(inner_path, nil)
    end
  end

  log_debug("resolved to:%s", path)
  return path
end


--
-- from some source code
--
---@return boolean
function C:gotoDefinition(line)
  log_debug("gotoDefinition line:|%s|")
  if line and line ~= '' then
    local path, jumpto = uframeworks.resolveSourceFrom(line, self.project_root)
    if not path then
      print('The given line cannot be resolved')
      return false
    end
    return Lang.openExistedFileWithJumpTo(self:getEditor(), path, jumpto or 1)
  end

  local path, jumpto = self:resolveDefinition()

  if not path or path == '' then
    local lt, word = self:getEditor():getContext(false):getLexemeType()
    local lname = api_constants.LEXEME_NAMES[lt or false]
    print(fmt('Not resolved for %s[%s]: %s', v2s(lname), v2s(lt), v2s(word)))
    return false
  end

  return Lang.openExistedFileWithJumpTo(self:getEditor(), path, jumpto)
end

--
-- resolve abs path to defintion (class, method, variable,...)
--
---@return string?
---@return number|string?
---@return number? resolved lexeme type
function C:resolveDefinition()
  local ctx = self:getEditor():getContext(true, JParser:new())
  local lt, word = ctx:selectLexem():getLexemeType()

  local lname = api_constants.LEXEME_NAMES[lt or false]
  log_debug("resolved lt:%s:%s word:%s", lt, lname, word)
  if not word or not lt then
    return nil, -1, lt --not found word or lexeme type
  end

  local path, jumpto, is_class
  if lt == LT.CLASS or lt == LT.TYPE or lt == LT.ANNOTATION then
    path, jumpto = resolveDefinitionOfClass(self, ctx, word, lt)
    is_class = true
  elseif lt == LT.METHOD or lt == LT.FUNCTION then
    path, jumpto = resolveDefinitionOfMethod(self, ctx, word, lt)
  elseif lt == LT.IDENTIFIER or lt == LT.VARNAME then
    path, jumpto = resolveDefinitionOfVariable(self, ctx, word, lt)
  end


  log_debug("resolved by JLang to %s %s", path, jumpto)

  if is_class and (not path or not file_exists(path)) then ---@cast path string
    path, jumpto = resolvePathByJarViewer(self, ctx, path, word)
  end

  return path, jumpto, lt
end

--
-- from some source code
-- main goal: jump to source file from stacktrace with errors
-- for example, from errors in tests
--
---@param editor env.ui.Editor
---@return boolean
function C.gotoDefinitionFromNonSource(editor, line)
  line = line or editor:getContext():getCurrentLine()
  log_debug("gotoDefinitionFromNonSource |%s|", line)


  local path, jumpto
  if line and string.match(line, '^([^%s]+)$') and file_exists(line) then
    path, jumpto = line, 1
    log_debug("line is a path fo existd file", path, jumpto)
  end

  if not path then
    path, jumpto = JParser.parseLineCompilerError(line)
    log_debug("parseLineCompilerError", path, jumpto)
  end

  if not path then
    local t = JParser.parseLineStacktrace(line)
    log_debug("parseLineStacktrace", t)
    if t and t.classname then
      path = Lang.resolvePathByClassNameInAll(t.classname)
      jumpto = t.lnum
    end
  end

  if not path then -- line with a full qualified classname only
    local classname = string.match(line, "^%s*([%l%.]+%.%u[%w_]+)%s*$")
    if classname ~= nil and classname ~= '' then
      local bufname = editor:getContext(false):getCurrentBufname()
      path = C.findFullPathByFullClassName(bufname, classname)
      jumpto = 1
    end
  end

  if not path then
    path = C.findPathToSourceOfClass(editor, JParser.parseImportLineToFQCN(line))
  end


  if not path then
    local t = JParser.parseFaildedTestLine(line) -- output from `gradle test`
    if t and t.fn then                           -- short-classname + ext
      path = Lang.findOpenedFile(t.fn)
      jumpto = t.lnum
    end
  end

  if not path then
    local project_root = os.getenv('PWD') or '.' -- ??
    path, jumpto = uframeworks.resolveSourceFrom(line, project_root)
  end

  if not path then
    return false
  end

  return Lang.openExistedFileWithJumpTo(editor, path, jumpto)
end

---@param editor env.ui.Editor
---@param fqcn string?
---@return string?
function C.findPathToSourceOfClass(editor, fqcn)
  if not fqcn then
    return nil
  end
  log_debug("findPathToSourceOfClass", fqcn)
  -- gradle sources
  if su.starts_with(fqcn, 'org.gradle.') then
    local bufname = editor:getContext(false):getCurrentBufname()
    return GradleBuilder.findPathToSourceOfClass(bufname, fqcn)
  end
end

--
-- for jump to Definition from line with classname only
--
---@param bufname string
---@param classname string
---@return string?
function C.findFullPathByFullClassName(bufname, classname)
  log_debug("findFullPathByFullClassName %s", classname)
  local path = nil
  local lang = Lang.findOpenedProjectByPath(bufname)
  if lang then
    local ipath = lang:getInnerPathForClassName(classname)
    if ipath then
      path = lang:getFullPathForInnerPath(ipath)
    end
  end
  log_debug("found: %s (lang: %s)", path, lang ~= nil)
  return path
end

--------------------------------------------------------------------------------
--                           Debugging
--------------------------------------------------------------------------------

---@param ci env.lang.ClassInfo
function C:doAttachToDebugger(ci)
  local ok, errmsg = udebugging.attach_to_debugger(ci, {})
  if not ok and errmsg then
    self:getEditor():echoInStatus(errmsg, 'ErrorMsg', true)
  end
end

--------------------------------------------------------------------------------

--
-- find existed path (with caching)
-- with JarViewer (ReverseEnginering mode) support
--
-- for reverse engineering mode, if such a class file is not in the project,
-- but is in the jar being studied, then it will pull it out of the jar,
-- decompile it and put it in the project source code.
--
---@param cn string?
---@return env.lang.ClassInfo?
---@return string? errmsg
function C:findExistedClassByClassName(cn)
  log_debug("findExistedClassByClassName %s", cn)
  if not cn or cn == '' then return nil, 'no classname' end

  local path, inner_path, err

  local ci = self:getCIbyClassName(cn) ---@cast ci env.lang.ClassInfo
  if ci and ci:getPath() ~= nil then
    path = ci:getPath()
    inner_path = ci:getInnerPath()
  else
    inner_path = self:getInnerPathForClassName(cn)
    path = self:getFullPathForInnerPath(inner_path)
  end

  -- to support ReverseEnginering mod (unzip and decompile from jar if has)
  if path and not file_exists(path) then
    local JarViewer = require 'env.langs.java.ui.JarViewer'
    local jviewer = JarViewer.getFirstViewer()
    if not jviewer then
      return nil, 'not found class:' .. v2s(cn) .. ' ' .. v2s(path)
    end

    inner_path = inner_path or self:getInnerPath(path)
    local path_in_jar = resolveInnerPathInJarViewer(self, jviewer, inner_path)

    if not path_in_jar then
      return nil, 'class not found in project and in opened jar-viewer ' ..
          'classname:' .. v2s(cn) .. ' cheched: ' .. v2s(path)
          .. ' and in jar: ' .. v2s(jviewer.jar_file) .. ' ' .. v2s(inner_path)
    end
    path, err = jviewer:doUnzipDecompileClassFile(path_in_jar, nil)
    if not file_exists(path) then
      return nil, 'class-file not found after decompile: ' .. v2s(path)
          .. "\n" .. v2s(err)
    end
  end

  if not ci then
    ci = self:getClassResolver(ClassInfo.CT.CURRENT, nil, cn):run()
        :getClassInfo(false) -- with cache

    if not ci then
      return nil, 'cannot resolve exited class ' .. v2s(ci)
          .. ' with exited path: ' .. v2s(path)
    end
  end

  return ci, nil
end

--
--
--
---@return boolean
---@return string? errmsg
function C:getClassInheritanceHierarchy(cn, out)
  log_debug("getClassInheritanceHierarchy cn:%s", cn)
  local ci, err = self:findExistedClassByClassName(cn)
  if not ci then return false, err end

  out[#out + 1] = cn

  local parentCi, has_extends

  while ci ~= nil do
    parentCi, has_extends = self:getParentClass(ci)
    log_debug('parent:%s has_extends:%s', parentCi, has_extends)
    if not parentCi then
      return not has_extends, has_extends == true and 'parent no found' or nil
    end

    local pcn = parentCi:getClassName()
    out[#out + 1] = pcn

    ci = parentCi
  end

  return true
end

--
-- for inheritance of classes and interfaces
-- B extends A
--
---@param ci env.lang.ClassInfo
---@return env.lang.ClassInfo?
---@return boolean?
---@return string? errmsg
function C:getParentClass(ci)
  return self:getSourcer():getParentClass(ci)
end

--
-- to get the package, imports, class name and the class definition
-- (extends, implements) from the current opened file(buf) of the source code.
--
---@return table?
function C:getClassDefintionInOpenedBuff()
  log_debug("getClassDefintionInOpenedBuff")
  local ctx = self:getEditor():getContext(true, JParser:new())
  local clazz = ctx:parse(JParser.predicate_stop_on.classdef):getResult()
  return clazz
end

---@param path2java string
---@return string? path to compiled class-file
---@return string? errmsg
---@return string? classname
function C:getCompiledClassFileForSource(path2java)
  local innerpath2java = self:getInnerPath(path2java)
  if not innerpath2java then
    return nil, 'not found inner path for ' .. v2s(path2java)
  end
  local classname = self:getClassNameForInnerPath(innerpath2java)
  if not classname then
    return nil, "classname not found for " .. v2s(path2java)
  end

  local inner_path2class = classname:gsub("%.", fs.path_sep) .. ".class"
  local srcpath = self:getSrcPath()
  local sourceset = string.find(path2java, srcpath) ~= nil and "main" or "test"
  -- todo if builder is maven then use "target/classes"
  local buildpref = fs.join_path("build", "classes", "java", sourceset)

  local path2class = fs.join_path(self.project_root, buildpref, inner_path2class)

  return path2class, nil, classname
end

--

class.build(C)
return C

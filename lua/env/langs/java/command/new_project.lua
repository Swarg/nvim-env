-- 14-07-2024 @author Swarg
--

-- local log = require 'alogger'
-- local su = require 'env.sutil'
-- local fs = require 'env.files'
-- local utbl = require 'env.util.tables'
local base = require 'env.lang.utils.base'
local ucmd = require 'env.lang.utils.command'
local pcache = require 'env.cache.projects'

local Lang = require 'env.lang.Lang'
local Editor = require 'env.ui.Editor'
local ObjEditor = require 'env.ui.ObjEditor'

local JavaLang = require 'env.langs.java.JLang'
local JavaBuilder = require 'env.langs.java.JBuilder'
local MavenBuilder = require 'env.langs.java.builder.Maven'
local ExecParams = require 'env.lang.ExecParams'


local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvProject new <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('generate maven project with pom.xml')
      :cmd('pom-xml', 'pom')

      :desc('Generate new Project via mnv archetype-generate')
      :cmd('mvn-archetype-generate', 'mag')

      :desc('Generate Project with Gradle build system')
      :cmd('grandle', 'g')

      :desc('Generate Flat-Dummy "Project" (RawSnippets in one directory)')
      :cmd('flat', 'f')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


-- local is_empty_str = base.is_empty_str
-- local tbl_merge_flat = utbl.tbl_merge_flat
local defineStdGenOpts = ucmd.defineStdGenOpts
local getDefaultProjProps = JavaBuilder.getDefaultProjectProperties
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


-- used w.vars.props to override defaults proj_props
---@param w Cmd4Lua
local function define_project_properties(w, title)
  local t = ucmd.define_project_properties(w, title, getDefaultProjProps())

  -- fill project property table(t) by cli options:
  w:opt_override('-c', '--main-class', t, "MAINCLASS")
  w:opt_override("-g", "--group_id", t, "GROUP_ID")
  w:opt_override("-a", "--artifact_id", t, "ARTIFACT_ID")
  w:opt_override("-v", "--version", t, "VERSION")
  w:opt_override("-j", "--java", t, "JAVA_VERSION", 'version of a java compiler')
  w:opt_override('-T', '--test-framework', t, 'TEST_FRAMEWORK')

  -- w:opt_override("-n", "--project_name", t, "PROJECT_NAME")
  -- w:opt_override("-r", "--project_root", t, "project_root")

  -- Notes: In the gradle artifactId by default takes from PROJECT_NAME

  return t
end

--
-- run given callback with project props
--
---@param w Cmd4Lua
---@param title string
---@param pp table
---@param callback function
local function apply(w, title, pp, callback)
  assert(type(title) == 'string', 'title')
  assert(type(callback) == 'function', 'expected function callback')
  assert(type(pp) == 'table', 'pp')

  if w.vars.yes then
    callback(pp, w:is_quiet())
  else
    ObjEditor.create(pp, title, callback,
      JavaBuilder.update_project_properties,
      JavaBuilder.getProjPropsKeyOrder())
  end
end

---@param pp table
---@param quiet boolean
---@param builder env.langs.java.JBuilder(parent for Ant, Maven, Gradle)
---@return env.lang.Lang
local function genProjectFiles(pp, quiet, builder, opts)
  assert(type(pp) == 'table', 'pp')
  assert(type(builder) == 'table', 'builder')

  -- local lang = JavaLang:new(nil, pp.project_root, MavenBuilder:new())
  -- lang:createBuilder()
  local lang = JavaLang:new()
      :setProjectBuilder(builder)
      :newProject(pp.project_root, opts) -- validate unique + sync-builder + cache

  lang:getLangGen():createProjectFiles(pp)

  local gen = lang:getLangGen()
  if not quiet then
    print(gen:getReadableSaveReport("Generated Files:"))
  end
  local path, code = gen:getFileWriter():getEntry(1)

  if code == base.OK_SAVED then
    Editor:new():open(path or '.')
  end

  return lang
end

--
--
--
---@param pp table
local function callback_new_pomxml(pp, quiet)
  -- run MavenBuilder to generate pom
  -- open pomfile in new buffer
  local builder = MavenBuilder:new(nil, pp.project_root)
  local cnt = builder:createBuildScript(pp, nil) -- ui.openFileCallback, force)
  if cnt < 1 then
    if not quiet then print(builder:getErrLogger():getReadableReport()) end
    return
  end

  if pp.CREATE_PROJECT then -- files
    genProjectFiles(pp, quiet, builder, {
      flat = nil,
      inmem = nil,
    })
  else
    local buildscript = builder:getFileWriter():getEntry(1)
    Editor:new():open(buildscript or '.')
  end

  if not quiet then
    print(builder:getFileWriter():getReadableReport())
  end
end

--
-- generate pom.xml file in current directory without parject files or
-- generate pom.xml + project files (without `mvn archetype:generate`)
--
---@param w Cmd4Lua
function M.cmd_pom_xml(w)
  defineStdGenOpts(w)
  w:about("Generate pom.xml (All keys are optional):")
  w:about("to create a whole New Project, use --new-project") -- CREATE_PROJECT=yes
  local pp = define_project_properties(w, { [1] = 'Generate pom.xml file' })
  -- TODO + webapp

  if not w:is_input_valid() then return end

  if w:is_verbose() then print(require "inspect" (pp)) end -- debugging

  apply(w, "NewMavenProject", pp, callback_new_pomxml)
end

--
-- Generate new Project via mnv archetype-generate
--
---@param w Cmd4Lua
function M.cmd_mvn_archetype_generate(w)
  defineStdGenOpts(w)
  w:about("Generate new Project via maven task 'archetype:generate'")

  w:desc('the archetype name of new generated project (quickstart|webapp)')
      :v_opt('--archetype-name', '-an')

  w.vars.props = MavenBuilder.getArchetypeDefaultProps(w.vars.archetype_name)

  local title = { [1] = 'New Java Project via `mvn archetype:generate`' }
  local pp = define_project_properties(w, title)
  pp.CREATE_PROJECT = true
  pp.openInEditor = true -- to open generated files

  if not w:is_input_valid() then return end

  if w:is_verbose() then print(require "inspect" (pp)) end -- debugging

  apply(w, "NewMavenProject", pp, function(pp0, quiet)
    if not pp0 or not pp0.CREATE_PROJECT then -- files
      if not quiet then print('canceled') end
    end

    local args = MavenBuilder.getArgsArchetypeGenerate(pp0)
    local opts = {
      handler_on_close = function(output)
        local dir = MavenBuilder.findGeneratedProjectDir(output)
        if dir then
          if vim then
            -- open buildscript in editor
            vim.api.nvim_exec(":e " .. ucmd.join_path(dir, 'pom.xml'), true)
          end
        else
          print('not found new project dir')
        end
      end
    }

    local params = ExecParams.of(nil, 'mvn', args):withOpts(opts)
    Editor:new():runCmdInBuffer('GenNewProject', params)
  end)
end

--
-- Generate Project with Gradle build system
--
---@param w Cmd4Lua
function M.cmd_grandle(w)
  defineStdGenOpts(w)
  w:about("Generate build.gralde")
  w:about("to create a whole New Project, use --new-project") -- CREATE_PROJECT=yes
  local pp = define_project_properties(w, { [1] = 'Generate build.gradle file' })

  if not w:is_input_valid() then return end

  if w:is_verbose() then print(require "inspect" (pp)) end -- debugging

  print("[DEBUG] pp:", require "inspect" (pp))
  error('Not implemented yet ')
end

--------------------------------------------------------------------------------
--                  Flat(Minimalistic to learning purposes)
--------------------------------------------------------------------------------

--
-- Dummy, in-memory project(flat)
-- TODO with makefile?
--
-- Generate Flat "Project" (RawSnippets in one directory)
-- for learning purposes
-- Goal: provide jumping from the sources to the tests files, back and so on.
-- In Memory Only
--
-- --project_root
--
---@param w Cmd4Lua
function M.cmd_flat(w)
  w:v_opt_quiet('-Q')
  w:v_opt_verbose('-V')
  local pp = define_project_properties(w, { [1] = 'New flat project' })
  ucmd.define_raw_snippets_opt(w, pp)

  if not w:is_input_valid() then return end

  local lang0 = Lang.getOpenedProject(pp.project_root)
  if lang0 then
    w:error('Project already exists for dir: ' .. v2s(lang0:getProjectRoot()))
    return
  end

  pp.flat = true
  pp.openInEditor = true -- to open generated files
  -- pp.inmem = true if not CREATE_PROJECT then in InMemoryOnly
  -- pp.RAW_SNIPPETS = w.vars.raw_snippets TODO

  -- check for an per-directory settings file
  pcache.readDirConf(ucmd.get_parent_dirpath(pp.project_root))
  -- todo auto add or ask from user to add new flat project into per-dir-settings

  if w:is_verbose() then print(require "inspect" (pp)) end -- debugging

  apply(w, "NewFlatProject", pp, function(pp0, quiet)      -- callback_new_flat_proj
    local lang, typ
    if not pp.CREATE_PROJECT then                          -- files
      typ = 'InMemoryOnly'
      lang = JavaLang:new():newProject(pp0.project_root, pp0)
    else
      typ = 'WithFiles'
      local builder = JavaBuilder:new(nil, pp.project_root, './', './')
      lang = genProjectFiles(pp, quiet, builder, {
        flat = true,
        inmem = false, -- generate main class
      })
    end
    if not quiet and lang then
      print(fmt('New Flat Project Initialized (%s): %s', typ, lang.project_root))
    end
  end)
end

return M

-- 03-10-2024 @author Swarg
--
-- Goal:
--  - EnvProjectManager(EnvPM) Impl for Java
--  - settings (jdtls) to show/hide diagnostic hints about TODO/FIX/XXX tags
--  - open the jar of the given arftifact (from g:a:v or under the cursor)
--
--  - Wrapper around the `mvn` cli tools  (:EnvPM mvn <subcmd>)


local log = require 'alogger'
local fs = require 'env.files'
local uclipboard = require 'env.util.clipboard'
local mvn = require 'env.langs.java.util.maven.cli'
local gradle_cache = require 'env.langs.java.util.gradle.cache'
local mvn_parser = require 'env.langs.java.util.maven.parser'
local JarViewer = require 'env.langs.java.ui.JarViewer'

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvPM <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('Project Management tools')
      :handlers(M)

      :desc('Setup the Project Settings')
      :cmd("settings", 's')

      :desc('Interact with Maven')
      :cmd("mvn", 'm')

  -- EnvGradle

      :desc('open the specified library (jar|war) to view its contents.')
      :cmd("open-jar", 'oj')

      :run()
      :exitcode()
end

--
-- :EnvPM settings <subcmd>
--
---@param w Cmd4Lua
function M.cmd_settings(w)
  w:handlers(M)

      :desc('set compiler taskTags to show in diagnostics (def:TODO,FIXME,XXX)')
      :cmd("compiler-task-tags", 'ctt')

      :run()
      :exitcode()
end

--

--
-- :EnvPM mvn
--
---@param w Cmd4Lua
function M.cmd_mvn(w)
  w:handlers(M)

      :desc('show info about given artifact name')
      :cmd("info", 'i')

      :desc('download(install) the given artifact into local maven repository')
      :cmd("download", 'd')

      :desc('convert data from selected lines to groupId:artifactId:vesion')
      :cmd("get-artifact-name", 'gan')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local is_valid_artifact_name = mvn.is_valid_artifact_name

---@param w Cmd4Lua
---@return env.langs.java.JLang
local function get_lang(w)
  local gen = assert(w:var('gen'), 'LangGen') ---@type env.langs.java.JGen
  local lang = gen:getLang() ---@cast lang env.langs.java.JLang
  return lang
end

local patt_gradle_deps = '^[/|%-%s%s+]+%s+([^%s]+)%s*[%(cn%*%)]+$'

---@param w Cmd4Lua
---@param lang env.langs.java.JLang
---@return false|string
local function get_artifact_name_under_cursor(w, lang)
  log_debug("get_artifact_name_under_cursor")
  local ctx = lang:getEditor():getContext(true)
  local word = ctx:resolveWordSimple('[^\'"]+').word_element or ''
  local version_required = false
  if not is_valid_artifact_name(word, version_required) then
    local rest = match(word, patt_gradle_deps) -- from `gradle dependencies` output
    if rest and is_valid_artifact_name(rest) then
      word = rest
    else
      rest = match(word, '^%s*//%s*([^%s]+)') -- from `// group:art:ver`
      if rest and is_valid_artifact_name(rest) then
        word = rest
      else
        w:error('expected artifact-name in quotes under the cursor got:'
          .. v2s(word))
        return false
      end
    end
  end ---@cast word string
  return word
end


--
-- show info about given artifact name
--
---@param w Cmd4Lua
function M.cmd_info(w)
  local name = w:tag('artifact'):desc('the artifact name to download'):pop():arg()
  w:desc('show the latest version'):v_has_opt('--latest-version', '-l')

  if not w:is_input_valid() then return end

  if name == '.' or not name then
    name = get_artifact_name_under_cursor(w, get_lang(w))
    if not name then return end -- with err output
  end ---@cast name string

  local info, err = mvn.artifact_info(name, w.vars)
  if not info then
    return w:error(err)
  end
  w:say(info)
end

--
-- download given artifact
--
---@param w Cmd4Lua
function M.cmd_download(w)
  local name = w:tag('artifact'):desc('the artifact name to download'):pop():arg()
  w:desc('with sources'):v_has_opt('--sources', '-s')
  w:desc('with javadocs'):v_has_opt('--javadoc', '-d')
  w:desc('custom repository'):v_opt('--repository', '-r')
  w:v_opt('--dependency-plugin-version', '-P')

  if not w:is_input_valid() then return end
  ---@cast name string
  if name == '.' then
    name = get_artifact_name_under_cursor(w, get_lang(w))
    if not name then return end -- with err output
  end
  local ok, err = mvn.dependency_get(get_lang(w), name, w.vars)
  if not ok then
    return w:error(err)
  end
end

--
-- convert data from selected lines to groupId:artifactId:vesion
--
---@param w Cmd4Lua
function M.cmd_get_artifact_name(w)
  local to_cb = w:desc('copy to clipboard'):has_opt('--to-clipboard', '-c')
  if not w:is_input_valid() then return end

  local opts = w.vars.vim_opts or {}
  local lang = get_lang(w)

  local lines = lang:getEditor():getContext(true):getLines(opts.line1, opts.line2)
  local name, err = mvn.lines_to_short_artifact_name(lines)
  if not name then
    return w:error(err);
  end

  if to_cb then
    uclipboard.copy_to_clipboard(name)
    w:say('copied!')
  else
    w:say(name)
  end
end

--
-- to toggle diagnostic display for TODO in source code
--
-- set compiler taskTags to show in diagnostic (default: TODO,FIXME,XXX)
--
-- COMPILER_TASK_TAGS org.eclipse.jdt.core.compiler.taskTags
--
-- another way to setup to permanent is add line to the file:
--
-- .settings/org.eclipse.jdt.core.prefs
--   org.eclipse.jdt.core.compiler.taskTags=TODO,FIXME,XXX
--
--   or empty value to disable all wrarnings in diagnostic messages:
--
--   org.eclipse.jdt.core.compiler.taskTags=
--
---@param w Cmd4Lua
function M.cmd_compiler_task_tags(w)
  w:usage('EnvPM set-task-tags off')
  w:usage('EnvPM set-task-tags TODO,FIXME,XXX')
  w:usage('EnvPM set-task-tags def')

  local tags = w:desc('tags to enable in diagnostic messages'):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast tags string

  local ejdtls = require 'env.bridges.eclipse'
  if not tags or tags == 'off' or tags == '0' or tags == '-' then
    tags = '' -- to hide all task tags
  elseif tags == 'def' or tags == 'default' or tags == 'on' or tags == '+' then
    tags = 'TODO,FIXME,XXX'
  else
    tags = string.upper(tags)
  end

  ejdtls.set_compiler_taskTags(tags)
end

--
-- find full path to given artifact(Dependency) in local maven repo or
-- in gradle cache
--
---@param w Cmd4Lua
---@param opts table? {preferring_source}
---@return table? files
---@return string? dir
local function findPathToArtifact(w, gav, opts)
  opts = opts or {}
  if not gav.version or gav.version == '' then
    local dirs, err = mvn.get_artifact_versions(gav)
    if type(dirs) ~= 'table' then return w:error(err) end
    if #dirs == 1 then
      gav.version = dirs[1]
    else
      local idx = Editor.pickItemIndex(dirs, "Choose version")
      if not idx or not dirs[idx] then
        return w:error('canceled')
      end
      gav.version = dirs[idx]
    end
  end

  local files, dir, raw_file_list, err1, err2

  raw_file_list, err1, dir = mvn.get_artifact_files(gav)

  -- in local maven repo
  if type(raw_file_list) == 'table' then
    files, err2 = mvn.parse_artifact_files(gav, raw_file_list)
    log_debug('mvn parser_artifact', gav, err2)
    if type(files) == 'table' then
      return files, dir
    end
  end

  -- in local gradle cache
  raw_file_list, err1, dir = gradle_cache.get_artifact_files(gav)
  if type(raw_file_list) == 'table' then
    files, err2 = mvn.parse_artifact_files(gav, raw_file_list)
    log_debug('gradle parser_artifact', gav, err2)
    if type(files) == 'table' then
      return files, dir
    end
  end

  log_debug("error:", err1)

  return nil, dir
end

--
-- open the specified library (jar|war) to view its contents
-- EnvPM open-jar
--
---@param w Cmd4Lua
function M.cmd_open_jar(w)
  w:v_opt_dry_run('-d')
  local name = w:tag('artifact'):desc('the artifact name to open'):pop():arg()

  if not w:is_input_valid() then return end

  if name == '.' then
    name = get_artifact_name_under_cursor(w, get_lang(w))
    if not name then return end -- with err output
  end
  ---@cast name string

  -- 1. get the path to given artifact
  local gav = mvn_parser.parse_artifact_coords(name)
  if not gav then
    return w:error('cannot parse "' .. v2s(name) .. '" to the artifact name')
  end ---@cast gav table

  local files, dir = findPathToArtifact(w, gav)
  if not files then return end -- with errmsg

  local path
  if files.sources then
    path = fs.build_path(dir, files.sources)
  elseif files.jar then
    path = fs.build_path(dir, files.jar)
  else
    return w:error('no jar and sources in local maven repo for given artifact')
  end

  if w:is_dry_run() then return w:say('path to open: ' .. v2s(path)) end

  local ok, err3 = JarViewer:new(nil, path):openJarFile()
  if not ok then
    w:error(err3)
  end
end

return M

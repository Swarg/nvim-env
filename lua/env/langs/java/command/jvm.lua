-- 02-10-2024 @author Swarg
--
-- :EnvJvm
--
-- Goals:
--  - Interact, Research and Manage JVM
--  - Decompile class-file from running jvm
--  - integration with Research-Agent
--
-- Notes:
-- - to make symllink for jb-fernflower from jdtls use luarock "task-extractor":
--   tet eclipse jdtls
--

local log = require 'alogger'
local cu = require 'env.util.commands'
local fs = require 'env.files'
local c4lv = require 'env.util.cmd4lua_vim'
local jvm = require 'env.langs.java.util.jvm'
local core = require 'env.langs.java.util.core'
local ui = require 'env.langs.java.util.ui'
local ja = require 'env.langs.java.util.javaagent'
local Lang = require 'env.lang.Lang'
local decomp = require 'env.langs.java.util.decompiler.decomp_manager'
local eclipse = require 'env.bridges.eclipse'
local Editor = require 'env.ui.Editor'
local cmd_decompile = require 'env.langs.java.command.decompile'
local ln_realign = require 'env.langs.java.util.realign_linenums'


local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvJvm <cmd>
--
---@param opts table
function M.handle(opts)
  return c4lv.newCmd4Lua(opts)
      :root(':EnvJvm')
      :about('Interact with Java Virtual Machine')
      :handlers(M)

      :desc('show the list of the running jvms')
      :cmd("list", 'ls')

      :desc('select one jvm for future use')
      :cmd("select", 's')

      :desc('show information about selected jvm')
      :cmd("info", 'i')

      :desc('forget already selected jvm')
      :cmd("forget", 'f')

      :desc('show all loaded classes or classes for given package')
      :cmd("all-loaded-classes", 'alc')

      :desc('get information about given class from the running jvm')
      :cmd("class-info", 'ci')

      --

      :desc('get bytecode of the specified class from jvm (with decompiling)')
      :cmd("get-bytecode", 'gb')

      :desc('update the bytecode of compiled classes in seleced jvm')
      :cmd("hotswap", 'hs')

      :desc('open compiled class file for given java source from opened Project')
      :cmd("open-compiled", 'oc')

      :desc('decompile class-files to readable sources')
      :cmd("decompile", 'd')

      :desc('fix decompiled class file by jdlts for debugging with correct lnums')
      :cmd("fix-line-numbers", 'fln')

      :run()
      :exitcode()
end

local _commands = { "list", "select", "info", "forget",
  "all-loader-classes", "class-info", "get-bytecode", "decompile",
  'hotswap', 'open-compiled', 'fix_line-numbers'
}
M.opts = { nargs = '*', range = true, complete = cu.mk_complete(_commands) }

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


--
-- decompile class-files to readable sources
-- route to subcommand aka aliase for `:EnvDecompile -PL java ...`
--
--
---@param w Cmd4Lua
function M.cmd_decompile(w)
  -- todo w.vars.gen =
  return cmd_decompile.handle(w)
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local home = os.getenv('HOME')
local isFullClassNameWithPackage = core.isFullClassNameWithPackage
local get_classname_from_clipboard = ui.get_classname_from_clipboard
local log_debug, lfmt = log.debug, log.format

-- table {cmd, pid, args...}
local current_selected_jvm = nil

-- ?? remove?
---@param w Cmd4Lua
local function define_decompiler_and_libs(w)
  w:desc('decompiler provider name'):v_opt('--decompiler', '-d')
  -- libs can used by decompile (like fernflower)
  -- to produce a more precision source code from bytecode (class-files)
  w:desc('libraries(dependencies)'):v_optl('--libs', '-l')
  -- defualt is decomp.DECOMP_DIR
  w:desc('output dir for decompiled sources'):v_opt('--output-dir', '-O')
end


---@param w Cmd4Lua
---@return string?
local function define_classname_arg(w)
  local cn = w:desc('the full classname to decompile'):tag('classname'):pop():arg()
  ---@cast cn string
  return cn
end

---@param w Cmd4Lua
local function define_classname_path2src_path2bin_opts(w)
  w:desc("the fully qualifed class name of the class-file to update in jvm")
      :v_opt("--classname", "-c")
  w:desc("path to a compiled class-file"):v_opt("--path-to-bin", "-b")
  w:desc("path to a java source file to update"):v_opt("--path-to-src", "-s")
end


-- ---@param w Cmd4Lua
-- ---@return env.langs.java.JLang
-- local function get_lang(w)
--   local gen = assert(w:var('gen'), 'LangGen') ---@type env.langs.java.JGen
--   local lang = gen:getLang() ---@cast lang env.langs.java.JLang
--   return lang
-- end
-- local lang = Lang.getLangByCtx(w, true, true, false)


-- w.vars.pid
---@param w Cmd4Lua
---@param idx number?
---@return table?{pid:number,cmd,args,version,..} -- cmd is binjava
local function pick_jvm(w, idx, not_remember)
  local pid_or_name = w.vars.pid
  log_debug("pick_jvm pid:%s idx:%s", pid_or_name, idx)

  local readable_list, elist = jvm.get_readable_jvm_list(home)
  idx = idx
      or jvm.find_jvm_idx_by_name(elist, pid_or_name, w.vars.pattern)
      or Editor.pickItemIndex(readable_list)
  log_debug("use idx", idx)

  if type(idx) ~= 'number' or idx < 1 then
    w:error('canceled')
    return nil
  end
  local entry = elist[idx]
  if not entry then
    w:error('not found jvm with index', idx)
    return nil
  end

  local pid = entry.pid
  if type(pid) ~= 'number' then
    w:error('out of bounts ' .. v2s(idx) .. ' max:' .. v2s(#elist))
    return nil
  end

  if not not_remember then
    current_selected_jvm = entry
  end

  return entry
end

---@param w Cmd4Lua
---@param q string?
---@return string?
local function pick_classname_from_ctx(w, q)
  q = q or 'classname'

  -- local ctx = get_lang(w):getEditor():getContext(true)
  -- classname = ctx:resolveWordSimple("[%w%.%$]").word_element
  local cline = vim.api.nvim_get_current_line()
  -- // pkg.app.Class
  local classname = match(cline, '%s*//%s*(.-)%s*$')
      or match(cline, '^%s*(.-)%s*$')

  if (q == 'package') then
    local pkg = match(cline, "^%s*package%s+(.-)%s*;%s*")
    if pkg and pkg ~= '' then return pkg end
  end

  log_debug("pick_classname_from_ctx %s found-word: '%s'", q, classname)

  if not isFullClassNameWithPackage(classname) then
    return w:error('cannot get ' .. v2s(q) .. ' from current line: ' .. v2s(cline))
  end

  if q == 'package' then
    local pkg = core.getPackage(classname) or classname
    classname = pkg
  end
  log_debug("ret:", classname)
  return classname
end


--
---@param path string?
---@param err string?
---@param lines table?
---@param w Cmd4Lua
local function show_agent_report(w, path, err, lines)
  if not path then return w:error(err) end
  vim.cmd(':e ' .. v2s(path))

  if type(lines) == 'table' then
    lines[#lines + 1] = ''
    vim.api.nvim_buf_set_lines(0, 0, 0, true, lines)
  end
end


local function verify_can_modify_buf(bufnr)
  local modifiable = vim.api.nvim_buf_get_option(bufnr, 'modifiable')
  if modifiable == false then
    vim.api.nvim_buf_set_option(bufnr, 'modifiable', true)
    vim.api.nvim_buf_set_lines(bufnr, 0, 0, true, { "// check insert" })
    vim.api.nvim_buf_set_option(bufnr, 'modifiable', false)
  end
end

---@param w Cmd4Lua
local function get_classname_from_jdt(w, bufnr)
  local bufname = vim.api.nvim_buf_get_name(bufnr)
  local t = eclipse.parse_jdt_path(bufname)
  local classname = eclipse.get_classname_from_parsed_jdt(t)
  if not classname then
    return w:error('(not jdt://?) cannot find classname from ' .. v2s(bufname))
  end
  return classname
end

---@param w Cmd4Lua w.vars.{[classname],[output_dir], decompiler, libs}
---@param logfile? string path to Agent report
---@param lines table? AgentLoader report
---@return string?
local function find_and_decompile_saved_classfile(w, logfile, lines)
  log_debug("find_and_decompile_saved_classfile", logfile)
  if not logfile then
    return w:error('no logfile')
  end
  if not fs.file_exists(logfile) then
    return w:error('not found agent report:' .. v2s(logfile)
      .. table.concat(lines or E, "\n"))
  end
  local path2bytecode, err2 = ja.report_get_saved_class_file(logfile)
  log_debug("found path2bytecode", path2bytecode)
  if not path2bytecode then
    return w:error(err2)
  end

  w.vars.decompiler = w.vars.decompiler or decomp.DEFAULT_DECOMPILTER
  w.vars.inner_path = path2bytecode -- ??

  -- decompiled source file
  local path, err3 = decomp.decompile_classfile(path2bytecode, w.vars)
  if not path then
    return w:error(err3)
  end
  return path
end

--------------------------------------------------------------------------------

--
-- show list of the runneing jvms
-- EnvDecompile jvms-list
---@param w Cmd4Lua
function M.cmd_list(w)
  local idx = w:desc():optn("--index", "-i")
  if not w:is_input_valid() then return end

  local readable_list, elist = jvm.get_readable_jvm_list(home)
  if idx then
    local vme = elist[idx]
    if vme then
      w:say(lfmt('Selected JVM: %s', vme))
      w:say(lfmt("Version: ", jvm.get_java_version(vme.cmd)))
    else
      w:error('not found jvm with idx: ' .. v2s(idx))
    end
    return
  end
  w:say(table.concat(readable_list or E, "\n"))
end

--
-- select one jvm for future use
--
---@param w Cmd4Lua
function M.cmd_select(w)
  local idx = w:desc():optn("--index", "-i")
  if not w:is_input_valid() then return end

  current_selected_jvm = pick_jvm(w, idx)
  w:say('select jvm with pid:' .. v2s((current_selected_jvm or E).pid))
end

--
-- info about current selected jvm
--
---@param w Cmd4Lua
function M.cmd_info(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end
  if type(current_selected_jvm) ~= 'table' then
    current_selected_jvm = nil
    return w:error('no selected jvm')
  end

  local vm = current_selected_jvm
  local args0 = ''
  if vm.args ~= nil then
    args0 = table.concat(vm.args, ' ')
    if #args0 > 60 then
      args0 = '..' .. v2s(vm.args[#vm.args])
    end
  end
  if not w:is_verbose() then
    w:say(v2s(vm.pid) .. ' ' .. v2s(vm.cmd) .. ' ' .. args0)
  else
    local t = {
      lfmt('pid: %s cpu:%s mem:%s etime:%s', vm.pid, vm.cpu, vm.mem, vm.etime),
      'cmd: ' .. v2s(vm.cmd),
      'args: ' .. table.concat(vm.args or E, " "),
    }
    w:say(table.concat(t, "\n"))
  end
end

--
-- forget already selected jvm
--
---@param w Cmd4Lua
function M.cmd_forget(w)
  if not w:is_input_valid() then return end
  current_selected_jvm = nil
end

--------------------------------------------------------------------------------
--               Research JVM State via Research-Agent
--------------------------------------------------------------------------------

--
-- show all loaded classes or classes for given package
--
---@param w Cmd4Lua
function M.cmd_all_loaded_classes(w)
  w:usage('EnvDecompile show-loaded-classes --package org.some.app.service')
  w:v_opt_verbose("-v")
  w:v_opt_dry_run("-d")
  w:desc('package filter to show loaded classes with given package')
      :v_opt('--package', '-p')

  if not w:is_input_valid() then return end

  if w.vars['package'] == '.' then -- pick pkg from the comment in the current line
    w.vars['package'] = pick_classname_from_ctx(w, 'package')
    if not w.vars['package'] then return end
  elseif w.vars['package'] == 'cb' then
    w.vars['package'] = get_classname_from_clipboard(w, 'package')
    if not w.vars['package'] then return end
  end

  local vme = current_selected_jvm or pick_jvm(w, nil, false)
  if not vme then return end
  if w:is_dry_run() then w:say('[DRY_RUN]') end

  local path, err, lines = ja.get_all_loaded_classes_from_jvm(vme.cmd, vme.pid, w.vars)
  show_agent_report(w, path, err, lines)
end

--
-- get information about given class in life jvm
--
---@param w Cmd4Lua
function M.cmd_class_info(w)
  w:v_opt_verbose("-v")
  local classname = define_classname_arg(w)

  if not w:is_input_valid() then return end

  if not classname or classname == '.' then
    classname = pick_classname_from_ctx(w)
  elseif classname == 'cb' then
    classname = get_classname_from_clipboard(w)
  end
  if not classname then return end

  local vme = current_selected_jvm or pick_jvm(w, nil, false)
  if not vme then return end

  local path, err, lines = ja.get_jvm_classinfo(vme.cmd, vme.pid, classname, w.vars)
  show_agent_report(w, path, err, lines)
end

--------------------------------------------------------------------------------

--
-- get bytecode of the specified class from a given jvm
-- and decompile it into readable java-source-file
--
-- aka get class bytecode from jvm
--
---@param w Cmd4Lua
function M.cmd_get_bytecode(w)
  w:usage('EnvDecompile get-bytecode . pkg.comp.App')
  w:usage('EnvDecompile get-bytecode -f -d procyon')

  w:v_opt_verbose("-v")
  w:desc("pick classname from current opend source file"):v_has_opt("--file", "-f")

  define_decompiler_and_libs(w)
  local classname = w:desc('the full classname to decompile')
      :tag('classname'):optional():pop():arg()

  if not w:is_input_valid() then return end

  local vme = current_selected_jvm or pick_jvm(w, nil, false)
  if not vme then return end

  if not classname and w.vars.file then
    local bufnr = vim.api.nvim_buf_get_name(0)
    local lang = Lang.findOpenedProjectByPath(bufnr)
    if not lang then
      return w:error('not found project for file ' .. v2s(bufnr))
    end
    classname = lang:getClassName(bufnr)
  end
  if not classname or classname == '.' then -- pick class from the comment in the current line
    classname = pick_classname_from_ctx(w)
  elseif classname == 'cb' then
    classname = get_classname_from_clipboard(w)
  end
  if not classname then return end -- with errmsg
  ---@cast classname string

  local opts = { binjava = vme.cmd }
  local logfile, err, loader_report = ja.get_bytecode_from_jvm(
    vme.cmd, vme.pid, classname, opts)
  if not logfile then
    return w:error(err)
  end

  if not w.vars.decompiler then
    show_agent_report(w, logfile, err, loader_report)
  else
    w.vars.classname = classname
    local path = find_and_decompile_saved_classfile(w, logfile, loader_report)
    if not path then return end
    vim.cmd(':e ' .. v2s(path))
  end
end

--
--
-- to fix decompiled class file by jdlts for debugging with correct lnums
-- fetch given class from jvm and decompile with line numbers
--
-- workaround to fix linenumbers for class decompiled by jdtls
-- when you call this command from buffer with decompiled *.class file
-- with broken line-numbers
--
-- Decompile classes without sources
-- https://github.com/eclipse-jdtls/eclipse.jdt.ls/issues/297
--
-- Use fernflower as the default decompiler
-- https://github.com/eclipse-jdtls/eclipse.jdt.ls/pull/2704
--
---@param w Cmd4Lua
function M.cmd_fix_line_numbers(w)
  log_debug("cmd_fix_line_numbers")
  define_decompiler_and_libs(w)

  if not w:is_input_valid() then return end

  local classname = get_classname_from_jdt(w, 0)
  if not classname then return end

  verify_can_modify_buf(0)

  local vme = current_selected_jvm or pick_jvm(w, nil, false)
  if not vme then return end

  local opts = { binjava = vme.cmd }
  -- path to agent report and response from AgentLoader
  local logfile, err, agent_loader_report = ja.get_bytecode_from_jvm(
    vme.cmd, vme.pid, classname, opts)
  if not logfile then return w:error(err) end

  -- find path to decompiled file
  local path = find_and_decompile_saved_classfile(w, logfile, agent_loader_report)
  if not path then return end

  -- realign linenumbers
  local lines = fs.read_lines(path)
  local mapping = ln_realign.build_mapping_from_source_lines(lines)
  local max_lnum = (mapping or E).max_lnum
  if not max_lnum or max_lnum < 1 then
    return w:error('Not found any line-number mappings in ' .. v2s(path))
  end

  local result = ln_realign.realign(lines, mapping)

  -- modify current buffer (output from jdtls with wrong decompiled class file)
  vim.api.nvim_buf_set_option(0, 'modifiable', true)
  vim.api.nvim_buf_set_lines(0, 0, -1, true, result)
  vim.api.nvim_buf_set_option(0, 'modifiable', false)

  w:say("Done")
end

--

---@param w Cmd4Lua
---@return string? classname
---@return string? path to compiled class file
local function resolve_classname_and_path2class(w)
  local classname = w.vars.classname  -- fqcn
  local path2src = w.vars.path_to_src -- source java file
  local path2bin = w.vars.path_to_bin -- compiled bytecode

  local lang, err
  local bufname = vim.api.nvim_buf_get_name(0)

  if classname and not path2bin then
    lang = Lang.findOpenedProjectByPath(bufname)
    if not lang then
      return w:error("cannot find opened project for current file")
    end
    ---@cast lang env.langs.java.JLang
    local ipath = lang:getInnerPathForClassName(classname)
    path2src = lang:getFullPathForInnerPath(ipath)
    error('Not implemented yet find compiled file in build/classes ' .. v2s(path2src))
  end

  if not classname and path2bin then
    error('Not implemented yet for case has binpath but no classname')
    -- ask via javap or own tool
  elseif not classname and not path2bin and not path2src then
    path2src = bufname
    lang = lang or Lang.findOpenedProjectByPath(path2src)
    if not lang then
      return w:error('not found opened project for ' .. v2s(path2src))
    end ---@cast lang env.langs.java.JLang

    path2bin, err, classname = lang:getCompiledClassFileForSource(path2src)
    if not path2bin then return w:error(err) end
    if not fs.file_exists(path2bin) then
      return w:error("not found path to compiled class-file: " ..
        "compiled file is not exists(found): " .. path2bin)
    end
  end
  return classname, path2bin
end

--
-- open compiled class file for given java source from opened Project
--
---@param w Cmd4Lua
function M.cmd_open_compiled(w)
  define_classname_path2src_path2bin_opts(w)
  define_decompiler_and_libs(w)

  if not w:is_input_valid() then return end

  local classname, path2class = resolve_classname_and_path2class(w)
  if not classname or not path2class then return end -- with error
  local opts = {
    classname = classname,
    decompiler = w.vars.decompiler or decomp.DEFAULT_DECOMPILTER,
  }
  local path2java, err = decomp.decompile_classfile(path2class, opts)
  -- when you open class-file via jdtls then it can use old cached state and
  -- do not show actual recompiled state! so i use a custom decompiler here
  -- to show actualy updated sources
  -- vim.cmd(":e " .. path2class)
  if not path2java then
    return w:error(err)
  end
  log_debug("path2java: |%s|", path2java)
  vim.cmd(":e " .. v2s(path2java))
end

--
-- update the bytecode of compiled classes in seleced jvm
--
---@param w Cmd4Lua
function M.cmd_hotswap(w)
  define_classname_path2src_path2bin_opts(w)
  w:desc("flag to open a compiled class file to inspect")
      :v_has_opt("--open-compiled", "-o")

  if not w:is_input_valid() then return end

  local classname, path2class = resolve_classname_and_path2class(w)
  if not classname or not path2class then return end -- with error

  local vme = current_selected_jvm or pick_jvm(w, nil, false)
  if not vme then return end

  local opts = { binjava = vme.cmd }
  local logfile, err0, loader_report = ja.update_bytecode_in_jvm(
    vme.cmd, vme.pid, classname, path2class, opts)
  if not logfile then
    return w:error(err0)
  end
  M.find_last_agent_output_message(logfile, vme.pid, 'hotswap', loader_report)

  if w.vars.open_compiled then
    vim.cmd(":e " .. path2class)
  end

  w:say(table.concat(loader_report or {}, "\n"))
end

---@param logfile string|table
---@param jvm_pid number
---@param cmd string
---@return table?
function M.find_last_agent_output_message(logfile, jvm_pid, cmd, output)
  local report_lines = nil
  if type(logfile) == 'string' then
    report_lines = fs.read_lines(logfile)
  elseif type(logfile) == 'table' then -- for testing
    report_lines = logfile
  else
    error('expected file name of the agent report file')
  end
  local pid = jvm_pid == nil and nil or tostring(jvm_pid)
  local patt = '%[Agent%] PID:%[(%d+)%] via agentmain, args:\'([%w_%-]+)%s+'
  for i = #report_lines, 1, -1 do
    local line = report_lines[i]

    local pid0, cmd0 = match(line, patt)
    if (pid == nil or pid0 == pid) and (cmd == nil or cmd == cmd0) then
      output = output or {}
      for j = i, #report_lines do
        output[#output + 1] = report_lines[j]
      end
      return output
    end
  end

  return nil
end

return M

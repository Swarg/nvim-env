-- 30-09-2024 @author Swarg
-- Goal:
-- NewStuff Command handler for JavaGen

local log = require 'alogger'
local fs = require 'env.files'
local ln_realign = require 'env.langs.java.util.realign_linenums'
local ufmt = require 'env.langs.java.util.formatting'
local urefactor = require 'env.langs.java.util.refactor'

local cmd_add = require 'env.langs.java.command.refactor.add'
local cmd_class = require 'env.langs.java.command.refactor.class'
local cmd_fix = require 'env.langs.java.command.refactor.fix'
local cmd_name_mapping = require 'env.langs.java.command.refactor.name_mapping'
local cmd_pkg_mapping = require 'env.langs.java.command.refactor.pkg_mapping'
local cmd_codegen = require 'env.langs.java.command.refactor.codegen'
local cmd_stacktrace = require 'env.langs.java.command.refactor.stacktrace'

local M, MR = {}, {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvRefactor <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  log.debug("EnvRefactor handle")
  w:about('Refactor JavaCode')
      :handlers(M)

      :desc('interact with name mappings (restore original names form deobf code)')
      :cmd("name-mapping", 'nm')

      :desc('interact with package mappings (used to add external source file)')
      :cmd("package-mapping", 'pm')

      :desc("rename substrings in the code")
      :cmd("rename", 'r')

      :desc('line numbers realignment (stretching lines for debugging)')
      :cmd("realign-line-numbers", 'rln')

      :desc('interact with specified class')
      :cmd("class", 'c')

      :desc('add resources and code from another projects')
      :cmd("add", 'a')

      :desc('generate the boilerplate source code')
      :cmd("generate", 'g')

      :desc('fix some issues with the source code')
      :cmd("fix", 'f')

      :desc('light source code formatting (to fix basics style issues)')
      :cmd("light-formating", 'lf')

      :desc('format only the current line with source code')
      :cmd("format-method-signature", 'fms')

      :desc('change the if condition to the opposite')
      :cmd("invert-condition", 'ic')

      :desc('interact with stack traces')
      :cmd("stack-trace", 'st')

      :run()
      :exitcode()
end

function M.cmd_rename(w)
  w:handlers(MR)

      :desc('rename all occurrences of a given substring using sed')
      :cmd("all-in-dir", 'a')

      :run()
end

--
-- interact with specified class
--
---@param w Cmd4Lua
function M.cmd_class(w) return cmd_class.handle(w) end

---@param w Cmd4Lua
function M.cmd_add(w) return cmd_add.handle(w) end

---@param w Cmd4Lua
function M.cmd_generate(w) return cmd_codegen.handle(w) end

---@param w Cmd4Lua
function M.cmd_fix(w) return cmd_fix.handle(w) end

function M.cmd_name_mapping(w) return cmd_name_mapping.handle(w) end

function M.cmd_package_mapping(w) return cmd_pkg_mapping.handle(w) end

function M.cmd_stack_trace(w) return cmd_stacktrace.handle(w) end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local file_exists = fs.file_exists

--------------------------------------------------------------------------------
--                           Renaming
--------------------------------------------------------------------------------


--
-- rename all occurrences of a given substring using sed
-- for unix OS
--
---@param w Cmd4Lua
function MR.cmd_all_in_dir(w)
  w:usage('EnvRefactor rename all ..')
  local dir = w:tag('dir'):desc('directory'):pop():arg()
  local old = w:tag('old'):desc('the string to be replaced'):pop():arg()
  local new = w:tag('new'):desc('replacement'):pop():arg()
  local ext = w:desc('extension of sources'):def('java'):opt('--extension', '-e')

  if not w:is_input_valid() then return end
  ---@cast dir string

  local cmd
  cmd = fmt("find %s -type f -name '*.%s' -exec sed -i 's/%s/%s/g' {} \\; 2>&1",
    dir, ext, old, new)
  if dir == '.' then
    dir = os.getenv('PWD')
  end
  if not dir or not fs.dir_exists(dir) then
    return w:error("Directory not exists: " .. v2s(dir))
  end
  if old == nil or old == "" or old == " " then
    return w:error("no string to be replaced")
  end
  if new == nil or new == "" or new == " " then
    return w:error("no replacement string")
  end
  if old == new then
    return w:error("replaced string is equal replacement")
  end
  w:say(cmd)
  os.execute(cmd)
end

----
--
-- Line Number realignment
--
-- local find_buff_endswith

--
-- line numbers realignment(stretching lines for debugging)
-- ReAlign the code in the currently open or specified file using the line
-- numbers specified in the comments at the end of the lines
--
---@param w Cmd4Lua
function M.cmd_realign_line_numbers(w)
  local filename = w:desc('file to realign'):optional():pop():arg()
  if not w:is_input_valid() then return end
  if not filename or filename == '.' then -- current buffer
    -- already opened
  elseif not file_exists(filename) then
    return w:error('Not Found: ' .. v2s(filename))
  else
    vim.cmd(':e ' .. filename) -- open file
  end

  local max_lnum, err = M.do_realign_lines(0)
  if not max_lnum then
    return w:error(err)
  end
  w:say('done. max_lnum: ' .. v2s(max_lnum))
end

function M.do_realign_lines(bufnr)
  bufnr = bufnr or 0
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, true)
  local mapping = ln_realign.build_mapping_from_source_lines(lines)
  local max_lnum = (mapping or E).max_lnum
  if not max_lnum or max_lnum < 1 then
    return nil, 'Not found any line-number mappings in current buffer'
  end

  local result = ln_realign.realign(lines, mapping)
  vim.api.nvim_buf_set_lines(0, 0, -1, true, result)
  return max_lnum, nil
end

--
-- light source code formatting (to fix basics style issues
-- to small improvement and source code formatting fix
--
-- //comment   -->  // comment
-- /*comment*/ -->  /* comment */
-- /**         -->  /**
-- for(x       -->  for (x)
--
---@param w Cmd4Lua
function M.cmd_light_formating(w)
  if not w:is_input_valid() then return end

  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, true)

  ufmt.format(lines)

  vim.api.nvim_buf_set_lines(0, 0, -1, true, lines)
end

---@param lines table
local function update_current_line(lines)
  local lnum = (vim.api.nvim_win_get_cursor(0) or E)[1]
  vim.api.nvim_buf_set_lines(0, lnum - 1, lnum, true, lines)
end

--
-- format only the current line with source code
--
---@param w Cmd4Lua
function M.cmd_format_method_signature(w)
  local max_len = w:desc('max_len'):defOptVal(90):opt('--max-len', '-m')

  if not w:is_input_valid() then return end

  local line = vim.api.nvim_get_current_line()
  local lines = ufmt.fmt_method_signature(line, max_len)
  update_current_line(lines)
end

--
-- change the if condition to the opposite
-- if (a != null && a.size() > 0)  -->  if (a == null || s.size <= 0)
--
---@param w Cmd4Lua
function M.cmd_invert_condition(w)
  if not w:is_input_valid() then return end

  local line = vim.api.nvim_get_current_line()

  local altered_line = urefactor.invert_condition(line)

  update_current_line({ altered_line })
end

return M

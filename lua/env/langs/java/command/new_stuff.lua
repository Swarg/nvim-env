-- 14-07-2024 @author Swarg
-- Goal:
-- NewStuff Command handler for JavaGen

local log = require 'alogger'
local fs = require 'env.files'
local Editor = require 'env.ui.Editor'
-- local Context = require 'env.ui.Context'
local ucmd = require 'env.lang.utils.command'
local Lang = require 'env.lang.Lang'
local JGen = require 'env.langs.java.JGen'
local ClassInfo = require 'env.lang.ClassInfo'
local ucommon = require 'env.lang.utils.common'
local spring_deps = require 'env.langs.java.util.spring.dependency'
local uluatest = require 'env.langs.java.util.testing.luatest'
local umakefile = require 'env.langs.java.util.makefile'
local pojo_openapi = require 'env.langs.java.command.pojo.openapi'
local git = require 'env.langs.java.util.git'

local M, MP = {}, {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvNew <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('New Stuff for JavaCode')
      :handlers(M)

      :desc('Generate new java class')
      :cmd("class", "c")

      :desc('Generate POJO')
      :cmd("pojo", 'p')

      :desc('Create new test-case file. (many-tests-for-one-source)')
      :cmd("test-case", 'tc')

      :desc('Create new test-method')
      :cmd("test-method", 'tm')

      :desc('add definition to build script to do not generate html reports')
      :cmd("test-no-html-report", 'tnhr')

      :desc('Add new dependency to buildscript file')
      :cmd("dependency", 'd')

      :desc('Add a template for writing integration tests')
      :cmd("integration-test", 'it')

      :desc('Add Makefile to project with common used commands')
      :cmd("makefile", 'mf')

      :desc('Add .gitignore file to the project')
      :cmd("gitignore", 'gi')

      :desc('Add Own Service Impl to /META-INF/services for ServiceLoader')
      :cmd("service-loader-meta", 'slm')

      :run()
end

--
-- EnvNew pojo <subcmd>
--
---@param w Cmd4Lua
function M.cmd_pojo(w)
  w:about('New Stuff for JavaCode')
      :handlers(MP)

      :desc('Generate POJO for given OpenAPI schemas')
      :cmd("open-api", "oa")

      :run()
end

--

--
-- Generate POJO for given OpenAPI schemas
---@param w Cmd4Lua
function MP.cmd_open_api(w) return pojo_openapi.handle(w) end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


function M.dump_state()
  pojo_openapi.dump_state()
end

function M.restore_state()
  pojo_openapi.restore_state()
end


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

local defineStdGenOpts = ucmd.defineStdGenOpts
local resolve_deps_names = ucommon.pick_deps_by_names
local pick_deps_interactively = ucommon.pick_deps_interactively
local join_path = fs.join_path

-- oop
---@param w Cmd4Lua
---@return false|string
---@return string?
function M.cmd_class(w)
  assert(type(w) == 'table', 'w')
  local gen = w:var('gen') ---@type env.langs.java.JGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X --force-rewrite|F

  local classname, pkg
  pkg = w:opt('--package', '-p', 'by default its taken from a current file')

  w:group('A', 1)
      :desc('If a shortname - generate with a package(ns) of current opened')
      :tag('classname'):pop():v_arg()
  w:group('A', 1)
      :desc('Take a ClassName from a current line with import or `x = new X`')
      :tag('classname')
      :v_optp('--from-use', '-u', JGen.getClassNameFromLine, gen)

  w:v_opt('--imports', '-i')
  w:v_opt('--extends', '-e')
  w:v_opt('--implements', '-m')

  -- params
  w:v_has_opt('--constructor', '-c')

  w:tag('test_class'):has_opt('--test', '-t', 'generate a test file')
  -- methodname to autogen pick from self:getContext().element(LEX_TYPE.FUNC)


  ----------------------------------- actually work (runs if all input correct)
  if not w:is_input_valid() then return false end


  -- convert from cli defintion to Field objects
  -- if fields_cli then -- list of strings from cli input to parse
  --   w:set_var('fields', FieldGen.parseFieldsFromStrings(fields_cli))
  -- end

  classname = w.vars.classname -- autovalidate in is_input_valid
  local for_current_file
  if classname == '.' then
    classname = gen:getCurrentOpenedFileName('basename')
    for_current_file = true
    -- w.vars.force_rewrite = gen:getContext():getLinesCount() < 2
  end

  -- if package(namespace) not defined when take from current opened class
  if not classname:find('.', 1, true) then
    if not pkg then
      local ctx = gen:getContext()
      local from_ci = gen.lang
          :resolveClass(ctx.bufname, ClassInfo.CT.CURRENT)
          :getClassInfo()
      if from_ci then
        pkg = from_ci:getPackage()
        log.debug('Resolve from current: %s pkg:', from_ci:toString(), pkg)
      end
    end
  end

  if pkg then
    classname = pkg .. '.' .. classname
  end

  if not for_current_file and not w:is_quiet() then
    classname = Editor.askValue('Full ClassName: ', classname)
    if not classname or classname == '' then
      gen:getEditor():echoInStatus('Canceled')
      return false, nil
    end
  end
  log.debug('New ClassName to Generate: %s', classname)

  local req_ctype = (w.vars.test_class == true) and ClassInfo.CT.TEST or
      ClassInfo.CT.SOURCE
  local new_ci = gen.lang:lookupByClassName(classname, req_ctype)

  local path, errmsg = gen:createClassFile(new_ci, w:get_vars())
  if w:is_verbose() then
    gen:getReadableSaveReport()
  end

  if gen:hasErrors() then
    w:say(gen:getErrLogger():getReadableReport("Errors:"))
  end

  return path, errmsg -- for testing
end

--
-- Create new test-case file. (many-tests-for-one-source)
--
-- new test-case - in subdir with source-file name
-- dy default test-case will be named from the method name under the cursor
--
---@param w Cmd4Lua
function M.cmd_test_case(w)
  local gen = w:var('gen') ---@type env.langs.java.JGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  w:desc('test-case name i.g. method name')
      :v_opt('--name', '-n') -- support name with subdirs:  sub1/sub2/

  if not w:is_input_valid() then return end

  return gen:createNewTestCaseFile(w.vars)
end

--
-- Create new test-method.
--
---@param w Cmd4Lua
function M.cmd_test_method(w)
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X
  w:desc('the name of the test-method'):v_opt('--name', '-n')

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@type env.langs.java.JGen
  return gen:createNewTestMethod(w.vars)
end

--
-- add definition to build script to do not generate html reports
--
---@param w Cmd4Lua
function M.cmd_test_no_html_report(w)
  if not w:is_input_valid() then return end
  -- to find Lang(Project) from buff with opened build-script
  local lang = Lang.findOpenedProjectByPath(vim.api.nvim_buf_get_name(0))
  if not lang then
    return w:error('cannot find already opened project for current file')
  end
  local lines = nil
  local builder = lang:getProjectBuilder()
  ---@diagnostic disable-next-line, undefined-field:
  if builder and builder.genNoHtmlReportDefinition then
    ---@diagnostic disable-next-line, undefined-field:
    lines = builder:genNoHtmlReportDefinition()
  end
  if lines == nil then
    return w:error('not supported by current builder: ' .. v2s(builder))
  end

  lang:getEditor():getContext(true):insertAfterCurrentLine(lines)
end

--------------------------------------------------------------------------------

--
-- Add new dependency to buildscript file
--
---@param w Cmd4Lua
function M.cmd_dependency(w)
  w:handlers(M)

      :desc('Add dependency for Spring Boot')
      :cmd("spring-boot", 'sb')

      :run()
end

--
-- Add dependency for Spring Boot
--
---@param w Cmd4Lua
function M.cmd_spring_boot(w)
  local names = w:desc():optional():pop():argl()

  if not w:is_input_valid() then return end


  local deps_to_add = nil

  if names and #names > 0 then
    deps_to_add = resolve_deps_names(spring_deps.map, names)
  else
    deps_to_add = pick_deps_interactively(spring_deps.map)
  end
  if type(deps_to_add) ~= 'table' or #deps_to_add == 0 then
    return w:error('cancel')
  end

  local gen = w:var('gen') ---@type env.langs.java.JGen
  gen:getLang():addDependencies(deps_to_add)
end

local MT = {}
M.integration_test = MT

--
-- Add a template for writing integration tests for a webapp with Lua
--
---@param w Cmd4Lua
function M.cmd_integration_test(w)
  w:about('New Integration test template')
      :handlers(MT)

      :desc('add template to writing integration test for webapp with LuaTest')
      :cmd("webapp-lua", "wal")

      :run()
end

--
-- Add template to write integration LuaTest
--
---@param w Cmd4Lua
function MT.cmd_webapp_lua(w)
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X
  w:usage(':EnvNew it wal')

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@type env.langs.java.JGen
  local project_root = gen:getLang():getProjectRoot()
  local path, body = uluatest.add_webapp_n_db_template(project_root, w.vars)

  if gen:createFile(false, path, body, w.vars) then
    gen:openFile(path, true, w.vars)
  else
    w:say(gen:getReadableSaveReport())
  end
  if w:is_quiet() then w.vars._res = { path = path, body = body } end -- testing
end

--
--
-- Add Makefile with common used commands
--
---@param w Cmd4Lua
function M.cmd_makefile(w)
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@type env.langs.java.JGen
  local project_root = gen:getLang():getProjectRoot()
  local path = join_path(project_root, 'Makefile')
  local luatest_dir = join_path(project_root, uluatest.SRC_TEST_LUA)

  if fs.file_exists(path) then
    return w:error('file already exists: ' .. v2s(path))
  end

  local builder = gen:getLang():getProjectBuilder()
  if not builder then
    return w:error('not found builder for: ' .. v2s(project_root))
  end

  local t = builder:getSettings()
  local proj = (t or E).project or E
  local artifact, version = proj.name or proj.artifactId, proj.version

  w.vars.openInEditor = true
  w.vars.is_springboot = spring_deps.is_spring_boot_project(t)
  w.vars.luatest = fs.dir_exists(luatest_dir)
  w.vars.use_qdb = true

  local body = umakefile.gen_Makefile_body(artifact, version, w.vars)

  if gen:createFile(false, path, body, w.vars) then
    gen:openFile(path, true, w.vars)
  else
    w:say(gen:getReadableSaveReport())
  end
  if w:is_quiet() then w.vars._res = { path = path, body = body } end -- testing
end

--
-- Add .gitignore file to the project
--
---@param w Cmd4Lua
function M.cmd_gitignore(w)
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@type env.langs.java.JGen
  local project_root = gen:getLang():getProjectRoot()
  local path = join_path(project_root, '.gitignore')

  if fs.file_exists(path) then
    return w:error('file already exists: ' .. v2s(path))
  end

  local body = git.gen_gitignore_body(w.vars)

  if gen:createFile(false, path, body, w.vars) then
    gen:openFile(path, true, w.vars)
  else
    w:say(gen:getReadableSaveReport())
  end
end

--
-- Add Own Service Impl to /META-INF/services for ServiceLoader
-- add current classname as a service implementation into
-- src/main/resources/META-INF/services/...
--
---@param w Cmd4Lua
function M.cmd_service_loader_meta(w)
  local interface = w:desc("Service(interface) name"):opt("--interface", "-i")
  local impl = w:desc("full class name of the ServiceImpl"):opt("--impl", "-c")
  w:v_opt_dry_run('-d')

  if not w:is_input_valid() then return end

  local gen = w:var('gen') ---@cast gen env.langs.java.JGen

  local path, err = gen:genServiceLoaderImpl(interface, impl, w:is_dry_run())
  if not path then return w:error(err) end
  if w:is_dry_run() then return w:say(path) end

  vim.cmd(':e ' .. v2s(path))
end

return M

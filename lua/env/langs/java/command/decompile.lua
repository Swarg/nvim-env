-- 02-10-2024(08-02-2025) @author Swarg
--
-- :EnvDecompile
--
-- Goals:
--  - Decompile specified class-file
--
-- Notes:
-- - to make symllink for jb-fernflower from jdtls use luarock "task-extractor":
--   tet eclipse jdtls
--

local log = require 'alogger'
local fs = require 'env.files'
local Lang = require 'env.lang.Lang'
local U = require 'env.langs.java.util.decompiler.decomp_manager'


local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvDecompile <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :root(':EnvDecompile')
      :about('Decompile class-files to readable source code')
      :handlers(M)

      :desc('show known decompiler names')
      :cmd("list", 'ls')

      :desc('select decompiler to work')
      :cmd('select', 's')

      :desc('show info about selected decompiler')
      :cmd('info', 'i')

      :desc('forget already selected decompiler')
      :cmd('forget', 'x')

  --

      :desc('decompile specified class or jar file')
      :cmd("file", 'f')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local log_debug, lfmt = log.debug, log.format


---@param w Cmd4Lua
local function define_decompiler_and_libs(w)
  w:desc('decompiler provider name'):v_opt('--decompiler', '-d')
  -- libs can used by decompile (like fernflower)
  -- to produce a more precision source code from bytecode (class-files)
  w:desc('libraries(dependencies)'):v_optl('--libs', '-l')
end

--
-- to copy decompiled file into the project (e.g. for debugging)
--
---@return string?
---@param w Cmd4Lua  {vars.fix_pkg}
function M.copy_source_file_to_project(w, path)
  local lang = Lang.findOpenedProjectByPath(vim.api.nvim_buf_get_name(0))
  if not lang then
    lang = Lang.pickOpenedProjectInteractively()
    if not lang then
      return w:error('not found Project for current opened file(or canceled)')
    end
  end
  local new_path, err = lang:addExternalSourceFile(path, w.vars)
  if not new_path then
    return w:error(err or 'cannot copy extenal source file to the project')
  end
  return new_path
end

--------------------------------------------------------------------------------

local selected_decompiler_name = U.DEFAULT_DECOMPILTER

--
-- show known decompilers list
--
---@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end

  w:say(table.concat(U.get_decompilers_list(), ' '))
end

--
-- select decompiler to work
--
---@param w Cmd4Lua
function M.cmd_select(w)
  local name_or_idx = w:desc('decompiler name or index in list'):pop():arg()
  if not w:is_input_valid() then return end

  if tonumber(name_or_idx) then
    local name = U.get_decompilers_list()[tonumber(name_or_idx) or -1]
    if not name then
      return w:error('out of bounds ' .. v2s(name_or_idx))
    end
  elseif name_or_idx then
    selected_decompiler_name = v2s(name_or_idx)
  end
end

--
-- show info about selected decompiler
--
---@param w Cmd4Lua
function M.cmd_info(w)
  if not w:is_input_valid() then return end
  w:say('default decompiler:  ' .. v2s(U.DEFAULT_DECOMPILTER))
  w:say('selected decompiler: ' .. v2s(selected_decompiler_name))
end

--
-- forget already selected decompiler
--
---@param w Cmd4Lua
function M.cmd_forget(w)
  if not w:is_input_valid() then return end
  selected_decompiler_name = U.DEFAULT_DECOMPILTER
end

--
-- decompile given classfile or jar from the given file
--
---@param w Cmd4Lua
function M.cmd_file(w)
  local path = w:desc('path to jar or classfile'):pop():arg()
  define_decompiler_and_libs(w)
  local copy2project = w
      :desc('copy to the current opened project (e.g. for debugging)')
      :has_opt('--copy', '-c')
  w:desc('use package and class name from source file to restore inner_path')
      :v_has_opt('--fix-pkg', '-F')

  if not w:is_input_valid() then return end
  ---@cast path string

  if path == '.' then
    path = vim.api.nvim_buf_get_name(0)
    if fs.extract_extension(path or '') ~= 'class' then
      local cline = vim.api.nvim_get_current_line()
      -- Saved: path/to/Some.class
      path = match(cline, '%s*([^%s]+)%s*$')
    end
    log_debug('path to decompile: ' .. v2s(path))
  end

  w.vars.decompiler = w.vars.decompiler or selected_decompiler_name
  w.vars.inner_path = path -- ??

  local opath, err = U.decompile_classfile(path, w.vars)
  if not opath then
    log_debug('error:', err)
    return w:error(err)
  end
  if copy2project then
    opath = M.copy_source_file_to_project(w, opath)
    if not opath then return end
  end

  local ok, err2 = pcall(vim.cmd, ":e " .. opath)
  if not ok then
    return w:error(err2)
  end
  w:say("decompiled: " .. v2s(opath))
end

return M

-- 23-02-2025 @author Swarg
--
-- Goals:
--  - open source file of a given classname in gradle sources
--    helper for GotoDefinition what works from file with gradle source
--    this is a start point to quick jump into gradle source installed locally
--    in the ~/.gradle/wrapper/dists/gradle-{VER}-all/{HASH}/gradle-{VER}/src/

local c4lv = require 'env.util.cmd4lua_vim'
local cmd_daemon = require 'env.langs.java.command.gradle.daemon'
local cmd_sources = require 'env.langs.java.command.gradle.sources'

local M, MPDC, MDV = {}, {}, {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvGradle <cmd>
--
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvGradle')
      :about('Gradle Build System')
      :handlers(M)

      :desc('manage a gradle daemons')
      :cmd("daemon", 'd')

      :desc('interact with gradle sources')
      :cmd("sources", 's')

      :desc('open source file of gradle by given classname')
      :cmd("find-in-source", 'f')

      :desc('interact with a versions of the wrapper distr with sources')
      :cmd("distr-version", 'dv')

      :desc('insteract with plugin_dir(in gradle source) cache')
      :cmd("plugin-dir-cache", 'pdc')

      :run()
end

--
-- insteract with plugin_dir(in gradle source) cache
--
---@param w Cmd4Lua
function M.cmd_distr_version(w)
  w:handlers(MDV)
      :about('wrapper distr with sources("-all") installed in the os')

      :desc('show the version of wrapper dirst with sources installed in the os')
      :cmd("get", 'g')

      :desc('set the version of wrapper dirst with sources installed in the os')
      :cmd("set", 's')

      :desc('show the list of locally installed wrapper distr versions with sources')
      :cmd("list", 'ls')

      :run()
end

--
-- insteract with plugin_dir(in gradle source) cache
--
---@param w Cmd4Lua
function M.cmd_plugin_dir_cache(w)
  w:handlers(MPDC)
      :about('manage cache of pkg -> plugin_dir mapping used in GotoDefinition')

      :desc('show the plugin dir cache')
      :cmd('show', 's')

      :desc('clear the plugin_dir cache')
      :cmd('clear', 'c')

      :run()
end

---@param w Cmd4Lua
function M.cmd_sources(w) return cmd_sources.handle(w) end

---@param w Cmd4Lua
function M.cmd_daemon(w) return cmd_daemon.handle(w) end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

local fs = require 'env.files'
local ugs = require 'env.langs.java.util.gradle.gradle_sources'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


-- disrt-version cmd

--
-- show the version of wrapper dirst with sources installed in the os
--
---@param w Cmd4Lua
function MDV.cmd_get(w)
  if not w:is_input_valid() then return end
  w:say('version: ' .. v2s(ugs.cached_version_of_gradle_wrapper_distr))
  w:say('srcdir:  ' .. v2s(ugs.cached_root_dir_of_distr_with_sources))
end

--
-- set the version of wrapper dirst with sources installed in the os
-- :EnvPM gradle disrt-version set
--
---@param w Cmd4Lua
function MDV.cmd_set(w)
  local version = w:desc("version of the wrapper distr with sources"):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast version string

  if version == 'reset' or version == 'nil' or version == 'null' then
    ugs.cached_version_of_gradle_wrapper_distr = nil
    ugs.cached_root_dir_of_distr_with_sources = nil
    return w:say('cached wrapper distr version is reset')
  end

  -- given not a version but a directory with sources
  if (#version or '') > 10 and fs.dir_exists(version) then
    ugs.cached_version_of_gradle_wrapper_distr = version
  end

  local path = ugs.find_root_dir_of_gradle_sources(version)
  if not path or not fs.dir_exists(path) then
    return w:error('not found sources for given version: ' .. v2s(version))
  end

  ugs.cached_version_of_gradle_wrapper_distr = version
  ugs.cached_root_dir_of_distr_with_sources = path
  w:say('setuped: ' .. v2s(path))
end

--
-- show the list of locally installed wrapper distr versions with sources
--
---@param w Cmd4Lua
function MDV.cmd_list(w)
  if not w:is_input_valid() then return end
  local versions = {}
  local max_ver, err = ugs.find_gradle_version_of_distr_with_sources(versions)
  if not max_ver then
    w:error(err)
  end
  w:say(table.concat(versions, " ") .. ' (max: ' .. v2s(max_ver) .. ')')
end

-- plugin-dir-cache

--
-- show the plugin dir cache
--
---@param w Cmd4Lua
function MPDC.cmd_show(w)
  if not w:is_input_valid() then return end
  local t = ugs.get_plugin_dir_cache()
  w:say(require "inspect" (t))
end

--
-- clear the plugin_dir cache
--
---@param w Cmd4Lua
function MPDC.cmd_clear(w)
  if not w:is_input_valid() then return end
  ugs.clear_plugin_dir_cache()
end

return M

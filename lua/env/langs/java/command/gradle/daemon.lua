-- 24-02-2025 @author Swarg
-- Goals:
--   - manage gradle daemons
-- TODO:
--   - open logs

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvGradle daemon <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)

      :desc("stop all gradle daemons")
      :cmd("stop", 's')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                             IMPLEMENTATION
--------------------------------------------------------------------------------

--
-- stop all gradle daemons
--
---@param w Cmd4Lua
function M.cmd_stop(w)
  if not w:is_input_valid() then return end
  os.execute("gradle --stop")
end

return M

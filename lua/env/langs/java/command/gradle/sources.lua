-- 24-02-2025 @author Swarg
local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvGradle daemon <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)

      :desc("open source file by given classname")
      :cmd("open", 'o')

      :desc("to find source file by specified short classname")
      :cmd("find", 'f')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                             IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
local fs = require 'env.files'
local Editor = require 'env.ui.Editor'
local JParser = require 'env.langs.java.JParser'
local ugs = require 'env.langs.java.util.gradle.gradle_sources'
local core = require 'env.langs.java.util.core'
local ui = require 'env.langs.java.util.ui'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local parse_path_to_gradle_source_file = ugs.parse_path_to_gradle_source_file
local get_classname_from_clipboard = ui.get_classname_from_clipboard
local log_debug = log.debug

--
--
---@param w Cmd4Lua
---@return string?
---@return string?
---@return string?
local function define_opts_to_resolve_sources_root_dir(w)
  w:desc('the gradle version'):v_opt('--version', '-v')
  w:desc('the root directory with gradle sources'):v_opt('--root-dir', '-D')
  w:desc('the plugin dir to search'):v_opt('--plugin-dir', '-p')
end

---@param w Cmd4Lua
local function resolve_sources_root_dir_opt(w)
  local version = w.vars.version or ugs.cached_version_of_gradle_wrapper_distr
  local src_root_dir = w.vars.root_dir or ugs.cached_root_dir_of_distr_with_sources
  local plugin_dir = w.vars.plugin_dir
  log_debug("resolve_sources_root_dir_opt", version, plugin_dir, src_root_dir)

  local err = nil

  if not version and not src_root_dir then
    local bufname = vim.api.nvim_buf_get_name(0)
    -- to check is current file is a file in gradle source
    version, src_root_dir, plugin_dir = parse_path_to_gradle_source_file(bufname)
    log_debug("parsed path to gradle-source-file:", version, src_root_dir, plugin_dir)
  end

  if not version then
    version = ugs.find_gradle_version_of_distr_with_sources()
    if version ~= nil and not ugs.cached_version_of_gradle_wrapper_distr then
      log_debug("found gradle version to use:", version)
      ugs.cached_version_of_gradle_wrapper_distr = version
    end
  end

  if not src_root_dir then
    src_root_dir, err = ugs.find_root_dir_of_gradle_sources(version)
    log_debug("found gradle sources root dir to use:", src_root_dir)
    if not src_root_dir then
      return w:error(err)
    end
    ugs.cached_root_dir_of_distr_with_sources = src_root_dir
  end

  if not src_root_dir then
    return w:error('not found root dir of gradle sources')
  end
  return src_root_dir, plugin_dir
end
--
-- open source file of gradle by given classname
--
---@param w Cmd4Lua
function M.cmd_open(w)
  w:usage("EnvGradle sources open cb")
  w:usage("EnvGradle sources open .")
  w:usage("EnvGradle s o .")

  define_opts_to_resolve_sources_root_dir(w)
  local fqcn = w:desc('the classname of the source file to open')
      :optional():pop():arg()

  if not w:is_input_valid() then return end

  local path, err

  if fqcn == 'cb' or fqcn == 'clipboard' then
    fqcn = get_classname_from_clipboard(w, 'classname')
    if not fqcn then return end -- with errmsg
  elseif not fqcn or fqcn == '.' then
    local line = vim.api.nvim_get_current_line()
    fqcn = JParser.parseImportLineToFQCN(line)
    if not fqcn and core.isFullClassNameWithPackage(line) then
      fqcn = line
    end
    if not fqcn then
      return w:error('expected line with import definition or with classname only')
    end
  end

  if not fqcn then
    return w:error('no classname')
  end ---@cast fqcn string
  if not core.isFullClassNameWithPackage(fqcn) then
    return w:error('expected classname got: ' .. fqcn)
  end

  local src_root_dir, plugin_dir = resolve_sources_root_dir_opt(w)
  if not src_root_dir then return nil end -- with errmsg

  path, err = ugs.findPathToSourceOfClass0(fqcn, src_root_dir, plugin_dir)
  if not path then
    return w:error(err or ('not found source of ' .. v2s(fqcn)))
  end
  vim.cmd(':e ' .. v2s(path))
end

--
--
-- to find source file by specified short classname
--
---@param w Cmd4Lua
function M.cmd_find(w)
  define_opts_to_resolve_sources_root_dir(w)
  local scn = w:tag("scn"):desc('short class name to find in the gradle sources')
      :pop():arg()

  if not w:is_input_valid() then return end

  local src_root_dir, plugin_dir = resolve_sources_root_dir_opt(w)
  if not src_root_dir then return nil end -- with errmsg

  local dir = fs.join_path(src_root_dir, plugin_dir)

  local cmd = fmt('find %s -name %s.java -type f', v2s(dir), v2s(scn))
  log_debug("cmd: ", cmd)
  local lines = fs.execrl(cmd)
  if not lines then
    return w:error("cannot execute system command:\n" .. v2s(cmd))
  end
  if #lines == 0 then
    return w:error("not found file to short class name:" .. v2s(scn))
  end
  local path = nil
  if #lines == 1 then
    path = lines[1]
  else
    local idx = Editor.pickItemIndex(lines)
    if not idx or idx < 1 or not lines[idx] then
      return w:error('canceled')
    end
    path = lines[idx]
  end

  vim.cmd("e " .. v2s(path))
end

return M

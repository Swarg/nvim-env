-- 28-01-2025 @author Swarg


local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvRefactor fix <subcmd>
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('to fix the arguments order in the method call')
      :cmd('method-args-order', 'mao')

      :desc('change the source code of the junit4 tests to switch to junit5')
      :cmd('update-test-sourcecode', 'uts')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
-- local fs = require 'env.files'
-- local ufmt = require 'env.langs.java.util.formatting'
local Editor = require 'env.ui.Editor'
local consts = require 'env.langs.java.util.consts'
local JTestDiagnost = require 'env.langs.java.JTestDiagnost'
local junit4to5 = require 'env.langs.java.util.codegen.testing.junit4to5'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--
-- to fix the arguments order in the method call
-- based on nvim diagnostic from jdtls
--
---@param w Cmd4Lua
function M.cmd_method_args_order(w)
  log_debug("cmd_method_args_order")
  local fix_all = w:desc('fix all the same errors'):has_opt('--all', '-a')

  if not w:is_input_valid() then return end

  local ctx = Editor:new():getContext(true)
  local dlist = JTestDiagnost.filter(ctx:getCurrentLineDiagnostic())
  local errinfo, err0 = JTestDiagnost.get_error_info((dlist[1] or E).message)
  log_debug('error-info:%s dlist:%s', errinfo, type(dlist), err0)
  if not errinfo then
    -- when there is no diagnosis(cannot parse), but it is known that the order
    -- of arguments is wrong  old(msg, exp, res) --> new (exp, res, msg)
    if string.find(ctx.current_line or '', 'assertEquals') ~= nil then
      errinfo = {
        class_name = 'Assertions',
        method_name = 'assertEquals',
        method_params = 'int, int, String',
        bad_args = 'String, int, int',
        errtype = consts.ERROR.WRONG_METHOD_ARGS,
      }
    end
    if not errinfo then
      return w:error(err0)
    end
  end

  local line, err = junit4to5.fix_method_args(ctx.current_line, errinfo)
  if not line then
    return w:error(err)
  end
  ctx:updateLine(line, ctx.cursor_row)

  -- for the entire current buffer
  local c, ec = 0, 0
  if fix_all and dlist[1] then
    local lnum = (dlist[1] or E).lnum
    dlist = ctx:getAllDiagnosticsSameAs(dlist[1])
    for n, d in ipairs(dlist) do
      if d.lnum ~= lnum then
        local orig_line = (ctx:getLines(d.lnum + 1, d.lnum + 1) or E)[1]
        errinfo = JTestDiagnost.get_error_info(d.message)
        line, err = junit4to5.fix_method_params(orig_line, errinfo)
        if not line then
          w:say(fmt('%s/%s [%s]: %s', v2s(n), v2s(#dlist), v2s(d.lnum), v2s(err)))
          ec = ec + 1
        else
          ctx:updateLine(line, d.lnum + 1)
          c = c + 1
        end
      end
    end
    w:say('fixed lines: ' .. v2s(c) .. ' errors: ' .. v2s(ec))
  end
end

--
-- change the source code of the junit4 tests to switch to junit5
--
-- remove all System.out.println used to output test names
-- It can be very useful when updating the code of tests from Junit4 on Junit5
--
---@param w Cmd4Lua
function M.cmd_update_test_sourcecode(w)
  local all = w:desc('all test files'):has_opt('--all-test', '-a')

  if not w:is_input_valid() then return end

  if not all then
    local path = vim.api.nvim_buf_get_name(0)
    local changes, err = junit4to5.update_test_source(path, w.vars)
    if not changes or changes == 0 then
      return w:error(err)
    end
  else
    error('Not implemented yet')
  end
  vim.cmd(':e')
end

return M

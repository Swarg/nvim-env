-- 19-01-2025 @author Swarg
--
-- rename package_mapping can be used to
--  - add external-source-code with the package renaming
--  - fix packages in imports section of the classes

local junit5 = require 'env.langs.java.util.codegen.testing.junit5'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvRefactor package-mapping <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)

      :desc('add the pair of the package mapping (old->new)')
      :cmd('add', 'a')

      :desc('list all package mappings')
      :cmd('list', 'ls')

      :desc('delete the entry of the package mapping')
      :cmd('remove', 'rm')

      :desc('save the current mapping to the specified file')
      :cmd('save', 's')

      :desc('load mapping from the specified file')
      :cmd('load', 'l')

      :desc('open saved mapping in the editor')
      :cmd('open', 'o')

      :desc('parse the output from a git-status command and add to the mappings')
      :cmd('add-from-git', 'afg')

      :desc('clear all mappings')
      :cmd('clear', 'clean')

      :desc('add mapping to update packages for given library')
      :cmd('update-lib', 'ul')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
local Editor = require 'env.ui.Editor'
local core = require 'env.langs.java.util.core'


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

-- to share mapping from one nvim instance with another one via file
local default_mapping_fn = (not log.is_win) and '/tmp/rename-pkg-mapping' or
    "c:/rename-pkg-mapping"


-- pkg.something --> pkg.smth
-- local pkg_mapping = { } -- stored in Sourcer (per project scrope)

---@param w Cmd4Lua
---@return env.langs.java.JSourcer
local function get_sourcer(w)
  local gen = w:var('gen') ---@type env.lang.oop.LangGen
  local sourcer = gen:getLang():getSourcer() ---@cast sourcer env.langs.java.JSourcer
  return sourcer;
end

---@param w Cmd4Lua
---@return table
local function get_pkg_mapping(w)
  return get_sourcer(w):getRenamePackageMapping()
end

local function get_pkg_mapping_count(w)
  local c = 0
  for _, _ in pairs(get_pkg_mapping(w)) do c = c + 1 end
  return c
end

---@param w Cmd4Lua
local function get_sorted_pkg_mapping_keys(w)
  local keys = {}
  for key, _ in pairs(get_pkg_mapping(w)) do keys[#keys + 1] = key end
  table.sort(keys)
  log_debug('sorted package-mapping keys: %s', keys)
  return keys
end


--
-- add the entry of the package mapping
--
---@param w Cmd4Lua
function M.cmd_add(w)
  local from = w:desc('from - the old name of the package'):pop():arg()
  local to = w:desc('to - the new name of the package'):pop():arg()

  if not w:is_input_valid() then return end

  -- pick mapping from the current line
  if from == to and from == '.' then
    from, to = core.parse_import_mapping(Editor.getCurrentLine())
    print()
    from = Editor.askValue("from: ", from)
    if (from == '' or from == 'q') then return w:error('canceled') end
    to = Editor.askValue("to: ", to)
    if (to == '' or to == 'q') then return w:error('canceled') end
  end

  if not from or from == '' or from == '.' then
    return w:error('error "from" package name')
  end
  if not to or to == '' or to == '.' then
    return w:error('error "to" package name')
  end

  local pkg_mapping = get_pkg_mapping(w)
  pkg_mapping[from] = to
  w:say("add package mapping: " .. v2s(from) .. ' --> ' .. v2s(to))
end

--
-- list all package mappings
--
---@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end

  local t = {}
  local pkg_mapping = get_pkg_mapping(w)
  if not (pkg_mapping) then
    return w:say('empty for the current project')
  end

  for n, key in ipairs(get_sorted_pkg_mapping_keys(w)) do
    t[#t + 1] = fmt("%2d %s --> %s", n, v2s(key), v2s(pkg_mapping[key]))
  end
  w:say(table.concat(t, "\n"))
end

--
-- delete the entry of the package mapping
--
---@param w Cmd4Lua
function M.cmd_remove(w)
  local from = w:tag("index"):desc('from package name'):pop():arg()

  if not w:is_input_valid() then return end

  local idx = tonumber(from)
  if idx then
    local from0 = get_sorted_pkg_mapping_keys(w)[idx]
    if not from0 then
      return w:error('not found package for index' .. v2s(idx))
    end
    from = from0
  end
  assert(from, 'expected "from" package name')

  local pkg_mapping = get_pkg_mapping(w)
  local to = pkg_mapping[from or false]
  pkg_mapping[from] = nil
  w:say('removed package mapping: ' .. v2s(from) .. ' --> ' .. v2s(to))
end

--
-- save the current mapping to the specified file
-- e.g. for sharing between multiple nvim instances
--
---@param w Cmd4Lua
function M.cmd_save(w)
  local fn = w:desc("filename to save current mapping")
      :def(default_mapping_fn):opt("--filename", "-f")
  local rewrite = w:desc("rewrite already existed"):has_opt("--rewrite", "-w")

  if not w:is_input_valid() then return end

  local ok, err = get_sourcer(w):saveRenamePackageMapping(fn, rewrite)
  if not ok then return w:error(err) end
  w:say("saved: " .. v2s(ok) .. " to " .. fn)
end

--
-- load mapping from the specified file
-- e.g. for sharing between multiple nvim instances
--
---@param w Cmd4Lua
function M.cmd_load(w)
  local fn = w
      :desc("filename from which to load mapping and add to current")
      :def(default_mapping_fn):opt("--filename", "-f")
  local clear_prev = w
      :desc("clear already existed (default is add to existed mapping")
      :has_opt('--clear_prev', '-c')

  if not w:is_input_valid() then return end

  local ok, err = get_sourcer(w):loadRenamePackageMapping(fn, not clear_prev)
  if not ok then return w:error(err) end
  local cnt = get_pkg_mapping_count(w)
  w:fsay("loaded: %s(%s) from %s", ok, cnt, fn)
end

--
-- open saved mapping in the editor
--
---@param w Cmd4Lua
function M.cmd_open(w)
  local fn = w:desc("filename with rename-pkg-mapping to edit")
      :def(default_mapping_fn):opt("--filename", "-f")

  if not w:is_input_valid() then return end

  vim.cmd(":e " .. v2s(fn))
end

--
-- clear all mappings
--
---@param w Cmd4Lua
function M.cmd_clear(w)
  if not w:is_input_valid() then return end
  local cnt = get_pkg_mapping_count(w)
  get_sourcer(w):clearRenamePackageMapping()
  w:say("cleaned " .. v2s(cnt))
end

--
-- parse the output from a git-status command and add to the mappings
--
---@param w Cmd4Lua
function M.cmd_add_from_git(w)
  if not w:is_input_valid() then return end
  local cnt, err = get_sourcer(w):addRenamePkgMappingFromGitStatus(nil)
  if not cnt then return w:error(err) end
  local total = get_pkg_mapping_count(w)
  w:say("added: " .. v2s(cnt) .. " total:" .. v2s(total))
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

local MUL = {}
--
-- add mapping to update packages for given library
--
---@param w Cmd4Lua
function M.cmd_update_lib(w)
  return w:handlers(MUL)

      :desc('add package mapping to migrate from junit-4 to junit-5')
      :cmd('junit-4-to-5', 'j4t5')

      :run()
end

--
-- add package mapping to migrate from junit-4 to junit-5
-- This functionality was done before the introduction of Junit-4-to-5
-- as a simplified way to fix imports in all test files.
-- But as practice has shown, in addition to correcting the name of the packages,
-- for example, it is necessary to change the print of arguments in assert
-- methods: assertEquals(String, int, int) -> assertEquals(int, int, String)
-- therefore, this functionality can be considered rudiment
--
---@param w Cmd4Lua
function MUL.cmd_junit_4_to_5(w)
  if not w:is_input_valid() then return end

  local pkg_mapping = get_pkg_mapping(w)
  local t = junit5.mapping_update_from_4
  local c = 0
  for k, v in pairs(t) do
    if not pkg_mapping[k] or pkg_mapping[k] ~= v then
      pkg_mapping[k] = v
      c = c + 1
    end
  end
  w:say('added package mappings:', c)
end

return M

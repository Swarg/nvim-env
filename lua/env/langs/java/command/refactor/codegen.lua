-- 24-01-2025 @author Swarg
-- boilerplate source code generations


local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvRefactor generate <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:about('Generate the source (boilerplate) code')
      :handlers(M)

      :desc("generate source code for @Value Class")
      :cmd("value-class", 'vc')

      :desc("generate source code for @Data Class")
      :cmd("data-class", 'dc')

      :run():exitcode()
end

--
--

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
local codegen_handler = require 'env.langs.java.util.codegen.handler'


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local log_debug = log.debug


--
-- Class @Value with final fields, AllArgsConstructor, hasCode, equals
--
---@param w Cmd4Lua
function M.cmd_value_class(w)
  w:about("generate the value-class with final fields,")
      :about(" AllArgsConstructor, hasCode, equals, getters, toString")

  local bufnr = w:desc('bufnr'):opt('--bufnr', '-b')

  if not w:is_input_valid() then return end

  codegen_handler.gen_value_class(bufnr)
end

--
-- @Data Class with ReqArgsConstructor, getters/setters, hasCode, equals, to String
--
---@param w Cmd4Lua
function M.cmd_data_class(w)
  w:about("@Data Class with ReqArgsConstructor, getters/setters, hasCode, ")
  w:about("equals, to String")

  local bufnr = w:desc('bufnr'):opt('--bufnr', '-b')

  if not w:is_input_valid() then return end

  codegen_handler.gen_data_class(bufnr)
end

return M

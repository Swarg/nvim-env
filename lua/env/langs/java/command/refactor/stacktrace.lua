-- 15-02-2025 @author Swarg

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

local M = {}
--
-- :EnvRefactor stack-trace <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)

      :desc('compare two stacktraces until first diff with given direction')
      :cmd('compare', 'c')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

-- local log = require 'alogger'
local ustacktrace = require 'env.langs.java.util.research.stacktrace'
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

-- local get_stacktrace_from_buf = refactor.get_stacktrace_from_buf

---@param w Cmd4Lua
---@return number lnum1 with the head of the first stacktrace
---@return number lnum2 with the head of the second stacktrace
---@return number bufnr1 with the first stacktrace
---@return number bufnr2 with the second stacktrace
local function define_lnum_and_buf(w)
  local ln1, ln2, bn1, bn2
  ln1 = w:desc('lnum of first stack-trace'):pop():argn()
  ln2 = w:desc('lnum of second stack-trace'):pop():argn()
  bn1 = w:desc("bufnr with first stack-trace"):def(0):optn("--bufnr1", "-b1")
  bn2 = w:desc("bufnr with second stack-trace"):def(0):optn("--bufnr2", "-b2")
  ---@cast ln1 number
  ---@cast ln2 number
  ---@cast bn1 number
  ---@cast bn2 number
  bn1 = bn1 or 0
  bn2 = bn2 or 0
  return ln1, ln2, bn1, bn2
end

---@param bufnr number?
---@param ln number?
---@return table? <string> of lines
---@return number the lnum with first StackTraceEntry
local function get_stacktrace_from_buf(bufnr, ln)
  bufnr = bufnr or 0
  ln = ln or 0
  local lnum_end = vim.api.nvim_buf_line_count(bufnr)
  local lines = vim.api.nvim_buf_get_lines(bufnr, ln, lnum_end, true)
  -- first line with Exception class + message
  if (lines == nil or #lines < 1) then
    return nil, -1
  end
  local t = {}
  -- it can be class + message of the Throwable|Exception
  if match(lines[1], '^%s*at%s+') then
    t[1] = lines[1]
  else
    ln = ln + 1
  end
  -- find the end of the stacktrace
  for i = 2, #lines do
    if match(lines[i], '^%s*at%s+') == nil then
      break
    end
    t[#t + 1] = lines[i]
  end

  return t, ln
end


--
-- compare two stacktraces unitl the first difference
--
---@param w Cmd4Lua
function M.cmd_compare(w)
  local ln1, ln2, bufnr1, bufnr2 = define_lnum_and_buf(w)
  w:desc("direction to compare [b2t|t2b] (from buttom-to-top or from top-to-bottom)")
      :def("b2t"):v_opt("--direction", "-d")

  if not w:is_input_valid() then return end

  if (ln1 ~= nil and (ln1 == ln2 and bufnr1 == bufnr2)) then
    return w:error("Two different places with stacktraces are expected."
      .. " But you pointed twice the same stacktace.")
  end
  local direction = w.vars.direction
  local dcode = ustacktrace.get_compare_direction_code(direction)
  if not dcode then
    return w:error("unknown direction " .. v2s(direction))
  end

  local stacktrace1, ln1start = get_stacktrace_from_buf(w.vars.bufnr1, ln1)
  local stacktrace2, ln2start = get_stacktrace_from_buf(w.vars.bufnr2, ln2)

  local res, err = ustacktrace.compare_ufd(stacktrace1, stacktrace2, direction)
  if not res then
    return w:error(err)
  end
  if res.equals then
    return w:say("two stacktraces are equal")
  end
  if not res.same_at then
    return w:error('broken results')
  end

  -- local fi1 = res.same_at.from1
  -- local fi2 = res.same_at.from2
  local ti1 = res.same_at.to1
  local ti2 = res.same_at.to2

  if dcode == ustacktrace.DIRECTION_CODE_BUTTOM_TO_TOP then
    w:fsay('found diff at stacktrace1: %s stacktrace2: %s',
      (ln1start + ti1), (ln2start + ti2))
  elseif dcode == ustacktrace.DIRECTION_CODE_TOP_TO_BUTTOM then
    w:fsay('found diff at stacktrace1: %s stacktrace2: %s',
      (ln1start + ti1), (ln2start + ti2))
  end
end

--
return M

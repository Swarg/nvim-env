-- 09-01-2025 @author Swarg
--

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------
-- :EnvRefactor class <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)

      :desc('show the hierarchy of class inheritance')
      :cmd("inheritance-hierarchy", 'ih')

      :desc('copy the full name of the current opened class file to clipboard')
      :cmd("full-name", 'fn')

      :desc('create a reference to the source code in the current opened buffer')
      :cmd("mk-ref", 'mr')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local ucb = require 'env.util.clipboard'
local api_constants = require 'env.lang.api.constants'
local eclipse = require 'env.bridges.eclipse'
local E, v2s = {}, tostring

local LT = api_constants.LEXEME_TYPE

--
-- show the hierarchy of class inheritance
--
---@param w Cmd4Lua
function M.cmd_inheritance_hierarchy(w)
  local cn = w:desc('classname'):optional():pop():arg();

  if not w:is_input_valid() then return end
  ---@cast cn string

  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang

  if not cn or cn == '.' then
    local path, _, lt = lang:resolveDefinition()
    if not path then
      return w:error('not found definition for lement under the cursor')
    end
    if lt == LT.CLASS or lt == LT.TYPE or lt == LT.ANNOTATION then
      cn = lang:getClassName(path)
    end
  end

  if not cn then return w:error('not found classname') end

  local t = {}

  local success, err = lang:getClassInheritanceHierarchy(cn, t)
  if not success and #t == 0 then return w:error(err) end

  ucb.copy_to_clipboard(table.concat(t, "\n"))
  print('copied to system clipboard')

  if not success and err then print(err) end -- if not all founed
end

--
-- copy the full name of the current opened class file
--
---@param w Cmd4Lua
function M.cmd_full_name(w)
  w:v_opt_verbose('-v')

  if not w:is_input_valid() then return end

  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang
  local path, classname = nil, nil
  if lang:getProjectRoot() then
    path = vim.api.nvim_buf_get_name(0)
    classname = lang:getClassName(path); --:getSourcer():getParserClass
    w:verbose('clazz by path:' .. tostring(classname))
  end

  -- jdt://
  if not classname or classname == '' then
    local clazz = lang:getClassDefintionInOpenedBuff()
    if clazz and clazz.name and clazz.pkg then
      classname = tostring(clazz.pkg) .. '.' .. tostring(clazz.name)
    end
    w:verbose('parsed clazz:' .. type(clazz), classname)
  end

  if classname ~= nil then
    w:say('copied: ' .. tostring(classname))
    ucb.copy_to_clipboard(classname)
  else
    w:error('cannot figureout class name for buffer ' .. tostring(path))
  end
end

--
-- create a reference to the source code in the current opened buffer
--
---@param w Cmd4Lua
function M.cmd_mk_ref(w)
  if not w:is_input_valid() then return end
  local path = vim.api.nvim_buf_get_name(0)
  local t, err = eclipse.parse_jdt_path(path)
  if not t then
    return w:error(err)
  end
  local path2jar = t.path
  local classname = eclipse.get_classname_from_parsed_jdt(t)
  local currline = vim.api.nvim_get_current_line()
  local lnum = (vim.api.nvim_win_get_cursor(0) or E)[1]
  local txt = v2s(path2jar) .. "\n" .. v2s(classname) .. "\n"
      .. currline .. " // " .. v2s(lnum)

  w:say('copied: ' .. v2s(classname) .. " " .. v2s(lnum))
  ucb.copy_to_clipboard(txt)
end

return M

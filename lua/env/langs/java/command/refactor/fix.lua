-- 18-01-2025 @author Swarg
--

local fix_junit = require 'env.langs.java.command.refactor.fix.junit'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvRefactor fix <subcmd>
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('fix the package by taking the correct package from the path to file')
      :cmd("package", 'p')

      :desc('add parametrization for raw types like Class and ArrayList')
      :cmd("raw-types", 'rt')

      :desc('appy package-mapping for the imports (with support for all sources)')
      :cmd("imports", 'i')

      :desc('fix issues with junit migration from old version to 5th')
      :cmd("junit", 'ju')

      :run()
      :exitcode()
end

---@param w Cmd4Lua
function M.cmd_junit(w) return fix_junit.handle(w) end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
local fs = require 'env.files'
local ufmt = require 'env.langs.java.util.formatting'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


---@param lines table
---@param lnum number? pass nil to update the current line
local function update_line(lines, lnum)
  lnum = lnum or (vim.api.nvim_win_get_cursor(0) or E)[1]
  vim.api.nvim_buf_set_lines(0, lnum - 1, lnum, true, lines)
end

---@param w Cmd4Lua
---@return env.langs.java.JLang
local function get_lang(w)
  local gen = w:var('gen') ---@type env.lang.oop.LangGen
  local lang = gen:getLang() ---@cast lang env.langs.java.JLang
  return lang
end


--
-- fix the package of the given class by taking the correct package from
-- the path to file.
--  - with fix the packages of the imported classes
--
---@param w Cmd4Lua
function M.cmd_package(w)
  local path = w:desc('path to class file(or dir of package) to fix package')
      :optional():pop():arg()

  if not w:is_input_valid() then return end
  ---@cast path string
  if not path or path == '.' then
    path = vim.api.nvim_buf_get_name(0)
  end

  -- ./ or ./.. or ./../..  etc
  if path and path:sub(1, 2) == './' then
    local dir = fs.get_parent_dirpath(vim.api.nvim_buf_get_name(0))
    path = fs.build_path(dir, path)
    if not fs.dir_exists(path) then
      return w:error('not exists dir: ' .. v2s(path))
    end
  end

  local ok, err = get_lang(w):getSourcer():fixPackageInClassFiles(path)
  if not ok then
    log.debug("cmd fix package:", err)
    return w:error(err)
  end
  vim.cmd(':e') -- update current buffer
end

--
-- appy package-mapping for the imports (with support for all sources
--
-- for example, to switch from junit4 to junit5
--
---@param w Cmd4Lua
function M.cmd_imports(w)
  w:usage('EnvRefactor fix imports --all-test --all-main')
  w:usage('EnvRefactor fix imports # for current source file only')
  w:usage('To setup mapping use `EnvRefactor package-mapping add`')

  w:desc('for all tests   in src/test/'):v_has_opt('--all-test', '-t')
  w:desc('for all sources in src/main/'):v_has_opt('--all-main', '-m')

  if not w:is_input_valid() then return end

  -- all sources files in src/main/ or(and) src/test/
  if w.vars.all_test or w.vars.all_main then
    local cnt, err = get_lang(w):getSourcer():updateAllImports(w.vars)
    if not cnt then
      log.debug("cmd fix all-imports:", err)
      return w:error(err)
    end
    vim.cmd(':e') -- update current buffer
    w:say('updated files: ' .. v2s(cnt))
    return
  end

  local path = vim.api.nvim_buf_get_name(0)
  local ok, err = get_lang(w):getSourcer():fixImportsInClassFile(path)
  if not ok then return w:error(err) end
  vim.cmd(':e') -- update current buffer
end

--
-- EnvRefactor fix raw-types
-- add parametrization for raw types like Class and ArrayList
--
---@param w Cmd4Lua
function M.cmd_raw_types(w)
  if not w:is_input_valid() then return end

  local line = vim.api.nvim_get_current_line()

  local altered_line = ufmt.fix_raw_types(line)

  update_line({ altered_line }, nil) -- update current line
end

return M

-- 19-01-2025 @author Swarg
--
--  interact with name mappings (deobf mapping or to fix package names
--
--  it can be used to restore original method and field names from
--  deobfuscaded sourcecode
--

local fs = require 'env.files'
local umapping = require 'env.langs.java.util.name_mapping'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

function M.cmd_name_mapping(w)
  w:handlers(M)

      :desc('load the name mappings from given directory or file')
      :cmd("load", 'l')

      :desc('show info about current loaded name mappings')
      :cmd("status", 'st')

      :desc('clear current loaded name mappings')
      :cmd("clear", 'cl')

      :desc('find mapping for given name')
      :cmd("map", 'm')

      :desc('apply name mapping for given sources')
      :cmd("apply-for-sources", 'afs')

      :run()
end

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local apply_name_mapping_to_dir = umapping.apply_name_mapping_to_dir

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

-- state for hot reloading
-- _G.name_mappings = _G.name_mappings


---@param w Cmd4Lua
---@param dir string
local function load_mapping(w, dir)
  local mapping, err, namescnt = umapping.load_csv_mappings(dir)
  if not mapping then
    return w:error(v2s(err))
  end
  w:say('Names in mapping:' .. v2s(namescnt))
  _G.name_mappings = mapping
end

--
-- load the name mappings from given directory or file
--
---@param w Cmd4Lua
function M.cmd_load(w)
  w:v_opt_verbose('-v')
  local mapping_dir = w:desc('directory with mappings'):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast mapping_dir string
  load_mapping(w, mapping_dir)
end

--
-- show info about current loaded name mappings
--
---@param w Cmd4Lua
function M.cmd_status(w)
  if not w:is_input_valid() then return end
  if type(_G.name_mappings) ~= 'table' then
    w:say('empty')
    _G.name_mappings = nil
    return
  end

  local s = ''
  local map = _G.name_mappings
  for section, mapping in pairs(map) do
    local c = 0
    for _, _ in pairs(mapping) do c = c + 1 end
    s = s .. fmt(" %s: %s", v2s(section), v2s(c))
  end
  w:say(s)
end

--
-- clear current loaded name mappings
--
---@param w Cmd4Lua
function M.cmd_clear(w)
  if not w:is_input_valid() then return end
  _G.name_mappings = nil
end

--
-- find mapping for given name
--
---@param w Cmd4Lua
function M.cmd_map(w)
  w:v_opt_verbose('-v')
  local oldname = w:desc('name to map'):optional():pop():arg()
  local do_replace = w:desc('with replace'):has_opt('--replace', '-r')

  if not w:is_input_valid() then return end
  ---@cast oldname string

  if not _G.name_mappings then
    return w:error('no loaded name_mappings')
  end

  -- local from_buff
  if oldname == '.' or oldname == nil then
    local gen = w:var('gen') ---@type env.lang.oop.LangGen
    local ctx = gen:getEditor():getContext(true):resolveWordSimple('[%w_]')
    oldname = ctx.word_element
    -- from_buff = true
  end

  local t = _G.name_mappings
  local new_name = (t.fields or E)[oldname] or
      (t.methods or E)[oldname] or
      (t.params or E)[oldname]

  if new_name and do_replace then --from_buff then
    local replace_cmd = "%s/" .. oldname .. '/' .. new_name .. '/g'
    w:verbose(replace_cmd)
    vim.cmd(replace_cmd)
  end

  w:say(new_name)
end

--------------------------------------------------------------------------------

--
-- apply name mapping for given sources
-- by default if no args is given when apply only for current opened file
--
---@param w Cmd4Lua
function M.cmd_apply_for_sources(w)
  w:v_opt_verbose('-v')
  local dir = w:desc('directory with sources'):optional():pop():arg()
  w:desc('with subdirs'):v_has_opt('--recursive', '-r')

  local mappingdir = w:desc('directory with mappings(fields.csv,methods.csv)')
      :opt('--mapping-dir', '-m')


  if not w:is_input_valid() then return end
  --
  local gen = w:var('gen') ---@type env.lang.oop.LangGen

  if not dir then
    dir = gen:getEditor():getCurrentFile()
    w:verbose('apply for current opened file: ' .. v2s(dir))
  elseif dir == '.' then -- for all files in same dir as the current one
    dir = fs.get_parent_dirpath(gen:getEditor():getCurrentFile())
    w:verbose('apply all in the directory of current file: ' .. v2s(dir))
  end

  if mappingdir then
    load_mapping(w, mappingdir)
    if w:has_error() then return end
  end
  ---@cast dir string

  local report, err = apply_name_mapping_to_dir(dir, _G.name_mappings, w.vars)
  if not report then
    return w:error(v2s(err))
  end

  -- updated opened files
  if type(report) == 'table' and report.files then
    for _, file_entry in ipairs(report.files) do
      if file_entry.success and file_entry.path then
        vim.cmd('e! ' .. v2s(file_entry.path))
      else
        w:fsay("%s: %s", file_entry.path, file_entry.error)
      end
    end
  end

  w:say('errors:' .. v2s(report.errors))
  w:say(require 'inspect' (report))
end

return M

-- 13-01-2025 @author Swarg
--
-- EnvRefactor add <subcmd>
--  - source-file  - add external source file from another project
-- Deps:
--   used rename_pkg_mapping

local M, MD = {}, {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvRefactor add <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:handlers(M)


      :desc("add source file from another project to the current one")
      :cmd("source-file", 'sf')

      :desc("add source class from setuped source-dir into current project")
      :cmd("source-class", 'sc')

      :desc("interact with a directory from which the source files will be taken")
      :cmd("source-root-dir", 'srd')

      :desc("generate line with loggin-message depends of currline context")
      :cmd("loggin-message", 'lm')

      :run()
      :exitcode()
end

--
-- intefact with the directory where the source files will be taken from
-- (for source-class cmd)
--
---@param w Cmd4Lua
function M.cmd_source_root_dir(w)
  w:handlers(MD)
      :desc('show current setuped root directory of the sources')
      :cmd('get', 'g')

      :desc('set a current root directory of sources')
      :cmd('set', 's')

      :desc('edit current root directory of sources')
      :cmd('edit', 'e')

      :desc('forget setuped root directory of sources')
      :cmd('clean', 'c')

      :run()
end

--

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'
local fs = require 'env.files'
local Editor = require 'env.ui.Editor'
local JParser = require 'env.langs.java.JParser'
local ui = require 'env.langs.java.util.ui'
local cmd_logmessage = require 'env.langs.java.command.refactor.add.loggin_message'

local src_directory = nil

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local log_debug = log.debug
local get_path_from_clipboard = ui.get_path_from_clipboard
local normalize_path = ui.normalize_path

function M.cmd_loggin_message(w) return cmd_logmessage.handle(w) end

--
--



--
-- add source file from another project to the current one
--
-- for example, for cutting and transferring class files from one library to
-- another
--
---@param w Cmd4Lua
function M.cmd_source_file(w)
  local path = w:desc('absolute path to the source file'):pop():arg()
  w:desc('use package and class name from source file to restore inner_path')
      :v_has_opt('--fix-pkg', '-F')

  if not w:is_input_valid() then return end
  ---@cast path string

  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang

  if path == 'cb' then          -- pickup path from clipboard
    path = get_path_from_clipboard(w)
    if not path then return end -- with err msg
  end
  ---@cast path string

  local new_path, err = lang:addExternalSourceFile(path, w.vars)
  if not new_path then return w:error(err) end
  w:say('done: ' .. v2s(new_path))
end

--------------------------------------------------------------------------------


--
-- show current setuped directory
--
---@param w Cmd4Lua
function MD.cmd_get(w)
  if not w:is_input_valid() then return end
  w:say(src_directory)
end

--
-- set a current directory of sources
--
---@param w Cmd4Lua
function MD.cmd_set(w)
  local dir = w:desc():pop():arg()

  if not w:is_input_valid() then return end
  src_directory = nil
  dir = normalize_path(dir)
  if not fs.dir_exists(dir) then
    return w:error("directory not exists!" .. v2s(dir))
  end
  src_directory = dir
end

--
-- forget setuped directory of sources
--
---@param w Cmd4Lua
function MD.cmd_clean(w)
  if not w:is_input_valid() then return end
  src_directory = nil
end

--
-- edit current directory of sources
--
---@param w Cmd4Lua
function MD.cmd_edit(w)
  if not w:is_input_valid() then return end
  local dir = Editor.askValue("update source-dir: ", src_directory or '')
  if not dir or dir == '' or dir == 'q' then
    return w:error('canceled')
  end
  src_directory = dir
  w:say("updated to " .. v2s(src_directory))
end

--

--
-- add source class from setuped source-dir into current project
--
---@param w Cmd4Lua
function M.cmd_source_class(w)
  local fqcn = w:desc("the fully qualifed class name"):optional():pop():arg()
  if not w:is_input_valid() then return end
  ---@cast fqcn string

  if not src_directory then
    return w:error("source directory is not defined!")
  end
  if not fs.dir_exists(src_directory) then
    return w:error("not found source directory: " .. src_directory)
  end

  if not fqcn or fqcn == '.' or fqcn == 'import' then
    fqcn = JParser.parseImportLineToFQCN(Editor.getCurrentLine())

    if not fqcn or fqcn == '' then
      w:error("cannot pick classname from the current line (import ...;)")
    end
  end

  local inner_path = string.gsub(fqcn or '', "%.", fs.path_sep) .. ".java"
  local path = fs.join_path(src_directory, inner_path)

  if not fs.file_exists(path) then
    return w:error("not found file: " .. v2s(path) .. " for class: " .. v2s(fqcn))
  end

  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang
  local new_path, err = lang:addExternalSourceFile(path, w.vars)
  if not new_path then return w:error(err) end
  w:say('done: ' .. v2s(new_path))
end

return M

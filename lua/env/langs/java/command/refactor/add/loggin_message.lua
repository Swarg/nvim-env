-- 03-02-2025 @author Swarg
-- generate lines with logging message

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- :EnvRefactor add logging-message <subcmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:about('Generate the source (boilerplate) code')
      :handlers(M)

      :desc("new line with log.debug")
      :cmd("debug", 'd')

      :run():exitcode()
end

--
--

--------------------------------------------------------------------------------
--                            IMPLEMENTATION
--------------------------------------------------------------------------------

local log = require 'alogger'


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
---@diagnostic disable-next-line: unused-local
local log_debug = log.debug


--
-- new line with log.debug
--
---@param w Cmd4Lua
function M.cmd_debug(w)
  if not w:is_input_valid() then return end

  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang
  local ctx = lang:getEditor():getContext(true):resolveWords()
  local method_name = lang.gen:getMethodUnderUICursor()
  local line = '        log.debug("';
  if method_name then
    line = line .. method_name .. '");';
    -- todo params from the method
  elseif ctx.word_element then
    local pref = ctx.word_container ~= nil and (ctx.word_container .. '.') or ''
    local vn = pref .. ctx.word_element;
    line = line .. vn .. ':{}", vn);'
  else
    line = line .. '");'
  end
  ctx:insertAfterCurrentLine(line)
end

return M

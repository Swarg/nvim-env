-- 01-03-2025 @author Swarg
--

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvNew pojo open-api<cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  return w:about('Generate POJO for OpenAPI componebt schemas')
      :handlers(M)

      :desc('open OpenAPI file with schemas')
      :cmd("open", 'o')

      :desc('close already open OpenAPI file')
      :cmd("close", 'c')

      :desc('open node with given full key name(path) (aka $ref) in the opened yml file')
      :cmd("open-key", 'ok')

      :desc('generate POJO for schema defined in given OpenAPI file')
      :cmd("generate", 'g')

      :desc('interact with package used for POJOs generation')
      :cmd("package", 'p')

      :desc('settings for code generation')
      :cmd("settings", 's')

      :run()
      :exitcode()
end

local M_PKG = {}
function M.cmd_package(w)
  return w
      :handlers(M_PKG)

      :desc('set the package name for generated POJOs')
      :cmd("set", 's')

      :desc('show current package used for POJOs generation')
      :cmd("get", 'g')

      :run()
end

local M_CFG = {}
--
-- settings for code generation
--
---@param w Cmd4Lua
function M.cmd_settings(w)
  return w
      :handlers(M_CFG)

      :desc('generate records(java16+) instead of regular java classes with')
      :cmd("records", 'r')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local state = {
  opened_yml = nil,
  opened_path = nil,
  pkg = nil,
  settings = {
    records = true,
  }
}

function M.dump_state()
  _G.pojo_open_api_state = state
end

function M.restore_state()
  state = _G.pojo_open_api_state
  _G.pojo_open_api_state = nil
end

-- local log = require 'alogger'
local fs = require 'env.files'
local lyaml = require "lyaml"
local uyaml = require 'env.util.yaml'
local Editor = require 'env.ui.Editor'
local CodeGen = require 'env.langs.java.util.openapi.CodeGen'
local ui = require 'env.langs.java.util.ui'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

-- local join_path = fs.join_path
local get_path_from_clipboard = ui.get_path_from_clipboard
local get_compoment_key_from_clipboard
--
---@param w Cmd4Lua
---@return env.langs.java.JLang
local function get_lang(w)
  local lang = w:var('gen'):getLang() ---@cast lang env.langs.java.JLang
  return lang
end

--
--
---@param w Cmd4Lua
local function ask_pkg(w)
  local path = Editor.getCurrentFile()
  local pkg0 = match(get_lang(w):getClassName(path) or '', '^(.-)%.([%w%$]+)$')
  local pkg = Editor.askValue("Package for generated models: ", pkg0)
  if not pkg or pkg == 'q' then
    w:error('canceled')
    return nil
  end
  state.pkg = pkg -- remember
  return pkg
end

--
-- set the package name for generated POJOs
--
---@param w Cmd4Lua
function M_PKG.cmd_set(w)
  local pkg = w:desc('package'):optional():pop():arg()

  if not w:is_input_valid() then return end

  if pkg == nil or pkg == '.' then
    pkg = ask_pkg(w)
    if not pkg then return end -- canceled
  end
  state = state or {}
  state.pkg = pkg
  w:say('package: ' .. v2s(pkg))
end

--
-- show current package used for POJOs generation
--
---@param w Cmd4Lua
function M_PKG.cmd_get(w)
  if not w:is_input_valid() then return end
  w:say('package: ' .. v2s((state or E).pkg))
end

-- settings

--
-- generate records(java16+) instead of regular java classes with
--
---@param w Cmd4Lua
function M_CFG.cmd_records(w)
  local flag = w:desc('generate the records instead regular classes')
      :optional():pop():argb()

  if not w:is_input_valid() then return end
  if flag ~= nil then
    state.settings.records = flag == true
  end
  w:say('records: ' .. v2s(state.settings.records))
end

--

--
--
---@param w Cmd4Lua
---@param yml_file string
---@return table? parsed yml
local function open_yml(w, yml_file)
  if state.opened_yml ~= nil then
    w:error('close previous opened yml file before. Opened file: '
      .. v2s(state.opened_path))
  end

  if not fs.file_exists(yml_file) then
    return w:error('not found ' .. v2s(yml_file))
  end
  w:say('opening yml file...')
  state.opened_yml = lyaml.load(fs.read_all_bytes_from(yml_file), { all = false })
  state.opened_path = yml_file

  w:say('yml file ' .. v2s(yml_file) .. type(state.opened_yml) .. ' is opened')

  return state.opened_yml
end

--
-- open OpenAPI file with schemas
--
---@param w Cmd4Lua
function M.cmd_open(w)
  local yml_file = w:desc('OpenAPI yml file with api-schemas'):pop():arg()

  if not w:is_input_valid() then return end ---@cast yml_file string

  open_yml(w, yml_file)
end

--
-- close already open OpenAPI file
--
---@param w Cmd4Lua
function M.cmd_close(w)
  if not w:is_input_valid() then return end
  if state.opened_yml == nil then
    return w:error('no opened yml file')
  end
  state = {}
  w:say('closed')
end

--
-- get the contents of the given key(path) inside opened yml file
--
---@param w Cmd4Lua
function M.cmd_open_key(w)
  local key = w:desc('key'):pop():arg()

  if not w:is_input_valid() then return end
  ---@cast key string

  if type(state) ~= 'table' or not state.opened_yml then
    state = {}
    return w:error('no opened yml file')
  end
  local node, err, _ = uyaml.open_node(state.opened_yml, key)
  if not node then
    return w:error(err or 'cannot open node with given key')
  end
  w:say('opened node:' .. v2s(key) .. "\n" .. require "inspect" (node))
end

--
-- Generate POJO from OpenAPI yaml file
-- for example to generate POJO from
-- https://github.com/cloudflare/api-schemas/blob/main/openapi.yaml
--
---@param w Cmd4Lua
function M.cmd_generate(w)
  w:v_opt_dry_run('-d')
  w:usage("EnvNew pojo open-api generate cb   # pick api-component key from clipboard")

  local keys = w:desc('full key names of the API compoment to generate'):pop():argl()
  local yml_file = w:desc('OpenAPI yml file with api-schemas'):opt('--file', '-f')
  local pkg = w:desc('java package for generated POJO classes'):opt('--package', '-p')
  w:desc('generate java records instead classes'):v_has_opt('--records', '-r')
  w:desc('rewrite already existed classes'):v_has_opt('--force-rewrite', '-w')

  if not w:is_input_valid() then return end ---@cast keys table

  pkg = pkg or state.pkg

  if yml_file == 'cb' then
    yml_file = get_path_from_clipboard(w)
    if not yml_file then return end -- with err msg
  end
  if pkg == nil or pkg == '.' then
    pkg = ask_pkg(w)
    if not pkg then return end -- canceled
  end
  if yml_file then
    if not open_yml(w, yml_file) then return end -- with errmsg
  end
  if keys == 'cb' or keys[1] == 'cb' or keys[1] == 'clipboard' then
    keys = get_compoment_key_from_clipboard(w)
  end

  local lang = get_lang(w)
  local yml = state.opened_yml ---@cast yml table
  local opts = {
    dry_run = w:is_dry_run(),
    force_rewrite = w.vars.force_rewrite,
    as_records = w.vars.records or state.settings.records,
    required_only = false,
    nested_classes = false, -- for nested object structure
  }

  if not yml then
    return w:error("you should open yml file first")
  end

  local o, err = CodeGen.generate_classes(yml, keys, pkg, lang, opts)
  if not o then
    return w:error(err)
  end

  if o.report and #o.report > 0 then
    local dir = lang:getProjectRoot()
    for n, e in ipairs(o.report) do
      local fqcn, path = e[1], e[2]
      if path:sub(1, #dir) == dir then
        path = path:sub(#dir + 1)       -- relative inner-paths
      end
      w:fsay('%s %s %s', n, fqcn, path) -- fqcn + path to generated file
    end
  end
  if #(state.errors or E) > 0 then
    w:say('errors:' .. require "inspect" (state.errors))
  end
end

-- helpers

-- #/compoment/schemas/...
---@return table? of string
---@param w Cmd4Lua
function get_compoment_key_from_clipboard(w)
  local s = vim.fn.getreg("+")
  if not s or s:sub(1, 2) ~= '#/' then
    s = vim.fn.getreg("*")
    if not s or s:sub(1, 2) ~= '#/' then
      return w:error('no compoment key in system clipboard')
    end
  end

  local keys = {}
  for line0 in s:gmatch("([^%s]*)%s?") do
    if line0 and line0 ~= '' then
      table.insert(keys, line0)
    end
  end
  return keys
end

return M

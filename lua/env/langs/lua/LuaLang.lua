-- 25-10-2023 @author Swarg
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')
local base = require 'env.lang.utils.base'
local Busted = require("env.langs.lua.Busted")
local LuaGen = require("env.langs.lua.LuaGen")
local CodeCheck = require("env.langs.lua.CodeCheck")
local LuaRocksBuilder = require("env.langs.lua.LuaRocksBuilder")
local LuaTestDiagnost = require 'env.langs.lua.LuaTestDiagnost'
local LuaTester = require 'env.langs.lua.LuaTester'

local Lang = require('env.lang.Lang')
---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")


class.package 'env.langs.lua'
--- define class LuaLang extends Lang
---@class env.langs.lua.LuaLang : env.lang.Lang
---@field new fun(self, o:table?): env.langs.lua.LuaLang
---@field getLangGen fun(self): env.langs.lua.LuaGen
---@field executable      string   bin of the lang interprater
---@field namespace_map   table    created by LuaRocksBuilder TODO
---@field modules_test_namespaces table - created after findTestFramework() TODO
---@field src string
---@field test string
---@field last_lsp_update number   time of a setup settings (os.time())
--                                 used to update already exists settings only
--                                 when rockspec file was changed
--
local LuaLang = class.new_class(Lang, 'LuaLang', {
  ext = 'lua',                                  -- extension of source files
  executable = 'lua',
  root_markers = { '.git', '.busted', 'spec' }, -- extension .rockspec dir rockspecs
  -- Note: there must be a slash at the end of the path
  src = 'src' .. fs.path_sep,
  test = 'spec' .. fs.path_sep,
  test_suffix = '_spec',
})

Lang.register(LuaLang)

local E = {}
local log_debug, log_trace = log.debug, log.trace
local join_path = fs.join_path

-- local dprint = require("dprint").mk_log_debug_for(LuaLang)

--------------------------------------------------------------------------------
--                                 INIT
--------------------------------------------------------------------------------

-- Lang:resolve for given filename resolve project properties:
-- project_root, builder, testframework
---@param fn string filename
---@return boolean
function LuaLang.isLang(fn)
  if fn then
    return fn:match('%.lua$') ~= nil or fn:match('%.rockspec$') ~= nil
  end
  return false
end

-- used at Lang:resolve
---@param filename string?
---return self
function LuaLang:findProjectBuilder(filename)
  ---@diagnostic disable-next-line: undefined-field
  log_debug("findProjectBuilder %s", (self or E).project_root)
  self.builder = nil
  local userdata = {} -- to pass src, test, project_root

  if LuaRocksBuilder.isProjectRoot(self.project_root, userdata) then
    self.builder = LuaRocksBuilder:new(userdata)
    self.builder:getSettings()
    --
  elseif LuaTester.isProjectRoot(self.project_root, userdata, filename) then
    self.builder = LuaTester:new(userdata)
    self.builder:getSettings()
    --
  else
    log_debug('[WARN] No LuaRocks for', self.project_root)
    return self
  end

  return self:syncSrcAndTestPathsWithBuilder() -- sync Lang and Builder
end

-- used at Lang:resolve
function LuaLang:findTestFramework()
  self.testframework = nil

  if self.project_root then
    local script = fs.join_path(self.project_root, '.busted')
    if fs.file_exists(script) then
      self.testframework = Busted:new():setProjectRoot(self.project_root)
      self.testframework:getConfig()
    end
  end
  return self
end

-- aka namespace
-- function LuaLang:getModulesMap()
--   if not self.namespace_map and self.builder and self.builder.getModulesMap then
--     -- from .rockspec
--     self.namespace_map = self.builder:getModulesMap() or {}
--   end
--   return self.namespace_map
-- end


--------------------------------------------------------------------------------
--                        TEST FRAMEWORK
--------------------------------------------------------------------------------

---@param ns string
---@return string?, string?  -  testsuite_name, test_path
function LuaLang:getTestSuiteName(ns)
  local tfw = self:getTestFramework()
  if tfw and class.instanceof(tfw, Busted) then
    return tfw:findTestSuiteNameFor(ns)
  end
end

--------------------------------------------------------------------------------

-- inner_path is a path inside the project without project_root and extension
-- in modular project it src + modulename + namespace
-- in lua src dir does not maps to namespace
--
---@param ns string? -- the namespace or innerpath (without root and extendsion)
--- override
function LuaLang:getClassNameForInnerPath(ns)
  if ns then
    local pref = ''
    if su.starts_with(ns, self:getSrcPath()) then
      pref = self:getSrcPath()
    elseif su.starts_with(ns, self:getTestPath()) then
      pref = self:getTestPath()
    end

    local cn = ns:sub(#pref + 1)
    return fs.path_to_classname(cn, '%.') -- class_name
  end
  return ns
end

-- TODO use rockspec with:
--   build.modules.mypkg ="src/mypkg/init.lua"
--                 ^^^^^ package name(module or classfile)
--- override
---@param cn string -- classname
---@return string
function LuaLang:getInnerPathForClassName(cn)
  if cn then
    if su.ends_with(cn, self.test_suffix) then
      -- TODO throught Busted
      -- cn = self.test_suffix .. '.' ..
      return fs.join_path(self.test, fs.classname_to_path(cn, '%.'))
    elseif self.builder then
      local cn0 = self.builder:findInnerPathForModule(cn)
      if cn0 then
        return cn0
      end
    end

    local path = fs.join_path(self:getSrcPath(), fs.classname_to_path(cn, '%.'))
    return path -- relative path in the project
  end
  return cn
end

--- override
---@param filename string
---@return boolean
function LuaLang:isSrcPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    return su.starts_with(ns, self:getSrcPath())
  end
  return false
end

--- override
---@param filename string
---@return boolean|nil
function LuaLang:isTestPath(filename)
  local ns = self:getInnerPath(filename)
  if ns then
    if self:hasTestFramework() then
      return self:getTestSuiteName(ns) ~= nil
    else
      return su.starts_with(ns, self:getTestPath())
    end
  end
end

-- check is a class are test class from some configured testsuite path
-- if so then pass testsuite path and name into classinfo
--
-- Lookup goes through InnerPath
--
-- findout test suites for given inner-path(namespace)
-- check is classinfo from some testsuite namespace (test-class)
-- then addd testsuite_name and testsuite_path into classinfo
---@param classinfo env.lang.ClassInfo
---@return boolean
function LuaLang:lookupTestSuite(classinfo)
  local ns = classinfo:getInnerPath()
  log_debug("lookupTestSuite inner:", ns)
  if ns then
    local testsuite_name, testsuite_path

    if self:hasTestFramework() then
      log_trace("take testsuite from TestFramework")
      testsuite_name, testsuite_path = self:getTestSuiteName(ns)
      --
    elseif su.starts_with(ns, self.test) then
      log_trace("set default testsuite")
      testsuite_name, testsuite_path = 'unit', self.test -- ?
    end

    if testsuite_name and testsuite_path then
      classinfo:setTestSuite(testsuite_name, testsuite_path)
      return true
    end
  end

  log_trace("classinfo is not a test class")
  return false
end

--
-- a simple and straightforward classname mappings: one source to one test
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- source-class: app.module.init
-- return      : app.module.init_spec
--- override
---@param src_classname string
function LuaLang:getTestClassForSrcClass(src_classname)
  local tcn = Lang.getWithSuffix(src_classname, self.test_suffix)
  log_debug("getTestClassForSrcClass scn:", src_classname, self.src, tcn)
  return tcn
end

--
-- a simple and straightforward classname mappings: one test to one source
-- without searching in subdir (one source file - many tests files in SubDir)
--
-- test-class: app.module.init_spec
-- return    : app.module.init
--
--- override
---@param test_classname string
function LuaLang:getSrcClassForTestClass(test_classname)
  local scn = Lang.getPathWithoutSuffix(test_classname, self.test_suffix)
  log_debug('getSrcClassForTestClass', test_classname, scn)
  return scn
end

--------------------------------------------------------------------------------
--                            USE CASES
--------------------------------------------------------------------------------
-- See env.lang.Lang.lua

--
--
-- Read dependencies from rockspec-file and add full-paths for modules into
-- workspace.library
--
-- triggers in on_attach to lsp server
--
-- triggers update only if either the values have not been set at all yet, or
-- if the rockspec-config file has been changed
--
---@param opts table {config.settings.Lua.workspace.library}
---@return self
function LuaLang:updateLspSettings(opts)
  log_debug("updateLspSettings workspace.library", self.project_root)
  assert(type(opts) == 'table', 'conf')

  if not self.builder then
    log_debug('No Builder(LuaRockBuilder)')
    return self
  end
  local builder = self.builder
  ---@cast builder env.langs.lua.LuaRocksBuilder

  if not builder:isBuildScriptChanged(self.last_lsp_update) then
    log_debug('lsp settings already updated')
    return self
  end

  -- config settings Lua workspace library
  opts.config = opts.config or {}
  opts.config.settings = opts.config.settings or {}
  local s = opts.config.settings
  s.Lua = s.Lua or {}
  s.Lua.workspace = s.Lua.workspace or {}
  local ws = s.Lua.workspace
  ws.library = ws.library or {}

  if not (s.Lua.telemetry or E).enable then
    s.Lua.telemetry = { enable = false }
  end

  local wslibs = builder:getWorkspaceLibs()

  local pkgs, paths_added = 0, 0

  log_trace('add modules paths into workspace.library for lsp:')
  for _, modules in pairs(wslibs) do
    if modules then
      pkgs = pkgs + 1
      for k, fn in pairs(modules) do
        log_trace(k, fn)
        if k ~= 'rockspec' then -- field with rockspec.path and mtime (cache)
          ws.library[fn] = true
          paths_added = paths_added + 1
        end
      end
    end
  end

  log_debug('dependencies:%s paths_added:%s', pkgs, paths_added)

  self.last_lsp_update = os.time()

  return self
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function LuaLang:getArgsToRunSingleTestFile(ci)
  log_debug("getArgsToRunSingleTestFile")
  self:ensurePathExists(ci)

  -- test framework bin
  local tfw_bin = self:getTestFrameworkExecutable() or 'busted'
  local path = ci:getInnerPath() .. '.' .. self.ext

  local params = self:newExecParams(tfw_bin, { path })

  local builder = self.builder ---@cast builder env.langs.lua.LuaTester
  if class.get_class(builder) == LuaTester then
    params = builder:fixRootDirInRunParams(params)
  end

  return params
end

---@param ci env.lang.ClassInfo -- test class to run
---@return env.lang.ExecParams
function LuaLang:getArgsToRunSingleSrcFile(ci)
  log_debug("getArgsToRunSingleSrcFile")
  self:ensurePathExists(ci)

  -- interpreter bin
  local bin = self.executable or 'lua'
  local path = ci:getInnerPath() .. '.' .. self.ext

  return self:newExecParams(bin, { path })
end

---@return env.langs.lua.LuaGen -- the instance of a LangGen class (ready to work)
function LuaLang:newLangGen()
  self.gen = LuaGen:new({ lang = self });
  return self.gen
end

---@param ci env.lang.ClassInfo
---@param w Cmd4Lua
function LuaLang:doCodeCheck(ci, w)
  log_debug("doCodeCheck")
  CodeCheck.handle(w, ci)
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

--
-- todo move old Lua stuff to env.langs.lua
--
function LuaLang:gotoDefinition()
  local lua = require 'env.lua'
  local lp = require 'env.lua_parser'
  local c4lv = require 'env.util.cmd4lua_vim'

  local editor = self:getEditor()

  local callback = function(path, jumpto)
    return Lang.openExistedFileWithJumpTo(editor, path, jumpto)
  end

  -- TODO rewrite to use stuff from env.langs.lua
  --
  local line = self:getEditor():getCurrentLine()
  local ctx = { -- deprecated ctx todo update
    bufnr = editor:getContext(true).bufnr,
    bufname = editor:getContext():getCurrentBufname(),
    project_root = self.project_root,
    builder = self.builder,
    test = self.test,
    src = self.src,
    lang = self,
  }

  if lp.find_path_from_code(ctx, line, callback) then return true, 2 end

  if lua.find_module_by_require(ctx, line, callback) then return true, 3 end

  if c4lv.find_function_by_cmdname(ctx, line, callback) then return true, 4 end

  if lua.find_source_by_current_word(ctx, line, callback) then return true, 5 end

  return false
  -- lua.find_path_to_source_for(ctx, ctx:getCurrentLine(), callback) -- workload
end

--
-- from some source code
-- main goal: jump to source file from stacktrace with errors
-- for example, from errors in tests
--
---@param editor env.ui.Editor
---@return boolean
--
function LuaLang.gotoDefinitionFromNonSource(editor, line)
  local lua = require('env.lua')
  line = line or editor:getContext():getCurrentLine()

  local callback = function(path, jumpto)
    return Lang.openExistedFileWithJumpTo(editor, path, jumpto)
  end

  return lua.find_source_by_tracemsg(line, callback)
end

function LuaLang:getTestDiagnost()
  log_debug("getTestDiagnost has:%s", self.test_diagnost ~= nil)
  if not self.test_diagnost then
    self.test_diagnost = LuaTestDiagnost:new(nil, self, 'BUSTED')
  end
  return self.test_diagnost
end

--

--
-- to generate a new project with LuaRocksBuilder
--
---@param pp table{project_root, PROJECT_NAME, ...}
---@param builder env.langs.lua.LuaRocksBuilder
---@return env.langs.lua.LuaLang
---@return env.langs.lua.LuaGen
local function genProjectFiles(pp, builder, opts)
  log_debug("genProjectFiles for ", (pp or E).project_root)
  assert(type(pp) == 'table', 'pp')
  assert(type(pp.project_root) == 'string', 'pp.project_root')
  assert(type(builder) == 'table', 'builder')

  local lang = LuaLang:new()
      :setProjectBuilder(builder)
      :newProject(pp.project_root, opts) -- validate unique + sync-builder + cache

  local gen = lang:getLangGen() ---@cast gen env.langs.lua.LuaGen

  gen:createProjectFiles(pp)

  -- if not quiet then
  --   w:say(gen:getReadableSaveReport("Generated Files:"))
  -- end
  local path, code = gen:getFileWriter():getEntry(1)

  if code == base.OK_SAVED and not opts.dont_open then
    lang:getEditor():open(path or '.')
  end
  ---@diagnostic disable-next-line: return-type-mismatch
  return lang, gen
end

--
-- generate by given properties a new project with LuaRocksBuilder
-- will be generated:
-- dirs, lua-files, rockspec, readme, license, gitignore, makefile
--
---@param pp table{project_root, ...}
---@param opts table
---@return env.langs.lua.LuaLang
---@return env.langs.lua.LuaGen
function LuaLang.createNewProject(pp, opts)
  assert(type(pp) == 'table', 'pp')
  assert(type(pp.project_root) == 'string', 'pp.project_root')

  local ureadme = require 'env.langs.lua.util.readme'
  local umakefile = require 'env.langs.lua.util.makefile'
  local uclitool = require 'env.langs.lua.util.clitool'

  local builder = LuaRocksBuilder.of(pp)
  local lang, gen = genProjectFiles(pp, builder, opts) -- main class + test

  -- generate files: rockspec, readme, license, gitignore, makefile:

  local rockspec = join_path(pp.project_root, pp.ROCKSPEC or 'lib-scm-1.rockspec')
  local body = builder:buildScriptToString()
  gen:createFile(false, rockspec, body, opts)

  local readme = join_path(pp.project_root, 'README.md')
  body = ureadme.gen_readme(pp)
  gen:createFile(false, readme, body, opts)

  local license = join_path(pp.project_root, 'LICENSE')
  local license_body, err = ureadme.gen_license(pp.LICENSE, pp.YEAR, pp.AUTHOR)
  if license_body then
    gen:createFile(false, license, license_body, opts)
  else
    -- w:say(err) -- unknown license, continue ...??
    gen:getFileWriter():addEntry(license, base.ERR_NO_CONTENT, err) -- exit
  end

  local gitignore = join_path(pp.project_root, '.gitignore')
  body = ureadme.gen_gitignore()
  gen:createFile(false, gitignore, body, opts)

  local makefile = join_path(pp.project_root, 'Makefile')
  body = umakefile.gen_Makefile_body(pp.ARTIFACT_ID, pp.VERSION)
  gen:createFile(false, makefile, body, opts)

  -- generate stuff for command-line-interface tools
  if pp.CLI_TOOL then
    local appname = pp.PROJECT_NAME or 'myapp'
    local bin = join_path(pp.project_root, 'bin', appname)
    body = uclitool.gen_bin_script_body(appname, pp.LUA_INTERPRETER or 'lua')
    gen:createFile(false, bin , body, opts)
    -- chmod u+x bin ?

    local handler = join_path(pp.project_root, 'src', appname, 'handler.lua')
    body = uclitool.gen_handler_body(appname, pp)
    gen:createFile(false, handler, body, opts)

    -- todo add to README.md in section USAGE
    -- ```sh\n appname --version \n```
  end

  return lang, gen -- w:say(gen:getReadableSaveReport())
end

--

if _G.TEST then
  LuaLang.genProjectFiles = genProjectFiles
end

class.build(LuaLang)
return LuaLang

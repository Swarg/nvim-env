-- 09-11-2023 @author Swarg
-- Embeded LuaTester
-- Goal:
-- - Support for integration tests written in Lua for other programming languages
--   with the ability to support dependencies from luarocks.
--
-- - project_root has NotLua env.langs.*.*Lang and in test subdirs places
--   Luatester as "embeded LuaProject with custom Builder without any buildscripts"
--

local log = require 'alogger'
local class = require 'oop.class'

local su = require 'env.sutil'
local fs = require 'env.files'

local Editor = require 'env.ui.Editor'
local LuaRocksBuilder = require 'env.langs.lua.LuaRocksBuilder'


class.package 'env.langs.lua'
---@class env.langs.lua.LuaTester: env.langs.lua.LuaRocksBuilder
---@field new fun(self, o:table?, project_root:string?): env.langs.lua.LuaTester
---@field target_lang string?
---@field actual_project_root string?
local C = class.new_class(LuaRocksBuilder, 'LuaTester', {
  name = 'luarocks',
})

local MAX_LINES_PER_SPEC_FILE = 32
local JVMTEST = "/src/test/_lua/" -- for testing jvm app with lua

-- defualt file with dependencies in comments
local TEST_HELPER = 'test_helper.lua'

local SUPPORTED_LANGS = {
  java = JVMTEST
}

-- constructor
function C:_init()
end

---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local match = string.match


---@param path string most often this is already the root directory of the project
---@param userdata table{test, target_lang}
---@param filename string? current opened file
function C.isProjectRoot(path, userdata, filename)
  log_debug("isProjectRoot", path, userdata ~= nil)

  for lang, test_path in pairs(SUPPORTED_LANGS) do
    local abs_test_path = fs.join_path(path, test_path)
    if fs.dir_exists(abs_test_path) then
      if type(userdata) == 'table' then
        -- not actual project_root to avoid collision with another Lang instance
        userdata.project_root = fs.ensure_dir(abs_test_path)
        userdata.target_lang = lang
        userdata.actual_project_root = path
        userdata.test = './' -- test_path
        -- remember the dynamic file from where the dependencies should be
        -- updated on the project reloading
        -- relative path from project root
        -- userdata.buildscript = fs.join_path(test_path, TEST_HELPER)
        userdata.buildscript = TEST_HELPER

        local abs_buildscript_path = fs.join_path(abs_test_path, userdata.buildscript)
        local th_exists, th_lm_time = fs.is_file(abs_buildscript_path)
        userdata.buildscript_lm = th_lm_time

        -- pick dependencies either from current file either from test_helper
        -- preferring to look for dependencies in the test_helepr if this file
        -- exists
        filename = th_exists == true and abs_buildscript_path or filename
        userdata.settings = C.loadSettingsFromSpecFile(filename)
        C.notifyAboutFoundDeps(userdata)
      end
      return userdata.settings ~= nil
    end
  end

  return false
end

function C.notifyAboutFoundDeps(t)
  if type(t) == 'table' then
    local msg
    if t.settings and t.settings.dependencies then
      local found = table.concat(t.settings.dependencies, " ")
      msg = 'Found used dependencies: ' .. found
    else
      msg = 'Not found any dependencies for lang: ' .. v2s(t.target_lang)
    end
    ---@diagnostic disable-next-line: param-type-mismatch
    Editor.echoInStatus(nil, 'EmbededLuaTests: ' .. v2s(msg))
  end
end

-- override from LuaRocksBuilder
---@param bin string raw text of buildscript to parse
---@param prev_lm number
---@return false|table
---@return string?
function C:parseBuildScript(fn, bin, prev_lm)
  if log.is_debug then
    if #(bin or '') > 256 then bin = string.sub(bin, 1, 256) .. "...\n" end
    log_debug("parseBuildScript has_settings:%s(%s) fn:%s(%s) bin:%s",
      self.settings ~= nil, prev_lm, -- prev lastModifTime for buildscript
      fn, self.buildscript_lm,       -- just updated lastModifTime for fn
      bin)
  end
  local new_settings = C.loadSettingsFromSpecFile(fn)

  C.notifyAboutFoundDeps(new_settings)

  self.settings = new_settings or self.settings
  return self.settings
end

-- to supports define dependencies via comments in per-test file
---@param filename string?
---@return table?
function C.loadSettingsFromSpecFile(filename, lines)
  local raw_deps = C.loadRawDependenciesFromComments(filename, lines)
  local t = nil
  if type(raw_deps) == 'table' then
    for _, line in ipairs(raw_deps) do
      local luarocks = match(line, '^luarocks:%s*(.*)%s*$')
      if luarocks then
        t = t or {}
        local list = su.split(luarocks, " ")
        t.dependencies = {}
        if #list > 0 then
          for _, rock in ipairs(list) do
            local rock_name = match(rock, "^%s*(.-)%s*,?$") or rock
            t.dependencies[#t.dependencies + 1] = rock_name
          end
        end
      end
    end
  end
  log_debug("loadedFromSpecFile:%s", t)
  return t
end

---@param filename string?
---@param lines table?
---@return table?
function C.loadRawDependenciesFromComments(filename, lines)
  log_debug("loadRawDependenciesFromComments", filename)
  local deps = nil
  if filename and filename ~= '' then
    lines = lines or fs.read_lines_in_range(filename, 1, MAX_LINES_PER_SPEC_FILE)
    local in_deps_block = false

    for _, line in ipairs(lines or E) do
      local comment = string.match(line, '^%-%-[%-]*%s*(.-):?%s*$')
      if in_deps_block and (comment == nil or comment == '') then
        -- end of dependencies block
        break
      end

      if comment then
        if in_deps_block then
          deps = deps or {}
          deps[#deps + 1] = match(comment, "^%s*%-%s*(.*)%s*$") or comment
        elseif comment == 'dependencies' then
          in_deps_block = true
        end
      end
    end

    log_debug("readed lines cnt:%s raw:%s", #(lines or E), deps)
  end
  return deps
end

--
--
---@param params env.lang.ExecParams
---@return env.lang.ExecParams
function C:fixRootDirInRunParams(params)
  local cwd, path, npath = self.actual_project_root, params.args[1], nil

  if cwd and path and su.starts_with(self.project_root, cwd) then
    -- Adding the missing part for the real project root
    npath = fs.join_path(self.project_root:sub(#cwd + 1), path)
    params = params:withCwd(cwd):withArgs({ npath })
  end
  log_debug("fixRunParams proj_root:%s actual_proj_root:%s path:%s np:%s",
    self.project_root, cwd, path, npath)
  return params
end

class.build(C)
return C

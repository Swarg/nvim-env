-- 09-11-2023 @author Swarg
local class = require 'oop.class'

local fs = require('env.files')
local log = require('alogger')

class.package 'env.langs.lua'
---@class env.langs.lua.Busted : oop.Object
---@field executable string
local Busted = class.new_class(nil, 'Busted', {
  executable = "busted",
})


local E = {}
-- local dprint = require("dprint").mk_dprint_for(Busted)


---@param path string
---@return self
function Busted:setProjectRoot(path)
  self.project_root = path
  return self
end

---@return table
function Busted:getConfig()
  if not self.config and self.project_root then
    local path = fs.join_path(self.project_root, '.busted')
    self.config = self:loadConfig(path) or {}
  end
  return self.config
end

---@param path string
---@return table?
function Busted:loadConfig(path)
  assert(type(path) == 'string', 'path')
  local bin = fs.read_all_bytes_from(path)

  local ok, func = pcall(loadstring, bin)
  log.debug('Busted parse ', ok, func)
  if not ok or type(func) ~= 'function' then
    log.debug('Busted Cannot parse err:', func)
    return nil
  end
  local t = func()
  if not t then
    return nil
  end
  return t
end

-- find testsuite name for namespacepath in project
---@param path string        -- can be full path to filename or namespacepath
---@return string?, string?  -- testsuitename and testpath
function Busted:findTestSuiteNameFor(path)
  log.debug('findTestSuiteNameFor', path)
  if self.config and path then
    local root = (((self.config or E).default or E).ROOT or {})[1]
    if root and root:sub(-1, -1) ~= fs.path_sep then
      root = root .. fs.path_sep
    end
    log.debug('check path:', path, 'testsuite root:', root)
    if root and path:find(root, 1, true) then
      return 'ROOT', root
    end
  end
  return nil
end

class.build(Busted)
return Busted

-- 10-04-2024 @author Swarg
-- Goal: checking the correctness of the lua-code. smtg like a static analyzer.
-- simple code validity check based on table behariour
--
-- the idea was to catch attempts to call functions that do not exist in modules
-- laid the foundation but did not implement it
--

local M = {}

---@param w Cmd4Lua
---@param ci env.lang.ClassInfo
function M.handle(w, ci)
  w:about('Code Check')
      :set_var('ci', ci)
      :handlers(M)

      :desc('A simple check for the use of functions that do not exist')
      :cmd('check-code-moving', 'm')

      :run()
end

--
-- A static analysis tool for
--
---@param w Cmd4Lua
function M.cmd_check_code_moving(w)
  if not w:is_input_valid() then return end

  error('Not implemented yet')
end

--[[
- take all the sources and tests that need to be checked
- ask from these files the names of the modules into which the code was moved
- build a list with the name of constants exported by these new modules
- go through all the other modules(line-by-line) and check
  if there are places where the same constants are called for aliases pointing
  to other modules
]]


return M

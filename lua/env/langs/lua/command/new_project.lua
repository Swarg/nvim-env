-- 17-11-2024 @author Swarg
--

local ucmd = require 'env.lang.utils.command'

local ObjEditor = require 'env.ui.ObjEditor'

local LuaLang = require 'env.langs.lua.LuaLang'
local LuaRocksBuilder = require 'env.langs.lua.LuaRocksBuilder'


local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvProject new <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('generate a new lua project with rockspec file')
      :cmd('rock', 'r')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


local defineStdGenOpts = ucmd.defineStdGenOpts
local getDefaultProjProps = LuaRocksBuilder.getDefaultProjectProperties



-- used w.vars.props to override defaults proj_props
---@param w Cmd4Lua
local function define_project_properties(w, title)
  local t = ucmd.define_project_properties(w, title, getDefaultProjProps())

  -- fill project property table(t) by cli options:
  w:opt_override('-c', '--main-class', t, "MAINCLASS")
  w:opt_override("-g", "--group_id", t, "GROUP_ID")
  w:opt_override("-a", "--artifact_id", t, "ARTIFACT_ID")
  w:opt_override("-v", "--version", t, "VERSION")
  w:opt_override('-T', '--test-framework', t, 'TEST_FRAMEWORK')
  w:opt_override('-L', '--lua-interpreter', t, 'LUA_INTERPRETER')

  return t
end

--
-- run given callback with project props
--
---@param w Cmd4Lua
---@param title string
---@param pp table
---@param callback function
local function apply(w, title, pp, callback)
  assert(type(title) == 'string', 'title')
  assert(type(callback) == 'function', 'expected function callback')
  assert(type(pp) == 'table', 'pp')

  if w.vars.yes then
    callback(pp, w:is_quiet())
  else
    ObjEditor.create(pp, title, callback,
      LuaRocksBuilder.update_project_properties,
      LuaRocksBuilder.getProjPropsKeyOrder())
  end
end

--
-- generate lua project with rockspec file
--
---@param w Cmd4Lua
function M.cmd_rock(w)
  defineStdGenOpts(w)
  w:about("Generate new Project with rockspec")

  local title = { [1] = 'New Lua Project with rockspec' }
  local pp = define_project_properties(w, title)
  pp.CREATE_PROJECT = true
  pp.openInEditor = true -- to open generated files

  if not w:is_input_valid() then return end

  if w:is_verbose() then print(require "inspect" (pp)) end -- debugging

  apply(w, "NewLuaProject", pp, function(pp0, quiet)
    if not pp0 or not pp0.CREATE_PROJECT then -- files
      if not quiet then w:say('canceled') end
      return
    end
    local _, gen = LuaLang.createNewProject(pp0, w.vars)
    w:say(gen:getReadableSaveReport())
  end)
end

return M

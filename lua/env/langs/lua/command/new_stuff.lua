-- 25-11-2023 @author Swarg
-- Goal:
-- NewStuff Command handler for LuaGen

---@diagnostic disable-next-line: unused-local
local log = require('alogger')
local ucmd = require 'env.lang.utils.command'
local Editor = require 'env.ui.Editor'
-- local Context = require("env.ui.Context")
local LuaGen = require 'env.langs.lua.LuaGen'
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")
-- local FieldGen = require("env.lang.oop.FieldGen")

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

-- :EnvNew <cmd>
--
---@param w Cmd4Lua
function M.handle(w)
  w:about('New Stuff for lua-lang')
      :handlers(M)

      :desc('Generate new lua class')
      :cmd("class", "c")

      :desc('Create new test-case file. (many-tests-for-one-source)')
      :cmd("test-case", 'tc')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local defineStdGenOpts = ucmd.defineStdGenOpts

-- oop
---@param w Cmd4Lua
function M.cmd_class(w)
  assert(type(w) == 'table', 'w')
  local gen = w:var('gen') ---@type env.langs.lua.LuaGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  local classname, pkg
  pkg = w:opt('--package', '-p', 'by default its taken from a current file')

  w:group('A', 1)
      :desc('If a shortname - generate with a package(ns) of current opened')
      :tag('classname'):pop():v_arg()
  w:group('A', 1)
      :desc('Take a ClassName from a current line with use(import)')
      :tag('classname')
      :v_optp('--from-use', '-u', LuaGen.getClassNameFromLine, gen) -- require

  w:v_opt('--imports', '-i')                                        -- require
  w:def('oop.Object'):v_opt('--extends', '-e')                      -- inheritance
  w:v_opt('--implements', '-m')

  -- params
  w:v_has_opt('--constructor', '-c')

  w:tag('test_class'):has_opt('--test', '-t', 'generate a test file')
  -- methodname to autogen pick from self:getContext().element(LEX_TYPE.FUNC)


  ----------------------------------- actually work (runs if all input correct)
  if not w:is_input_valid() then return false, w:get_vars() end


  -- convert from cli defintion to Field objects
  -- if fields_cli then -- list of strings from cli input to parse
  --   w:set_var('fields', FieldGen.parseFieldsFromStrings(fields_cli))
  -- end

  classname = w.vars.classname -- autovalidate in is_input_valid

  -- if package(namespace) not defined when take from current opened class
  if not classname:find('.', 1, true) then
    if not pkg then
      local ctx = gen:getContext()
      local from_ci = gen.lang
          :resolveClass(ctx.bufname, ClassInfo.CT.CURRENT)
          :getClassInfo()
      if from_ci then
        pkg = from_ci:getPackage()
        log.debug('Resolve from current: %s pkg:', from_ci:toString(), pkg)
      end
    end
  end

  if pkg then
    classname = pkg .. '.' .. classname
  end

  if not w:is_quiet() then
    classname = Editor.askValue('Full ClassName: ', classname)
    if not classname or classname == '' then
      gen:getEditor():echoInStatus('Canceled')
      return false, w:get_vars()
    end
  end
  log.debug('New ClassName to Generate: %s', classname)
  local new_ci = ClassResolver.of(gen.lang):lookupByClassName(classname)

  return gen:createClassFile(new_ci, w:get_vars())
end

--
-- Create new test-case file. (many-tests-for-one-source)
--
-- new test-case - in subdir with source-file name
-- dy default test-case will be named from the method name under the cursor
--
---@param w Cmd4Lua
function M.cmd_test_case(w)
  local gen = w:var('gen') ---@type env.langs.lua.LuaGen
  defineStdGenOpts(w) --dry-run|R --quiet|Q --author|A --no-date|D --not-open|X

  w:desc('test-case name i.g. method name')
      :v_opt('--name', '-n') -- support name with subdirs:  sub1/sub2/

  if not w:is_input_valid() then return end

  return gen:createNewTestCaseFile(w.vars)
end

return M

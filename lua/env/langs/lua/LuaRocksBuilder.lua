-- 09-11-2023 @author Swarg
-- Goal: bridge with luarocks and *.rockspec - a lua-project configuration file
--
-- Dependencies:
--  luarocks -- for update_settings (additional features, can work without it)
local class = require 'oop.class'

local su = require('env.sutil')
local fs = require('env.files')
local utbl = require 'env.util.tables'

---@diagnostic disable-next-line: unused-local
local log = require('alogger')
local library_cache = require("env.cache.library")

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") -- vim.loop

local base = require 'env.lang.utils.base'
local Builder = require('env.lang.Builder')

class.package 'env.langs.lua'
---@class env.langs.lua.LuaRocksBuilder : env.lang.Builder
---@field new fun(self, o:table?, project_root:string?): env.langs.lua.LuaRocksBuilder
---@field project_root    string
---@field buildscript     string   *.rockspec (config-file with a settings)
---@field buildscript_lm  number?  last modify time of the rockspec-file
--                                 todo: checks changes and reload config
--- field settings        table?   parsed a rockspec file (buildscript)(config)
---@field workspace_libs  table?   module -> absolute path (deps-mappings)
--                                 mappings for all dependencies(rock-packages)
--                                 specified in the rockspec-file in secrion
--                                 dependencies. real paths taken via luarocks show
local LuaRocksBuilder = class.new_class(Builder, 'LuaRocksBuilder', {
  name = 'luarocks',
  project_props = nil,
  modules_map = nil, -- .rockspec  build.modules
})


local E = {}
local log_debug, log_trace = log.debug, log.trace
local mk_full_classname = class.base.mk_full_classname

--
-- public static
-- check is the given directory is Root of Source Code with .rockspec
-- userdata used to pass infos need to build instance of LuaRocksBuilder
--
---@param path string
---@param userdata table|nil output data to build instance
---@return string?  full path to script file or nil
function LuaRocksBuilder.isProjectRoot(path, userdata)
  log_debug("LuaRocksBuilder.isProjectRoot %s", path)
  if type(path) == 'string' then
    if path:match('%.rockspec$') then
      return path
    end

    local dir = fs.ensure_dir(path)
    local handle, errmsg = uv.fs_scandir(dir)
    log_debug('find at dir %s handle %s', dir, type(handle))
    -- local exists = (stat and stat.type == "directory") == true
    if type(handle) == 'string' or not handle then
      log_debug('Scandir Error %s', errmsg)
      return nil
    end

    local rockspec = nil
    -- hack to check is buildscript exists + check what is src|test|spec dirs
    -- what is full name of *.rockspec file(buildscript for luarocks)
    while true do
      local name, _ = uv.fs_scandir_next(handle)
      if not name then break end

      if name:match('%.rockspec$') and fs.is_file(fs.join_path(dir, name)) then
        rockspec = name --
        if not userdata then break end

        userdata.buildscript = rockspec
        userdata.project_root = path
        --
      elseif userdata and (name == 'src' or name == 'lua') then
        userdata.src = userdata.src or (name .. fs.path_sep)
        --
      elseif userdata and (name == 'spec' or name == 'test') then
        userdata.test = userdata.test or (name .. fs.path_sep)
      end
    end

    if rockspec then
      return fs.join_path(path, rockspec)
    end
  end
  return nil
end

--
-- wrap a string with readed data from a rockspec-file data into safe lua code
-- workaroud to load lua-table of rockspec-file
-- wrap all variables in the config to one lua-table
--
-- local t = {}                original:
-- t.package = ...      <--    package ..
-- t.build = ..                build = ..
-- return t
--
---@param s string - body with code of rockspec file
local function wrap_conf_to_luatable(s)
  if s then
    local lines = su.split_range(s, '\n')
    if lines then
      local t = {}
      t[#t + 1] = 'local t = {}'
      for _, line in ipairs(lines) do
        if line:match('^[%w]+%s*=') then
          line = 't.' .. line
        end
        t[#t + 1] = line
      end
      return table.concat(t, "\n") .. "\nreturn t"
    end
  end
end

-- @Override
---@param bin string
---@param prev_lm number
---@return false|table
---@return string?
function LuaRocksBuilder:parseBuildScript(fn, bin, prev_lm)
  log_debug("parseBuildScript", fn)
  assert(type(bin) == 'string', 'bin')
  prev_lm = prev_lm

  bin = wrap_conf_to_luatable(bin)

  local ok, func = pcall(loadstring, bin)
  if not ok or type(func) ~= 'function' then
    log_debug('LuaRockBuilder Cannot parse rockspec err:', func)
    return false, 'cannot parse file ' .. tostring(fn)
  end

  local okt, settings = pcall(func)
  if not okt or type(settings) ~= 'table' then
    return false, settings
  end

  return settings, nil
end

-- --
-- -- check is a config(buildscript) was changed by lastmodify time
-- -- if the time is not specified (nil) assume that it has changed
-- ---@param time number
-- function LuaRocksBuilder:isBuildScriptChanged(time, update_lm)
--   if update_lm then
--     self.buildscript_lm = fs.last_modified(self:getBuildScriptPath())
--   end
--   log_debug("isBuildScriptChanged rockspec-lm:%s time:%s", self.buildscript_lm, time)
--   return not time or (self.buildscript_lm and self.buildscript_lm > time)
-- end

-- config.build.modules
---@return table (readonly)
function LuaRocksBuilder:getModulesMap()
  return ((self.settings or E).build or E).modules or {}
end

-- find innner path by module name iek
-- find module( for cn - classname
--
---@param cn string - classname
---@return string? inner_path without extension
function LuaRocksBuilder:findInnerPathForModule(cn)
  log_debug("findInnerPathForModule", cn)
  -- print("\n\n\n-----------------------------------", cn)

  local modules = self:getModulesMap()
  local path0 = modules[cn]
  if path0 then
    -- without extension
    if su.ends_with(path0, '.lua') then
      path0 = path0:sub(1, #path0 - 4)
    end
    -- dprint('found: ', path0)
    return path0
  end -- full

  ---@param mpath string
  ---@param cn0 string
  local function build_path(mpath, cn0)
    if not mpath or not cn0 then
      error('[DEBUG] for cn: ' .. tostring(cn) .. 'mpath:' .. tostring(mpath) .. ' ' .. tostring(cn0))
      return nil
    end
    -- root-ns.sub ->     root-ns/sub/
    local rel_path = fs.classname_to_path(cn0) .. fs.path_sep
    local i = mpath:find(rel_path, 1, true)
    if i then
      --   src/root-ns + sub/module
      return mpath:sub(1, i + #rel_path - 1)
          .. fs.classname_to_path(cn:sub(#cn0 + 2, #cn))
    else
      error('[DEBUG] Cannot find i for rel-path: ' .. tostring(rel_path)
        .. ' in mpath: ' .. mpath .. ' i ' .. tostring(i))
    end
  end

  --rel-path:      root-ns/ in
  --mpath:     src/root-ns/util.lua i nil

  -- find similar path by module name
  --  root-ns.sub.module    ---->   src/root-ns/sub/module
  --  ^^^^^^^        root-ns  = "src/root-ns/init.lua",
  --  ^^^^^^^ -----> ^^^^^^^
  local cn0 = cn
  while true do
    cn0 = cn0:match("(.*)[%.]") -- org.comp.ns -> org.comp
    if not cn0 then break end   -- no more pkg deep
    path0 = modules[cn0]
    -- print('[DEBUG] try:', cn0, path0)
    if path0 then
      path0 = build_path(path0, cn0)
      break
    end -- found!
  end

  if not path0 then
    -- ert.same('busted/modules/cli.lua', f('busted.modules'))
    cn0 = cn:match("(.*)[%.]") -- org.comp.ns -> org.comp
    for module, mpath in pairs(modules) do
      -- print('[DEBUG] try:', cn0, module, mpath)
      -- root-ns.tool & root-ns
      if su.starts_with(module, cn0) then
        path0 = build_path(mpath, cn0)
        break
      end
    end
  end
  -- print('[DEBUG] cooked: ', path0)
  return path0
end

--
-- TODO
---@diagnostic disable-next-line: unused-local
function LuaRocksBuilder:argsTestSingleClass(class_name, opts, method_name)
  if class_name then
    local test = class_name
    -- if defined method name then run test only for this method
    if method_name and method_name ~= '' then
      test = test .. '.' .. method_name
    end
    local args = { "test", "--tests", test, "-i" } -- args
    -- return setup_opts(args, opts)
    return args
  end
  return {}
end

--
-- parse output of "luarocks show ROCKNAME"
-- used to create mappings module-name --> absolute path to source-file
--
---@param output string
---@param t table
local function parse_luarocks_show(output, t)
  t = t or {}
  local cnt = 0
  local started = false

  -- parse output of luarocks.show command
  -- mappings module -> full-path
  for line in output:gmatch("([^\n]*)\n?") do
    if line then
      if started then
        if line and line:sub(1, 1) ~= "\t" then break end
        local mod, path = line:match("^\t([^%s]+) %((.-)%)")
        log_trace('mod:', mod, 'path:', path)
        -- mod is a "string" that used in the "require" - function
        -- path is a absolute path to the installed module - a lua-file
        if mod then
          t[mod] = path
          cnt = cnt + 1
        end
      end
      if not started and line:match('Modules:') then started = true end
    end
  end
  return t, cnt
end

--
-- run external system command and read output
--
---@param cmd string
local function os_run(cmd)
  assert(type(cmd) == 'string', 'cmd')
  local output = fs.execr(cmd .. ' 2>&1')
  local len = -1
  if type(output) == 'string' then len = #output end
  log_debug('run command: [%s] output_len: %s', cmd, len)
  log_trace(output)
  return output
end

--
-- by rock name (package in the luarocks pkg-manager) find paths to all modules.
-- luarocks must be installed in the system and added to PATH
--
-- luarocks show cmd4lua
-- works using cache
-- stored rockspec record with path and mtime in the result table of mappings
--
---@param pkg_name string (rockname)
---@return table? -- mappings: module - absolute path to this module
function LuaRocksBuilder.getPackageModules(pkg_name, version, no_cache)
  log_debug("getPackageModules", pkg_name, version)
  assert(type(pkg_name) == 'string' and pkg_name ~= '', 'pkg_name')
  version = version or 1

  local t = nil        -- mappings with mod-name --> mod-abs-path

  if not no_cache then -- use Cache
    local created
    -- TODO check updates of rock-package by last-modify-time
    t, created = library_cache.get('lua', pkg_name, version)

    if not created then
      log_debug('taken already cached for', pkg_name, version)
      return t
    end
  else
    t = {}
  end

  return LuaRocksBuilder.queryRockModules(t, pkg_name, version, not no_cache)
end

--
-- Create mappings for given rockname(package) via luarocks show
-- add a full path to the rockspec-file and last-modify-time of it
-- mtime and path used in refreshBuildscript to auto update cached package
-- mappings on save rockspec-file opened in nvim buffer
--
---@param rockname string
---@param version string
---@param with_rockspec boolean - when used cache
---@return table
function LuaRocksBuilder.queryRockModules(t, rockname, version, with_rockspec)
  log_debug("queryRockModules", rockname)

  assert(type(t) == 'table', 'table for output result')
  assert(type(rockname) == 'string', 'rockname')
  version = version or 1 -- ? TODO how use version for show?

  -- todo cache a fails access to luarocks?
  local output = os_run('luarocks show ' .. tostring(rockname))
  if not output or output == '' then
    log_debug('[WARN] Wrong output from luarocks. Is luarocks installed?')
    return t
  end

  local _, cnt = parse_luarocks_show(output, t)

  -- for cache: last-modify time and full path to the installed rockspec
  -- used to check for updates when rockspec opened in nvim are saved
  if with_rockspec and cnt > 0 then
    t.rockspec = nil
    log_debug('fetch rockspec mtime for', rockname)
    local fn = os_run('luarocks show --rockspec ' .. tostring(rockname))
    if fn then
      fn = fn:gsub("\n", "")
      local lm = fs.last_modified(fn)
      if lm then
        t.rockspec = { path = fn, mtime = lm }
      end
    end
    if not t.rockspec then
      log_debug('[WARN] could not get a rockpec and mtime for', rockname)
    end
  end

  return t
end

--
-- based on the rockspec file, get a mapping of the absolute paths of all
-- modules for each rock-package(dependency) specified in the "dependencies"
-- section
--
-- update self.workspace_libs
--
-- it used for update lsp settings to attach needed library into workspace
-- the "dependencies" is the same as a "workspace library"
--
---@return self
function LuaRocksBuilder:resolveDependencies()
  log_debug("resolveDependencies")

  local rock_deps = self:getSettings().dependencies
  self.workspace_libs = self.workspace_libs or {}

  local skipped, checked = 0, 0

  if rock_deps then
    for _, line in pairs(rock_deps) do
      checked = checked + 1
      -- line is a definition of a rock-package (package_name <= version)
      local pkg, ver = line:match("%s*([^%s]+)%s*[<>=]+%s([^%s]+)")
      if pkg == nil and line and not line:find(' ') then
        pkg, ver = line, '' -- ? or set ver as 'latest' ?
      end
      log_trace("package ", pkg, ver)
      if pkg == 'lua' then
        skipped = skipped + 1
      else
        local mods = LuaRocksBuilder.getPackageModules(pkg, ver)
        log_debug('for package %s-%s found modules:%s', pkg, ver, mods ~= nil)
        -- mods is a mappings module-name -- absolute path
        -- and rockspec for cache with full path to rockspec and mtime of it
        self.workspace_libs[pkg] = mods
      end
    end
  end

  log_debug('dependencies:%s skipped:%s', checked, skipped)
  return self
end

--
-- the "dependencies" is the same as a "workspace library"
---@return table
function LuaRocksBuilder:getWorkspaceLibs()
  log_trace("getWorkspaceLibs")
  if not self.workspace_libs then
    self:resolveDependencies()
  end
  return self.workspace_libs
end

---@param path string
function LuaRocksBuilder:refreshBuildscript(path)
  log_debug("refreshBuildscript", path)
  if self.settings then
    self.settings = nil -- to reload in getSettings
  end

  -- check used dependencies to update( if rockspec file is changed by lm-time)
  if self.workspace_libs then
    for pkg, m in pairs(self.workspace_libs) do
      if m and m.rockspec and m.rockspec.mtime and m.rockspec.path then
        local lm = fs.last_modified(m.rockspec.path)
        log_debug('check [%s] last:%s now:%s rockspec:%$',
          pkg, m.rockspec.mtime, lm, m.rockspec.path
        )
        if type(lm) == 'number' and lm > m.rockspec.mtime then
          log_debug('detect updated', pkg)
          library_cache.clean('lua', pkg)
          -- clean cache for reload in resolveDependencies
        else
        end
      end
    end
  end

  self:resolveDependencies()
end

--------------------------------------------------------------------------------
--                     Generate new Project


local default_package = 'pkg'

---@param opts table?{date}
function LuaRocksBuilder.getDefaultProjectProperties(opts)
  opts = opts or E
  local _, _, year = base.getTodayDayMonthYear(opts.date)
  local pkg = opts.default_package or default_package

  return {
    UI_TITLE = "New LuaRocks Project", -- for ObjEditor
    UI_ERRORS = nil,                   -- for ObjEditor
    -- CREATE_PROJECT = "no",
    AUTHOR = Builder.getDeveloperName(),
    YEAR = year or "",
    PROJECT_NAME = "app",
    GROUP_ID = pkg,
    ARTIFACT_ID = "app",
    VERSION = "0.1.0-1",
    MAINCLASS = mk_full_classname(pkg, "Main"),
    DEPENDENCIES = "",
    TEST_FRAMEWORK = "busted",
    LUA_VERSION = "5.1",
    -- description
    HOMEPAGE = "http://gitlab.com/me/app", -- todo via config
    LICENSE = "MIT",
    --
    ROCKSPEC = 'app-0.1.0-1.rockspec',
    CLI_TOOL = false,
    LUA_INTERPRETER = 'lua', -- or luajit for cli-tool
  }
end

local PROJECT_PROPS_KEY_ORDER = {
  'UI_TITLE',  -- for ObjEditor
  'UI_ERRORS', -- for ObjEditor
  'AUTHOR',
  'YEAR',
  'CREATE_PROJECT', -- used then needs to generate buildscript only
  'PROJECT_NAME',   -- aka package
  'GROUP_ID',
  'ARTIFACT_ID',
  'VERSION',
  'MAINCLASS',
  'DEPENDENCIES',
  'TEST_FRAMEWORK',
  'LUA_VERSION',
  'HOMEPAGE',
  'LICENSE',
  'ROCKSPEC',
  'CLI_TOOL',
  'LUA_INTERPRETER',
}

function LuaRocksBuilder.getProjPropsKeyOrder()
  return PROJECT_PROPS_KEY_ORDER
end

local function validate_not_empty(t, key)
  local s = t[key]
  if s == nil or s == '' or string.match(s, "^%s*$") then
    t.UI_ERRORS = t.UI_ERRORS or ''
    if t.UI_ERRORS ~= '' then
      t.UI_ERRORS = t.UI_ERRORS .. ', '
    end
    t.UI_ERRORS = t.UI_ERRORS .. 'no ' .. key
  end
end

---@param t table
---@return false|table
---@return string? err
function LuaRocksBuilder.update_project_properties(t)
  log.debug("update_project_properties", t)
  if not type(t) == 'table' then
    return false, 'expected table project-properties got:' .. type(t)
  end
  t.UI_ERRORS = nil
  validate_not_empty(t, 'MAINCLASS')
  validate_not_empty(t, 'GROUP_ID')
  validate_not_empty(t, 'VERSION')
  validate_not_empty(t, 'PROJECT_NAME')
  validate_not_empty(t, 'LUA_VERSION') -- 5.1
  validate_not_empty(t, 'project_root')

  local defpkg = default_package .. "."

  if not t.ARTIFACT_ID or t.ARTIFACT_ID == '' then
    t.ARTIFACT_ID = t.PROJECT_NAME
  end

  if su.starts_with(t.MAINCLASS, defpkg) then
    t.MAINCLASS = t.GROUP_ID .. '.' .. t.MAINCLASS:sub(#defpkg + 1, #t.MAINCLASS)
    -- MainClass without pakage is not supported
  elseif not t.MAINCLASS:match('%.') then
    t.MAINCLASS = t.GROUP_ID .. '.' .. t.MAINCLASS
  end

  t.ROCKSPEC = string.format('%s-%s.rockspec', t.ARTIFACT_ID, t.VERSION)

  return t
end

LuaRocksBuilder.rockspec_order = {
  root = {
    'package', 'version', 'source', 'description', 'dependencies', 'build',
  },
  source = { 'url', 'tag', 'branch' },
  build = {
    'type', 'modules', 'install'
  }
}

--
--
--
---@return table
function LuaRocksBuilder.newRockspecSettings(pp)
  local settings = {
    package = pp.PROJECT_NAME,
    version = pp.VERSION, -- 'scm-1',
    description = {
      homepage = pp.DESC_HOMEPAGE or 'a project homepage',
      license = pp.LICENSE or 'MIT'
    },
    source = {
      url = 'URL for source tarball'
    },
    dependencies = {
      'lua >= ' .. pp.LUA_VERSION,
    },
    build = {
      type = 'builtin',
      modules = {
        mypkg = 'src/mypkg/init.lua -- FIXME'
      }
    },
  }
  if pp.CLI_TOOL then
    settings.build.install = {
      bin = {
        [pp.ARTIFACT_ID] = 'bin/' .. pp.ARTIFACT_ID
      }
    }
  end
  return settings
end

-- serialize to test for saving
function LuaRocksBuilder:buildScriptToString()
  local sections_order = LuaRocksBuilder.rockspec_order.root
  local t = { "---@diagnostic disable: lowercase-global" }

  for i = 1, #sections_order do
    local key = sections_order[i]
    local val = self.settings[key]
    local vtyp = type(val)
    if vtyp == 'string' then
      t[#t + 1] = tostring(key) .. ' = ' .. utbl.str2code(val)
    elseif vtyp == 'table' then
      local order = LuaRocksBuilder.rockspec_order[key]
      local s = utbl.table2code(val, '  ', nil, order, true)
      t[#t + 1] = tostring(key) .. ' = ' .. s
    else -- number boolean
      t[#t + 1] = tostring(key) .. ' = ' .. tostring(val)
    end
  end
  t[#t + 1] = '' -- to end with "\n"

  return table.concat(t, "\n")
end

--
-- create new a instance of LuaRocksBuilder for new project by given project props
--
---@param pp table
---@return env.langs.lua.LuaRocksBuilder
function LuaRocksBuilder.of(pp)
  return LuaRocksBuilder:new({
    project_root = pp.project_root,
    buildscript = pp.ROCKSPEC,
    settings = LuaRocksBuilder.newRockspecSettings(pp)
  })
end

class.build(LuaRocksBuilder)
return LuaRocksBuilder

-- 17-11-2024 @author Swarg
--
-- local log = require 'alogger'
local utempl = require 'env.lang.utils.templater'


local substitute = utempl.substitute
-- local log_debug = log.debug

local M = {}

local TEMP_README = [[
# ${PROJECT_NAME}

${DESCRIPTION}

## Status

${STATUS}
${STATUS_LINKS}

## Features

${FEATURES}

## Installation

${DESC_HOMEPAGE}


## Usage

${USAGE}


## Author

${AUTHOR}

## License

This module is licensed under the [${LICENSE}](./LICENSE)
]]


local TEMP_GITIGNORE = [[
*.swp
/.luacheckcache
/luacov.report.out
/*.rock
]]


local TEMP_LICENSE_MIT = [[
Copyright (c) ${YEAR} ${AUTHOR}

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]
local LICENSES = {
  mit = TEMP_LICENSE_MIT
}

function M.gen_readme(pp)
  return substitute(TEMP_README, {
    AUTHOR = pp.AUTHOR,
    PROJECT_NAME = pp.PROJECT_NAME,
    DESCRIPTION = pp.DESCRIPTION,
    FEATURES = pp.FEATURES,
    USAGE = pp.USAGE,
    STATUS = pp.STATUS or 'Work In Progress',
    STATUS_LINKS = pp.STATUS_LINKS or '',
    LICENSE = pp.DESC_LICENSE,
  })
end

function M.gen_gitignore()
  return TEMP_GITIGNORE
end

---@param typ string
---@return string?
---@return string? errmsg
function M.gen_license(typ, author, year)
  local templ = LICENSES[string.lower(typ or '')]
  if not templ then
    return nil, 'unknown license ' .. tostring(typ)
  end
  return substitute(templ, { YEAR = year, AUTHOR = author })
end

return M

-- 05-01-2025 @author Swarg
--
-- Create bin/myapp script for cli apps
-- for CLI_TOOL = true
--

local log = require 'alogger'
local utempl = require 'env.lang.utils.templater'

local M = {}

local substitute = utempl.substitute
local log_debug = log.debug

-- for ./bin/myapp
local TEMPL_BIN = [[
#!/usr/bin/env ${_LUA}

-- require '${_APPNAME}.settings'.setup()
os.exit(require '${_APPNAME}.handler'.handle(arg))
]]

--
-- generate the body of executable script to start cli app (bin/myapp)
--
---@param appname string
function M.gen_bin_script_body(appname, lua)
  log_debug("gen_ShellScript_body", appname, lua)

  return substitute(TEMPL_BIN, {
    _APPNAME = appname or 'myapp',
    _LUA = lua or 'lua', -- also can be luajit
  })
end

--
--

local TEMPL_HANDLER = [=[
-- ${_DATE} @author ${_AUTHOR}
--
-- CLI Command Handler
-- Entry point of a cli application
--

-- local log = require 'alogger'
local Cmd4Lua = require 'cmd4lua'

local M = {}
M._VERSION = '${_APPNAME} ${_VERSION}'
M._URL = '${_HOMEPAGE}'

---@param args string|table
function M.handle(args)
  return
      Cmd4Lua.of(args)
      :root('${_APPNAME}')
      :about('TODO add description for my tool')
      :handlers(M)

      :desc('version of this application')
      :cmd('version', 'v')

      :run()
      :exitcode()
end

--
-- version of this application
--
function M.cmd_version()
  io.stdout:write(M._VERSION .. "\n")
end

return M
]=]

--
-- generate the body of lua-source file with entrypoint to cli app
--
---@param appname string
---@param pp table
function M.gen_handler_body(appname, pp, nodate)
  log_debug("gen_handler_body", appname, pp)
  pp = pp or {}
  appname = appname or pp.PROJECT_NAME or 'myapp'

  return substitute(TEMPL_HANDLER, {
    _DATE = nodate == true and 'now' or os.date(' %d-%m-%Y'),
    _AUTHOR = pp.AUTHOR or 'me',
    _APPNAME = appname or 'myapp',
    _VERSION = pp.VERSION or '0.1.0',
    _HOMEPAGE = pp.HOMEPAGE
  })
end

return M

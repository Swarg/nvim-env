-- 17-11-2024 @author Swarg
--

local log = require 'alogger'
local utempl = require 'env.lang.utils.templater'

local M = {}

local substitute = utempl.substitute
local log_debug = log.debug

local TEMPL = [[
.PHONY: ${_PHONY}

install:
	sudo luarocks make

NAME := ${_ARTIFACT}
VERSION := ${_VERSION}

upload:
	luarocks upload $(NAME)-$(VERSION).rockspec

test:
	busted ./spec/
]]

---@param artifact string
---@param version string
function M.gen_Makefile_body(artifact, version)
  log_debug("gen_Makefile_body", artifact, version)

  return substitute(TEMPL, {
    _ARTIFACT = artifact or '',
    _VERSION = version or '',
    _PHONY = 'install upload test',
  })
end

return M

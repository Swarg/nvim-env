--- diagnostic disable: unused-local
-- 10-11-2023 @author Swarg
local class = require 'oop.class'

local log = require 'alogger'

---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")

local lapi_consts = require 'env.lang.api.constants'
local AccessModifiers = require("env.lang.oop.AccessModifiers")
local LangGen = require("env.lang.oop.LangGen")
---@diagnostic disable-next-line: unused-local
local ComponentGen = require("env.lang.oop.ComponentGen")


class.package 'env.langs.lua'
--- define class LuaGen extends LangGen
---@class env.langs.lua.LuaGen : env.lang.oop.LangGen
---@field lang env.langs.lua.LuaLang?
---@field ext string
---@field tab string
---@field default_field_type string
---@field default_field_amod number  -- access modifier (private|public, etc)
local LuaGen = class.new_class(LangGen, 'LuaGen', {
  lang = nil,
  ext = 'lua', -- extension of source files
  test = 'spec',
  tab = '  ',
  default_field_type = 'string',
  default_field_amod = AccessModifiers.ID.PRIVATE,

  cmdhandler_new_stuff = "env.langs.lua.command.new_stuff", -- :EnvNew ..
})


local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, log_debug = {}, tostring, string.format, log.debug

--
-- take classname from current line with use(import)(from sourcefile via editor)
function LuaGen:getClassNameFromLine()
  local line = self:getContext().current_line
  if line then
    -- use(import)
    local classname = line:match('^s*use ([^;]+);')

    return classname
  end
end

-- local template_docsblock = [[
-- /**
--  *
--  */
-- ]]
local template_method = [[
  ---@return ${RETURN_TYPE}
  ${ACCESSMOD} function ${NAME}(${PARAMS})
${BODY}
  end
]]

local template_constructor = [[
  ${ACCESSMOD} function ${CLASS}:init(${PARAMS})
${BODY}
  end
]]

local template_field = [[    ${ACCESSMOD} ${TYPE} ${NAME}${DEFAULT_VALUE};]]
-- local template_asign_to_self_field = [[${THIS} ${EQUALS} ${VALUE};]]

local def_busted_test_imports = [[
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
]]

local test_file_pattern = [[
--${DATE}${AUTHOR}
${IMPORTS}

describe("${PAIR_CLASSNAME}", function()
${FIELDS}
${METHODS}
end)
]]

-- as "method" of the test-class in oop
local test_busted_it_pattern = [[
  it("${NAME}", function()
${BODY}
  end)
]]


-- TODO
local template_class = [[
-- ${DATE} ${AUTHOR}
--

local class = require 'oop.class'
${IMPORTS}

class.package '${PACKAGE}'

---@class ${PACKAGE}.${CLASS}${EXTENDS}
local ${CLASS} = class.new_class(nil, '${CLASS}', {
${FIELDS}
})

-- constructor used in ${CLASS}:new()
function ${CLASS}:_init()
end

${METHODS}

class.build(${CLASS})
return ${CLASS}
]]

-- override
---@param ci env.lang.ClassInfo?
function LuaGen:getClassTemplate(ci)
  if ci and ci:isTestClass() then return test_file_pattern end
  return template_class
end

---@param ci env.lang.ClassInfo?
function LuaGen:getMethodTemplate(ci)
  if ci and ci:isTestClass() then return test_busted_it_pattern end
  return template_method
end

---@diagnostic disable-next-line: unused-local
function LuaGen:getConstructorName(classinfo) return '__construct' end

function LuaGen:getKWordExtands() return ' : ' end

---@return string
function LuaGen:getFieldTemplate() return template_field end

---@return string, string template + separator
function LuaGen:getParamTemplate() return '${NAME}', ',' end

function LuaGen:getKWordThis() return 'self' end

function LuaGen:getKWordBoolean() return 'boolean' end

---@return string
function LuaGen:getVarPrefix() return '' end

---@return string
function LuaGen:getReturnPrefix() return '' end

---@return string
function LuaGen:getAttrDot() return '.' end

---@return string
function LuaGen:getOpCompareIdentical() return '==' end

---@return string
function LuaGen:getConstructorTemplate() return template_constructor end

---@return string
function LuaGen:getGetterTemplate() return '' end

---@return string
function LuaGen:getSetterTemplate() return '' end

---@return string
function LuaGen:getEqualsTemplate() return '' end

---@return string
function LuaGen:getDefaultFieldType() return self.default_field_type end

---@return number
function LuaGen:getDefaultFieldAMod() return self.default_field_amod end

--------------------------------------------------------------------------------



---@param tci env.lang.ClassInfo
---@param opts table?
function LuaGen:genOptsForTestFile(tci, opts)
  log.debug("createOptsForTestFile")
  assert(tci ~= nil)

  local sci, c, tmn, t, body, exp, res
  sci = tci:getPair()
  assert(sci, 'source classinfo')

  opts = opts or {}
  opts.test_class = true

  opts.imports = def_busted_test_imports ..
      'local M = require \'' .. sci:getClassName() .. "'"

  -- local tcn_short = sci:getShortClassName() or ''

  -- method|function name in source file under the cursor
  tmn, t, body = 'success', self:getTab(), ''
  local ok, err = pcall(function() c = self:getContext(true):resolveWords() end)
  if not ok then
    log.error(err) -- errmsg for outside nvim testing
    c = {}         -- stub to continue outside nvim
  end
  log_debug('ok: %s ctx: %s', ok, c)

  local LT = LEXEME_TYPE
  if c.lexeme_type == LT.FUNCTION or c.lexeme_type == LT.METHOD then
    tmn = c.word_element
    log.debug("has method under the cursor:", tmn, c.lexeme_type)

    if c.lexeme_type == LT.FUNCTION then
      body = ("%s%slocal res = M.%s()\n"):format(t, t, tmn)
    else
      body = ("%s%slocal res = %s:%s()\n"):format(t, t, c.word_container, tmn)
    end

    body = body .. ("%s%slocal exp = 1\n"):format(t, t)
    exp, res = "exp", "res"
    self:getEditor():setSearchWord(tmn) -- to jump to a generated test method name
    -- local obj_vn = ClassInfo.getVarNameForClassName(tcn_short)
  else
    exp, res = '1', '0'
  end

  body = body .. string.format("%s%sassert.same(%s, %s)", t, t, exp, res)

  opts.methods = { self:getMethodGen(tci):genTest(tmn, false, body) }
  return opts
end

---@param classname string
---@param ctype number?
function LuaGen:isClassExists(classname, ctype)
  log_debug("isClassExists cn:%s ctype:%s", classname, ctype)
  ctype = ctype or ClassInfo.CT.SOURCE
  local ci = self.lang:getClassResolver(ctype, nil, classname)
      :run():getClassInfo(false)

  local b = self.lang:isPathExists(ci)
  return b, ci
end

--
-- generate projects file (structure for given project properties
--
---@param pp table{dry_run, MAINCLASS, openInEditor}
---@return number
function LuaGen:createProjectFiles(pp)
  local mainclass = (pp or E)["MAINCLASS"]
  log_debug("createProjectFiles mainclass:%s lang.project_root:%s",
    mainclass, (self.lang or E).project_root)

  local cnt = 0
  -- if mainclass present in props but not in the disk - generate a new one
  if mainclass then
    local exists, ci = self:isClassExists(mainclass, nil)
    log_debug('mainclass:%s exists:%s ci:', mainclass, exists, ci)
    if not exists then
      if self:createClassFile(ci, pp) then
        cnt = cnt + 1
        local tci = self.lang:getOppositeClassInfo(ci)
        if self.lang:generateTestFile(tci, pp) then
          cnt = cnt + 1
        end
      end
    end
  end
  log_debug('created files:', cnt)
  return cnt
end

class.build(LuaGen)
return LuaGen

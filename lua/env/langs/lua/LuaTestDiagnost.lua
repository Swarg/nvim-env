-- 06-08-2024 @author Swarg

local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'
local du = require 'env.util.diagnostics'
local parser = require 'env.lua_parser'

local R = require 'env.require_util'
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local cjson = R.require("cjson", vim, "json")        -- vim.json


local TestDiagnost = require 'env.lang.TestDiagnost'


local class = require 'oop.class'
class.package 'env.langs.lua'
---@class env.langs.lua.LuaTestDiagnost : env.lang.TestDiagnost
---@field new fun(self, o:table?, lang:env.lang.Lang, namespace:string): env.langs.lua.LuaTestDiagnost
---@field lang env.lang.Lang
---@field namespace_id number
local C = class.new_class(TestDiagnost, 'LuaTestDiagnost', {
})


---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local E, v2s, fmt = {}, tostring, string.format

-- for diagnostics: assume there is only one assert per line
-- used to quickfix via cmd EnvHUsePassedIn from the line with diagnostic
C.IN_ROW_COL_START = 1
C.IN_ROW_COL_END = 80

--
-- process output and showDiagnostics
-- handle only sucess case, handling the pp.code ~= 0
-- the erros handling in the lua via proxy func
--
---@param output string
---@param pp table
-- [DEBUG] props passed via pp:
--   save_output=true - to save raw decoded json into tmp file
--   add_original_json=true - to add into nvim diagnostic the decoded json
---@param exit_ok boolean
---@param signal number
---@diagnostic disable-next-line: unused-local
function C:processSingleTestOutput(output, pp, exit_ok, signal)
  local bufnr, src_bufnr = (pp or E).bufnr, (pp or E).src_bufnr
  log_debug("processSingleTestOutput buf:%s src_buf:%s out:%s pp:%s exit:%s sig:%s",
    bufnr, src_bufnr, type(output), type(pp), exit_ok, signal)

  if not output or output == '' or not pp then
    return false
  end

  local json_str = output
  if output:sub(1, 1) ~= '{' then -- if has some stdout data before json
    local j = su.last_indexof(output, "\n{")
    if not j then
      C.debug_save_output('Cannot find "{" in output ', pp, output)
      return
    end
    json_str = output:sub(j, #output) -- take a json only
  end

  local ok, decoded = pcall(cjson.decode, json_str)
  if not ok then
    local err_msg = decoded
    log_debug('Cannot parse json responce: %s', err_msg)
    C.debug_save_output(err_msg or 'Parsing error', pp, output)
    return
  end

  local lines, all_passed = C.aboutResultBriefly(decoded)
  lines[#lines + 1] = ''

  C.sort_decoded(decoded)
  C.format_decoded(decoded, lines)

  -- replace json by readable report
  vim.api.nvim_buf_set_lines(bufnr, -2, -1, true, lines)

  local diagnostics = {}
  if not all_passed then
    local aoj = pp.add_original_json or false -- DEBUG and research
    diagnostics = C.buildDiagnosticsFromDecoded(decoded, src_bufnr, aoj)
  end

  self:showAllDiagnostics(diagnostics)

  -- debugging and dev
  C.save_single_test_output(decoded, pp, exit_ok, signal)
end

-- debug
---@param output string
function C.debug_save_output(msg, pp, output)
  if log.is_debug() then
    local len = output and #output or -1
    log_debug('%s, output len: ', msg, len)
    local f = fs.str2file('/tmp/pp.txt', inspect(pp), 'w')
    log_debug('See /tmp/pp.txt ', f)
    if type(output) == 'string' and output ~= '' then
      f = fs.str2file('/tmp/pp-output.txt', output, 'w')
      log_debug('See /tmp/pp-output.txt ', f)
    end
  end
end

-- triggers by "pp.save_output"
-- debug
---@diagnostic disable-next-line: unused-local
function C.save_single_test_output(output, pp, exit_ok, signal)
  if (pp or E).save_output and output then
    log_debug('save_output: %s path: %s', pp.save_output, pp.save_path)
    local dst = pp.save_path or "/tmp/busted_report.lua"
    local body = ''
    if output then                        -- decoded json to (lua tbl)
      body = 'return ' .. inspect(output) --:gsub('\\n', "\n")
      -- C.mkBufReadable(pp.bufnr, pp.decoded, lines)
    else
      body = pp.output -- raw text
    end
    if (fs.str2file(dst, body, "w")) then
      log_debug('Saved to %s', dst)
    else
      log_debug('Cannot save to %s', dst)
    end
  end
end

---@param t table
---@return table, boolean -- lines with readable info, flag - all_passed
function C.aboutResultBriefly(t)
  local lines = {}
  local successes = su.table_size(t.successes)
  local failures = su.table_size(t.failures)
  local errors = su.table_size(t.errors)
  local pendings = su.table_size(t.pendings)
  local all_passed = successes > 0 and failures == 0 and errors == 0
      and pendings == 0
  table.insert(lines, fmt(
    "%s successes / %s failures / %s errors / %s pending : %.6f seconds",
    successes, failures, errors, pendings, t.duration
  ))
  return lines, all_passed
end

-- stage-1: Json to Readable form

-- make a Decoded Json output Readable
-- for errors and failures tests
--
---@param tbl table -- the decoded from json output from the busted
---@param lines table<string>|nil
function C.format_decoded(tbl, lines)
  lines = lines or {}
  -- errors first
  for _, e in pairs(tbl.errors) do
    C.format_decoded_element('Error ->', e, lines)
    lines[#lines + 1] = ''
  end
  -- failures
  for _, f in pairs(tbl.failures) do
    C.format_decoded_element('Failure -> ', f, lines)
    -- element, message, name, trace
    lines[#lines + 1] = ''
  end
  -- show successes names?
  return lines
end

--
-- make a Decoded JSON data about one test readable
--
---@param f table -- json about one test
---@param lines table<string>
function C.format_decoded_element(prefix, f, lines)
  local short_src, ln = '', 0
  if f.element and f.element.trace then
    ln = f.element.trace.currentline
    short_src = f.element.trace.short_src
  end
  lines[#lines + 1] = fmt("%s %s @ %s", prefix, short_src, ln)
  lines[#lines + 1] = su.escape_nl(f.name) or ''
  if f.message then
    su.split_range(f.message, '\n', 1, #f.message, false, lines)
  end
  -- lines[#lines + 1] = "stack traceback:"
  local traceback
  if f.trace then
    traceback = f.trace.traceback
  elseif f.element and f.element.trace then
    traceback = f.element.trace.traceback
  end
  if traceback then
    su.split_range(traceback, '\n', 1, #traceback, false, lines)
  end
  return lines
end

-- reuse last for consecutively repeated paths
-- if src == last_src then
--   src = last_full_src
-- else
--   last_src = src
--   -- relative to full path only once for
--   if src:sub(1, 1) ~= fs.path_sep then
--     src = fs.build_path(cwd, src)
--   end
--   last_full_src = src
-- end

-- return current line number from decoded json entry
---@param e table
---@return number
local function get_currentline(e, def)
  def = def or 0
  if e then
    local trace = e.trace or ((e.element or E).trace)
    return (trace or E).currentline or 1
  end
  return def
end


---@param e table
---@return string
local function get_traceback(e)
  if e then
    if e.trace then
      return e.trace.traceback or ''
    elseif e.element then
      return (e.element.trace or E).traceback or ''
    end
  end
  return ''
end

-- sort giagnostics by linenumbers
function C.sort_decoded(decoded)
  log_debug("sort_decoded")
  if decoded then
    local sort0_by_lnum = function(a, b)
      return get_currentline(a, math.huge) < get_currentline(b, math.huge)
    end
    if decoded.failures then
      table.sort(decoded.failures, sort0_by_lnum)
    end
    if decoded.errors then
      table.sort(decoded.errors, sort0_by_lnum)
    end
  end
  return decoded
end

-- Convert json from busted into the nvim diagnostics table
--
---@param tbl table  -- decoded json output with tests results from busted
---@param bufnr number
---@return table  -- vim diagnostic
function C.buildDiagnosticsFromDecoded(tbl, bufnr, add_original_json)
  assert(type(bufnr) == 'number', 'bufnr')
  if not tbl then
    return {}
  end
  local dlist = {}

  local function _add(d)
    if d then
      local ls = dlist[bufnr] or {}
      ls[#ls + 1] = d
      dlist[bufnr] = ls
    end
  end

  if tbl.successes then
    for _, e in pairs(tbl.successes) do
      _add(C.mkDSuccessFromDecoded(e, add_original_json))
    end
  end

  if tbl.failures then
    for _, e in pairs(tbl.failures) do
      _add(C.mkDFailureFromDecoded(e, add_original_json))
    end
  end

  if tbl.errors then
    for _, e in pairs(tbl.errors) do
      _add(C.mkDErrorFromDecoded(e, add_original_json))
    end
  end

  return dlist
  -- pendings = {}, successes = { {
end

------- conversion decoded json entry into the nvim diagnostic element

---@param severity number
---@param message string
---@param lnum number
---@param original_json table|nil
local function mkDEntry(severity, message, lnum, original_json)
  assert(type(severity) == 'number', 'severity')
  assert(type(message) == 'string', 'message')
  assert(type(lnum) == 'number', 'lnum')

  local col, end_col = C.IN_ROW_COL_START, C.IN_ROW_COL_END
  local d = du.mkDiagnosticEntry(severity, message, lnum, col, end_col)
  if original_json then
    d.user_data = d.user_data or {}
    d.user_data.original_json = original_json
  end
  return d
end
--
-- convert one decoded json element(from busted) into nvim diagnosctics element
--
---@param e table - one element from decoded json from the "successes"
---@return table?
function C.mkDSuccessFromDecoded(e, add_original_json)
  if e.element then
    local msg = 'Success: ' .. v2s(e.name)
    local lnum = get_currentline(e)
    return mkDEntry(du.severity.HINT, msg, lnum, add_original_json and e)
  end
end

---@param e table one element from decoded json from failures
---@return table?
function C.mkDFailureFromDecoded(e, add_original_json)
  if e.element then
    local msg = fmt("Failure:\n%s\n%s\n%s\n",
      e.message, get_traceback(e), e.name
    )
    local lnum = get_currentline(e)
    return mkDEntry(du.severity.WARN, msg, lnum, add_original_json and e)
  end
end

---@param e table one element from decoded json from errors
---@return table?
function C.mkDErrorFromDecoded(e, add_original_json)
  if e.element then
    local lnum, traceback
    -- the e.trace.currentline gives the line number in the source code itself
    -- where the error occurred, but we need a lnum inside the test method
    -- where this function is called, which produces an error.
    -- Here this line number is extracted from the stack trace itself
    if e.trace and e.element.trace then
      local find = '<' .. v2s(e.element.trace.short_src) .. ':' ..
          v2s(e.element.trace.currentline) .. '>'

      traceback = e.trace.traceback
      if traceback then
        local i = string.find(traceback, find, 1, true)
        if i then
          local s = su.back_indexof(traceback, "\n", 1, i)
          if s then -- :220: in function
            lnum = tonumber(traceback:sub(s, i):match("%a:([%d]+):%s")) or
                e.element.trace.currentline or 1
          end
        end
      else
        -- no traceback in busted output in entry
        -- it can be cause of Lua5.1 syntax error like trying to use a continue
        lnum = tonumber(e.message:match("%a+:([%d]+):%s"))
      end
    else
      traceback = get_traceback(e)
    end
    if not lnum then
      lnum = get_currentline(e)
    end

    local msg = "Error: \n" .. v2s(e.message) .. "\n" .. v2s(traceback)
    return mkDEntry(du.severity.ERROR, msg, lnum, add_original_json and e)
  end
end

-----

-- sort giagnostics by linenumbers
function C.sortDiagnostics(diagnostics)
  log_debug("sortDiagnostics")
  if diagnostics then
    for _, bufnr_diagnostics in pairs(diagnostics) do
      table.sort(bufnr_diagnostics, function(a, b)
        return (a.lnum or math.huge) < (b.lnum or math.huge)
      end)
    end

    log_debug(inspect(diagnostics))
  end
  return diagnostics
end

-- Dev&Debug
-- Goal: replace minify json by the lua table in readable form (via inspect)
--
-- [[1 successes / 1 failures / 0 errors / 0 pending : 0.019805 seconds]]
---@param bufnr number
---@param toutput table  -- output with tests results from busted
---@param lines table|nil optional then need some extra prefix text before json
---@return string inpect(tbl)
function C.mkBufReadable(bufnr, toutput, lines)
  local text = inspect(toutput)
  lines = lines or {}
  -- split  text to lines
  for line in text:gmatch("([^\n]*)\n?") do
    if line and line ~= '' then
      table.insert(lines, line)
    end
  end
  vim.api.nvim_buf_set_lines(bufnr, 2, -1, true, lines)
  return text
end

--------------------------------------------------------------------------------

--
-- enrich params to run single test file for specific test framework
--
-- add a callback that will start parsing the output of the test results
-- and displaying them in nvim diagnostics
--
---@param params env.lang.ExecParams
---@return env.lang.ExecParams
function C:enrichedArgsRunSingleTest(params)
  log_debug("enrichedArgsRunSingleTest", params)
  local path2test = (params.args or E)[1]

  -- make relative path from cwd for short paths in the errors
  if params.cwd and path2test:sub(1, #params.cwd) == params.cwd then
    path2test = '.' .. path2test:sub(#params.cwd + 1)
  end

  -- local project_paths = "./lua/?.lua;./lua/env/?.lua;./test/?.lua"
  local args = {
    '-v',               -- verbose  see .busted default.verbose
    '--output', 'json', -- to parse busted output to nvim-diagnostic
    -- '--lpath', project_paths,
  }

  -- a way to manually specify where to look for modules when testing
  -- useful when you need to place a helper modules in the test directory.
  -- without this setting, when running tests, they simply cannot be found by
  -- require.
  -- to support testframework configuration ${proj_root}/.busted
  local tw = self.lang:getTestFramework()
  if (tw or E).getConfig then
    local lpath = ((tw:getConfig() or E).default or E).lpath
    if lpath then
      log_debug("add from .busted lpath:", lpath)
      args[#args + 1] = '--lpath'
      args[#args + 1] = lpath
    end
  end

  args[#args + 1] = path2test

  return args
end

---@return boolean
function C:usePassedIn()
  local dlist, bufnr, cur_pos = self:getBufDiagnostic()
  log_debug("usePassedIn dlist:%s bur:%s cur:%s", type(dlist), bufnr, cur_pos)

  if dlist and cur_pos then
    -- pick real bufnr from firs record
    bufnr = (dlist[1] or {}).bufnr or 0
    -- Note cur_pos starts from 1, buf in nimbuf lines from 0
    local ln = cur_pos[1]
    local col = cur_pos[2]
    local curr_da = du.filter_by_position(dlist, bufnr, ln, col)
    if curr_da then
      -- todo pick one if many
      local ns = self.namespace_id
      if parser.update_line_from_passedin(bufnr, ln, col, curr_da[1], ns) then
        du.remove_all(bufnr, dlist, { curr_da[1] })
        return true
      end
    end
  end
  return false
end

class.build(C)
return C

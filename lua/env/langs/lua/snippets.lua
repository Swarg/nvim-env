-- 15-03-2024 @author Swarg
-- snippets for EnvLineInsert for Lua

local ui = require 'env.ui'
local log = require 'alogger'
local class = require 'oop.class'
local dis_unused = "\n---@diagnostic disable-next-line: unused-local"

local insert_items = {
  -- shared: "${HANDLER_ADD_SAMPLE}",
  'local ${LINE}',
  '\n---@param ${WORD} string',                    --$VAR $TYPE  .$CURSOR acessed via code-act
  '\n---@return ',
  '\nfunction M.${WORD}(${FUNC_PARAMS})\nend\n\n', -- TODO auto create function in module
  '${TAB} print("[DEBUG] ${IDENTIFIER}:", require"inspect"(${IDENTIFIER}))\n',
  "\nlocal ${MODULE} = require '${MODULE}'",
  '  log_debug("${WORD}")\n',                                                                -- 7
  '${ADD_PKG2WORD}',                                                                         -- 8 helper to update "oop"
  '  ${WORD} = function(obj)\n    return\n  end,\n',                                         -- 9
  '\n  it("${WORD}", function()\n    local f = M.${WORD}\n    assert.same("", f())\n  end)', -- 10
  "  if type(${WORD}) == 'table' then\n    \n  end\n",
  "  assert(type(${IDENTIFIER}) == 'table', '${IDENTIFIER}')\n",
  "\n    local exp = 1",
  '${MODULE}',
  '\nlocal ${WORD} = [[\n]]\n',
  '${DATE} ${TIME}',
  '--[[\n${LINE}\n]]\n',                               -- 17
  "'${IDENTIFIER}:', ${IDENTIFIER}",                   -- 18
  '${HANDLER_SUBSTITUTE_VALUE}',                       -- 19
  '    if 0 == 0 then return end\n',                   -- 20
  "assert.same(1, ${LINE})",                           -- 21
  '    assert.same("X", ${WORD})\n',
  '    local res = ${WORD}\n  assert.same("", res)\n', -- 23
  '    assert.same("", ${VARS_TOSTRING})\n',
  '${${WORD}}',
  '\n---@field ${WORD} function',
  '${NEW_TESTING_MODULE}',
  "set own pattern via :EnvLineInsert mappings --key 28 --value 'my-pattern'", -- 28
  "function ${CLASS}:${WORD}()\n  return self.${PREV_WORD}:${WORD}()\nend\n",  -- 29
  '\n-- ${DATE} @author ${AUTHOR} \nlocal M = {}\n--\nreturn M',               -- 30
  "${TOGGLE_BOOL}",                                                            -- 31
  "'${WORD}'",                                                                 -- 32
  "'${SWORD}'",                                                                -- 33
  '{${SWORD}}',                                                                -- 34
  "['${SWORD}']",                                                              -- 35
  "  dprint(${WORD})\n",                                                       -- 36
  "  require'dprint'.enable()\n",                                              -- "local dprint = require('dprint').mk_dprint_for(M, true)\n",
  "  require'alogger'.fast_setup()\n",
  'tostring(${WORD})',                                                         -- 39
  "\n${DEF_LOCAL}",
  "\n${TAB} local ${WORD} = ''",                                               -- 41
  "${TAB} error('Not implemented yet')\n",
  "['${WORD_UTF8}'] = '${WORD_UTF8}',",
  '(${SWORD} or E)',
  'v2s(${SWORD})', -- local v2s = tostring
  '',
  '',
  'M.${WORD}',
  "\nlocal ${WORD} = M.${WORD}",
  "${TAB} :desc(\"\")\n${TAB} :cmd(\"\")\n",
  "local class = require 'oop.class'\nclass.package '${PACKAGE}'\n",
  dis_unused .. "\nlocal E, v2s, fmt, match = {}, tostring, string.format, string.match",
  "-- dependencies:\n--   - luarocks: busted, lhttp, db-env\n", -- for LuaTester
  "\nlocal log_debug = log.debug",
}

local U = require 'env.lang.utils.insertion'
local Editor = require 'env.ui.Editor'
local lua_parser = require 'env.lua_parser'
local lapi_consts = require 'env.lang.api.constants'
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE


local insert_mappings = {
  -- @param type name
  [2] = { -- local mappings = {
    ['self'] = { 'self' },
    table = { 't', 'tbl', 'o', 'opts', 'obj', 'ctx', 'token', 'cbi', 'lines',
      'token', 'tokens', 'tree', 'tokenizer', 'cursor', 'response', 'position',
      'headers', 'state', 'node', 'clazz',
    },
    ['function'] = { 'callback', 'handler', 'predicate', 'consumer', 'supplier' },
    number = { 'n', 'i', 'bufnr', 'lnum', 'lnum_end', 'winnr', 'offset',
      'off', 'fd', 'x', 'y', 'z', 'x1', 'y1', 'x2', 'y2', 'col', 'limit', 'pos',
      'depth',
    },
    Cmd4Lua = { 'w' },
    ['stub.os.Args'] = { 'a' },
    boolean = { 'enable', 'quiet', 'dry_run', 'persistent' },
    -- ?
    ['env.lang.ClassInfo'] = { 'classinfo', 'ci' },
    ['env.lang.Lang'] = { 'lang' },
    ['env.ui.Editor'] = { 'editor' },
    ['env.ui.Context'] = { 'context' },
    ['env.ui.Selection'] = { 'selection' },
    ['env.ui.ObjEditor'] = { 'obj_editor' },
    ['env.lang.ExecParams'] = { 'exec_params' },
    --
    ['stub.os.UG'] = { 'ug' },
    ['stub.os.OSystem'] = { '_os' },
    ['stub.os.Proc'] = { 'proc' },
    --
    ['olua.util.Iterator'] = { 'iter', 'iterator' },
    ['olua.util.lang.AParser'] = { 'parser' },
  },
  [24] = nil, -- assert multiresult
  [40] = {},
}


local M = {}
--------------------------------------------------------------------------------
--                   Subscribers on Insection Item
--------------------------------------------------------------------------------


-- subscriber for insertion '\n---@param ${WORD} string' - aimed to auto
-- substitute the type instead the default "string"
---@param key string
---@param cbi table
---@param str string
---@param word string key to be replaced in this case is "${WORD}"
---@diagnostic disable-next-line: unused-local
function M.subscriber_param(key, cbi, str, word)
  -- local idx = U.get_subscriber_index(M.subscriber_add_pkg2word, cbi.subscribers)
  local idx = assert(cbi.index, 'snippet index')
  -- idx == 2 by definition in mapping table but to support swap items on the fly:
  -- there is provision for searching for matches during operation
  -- index of subscriber corresponds index of the mappings(subscriber state)
  assert(idx, 'expected index for subscriber')

  local map = assert((cbi.mappings or E), 'insert_line_mappings')[idx] or {}
  local replace = U.map_word(map, word)
  if replace then
    str = str:gsub('string', replace)
  end
  return str
end

-- add full package for ShortClass name under the cursor
-- '${ADD_PKG2WORD}',
-- lua vim.api.nvim_del_user_command('EnvLineInsert'); package.loaded["env.command.line_insert"] = nil; require("env.util.commands").reg_handler('EnvLineInsert', "env.command.line_insert")
---@param klass string (word)
---@param cbi table
---@diagnostic disable-next-line: unused-local
function M.subscriber_add_pkg2word(key, cbi, str, klass)
  local lang = cbi.ext or 'lua'
  assert(klass, 'word - klass')

  -- todo lang
  _G.env_pkg_mappings = _G.env_pkg_mappings or {}
  local map = _G.env_pkg_mappings
  map[lang] = map[lang] or {}
  local pkg = map[lang][klass]

  local cline = (cbi or {}).current_line or ''
  -- return MyClass -> return class.build(MyClass)
  if cline:match('^%s*return ' .. klass) then
    return 'class.build(' .. klass .. ')'
  end

  ---*class env.lang.oop.Field: oop.Object
  local cn = cline:match('^%-%-%-@class ([%w%.]+)%s*:?')
  if cn then
    log.debug('cursor in line with @class:%s pkg-in-map:%s', cn, pkg)
    local scn = class.get_short_class_name(cn)
    if scn == klass then
      log.debug('cursor under the short-class-name of defined class')
      local cn_has_package = cn:find('%.')
      if not pkg then -- no package for this scn in map
        if cn_has_package then
          log.debug('take package from current line')
          pkg = class.get_package_name(cn)
        end
        local updated_pkg = false
        if pkg == nil then
          -- take from class.package 'PACKAGE_NAME'
          pkg = U.find_class_package(cbi.ext, cbi.bufnr)
          updated_pkg = pkg ~= nil
        end
        if scn and pkg then
          map[lang][scn] = pkg
          log.debug("add mappings %s -> %s", scn, pkg)
          if not updated_pkg then
            log.debug('pkg taken from currline no changes needed')
            return
          end
        end
        -- return nil -- no replace
      elseif cn_has_package then
        local curr_pkg = class.get_package_name(cn)
        if curr_pkg == pkg then
          log.debug('no changes needed. packages are same:', pkg)
          return nil
        else
          log.debug('diff packages in map:%s curr:%s', pkg, curr_pkg)
          -- fix diff manualy
        end
      end
    end
  end

  if not pkg then
    local msg = 'enter the package for class [' .. tostring(klass) .. ']: '
    pkg = ui.ask_value(msg)
    if pkg then
      map[lang][klass] = pkg
      log.debug("add mappings %s -> %s", klass, pkg)
    end
  end
  if pkg then return pkg .. '.' .. klass end

  return nil -- no replace
end

--
-- local x, y = get_coords()     -->  "tostring(x)..'|'..tostring(y)"
--       ^
--
---@diagnostic disable-next-line: unused-local
function M.subscriber_vars_tostring(key, cbi, str, word)
  local ret = ''
  local line, col = cbi.current_line, cbi.cursor_col
  local pend = string.find(line, '=', col, true) or (#line + 1)
  line = line:sub(col, pend - 1)

  for s in string.gmatch(line .. ',', '([^,]+)') do
    local varname = s:match('%s*([^%s]+)%s*')
    if varname ~= 'local' then
      if #ret > 0 then ret = ret .. ' .. "|" .. ' end
      ret = ret .. 'tostring(' .. varname .. ')'
    end
  end
  return ret
end

-- get the name of the most used module
---@param map table{function=module}
local function get_hot_module(map)
  local max, name, t = 0, '', {}
  -- aliase modname
  for _, modname in pairs(map) do
    t[modname] = t[modname] or 0
    t[modname] = t[modname] + 1
  end
  for modname, cnt in pairs(t) do
    if cnt > max then max, name = cnt, modname end
  end
  return name
end

--
-- local function_name = MODULE_FROM_MAP.function_name
---@diagnostic disable-next-line: unused-local
function M.subscriber_def_local(key, cbi, str, word)
  local idx = assert(cbi.index, 'snippet index')
  local map = assert((cbi.mappings or E), 'insert_line_mappings')[idx] or {}
  if word then
    if not map[word] then
      local hot = tostring(get_hot_module(map))
      local v = ui.ask_value('module for ' .. tostring(word) .. ': ', hot)
      if v and v ~= '' then
        map[word] = v
      else
        return ''
      end
    end
    local mod = map[word]
    return string.format('local %s = %s.%s', word, tostring(mod), word)
  end
  return ''
end

--
-- replace variable by its own value, defined in prev lines(16)
-- Example:
--
--    local variable_name = 'text to be substitute into function'
--    assert.same(exp, variable_name) --
--
-- will be updated to:
--
--    assert.same(exp, 'text to be substitute into function')
--
function M.handler_substitute_value()
  local editor = Editor:new()
  local lexeme = LEXEME_TYPE.VARNAME
  local vn, errmsg = editor:resolveWords():getResolvedWord(lexeme)
  if not vn then
    return false, errmsg
  end
  -- lua_parser.find_var_definition
  local t, err = editor.ctx:find_var_definition0(lua_parser, vn, 16)
  if not t or not t.expr then
    return false, err
  end

  if editor:replaceCurrentWord(t.expr) then
    editor:setLines(nil, t.lnum, t.lnum_end, {}) -- replace variable definition
    return true
  end

  return false, 'cannot replace current word'
end

-- for [0]: "${HANDLER_ADD_SAMPLE}",
local samples = {
  -- lua test framework config
  busted = {
    'return {',
    '  _all = {',
    '    coverage = false,',
    '  },',
    '  default = {',
    '    coverage = false,',
    '    verbose = true,',
    '    ROOT = {"test"},',
    '    lpath = "lua/?.lua;test/?.lua;",',
    '    -- ["exclude-pattern"] = "only-for-files-without-dir",',
    '  }',
    '}'
  },
}
--------------------------------------------------------------------------------

-- subscribers that works based on theinsert_line_mappings
local insert_handlers = {
  [2] = M.subscriber_param,
  [8] = M.subscriber_add_pkg2word,
  [24] = M.subscriber_vars_tostring,
  [40] = M.subscriber_def_local,
  SUBSTITUTE_VALUE = M.handler_substitute_value,
}

---@param reg function
function M.add(reg)
  reg('lua', insert_items, insert_mappings, insert_handlers, samples)
end

--[==[
 old
  --'\n  function M.call_${WORD}()\n    M.${WORD}()\n  end\n\n', -- 13 for EnvCallFunc
  --'\n-- lua print(require("${MODULE}").$FUNCTION())',

  [[
describe('${WORD}', function()
  local cases = {
    -- input   exp1, exp2
    { '7+9', {'16', false } },
  }
  for i, case in pairs(cases) do
    describe('case: ' .. i, function()
      it("input: [" .. case[1] .. "]", function()
        local input, exp = case[1], case[2]
        local r1, r2 = M.${WORD}(input)
        assert.same(exp[1], r1)
        assert.same(exp[2], r2)
      end)
    end)
  end
end)
    ]],

-- 4 over function name - to create func body
--'\n---@diagnostic disable-next-line', covered by code-action <leader>la
-- run function under cursor - acessed via hot key <leader>zm <leader>zv
  -- mk wrapper delegate method call to own instance  - 28
]==]

return M

-- 08-09-2024 @author Swarg
-- snippets for EnvLineInsert for JavaScript
--
--
--
--

local M = {}

local insert_items = {
  '',
  '',
  '',
  '',
  '',
  '',
  "${TAB} console.log('${IDENTIFIER}:('+typeof(${IDENTIFIER})+')', ${IDENTIFIER});\n",
  "${HANDLER_NEW_ESM_FILE}", -- 8 create new ECMAScript Module (file)
  '',
  '',
  '',
  '',
}

local insert_mappings = {
}

local log = require 'alogger'
local su = require 'env.sutil'
local fs = require 'env.files'
local Editor = require 'env.ui.Editor'
local common = require 'env.lang.utils.common'

--
-- import './src/some.js'
-- import data from './src/assets/07.json'
function M.handler_new_esm_file()
  local editor = Editor:new()
  local cl = su.trim(editor:getCurrentLine())
  local from, fn = string.match(cl, "^import%s+([^']*)%s*'([^']+)'$")
  if not fn then
    -- to support Vite glob import:
    -- const modules = import.meta.glob('./src/10/*.js')
    fn = string.match(cl, "import%.meta%.glob%('([^']+)'")
    if fn and string.find(fn, '*', 1, true) then
      fn = string.gsub(fn, '%*', '0') -- replace * by * to crearte stub file name
    end
  end

  if not fn then
    return false, 'expected line with import .* <path>'
  end
  local bufname = editor:getCurrentFile()
  if not bufname then
    return false, 'no current file'
  end
  local dir = fs.get_parent_dirpath(bufname)
  log.debug("dir", dir)
  local path = fs.build_path(dir, fn)
  if fs.file_exists(path) then
    return false, 'file already exists'
  end
  local dir0 = fs.get_parent_dirpath(path)
  if dir0 and not fs.dir_exists(dir0) then
    fs.mkdir(dir0)
  end
  if fs.write(path, from or '') then
    log.debug('create new file', path)
    editor:open(fn)
  end
  return true
end

local insert_handlers = {
  NEW_ESM_FILE = M.handler_new_esm_file,
  MK_FILE_REF = common.handler_mk_file_ref,
}



---@param reg function
function M.add(reg)
  reg('js', insert_items, insert_mappings, insert_handlers)
end

return M

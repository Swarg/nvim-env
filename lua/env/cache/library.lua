-- 14-11-2023 @author Swarg
local M = {}
--
-- [package][version] --> table
local cache = {}

function M.get(lang, pkg, ver)
  assert(type(lang) == 'string', 'lang')
  assert(type(pkg) == 'string', 'pkg')
  assert(type(ver) ~= nil, 'ver')

  cache[lang] = cache[lang] or {}
  cache[lang][pkg] = cache[lang][pkg] or {}

  local created = not cache[lang][pkg][ver]
  cache[lang][pkg][ver] = cache[lang][pkg][ver] or {}

  return cache[lang][pkg][ver], created
end

function M.get_cache()
  return cache
end

function M.clean_all()
  cache = {}
end

---@param lang string
---@param pkg string
---@param ver string? if version not specified remove all for this package
function M.clean(lang, pkg, ver)
  assert(type(lang) == 'string', 'lang')
  assert(type(pkg) == 'string', 'pkg')

  if cache[lang] and cache[lang][pkg] then
    if ver ~= nil then
      if cache[lang][pkg][ver] then
        cache[lang][pkg][ver] = nil
        return true
      else
        return false
      end
    end
    cache[lang][pkg] = nil
    -- TODO autoclean "parent" i.e. pkg and lang if no more packages in it
    return true
  end
  return false
end

return M

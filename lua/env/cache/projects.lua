-- Project_Roots Cache
-- Goal:
-- - used as middlewire in cases when you need find project-root by given
--   markers(a set of file-names like .git Makefile pom.xml build.gradle, etc
--   the main goal is to cache already checked directories and returns known
--   project-roots without access to real filesystem
-- - Reduce fs access for each file opening
-- - Reduce the number of requests to the file system for each open source file
--   Designed to be used with the nvim ftplugin (java)
--
-- - supporting per-dirirectory settings described in .nvim-env.yml file
--   - the main purpose of which is to provide the ability to define
--     subdirectories of a relative parent directory as the roots of independent
--     projects, regardless of the fact that they do not contain any markers
--     (for small toy, snippets or education projects without any build scripts)
--     (feature introduced 18-07-2024)
--     can be usefull in nvim-configs for lsp servers like jdlsp(ftplugin)
--
-- 29-05-2023 @author Swarg
--

--
local fs = require('env.files')
local su = require("env.sutil")
local log = require('alogger')

local ok_lyaml, lyaml = pcall(require, "lyaml")

local M = {}
-- Cache for storing projects_roots
-- keys: project_roots, values: table with subprojects
-- subprojects - is a list of key:boolean-value
-- Example:
-- {
--   ['/home/dev/singlproj'] = { subprojects = { src = false },
--   ['/home/dev/multiproj'] = { subprojects = { app = true, util = true },
-- }
-- Another one: (Simple)
-- {
--   ["/d/Dev/2023/java/mc/mclogs"] = { subprojects = {} }
-- }
--
---@private
M.projects_cache = {}
M.statistics = { calls = 0, fs_access = 0 }

--------------------------------------------------------------------------------
--
--                       Per-Directory settings.
--
-- Goal: keep multiple subpdirectories without any markers(file like .git)
-- as a project_roots for simple|toy|enducation projects without any build system
-- configs per directory
-- filename -> luatable (config)
local dir_settings_cache = {}
local dir_conf_filename = '.nvim-env.yml'

function M.get_dir_conf_basename() return dir_conf_filename end

--------------------------------------------------------------------------------
--           To keep state on lua-code runtime reloading (hotswap)

function M.dump_state()
  _G.__env_projects_cache_state = {
    statistics = M.statistics,
    projects_cache = M.projects_cache,
    dir_settings_cache = dir_settings_cache,
  }
end

function M.restore_state()
  local state = _G.__env_projects_cache_state
  _G.__env_projects_cache_state = nil
  M.statistics = state.statistics
  M.projects_cache = state.projects_cache
  dir_settings_cache = state.dir_settings_cache
end

--------------------------------------------------------------------------------


local E, log_debug, log_trace = {}, log.debug, log.trace
local ensure_dir = fs.ensure_dir
local join_path = fs.join_path
local get_parent_dirpath = fs.get_parent_dirpath
local get_parent_dirname = fs.get_parent_dirname
local path_sep = fs.path_sep

--------------------------------------------------------------------------------

function M.full_cleanup()
  log_debug("full cleanup")
  M.projects_cache = {}
  M.statistics = { calls = 0, fs_access = 0 }
  dir_settings_cache = {}
end

--
-- 'parent/child'    -->   'parent', 'child'      (multiproject with "childs")
-- 'simple_project'  -->   'simple_project'
--
---@return string parent
---@return string child  (subproject)
function M.parse_project_name(pname)
  local parent, child = string.match(pname, "^([^/\\]+)[/\\]([^/\\]*)")
  parent = parent or pname
  if parent == '/' then parent = nil end
  if child == '' then child = nil end
  return parent, child
end

---@param path string
---@param markers table {.git pom.xml build.gradle}
---@return string|nil
function M.get_project_root_marker(markers, path)
  if type(markers) == 'table' and path then
    M.statistics.fs_access = M.statistics.fs_access + 1
    return fs.find_marker(path, markers)
  end
  return nil
end

local function fs_find_root(markers, bufname)
  M.statistics.fs_access = M.statistics.fs_access + 1 -- access to real fs
  local root, marker = fs.find_root(markers, bufname)
  log_trace('fs_find_root bufname:%s markers:%s found:%s (%s) fs_access:%s',
    bufname, markers, root, marker, M.statistics)
  return root, marker
end

-- nearest child directory without subdirs
---@param parent string
---@param child string
---@return boolean
function M.is_nearest_subdir(parent, child)
  if #parent < #child and su.starts_with(child, parent) then
    local child_len = #child
    if child:sub(-1, -1) == fs.path_sep then
      child_len = child_len - 1
    end
    local remain = string.sub(child, #parent + 1, child_len)
    return #remain > 1 and string.find(remain, fs.path_sep) == nil
  end
  return false
end

-- Case: some subprojects has opened and placed to cache before its root_project
-- then when their root is added to the cache, they are moved to it:
-- Absorbe own parts (subprojects) already exists in M.project_cache and
-- place to subprojects list. Its Possible because here
-- this is possible due to the fact that subprojects cannot have own subprojects
-- The depth of subprojects is no more than two
---@param cache table
---@param root_dir string -- always ends with "/"(path sep)
function M.absorbe_subprojects(cache, root_dir)
  if not cache or not root_dir or root_dir == '' then
    return
  end
  root_dir = fs.ensure_dir(root_dir)
  if not cache[root_dir] then
    return
  end

  for path, _ in pairs(cache) do
    if M.is_nearest_subdir(root_dir, path) then
      -- if #root_dir < #path and su.starts_with(path, root_dir) then
      local dirname = path:sub(#root_dir + 1) -- /dev/mproj/app/ -> app/
      if dirname:sub(-1, -1) == '/' then
        dirname = dirname:sub(1, -2)          -- app/ --> app
      end

      -- Found SubProject
      log_debug('absorbe_subprojects %s sub:%s', root_dir, dirname)
      local e = cache[root_dir]
      e.subprojects[dirname] = true
      cache[path] = nil
    end
  end
end

-- Smart Search for Project Roots for given path based cache
-- Behavior:
-- check all cached roots, find the corresponds one.
-- extract next after root sub dirname and check is this dir contains subproject
-- if so - then return it path else return correspond root
-- Test is subdirname has subproject based on known dirs for this root.
-- Already checked dirnames stored in filed subprojects (list of dirname:boolean)
--
---@param markers table<string>
---@param bufname string full path to file
---@return string? directory
---@return string? root_marker
function M.find_root(markers, bufname)
  log_trace("find_root file:%s markers:%s", bufname, markers, M.statistics)
  if type(markers) ~= 'table' or not bufname or bufname == "" then
    return nil
  end
  -- print('# Find Root in Cache for', bufname)
  M.statistics.calls = M.statistics.calls + 1
  -- check cahed project_roots
  local root, marker = M.find_root_in_cache(markers, bufname)
  if root then
    root = fs.ensure_dir(root)
    log_debug('Found Cached root_dir: %s for %s', root, bufname, M.statistics)
    return root
  end

  root, marker = fs_find_root(markers, bufname) -- fs_access++
  if root and root ~= '' and root ~= '.' and root ~= '/' then
    root = fs.ensure_dir(root)
    M.projects_cache[root] = {
      subprojects = { -- to store already checked sub dir names
      }
      -- true - dirname contains subproject, false - no subproject
    }
    local next_dir = fs.get_next_dirname_after_root(root, bufname)
    if next_dir then
      -- remember that there are no markers in this sub-directory
      M.projects_cache[root].subprojects[next_dir] = false
    end
    M.absorbe_subprojects(M.projects_cache, root)
    log_trace('projects_cache[%s]:%s', M.projects_cache[root])
  end

  if M.readDirConf(root) then
    root, marker = M.checkDirConf(bufname, root, marker)
  end

  log_debug('found root: %s markers:%s', root, marker)
  return root, marker
end

---@param path string
---@return boolean
function M.add_project_root(path)
  log_debug("add_project_root", path)
  log_trace("projects_cache before:", M.projects_cache)
  assert(type(path) == 'string' and path ~= '', 'valid path')

  if path and path ~= '' and path ~= '/' then
    path = fs.ensure_dir(path) -- ends with /
    local parent_root_dir, subproj = M.get_parent_root_dir(path)
    log_trace("parent_root_dir:%s subproj:%s", parent_root_dir, subproj)
    if parent_root_dir ~= nil then
      -- /home/dev/mproj/  /home/dev/mproj/src/test/_lua/
      -- to support embeded LuaTester inside annother Lang
      if not M.is_nearest_subdir(parent_root_dir, path) and
          parent_root_dir ~= path then
        log_debug("not nearest_subdir", parent_root_dir, path)
        M.projects_cache[path] = { subprojects = {} }
        (M.projects_cache[parent_root_dir] or {}).embeded = path
        log_trace("projects_cache after:", M.projects_cache)
        return true
      end

      if subproj ~= nil and not M.get_subproj_entry(parent_root_dir, subproj) then
        return M.set_subproj_entry(parent_root_dir, subproj, true)
      end
      log_debug('Attempt to rewrite already cached root_dir:', parent_root_dir)
      return false
    end
    log_debug('Caching new project_dir: ', path)
    M.projects_cache[path] = { subprojects = {} }
    M.absorbe_subprojects(M.projects_cache, path)
    log_trace("projects_cache absorbed:", M.projects_cache)
    return true
  end
  return false
end

---@param parent string? root_dir (full path)
---@param name string?   subproject dirname (one word)
function M.get_subproj_entry(parent, name)
  if parent and name then
    local e = ((M.projects_cache[parent or 0] or E).subprojects or E)[name or 0]
    log_debug("get_subproj_entry %s %s entry:%s", parent, name, e)
    return e
  end
  return nil
end

---@return boolean
---@param entry any not null
function M.set_subproj_entry(parent, name, entry)
  assert(entry ~= nil, 'expected not nil entry')
  if parent and name then
    log_debug("set_subproj_entry parent:%s name:%s entry:%s", parent, name, entry)
    log_trace("projects_cache before:", M.projects_cache)
    M.projects_cache[parent] = M.projects_cache[parent] or { subprojects = {} }
    M.projects_cache[parent].subprojects[name] = entry
    log_trace("projects_cache after:", M.projects_cache)
    return true
  end
  return false
end

--
-- Get parent root dir FROM CACHE
--
-- even if the specified path belongs to a subproject, return the root of the
-- parent project
--
-- WARNING: directories are expected to end with slash `/` (PathSeparator)
-- and if there is no slash at the end then it will be considered as a file
--
---@param path string
---@return string?
---@return string? subproj name
function M.get_parent_root_dir(path)
  local fn_len = #path
  if M.projects_cache[path] then return path end -- fast

  for root, _ in pairs(M.projects_cache) do
    -- case-1: /dev/proj/ and /dev/proj   or   /dev/proj/ and /dev/proj/
    if (fn_len + 1 == #root and su.starts_with(root, path)) then
      return root, nil
    end
    if su.starts_with(path, root) then
      local subproj = fs.get_next_dirname_after_root(root, path)
      return root, subproj
    end
  end
  return nil
end

--
---@param path string full path to some file
---@return string? the root_dir for cached project_root
function M.get_cached_root_dir_for(path)
  local root_dir = M.get_parent_root_dir(path)
  if root_dir then
    local subproj = fs.get_next_dirname_after_root(root_dir, path)
    if M.get_subproj_entry(root_dir, subproj) then
      return join_path(root_dir, subproj) .. path_sep
    end
    return root_dir
  end
  return nil
end

--
--
---@return table
function M.get_all_roots()
  local t = {}
  for dir, _ in pairs(M.projects_cache) do
    t[#t + 1] = dir
  end
  return t
end

--
-- find by full path to file (bufname) the root of the project in cache
-- with subprojects supports.
--
-- when you need to find only already existed in cache projects without checking
-- subprojects(via access to filesystem) specify markers = nil
-- In this case, only the path to an existing project_root will be returned
-- without additional search for a subproject
--
---@param markers table?        markers for find subproject
---@param fn string             bufname of opened file (full path)
---@return string? dir
---@return string? marker
function M.find_root_in_cache(markers, fn)
  log_debug("find_root_in_cache", fn, markers, M.statistics)
  log_trace('projects_cache:', M.projects_cache)
  local fn_len = #fn

  for root, entry in pairs(M.projects_cache) do
    log_trace('itrecation root:%s entry:%s', root, entry)
    local root_len = #root
    -- case-1: /dev/proj/ and /dev/proj   or   /dev/proj/ and /dev/proj/
    if root == fn or (fn_len + 1 == root_len and su.starts_with(root, fn)) then
      return root -- case-1
    end
    if su.starts_with(fn, root) then
      local tail = fn:sub(root_len + 1, fn_len) or ''

      -- log_trace('# Root Candidate for %s is %s tail:(%s)', fn, root, tail)
      -- case=2: /dev/proj/ and /dev/proj/file  -- sub element is filename not dir
      local e = string.find(tail, fs.path_sep, 1, true)
      if not e then
        -- log_trace('# case-2: Path with deep1 and file', root)
        return root
      end
      local subdir = tail:sub(1, e - 1)
      -- log_trace('# Check SubDirName ', subdir, root)
      assert(type(entry.subprojects) == 'table')

      local is_subproject = entry.subprojects[subdir]
      -- to support LuaTester embeded into another Langs
      if entry.embeded and su.starts_with(fn, entry.embeded) and
          M.projects_cache[entry.embeded] then
        log_debug('found embeded (sub)project_root', entry.embeded)
        return entry.embeded
      end
      if is_subproject == false then
        return root
        --
      elseif is_subproject == true then
        return join_path(root, subdir) .. path_sep
        --
      elseif is_subproject == nil then
        if not markers then
          -- case: find only already existed in cache without subprojects checks
          return root
        end
        -- log_trace('# Check subdir', subdir)
        local subpath = join_path(root, subdir) .. path_sep
        -- log_trace('# Find Marker Check FS for SubDirName ', subpath)
        local marker = M.get_project_root_marker(markers, subpath) -- fs_access ++
        if marker then
          log_debug('# Found New SubProject Root_dir', subpath)
          entry.subprojects[subdir] = true
          return subpath, marker
        else
          -- log_trace('# %s is not SubProject, Project_Root is %s', subpath, root)
          entry.subprojects[subdir] = false
          return root
        end
      end
    end
  end
  return nil -- not found
end

--
---@param dir string
function M.clear_enrty(dir)
  M.projects_cache[dir] = nil

  if dir:sub(-1, -1) == fs.path_sep then
    M.projects_cache[dir:sub(1, -2)] = nil
  else
    M.projects_cache[dir .. fs.path_sep] = nil
  end
end

--
-- get cached root_dir for given project_name
-- e.g "simple_proj_root_dir"  or "multiproj_root/subproject"
--
-- Return the root of the given Project Name (Project Directory Name)
-- Used for goto-definition into system and libs classes (jdt://)
--
---@param project_name string -- the directory name of the project
---@param or_any boolean?     -- when any project root is suitable
function M.get_root_by_name(project_name, or_any)
  log_debug("get_root_by_name project: %s or_any:%s", project_name, or_any)
  if project_name and project_name ~= "" then
    local last_root = nil
    or_any = or_any or false

    local pname, subproj = M.parse_project_name(project_name)
    local cache = M.projects_cache
    pname = fs.ensure_dir(pname)

    for root, entry in pairs(cache) do
      last_root = root
      if su.ends_with(root, pname) then
        if not subproj or subproj == '' then
          return root
        end
        if (entry.subprojects or E)[subproj] then
          return join_path(root, subproj) .. path_sep
        end
      end
    end
    if or_any then
      return last_root
    end
  end
  return nil
end

--
-- for jdtls to define workspace dir for subproject with parent project
-- used also with flat-structure projects powered by .nvim-env.yml
--
-- get full project name by given project_root
-- if current project is subproject inside parent project return
-- parentproject_name/subproject_name
-- overwise return subproject_name
-- Check is subproject supports: Gradle(build.gradle and settings.gradle)
---@param project_root string full path to project root
---@return string|nil project_name
function M.get_full_project_name(project_root)
  log_debug("get_full_project_name", project_root)
  if not project_root or project_root == '' then
    return nil
  end
  local project_name
  project_root = ensure_dir(project_root)

  local nomarkers = M.isProjectWithoutMarkers(project_root)

  if nomarkers then
    project_name = fs.get_dirname_with_parentdir(project_root) -- parent/subproj
  else
    -- the idea here is to check if the current path is a child subdirectory
    -- inside another root project and if so then return the full project name
    -- along with the parent like "parent/child"
    local root, subproj = M.get_parent_root_dir(project_root) -- from Cache
    if root and subproj then
      local parent = get_parent_dirname(root)
      project_name = join_path(parent, subproj)
    else
      -- check for parent project by common marker (.git directory)
      project_name = get_parent_dirname(project_root)
      local parent_dir = get_parent_dirpath(project_root)
      local marker = join_path(parent_dir, '.git') -- settings.gradle
      if parent_dir and fs.file_exists(marker) then
        M.add_project_root(parent_dir)
        local parent = get_parent_dirname(parent_dir)
        subproj = get_parent_dirname(project_root)
        project_name = join_path(parent, subproj)
      end
    end
  end
  log_debug("get_full_project_name name:'%s' dir:%s nomarkers:%s",
    project_name, project_root, nomarkers)
  return project_name
end

-- Project dir name from auto opened bufname with jdt://
-- jdt://contents/java.base/java.lang/System.class?=project_a/
--                                                  ^^^^^^^^^
-- %5C/usr%5C/lib%5C/jvm%5C/java17%5C/lib%5C/jrt-fs.jar%60java.base
-- =/javadoc_location=...
function M.get_project_name_from_jdt(bufname)
  local name = string.match(bufname or '', 'jdt://[^=]+=([^\\/]+).*')
  log_debug("get_project_name_from_jdt bufname:%s projname:%s", bufname, name)
  return name
end

--------------------------------------------------------------------------------
--                     Per-Directory Settings
--------------------------------------------------------------------------------

function M.getDirConfings()
  return dir_settings_cache
end

-- drom DockerCompose
local function parse_yaml(fn)
  -- local ok, lyaml = pcall(require, "lyaml")
  if not ok_lyaml then
    log_debug('lyaml luarock-package is not installed', lyaml) -- err
    return nil
  end

  if fn and fs.file_exists(fn) then
    local t = lyaml.load(fs.read_all_bytes_from(fn), { all = true })
    return type(t) == 'table' and t or nil
  end
  return nil
end

local function normalize_dir_name(dir)
  local dir0 = dir
  if dir and su.ends_with(dir, dir_conf_filename) then
    dir0 = string.sub(dir, 1, - #dir_conf_filename)
  end
  return fs.ensure_dir(dir0 or '')
end

--
-- Goal: provide settings for bounch of flat projects without any marker files
-- Per workspace or directory configs
--
-- mylist:
--   - 'some text value item 1'
--   - 'some text value item 2'
--
---@param dir string?
---@param nocache boolean?
---@return table?
function M.readDirConf(dir, nocache)
  log_debug("readDirConf %s (lyaml:%s) nocache:%s", dir, ok_lyaml == true, nocache)
  if not ok_lyaml then return nil end -- lyaml not installed

  local dir0 = normalize_dir_name(dir)

  local entry
  if not nocache then
    entry = dir_settings_cache[dir0 or false]
  end

  if dir0 and not entry then -- todo check lmtime
    local fn = fs.join_path(dir0, dir_conf_filename)
    local exists, lm = fs.is_file(fn)
    log_debug("readDirConf %s exists:%s lmtime:%s", fn, exists, lm)
    if exists then
      local t = parse_yaml(fn)
      if t then
        entry = { conf = t, lmtime = lm }
        if not nocache then
          dir_settings_cache[dir0] = entry
        end
      end
    end
  end
  return entry
end

--
-- change project_root based on per-dir-conf (for flat projets)
--
---@param dir_settings table?
---@param filename string?    full filename for which needs to find project root
---@param project_root string?
---@param marker string?
---@return string? project_root
---@return string? marker
function M.updateRootDir(dir_settings, filename, project_root, marker)
  if type(dir_settings) == 'table' and project_root then
    local conf = (dir_settings or E)[1] or E
    local roots = conf.project_roots or conf['project-roots']

    local subdir = fs.get_child_dirname(project_root, filename)
    log_debug("checkDirConf subdir:'%s' fn:%s root:%s Conf:%s roots:%s",
      subdir, filename, project_root, conf ~= nil, #(roots or E))
    local subprojects = (M.projects_cache[project_root] or E).subprojects or E

    if type(roots) == 'table' then
      -- check is given subdir are independed project (without any marker)
      for _, item in ipairs(roots) do
        subprojects[item] = true -- now the cache knows that this is a subproject
        log_trace('item:%s subdir:%s', item, subdir)
        if item == subdir then
          local new_root = fs.join_path(project_root, subdir)
          log_debug('Apply settings from dirconf:', dir_conf_filename, new_root)
          return new_root, nil
        end
      end
      log_debug('Not Found ProjectWithout markers subdir: %s %s', subdir, roots)
    end
  end
  return project_root, marker
end

--
---@param path string?       full filename for which needs to find project root
---@param project_root string?
---@param marker string?
---@return string? project_root
---@return string? marker
function M.checkDirConf(path, project_root, marker)
  -- local fn = fs.join_path(project_root, per_dir_confs_basename)
  project_root = fs.ensure_dir(project_root or '')
  local dir_settings = (dir_settings_cache[project_root] or E).conf
  return M.updateRootDir(dir_settings, path, project_root, marker)
end

--
-- return child subdirs for given dir which configured by per-dir-conf file
-- as a separate projects (without any markers)
--
---@param dir string
---@return table of strings
---@param reload boolean?
function M.getProjectsWithoutMarkers(dir, reload)
  local entry
  if reload then
    entry = M.readDirConf(dir, false)
  else
    dir = normalize_dir_name(dir)
    entry = dir_settings_cache[dir or false]
  end

  log_debug("getProjectsWithoutMarkers dir:%s hasConf:%s", dir, entry ~= nil)

  local t = {}
  if entry and type((entry or E).conf) == 'table' then
    for _, item in ipairs((entry.conf[1] or E).project_roots or E) do
      t[#t + 1] = item
    end
  end
  return t
end

--
-- flat or study project with project root configured via .nvim-env.yml
-- The goal is to get rid of the need to copy-paste build scripts inside each
-- directory with a simple (toy, educational) project
--
---@param project_root string?
---@return boolean
function M.isProjectWithoutMarkers(project_root)
  if not project_root or project_root == '' then
    return false
  end
  local dir = fs.ensure_dir(project_root) ---@cast dir string

  for parent_dir, _ in pairs(dir_settings_cache) do
    if su.starts_with(dir, parent_dir) then
      -- indexof(e.project_roots, subdir)
      return true
    end
  end

  return false
end

return M

local M = {}

local su = require 'env.sutil'
local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") --vim.loop

local E = {}

---@private
function M.is_os_windows()
  return uv.os_uname().version:match('Windows')
end

M.is_win = M.is_os_windows()
M.path_sep = M.is_win and '\\' or '/'

M.tmp_path = '/tmp/'
if M.is_win then
  M.tmp_path = os.getenv("TEMP") or "c:\\tmp"
end

-- ~/somedir -> /home/user/shomedir
function M.mk_abspath(path)
  if path then
    local fc2 = path:sub(1, 2)
    if fc2 == '~/' then
      path = (os.getenv('HOME') or '/home/user/') .. path:sub(2)
    elseif fc2 == './' then
      path = M.join_path(os.getenv('PWD'), path:sub(2))
    end
  end
  return path
end

---@param fn string?
---@return boolean
function M.is_basename(fn)
  return fn ~= nil and fn ~= '' and fn ~= '.' and fn ~= '..' and
      not string.find(fn, M.path_sep, 1, true)
end

---@param path string
---@return string|nil
---@param pkg_sep string|nil defualt is dot (java)
function M.path_to_classname(path, pkg_sep)
  local name = nil
  pkg_sep = pkg_sep or '.'
  if path and type(path) == "string" then
    name = path:gsub(M.path_sep, pkg_sep)
  end
  return name
end

---@param name string
---@param pkg_sep string|nil defualt is dot (java)
---@return string|nil
function M.classname_to_path(name, pkg_sep)
  local path = nil
  pkg_sep = pkg_sep or '%.'
  if name and type(name) == "string" then
    path = name:gsub(pkg_sep, M.path_sep)
  end
  return path
end

--
-- ignore './'
-- not support relative paths
---@return string
function M.join_path(...)
  local sep = M.path_sep
  local result = ''
  local current_dir = '.' .. M.path_sep -- './'

  for i = 1, select('#', ...) do
    local v = select(i, ...)
    if v and v ~= current_dir then
      if type(v) ~= 'string' then
        error('String Expected has[' .. i .. ']: ' .. type(v) .. tostring(v))
      end
      if #result > 0 then
        local has_left = result:sub(-1, -1) == sep
        local has_right = v:sub(1, 1) == sep
        if not has_left and not has_right then
          result = result .. sep
        elseif has_left and has_right then
          result = result:sub(1, -2)
        end
      end
      result = result .. v
    end
  end
  --local result = table.concat(vim.tbl_flatten { ... }, s):gsub(s .. '+', s)
  return result
end

---@return boolean
function M.is_absolute_path(path)
  local c = path ~= nil and path:sub(1, 1) or nil
  return c == '/' or c == '\\'
end

-- simple implementation for movin from current working dir by given path
--
-- "/home/dev/project/" , "./api      -> /home/dev/project/api
-- "/home/dev/pro/",      "../../api" -> /home/api
--
-- check relative only from begining of the path.
-- /dir/a/b/c/../..  -  this won't work correctly:
--
function M.build_path(cwd, path)
  if cwd and path then
    -- / no use cwd - defined absolute path
    if path:sub(1, 1) == M.path_sep then
      return path

      -- ./  relative path from cwd
    elseif path:sub(1, 2) == '.' .. M.path_sep then
      path = path:sub(3)

      -- checks ../  -- relative moving from cwd
    else
      while true do
        local s = path:sub(1, 3)
        if s == '..' .. M.path_sep then
          path = path:sub(4)              -- remove ../ from start
          cwd = M.get_parent_dirpath(cwd) -- dir up
          if not cwd then
            cwd = '/'
            break
          end
        else
          break
        end
      end
    end

    path = M.join_path(cwd, path)
    if path and path:find(M.path_sep .. '.' .. M.path_sep) then
      path = string.gsub(path, M.path_sep .. '%.' .. M.path_sep, M.path_sep)
    end
    return path
  end
  return cwd
end

-- add a slash to the end of the path if it is not already there
---@param path string
---@return string
function M.ensure_dir(path)
  if path and #path > 1 then
    if path:sub(-1, -1) ~= M.path_sep then
      path = path .. M.path_sep
    end
  end
  return path
end

function M.ensure_no_dir_slash(path)
  if path and #path > 1 then
    if path:sub(-1, -1) == M.path_sep then
      path = path:sub(1, #path - 1)
    end
  end
  return path
end

--
-- it is strongly recommended that project_root always have a trailing slash
-- so that there are no accidental mistakes like "/tmp/file" & "/tmpextra/file"
--
---@param project_root string
---@param path string
function M.get_inner_path_or_full(project_root, path)
  if project_root and project_root ~= "" and path then
    if path:sub(1, #project_root) == project_root then -- starts_with
      local ch = string.sub(project_root, -1, -1)
      local off = (ch == '/' or ch == '\\') and 0 or 1
      return path:sub(#project_root + 1 + off, #path)
    end
  end
  return path
end

---@param root string?
---@param fn string?
---@return string?
function M.get_child_dirname(root, fn)
  if type(root) == 'string' and type(fn) == 'string' and #fn > #root then
    local rootdir = M.ensure_dir(root)
    if su.starts_with(fn, rootdir) then
      local tail = string.sub(fn, #rootdir)
      return string.match(tail, '^[/\\]?([^/\\]+)')
    end
  end
  return nil
end

-- check is current os is windows and fix path to backslashes
---@param path string
function M.fixpath4os(path)
  if M.is_win and type(path) == "string" then
    path = path:gsub('/', '\\')
  end
  return path
end

-- Extract file basename from path
-- "path/to/file.ext" -> "file.ext"
-- [WARNING] the path should never end by / or \ except if its '/' (unix root)
-- or 'C:\' (windows drive)
-- otherwise, instead of the file name, it will return the full path
function M.basename(path)
  if type(path) == "string" then
    return path:match("[^\\/]+$") or path
  end
end

-- filename without extension
---@param path string
function M.extract_filename(path)
  local fn = path:match("[^\\/]+$")
  if fn then
    return fn:match("(.+)[%.]") or fn -- (.*)
  end
end

function M.extract_extension(path)
  if path then
    local ext = string.match(path, "^.*%.(.+)$") or '';
    if ext and string.find(ext, M.path_sep) then ext = '' end
    return ext
  end
  return ''
end

-- Extract dir-path from full-file-name
-- '/tmp/dir/file' -> '/tmp/dir/'
-- '/tmp/dir/' -> '/tmp/dir/'
-- 'text' -> nil
---@param path string
function M.extract_path(path)
  if type(path) == "string" then
    return path:match("(.*[/\\])") --or path
  end
end

-- return parent dir path
-- /path/to/subdir -> /path/to/
-- /path/to/subdir/ -> /path/to/
---@param path string
---@return string?
--
function M.get_parent_dirpath(path)
  if type(path) == "string" then
    while true do
      local last = path:sub(-1, -1)
      if last == '/' or last == '\\' then
        path = path:sub(1, -2)
      else
        break
      end
    end
    return path:match("(.*[/\\])")
  end
end

--
-- "/dir/sub/" -->  "sub"
-- "/dir/sub"  -->  "sub"
---@return string?
function M.get_parent_dirname(path)
  if type(path) == "string" then
    while true do
      local last = path:sub(-1, -1)
      if last == '/' or last == '\\' then
        path = path:sub(1, -2)
      else
        break
      end
    end
    return path:match("[^\\/]+$") or path
  end
end

-- find in dirname first marker of exists full filename
---@param dirname string
---@param markers table<string>
---@return string|nil
function M.find_marker(dirname, markers)
  if markers and dirname then
    for _, marker in ipairs(markers) do
      if uv.fs_stat(M.join_path(dirname, marker)) then
        return marker
      end
    end
  end
end

-- Find root for some project in which opened file with bufname
-- WARN: is bufname is not a file name but is a dirname then specify it with /
--
---@param markers table {string}
---@param bufname string fullname of file
---@return string? path
---@return string? marker
function M.find_root(markers, bufname)
  if not markers or not bufname then
    return nil, nil
  end
  bufname = bufname -- or api.nvim_buf_get_name(api.nvim_get_current_buf())
  -- local dirname = vim.fn.fnamemodify(bufname, ':p:h')
  local dirname, marker

  if bufname:sub(-1, -1) ~= M.path_sep then -- some file
    dirname = M.get_parent_dirpath(bufname)
  else
    dirname = bufname -- case: /path/to/some/dir/
  end
  -- local getparent = function(p)
  --   return vim.fn.fnamemodify(p, ':h')
  -- end
  -- while getparent(dirname) ~= dirname do
  ---@diagnostic disable-next-line: param-type-mismatch
  while M.get_parent_dirpath(dirname) ~= dirname do
    for _, marker0 in ipairs(markers) do
      local trypath = M.join_path(dirname, marker0)
      if uv.fs_stat(trypath) then
        marker = marker0
        -- tmp remove last slash - this slash breake resolve_cxt
        if dirname and dirname:sub(-1, -1) == M.path_sep then
          dirname = dirname:sub(1, -2)
        end
        return dirname, marker
      end
    end
    -- dirname = getparent(dirname)
    ---@diagnostic disable-next-line: param-type-mismatch, cast-local-type
    dirname = M.get_parent_dirpath(dirname)
  end
  return nil, nil -- not found
end

-- '/tmp/dev/parent/child'  -->   'parent/child'
function M.get_dirname_with_parentdir(s)
  return string.match(s or '', "([^/\\]+[/\\][^/\\]+)[/\\]?$")
end

-- /tmp/proj_root/  & /tmp/proj_root/sub_proj/file  -->  "sub_proj"
---@param path string
---@param root string
---@return string?
function M.get_next_dirname_after_root(root, path)
  if path and root and su.starts_with(path, root) then
    root = M.ensure_dir(root)
    local off = #root + 1
    if #root:sub(-1, -1) == M.path_sep then
      off = off + 1
    end

    local j = string.find(path, M.path_sep, off, true)
    if j then
      return path:sub(off, j - 1)
    end
  end
  return nil
end

-- IO Utils

-- Create random filename for temp file
---@param prefix string
function M.tmpfilename(prefix)
  -- std func os.tmpname() give for linux pattern: '/tmp/lua_XXxxXX'
  local res = su.rand_str(8) -- os.tmpname()
  if not prefix then prefix = "" end
  if M.is_win then
    res = M.tmp_path .. M.path_sep .. prefix .. res
  else
    res = '/tmp/' .. prefix .. res
  end
  return res
end

-- http://lua-users.org/wiki/FileInputOutput

-- see if the file exists
function M.file_exists(filename)
  if type(filename) == 'string' and filename ~= '' then
    local f = io.open(filename, "rb")
    if f then
      f:close()
      return true
    end
  end
  return false
end

-- is given path an existed file not a directory
---@return boolean
---@return number last_modified time
function M.is_file(path)
  if path then
    local stat = uv.fs_stat(path)
    if stat and stat.type == "file" then
      local mtime = tonumber((stat.mtime or E).sec) or 0 -- last modified time
      return true, mtime
    end
  end
  return false, -1
end

---@param path string?
---@return number
function M.file_size(path)
  if path then
    local stat = uv.fs_stat(path)
    if stat and stat.type == "file" then
      return stat.size or -1
    end
  end
  return -1
end

---@param path string
---@return boolean
function M.is_directory(path)
  return M.file_type(path) == 'directory'
end

-- check is given path is existed directory
---@param path string
---@return boolean
---@return string? actual type
function M.dir_exists(path)
  if path then
    local stat = uv.fs_stat(path)
    --  print(vim.inspect(stat))
    local typ = stat and stat.type or nil
    local exists = typ == "directory"
    return exists, typ
  end
  return false, nil
end

---@return string?
function M.file_type(path)
  if path then
    local stat = uv.fs_stat(path)
    return stat and stat.type
  end
end

-- Create All Directories in specified dir-path
---@param dir string
---@param mode number?
---@return boolean
---@return string? last deep
---@return string? last file|dir type
function M.mkdir(dir, mode)
  mode = mode or 448 -- 0700 -> decimal
  if uv.fs_mkdir(dir, mode) then
    return true
  end

  -- create the full deep of all subdirs like `mcdir -p $dir`
  local path = ''
  if not M.is_win then path = '/' end
  for s in string.gmatch(dir, '([^/\\]+)') do
    if s ~= '' then
      path = path .. s .. M.path_sep
      local exists, ftyp = M.dir_exists(path)
      if not exists then
        if not uv.fs_mkdir(path, mode) then
          return false, path, ftyp
        end
      end
    end
  end
  return true
end

-- Get all lines from a file, or empty it file not exist
---@param path string
---@return table {string} all lines from file
---@param lines table? output
function M.read_lines(path, lines)
  lines = lines or {}
  -- if M.file_exists(path) then
  --   for line in io.lines(path) do
  --     lines[#lines + 1] = line
  --   end
  -- end
  if not M.file_exists(path) then
    return lines
  end

  local file = io.open(path, "rb") -- no error raised when file not exists
  if file then
    while true do
      local line = file:read("*l")
      if not line then
        break
      end
      lines[#lines + 1] = line
    end

    file:close()
  end
  return lines
end

--
-- read line numbers into table
-- from ln_start to ln_end
-- if ln_end not specified read one line from ln_start
--
-- ln_end == -1 means read to end of file
--
---@param path string
---@param ln_start number
---@param ln_end number?
function M.read_lines_in_range(path, ln_start, ln_end)
  if ln_end == nil then
    ln_end = ln_start
  end

  if ln_start <= 0 or ln_end > 0 and ln_start > ln_end then
    return nil -- bad range
  end

  local n, t = 0, {}

  local file = io.open(path, "rb") -- no error raised when file not exists
  if file then
    while true do
      n = n + 1
      local line = file:read("*l")

      if n >= ln_start then
        t[#t + 1] = line
      end

      if ln_end > 0 and n >= ln_end then
        break
      end
    end

    file:close()
    return t
  end

  return nil
end

-- Open File for given mode with error handling
-- if need silent mode with no error set throw_error = "no"
---@param filename string
---@param mode string rb, w, a
---@param throw_error any|nil
function M.open_file(filename, mode, throw_error)
  if filename then
    if not mode then mode = "r" end
    local f, err = io.open(filename, mode)
    if f then
      return f
    else
      if not throw_error or throw_error ~= "no" then
        error("Error opening file: " .. err)
      end
    end
  end
  if not throw_error or throw_error ~= "no" then
    error("No FileName to Open")
  end
  return nil
end

-- Read all content in binary mode
---@param path string
---@return string|nil all bytes from file with path
function M.read_all_bytes_from(path)
  local file = io.open(path, "rb") -- r read mode and b binary mode
  if file then
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
  end
  return nil
end

-- write text to given filename
-- if append - add to the end of file or rewrite last content
---@param filename string
---@param text string
---@param mode string|nil Defaul "a" -- Append
function M.str2file(filename, text, mode)
  text = text or ""
  mode = mode or "a"
  local file = M.open_file(filename, mode) -- mode "w" | "a"
  if file then
    file:write(text)
    file:close()
    return true
  end
  return false
end

--
---@param filename string
---@param content string|table
---@param mode string? default is wb - to re
---@return boolean
function M.write(filename, content, mode)
  local cont_type = type(content)
  assert(cont_type == 'table' or cont_type == 'string', 'expected valid content')
  mode = mode or "wb"

  local file = M.open_file(filename, mode)
  if file then
    if cont_type == 'table' then
      content = table.concat(content, "\n")
    end
    file:write(content)
    file:close()
    return true
  end
  return false
end

-- Check is port opened and trigg action if not via call on_error
---@param ip_addr string|nil  def:localhost
---@param port number
---@param on_error function|nil   (err client userdata)
---@param on_connect function|nil (client, userdata)
---@param userdata any
function M.check_port(ip_addr, port, on_error, on_connect, userdata)
  ip_addr = ip_addr or "127.0.0.1"
  if port and type(port) == "number" then
    local client = uv.new_tcp() -- userdata C-object
    if not client then return false end
    ---@diagnostic disable-next-line: unused-local
    local req = uv.tcp_connect(client, ip_addr, port, function(err)
      -- client on connect
      if err then -- type string error can be like "ECONNREFUSED"
        if on_error then
          on_error(err, client, userdata)
        end
      elseif on_connect then
        on_connect(client, userdata)
      else                       -- auto close connection
        uv.shutdown(client, function(err0)
          assert(not err0, err0) -- client on close
          uv.close(client, function()
          end)
        end)
      end
    end)
  end
end

-- is given path a exists directory
---@param path string
local function is_exists_dir(path)
  if (uv and path) then
    local st = uv.fs_stat(path)
    if st and st.type == 'directory' then
      return true
    end
  end
  return false
end

-- Check is given path is exists if not create it with all subdirs as possible
-- if in path occerse existen file with given subdir-name - return error
---@param path string
---@param mode number? permissions for mkdir (default 0700)
function M.ensure_dir_exists(path, mode)
  if is_exists_dir(path) then
    return true
  end
  if path then
    mode = mode or 448 -- 0700 -> decimal
    local t = {}
    for str in string.gmatch(path, '([^/\\]+)') do
      table.insert(t, str)
    end
    local path0 = ''
    for _, v in pairs(t) do
      path0 = path0 .. M.path_sep .. v
      local st = uv.fs_stat(path0)
      -- print("[DEBUG] path: ", inspect(path0), (st and st.type))
      if st and st.type ~= 'directory' then
        error('Existed not directory on path' .. path0)
        return false
      end
      if not st and not uv.fs_mkdir(path0, mode) then
        error('Cannot Create Directory ' .. path0)
        return false
      end
    end
    return true
  end
end

--
-- Remove all sub-dirs from given path while his subdirs are empty
-- as soon as a directory with files is encountered, it will return false
--
-- root = /tmp/            -- base point from which starts work
-- path = /tmp/abc/de/fg   -- absolute path to cleaning empty dirs
-- another valid ways to specify path to cleaning:
-- path = ./abc/de/fg
-- path = abc/de/fg
-- As result brunch abc/ with all subdirs will be deleted if they are empty
--
---@param root string
---@param path string can be absolute (full-path from root) or relative
---@return boolean
function M.ensure_no_path_with_rm(root, path)
  assert(type(root) == 'string', 'root must be a string')
  assert(type(path) == 'string', 'path must be a string')

  if not path or not root then
    return false
  end
  if not is_exists_dir(root) then
    return true
  end

  local tbl = {}
  local path0 = ''
  if not su.starts_with(path, root) then -- case then path is absolute path
    path0 = root
  end
  local i = 1
  for str in string.gmatch(path, '([^/\\]+)') do
    if str and str ~= '' then
      path0 = path0 .. M.path_sep .. str
      table.insert(tbl, path0)
      i = i + 1
    end
  end
  -- remove dirs in reverse order from deeper
  for k = i - 1, 1, -1 do
    local st = uv.fs_stat(tbl[k])
    if st then
      if st.type == 'directory' then
        -- print("[DEBUG] rm:", inspect(tbl[k]))
        uv.fs_rmdir(tbl[k]) -- can remove only empty dirs
      else                  -- file? instead a dir in the path -- error
        return false
      end
    end
  end
  return true
end

-- Return last_modified time for given file or nil
---@param path string full path to file
---@return number|nil
function M.last_modified(path)
  local stat = uv.fs_stat(path)
  if type(stat) == 'table' and stat.mtime and stat.mtime.sec then
    return stat.mtime.sec
  end
  return nil
end

-- Change file timestamps
-- assume in win used cygwin
function M.touch(path)
  if path then
    return os.execute('touch ' .. path) == 0
    -- local file = io.open(path, 'a')
    -- if file then
    --   file:write('')  -- dont work as expected!
    --   io.close(file)
    --   return true
    -- end
  end
end

-- run external os command and return its output
---@param cmd string
---@return string? Output of command
function M.execr(cmd)
  local fh = io.popen(cmd)
  if fh then
    local data = fh:read('*a')
    fh:close()
    return data
  end
end

-- run external os command and return its output as lines
---@param cmd string
---@return table Output of the command
function M.execrl(cmd, t)
  local fh = io.popen(cmd)
  t = t or {}
  if fh then
    t = t or {}
    while true do
      local line = fh:read('*l')
      if not line then break end
      t[#t + 1] = line
    end
    fh:close()
  end
  return t
end

-- print(execr [[/usr/bin/perl -e 'for my $x (1..3) { print "$x\n"; } ']])
-- prints "1 2 3"

---@param cmd string
---@param data string
---@return boolean
function M.execw(cmd, data)
  local fh = io.popen(cmd, 'w')
  if fh then
    fh:write(data)
    fh:close()
    return true
  end
  return false
end

---@param dir string
---@param pattern string?  lua pattern to match the filename
---@param ftype string file|directory|*
---@return false|string?
---@return string? errmsg
function M.find_first_in(dir, pattern, ftype)
  local handle, errmsg = uv.fs_scandir(dir)
  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end

  local anyt = ftype == nil or ftype == '*'

  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    if (anyt or typ == ftype) and (not pattern or string.match(name, pattern))
    then
      return name
    end
  end
  return nil
end

--
---@param opts table?{no_dir, ext}
---@return table?
---@return string? errmsg
function M.list_of_files(dir, opts)
  local handle, errmsg = uv.fs_scandir(dir)
  if type(handle) == "string" then
    return nil, handle
  elseif not handle then
    return nil, errmsg
  end
  opts = opts or {}
  local ext, no_dir = nil, opts.no_dir
  if opts.ext then
    ext = '.' .. tostring(opts.ext)
    no_dir = true -- ?
  end

  local ls = {}

  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end

    local skip = false
    if ext and typ ~= 'directory' then
      skip = not su.ends_with(name, ext)
    elseif typ == 'directory' and no_dir then
      skip = true
    end

    if not skip then
      table.insert(ls, name)
    end
  end
  return ls
end

--
--
---@return false|table
---@return string? msgerr
function M.list_of_dirs(dir)
  local handle, errmsg = uv.fs_scandir(dir)
  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end
  local ls = {}
  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    if typ == 'directory' then
      table.insert(ls, name)
    end
  end
  return ls
end

---@return table|false
---@return string? errmsg
---@param ls table? output
function M.list_of_files_deep(dir, pattern, ls) --, subdir)
  local handle, errmsg = uv.fs_scandir(dir)

  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end

  ls = ls or {}
  while true do
    local name, typ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    if typ == 'directory' then
      M.list_of_files_deep(M.join_path(dir, name), pattern, ls) --, name)
    elseif typ == 'file' and not pattern or string.match(name, pattern) then
      ls[#ls + 1] = M.join_path(dir, name)
    end
  end
  return ls
end

--
function M.cwd()
  return uv and uv.cwd()
end

---@return boolean
---@return string?
function M.copy(src, dst)
  if src == dst or not src or src == '' or not dst or dst == '' then
    return false, 'bas src and dst files'
  end

  local h_src, err = io.open(src, "rb")
  if h_src then
    local h_dst, err2 = io.open(dst, "wb")
    if not h_dst then
      h_src:close()
      return false, err2
    end

    local size = 2 ^ 13 -- good buffer size (8K)
    while true do
      local block = h_src:read(size)
      if not block then break end
      h_dst:write(block)
    end

    h_src:close()
    h_dst:close()
    return true, nil
  end
  return false, err
end

-- execw ([[/usr/bin/perl -e 'for my $x (<STDIN>) { print $x*2, "\n" for 1..1000000; } ' >out.txt]], "1\n2\n3")
-- writes "1 2 3" to the file out.txt.

return M

local M = {}

local log = require('alogger')
local su = require('env.sutil')
local ui = require('env.ui')
local files = require('env.files')
local spawner = require('env.spawner')
-- local lbase = require('env.lang_base')
local cp = require("env.util.code_parser")
local lp = require('env.lua_parser')
-- local c4lv = require("env.util.cmd4lua_vim")
-- local Editor = require("env.ui.Editor")
local lapi_consts = require 'env.lang.api.constants'
-- local ExecParams = require 'env.lang.ExecParams'

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") -- vim.loop

M.root_markers = { ".git" }
M.LUA = "lua"          -- then run files directly via lua interpretter
M.TF_BUSTED = "busted" -- test framework for lua

local SRC = "/src/"
local SRC2 = "/lua/"              -- for nvim plugins
local JVMTEST = "/src/test/_lua/" -- for testing jvm app with lua
local TEST = "/test/"
local TEST_SUFF = "_spec"
local EXT_LEN = #".lua"
local OTHER_NOT_SUPPORTED = "Other Build Systems not implemented yet"

local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

-- like resolve class name for java
--  set ctx.class_name
---param@ ctx
function M.resolve_module_name(ctx)
  log.debug("resolve_module_name ctx:", ctx)
  if ctx and ctx.project_root then
    ctx.class_name = nil
    ctx.jvmtest = nil

    local len = string.len(ctx.project_root)
    local m -- module(file) name
    -- 'root/dir' + /pkg/Class + '.ext'  -> /pkg/Class
    m = string.sub(ctx.bufname, len + 1, string.len(ctx.bufname) - EXT_LEN)
    log.debug("innner path %s", m)

    if m and su.starts_with(m, JVMTEST) then
      m = string.sub(m, string.len(JVMTEST) + 1, string.len(m))
      ctx.jvmtest = true
      --
    elseif m and su.starts_with(m, ctx.src) then
      m = string.sub(m, string.len(ctx.src) + 1, string.len(m))
    elseif m and su.starts_with(m, TEST) then
      m = string.sub(m, string.len(TEST) + 1, string.len(m))
      if su.ends_with(m, TEST_SUFF) then
        m = string.sub(m, 0, #m - #TEST_SUFF)
      end
      -- without subdir(no "package")
    elseif su.starts_with(m, files.path_sep) then
      m = string.sub(m, 2, #m)
    end
    ctx.class_name = files.path_to_classname(m)
    log.debug('class_name', ctx.class_name)
  end
end

-- find project root, build system, func
---@diagnostic disable-next-line: unused-local
local function resolve_ctx(ctx, opts)
  -- if not ctx.project_root then
  --
  -- end
  ctx.project_root = ctx.project_root or files.find_root(M.root_markers, ctx.bufname)
  if not ctx.project_root or not uv.fs_stat(ctx.project_root) then
    if opts and opts.can_take_cwd then -- for run script from current work directory
      ctx.project_root = vim.fn.getcwd()
    else
      ui.echo_err("Not Found Project Root ")
      return false
    end
  end

  if not ctx.test or not ctx.src then
    ctx.test = TEST
    if uv.fs_stat(ctx.project_root .. '/spec/') then
      ctx.test = '/spec/'
    end

    -- find path with sources
    if uv.fs_stat(ctx.project_root .. SRC) then
      ctx.src = SRC
    elseif uv.fs_stat(ctx.project_root .. SRC2) then
      ctx.src = SRC2
    else
      ctx.src = "" -- ???
    end
  end

  -- module name == class_name
  M.resolve_module_name(ctx)
  if ctx.class_name == nil then
    ui.echo_err("Not Resolve Module Name ")
    return false
  end

  local builder = ctx.builder or M.TF_BUSTED -- for test only!
  ctx.builder = builder
  if builder == nil or builder == "" then
    ui.echo_err("Not Found Builder System for " .. ctx.class_name)
    return false
  end
  return true;
end

--[[
-- Check if the file opened in the current buffer belongs to test files
---@param ctx table with project_root
local function is_test_path(ctx)
  if ctx and ctx.bufname and ctx.project_root then
    local i = string.find(ctx.bufname, ctx.project_root .. ctx.test, 1, true)
    return i == 1
  end
  return false
end
]]

-- Check if the file opened in the current buffer belongs to source files
---@param ctx table with project_root
function M.is_src_path(ctx)
  if ctx and ctx.bufname and ctx.project_root then
    local i = string.find(ctx.bufname, ctx.project_root .. ctx.src, 1, true)
    return i == 1
  end
  return false
end

function M.is_in_project(ctx, path)
  if ctx and ctx.project_root then
    path = path or ctx.bufname
    if path then
      -- for 100% - project_root must ends with '/'
      return string.find(path, ctx.project_root, 1, true) == 1
    end
  end
  return false
end

-- for toggle src-file to test-file
function M.mk_path_to_test(ctx)
  local ocn = ctx.class_name
  local tcn = files.classname_to_path(ocn)
  local path = nil
  if tcn then
    if not su.ends_with(tcn, TEST_SUFF) then -- add _spec
      tcn = tcn .. TEST_SUFF
    end
    -- to support test jvm apps with lua
    local test_pref = ctx.jvmtest == true and JVMTEST or ctx.test
    path = ctx.project_root .. test_pref .. tcn .. '.lua'
  end
  log.debug("mk_path_to_test class_name:%s tcn:%s  ret:%s", ocn, tcn, path)
  return path
end

-- for toggle test-file to scr-file
---@return string?
---@param class_name string|nil -- if not defined take from ctx.class_name
function M.mk_path_to_source(ctx, class_name)
  if class_name and su.starts_with(class_name, ctx.project_root) then
    return class_name
  end

  local scn = files.classname_to_path(class_name or ctx.class_name)
  if scn then
    if su.ends_with(scn, TEST_SUFF) then -- remove _spec
      scn = string.sub(scn, 0, #scn - #TEST_SUFF)
    end
    return ctx.project_root .. ctx.src .. scn .. '.lua'
  end
end

-- TODO define Project structure. Is Lua has build file?
---@diagnostic disable-next-line: unused-local
function M.args_run_single_file(ctx, opts)
  if ctx and ctx.bufname then
    local project_paths = './src/?.lua;./lua/?.lua'
    local paths = "package.path = package.path .. ';" .. project_paths .. "'"
    return { '-e', paths, ctx.bufname }
  end
  return {}
end

local function get_args_run_main_in_file(ctx, opts)
  local args
  if ctx.builder == M.LUA then
    args = M.args_run_single_file(ctx, opts)
  else
    ui.echo_err(OTHER_NOT_SUPPORTED)
    args = {}
  end
  return args
end

-- run single module file
---@param ctx any
---@param opts any
function M.run_main_in_file(ctx, opts)
  log.debug("[Lua] Run File")
  opts.can_take_cwd = true
  if not resolve_ctx(ctx, opts) then
    return
  end
  ctx.builder = M.LUA
  local args = get_args_run_main_in_file(ctx, opts)
  if args and #args == 0 then
    return
  end

  local bname = "Run Module: " .. ctx.class_name
  ui.create_buf_nofile(ctx, bname)      -- buffer to show stdout of command
  local pp = spawner.new_process_props( -- create cmd to run test
    ctx.out_bufnr,
    ctx.builder,                        -- cmd
    args,
    ctx.project_root                    -- cwd
  )
  spawner.run(pp)
end

--[===[ TODO REMOVE
function M.run_test_file(ctx, opts)
  log.debug("[Lua] Run Test File")
  if not resolve_ctx(ctx, opts) then
    return
  end
  if not ensure_test_file_opened(ctx) then
    ui.echo_err("Error onEnsureTestOpened")
    return false
  end
  -- output2diagnostics -- to show output results in nvim as diagnostics
  local output2diagnostics, save_ouput = false, false
  local add_original_json = false
  local only_clear_diagnostics = false

  local w = c4lv.newCmd4Lua(opts)

  if not w:desc('Only show a test report in new buf without the diagnostics')
      :has_opt("--no-diagnostics", "-nd") then
    -- Note: with opt `--output json` the fatal errors will not be shown
    opts.output = 'json'
    output2diagnostics = true
  end

  save_ouput = w:desc('Save a decoded json from the testframework output')
      :has_opt("--save-decoded", "-s")

  -- debuggin & research tool
  add_original_json =
      w:desc('Add a original json into user_data or the nvim diagnostic entry')
      :has_opt("--add-original-json", "-j")

  -- clear prev diagnostic
  vim.diagnostic.set(busted.NAMESPACE, ctx.bufnr, {})

  only_clear_diagnostics =
      w:desc("Only clear prev diagnostics without running the test")
      :has_opt("-cd", "--clear-diagnostics")

  -- if input has errors or the help request - print help|usage and exit
  if not w:is_input_valid() or only_clear_diagnostics then
    return
  end

  -- work

  --log.debug_inspect(ctx)
  local args = get_args_test_single_class(ctx, opts)
  if args and #args == 0 then
    return
  end

  local bname = "Run Test: " .. ctx.class_name
  ui.create_buf_nofile(ctx, bname, 'lua') -- buffer to show stdout of command
  -- to jump back to the testfile from the buf with report
  ui.bind_bufs(ctx.bufnr, ctx.out_bufnr)
  local cwd, cmd = ctx.project_root, ctx.builder
  local pp = spawner.new_process_props(ctx.out_bufnr, cmd, args, cwd)
  -- pass debug opts
  pp.save_output = save_ouput -- dev&debug
  pp.add_original_json = add_original_json

  if output2diagnostics then -- json to in-buffer-hints
    pp.src_bufnr = ctx.bufnr
    ---@diagnostic disable-next-line: redefined-local
    pp.handler_on_close = function(output, pp, exit_ok, signal)
      if not exit_ok and (not output or output == '') then
        opts.output = nil -- Rerun without --no-diagnostics in new buf
        args = get_args_test_single_class(ctx, opts)
        local params = ExecParams.of(cwd, cmd, args)
        Editor:new():runCmdInBuffer('~' .. bname, params)
      else
        busted.process_single_test_output(output, pp, exit_ok, signal)
      end
    end
  elseif save_ouput then
    pp.handler_on_close = busted.save_single_test_output
  end

  pp.hide_start = true
  log.debug('out2diag: %s save_decoded: %s ', output2diagnostics, save_ouput)
  spawner.run(pp)
  --api.nvim_exec('PlenaryBustedFile ' .. ctx.bufname, true)
end
]===]

--[==[ TODO REMOVE
---@param ctx table
---@param line string
---@param callback function
---@return boolean, number
function M.find_path_to_source_for(ctx, line, callback)
  log.debug("find_path_to_source_for")
  if not ctx or not line or line == '' then
    return false, 0
  end
  if M.find_source_by_tracemsg(line, callback) then return true, 1 end

  -- from lua source code
  if ctx.bufname:match('%.lua$') then
    if lp.find_path_from_code(ctx, line, callback) then return true, 2 end

    if M.find_module_by_require(ctx, line, callback) then return true, 3 end

    if c4lv.find_function_by_cmdname(ctx, line, callback) then return true, 4 end

    if M.find_source_by_current_word(ctx, line, callback) then return true, 5 end
  end

  return false, 0
end
]==]

-- from busted error
-- ./lua/env/lang/oop/ClassGen.lua:106: attempt to concatenate a nil value
-- or from trace message from alogger
--
---@param line string
---@param callback function
function M.find_source_by_tracemsg(line, callback)
  log.debug("find_source_by_tracemsg")
  local path, line_nr = line:match("^[%s]*(%.?/?[^%s]+)%:([%d]+)%: ")
  if not path and not line_nr then
    -- path, line_nr = line:match("%[DEBUG%] @?(%.?/[^%s]+)%:([%d]+)%: ")
    path, line_nr = line:match("%[%w+%] @?(%.?/?[^%s]+)%:([%d]+)%: ")
  end
  -- "Error -> ./test/env/util/cmd4lua/parse_line.lua @ 63"
  if not path then
    path, line_nr = line:match("Error %-> (%.?/[^%s]+) @ ([%d]+)")
  end
  if not path then -- from exunit trace
    path, line_nr = line:match("^[%s]*(%.?/?[^%s]+)%:([%d]+)%s*$")
  end
  if not path then -- warning from exunit
    path, line_nr = line:match("^[%s]*└─ (%.?/?[^%s]+)%:([%d]+)%:%d")
  end


  if path and line_nr then
    line_nr = tonumber(line_nr) or 1
    -- path = files.build_path(vim.loop.cwd(), path)
    path = files.build_path(uv.cwd(), path)
    log.debug("has trace message jump to lnum:%s file:%s", line_nr, path)
    assert(type(callback) == 'function')
    return callback(path, line_nr)
  end
  return false
end

-- jump to module from line with local m = require('module')
--
---@param ctx table{bufnr, bufname, lang:env.lang.Lang}
---@param line string
---@param callback function
function M.find_module_by_require(ctx, line, callback)
  local module_varname, module = lp.parse_line_require(line)
  log.debug("find_module_by_require", module_varname, module)

  if module_varname and module and ctx and ctx.lang then
    local lang = ctx.lang
    if lang then ---@cast lang env.lang.Lang
      local path = lang:resolvePathByClassName(module, nil, true, true)
      return callback(path, 1)
    end
  end
  return false
end

---@param ctx table{bufnr, bufname}
---@param line string
---@param callback function
function M.find_source_by_current_word(ctx, line, callback)
  log.debug("find_source_by_current_word")
  -- find by the word under the cursor
  if cp.resolve_words(ctx, line) then
    log.debug("has words try to find its source")

    -- line_nr can be find-string like ClassName:method(
    local module, line_nr = M.find_source_of_words(ctx)
    if module then
      local path
      log.debug("found module %s, resolve ctx...", module)

      if module and module == ctx.bufname then
        path = ctx.bufname
        log.debug("same file %s", path)
      elseif resolve_ctx(ctx) then
        path = M.mk_path_to_source(ctx, module)
        log.debug("resolved to %s", path)
      else
        log.debug("Cannot resolve ctx")
      end
      assert(type(callback) == 'function')
      return callback(path, line_nr)
    end
  end
  return false
end

--
-- case-1:   jump from current method of some class into definition inside Class
--   local instance = MyClass:new({})
--   instance:method(...
--               ^cursor
-- case-2:                   jump to method body check is in same class-file
--   MyClass:staticMethod(...
--               ^cursor
--
-- case-3:                  jump to method body, the classname in another line
-- local instance = MyClass:new({         << classname here
--    })               ^
--    :dynMethod()      `needs to find this classname
--        ^cursor
--
-- TODO
-- case-4:
-- '---@class Lang'
-- '---@field editor Editor'          <-- here ClassName of instance self.editor
-- local Lang = Object:new({ ...})    <-- current class definition     ^
--  ...                                                                |
--     self.editor:doFirst(<params>):doNext(<params>)                  |
--            ^                         ^cursor                        |
--             `instance of needed class - ->  - - - - - - - - - - - - '
--
---@param ctx table {with extra word_container & word_element}
---@return string?,number?|string? -- path, line-number|find-str
function M.find_source_of_words(ctx)
  if ctx and ctx.word_element then
    local wcontainer, welement, wetype =
        ctx.word_container, ctx.word_element, ctx.word_lexeme_type
    log.debug('find_source_of_words: %s %s', wcontainer, welement)

    local bufnr, row = ctx.bufnr, (vim.api.nvim_win_get_cursor(0) or {})[1]
    local lnum = row and row - 1 or 0

    -- local var = vn_ref .. 'value'
    -- local lines = vim.api.nvim_buf_get_lines(ctx.bufnr, startln, curline - 1, false)
    local short_classname, varname = nil, wcontainer
    -- case-3:
    if not wcontainer then
      log.debug('try to find for word-element only %s %s', welement, wetype)
      if wetype == LEXEME_TYPE.CLASS or wetype == LEXEME_TYPE.UNKNOWN then
        short_classname = welement
        varname = short_classname
      elseif wetype == LEXEME_TYPE.FUNCTION then
        -- find "container" name
        local input = ui.ask_value('ShortClassName: ', welement)
        if not input then
          log.debug('canceled by user')
          return
        end
        if input ~= welement and input ~= '' then
          short_classname = input
        else
          error('Case-3: AutoLookup Not implement yet')
        end
      end
    end
    -- case-2: from static method
    if lp.is_class_name(varname) then -- MyClass not myClass
      short_classname = varname
      -- method = ctx.word_element
    end
    -- case-1: from dynamic method of instance to method definition in classfile
    if not short_classname then
      log.debug('try to find short_classname for %s', varname)
      local expr, _, _ = lp.find_var_definition(varname, bufnr, 0, lnum, true)
      if expr then
        -- expr = 'ClassGen:new({' -->  class='ClassGen', method = 'new'
        short_classname, _ = lp.parse_line_expr_class_method(expr)
        -- ^ here the method is a constructor(new)

        if not short_classname then
          log.debug('Cannot recognize var:[%s] in expr: [%s]', varname, expr)
        end
      else
        --log.debug('cannot find the assignment to the variable [%s]', varname)
        log.debug('cannot find the var definition for [%s]', varname)
      end
    end

    -- ask
    if not short_classname then
      local input = ui.ask_value('ShortClassName: ', '')
      if input then
        short_classname = input --
      end
    end

    --
    if short_classname then
      log.debug('try to find module-namespace: for %s', short_classname)
      -- ns - namespace is a full-classname
      local ns, _ = lp.find_module_namespace(short_classname, bufnr, 0, lnum)
      -- insted linenumber send method name
      if ns then
        -- module-namespace, string-to-search after opening module-file
        return ns, (short_classname .. ':' .. ctx.word_element .. '(')
      end

      -- if no short_class as lua module in import then check in self file
      if not ns then
        log.debug('try to find in current file')
        local m, f = lp.parse_function_definition(ctx.current_line)
        if m == ctx.word_container and f == ctx.word_element then
          log.debug('already at the destination place')
          -- todo jump to overrided method in parent class from extended
          return
        end
        -- search in all buffer
        local md, ln3 = lp.find_in_class_method_definition(
          short_classname, ctx.word_element, ctx.bufnr, 0, -1
        )
        log.debug('found: method_def:%s lnum:%s bufname:%s', md, ln3, ctx.bufname)
        if md and type(ln3) == 'number' then
          return ctx.bufname, ln3
        end
      end
    end
  end
end

return M

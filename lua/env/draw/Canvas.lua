--  30-03-2024  @author Swarg
--

local class = require 'oop.class'

local log = require 'alogger'
local inspect = require "inspect"
local ICanvas = require 'env.draw.ICanvas'
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'
local Container = require 'env.draw.Container'
local Selection = require 'env.draw.ui.Selection'
local ubase = require 'env.draw.utils.base'
local ugeo = require 'env.draw.utils.geometry'
local utbl = require 'env.draw.utils.tbl'
local conf = require 'env.draw.settings'
local u8 = require("env.util.utf8")
local instanceof = class.instanceof

class.package 'env.draw'
---@class env.draw.Canvas : oop.Object, env.draw.ICanvas, env.draw.IEditable
---@field new fun(self, o:table?, height:number?, width:number?, bg_color:string?, border:string?): env.draw.Canvas
---@field bg table           -- background that cannot be changed
--                              (objects baked into canvas)
---@field layer table        -- layer with objects (two-dimensional cell array)
--                              on object:draw render to this place
---@field width number
---@field height number
---@field border string
---@field objects table{env.draw.Drawable}
---@field hooks table? {pre_draw, post_draw}
local Canvas = class.new_class(nil, 'Canvas', {
}, --[[ implements ]] ICanvas, IEditable)

Canvas.DEFAULT_WIDTH = 80
Canvas.DEF_BACKGROUND = ' '
local dprint = require('dprint').mk_dprint_for(Canvas, true)

-- Initially the idea was to look at the merger of two objects in tests
-- but this can be useful for merging characters for example "-" and "|" to "+"
-- at the edges of the rectangle or at the intersection of lines
-- but the question arises: how to distinguish when merging is not necessary?
-- and as an option, can enable merging only within the drawing of one element
Canvas.MERGE_COLORS = false -- testing

-- hooks
local HOOK_PRE_DRAW = 'pre_draw'
local HOOK_POST_DRAW = 'post_draw'
-- local HOOK_MERGE_COLORS = 'merge_colors'
Canvas.HOOK_PRE_DRAW = HOOK_PRE_DRAW
Canvas.HOOK_POST_DRAW = HOOK_POST_DRAW
-- Canvas.HOOK_MERGE_COLORS = HOOK_MERGE_COLORS
-- used insted of setHook(name, idx, nil) when need to remove one specific hook
Canvas.HOOK_STUB = function() --[[do nothing]] end

Canvas.HOOK_NAMES = {
  HOOK_PRE_DRAW,
  HOOK_POST_DRAW,
  -- HOOK_MERGE_COLORS,
}

local ORDERED_KEY_LIST = {
  { width = 'number' },
  { height = 'number' },
  { border = 'string' },
}

local E = {}
local v2s, fmt = tostring, string.format
local log_trace = log.trace
local log_debug = log.debug
local normalize_range = ubase.normalize_range
local slist2str = ubase.slist2str

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id
local invert_map = SH.invert_map
local mk_patch = IEditable.cli().TESTING.mk_patch
local add_class_ref = SH.add_class_ref

local P = {} -- private

--?
---param top number - lnum in the buffer of the doc (Note nvim give lines from 1)

-- constructor used in Canvas:new()
---@param width number
---@param height number
---@param bg_color string? default is space ' '
---@param border string?
function Canvas:_init(height, width, bg_color, border)
  -- self.bg     -- backgroud (backed objects) layer0
  -- self.layer  -- layer to render objects on draw (two-dimensional array)
  self.objects = {} -- movable object to render(not yet baked into canvas)

  self.width = self.width or width or Canvas.DEFAULT_WIDTH
  self.height = self.height or height

  assert(self.width, 'width')
  assert(self.height, 'height')

  self.bg_color = self.bg_color or bg_color
  self.border = self.border or border or ''

  if width and height then
    self.bg = P.create_layer(width, height, bg_color, border)
    -- self.layer = create_layer(width, height, bg_color, border)
  end

  log_debug('new Canvas w:%s h:%s ', self.width, self.height)
end

--
-- to check if the given object is an Canvas instance if not, throw an error
--
---@return env.draw.Canvas
function Canvas.cast(obj)
  if type(obj) == 'table' and instanceof(obj, Canvas) then
    return obj
  else
    local b = class.base
    local ctype = b.CTYPE[obj._ctype or false] or '?'
    local cname = class.name(obj)
    local canvas_cname = class.name(Canvas)

    -- fix issue: on runtime code reloading
    if ctype == b.CTYPE[b.CLASS] and cname == canvas_cname then
      -- set updated version of the class for this instance  -- deep?
      setmetatable(obj, Canvas)
      return obj
    end
    error(fmt('expected instance of %s got: %s %s',
      v2s(canvas_cname), v2s(ctype), v2s(cname)))
  end
end

-- ---@param top number lnum - absolute lnum(line number) in the test file(buffer)
-- ---@return self
-- function Canvas:setTop(top)
--   assert(type(top) == 'number' and top >= 0, 'top')
--   self.top = top
--   self.bottom = self.top + self.height
--   return self
-- end

--
-- aka render all background + movable objects
--
---@param t table?
---@return table?{string}
function Canvas:toLines(t)
  if self then
    -- assert(type(self.layer) == 'table', 'use after Canvas.draw')

    t = t or {}
    for y = 1, self.height do
      -- local line, row = '', self.layer[y]
      local bg_row = self.bg[y]
      local row = (self.layer or E)[y] or E
      local line = ''
      for x = 1, self.width do
        local pixel = row[x]
        if not pixel or pixel == '' then
          pixel = bg_row[x]
        end
        line = line .. pixel
      end
      t[#t + 1] = line
    end

    return t
  end

  return nil
end

---@param self self?
function Canvas.__tostring(self, verbose)
  if self then
    local border = self.border
    if border and border ~= '' and border ~= ' ' then
      border = ' ' .. v2s(border)
    end
    local s = string.format('Canvas w:%s h:%s%s objects:%s',
      v2s(self.width), v2s(self.height), v2s(border or ''),
      #(self.objects or E)
    )
    if verbose then
      s = s .. "\n"
      for i, elm in ipairs(self.objects or E) do
        local l
        if elm.__tostring then
          l = elm.__tostring(elm, true, 4)
        else
          l = tostring(elm)
        end
        s = s .. fmt("  %2s:  %s\n", i, l)
      end
    end
    return s
  end
  return 'nil'
end

---@param obj env.draw.IDrawable|env.draw.IEditable
---@return self
---@return env.draw.IDrawable|env.draw.IEditable
function Canvas:add(obj)
  assert(instanceof(obj, IDrawable), 'IDrawable expected')
  self.objects[#self.objects + 1] = obj

  return self, obj
end

-- insert new given object before given existed obj instances
---@return number?
function Canvas:insertBefore(obj, new_obj)
  assert(instanceof(obj, IDrawable), 'obj IDrawable expected')
  assert(instanceof(new_obj, IDrawable), 'new_obj IDrawable expected')
  self.objects[#self.objects + 1] = obj

  -- index of
  local i = self:indexOf(obj)
  if i then
    local tmp = self.objects[i]
    self.objects[i] = new_obj
    self:add(tmp)
  end

  return i
end

function Canvas:indexOf(obj)
  assert(instanceof(obj, IDrawable), 'IDrawable expected')
  for i, v in pairs(self.objects or E) do
    if v == obj then
      return i
    end
  end
  return nil
end

local function indexOfValue(t, v)
  for n, elm in ipairs(t) do
    if elm == v then
      return n
    end
  end
  return nil
end

--
-- find indexes of given elements (keep original order for elements in list)
--
---@param list table
---@return table? of indexes
function Canvas:indexOfAll(list)
  if type(list) == 'table' and next(list) then
    local t = {}
    for index, element in pairs(self.objects or E) do
      local n = indexOfValue(list, element)
      if n then
        t[n] = index -- n to keep order
      end
    end

    return t
  end

  return nil
end

--
-- delete given object from list
--
---@param obj table
---@return table|nil
function Canvas:remove(obj)
  local idx = self:indexOf(obj)
  if idx then
    local removed = table.remove(self.objects, idx)
    return removed
  end

  return nil
end

--
-- remove all elements from the canvas by indexes
-- works via reversed order from end to begining of the list
--
---@param indexes table
---@return table? of the deleted elements in the reversed order
function Canvas:remove_all_by_indexes(indexes)
  if type(indexes) == 'table' then
    local removed = {}
    table.sort(indexes, function(a, b) return a > b end) -- descending
    for _, index in ipairs(indexes) do
      removed[#removed + 1] = table.remove(self.objects, index)
    end
    return removed
  end

  return nil
end

---@param list table of elements to delete
function Canvas:remove_all(list)
  assert(type(list) == 'table', 'list')
  local new_list = {}
  local removed = {}

  if next(list) then
    for _, element in ipairs(self.objects) do
      local n = indexOfValue(list, element)
      if n then
        removed[n] = element
      else
        new_list[#new_list + 1] = element
      end
    end
    self.objects = new_list

    return removed
  end

  return nil
end

--
-- pack given elements into container (move from canvas to container-"parent")
--
---@param elms table
---@param indexes table
---@return number index of container
---@return number removed elements from canvas
---@return env.draw.Container?
function Canvas:join_to_container(elms, indexes)
  if elms and next(elms) then
    local x1, y1, x2, y2 = Selection.get_range_coords(elms)
    local min_idx = #self.objects + 1
    local inv, moved = {}, 0

    -- make sure the indexes are sorted in ascending order
    for i, elm in ipairs(elms) do
      local idx = indexes[i]
      assert(self.objects[idx] == elm, 'enruse index not lie')
      if idx < min_idx then min_idx = idx end

      inv[i] = elm

      self.objects[idx] = nil -- remove from Canvas
      moved = moved + 1
    end

    local container = Container:new(nil, x1, y1, x2, y2, inv)
    self.objects[min_idx] = container

    local c = 0
    for _, idx in pairs(indexes) do
      if not self.objects[idx] then
        table.remove(self.objects, idx)
        c = c + 1
      end
    end
    return min_idx, moved, container
  end

  return -1, 0, nil
end

--
-- reverse action for a function join_to_container
--
---@param c env.draw.Container
---@param elms table?
function Canvas:unpack_from(c, elms)
  local j = 0
  if c and elms and next(elms) then
    local list = c:unpack(elms)
    if self:remove(c) then
      for _, elm in ipairs(list) do
        self:add(elm)
        j = j + 1
      end
    end
  end
  return c
end

---@param t table
---@param el any
local function indexof(t, el)
  for k, v in pairs(t) do
    if v == el then
      return k
    end
  end
  return nil
end

--
-- only elements by given indexes
--
---@param indexes table<number>
---@return table<env.draw.IEditable>
function Canvas:get_elements_by_indexes(indexes)
  local t = {}
  for _, index in ipairs(indexes) do
    t[#t + 1] = self.objects[index or false]
  end

  return t
end

function Canvas:get_indexes_by_elements(elms)
  local t = {}

  for i = #self.objects, 1, -1 do
    local elm = self.objects[i]
    if utbl.tbl_indexof(elms, elm) then
      t[#t + 1] = i
    end
  end

  return t
end

--
-- elements + indexes
--
-- return two table of elements from stat to end
---@return table elements{env.draw.IEditable}
---@return table indexes{number}
function Canvas:get_elements_by_index_range(sidx, eidx)
  local elements, indexes = {}, {}
  local sz = #(self.objects or E)
  sidx = sidx or 1
  if eidx and eidx < 0 then eidx = sz + eidx + 1 end
  if not eidx or eidx > sz then eidx = sz end
  if sidx > eidx then
    local tmp = sidx
    sidx, eidx = eidx, tmp
  end

  for i = eidx, sidx, -1 do
    elements[#elements + 1] = self.objects[i]
    indexes[#indexes + 1] = i
  end

  return elements, indexes
end

--
-- place first given element from elements to backgroud relatively to all
-- elements in its range
--
---@param self env.draw.Canvas
---@param elements table  - to pass the elements that needs to be paced in
---@param indexes table?    the backgroud and its index in canvas
---@param exclude_cb function? callback to exclude elms from the considered list
---@param mkreport boolean?
function Canvas:mkElementBackgroud(elements, indexes, exclude_cb, mkreport)
  if not indexes or #indexes ~= #elements then
    return false, fmt('Illegal elements indexes %s %s', #elements, #(indexes or E))
  end

  -- element to place as background
  local ebg = elements[1] ---@cast ebg env.draw.IEditable
  local idx = indexes[1]
  local x, y, x2, y2 = ebg:get_coords()
  local elms, idxs = self:get_objects_at(x, y, x2, y2, nil, true)

  if #elms < 2 then
    return true, 'no change - no internal elements were found for the current one'
  end

  -- exclude elements by external function, if it present
  if type(exclude_cb) == 'function' then
    elms, idxs = exclude_cb(self, elms, idxs)
  end

  local max, min = utbl.tbl_max_min_num_values(idxs)
  if not max or not min then
    return false, 'internal error no max or min: ' .. v2s(max) .. ' ' .. v2s(min)
  end
  if min == idx or idx < min then
    local cls = string.match(class.name(ebg) or '?', '([^%.]+)$') or '?'
    return true, fmt('%s:%s element alredy in backgroud', v2s(cls), v2s(idx))
  end
  -- asset
  if idx < min or idx > max then
    return false, fmt('%s out of range %s-%s', v2s(idx), v2s(min), v2s(max))
  end
  local j = assert(utbl.tbl_indexof(idxs, idx), 'sure in same indexes')

  local mk_readable, after = Canvas.mk_elements_readable, ''
  local before = fmt('before %s-%s (%s) %s', min, max, idx, vim.inspect(idxs))

  if mkreport then
    before = before .. "\n" .. table.concat(mk_readable(elms, idxs), "\n")
  end

  -- moving element to place ebg to the backgroud
  for n = j, #idxs - 1 do
    local index = idxs[n]
    self.objects[index] = elms[n + 1]
  end
  self.objects[idxs[#idxs]] = ebg


  if mkreport then
    local elms_new_order = self:get_elements_by_indexes(idxs)
    after = "after:\n" .. table.concat(mk_readable(elms_new_order, idxs), "\n")
  end

  return true, before .. after
end

--------------------------------------------------------------------------------
--                               Hooks
--------------------------------------------------------------------------------

---@param name string
---@param callback function
---@return number index of the added callback in list
function Canvas:addHook(name, callback)
  assert(type(name) == 'string', 'name')
  assert(type(callback) == 'function', 'callback')
  assert(indexof(Canvas.HOOK_NAMES, name), 'expected known hook name')

  self.hooks = self.hooks or {}
  self.hooks[name] = self.hooks[name] or {}
  local t = self.hooks[name]
  local idx = #t + 1
  t[idx] = callback

  log_debug("addHook", name, idx)
  return idx
end

-- replace old hook by given index
---@param name string
---@param idx number
---@param new_callback function|nil
function Canvas:setHook(name, idx, new_callback)
  assert(type(name) == 'string', 'name')
  assert(type(new_callback) == 'function', 'callback')
  assert(indexof(Canvas.HOOK_NAMES, name), 'expected known hook name')

  self.hooks = self.hooks or {}
  self.hooks[name] = self.hooks[name] or {}
  local updated
  if self.hooks[name][idx] then
    self.hooks[name][idx] = new_callback
    updated = true
  end
  log_debug("setHook", name, idx, updated)
  return updated
end

---@param name string
---@param hook_callback function
function Canvas:removeHook(name, hook_callback)
  assert(type(name) == 'string', 'expected string name got: ' .. type(name))
  assert(type(hook_callback) == 'function',
    'expected function callback got: ' .. type(hook_callback))

  self.hooks = self.hooks or {}
  self.hooks[name] = self.hooks[name] or {}
  local removed
  local i = utbl.tbl_indexof(self.hooks[name], hook_callback)
  if i then
    removed = table.remove(self.hooks[name], i) == hook_callback
  end

  log_debug("removeHook", name, i, removed)
  return removed
end

function Canvas:clearHooks(name)
  if name == '*' then
    self.hooks = nil
    return true
  elseif (self.hooks or E)[name] then
    self.hooks[name] = {}
    return true
  end
  return false
end

---@return number
function Canvas:hooksCount(name)
  name = name or '*'

  if self.hooks then
    if name == '*' then
      local c = 0
      for _, hooks in pairs(self.hooks) do
        c = c + #hooks
      end
      return c
    elseif self.hooks[name] then
      return #self.hooks[name]
    else
      return -1
    end
  end

  return 0
end

---@param self self
---@param hook_name string
local function apply_hooks(self, hook_name)
  local t = (self.hooks or E)[hook_name]
  if t then
    for _, cb in ipairs(t) do
      cb(self)
    end
  end
end

local function smallest_first(a, b)
  if a < b then return a, b else return b, a end
end
--
-- merget two char into one
--
---@param a string?
---@param b string?
---@format disable-next
function Canvas.merge_colors(a, b)
  if not Canvas.MERGE_COLORS then return b end
  if not a or a == '' then return b end
  if not b or b == '' then return a end
  local top = b
  a, b = smallest_first(a, b)
  -- =

      if a == '-' and b == '_' then return '='
  elseif a == '-' and b == '|' then return '+'
  elseif a == '-' and b == '~' then return '='
  elseif a == '|' and b == '~' then return '+'
  elseif a == '-' and b == '.' then return 'v'
  elseif a == '\''and b == '-' then return '^'
  elseif a == '\''and b == '.' then return ':'
  elseif a == '.' and b == '`' then return ':'
  elseif a == '+' and b == '~' then return '*' -- or # ?
  elseif a == '+' and b == '=' then return '#'
  elseif a == '=' and b == '|' then return '#'
  elseif a == '+' and b == 'o' then return '*'
  elseif a == '(' and b == ')' then return 'O'
  elseif a == 'O' and b == 'a' then return '@'
  elseif a == 'S' and b == '|' then return '$'
  elseif a == '/' and b == '\\' then return 'X'
  elseif a == '\'' and b == '`' then return '"'
  -- elseif a == '.' and b == '|' then return '!'
  end

  return top
end

--------------------------------------------------------------------------------
--                              Layer
--------------------------------------------------------------------------------

---@param layer table
---@param border string? "tbrl"
function P.draw_border(layer, border)
  assert(type(layer) == 'table', 'layer')
  if border then
    local height = #layer
    local width = #(layer[1])
    --string.find(border, 't') then
    -- ordered
    for i = 1, #border do
      local c = border:sub(i, i)
      if c == 't' or c == 'T' then
        for x = 1, width do layer[1][x] = '-' end
      elseif c == 'b' or c == 'B' then
        for x = 1, width do layer[height][x] = '-' end
      elseif c == 'r' or c == 'R' then
        for y = 1, height do layer[y][width] = '|' end
      elseif c == 'l' or c == 'L' then
        for y = 1, height do layer[y][1] = '|' end
      end
    end
  end
end

---@return table
---@param width number
---@param height number
---@param bg_color string?
---@param border string?
function P.create_layer(width, height, bg_color, border)
  local layer = {}
  bg_color = bg_color or Canvas.DEF_BACKGROUND

  for y = 1, height do
    local row = {}
    for x = 1, width do
      row[x] = bg_color
    end
    layer[y] = row
  end

  P.draw_border(layer, border)

  return layer
end

---@param layern number?
---@param x number
---@param y number
---@return number
---@return number
---@return string? cell
function Canvas:getFirstNotEmptyLayoutCell(layern, x, y, direction)
  local layer = self:getLayer(layern)

  if direction == 'U' then
    for yi = y - 1, 1, -1 do
      local cell = (layer[yi] or E)[x]
      if cell ~= nil and cell ~= '' then return x, yi, cell end
    end
    return x, 1, nil
    --
  elseif direction == 'D' then
    for yi = y + 1, self.height do
      local cell = (layer[yi] or E)[x]
      if cell ~= nil and cell ~= '' then return x, yi, cell end
    end
    return x, self.height, nil
    --
  elseif direction == 'R' then
    local row = layer[y] or E
    for xi = x + 1, self.width, 1 do
      local cell = row[xi]
      if cell ~= nil and cell ~= '' then return xi, y, cell end
    end
    return self.width, y, nil
    --
  elseif direction == 'L' then
    local row = layer[y] or E
    for xi = x - 1, 1, -1 do
      local cell = row[xi]
      if cell ~= nil and cell ~= '' then return xi, y, cell end
    end
    return 1, y, nil
    --
  else
    error('Unsupported direction "' .. tostring(direction) .. '"')
  end
end

--------------------------------------------------------------------------------
--                        ICanvas Implementations
--------------------------------------------------------------------------------

function Canvas:getLayer(layern)
  local layer = self.layer
  if layern == 0 then
    layer = self.bg
  end
  return layer
end

---@param layern number?
---@param x number
---@param y number
---@param color string
function Canvas:draw_point(layern, x, y, color)
  local layer = self:getLayer(layern)
  local line = layer[y]
  if line then
    line[x] = Canvas.merge_colors(line[x], color)
  end
end

local function min_first(a, b)
  if a <= b then
    return a, b
  else
    return b, a
  end
end

---@param layern number?
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param color string
function Canvas:draw_line(layern, x1, y1, x2, y2, color)
  log_trace("draw_line", x1, y1, x2, y2, color)

  -- coord out of canvas
  if y1 < 1 and y2 < 1 or y1 > self.height and y2 > self.height then
    log_trace('out ot canvas by y')
    return self
  end
  if x1 < 1 and x2 < 1 or x1 > self.width and x2 > self.width then
    log_trace('out ot canvas by x')
    return self
  end

  local layer = self:getLayer(layern)


  if y1 < 1 then y1 = 1 end
  if y2 < 1 then y2 = 1 end

  if y1 > self.height then y1 = self.height end
  if y2 > self.height then y2 = self.height end

  log_trace("draw_line normalized:", x1, y1, x2, y2)
  local merge_colors = Canvas.merge_colors

  -- point
  if x1 == x2 and y1 == y2 then
    local prev_color = layer[y1][x1]
    layer[y1][x1] = merge_colors(prev_color, color) -- draw_point
    return self
  end

  -- horizontal from left to right

  if y1 == y2 then
    -- x1 < x2
    x1, x2 = min_first(x1, x2)
    local row = layer[y1]
    for x = x1, x2, 1 do
      row[x] = merge_colors(row[x], color)
    end

    -- vertical
  elseif x1 == x2 then
    -- y1 < y2
    y1, y2 = min_first(y1, y2)
    for y = y1, y2 do
      local row = layer[y]
      if not row then
        error(fmt('out of range y:%s y1:%s y2:%s height:%s',
          v2s(y), v2s(y1), v2s(y2), v2s(self.height)))
      end
      row[x1] = merge_colors(row[x1], color)
    end
  else
    error('Not implemented yet for diagonals')
  end

  return self
end

--
-- only the boundaries of the rectangular area without filling
--
---@param layern number?
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param color string?
function Canvas:draw_rect(layern, x1, y1, x2, y2, color)
  log_trace("draw_rect", x1, y1, x2, y2, color)
  color = color or '~'

  self:draw_line(layern, x1, y1, x1, y2, color) -- left
  self:draw_line(layern, x2, y1, x2, y2, color) -- right

  self:draw_line(layern, x1, y1, x2, y1, color) -- top
  self:draw_line(layern, x1, y2, x2, y2, color) -- bottom
end

--
--
--
---@param letter string
---@param t table
local function fsm_draw_letter(letter, t)
  if letter == "\n" then
    t.y = t.y + 1
    t.x = t.x0
  else
    local x, y, mx, my = t.x, t.y, t.mx, t.my
    if x <= mx and y <= my then
      -- print('fsm_draw_letter draw', letter, t.x, t.y)
      local lt = type(letter)
      if lt ~= 'string' then
        error('nil letter! type:' .. lt)
      end
      local row = t.layer[y]
      if not row then
        if not conf.DEV_MODE then return end
        error(fmt('out of range y:%s rows in layer:%s', v2s(y), v2s(#t.layer)))
      end
      row[x] = letter
    end
    t.x = x + 1
  end
end



--
---@param layern number?
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param text string
function Canvas:draw_text(layern, x1, y1, x2, y2, text)
  local layer = self:getLayer(layern)
  assert(layer, 'layer')
  assert(x2, 'x2')
  assert(y2, 'y2')
  local state = { layer = layer, x0 = x1, x = x1, y = y1, mx = x2, my = y2 }
  u8.foreach_letter(text, fsm_draw_letter, state)
end

--------------------------------------------------------------------------------

--
-- render all drawable objects with a layer recreation
--
---@return self
function Canvas:draw()
  log_trace('Canvas.draw')
  local transparent = ''
  self.layer = P.create_layer(self.width, self.height, transparent, self.border)

  apply_hooks(self, HOOK_PRE_DRAW)

  for _, drawable in ipairs(self.objects) do
    -- todo layers
    drawable:draw(self)
  end

  apply_hooks(self, HOOK_POST_DRAW)

  return self
end

---@return self
function Canvas:rm_objects()
  -- backgroud?
  self.objects = {}
  return self
end

-- return the list of the objects which placed under given in-editor-coords
---@param x number  inner inside canvas
---@param y number
---@param x2 number?  inner inside canvas
---@param y2 number?
---@param limit number?
---@return table<env.draw.IEditable>
---@return table<number> corresponding indexes in the elements list
---@param all boolean? ignore overlap
function Canvas:get_objects_at(x, y, x2, y2, limit, all)
  local t = {}
  local indexes = {}
  local sz = #(self.objects or E)
  x, y, x2, y2 = normalize_range(x, y, x2, y2)

  log_debug('element filter xy-range [%s:%s, %s:%s]', x, y, x2, y2)
  local elements = self.objects

  for i = sz, 1, -1 do -- in pairs(self.objects or E) do
    local o = elements[i]
    if o:isIntersects(x, y, x2, y2) then
      t[#t + 1] = o
      indexes[#indexes + 1] = i
      if limit and #t >= limit or (not all and o:isOverlap(x, y, x2, y2)) then
        break
      end
    end
  end
  return t, indexes
end

--
-- get a list of elements in order of those closest to a given point(x,y)
-- first element in the result list is a closest element.
--
---@param klass table
---@param x number
---@param y number
---@param max_dist number?
---@return table {dist:number, elm:env.draw.IEditble}
function Canvas:get_distsq_elms_to_pos(klass, x, y, max_dist)
  assert(class.is_class(klass), 'class_expected')
  assert(type(x) == 'number', 'x')
  assert(type(x) == 'number', 'y')
  local elements = self.objects

  local t = {}
  local max_distsq = max_dist ~= nil and (max_dist * max_dist) or nil

  local get_distance_sq_to_closest_edge = ugeo.get_distance_sq_to_closest_edge

  for _, elm in ipairs(elements) do ---@cast elm env.draw.IEditable
    if instanceof(elm, klass) then
      local x1, y1, x2, y2 = elm:get_coords()
      local distsq = get_distance_sq_to_closest_edge(x, y, x1, y1, x2, y2)
      if not max_distsq or distsq < max_distsq then
        t[#t + 1] = { dist = distsq, elm = elm }
      end
    end
  end

  table.sort(t, function(a, b)
    return a.dist < b.dist
  end)

  return t
end

---@param patch_order table
---@return number of swapped pairs
function Canvas:swapElementsOrder(patch_order) -- , indexes)
  local t, c = {}, 0
  for _, patch in ipairs(patch_order) do
    local old, new = patch.old, patch.new
    local max = #self.objects
    assert(old > 0 and old <= max, 'old')
    assert(new > 0 and new <= max, 'new')

    if not t[old] and not t[new] then
      local tmp = self.objects[new]
      self.objects[new] = self.objects[old]
      self.objects[old] = tmp
      t[new] = true
      t[old] = true
      c = c + 1
    end
  end
  return c
end

--
-- apply given patch for elements in canvas
--
-- Goal: update elements properties without create a new instances
-- by transferring properties of elements through "serialize"-method
--
-- patch = {elm = {env.draw.IEditable}, old_idx:number, new_idx:number?}
---@param t table {path1, patch2...} patches to modify canvas elements
---@return boolean
---@return table?
function Canvas:apply_patch(t)
  assert(type(t) == 'table', 't')
  local order, errors, elements, max = {}, {}, self.objects, #self.objects
  local updated, chparts, swapped, deleted = 0, 0, 0, 0
  local to_delete = {}

  dprint('apply patch for elements', ':keys:', elements, 'max:', max)

  local function add_error(line)
    errors[#errors + 1] = line
  end

  -- modt is a elm:serialize(nil)
  for _, patch in pairs(t) do
    local modt, old_idx, new_idx = patch.elm_modt, patch.old_idx, patch.new_idx
    new_idx = new_idx or old_idx

    local oelm = elements[old_idx]
    if oelm then
      if not modt and patch.delete then
        elements[old_idx] = false -- mark for deletion
        to_delete[old_idx] = true
        dprint('[to_delete] on delete add: old_idx:', old_idx)
      else
        local elm_patch = mk_patch(oelm, modt)
        local updated0, report = oelm:edit(elm_patch)
        if updated0 then
          updated = updated + 1
          if type(report) == 'table' and report.updated_cnt then
            chparts = chparts + report:updated_cnt()
          end
        end
      end
    else
      add_error('Not Found element with index ' .. tostring(old_idx))
    end

    -- change ordering
    if old_idx ~= new_idx then
      if new_idx > 0 and new_idx <= max then
        if order[new_idx] ~= nil then
          add_error('attempting to reuse a occupied index: ' ..
            v2s(old_idx) .. ' -> ' .. v2s(new_idx))
        else
          order[new_idx] = { elm = oelm, old_idx = old_idx, new_idx = new_idx }
        end
      else
        add_error(fmt('out of range at (%s -> %s) max: %s',
          v2s(old_idx), v2s(new_idx), max))
      end
    end
  end

  if #errors > 0 then
    return false, { updated = updated, chparts = chparts, errors = errors }
  end

  -- apply order changes

  -- has changes in the order
  if next(order) then
    for _, e in pairs(order) do
      elements[e.old_idx] = false -- mark for deletion
      to_delete[e.old_idx] = true
      dprint('[to_delete] on order add old_idx:', e.old_idx)
    end

    for new_idx, e in pairs(order) do
      local elm = e.elm
      assert(new_idx ~= e.old_idx, 'sure swap to new index')
      elements[new_idx] = elm
      to_delete[new_idx] = nil -- mark as used
      swapped = swapped + 1
    end
  end


  -- closing holes in indexes from end to beginning
  if next(to_delete) ~= nil then
    dprint('indexes to_delete', ':keys:', to_delete)
    local tdel = utbl.tbl_keys_sorted(to_delete, utbl.func_compare_descending)

    for _, index in ipairs(tdel) do
      if false ~= elements[index] then
        error(fmt('ensure not used index: %s got: %s',
          v2s(index), v2s(elements[index])))
      end
      table.remove(elements, index)
      deleted = deleted + 1
    end
  end

  return true, {
    updated = updated, -- root elements in canvas
    chparts = chparts, -- childs elements of Container
    swapped = swapped, -- how many elements changed it render order
    deleted = deleted
  }
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Canvas:orderedkeys()
  return ORDERED_KEY_LIST
end

---@return table
function Canvas:serialize(opts) --modt()
  local cname2id = (opts or E).cname2id
  local serialized = {
    width = self.width, height = self.height, border = self.border
  }
  add_class_ref(serialized, Container, cname2id)
  return serialized
end

---@param patch table {x1, y1, x2, y2, color}
function Canvas:edit(patch)
  local Patch = IEditable.cli().Patch
  local update = Patch.update_in
  local bg_color = patch.bg or self.bg_color or ' '

  update(patch, 'width', self)
  update(patch, 'height', self)
  update(patch, 'border', self) -- ''


  -- the fields what require to recreate the backgroud layer
  local has_chages = Patch.changed_keys(patch, 'width', 'height', 'border')
  if has_chages then
    self.bg = P.create_layer(self.width, self.height, bg_color, self.border)
    -- needs hook to: ItemPlaceHolder update lnum_end col_end
  end

  return has_chages > 0, patch
end

function Canvas:setStyle()
  -- ignore
end

function Canvas:getStyle()
  return nil
end

---@param offx number
---@param offy number
function Canvas:move(offx, offy)
  offx = offx
  offy = offy
  -- self.top = self.top + offy
  -- offx = offx -- ignore
end

--
-- x and y - point from which statrs to resize
--
---@param x number
---@param y number
---@param offx number
---@param offy number
---@diagnostic disable-next-line: unused-local
function Canvas:resize(x, y, offx, offy)
  error('Not implemented yet')
end

---@return env.draw.Canvas
function Canvas:copy()
  local arr = self:toArray(nil, true)
  local copy = Canvas.fromArray(nil, arr, true)
  return copy
end

--
-- to string for saving
--
---@param opts table?{new_lines_in_container}
---@return string
function Canvas:serialize_to_binary(opts)
  local head = "{\n" ..
      fmt("  width = %s, height = %s, border = '%s',\n",
        self.width or 0, self.height or 0, self.border or '')
  -- TODO bg as lines

  local cname2id = new_cname2id()
  opts = opts or { newline = '', ident = '' }
  opts.cname2id = cname2id
  -- opts.inspect = { newline = '', indent = '' }

  local bin = "  objects = {\n" -- deep 2
  for _, elm in ipairs(self.objects) do ---@cast elm env.draw.IEditable
    local t = elm:serialize(opts)
    bin = bin .. elm:fmtSerialized(t, 4, opts) .. ",\n"
  end
  bin = bin .. '  },'

  local id2cname = invert_map(cname2id)
  local classes_map = "  classes = " .. slist2str(id2cname, '    ') .. ",\n"

  return head .. classes_map .. bin .. "\n}\n"
end

--
-- build canvas with objects from string
--
---@return env.draw.Canvas
function Canvas.deserialize_from(bin)
  -- todo safe
  local code = 'return ' .. tostring(bin)
  local func = loadstring(code, 'table')
  local t = func ~= nil and func() or nil
  assert(t, 'load luatable')

  local width = assert(t.width, 'width')
  local height = assert(t.height, 'height')
  local border = t.border or ''
  local id2cname = t.classes

  -- todo bg
  local c = Canvas:new(nil, height, width, ' ', border)

  for i, ot in pairs(t.objects) do
    -- local classname = assert(ot._class, 'has classname item:' .. tostring(i))
    -- local C = require(classname)
    -- print(classname)
    local obj = IEditable.deserialize(ot, id2cname)
    if not obj then
      error('cannot deserialize item:' .. tostring(i) .. ' ' .. inspect(obj))
    end
    ---@diagnostic disable-next-line: param-type-mismatch
    c:add(obj)
  end

  return c
end

--------------------------------------------------------------------------------
--                              Helpers
--------------------------------------------------------------------------------

---@param self env.draw.Canvas
---@param n number?
---@param cnt number?
function Canvas.get_elements_readable_list(self, n, cnt)
  n = n or 1
  cnt = cnt or 40
  local t = Canvas.cast(self).objects
  local l = {}
  local s = math.max(0, cnt * (n - 1)) + 1
  local e = math.min(#t, cnt * n)

  for i = s, e do
    l[#l + 1] = fmt('%3d  %s', i, tostring(t[i]))
  end
  if #t >= cnt then
    l[#l + 1] = fmt('Page: #%d/%d[%s]', n, math.ceil(#t / cnt), cnt)
  end

  return l
end

---@param elms table{env.draw.IEditable}
---@param idxs table{number}
---@return table{string}
function Canvas.mk_elements_readable(elms, idxs)
  local t = {}

  for i, elm in ipairs(elms) do
    local index = idxs[i]
    t[#t + 1] = fmt('%3d  %s', index, tostring(elm))
  end

  return t
end

--------------------------------------------------------------------------------
--                         Built-in Hooks
--------------------------------------------------------------------------------

--
-- callback used to display indexes directly above canvas elements
-- indexed to be used with HOOK_POST_DRAW
--
---@param c env.draw.Canvas
function Canvas.hook_display_indexes(c)
  for index, elm in ipairs(c.objects) do
    local x, y = elm:pos()
    local text = tostring(index)
    c:draw_text(nil, x, y, x + #text, y, text)
  end
end

--------------------------------------------------------------------------------
--
if _G.TEST then
  Canvas.P = P
end

class.build(Canvas)
return Canvas

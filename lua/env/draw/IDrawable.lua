--  30-03-2024  @author Swarg
--

local class = require 'oop.class'
local interface, num = class.interface, 'number'
local ICanvas = require("env.draw.ICanvas")

class.package 'env.draw'
---@class env.draw.IDrawable : oop.Interface
---@field draw function(self, env.draw.ICanvas)
---@field get_coords fun(self): number, number, number, number  x1 y1 x2 y2 (left top rigth bottom  in the canvas)
---@field isInsidePos fun(self, x:number, y:number): boolean
---@field isIntersects fun(self, x1:number, y1:number, x2:number, y2:number): boolean
---@field isOverlap fun(self, x1:number, y1:number, x2:number, y2:number): boolean
local IDrawable = class.new_interface(nil, 'IDrawable', {

  -- define signature of method "handle"
  draw = interface.method('self', ICanvas, 'canvas', num, 'layernum'),

  get_coords = interface.method('self'),

  -- -- checking that a given figure completely covers a given area
  -- isOverlap = interface.method('self', num, 'x1', num, 'y1', num, 'x2', num, 'y2'),

  -- is given pos(x,y) inside this(self) figure(x1, y1, x2, y2)
  ---@param x number
  ---@param y number
  ---@return boolean
  isInsidePos = function(self, x, y)
    local x1, y1, x2, y2 = self:get_coords() -- self.x1, self.y1, self.x2, self.y2
    return x1 ~= nil and x >= x1 and x <= x2 and y >= y1 and y <= y2
  end,

  -- If the coordinates of the vertices of the rectangles are normalized, i.e.
  -- it is guaranteed that left < right and bottom < top, then
  -- in order to determine the fact of intersection of two rectangles, the sides
  -- of which are parallel to the coordinate axes, such a check is sufficient

  ---@param x1 number
  ---@param y1 number
  ---@param x2 number
  ---@param y2 number
  ---@return boolean
  isIntersects = function(self, x1, y1, x2, y2)
    local fx1, fy1, fx2, fy2 = self:get_coords()
    return (fx1 <= x2 and fx2 >= x1) and (fy1 <= y2 and fy2 >= y1)
  end,

  --
  --  checking that a given figure completely covers a given area
  --
  ---@param x1 number
  ---@param y1 number
  ---@param x2 number
  ---@param y2 number
  ---@return boolean intersects
  ---@diagnostic disable-next-line: unused-local
  isOverlap = function(self, x1, y1, x2, y2)
    local fx1, fy1, fx2, fy2 = self:get_coords()
    return fx1 <= x1 and fx2 >= x2 and fy1 <= y1 and fy2 >= y2
    -- return false
  end,
})


interface.build(IDrawable)
return IDrawable

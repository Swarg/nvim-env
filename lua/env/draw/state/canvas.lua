-- 09-04-2024 @author Swarg
-- state of CanvasEditor
-- clipboard, undo, canvas
--
local M = {}

local log = require('alogger')
local class = require 'oop.class'
-- local utbl = require("env.draw.utils.tbl")
local ubase = require("env.draw.utils.base")
local ubuf = require("env.draw.utils.buf")
local utbl = require("env.draw.utils.tbl")
local conf = require("env.draw.settings")
local FS = require('env.draw.utils.fs')
-- local ubase = require("env.draw.utils.base")
local Canvas = require('env.draw.Canvas')
local Selection = require("env.draw.ui.Selection");
local IPH = require("env.draw.ui.ItemPlaceHolder")

-- local LINE_WIDTH = conf.LINE_WIDTH
-- local WORK_WITH_UTF8 = conf.WORK_WITH_UTF8
-- local FIND_SYNTAX_BLOCK_LN_LIMIT = conf.FIND_SYNTAX_BLOCK_LN_LIMIT
M.CLIPBOARD_HISTORY_LIMIT = conf.CLIPBOARD_HISTORY_LIMIT
-- local SELECT_LIMIT = conf.SELECT_LIMIT

-- local DEF_RECTANGLE = conf.DEF_RECTANGLE
-- local DEF_LINE = conf.DEF_LINE

--------------------------------------------------------------------------------

local E, v2s, fmt = {}, tostring, string.format
local instanceof = class.instanceof
local log_debug = log.debug
local get_actual_bufnr = ubuf.get_actual_bufnr
local get_editor_coords = ubuf.get_editor_coords
local normalize_range = ubase.normalize_range
local file_exists = FS.file_exists
local file_write_content = FS.file_write_content
local file_read_all = FS.file_read_all



-- in regular buffers and in this CanvasEditor
local ui_items_map = {
  -- bufnr = ItemsPlaceHolder_list { item = Canvas }
  -- [bufnr] = { [1] = ItemsPlaceHolder, [2] = ItemsPlaceHolder, ...}
}
local ui_item_instances = {
  -- [canvas] = {box1, box2, boxN}
}

-- clipboard of list of Selections
local clipboard_buffer = {
  -- Selection, Selection,
  -- Selection.items - table<env.draw.IEditable>
}
-- TODO for undo
local deleted_history = {}

local last_selection = nil
local last_selection_copy = nil

--------------------------------------------------------------------------------

function M.dump_state()
  _G.__env_draw_ui_items_map = {
    ui_items_map = ui_items_map,
    ui_item_instances = ui_item_instances,
  }
end

function M.restore_state()
  -- ui_items_map =
  local state = _G.__env_draw_ui_items_map
  _G.__env_draw_ui_items_map = nil
  ui_items_map = {}      -- state.ui_items_map
  ui_item_instances = {} -- state.ui_item_instances

  -- recreate objects states with new Classes
  local clone_with_new_class = class.serialize_helper.clone_with_new_class

  for bufnr, list in pairs(state.ui_items_map) do
    local new_list = {}
    for n, itemsPlaceHolder in ipairs(list) do
      new_list[n] = clone_with_new_class(itemsPlaceHolder)
    end
    ui_items_map[bufnr] = new_list
  end

  for canvas, list in pairs(state.ui_item_instances) do
    local newCanvas = clone_with_new_class(canvas)
    local new_list = {}
    for n, box in ipairs(list) do
      new_list[n] = clone_with_new_class(box)
    end
    ui_item_instances[newCanvas] = new_list
  end
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

---@param box env.draw.ui.ItemPlaceHolder
---@param fn string?
---@param can_rewrite boolean?
---@param ask boolean? can ask user confirmation to rewrite existed file
---@param confirm_callback function?
---@return boolean
---@return string|env.draw.Canvas canvas or err_msg
function M.save_canvas(box, fn, can_rewrite, ask, confirm_callback)
  if not box.filename and not fn then
    return false, 'no filename'
  elseif type(box.filename) == 'string' and not fn then
    fn = box.filename
  end

  if fn and fn ~= box.filename and file_exists(fn) and not can_rewrite then
    if ask and type(confirm_callback) ~= 'function' then
      return false, 'cannot ask confirmation to rewrite file - no callback'
    end
    ---@cast confirm_callback function
    if ask and confirm_callback('rewrite existed file? ' .. v2s(fn)) then
      -- print(fmt('rewrite %s ...', fn))
    else
      return false, 'file already existed ' .. v2s(fn) -- .. ' use --rewrite'
    end
  end

  if not fn then
    return false, 'no fn box.fn:' .. v2s(box.filename) .. ' fn:' .. v2s(fn)
  end

  local bin = Canvas.cast(box.item):serialize_to_binary()
  local saved = file_write_content(fn, bin)
  -- try to resave with mkdir -p
  if not saved then
    local dir = FS.get_parent_dirpath(fn)
    if not file_exists(dir) and ask then
      local msg = 'Create dir ' .. tostring(dir) .. '?'
      if ask and confirm_callback and confirm_callback(msg) then
        os.execute('mkdir -p ' .. tostring(dir))
        saved = file_write_content(fn, bin)
      end
    end
  end

  return true, "Canvas Saved: " .. v2s(saved) .. ' ' .. fn
end

---@param fn string
---@return boolean
---@return string|env.draw.Canvas canvas or err_msg
function M.load_canvas(fn)
  if not fn or fn == '' then
    return false, 'no filename'
  end
  if not file_exists(fn) then
    return false, 'Not Found "' .. tostring(fn) .. '"'
  end

  local bin = file_read_all(fn)
  if not bin then
    return false, 'Cannot read file ' .. tostring(fn)
  end

  local canvas = Canvas.deserialize_from(bin)
  if not canvas then
    return false, 'Cannot deserialize canvas from ' .. tostring(fn)
  end

  return true, canvas
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

-- restore via cmd
function M.get_deleted_readable_list(n, cnt)
  n = n or 1
  cnt = cnt or 40
  local l, t = {}, deleted_history
  local s = math.max(0, cnt * (n - 1)) + 1
  local e = math.min(#t, cnt * n)

  for i = s, e do
    local selection = Selection.cast(t[i])
    l[#l + 1] = fmt('%3d  %s', i, tostring(selection))
  end
  l[#l + 1] = fmt('Page: #%d/%d[%s]', n, math.ceil(#t / cnt), cnt)

  return l
end

function M.deleted_hist_add_entry(selection)
  -- for undo
  local limit = M.CLIPBOARD_HISTORY_LIMIT / 2
  while #deleted_history >= limit do
    table.remove(deleted_history, 1)
  end

  deleted_history[#deleted_history + 1] = selection
end

---@param idx number?
---@return table?
---@return number size
function M.deleted_hist_get_entry(idx)
  return deleted_history[idx or false], #deleted_history
end

---@return env.draw.ui.Selection
---@param idx number
---@param selection env.draw.ui.Selection
function M.deleted_hist_remove_entry_safely(idx, selection)
  assert(selection == table.remove(deleted_history, idx), 'same instance')
  return Selection.cast(selection)
end

--------------------------------------------------------------------------------

---@param limit number
function M.clipboard_set_limit(limit)
  assert(type(limit) == 'number' and limit > 0, 'limit')
  M.CLIPBOARD_HISTORY_LIMIT = limit
end

function M.clipboard_is_empty()
  return #clipboard_buffer == 0
end

function M.get_clipboard()
  return clipboard_buffer, deleted_history
end

function M.clear_clipboard()
  clipboard_buffer = {}
end

-- history of deleted selections(an set of deleted canvas elements)
function M.clear_deleted_history()
  deleted_history = {}
end

---@param n number?
---@param cnt number?
---@return table{string}
function M.get_clipboard_readable_list(n, cnt)
  n = n or 1
  cnt = cnt or 40
  local list = clipboard_buffer

  local s = math.max(0, cnt * (n - 1)) + 1
  local e = math.min(#list, cnt * n)
  local t = {}
  for i = s, e do
    local selection = list[i]
    t[#t + 1] = fmt('%3d  %s', i, tostring(selection))
  end
  t[#t + 1] = fmt('Page: #%d/%d[%s]', n, math.ceil(#list / cnt), cnt)

  return t
end

--
-- take from cb and place to canvas
--
---@param n number
function M.clipboard_take(n)
  M.clipboard_get_entry(n)
  if clipboard_buffer[n or false] then
    return M.do_paste(n)
  end
  return nil
end

---@param n number
---@return boolean
---@return string|table? --  selection
function M.clipboard_drop(n)
  n = n or #clipboard_buffer
  if n < 0 then
    n = #clipboard_buffer + n + 1 -- from end
  end
  if n <= 0 then n = 1 end

  if clipboard_buffer[n] then
    local wi = table.remove(clipboard_buffer, n)
    return true, wi
  end
  return false, 'max index: ' .. tostring(#clipboard_buffer)
end

--
-- add selections(items) into clipboard
--
---@param selection env.draw.ui.Selection
-- function M.add_to_clipboard(selection)
function M.clipboard_add_entry(selection)
  while #clipboard_buffer >= M.CLIPBOARD_HISTORY_LIMIT do
    table.remove(clipboard_buffer, 1)
  end

  local idx = #clipboard_buffer + 1
  clipboard_buffer[idx] = selection

  return idx
end

-- get or popup if element is sel_typ == 'CUT'
---@param n number?
---@return env.draw.ui.Selection
---@return boolean needs copy on paste
-- function M.get_from_clipboard(n)
function M.clipboard_get_entry(n, keep)
  n = n or #clipboard_buffer
  if n < 0 then
    n = #clipboard_buffer + n + 1 -- from end
  end
  if n <= 0 then n = 1 end

  local selection = clipboard_buffer[n]

  local needs_copy = true
  if not keep then
    if selection and selection:isCut() then
      assert(selection == table.remove(clipboard_buffer, n), 'sure deleted')
      needs_copy = false
    end
  end

  return selection, needs_copy
end

---@param n number?
---@return env.draw.ui.Selection
-- function M.delete_from_clipboard(n)
function M.clipboard_delete_entry(n)
  n = n or #clipboard_buffer
  if n < 0 then
    n = #clipboard_buffer + n + 1 -- from end
  end
  if n <= 0 then n = 1 end

  return table.remove(clipboard_buffer, n)
end

--

-- for primitive undo: cut/paste/undo-paste

---@param selection env.draw.ui.Selection?
---@param copy boolean?
function M.setLastSelection(selection, copy)
  last_selection = selection
  last_selection_copy = copy
end

---@return env.draw.ui.Selection?
---@return boolean?
function M.getLastSelection()
  return last_selection, last_selection_copy
end

--------------------------------------------------------------------------------

---@param bufnr number?
---@return table
function M.get_local_canvas_list(bufnr)
  bufnr = get_actual_bufnr(bufnr)
  ui_items_map[bufnr] = ui_items_map[bufnr] or {}
  local list = ui_items_map[bufnr]
  return list
end

function M.get_buffers_with_canvas()
  local s = ''
  for bufnr, list in pairs(ui_items_map) do
    s = s .. fmt('bufnr:%s  canvases:%s', bufnr, #(list)) .. "\n"
  end
  return s
end

---@param bufnr number?
---@param rowb number
---@param rowe number?
---@param colb number?
---@param cole number?
---@return env.draw.ui.ItemPlaceHolder?
---@return number? idx
---@return table?
function M.get_canvas_box(bufnr, rowb, rowe, colb, cole)
  local list = M.get_local_canvas_list(bufnr)

  rowb, colb, rowe, cole = normalize_range(rowb, colb or 0, rowe, cole)

  for i, box in pairs(list) do
    if IPH.isIntersects(box, rowb, rowe, colb, cole) then
      return box, i, list
    end
  end

  return nil, -1, list
end

---@param bufnr number
---@param index number
function M.get_canvas(bufnr, index)
  return ((M.get_local_canvas_list(bufnr) or E)[index or false] or E).item
end

---@param canvas env.draw.Canvas
---@param bufnr number
---@param lnum number?
---@param col number?
---@param fn string?
---@param draw_cb function
function M.add_ui_item(canvas, bufnr, lnum, col, fn, draw_cb)
  assert(instanceof(canvas, Canvas),
    'expected Canvas instance got:' .. v2s(class.name(canvas)))
  assert(type(draw_cb) == 'function', 'draw_callback')

  lnum = lnum or 0 -- 1?
  col = col or 0

  local list = M.get_local_canvas_list(bufnr)
  local box = IPH.wrap(canvas, bufnr, lnum, col, fn, draw_cb)
  local idx = #list + 1
  list[idx] = box

  ui_item_instances[canvas] = ui_item_instances[canvas] or {}
  table.insert(ui_item_instances[canvas], box)

  return idx, box
end

--
-- find instance of the canvas wrapped without exceptions
--
-- get box with canvas by buffnr and just a index in list of items
---@return env.draw.ui.ItemPlaceHolder?     -- wrapped canvas
---@return table{env.draw.ItemPlaceHolder}  -- list of canvases
function M.get_ui_item(bufnr, idx)
  local list = M.get_local_canvas_list(bufnr)
  return (list or E)[idx or false], list
end

-- find index of the given canvas wrapper
function M.get_ui_item_index(bufnr, item)
  local list = M.get_local_canvas_list(bufnr)
  if list then
    for i, v in ipairs(list) do
      if v == item then
        return i
      end
    end
  end
  return nil
end

--
-- throw exceptions if instance for given place are not found
--
---@return env.draw.ui.ItemPlaceHolder
function M.get_ui_item_with_validate(bufnr, idx)
  local box, list = M.get_ui_item(bufnr, idx)
  if not box then
    local sz = #(list or E)
    error(fmt('not found box in bufnr: %s idx:%s/%s', v2s(bufnr), v2s(idx), sz))
  end
  return box
end

-- ---@param bufnr number
-- ---@param idx number
-- ---@param new_canvas env.draw.Canvas
-- ---@param old_box env.draw.ui.ItemPlaceHolder?
-- function M.set_canvas(bufnr, idx, new_canvas, old_box)
--   assert(type(idx) == 'number', 'idx')
--   local list = M.get_local_canvas_list(bufnr)
--   assert(list[idx] == old_box, 'ensure replace exact old_canvas')
--
--   local old_box = list[idx]
--   list[idx] = IPH.wrap(new_canvas,,)
--
--   error('Not implemented yet')
--
--   -- TODO check intersection with the nearest canvas
--
--   return old_box, idx
-- end

---@param bufnr number?
---@param idx number?
---@param box env.draw.ui.ItemPlaceHolder? (canvas)
---@return env.draw.ui.ItemPlaceHolder?
function M.remove_ui_item(bufnr, idx, box)
  local list = M.get_local_canvas_list(bufnr)
  if idx then
    if box and list[idx] == box then
      list[idx] = list[#list] -- just swap with latest
      list[#list] = nil

      local instances = ui_item_instances[box.item]
      local i = utbl.tbl_indexof(instances, box)
      if not i or not instances then
        local keys = utbl.tbl_keys_count(ui_item_instances)
        error(fmt('Not found box:"%s" in instances: %s', v2s(box), keys))
      end
      assert(table.remove(instances, i) == box, 'sure removed')

      return box
    else
      error('Attempt to delete another box from a list.' ..
        ' Bad index: ' .. tostring(idx) ..
        ' given: ' .. tostring(box)
        .. ' actual: ' .. tostring(list[idx]))
    end
  end
  return nil
end

-- update loaded UI-item of same instance in more than one wrapped box
-- Goal: sync instance of same canvas on load and pass assigned filename
--
---@param old_canvas env.draw.Canvas
---@param new_canvas env.draw.Canvas
---@param filename string
---@return number updated box
function M.update_ui_item(old_canvas, new_canvas, filename)
  assert(type(old_canvas) == 'table', 'old canvas')
  assert(type(new_canvas) == 'table', 'new canvas')

  local instances = ui_item_instances[old_canvas] or E
  local c = 0
  if #instances > 1 then
    for _, box in pairs(instances) do
      if box.item == old_canvas then
        box:setItem(Canvas.cast(new_canvas), filename)
        c = c + 1
      end
    end
    if old_canvas ~= new_canvas then
      ui_item_instances[old_canvas] = nil
      ui_item_instances[new_canvas] = instances
    end
  end

  return c
end

--
-- return current context for position in the current buffer
--
---@return env.draw.ui.ItemPlaceHolder{env.draw.ICanvas}
---@return number lnum
---@return number col
---@return number lnum2
---@return number col2
---@return table?{env.draw.IEditable} canvas elements
---@return table?{number} corresponding indexes of the elements
function M.get_ctx(find_obj, limit)
  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  log_debug('get_ctx bufnr:%s [%s:%s, %s:%s]', bufnr, lnum, col, lnum2, col2)

  local box, _, list = M.get_canvas_box(bufnr, lnum, lnum2, col, col2)
  if not box then
    error(fmt('Not Found Canvas for current bufnr:%s/%s', bufnr, #(list or E)))
  end

  local objects, indexes = nil, nil
  if find_obj then
    log_debug('find_elements lnum:col [%s:%s, %s:%s]', lnum, col, lnum2, col2)
    objects, indexes = box:get_objects_at(lnum, col, lnum2, col2, limit)
  end
  return box, lnum, col, lnum2, col2, objects, indexes
end

--
---@param bufnr number
function M.get_readable_canvas_list(bufnr, loc)
  local function list2str(list)
    local s = ''
    for k, v in pairs(list) do
      s = s .. v2s(k) .. '  ' .. tostring(v) .. "\n"
    end
    return s
  end

  local s = ''
  if loc then
    s = list2str(M.get_local_canvas_list(bufnr) or {})
  else
    for bufn, list in pairs(ui_items_map) do
      if next(list) then
        local ok, ft = pcall(vim.api.nvim_buf_get_option, bufn, 'filetype')
        ---@diagnostic disable-next-line: cast-local-type
        if not ok then ft = '?' end

        s = s .. "bufnr #" .. v2s(bufn) .. " (" .. v2s(ft) .. "):\n"
            .. list2str(list) .. "\n"
      end
    end
  end
  if #s == 0 then s = 'empty' end
  return s
end

---@param bufnr number
---@diagnostic disable-next-line: unused-local
function M.getCanvasName(canvas, bufnr, lnum, col)
  -- TODO object hash for canvas without bufnr+lnum ?
  return 'Canvas-' .. tostring(bufnr) .. ':' .. tostring(lnum) .. ':' .. tostring(col)
end

---@param box env.draw.ui.ItemPlaceHolder?
function M.getReadableItemPlace(box)
  return IPH.shortly(box)
end

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

if _G.TEST then
  M.get_ui_items_map = function() return ui_items_map end
  M.get_ui_item_instances = function() return ui_item_instances end
  M.clear_all_ui_items = function() ui_items_map, clipboard_buffer = {}, {} end

  M.get_cb = function()
    return clipboard_buffer
  end
  M.deleted_history = deleted_history

  M.clear_all = function()
    M.clear_deleted_history()
    M.clear_clipboard()
    M.clear_all_ui_items()
  end
end

return M

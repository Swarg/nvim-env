-- 01-04-2024  @author Swarg
--
-- UI Item PlaceHolder (ICanvas-Wrapper)
--
-- Goal:
-- - wrapper around ICanvas instances to provide way to edit same object(Canvas)
--   in multiples windows(buffers).
-- - keep position of the item(ICanvas) in the buffer
-- - store the name of the file in which this canvas is saved


local class = require 'oop.class'
local log = require('alogger')
local u8 = require("env.util.utf8")
local ubuf = require("env.draw.utils.buf")

local log_trace = log.trace

class.package 'env.draw.ui'

---@class env.draw.ui.ItemPlaceHolder: oop.Object
---@field item table
---@field bufnr number
---@field lnum number
---@field lnum_end number
---@field col number
---@field col_end number
---@field filename string?
---@field has_changes boolean?
---@field marked_positions table?{{x,y},{x,y}}
---@field render function?
---@field new fun(self, table?): env.draw.ui.ItemPlaceHolder
local ItemPH = class.new_class(nil, 'ItemPlaceHolder', {
})

ItemPH.DEF_COL_END = 80

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- constructor used in Container:new()
function ItemPH:_init()
  -- if self.item and  self.lnum_end self.col_end TODO
end

---@return env.draw.ui.ItemPlaceHolder
function ItemPH.cast(obj)
  if type(obj) and getmetatable(obj) == ItemPH then
    return obj
  else
    error('expected instance of Container got: ' .. v2s(class.name(obj)))
  end
end

local function validate_item(item)
  assert(type(item) == 'table', 'item')
  assert(type(item.height) == 'number', 'expected item.height:number')
  assert(type(item.width) == 'number', 'expected item.widt:number')
end

---@param item table{height:number}
---@param bufnr number
---@param lnum number
---@param col number?
---@return env.draw.ui.ItemPlaceHolder
function ItemPH.wrap(item, bufnr, lnum, col, fn, render)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(lnum) == 'number', 'lnum')
  validate_item(item)
  col = col or 0

  log_trace('wrap bufnr:%s lnum:%s col:%s item: (%s)', bufnr, lnum, col, item)
  return ItemPH:new({
    item = item,
    bufnr = bufnr,
    lnum = lnum,                   -- top
    lnum_end = lnum + item.height, -- bottom
    col = col,
    col_end = col + item.width,
    filename = fn,
    has_changes = false,
    marked_positions = nil,
    render = render,
  })
end

-- replace the old item by the new one
---@return table replaced item
---@return string?
function ItemPH:setItem(item, fn)
  validate_item(item)
  local old_item, old_fn = self.item, self.filename
  self.item = item
  self.lnum_end = self.lnum + item.height
  self.col_end = self.col + item.width
  self.filename = fn or self.filename
  self.has_changes = true

  return old_item, old_fn
end

--
-- Check is given container intersects with given range
--
---@param box self
---@param y1 number
---@param y2 number?
---@param x1 number?
---@param x2 number?
function ItemPH.isIntersects(box, y1, y2, x1, x2)
  assert(type(box) == 'table', 'container')
  assert(type(box.lnum) == 'number', 'container lnum (top)')
  assert(type(box.lnum_end) == 'number', 'container lnum_end (bottom)')
  assert(type(y1) == 'number', 'y1')
  y2 = y2 or y1
  x1 = x1 or 0
  x2 = x2 or ItemPH.DEF_COL_END

  if box and y1 <= y2 then
    local ln, ln_end, cn, cn_end = box.lnum, box.lnum_end, box.col, box.col_end

    local vertical = ln <= y1 and ln_end >= y1 or ln <= y2 and ln_end >= y2

    local horizontal = vertical and
        (cn <= x1 and cn_end >= x1 or cn <= x2 and cn_end >= x2)

    return vertical and horizontal
  end
end

function ItemPH:__tostring()
  local cn = class.name(self.item) or type(self.item)
  local iteminfo = ''
  if (self.item or E).__tostring then
    iteminfo = ' ' .. tostring(self.item)
  end
  return fmt('ItemPH lnum:%s:%s col:%s:%s (%s)%s',
    v2s(self.lnum), v2s(self.lnum_end), v2s(self.col), v2s(self.col_end),
    v2s(cn), iteminfo
  )
end

-- in nvim format
---@param self env.draw.ui.ItemPlaceHolder?
function ItemPH.shortly(self)
  local s, e, w, h, elms = -2, -2, -1, -1, -1
  if self and self.lnum and self.lnum_end then
    s, e = self.lnum, self.lnum_end
    if self.item and self.item.width and self.item.height then
      w, h = self.item.width or -1, self.item.height or -1
    end
    if type(self.item.objects) == 'table' then
      elms = #self.item.objects
    end
  end
  return fmt('lnum: %s:%s size:%sx%s elms:%s', s + 1, e + 1, w, h, elms)
end

---@param top number
---@param left number
---@param lnum number
---@param col number
function ItemPH.toInnerXY0(top, left, lnum, col)
  local y, x = lnum - top + 1, col - left
  return x, y
end

--
-- note get_ctx give col as letter number not as raw byte in utf8 strnig
--
---@return number
---@return number
function ItemPH:toInnerXY(lnum, col)
  local x, y = col - self.col + 1, lnum - self.lnum + 1
  return x, y
end

--
-- simple conversion without taking into account utf8 characters in the
-- lnum line
--
---@param x number
---@param y number
---@return number lnum
---@return number col
function ItemPH:toOuterXY(x, y)
  local col, lnum = self.col + x - 1, self.lnum + y - 1
  return lnum, col
end

--
-- conversion of coordinates inside the canvas to coordinates in the editor
-- buffer, taking into account utf8 characters in the line
--
---@param x number
---@param y number
---@return number lnum
---@return number col
---@return string? line
function ItemPH:toOuterLnumCol(x, y)
  local lnum, col = self:toOuterXY(x, y) -- here col is position in bytes
  local line = nil

  if lnum > 0 and col > 0 then
    -- make utf8 fix (bytes to letter num)
    line = ubuf.get_raw_line(self.bufnr, lnum)
    if line then
      local new_cn, len = u8.byte_pos_of_letter(line, col)
      col = new_cn + len - 1
    end
  end

  return lnum, col, line
end

--
-- add the given object to the wrapped item
--
-- create sub Drawable Object for Item
-- like Rectangle for Canvas
-- Idea:
--  - new object created with x1:0 y1:0 coords
--  - convert absolute coords(lnum, col) to inner for item(canvas)
--  - move object to converted inner coords
--  - add object to the item via item.add(obj)
--
---@param obj table (IDrawable)
---@param lnum number current absolute line number in the doc(buffer)
---@param col number -//-
function ItemPH:add(obj, lnum, col)
  assert(self.item and type(self.item.add) == 'function') -- ICanvas
  assert(obj and type(obj.move) == 'function')            -- IDrawable

  local x, y = self:toInnerXY(lnum or 0, col or 0)
  obj:move(x, y)
  log_trace('add obj:(%s) x:%s %y:%s', obj, x, y)
  return self:addWithCoords(obj)
end

--
-- add an element with already setuped coordinates
--
--- param obj env.draw.IEditable|env.draw.IDrawable
function ItemPH:addWithCoords(obj)
  assert(self.item and type(self.item.add) == 'function') -- ICanvas
  assert(obj and type(obj.move) == 'function')            -- IDrawable

  self.item:add(obj)
  self:changed()

  return obj
end

-- mark as having changes that need to be saved
---@return self
function ItemPH:changed()
  if self then
    self.has_changes = true
  end
  return self
end

---@return boolean
function ItemPH:isChanged()
  return self.has_changes == true
end

--
-- move the given object to top-left position (0:0)
--
---@return number old_x
---@return number old_y
function ItemPH.resetCoords(obj)
  local old_x, old_y = obj:pos()
  if old_x == nil or old_y == nil then
    error(fmt('Seems the class %s needs to override method IEditable:pos' ..
      ' ret x:%s y:%s', v2s(class.name(obj)), v2s(old_x), v2s(old_y)))
  end
  obj:move(-old_x, -old_y) -- set top left position to 0:0

  return old_x, old_y
end

--
-- remove the given object from the wrapped item
--
---@param obj env.draw.IEditable
---@return env.draw.IEditable?
---@return number? prev pos_x
---@return number? prev pos_y
function ItemPH:remove(obj)
  assert(self.item and type(self.item.add) == 'function') -- ICanvas
  assert(obj and type(obj.move) == 'function')            -- IDrawable & IEditable

  local x, y
  if self.item:remove(obj) then
    x, y = ItemPH.resetCoords(obj)
    log_trace('removed (%s) from pos(%s:%s)', obj, x, y)
    self.has_changes = true
    return obj, x, y
  else
    log_trace('Not Found in canvas obj:%s', obj)
    return nil, nil, nil -- not found in canvas!
  end
end

--
-- return the list of the objects which placed under given in-editor-coords
--
---@param lnum number absolute lnum in the editor
---@param col number
---@param lnum2 number
---@param col2 number
---@param limit number?
function ItemPH:get_objects_at(lnum, col, lnum2, col2, limit)
  assert(type(lnum) == 'number', 'lnum')
  assert(type(col) == 'number', 'col')

  local x, y = self:toInnerXY(lnum, col)
  local x2, y2 = self:toInnerXY(lnum2 or lnum, col2 or col)

  return self.item:get_objects_at(x, y, x2, y2, limit)
end

---@param self self?
function ItemPH.maxElementIndex(self)
  if self and self.item then
    return #(self.item.objects or E)
  end
  return -1
end

--------------------------------------------------------------------------------

local function get_last_marked_pos(self)
  local t = (self or E).marked_positions
  if t then
    return t[#t], #t
  end
  return nil, 0
end


--
---@param x number
---@param y number
---@return table?{x,y}
---@return number count of positions
function ItemPH:mark_pos(x, y)
  assert(type(x) == 'number', 'x')
  assert(type(y) == 'number', 'y')

  self.marked_positions = self.marked_positions or {}
  local prev, count = get_last_marked_pos(self)
  local t = self.marked_positions
  t[#t + 1] = { x = x, y = y }

  return prev, count
end

function ItemPH:has_marked_pos()
  return self.marked_positions ~= nil and next(self.marked_positions) ~= nil
end

---@return self
function ItemPH:clear_marked_pos()
  self.marked_positions = nil
  return self
end

---@return table?{x,y}
---@return number
function ItemPH:pop_marked_pos()
  local prev = get_last_marked_pos(self)
  local remains = 0
  if prev then
    local t = self.marked_positions
    t[#t] = nil
    remains = #t
  end
  return prev, remains
end

---@return table?
function ItemPH:pop_marked_positions()
  local list = self.marked_positions
  self.marked_positions = nil
  return list
end

---@return table?
function ItemPH:get_marked_positions()
  return self.marked_positions
end

---@return number? x
---@return number? y
function ItemPH:getMarkedPosXY()
  local t = get_last_marked_pos(self)
  if t then
    return t.x, t.y
  end
  return nil, nil
end

---@return number
function ItemPH:get_marked_pos_count()
  local _, count = get_last_marked_pos(self)
  return count
end

function ItemPH:draw()
  if type(self.render) == 'function' then
    self.render(self)
  end
end

class.build(ItemPH)
return ItemPH

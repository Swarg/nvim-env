--  02-04-2024  @author Swarg
-- Goal:
-- selection is a container to keep elements from the canvas.
-- to be able to select, move, cut and then paste them back to canvas
--  - intended to be used in:
--     - clipboard history
--     - deletions history
--     - selection in the canvas ??
--  - all containing elements have coordinates from the beginning of the selection
--  - xoff yoff - is a offsets relative to the cursor at the time of cutting
--    elements from the canvas( used in past back relatively of the cursor)
--  - width, height - size of the selection(otional)
--
--  - when the selection simply contains references to canvas elements that will
--    not be cut from it then its doesnt change coords of elements and xoff:yoff
--    contains coord of selection top-left position in the canvas ?
--
-- provide the ability to cut an item(drawable object) from the canvas and paste
-- it into a new location while maintaining the offset relative to the cursor

local class = require 'oop.class'

class.package 'env.draw.ui'

---@class env.draw.ui.Selection : oop.Object
---@field new fun(self, o:table?, type:number, items:table<env.draw.IEditable>, x1:number?, y1:number?, x2:number?, x2:number?, xoff:number?, yoff:number?): env.draw.ui.Selection
---@field type number select:1|cut:2|copy:3
---@field items env.draw.IEditable
---@field x1 number  inCanvasCoords
---@field y1 number
---@field x2 number of selection
---@field y2 number
---@field xoff number  x offset relative to the cursor
---@field yoff number
local Selection       = class.new_class(nil, 'Selection', {
})

local v2s, fmt        = tostring, string.format

-- the selection contains links to elements that are simply located on the canvas
Selection.TYPE_SELECT = 1

-- the selection contains elements that were cut from the canvas
Selection.TYPE_CUT    = 2

-- the selection contains copies of elements that are still on the canvas
Selection.TYPE_COPY   = 3


Selection.TSHORTNAMES = {
  [Selection.TYPE_SELECT] = 's',
  [Selection.TYPE_CUT] = 'x',
  [Selection.TYPE_COPY] = 'c',
}

--
-- constructor used in Selection:new()
--
---@param stype number
---@param items table?
---@param x1 number? selection boundaries
---@param y1 number?
---@param x2 number?
---@param y2 number?
---@param xoff number? offset from the cursor
---@param yoff number? offset from the cursor
function Selection:_init(stype, items, x1, y1, x2, y2, xoff, yoff)
  self.type = stype or self.items or Selection.TYPE_SELECT
  self.items = items or self.items or {}
  self.x1 = x1 or self.x1 or 0
  self.y1 = y1 or self.y1 or 0
  self.x2 = x2 or self.x2 or 0
  self.y2 = y2 or self.y2 or 0
  self.xoff = xoff or self.xoff or 0
  self.yoff = yoff or self.yoff or 0
end

---@return env.draw.ui.Selection
function Selection.cast(obj)
  if type(obj) and getmetatable(obj) == Selection then
    return obj
  else
    error('expected instance of Selection got: ' ..
      tostring(class.name(obj) or type(obj)))
  end
end

---@return boolean
function Selection:isSelect()
  return self.type == Selection.TYPE_SELECT
end

---@return boolean
function Selection:isCopy()
  return self.type == Selection.TYPE_COPY
end

---@return boolean
function Selection:isCut()
  return self.type == Selection.TYPE_CUT
end

-- ?
--
---@return env.draw.IEditable item
---@return number xoff
---@return number yoff
function Selection:unpack()
  return self.items, self.xoff, self.yoff
end

---@param element env.draw.IEditable
---@param old_x number?
---@param old_y number?
function Selection:add(element, old_x, old_y)
  if old_y and old_y then
    element:move(old_x - self.x1, old_y - self.y1)
  end
  self.items[#self.items + 1] = element
end

-- calculate Selection Range by items get_coords
---@return self
function Selection:calculateRangeByItems()
  self.x1, self.y1, self.x2, self.y2 = Selection.get_range_coords(self.items)
  return self
end

--
---@return self
function Selection:mkRelativeItemsCoords()
  local x1, y1 = self.x1, self.y1
  for _, elm in ipairs(self.items) do
    elm:move(-x1, -y1)
  end
  return self
end

--
-- elements stored by this selection
--
---@return table
function Selection:elements()
  return self.items
end

---@return self
---@param xoff number?
---@param yoff number?
function Selection:setOffset(xoff, yoff)
  xoff = xoff or 0
  yoff = yoff or 0
  self.xoff, self.yoff = xoff, yoff
  return self
end

---@return string
function Selection:__tostring()
  local sel_type = Selection.TSHORTNAMES[self.type or false] or '?'

  -- local cn = class.name(self.items) or type(self.items)

  local items_info = '' -- items info
  if self.items then
    for _, elm in ipairs(self.items) do
      ---@cast elm env.draw.IEditable
      local x, y = elm:pos()
      if #items_info > 0 then items_info = items_info .. ' ' end
      items_info = items_info .. fmt('%s(%s:%s)', elm:short_info(), x, y)
    end
  end

  local x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
  return fmt('%s [%s:%s %s:%s][%s:%s] {%s}',
    sel_type, v2s(x1), v2s(y1), v2s(x2), v2s(y2),
    v2s(self.xoff), v2s(self.yoff), -- of cursor pos
    items_info
  )
end

---@return number x1
---@return number y1
function Selection:pos()
  return self.x1 or 0, self.y1 or 0
end

---@return number xoff
---@return number yoff
function Selection:offsets()
  return self.xoff or 0, self.yoff or 0
end

--------------------------------------------------------------------------------
--                               Utils


--
-- find min and max position of given elements in the canvas
--
---@param elements table{env.draw.IEditable}
---@return number, number, number, number  -- x1 y1 x2 y2
function Selection.get_range_coords(elements)
  local min_x, min_y, max_x, max_y
  for _, e in pairs(elements) do
    ---@cast e env.draw.IEditable
    local x1, y1, x2, y2 = e:get_coords()

    if not min_x or x1 < min_x then min_x = x1 end
    if not min_y or y1 < min_y then min_y = y1 end

    if not max_x or x2 > max_x then max_x = x2 end
    if not max_y or y2 > max_y then max_y = y2 end
  end

  return min_x, min_y, max_x, max_y
end

--------------------------------------------------------------------------------


class.build(Selection)
return Selection

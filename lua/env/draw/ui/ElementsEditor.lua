-- 09-04-2024 @author Swarg
--

-- local log = require('alogger')
local class = require 'oop.class'
local utbl = require("env.draw.utils.tbl")
local ubuf = require("env.draw.utils.buf")
local ubase = require("env.draw.utils.base")
local Canvas = require('env.draw.Canvas')
local state = require('env.draw.state.canvas')
local IEditable = require('env.draw.IEditable')
-- local IDrawable = require('env.draw.IDrawable')
local StatefulBuf = require("env.draw.ui.StatefulBuf")
-- local inspect = require "inspect"

-- local Point = require 'env.draw.Point'
-- local Line = require 'env.draw.Line'
-- local Text = require 'env.draw.Text'
-- local Rectangle = require 'env.draw.Rectangle'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local M = {}

local get_buf_var = ubuf.get_buf_var
local get_ui_item_with_validate = state.get_ui_item_with_validate
-- local tbl_key_of_value = base.tbl_find_key_of_value
-- local tbl_keys_count = base.tbl_keys_count

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id
local invert_map = SH.invert_map

--
-- serialize given elements into code(text) for human editing
--
---@param elements table{env.draw.IEditable}
---@param indexes table{number}
---@return table{string} lines
---@return table{data={letter2idx:table, lnum2letter:table}} mappings
function M.elements_to_code(elements, indexes)
  local lines, letter2idx, lnum2letter = {}, {}, {}

  local opts = { force_oneliner = true, newline = '', indent = '' }

  -- single
  if #elements == 1 then
    local elm = elements[1]
    opts.force_oneliner = false
    local cname2id = new_cname2id()
    local t = elm:serialize({ cname2id = cname2id })

    opts.id2cname = invert_map(cname2id)
    local code = elm:fmtSerialized(t, 0, opts)

    lines[#lines + 1] = 'classes = '
    ubase.split(ubase.slist2str(opts.id2cname, "  "), "\n", lines)
    lines[#lines] = lines[#lines] .. ','

    lines[#lines + 1] = 'element = '
    ubase.split(code, "\n", lines)

    return lines, { single = true, index = indexes[1] }
  end

  -- multiple elements in oneline order to swap render order
  for i = #elements, 1, -1 do
    local elm = elements[i] ---@cast elm env.draw.IEditable
    local t = elm:serialize(nil)
    local index = (indexes or E)[i] -- element index in the canvas
    local lnum = #elements - i + 1
    local letter = ubase.n2letters(lnum)
    -- local line = letter .. ': ' .. inspect(t, opts):gsub("\n", "\\n")
    local line = letter .. ': ' .. elm:fmtSerialized(t, 0, opts)
    lines[#lines + 1] = line
    letter2idx[letter] = index
    lnum2letter[lnum] = letter -- used to change order of lements
  end

  local mappings = {
    letter2idx = letter2idx,
    lnum2letter = lnum2letter,
  }
  return lines, mappings
end

---@param box env.draw.ui.ItemPlaceHolder
---@param box_idx number
---@param elements table{env.draw.IEditable}
---@param indexes table{number}
function M.edit_elements_as_code(box, box_idx, elements, indexes)
  local lines, mappings = M.elements_to_code(elements, indexes)

  assert(type(lines) == 'table', 'lines')
  assert(type(mappings) == 'table', 'mappings')
  assert(type(box_idx) == 'number' and box_idx > 0, 'box_idx')

  local vars = {
    data = {
      mappings = mappings,
      canvas_addr = {
        bufnr = box.bufnr,
        -- validation to make sure this "coords" definitely contain this wrapper
        idx = box_idx,
        elements = #((box.item or E).objects),
      }
    }
  }


  local dispatcher = {
    on_write = M.cb_apply_edit_elements_changes
  }
  StatefulBuf.create_buf('edit elements', lines, dispatcher, vars)
end

--
-- callback for StatefulBuf
-- its running on BufWriteCmd registered for new buffer
--
-- Task:
--   - parse user input from the nvim buffer - create "patches" and
--   - apply changes to canvas elements(created patches)
--   - close "fly" buffer on success.
--     or leave it life if there are errors in the user input.
--
-- Allow:
--   - edit elements as code,
--   - swap elements to change its order in canvas list (affects to render)
--     (elements with a higher index overlap elements with a lower index)
--
-- t.buf, t.event = "BufWriteCmd", t.file, t.id, t.match(full filename)
--
---@param t table{buf, event, file, id, match}
function M.cb_apply_edit_elements_changes(t)
  local bufnr = t.buf
  local lines = vim.api.nvim_buf_get_lines(bufnr, 0, -1, false)
  if not lines or #lines < 1 then
    return print('empty lines bufnr:', bufnr)
  end
  local ok, data = get_buf_var(bufnr, 'data')
  if not ok then
    return print(data) -- errmsg
  end

  -- variables passed via nvim buffer nvim_buf_set_var
  assert(type(data) == 'table', 'data')
  assert(type(data.mappings) == 'table', 'data.mappings')
  assert(type(data.canvas_addr) == 'table', 'data.canvas_addr')

  local box_bufnr = assert(data.canvas_addr.bufnr, 'bufnr with canvas')
  local box_idx = assert(data.canvas_addr.idx, 'box index')

  local box = get_ui_item_with_validate(box_bufnr, box_idx)
  local canvas = Canvas.cast(box.item)


  local ok_patch, patch

  if (data.mappings or E).single then
    -- TODO: format one container into multiple lines to make editing easier.
    ok_patch, patch = M.build_edit_element_patch(lines, data.mappings)
  else
    ok_patch, patch = M.build_edit_elements_patch(lines, data.mappings)
  end

  if not ok_patch or type(patch) ~= 'table' then
    return print(patch) -- err msg
  end

  -- apply
  local success, report = canvas:apply_patch(patch)

  if success and type(report) == 'table' then
    local up, swpd, deleted = report.updated, report.swapped, report.deleted

    print(fmt('changes applied. updated:%s(parts:%s) swapped:%s deleted:%s',
      v2s(up), v2s(report.chparts), v2s(swpd), v2s(deleted)))

    StatefulBuf.close(bufnr, box_bufnr)

    if up > 0 or swpd > 0 or deleted > 0 then box:draw() end
  else
    print('fail', vim.inspect(report))
  end
end

---@param lines table{string}
---@param mappings table{letter2idx:table, lnum2letter:table}
---@return boolean
---@return table|string
function M.build_edit_elements_patch(lines, mappings)
  assert(type(mappings) == 'table', 'data.mappings')
  local letter2idx = assert(mappings.letter2idx, 'letter2idx')
  local lnum2letter = assert(mappings.lnum2letter, 'lnum2letter')

  local last = #lines
  while last > 0 and lines[last] == '' do
    last = last - 1
  end

  local patch, used_idxs = {}, {}

  -- create patches
  for lnum = 1, last, 1 do
    local letter, line = string.match(lines[lnum], '^(%w+): (.*)$')
    local old_index = letter2idx[letter or false] -- of elm in canvas

    local okc, t0 = utbl.luacode2table(line, lnum)
    if not okc then
      return false, v2s(t0) .. ' at ' .. lnum .. ':' .. v2s(lines[lnum])
    end

    local ok_des, elm = pcall(IEditable.deserialize, t0)
    if not ok_des or type(elm) ~= 'table' then
      return false, ('Cannot deserialize element from lnum: ' .. lnum ..
        ' error:' .. tostring(elm))
    end

    local new_index = old_index
    if lnum2letter[lnum] ~= letter then -- order is changed
      -- what letter was on this line
      local prev_letter = lnum2letter[lnum]
      new_index = letter2idx[prev_letter]
      if not new_index then
        return false, 'not found new index for:' .. letter .. 'at lnum:' .. lnum
      end
    end

    patch[#patch + 1] = {
      elm_modt = elm:serialize(), --  mk_patch(elm, elm:serialize()),
      old_idx = old_index,
      new_idx = assert(new_index, 'sure has index')
    }
    if used_idxs[old_index] ~= nil then
      return false, 'attempt to reuse a busy index at lnum: ' .. v2s(lnum)
    end
    used_idxs[old_index] = true
  end

  -- treat elements for which there is no patch as deletion
  for _, idx in pairs(letter2idx) do
    if not used_idxs[idx] then
      patch[#patch + 1] = {
        old_idx = idx,
        new_idx = idx, -- ?
        delete = true,
      }
    end
  end

  return true, patch
end

---@param lines table
function M.build_edit_element_patch(lines, mappings)
  local index = assert((mappings or E).index, 'has index of element')

  local code = '{' .. table.concat(lines, ' ') .. '}'
  local okc, t = utbl.luacode2table(code, 'all')
  if not okc or type(t) ~= 'table' then
    return false, v2s(t)
  end

  local id2cname = assert(t.classes, 'classes')
  local elm = assert(t.element, 'element code')

  local repl = IEditable.deserialize(elm, id2cname)
  local serialized = repl:serialize()
  -- serialized.as_code = true
  local patch = { [1] = { elm_modt = serialized, old_idx = index, new_idx = index } }

  return true, patch
end

return M

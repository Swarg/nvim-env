-- 01-04-2024 @author Swarg
-- Canvas Editor
-- Features and Goals:
--  - window with keybinding to edit env.draw.Canvas
--  - ability to select, copy, cut, delete and paste multiple canvas elements
--  - clipboard history where one entry can contains multiple elements
--  - deleted selections history to restore deleted elements from the canvas
--  - edit one or multiple selected elements properties via cli.
--  - ability to change the drawing order of specific elements in the selection
--    (via cli edit)
--  - quick jumps to canvas elements
--    - set v-block and select all in range of current object(rectangle)
--  - subwindow for more convenient editing of the selected elements properties
--    and ites render ordering more advanced add-on for manyaly cli-edit
--
-- TODO:
--  - new Elements:
--    - Container with the ability to combine several objects into one whole
--    - Arrow
--    - Glue(Join or Coupling) of two object. To automatically move the second
--      object when the coordinates of the first change
--
--  - undo with
--    - undo paste selected lements back to clipboard from canvas
--
--  - subwindow with all objects in the current canvas with features:
--    - select object by index for copy, cut, delete, move, or edit
--
--  - subwindow with clibloard and deleted_from_cb(undo) history with features:
--    - delete, pick, move in history list, move back from deleted_from_cb
--

local log = require('alogger')
local u8 = require("env.util.utf8")
local class = require 'oop.class'
local state = require('env.draw.state.canvas')
local conf = require('env.draw.settings')
local utbl = require("env.draw.utils.tbl")
local ubuf = require("env.draw.utils.buf")
-- local ugeo = require("env.draw.utils.geometry")
local Canvas = require('env.draw.Canvas')
local Selection = require("env.draw.ui.Selection");
local IPH = require("env.draw.ui.ItemPlaceHolder")
local IEditable = require('env.draw.IEditable')
local IDrawable = require('env.draw.IDrawable')
local ElementsEditor = require("env.draw.ui.ElementsEditor")

local Point = require 'env.draw.Point'
local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Rectangle = require 'env.draw.Rectangle'
---@diagnostic disable-next-line: unused-local
local Container = require 'env.draw.Container'
local usymbols = require 'env.draw.utils.unicode_symbols'
local builtin_mod_elm = require 'env.draw.command.builtin_mod_elm'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_trace, get_class = log.trace, class.get_class
local instanceof = class.instanceof
local log_debug = log.debug
local log_is_debug = log.is_debug
local indexof = utbl.tbl_indexof
local to_indexes = utbl.to_indexes
local get_actual_bufnr = ubuf.get_actual_bufnr
local set_cursor_pos = ubuf.set_cursor_pos
local get_buf_var = ubuf.get_buf_var
local set_buf_var = ubuf.set_buf_var
local ask_value = ubuf.ask_value
local ask_confirm_and_do = ubuf.ask_confirm_and_do
local get_buf_with_name = ubuf.get_buf_with_name

local close_visual_mode = ubuf.close_visual_mode
local open_visual_mode = ubuf.open_visual_mode
local nvim_move_cursor = ubuf.nvim_move_cursor
local is_visual_mode = ubuf.is_visual_mode
local get_editor_coords = ubuf.get_editor_coords
local nvim_find_syntax_block_range = ubuf.nvim_find_syntax_block_range
local get_lineends_command = IEditable.cli().UTILS.get_lineends_command
local edit_transaction = IEditable.cli().edit_transaction
local cli_parse_line = IEditable.cli().cli_parse_line
local get_line_n_command = IEditable.cli().UTILS.get_line_n_command

-- state
local get_ctx = state.get_ctx
local save_canvas = state.save_canvas
local load_canvas = state.load_canvas
local add_ui_item = state.add_ui_item
local get_ui_item_index = state.get_ui_item_index
local update_ui_item = state.update_ui_item
local remove_ui_item = state.remove_ui_item
-- local get_local_canvas_list = state.get_local_canvas_list
local get_canvas_box = state.get_canvas_box
local clipboard_is_empty = state.clipboard_is_empty
local clipboard_add_entry = state.clipboard_add_entry
local clipboard_delete_entry = state.clipboard_delete_entry
local clipboard_get_entry = state.clipboard_get_entry

local deleted_hist_add_entry = state.deleted_hist_add_entry
local setLastSelection = state.setLastSelection

-- local smallest_first = IEditable.smallest_first
-- local tbl_key_of_value = base.tbl_find_key_of_value
-- local tbl_keys_count = base.tbl_keys_count

-- geomentry
local create_combined_line = Container.create_combined_line

local M = {}

-- local SYNTAX_BLOCK_NAME = conf.SYNTAX_BLOCK_NAME
local SYNTAX_BLOCK_START = conf.SYNTAX_BLOCK_START
local SYNTAX_BLOCK_END = conf.SYNTAX_BLOCK_END

local SELECT_LIMIT = conf.SELECT_LIMIT

local DEF_RECTANGLE = conf.DEF_RECTANGLE
local DEF_LINE = conf.DEF_LINE

M.BUF_FILETYPE = 'xcanvas_editor'
M.VN_ELM_MOD_MODE = 'elm_modification_mode'
M.VN_DISPLAY_INDEXES = 'dispaly_indexes'
M.ELM_MODIF_MODES = { 'drag', 'slice', 'join' }

local help_page_lines = {
  '========        Canvas-Editor    (Help)     ========',
  "",
  "  g?            show/hide the help_page (g, shirf+/)",
  "  q             close",
  "",
  "",
  "  y             copy    the element or elements in the selection",
  "  p             paste   element(s)",
  "  x             cut     element(s)",
  "  D             delete  a last copied selection from the clipboard",
  "  c             cut off part of an element",
  "",
  "  \\             console: run builtin commands",
  "  I             inspect the element(s) under the cursor",
  "  i             insert new element",
  "  o or r        edit element(s)",
  "  O or R        edit element(s) as code in new buffer (apply changes on save)",
  "  gi            toggle disply indexes mode",
  "  gb            place an element in the background for all elements in it range",
  "",
  "                ---- Movements ----",
  "  v,[be1234]       select the whole element and go to corresponding vertex",
  "",
  "  Alt+[hjkl]       go to the corresponding vertex of the element",
  "  Ctrl+Alt+[hjkl]  go to the center of edge of corresponding vertex",
  "  Ctrl+[hjkl]      apply element modifiacation based on mod-mode (move,resize,..)",
  "  m                toggle current modifiacation mode: "
  .. table.concat(M.ELM_MODIF_MODES, ' '),
  "",
  "                ---- Save/Load ----",
  "  Ctrl+s        save the canvas",
  "  Ctrl+S        save the canvas as (Ctrl+Shift+S)",
  "  F4            reload canvas",
  "",
  "  Q             close Editor",
  "",
}
local utf8_icons_samples = [[
⚙   
                                
]]
local utf8_icons = nil -- build on first access

--------------------------------------------------------------------------------
function M.draw_help(bufnr)
  bufnr = bufnr or 0
  if not M.is_valid_buf(bufnr) then
    return
  end
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, help_page_lines)
end

--
-- create keybindings map for main CanvasEditor window(buffer)
--
-- action = keys
---@param opts table?
function M.mkKeybindings(opts)
  local map = {
    [M.do_toggle_help_page] = {
      n = { 'g?' },
      desc = 'show/hide the help_page (g, shirf+/)'
    },
    [M.do_close] = {
      nv = { 'q' },
      desc = 'close'
    },
    [M.do_close_and_clean_buf] = {
      n = { 'Q' },
      desc = 'close CanvasEditor'
    },
    [M.do_open_console] = {
      nv = { '\\' },
    },
    [M.do_new_object] = {
      nv = { 'i', ' i' },
    },
    [M.do_new_text] = {
      nv = { 't', },
    },
    [M.do_mark_pos_start] = {
      nv = { 'a' },
    },
    [M.do_mark_pos_middle] = {
      nv = { 'A' },
    },
    [M.do_toggle_display_indexes] = {
      nv = { 'gi' },
      desc = 'draw indexes at the top left position of each canvas element'
    },
    [M.do_make_element_as_background] = {
      nv = { 'gb' },
      desc = 'set current element as backgroup for all in it range',
    },
    [M.do_inspect] = {
      nv = { 'I' },
      desc = 'inspect element under the cursor or selected'
    },
    [M.do_edit] = {
      nv = { 'o', 'r' },
      desc = 'edit selected element(s) or under the cursor'
    },
    [M.do_edit_as_code] = {
      nv = { 'O', 'R' },
      desc = 'edit selected element(s) as code in new buffer'
    },
    [M.do_inc_number_in_text] = {
      n = { '<c-a>' },
      desc = 'increment number in the current Text element'
    },
    [M.do_dec_number_in_text] = {
      n = { '<c-x>' },
      desc = 'decrement number in the current Text element'
    },
    [M.do_copy] = {
      nv = { 'y' },
      desc = 'copy selected element(s)'
    },
    [M.do_cut] = {
      nv = { 'x', 'X' },
      desc = 'cut selected element(s)'
    },
    [M.do_cutoff_element] = {
      nv = { 'c' },
      desc = 'cut off the part of an element'
    },
    [M.do_paste] = {
      nv = { 'p', 'P' },
      desc = 'cut selected element(s)'
    },
    [M.do_delete_last_from_cb] = {
      nv = { 'D' },
      desc = 'delete last entry from clipboard'
    },
    [M.do_undo] = {
      nv = { 'u' },
    },
    [M.do_save_canvas] = {
      nv = { '<c-s>' },
    },
    [M.do_save_canvas_as] = {
      nv = { '<c-a-s>' },
    },
    [M.do_load_canvas] = {
      nv = { '<F4>' },
    },
    [M.do_select_whole_element_tl] = { v = { 'b', }, },
    [M.do_select_whole_element_br] = { v = { 'e', }, },
    -- [M.do_select_whole_element_tr] = { v = { '2' }, },
    -- [M.do_select_whole_element_bl] = { v = { '4' }, },
    [M.do_go_to_vertex_h] = { n = { '<a-h>' }, },
    [M.do_go_to_vertex_j] = { n = { '<a-j>' }, },
    [M.do_go_to_vertex_k] = { n = { '<a-k>' }, },
    [M.do_go_to_vertex_l] = { n = { '<a-l>' }, },
    [M.do_go_to_vertex_hc] = { n = { '<c-a-h>' }, }, -- ctrl+alt+h
    [M.do_go_to_vertex_jc] = { n = { '<c-a-j>' }, },
    [M.do_go_to_vertex_kc] = { n = { '<c-a-k>' }, },
    [M.do_go_to_vertex_lc] = { n = { '<c-a-l>' }, },
    [M.do_toggle_elm_modification_mode] = { nv = { 'm' } }, -- ctrl+m ?
    [M.do_mod_element_h] = { nv = { '<c-h>' } },
    [M.do_mod_element_j] = { nv = { '<c-j>' } },
    [M.do_mod_element_k] = { nv = { '<c-k>' } },
    [M.do_mod_element_l] = { nv = { '<c-l>' } },
  }
  local f2n, n2f = M.buildActionNameMappings()
  --
  -- do_nothing
  local ignored_keys = {
    'C', 'd', 's', 'S', 'P', 'U', 'J'
  }
  -- modify by config
  if opts then
    -- todo modify map by opts with n2f
    opts = opts
  end
  --
  return { active = map, ignored = ignored_keys, fn2name = f2n, name2fn = n2f }
end

function M.buildActionNameMappings()
  local func2name = {
    [M.do_toggle_help_page] = 'do_toggle_help_page',
    [M.do_close] = 'do_close',
    [M.do_close_and_clean_buf] = 'do_close_and_clean_buf',
    [M.do_new_object] = 'do_new_object',
    [M.do_inspect] = 'do_inspect',
    [M.do_edit] = 'do_edit',
    [M.do_copy] = 'do_copy',
    [M.do_cut] = 'do_cut',
    [M.do_paste] = 'do_paste',
    [M.do_delete_last_from_cb] = 'do_delete_last_from_cb',
    [M.do_undo] = 'do_undo',
    [M.do_save_canvas] = 'do_save_canvas',
    [M.do_save_canvas_as] = 'do_save_canvas_as',
    [M.do_load_canvas] = 'do_load_canvas',
    [M.do_select_whole_element_tl] = 'do_select_element_range_tl',
    [M.do_select_whole_element_br] = 'do_select_element_range_br',
    [M.do_select_whole_element_bl] = 'do_select_element_range_bl',
    [M.do_select_whole_element_tr] = 'do_select_element_range_tr',
    [M.do_go_to_vertex_h] = 'do_go_to_vertex_h',
    [M.do_go_to_vertex_j] = 'do_go_to_vertex_j',
    [M.do_go_to_vertex_k] = 'do_go_to_vertex_k',
    [M.do_go_to_vertex_l] = 'do_go_to_vertex_l',
    [M.do_go_to_vertex_hc] = 'do_go_to_vertex_hc',
    [M.do_go_to_vertex_jc] = 'do_go_to_vertex_jc',
    [M.do_go_to_vertex_kc] = 'do_go_to_vertex_kc',
    [M.do_go_to_vertex_lc] = 'do_go_to_vertex_lc',
    [M.do_toggle_elm_modification_mode] = 'do_toggle_elm_modification_mode',
    [M.do_mod_element_h] = 'do_mod_element_h',
    [M.do_mod_element_j] = 'do_mod_element_j',
    [M.do_mod_element_k] = 'do_mod_element_k',
    [M.do_mod_element_l] = 'do_mod_element_l',
    [M.do_drag_element_h] = 'do_drag_element_h',
    [M.do_drag_element_j] = 'do_drag_element_j',
    [M.do_drag_element_k] = 'do_drag_element_k',
    [M.do_drag_element_l] = 'do_drag_element_l',
    [M.do_mark_pos_start] = 'do_mark_pos_start',
    [M.do_mark_pos_middle] = 'do_mark_pos_middle',
  }
  local name2func = {
    do_toggle_help_page = M.do_toggle_help_page,
    do_close = M.do_close,
    do_close_and_clean_buf = M.do_close_and_clean_buf,
    do_new_object = M.do_new_object,
    do_inspect = M.do_inspect,
    do_edit = M.do_edit,
    do_copy = M.do_copy,
    do_cut = M.do_cut,
    do_paste = M.do_paste,
    do_delete_last_from_cb = M.do_delete_last_from_cb,
    do_undo = M.do_undo,
    do_save_canvas = M.do_save_canvas,
    do_save_canvas_as = M.do_save_canvas_as,
    do_load_canvas = M.do_load_canvas,
    do_select_element_range_tl = M.do_select_whole_element_tl,
    do_select_element_range_tr = M.do_select_whole_element_tr,
    do_select_element_range_br = M.do_select_whole_element_br,
    do_select_element_range_bl = M.do_select_whole_element_bl,
    do_go_to_vertex_h = M.do_go_to_vertex_h,
    do_go_to_vertex_j = M.do_go_to_vertex_j,
    do_go_to_vertex_k = M.do_go_to_vertex_k,
    do_go_to_vertex_l = M.do_go_to_vertex_l,
    do_go_to_vertex_hc = M.do_go_to_vertex_hc,
    do_go_to_vertex_jc = M.do_go_to_vertex_jc,
    do_go_to_vertex_kc = M.do_go_to_vertex_kc,
    do_go_to_vertex_lc = M.do_go_to_vertex_lc,
    do_toggle_elm_modification_mode = M.do_toggle_elm_modification_mode,
    do_mod_element_h = M.do_mod_element_h,
    do_mod_element_j = M.do_mod_element_j,
    do_mod_element_k = M.do_mod_element_k,
    do_mod_element_l = M.do_mod_element_l,
    do_drag_element_h = M.do_drag_element_h,
    do_drag_element_j = M.do_drag_element_j,
    do_drag_element_k = M.do_drag_element_k,
    do_drag_element_l = M.do_drag_element_l,
    do_mark_pos_start = M.do_mark_pos_start,
    do_mark_pos_middle = M.do_mark_pos_middle,
  }
  return func2name, name2func
end

M.SURE_CLOSE_EDITOR = 'Are you sure want to close the CanvasEditor?'

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- feature: access to element in the canvas via command
--
-- UseCase: it is impossible to get to the element in the usual way:
-- useful when an element is either not visible on the canvas or goes beyond
-- the boundaries of the canvas



-- cut element from canvas by its in-list index
---@param box env.draw.ui.ItemPlaceHolder
---@param idx number|table
---@return boolean
---@return string|env.draw.ui.Selection?
function M.cut_element_by_idx(box, idx)
  local indexes = to_indexes(idx)
  local elements = Canvas.cast(box.item):get_elements_by_indexes(indexes)

  if #elements > 0 then
    local x, y, _, _ = Selection.get_range_coords(elements)
    local lnum, col = box:toOuterLnumCol(x, y)
    local n, selection = M.cut_from_canvas(box, elements, lnum, col)

    if n > 0 and selection then
      assert(selection.type == Selection.TYPE_CUT, 'as cut')
      return true, selection
    end

    return false, 'internal error on cut_from_canvas n:' .. tostring(n)
  end

  return false, 'not found element with index ' .. tostring(idx)
end

--
-- delete element from canvas by index
-- put deleted element to deleted_history list (for possible undo)
--
---@param box env.draw.ui.ItemPlaceHolder
---@param idx number|table one or many indexes to be deleted as one selection
---@param no_undo boolean? can be restored or not
---@return boolean success
---@return string|env.draw.ui.Selection|table<env.draw.IEditable>
function M.delete_element_by_idx(box, idx, confirm_callback, no_undo)
  local indexes = to_indexes(idx)
  local canvas = Canvas.cast(box.item)

  local elements = canvas:get_elements_by_indexes(indexes)

  if #elements > 0 then
    if type(confirm_callback) == 'function' then
      if not confirm_callback(elements) then
        return false, 'canceled'
      end
    end

    -- assert(elm == table.remove(elements, idx), 'same')
    local removed = canvas:remove_all(elements)
    if #removed ~= #elements then
      error('expected found all elements to delete got:' ..
        v2s(#removed) .. ' scheduled to delete: ' .. v2s(#elements))
    end
    box:changed()

    if not no_undo then
      -- with autocalculation of a selection boundaries
      local selection = Selection:new(nil, Selection.TYPE_CUT, elements)
          :calculateRangeByItems()
          :mkRelativeItemsCoords()

      deleted_hist_add_entry(selection)

      return true, selection
    end

    return true, elements
  end

  return false, 'not found element with index ' .. v2s(idx)
end

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------


---@param canvas env.draw.Canvas
---@param bufnr number?
---@param lnum number?
---@param col number?
---@param fn string?
---@return number index in the list
---@return env.draw.ui.ItemPlaceHolder
function M.add_canvas(canvas, bufnr, lnum, col, fn)
  bufnr = get_actual_bufnr(bufnr)
  return add_ui_item(canvas, bufnr, lnum, col, fn, M.draw_item)
end

--
-- find the position in the buffer where to insert the outline and insert it
-- used in one case: place in the nvim buffer(regular file)
--
-- Note: box.lnum starts from 0, nvim from 1
--
-- ```\n canvas \n```\n
---@param box env.draw.ui.ItemPlaceHolder
function M.prepare_and_place(box, can_init_new)
  log.debug("prepare_canvas_place (%s) can_init_new:%s", box, can_init_new)
  assert(type(box.bufnr) == 'number' and box.bufnr > 0, 'bufnr')
  assert(type(box.lnum) == 'number' and box.lnum > -1, 'bufnr')

  local bufnr, lnum = box.bufnr, box.lnum

  local ls, le = nvim_find_syntax_block_range(bufnr, lnum + 1)
  local st, msg = nil, nil

  if ls then -- inside already existed syntax block
    le = le or ls
    local lines = Canvas.cast(box.item):toLines()
    log.debug('insert canvas to ls:%s le:%s lines:%s', ls, le, #lines)
    vim.api.nvim_buf_set_lines(bufnr, ls, le - 1, true, lines)
    st, msg = true, nil
  else -- create new syntax block and place canvas
    if can_init_new then
      M.insert_canvas_to_buf(box, true, box.lnum)
      -- take into account the boundaries of the inserted syntax block
      box.lnum, box.lnum_end = box.lnum + 1, box.lnum_end + 1
      st, msg = true, nil
    else
      st, msg = false, 'Not Found place for canvas. Use block: ```xcanvas\\n```'
    end
  end

  if st then
    -- auto jump to the top-left of the canvas
    set_cursor_pos(0, box.lnum + 1, box.col)
  end

  log.debug('ret st:%s msg:%s', st, msg, '::BACKTRACE::')
  return st, msg
end

--
-- just place without redraw (Canvas.draw)
-- used in two cases: CanvasEditor and place in the nvim buffer(regular file)
--
---@param box env.draw.ui.ItemPlaceHolder?{item, bufnr, lnum}
-- function M.place_canvas(canvas, bufnr, lnum, lnum_end)
---param inblock boolean? wrap to syntax block
---@param le number?
function M.insert_canvas_to_buf(box, inblock, le)
  log.debug("insert_canvas_to_buf (%s) inblock:%s le:%s", box, inblock, le)
  box = IPH.cast(box)
  local lines = {}
  if inblock then lines[#lines + 1] = SYNTAX_BLOCK_START end
  Canvas.cast(box.item):toLines(lines) -- render background

  local bufnr = box.bufnr              -- already get_actual_bufnr(bufnr)
  local lnum = box.lnum
  local lnum_end = le or box.lnum      --

  local lines_cnt = vim.api.nvim_buf_line_count(bufnr)

  if lnum_end > lines_cnt then lnum_end = lines_cnt end

  if inblock then
    lines[#lines + 1] = SYNTAX_BLOCK_END
    -- lnum = math.max(0, lnum - 1)
    -- lnum_end = math.min(lines_cnt, lnum_end + 1)
  end

  vim.api.nvim_buf_set_lines(bufnr, lnum, lnum_end, true, lines)
end

---@param canvas env.draw.Canvas
---@param bufnr number?
---@param lnum number?
function M.draw_canvas(canvas, bufnr, lnum, lnum_end)
  bufnr = get_actual_bufnr(bufnr)
  lnum = lnum or 0
  lnum_end = lnum_end or (lnum + canvas.height) -- #lines)
  local lines = canvas:draw():toLines()
  local max = vim.api.nvim_buf_line_count(bufnr)
  if lnum_end <= max then
    vim.api.nvim_buf_set_lines(bufnr, lnum, lnum_end, true, lines)
  else
    error('expected lnum_end' .. v2s(lnum_end) .. ' has lines: ' .. v2s(max))
  end
end

---@param box env.draw.ui.ItemPlaceHolder
function M.draw_item(box)
  log_debug("draw_item", box)
  local canvas = Canvas.cast(box.item)
  local lines = canvas:draw():toLines()
  if vim then -- testing
    vim.api.nvim_buf_set_lines(box.bufnr, box.lnum, box.lnum_end, false, lines)
  end
end

---@param bufnr number
---@param lnum number (box offset from 0)
---@param lnum_end number
---@return boolean
function M.is_item_in_syntax_block(bufnr, lnum, lnum_end)
  local ls, le = lnum - 1, lnum_end + 1
  local max = vim.api.nvim_buf_line_count(bufnr)
  if le > max then le = max end
  local la = vim.api.nvim_buf_get_lines(bufnr, ls, le, false)

  log.debug("is_item_in_syntax_block", bufnr, lnum, lnum_end, la[1], la[#la])
  return la[1] == SYNTAX_BLOCK_START and la[#la] == SYNTAX_BLOCK_END
end

--
-- check for possible displacements of the canvas in the buffer, look for a new
-- position and ask for confirmation for drawing with new line numbers
--
-- UseCase: draw canvas in buffer with regualr text-document not a CanvasEditor
-- when the canvas is placed somewhere in the middle of the text document and
-- changes in its position are possible when editing the text buffer.
--
---@param ask_value_callback function
function M.draw_item_with_check(box, ask_value_callback)
  log_debug("draw_item_with_check", box)
  local canvas = Canvas.cast(box.item)
  local can_draw, msg = true, nil
  -- do not check for CanvasEditor
  if box.lnum > 0 then
    can_draw = M.is_item_in_syntax_block(box.bufnr, box.lnum, box.lnum_end)
    if not can_draw then
      local default
      msg = 'Buffer position offset detected. '
      local cls, cle = nvim_find_syntax_block_range(box.bufnr, box.lnum)

      if cls and cle then
        msg = msg .. 'Update position to found lnums?: '
        default = v2s(cls) .. ' ' .. v2s(cle)
      else
        msg = msg .. 'New position not found. Provide position manually, was:'
        default = v2s(box.lnum) .. ' ' .. v2s(box.lnum_end)
      end

      if type(ask_value_callback) == 'function' then
        local ret = ask_value_callback(msg, default)
        if type(ret) ~= 'string' then
          msg = 'canceled'
        else
          local nls, nle = string.match(ret, '%s*(%d+)%s+(%d+)%s*')
          nls, nle = tonumber(nls), tonumber(nle)
          if not nls or not nle then
            return false, fmt('two numbers expected got: "%s"', v2s(ret))
          end
          can_draw = M.is_item_in_syntax_block(box.bufnr, nls, nle - 1)
          if can_draw then
            box.lnum, box.lnum_end = nls, nle - 1
          else
            msg = fmt('Not Found Valid Syntax block for Canvas at %s: %s', nls, nle)
          end
        end
      else
        msg = 'no callback to ask confirmation'
      end
    end
  end

  if can_draw then
    local lines = canvas:draw():toLines()
    vim.api.nvim_buf_set_lines(box.bufnr, box.lnum, box.lnum_end, false, lines)
  end

  return can_draw, msg
end

--------------------------------------------------------------------------------


---@param elm env.draw.IEditable
---@param n number?  position in the list
---@param all number? list size of all elements to edit
---@param x number?
---@param y number?
---@return boolean updated
---@return string|table|nil err|patch
function M.edit_element(elm, n, all, box, x, y)
  log_debug("edit_element xy:(%s:%s) (%s/%s) %s ", x, y, n, all, elm)
  local parent
  -- support for editing child elements in a container, etc.
  if x and y then
    local child = elm:get_child_at(x, y)
    if child then
      parent = elm
      elm = child
    end
  end

  local updated, patch = edit_transaction(elm, {
    context = box,
    serialize_data = { cli = true, x = x, y = y, },
    ---@param ctx table
    ---@param obj table
    ---@param args table
    dispatcher = function(ctx, obj, args)
      -- code, ret
      return builtin_mod_elm.handle(args, { box = ctx, elm = obj, x = x, y = y })
    end,
    --
    ---@param prompt string
    ---@param line string
    ---@return string
    ui_read_line = function(prompt, line)
      local prefix = ''
      if n and type(all) == 'number' and all > 1 then
        prefix = '[element:' .. v2s(n) .. '/' .. v2s(all) .. '] '
      end
      prefix = prefix .. IEditable.mkChildPrefix(parent, elm, x, y)
      return ask_value(prefix .. prompt .. ': ', line)
    end,
  })

  log_debug('edit_element updated:%s %s', updated, patch)
  return updated, patch
end

--------------------------------------------------------------------------------
--                      Edit Canvas Element Order
--
-- Goal: make it possible to swap elements through pairwise replacement of
-- indices in the list.
-- This can be useful when you need to pull an element higher so that
-- when drawn it overlaps another

--
-- prepare message and default value to edit by user
--
---@param indexes table
---@param elements table
function M.cli_elements_order_prompt(elements, indexes)
  assert(type(elements) == 'table', 'elements')
  assert(type(indexes) == 'table', 'indexes')
  assert(#elements == #indexes and #elements > 1, 'expected synced lists')

  local msg, def = '', ''
  local Abyte = string.byte('A', 1, 1) - 1
  for i, elm in ipairs(elements) do
    ---@cast elm env.draw.IEditable
    local index = indexes[i]
    local letter = string.char(Abyte + i)
    msg = msg .. fmt(' %s:%s;', letter, elm:short_info(true))
    def = def .. fmt(' %s: %s', letter, tostring(index))
  end
  return msg, def
end

--
-- parse the user input with indexes validation
-- Prohibits out of range of the maximum and minimum index and also
-- using the same new index for two elements
--
-- the goal is to make it possible to change the order but not remove or
-- replace elements.
--
-- input_line ' A: 6 B: 5 C: 4 D: 3 E: 2 F: 1'
--
---@return boolean updated
---@return table|string? message
function M.cli_elements_order_parse_input(indexes, input_line)
  local raw_parsed = cli_parse_line(input_line)
  local t, i, min, max, unchanged = {}, 1, nil, 0, {}
  local Abyte = string.byte('A', 1, 1) - 1

  for _, element_idx in ipairs(indexes) do
    if element_idx > max then max = element_idx end
    if not min or element_idx < min then min = element_idx end
  end

  while i <= #raw_parsed do
    local letter = raw_parsed[i]:sub(1, 1)
    local n = string.byte(letter, 1, 1) - Abyte -- index of index
    -- A corresponds to 1 index
    local si = raw_parsed[i + 1]
    local element_idx = tonumber(si)
    if not element_idx then
      return false, fmt('expected number got: %s (%s:%s) ', v2s(si), letter, i)
    end

    local current_element_idx = indexes[n]
    if current_element_idx ~= element_idx then -- has changes
      t[#t + 1] = { old = current_element_idx, new = element_idx }
    else
      unchanged[current_element_idx] = true
    end
    i = i + 2
  end

  -- validate symantic
  if #t > 0 then
    local u = {}
    for _, patch in pairs(t) do
      local idx = patch.new
      if not idx then
        return false, 'no value of a new index'
      end
      if idx > max then
        return false, fmt('%s too big! valid range: %s - %s', idx, min, max)
      end
      if idx < min then
        return false, fmt('%s too small! valid range: %s - %s', idx, min, max)
      end
      if u[idx] then
        -- collision elements cannot placed to one index
        return false, fmt('index %s already used by %s', v2s(idx), v2s(u[idx]))
      end
      if unchanged[idx] then
        return false, fmt('index %s already in use', v2s(idx))
      end
      u[idx] = patch.old or true
    end
  end

  return #t > 0, t
end

---@param canvas env.draw.Canvas
---@param indexes table
---@param elements table
---@return boolean updated
---@return string? message
function M.do_element_order_edit(canvas, elements, indexes)
  local msg, def = M.cli_elements_order_prompt(elements, indexes)

  local input_line = ask_value('[RenderOrder]' .. msg .. ': ', def)
  local updated, ret = false, nil

  if input_line and input_line ~= 'q' and input_line ~= '' then
    local cmd = get_lineends_command(input_line)
    if cmd then return false, cmd end

    updated, ret = M.cli_elements_order_parse_input(indexes, input_line)
    if updated and type(ret) == 'table' then
      local patch_order = ret
      local swapped_pairs = canvas:swapElementsOrder(patch_order)
      updated = swapped_pairs > 0
      ret = v2s(swapped_pairs) .. ' pair of elements swapped'
    else
      return false, tostring(ret) -- errmsg
    end
  else
    updated, ret = false, 'canceled'
  end

  return updated, ret
end

--------------------------------------------------------------------------------
--                        Interact with Canvas
--------------------------------------------------------------------------------



--
-- insert selection with elements into canvas wrapped to box
-- when inserting, the offset relative to the cursor that
-- was when cutting elements is preserved.
--
---@param box env.draw.ui.ItemPlaceHolder{env.draw.ICanvas}
---@param lnum number absolute position of the cursot in the editor
---@param col number
---@param selection env.draw.ui.Selection
---@param mkcopy boolean?
---@return number -- of inserted elements
function M.add_to_canvas(box, lnum, col, selection, mkcopy)
  local cnt = 0

  if selection then
    local cur_x, cur_y = box:toInnerXY(lnum, col)
    local canvas = Canvas.cast(box.item)

    local xoff, yoff = selection:offsets()
    local elm_xoff, elm_yoff = cur_x - xoff, cur_y - yoff

    -- log_debug('add_to_canvas cursor:(%s:%s) selection_offset:(%s:%s)',
    --   cur_x, cur_y, xoff, yoff)

    for _, elm in ipairs(selection:elements()) do
      if not instanceof(elm, IEditable) then
        error('expected IEditable object got:' .. v2s(class.name(elm)))
      end
      -- ? past inside another objects? or  weld to another? TODO

      if mkcopy then elm = elm:copy() end

      elm:move(elm_xoff, elm_yoff)
      canvas:add(elm)

      cnt = cnt + 1
    end
  end

  return cnt
end

--
-- either one element or a group of selected elements can be copied
--
---@param box env.draw.ui.ItemPlaceHolder{env.draw.Canvas}
---@param objects table?{env.draw.IEditable}
---@param lnum number
---@param col number
---@return number count of copied objects
---@return env.draw.ui.Selection?
function M.copy_from_canvas(box, objects, lnum, col, lnum2, col2)
  assert(instanceof(box, IPH), 'box')
  if not objects or not next(objects) then
    return 0, nil
  end
  lnum2, col2 = lnum2, col2
  --
  local cursor_x, cursor_y = box:toInnerXY(lnum or 0, col or 0)
  local x1, y1, x2, y2 = Selection.get_range_coords(objects)

  local selection = Selection:new(nil,
    Selection.TYPE_COPY,
    {},                          -- list for elements
    x1, y1, x2, y2,              -- coords of selection itself
    cursor_x - x1, cursor_y - y1 -- xoff yoff
  )

  for _, obj in ipairs(objects) do
    local elmc = obj:copy()
    local old_x, old_y = IPH.resetCoords(elmc)
    selection:add(elmc, old_x, old_y)
  end

  clipboard_add_entry(selection)

  return #selection:elements(), selection
end

--
-- remove object from Canvas
--
---@param box env.draw.ui.ItemPlaceHolder{env.draw.Canvas}
---@param objects table?{env.draw.IEditable}
---@param lnum number
---@param col number
---@return number the number of cut elements in selection(objects)
---@return env.draw.ui.Selection? the selection with cut out elements
---@diagnostic disable-next-line: unused-local
function M.cut_from_canvas(box, objects, lnum, col, lnum2, col2)
  assert(instanceof(box, IPH), 'box')

  if not objects or not next(objects) then
    return 0, nil
  end

  local cursor_x, cursor_y = box:toInnerXY(lnum or 0, col or 0)
  local x1, y1, x2, y2 = Selection.get_range_coords(objects)

  local selection = Selection:new(nil,
    Selection.TYPE_CUT,
    {},                          -- list for elements
    x1, y1, x2, y2,              -- coords of selection itself
    cursor_x - x1, cursor_y - y1 -- xoff yoff
  )
  if log_is_debug then
    log_debug(
      "cut_from_canvas lnum:col(%s:%s-%s:%s) cursorXY(%s:%s) selection: %s",
      lnum, col, lnum2, col2, cursor_x, cursor_y, selection
    )
  end

  for i, elm0 in ipairs(objects) do
    local elm, old_x, old_y = box:remove(elm0) -- make elm.x1,elm.y2 = 0,0
    if not elm then
      -- not removed because not found in the canvas
      error('Cannot found element: ' .. tostring(i) .. ' ' .. tostring(elm0))
    end

    selection:add(elm, old_x, old_y)
  end

  clipboard_add_entry(selection)

  M.draw_item(box) -- redraw Canvas

  return #selection:elements(), selection
end

---@diagnostic disable-next-line: unused-local
---@param box env.draw.ui.ItemPlaceHolder{env.draw.Canvas}
---@param objects table?{env.draw.IEditable}
---@param lnum number
---@param col number
---@return number the number of cut elements in selection(objects)
---@return env.draw.ui.Selection? the selection with cut out elements
function M.cutoff_element(box, objects, lnum, col, lnum2, col2)
  local elm = (objects or E)[1]
  if not elm then return 0, nil end

  local x1, y1 = box:toInnerXY(lnum or 0, col or 0)
  local x2, y2 = box:toInnerXY(lnum2 or 0, col2 or 0)

  x1, y1, x2, y2 = IEditable.normalize(x1, y1, x2, y2)

  if get_class(elm) == Container then
    return M.cutoff_container(box, elm, x1, x2, y1, y2)
  end

  local has, parts = elm:cutoff(x1, y1, x2, y2)
  local cnt = 0
  if has then -- parts and next(parts) then
    local canvas = Canvas.cast(box.item)

    -- place new elements(parts) in the canvas
    for _, epart in ipairs(parts) do
      canvas:add(epart)
      cnt = cnt + 1
    end

    -- place the cut element into undo to rollback
    local elm0, old_x, old_y = box:remove(elm)
    if not elm0 then -- not removed because not found in the canvas
      error('Not Found element: ' .. tostring(elm0))
    end
    -- local elm0 = canvas:remove(elm)
    -- assert(elm0 == elm)

    deleted_hist_add_entry(
      Selection:new(nil,
        Selection.TYPE_COPY,        -- ?
        { elm0 },                   -- list for elements
        old_x, old_y, old_x, old_y, -- coords of selection itself
        0, 0                        -- xoff yoff
      )
    )

    box:changed()
    box:draw()
    -- if cnt > 0 then end
  end

  return cnt
end

--
-- extract elmenets from container, and delete continaer itself
--
-- todo: idea: split a large container into two parts
--
---@param box env.draw.ui.ItemPlaceHolder{env.draw.Canvas}
---@param c env.draw.Container
---@param x1 number
---@param y1 number
---@return number
---@diagnostic disable-next-line: unused-local
function M.cutoff_container(box, c, x1, y1, x2, y2)
  local opts = { vertex_only = true }
  local any_class = nil

  -- check is cursor not in the edge or vertex of the some elment from container
  -- the idea is that if the cursor is over the container but not over its
  -- element, then suggest pulling all the elements out into the canvas by
  -- deleting the container
  local elms = c:find_by_xy(any_class, x1, y1, opts)
  for i, elm in ipairs(elms) do
    print(i, elm)
  end

  if next(elms) == nil then
    if c:isEmpty() then
      print('empty')
    else
      local msg = fmt('Unpack elements from [%s] ', c) -- ?
      if ask_confirm_and_do(msg) then
        local moved = box.item:unpack_from(c, c.inv)
        print('container deleted moved elms:', moved)
        return moved
      end
    end
  end

  return 0
end

--
-- ask the user interactively
--
---@return boolean?
---@return table?{hl, vl, corners}
function M.ask_rect_style_interactively()
  local v
  v = ask_value('Choose a style[]: ', 'light')
  if v == '' or v == '\\' or v == '?' or v == 'help!' or v == 'ls' then
    v = nil
    local msg = usymbols.build_help_rect() .. "\nRectStyle: "
    v = ubuf.select_interactively(msg, 'light')
  end
  if not v or v == '' then return false, nil end

  local t = usymbols.pick_rect_style(v)

  if type(t) == 'table' then
    return true, t
  end

  return false, nil
end

--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
--                               Window
--------------------------------------------------------------------------------

---@return boolean
---@param bufnr number
function M.isCanvasEditor(bufnr)
  bufnr = bufnr or 0
  bufnr = get_actual_bufnr(bufnr)
  local _, value = pcall(vim.api.nvim_buf_get_option, bufnr, 'filetype')
  local b = value == M.BUF_FILETYPE
  log_debug('isCanvasEditor:%s (%s)', b, value)
  return b
end

--
-- create buffer with keybinding to edit Canvas
-- see help by g?
--
---@param ctx table{canvas}
---@param bname string|nil
function M.create_buf(ctx, bname)
  assert(type(ctx) == 'table', 'ctx')
  assert(type(ctx.canvas) == 'table', 'ctx.canvas')
  assert(instanceof(ctx.canvas, Canvas), 'expected canvas')
  assert(type(ctx.bufnr) == 'number', 'bufnr of canvas')
  assert(type(ctx.lnum) == 'number', 'lnum of canvas')
  -- ctx.col?

  local api = vim.api
  local reuse = false
  local win = api.nvim_get_current_win()
  bname = bname or 'CanvasEditor'
  -- reuse already exists buf
  if bname then
    local oldbufnr = get_buf_with_name(bname)
    if oldbufnr and oldbufnr ~= -1 then
      ctx.out_bufnr = oldbufnr -- reuse
      reuse = true
    end
  end

  if not ctx.bufnr or ctx.bufnr == 0 then
    ctx.bufnr = api.nvim_get_current_buf()
  end

  if not reuse then
    ctx.out_bufnr = api.nvim_create_buf(true, true)
  end

  api.nvim_win_set_buf(win, ctx.out_bufnr) -- mk foreground

  if not reuse then
    local bufnr = ctx.out_bufnr -- own

    pcall(api.nvim_buf_set_name, bufnr, bname)

    api.nvim_buf_set_option(bufnr, 'modifiable', true)  -- clear old
    api.nvim_buf_set_lines(bufnr, 0, -1, true, {})
    api.nvim_buf_set_option(bufnr, 'buftype', 'nofile') -- ?
    api.nvim_buf_set_option(bufnr, 'swapfile', false)
    api.nvim_buf_set_option(bufnr, 'filetype', M.BUF_FILETYPE)
    api.nvim_win_set_option(win, 'cursorline', false)

    api.nvim_buf_set_var(bufnr, 'prev_buf', ctx.prev_bufnr or ctx.bufrn)
    -- default mode is move element
    api.nvim_buf_set_var(bufnr, M.VN_ELM_MOD_MODE, M.ELM_MODIF_MODES[1])

    ubuf.bind_keybindings(bufnr, M.mkKeybindings())

    -- bind_state
    local _, box = M.add_canvas(ctx.canvas, bufnr, 0, 0, ctx.filename)
    M.insert_canvas_to_buf(box, false)
    local place_in_doc = v2s(bufnr) .. ':' .. v2s(ctx.lnum) .. ':0'
    vim.api.nvim_buf_set_var(bufnr, 'state', place_in_doc)
    M.draw_item(box)

    local y, x = math.ceil(ctx.canvas.height / 2), math.ceil(ctx.canvas.width / 2)
    set_cursor_pos(0, y, x)
    vim.cmd([[setlocal nowrap]])
    --
  else -- reuse
    M.draw_canvas(ctx.canvas, ctx.out_bufnr, 0, nil)
  end

  return ctx.out_bufnr
end

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------


function M.is_valid_buf(bufnr)
  local ok, _ = pcall(vim.api.nvim_buf_get_var, bufnr, 'state')
  if not ok then
    log.debug('Stop! Not found state in buff: %s', bufnr)
    return false
  end
  return true
end

-- -- ?
-- function M.draw2buf(o, bufnr)
--   log.debug("draw2buf ", bufnr)
--   assert(type(o) == 'table', 'o table state has:' .. tostring(o))
--   bufnr = bufnr or 0
--   if not M.is_valid_buf(bufnr) then
--     return
--   end
--
--   M.draw_canvas(bufnr, o.canvas)
-- end

--------------------------------------------------------------------------------


---@param bufnr number
---@return boolean
local function isHelpPageOpened(bufnr)
  local ok, value = pcall(vim.api.nvim_buf_get_var, bufnr, 'help_page')
  return ok ~= nil and value == true
end

local function close_help_page(bufnr)
  vim.api.nvim_buf_set_var(bufnr, 'help_page', nil)
  local bn, lnum, col = get_editor_coords()
  local box = get_canvas_box(bn, lnum, lnum, col, col)
  if box then
    if M.isCanvasEditor(box.bufnr) then
      vim.api.nvim_buf_set_lines(box.bufnr, 0, -1, false, {})
    end
    M.draw_item(box)
  else
    print('Error: cannot find Canvas for bufnr:', bn, lnum, col)
  end
end

function M.do_toggle_help_page()
  local bufnr = vim.api.nvim_get_current_buf()
  local in_help = isHelpPageOpened(bufnr)
  log.trace("do_toggle_help_page", bufnr, in_help)

  if not in_help then
    -- vim.api.nvim_buf_set_var(bufnr,
    set_buf_var(bufnr, 'help_page', true)
    M.draw_help(bufnr)
  else
    close_help_page(bufnr)
  end
end

--
-- q
--
function M.do_close()
  local bufnr = vim.api.nvim_get_current_buf()
  local in_help = isHelpPageOpened(bufnr)
  log.debug("close", bufnr, in_help)

  if isHelpPageOpened(bufnr) then
    close_help_page(bufnr)
    return
  else
    local box, _, _ = get_ctx(false, 0)
    if box:has_marked_pos() then
      box:clear_marked_pos()
      print('clear marked position')
    end
  end
end

local function unchanged(box)
  return box ~= nil and (not box.has_changes)
end

--
-- Q
-- todo ask to save or release as test to the buffer of doc
--
-- close CanvasEditor Window and delete copy of the canvas from canvas_map
--
function M.do_close_and_clean_buf()
  local bufnr = vim.api.nvim_get_current_buf()
  local in_help = isHelpPageOpened(bufnr)
  log.debug("close_and_clean_buf", bufnr, in_help)

  if isHelpPageOpened(bufnr) then
    close_help_page(bufnr)
    return
  end

  local box, idx = get_canvas_box(bufnr, 1)

  if unchanged(box) or ask_confirm_and_do(M.SURE_CLOSE_EDITOR) then
    if box and idx then
      if box.has_changes ~= false and ask_confirm_and_do('Save changes?') then
        M.do_save_canvas(box)
      end
      local was = remove_ui_item(bufnr, idx, box)
      log.debug('Cleaned for bufnr:%s %s', bufnr, was ~= nil)
    end
    -- standart cmd bdelete - can close nvim
    -- Bdelete cmd from moll/vim-bbye
    if vim then
      vim.cmd(':EnvBufClose') -- '<cmd>Bdelete<cr>',
    end
  end
end

local function items_with_idx(items)
  local t = {}
  for i, item in ipairs(items) do
    table.insert(t, fmt('%d: %s', i, v2s(item)))
  end
  return t
end


function M.do_select_utf8_ico()
  -- build on first access
  if not utf8_icons then
    utf8_icons = u8.foreach_letter(utf8_icons_samples,
      function(letter, acc)
        if letter ~= "\n" and letter ~= " " then
          acc[#acc + 1] = letter
        end
      end,
      {})
  end

  local n = vim.fn.inputlist(items_with_idx(utf8_icons))
  local choice = utf8_icons[n or false]
  if choice == nil then
    print('wrong index', n)
    return
  end
  return choice
end

--
-- insert the new object into canvas
-- triggers by `i`
--
---@param edit boolean?
function M.do_new_object(edit)
  local items = {
    'utf8-ico', 'text', 'rectangle', 'line', 'point', 'container'
  }
  local n = vim.fn.inputlist(items_with_idx(items))
  local choice = items[n or false]
  if choice == nil then return end

  local box, lnum, col, lnum2, col2 = get_ctx(false)
  local o = nil
  local w, h, c = nil, nil, ''
  if lnum2 > lnum then h = lnum2 - lnum end
  if col2 > col then w = col2 - col end

  if choice == 'utf8-ico' then
    local s = M.do_select_utf8_ico()
    if s and s ~= '' then
      o = box:add(Point:new(nil, 0, 0, s), lnum, col)
    end
    --
  elseif choice == 'point' then
    o = box:add(Point:new(nil, 0, 0, '+'), lnum, col)
    --
  elseif choice == 'rectangle' then
    w = w or DEF_RECTANGLE.w
    h = h or DEF_RECTANGLE.h
    o = box:add(Rectangle:new(nil, 0, 0, w, h), lnum, col)
    --
  elseif choice == 'line' then
    if type(w) == 'number' and not h then
      h, c = 0, '-'
    elseif type(h) == 'number' and not w then
      w, c = 0, '|'
    else
      w, h, c = DEF_LINE.w, DEF_LINE.h, DEF_LINE.c
    end
    if h > 0 and w > 0 then h = 0 end -- mk horizontal, remove diagonal

    o = box:add(Line:new(nil, 0, 0, w, h, c), lnum, col)
    --
  elseif choice == 'text' then
    -- default w and h auto calculated
    o = box:add(Text:new(nil, 0, 0, 'Text', w, h), lnum, col)
    edit = true
    --
  elseif choice == 'container' then
    -- join selected elements into container "pack elements into container"
    if ubuf.is_visual_mode() then
      local limit = SELECT_LIMIT
      local elms, idxs = box:get_objects_at(lnum, col, lnum2, col2, limit)
      if elms and next(elms) then
        local container_index, moved = box.item:join_to_container(elms, idxs)
        print('container_index:', container_index, 'moved:', moved)
      end
    end
  end

  if o then
    if edit then
      -- todo edit via popup window
      local _, err_msg = M.edit_element(o, nil, nil, box)
      if err_msg then print(err_msg) end
    end
    M.draw_item(box)
    close_visual_mode()
  end
end

--
-- to wrap text use roughly similar input with a command at the end:
-- "Some Test \wrap,help!"
--
-- insert test element into canvas
function M.do_new_text()
  local input = ask_value('Text:', '')
  if not input or input == '' then
    return print('canceled')
  end

  local box, lnum, col, lnum2, col2, _ = get_ctx(false, 0)
  local w, h = nil, nil
  if lnum2 > lnum then h = lnum2 - lnum end
  if col2 > col then w = col2 - col end
  input = input:gsub("\\n", "\n")

  local text, args = get_line_n_command(input, ' ')

  -- deferred creation of an element on demand from within the command
  local function mk_elm()
    local x, y = box:toInnerXY(lnum or 0, col or 0)
    return Text:new(nil, 0, 0, text, w, h):move(x, y)
  end

  if args and next(args) then
    local params = {
      box = box, elm = mk_elm, lnum = lnum, col = col, lnum2 = lnum2, col2 = col2
    }
    if builtin_mod_elm.handle(args, params) == 0 then
      M.draw_item(box:changed())
    end
    -- else: cancel on unknown command
    return
  end

  box:addWithCoords(mk_elm())
  M.draw_item(box)
end

--
--
--
function M.do_open_console()
  log_debug('do_open_console')

  local input = ask_value('Console: ', '\\')
  if not input or input == '' or input == '\\' then return print('canceled') end

  local _, args = get_line_n_command(input, ' ')

  if args and next(args) then
    local box, lnum, col, lnum2, col2, _ = get_ctx(false, 0)
    local params = {
      box = box, lnum = lnum, col = col, lnum2 = lnum2, col2 = col2
    }
    if builtin_mod_elm.handle(args, params) == 0 then
      M.draw_item(box:changed())
    end
  end
end

--
-- toggle mode show indexes or not
--
---@param box env.draw.ui.ItemPlaceHolder?
function M.do_toggle_display_indexes(box)
  local bufnr = vim.api.nvim_get_current_buf()
  local ok, value = get_buf_var(bufnr, M.VN_DISPLAY_INDEXES)
  log.debug("do_toggle_display_indexes", bufnr, value)

  if not ok or value == false then
    if M.set_display_indexes(true, box) then
      local b = set_buf_var(bufnr, M.VN_DISPLAY_INDEXES, true)
      _, value = get_buf_var(bufnr, M.VN_DISPLAY_INDEXES)
      log_debug("set_buf_var", M.VN_DISPLAY_INDEXES, value, b)
      return true
    end
  elseif value == true then
    set_buf_var(bufnr, M.VN_DISPLAY_INDEXES, false)
    M.set_display_indexes(false, box)
    return false
  end
end

--
--
---@param enable boolean
---@param box env.draw.ui.ItemPlaceHolder?
function M.set_display_indexes(enable, box)
  local lnum, col
  if not box then
    box, lnum, col = get_ctx(false, 0)
  end
  if not box then
    return false, 'Not Found Canvas ' .. v2s(lnum) .. ' ' .. v2s(col)
  end

  local canvas = Canvas.cast(box.item)
  if enable then
    canvas:addHook(Canvas.HOOK_POST_DRAW, Canvas.hook_display_indexes)
  else
    canvas:removeHook(Canvas.HOOK_POST_DRAW, Canvas.hook_display_indexes)
  end

  box:draw()

  return true, nil
end

--
-- place an element in the background relative to all elements included in
-- the area of this element
--
function M.do_make_element_as_background()
  local box, lnum, col, _, _, elements, indexes = get_ctx(true, 1)

  if not elements or #(elements) < 1 then
    return false, fmt('Not Found element in current position %s:%s', lnum, col)
  end
  local canvas = Canvas.cast(box.item)
  local mkrepo = conf.REPORT_ON_MAKE_ELM_BACKGROUND
  local _, msg = canvas:mkElementBackgroud(elements, indexes, nil, mkrepo)
  print(msg)
end

--

function M.do_inspect()
  local limit = is_visual_mode() and SELECT_LIMIT or 1
  local box, lnum, col, lnum2, col2, elements, indexes = get_ctx(true, limit)

  -- local m = vim.api.nvim_get_mode().mode or '?'
  -- local col0 = (vim.api.nvim_win_get_cursor(0) or E)[2]

  local x, y = box:toInnerXY(lnum, col)
  local x2, y2 = box:toInnerXY(lnum2, col2)
  local s = fmt('Position[%s:%s %s:%s]', v2s(x), v2s(y), v2s(x2), v2s(y2))

  if not elements or not next(elements) then
    -- about canvas
    s = s .. ' ' .. v2s(box.item) .. ' ' .. v2s(box.filename)
    if box.has_changes == nil or box.has_changes == true then
      s = s .. ' U'
    end
    if box:has_marked_pos() then
      local mx, my = box:getMarkedPosXY()
      s = s .. fmt(' marked pos(%s:%s)', mx, my)
    end
    s = s .. ' hooks:' .. box.item:hooksCount()
  else
    local n, sep = 0, ' '
    for i, elm in ipairs(elements) do
      local prefix, parent = '', nil
      if x == x2 and y == y2 then -- not a selected range
        local child = elm:get_child_at(x, y)
        if child then
          parent = elm
          elm = child
          prefix = IEditable.mkChildPrefix(parent, elm, x, y)
        end
      end
      local index = (indexes or E)[i]
      local editable = instanceof(elm, IEditable)
      local drawable = instanceof(elm, IDrawable)
      if n > 0 then sep = "\n" end
      s = s .. sep .. fmt('%s IEditable:%s IDrawable:%s index: %s',
        prefix, elm, v2s(editable), v2s(drawable), v2s(index))
    end
  end
  print(s)
end

--
-- edit object in the canvas under the cursor
-- if not in visual mode, then edit only the topmost element.
-- otherwise edit all elements that fall into the selection
--
-- TODO: create a special window(buffer with keybindings) for more convenient
-- editing of element properties
--
function M.do_edit()
  log_debug("do_edit")
  local limit = is_visual_mode() and SELECT_LIMIT or 1

  ---@diagnostic disable-next-line: unused-local
  local box, lnum, col, lnum2, col2, elements, indexes = get_ctx(true, limit)
  if not elements or not elements[1] then
    return print('Nothing to edit')
  end

  local has_changes, edit_all_elements = false, true
  local sz = #(elements)

  -- edit order of the canvas elements to render
  if sz > 1 and indexes then
    print('Edit Order of the selected elements')
    local updated, ret = M.do_element_order_edit(box.item, elements, indexes)
    if updated then
      has_changes = true
      print(ret) -- "N pairs of elements was swapped"
      local msg0 = fmt('Start editing all %d elements?', sz)
      edit_all_elements = ask_confirm_and_do(msg0)
    elseif ret == '\\q' then
      return print('stop the edit.')
    else
      print('Order unchanged.')
    end
  end

  -- Edit elements properties via cli input
  if edit_all_elements then
    local c, msg = 0, ''
    local x, y = box:toInnerXY(lnum, col)

    for i, elm in ipairs(elements) do
      local updated, err_msg = M.edit_element(elm, i, sz, box, x, y)
      if updated then
        has_changes = true
        c = c + 1
      elseif sz == 1 then
        if err_msg ~= nil then
          print(updated, err_msg)
        end
      elseif err_msg == '\\q' then
        msg = 'stop the edit.'
        break
      end
    end
    if sz > 1 then print(msg, c, 'elements was changed') end
  else
    print('elements editing was canceled')
  end

  if has_changes or box:isChanged() then
    M.draw_item(box:changed())
  end
end

--
-- edit selected elements in new buffer
--
-- Note: this does not work correctly when called via a command
-- rather than via a hot key
--
function M.do_edit_as_code()
  local limit = is_visual_mode() and SELECT_LIMIT or 1

  local box, _, _, _, _, elements, indexes = get_ctx(true, limit)
  if not elements or not next(elements) then
    return print('Nothing to edit')
  end
  if not indexes or #indexes ~= #elements then
    return print('Cannot edit without indexes', #(elements), #(indexes or E))
  end

  local box_idx = get_ui_item_index(box.bufnr, box)
  assert(type(box_idx) == 'number', 'index of box in list')

  return ElementsEditor.edit_elements_as_code(box, box_idx, elements, indexes)
end

--
-- inc/dec number in the text of element

function M.do_inc_number_in_text() M.inc_num_in_text_elm(1) end

function M.do_dec_number_in_text() M.inc_num_in_text_elm(-1) end

function M.inc_num_in_text_elm(offset)
  if offset == 0 then return print('offset is 0 nothing to do') end

  local box, _, _, _, _, elements, indexes = get_ctx(true, 1)
  if not box or not elements or not next(elements) then
    return print('Nothing to edit')
  end
  if not indexes or #indexes ~= #elements then
    return print('Cannot edit without indexes', #(elements), #(indexes or E))
  end

  local e, updated = elements[1], false
  if instanceof(e, Text) then
    ---@cast e env.draw.Text
    local n = tonumber(e.text)
    if n then
      e.text = tostring(n + offset)
      updated = true
    else
      local digit = string.match(e.text, '(%-?%d+)')
      if digit then
        local i, j = string.find(e.text, digit, 1, true)
        assert(i and j, 'sure found digit in text')
        n = tonumber(digit) + offset
        e.text = e.text:sub(1, i - 1) .. tostring(n) .. e.text:sub(j + 1)
        updated = true
      end
    end
    -- resize
    if updated and #e.text > e.x2 - e.x1 then
      e.x2 = e.x1 + #e.text
    end
  else
    return print('Not supported for ' .. class.name(e))
  end

  if updated then
    box:changed():draw()
  end
end

--
-- mark positions for quick insert new element to the canvas

function M.do_mark_pos_start() M.do_mark_pos(1) end

function M.do_mark_pos_middle() M.do_mark_pos(2) end

---@param n number
function M.do_mark_pos(n)
  local box, lnum, col = get_ctx(false, 0)

  local x, y = box:toInnerXY(lnum, col)

  if n == 1 then -- start or finish
    -- cleate from 3+ points two lines as one
    if box:get_marked_pos_count() > 1 then
      local ok, style = M.ask_rect_style_interactively()
      if not ok or type(style) ~= 'table' then
        return print('canceled')
      end

      box:mark_pos(x, y)
      local pos_list = box:pop_marked_positions() ---@cast pos_list table
      local container = create_combined_line(pos_list, style)
      if not container then
        return print('error')
      end

      box:addWithCoords(container)

      return M.draw_item(box:changed())
    end

    local prev = box:pop_marked_pos()

    if not prev then
      box:mark_pos(x, y)
      print('mark position', x, y)
      return
    end

    local elm, w, h = nil, math.abs(prev.x - x) + 1, math.abs(prev.y - y) + 1


    if w == 1 and h == 1 then
      elm = box:addWithCoords(Point:new(nil, x, y, '+'))
    elseif w == 1 or h == 1 then
      local c = w == 1 and '|' or '-'
      elm = box:addWithCoords(Line:new(nil, prev.x, prev.y, x, y, c))
    elseif w > 1 and h > 1 then
      elm = box:addWithCoords(Rectangle:new(nil, prev.x, prev.y, x, y))
    end

    if elm then
      print(elm)
      M.draw_item(box)
    end
    --
  elseif n == 2 then
    local _, cnt = box:mark_pos(x, y)
    print('mark middle position', x, y, ' points:', cnt)
  else
    print('todo select element')
  end
end

--
-- if not in visual mode, then copy only the topmost element.
-- in visual mode, select all elements that intersect with the selection,
-- even if they are hidden behind other elements.
--
function M.do_copy()
  local limit = is_visual_mode() and SELECT_LIMIT or 1
  local box, lnum, col, lnum2, col2, objects = get_ctx(true, limit)

  if not objects or not objects[1] then
    print('Nothing to copy')
    return
  end

  local n = M.copy_from_canvas(box, objects, lnum, col, lnum2, col2)
  print('copied', n)

  close_visual_mode()
end

--
-- if not in visual mode, then cut only the topmost element.
-- in visual mode, select all elements that intersect with the selection,
-- even if they are hidden behind other elements.
--
---@return number
function M.do_cut()
  local limit = is_visual_mode() and SELECT_LIMIT or 1
  local box, lnum, col, lnum2, col2, objects = get_ctx(true, limit)

  if not objects or not objects[1] then
    print('Nothing to cut')
    return 0
  end

  local n = M.cut_from_canvas(box, objects, lnum, col, lnum2, col2)
  print('cutted', n)

  close_visual_mode()
  return n
end

--
-- paste element from cb to the canvas
--
---@param n number? index of wraped element in the cb-list
function M.do_paste(n)
  if clipboard_is_empty() then
    return print('empty')
  end
  local box, lnum, col = get_ctx(false, 0)

  local selection, needs_copy = clipboard_get_entry(n) -- selection list
  local cnt = M.add_to_canvas(box, lnum, col, selection, needs_copy)

  print(tostring(cnt) .. ' elements pasted')

  if cnt > 0 then
    setLastSelection(selection, needs_copy)
    M.draw_item(box:changed())
  end

  close_visual_mode()
  return selection
end

-- do_cut, do_paste -> do_undo_paste
--
---@param box env.draw.ui.ItemPlaceHolder?
function M.do_undo_paste(box)
  local sel = state.getLastSelection()
  if not sel then
    return print('empty')
  end

  box = box or get_ctx(false, 0)
  local n, sel0, msg
  -- local canvas = Canvas.cast(box.item)
  -- local idxs = canvas:get_indexes_by_elements(sel:elements())
  if sel:isCut() then
    n, sel0 = M.cut_from_canvas(box, sel:elements(), 0, 0)
    msg = fmt('%s elements undo back to clipboard', n)
    if sel0 then
      state.setLastSelection(nil)
      sel0:setOffset(0, 0)
    end
  elseif sel:isCopy() then
    -- todo just remove from canvas same elements
    msg = 'undo past from copy not implemented yet'
  elseif sel:isSelect() then
    msg = 'undo select not implemented yet'
  end

  if msg then print(msg) end
  return n
end

--
function M.do_undo()
  -- TODO track actions and apply undo by last actions
  M.do_undo_paste()
end

--
---@param quiet boolean
function M.do_delete_last_from_cb(quiet)
  local selection = clipboard_delete_entry(-1)

  -- show what was deleted
  if not quiet and selection then
    print('deleted from cb: ', tostring(selection))
  end
  if not selection then
    return print('nothing to deleted from cb: ')
  end

  deleted_hist_add_entry(selection)

  return selection
end

--
-- cut off a part from an element breaking it into simpler elements
--
function M.do_cutoff_element()
  local limit = 1 -- is_visual_mode() and SELECT_LIMIT or 1
  local box, lnum, col, lnum2, col2, objects = get_ctx(true, limit)

  if not objects or not objects[1] then
    print('Nothing to cut')
    return 0
  end

  local n = M.cutoff_element(box, objects, lnum, col, lnum2, col2)
  print('the element has been cut into pieces', n)

  close_visual_mode()
  return n
end

-- select whole element under the cursor from bottom-right to top-left
function M.do_select_whole_element_tl() M.do_select_whole_element('TL') end

function M.do_select_whole_element_br() M.do_select_whole_element('BR') end

function M.do_select_whole_element_bl() M.do_select_whole_element('BL') end

function M.do_select_whole_element_tr() M.do_select_whole_element('TR') end

-- in visual mode
-- select area with vertex of the element under the cursor
function M.do_select_whole_element(distination)
  local box, _, _, _, _, elements = get_ctx(true, 1)
  local elm = (elements or E)[1]
  if not elm then return print('No element found') end

  local x, y, x2, y2 = elm:get_coords()
  local w, h = x2 - x, y2 - y
  local canvas_width, canvas_height = box.item.width, box.item.height

  local cx, cy, dx, dy = nil, nil, nil, nil
  close_visual_mode()

  if distination == 'TL' then -- to select from bottom-right to topleft
    cy, cx, dx, dy = y2, x2, 'L', 'U'
  elseif distination == 'BR' then
    cy, cx, dx, dy = y, x, 'R', 'D'
  elseif distination == 'TR' then
    cy, cx, dx, dy = y2, x, 'R', 'U'
  elseif distination == 'BL' then
    cy, cx, dx, dy = y, x2, 'L', 'D'
  else
    error('Unknown destination: "' .. v2s(distination) .. '"')
  end

  -- fix selection range in the corners of x,y coordinate axis
  local xoff = dx == 'L' and -w or w
  local yoff = dy == 'U' and -h or h

  if cx <= 0 then
    w = w + cx - 1
    cx = 0
  elseif cx >= canvas_width then
    w = w - (cx - canvas_width)
    cx = canvas_width
  else
    local dcx = cx + xoff
    if dcx <= 0 then
      w = w + dcx - 1
    elseif dcx >= canvas_width then
      w = w - (dcx - canvas_width)
    end
  end

  if cy <= 0 then
    h = h + cy - 1
    cy = 0
  elseif cy >= canvas_height then
    h = h - (cy - canvas_height)
    cy = canvas_height
  else
    local dcy = cy + yoff
    if dcy <= 0 then
      h = h + dcy - 1
    elseif dcy >= canvas_height then
      h = h - (dcy - canvas_height)
      local lnum_max = vim.api.nvim_buf_line_count(box.bufnr)
      if h >= lnum_max then h = lnum_max - 1 end
    end
  end

  local c_lnum, c_col = box:toOuterLnumCol(cx, cy)
  set_cursor_pos(0, c_lnum + 1, c_col)
  open_visual_mode()
  nvim_move_cursor(dx, w, dy, h)
end

--

function M.do_go_to_vertex_h() M.do_go_to_vertex('L') end

function M.do_go_to_vertex_j() M.do_go_to_vertex('D') end

function M.do_go_to_vertex_k() M.do_go_to_vertex('U') end

function M.do_go_to_vertex_l() M.do_go_to_vertex('R') end

function M.do_go_to_vertex_hc() M.do_go_to_vertex('L', true) end

function M.do_go_to_vertex_jc() M.do_go_to_vertex('D', true) end

function M.do_go_to_vertex_kc() M.do_go_to_vertex('U', true) end

function M.do_go_to_vertex_lc() M.do_go_to_vertex('R', true) end

--
--
function M.edges_shortly(top, right, bottom, left)
  local t = top == true and 'T' or 't'
  local r = right == true and 'R' or 'r'
  local b = bottom == true and 'B' or 'b'
  local l = left == true and 'L' or 'l'
  return t .. r .. b .. l
end

--
--
-- Note: box.lnum starts from 0 nvim.lnum from 1
--
---@param direction string U|D|R|L
---@param center boolean?
function M.do_go_to_vertex(direction, center)
  local box, lnum, col, _, _, elements = get_ctx(true, 1)
  local elm = (elements or E)[1]
  if not elm then
    return M.go_to_nearest_element(box, lnum, col, direction, center)
  end

  local x, y
  local x1, y1, x2, y2 = elm:get_coords() -- coords in canvas (cell utf8-letters)
  local cx, cy = box:toInnerXY(lnum, col)
  local left_edge, right_edge = cx == x1, cx == x2
  local top_edge, bottom_edge = cy == y1, cy == y2

  local is_debug = log_is_debug()
  if is_debug then
    log_debug('from box:(%s:%s) goto[%s] edges:[%s] c:(%s:%s) e:(%s:%s %s:%s)',
      lnum, col, -- coords starts from 0,
      direction,
      M.edges_shortly(top_edge, right_edge, bottom_edge, left_edge),
      cx, cy,        -- inner cursor pos in the Canvas from box.lnum&col
      x1, y1, x2, y2 -- inner coords of the element in the canvas
    )
  end

  if left_edge and direction == 'L' or right_edge and direction == 'R' or
      top_edge and direction == 'U' or bottom_edge and direction == 'D' then
    return M.go_to_nearest_element(box, lnum, col, direction, center)
  end

  -- calculate the position coordinate for half or insert the first argument
  local p = function(center0, a, b)
    if center0 then return a + math.ceil((b - a) / 2) else return a end
  end

  ---@format disable-next
  if left_edge then
        if direction == 'U' then x, y = cx, p(center, y1, cy) -- cx y1
    elseif direction == 'D' then x, y = cx, p(center, y2, cy) -- cx y2
    elseif direction == 'R' then x, y = p(center, x2, cx), cy -- x2 cy
    end
  elseif right_edge then
        if direction == 'U' then x, y = cx, p(center, y1, cy) -- cx y1
    elseif direction == 'D' then x, y = cx, p(center, y2, cy) -- cx y2
    elseif direction == 'L' then x, y = p(center, x1, cx), cy
    end
  elseif top_edge then
        if direction == 'D' then x, y = cx, p(center, y2, cy) -- cx y2
    elseif direction == 'L' then x, y = p(center, x1, cx), cy -- x1 cy
    elseif direction == 'R' then x, y = p(center, x2, cx), cy -- x2 cy
    end
  elseif bottom_edge then
        if direction == 'U' then x, y = cx, p(center, y1, cy)
    elseif direction == 'L' then x, y = p(center, x1, cx), cy
    elseif direction == 'R' then x, y = p(center, x2, cx), cy
    end
  else -- from within the element itself
        if direction == 'U' then x, y = cx, p(center, y1, cy)
    elseif direction == 'L' then x, y = p(center, x1, cx), cy
    elseif direction == 'R' then x, y = p(center, x2, cx), cy
    elseif direction == 'D' then x, y = cx, p(center, y2, cy)
    end
  end

  if x and y then
    local ln, cn = box:toOuterLnumCol(x, y)
    set_cursor_pos(0, ln + 1, cn)
  else
    log_debug('not found position to jump x:%s, y:%s', x, y)
    print(x, y)
  end
end

---@param box env.draw.ui.ItemPlaceHolder
---@param lnum number
---@param col number
function M.go_to_nearest_element(box, lnum, col, direction, center)
  log_debug("go_to_nearest_element[%s] center:%s %s %s", direction, center, lnum, col)

  local canvas = Canvas.cast(box.item)
  local x1, y1 = box:toInnerXY(lnum, col)
  local x, y = canvas:getFirstNotEmptyLayoutCell(nil, x1, y1, direction)
  log_debug('InCanvasXY:(%s:%s) first-not-empty-cell:(%s:%s)', x1, y1, x, y)

  if center then
    ---@format disable-next
        if direction == 'R' then x = x1 + math.ceil((x - x1) / 2)
    elseif direction == 'L' then x = x1 - math.ceil((x1 - x) / 2)
    elseif direction == 'U' then y = y1 - math.ceil((y1 - y) / 2)
    elseif direction == 'D' then y = y1 + math.ceil((y - y1) / 2)
    end
  end

  local new_lnum, new_col = box:toOuterLnumCol(x, y)
  set_cursor_pos(0, new_lnum + 1, new_col)
end

---@param bufnr number
local function get_elm_modif_mode(bufnr)
  local ok, value = pcall(vim.api.nvim_buf_get_var, bufnr, M.VN_ELM_MOD_MODE)
  if not ok then
    error('Not found "' .. v2s(M.VN_ELM_MOD_MODE) .. '" in bn:' .. v2s(bufnr))
  end
  return value
end

-- mode resize
local function toggleElmModificationMode(bufnr)
  local value = get_elm_modif_mode(bufnr)
  local i = indexof(M.ELM_MODIF_MODES, value)
  if not i then error('unknown elm modification mode ' .. tostring(value)) end
  i = i + 1
  if i > #M.ELM_MODIF_MODES then i = 1 end
  local new_mode = M.ELM_MODIF_MODES[i]
  vim.api.nvim_buf_set_var(bufnr, M.VN_ELM_MOD_MODE, new_mode)
  print('modification mode: ' .. v2s(new_mode))
end

-- move, resize,
-- to take apart?, combine selected elements into one whole
function M.do_toggle_elm_modification_mode()
  local bufnr = vim.api.nvim_get_current_buf()
  toggleElmModificationMode(bufnr)
end

--
function M.do_mod_element_h() M.do_mod_element('L') end

function M.do_mod_element_j() M.do_mod_element('D') end

function M.do_mod_element_k() M.do_mod_element('U') end

function M.do_mod_element_l() M.do_mod_element('R') end

--
-- entry point for several actions on elements using the same key combinations
-- but depending on the current element modifiacation mode
-- "drag" is default
--
function M.do_mod_element(direction)
  local bufnr = vim.api.nvim_get_current_buf()
  local mode = get_elm_modif_mode(bufnr)

  ---@format disable-next
      if mode == 'drag' then M.do_resize_element(direction) -- resize+move
  elseif mode == 'slice' then M.do_slice_element(direction)
  elseif mode == 'join' then M.do_join_element(direction)
  end
end

--
--
---@param direction string
---@param step number? 1 is default
local function direction_to_offsetXY(direction, step)
  step = step or 1
  local xoff, yoff = 0, 0

  ---@format disable-next
      if direction == 'U' then yoff = -step
  elseif direction == 'D' then yoff =  step
  elseif direction == 'L' then xoff = -step
  elseif direction == 'R' then xoff =  step
  end

  return xoff, yoff
end

--
---@param direction string
function M.do_move_element(direction)
  -- TODO optimization via toggle move-mode and selection to fix issue:
  -- losing an element with a lower index when bumping into another with
  -- a high one
  local limit = is_visual_mode() and SELECT_LIMIT or 1
  local box, _, _, _, _, elements = get_ctx(true, limit)
  if not (elements or E)[1] then
    return print('nothing to move')
  end

  local xoff, yoff = direction_to_offsetXY(direction, 1)

  if xoff ~= 0 or yoff ~= 0 then
    ---@diagnostic disable-next-line: param-type-mismatch
    for _, elm in ipairs(elements) do
      elm:move(xoff, yoff)
    end

    M.draw_item(box:changed())
    nvim_move_cursor(direction, 1)
  end
end

-- move or resize based on position inside element

function M.do_drag_element_h() M.do_resize_element('L') end

function M.do_drag_element_j() M.do_resize_element('D') end

function M.do_drag_element_k() M.do_resize_element('U') end

function M.do_drag_element_l() M.do_resize_element('R') end

--
-- aliase drag_element(resize or move)
-- on enges of element - resize, from inside the element - move
--
---@param direction string
function M.do_resize_element(direction)
  log_trace("do_resize_element", direction)

  local limit = is_visual_mode() and SELECT_LIMIT or 1
  local box, lnum, col, _, _, elements = get_ctx(true, limit)
  if not (elements or E)[1] then
    return print('nothing to resize')
  end

  local updated, cx, cy = false, box:toInnerXY(lnum, col)
  ---@diagnostic disable-next-line: param-type-mismatch
  for _, elm in ipairs(elements) do
    local u, r, d, l, elm_part = elm:offsets_on_drag(cx, cy, direction)
    local resize = not (u == 0 and r == 0 and d == 0 and l == 0)

    if resize then
      elm = elm_part or elm -- eg. inner line in the combined line(or relation)
      log_trace('resize urdl: %s %s %s %s elm:%s', u, r, d, l, elm)
      elm:resize(u, r, d, l)
      updated = true
    else
      local xoff, yoff = direction_to_offsetXY(direction, 1)
      if xoff ~= 0 or yoff ~= 0 then
        elm:move(xoff, yoff)
        updated = true
      end
    end
  end

  if updated then
    M.draw_item(box:changed())
    nvim_move_cursor(direction, 1)
  end
end

--
-- cutting of one element into several element-parts
--
---@param direction string
function M.do_slice_element(direction)
  error('Not implemented yet do_slice_element ' .. v2s(direction))
end

--
-- gluing-combining several elements into one whole
--
---@param direction string
---@diagnostic disable-next-line: unused-local
function M.do_join_element(direction)
  error('Not implemented yet do_join_element ' .. v2s(direction))
end

--------------------------------------------------------------------------------

function M.do_load_canvas()
  local box = get_ctx(false, 0)
  if not box then
    return print('Not Found Canvas')
  end
  if not box.filename then
    return print('No filename')
  end

  local ok, canvas = load_canvas(box.filename)
  if not ok or type(canvas) ~= 'table' then
    print(canvas)
  else
    local prev_height = box.item.height
    local old_canvas, old_fn = box:setItem(Canvas.cast(canvas), box.filename)
    local updated = update_ui_item(old_canvas, canvas, box.filename)
    box.has_changes = false
    print('loaded from', box.filename, 'updated:', updated)

    M.draw_item(box)

    if prev_height > box.item.height and M.isCanvasEditor(box.bufnr) then
      -- remove hanging lines not related to the canvas
      vim.api.nvim_buf_set_lines(box.bufnr, box.lnum_end, -1, true, {})
    end

    assert(old_fn == nil or old_fn == box.filename, 'same filename for wrappers')
  end
end

---@param box env.draw.ui.ItemPlaceHolder?
function M.do_save_canvas(box)
  box = box or get_ctx(false, 0)
  if not box then
    print('Not Found Canvas')
    return
  end
  if not box.filename then
    M.do_save_canvas_as(box)
  end
  local saved, msg = save_canvas(box, nil, false, true, ask_confirm_and_do)
  print(saved, v2s(msg))
  if saved then
    box.has_changes = false
    update_ui_item(box.item, box.item, box.filename)
  end
end

---@param box env.draw.ui.ItemPlaceHolder?
function M.do_save_canvas_as(box)
  box = box or get_ctx(false, 0)
  if not box then
    print('Not Found Canvas')
    return
  end
  local fn = ask_value('Canvas: Save as filename: ', box.filename, nil)
  if fn and fn ~= '' and fn ~= 'q' then
    local saved, msg = save_canvas(box, fn, false, true, ask_confirm_and_do)
    print(msg)
    -- update source file
    if saved then
      box.filename = fn
      box.has_changes = false
    end
  else
    print('canceled')
  end
end

function M.do_nothing()
end

--------------------------------------------------------------------------------

-- restore deleted selection with elements from deleted_history
--
---@param box env.draw.ui.ItemPlaceHolder
---@param idx number
---@param lnum number? if given when restore to cursor
---@param col number?
function M.restore_deleted(box, idx, lnum, col)
  local canvas = Canvas.cast(box.item)
  local selection, size = state.deleted_hist_get_entry(idx)
  if not selection then
    return false, fmt('not found selection for index:%s max:%s', v2s(idx), size)
  end

  if selection then
    selection = state.deleted_hist_remove_entry_safely(idx, selection)

    local x1, y1 = selection:pos()
    local cnt, details = 0, ''

    -- use current cursor coords insead of original coords stored by selection
    if lnum and col and lnum > 0 and col >= 0 then
      x1, y1 = box:toInnerXY(lnum, col)
    end

    for _, elm in ipairs(selection:elements()) do
      ---@cast elm env.draw.IEditable
      -- place into canvas with original coords
      elm:move(x1, y1)
      canvas:add(elm)

      if #details < 40 then
        if #details > 0 then details = details .. ' ' end
        local x, y = elm:pos()
        details = details .. fmt('%s(%s:%s)', elm:short_info(), v2s(x), v2s(y))
      end
      cnt = cnt + 1
    end

    M.draw_item(box:changed())
    -- elements in one selection(entry for clipboard history)
    return true, tostring(cnt) .. ' elements restored ' .. details
  end
end

--
-- testing tools

if _G.TEST then
  -- ui_item is a ItemPlaceHolder wrapper for Canvas
  M.get_ui_items_map = state.get_ui_items_map
  M.clear_all_ui_items = state.clear_all_ui_items

  M.get_cb = state.get_cb

  M.get_deleted = function()
    return state.deleted_history
  end

  ---@param func function
  M.set_get_ctx = function(func)
    assert(type(func) == 'function', 'callback')
    get_ctx = func
  end
  ---@param func function
  M.set_close_visual_mode = function(func) close_visual_mode = func end
  M.set_is_visual_mode = function(func) is_visual_mode = func end
end

return M

-- 08-04-2024 @author Swarg
-- Goal: register an autocommand that will calling the
-- specified callback for specified buffer on save i.g. in ':w' - nvim-command
-- and give the way to update or validate imidiatly value via `:e!` see
-- CmdlineLeave  in do_dispatch

-- local log = require 'alogger'

local M = {}

M.aug_statefulbuf = nil
M.BUF_FILETYPE = 'stateful_buf'
M.BUF_VN_DISPATCHER = 'event_dispatcher'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local events_to_dispatch = {
  'BufWriteCmd',
  'BufReadCmd',   -- alfter :e! all lines will be removed
  'BufWinEnter',  -- triggers and after :e! (can used to update custom content)
  'CmdlineLeave', -- to catch and perform any commands
  'BufDelete',
  --[[ ignored with buftype=acwrite: (see :h buftype)
  'BufReadPre', 'CmdwinEnter', 'FileReadCmd', 'FileReadPost', 'FileReadPre'
]]
}
--
-- callback called to process registered event
--
---@param t table{buf, event, file, id, match}
function M.do_dispatch(t)
  -- local cmd = vim.fn.getcmdline() -- give prev cmd: vim.fn.getreg(":")
  -- local cnt = t.buf and vim.api.nvim_buf_line_count(t.buf) or -1
  -- log.debug("do_dispatch", (t or {}).event, 'lines:', cnt, cmd, t)

  local bufnr = t.buf
  local dispatcher = M.get_buf_var(bufnr, M.BUF_VN_DISPATCHER)
  local vt = type(dispatcher)

  local function has(f) return type(f) == 'function' end -- is_function

  if vt == 'table' then
    local e = t.event

    if e == 'BufWriteCmd' and has(dispatcher.on_write) then
      dispatcher.on_write(t)
      --
    elseif e == 'CmdlineLeave' and has(dispatcher.on_cmd) then
      if vim.v.event.abort == false then
        dispatcher.on_cmd(t, vim.fn.getcmdline()) -- after command input like :e!
        -- curr_cmdline = vim.fn.getcmdline(), prev - vim.fn.getreg(":")
      end
      --
    elseif e == 'BufReadCmd' and has(dispatcher.on_read) then
      dispatcher.on_read(t)
      --
    elseif e == 'BufWinEnter' and has(dispatcher.on_enter) then
      dispatcher.on_enter(t) -- triggers after :e! (or entrer to this buffer)
      --
    elseif e == 'BufDelete' and has(dispatcher.on_delete) then
      dispatcher.on_delete(t)
    end
  elseif vt == 'function' then
    dispatcher(t)
  end
end

--
-- register autocommand for given buffer
-- by default used M.do_dispatch callback, but you can assign your own custom.
-- This callback will be called automatically on one of three events:
--   BufWriteCmd, BufReadCmd, BufDelete
--
---@param bufnr number
---@param callback function?
---@return number
function M.register_autocmd_for(bufnr, callback)
  assert(type(bufnr) == 'number' and bufnr > 0, 'bufnr')
  local api = vim.api

  if not M.aug_statefulbuf then
    M.aug_statefulbuf = api.nvim_create_augroup("statefulbuf", { clear = true })
  end

  return api.nvim_create_autocmd(events_to_dispatch, {
    group = M.aug_statefulbuf,
    buffer = bufnr,
    callback = callback or M.do_dispatch,
  })
end

---@param bufnr number
---@param vn string
---@return false|any
---@return string? errmsg
function M.get_buf_var(bufnr, vn)
  local ok, value = pcall(vim.api.nvim_buf_get_var, bufnr, vn)
  if not ok then
    return false, value -- errmsg
  end
  return value
end

---@param bufnr number
---@param vn string
---@param value any
---@return boolean
---@return string? errmsg
function M.set_buf_var(bufnr, vn, value)
  local ok, err = pcall(vim.api.nvim_buf_set_var, bufnr, vn, value)
  return ok, err
end

--
-- create float window with autocommand and keybindin(q-to close window)
-- it will be closed on q
-- the created window will be closed when pressing the `q` key
-- without changing the held state
--
---@param bufnr number
local function buf_in_float_window(bufnr)
  assert(type(bufnr) == 'number' and bufnr > 0, 'bufnr')
  local ui = vim.api.nvim_list_uis()[1] -- current ui

  local width = math.floor(ui.width * 0.75)
  local height = math.floor(ui.height * 0.5)
  local win_config = {
    relative = "editor",
    width = width,
    height = height,
    col = (ui.width - width) / 2,
    row = (ui.height - height) / 2,
    style = 'minimal',
    focusable = false, -- false
    border = 'single', -- double
  }
  local winnr = vim.api.nvim_open_win(bufnr, true, win_config)

  -- autocommand to close this floating window
  vim.api.nvim_create_autocmd("BufLeave", {
    buffer = bufnr,
    callback = function() M.close(bufnr) end
  })

  -- to close buffer on `q`
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'q', '', {
    desc = "CloseBufInFloatWin",
    silent = true,
    noremap = true,
    nowait = true,
    callback = function() M.close(bufnr) end
  })

  return winnr
end

--
-- create new buffer with given lines and
--
---@param bname string
---@param lines table
---@param dispatcher function|table{on_write,on_read,on_delete}
---@param vars table? used to store data inside the buffer
---@param float boolean?
---@return false|number bufnr
---@return string? errmsg
---@return string? bufname
function M.create_buf(bname, lines, dispatcher, vars, float)
  assert(type(lines) == 'table', 'expected table lines')
  assert(type(dispatcher) == 'table' or type(dispatcher) == 'function',
    'expected dispatcher is function or table got: ' .. tostring(dispatcher))

  local api = vim.api
  bname = bname or 'StatefulBuf'

  local win, bufnr

  if float then
    bufnr = api.nvim_create_buf(false, true)
    win = buf_in_float_window(bufnr)
  else
    bufnr = api.nvim_create_buf(true, true)
    win = api.nvim_get_current_win()
    api.nvim_win_set_buf(win, bufnr) -- mk foreground
  end

  local newname, errmsg = M.assign_unique_bufname(bufnr, bname)
  if not newname then return false, errmsg end
  bname = newname
  if next(lines) ~= nil and lines[1] == nil then
    error('expected lines are a flat list without holes')
  end
  for n, line in pairs(lines) do
    if type(line) ~= 'string' then
      error('expected string line got[' .. v2s(n) .. ']:' .. type(line))
    end
  end
  api.nvim_buf_set_option(bufnr, 'modifiable', true) -- clear old
  api.nvim_buf_set_lines(bufnr, 0, -1, true, lines)
  api.nvim_buf_set_option(bufnr, 'buftype', 'acwrite')
  api.nvim_buf_set_option(bufnr, 'swapfile', false)
  api.nvim_buf_set_option(bufnr, 'filetype', M.BUF_FILETYPE)

  if type(vars) == 'table' then
    for key, value in pairs(vars) do
      local ok, err = pcall(api.nvim_buf_set_var, bufnr, key, value)
      if not ok then
        print('error on set_var for key: ' .. v2s(key) .. ": " .. v2s(err))
      end
    end
  end

  api.nvim_buf_set_var(bufnr, M.BUF_VN_DISPATCHER, dispatcher)
  M.register_autocmd_for(bufnr)

  return bufnr, nil, bname
end

--
-- this function also works correctly with buffers in floating windows:
-- closing the buffer closes the floating window (BufLeave?)
--
-- that is, to close floating window, there is NO need to call
--   vim.api.nvim_command('hide')
--
---@param bufnr number
function M.close(bufnr, parent_bufnr)
  assert(type(bufnr) == 'number' and bufnr > 0, 'bufnr')

  -- so as not to ask to save changes
  vim.api.nvim_buf_set_option(bufnr, 'buftype', 'nofile')

  vim.cmd(':bdelete ' .. bufnr)

  -- open parent buffer
  if type(parent_bufnr) == 'number' and parent_bufnr ~= bufnr then
    pcall(vim.cmd, ':b ' .. tostring(parent_bufnr))
  end
end

--
--
---@return string|false new bufname
---@return string? errmsg
function M.assign_unique_bufname(bufnr, bname)
  local ok, err = pcall(vim.api.nvim_buf_set_name, bufnr, bname)
  if not ok then -- name alredy in use
    if M.get_bufnr_by_name(bname) > 0 then
      local s = string.match(bname, ".-(%d+)$")
      if s and s ~= '' then
        bname = bname:sub(1, #bname - #s)
      end
      local i = tonumber(s) or 1
      bname = bname .. tostring(i + 1)

      return M.assign_unique_bufname(bufnr, bname)
    end
    return false, v2s(bname) .. v2s(err) -- print(err, bname)
  end
  return bname, nil
end

--
-- find already existed buffnr by given buffname in all buffers
-- [Note] Each Buffer have unique name.
--        You cannot create new one with already existed buffer name
--
---@param bname string base name of the buffer name (without full path)
---@return number
function M.get_bufnr_by_name(bname)
  if type(bname) == 'string' and bname ~= "" then
    local list = vim.api.nvim_list_bufs()
    for _, bn in ipairs(list) do
      local loaded = vim.api.nvim_buf_is_loaded(bn)
      if loaded then
        local ok, name = pcall(vim.api.nvim_buf_get_name, bn)
        if ok and name and name ~= '' then
          local basename = string.match(name, ".-([^/\\]+)$")
          if basename == bname then
            return bn
          end
        end
      end
    end
  end
  return -1
end

return M

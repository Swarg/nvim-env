-- 09-04-2024 @author Swarg
local M = {}
--
--------------------------------------------------------------------------------
--                               Settings

M.SYNTAX_BLOCK_NAME = 'xcanvas'
M.SYNTAX_BLOCK_START = '```xcanvas'
M.SYNTAX_BLOCK_END = '```'
M.LINE_WIDTH = 80
M.WORK_WITH_UTF8 = true
M.FIND_SYNTAX_BLOCK_LN_LIMIT = 100
M.CLIPBOARD_HISTORY_LIMIT = 100
M.SELECT_LIMIT = 100

M.DEF_RECTANGLE = { w = 16, h = 4 }
M.DEF_LINE = { w = 8, h = 8, c = '-' }

M.DEV_MODE = false
M.REPORT_ON_MAKE_ELM_BACKGROUND = true

M.CONTAINER_EDIT_INNER_ELEMENTS = false

return M

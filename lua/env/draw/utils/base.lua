-- 09-04-2024 @author Swarg
--
local log = require 'alogger'

---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace

local M = {}

-- local conf = require('env.draw.settings')

-- local SYNTAX_BLOCK_NAME = conf.SYNTAX_BLOCK_NAME
-- local SYNTAX_BLOCK_START = conf.SYNTAX_BLOCK_START
-- local SYNTAX_BLOCK_END = conf.SYNTAX_BLOCK_END

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


--------------------------------------------------------------------------------

---@param a number, b number
---@return number, number
function M.smallest_first(a, b)
  if a < b then return a, b else return b, a end
end

---@param x1 number, y1 number, x2 number, y2 number
---@return number, number, number, number
function M.normalize(x1, y1, x2, y2)
  x1, x2 = M.smallest_first(x1, x2)
  y1, y2 = M.smallest_first(y1, y2)
  return x1, y1, x2, y2
end

---@param a number
---@param b number
function M.center(a, b)
  a, b = M.smallest_first(a, b)
  if a == b then
    return a
  else
    return a + math.floor((b - a) / 2)
  end
end

--
-- normalized area for intersection search
-- guarantees that xy will be higher and to the left than x2y2
-- that is, x is always less than x2 and so on
--
---@param y number
---@param x number
---@param y2 number?
---@param x2 number?
---@return number x   definitely less than x2
---@return number y   definitely less than x2
---@return number x2
---@return number y2
function M.normalize_range(x, y, x2, y2)
  x2 = x2 or x
  y2 = y2 or y
  if x > x2 then x2, x = x, x2 end
  if y > y2 then y2, y = y, y2 end

  return x, y, x2, y2
end

--
-- aka size of object (width and height)
--
---@param x1 number, y1 number, x2 number, y2 number
---@return number width, number height
function M.get_width_n_height(x1, y1, x2, y2)
  local width, height = math.abs(x2 - x1) + 1, math.abs(y2 - y1) + 1
  return width, height
end

local preAbyte = string.byte('A', 1, 1)

function M.n2letters(n)
  local s = ''
  local l = 10
  while n > 0 and l > 0 do
    local i = (n - 1) % 26
    s = string.char(preAbyte + i) .. s
    n = math.floor((n - i) / 26)
    l = l - 1
  end
  return s
end

function M.letters2n(letters)
  local n, k = 0, 0
  for i = #letters, 1, -1 do
    n = n + ((string.byte(letters, i, i) - preAbyte) + 1) * math.pow(26, k)
    k = k + 1
  end
  return n
end

--
-- mechanics for specifying a set of unique indexes from a given range
-- and the index values are in descending order
-- this is used when deleting by indexes from the end to the beginning of the
-- "list"(table)
--
--  "1,2,3,4"     -->  {4,3,2,1}
--  "1-4"         -->  {4,3,2,1}
--  ">0", 4       -->  {4,3,2,1}
--  ">0, <5"      -->  {4,3,2,1}
--  "1,2,4-5"     -->  {5,4,2,1}
--  ">0,-3,-2",5  -->  {5,4,1}
--
---@param s string?
---@param max_idx number
function M.range2indexes(s, max_idx)
  local set = {}
  if type(s) ~= 'string' or type(max_idx) ~= 'number' or max_idx < 1 then
    return set
  end

  for token in (s .. ','):gmatch("([^,]*),\n?") do
    local min, max = token:match('^(%d+)-(%d+)$')
    if min and max then
      for i = max, min, -1 do
        set[i] = true
      end
    else
      local fc = token:sub(1, 1)
      if fc == '>' then -- ">5" means from 5 to max
        min = tonumber(token:sub(2))
        if min then
          for i = max_idx, min + 1, -1 do -- +1 > not >=
            set[i] = true
          end
        end
      elseif fc == '-' then --           -5  remove this number from the set
        local n = tonumber(token:sub(2))
        if n then
          set[n] = nil
        end
      elseif fc == '<' then --            >4,<8 same as 6-7
        local n = tonumber(token:sub(2))
        if n then
          for i = n, max_idx do set[i] = nil end
        end
      elseif tonumber(token) then
        set[tonumber(token)] = true
      end
    end
  end

  local t = {}
  for n, _ in pairs(set) do
    t[#t + 1] = n
  end

  table.sort(t, function(a, b) return (a or 0) > (b or 0) end) -- descending

  return t
end

local function tbl_indexof(list, v)
  for i, v0 in ipairs(list) do
    if v == v0 then
      return i
    end
  end
  return nil
end

---@param exclude table list of indexes to exclude
---@param elms table elements
---@param idxs table indexes
function M.exclude_elms(exclude, elms, idxs)
  local te, ti = {}, {}
  for i, index in ipairs(idxs) do
    if not tbl_indexof(exclude, index) then
      te[#te + 1] = elms[i]
      ti[#ti + 1] = index
    end
  end
  return te, ti
end

function M.validate_pos_xy(t, name)
  if type(t) ~= 'table' then
    name = name or '?'
    error(fmt('expected %s table{x:number, y:number} got: ', v2s(name), type(t)))
  end
  if type(t.x) ~= 'number' then
    error(fmt('expected %s.x got: %s', v2s(name or '?'), type(t.x)))
  end
  if type(t.y) ~= 'number' then
    error(fmt('expected %s.y got: %s', v2s(name or '?'), type(t.y)))
  end
  return true
end

-- get width and height of two points
---@param a table{x,y}
---@param b table{x,y}
---@return number width
---@return number height
function M.sizeof_points(a, b)
  local x1, x2 = M.smallest_first(a.x, b.x)
  local y1, y2 = M.smallest_first(a.y, b.y)

  return (x2 - x1 + 1), (y2 - y1 + 1) -- width, height
end

-- get width and height of two points
---@param a table{x,y}
---@param x number
---@param y number
---@return number width
---@return number height
function M.sizeof_points2(a, x, y)
  local x1, x2 = M.smallest_first(a.x, x)
  local y1, y2 = M.smallest_first(a.y, y)

  return (x2 - x1 + 1), (y2 - y1 + 1) -- width, height
end

function M.split(s, sep, t)
  assert(not t or type(t) == 'table')
  if sep == nil or sep == ' ' then sep = "%s" end

  t = t or {}
  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    table.insert(t, str)
  end

  return t
end

---@param t table
---@param tab string?
function M.slist2str(t, tab)
  tab = tab or ''
  local s = "{\n"
  for i, cname in ipairs(t) do
    s = s .. fmt("%s[%s] = '%s',\n", tab, i, v2s(cname))
  end
  return s .. (tab:sub(1, #tab - 2) or '') .. "}"
end

--
-- simple values goes in one-liner
--
---@param key string
---@param v any
---@param ident string?
function M.fmt_kvpair(key, v, ident)
  local kq, vq, vp
  if type(v) == 'string' then
    if string.find(v, "\n", 1, true) then
      vq, vp = '[==[', ']==]'
    else
      vq, vp = '\'', '\''
    end
  else
    vq, vp = '', ''
  end
  kq = '' -- todo [some.key]
  ident = ident or ''

  return fmt('%s%s%s%s=%s%s%s%s',
    kq, v2s(key), kq, ident, ident, vq, v2s(v), vp)
end

return M

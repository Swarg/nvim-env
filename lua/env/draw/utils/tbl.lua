-- 09-04-2024 @author Swarg
--

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


-- table as list
-- Note it will not works with non number indexes( i.g. for maps)
---@param list table
---@param v any
---@return number?
function M.tbl_indexof(list, v)
  if list then
    for i, v0 in ipairs(list) do
      if v == v0 then
        return i
      end
    end
  end
  return nil
end

-- find first key with given value
---@return any?
function M.tbl_find_key_of_value(t, v)
  for k, v0 in pairs(t) do
    if v == v0 then
      return k
    end
  end
  return nil
end

M.func_compare_descending = function(a, b)
  if a ~= nil and b ~= nil then
    return a > b
  else
    return false
  end
end -- descending


-- {1,2,3} --> {3,2,1}
function M.sort_num_list_descending(list)
  return table.sort(list, function(a, b) return (a or 0) > (b or 0) end) -- descending
end

---@param list table(list)
---@param value any
function M.tbl_remove_value(list, value)
  local idxs = {}
  for k, v in ipairs(list) do
    if v == value then
      idxs[#idxs + 1] = k
    end
  end
  M.sort_num_list_descending(idxs)
  for _, i in ipairs(idxs) do
    table.remove(list, i)
  end
  return list
end

---@return number
function M.tbl_keys_count(t)
  if type(t) == 'table' then
    local c = 0
    for _, _ in pairs(t) do
      c = c + 1
    end
    return c
  else
    return -1
  end
end

---@param map table
---@return table
function M.tbl_keys(map)
  local kt = {}
  for k, _ in pairs(map) do
    kt[#kt + 1] = k
  end
  return kt
end

--
-- {'a', 'b', 'c'}   ->   { b = true, c = true, a = true }
--
---@param list table
---@return table
function M.tbl_values_to_keymap(list)
  local t = {}
  for _, v in pairs(list) do
    t[v] = true
  end
  return t
end

---@param t table
---@param sort_func function
---@return table
function M.tbl_keys_sorted(t, sort_func)
  assert(type(t) == 'table', 'table exptedted')
  local kt = {}
  for k, _ in pairs(t) do
    kt[#kt + 1] = k
  end

  table.sort(kt, sort_func)

  return kt
end

---@param v number|table{number}
---@return table<number>
function M.to_indexes(v)
  if type(v) == 'table' and type(v[1]) == 'number' then
    return v
  elseif type(v) == 'number' then
    return { v }
  else
    error('Expected list of numbers or number got: ' .. type(v))
  end
end

---@param t table
---@return number? max
---@return number? min
function M.tbl_max_min_num_values(t)
  local min, max = nil, nil
  for _, v in pairs(t) do
    if not max or v > max then max = v end
    if not min or v < min then min = v end
  end
  return max, min
end

---@param s string
---@param sep string? def is space
---@param t table?
function M.split(s, sep, t)
  assert(not t or type(t) == 'table')
  if type(sep) ~= 'string' then sep = nil end -- case t moved to sep
  if sep == nil or sep == ' ' then
    sep = "%s"
  end
  t = t or {}
  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    table.insert(t, str)
  end
  return t
end

---@param ... table
function M.join_lists(...)
  local t = {}
  for i = 1, select('#', ...) do
    local l = select(i, ...)
    for _, v in ipairs(l) do
      t[#t + 1] = v
    end
  end
  return t
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

--
---@param line string
function M.luacode2table(line, i)
  local lt = type(line)
  if lt ~= 'string' then
    print(fmt('Expected line got: %s for lnum: %s', lt, v2s(i)))
    return false
  end
  local okf, func = pcall(loadstring, 'return ' .. line, 'chunkname_' .. v2s(i))
  if not okf or type(func) ~= 'function' then
    print('Cannot parse line ', i, 'as lua-table', v2s(func))
    return false
  end
  local ok_call, t = pcall(func)
  if not ok_call then
    print('Error on build lua-table', t)
    return false
  end
  return true, t
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

return M

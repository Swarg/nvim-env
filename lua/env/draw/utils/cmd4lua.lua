-- 22-05-2024 @author Swarg
-- Goal: complete

local log = require 'alogger'
local fs = require 'env.draw.utils.fs'

local M = {}

local E = {}

---@diagnostic disable-next-line: unused-local
local log_debug = log.debug

--
-- for complete. filter words which starts with arg
--
---@param arg string
---@param words table
---@return table
function M.filter_keys_starts(words, arg, sort)
  local t = {}
  if type(arg) == 'string' and type(words) == 'table' then
    local len = #arg
    for keyword, _ in pairs(words) do
      if len == 0 or (#keyword >= len and keyword:sub(1, len) == arg) then
        t[#t + 1] = keyword
      end
    end
    if sort then
      table.sort(t)
    end
  end
  return t
end

---@param line string
---@param pos number
---@param offset number?
function M.get_path(line, pos, offset)
  offset = offset or 0
  local t, wn, p = {}, 0, 1

  while p < pos do
    local i, j = string.find(line, '%s+', p, false)
    if not i or not j then break end
    local word = line:sub(p, i - 1)

    if wn >= offset then t[#t + 1] = word end
    wn = wn + 1
    p = j + 1
  end
  -- last word
  if p < pos then t[#t + 1] = line:sub(p, pos) end

  return t
end

---@param map table {cmd = {subcmd1 = 1, subcmd2 = 1}, cmd2 = {..}}
---@param path table{cmd subcmd1}
---@return table node
---@return number deep
function M.get_cmd_node(map, path)
  local prev = map
  local deep = 0
  for _, key in ipairs(path) do
    map = map[key]
    local is_branch = type(map) == 'table'
    -- log.debug('open cmdname:"%s" is_branch: %s', key, is_branch)
    if not is_branch then return prev, deep end

    prev = map
    deep = deep + 1
  end
  return map, deep
end

--
-- factory to build function for complete the arg-input to existed files
--
---@param cmdtree table
function M.build_complete(cmdtree, offset)
  ---@param arg string
  ---@param cmdline string
  ---@param cursor_pos number
  ---@diagnostic disable-next-line: unused-local
  return function(arg, cmdline, cursor_pos)
    local path = M.get_path(cmdline, cursor_pos, offset)
    local pathdeep = #(path or E)
    local subcmds, deep = M.get_cmd_node(cmdtree, path)
    -- log_debug('path:', vim.inspect(path))
    -- log_debug('subcmds:', vim.inspect(subcmds))

    local pc = cmdline:sub(cursor_pos, cursor_pos)
    if pc ~= ' ' and pathdeep - deep == 1 then pathdeep = deep end

    -- log_debug('deep:%s path:%s(%s) "%s"', deep, pathdeep, #path, pc)

    -- if type(subcmds) == 'table' and deep + 1 >= pathdeep then
    if type(subcmds) == 'table' and deep == pathdeep then
      local t = M.filter_keys_starts(subcmds, arg, true)
      if next(t) then
        -- log_debug('found words: %s', t)
        return t
      end
    end

    -- files
    if arg and #arg > 1 then
      -- local fc = arg:sub(1, 1)
      -- filename
      -- if fc == '/' or fc == '~' or fc == '\\' or fc == '.' then
      return fs.find_complete_variants(arg)
      -- end
    end
    return {}
  end
end

return M

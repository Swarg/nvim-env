-- 09-04-2024 @author Swarg
-- Goal: utils to work with nvim buffer

local M = {}

local u8 = require("env.util.utf8")

local conf = require('env.draw.settings')

local WORK_WITH_UTF8 = conf.WORK_WITH_UTF8
local SYNTAX_BLOCK_NAME = conf.SYNTAX_BLOCK_NAME
local FIND_SYNTAX_BLOCK_LN_LIMIT = conf.FIND_SYNTAX_BLOCK_LN_LIMIT

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
-- local instanceof = class.instanceof
-- local log_debug = log.debug
-- local log_is_debug = log.is_debug
-- local indexof = utbl.tbl_indexof

local log_debug = function(...) end


---@param bufnr number?
---@return number
function M.get_actual_bufnr(bufnr)
  if not bufnr or bufnr == 0 then
    return vim.api.nvim_get_current_buf()
  end
  return bufnr
end

---@param bufnr number
---@param vn string
function M.get_buf_var(bufnr, vn)
  local ok, value = pcall(vim.api.nvim_buf_get_var, bufnr, vn)
  if not ok then
    return false, ('Not found "' .. v2s(vn) .. '" in bufnr:' .. v2s(bufnr))
  end
  return true, value
end

function M.set_buf_var(bufnr, vn, value)
  local ok, err = pcall(vim.api.nvim_buf_set_var, bufnr, vn, value)
  if not ok then
    error(err)
  end
  return ok
end

function M.is_visual_mode()
  local m = vim.api.nvim_get_mode().mode
  return m == 'v' or m == 'V' or m == "\x16"
end

---@param winnr number
---@param lnum number
---@param col number
function M.set_cursor_pos(winnr, lnum, col)
  winnr = winnr or 0
  local lnum0 = math.max(1, lnum)
  local col0 = math.max(0, col)

  local ok, err = pcall(vim.api.nvim_win_set_cursor, 0, { lnum0, col0 })
  if not ok then
    error(fmt('lnum:%s col:%s error:%s', v2s(lnum), v2s(col), v2s(err)))
  end
end

---@param bufnr number
---@param lnum number
function M.get_raw_line(bufnr, lnum)
  return (vim.api.nvim_buf_get_lines(bufnr, lnum, lnum + 1, false) or E)[1]
end

function M.is_buf_modifiable(bufnr)
  return vim.api.nvim_buf_get_option(bufnr, 'modifiable')
end

--------------------------------------------------------------------------------

---@param message string
---@param default string?
---@param completion function?
function M.ask_value(message, default, completion)
  local output = default
  message = message or "Value"
  vim.ui.input({ prompt = message, default = default, completion = completion },
    function(input)
      output = input
    end)
  return output
end

function M.is_yes(s)
  if s and s ~= "" then
    s = string.lower(s)
    return (s == "y" or s == "yes" or s == "+" or s == "1") == true
  end
  return false
end

-- Ask the user for confirmation before performing a given action
-- Ask comfirmation
---@param message string
---@param callback function? - callback with action
---@param userdata any      - data for callback
---@return any -- output from callback
function M.ask_confirm_and_do(message, callback, userdata)
  local output = nil
  message = message or "Do you agree"
  vim.ui.input({ prompt = message .. ' ? (y/N) ' },
    function(input)
      if M.is_yes(input) then
        output = true
        if type(callback) == 'function' then
          output = callback(userdata) or true
        end
      end
    end)
  return output
end

--
---@param message string
---@param default string?
---@param completion string?
function M.select_interactively(message, default, completion)
  local new_input = nil

  vim.ui.input(
    { prompt = message, default = default or '', completion = completion },
    function(input)
      new_input = input
    end
  )
  return new_input
end

--------------------------------------------------------------------------------

--
-- find by buffname in all buffers
-- [Note] Each Buffer have unique name. You cannot create new one with
-- already existed buffer name
--
---@param bname string
---@return number
function M.get_buf_with_name(bname)
  if bname and bname ~= "" then
    local list = vim.api.nvim_list_bufs()
    for _, bn in ipairs(list) do
      local loaded = vim.api.nvim_buf_is_loaded(bn)
      if loaded then
        local ok, name = pcall(vim.api.nvim_buf_get_name, bn)
        if ok and name and name == bname then
          return bn
        end
      end
    end
  end
  return -1
end

--------------------------------------------------------------------------------


--
-- starts from zero not from 1
--
---@param lnum_offset number? (top)  offset by lnum
---@param col_offset number?  (left) offset by col
---@return number bufnr
---@return number row
---@return number col
---@return number row2 used in visual mode
---@return number col2 in normal mode always equals row and col
function M.get_editor_coords(lnum_offset, col_offset)
  local api = vim.api
  local bufnr = api.nvim_get_current_buf()
  lnum_offset = lnum_offset or 0
  col_offset = col_offset or 0
  local nvim_buf_get_lines = api.nvim_buf_get_lines

  local c_row, c_col, c_row2, c_col2 -- cursor index of raw byte not utf8-char

  local vm = M.is_visual_mode()
  if vm then
    -- gets the start of the current selection and
    -- the cursor position respectfully
    local p1, p2 = vim.fn.getpos('.'), vim.fn.getpos('v')
    if p1 then c_row, c_col = p1[2], p1[3] end
    if p2 then c_row2, c_col2 = p2[2], p2[3] end
    c_col, c_col2 = c_col - 1, c_col2 - 1
  else
    local cursor_pos = api.nvim_win_get_cursor(0)
    col_offset = col_offset or 0

    if cursor_pos then
      c_row, c_col = cursor_pos[1], cursor_pos[2]
    end
  end
  -- log_debug('is-visual-mode:%s %s:%s, %s:%s', vm, c_row, c_col, c_row2, c_col2)

  local row, col = 0, 0

  if c_row and c_col then
    if WORK_WITH_UTF8 and c_col > 0 then
      local line = (nvim_buf_get_lines(bufnr, c_row - 1, c_row, true) or E)[1]
      c_col = u8.letter_pos(line, c_col)
    end

    row = math.max(0, c_row - 1 + (lnum_offset or 0))
    col = math.max(0, c_col + col_offset)
  end

  local row2, col2 = row, col -- row2 col2 used in visual mode for selection

  if c_row2 and c_col2 then
    if WORK_WITH_UTF8 and c_col2 > 0 then
      local line = (nvim_buf_get_lines(bufnr, c_row2 - 1, c_row2, true) or E)[1]
      c_col2 = u8.letter_pos(line, c_col2)
    end
    row2 = math.max(0, c_row2 - 1)
    col2 = math.max(0, c_col2)
  end

  -- log_debug('ret bufnr:%s [%s:%s, %s:%s]', bufnr, row, col, row2, col2)
  return bufnr, row, col, row2, col2
end

--------------------------------------------------------------------------------

local nvim_replace_termcodes = ((vim or E).api or E).nvim_replace_termcodes or
    -- for testing outside nvim
    ---@diagnostic disable-next-line: unused-local
    function(key, f1, f2, f3) --[[stub]] end

local NVIM_KEY_ESC = nvim_replace_termcodes('<esc>', true, false, true)    -- "\27"
local NVIM_KEY_CTRL_V = nvim_replace_termcodes('<c-v>', true, false, true) --"\22"
-- <c-a> -> "\1",   <c-z> -> "\26"
-- <c-a-a>  -> "<80><fc>\b\1"

local NVIM_KEY_H = nvim_replace_termcodes('h', true, false, true)
local NVIM_KEY_L = nvim_replace_termcodes('l', true, false, true)
local NVIM_KEY_J = nvim_replace_termcodes('j', true, false, true)
local NVIM_KEY_K = nvim_replace_termcodes('k', true, false, true)

local DIRECTIONS_KEYS = {
  U = NVIM_KEY_K, D = NVIM_KEY_J, L = NVIM_KEY_H, R = NVIM_KEY_L,
}

-- esc ??
function M.close_visual_mode()
  if M.is_visual_mode() then
    vim.api.nvim_feedkeys(NVIM_KEY_ESC, 'x', false)
  end
end

function M.open_visual_mode()
  if not M.is_visual_mode() then
    vim.api.nvim_feedkeys(NVIM_KEY_CTRL_V, 'n', false) --?
  end
end

--
-- move cursor in the buffer via hjkl keys in
-- UseCase - select range in the buffer in visual mode
-- set_visual + set_cursor_pos - do not work, the required area is not selected
--
-- these functions change the position of the cursor on the screen but the
-- selection does not occur:
-- vim.api.nvim_win_set_cursor(0, { lnum, col })
-- vim.fn.setpos('.', { box.bufnr, lnum, col, 0 })
--
---@return number
function M.nvim_move_cursor(...)
  -- mode = mode or 'n' -- n|v|x ?
  local cnt = select('#', ...)
  assert(cnt > 1 and cnt % 2 == 0,
    'expected at least one pair of direction and steps got: ' .. v2s(cnt))
  local nvim_feedkeys = vim.api.nvim_feedkeys
  local nvim_win_get_cursor = vim.api.nvim_win_get_cursor
  local utf8_slen = u8.utf8_slen
  local letter_pos = u8.letter_pos
  local i, moves = 1, 0

  local function get_buf_line(winnr)
    local cpos = nvim_win_get_cursor(winnr or 0)
    local lnum, col = (cpos or E)[1], (cpos or E)[2]
    local line = M.get_raw_line(0, lnum - 1) or ''
    return line, lnum, col
  end

  while i <= cnt do
    local direction = select(i, ...)
    local offset = select(i + 1, ...)
    i = i + 2
    local key = assert(DIRECTIONS_KEYS[direction],
      'expected direction: [U|D|L|R] got: ' .. v2s(direction))
    assert(type(offset) == 'number',
      'expected number offset got: ' .. type(offset))

    -- check possible transitions to other lines and block them
    if direction == 'L' then
      local line, _, col = get_buf_line(0)
      local ok, rems = pcall(letter_pos, line, col + 1)
      if ok and offset > rems then offset = rems - 1 end

      -- fixes excessive jumping when dragging unicode characters
      local ok2, u8len = pcall(u8.get_utf8char_len_at_byte, line, col)
      if not ok2 or u8len > 1 then
        offset = offset - 1
      end
      --
    elseif direction == 'R' then
      local line, _, col = get_buf_line(0)
      local ok, rems = pcall(utf8_slen, line, col + 1)
      if ok and offset > rems then offset = rems - 1 end
    end

    for _ = 1, offset do
      nvim_feedkeys(key, 'n', false)
    end
    moves = moves + 1
  end

  return moves
end

--------------------------------------------------------------------------------
--                            KeyBindings
--------------------------------------------------------------------------------

--
-- create coresponding mappings for given keyboard layout
-- to allow registeration of the same actions on the keys of a different
-- keyboard layout
--
-- Example:
--   here the '%' symbol denotes the local representation of the same key in a
--   different keyboard layout:
--
--   "x"        -> "%"
--   "<c-x>"    -> "<c-%>"
--   "<c-a-x>"  -> "<c-a-%>"
--   "<leader>xyz"  -> "<leader>%%%"
--
--   not supported:
--   "<c-a-xx>" -> nil
--   "<-x>"     -> nil
--   "<x>"      -> nil
--
---@param key string
---@param layout table
function M.applay_keymap_layout(key, layout)
  local letter = layout[key]
  if not letter then
    local en = string.match(key, '<.+%-(%w)>')
    letter = layout[en or false]
    if letter then
      return key:sub(1, -3) .. letter .. '>'
    end
    -- <leader>xyz
    local letters = string.match(key, '^<leader>([%w{}%[%]><~%?:;,%.`/]+)$')

    if letters then
      local s = '<leader>'
      for i = 1, #letters do
        local ch = letters:sub(i, i)
        letter = layout[ch]
        if not letter then
          return nil
        end
        s = s .. letter
      end
      return s
    end
  end
  return letter
end

--
-- register given action to given key and for key in given layout
--
-- vim.api.nvim_buf_set_keymap
function M.register_buf_key(bufn, mode, key, action_opts, layout)
  local set_keymap = vim.api.nvim_buf_set_keymap
  layout = layout or u8.layout_lattin2ru

  --                key  'q'                     M.do_close
  set_keymap(bufn, mode, key, '', action_opts)

  local lkey = M.applay_keymap_layout(key, layout)
  if lkey then set_keymap(bufn, mode, lkey, '', action_opts) end
end

---@param out table
function M.mk_register_buf_key(out)
  assert(type(out) == 'table', 'out')

  -- register_buf_key
  ---@param bufn number
  ---@param mode string
  ---@param key string
  ---@param action_opts table
  ---@param layout table?
  return function(bufn, mode, key, action_opts, layout)
    local set_keymap = vim.api.nvim_buf_set_keymap
    layout = layout or u8.layout_lattin2ru

    --                key  'q'                     M.do_close
    set_keymap(bufn, mode, key, '', action_opts)

    local lkey = M.applay_keymap_layout(key, layout)
    if lkey then set_keymap(bufn, mode, lkey, '', action_opts) end

    out[#out + 1] = {
      desc = (action_opts or E).desc,
      mode = mode,
      key = key,
    }
  end
end

---@param mode string (n|v)
---@param keys table
---@param action_opts table{callback=function}
local function register_keys(bufn, mode, keys, action_opts, action_name)
  -- local set_keymap = vim.api.nvim_buf_set_keymap
  if type(keys) ~= 'table' then
    error(fmt('action: "%s" expected keys-table for mode:%s  got: %s',
      v2s(action_name), v2s(mode), type(keys)))
  end
  -- type(action_opts) ~= 'table' or type(action_opts.callback) ~= 'function')

  local layout = u8.layout_lattin2ru

  for _, key in pairs(keys) do
    M.register_buf_key(bufn, mode, key, action_opts, layout)
  end
end

--
--
---@param keybindings table{active={}, ignored={}, fn2name={}, name2fn={}}
---@param bufnr number
function M.bind_keybindings(bufnr, keybindings)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(keybindings) == 'table', 'keybindings')
  assert(type(keybindings.active) == 'table', 'keybindings.active')
  assert(type(keybindings.ignored) == 'table', 'keybindings.ignored')
  assert(type(keybindings.fn2name) == 'table', 'keybindings.fn2name')

  local map = vim.api.nvim_buf_set_keymap
  local function mk_opts(callback)
    return {
      desc = nil,
      silent = true,
      noremap = true,
      nowait = true,
      callback = callback
    }
  end

  local fn2name = keybindings.fn2name or E

  for action, keys in pairs(keybindings.active) do
    assert(type(action) == 'function', 'expected action is function')
    assert(type(keys) == 'table', 'expected keys is a table of strings')

    local actname, action_opts = fn2name[action], mk_opts(action)
    action_opts.desc = action_opts.desc or actname

    if keys.n then register_keys(bufnr, 'n', keys.n, action_opts, actname) end
    if keys.v then register_keys(bufnr, 'v', keys.v, action_opts, actname) end
    if keys.nv then
      register_keys(bufnr, 'n', keys.nv, action_opts, actname)
      register_keys(bufnr, 'v', keys.nv, action_opts, actname)
    end
  end

  local do_nothing_opts = mk_opts(M.do_nothing)

  for _, letter in pairs(keybindings.ignored) do
    map(bufnr, 'n', letter, '', do_nothing_opts)
    map(bufnr, 'v', letter, '', do_nothing_opts)
    map(bufnr, 'x', letter, '', do_nothing_opts)

    letter = u8.layout_lattin2ru[letter]
    if letter then
      map(bufnr, 'n', letter, '', do_nothing_opts)
      map(bufnr, 'v', letter, '', do_nothing_opts)
      map(bufnr, 'x', letter, '', do_nothing_opts)
    end
  end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--
-- Find, starting from the current cursor position, a syntax block in which
-- the canvas can be placed.
-- do not treat the block below the cursor as a block for placement
--
-- lnum starts from 1(nvim-api)
-- Note: box.lnum starts from 0 here used coords from api.nvim_win_get_cursor
-- (so use box.lnum +1)
--
--   ```xscheme             << lnum_start
--   canvas (cursor here)
--   ```                    << lnum_end
--
---@param bufnr number
---@param lnum number (starts from 1 not from 0 like in box.lnum) use +1
---@param limit number? default is 100 (up and down from current cursor pos)
---@param fsyntax string?
---@return number? start lnum
---@return number? end lnum
---@return string? syntax
function M.nvim_find_syntax_block_range(bufnr, lnum, limit, fsyntax)
  log_debug("nvim_find_syntax_block_range bufnr:%s lnum:%s limit:%s", bufnr, lnum, limit)
  local ls, le, found, syntax = lnum, lnum, false, nil
  limit = limit or FIND_SYNTAX_BLOCK_LN_LIMIT
  fsyntax = fsyntax or SYNTAX_BLOCK_NAME

  local get_lines = vim.api.nvim_buf_get_lines
  local max = vim.api.nvim_buf_line_count(bufnr)
  local min = math.max(0, lnum - limit)

  if lnum + limit < max then
    max = lnum + limit
  end

  -- first try to find the beginning of the block in the line above the lnum
  local direction = 1 -- up
  local x = 100000    --just in case - protection against infinite loop

  while ls > min and ls < max and x > 0 do
    x = x - 1
    local lines = get_lines(bufnr, ls - 1, ls, true)
    local line = (lines or E)[1]
    if not line then break end
    if line == '```' and ls < lnum then
      log_debug('found end of syntax block at lnum:', ls)
      if direction == -1 then
        log_debug('but before not found expected its begining')
        return nil, nil
      end
      ls = lnum - direction
      log_debug('change the direction of the search and go down from lnum:', ls)
      -- to find the beginning of the block')
      direction = -1 -- down
    end

    syntax = line:match('^```([^%s]+)') -- SYNTAX_BLOCK_START
    if syntax then
      if syntax == fsyntax then
        found = true
      else
        log_debug('found another syntax block:|%s| at lnum: %s', syntax, ls)
      end
      break -- found
    end
    ls = ls - direction
  end

  if not found then return nil, nil end

  found = false
  if le == ls then le = le + 1 end

  while le <= max do
    local lines = get_lines(bufnr, le - 1, le, false)
    local line = (lines or E)[1]
    if not line then break end
    if line:match('^```$') then -- SYNTAX_BLOCK_END
      found = true
      break                     -- found
    end
    le = le + 1
  end

  log_debug("nvim_find_syntax_block_range ret ls:%s le:%s %s %s", ls, le, syntax, found)
  return ls, (found and le or nil), syntax
end

return M

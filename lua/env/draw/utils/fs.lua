-- 13-04-2024 @author Swarg
local M = {}
local E = {}
--

local function get_uv()
  local uv = (vim or E).loop
  if not uv then
    local ok, modt = pcall(require, "luv")
    if ok and type(modt) == 'table' then
      uv = modt
    end
  end
  return uv
end

local uv = get_uv()

---@param file string?
---@return boolean
function M.file_exists(file)
  if type(file) == 'string' and file ~= '' then
    local f = io.open(file, "rb")
    if f then f:close() end
    return f ~= nil
  end
  return false
end

---@param fn string
---@param lines table?{string}
function M.file_write_lines(fn, lines)
  if type(fn) == 'string' and fn ~= '' and type(lines) == 'table' then
    local content = table.concat(lines, "\n")
    local f = io.open(fn, "wb")
    if not f then
      return false
    end
    f:write(content)
    f:close()
    return true
  end

  return false
end

---@param fn string
---@param str string
---@param mode string?
function M.file_write_content(fn, str, mode)
  mode = mode or 'wb' -- w, a

  if type(fn) == 'string' and fn ~= '' and type(str) == 'string' then
    local f = io.open(fn, mode)
    if not f then
      return false
    end
    f:write(str)
    f:close()
    return true
  end

  return false
end

function M.file_read_all(path)
  local file = io.open(path, "rb")  -- r read mode and b binary mode
  if file then
    local content = file:read("*a") -- *a or *all reads the whole file
    file:close()
    return content
  end
  return nil
end

-- return parent dir path
-- /path/to/subdir -> /path/to/
-- /path/to/subdir/ -> /path/to/
---@param path string
---@return string?
--
function M.get_parent_dirpath(path)
  if type(path) == "string" then
    while true do
      local last = path:sub(-1, -1)
      if last == '/' or last == '\\' then
        path = path:sub(1, -2)
      else
        break
      end
    end
    return path:match("(.*[/\\])")
  end
end

function M.dir_exists(path)
  if path then
    local stat = uv.fs_stat(path)
    local exists = (stat and stat.type == "directory") == true
    return exists
  end

  return false
end

---@return boolean
---@return table|string
function M.list_of_files(dir, t)
  local handle, errmsg = uv.fs_scandir(dir)

  if type(handle) == "string" then
    return false, handle
  elseif not handle then
    return false, errmsg
  end

  t = t or {}
  while true do
    local name, _ = uv.fs_scandir_next(handle)
    if not name then
      break
    end
    t[#t + 1] = name
  end

  return true, t
end

--
---@param str string
---@param start string ends
function M.starts_with(str, start)
  return str ~= nil and start ~= nil and str:sub(1, #start) == start
end

--
-- "/tmp/file_" TAB -> {"/tmp/file_1", "/tmp/file_2", ... }
-- "file_" TAB      -> {"./file_1", "./file_2", ... }
--
---@param arg string
---@return table
function M.find_complete_variants(arg)
  local t = {}
  if arg then
    local dir = M.get_parent_dirpath(arg)
    local fns = arg:sub(#(dir or '') + 1, #arg)
    dir = dir or './'

    if M.dir_exists(dir) then
      local ok, ls = M.list_of_files(dir)
      if ok and type(ls) == 'table' then
        for _, fn in ipairs(ls) do
          if M.starts_with(fn, fns) then
            t[#t + 1] = dir .. fn
          end
        end
      end
    end
  end

  return t
end

return M

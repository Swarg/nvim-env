-- 30-04-2024 @author Swarg
-- Goal:
-- to quick change elemnt style via cli-like UI on "do_edit" at CanvasEditor
-- select the desired Unicode character via cli-like UI
-- for rectangle, line, point

local u8 = require("env.util.utf8")
local utbl = require("env.draw.utils.tbl")
local M = {}

local E, fmt, v2s = {}, string.format, tostring

-- for arrow and points
local PART_NAMES_ORDER = {
  'left',
  'top',    -- 'up',
  'right',
  'bottom', --'down',
  'top-left',
  'top-right',
  'bottom-right',
  'bottom-left',
  'horizontal',
  'vertical',
  'cross',
  'cross-diagonal',
  'horizontal-double',
  'horizontal-single',
  'vertical-double',
  'vertical-single',
  'right-single',
  'left-single',
  'right-double',
  'left-double',
  'diagonal-right', -- 'upper-right-to-lower-left',
  'diagonal-left',  --'upper-left-to-lower-right',
}

local PART_NAMES_MAP = utbl.tbl_values_to_keymap(PART_NAMES_ORDER)

-- for point with words_in_main == 2
-- main and parts double mappings
local SYMBOLS_W2 = {
  __short_main_names = {
    ac = 'arc-corner', -- for light line
    bo = 'block-one',
    cf = 'crows-foot',
    cF = 'crows-foot2',
    vs = 'vertical-single',
    vd = 'vertical-double',
    hs = 'horizontal-single',
    hd = 'horizontal-double',
    us = 'up-single',
    ud = 'up-double',
    ds = 'down-single',
    dd = 'down-double',
    ll = 'line-light',
    lh = 'line-heavy',
    ld = 'line-double',
  },
  __short_parts_names = {
    l = 'left',
    r = 'right',
    t = 'top',
    b = 'bottom',
    h = 'horizontal',
    v = 'vertical',
    tl = 'top-left',
    tr = 'top-right',
    bl = 'bottom-left',
    br = 'bottom-right',
    c = 'cross',
    cd = 'cross-diagonal',
    hd = 'horizontal-double',
    hs = 'horizontal-single',
    vd = 'vertical-double',
    vs = 'vertical-single',
    rs = 'right-single',
    ls = 'left-single',
    rd = 'right-double',
    ld = 'left-double',
    dr = 'diagonal-right',
    dl = 'diagonal-left',
  },

  ['crows-foot'] = {
    left = '⪫', -- 2AAA
    right = '⪪', -- 2AAB
    bottom = '⩚', -- 2A5A
    top = '⩛', -- 2A5B
  },
  ['crows-foot2'] = {
    left = '⚞',
    right = '⚟',
    bottom = '₼', -- 	20BC	 	MANAT SIGN     ⋔
    top = 'Ѱ' -- 0470	CAPITAL LETTER PSI
  },
  --
  ['block-one'] = {
    top = '▔', -- 2581	LOWER ONE EIGHTH BLOCK (as undescore)
    bottom = '▁',
    left = '▏',
    right = '▕',
  },

  ['line-light'] = {
    horizontal = '─',
    vertical = '│',
    ['top-left'] = '┌',
    ['top-right'] = '┐',
    ['bottom-right'] = '┘',
    ['bottom-left'] = '└',
    cross = '┼',
    ['cross-diagonal'] = '╳',
    ['diagonal-right'] = '╱', -- dr roll direction
    ['diagonal-left'] = '╲', -- dl
  },
  ['arc-corner'] = {
    ['top-left'] = '╭',
    ['top-right'] = '╮',
    ['bottom-right'] = '╯',
    ['bottom-left'] = '╰',
  },
  ['line-heavy'] = {
    horizontal = '━',
    vertical = '┃',
    cross = '╋',
    ['top-left'] = '┏',
    ['top-right'] = '┓',
    ['bottom-right'] = '┛',
    ['bottom-left'] = '┗',
  },
  ['line-double'] = {
    horizontal = '═',
    vertical = '║',
    cross = '╬',
    ['top-left'] = '╔',
    ['top-right'] = '╗',
    ['bottom-right'] = '╝',
    ['bottom-left'] = '╚',
  },
  --
  ['vertical-single'] = {
    ['horizontal-double'] = '╪',
    ['right-double'] = '╞', -- 255E
    ['left-double'] = '╡', -- 2561	VERTICAL SINGLE AND LEFT DOUBLE
  },
  ['vertical-double'] = {
    ['horizontal-double'] = '╬',
    ['horizontal-single'] = '╫',
    ['left-single'] = '╢',
    ['right-single'] = '╟',
    ['right'] = '╣',
    ['left'] = '╠',
  },
  ['horizontal-single'] = {
    vertical = '┼',
    ['vertical-double'] = '╫',
  },
  ['horizontal-double'] = {
    vertical = '╬',
    ['vertical-single'] = '╪',
  },
  ['up-single'] = {
    left = '┘',
    right = '└',
    ['left-double'] = '╛', -- UP SINGLE AND LEFT DOUBLE
    ['horizontal-double'] = '╧',
    horizontal = '┴',
  },
  ['up-double'] = {
    left = '╝',
    right = '╚',
    ['left-single'] = '╙',
    ['right-single'] = '╜',
    ['horizontal-single'] = '╨',
    horizontal = '╩', --	DOUBLE UP AND HORIZONTAL
  },
  ['down-single'] = {
    left = '┐',
    right = '┌',
    ['right-double'] = '╒',
    ['left-double'] = '╕',
    ['horizontal-double'] = '╤',
    horizontal = '┬',
  },
  ['down-double'] = {
    right = '╔',
    left = '╗',
    ['right-single'] = '╓',
    ['left-single'] = '╖',
    ['horizontal-single'] = '╥',
    ['horizontal'] = '╦',
  },
}

-- for words_in_main == 1
local SYMBOLS_W1 = {
  cross = {
    light = '┼',
    heavy = '╋',
    double = '╬',
    diagonal = '╳',
    matrix = '⊹', -- HERMITIAN CONJUGATE MATRIX
    doted = '⁜', -- 205C	 	DOTTED CROSS
  },
  circle = {
    l2 = '🞈'
  },
  diagonal = {
    single = '╱', -- right
    single2 = '╲', -- left
    elipsis = '⋰',
    elipsis2 = '⋱',
    double = '⫽', -- todo find double2
  },

  block = {
    full = '█',
    ['light-shade'] = '░',
    ['medium-shade'] = '▒',
    ['dark-shade'] = '▓',

    ['left-half'] = '▌',
    ['right-half'] = '▐',
    ['up-half'] = '▀', -- 2580 UPPER HALF BLOCK
    ['down-half'] = '▄', --  2584	LOWER HALF BLOCK
  },
  __short_main_names = {
    c = 'cross',
    o = 'circle',
    d = 'diagonal',
    b = 'block',
  },
  __short_parts_names = {
    s = 'single',
    S = 'single2',
    e = 'elipsis',
    E = 'elipsis2',
    d = 'double',
    m = 'matrix',
    x = 'diagonal',
    h = 'heavy',
    l = 'light',
    ['.'] = 'doted',
    f = 'full',
    ls = 'light-shade',
    ms = 'medium-shade',
    ds = 'dark-shade',
    lh = 'left-half',
    rh = 'right-half',
    uh = 'up-half',
    dh = 'down-half',
    ['2'] = 'l2'
  }
}

local LINES_ORDERED_PART_NAMES = {
  'horizontal',
  'vertical',
  'cross',
  'top-left',
  'top-right',
  'bottom-left',
  'bottom-right',
}

-- main and parts where parts maps to char position  words_in_main == 1
local LINES_PARTS = {
  ascii = '-|+++++',
  light = '─│┼┌┐┘└',
  light_arc = '─│┼╭╮╯╰',
  heavy = '━┃╋┏┓┛┗',
  double = '═║╬╔╗╝╚',
  triple = '┄┆     ',
  quadruple = '┈┊     ',
  heavy_triple = '┅┇     ',
  heavy_quadruple = '┉┋     ',
  elipsis = '⋯⋮ ⋰⋱⋰⋱', -- 22F0	ELLIPSIS UP RIGHT DIAGONAL, VERTICAL
  block_out8 = '-|+▔▁▏▕', -- 2581	LOWER ONE EIGHTH BLOCK (as undescore) 1/8
  block_out = '▀▋█▛▜▟▙▄▐', -- 1/2
  block_in = '▄▐█▗▖▘▝▀▋',

  __name_to_pos = {
    horizontal = 1,
    vertical = 2,
    cross = 3,
    ['top-left'] = 4,
    ['top-right'] = 5,
    ['bottom-right'] = 6,
    ['bottom-left'] = 7,
  },

  __short_main_names = {
    A = 'ascii',
    l = 'light',
    L = 'light_arc',
    h = 'heavy',
    d = 'double',
    e = 'elipsis',
    t = 'triple',
    q = 'quadruple',
    T = 'heavy_triple',
    Q = 'heavy_quadruple',
    b = 'block_out',
    B = 'block_in',
    ['8'] = 'block_out8',
  },

  __short_parts_names = {
    h = 'horizontal',
    v = 'vertical',
    c = 'cross',
    tl = 'top-left',
    tr = 'top-right',
    bl = 'bottom-left',
    br = 'bottom-right',
  }
}

-- main + parts  words_in_main == 1
local ARROWS = {
  -- Sans-serif
  ['wide0'] = '🡐🡑🡒🡓🡔🡕🡖🡗🡘🡙', -- 1F850 1F859
  -- Wide Head Arrows
  ['wide1'] = '🡠🡡🡢🡣🡤🡥🡦🡧', -- 1F860
  ['wide2'] = '🡨🡩🡪🡫🡬🡭🡮🡯', -- 1F868
  ['wide3'] = '🡰🡱🡲🡳🡴🡵🡶🡷', -- 1F870
  ['wide4'] = '🡸🡹🡺🡻🡼🡽🡾🡿', -- 1F878
  ['wide5'] = '🢀🢁🢂🢃🢄🢅🢆🢇', -- 1F880
  -- ⯸ ⟷ ⤪        ⯅
  -- -   ⤫  ⬜ ⯇─⯈|
  -- Heavy Arrows
  ['heavy'] = '🡄🡅🡆🡇',
  ['heavy2'] = '🠴🠵🠶🠷',
  ['triangle'] = '⯇⯅⯈⯆',
  ['acute'] = '🠜🠝🠞🠟', -- 1F81C - 1F81F   acute  angle
  ['obtuse'] = '🠈🠉🠊🠋', -- 1F808 - 1F80B   obtuse angle
  ['circle'] = '⮈⮉⮊⮋',
  -- arrow_oo = '⯺⯼⯻⯹', --	2BF9,2BFA,
  ['crowsfoot'] = '⪫⩛⪪⩚',
  --
  __name_to_pos = {
    left = 1,
    up = 2,
    top = 2,
    right = 3,
    down = 4,
    bottom = 4,
    ['top-left'] = 5,
    ['top-right'] = 6,
    ['bottom-right'] = 7,
    ['bottom-left'] = 8,
    ['horizontal'] = 9, -- only for arrow_0
    ['vertical'] = 10,
  },

  __short_main_names = {
    ['0'] = 'wide0',
    ['1'] = 'wide1',
    ['2'] = 'wide2',
    ['3'] = 'wide3',
    ['4'] = 'wide4',
    ['5'] = 'wide5',
    h = 'heavy',
    H = 'heavy2',
    t = 'triangle',
    a = 'acute',
    o = 'obtuse',
    c = 'circle',
    f = 'crowsfoot',
  },
  __short_parts_names = {
    l = 'left',
    r = 'right',
    t = 'top',
    b = 'bottom',
    u = 'up',
    d = 'down',
    tl = 'top-left',
    tr = 'top-right',
    bl = 'bottom-left',
    br = 'bottom-right',
    v = 'vertical',
    h = 'horizontal',
  }
}

-- main no parts
local RECT_STYLES = {
  ascii = { '-', '|', '++++' },
  light = { '─', '│', '┌┐┘└' },
  light_arc = { '─', '│', '╭╮╯╰' },
  heavy = { '━', '┃', '┏┓┛┗' },
  double = { '═', '║', '╔╗╝╚' },
  --
  block_out = { '▀', '▋', '▛▜▟▙', '▄', '▐' }, -- top left corners bottom right
  block_in = { '▄', '▐', '▗▖▘▝', '▀', '▋' },
  --
  __short_names = {
    a = 'ascii',
    l = 'light',
    L = 'light_arc',
    h = 'heavy',
    d = 'double',
    b = 'block_out',
    B = 'block_in',
  }
}



--------------------------------------------------------------------------------

local function find_short0(t, key)
  for shortkey, fullkey in pairs(t) do
    if fullkey == key then
      return tostring(shortkey)
    end
  end
  return ''
end

---@param t table
---@param key string
---@param sep string?
local function find_short(t, key, sep)
  sep = sep or '|'
  for shortkey, fullkey in pairs(t) do
    if fullkey == key then
      return string.format('%2s%s', tostring(shortkey), sep)
    end
  end
  return ''
end

local function sorted_keys(map)
  local t = {}
  for key, _ in pairs(map) do
    if key:sub(1, 2) ~= '__' then
      t[#t + 1] = key
    end
  end
  table.sort(t)
  return t
end

local function get_readable_keys(map)
  local s = ''
  for k, _ in pairs(map) do
    if k:sub(1, 2) ~= '__' then
      s = s .. v2s(k) .. ' '
    end
  end
  return s
end

-- __name_to_pos
---@param map table
---@param parts_names_order table?
local function build_parts0(map, parts_names_order)
  local parts = ''
  local part_keys = sorted_keys(map.__name_to_pos)

  if type(parts_names_order) == 'table' then
    -- NOTE:the caller is responsible for ensuring that
    -- all possible names in the map are described in parts_names_order
    for _, key in pairs(parts_names_order) do
      local short_part_key = find_short0(map.__short_parts_names, key)
      if short_part_key and short_part_key ~= '' then
        parts = parts .. fmt("  %2s|%s\n", short_part_key, key)
      end
    end
  else
    for _, key in pairs(part_keys) do
      local shortkey = find_short(map.__short_parts_names, key)
      parts = parts .. '  ' .. shortkey .. key .. "\n"
    end
  end

  return parts
end

---@param map table
---@param parts_names table?
---@param width number?
local function build_help_main_n_part(map, width, parts_names)
  local s = ''
  local parts = build_parts0(map, parts_names)

  local main_keys = sorted_keys(map)
  width = width or 12

  for _, key in pairs(main_keys) do
    local value = map[key]
    if #s > 0 then s = s .. "\n" end
    local shortkey = find_short0(map.__short_main_names, key)
    local len = u8.utf8_slen(value)
    local pad = fmt('%' .. math.max(width - len, 1) .. 's', '')
    s = s .. fmt('%s%s- %2s|%s', value, pad, shortkey, key)
  end

  s = s .. "\n\nParts:\n" .. parts
  return s
end



function M.build_help_line_parts()
  return build_help_main_n_part(LINES_PARTS, 12, LINES_ORDERED_PART_NAMES)
end

function M.build_help_arrow()
  return build_help_main_n_part(ARROWS, 12, PART_NAMES_ORDER)
end

function M.build_help_rect()
  local s = ''
  local main_keys = sorted_keys(RECT_STYLES)
  for _, key in pairs(main_keys) do
    local v = RECT_STYLES[key]
    if type(v) == 'table' then
      if #s > 0 then s = s .. "\n" end
      local parts = tostring(v[1]) .. tostring(v[2]) .. tostring(v[3])
      local shortkey = find_short(RECT_STYLES.__short_names, key)
      s = s .. parts .. '  ' .. shortkey .. key
    end
  end

  return s
end

-- point verbose
local function build_help_double_mapping(map)
  local s = ''
  local main_keys = sorted_keys(map)
  for _, key in pairs(main_keys) do
    local part_map = map[key]

    local short_mainkey = find_short(map.__short_main_names, key, '')

    local part_keys = sorted_keys(part_map)
    for _, part_key in pairs(part_keys) do
      local short_pkey = find_short(map.__short_parts_names, part_key, '')
      local value = part_map[part_key]
      if #s > 0 then s = s .. "\n" end
      local spk = string.match(short_pkey, '^%s*([^%s]+)%s*$') or ''
      s = s .. tostring(value) .. '  ' ..
          string.format('%4s | %s-%s', (short_mainkey .. spk), key, part_key)
    end
  end

  return s
end

-- for build-help with compact mappings
-- parts name coresponded to Capital Letter
local function build_letter_mappings(parts_names)
  local s, n = '', 0
  local max = { [1] = 0, [2] = 0, [3] = 0, [4] = 0 }
  local max_len = 22

  for i, k in ipairs(parts_names) do
    local idx = i % 4 + 1
    local len = #k
    if len > max[idx] then max[idx] = math.min(max_len, len) end
  end

  local trim = _G.TEST

  for i, k in ipairs(parts_names) do
    n = n + 1

    local idx = (i % 4) + 1
    local len = max[idx] + 1
    s = s .. fmt('%-' .. len .. 's', k) -- A: part_name

    if n == 4 then
      if trim then s = s:gsub("^(.-)%s*$", "%1") end
      s = s .. "\n"
      n = 0
    end
  end

  if trim then s = s:gsub("^(.-)%s*$", "%1") end

  return s
end

-- example
local function build_example_for_compact_mapping(map, main_keys, parts_names_map)
  local s        = "\nExample: "
  -- horizontal-double + J  ->     hdv|horizontal-double-vertical
  --     ^key         Letter   smk+spk|        ^main    -  ^part
  --                  of part e.g. "J" is vertical
  local idx      = math.floor((#main_keys) / 2)
  local main_key = main_keys[idx]


  local parts_map = map[main_key]
  if parts_map then
    local parts_keys = sorted_keys(parts_map) ---@cast parts_keys table
    local part_key = parts_keys[#parts_keys / 2]
    local letter = parts_names_map[part_key or false]

    local smk = find_short0(map.__short_main_names, main_key)
    local spk = find_short0(map.__short_parts_names, part_key)
    local symb = v2s((map[main_key] or E)[part_key])
    s = s .. fmt('%s -> %s + (%s is %s)  ->  %s%s|%s-%s',
      symb, main_key, v2s(letter), v2s(part_key),
      v2s(smk), v2s(spk), v2s(main_key), v2s(part_key))
  else
    s = s .. 'Not Found by: ' .. main_key
  end

  return s
end

--
-- point
---@param part_names_order table
---@param part_names_map table
local function build_help_double_mapping_compact(
  map, part_names_order, part_names_map)
  local s = ''
  local unique = {}

  -- built the scale to compare with the name of the part
  local parts_names = {}
  local map_pn2letter = {}
  local byte_A = string.byte('A', 1, 1) - 1
  for j, key in pairs(part_names_order) do
    local char = string.char(byte_A + j)
    s = s .. char
    parts_names[#parts_names + 1] = char .. ': ' .. key
    map_pn2letter[key] = char
    map_pn2letter[char] = key
  end
  s = s .. "\n"

  local main_keys = sorted_keys(map)

  for _, mkey in pairs(main_keys) do
    local parts_map = map[mkey]

    -- local part_keys = utbl.tbl_keys(parts_map)
    local part_keys = sorted_keys(parts_map)

    for _, key0 in pairs(part_names_order) do
      if parts_map[key0] then
        local value = parts_map[key0]
        s = s .. value
      else
        s = s .. " "
      end
    end

    -- show unique names separatly
    for _, part_key in ipairs(part_keys) do
      if not part_names_map[part_key] then
        unique[# unique + 1] = { mkey, part_key }
      end
    end

    local smkey = find_short0(map.__short_main_names, mkey)
    s = s .. fmt(" - %2s|%s\n", smkey, mkey)
  end

  s = s .. "\n" ..
      build_letter_mappings(parts_names) ..
      build_example_for_compact_mapping(map, main_keys, map_pn2letter)

  -- unique - to show extra entries that not in the PART_NAMES_ORDER namespace
  if #unique > 0 then
    for _, pair in ipairs(unique) do
      local mk, pk = pair[1], pair[2]
      local symb = v2s((map[mk] or E)[pk])
      s = s .. fmt('%s  -  %s-%s', symb, mk, pk)
    end
  end

  return s .. "\n"
end



-- for point and lines
function M.build_help_symbols()
  return
      build_help_double_mapping_compact(
        SYMBOLS_W2, PART_NAMES_ORDER, PART_NAMES_MAP) ..
      "\n" ..
      build_help_double_mapping(SYMBOLS_W1)
end

--------------------------------------------------------------------------------


function M.get_name_parts(name, words_in_main)
  local pi, i, n = 1, nil, 0
  local main, part

  words_in_main = words_in_main or 1

  while true do
    i = string.find(name, '-', pi, true)
    if not i then break end

    n = n + 1

    if n == words_in_main then
      main = name:sub(1, i - 1)
      part = name:sub(i + 1, #name)
      return main, part
    end
    pi = i + 1
  end

  if not i then
    local mpos = words_in_main
    main = name:sub(1, mpos)
    part = name:sub(mpos + 1, #name)
    return main, part
  end

  return nil, nil
end

function M.is_help_query(s)
  return s == '--help' or s == 'help!' or s == '?' or s == '-h' or s == 'h!'
end

-- where parts maps to position in the line not a table
---@param map table
local function pick_by_main_n_parts(map, full_name, words_in_main)
  if M.is_help_query(full_name) then
    return build_help_main_n_part(map)
  end

  if not full_name then return nil end

  local main, part = M.get_name_parts(full_name, words_in_main)

  if not main or not part then
    return nil
  end

  local s = map[main]
  if not s then -- pick by short name : short => long and fetch
    s = map[map.__short_main_names[main] or false]
  end

  if not s then
    local keys = get_readable_keys(map)
    return nil, fmt('Not Found section for "%s" has [%s]', v2s(main), keys)
  end

  if part == '' or not part then
    part = 'horizontal' -- defualt
    -- show available keys for parts
    -- return fmt('to pick one of [%s] specify one of:\n%s', s, build_parts0(map))
  end

  local pos = map.__name_to_pos[part or false]
  if not pos then
    local long_name = map.__short_parts_names[part]
    pos = map.__name_to_pos[long_name or false]
  end
  -- if _G.TEST_DEBUG then print('[DEBUG]', full_name, main, part, 'pos:', pos, s) end

  if type(s) == 'string' and pos then
    return u8.letter_at(s, pos)
  end

  return nil, fmt('Not Found part: "%s" in section: "%s"\nknown_parts: [%s]',
    v2s(part), v2s(main), get_readable_keys(map.__name_to_pos))
end

--
-- light
--
---@param full_name string?
---@return string?
function M.pick_line_part(full_name)
  return pick_by_main_n_parts(LINES_PARTS, full_name, 1)
end

---@param full_name string
---@return string?
function M.pick_arrow(full_name)
  return pick_by_main_n_parts(ARROWS, full_name, 1)
end

--
---@param name string
---@return table|string?
function M.pick_rect_style(name) -- full_name, words_in_main)
  if M.is_help_query(name) then
    return M.build_help_rect()
  end

  if name then
    -- local main, part = M.get_name_parts(full_name, words_in_main)
    local main = name
    local t = RECT_STYLES[main or false]
    if not t then
      t = RECT_STYLES[RECT_STYLES.__short_names[main] or false]
    end

    return M.build_rect_style(t)
  end
end

---@param t table
---@return table{hl, vl, corners, bl, rl}
function M.build_rect_style(t)
  if type(t) == 'table' then
    return {
      hl = t[1],         -- horizontal or left
      vl = t[2],         -- vertical or top
      corners = M.corners_rect_parse(t[3]),
      bl = t[5] or t[1], -- bottom (horizontal)
      rl = t[2] or t[1], -- right (vertical)
    }
  end

  return t
end

-- for point
function M.pick_double_mapping(map, name, words_in_main)
  local main, part = M.get_name_parts(name, words_in_main)
  if main and part then
    local t = map[main] or map[map.__short_main_names[main] or false]
    if t then
      local v = t[part] or t[map.__short_parts_names[part] or false]
      -- if not v then
      --   local full = map.__short_parts_names[part]
      --   error('not found value for part ' .. tostring(part) .. 'full:' .. tostring(full))
      -- end
      return v
    else
      -- local full = map.__short_main_names[main]
      -- error('not found section for main: ' .. tostring(main) .. ' full:' .. tostring(full))
    end
  end
  return nil
end

function M.pick_symbol(name)
  if M.is_help_query(name) then
    return M.build_help_symbols()
  end
  local s = M.pick_double_mapping(SYMBOLS_W2, name, 2)
  if not s then
    s = M.pick_double_mapping(SYMBOLS_W1, name, 1)
  end
  return s
end

--
-- "lrRL" -> {"l", "r", "R", "L"}
-- with utf8 support
--
-- 1 top-left
-- 2 top-right
-- 3 bottom-right
-- 4 bottom-left
--
---@param corners string?
---@return table{hl, vl, corners:table, rl, bl}
function M.corners_rect_parse(corners)
  assert(type(corners) == 'string', 'expected string corners')

  local len = u8.utf8_slen(corners)
  if len >= 1 then
    local t = u8.foreach_letter(corners, function(l, acc)
      acc[#acc + 1] = l
    end, {})

    return M.corners_rect_normolize(t)
  else
    error('expected 1-4 letters got: ' .. tostring(len))
  end
end

---@param t table
function M.corners_rect_normolize(t)
  assert(type(t) == 'table', 't')

  t[1] = t[1] or '+'
  t[2] = t[2] or t[1]
  t[3] = t[3] or t[2]
  t[4] = t[4] or t[3]
  t[5] = nil --?

  return t
end

--[[
UTF-8 Block Elements

▟

▀	2580	UPPER HALF BLOCK
▁	2581	LOWER ONE EIGHTH BLOCK
▂	2582	LOWER ONE QUARTER BLOCK
▃	2583	LOWER THREE EIGHTHS BLOCK
▄	2584	LOWER HALF BLOCK
▅	2585	LOWER FIVE EIGHTHS BLOCK
▆	2586	LOWER THREE QUARTERS BLOCK
▇	2587	LOWER SEVEN EIGHTHS BLOCK
█	2588	FULL BLOCK
▉	2589	LEFT SEVEN EIGHTHS BLOCK
▊	258A	LEFT THREE QUARTERS BLOCK
▋	258B	LEFT FIVE EIGHTHS BLOCK
▌	258C	LEFT HALF BLOCK
▍	258D	LEFT THREE EIGHTHS BLOCK
▎	258E	LEFT ONE QUARTER BLOCK
▏	258F	LEFT ONE EIGHTH BLOCK
▐	2590	RIGHT HALF BLOCK
░	2591	LIGHT SHADE
▒	2592	MEDIUM SHADE
▓	2593	DARK SHADE
▔	2594	UPPER ONE EIGHTH BLOCK
▕	2595	RIGHT ONE EIGHTH BLOCK
▖	2596	QUADRANT LOWER LEFT
▗	2597	QUADRANT LOWER RIGHT

▘	2598	QUADRANT UPPER LEFT

▙	2599	QUADRANT UPPER LEFT AND LOWER LEFT AND LOWER RIGHT

▚	259A	QUADRANT UPPER LEFT AND LOWER RIGHT

▛	259B	QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER LEFT

▜	259C	QUADRANT UPPER LEFT AND UPPER RIGHT AND LOWER RIGHT

▝	259D	QUADRANT UPPER RIGHT

▞	259E	QUADRANT UPPER RIGHT AND LOWER LEFT

▟	259F	QUADRANT UPPER RIGHT AND LOWER LEFT AND LOWER RIGHT

]]

-- local MATH = {
-- ⋔	22D4	PITCHFORK
-- ⋀	22C0	N-ARY LOGICAL AND
-- ⋁	22C1	N-ARY LOGICAL OR
--
-- ⋂	22C2	N-ARY INTERSECTION
-- ⋃
-- ∧	2227	LOGICAL AND
-- ∨	2228	LOGICAL OR
-- ∩	2229	INTERSECTION
-- ∪	222A	UNION
-- ∘	2218	RING OPERATOR
-- ∙	2219	BULLET OPERATOR
-- ∈	2208	ELEMENT OF
-- ∉	2209	NOT AN ELEMENT OF
-- ∊	220A	SMALL ELEMENT OF
-- ∋	8715	220B	CONTAINS AS MEMBER
-- ∅	2205	EMPTY S
-- ∀	2200	FOR ALLET

-- ⊢	8866	22A2	 RIGHT TACK
-- ⊣	8867	22A3	 LEFT TACK
-- ⊤	8868	22A4	 DOWN TACK
-- ⊥	8869	22A5	UP TACK
-- }
--------------------------------------------------------------------------------

if _G.TEST then
  M.LINES_PARTS = LINES_PARTS
  M.find_short = find_short
  M.sorted_keys = sorted_keys
end

--
return M

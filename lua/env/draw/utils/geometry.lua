-- 06-05-2024 @author Swarg
-- Goal:
--  work with simple elements for constructing composite geometric objects
--  such as:
--    - such as a line with corners
--

local class = require 'oop.class'
local log = require('alogger')
local ubase = require("env.draw.utils.base")

local Point = require 'env.draw.Point'
local Line = require("env.draw.Line");
-- local Container = require 'env.draw.Container'
local IEditable = require('env.draw.IEditable')
-- local usymbols = require 'env.draw.utils.unicode_symbols'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local instanceof = class.instanceof
local get_class = class.get_class

local log_trace = log.trace

local M = {}

M.DIRECTION_NO = 0
M.DIRECTION_TOP = 1
M.DIRECTION_RIGHT = 2
M.DIRECTION_BOTTOM = 3
M.DIRECTION_LEFT = 4

-- corners = { '┌', '┐', '┘', '└' } tl tr br bl
M.DIRECTION_TOP_LEFT = 5
M.DIRECTION_TOP_RIGHT = 6
M.DIRECTION_BOTTOM_RIGHT = 7
M.DIRECTION_BOTTOM_LEFT = 8

M.DIRNAME = {
  [M.DIRECTION_NO] = 'no',
  [M.DIRECTION_TOP] = 'top',
  [M.DIRECTION_RIGHT] = 'right',
  [M.DIRECTION_BOTTOM] = 'bottom',
  [M.DIRECTION_LEFT] = 'left',
  [M.DIRECTION_TOP_LEFT] = 'top-left',
  [M.DIRECTION_TOP_RIGHT] = 'top-right',
  [M.DIRECTION_BOTTOM_RIGHT] = 'bottom-right',
  [M.DIRECTION_BOTTOM_LEFT] = 'bottom-left',
}

-- horizontal + vertical
M.AXIS_TO_DIR = {
  [M.DIRECTION_NO] = {
    [M.DIRECTION_NO] = M.DIRECTION_NO,
    [M.DIRECTION_TOP] = M.DIRECTION_TOP,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM
  },
  [M.DIRECTION_LEFT] = {
    [M.DIRECTION_TOP] = M.DIRECTION_TOP_LEFT,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM_LEFT,
    [M.DIRECTION_NO] = M.DIRECTION_LEFT
  },
  [M.DIRECTION_RIGHT] = {
    [M.DIRECTION_TOP] = M.DIRECTION_TOP_RIGHT,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM_RIGHT,
    [M.DIRECTION_NO] = M.DIRECTION_RIGHT
  }
}


M.DIR_TO_CORNER = {
  [M.DIRECTION_TOP] = {
    [M.DIRECTION_LEFT] = M.DIRECTION_TOP_LEFT,
    [M.DIRECTION_RIGHT] = M.DIRECTION_TOP_RIGHT,
    [M.DIRECTION_TOP] = M.DIRECTION_TOP,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM,
  },
  [M.DIRECTION_BOTTOM] = {
    [M.DIRECTION_LEFT] = M.DIRECTION_BOTTOM_LEFT,
    [M.DIRECTION_RIGHT] = M.DIRECTION_BOTTOM_RIGHT,
    [M.DIRECTION_TOP] = M.DIRECTION_TOP,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM,
  },
  [M.DIRECTION_LEFT] = {
    [M.DIRECTION_LEFT] = M.DIRECTION_LEFT,
    [M.DIRECTION_RIGHT] = M.DIRECTION_RIGHT,
    [M.DIRECTION_TOP] = M.DIRECTION_TOP_LEFT,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM_LEFT,
  },
  [M.DIRECTION_RIGHT] = {
    [M.DIRECTION_LEFT] = M.DIRECTION_LEFT,
    [M.DIRECTION_RIGHT] = M.DIRECTION_RIGHT,
    [M.DIRECTION_TOP] = M.DIRECTION_TOP_RIGHT,
    [M.DIRECTION_BOTTOM] = M.DIRECTION_BOTTOM_RIGHT,
  },
}

M.DIR_OPPOSITE = {
  [M.DIRECTION_NO] = M.DIRECTION_NO,
  [M.DIRECTION_TOP] = M.DIRECTION_BOTTOM,
  [M.DIRECTION_BOTTOM] = M.DIRECTION_TOP,
  [M.DIRECTION_LEFT] = M.DIRECTION_RIGHT,
  [M.DIRECTION_RIGHT] = M.DIRECTION_LEFT,
  [M.DIRECTION_TOP_LEFT] = M.DIRECTION_BOTTOM_RIGHT,
  [M.DIRECTION_TOP_RIGHT] = M.DIRECTION_BOTTOM_LEFT,
  [M.DIRECTION_BOTTOM_RIGHT] = M.DIRECTION_TOP_LEFT,
  [M.DIRECTION_BOTTOM_LEFT] = M.DIRECTION_TOP_RIGHT,
}

-- direction to offset
M.DIR_OFFSET = {
  [M.DIRECTION_NO] = { 0, 0 },
  [M.DIRECTION_TOP] = { 0, -1 },
  [M.DIRECTION_RIGHT] = { 1, 0 },
  [M.DIRECTION_BOTTOM] = { 0, 1 },
  [M.DIRECTION_LEFT] = { -1, 0 },
  [M.DIRECTION_TOP_LEFT] = { -1, -1 },
  [M.DIRECTION_TOP_RIGHT] = { 1, -1 },
  [M.DIRECTION_BOTTOM_RIGHT] = { 1, 1 },
  [M.DIRECTION_BOTTOM_LEFT] = { -1, 1 },
}


M.OFFSET_TO_DIR = {
  [0] = {
    [-1] = M.DIRECTION_TOP,
    [0] = M.DIRECTION_NO,
    [1] = M.DIRECTION_BOTTOM,
  },
  [1] = {
    [-1] = M.DIRECTION_TOP_RIGHT,
    [0] = M.DIRECTION_RIGHT,
    [1] = M.DIRECTION_BOTTOM_RIGHT,
  },
  [-1] = {
    [-1] = M.DIRECTION_TOP_LEFT,
    [0] = M.DIRECTION_LEFT,
    [1] = M.DIRECTION_BOTTOM_LEFT,
  }
}


-- if n == 0 then def
-- if n < min then min
function M.def_or_min(n, def, min)
  if n == 0 then n = def end
  if n < min then n = min end
  return n
end

---@param cx number
---@param x1 number
---@param x2 number
function M.closer_to(cx, x1, x2)
  local x1dist = math.abs(x1 - cx)
  local x2dist = math.abs(x2 - cx)
  if x1dist > x2dist then
    return x2, x1
  end
  return x1, x2
end

M.smallest_first = ubase.smallest_first
M.normalize = ubase.normalize

-- the first point will be the one closest to the specified point cx cy
function M.closer_first(cx, cy, x1, y1, x2, y2)
  x1, x2 = M.closer_to(cx, x1, x2)
  y1, y2 = M.closer_to(cy, y1, y2)
  return x1, y1, x2, y2
end

-- issue: line does not keep the original direction, it changing coordinates
-- in contructor(_init) so that the second point is always to the right and lower
-- than the first one: (x1 < x2) and (y1 < y2)
-- therefore, in order to understand where the line is looking, needs a point
-- relative to which determine the direction (cx, cy)
--
---@param line env.draw.Line
---@param cx number
---@param cy number
function M.get_line_direction(line, cx, cy)
  assert(instanceof(line, Line), 'expected Line')
  local x1, y1, x2, y2 = line:get_coords()
  x1, y1, x2, y2 = M.closer_first(cx, cy, x1, y1, x2, y2)

  local no = M.DIRECTION_NO
  local h = x1 < x2 and M.DIRECTION_LEFT or x1 > x2 and M.DIRECTION_RIGHT or no
  local v = y1 < y2 and M.DIRECTION_TOP or y1 > y2 and M.DIRECTION_BOTTOM or no

  local res = M.AXIS_TO_DIR[h][v] or no

  -- print('[DEBUG]', M.DIRNAME[res], 'from', M.DIRNAME[h], M.DIRNAME[v])
  return res
end

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return number
function M.get_direction_code(x1, y1, x2, y2)
  local xoff, yoff = x2 - x1, y2 - y1
  assert(math.abs(xoff) < 2, 'expected xoff in -1,0,1')
  assert(math.abs(yoff) < 2, 'expected yoff in -1,0,1')
  local yarr = M.OFFSET_TO_DIR[xoff]
  assert(yarr, 'expected existed array for xoff')

  return (M.OFFSET_TO_DIR[xoff] or E)[yoff] -- or M.DIRECTION_NO
end

---@param pos1 table{x:number, y:number}
---@return number?
function M.get_direction_code2(pos1, pos2)
  return M.get_direction_code(pos1.x, pos1.y, pos2.x, pos2.y)
end

-- without sqrt
---@return number
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
function M.distance_sq(x1, y1, x2, y2)
  return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)
end

function M.is_inside_range(x, y, x1, y1, x2, y2)
  return x1 ~= nil and x >= x1 and x <= x2 and y >= y1 and y <= y2
end

--
-- find the distance from a point(x,y) to the nearest edge of a given
-- rectangular area(x1:y1, x2:y2)
--
-- consider x1:y1, x2:y2 coordinates normalized  that is: x1 <= x2 and y1 <= y2
--
---@param x1 number, y1 number, x2 number, y2 number
---@return number distance square
---@return number direction code
function M.get_distance_sq_to_closest_edge(x, y, x1, y1, x2, y2)
  if M.is_inside_range(x, y, x1, y1, x2, y2) then
    return 0, M.DIRECTION_NO
  end
  -- vertex
  if x < x1 and y < y1 then
    return M.distance_sq(x, y, x1, y1), M.DIRECTION_TOP_LEFT
  elseif x > x2 and y < y1 then
    return M.distance_sq(x, y, x2, y1), M.DIRECTION_TOP_RIGHT
  elseif x > x2 and y > y2 then
    return M.distance_sq(x, y, x2, y2), M.DIRECTION_BOTTOM_RIGHT
  elseif x < x1 and y > y2 then
    return M.distance_sq(x, y, x1, y2), M.DIRECTION_BOTTOM_LEFT
    -- edges
  elseif x < x1 then -- and y >= y1 and y<= y2
    return (x1 - x) * (x1 - x), M.DIRECTION_LEFT
  elseif x > x2 then -- and y >= y1 and y<= y2
    return (x - x2) * (x - x2), M.DIRECTION_RIGHT
  elseif y < y1 then -- and y >= y1 and y<= y2
    return (y1 - y) * (y1 - y), M.DIRECTION_TOP
  elseif y > y2 then -- and y >= y1 and y<= y2
    return (y - y2) * (y - y2), M.DIRECTION_BOTTOM
  end

  return -1, M.DIRECTION_NO
end

--
-- find two points closest to each other
--
---@param a env.draw.Line
---@param b env.draw.Line
function M.find_closed_points(a, b)
  local dist = M.distance_sq
  local d1 = dist(a.x1, a.y1, b.x1, b.y1)
  local d2 = dist(a.x1, a.y1, b.x2, b.y2)
  local d3 = dist(a.x2, a.y2, b.x1, b.y1)
  local d4 = dist(a.x2, a.y2, b.x2, b.y2)

  if d1 < d2 then
    if d3 < d4 then
      if d1 < d3 then
        return a.x1, a.y1, b.x1, b.y1 -- min d1
      else
        return a.x2, a.y2, b.x1, b.y1 -- min d3
      end
    else                              -- d4 < d3
      if d1 < d4 then
        return a.x1, a.y1, b.x1, b.y1 -- min d1
      else
        return a.x2, a.y2, b.x2, b.y2 -- min d4
      end
    end
  else -- d2 < d1
    if d3 < d4 then
      if d2 < d3 then
        return a.x1, a.y1, b.x2, b.y2 -- min d2
      else
        return a.x2, a.y2, b.x1, b.y1 -- min d3
      end
    else                              -- d4 < d3
      if d2 < d4 then
        return a.x1, a.y1, b.x2, b.y2 -- min d2
      else
        return a.x2, a.y2, b.x2, b.y2 -- min d4
      end
    end
  end
end

---@param a env.draw.Line
---@param b env.draw.Line
---@return number?
---@return number?
function M.find_contact_point(a, b)
  local x1, y1, x2, y2 = M.find_closed_points(a, b)
  log.debug("found_closed_points: (%s:%s) (%s:%s)", x1, y1, x2, y2)

  local x, y = nil, nil

  if x1 == x2 and y1 == y2 then
    x, y = x1, y1
  else
    local dx, dy = math.abs(x2 - x1), math.abs(y2 - y1)
    if dx < 2 and dy < 2 then
      x, y = math.ceil((x2 + x1) / 2), math.ceil((y2 + y1) / 2)
    end
  end

  return x, y
end

--
-- find element from inv by given params (x:y-position)
--
---@param inv table
---@param klass table?  use nil to any class
---@param x number
---@param y number
---@param opts table?{tag, vertex_only, limit, offset}
---@param t table? -- outtable
--- tag string? -- '*' to any
--- vertex_only boolean? -- it false use intersects if true - check vertex
function M.find_by_xy(inv, klass, x, y, opts, t)
  if inv and next(inv) and x and y then
    t = t or {}

    local offset = (opts or E).offset or 1
    local vertex_only = (opts or E).vertex_only
    local tag = (opts or E).tag
    local limit = (opts or E).limit or 0
    if tag == nil then tag = '*' end -- any

    for i = offset, #inv do
      local elm, add = inv[i], false

      if (not klass or klass == '*' or klass == get_class(elm)) then
        if (tag == '*' or tag == elm:tag() or tag == false and elm:tag() == nil)
        then
          if not vertex_only then
            if elm:isIntersects(x, y, x, y) then
              add = true
            end
          else
            local x1, y1, x2, y2 = elm:get_coords()
            if (x1 == x and y1 == y) or (x2 == x and y2 == y) then
              add = true
            end
          end
        end
      end

      if add then
        t[#t + 1] = elm
        if limit and limit > 0 and #t >= limit then
          return t
        end
      end
    end
  end

  return t
end

local d2n = function(d)
  return M.DIRNAME[d or false] or '?'
end

--
-- return corner direction and coords(x:y) of contact point
--
-- light = '┌┐┘└',
-- 1: top-left
-- 2: top-right
-- 3: bottom-right
-- 4: bottom-left
--
---@param a env.draw.Line
---@param b env.draw.Line
---@param x number? optional
---@param y number?
---@return number? dir
---@return number? x contact point
---@return number? y
function M.get_corner_rect_direction_xy(a, b, x, y)
  assert(type(a) == 'table', 'a')
  assert(type(b) == 'table', 'b')

  if x == nil or y == nil then
    x, y = M.find_contact_point(a, b)
  end
  log_trace('contact_point %s:%s', x, y)

  if x and y then
    local da = M.get_line_direction(a, x, y)
    local db = M.get_line_direction(b, x, y)
    local dc = (M.DIR_TO_CORNER[da] or E)[db]

    log_trace('corner dir:%s from: da:%s db:%s', dc, d2n(da), d2n(db))
    return dc, x, y
  end

  return nil, nil, nil
end

---@param style table?{hl, vl, corners}
---@param d number?
function M.rect_char_by_direction(style, d)
  local i, res = nil, nil
  if d and style then
    if d >= M.DIRECTION_TOP_LEFT then
      i = d - M.DIRECTION_TOP_LEFT + 1
      res = style.corners[i]
      --
    elseif d == M.DIRECTION_TOP or M.DIRECTION_BOTTOM then
      res = style.vl
      --
    elseif d == M.DIRECTION_LEFT or M.DIRECTION_RIGHT then
      res = style.hl
    end
  end

  log_trace('rect_char_by_direction res:%s dname:%s i:%', res, M.DIRNAME[d], i)

  return res or '?'
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------


--
-- add env.draw.Point for list of Lines
--
---@param inv table{env.draw.Lines}
---@param points table{[{x,y}] = env.draw.Point}
---@param style table?{hl, vl, corners:table} -- corners 1:tl 2:tr 3:br 4:bl
function M.join_lines_by_points(inv, points, style)
  local size, prev = #inv, nil
  local direction = M.get_corner_rect_direction_xy

  for i = 1, size do
    local elm = inv[i]

    if prev and get_class(prev) == Line then
      local klass = get_class(elm)
      if klass == Line then
        local dc, x, y = direction(prev, elm)
        if dc and x and y and not points[{ x, y }] then
          local c = M.rect_char_by_direction(style, dc)

          log_trace('new point (%s:%s) c:"%s"', x, y, c)
          inv[#inv + 1] = Point:new(nil, x, y, c)
        end
      else
        elm = prev -- to keep line in prev
      end
    end
    prev = elm
  end

  for _, point in pairs(points) do
    inv[#inv + 1] = IEditable.cast(point)
  end

  return inv
end

--
-- create, based on the marked points, one combined line wrapped in a container
-- with automatic connection of lines at the corners with insertion of
-- connecting Points.
-- automatically determining:
--  - the type of corners according to a predefined style
--  - connecting points of two sequentaly lines
--
---@param plist table{{x:number,y:number}, {x,y}, {x, y}, ...}
---@param style table?{hlc, vlc, corners}
---@return table{env.draw.IEditable}, x1, y1, x2, y2
---@return number x1
---@return number y1
---@return number x2
---@return number y2
function M.create_combined_line(plist, style)
  local x1, y1, x2, y2 = 65535, 65535, 0, 0 -- range

  local hl = (style or E).hl or '-'
  local vl = (style or E).vl or '|'

  local function mk_line(inv, points, a, cx, cy)
    local w, h = ubase.sizeof_points2(a, cx, cy)
    local c1 = w == 1 and vl or hl

    do -- findout area for container
      local cx1, cx2 = M.smallest_first(a.x, cx)
      local cy1, cy2 = M.smallest_first(a.y, cy)

      if cx1 < x1 then x1 = cx1 end
      if cy1 < y1 then y1 = cy1 end
      if cx2 > x2 then x2 = cx2 end
      if cy2 > y2 then y2 = cy2 end
    end

    if w == 1 and h == 1 then -- ?
      log_trace('new point %s:%s', cx, cy)
      points[{ cx, cy }] = Point:new(nil, cx, cy, '+')
      --
    elseif w > 1 and h > 1 then
      error('diagonal lines are not supported')
    else
      log_trace('new line (%s:%s)(%s:%s) "%s"', a.x, a.y, cx, cy, c1)
      inv[#inv + 1] = Line:new(nil, a.x, a.y, cx, cy, c1)
    end
  end

  local inv, points, prev = {}, {}, plist[1]
  for i = 2, #plist do
    local t = plist[i]
    ubase.validate_pos_xy(t, i)
    mk_line(inv, points, prev, t.x, t.y)
    prev = t
  end

  M.join_lines_by_points(inv, points, style)

  return inv, x1, y1, x2, y2
end

---@param inv table<env.draw.IEditable> -- Line & Point
---@param fp table
---@param lp table
function M.combined_line_to_plist(inv, fp, lp)
  assert(type(inv) == 'table', 'inv')
  local fx, fy = fp.x, fp.y
  local lx, ly = lp.x, lp.y

  local t = {}
  t[#t + 1] = { x = fx, y = fy }
  -- inv[1] -- line and line.x1 == fp.x or line.x2 == fp.x
  for _, elm in ipairs(inv) do
    local klass = get_class(elm)
    if klass == Point then
      local x, y = elm:pos()
      t[#t + 1] = { x = x, y = y }
    end
  end

  t[#t + 1] = { x = lx, y = ly }

  return t
end

return M

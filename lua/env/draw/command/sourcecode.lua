-- 08-04-2024 @author Swarg
-- Goal:
--  - view canvas as code and code as canvas,
--  - one shot render into given place(buffer) (source-code to text-view)
--
--  TODO:
--  idea: show selected elements in the canvas editor as code in new buffer
--  and apply

local Canvas = require('env.draw.Canvas')
local state = require('env.draw.state.canvas')
local ubuf = require('env.draw.utils.buf')
local FS = require('env.draw.utils.fs')
local H = require("env.draw.command.helper")
local EE = require("env.draw.ui.ElementsEditor");

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- work with source code of the canvas
-- - open canvas as source-code            (to edit directly)
-- - open source-code of canvas in Editor  (to view)
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :desc('show source-code of current canvas in new buffer')
      :desc('(canvas -> code)')
      :cmd('code', 'c')

      :desc('open already opened source code in CanvasEditor')
      :desc('(code -> canvas)')
      :cmd('view', 'v')

      :desc('render the given canvas source code to text view(in new buffer)')
      :desc('(one shot render in new buffer)')
      :cmd('render', 'r')

  --
      :desc('show selected canvas elements as code in the new special buffer')
      :desc('(elements -> code)')
      :cmd('elements-code', 'ec')

      :cmd('debugging', 'd')

      :run()
end

local leaf = 1

M.cmdtree = {
  code = leaf,
  view = leaf,
  render = leaf,
  ['elements-code'] = leaf,
  debugging = {
    ['new-statefulbuf'] = leaf,
    nvim_move_cursor = leaf,
  },
}

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local ask_confirm_and_do = ubuf.ask_confirm_and_do
local get_editor_coords = ubuf.get_editor_coords
local openCodeInCanvasEditor = H.openCodeInCanvasEditor
local buildCanvasFromCodeInBuf = H.buildCanvasFromCodeInBuf
local get_canvas_or_error = H.get_canvas_or_error
local get_actual_bufnr = ubuf.get_actual_bufnr
local save_canvas = state.save_canvas
local load_canvas = state.load_canvas
local get_canvas_box = state.get_canvas_box
local edit_elements_as_code = EE.edit_elements_as_code
local file_exists = FS.file_exists
local file_write_lines = FS.file_write_lines

--
-- edit(open) already opened source code in the CanvasEditor
-- (code -> canvas)
--
---@param w Cmd4Lua
function M.cmd_view(w)
  local bufnr = w:desc('bufnr with source-code'):def(0):optn('--bufnr', '-b')

  if not w:is_input_valid() then return end

  bufnr = get_actual_bufnr(bufnr)
  local _, msg = openCodeInCanvasEditor(bufnr, 0, -1)
  w:say(msg)
end

--
-- open source-code of current canvas in new buffer
-- (canvas -> code)
--
---@param w Cmd4Lua
function M.cmd_code(w)
  w:v_opt_verbose('-v')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local msg
  -- if the canvas is loaded from a certain source file, then save the changes
  -- to the file before showing its source code
  local has_file = file_exists(box.filename)
  w:verbose('has_file:', has_file, box.filename)

  if has_file then
    if box:isChanged() then
      _, msg = save_canvas(box, nil, true, true, ask_confirm_and_do)
    else
      msg = 'open unchanged ' .. v2s(box.filename)
    end
    vim.cmd(":e " .. box.filename)
  else
    msg = 'Not Found file "' .. v2s(box.filename) .. '"'
    local code = Canvas.cast(box.item):serialize_to_binary()
    -- show code as simple text in the new buffer
    local newbufnr = H.create_new_buffer()
    if newbufnr ~= bufnr then
      local lines = {}
      for str in string.gmatch(code, '([^\n]+)') do
        table.insert(lines, str)
      end
      vim.api.nvim_buf_set_lines(newbufnr, 0, 0, false, lines)
    else
      w:error('Cannot create new buffer')
    end
  end
  w:say(msg)
end

--
-- render a canvas vied(just text) based on the given source code of canvas
-- (one shot render in new buffer)
--
---@param w Cmd4Lua
function M.cmd_render(w)
  -- -- where to get it from
  local src_fn = w
      :desc('filename with canvas source code(by default take from current buff)')
      :opt('--input-file', '-i')

  local dst_fn = w
      :desc('filename in which to save the drawn text')
      :opt('--output-file', '-o')

  -- -- where to render
  local bufnr = w:desc('bufnr to draw rendered view(use 0 to draw to current')
      :optn('--bufnr', '-b')
  local lnum = w:desc('stat line number in the buffer')
      :def(0):optn('--lnum', '-s')
  local lnum_end = w:desc('end line number in the buffer')
      :def(0):optn('--lnum-end', '-e')

  local yes = w:desc('force to insert on space conflict'):has_opt('--yes', '-y')

  if not w:is_input_valid() then return end
  ---@cast lnum number
  ---@cast lnum_end number

  local ok, canvas
  if src_fn then
    if not file_exists(src_fn) then
      return w:error('Not Found source file: ' .. v2s(src_fn))
    end
    ok, canvas = load_canvas(src_fn)
  else
    ok, canvas = buildCanvasFromCodeInBuf(0, 0, -1)
    if not ok then
      local cbufnr, clnum, ccol = get_editor_coords()
      local box = get_canvas_or_error(w, cbufnr, clnum, ccol, Canvas)
      if box then canvas = Canvas.cast(box.item) end
    end
  end
  if w:has_error() then return end -- box

  ---@cast canvas env.draw.Canvas
  local lines = canvas:draw():toLines()

  if dst_fn then
    if file_exists(dst_fn) then
      local msg = fmt('rewrite already existed file "%s"?', v2s(dst_fn))
      if not ask_confirm_and_do(msg) then
        return w:say('canceled')
      end
    end
    local saved = file_write_lines(dst_fn, lines)
    return w:say('Render saved: ', saved, dst_fn)
  end

  -- if it is not explicitly specified where to render the image
  if bufnr == nil or bufnr < 0 then bufnr = H.create_new_buffer() end
  if lnum_end < lnum then lnum_end = lnum end
  bufnr = get_actual_bufnr(bufnr)

  -- check for intersection with other canvases
  local box0 = get_canvas_box(bufnr, lnum, lnum + #lines, 0, canvas.width)
  if box0 then
    local msg = fmt(
      'The given space(bufnr#%s lnum:%s:%s) is already occupied by another' ..
      ' canvas(%s) and insertion will result in an intersection. continue?',
      bufnr, lnum, lnum_end + #lines, state.getReadableItemPlace(box0)
    )
    if yes or ask_confirm_and_do(msg) then
      w:say('forced insertion with space conflict')
    else
      return w:error('canceled due to intersection with another canvas')
    end
  end

  vim.api.nvim_buf_set_lines(bufnr, math.max(0, lnum - 1), lnum_end, false, lines)
  w:fsay('Text View of Canvas inserted at bufnr:%s lnum:%s lnum_end:%s',
    bufnr, lnum, lnum + #lines)

  return lines
end

--
-- show selected canvas elements as code in the new special buffer
-- TODO selected by v-block mode (does not work via cmd)
--
---@param w Cmd4Lua
function M.cmd_elements_code(w)
  local si = w:desc('start element index'):def(1):optn('--start-index', '-s')
  local ei = w:desc('end element index'):def(-1):optn('--end-index', '-e')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords()
  local box, box_idx = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder
  ---@cast box_idx number

  local canvas = Canvas.cast(box.item)
  local elms, idxs = canvas:get_elements_by_index_range(si, ei)

  edit_elements_as_code(box, box_idx, elms, idxs)
end

--------------------------------------------------------------------------------
--                             Debugging
--------------------------------------------------------------------------------

function M.cmd_debugging(w)
  w:handlers(M)
      :cmd('new-statefulbuf', 'nsb')
      :cmd('nvim_move_cursor', 'mc')
      :run()
end

--
-- manual testing
--
---@param w Cmd4Lua
function M.cmd_new_statefulbuf(w)
  if not w:is_input_valid() then return end
  local StatefulBuf = require("env.draw.ui.StatefulBuf")
  local dispatch = {
    on_write = function(t)
      local data = StatefulBuf.get_buf_var(t.buf, 'data')
      print('on_write:', vim.inspect(t))
      print('data:', vim.inspect(data))
    end,
    on_read = function(t) print('on_read:', vim.inspect(t)) end,
    on_delete = function(t) print('on_delete:', vim.inspect(t)) end
  }
  local vars = {
    data = { 'some-value' }
  }
  local lines = { "line 1", 'line 2', 'line 3' }
  local bn = StatefulBuf.create_buf('name', lines, dispatch, vars)

  w:say('Created buf:', bn)
end

--
--
--
---@param w Cmd4Lua
function M.cmd_nvim_move_cursor(w)
  local direction = w:desc('direction to move one of TRBL'):pop():arg()
  local offset = w:desc('offset(steps to move in given direction'):pop():argn()

  if not w:is_input_valid() then return end

  local cpos1 = vim.api.nvim_win_get_cursor(0)

  ubuf.nvim_move_cursor(direction, offset)

  local cpos2 = vim.api.nvim_win_get_cursor(0)

  w:say(vim.inspect(cpos1), vim.inspect(cpos2))
end

return M

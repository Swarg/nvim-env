-- 07-04-2024 @author Swarg
--
-- Implementation notes:
-- Initially, I thought that I would edit directly in a regular current buffer and
-- not in a separate buffer, like a separate built-in editor. But then I came to
-- the conclusion that it is more convenient to create and edit canvases in a
-- separate window (buffer), so there is some confusion and redundancy in the
-- commands. Due to the support of two approaches when the canvas is linked to
-- the current buffer and to a separate buffer CanvasEditor.

local log = require('alogger')
local Canvas = require('env.draw.Canvas')
local ubuf = require('env.draw.utils.buf')
local state = require('env.draw.state.canvas')
local conf = require('env.draw.settings')
local FS = require('env.draw.utils.fs')
local H = require("env.draw.command.helper")
local CE = require('env.draw.ui.CanvasEditor')

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :desc('create new canvas with given size')
      :cmd('new', 'n')

      :desc('show all existed canvas')
      :cmd('list', 'ls')

      :desc('show info about current canvas(under cursor)')
      :cmd('info', 'i')

      :desc('open canvas in the CanvasEditor from given file')
      :cmd('open', 'o')

      :desc('insert canvas as text from given file into current buffer')
      :cmd('insert-from-file', 'iff')

      :desc('open current canvas in the CanvasEditor')
      :cmd('edit', 'e')

      :desc('remove canvas-object from current position')
      :cmd('remove', 'rm')

      :desc('redraw canvas under the cursor')
      :cmd('draw', 'd')

      :desc('edit canvas settings')
      :cmd('settings', 'conf')

      :desc('save current canvas')
      :cmd('save', 's')

      :desc('load current canvas')
      :cmd('load', 'l')

      :desc('place canvas from canvas list into cuttent buffer')
      :cmd('place', 'p')

      :run()
end

local leaf = 1

M.cmdtree = {
  new = leaf,
  list = leaf,
  info = leaf,
  open = leaf,
  ['insert-from-file'] = leaf,
  edit = leaf,
  remove = leaf,
  draw = leaf,
  settings = leaf,
  save = leaf,
  load = leaf,
  place = leaf,
}

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local E, v2s, fmt = {}, tostring, string.format
local get_editor_coords = ubuf.get_editor_coords
local file_exists = FS.file_exists

local get_canvas_or_error = H.get_canvas_or_error
local openInCanvasEditor = H.openInCanvasEditor
local is_buf_modifiable = ubuf.is_buf_modifiable
local ask_confirm_and_do = ubuf.ask_confirm_and_do
local get_actual_bufnr = ubuf.get_actual_bufnr

local get_ui_item = state.get_ui_item
local update_ui_item = state.update_ui_item
local save_canvas = state.save_canvas
local load_canvas = state.load_canvas
local get_canvas_box = state.get_canvas_box
local remove_ui_item = state.remove_ui_item
local getReadableItemPlace = state.getReadableItemPlace
local get_buffers_with_canvas = state.get_buffers_with_canvas
local get_readable_canvas_list = state.get_readable_canvas_list
local nvim_find_syntax_block_range = ubuf.nvim_find_syntax_block_range
-- local to_list_of_numbers = H.to_list_of_numbers
-- local say_list = H.say_list

--
-- create new canvas with given size
--
---@param w Cmd4Lua
function M.cmd_new(w)
  log.debug("cmd_canvas_new")
  w:usage('canvas new --no-place-as-text -f ~/dia/some.clt (rewrite new to file)')

  w:v_opt_quiet('-q')
  local width = w:desc('the width of the canvas'):def(80):optn('--width', '-w')
  local height = w:desc('the height of the canvas'):def(40):optn('--height', '-h')
  local border = w:desc('add a border (trbl)'):def(''):opt('--border', '-b')
  local fn = w:desc('filename from which to create a new canvas')
      :opt('--file', '-f')

  local edit = w:desc('open canvas in the editor'):has_opt('--edit', '-e')
  local noplace = w:desc('only in editor do not place as text in current bufer')
      :has_opt('--no-place-as-text', '-n')

  if not w:is_input_valid() then return end
  ---@cast width number
  ---@cast height number

  if noplace then
    if fn and file_exists(fn) then
      if not ask_confirm_and_do(fmt('Rewrite existed file? ""', fn)) then
        return w:error('canceled')
      end
    end
    fn = fn or ''
    local canvas = Canvas:new(nil, height, width, Canvas.DEF_BACKGROUND, border)
    openInCanvasEditor(true, canvas, 0, 0, 0, fn)
    w:fsay('New Canvas: %s x %s %s', v2s(canvas.width), v2s(canvas.height), fn)
    return canvas
  end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  col = 0 -- starts canvas from the left

  -- check whether the new area intersects with an existing canvas
  local box, idx = get_canvas_box(bufnr, lnum, lnum + height, col, col + width)
  if box then
    -- select lines ?
    return w:error(fmt('This space (%s:%s) is already occupied by ' ..
      'an existing canvas: %s i:%s', lnum, lnum + height, tostring(box), (idx)))
  end

  local loaded, canvas, ok
  if not fn then
    canvas = Canvas:new(nil, height, width, Canvas.DEF_BACKGROUND, border)
  else
    loaded, canvas = load_canvas(fn)
    if not loaded or type(canvas) == 'string' then
      return w:error(v2s(canvas))
    end
    w:say('New canvas is a copy from file:', fn)
    -- if canvas.height ~= height then canvas.height = height end
    -- if canvas.width ~= width then canvas.width = width end
  end

  ok, box = H.place_canvas(w, canvas, bufnr, lnum, col)
  edit = edit or not ok

  w:fsay('New Canvas: %s x %s', v2s(canvas.width), v2s(canvas.height))

  openInCanvasEditor(edit, canvas, bufnr, lnum, col, (box or E).filename)
  return canvas
end

--
-- open canvas from doc-buffer in the CanvasEditor
--
---@param w Cmd4Lua
function M.cmd_edit(w)
  w:v_opt_quiet('-q')
  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  col = 0

  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  w:fsay('Open Canvas:', tostring(box))
  local canvas = Canvas.cast(box.item)

  openInCanvasEditor(true, canvas, bufnr, lnum, col, box.filename)
end

--
-- redraw canvas under the cursor
--
---@param w Cmd4Lua
function M.cmd_draw(w)
  w:v_opt_quiet('-q')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local box, _ = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  H.draw(w, box)
end

--
-- remove canvas-object from current lnum
-- release? drop?
-- remove canvas-instance from traked canvas list
--
---@param w Cmd4Lua
function M.cmd_remove(w)
  w:v_opt_quiet('-q')

  local bufnr = w:desc('bufnr from which remove canvas'):optn('--bufnr', '-b')
  local index = w:desc('canvas index in bufnr-list'):optn('--index', '-i')

  local yes = w:desc('confirm the canvas deletion'):has_opt('--yes', '-y')

  if not w:is_input_valid() then return end

  local box, msg
  if not bufnr or bufnr < 0 then
    local lnum, col
    bufnr, lnum, col = get_editor_coords(0, 0)
    box, index = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  else
    local list
    box, list = get_ui_item(bufnr, index)
    if not box then
      return w:error(fmt(
        'Not Found canvas for bufnr:%s index:%s (sz:%s)',
        bufnr, index, #(list or E))
      )
    end
  end

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local info = getReadableItemPlace(box)
  if yes or ask_confirm_and_do('Delete Canvas?  ' .. info, nil) then
    local droped = remove_ui_item(bufnr, index, box)
    msg = "\nCanvas removed:" .. v2s(Canvas.__tostring(droped))
  else
    msg = "\nCancaled"
  end

  w:say(msg)
end

--
-- show all existed canvas
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:v_opt_quiet('-q')

  local loc = w:desc('Show only for current buffer'):has_opt('--local', '-l')
  local all = w:desc('Show Abriefly all opened canvases'):has_opt('--all', '-a')

  if not w:is_input_valid() then return end
  local info
  if all then
    info = get_buffers_with_canvas()
  else
    info = get_readable_canvas_list(0, loc)
  end
  info = v2s(info) .. "\nCurrent Buffer: #" .. v2s(get_actual_bufnr(0))

  w:say(info)
end

--
-- open canvas from given file in the CanvasEditor
--
---@param w Cmd4Lua
function M.cmd_open(w)
  w:usage('canvas open ~/dia/some.clt --new')

  w:v_opt_quiet('-q')
  local fn = w:desc('filename'):pop():arg()

  local new = w:desc('Create new for given filename'):has_opt('--new', '-n')
  local width = w:desc('the width of the canvas'):def(80):optn('--width', '-w')
  local height = w:desc('the height of the canvas'):def(40):optn('--height', '-h')

  if not w:is_input_valid() then return end ---@cast fn string

  local loaded, canvas

  if new then
    if file_exists(fn) then
      if not ask_confirm_and_do('file already exists rewrite?') then
        return w:error('canceled')
      end
    end
    canvas = Canvas:new(nil, height, width, Canvas.DEF_BACKGROUND)
    local cbufnr = openInCanvasEditor(true, canvas, 0, 0, 0, fn)
    return print(cbufnr)
  end

  local bufnr, _, _ = get_editor_coords(0, 0)
  loaded, canvas = load_canvas(fn)
  if not loaded or type(canvas) ~= 'table' then
    return w:error(v2s(canvas))
  end
  w:say('Canvas Loaded from: ', fn)
  openInCanvasEditor(true, canvas, bufnr, 0, 0, fn)
end

--
-- insert canvas as text from given file into current buffer
-- just render saved canvas into current buffer without keeping the canvas as
-- etitable object
--
---@param w Cmd4Lua
function M.cmd_insert_from_file(w)
  w:v_opt_quiet('-q')
  local fn = w:desc('filename'):pop():arg()

  if not w:is_input_valid() then return end ---@cast fn string

  local loaded, canvas = load_canvas(fn)
  if not loaded or type(canvas) ~= 'table' then
    return w:error(v2s(canvas))
  end

  local dst_bufnr, lnum, col = get_editor_coords()

  local ok, box, dst_idx = H.place_canvas(w, canvas, dst_bufnr, lnum, col)
  if ok then
    remove_ui_item(dst_bufnr, dst_idx, box)
  end

  w:fsay('Canvas inserted from %s %s', v2s(fn), v2s(ok))
end

--
-- show info about current canvas(under cursor)
--
---@param w Cmd4Lua
function M.cmd_info(w)
  w:v_opt_quiet('-q')
  local inspect_elements = w:desc('inspect canvas elements')
      :has_opt('--inpect-elements', '-e')

  if not w:is_input_valid() then return end

  -- for topleft give  {1, 0} e.i line starts from 1,  col from 0
  -- local cpos = vim.api.nvim_win_get_cursor(0)

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local cpos = vim.api.nvim_win_get_cursor(0) or E

  local box, _ = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder
  local canvas = Canvas.cast(box.item)

  local x, y = box:toInnerXY(lnum, col)
  local lnum0, col0 = cpos[1], cpos[2]
  w:fsay('%s XYCoords:(%s:%s) nvim pos:(%s:%s) filename: %s',
    v2s(canvas), x, y, lnum0, col0, v2s(box.filename))
  if inspect_elements then
    print(require "inspect" (canvas.objects))
  end
end

--
-- edit canvas settings and properties
--
---@param w Cmd4Lua
function M.cmd_settings(w)
  w:v_opt_quiet('-q')
  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder
  local canvas = Canvas.cast(box.item)

  -- todo edit box wrapper props lnum, filename etc
  w:say(box)

  local updated, msg = CE.edit_element(canvas)
  if updated then H.draw(w, box) end
  w:say('updated:', updated, msg)
end

--
-- save current canvas
--
---@param w Cmd4Lua
function M.cmd_save(w)
  w:v_opt_quiet('-q')
  local fn = w:desc('filename in which to save the canvas')
      :opt('--file', '-f')
  local rewrite = w:desc('confirm rewrite already existed file')
      :has_opt('--rewrite', '-w')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local ok, msg = save_canvas(box, fn, rewrite, true, ask_confirm_and_do)
  if not ok then
    return w:error(v2s(msg))
  end
  update_ui_item(box.item, box.item, fn)
  w:say(v2s(msg))
end

--
-- load current canvas
--
---@param w Cmd4Lua
function M.cmd_load(w)
  log.debug("cmd_canvas_load")

  w:v_opt_quiet('-q')
  local fn = w:desc('filename from which to load the canvas')
      :opt('--file', '-f')
  local yes = w:desc('confirm replace old canvas by loaded')
      :has_opt('--yes', '-y')
  local edit = w:desc('open loaded canvas in the editor')
      :has_opt('--edit', '-e')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local old_box, idx, list = get_canvas_box(bufnr, lnum, lnum, col, col)
  local box = nil

  log.debug('bufn:%s ln:%s idx:%s/%s old_box:(%s) yes:%s',
    bufnr, lnum, idx, #(list or E), box, yes ~= nil)

  if old_box then
    -- just reload from same filename
    local reload = old_box.filename and (old_box.filename == fn or not fn)
    if reload then
      fn = old_box.filename
    end
    log.debug('for existed box: yes: %s reload:%s fn:%s', yes, reload, fn)

    local msg = fmt('Replace existed %s(%s) from fn: %s?',
      v2s(old_box.item), v2s(old_box.filename), v2s(fn))

    if reload or yes or ask_confirm_and_do(msg) then
      assert(idx, 'index of canvas in map')
      local loaded, canvas = load_canvas(fn)
      if not loaded or type(canvas) ~= 'table' then
        return w:error(v2s(canvas))
      end

      local old_lnum_end = old_box.lnum_end
      local old_canvas, old_fn = old_box:setItem(canvas, fn)
      box = old_box

      if box.lnum_end ~= old_lnum_end then
        local ln, ln_end = box.lnum, box.lnum_end
        log.debug('resize place for new canvas %s %s', ln, ln_end)
        local st, msg0 = CE.prepare_and_place(box, false)
        if not st then
          old_box:setItem(old_canvas, old_fn) -- rollback
          if conf.DEV_MODE then error(msg0) end
          return w:error(msg0)
        end
      end

      local updated = update_ui_item(old_canvas, canvas, box.filename)
      old_box.has_changes = false
      log.debug('updated canvas instances: %s', updated)
    else
      -- log.debug('an attempt to load a canvas into a occupied space is blocked')
      return w:error('to replace existed canvas use --yes opts')
    end
    --
  else --
    log.debug('place canvas to a free space')

    if not fn then
      return w:error('Use `--file <filename>` to specify where to get it from')
    end
    local can_insert = yes

    local modifiable = is_buf_modifiable(bufnr)
    if not modifiable and not edit then
      return w:error('cannot insert into not modifiable buffer')
    end

    if not can_insert and modifiable then
      local ls, _ = nvim_find_syntax_block_range(bufnr, lnum + 1)
      if not ls then
        if ask_confirm_and_do('insert to current line?') then
          can_insert = true
        else
          return w:error('cannot insert without syntax block')
        end
      end
    end

    local loaded, canvas = load_canvas(fn)
    if not loaded then
      return w:error(v2s(canvas))
    end
    ---@cast canvas env.draw.Canvas
    if modifiable then
      _, box = CE.add_canvas(canvas, bufnr, lnum, col, fn)
      local ok, errmsg = CE.prepare_and_place(box, can_insert)
      if not ok then return w:error(errmsg) end
    else
      w:say('Canvas Loaded from: ', fn)
      openInCanvasEditor(edit, canvas, bufnr, lnum, col, fn)
      return
    end
  end

  if box then
    H.draw(w, box)
    w:say('Canvas Loaded from: ', fn)

    openInCanvasEditor(edit, box.item, bufnr, lnum, col, fn)
  end
end

--
-- place canvas from canvas list into cuttent buffer
--
---@param w Cmd4Lua
function M.cmd_place(w)
  local src_bufnr = w:desc('bufnr in which canvas is stored'):pop():argn()
  local idx = w:desc('index of canvas in the bufnr list')
      :def(1):optn('--index', '-i')

  local keep = w:desc('keep canvas object in the current buff(Or only draw)')
      :has_opt('--keep', '-k')

  if not w:is_input_valid() then return end
  ---@cast src_bufnr number
  ---@cast idx number

  local canvas = state.get_canvas(src_bufnr, idx)
  if not canvas then
    return w:error(fmt(
      'Not Found canvas for bufnr:%s idx:%s', v2s(src_bufnr), v2s(idx)))
  end

  local dst_bufnr, lnum, col = get_editor_coords()

  local ok, box, dst_idx = H.place_canvas(w, canvas, dst_bufnr, lnum, col)

  if not keep then -- to draw only
    remove_ui_item(dst_bufnr, dst_idx, box)
  end

  w:say('placed', ok, box)
end

return M

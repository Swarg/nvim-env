-- 29-04-2024 @author Swarg
-- Goal:
--  - modify the element on create or edit actions
--  - extra "in-the-end-of-string-commands" for advanced features
--    runs after creating a canvas element

local class = require 'oop.class'
local ubuf = require("env.draw.utils.buf");
local usymbols = require("env.draw.utils.unicode_symbols");
local Canvas = require('env.draw.Canvas')

-- local Point = require 'env.draw.Point'
-- local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Rectangle = require 'env.draw.Rectangle'
local Container = require 'env.draw.Container'

local c4lv = require("env.util.cmd4lua_vim")


--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

local M = {}

--
---@param args table{string}
---@param params table{box, elm, input, lnum, col}
function M.handle(args, params)
  return c4lv.newCmd4Lua({ fargs = args, params = params })
      :about('Built-in Element Edit')
      :handlers(M)

      :desc('edit (or create) a rectangle')
      :cmd('rect', 'r')

      :desc('edit line style("color")')
      :cmd('line', 'l')

      :desc('edit point style("color")')
      :cmd('point', 'p')

      :desc('select arrow head for point or line')
      :cmd('arrow', 'a')

      :desc('wrap element to rectangle')
      :cmd('wrap', 'w')

      :desc('interact with Container')
      :cmd('container', 'c')

      :desc('create objects for database design')
      :cmd('database', 'db')

      :run()
      :exitcode()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local E = {}
local fmt = string.format
local instanceof, get_class = class.instanceof, class.get_class

local SELECT_INTERACTIVELY = '\\'

local pick_line_part = usymbols.pick_line_part
local pick_arrow = usymbols.pick_arrow
local pick_symbol = usymbols.pick_symbol
local select_interactively = ubuf.select_interactively

---@param w Cmd4Lua
local function get_params(w)
  return ((w.vars or E).vim_opts or E).params
end

---@param w Cmd4Lua
---@param build_elm boolean?
---@return env.draw.ui.ItemPlaceHolder
---@return env.draw.IEditable
local function box_and_elm(w, build_elm)
  local t = get_params(w) or E
  if build_elm and type(t.elm) == 'function' then
    t.elm = t.elm()
    t.elm_created = true -- mark as an elm created but not added into the canvas
  end
  return t.box, t.elm
end


---@param w Cmd4Lua
---param style string
-- return boolean, hlc, vlc, corners, rlc blc
local function pick_rect_style0(w, style)
  if style == SELECT_INTERACTIVELY then
    style = select_interactively(usymbols.build_help_rect() .. "\nRectStyle: ")
    if not style or style == '' then return end
  end

  local t = usymbols.pick_rect_style(style)

  if type(t) == 'table' then
    return true, t
  else
    w:error('error: ' .. tostring(t))
  end

  return false
end

--
---@param w Cmd4Lua
---param corners_name string?
local function pick_rect_corners(w, corners_name)
  local corners
  if corners_name == 'no' or corners_name == 'n' then
    corners = { '', '', '', '' }
  elseif corners_name == 'arc' or corners_name == 'a' then
    corners = '╭╮╯╰'
  elseif corners_name ~= nil then
    w:error('Unknown corners: ' .. tostring(corners_name))
    return false
  end

  return true, corners
end


--
-- edit (or create) a rectangle
--
---@param w Cmd4Lua
function M.cmd_rect(w)
  w:usage('use help! to show all supported style names')

  local style = w:desc('a style name of rectange edges')
      :def(SELECT_INTERACTIVELY):pop():arg()

  local opt_corners = w:desc('corners one of [no arc]')
      :opt('--corners', '-c')

  if not w:is_input_valid() then return end

  local box, elm = box_and_elm(w, true)

  if not (instanceof(elm, Rectangle) or instanceof(elm, Container)) then
    if (instanceof(elm, Text)) then
      return w:error('Use /wrap or /w to wrap text into a rectange')
    end
    return w:error('Expected Rectangle|Container got: ' .. tostring(elm))
  end

  if opt_corners then -- change only corners
    local ok, corners = pick_rect_corners(w, opt_corners)
    if ok then
      local tstyle = elm:getStyle()
      tstyle.corners = corners
      elm:setStyle(tstyle)
      box:changed()
      return true
    end
  end

  local ok, tstyle = pick_rect_style0(w, style)
  if ok and type(tstyle) == 'table' then
    elm:setStyle(tstyle)
    box:changed()
  end
end

--
-- set line props
--
---@param w Cmd4Lua
function M.cmd_line(w)
  local c = w:desc('a symbol name to fill the line')
      :def(SELECT_INTERACTIVELY):pop():arg()

  if not w:is_input_valid() then return end ---@cast c string

  local _, elm = box_and_elm(w, true)
  if type(elm) ~= 'table' then
    return w:error('Designed for editing an existing line. No found line')
  end
  if rawget(elm, 'inv') then -- container
    return w:error('to edit combined lines use \\rect')
  end

  local color = rawget(elm, 'color')
  if not color then
    return w:error('Line expected, got: ' .. tostring(elm))
  end

  if c == SELECT_INTERACTIVELY then
    c = select_interactively(usymbols.build_help_line_parts() .. "\nLine: ")
    if not c or c == '' then return end
  end

  local part, err = pick_line_part(c)

  if not part then
    return w:error(fmt('Unknown line symbol: "%s" %s', tostring(c), err))
  end
  if #part > 4 then return w:error(part) end -- show help

  elm:setStyle(part)
end

--
-- edit point style("color
--
---@param w Cmd4Lua
function M.cmd_point(w)
  w:usage('\\point help! - to list supported symbols names')
  w:usage('\\point crows-foot-left') --
  w:usage('\\point cfl')

  local c = w:desc('a symbol name to fill the point')
      :def(SELECT_INTERACTIVELY):pop():arg()

  if not w:is_input_valid() then return end ---@cast c string

  local _, elm = box_and_elm(w, true)
  if type(elm) ~= 'table' then
    return w:error('Designed for editing an existing point. No found point')
  end

  local color = rawget(elm, 'color')
  if not color then
    return w:error('Point expected, got: ' .. tostring(elm))
  end

  if usymbols.is_help_query(c) then
    return w:error(usymbols.build_help_symbols(), 2)
  elseif c == SELECT_INTERACTIVELY then
    c = select_interactively(usymbols.build_help_symbols() .. "\nSymbol: ")
    if not c or c == '' then return end
  end

  local p = pick_symbol(c) or pick_arrow(c)

  if not p then
    return w:error(fmt('Unknown point style: "%s"', tostring(c)))
  end

  elm:setStyle(p) -- elm.color = color
end

--
-- select arrow for point or line
--
---@param w Cmd4Lua
function M.cmd_arrow(w)
  local c = w:desc('the name of arrow symbol (or help! or \\ to select)')
      :def(SELECT_INTERACTIVELY):pop():arg()

  if not w:is_input_valid() then return end ---@cast c string

  local _, elm = box_and_elm(w, true)
  local text = false
  if type(elm) ~= 'table' then
    return w:error('Designed for editing an existing element.')
  end

  local color = rawget(elm, 'color')
  if not color then
    text = rawget(elm, 'text')
    if not text then
      return w:error('Point|Line|Text expected, got: ' .. tostring(elm))
    end
  end

  if usymbols.is_help_query(c) then
    return w:error(usymbols.build_help_arrow(), 2)
  end

  if c == SELECT_INTERACTIVELY then -- enter interactive selection mode
    c = select_interactively(usymbols.build_help_arrow() .. "\nArrowName: ")
    if not c or c == '' then return end
  end

  local p = pick_arrow(c)

  if not p then
    return w:error(fmt('Unknown arrow: "%s"', tostring(c)))
  end

  if color then
    ---@cast elm env.draw.Point -- or Line
    elm.color = p
  elseif text then
    -- todo add support utf8 for Text
    ---@cast elm env.draw.Text
    elm.text = elm.text .. p
  end
end

-------------------------------------------------------------------------------

--
-- Wrap a new created element(Text) into rectangle
-- shortcut t: Some text\w double --distance 2
-- shortcut t: Some text\w 2
--
---@param w Cmd4Lua
function M.cmd_wrap(w)
  local style = w:desc('a style name of rectange edges'):optional():pop():arg()

  local offy_up = w:desc('height to up'):def(1):optn('--off-y-up', '-u')
  local offy_down = w:desc('height to down'):def(1):optn('--off-y-down', '-d')

  local offx_left = w:desc('width to left'):def(2):optn('--off-x-left', '-l')
  local offx_right = w:desc('width to right'):def(2):optn('--off-x-right', '-r')
  local dist = w:desc('distance between text and edges'):optn('--distance', '-d')

  if not w:is_input_valid() then return end ---@cast style string

  local box, elm = box_and_elm(w, true)
  local canvas = Canvas.cast((box or E).item)

  if not elm then
    return w:error('No element to wrap')
  end

  -- first arg can be style or dist with text and rectange edges
  if style and type(dist) ~= 'number' and tonumber(style) then
    dist = tonumber(style)
    style = nil
  end

  if dist and dist > 0 then
    offy_up = offy_up + dist
    offy_down = offy_down + dist
    offx_left = offx_left + dist
    offx_right = offx_right + dist
  end

  local x1, y1, x2, y2 = elm:get_coords()
  x1, x2 = x1 - offx_left, x2 + offx_right
  y1, y2 = y1 - offy_up, y2 + offy_down

  local hlc, vlc, corners
  local t = style and usymbols.pick_rect_style(style) or nil
  if t then
    hlc, vlc, corners = t.hl, t.vl, t.corners
  else
    hlc, vlc = Rectangle.DEFAULT_HLINE, Rectangle.DEFAULT_VLINE
    corners = Rectangle.DEFAULT_CORNERS
  end

  local r = Rectangle:new(nil, x1, y1, x2, y2, hlc, vlc, corners)
  -- canvas:insertBefore(elm, r)
  canvas:add(r)
  canvas:add(elm)
end

--------------------------------------------------------------------------------

local C = {}
--
-- interact with Container
--
---@param w Cmd4Lua
function M.cmd_container(w)
  w:about('Modify the Selected Lines in shell output')
      :handlers(C)

      :desc('join selected elements into new container')
      :cmd('join', 'j')

      :desc('extract(unpack) elements from container to canvas')
      :cmd('extract', 'e')

      :run()
end

--
-- join selected elements into new container
--
---@param w Cmd4Lua
function C.cmd_join(w)
  local limit = w:desc('limit of elements to join'):def(99)
      :optn('--limit', '-l')

  if not w:is_input_valid() then return end

  local box, _ = box_and_elm(w)
  local p = get_params(w)

  if ubuf.is_visual_mode() then
    local elms, idxs = box:get_objects_at(p.lnum, p.col, p.lnum2, p.col2, limit)

    if elms and next(elms) then
      local index, moved, cont = box.item:join_to_container(elms, idxs)
      print('joined:', moved, 'to', cont, 'container index:', index)
    end
  else
    print('only for selected range in visual bock mode')
  end
end

--
-- extract(unpack) elements from container to canvas
--
---@param w Cmd4Lua
function C.cmd_extract(w)
  if not w:is_input_valid() then return end

  local box, _ = box_and_elm(w)
  local p = get_params(w)
  local elms, _ = box:get_objects_at(p.lnum, p.col, p.lnum2, p.col2, 1)
  local elm = (elms or E)[1]

  if get_class(elm) ~= Container then
    return w:error('expected Container got ' .. tostring(elm))
  end
  ---@cast elm env.draw.Container

  local moved = box.item:unpack_from(elm, elm.inv)
  print('extracted ', moved)
end

--------------------------------------------------------------------------------

--
-- create objects to design database
--
---@param w Cmd4Lua
function M.cmd_database(w)
  require("env.draw.command.builtin_db").handle(w)
end

return M

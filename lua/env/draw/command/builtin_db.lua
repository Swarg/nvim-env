-- 05-05-2024 @author Swarg
--
-- Goals:
-- - helper for creating compound elements for database crowsfoot notation
-- - called via cli at the time of Text-element creation via the command at
--   the end of the line aka \db in the CanvasEditor
--

local log = require('alogger')
local class = require 'oop.class'
local ucb = require 'env.util.clipboard'
local conf = require 'env.draw.settings'
-- local Container = require 'env.draw.Container'
local IEditable = require 'env.draw.IEditable'
local ubuf = require "env.draw.utils.buf"
local Entity = require "env.draw.notation.db.Entity"
local Attribute = require "env.draw.notation.db.Attribute"
local Relationship = require "env.draw.notation.db.Relationship"
local dbn = require "env.draw.notation.db.base"
-- local utbl = require 'env.draw.utils.tbl'

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

---@param w Cmd4Lua
function M.handle(w)
  w:about('helper for database notation')
      :handlers(M)

      :desc('create entity with properties by given params')
      :cmd('entity', 'e')

      :desc('interact with entity attributes')
      :cmd('attribute', 'a', M.sub_cmd_attribute)

      :desc('interact with relationship')
      :cmd('relationship', 'r', M.sub_cmd_relationship)

      :desc('Relation(Logic Model)')
      :cmd('relation', 'R', M.sub_cmd_relation)

      :cmd('debugging', 'dbg')

      :run()
end

local A = {} -- sub cmds for entity attribute
---@param w Cmd4Lua
function M.sub_cmd_attribute(w)
  w:handlers(A)

      :desc('add attribute to entity')
      :cmd('add', 'a')

      :desc('remove attribute from entity')
      :cmd('remove', 'rm')

      :desc('edit entity attribute')
      :cmd('edit', 'e')

      :desc('add bunch of attributes to entity')
      :cmd('add-list', 'al')

      :run()
end

local R = {}
---@param w Cmd4Lua
function M.sub_cmd_relationship(w)
  w:handlers(R)

      :desc('add new the relationship')
      :cmd('new', 'n')

      :desc('remove the relationship')
      :cmd('remove', 'rm')

      :desc('edit the relationship')
      :cmd('edit', 'e')

      :run()
end

--
-- Relation(Logic Model)
--
local RM = {}
---@param w Cmd4Lua
function M.cmd_relation(w)
  w:handlers(RM)

      :desc('convert current entity into relation')
      :cmd('from-entity', 'fe')

      :desc('convert current entity into relation and build empty table')
      :cmd('mk-table', 'mt')

      :run()
end

local DBG = {}
---@param w Cmd4Lua
function M.cmd_debugging(w)
  w:handlers(DBG)

      :desc('todo')
      :cmd('nop')

      :run()
end

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local log_debug = log.debug
local instanceof = class.instanceof
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local normalize = IEditable.normalize

local is_relationship_end = dbn.is_relationship_end
local is_valid_relationship_name = dbn.is_valid_relationship_name
local add_simplified_attrs = dbn.add_simplified_attrs

---@param w Cmd4Lua
---@return table{box:env.draw.ui.ItemPlaceHolder}
local function get_params(w)
  return ((w.vars or E).vim_opts or E).params
end

--
-- from CE.do_edit action params
-- the instance of the object being edited is passed through the params
-- but this does not happen on CE.do_open_consle
--
---@param klass table
---@param w Cmd4Lua
---@return env.draw.IEditable?
local function get_element_only(klass, w, find)
  local params = get_params(w) or E
  log_debug('get_element_only elm:%s class:%s', params.elm, klass ~= nil)
  assert(type(klass) == 'table', 'klass')

  if instanceof(params.elm, klass) then
    return params.elm
    --
  elseif find then
    -- console called without providing the current element
    -- this works only for "do_edit" action.
    -- Try to find the element with given klass
    local lnum, col = params.lnum, params.col
    local lnum2, col2 = params.lnum2, params.col2
    local limit = conf.SELECT_LIMIT
    local elms, _ = params.box:get_objects_at(lnum, col, lnum2, col2, limit)

    if elms and next(elms) then
      for _, elm in ipairs(elms) do
        if instanceof(elm, klass) then
          return elm
        end
      end
    end
  end
  return nil
end

--
-- take passed entity already taken from the current possition of the cursor
-- if instead entity caller passed attribute and current coords when try to
-- find the entity to given attribute(IEditable)
--
---@return env.draw.notation.db.Entity
local function get_entity(w)
  local params = get_params(w)
  local box, elm = params.box, params.elm ---@cast box env.draw.ui.ItemPlaceHolder

  if (not elm or not instanceof(elm, Entity)) then
    local x, y = params.x, params.y
    if x and y then
      elm = (box.item:get_objects_at(x, y, x, y) or E)[1]
    elseif params.lnum and params.col then
      local t = params
      elm = (box:get_objects_at(t.lnum, t.col, t.lnum, t.col, 1) or E)[1]
    else
      error('no current coords in params. expected lnum:col or x:y')
    end
  end

  if instanceof(elm, Entity) then
    return elm
  end

  w:error('Not Found Entity ' .. v2s(class.name(elm)))

  ---@diagnostic disable-next-line: return-type-mismatch
  return nil
end

local function get_all_entities(w)
  local params = get_params(w)
  local box = params.box

  local elms = box.item.objects or {}
  local t = {}
  for _, elm in pairs(elms) do
    if (instanceof(elm, Entity)) then
      t[#t + 1] = elm
    end
  end
  return t
end
--
-- redraw Canvas and mark box as changed (needs save)
---@param w Cmd4Lua
local function redraw(w, flag)
  if flag then
    local box = get_params(w).box
    if box then
      box:changed()
      box:draw()
    end
  end
end


--
---@param w Cmd4Lua
local function get_edit_cords(w)
  local t = get_params(w)
  local lnum, col, lnum2, col2 = normalize(t.lnum, t.col, t.lnum2, t.col2)
  return lnum, col, lnum2, col2
end




--
--
--

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

-- {"attr_name", "attr_domain", "attr_props;" "attr2_name", ...}
--                                         ^ attrs separator(; or ,)
-- -a [ Date date M ; Name name M] -> ";"
---@param w Cmd4Lua
---@param attrs table?
---@param ename string?
---@return table?
local function parse_attributes(w, attrs, ename)
  if attrs and next(attrs) then
    local ok, t = dbn.CLI.parse_attributes(ename, attrs)
    if not ok or type(t) ~= 'table' then
      return w:error(t)
    end
    return t
  end
end


--
-- create new entity with properties by given params
--
---@param w Cmd4Lua
function M.cmd_entity(w)
  w:usage('entity new Order --attributes [Data data M ; Name name O]')
  w:usage('entity new Person -a [id; Name name M]')
  w:usage('entity new Major -a [id, Department, MajorName, TotalCreditsRequired]')
  -- w:usage('entity new Position -t table --columns [Data data M ; Name name O]')

  local name = w:tag('name'):desc('a name of the new entity'):pop():arg()
  local etype_name = w:desc('an entity type (strong|weak|association|table)')
      :def('strong'):opt('--type', '-t')

  local attrs = w:desc('attributes'):optl('--attributes', '-a')

  local simplified_attrs = w:desc('simplified attrs - empty atype and aprops')
      :optl('--simplified-attributes', '-s')

  if not w:is_input_valid() then return end ---@cast name string

  local params = get_params(w)

  -- box size
  local lnum, col, lnum2, col2 = get_edit_cords(w)
  local x1, y1 = params.box:toInnerXY(lnum, col)
  local x2, y2 = params.box:toInnerXY(lnum2, col2)
  x1, y1, x2, y2 = normalize(x1, y1, x2, y2)
  log_debug('New Entity (%s) "%s" %s:%s %s:%s', etype_name, name, x1, y1, x2, y2)

  local attrs_list = parse_attributes(w, attrs, name)
  if w:has_error() then return end

  local entity = Entity:new(nil, x1, y1, x2, y2, name, etype_name)
  params.box.item:add(entity)

  --
  log_debug('attrs:', #(attrs_list or E))
  if attrs_list and next(attrs_list) then
    for _, e in ipairs(attrs_list) do
      log_debug('add attr:', e.name, e.atype, e.props, e.pk)
      entity:add_attribute(e.name, e.atype, e.props, e.pk)
    end
  end
  add_simplified_attrs(entity, simplified_attrs)
end

--
-- attribute add
--
-- add to already existed entity the named attribute with domain and props
-- attribute(column) name, domain options
--
---@param w Cmd4Lua
function A.cmd_add(w)
  local name = w:tag('attr-name'):desc('a name of the new attribute'):pop():arg()

  local atype = w:tag('type'):desc('a type(domain) of the attribute')
      :optional():pop():arg()
  local opts = w:tag('opts'):desc('a options of the new attribute')
      :optional():pop():arg()

  local pk = w:desc('a options of the new column')
      :has_opt('--primary-key', '-p')

  if not w:is_input_valid() then return end ---@cast name string
  ---@cast atype string
  ---@cast opts string

  local entity = get_entity(w)
  if w:has_error() then return end

  local attr = entity:add_attribute(name, atype, opts, pk)
  redraw(w, attr ~= nil)
  print(attr)
end

--
-- add bunch of attributes with same domain(type) to the entity
--
---@param w Cmd4Lua
function A.cmd_add_list(w)
  local names = w:desc('a list of attr names to add'):pop():argl()

  local atype = w:desc('one attr-type(domain) for all new attributes')
      :def(''):opt('--type', '-t')
  local opts = w:desc('one props for all new attributes')
      :def(''):opt('--props', '-p')

  --
  local simplified_attrs = w:desc('simplified attrs - empty atype and aprops')
      :optl('--simplified-attributes', '-s')


  if not w:is_input_valid() then return end
  ---@cast names table

  local entity = get_entity(w)
  if w:has_error() then return end

  local pk = false
  if names and next(names) then
    for _, name in ipairs(names or E) do
      entity:add_attribute(name, atype or '', opts or '', pk)
    end
  end

  add_simplified_attrs(entity, simplified_attrs)

  redraw(w, #(names or E) > 0)
end

--
-- attribute remove
--
-- remove entity attribute by given name
-- or inside edit input(into params passed Attribute element)
--
---@param w Cmd4Lua
function A.cmd_remove(w)
  -- return attr only from edit mode
  local attr = get_element_only(Attribute, w, true)
  ---@cast attr env.draw.notation.db.Attribute

  local name = w:tag('name'):desc('a name of the attribute to be deleted')
      :def((attr or E).name or ''):pop():arg()

  if not w:is_input_valid() then return end ---@cast name string

  local entity = get_entity(w)
  if w:has_error() then return end

  if name == '' then
    return w:error('to delete, specify the attribute name ' ..
      '(or call this command in edit mode)')
  end

  local removed_attr = entity:remove_attribute(name)
  redraw(w, removed_attr)
  print('removed: ', removed_attr)
end

--
-- attribute edit
--
-- interactive mode
--
-- edit entity attribute by given attr name
-- multiple raw Text via one-liner cli prompt
--
---@param w Cmd4Lua
function A.cmd_edit(w)
  local old_attr_name = w:desc('name of the attribute to edit'):pop():arg()

  if not w:is_input_valid() then return end ---@cast old_attr_name string

  local entity = get_entity(w)
  if w:has_error() then return end

  local ok, err = dbn.edit_attr_by_name(entity, old_attr_name, ubuf.ask_value)
  if not ok then
    return w:error(tostring(err))
  end
  redraw(w, ok)
end

--------------------------------------------------------------------------------
--                     Relationships (ER-model)
--------------------------------------------------------------------------------


--
-- add the relationship
--
-- create new relationship from marked positions (do_mark_pos)
--
-- 0..1 -----
-- 1..1 ---|-
-- 0..n ----<
-- 1..n ---|<
--
---@param w Cmd4Lua
function R.cmd_new(w)
  w:usage('0..1 1..n')
  w:usage('0..1 1..n RelName --type identical')
  w:usage('RelationName')
  w:usage('supported endpoints for ERM: 0..1 1..1 0..n 1..n for PDM: from to')
  w:usage('supported types: simple identical (-|=)')

  local ep_a = w:desc('(a) start of the relationship'):def('0..1'):pop():arg()
  local ep_b = w:desc('(b) end of the relationship'):def('0..1'):pop():arg()
  local name = w:desc('name of the relationship'):optional():pop():arg()
  -- [simple, identical]
  local rtype = w:desc('relationship type'):def('simple'):opt('--type', '-t')

  local a_role = w:desc('the role of the A-leg in this relationship')
      :opt('--a-role', '-a')
  local b_role = w:desc('the role of the B-leg in this relationship')
      :opt('--b-role', '-b')

  if not w:is_input_valid() then return end
  ---@cast ep_a string
  ---@cast ep_b string

  local params = get_params(w)
  local box = params.box ---@cast box env.draw.ui.ItemPlaceHolder


  -- use current position as end of relationship
  local pos_cnt = box:get_marked_pos_count()

  if pos_cnt == 0 then
    -- create mark position point automaticaly base on position of the cursor
    return w:error('automatic drawing of relationship lines between entities '
      .. 'has not yet been implemented. Use a-key to mark points before this cmd')
  end

  if pos_cnt == 1 then
    -- todo and last marker only if is not at current point nlum col
    local x, y = box:toInnerXY(params.lnum, params.col)
    box:mark_pos(x, y)
    --
  elseif pos_cnt < 2 then
    return w:error(fmt('not enough markers to create a relationship', pos_cnt))
  end

  local plist = box:get_marked_positions()
  assert(type(plist) == 'table', 'expected table markers (pos_list)')

  -- auto bind to the nearest Entity
  local a_ent = Entity.find_closest(box.item, plist[1])
  local b_ent = Entity.find_closest(box.item, plist[#plist])
  local a_ref, b_ref = Entity.getName(a_ent), Entity.getName(b_ent)

  -- auto detect PDM and use another notation (arrow head instead crowsfoot)
  if a_ent and a_ent.etype == dbn.ETYPE.TABLE then
    if not is_relationship_end(ep_a) and is_valid_relationship_name(ep_a) then
      name = ep_a
    end
    ep_a = dbn.RELATIONSHIP.NAME[dbn.RELATIONSHIP.t.from]
    ep_b = dbn.RELATIONSHIP.NAME[dbn.RELATIONSHIP.t.to]
    -- auto pickup Foreign key to ref table from Attribute(Column)
  end

  -- swap input then instead end type first is an Relationship name
  if not is_relationship_end(ep_a) and is_valid_relationship_name(ep_a) then
    name = ep_a
    ep_a = '0..1' -- def
  end
  if not is_relationship_end(ep_b) and is_valid_relationship_name(ep_b) then
    name = ep_b
    ep_b = '0..1' -- def
  end

  if not is_relationship_end(ep_a) then
    return w:error('expected valid endtype for A-leg, got: ' .. v2s(ep_a))
  end
  if not is_relationship_end(ep_b) then
    return w:error('expected valid endtype for B-leg, got: ' .. v2s(ep_b))
  end

  local opts = {
    name = name, a_ref = a_ref, b_ref = b_ref, a_role = a_role, b_role = b_role
  }

  local ok, ret = Relationship.new_from_plist(plist, rtype, ep_a, ep_b, opts)

  if ok and type(ret) == 'table' then
    local elm = ret ---@cast elm env.draw.notation.db.Relationship
    box.item:add(elm)
    box:changed()
    box:clear_marked_pos()
  end

  print(ok, ret)
end

--
-- remove the relationship
--
---@param w Cmd4Lua
function R.cmd_remove(w)
  if not w:is_input_valid() then return end
end

--
-- edit the relationship
--
---@param w Cmd4Lua
function R.cmd_edit(w)
  if not w:is_input_valid() then return end
end

--------------------------------------------------------------------------------
--                      Relation (Logic Model)
--------------------------------------------------------------------------------

--
-- convert current entity into relation
--
---@param w Cmd4Lua
function RM.cmd_from_entity(w)
  if not w:is_input_valid() then return end

  local entity = get_entity(w)
  if w:has_error() then return end

  local relation = dbn.entity_to_relation(entity)
  local s = dbn.relation_tostring(relation)
  ucb.copy_to_clipboard(s)
  w:say('copied: ' .. s)
end

--
-- convert current entity into relation and build empty table
--
---@param w Cmd4Lua
function RM.cmd_mk_table(w)
  local all = w:desc('for all entities in the canvas')
      :has_opt('--all-entities', '-A')

  local rows = w:desc('rows count'):def(4):optn('--rows', '-r')
  local ttype = w:desc('type of the table'):def('text'):opt('--type', '-t')
  local cwidth = w:desc('width of columns'):optl('--columns-width', '-c')
  local align = w:desc('columns align'):def('center'):opt('--columns-align', '-a')

  if not w:is_input_valid() then return end

  local opts = {
    ttype = ttype,
    rows = rows,
    columns_width = cwidth,
    align_type = align
  }

  local function entity_to_table(entity, opts0)
    local relation = dbn.entity_to_relation(entity)
    return dbn.mk_empty_str_table_for_relation(relation, opts0)
  end

  local s = ''
  if all then
    local ents = get_all_entities(w)
    if w:has_error() then return end
    for _, ent in ipairs(ents) do
      s = s .. entity_to_table(ent, opts) .. "\n"
    end
  else
    local entity = get_entity(w)
    if w:has_error() then return end
    s = entity_to_table(entity, opts)
  end

  ucb.copy_to_clipboard(s)
  w:say('table copied to system buffer sz: ' .. #s)
end

return M

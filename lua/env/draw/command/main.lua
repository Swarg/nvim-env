-- 29-03-2024 @author Swarg
-- Goal:
--  entrypoint to subcommand for interact with canvas
--

-- local log = require('alogger')
local c4lv = require "env.util.cmd4lua_vim"
local state = require "env.draw.state.canvas"
local c4l = require "env.draw.utils.cmd4lua"
--
local m_canvas = require "env.draw.command.canvas"
local m_element = require "env.draw.command.element"
local m_clipboard = require "env.draw.command.clipboard"
local m_sourcecode = require "env.draw.command.sourcecode"
local m_undo = require "env.draw.command.undo"

local M = {}

--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

---@param opts table
function M.handle(opts)
  c4lv.newCmd4Lua(opts)
      :root(':EnvXC')
      :about('Text Canvas for drawing diagrams')
      :handlers(M)

      :desc('interact with canvas in different buffers')
      :cmd('canvas', 'c')

      :desc('access to an element in the canvas')
      :cmd('element', 'e')

      :desc('interact with clipboard of the CanvasEditor')
      :cmd('clipboard', 'cb')

      :desc('access to an deleted selections of canvas elements')
      :cmd('undo', 'u')

      :desc('work with source code of the canvas and elements')
      :cmd('source-code', 'sc')

      :run()
      :exitcode()
end

local cmdtree = {
  canvas = m_canvas.cmdtree,
  element = m_element.cmdtree,
  clipboard = m_clipboard.cmdtree,
  ['source-code'] = m_sourcecode.cmdtree,
  undo = m_undo.cmdtree,
}

local cmpl = c4l.build_complete(cmdtree, 1)
M.opts = { desc = 'nvim-xcanvas', nargs = '*', range = true, complete = cmpl }



--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
--                    Implementation of IStatefullModule

-- for reload
function M.dump_state() state.dump_state() end

function M.restore_state() state.restore_state() end

--------------------------------------------------------------------------------

function M.cmd_canvas(w)
  m_canvas.handle(w)
end

function M.cmd_element(w)
  m_element.handle(w)
end

function M.cmd_clipboard(w)
  m_clipboard.handle(w)
end

function M.cmd_undo(w)
  m_undo.handle(w)
end

function M.cmd_source_code(w)
  m_sourcecode.handle(w)
end

return M

-- 07-04-2024 @author Swarg
--------------------------------------------------------------------------------
-- interact with history of deleted canvas elements(selections)
-- Goal: restore deleted elements back to the canvas
--------------------------------------------------------------------------------

-- local log = require('alogger')
local Canvas = require('env.draw.Canvas')
local ubuf = require('env.draw.utils.buf')
local H = require("env.draw.command.helper")
local CE = require('env.draw.ui.CanvasEditor')
local state = require('env.draw.state.canvas')

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :desc('list of all deleted selections of canvas elements')
      :cmd("list", "ls")

      :desc('restore deleted selection by given index of delhist entry')
      :cmd("restore", "r")

      :desc('clean all deleted elements from the history')
      :cmd("clear-all", "ca")

      :desc('unodo paste - cut latest pasted elements back to clipboard')
      :cmd("unpaste", "up")

      :run()
end

local leaf = 1

M.cmdtree = {
  list = leaf,
  restore = leaf,
  ['clear-all'] = leaf,
  unpaste = leaf,
}

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

--
local get_canvas_or_error = H.get_canvas_or_error
local to_list_of_numbers = H.to_list_of_numbers
local get_editor_coords = ubuf.get_editor_coords
local ask_confirm_and_do = ubuf.ask_confirm_and_do
local get_deleted_readable_list = state.get_deleted_readable_list
local clear_deleted_history = state.clear_deleted_history

--
-- list of all deleted selections of canvas elements
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:v_opt_quiet('-q')
  local cnt = w:desc('end index'):def(40):optn('--count-in-page', '-c')
  local n = w:desc('starts from index'):def(1):optn('--page-number', '-n')

  if not w:is_input_valid() then return end
  ---@cast n number
  ---@cast cnt number

  local s = ''
  local list = get_deleted_readable_list(n, cnt)
  for _, line in ipairs(list) do s = s .. ' ' .. line .. "\n" end
  w:say(s)
end

--
-- cut an element from the canvas by given index
--
---@param w Cmd4Lua
function M.cmd_restore(w)
  w:v_opt_quiet('-q')
  local sidxs = w:desc('selection indexes(one or list) to be restored')
      :pop():argl()
  local keep_position = w
      :desc('keep original position(by default paste under the cursor)')
      :has_opt('--keep-pos', '-p')

  if not w:is_input_valid() then return end

  ---@diagnostic disable-next-line: unused-local
  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  local indexes = to_list_of_numbers(w, sidxs)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  -- in this case it will use the original coordinates before deleting
  if keep_position then lnum, col = -1, -1 end

  for _, idx in ipairs(indexes) do
    local st, msg = CE.restore_deleted(box, idx, lnum, col)
    if st == true then
      w:say(msg)
    else
      w:fsay('fail: %s', tostring(msg))
    end
  end
end

--
-- clean all deleted elements from the history
--
---@param w Cmd4Lua
function M.cmd_clear_all(w)
  w:v_opt_quiet('-q')
  local yes = w:desc('confirm'):has_opt('--yes', '-y')

  if not w:is_input_valid() then return end

  if yes or ask_confirm_and_do('Sure delete all history?') then
    clear_deleted_history()
    w:say('cleaned')
  end
end

--
-- unodo paste - cut latest pasted elements back to clipboard
--
---@param w Cmd4Lua
function M.cmd_unpaste(w)
  if not w:is_input_valid() then return end
  state.unpaste()
end

return M

-- 07-04-2024 @author Swarg
--

-- local log = require('alogger')
local class = require 'oop.class'
local ubase = require("env.draw.utils.base");
-- local state = require('env.draw.state.canvas')
local ubuf = require('env.draw.utils.buf')
local Canvas = require('env.draw.Canvas')
local Point = require 'env.draw.Point'
local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Rectangle = require 'env.draw.Rectangle'
local IPH = require("env.draw.ui.ItemPlaceHolder");
local H = require("env.draw.command.helper")
local CE = require('env.draw.ui.CanvasEditor')
local IEditable = require('env.draw.IEditable')

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)

      :desc('list of all elements and indexes in the canvas')
      :cmd("list", "ls")

      :desc('cut an elements from the canvas by given indexes')
      :cmd("cut-by-index", "x")

      :desc('jump to top-left position of the element by its index')
      :cmd("jump-to", "jt")

      :desc('delete the elements from the canvas by given indexes')
      :cmd("delete-by-index", "del")

      :desc('make given element as backgroud for all elements in it range')
      :cmd("mk-background", "bg")

      :desc('draw indexes at the top left position of each canvas element')
      :cmd("show-indexes", "si")

  --

      :desc('edit the drawbale element under the cursor')
      :cmd('edit', 'e')

      :desc('move the drawbale element under the cursor')
      :cmd('move', 'm')

      :desc('delete the drawbale element under the cursor')
      :cmd('remove', 'rm')

      :desc('join selected elements into new container')
      :cmd('join', 'j')

  -- add new element into canvas
      :desc('create a new element in the canvas')
      :cmd('new', 'n')


  -- debugging

      :desc('inspect the drawbale object under the cursor')
      :cmd('inspect', 'i')

      :run()
end

local leaf = 1

M.cmdtree = {
  list = leaf,
  ['cut-by-index'] = leaf,
  ['jump-to'] = leaf,
  ['delete-by-index'] = leaf,
  ['mk-background'] = leaf,
  ['show-indexes'] = leaf,
  edit = leaf,
  move = leaf,
  remove = leaf,
  join = leaf,
  new = leaf,
}

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

local E, v2s, fmt = {}, tostring, string.format
local get_editor_coords = ubuf.get_editor_coords
local ask_confirm_and_do = ubuf.ask_confirm_and_do
local range2indexes = ubase.range2indexes

local get_canvas_or_error = H.get_canvas_or_error
local say_list = H.say_list


---@param w Cmd4Lua
local function define_opts_top_left_offset(w)
  local top = w:desc('the top position - the offset from a current lnum')
      :def(0):optn('--top', '-t')
  local left = w:desc('the left position - the offset from curren column')
      :def(0):optn('--left', '-l')

  return top, left
end

local NE = {}
function M.cmd_new(w)
  w:handlers(NE)
      :desc('draw a new point')
      :cmd('point', 'p')

      :desc('draw a new line')
      :cmd('line', 'l')

      :desc('draw a new rectangle')
      :cmd('rectangle', 'r')

      :desc('draw a new text')
      :cmd('text', 't')

      :run()
end

-- draw a rectange with given positions
--
---@param w Cmd4Lua
function NE.cmd_rectangle(w)
  w:v_opt_quiet('-q')
  w:v_opt_verbose('-v')
  local top, left = define_opts_top_left_offset(w)

  local height = w:desc('the height of the rectange'):def(4):optn('--height', '-h')
  local width = w:desc('the width of the rectange'):def(16):optn('--width', '-w')

  if not w:is_input_valid() then return end

  if not height or height < 0 then
    return w:error('bad height of the rectange got: ' .. tostring(height))
  end
  if not width or width < 0 then
    return w:error('bad height of the rectange got: ' .. tostring(height))
  end

  local bufnr, lnum, col = get_editor_coords(top, left)
  w:verbose(1, 'editor lnum:col', lnum, col)

  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder
  -- local canvas = Canvas.cast(box.item)

  local r = box:add(Rectangle:new(nil, 0, 0, width - 1, height - 1), lnum, col)
  w:say('New ', r)
  H.draw(w, box)

  if w:is_verbose() then
    local x, y = box:toInnerXY(lnum, col)
    w:say('InCanvasCoords:', x, y)
  end

  return r
end

--
-- draw a new line
--
---@param w Cmd4Lua
function NE.cmd_line(w)
  w:v_opt_quiet('-q')
  local top, left = define_opts_top_left_offset(w)

  local offx = w:desc('the offset by x from the cursor')
      :def(4):optn('--offx', '-x')
  local offy = w:desc('the offset by y from the cursor')
      :def(0):optn('--offy', '-y')

  local color = w:desc('the offset by y from the cursor')
      :def('-'):opt('--color', '-c')

  if not w:is_input_valid() then return end

  if not offx then return w:error('no offset by x ') end
  offy = offy or 0

  local bufnr, lnum, col = get_editor_coords(top, left)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  -- todo auto vertical horizontal
  local l = box:add(Line:new(nil, 0, 0, offx, offy, color), lnum, col)
  w:say('New ', l)
  H.draw(w, box)
  return l
end

--
-- draw a point
--
---@param w Cmd4Lua
function NE.cmd_point(w)
  w:v_opt_quiet('-q')
  local top, left = define_opts_top_left_offset(w)

  local color = w:desc('char to draw a point'):def('+'):opt('--color', '-c')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(top, left)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  -- todo auto vertical horizontal
  local p = box:add(Point:new(nil, 0, 0, color), lnum, col)
  w:say('New ', p)
  H.draw(w, box)
  return p
end

--
-- draw a new label with a text
--
---@param w Cmd4Lua
function NE.cmd_text(w)
  w:v_opt_quiet('-q')
  local text = w:desc('the text'):pop():arg()
  local width = w:desc('limited width'):opt('--width', '-w')
  local height = w:desc('limited height'):opt('--height', '-h')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords(0, 0)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  -- todo vertical? aligment?
  local t = box:add(Text:new(nil, 0, 0, v2s(text), width, height), lnum, col)
  w:say('New ', t)
  H.draw(w, box)
  return t
end

--------------------------------------------------------------------------------

--
-- inspect the drawbale object under the cursor
--
---@param w Cmd4Lua
function M.cmd_inspect(w)
  w:v_opt_quiet('-q')
  w:v_opt_verbose('-v')

  local editor = w:desc('in-editor-inspect'):has_opt('--editor', '-e')
  local as_array = w:desc('show object as array'):has_opt('--as-array', '-A')
  local limit = w:desc('limit number of picked objects under the cursor')
      :optn('--max', '-m')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords(0, 0)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  if editor then
    CE.do_inspect()
    return
  end

  local s = fmt('Position(%s:%s,%s:%s)', v2s(lnum), v2s(col), v2s(lnum2), v2s(col2))
  -- local len = #s
  local list = box:get_objects_at(lnum, col, lnum2, col2, limit or 10)
  local verbose = w:is_verbose()
  local inspect = (vim or E).inspect or require 'inspect'

  for _, o in pairs(list) do ---@cast o env.draw.IEditable
    local info

    if verbose then
      local t = o.toArray and o:toArray() or o
      info = v2s(class.name(o)) .. ' ' .. inspect(t) .. "\n"
    else
      if as_array then -- and not rawget(getmetatable(o), '__tostring') then
        local x, y = box:toInnerXY(lnum, col)
        info = o:fmtSerialized(o:serialize({ cli = true, x = x, y = y }))
        -- info = inspect(class.Object.toArray(o))
      else
        info = o.__tostring(o)
      end
    end
    if #s > 0 then s = s .. "\n" end
    s = s .. info
  end

  w:say(s)
  return s
end

--
-- edit the drawbale object under the cursor
--
---@param w Cmd4Lua
function M.cmd_edit(w)
  w:v_opt_quiet('-q')
  local top, left = define_opts_top_left_offset(w)
  local limit = w:desc('limit number of picked objects under the cursor')
      :def(1):optn('--max', '-m')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords(top, left)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local list = box:get_objects_at(lnum, col, lnum2, col2, limit)
  if not next(list) then
    return w:error('Not Found object in this position')
  end
  local refresh = false

  for _, o in pairs(list) do
    local updated, msg = CE.edit_element(o)
    w:say('updated:', updated, msg)
    if updated == true then refresh = true end
  end

  if refresh then
    H.draw(w, box)
  end
end

--
-- delete the drawbale object under the cursor form canvas
--
---@param w Cmd4Lua
function M.cmd_remove(w)
  w:v_opt_quiet('-q')
  local top, left = define_opts_top_left_offset(w)
  local yes = w:desc('confirm the objects deletion'):has_opt('--yes', '-y')

  local limit = w:desc('limit number of picked objects under the cursor')
      :def(10):optn('--max', '-m')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords(top, left)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local list = box:get_objects_at(lnum, col, lnum2, col2, limit)
  if not next(list) then
    return w:error('Not Found object in this position')
  end

  local removed = 0
  local canvas = Canvas.cast(box.item)
  local x, y = box:toInnerXY(lnum, col)

  for i, elm in pairs(list) do
    local parent = nil
    local prefix = class.name(elm)
    if lnum == lnum2 and col == col2 then -- not a range selection
      local child = elm:get_child_at(x, y)
      if child then
        parent = elm
        elm = child
        prefix = IEditable.mkChildPrefix(parent, elm, x, y)
      end
    end
    local msg = fmt('Delete Element? %s %s', prefix, v2s(elm))
    if yes or ask_confirm_and_do(msg) then
      local fremoved
      if parent then
        fremoved = parent:remove_child(elm) ~= nil
      else
        fremoved = canvas:remove(elm) ~= nil
      end
      w:say('Removed:', fremoved, i, '/', #list)
      removed = removed + 1
    end
  end

  if removed > 0 then
    w:say('Removed elements: ', removed, '/', #(canvas.objects or E))
    H.draw(w, box)
  end
end

--
-- move the drawbale object under the cursor
--
---@param w Cmd4Lua
function M.cmd_move(w)
  w:v_opt_quiet('-q')
  local top, left = define_opts_top_left_offset(w)
  local limit = w:desc('limit number of picked objects under the cursor')
      :optn('--max', '-m')

  local xoff = w:desc('offset by x'):def(0):optn('--offset-x', '-x')
  local yoff = w:desc('offset by y'):def(0):optn('--offset-y', '-y')

  if not w:is_input_valid() then return end

  if xoff == 0 and yoff == 0 then
    return w:say('Nothing is done no offsets by x and y')
  end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords(top, left)
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local list = box:get_objects_at(lnum, col, lnum2, col2, limit)
  if not next(list) then
    return w:error('Not Found object in this position')
  end

  w:say(xoff, yoff)

  local updated = 0
  for _, o in pairs(list) do
    o:move(xoff, yoff)
    updated = updated + 1
  end

  if updated > 0 then
    H.draw(w, box)
  end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


--
-------------------------------------------------------------------------------
-- interact with elements of the canvas
-- Goal: get to elements that cannot be reached through the CanvasEditor
-------------------------------------------------------------------------------


--
-- list of all elements in the canvas
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:v_opt_quiet('-q')
  local cnt = w:desc('end index'):def(40):optn('--count-in-page', '-c')
  local n = w:desc('starts from index'):def(1):optn('--page-number', '-n')

  if not w:is_input_valid() then return end
  ---@cast n number
  ---@cast cnt number

  ---@diagnostic disable-next-line: unused-local
  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local canvas = Canvas.cast(box.item)
  return say_list(w, Canvas.get_elements_readable_list(canvas, n, cnt))
end

--
-- take the element from canvas by given index to top-left
--
-- UseCase: it is impossible to get to the element in the usual way:
-- useful when an element is either not visible on the canvas or goes beyond
-- the boundaries of the canvas
--
---@param w Cmd4Lua
function M.cmd_cut_by_index(w)
  w:v_opt_quiet('-q')
  w:v_opt_dry_run('-d')
  w:usage('elements cut-by-index 1,2,3')
  w:usage('elements x 1,2,4-5,>16')

  local sindexes = w:desc('one or more indexes of the elements to be cut')
      :pop():arg()

  if not w:is_input_valid() then return end ---@cast sindexes string

  ---@diagnostic disable-next-line: unused-local
  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  local max = IPH.maxElementIndex(box)
  local indexes = range2indexes(sindexes, max)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  if w:is_dry_run() then
    w:fsay('bufnr:%s lnum:%s lnum2:%s\n indexes:%s (max:%s)',
      bufnr, lnum, lnum2, vim.inspect(indexes), max)
    return
  end

  local ok, selection = CE.cut_element_by_idx(box, indexes)
  if not ok then
    w:say('fail:', selection)
  else
    w:say(tostring(selection))
  end
end

--
-- delete the given element from canvas
--
---@param w Cmd4Lua
function M.cmd_delete_by_index(w)
  w:v_opt_quiet('-q')
  w:usage('elements del 1,2,4-5,>16')

  local sidxs = w:desc('one index or list of indexes of the elements to delete')
      :pop():arg()

  local yes = w:desc('confirm an element deletion'):has_opt('--yes', '-y')
  local no_undo = w:desc('with no ability to restore via undo')
      :has_opt('--no-undo', '-U')

  if not w:is_input_valid() then return end ---@cast sidxs string

  ---@diagnostic disable-next-line: unused-local
  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)
  local indexes = range2indexes(sidxs, IPH.maxElementIndex(box))

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local function confirm_cb(list)
    local info = ''
    for _, el in ipairs(list) do
      info = info .. tostring(el) .. "\n"
    end
    return yes or ask_confirm_and_do('Delete Elements? ' .. info)
  end

  local st, ret = CE.delete_element_by_idx(box, indexes, confirm_cb, no_undo)

  if not st or type(ret) == 'string' then
    w:say(ret)
  else
    w:say(tostring(ret))
    H.draw(w, box)
  end
end

--------------------------------------------------------------------------------

--
-- jump to top-left position of the element by its index
--
---@param w Cmd4Lua
function M.cmd_jump_to(w)
  local idx = w:tag('index'):pop():argn()

  if not w:is_input_valid() then return end

  local bufnr, lnum, col, _, _ = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local e = (box.item.objects or E)[idx or false]
  if not e then
    return w:error('Not Found element with index: ' .. v2s(idx))
  end
  ---@cast e env.draw.IEditable
  local x, y = e:pos()
  local elnum, ecol = box:toOuterLnumCol(x, y)
  ubuf.set_cursor_pos(0, elnum + 1, ecol)
  w:say(elnum, ecol)
end

--
-- make given element as backgroud for all elements in it range
--
---@param w Cmd4Lua
function M.cmd_mk_background(w)
  w:v_opt_verbose('-v')
  local sexclude = w
      :desc('indexes that should not be taken into account during this operation')
      :opt('--exclude-indexes', '-e')

  if not w:is_input_valid() then return end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local elms, idxs = box:get_objects_at(lnum, col, lnum2, col2, 1)

  local exclude_cb = function(canvas, elms0, idxs0)
    local exclude = ubase.range2indexes(sexclude, #canvas.objects)
    local nelms, nidxs = ubase.exclude_elms(exclude, elms0, idxs0)
    return nelms, nidxs
  end

  local st, msg = Canvas.cast(box.item)
      :mkElementBackgroud(elms, idxs, exclude_cb, w:is_verbose())

  if st then
    w:say(msg)
  else
    w:error(msg)
  end
end

--
-- draw indexes at the top left position of each canvas element
--
---@param w Cmd4Lua
function M.cmd_show_indexes(w)
  if not w:is_input_valid() then return end

  local bufnr, lnum, col = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local enabled = CE.do_toggle_display_indexes(box)
  print('display indexes enabled: ', enabled)
end

--
-- join selected elements into container
-- TODO fix.
-- for a reason that is not yet clear, the selection is reset when entering the
-- command and before entering this code
--
---@param w Cmd4Lua
function M.cmd_join(w)
  local limit = w:desc('limit of elements to join'):def(99)
      :optn('--limit', '-l')

  if not w:is_input_valid() then return end

  -- selection is reset when entering a command from the console
  -- back prev selected range
  local vm = ubuf.is_visual_mode()
  if not vm then
    vim.cmd([[normal! gv]])
  end

  local bufnr, lnum, col, lnum2, col2 = get_editor_coords()
  local box = get_canvas_or_error(w, bufnr, lnum, col, Canvas)

  if w:has_error() then return end ---@cast box env.draw.ui.ItemPlaceHolder

  local elms, idxs = box:get_objects_at(lnum, col, lnum2, col2, limit)

  if not next(elms) then
    local a = fmt('%s:%s-%s:%s vm:%s', lnum, col, lnum2, col2, v2s(vm))
    return w:error('Not Found elements in selected area: ' .. a)
  end
  local canvas = Canvas.cast(box.item)
  local container_index, moved = canvas:join_to_container(elms, idxs)

  print('container_index:', container_index, 'moved:', moved)
end

--------------------------------------------------------------------------------

if _G.TEST then
  M.NE = NE
end
--
return M

-- 07-04-2024 @author Swarg
local M = {}
--

local log = require('alogger')
local class = require 'oop.class'
local Canvas = require('env.draw.Canvas')
local state = require('env.draw.state.canvas')
local ubuf = require('env.draw.utils.buf')
local CE = require('env.draw.ui.CanvasEditor')

--

local v2s, fmt = tostring, string.format
local instanceof = class.instanceof
-- local draw_item = CE.draw_item
local get_actual_bufnr = ubuf.get_actual_bufnr
local get_canvas_box = state.get_canvas_box
local get_local_canvas_list = state.get_local_canvas_list
local getReadableItemPlace = state.getReadableItemPlace
local getCanvasName = state.getCanvasName

---@param w Cmd4Lua
---@param bufnr number?
---@param lnum number
---@param col number?
---@param cls oop.Object? -- class
---@return env.draw.ui.ItemPlaceHolder? of env.draw.Canvas
---@return number?
function M.get_canvas_or_error(w, bufnr, lnum, col, cls)
  bufnr = get_actual_bufnr(bufnr)
  log.debug("get_canvas_or_error bufnr:%s lnum:%s col:%s", bufnr, lnum, col)

  local box, idx = get_canvas_box(bufnr, lnum, lnum, col, col)
  if not box then
    local list = get_local_canvas_list(bufnr)
    local details
    if not next(list) then
      details = ' There are no canvases in the current buffer.'
    else
      local cnt = #list
      details = fmt(' There are %s canvases in the current buffer ', v2s(cnt))
      if cnt == 1 and list[1] then
        details = details .. ':(' .. getReadableItemPlace(list[1]) .. ')'
      end
    end
    w:error(fmt('Not Found Canvas for bufnr:%s lnum:%s %s',
      v2s(bufnr), v2s(lnum), details))
    return nil, nil
    --
  elseif not box.item then
    w:error('Box without item!')
    return nil, nil
  elseif cls and class.name(cls) ~= class.name(box.item) then -- not instanceof(box.item, cls) then
    w:error(fmt('Unexpected class got:%s Expected:%s',
      v2s(class.name(box.item)), v2s(class.name(cls))))
    return nil, nil
  end

  return box, idx
end

---@param list table
---@param w Cmd4Lua
function M.say_list(w, list, sep)
  local s = ''
  if not w:is_quiet() then
    sep = sep or "\n"
    if list and next(list) then
      for _, line in ipairs(list) do s = s .. ' ' .. line .. sep end
    end
    w:say(s)
  end

  return list
end

--
-- Create descending list of given numbers
-- todo into cmd4lua
--
---param slist table?{string}
-- local function descending_nums(w, slist)
--   local t = {} -- indexes
--
--   if slist then
--     for _, s in ipairs(slist) do
--       t[#t + 1] = tonumber(s)
--     end
--   end
--
--   table.sort(t, function(a, b) return a > b end) -- descending
--   if #t == 0 then
--     w:error('Expected not an empty list of numbers, got: slist:' .. v2s(slist))
--   end
--   return t
-- end

function M.to_list_of_numbers(w, slist)
  local t = {} -- indexes

  if slist then
    for _, s in ipairs(slist) do
      t[#t + 1] = tonumber(s)
    end
  end

  -- table.sort(t, function(a, b) return a > b end) -- descending
  if #t == 0 then
    w:error('Expected not an empty list of numbers, got: slist:' .. v2s(slist))
  end
  return t
end

---@param w Cmd4Lua
function M.draw(w, box)
  local ok, errmsg = CE.draw_item_with_check(box, M.ask_value)
  if not ok then w:say(errmsg) end
end

--------------------------------------------------------------------------------


---@return number
function M.create_new_buffer()
  local curbufnr = vim.api.nvim_get_current_buf()
  vim.cmd(":enew")
  --  newbufnr = vimv.api.nvim_create_buf(true, true)
  local newbufnr = vim.api.nvim_get_current_buf()
  assert(curbufnr ~= newbufnr, 'ensure new buffer created')
  return newbufnr
end

---@return boolean
---@return env.draw.ui.ItemPlaceHolder?
---@return number? index
function M.place_canvas(w, canvas, bufnr, lnum, col)
  if ubuf.is_buf_modifiable(bufnr) then
    local idx, box = CE.add_canvas(canvas, bufnr, lnum, col)

    local can_init_new = true
    local ok, errmsg = CE.prepare_and_place(box, can_init_new)
    if not ok then
      state.remove_ui_item(bufnr, idx, box)
      w:error(errmsg)
      return false, nil, nil
    end
    -- if loaded then
    M.draw(w, box)
    --
    return true, box, idx
  end
  -- w:say('cannot place canvas into not modifiable buffer open in editor')
  return false, nil, nil
end

--------------------------------------------------------------------------------

---@param canvas env.draw.Canvas
---@param bufnr number
---@param lnum number
---@param col number
---@param fn string?
---@return number? of buffer with CanvasEditor
function M.openInCanvasEditor(flag, canvas, bufnr, lnum, col, fn)
  if flag then
    assert(instanceof(canvas, Canvas), 'expected Canvas')
    assert(type(bufnr) == 'number', 'bufnr')
    assert(type(lnum) == 'number', 'lnum')
    assert(type(col) == 'number', 'col')

    -- bufnr here used to bound a new buffer for the current one (with a canvas)
    local ctx = {
      canvas = canvas, bufnr = bufnr, lnum = lnum, col = col, filename = fn
    }
    local bufname = (fn or getCanvasName(canvas, bufnr, lnum, col)) .. '_'
    return CE.create_buf(ctx, bufname)
  end
end

function M.buildCanvasFromCodeInBuf(bufnr, lnum, lnum_end)
  bufnr = bufnr or 0
  lnum = lnum or 0
  lnum_end = lnum_end or -1

  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum, lnum_end, false)
  local code = table.concat(lines, "\n")

  local ok, canvas = pcall(Canvas.deserialize_from, code)
  if not ok or type(canvas) ~= 'table' then
    local err = fmt(
      'Cannot deserialize canvas from bufnr:%s: %s', v2s(bufnr), v2s(canvas)
    )
    return false, err
  end
  return true, canvas
end

--
-- open already opened .clt-file in CanvasEditor
--
---@param bufnr number
---@param lnum number?
---@param lnum_end number?
---@return boolean
---@return string|number?
function M.openCodeInCanvasEditor(bufnr, lnum, lnum_end)
  assert(type(bufnr) == 'number', 'bufnr')
  assert(type(lnum) == 'number', 'lnum')
  -- lnum = lnum or 0 --?
  -- bufnr here used to bound a new buffer for the current one (with a canvas)
  local ok, canvas = M.buildCanvasFromCodeInBuf(bufnr, lnum, lnum_end)
  if not ok then
    return false, v2s(canvas) -- errmsg
  end

  local col, fn = 0, vim.api.nvim_buf_get_name(bufnr)
  local ctx = {
    canvas = canvas, bufnr = bufnr, lnum = lnum, col = col, filename = fn
  }

  local bufname = (fn or getCanvasName(canvas, bufnr, lnum, col)) .. '_'
  local new_bn = CE.create_buf(ctx, bufname)
  return true, new_bn
end

return M

-- 07-04-2024 @author Swarg
-- local log = require('alogger')
-- local class = require 'oop.class'
-- local Canvas = require('env.draw.Canvas')
local H = require("env.draw.command.helper")
local state = require('env.draw.state.canvas')

local M = {}
--------------------------------------------------------------------------------
--                              INTERFACE
--------------------------------------------------------------------------------

--
-- interact with clipboard of the CanvasEditor
--
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :desc('list of all objects in the clipboard')
      :cmd("list", "ls")

      :desc('remove all objects from the clipboard')
      :cmd("clear-all", "ca")

      :desc('cut elements by given indexes into clipboard')
      :cmd("cut", "x")

      :desc('take an element from cb at a given number and place it on the canvas')
      :cmd("take", "t")

      :desc('delete the given element from cb')
      :cmd("drop", "d")

      :desc('edit selection offset (the clipboard entry)')
      :cmd("edit-offset", "e")

      :run()
end

local leaf = 1

M.cmdtree = {
  list = leaf,
  ['clear-all'] = leaf,
  cut = leaf,
  take = leaf,
  drop = leaf,
  ['edit-offset'] = leaf,
}

--------------------------------------------------------------------------------
--                           IMPLEMENTATIONS
--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
-- local to_list_of_numbers = H.to_list_of_numbers
local say_list = H.say_list
local get_clipboard_readable_list = state.get_clipboard_readable_list
local get_clipboard = state.get_clipboard
local clear_clipboard = state.clear_clipboard
local clipboard_take = state.clipboard_take
local clipboard_drop = state.clipboard_drop

--
-- list of all objects in the clipboard
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:v_opt_quiet('-q')
  local cnt = w:desc('end index'):def(40):optn('--count-in-page', '-c')
  local n = w:desc('starts from index'):def(1):optn('--page-number', '-n')

  if not w:is_input_valid() then return end

  return say_list(w, get_clipboard_readable_list(n, cnt))
end

--
-- remove all objects from the clipboard
--
---@param w Cmd4Lua
function M.cmd_clear_all(w)
  w:v_opt_quiet('-q')
  if not w:is_input_valid() then return end
  local cb, dfcb = get_clipboard()
  clear_clipboard()
  w:say('cleaned ', #(cb or E), #(dfcb))
end

--
-- take an element from cb at a given number and place it on the canvas
--
---@param w Cmd4Lua
function M.cmd_take(w)
  w:v_opt_quiet('-q')
  local idx = w:desc('index of the element in the cb list'):pop():argn()

  if not w:is_input_valid() then return end ---@cast idx number

  w:say(tostring(clipboard_take(idx)))
end

--
-- delete the given element from cb
--
---@param w Cmd4Lua
function M.cmd_drop(w)
  w:v_opt_quiet('-q')
  local idx = w:desc('index of the element in the cb list'):pop():argn()

  if not w:is_input_valid() then return end ---@cast idx number

  local ok, ret = clipboard_drop(idx)
  if ok and type(ret) == 'table' then
    w:say('Dropped:', tostring(ret))
  else
    w:say(tostring(ret))
  end
end

--
-- edit offset of the selection (the clipboard entry)
--
---@param w Cmd4Lua
function M.cmd_edit_offset(w)
  w:v_opt_quiet('-q')
  local idx = w:desc('index of the entry in the clipboard list'):pop():argn()
  local xoff = w:desc('new xoff for the selection'):def(0):optn('--xoff', '-x')
  local yoff = w:desc('new yoff for the selection'):def(0):optn('--yoff', '-y')

  if not w:is_input_valid() then return end ---@cast idx number

  local selection = state.clipboard_get_entry(idx, true)
  if type(selection) == 'table' then
    selection.xoff = tonumber(xoff) or 0
    selection.yoff = tonumber(yoff) or 0
    w:say(selection)
  else
    w:say('not found clipboard entry by index ' .. v2s(idx))
  end
end

return M

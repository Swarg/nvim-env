-- 05-05-2024 @author Swarg
-- Goal:
-- - Container to hold another elements
--   to represent a certain set of simple elements as one entity
--

local class = require 'oop.class'
local log = require 'alogger'

local Line = require 'env.draw.Line'
local Point = require 'env.draw.Point'
local Rectangle = require 'env.draw.Rectangle'
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'
local ugeo = require 'env.draw.utils.geometry'
-- local conf = require 'env.draw.settings'
local inspect = require "inspect"


class.package 'env.draw'
---@class env.draw.Container : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, x1:number, y1:number, x2:number, y2:number, elements:table?) : env.draw.Container
---@field x1 number
---@field y1 number
---@field x2 number
---@field y2 number
---@field inv table<env.draw.IEditable>  -- inventory child elemnts private
---@field inventory fun(self): table<env.draw.IEditable>  -- inventory of childs
-- IEditable:
---@field setStyle fun(self, ...)
---@field getStyle fun(self): nil
---@field serialize fun(self, table?): table
---@field deserialize fun(self, table, table?): env.draw.Container
---@field cutoff fun(self, x1, y1, x2, y2, t:table?): boolean, table?
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local Container = class.new_class(nil, 'Container', {
}, --[[ implements ]] IDrawable, IEditable)

local ORDERED_KEY_LIST = {
  { x1 = 'number' },
  { y1 = 'number' },
  { x2 = 'number' },
  { y2 = 'number' },
  { inv = 'table' },
  { w = 'number', _def = IEditable.get_width }, -- phantom
  { h = 'number', _def = IEditable.get_height },
}

local E = {}
local v2s, fmt = tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_trace, log_debug = log.trace, log.debug
local instanceof, get_class = class.instanceof, class.get_class
local shortClassName = class.get_short_class_name
local classForName = class.forName

local SH = class.serialize_helper
local add_class_ref = SH.add_class_ref
local get_class_name = SH.get_class_name
local invert_map = SH.invert_map
local Patch = IEditable.cli().Patch
local mkpatch = IEditable.cli().TESTING.mk_patch

-- constructor used in Container:new()
---@param elms table
function Container:_init(x1, y1, x2, y2, elms)
  x1 = x1 or self.x1 or 0
  y1 = y1 or self.y1 or 0
  x2 = x2 or self.x1 or 0
  y2 = y2 or self.y2 or 0
  self.inv = elms or self.inv or {}

  self.x1, self.y1, self.x2, self.y2 = IEditable.normalize(x1, y1, x2, y2)
end

---@return table
function Container:inventory()
  self.inv = self.inv or {}
  return self.inv
end

-- function get_coords(self) return self.x1, self.y1, self.x2, self.y2 end



---@param self self
---@param elm env.draw.IEditable
function Container:indexof(elm)
  local inv = self:inventory()
  if elm and inv then
    for index, value in ipairs(inv) do
      if value == elm then
        return index
      end
    end
  end
  return -1
end

-- find index of element by isInsidePos from end to head
-- the latter are closer to the foreground, that is, they
-- overlap the first ones in the list
---@param x number
---@param y number
function Container:indexof_by_pos(x, y, index_back_offset)
  local inv = self:inventory()
  if x and y and inv then
    index_back_offset = index_back_offset or #inv
    for index = #inv, 1, -1 do
      local elm = inv[index]
      if index <= index_back_offset then
        local inside = elm:isInsidePos(x, y)
        if inside then
          return index
        end
      end
    end
  end

  return -1
end

local indexof = Container.indexof
-- local is_func = function(v) return type(v) == 'function' end


---@param elm env.draw.IEditable
---@return env.draw.IEditable
function Container:add(elm)
  if elm and indexof(self, elm) == -1 then
    local inv = self:inventory()
    inv[#inv + 1] = elm
  end

  return elm
end

---@param elm env.draw.IEditable
---@return env.draw.IEditable?
function Container:remove(elm)
  local inv = self:inventory()
  if inv then
    local i = indexof(inv, elm)
    if i > 0 then
      return table.remove(inv, i)
    end
  end
  return nil
end

--
-- no elements in inv
--
---@return boolean
function Container:isEmpty()
  return #(self:inventory() or E) == 0
end

function Container:invSize()
  return #(self:inventory() or E)
end

---@param elms table{env.draw.IEditable}
---@return table
function Container:unpack(elms)
  -- todo check in inv
  return elms
end

--
-- find element from inventory by given params (x:y-position)
--
---@param klass table?  use nil to any class
---@param x number
---@param y number
---@param opts table?{tag, vertex_only, limit, offset}
---@param t table? -- outtable
--- tag string? -- '*' to any
--- vertex_only boolean? -- it false use intersects if true - check vertex
--- limit number?
--- offset number?
function Container:find_by_xy(klass, x, y, opts, t)
  local inv = self:inventory()
  return ugeo.find_by_xy(inv, klass, x, y, opts, t)
end

--------------------------------------------------------------------------------

--
---@return string
---@param verbose boolean?
---@param tabn number?
function Container:__tostring(verbose, tabn)
  if self then
    local x1, y1, x2, y2 = self:get_coords()
    local w, h = IEditable.get_width_n_height(x1, y1, x2, y2)
    local inv = self:inventory()
    local inv_size = #(inv or E)
    local classname = shortClassName(self) or 'Container'

    local s = string.format('%s (%s:%s %s:%s) (%sx%s) elms: %s', classname,
      v2s(x1), v2s(y1), v2s(x2), v2s(y2), v2s(w), v2s(h), inv_size)

    if self._tag and self._tag ~= '' then
      s = s .. ' ' .. v2s(self._tag)
    end

    if verbose and inv and next(inv) then
      s = s .. " inv:\n"
      local tabs = ''
      if tabn and tabn > 0 then
        tabs = string.rep(' ', tabn, '')
      end
      for i, elm in ipairs(inv) do
        local line
        if elm.__tostring then
          line = elm.__tostring(elm, verbose, (tabn or 0) + 2)
        else
          line = tostring(elm)
        end
        s = s .. fmt("%s%2s: %s\n", tabs, i, line)
      end
    end

    return s
  end
  return 'nil'
end

-- to support edit child element in container via CanvasEditor.do_edit
---@param x number
---@param y number
---@return env.draw.IEditable?
---@return any
function Container:get_child_at(x, y)
  assert(type(x) == 'number', 'expected number x')
  assert(type(y) == 'number', 'expected number y')

  local elm, elm_idx = nil, self:indexof_by_pos(x, y)

  if elm_idx > 0 then
    elm = self:inventory()[elm_idx]
    return elm, elm_idx
  end
  log_trace('child at x:%s y:%s  elm_idx:%s %s', x, y, elm_idx, elm)
  return elm, elm_idx
end

function Container:remove_child(elm)
  if elm then
    local pos = self:indexof(elm)
    if pos then
      return table.remove(self.inv, pos)
    end
  end
  return nil
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layer number?
---@param canvas env.draw.Canvas
function Container:draw(canvas, layer)
  local inv = self:inventory()

  if canvas and inv then
    for _, elm in ipairs(inv) do
      elm:draw(canvas, layer)
    end
  end
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Container:orderedkeys() return ORDERED_KEY_LIST end

---@param patch table  mkpatch(obj,{x1, y1, x2, y2, hline, vline, corners, bg})
---@return boolean
---@return table box with details
function Container:edit(patch)
  local has_changes = Patch.has_changes(patch)
  log_trace("Container:edit has_changes:%s patch:%s", has_changes, patch)
  if not has_changes then
    return false, patch
  end

  -- local data = Patch.get_data(patch) or E -- serialize_opts -- used to serialized patch

  IEditable.apply_patch_to_coords_4v(self, patch)

  -- inventory of child elements
  --
  local new_inv, inv_changed = Patch.get_new_value(patch, 'inv')
  if inv_changed and new_inv then
    assert(type(new_inv) == 'table', 'expected table new_inv')
    local inv = self:inventory()
    assert(#inv == #new_inv, 'expected same size of old and new inventory')

    for i, serialized_elm in ipairs(new_inv) do
      local elm = inv[i]
      log_debug('%s(%s) elm:%s t:%s', i, type(i), elm, serialized_elm)
      if not elm then
        error('unsync not element with index ' .. v2s(i))
      end
      local patch0 = mkpatch(elm, serialized_elm)
      local updated0, _ = elm:edit(patch0)
      log_debug('elm updated:', updated0)
      if updated0 then
        has_changes = true
        -- IEditable.merge_update_report(box, box0, 'inv', i) --deprecated
      end
    end
    Patch.set_updated_risky(patch, 'inv')
  end

  return has_changes, patch
end

--
-- to change lines style(chars to draw)
--
---@param style table{hl, vl, corners, rl?, bl?}
---@diagnostic disable-next-line: unused-local
function Container:setStyle(style)
  local inv = self:inventory()
  if inv and next(inv) and type(style) == 'table' then
    --
    if not instanceof(inv[1], Line) then
      -- supported only for combined lines
      return
    end

    local get_corner_dir = ugeo.get_corner_rect_direction_xy
    -- combined line from lines and points
    for _, elm in ipairs(inv) do
      local klass = get_class(elm)

      if klass == Line then
        local w, h = elm:size()
        local c = '#'

        if (w > 1 and h == 1) then
          c = style.hl or Rectangle.DEFAULT_HLINE or '-'
        elseif (w == 1 and h > 1) then
          c = style.vl or Rectangle.DEFAULT_VLINE or '|'
        end
        elm:setStyle(c)
        --
      elseif klass == Point then
        local tag = elm:tag()
        if not tag then
          local lines = self:find_by_xy(Line, elm.x, elm.y, {
            tag = false, limit = 2, vertex_only = true,
          })
          if #(lines or E) == 2 then
            local dc, cx, cy = get_corner_dir(lines[1], lines[2], elm.x, elm.y)
            log_trace('found dc:%s at (%s:%s) for point at(%s:%s)',
              dc, cx, cy, elm.x, elm.y)
            if dc then
              local c = ugeo.rect_char_by_direction(style, dc)
              elm:setStyle(c)
            end
          end
        end
      end
    end
  end
end

function Container:getStyle()
  return nil
end

---@param offx number
---@param offy number
function Container:move(offx, offy)
  assert(type(offy) == 'number', 'offy')
  assert(type(offx) == 'number', 'offx')
  if offx == 0 and offy == 0 then
    return
  end

  local x1, y1, x2, y2 = self:get_coords()
  log_trace('move offset(%s:%s) before(%s:%s %s:%s)', offx, offy, x1, y1, x2, y2)

  self:set_coords(self.x1 + offx, self.y1 + offy, self.x2 + offx, self.y2 + offy)

  local inv = self:inventory()
  for _, elm in ipairs(inv or E) do
    elm:move(offx, offy)
  end
end

-- topleft in canvas
---@return number x left
---@return number y top
function Container:pos()
  local x1, y1, _, _ = self:get_coords()
  return x1 or 0, y1 or 0
end

---@return number, number, number, number
function Container:get_coords()
  local x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
  return x1 or 0, y1 or 0, x2 or 0, y2 or 0
end

-- inherit IEditable.set_coords

--
-- coords starts from 1 not from 0 - so apply +1
--
---@return number width
---@return number height
function Container:size()
  local x1, y1, x2, y2 = self:get_coords()
  return IEditable.get_width_n_height(x1, y1, x2, y2)
end

-- Container:offsets_on_drag(cx, cy, direction) from IEditable

--
-- Resize an element based on specified offsets along the coordinate axes
--
---@param up number
---@param right number
---@param down number
---@param left number
function Container:resize(up, right, down, left)
  if up ~= 0 or right ~= 0 or down ~= 0 or left ~= 0 then
    log_trace('resize', up, right, down, left)

    local x1, y1, x2, y2 = self:get_coords()

    local nx1 = x1 + left
    local ny1 = y1 + up
    local nx2 = x2 + right
    local ny2 = y2 + down

    -- issue: continaer with one line not moving
    if nx1 == nx2 or ny1 == ny2 then
      return false -- block attempts to shrink to line width
    end

    self:set_coords(nx1, ny1, nx2, ny2)
  end
end

---@return env.draw.Container
function Container:copy()
  local x1, y1, x2, y2 = self:get_coords()
  assert(x1 and y1 and x2 and y2, 'has coords')
  local elms = {}

  local inv = self:inventory()
  for _, elm in ipairs(inv or E) do
    elms[#elms + 1] = elm:copy()
  end

  -- ? there will be issues with the heirs
  return Container:new(nil, x1, y1, x2, y2, elms)
end

--
---@return table
---@param opts table?{cname2id, cli, x, y}
function Container:serialize(opts)
  opts = opts or {}


  local x1, y1, x2, y2 = self:get_coords()
  -- build serialized inv
  local inv = self:inventory()
  local t = {}
  for _, elm in ipairs(inv) do
    t[#t + 1] = elm:serialize(opts)
  end

  local serialized = {
    x1 = x1, y1 = y1, x2 = x2, y2 = y2, inv = t, _tag = self._tag,
  }

  add_class_ref(serialized, Container, opts.cname2id)
  return serialized
end

--
-- static factory not a dynamic method of instance!
--
---@param t table -- serialized
function Container.deserialize(t, id2cname)
  -- avoid wrong usage Container:deserialize insteds dot notation
  assert(type(t) == 'table' and not getmetatable(t), 'simple table expected')

  local inv = {}
  for i, elm0 in ipairs(t.inv) do
    local ok, ret = pcall(IEditable.deserialize, elm0, id2cname)
    if ok and type(ret) == 'table' then
      inv[#inv + 1] = ret
    else
      log.warn('Element without deserialize method: %s %s', i, elm0, v2s(ret))
    end
  end

  return Container:new(nil, t.x1, t.y1, t.x2, t.y2, inv):tagged(t._tag)
end

--
--
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param t table?
---@return boolean, table?
function Container:cutoff(x1, y1, x2, y2, t)
  if self:isIntersects(x1, y1, x2, y2) then
    local xx1, yy1, xx2, yy2 = self:get_coords()
    assert(xx1 and yy1 and xx2 and yy2, 'coords')

    t = t or {}
    local changes = 0
    local inv = self:inventory()

    for _, elm in ipairs(inv) do
      if elm:cutoff(x1, y1, x2, y2, t) then
        changes = changes + 1
      end
    end

    return changes > 0, t
  end

  return false, t
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

---@param plist table{{x, y}, {x, y}}
---@param style table{hl, vl, corners}
function Container.create_combined_line(plist, style)
  local inv, x1, y1, x2, y2 = ugeo.create_combined_line(plist, style)

  local container = Container:new(nil, x1, y1, x2, y2, inv)
  log_trace('new container:%s', container)

  return container
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

-- handler to format serialized table value on IEditable.fmtSerialized
function Container.mk_inv_handler(keyname)
  ---@param inv table
  ---@param deep number
  ---@param ident string
  ---@param opts table
  ---@diagnostic disable-next-line: unused-local
  return function(inv, deep, ident, opts)
    deep = deep or 0
    local nl = "\n"
    if (opts or E).force_oneliner then nl, deep = "", 0 end
    local tabk = string.rep(' ', deep, '')

    local id2cname = opts.id2cname or invert_map(opts.cname2id or E)

    local s = tabk .. v2s(keyname) .. " = {" .. nl

    for _, serialized in ipairs(inv) do -- a serialized element
      local ok, cn = pcall(get_class_name, serialized, id2cname)
      if not ok then
        error(fmt('Error: %s t:%s opts:%s ',
          v2s(cn), inspect(serialized), inspect(opts)))
      end

      local line
      if cn then
        local klass = classForName(cn, true)
        if not klass then error('Not Found class for name: ' .. v2s(cn)) end
        line = IEditable.fmtSerialized(klass, serialized, deep + 2, opts)
      else
        line = inspect(serialized, opts.inspect)
      end

      s = s .. line .. "," .. nl
    end

    return s .. tabk .. "}" .. nl
  end
end

-- to format content of Container.inv in multiline mode
local serialize_handlers = {
  inv = Container.mk_inv_handler('inv')
}

--
-- format serialized data
-- Goal: format inventory elements in new lines in fancy way
--
---@param t table
---@param opts table
---@param deep number?
function Container:fmtSerialized(t, deep, opts)
  local prev = opts.handlers
  opts.handlers = serialize_handlers

  local res = IEditable.fmtSerialized(self, t, deep, opts)

  opts.handlers = prev

  return res
end

class.build(Container)
return Container

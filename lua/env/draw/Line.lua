--  30-03-2024  @author Swarg
--

local class = require 'oop.class'

local log = require 'alogger'
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'

class.package 'env.draw'
---@class env.draw.Line : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, x1:number, y1:number, x2:number, y2:number, color:string?) : env.draw.Line
---@field x1 number
---@field y1 number
---@field x2 number
---@field y2 number
---@field color string
-- IEditable:
---@field pos fun(self): number, number -- x, y
---@field size fun(self): number, number -- width, height
---@field setStyle fun(self, color:string)
---@field getStyle fun(self): string
---@field serialize fun(self, opts:table?): table
---@field deserialize fun(self, table, id2cnmae:table?): env.draw.Line
---@field cutoff fun(self, x1, y1, x2, y2, t:table?): boolean,table?
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local Line = class.new_class(nil, 'Line', {
}, --[[ implements ]] IDrawable, IEditable)

local ORDERED_KEY_LIST = {
  { x1 = 'number' },
  { y1 = 'number' },
  { x2 = 'number' },
  { y2 = 'number' },
  { color = 'string' },
}

local v2s, fmt = tostring, string.format
local log_trace = log.trace

-- constructor used in Line:new()
function Line:_init(x1, y1, x2, y2, color) -- , color_left, color_right)
  self.color = self.color or color or '#'
  x1 = x1 or self.x1 or 0
  y1 = y1 or self.y1 or 0
  x2 = x2 or self.x2 or 0
  y2 = y2 or self.y2 or 0

  -- normalize for isIntersects
  self.x1, self.y1, self.x2, self.y2 = IEditable.normalize(x1, y1, x2, y2)
end

--
---@return string
function Line:__tostring()
  if self then
    local tag = self._tag and (' ' .. v2s(self._tag)) or ''
    local c = self.color
    -- id?
    return fmt('Line (%s:%s %s:%s) color: %s%s',
      v2s(self.x1), v2s(self.y1), v2s(self.x2), v2s(self.y2), v2s(c), tag)
  end
  return 'nil'
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layer number?
---@param canvas env.draw.Canvas
function Line:draw(canvas, layer)
  if canvas then
    local x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
    log_trace('Line.draw layer:%s coords:%s:%s-%s:%s %s', layer, x1, y1, x2, y2, self.color)
    canvas:draw_line(layer, x1, y1, x2, y2, self.color)
    --, self.color_left, self.color_rigth)
  end
end

--
--  checking that a given figure completely covers a given area
--
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return boolean intersects
---@diagnostic disable-next-line: unused-local
function Line:isOverlap(x1, y1, x2, y2)
  return false
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Line:orderedkeys() return ORDERED_KEY_LIST end

---@param offx number
---@param offy number
---@return self
function Line:move(offx, offy)
  self.x1, self.y1 = self.x1 + offx, self.y1 + offy
  self.x2, self.y2 = self.x2 + offx, self.y2 + offy
  return self
end

-- function Text:pos() by IEditable used x1 y1
-- function Text:size() by IEditable used x1 y1 x2 y2

-- override IEditable
function Line:offsets_on_drag(cx, cy, direction)
  local up, right, down, left = 0, 0, 0, 0
  local x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
  local vertical, horizontal = x1 == x2, y1 == y2

  local left_edge, right_edge = cx == x1, cx == x2
  local top_edge, bottom_edge = cy == y1, cy == y2

  ---@format disable-next
  if vertical then
    if top_edge then
          if direction == 'U' then up = -1
      elseif direction == 'D' then up =  1 end
    end
    if bottom_edge then
          if direction == 'U' then down = -1
      elseif direction == 'D' then down =  1 end
    end
  elseif horizontal then
    if right_edge then
          if direction == 'L' then right = -1
      elseif direction == 'R' then right =  1 end
    end
    if left_edge then
          if direction == 'L' then left = -1
      elseif direction == 'R' then left =  1 end
    end
  end

  return up, right, down, left
end

--
-- Resize an element based on specified offsets along the coordinate axes
-- Note: Currently only lines parallel to coordinate axes are supported.
-- diagonal lines are not supported.
--
---@param up number
---@param right number
---@param down number
---@param left number
function Line:resize(up, right, down, left)
  local x1, y1, x2, y2, updated = self.x1, self.y1, self.x2, self.y2, nil

  local vertical, horizontal = x1 == x2, y1 == y2

  if horizontal then
    if left ~= 0 then x1, updated = x1 + left, true end
    if right ~= 0 then x2, updated = x2 + right, true end
    --
  elseif vertical then
    if up ~= 0 then y1, updated = y1 + up, true end
    if down ~= 0 then y2, updated = y2 + down, true end
  end

  if updated then
    self:set_coords(x1, y1, x2, y2)
  end
end

---@return env.draw.Line
function Line:copy()
  return Line:new(nil, self.x1, self.y1, self.x2, self.y2, self.color)
end

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param t table?
---@return boolean, table?
function Line:cutoff(x1, y1, x2, y2, t)
  if self:isIntersects(x1, y1, x2, y2) then
    local xx1, yy1, xx2, yy2 = self.x1, self.y1, self.x2, self.y2
    t = t or {}

    -- horizontal
    if xx1 < x1 and yy1 >= y1 and yy1 <= y2 then
      t[#t + 1] = Line:new(nil, xx1, yy1, x1 - 1, yy2, self.color)
    end

    if xx2 > x2 and yy1 >= y1 and yy1 <= y2 then
      t[#t + 1] = Line:new(nil, x2 + 1, yy1, xx2, yy2, self.color)
    end

    -- vertical
    if yy1 < y1 and xx1 >= x1 and xx1 <= x2 then
      t[#t + 1] = Line:new(nil, xx1, yy1, xx2, y1 - 1, self.color)
    end

    if yy2 > y2 and xx1 >= x1 and xx1 <= x2 then
      t[#t + 1] = Line:new(nil, xx1, y2 + 1, xx2, yy2, self.color)
    end

    return true, t
  end

  return false, t
end

class.build(Line)
return Line

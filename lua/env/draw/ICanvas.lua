--  30-03-2024  @author Swarg
--

local class = require 'oop.class'
local interface = class.interface
local method = interface.method

class.package 'env.draw'

---@class env.draw.ICanvas : oop.Interface
---@field add fun(self, obj:env.draw.IDrawable): env.draw.IDrawable
---@field remove fun(self, obj:env.draw.IDrawable): boolean
---@field draw_point fun(self, layernr?, x, y, color)
---@field draw_line fun(self, layernr?, x1, y1, x2, y2, lbody, lleft, lright)
---@field draw_text fun(self, layernr?, x1, y1, x2, y2, text)
local ICanvas = class.new_interface(nil, 'ICanvas', {

  -- define signature of method "draw_point"
  add = method('self', 'table' --[[env.draw.IDrawable']], 'obj'),

  remove = method('self', 'table' --[[env.draw.IDrawable']], 'obj'),

  draw_point = method('self', 'number', 'x'),

  draw_line = method('self', 'number', 'x1'),

  draw_text = method('self', 'number', 'x1', 'number', 'y1', 'string', 'text'),

  -- get_objects_at -- todo?
  -- indexOf remove
})

interface.build(ICanvas)
return ICanvas

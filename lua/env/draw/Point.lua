--  30-03-2024  @author Swarg
--

local log = require 'alogger'
local class = require 'oop.class'
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'

class.package 'env.draw'
---@class env.draw.Point : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, x:number, y:number, color:string?): env.draw.Point
---@field x number
---@field y number
---@field color string "color"
-- IEditable:
---@field setStyle fun(self, color:string)
---@field getStyle fun(self): string
---@field serialize fun(self, table?): table
---@field deserialize fun(self, table, table?): env.draw.Point
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local Point = class.new_class(nil, 'Point', {

}, --[[ implements ]] IDrawable, IEditable)

local ORDERED_KEY_LIST = {
  { x = 'number' }, { y = 'number' }, { color = 'string' }
}

local v2s = tostring
local log_trace = log.trace

-- constructor used in Point:new()
---@param x number
---@param y number
---@param color string char
function Point:_init(x, y, color)
  self.x = self.x or x or 1
  self.y = self.y or y or 1
  self.color = self.color or color or '+'
end

--
---@return string
function Point:__tostring()
  if self then
    local tag = self._tag and (' ' .. v2s(self._tag)) or ''
    -- id?
    return string.format('Point %s:%s color: %s%s',
      v2s(self.x), v2s(self.y), v2s(self.color), tag)
  end
  return 'nil'
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layernum number?
---@param canvas env.draw.Canvas
function Point:draw(canvas, layernum)
  log_trace("Point.draw has_canvas: ", canvas ~= nil)

  if canvas then
    canvas:draw_point(layernum, self.x, self.y, self.color)
  end
end

---@param x number
---@param y number
---@return boolean
function Point:isInsidePos(x, y)
  return self.x == x and self.y == y
end

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return boolean
function Point:isIntersects(x1, y1, x2, y2)
  local x, y = self.x, self.y
  return x >= x1 and x <= x2 and y >= y1 and y <= y2
end

--
--  checking that a given figure completely covers a given area
--
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return boolean intersects
---@diagnostic disable-next-line: unused-local
function Point:isOverlap(x1, y1, x2, y2)
  return false
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Point:orderedkeys() return ORDERED_KEY_LIST end

---@param offx number
---@param offy number
---@return self
function Point:move(offx, offy)
  self.x, self.y = self.x + offx, self.y + offy
  return self
end

-- top left position in the canvas
function Point:pos()
  return self.x, self.y
end

---@return number, number, number, number
function Point:get_coords()
  return self.x, self.y, self.x, self.y
end

---@param x number
---@param y number
---@return self
function Point:set_coords(x, y, _, _)
  self.x, self.y = x, y
  return self
end

--
-- coords starts from 1 not from 0 - so apply +1
--
---@return number width
---@return number height
function Point:size()
  return 1, 1
end

--
-- cannot change its size
--
---@diagnostic disable-next-line: unused-local
function Point:offsets_on_drag(cx, cy, direction)
  return 0, 0, 0, 0
end

--
-- Resize an element based on specified offsets along the coordinate axes
--
function Point:resize()
  return false
end

---@return env.draw.Point
function Point:copy()
  return Point:new(nil, self.x, self.y, self.color)
end

class.build(Point)
return Point

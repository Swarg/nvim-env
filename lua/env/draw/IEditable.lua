--  30-03-2024  @author Swarg
--

local class = require 'oop.class'
local log = require 'alogger'
local cli = require 'clieos'
local ubase = require 'env.draw.utils.base'
local interface, num = class.interface, 'number'

local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_debug, log_trace = log.debug, log.trace
local instanceof, get_class = class.instanceof, class.get_class
local is_class, classForName = class.is_class, class.forName
local Object_fromArray = class.Object.fromArray
local get_class_name = class.serialize_helper.get_class_name
local add_class_ref = class.serialize_helper.add_class_ref
local fmt_kvpair = ubase.fmt_kvpair

local CLI_API = {
  cli_parse_line = cli.cli_parse_line,

  Editor = {
    init = cli.init,
    get_kt_fr_raw_orderedkey = cli.Editor.get_kt_fr_raw_orderedkey_entry
    -- build_prompt_oneliner = cli.Editor.build_prompt_oneliner -- method
  },

  Patch = {
    set_rule = cli.Patch.set_rule,
    get_rules = cli.Patch.get_rules,
    get_data = cli.Patch.get_data,
    is_changed = cli.Patch.is_changed,
    is_changed_to = cli.Patch.is_changed_to,
    changed_cnt = cli.Patch.changed_cnt,
    has_changes = cli.Patch.has_changes,
    changed_keys = cli.Patch.changed_cnt_of_keys,
    update_in = cli.Patch.update_one_in, -- one key in given object direct
    update_in_sk = cli.Patch.update_one_in_sk,
    update_all = cli.Patch.update_all,
    update_ex = cli.Patch.update_one_out, -- external
    update_ex_n = cli.Patch.update_ex_n,  -- number
    update_ex_s = cli.Patch.update_ex_s,  -- string
    update_ex_b = cli.Patch.update_ex_b,  -- boolean
    update_ex_t = cli.Patch.update_ex_t,  -- table
    get_new_value = cli.Patch.get_new_value,
    set_updated_risky = cli.Patch.report_set_updated_risky,
    report_set_updated = cli.Patch.report_set_updated,
  },

  edit_transaction = cli.edit_transaction,
  TESTING = cli.TESTING,
  UTILS = cli.UTILS,
  CONSTS = {
    TOUCHED_KEY = cli.TOUCHED_KEY,
    UPDATED_KEY = cli.UPDATED_KEY,
    TYPE_MISMATCH = cli.TYPE_MISMATCH,
  }
}

class.package 'env.draw'
---@class env.draw.IEditable: oop.Interface
---@field orderedkeys fun(self, serialize_opts:table?): table
---@field serialize fun(self, cname2id:table?, opts:table?): table
---@field deserialize fun(table, id2cname:table?, boolean?): self
---@field fmtSerialized fun(self, data:table, deep:number?, opts:table?): string
---@field toArray fun(self, output:table?, serialize:boolean?, cname2id:table?): table
---@field copy fun(self): self
---@field edit fun(self, patch:table, report:table?) -- todo rename to apply_patch
---@field setStyle fun(self, ...)
---@field getStyle fun(self): string|table
---@field move fun(self, offx:number, offy:number): self
---@field pos fun(self): number, number   x y (left top in the canvas)
---@field get_coords fun(self): number, number, number, number  x1 y1 x2 y2 (left top rigth bottom  in the canvas)
---@field set_coords fun(self, x1:number, y1:number, x2:number, y2:number): self
---@field get_width fun(self): number
---@field get_height fun(self): number
---@field size fun(self): number, number  width, height
---@field resize fun(self, x:number, y:number, offx:number, offy:number)
---@field short_info fun(self, with_coords:boolean?): string
---@field get_child_at fun(self, x:number, y:number): env.draw.IEditable?
---@field remove_child fun(self, elm:env.draw.IEditable): env.draw.IEditable?
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local IEditable = class.new_interface(nil, 'IEditable', {

  move = interface.method('self', num, 'offset_x', num, 'offset_y'),

  -- Resize an element based on specified offsets along the coordinate axes
  resize = interface.method('self', num, 'x', num, 'y', num, 'offx', num, 'offy'),

  -- default methods (aka trait)

  -- return top-left position in the canvas.
  -- used x1, y1  if your object use another fields name you should override this
  ---@return number x
  ---@return number y
  pos = function(self)
    return self.x1, self.y1
  end,

  --
  -- coordinates of the vertices
  -- x1 y1 x2 y2
  ---@return number, number, number, number
  get_coords = function(self)
    return self.x1, self.y1, self.x2, self.y2
  end,

  -- to override
  ---@return self
  ---@param x1 number
  ---@param y1 number
  set_coords = function(self, x1, y1, x2, y2)
    local u = false
    if rawget(self, 'x1') and rawget(self, 'y1') then
      self.x1, self.y1, u = x1, y1, true
    elseif x2 == nil and y2 == nil and rawget(self, 'x') and rawget(self, 'y') then
      self.x, self.y, u = x1, y1, true
    end

    if x2 ~= nil and y2 ~= nil and rawget(self, 'x2') and rawget(self, 'y2') then
      self.x2, self.y2, u = x2, y2, true
    end

    if not u then
      error('Seems you need to override this method to set your coord manually')
    end

    return self
  end,

  -- width and height of elment
  ---@return number, number
  size = function(self)
    local x1, y1, x2, y2 = self:get_coords()

    if x1 == nil or y2 == nil then
      error(fmt('Seems the class %s needs to override method IEditable:size' ..
        ' ret x1:%s y2:%s', v2s(class.name(self)), v2s(x1), v2s(y2)))
    end
    -- x1==x2 is width = 1 so +1
    return ubase.get_width_n_height(x1, y1, x2, y2)
  end,

  ---@return number
  get_width = function(self)
    local x1, _, x2, _ = self:get_coords()
    return math.abs(x2 - x1) + 1
  end,

  ---@return number
  get_height = function(self)
    local _, y1, _, y2 = self:get_coords()
    return math.abs(y2 - y1) + 1
  end,

  --
  -- Calculates the coordinate offset for resizing an element based on where the
  -- force is applied from on the element and in which direction the
  -- displacement occurs.
  --
  -- cx, cy - the point at which force is applied(cursor, inner in Canvas coords)
  -- direction - one of U|R|D|L
  --
  ---@param cx number
  ---@param cy number
  ---@param direction string
  ---@param self self
  ---@return number up
  ---@return number right
  ---@return number down
  ---@return number left
  offsets_on_drag = function(self, cx, cy, direction)
    local up, right, down, left = 0, 0, 0, 0
    local x1, y1, x2, y2 = self:get_coords() -- coords in canvas (cell utf8-letters)
    local left_edge, right_edge = cx == x1, cx == x2
    local top_edge, bottom_edge = cy == y1, cy == y2

    ---@format disable-next
    do
      if right_edge then
            if direction == 'L' then right = -1
        elseif direction == 'R' then right = 1 end
      end
      if left_edge then
            if direction == 'L' then left = -1
        elseif direction == 'R' then left = 1 end
      end
      if top_edge then
            if direction == 'U' then up = -1
        elseif direction == 'D' then up = 1 end
      end
      if bottom_edge then
            if direction == 'U' then down = -1
        elseif direction == 'D' then down = 1 end
      end
    end

    return up, right, down, left
  end,


  ---@param with_coords boolean
  ---@return string
  short_info = function(self, with_coords)
    local w, h = self:size()
    local classname = (class.name(self) or '?'):match('[^%.]+$') or '?'
    local coords = ''
    if with_coords then
      local x, y = self:pos()
      coords = '(' .. v2s(x) .. ':' .. v2s(y) .. ')'
    end
    return string.format('%s%s(%sx%s)', classname, coords, v2s(w), v2s(h))
  end,

  ---@return table
  ---@diagnostic disable-next-line: unused-local
  orderedkeys = function(self, serializ_opts)
    local cn = v2s(class.name(self) or '?')
    error('this method must be overridden by a specific implementation: ' .. cn)
  end,

  --
  -- update the current state  of the object by given patch table
  -- modt creates by cli.Editor(obj, opts)
  --
  -- to custom update phantom fields you need to override this method
  --
  -- TODO: rename to apply_patch
  --
  ---@param patch table{x, y, text}  modt
  ---@return boolean, table
  edit = function(self, patch)
    if cli.Patch.has_changes(patch) then
      -- via self[key] = patch[key].new_value
      cli.Patch.update_all(patch, self)
      -- .. onUpdate
      return true, patch
    end

    return false, patch
  end,

  --- IContainer
  ---@param x number
  ---@param y number
  ---@return env.draw.IEditable?
  ---@diagnostic disable-next-line: unused-local
  get_child_at = function(self, x, y)
    return nil
  end,

  ---@param elm env.draw.IEditable
  ---@return env.draw.IEditable?
  ---@diagnostic disable-next-line: unused-local
  remove_child = function(self, elm)
    return nil
  end,

  --
  -- cut the current element along a given area into several smaller elements
  ---@return boolean
  ---@return table?
  ---@diagnostic disable-next-line: unused-local
  cutoff = function(x1, y1, x2, y2, t)
    return false, t
  end,

  ---@param char string
  setStyle = function(self, char)
    assert(type(char) == 'string', 'char')
    if not rawget(self, 'color') then
      error(fmt('Seems the class %s needs to override method IEditable:setStyle',
        v2s(class.name(self))))
    end
    self.color = char
  end,

  getStyle = function(self)
    if not rawget(self, 'color') then
      error(fmt('Seems the class %s needs to override method IEditable:getStyle',
        v2s(class.name(self))))
    end
    return self.color
  end,

  -- full deep all keys
  -- this method for  Rectangle.tline  produce -> Line:serialize
  -- the modt method produce only for given keys
  --
  ---@param opts table?{cname2id, x, y, cli}
  serialize = function(self, opts)
    --[[
    local orderedkeys = self:orderedkeys()
    local t = {}
    for i, e in ipairs(orderedkeys) do
      local key, _ = cli.Editor.get_kt_fr_orderedkey_entry(e, i)
      t[key] = self[key]
    end

    self:addClassRef(t, (opts or E).cname2id)
    return t
    ]]
    return self:toArray(nil, true, (opts or E).cname2id) -- oop.Object
  end,

  ---@param array table,
  ---@param id2cname table
  ---@param usestd boolean?
  deserialize = function(array, id2cname, usestd)
    assert(type(array) == 'table', 'array')
    local cn = assert(get_class_name(array, id2cname), 'classname expected')
    local cls = assert(classForName(cn, true), 'class expected for ' .. cn)

    -- check is Class has own overriden method of this interface
    if not usestd and rawget(cls, 'deserialize') then
      return cls['deserialize'](array, id2cname, true)
    elseif is_class(cls) then ---@cast cls oop.Object
      return Object_fromArray(cls, array, true, id2cname)
    end
  end,

  --------------------------------------------------------------------------------

  --
  -- format serialized data (from IEditable.serialize)
  -- Goal: keep nice, predefined order of the keys, and
  -- correct indentation for containers and its child elements
  --
  -- To format table-values, a predefined handler based on the key name is used
  -- opts.handlers[key]
  --
  ---@param t table serialized data to format
  ---@param opts table
  ---@param deep number?
  fmtSerialized = function(self, t, deep, opts)
    deep = deep or 0
    local orderedkeys = self:orderedkeys()
    local ident = ''
    if opts then
      ident = opts.ident or '' -- between tokens: k_=_v (not a tab!)
      -- newline
    end


    local s, nl_pref = "", ""
    local tab = string.rep(' ', deep, '')

    -- add class ref
    if t._clsid then
      s = s .. fmt_kvpair('_clsid', t._clsid, ident)
    elseif t._class then
      s = s .. fmt_kvpair('_class', t._class, ident)
    end

    local get_kt_fr_orderedkey_entry = CLI_API.Editor.get_kt_fr_raw_orderedkey

    for i, e in ipairs(orderedkeys) do
      local key, _ = get_kt_fr_orderedkey_entry(e, i)
      local v = t[key]
      local vt = type(v)

      if v ~= nil then
        if #s > 1 then s = s .. ',' .. ident end
        if vt ~= 'table' then
          s = s .. fmt_kvpair(key, v, ident)
          --
        elseif vt == 'table' then
          -- to transform tables, you need a predefined handler specifically
          -- for this specific named key.
          -- Its job is to format the contents of this table value
          -- eg: elements of Contanier or attributes of Entity
          if type(opts.handlers) ~= 'table' then
            error(fmt('To format a table value for key:"%s", a handler is needed'
              .. ', got opts.handlers: %s', v2s(key), type(opts.handlers)))
          end
          local handler = opts.handlers[key]
          if type(handler) == 'function' then
            local line = handler(v, deep + 2, ident, opts)
            if line then
              if string.find(line, "\n") then
                if nl_pref == "" then
                  nl_pref = "\n" .. tab
                end
                line = "\n" .. line:sub(1, -3) .. '}'
              end
              s = s .. line
            end
          else
            -- See Container:fmtSerialized
            error(fmt(
              'Not Found predefined handler to format table value for key: %s',
              key, opts.handlers ~= nil
            ))
          end
        end
      end
    end

    local itab = nl_pref ~= '' and '  ' or ''

    return tab .. "{" .. nl_pref .. itab .. s .. nl_pref .. "}"
  end,
  --------------------------------------------------------------------------------

  ---@return env.draw.IEditable
  copy = function(self)
    local klass = assert(get_class(self), 'has Class')
    return klass.deserialize(self:serialize(nil))
  end,

  --------------------------------------------------------------------------------

  -- tags to keep not drawble element-metainfo
  tagged = function(self, tag)
    self._tag = tag
    return self
  end,

  tag = function(self)
    return (self or E)._tag
  end,

})


IEditable.normalize = ubase.normalize
IEditable.get_width_n_height = ubase.get_width_n_height
local Patch = CLI_API.Patch

--
-- to check if the given object is an IEditable instance if not, throw an error
--
---@return env.draw.Canvas
function IEditable.cast(obj)
  if type(obj) == 'table' and instanceof(obj, IEditable) then
    return obj
  else
    local base = class.base
    local ctype = base.CTYPE[obj._ctype or false] or '?'
    local cname = class.name(obj)
    local own_cname = class.name(IEditable)

    error(fmt('expected instance of %s got: %s %s',
      v2s(own_cname), v2s(ctype), v2s(cname)))
  end
end

--
-- used for serialize| deserialize
--
---@return table
---@param cname2id table?
function IEditable:addClassRef(t, cname2id)
  local klass = assert(get_class(self), 'has class')
  -- add _class or _clsid to t
  add_class_ref(t, klass, cname2id)
  t._tag = self._tag or t._tag or nil -- additional field
  return t
end

function IEditable.mkChildPrefix(parent, elm, x, y)
  if parent ~= nil then
    local scn = v2s(class.get_short_class_name(elm))
    return fmt('[child-at:%s:%s](%s): ', v2s(x), v2s(y), scn)
  end
  return ''
end

--
-- api to clieos
--
-- through a function so that during serialization this table does not end up
-- in the result
---@return table
function IEditable.cli() return CLI_API end

--
-- Relevant for figures with four vertices (Rectangle, Container)
-- editing coordinates and size by 4 points and w h
---@param self self
---@param patch table
function IEditable.apply_patch_to_coords_4v(self, patch)
  local data = Patch.get_data(patch) or E
  -- Patch.set_rule(patch, 'can_set_nil', false)

  log_trace("update_coords_4v serialize_opts:%s", data)
  local update_ex_n = Patch.update_ex_n

  local x1, y1, x2, y2 = self:get_coords()

  local coords_changed = Patch.changed_keys(patch, 'x1', 'y1', 'x2', 'y2') > 0
  local size_changed = false

  if coords_changed then
    x1 = update_ex_n(patch, 'x1', x1)
    y1 = update_ex_n(patch, 'y1', y1)
    x2 = update_ex_n(patch, 'x2', x2)
    y2 = update_ex_n(patch, 'y2', y2)
    Patch.set_updated_risky(patch, 'x1', 'y1', 'x2', 'y2')
  end

  -- in cli mode, provide support for additional phantom fields for convenient
  -- and easy resizing of an container
  if data.cli then
    size_changed = Patch.changed_keys(patch, 'w', 'h') > 0
    if size_changed then
      local w, h = IEditable.get_width_n_height(x1, y1, x2, y2)
      w = update_ex_n(patch, 'w', w)
      h = update_ex_n(patch, 'h', h)

      if w and h and size_changed and not coords_changed then
        -- to support negative h or w to "move" figure on the canvas pos
        local xcorr, ycorr = 0, 0 -- correction
        if w < 0 then xcorr = 1 elseif w > 0 then xcorr = -1 end
        if h < 0 then ycorr = 1 elseif h > 0 then ycorr = -1 end
        x2, y2 = (x1 + w + xcorr), (y1 + h + ycorr)
        coords_changed = true
      end
      Patch.set_updated_risky(patch, 'w', 'h')
    end
  end

  log_trace("size_changed:%s coords_changed:%s", size_changed, coords_changed)

  if coords_changed then
    x1, y1, x2, y2 = IEditable.normalize(x1, y1, x2, y2)
    self:set_coords(x1, y1, x2, y2)
  end

  return coords_changed == true
end

interface.build(IEditable)
return IEditable

--  02-04-2024  @author Swarg
-- Goal: hold the randerable text

local log = require 'alogger'
local class = require 'oop.class'
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'
local u8 = require("env.util.utf8")

class.package 'env.draw'
---@class env.draw.Text : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, x1:number, y1:number, text:string?, x2:number?, y2:number?): env.draw.Text
---@field x1 number
---@field y1 number
---@field x2 number
---@field y2 number
---@field text string
-- IEditable:
---@field pos fun(self): number, number -- x, y
---@field size fun(self): number, number -- width, height
---@field setStyle fun(self, color:string)
---@field getStyle fun(self): string?
---@field serialize fun(self, table?): table
---@field deserialize fun(self, table, table?): env.draw.Text
---@field cutoff fun(self, x1, y1, x2, y2, t:table): table?
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local Text = class.new_class(nil, 'Text', {

}, --[[ implements ]] IDrawable, IEditable)

local v2s, fmt = tostring, string.format
local Patch = IEditable.cli().Patch
local log_trace = log.trace

local ORDERED_KEY_LIST = {
  { x1 = 'number' },
  { y1 = 'number' },
  { x2 = 'number' },
  { y2 = 'number' },
  { KeepRange = 'bool', _def = false }, -- cli-only phantom
  { UseEscape = 'bool', _def = true },
  { text = 'string' },
}

-- constructor used in Text:new()
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param text string char
function Text:_init(x1, y1, text, x2, y2)
  self.text = self.text or text or 'Text'
  x1 = self.x1 or x1 or 1
  y1 = self.y1 or y1 or 1

  x2 = self.x2 or x2
  y2 = self.y2 or y2

  if not x2 or not y2 then
    x1, y1, x2, y2 = Text.calc_text_range(self.text, x1, y1, x2, y2)
  end

  -- normalize for isIntersects
  self.x1, self.y1, self.x2, self.y2 = IEditable.normalize(x1, y1, x2, y2)
end

---@param letter string
---@param t table acc
function Text.fsm_calc_text_range_iter(letter, t)
  if not t.prev_i then
    t.prev_i, t.i, t.max_x, t.y = 1, 0, 0, 0
  end
  t.i = t.i + 1
  if letter == "\n" then
    local len = t.i - t.prev_i
    if len > t.max_x then t.max_x = len end
    t.prev_i = t.i + 1
    t.y = t.y + 1
  end
end

function Text.calc_text_range(text, x1, y1, x2, y2)
  local w, h = Text.do_calc_text_range(text)
  x2, y2 = x1 + w - 1, y1 + h - 1
  return x1, y1, x2, y2
end

function Text:recalc_text_range()
  local x1, y1, x2, y2 = self:get_coords()
  x1, y1, x2, y2 = Text.calc_text_range(self.text, x1, y1, x2, y2)
  self:set_coords(x1, y1, x2, y2)
end

---@return number width
---@return number height
function Text.do_calc_text_range(text)
  local t = u8.foreach_letter(text, Text.fsm_calc_text_range_iter, {})
  -- finish fsm
  if text:sub(-1, -1) ~= "\n" then
    Text.fsm_calc_text_range_iter("\n", t)
  end
  -- width height
  return t.max_x, t.y
end

-- local function self:get_coords()
--   local x1, y1, x2, y2 = self.x1, self.y1, self.x2, self.y2
--   assert(x2 and y2, 'calculated')
--   return x1, y1, x2, y2
-- end

--
---@return string
function Text:__tostring()
  if self then
    local x1, y1, x2, y2 = self:get_coords()
    local w, h = self:size()
    local text = self.text
    local tag = self._tag ~= nil and (' ' .. v2s(self._tag)) or ''

    return fmt('Text (%s:%s %s:%s) (%sx%s) "%s"%s',
      v2s(x1), v2s(y1), v2s(x2), v2s(y2), v2s(w), v2s(h), v2s(text), tag)
  end
  return 'nil'
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------


---@param layernum number
---@param canvas env.draw.Canvas
function Text:draw(canvas, layernum)
  log_trace("Text.draw has_canvas: ", canvas ~= nil)

  if canvas then
    canvas:draw_text(layernum, self.x1, self.y1, self.x2, self.y2, self.text)
  end
end

---@diagnostic disable-next-line: unused-local
function Text:isOverlap(x1, y1, x2, y2)
  return false
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Text:orderedkeys( --[[opts{data.cli}]])
  return ORDERED_KEY_LIST
end

-- R - recall range of the new text
function Text:cli_prompt()
  local msg = 'x1 y1 x2 y2 KeepRange UseEscape text'
  local x1, y1, x2, y2 = self:get_coords()
  local text = (self.text or ''):gsub("\n", '\\n')
  local def = fmt('%s %s %s %s %s %s "%s"',
    v2s(x1), v2s(y1), v2s(x2), v2s(y2), '-', '+', v2s(text)
  )
  return msg, def
end

---@param patch table{x = {new=1, old =0}}
---@return boolean, table
function Text:edit(patch)
  local update = Patch.update_in
  local has_changes = Patch.has_changes(patch)
  if not has_changes then
    return false, patch
  end

  update(patch, 'x1', self)
  update(patch, 'y1', self)
  update(patch, 'x2', self)
  update(patch, 'y2', self)
  update(patch, 'text', self)

  if Patch.is_changed_to(patch, 'UseEscape', '+') then
    self.text = self.text:gsub("\\n", "\n")
    Patch.set_updated_risky(patch, 'UseEscape')
    log_trace('ApplyEscapes')
  end

  if Patch.is_changed_to(patch, 'KeepRange', '-') and
      Patch.changed_keys(patch, { 'text', 'x1', 'y1' }) > 0
  then
    local x1, y1, x2, y2 = self:get_coords()
    self.x1, self.y1, self.x2, self.y2 =
        Text.calc_text_range(self.text, x1, x2, y1, y2)
  end

  Patch.set_updated_risky(patch, 'x1', 'y1', 'x2', 'y3', 'KeepRange', 'UseEscape')
  return true, patch
end

function Text:setStyle() -- background?)
  -- todo
end

function Text:getStyle()
  return nil
end

---@param offx number
---@param offy number
---@return self
function Text:move(offx, offy)
  self.x1, self.y1 = self.x1 + offx, self.y1 + offy
  self.x2, self.y2 = self.x2 + offx, self.y2 + offy
  return self
end

-- function Text:pos() by IEditable used x1 y1
-- function Text:size() by IEditable used x1 y1 x2 y2
-- function Text:offsets_on_drag by IEditable same behaviour as for rectangle

--
-- Resize an element based on specified offsets along the coordinate axes
--
---@param up number
---@param right number
---@param down number
---@param left number
function Text:resize(up, right, down, left)
  if up ~= 0 or right ~= 0 or down ~= 0 or left ~= 0 then
    local x1, y1, x2, y2 = self:get_coords()

    local nx1 = x1 + left
    local ny1 = y1 + up
    local nx2 = x2 + right
    local ny2 = y2 + down

    if ny1 == ny2 or nx1 == nx2 then
      return false -- block attempts to shrink to line width
    end

    self:set_coords(nx1, ny1, nx2, ny2)
  end
end

---@return env.draw.Text
function Text:copy()
  return Text:new(nil, self.x1, self.y1, self.text, self.x2, self.y2)
end

class.build(Text)
return Text

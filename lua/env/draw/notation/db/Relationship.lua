--  08-05-2024  @author Swarg
-- connecting line between two entities
-- Goal:
--   - store a line (possibly composed of several turns) and the ends of
--     the relationship

local log = require 'alogger'
local class = require 'oop.class'
local ubase = require 'env.draw.utils.base'
local udb = require 'env.draw.notation.db.base'
local ugeo = require 'env.draw.utils.geometry'
local usymbs = require 'env.draw.utils.unicode_symbols'
local Point = require 'env.draw.Point'
local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Container = require 'env.draw.Container'
local IEditable = require 'env.draw.IEditable'
local u8 = require("env.util.utf8")

---@class env.draw.notation.db.RelationshipEnd
---@field endtype number -- (0..1 1..n, etc) - then modifier type of this leg
---@field ref string? -- what this leg(end) of the relationship indicates(entity name)
---@field role string?
---@field elms table{head:env.draw.Point, mod:env.draw.Point}

class.package 'env.draw.notation.db'
---@class env.draw.notation.db.Relationship : env.draw.Container
---@field new fun(self, o:table?, pos_list:table, rtype:number, rstart:number, rend:number, params:table?): env.draw.notation.db.Relationship
---@field name env.draw.Text
---@field rtype number (simple or identifying)
---@field a env.draw.notation.db.RelationshipEnd -- leg relationship leg a(start)
---@field b env.draw.notation.db.RelationshipEnd -- leg relationship leg b(end)
---field load table? association  <--()-->
local Relationship = class.new_class(Container, 'Relationship', {
})

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_trace = log.trace
local RELATIONSHIP = udb.RELATIONSHIP
local RELATIONSHIP_STYLES = udb.STYLES.RELATIONSHIP
local RELATIONSHIP_SIMPLE = udb.RELATIONSHIP.TYPES.simple
-- local RELATIONSHIP_IDENTIFYING = udb.RELATIONSHIP.TYPES.identifying

function Relationship:get_rtype_name()
  return RELATIONSHIP.NAME[self.rtype or false] or '?'
end

function Relationship:get_endtype_name_a()
  return RELATIONSHIP.NAME[(self.a or E).endtype] or '?'
end

function Relationship:get_endtype_name_b()
  return RELATIONSHIP.NAME[(self.b or E).endtype] or '?'
end

-- for saving
local ORDERED_KEY_LIST = {
  { name = 'string' }, -- the name of this Relationship
  { nx = 'number' },
  { ny = 'number' },
  { rtype = 'number' },
  -- start or first leg of relationship
  { a_et = 'number' },   -- 1..n - the type of the leg - modifier
  { a_ref = 'string' },  -- Entity name to which this relation is refs
  { a_role = 'string' }, --
  -- end or second leg of relationship
  { b_et = 'number' },
  { b_ref = 'string' },
  { b_role = 'string' },
  -- lines
  { plist = 'table' }
}

-- for cli edit via one-liner
local ORDERED_KEY_LIST_CLI = {
  { name = 'string' },
  { nx = 'number' },
  { ny = 'number' },
  { rtypen = 'string', _def = Relationship.get_rtype_name },
  { a_etn = 'string',  _def = Relationship.get_endtype_name_a },
  { a_ref = 'string' },
  { a_role = 'string' },
  { b_etn = 'string',  _def = Relationship.get_endtype_name_b },
  { b_ref = 'string' },
  { b_role = 'string' },
  { plist = 'table' },
}

---@diagnostic disable-next-line: unused-local
local instanceof, get_class = class.instanceof, class.get_class
local shortClassName = class.get_short_class_name
local fmt_kvpair = ubase.fmt_kvpair

local validate_relationship = udb.validate_relationship
local get_relationship_notation = udb.get_relationship_notation
local Patch = IEditable.cli().Patch



function Relationship.validated_ends_of_pos_list(pos_list)
  local fp, lp = pos_list[1], pos_list[#pos_list]
  assert(fp.x and fp.y, 'has first point')
  assert(lp.x and lp.y, 'has last point')
  assert(fp.x ~= lp.x or fp.y ~= lp.y, 'expected diff points')
  return fp, lp
end

--
-- recreate simple elements that represent current relationship
--
---@param pos_list table
---@param rtype number - or = simple or identifying
---@param a_et number (start) type of relationship ends 0..1, 0..n 1..n, etc
---@param b_et number (end)   type of relationship ends
---@param t table?{name, a_ref, a_role, b_ref, b_role}
local function build_elements(self, pos_list, rtype, a_et, b_et, t)
  rtype = rtype or RELATIONSHIP_SIMPLE
  local raw_style_tbl = RELATIONSHIP_STYLES[rtype]

  log_trace("build_elements rt:%s a:%s b:%s pl:%s style:",
    rtype, a_et, b_et, pos_list, raw_style_tbl)

  local style = usymbs.build_rect_style(raw_style_tbl)

  local inv, x1, y1, x2, y2 = ugeo.create_combined_line(pos_list, style)

  if not inv or not next(inv) then
    error(fmt('error on build relationship: no lines for pos_list', pos_list))
  end
  if x1 == nil or y1 == nil or x2 == nil or y2 == nil then
    error('error on build relationship: no coords for pos_list')
  end

  local fp, lp = pos_list[1], pos_list[#pos_list]
  assert(fp.x ~= nil and fp.y ~= nil, 'has first point')
  assert(lp.x ~= nil and lp.y ~= nil, 'has last point')
  assert(fp.x ~= lp.x or fp.y ~= lp.y, 'expected diff points')

  self.x1, self.y1, self.x2, self.y2 = x1, y1, x2, y2
  self.inv = inv

  local find = function(tt, pos, out)
    local opts = { vertex_only = true, limit = 1 }
    ugeo.find_by_xy(tt, Line, pos.x, pos.y, opts, out)
  end

  local lines = {}
  find(inv, fp, lines)
  find(inv, lp, lines)
  log_trace('fp:%s lp:%s, lines:', fp, lp, lines)

  local first_line, last_line = lines[1], lines[#lines]

  local st = get_relationship_notation(first_line, fp, rtype, a_et)
  local et = get_relationship_notation(last_line, lp, rtype, b_et)

  t = t or E
  self.a = Relationship.newRelEnd(st, a_et, t.a_ref, t.a_role)
  self.b = Relationship.newRelEnd(et, b_et, t.b_ref, t.b_role)

  local nx, ny = self:get_name_xy()
  if nx == 0 and ny == 0 then
    local name = self:get_name()
    nx = ubase.center(fp.x, lp.x) - math.ceil(u8.utf8_slen(name) / 2)
    ny = ubase.center(fp.y, lp.y)
    -- todo textlen
    self:set_name_xy(nx, ny)
  end
end

--
-- constructor used in Relationship:new()
--
---@param pos_list table
---@param rtype number - or = simple or identifying
---@param a_et number (start) type of relationship ends 0..1, 0..n 1..n, etc
---@param b_et number (end)   type of relationship ends
---@param params table?{name}
function Relationship:_init(pos_list, rtype, a_et, b_et, params)
  params = params or {}

  self:set_name(params.name)
  self:set_name_xy(params.name_x, params.name_y)

  self.rtype = rtype or RELATIONSHIP.TYPES.simple

  build_elements(self, pos_list, rtype, a_et, b_et, params)
end

---@return string
function Relationship:get_name()
  return (self.name or E).text or ''
end

---@param name string?
function Relationship:set_name(name)
  if not self.name then
    self.name = Text:new(nil, 0, 0, name or '')
  else
    self.name.text = name or ''
  end
  self.name:recalc_text_range()
  return self.name
end

---@return number x, number y
function Relationship:get_name_xy()
  if self.name then
    return self.name:pos()
  end
  return 0, 0
end

function Relationship:set_name_xy(x, y)
  x, y = x or 0, y or 0
  if not self.name then
    self.name = Text:new(nil, x, y, '')
  else
    self.name.x1, self.name.y1 = x, y
  end

  self.name:recalc_text_range()

  return self.name
end

--
-- cli helper
--
---@param pos_list table{{x,y},..}
---@param rtype string?
---@param rstart string?
---@param rend string?
---@param params table?
---@return boolean
---@return env.draw.notation.db.Relationship|string
function Relationship.new_from_plist(pos_list, rtype, rstart, rend, params)
  local ok_ep, rs, re, rt, err = validate_relationship(rstart, rend, rtype)
  if not ok_ep then
    return false, 'invalid relationship endpoints: ' .. v2s(err)
  end
  if #(pos_list or E) < 2 then
    return false, 'expected at least two position got: ' .. #(pos_list or E)
  end

  local rel = Relationship:new(nil, pos_list, rt, rs, re, params)
  -- todo src_start (entity name)
  return true, rel
end

---@param t table { head:string, second:string, direction = d, x = p.x, y = p.y }
---@param rel_end_type number
---@param ref string? name of the entity
---@param role string?
---@return env.draw.notation.db.RelationshipEnd
function Relationship.newRelEnd(t, rel_end_type, ref, role)
  assert(type(t) == 'table', 't')
  assert(type(t.head) == 'string', 't.head')
  assert(type(t.direction) == 'number', 't.direction')
  assert(type(t.x) == 'number', 't.x')
  assert(type(t.y) == 'number', 't.y')

  local offset = ugeo.DIR_OFFSET[ugeo.DIR_OPPOSITE[t.direction] or 0] or E
  local xoff, yoff = offset[1], offset[2]
  assert(xoff and yoff, 'has offset')

  return {
    endtype = rel_end_type,
    ref = ref, -- name of entity to which this end(arrow) points to
    role = role,

    elms = {
      head = Point:new(nil, t.x, t.y, t.head),
      mod = Point:new(nil, t.x + xoff, t.y + yoff, t.second or ''),
    }
  }
end

--
-- change given leg(a or b) of relation
--
---@param e env.draw.notation.db.RelationshipEnd
---@param rtype number relation type [single|identifying]
---@param endtype number one of [0..1, 1..1, 0..n, 1..n]
function Relationship.set_rel_end_type(e, rtype, endtype)
  assert(type(e) == 'table', 'e')
  assert(type(rtype) == 'number', 'rtype')
  assert(type(endtype) == 'number', 'endtype')
  assert(RELATIONSHIP.NAME[endtype], 'valid end_type')
  assert(RELATIONSHIP.TYPES.simple ~= endtype and
    RELATIONSHIP.TYPES.identifying ~= endtype, 'endtype not a relation type')

  local d = ugeo.get_direction_code2(e.elms.head, e.elms.mod)
  assert(type(d) == 'number', 'expected direction from offset')
  d = ugeo.DIR_OPPOSITE[d]

  local tdirs = (RELATIONSHIP_STYLES.ENDS[rtype] or E)[endtype]
  assert(type(tdirs) == 'table', 'tdirs')

  local style = (tdirs or E)[d]
  local head, mod = style[1], style[2]

  e.endtype = endtype

  e.elms.head:setStyle(head)
  e.elms.mod:setStyle(mod)
end

--
--
--
---@param e env.draw.notation.db.RelationshipEnd
---@param verbose boolean?
---@param tabn number?
---@return string
function Relationship.endpoint2str(e, verbose, tabn)
  if type(e) ~= 'table' then
    return tostring(e)
  end
  local stype = v2s(RELATIONSHIP.NAME[e.endtype or false] or '?')
  local tab = string.rep(' ', tabn or 0)
  local ref = e.ref or ''
  local role = e.role or ''

  local s
  if verbose then
    s = fmt('%s%s to "%s" role:"%s"', tab, stype, ref, role)
    if e.elms then
      s = s .. '  head: ' .. (e.elms.head ~= nil and e.elms.head:__tostring() or '-')
      s = s .. '  mod: ' .. (e.elms.mod ~= nil and e.elms.mod:__tostring() or '-')
    end
  else
    if role ~= '' then role = '(' .. role .. ')' else role = ' ' end
    s = fmt(' %s%s%s', ref, role, stype)
  end

  return s
end

--
---@return string
---@param verbose boolean?
---@param tabn number?
function Relationship:__tostring(verbose, tabn)
  if self then
    tabn = tabn or 0
    local name, s = '', nil

    if verbose then
      if self.name and self.name ~= '' then
        name = string.rep(' ', tabn + 2) .. self.name:__tostring() .. "\n"
      end
      s = Container.__tostring(self, verbose, tabn) ..
          name ..
          Relationship.endpoint2str(self.a, verbose, tabn + 2) .. "\n" ..
          Relationship.endpoint2str(self.b, verbose, tabn + 2) .. "\n"
    else
      local sep = '--'
      if self.rtype == RELATIONSHIP.TYPES.identifying then
        sep = '=='
      end
      name = self:get_name()

      s = v2s(shortClassName(self)) .. ' ' .. name ..
          Relationship.endpoint2str(self.a, false, 0) .. ' ' .. sep ..
          Relationship.endpoint2str(self.b, false, 0)
    end
    return s
  end
  return 'nil'
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return boolean
function Relationship:isIntersects(x1, y1, x2, y2)
  local fx1, fy1, fx2, fy2 = self:get_coords()
  if (fx1 <= x2 and fx2 >= x1) and (fy1 <= y2 and fy2 >= y1) then
    local inv = self:inventory()
    for _, elm in ipairs(inv) do
      -- skip points?
      if elm:isIntersects(x1, y1, x2, y2) then
        return true
      end
    end
  end
  return false
end

---@param endpoint env.draw.notation.db.RelationshipEnd
---@param canvas env.draw.Canvas
---@param layer number
local function draw_endpoint(endpoint, canvas, layer)
  if endpoint and endpoint.elms then
    local elms = endpoint.elms
    if elms.head and elms.head.color ~= '' then
      elms.head:draw(canvas, layer)
    end
    if elms.mod and elms.mod.color ~= '' then
      elms.mod:draw(canvas, layer)
    end
  end
end

---@param canvas env.draw.Canvas
---@param layer number
function Relationship:draw(canvas, layer)
  local lines = self:inventory()

  if canvas and lines then
    for _, elm in ipairs(lines) do
      elm:draw(canvas, layer)
    end
    draw_endpoint(self.a, canvas, layer)
    draw_endpoint(self.b, canvas, layer)

    if self.name and self.name.text ~= '' then
      self.name:draw(canvas, layer)
    end
  end
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Relationship:orderedkeys(opts)
  if (opts or E).cli then
    return ORDERED_KEY_LIST_CLI
  end
  return ORDERED_KEY_LIST
end

---@diagnostic disable-next-line: unused-local
function Relationship:get_child_at(x, y)
  -- to edit as a whole object not a separete lines from self.inv
  return nil
end

---@param ep table
---@param x number
---@param y number
---@return boolean
function Relationship.is_rel_end_pos(ep, x, y)
  local headp = ((ep or E).elms or E).head
  return headp and headp:isInsidePos(x, y)
end

--
-- if given pos(x,y) has relationship endpoint then return it
-- otherwise return nil
--
---@return env.draw.notation.db.RelationshipEnd?
function Relationship:get_endpoint_at(x, y)
  if Relationship.is_rel_end_pos(self.a, x, y) then
    return self.a
  elseif Relationship.is_rel_end_pos(self.b, x, y) then
    return self.b
  end
end

---@param ep env.draw.notation.db.RelationshipEnd
local function move_endpoint(ep, offx, offy)
  if (offx ~= 0 or offy ~= 0) and ep and ep.elms then
    if ep.elms.head then ep.elms.head:move(offx, offy) end
    if ep.elms.mod then ep.elms.mod:move(offx, offy) end
  end
end

---@param offy number
---@param offx number
function Relationship:move(offx, offy)
  assert(type(offy) == 'number', 'offy')
  assert(type(offx) == 'number', 'offx')
  if offx == 0 and offy == 0 then
    return
  end

  local x1, y1, x2, y2 = self:get_coords()
  log_trace('move offset(%s:%s) before(%s:%s %s:%s)', offx, offy, x1, y1, x2, y2)

  for _, line in ipairs(self.inv or E) do
    line:move(offx, offy)
  end

  move_endpoint(self.a, offx, offy)
  move_endpoint(self.b, offx, offy)

  self.name:move(offx, offy)

  self:set_coords(x1 + offx, y1 + offy, x2 + offx, y2 + offy)
end

---@return env.draw.Line?
---@return number index in inv
local function get_next_line(inv, x2, y2)
  for i, elm in ipairs(inv) do
    if elm.x1 and elm.x1 == x2 and elm.y2 == y2 then
      return elm, i
    end
  end
  return nil, -1
end

-- performing the action will reduce the line to less than the allowed minimum
-- dimensions. in which it must be cleaned properly
-- for example, when trying to reduce the line size to less than 2-char in a
-- compound line, remove the rotation angle and straighten the line into a straight line
---@param line env.draw.Line
---@param up number
---@param right number
---@param down number
---@param left number
local function get_reduce_line_direction(line, up, right, down, left)
  local x1, y1, x2, y2 = line:get_coords()
  local is_vertical, is_horizontal = y1 ~= y2, x1 ~= x2
  if is_horizontal then
    if x1 + left + 1 >= x2 then
      return 'L'
    elseif x2 + right - 1 <= x1 then
      -- local nline = get_next_line(inv, x2, y2)
      return 'R'
    end
  elseif is_vertical then
    if y1 + up + 1 >= y2 then
      -- local nline = get_next_line(inv, x2, y2)
      -- if nline then rotate = nline.x2 < line.x1 and 90 or -90 end
      return 'U'
    elseif y2 + down - 1 == y1 then
      return 'D'
    end
  end
  return nil
end

-- move the end of the line with turns
--
---@param self self
---@param leg env.draw.notation.db.RelationshipEnd
---@param line env.draw.Line
---@param up number
---@param right number
---@param down number
---@param left number
---@return number up
---@return number right
---@return number down
---@return number left
---@return env.draw.IEditable?
local function move_endpoint_with_turns(self, leg, line, up, right, down, left)
  log_trace('move relationship leg(endpoint)')
  local rd = get_reduce_line_direction(line, up, right, down, left)
  if rd then
    -- TODO straighten the line if there is an angle
    return 0, 0, 0, 0, nil
  end

  move_endpoint(leg, right + left, up + down)

  -- range of combined line. If this is not done, the line boundary will not be
  -- updated and you will not be able to access the extended edge of the line.
  Container.resize(self, up, right, down, left)

  return up, right, down, left, line
end

---@return number up
---@return number right
---@return number down
---@return number left
---@return env.draw.IEditable?
---@diagnostic disable-next-line: unused-local
local function drag_lines_by_corner(inv, line, cx, cy, direction)
  -- TODO move by drag corner (connection of two lines)
  -- at the moment this blocks the movement of internal lines connected together,
  -- as a result, the next step will move all the lines as a whole relationship
  return 0, 0, 0, 0, nil
end

-- used on resize and move
--
---@param cx number
---@param cy number
---@param direction string
---@param self self
---@return number up
---@return number right
---@return number down
---@return number left
---@return env.draw.IEditable?
function Relationship:offsets_on_drag(cx, cy, direction)
  local inv = self:inventory()
  local i, sz = 0, #inv
  while i <= sz do
    i = i + 1
    local elm = inv[i]
    local cls = get_class(elm)
    if cls == Line then
      local line = elm ---@cast line env.draw.Line
      if line:isInsidePos(cx, cy) then
        local up, right, down, left = elm:offsets_on_drag(cx, cy, direction)

        if up ~= 0 or right ~= 0 or down ~= 0 or left ~= 0 then -- drag
          local leg = self:get_endpoint_at(cx, cy)
          if not leg then
            return drag_lines_by_corner(inv, line, cx, cy, direction)
          end
          return move_endpoint_with_turns(self, leg, line, up, right, down, left)
        end
      end
    end
  end
  return 0, 0, 0, 0, nil
end

--
---@diagnostic disable-next-line: unused-local
function Relationship:cutoff(x1, y1, x2, y2, t)
  error('Not implemented yet')
end

---@return table {{x:number, y:number},{..}}
function Relationship:get_plist()
  local a, b = self.a, self.b
  local a_head = ((a or E).elms or E).head or E
  local b_head = ((b or E).elms or E).head or E

  local fp = { x = a_head.x, y = a_head.y }
  local lp = { x = b_head.x, y = b_head.y }

  local plist = ugeo.combined_line_to_plist(self.inv, fp, lp)
  return plist
end

---@return env.draw.notation.db.RelationshipEnd
function Relationship:get_endpoint_a()
  return self.a or E
end

---@return env.draw.notation.db.RelationshipEnd
function Relationship:get_endpoint_b()
  return self.b or E
end

--
--
---@param opts table?
function Relationship:serialize(opts)
  -- local x1, y1, x2, y2 = self:get_coords()

  local a, b = self.a, self.b
  local plist = self:get_plist()
  local name_x, name_y = self:get_name_xy()

  -- orderedkeys
  local t = {
    -- these coordinates are essentially a duplication of the area into which
    -- all the lines indicating this relationship fit.
    -- when deserialize to the object, this can be obtained from the plist values
    -- x1 = x1, y1 = y1, x2 = x2, y2 = y2,
    name = self:get_name(),
    nx = name_x,
    ny = name_y,

    rtype = self.rtype or RELATIONSHIP.TYPES.simple,
    plist = plist,

    a_et = (a or E).endtype or RELATIONSHIP.t.one_optional,
    a_ref = (a or E).ref,
    a_role = (a or E).role,

    b_et = (b or E).endtype or RELATIONSHIP.t.one_optional,
    b_ref = (b or E).ref,
    b_role = (b or E).role,
  }

  self:addClassRef(t, (opts or E).cname2id)
  return t
end

-- if name given sync code to this name(for cli edit)
---@param code number
---@param name string
local function update_mod_type_code(code, name)
  if code and name and name ~= RELATIONSHIP.NAME[code] then
    code = RELATIONSHIP.CODE[name] or RELATIONSHIP.t.one_optional
  end
  return code
end

--
-- static factory not a dynamic method of instance!
--
---@param t table
---@param id2cname table?
---@diagnostic disable-next-line: unused-local
function Relationship.deserialize(t, id2cname)
  assert(type(t) == 'table' and not getmetatable(t), 'simple table expected')

  local a_et = update_mod_type_code(t.a_et, t.a_etn)
  local b_et = update_mod_type_code(t.b_et, t.b_etn)
  local params = {
    name = t.name,
    name_x = t.nx,
    name_y = t.ny,
    a_ref = t.a_ref,
    a_role = t.a_role,
    b_ref = t.b_ref,
    b_role = t.b_role,
  }
  log_trace('deserialize rtype:%s a_et: %s b_et:%s t:%s', t.rtype, a_et, b_et, t)
  return Relationship:new(nil, t.plist, t.rtype, a_et, b_et, params)
end

function Relationship:fmtSerialized(t, deep, opts)
  opts = opts or {}

  local prev = opts.handlers
  opts.handlers = {
    plist = Relationship.dump_plist
  }
  local res = IEditable.fmtSerialized(self, t, deep, opts)
  opts.handlers = prev

  return res
end

--
-- plist ot string
--
---@param plist table{{x:number, y:number}, {x, y},..}
---@param deep number?
---@diagnostic disable-next-line: unused-local
function Relationship.dump_plist(plist, deep, ident, opts)
  deep = deep or 0
  -- if (opts or E).force_oneliner then nl, deep = "", 0 end
  local tabk = string.rep(' ', deep, '')

  local c = ''
  ident = ident or ''
  local fkv = fmt_kvpair
  local s = tabk .. v2s('plist') .. " = {"

  for i, pos in ipairs(plist) do -- a serialized element
    if i > 1 then c = ',' end
    local x, y = pos.x, pos.y
    s = s .. fmt('%s{%s,%s%s}', c, fkv('x', x, ident), ident, fkv('y', y, ident))
  end

  return s .. tabk .. "}"
end

---@param patch table
---@param rtype number
---@param ep env.draw.notation.db.RelationshipEnd
---@param name string a|b (start or end)
---@param cli boolean
local function edit_update_ex_endpoint(patch, rtype, ep, name, cli)
  assert(type(name) == 'string' and (name == 'a' or name == 'b'), 'name')
  assert(type(rtype) == 'number', 'rtype')

  local new_value, changed

  if not cli then
    -- edit contant number representation of one of 0..1, 1..1, 0..n, 1..n
    new_value, changed = Patch.update_ex_n(patch, name .. '_et', ep.endtype)
    if changed then
      Relationship.set_rel_end_type(ep, rtype, new_value)
      Patch.set_updated_risky(patch, name .. '_et')
    end
  else
    -- edit by readbale notation name one of {"0..1", "1..1", "0..n", "1..n"}
    local old_etname = RELATIONSHIP.NAME[ep.endtype]
    new_value, changed = Patch.update_ex_s(patch, name .. '_etn', old_etname)
    if changed then
      local et = RELATIONSHIP.CODE[new_value]
      Relationship.set_rel_end_type(ep, rtype, et)
      Patch.set_updated_risky(patch, name .. '_etn')
    end
  end

  Patch.update_in_sk(patch, name .. '_ref', ep, 'ref')
  Patch.update_in_sk(patch, name .. '_role', ep, 'role')
end

--
---@param self self
---@param patch table
local function edit_update_name(self, patch)
  local new_value, changed, newx, newy

  new_value, changed = Patch.update_ex_s(patch, 'name', self:get_name())
  if changed then
    self:set_name(new_value)
    Patch.set_updated_risky(patch, 'name')
  end

  local name_x, name_y = self:get_name_xy()

  newx = Patch.update_ex_n(patch, 'nx', name_x)
  newy = Patch.update_ex_n(patch, 'ny', name_y)
  if name_x ~= newx or name_y ~= newy then
    self:set_name_xy(newx, newy)
    Patch.set_updated_risky(patch, 'nx', 'ny')
  end
end

--
---@param patch table
---@return boolean
---@return table box with details
function Relationship:edit(patch)
  log_trace("edit t:%s", patch)

  local data = Patch.get_data(patch) or E
  local is_cli = data.cli == true

  edit_update_name(self, patch)

  local epa = self:get_endpoint_a()
  local epb = self:get_endpoint_b()
  local prev_rtype = self.rtype

  if not is_cli then
    -- relationship type [single|identifying] (line)
    Patch.update_in(patch, 'rtype', self)
  else -- cli edit by names, not by constant numbers
    -- relation line
    local old_rtypen = self:get_rtype_name()
    local new_rtypen, changed = Patch.update_ex_s(patch, 'rtypen', old_rtypen)
    if changed then
      self.rtype = RELATIONSHIP.CODE[new_rtypen] or RELATIONSHIP.TYPES.simple
      Patch.set_updated_risky(patch, 'rtypen')
    end
  end

  -- ends(legs) of then relationship (points or "arrow" heads)
  edit_update_ex_endpoint(patch, self.rtype, epa, 'a', is_cli)
  edit_update_ex_endpoint(patch, self.rtype, epb, 'b', is_cli)

  if Patch.is_changed(patch, 'plist') or prev_rtype ~= self.rtype then
    local plist = Patch.get_new_value(patch, 'plist')
    log_trace('rebuild elements new rtype:%s', prev_rtype ~= self.rtype)
    local params = {
      name = (self.name or E).text,
      name_x = (self.name or E).x1,
      name_y = (self.name or E).y1,
      a_ref = epa.ref,
      a_role = epa.role,
      b_ref = epb.ref,
      b_role = epb.role,
    }
    -- can throw errors
    build_elements(self, plist, self.rtype, epa.endtype, epb.endtype, params)
    Patch.set_updated_risky(patch, 'plist')
  end

  return true, patch
end

if _G.TEST then
  Relationship.edit_update_ex_endpoint = edit_update_ex_endpoint
  Relationship.move_endpoint_with_turns = move_endpoint_with_turns
  Relationship.get_reduce_line_direction = get_reduce_line_direction
  Relationship.get_next_line = get_next_line
end

class.build(Relationship)
return Relationship

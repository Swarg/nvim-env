-- 05-05-2024 @author Swar
--
-- Goals:
--  wrapped around primitive Canvas element to represent db notation (crowsfoot)
--
-- Notes:
--  - the entity attribute in ERM is a table column in the PDM
--

local log = require 'alogger'
local class = require 'oop.class'
-- local utbl = require 'env.draw.utils.tbl'
-- local Point = require 'env.draw.Point'
-- local Line = require 'env.draw.Line'
local Rectangle = require 'env.draw.Rectangle'
-- local Container = require 'env.draw.Container'
local IEditable = require 'env.draw.IEditable'
local ugeo = require 'env.draw.utils.geometry'
-- local usymbs = require 'env.draw.utils.unicode_symbols'

---@diagnostic disable-next-line: unused-local
local instanceof = class.instanceof
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local log_trace, log_debug = log.trace, log.debug

local M = {}

M.DEFAULTS = {
  ENTITY_WIDTH = 24,
  ENTITY_HEIGHT = 8,
  MIN_ENTITY_WIDTH = 8,
  MIN_ENTITY_HEIGHT = 3,
  ENTITY_ATTR_TYPE = 'id',
  ENTITY_ATTR_PROPS = 'M', -- Mandatory
}

-- ERM
local ETYPE_ENTITY = 1
local ETYPE_WEAK_ENTITY = 2
local ETYPE_ASSOCIATION = 3
local ETYPE_RELATIONSHIP = 4
-- PDM
local ETYPE_TABLE = 10

M.ETYPE = {
  -- ERM
  ENTITY = ETYPE_ENTITY,
  WEAK_ENTITY = ETYPE_WEAK_ENTITY,
  ASSOCIATION = ETYPE_ASSOCIATION,
  RELATIONSHIP = ETYPE_RELATIONSHIP,
  -- PDM
  TABLE = ETYPE_TABLE,
}


-- M.ENTITY_TAG = 'db-entity'
local ENTITY_ATTRIBUTE = 17

-- name to code
local ETYPE_NAME = {
  strong       = ETYPE_ENTITY,
  weak         = ETYPE_WEAK_ENTITY,
  association  = ETYPE_ASSOCIATION,
  relationship = ETYPE_RELATIONSHIP,
  -- shortcuts
  s            = ETYPE_ENTITY,
  w            = ETYPE_WEAK_ENTITY,
  a            = ETYPE_ASSOCIATION,
  r            = ETYPE_RELATIONSHIP,
  -- PDM
  ['table']    = ETYPE_TABLE,
  t            = ETYPE_TABLE,
}

-- code to name
local ETYPE_CODE = {
  [ETYPE_ENTITY]       = 'strong', -- regular, not weak entity
  [ETYPE_WEAK_ENTITY]  = 'weak',
  [ETYPE_ASSOCIATION]  = 'association',
  [ETYPE_RELATIONSHIP] = 'relationship',
  -- PDM
  [ETYPE_TABLE]        = 'table'
}



local LIGHT_LINE_HORIZ = '─'
local PK_UNDERSCORE = '▔'
local RECT_STYLES = {
  [ETYPE_ENTITY] = { '━', '┃', '┏┓┛┗' }, -- heavy
  [ETYPE_WEAK_ENTITY] = { '═', '║', '╔╗╝╚' }, -- double
  [ETYPE_ASSOCIATION] = { '─', '│', '╭╮╯╰' }, -- light_arc
  [ETYPE_TABLE] = { '━', '┃', '┏┓┛┗' },
}

-- ERM or ERD
local RELATIONSHIP_ONE = 101           -- 0..1  one (optional) or none
local RELATIONSHIP_ONE_MANDATORY = 111 -- 1..1  one mandatory
local RELATIONSHIP_MANY = 108          -- 0..n  any
local RELATIONSHIP_MANY_ONE = 118      -- 1..n  many, but at least one

local RELATIONSHIP_SIMPLE = 122        -- regular(simple) ----
local RELATIONSHIP_IDENTIFYING = 144   -- identifying link ====

-- PDM
local RELATIONSHIP_FROM = 200 -- no arrow
local RELATIONSHIP_TO = 201   -- arrow head


local DIR_U = ugeo.DIRECTION_TOP    -- 1
local DIR_R = ugeo.DIRECTION_RIGHT  -- 2
local DIR_L = ugeo.DIRECTION_LEFT   -- 3
local DIR_D = ugeo.DIRECTION_BOTTOM -- 4


local RELATIONSHIP_STYLES = {
  [RELATIONSHIP_SIMPLE] = { '─', '│', '┌┐┘└' }, -- light
  [RELATIONSHIP_IDENTIFYING] = { '═', '║', '╔╗╝╚' }, -- double

  -- crowsfoot notation of relationship leggs
  ENDS = {
    [RELATIONSHIP_SIMPLE] = { -- single line
      -- DIR_U, DIR_R, DIR_D, DIR_L
      [RELATIONSHIP_ONE] = {
        { '│', '│' }, { '─', '─' }, { '│', '│' }, { '─', '─' },
      },
      [RELATIONSHIP_ONE_MANDATORY] = {
        { '┼', '│' }, { '┼', '─' }, { '┼', '│' }, { '┼', '─' }
      },
      [RELATIONSHIP_MANY] = {
        { '⩛', '│' }, { '⪪', '─' }, { '⩚', '│' }, { '⪫', '─' }
      },
      -- listed here with keys for better understanding, access will go through them
      [RELATIONSHIP_MANY_ONE] = {
        [DIR_U] = { '⩛', '┼' }, -- 1
        [DIR_R] = { '⪪', '┼' }, -- 2
        [DIR_L] = { '⪫', '┼' }, -- 3
        [DIR_D] = { '⩚', '┼' }, -- 4
      },
      -- PDM
      [RELATIONSHIP_FROM] = {
        [DIR_U] = { '', '' }, -- 1
        [DIR_R] = { '', '' }, -- 2
        [DIR_L] = { '', '' }, -- 3
        [DIR_D] = { '', '' }, -- 4
      },
      [RELATIONSHIP_TO] = {
        [DIR_U] = { '⯅', '' }, -- 1
        [DIR_R] = { '⯈', '' }, -- 2
        [DIR_L] = { '⯇', '' }, -- 3
        [DIR_D] = { '⯆', '' }, -- 4
      },
    },
    [RELATIONSHIP_IDENTIFYING] = { -- double line
      [RELATIONSHIP_ONE] = {
        { '║', '║' }, { '═', '═' }, { '║', '║' }, { '═', '═' }
      },
      [RELATIONSHIP_ONE_MANDATORY] = {
        { '╫', '║' }, { '╪', '═' }, { '╫', '║' }, { '╪', '═' }
      },
      [RELATIONSHIP_MANY] = {
        { '⩛', '║' }, { '⪪', '═' }, { '⩚', '║' }, { '⪫', '═' }
      },
      [RELATIONSHIP_MANY_ONE] = {
        { '⩛', '╫' }, { '⪪', '╪' }, { '⩚', '╫' }, { '⪫', '╪' },
      },
      -- not used
      [RELATIONSHIP_FROM] = {},
      [RELATIONSHIP_TO] = {},
    }
  },
}


-- endpoint of relationship between entinties
local RELATIONSHIP_TO_READABLE = {
  [RELATIONSHIP_ONE] = '0..1',
  [RELATIONSHIP_ONE_MANDATORY] = '1..1',
  [RELATIONSHIP_MANY] = '0..n',
  [RELATIONSHIP_MANY_ONE] = '1..n',
  --
  [RELATIONSHIP_SIMPLE] = 'simple',           -- single line
  [RELATIONSHIP_IDENTIFYING] = 'identifying', -- double line

  -- PDM
  [RELATIONSHIP_FROM] = 'from', -- .
  [RELATIONSHIP_TO] = 'to',     -- >
}

local RELATIONSHIP_ENDS = {
  -- ERM
  [RELATIONSHIP_ONE] = true,
  [RELATIONSHIP_ONE_MANDATORY] = true,
  [RELATIONSHIP_MANY] = true,
  [RELATIONSHIP_MANY_ONE] = true,
  -- PDM
  [RELATIONSHIP_FROM] = true,
  [RELATIONSHIP_TO] = true
}

-- input to code
local RELATIONSHIP_FROM_READABLE = {
  -- Min&Max Cardinality 0..n same as 0..* in UML-notation
  ['0..1'] = RELATIONSHIP_ONE,
  ['1..1'] = RELATIONSHIP_ONE_MANDATORY,
  ['0..n'] = RELATIONSHIP_MANY,
  ['1..n'] = RELATIONSHIP_MANY_ONE,
  --
  ['0-1'] = RELATIONSHIP_ONE,
  ['1-1'] = RELATIONSHIP_ONE_MANDATORY,
  ['0-n'] = RELATIONSHIP_MANY,
  ['1-n'] = RELATIONSHIP_MANY_ONE,

  --
  ['simple'] = RELATIONSHIP_SIMPLE,
  ['identifying'] = RELATIONSHIP_IDENTIFYING,
  ['s'] = RELATIONSHIP_SIMPLE,
  ['i'] = RELATIONSHIP_IDENTIFYING,
  ['-'] = RELATIONSHIP_SIMPLE,
  ['='] = RELATIONSHIP_IDENTIFYING,

  -- for PDM notation
  ['.'] = RELATIONSHIP_FROM,
  ['from'] = RELATIONSHIP_FROM,
  ['>'] = RELATIONSHIP_TO,
  ['to'] = RELATIONSHIP_TO,
}

M.RELATIONSHIP = {
  NAME = RELATIONSHIP_TO_READABLE,
  CODE = RELATIONSHIP_FROM_READABLE,

  -- type of the relationship end
  t = {
    one_optional = RELATIONSHIP_ONE,            -- '0..1'  one optional
    one_mandatory = RELATIONSHIP_ONE_MANDATORY, -- '1..1'  one mandatory
    many = RELATIONSHIP_MANY,                   -- '0..n'
    many_or_one = RELATIONSHIP_MANY_ONE,        -- '1..n' -- at least one
    -- PDM
    from = RELATIONSHIP_FROM,
    to = RELATIONSHIP_TO,
  },

  -- type of relationship itself
  TYPES = {
    simple = RELATIONSHIP_SIMPLE,
    identifying = RELATIONSHIP_IDENTIFYING,
  }
}

M.STYLES = {
  LIGHT_LINE_HORIZ = LIGHT_LINE_HORIZ,
  PK_UNDERSCORE = PK_UNDERSCORE,
  RECT = RECT_STYLES,
  RELATIONSHIP = RELATIONSHIP_STYLES,
}

-- light = { '─', '│', '┌┐┘└' },

---@param etype number?
---@return string
---@return string
---@return string|table
function M.etype2rectparts(etype)
  -- local t = style and usymbols.pick_rect_style(style) or nil
  local t = RECT_STYLES[etype or ETYPE_ENTITY] or RECT_STYLES[ETYPE_ENTITY]
  local hlc, vlc, corners
  if t then
    hlc, vlc, corners = t[1], t[2], t[3]
  else
    hlc, vlc = Rectangle.DEFAULT_HLINE, Rectangle.DEFAULT_VLINE
    corners = Rectangle.DEFAULT_CORNERS
  end

  return hlc, vlc, corners
end

---@param etype string?|number
---@return number
function M.get_entity_type(etype)
  local vt = type(etype)
  if not etype then
    return ETYPE_ENTITY -- default
  elseif vt == 'number' then
    assert(ETYPE_CODE[etype], 'expected known type, got: ' .. tostring(etype))
    return etype
    --
  elseif vt == 'string' then
    return ETYPE_NAME[etype] or ETYPE_ENTITY
  end
  error('Unsuported entity type: (' .. vt .. ') ' .. v2s(etype))
end

-- readable entity type
---@param etype number
function M.get_entity_typename(etype)
  return ETYPE_CODE[etype or false] or ETYPE_CODE[ETYPE_ENTITY]
end

--
local ATTR_TAGS = {
  n = 'attribute_name',
  t = 'attribute_type',
  o = 'attribute_option',
  _ = 'attribute_underscore',
}


--------------------------------------------------------------------------------

--
--
--
---@param entity env.draw.notation.db.Entity
---@param old_attr_name string
---@param ask_value_cb function
function M.edit_attr_by_name(entity, old_attr_name, ask_value_cb)
  if not entity then
    return false, 'No Entity to edit'
  end
  if not old_attr_name or old_attr_name == '' then
    return false, 'No attr name '
  end

  local i = entity:indexof_attr(old_attr_name)
  if i <= 0 then
    return false, 'Not found attribute with name: "' .. v2s(old_attr_name) .. '"'
  end
  local attr = entity:inventory()[i]
  if not attr then
    return false, 'Not Found attribute with name: ' .. tostring(old_attr_name)
  end
  local updated, ret = IEditable.cli().edit_transaction(attr, {
    ui_read_line = function(prompt, line)
      return ask_value_cb(prompt, line)
    end
  })

  return updated, ret
end

--------------------------------------------------------------------------------


---@param rstart any -- number|string
---@param rend any   -- number|string
function M.validate_relationship(rstart, rend, rtype)
  local ok, err = true, nil
  if type(rstart) == 'string' then rstart = M.RELATIONSHIP.CODE[rstart] end
  if type(rend) == 'string' then rend = M.RELATIONSHIP.CODE[rend] end
  if type(rtype) == 'string' then rtype = M.RELATIONSHIP.CODE[rtype] end

  if not M.RELATIONSHIP.NAME[rstart or 0] then
    ok, err = false, 'start'
  end
  if not M.RELATIONSHIP.NAME[rend or 0] then
    ok, err = false, (err and err .. ' end' or 'end')
  end
  if not M.RELATIONSHIP.NAME[rtype or 0] then
    ok, err = false, (err and err .. ' type' or 'type')
  end

  return ok, rstart, rend, rtype, err
end

--
-- by direction and line type (single|double) and relationship modifier
--
---@param line env.draw.Line
---@param p table{x:number, y:number}
---@param rtype number
---@param rmod number
---@return table{head, second, direction}
function M.get_relationship_notation(line, p, rtype, rmod)
  assert(type(rtype) == 'number', 'expected number rtype')
  assert(type(rmod) == 'number', 'expected number rmod')

  local d = ugeo.get_line_direction(line, p.x, p.y)
  assert(d, 'has direction')

  local tdirs = (RELATIONSHIP_STYLES.ENDS[rtype] or E)[rmod]
  local t = (tdirs or E)[d]

  if not t then
    local i = require "inspect"
    error(fmt('type:%s rmod:%s tdirs:%s ENDS[type]:%s',
      rtype, rmod, i(tdirs), i(RELATIONSHIP_STYLES.ENDS[rtype])))
  end

  return { head = t[1], second = (t[2] or ''), direction = d, x = p.x, y = p.y }
end

---@param s string
---@return boolean
function M.is_relationship_end(s)
  local code = RELATIONSHIP_FROM_READABLE[s or false]
  return RELATIONSHIP_ENDS[code or false] == true
end

---@param s string?
---@return boolean
function M.is_valid_relationship_name(s)
  return type(s) == 'string' and string.match(s, '^%a[%w_]+$') ~= nil
end

---@param ename string
function M.build_attr_name(ename, suffix)
  ename = ename or ''
  suffix = suffix or 'Id'
  suffix = suffix:sub(1, 1):upper() .. suffix:sub(2)
  local aname = ename:sub(1, 1):upper() .. ename:sub(2) .. suffix
  return aname
end

--------------------------------------------------------------------------------
--                              cli helpers
--------------------------------------------------------------------------------

M.CLI = {}
C = M.CLI

function C.normalize_words(list)
  local t = {}
  for _, word in ipairs(list) do
    local lc = #word > 1 and word:sub(-1, -1) or nil
    if lc == ';' or lc == ',' then
      t[#t + 1] = word:sub(1, -2)
      word = ';'
    end
    t[#t + 1] = word
  end
  return t
end

---@param words table
---@param start number
---@param i number
---@return boolean
---@return table|string
---@param ent_name string?
function C.build_attr_desc(words, start, i, ent_name)
  local wn = i - start + 1
  if wn < 1 then
    return false, 'too few words to build an attribute description: ' .. v2s(wn)
  end
  if wn > 4 then
    return false, fmt('use ";" to split multiple attributes s:%s i:%s wn:%s',
      v2s(start), v2s(i), wn)
  end

  local name = words[start]

  if wn == 1 and ent_name then
    if name == 'id+' or name == '+id' then
      return true, {
        name = M.build_attr_name(ent_name),
        atype = 'id',
        props = 'M', -- Mandatory
        pk = true,   -- primary key
      }
      --
    elseif #name > 2 and name:sub(-2, -1) == 'Id' then
      return true, { name = name, atype = 'id', props = 'M', pk = false }
    end
  end

  local attr = {}

  if wn >= 1 then
    attr.name = name
  end
  if wn >= 2 then
    attr.atype = words[start + 1]
  end
  if wn >= 3 then
    attr.props = words[start + 2]
  end
  if wn >= 4 then
    local word = words[start + 3]
    if string.upper(word) ~= 'PK' then
      local msg = fmt('expected PK flag, got:"%s" ' ..
        'use quotes to define attribute props. ', v2s(word))
      return false, msg
    end
    attr.pk = true
  end

  return true, attr
end

-- Goal: parse words(args) list and build attributes description
-- {"attr_name", "attr_domain", "attr_props;" "attr2_name", ...}
--                                         ^ attr separator
-- -a [ Date date M ; Name name M] -> ";"
---@param ename string?
---@param attrs table?
---@return boolean
---@return table|string|nil
function C.parse_attributes(ename, attrs)
  if not attrs then
    return false, nil
  end

  local words = C.normalize_words(attrs)

  local t
  if attrs and next(attrs) then
    t = {}

    local start = 1
    for i, word in ipairs(words) do
      if word == ';' then
        local ok, attr = C.build_attr_desc(words, start, i - 1, ename)
        if not ok then
          return false, attr -- errmsg
        end
        t[#t + 1] = attr
        start = i + 1
      end
    end
    -- add last
    local ok, attr = C.build_attr_desc(words, start, #words, ename)
    if not ok then
      return false, attr -- errmsg
    end
    t[#t + 1] = attr
  end

  return true, t
end

--------------------------------------------------------------------------------
--           A tool for quickly creating a simplified ER model
--
--   simplification useful for studying er-modeling.
--   the main task is to quickly create entities through console commands
--

local SIMPL_NAME_ATTRS = { 'FirstName', 'LastName' }
local SIMPL_ADDRESS_ATTRS = { 'Street', 'Apartment', 'City', 'State', 'ZipCode' }
local SIMPL_CONTACT_INFO_ATTRS = { 'PhoneNumber', 'EmailAddress' }
local SIMPL_SOCIALSECNUM = { 'SocialSecurityNumber' }

--
-- add attributes without domain(type) and props
--
---@param entity env.draw.notation.db.Entity
---@param simplified_attrs table?
function M.add_simplified_attrs(entity, simplified_attrs)
  if simplified_attrs and next(simplified_attrs) then
    for _, aname in ipairs(simplified_attrs) do
      local ename = entity ~= nil and entity:getName() or ''
      if aname == 'id+' then
        if ename ~= '' then
          aname = M.build_attr_name(ename, 'Id')
          -- remove already existed
          entity:remove_attribute(aname)
        else
          aname = 'ID'
        end
        entity:add_attribute(aname, "", "", false)
        --
      elseif aname == 'name+' then
        M.add_simplified_attrs(entity, SIMPL_NAME_ATTRS)
      elseif aname == 'addr+' then
        M.add_simplified_attrs(entity, SIMPL_ADDRESS_ATTRS)
      elseif aname == 'contact+' then
        M.add_simplified_attrs(entity, SIMPL_CONTACT_INFO_ATTRS)
      elseif aname == 'ssn+' then
        M.add_simplified_attrs(entity, SIMPL_SOCIALSECNUM)
        --
      else
        -- Departament & +name --> DepartamentName
        if aname and aname:sub(1, 1) == '+' then
          aname = M.build_attr_name(ename, aname:sub(2))
        end
        entity:add_attribute(aname, "", "", false)
      end
    end
  end
end

--------------------------------------------------------------------------------
--                      Relation (Logic Model
--------------------------------------------------------------------------------

---@param e env.draw.notation.db.Entity
---@return table?
function M.entity_to_relation(e)
  if e then
    local t = {
      name = (e:getName() or 'EntityName'):gsub(' ', ''),
      attrs = {}
    }
    local attrs = e:inventory()
    for _, attr in ipairs(attrs) do ---@cast attr env.draw.notation.db.Attribute
      local aname = attr:getName()
      local atype = attr:getType() or ''

      t.attrs[#t.attrs + 1] = {
        name = aname,
        is_key = atype:upper() == 'ID' or (aname:sub(-2):upper() == 'ID')
      }
    end
    return t
  end
  return nil
end

---@param t table?
function M.relation_tostring(t)
  if type(t) == 'table' then
    local s = t.name .. ' ('
    for _, attr in ipairs(t.attrs) do
      local suffix = attr.is_key == true and ' (Key)' or ''
      s = s .. v2s(attr.name) .. suffix .. ', '
    end
    return s:sub(1, -3) .. ')'
  end
  return 'nil'
end

---@return string
---@param opts? table{rows, columns_width, align type}
function M.mk_empty_table_for_relation(t, opts)
  local ttype = (opts or E).ttype

  if ttype == 'text' then
    return M.mk_empty_str_table_for_relation(t, opts)
  elseif ttype == 'html' then
    error('Not implemented yet')
  else
    return 'not supported type ' .. v2s(ttype)
  end
end

---@param s string
---@param len number
---@param at string
function M.align_str_to(s, len, at)
  at = at or ' - ' -- center
  local slen = #s
  if slen > len then
    return s:sub(1, len)
  else
    local rems = len - slen
    local l = math.ceil(rems / 2)
    local r = rems - l
    if at == ' - ' or at == 'center' then
      return string.rep(' ', l, '') .. s .. string.rep(' ', r, '')
    elseif at == '- ' or at == 'left' then
      return s .. string.rep(' ', rems, '')
    elseif at == ' -' or at == 'right' then
      return string.rep(' ', rems, '') .. s
    end
  end
end

--
-- create an empty table(just a string) to place into nvim buffer
---@param t table?
---@return string
---@param opts? table{rows, columns_width, align type}
function M.mk_empty_str_table_for_relation(t, opts)
  if type(t) == 'table' then
    local s = t.name .. "\n"
    opts = opts or {}
    local rows = opts.rows or 4
    local columns_width = opts.columns_width or {}
    local align_type = opts.align_type

    local title = ''
    local line = ''
    local sepline = ''
    local sep = ' | '
    local lsep = '-+-'

    for i, attr in ipairs(t.attrs) do
      if attr.name then
        local len = #(attr.name or '')
        if tonumber(columns_width[i]) then
          len = columns_width[i]
        end
        title = title .. M.align_str_to(attr.name, len, align_type) .. sep
        line = line .. string.rep(' ', len, '') .. sep
        sepline = sepline .. string.rep('-', len, '') .. lsep
      end
    end

    s = s .. title:sub(1, -2) .. "\n" .. sepline:sub(1, -2) .. "\n"
    line = line:sub(1, -2)

    for _ = 1, rows, 1 do
      s = s .. line .. "\n"
    end

    return s
  end
  return 'nil'
end

--------------------------------------------------------------------------------

if _G.TEST then
  M.ATTR_TAGS = ATTR_TAGS
  M.ENTITY_ATTRIBUTE = ENTITY_ATTRIBUTE
end

return M

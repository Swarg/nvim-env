--  08-05-2024  @author Swarg
-- Goal:
-- - Represent Entity Attribute in ERM and table column in PDM
-- - store attribute parts:
--   - name
--   - type|domain
--   - options
--   - name undescode (for PrimaryKeys)

local class = require 'oop.class'
local log = require('alogger')

local u8 = require "env.util.utf8"
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'
local udb = require("env.draw.notation.db.base");

class.package 'env.draw.notation.db'

---@class env.draw.notation.db.Attribute : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, name:string, atype:string, props:string?, pk:boolean?, pos:table?): env.draw.notation.db.Attribute
---@field name string
---@field atype string
---@field props string
---@field pk boolean
---@field y number
---@field x1 number  -- name_x
---@field xne number -- name_x2 end (used to draw primary-key via underscore)
---@field xt number  -- type_x
---@field xp number  -- props_x
---@field x2 number  -- name_x
local Attribute = class.new_class(nil, 'Attribute', {
}, --[[ implements ]] IDrawable, IEditable)

local ORDERED_KEY_LIST = {
  { y = 'number' },
  { x1 = 'number' },
  { xne = 'number' },
  { xt = 'number' },
  { xp = 'number' },
  { x2 = 'number' },
  { name = 'string' },
  { atype = 'string' },
  { props = 'string' },
  { pk = 'boolean' },
}

local STYLES = udb.STYLES
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
---@diagnostic disable-next-line: unused-local
local instanceof, get_class = class.instanceof, class.get_class
local shortClassName = class.get_short_class_name
local cli = IEditable.cli()

---@diagnostic disable-next-line: unused-local
local log_trace, log_debug = log.trace, log.debug
local slen = u8.utf8_slen

-- constructor used in Attribute:new()
---param pos table{y1, x1(name_x), name_xe type_x, props_x}
function Attribute:_init(name, atype, props, primary_key)
  assert(name, 'name')
  assert(atype, 'atype')
  props = props or ''

  self.name = name
  self.atype = atype
  self.props = props or ''
  self.pk = primary_key

  self.y, self.x1, self.xne, self.xt, self.xp, self.x2 = 0, 0, 0, 0, 0, 0
end

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param mesh table?{max_name_len, max_type_len, max_props_len}
---@diagnostic disable-next-line: unused-local
function Attribute:set_coords(x1, y1, x2, y2, mesh)
  x1, y1, x2, y2 = IEditable.normalize(x1, y1, x2, y2)

  self.x1, self.y = x1, y1
  self.x2 = x2
  -- self.y2 = y1 + self:get_height() -- ignore y2

  if mesh then
    local width = math.abs(x2 - x1)
    local half = math.ceil(width / 2)
    local quarter = math.ceil(half / 2)
    local mnl = mesh.max_name_len or half
    local mtl = mesh.max_type_len or quarter
    -- local mpl = mesh.max_props_len or quarter
    -- local name_len, type_len, props_len = self:getWordsLength()
    self.xne = x1 + slen(self:getName()) --mnl
    self.xt = x1 + mnl + 2
    self.xp = self.xt + mtl + 2
    --
  else
    self:calcIdents(self.x1, self.x2)
  end

  return self
end

---@return number name_len
---@return number type_len
---@return number props_len
---@return number height
function Attribute:getWordsLength()
  local name_len = slen(self.name or '')
  local type_len = slen(self.atype or '')
  local props_len = slen(self.props or '')
  local height = self.pk == true and 2 or 1
  return name_len, type_len, props_len, height
end

--
-- positions to draw words name atype props
--
---@param x1 number
---@param x2 number
function Attribute:calcIdents(x1, x2)
  log_debug("calcIdents", x1, x2)

  local last_width = (self.x2 or 0) - (self.x1 or 0)
  local width = x2 - x1
  if last_width == width and width ~= 0 and self.x1 ~= x1 then
    self:move(self.x1 - x1, 0)
  else
    self.x1, self.x2 = x1, x2
    local name_len, type_len, props_len = self:getWordsLength()
    local total_len = name_len + type_len + props_len
    local remains = width - total_len
    local ident = math.max(2, math.ceil(remains / 2)) -- min 2
    self.xne = x1 + name_len
    self.xt = self.xne + ident
    self.xp = self.xt + type_len + ident
  end
end

--
---@return string
function Attribute:getName()
  return (self or E).name
end

---@return string
function Attribute:getType()
  return (self or E).atype
end

function Attribute:getProps()
  return (self or E).props
end

function Attribute:isPrimaryKey()
  return (self or E).pk == true
end

---@return string
function Attribute:__tostring()
  local x1, y1, x2, y2 = self:get_coords()

  local classname = shortClassName(self)

  local s = fmt('%s (%s:%s %s:%s): %s %s %s PK:%s',
    v2s(classname), v2s(x1), v2s(y1), v2s(x2), v2s(y2),
    v2s(self.name), v2s(self.atype), v2s(self.props), v2s(self.pk))

  return s
end

function Attribute:get_height()
  return self.pk == true and 2 or 1
end

--
--
---@return env.draw.notation.db.Attribute
function Attribute.cast(obj)
  if type(obj) == 'table' and instanceof(obj, Attribute) then
    return obj
  else
    -- fix possible hot code realod issue
    local b = class.base
    local ctype = b.CTYPE[obj._ctype or false] or '?'
    local cname = class.name(obj)
    local canvas_cname = class.name(Attribute)

    -- fix issue: on runtime code reloading
    if ctype == b.CTYPE[b.CLASS] and cname == canvas_cname then
      -- set updated version of the class for this instance  -- deep?
      setmetatable(obj, Attribute)
      return obj
    end

    error(fmt('expected instance of %s got: %s %s',
      v2s(canvas_cname), v2s(ctype), v2s(cname)))
  end
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layernum number
---@param canvas env.draw.Canvas
function Attribute:draw(canvas, layernum)
  local ln = layernum
  if canvas then
    canvas:draw_text(ln, self.x1, self.y, self.xne, self.y, self.name)
    canvas:draw_text(ln, self.xt, self.y, self.x2, self.y, self.atype)
    if self.props and self.props ~= '' then
      canvas:draw_text(ln, self.xp, self.y, self.x2, self.y, self.props)
    end
    if self.pk then
      local under = STYLES.PK_UNDERSCORE
      canvas:draw_line(ln, self.x1, self.y + 1, self.xne - 1, self.y + 1, under)
    end
  end
end

--
--  checking that a given figure completely covers a given area
--
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return boolean intersects
---@diagnostic disable-next-line: unused-local
function Attribute:isOverlap(x1, y1, x2, y2)
  return false
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Attribute:orderedkeys() return ORDERED_KEY_LIST end

--
---@return number, number, number, number
function Attribute:get_coords()
  local y2 = self.y + (self:get_height() - 1)
  return self.x1, self.y, self.x2, y2
end

---@param patch table{x, y, text}  modt
---@return boolean, table
function Attribute:edit(patch)
  local Patch = cli.Patch
  local update_in = Patch.update_in

  update_in(patch, 'pk', self)

  update_in(patch, 'y', self)
  update_in(patch, 'x1', self)
  update_in(patch, 'xne', self)
  update_in(patch, 'xt', self)
  update_in(patch, 'xp', self)
  update_in(patch, 'x2', self)

  if update_in(patch, 'name', self) then
    -- used for pk to underscore the name
    self.xne = self.x1 + slen(self.name)
  end

  update_in(patch, 'atype', self)
  update_in(patch, 'props', self)
  -- not change xt xp on change atype and props
  -- TODO: here need to recalculate the entire grid for all
  -- attributes in the parent entity. how?

  return Patch.has_changes(patch), patch
end

--
function Attribute:setStyle() -- background?)
  -- todo
end

function Attribute:getStyle()
  return nil
end

---@param offx number
---@param offy number
function Attribute:move(offx, offy)
  self.y = self.y + offy
  self.x1 = self.x1 + offx
  self.xne = self.xne + offx
  self.xt = self.xt + offx
  self.xp = self.xp + offx
  self.x2 = self.x2 + offx
end

--
-- Resize an element based on specified offsets along the coordinate axes
-- relative point? it drag under the vertex?
--
---@param up number
---@param right number
---@param down number
---@param left number
---@diagnostic disable-next-line: unused-local
function Attribute:resize(up, right, down, left)
  if right ~= 0 or left ~= 0 and right ~= -left then
    self.x1 = self.x1 + left
    self.x2 = self.x2 + right
    -- ? pk underscore
  end
end

---@return env.draw.notation.db.Attribute
function Attribute:copy()
  local pos = {
    x1 = self.x1,
    name_xe = self.xne,
    type_x = self.xt,
    props_x = self.xp,
    x2 = self.x2,
  }
  return Attribute:new(nil, self.name, self.atype, self.props, self.pk, pos)
end

class.build(Attribute)
return Attribute

--  08-05-2024  @author Sware
-- Goal: represent Entity in the ERM and table in the PDM

local log = require 'alogger'
local class = require 'oop.class'

local u8 = require "env.util.utf8"
-- local utbl = require("env.draw.utils.tbl");
local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Container = require 'env.draw.Container'
local Rectangle = require 'env.draw.Rectangle'
local Attribute = require 'env.draw.notation.db.Attribute'
local IEditable = require 'env.draw.IEditable'
local udb = require 'env.draw.notation.db.base'
local ugeo = require 'env.draw.utils.geometry'


class.package 'env.draw.notation.db'

---@class env.draw.notation.db.Entity : env.draw.Container
---@field new fun(self, o:table?, x1:number?, y1:number?, x2:number?, y2:number?, name:string, etype:string?, attrs:table?): env.draw.notation.db.Entity
---@field name env.draw.Text
---@field line env.draw.Line
---@field body env.draw.Rectangle
---@field etype number -- enum
---@field inv table -- attributes
local Entity = class.new_class(Container, 'Entity', {
})

local ORDERED_KEY_LIST = {
  { x1 = 'number' },
  { y1 = 'number' },
  { x2 = 'number' },
  { y2 = 'number' },
  { name = 'string' },
  { etype = 'string' },
  { attributes = 'table' },
  { w = 'number',        _def = IEditable.get_width },
  { h = 'number',        _def = IEditable.get_height },
}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local shortClassName = class.get_short_class_name
local instanceof = class.instanceof
local log_trace, log_debug = log.trace, log.debug
local etype2rectparts = udb.etype2rectparts
local get_entity_type = udb.get_entity_type
local def_or_min = ugeo.def_or_min
local slen = u8.utf8_slen
local cli = IEditable.cli()

local DEF = udb.DEFAULTS
local ETYPE = udb.ETYPE
local STYLES = udb.STYLES

--
--
---@return table{x1,y1,x2,y2,max_name_len, max_type_len, max_props_len, height}
---@param name string?
---@param atype string?
---@param props string?
local function mk_attr_mesh(self, name, atype, props)
  local x1, y1, x2, y2 = self:get_coords()
  local t = {
    x1 = x1 + 2, y1 = y1 + 2, x2 = x2 - 2, y2 = y2
  }

  local mnl, mtl, mpl = slen(name or ''), slen(atype or ''), slen(props or '')
  local th = 0

  local attrs = self:inventory()
  for _, attr in ipairs(attrs) do
    local nl, tl, pl, h = attr:getWordsLength()
    if nl > mnl then mnl = nl end
    if tl > mtl then mtl = tl end
    if nl > mpl then mpl = pl end
    th = th + h
  end

  t.max_name_len = mnl
  t.max_type_len = mtl
  t.max_props_len = mpl
  -- underline is drawn on the next line so track total height of all attrs:
  t.height = th -- how many lines are needed to place all the attributes

  return t
end

--
-- Create new Entity from basic Canvas Elements(?)
-- constructor used in Entity:new()
--
---@param ename string -- the entity name
---@param etype_name string?  one of [strong|weak|link|association]
-- function M.new_entity(ename, etype_name, width, height)
function Entity:_init(x1, y1, x2, y2, ename, etype_name, attrs)
  self.etype = get_entity_type(etype_name)
  -- border based on entity type
  local hlc, vlc, corners = etype2rectparts(self.etype)
  self.body = Rectangle:new(nil, 0, 0, 0, 0, hlc, vlc, corners)
  self.name = Text:new(nil, 0, 0, ename)
  self.line = Line:new(nil, 0, 0, 0, 0, STYLES.LIGHT_LINE_HORIZ)

  self:set_coords(x1, y1, x2, y2)

  self.inv = attrs or {} -- attributes

  -- if attrs not defined create a default attr
  if attrs == nil then
    -- id for strong entity with PK underscore + domain + options
    if self.etype == ETYPE.ENTITY then
      -- generate default attribute
      local idname = ename:sub(1, 1):upper() .. ename:sub(2) .. 'Id'
      local atype = DEF.ENTITY_ATTR_TYPE            -- id
      local props, pk = DEF.ENTITY_ATTR_PROPS, true -- default opts - Mandatory
      self:add_attribute(idname, atype, props, pk)
      --
    elseif self.etype == ETYPE.WEAK_ENTITY then
      -- empty
      --
    elseif self.etype == ETYPE.ASSOCIATION then
      -- empty
      --
    end
  end
end

---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@return self
function Entity:set_coords(x1, y1, x2, y2)
  x1, y1, x2, y2 = IEditable.normalize(x1 or 0, y1 or 0, x2 or 0, y2 or 0)
  local width = def_or_min(x2 - x1, DEF.ENTITY_WIDTH, DEF.MIN_ENTITY_WIDTH)
  local height = def_or_min(y2 - y1, DEF.ENTITY_HEIGHT, DEF.MIN_ENTITY_HEIGHT)

  x2, y2 = x1 + width, y1 + height

  if self.body then
    self.body:set_coords(x1, y1, x2, y2)
  end

  if self.line then
    self.line:set_coords(x1 + 1, y1 + 2, x2 - 1, y1 + 2)
  elseif height > 3 then
    -- create line
  end

  local title_len = slen(self:getName())
  local xoff = math.max(0, math.ceil((width - title_len) / 2))
  self.name:set_coords(x1 + xoff, y1 + 1, x1 + width, y1 + 1)

  self:align_attrs()
  return self
end

function Entity:align_attrs(mesh)
  local attrs = self:inventory()
  mesh = mesh or mk_attr_mesh(self)
  local yoff = 1

  for _, attr in ipairs(attrs) do
    local x1, y1, x2, y2 = mesh.x1, mesh.y1, mesh.x2, mesh.y2
    attr:set_coords(x1, y1 + yoff, x2, y2 + yoff, mesh)
    yoff = yoff + attr:get_height()
  end
end

---@return string
---@param self env.draw.notation.db.Entity?
function Entity.getName(self)
  return ((self or E).name or E).text or '?'
end

---@param name string
---@return self
function Entity:setName(name)
  if self.name and name then
    assert(type(name) == 'string', 'name')
    self.name.text = name
  end
  return self
end

---@return string
function Entity:getType()
  return udb.get_entity_typename(self.etype or false) or '?'
end

---@param typename string
---@return self
function Entity:setType(typename)
  local new_type = udb.get_entity_type(typename)
  assert(type(new_type) == 'number', 'new_type')
  self.etype = new_type

  if self.body then
    local hlc, vlc, corners = etype2rectparts(new_type)
    self.body:setStyle({
      hl = hlc, vl = vlc, rl = vlc, bl = hlc, corners = corners
    })
  end

  return self
end

--
---@return string
function Entity:__tostring(verbose, tabn)
  if self then
    local x1, y1, x2, y2 = self:get_coords()
    local w, h = IEditable.get_width_n_height(x1, y1, x2, y2)
    local attrs = self:inventory()
    local inv_size = #(attrs or E)
    local classname = shortClassName(self) or 'Container'
    local name = self:getName()
    local etype = self:getType()

    local s = string.format('%s(%s) %s (%s:%s %s:%s) (%sx%s) attrs: %s',
      classname, etype, name,
      v2s(x1), v2s(y1), v2s(x2), v2s(y2), v2s(w), v2s(h), inv_size)

    if self._tag and self._tag ~= '' then
      s = s .. ' ' .. v2s(self._tag)
    end

    if verbose and attrs and next(attrs) then
      s = s .. " inv:\n"
      local tabs = ''
      if tabn and tabn > 0 then
        tabs = string.rep(' ', tabn, '')
      end
      for i, elm in ipairs(attrs) do
        local line
        if elm.__tostring then
          line = elm.__tostring(elm, verbose, (tabn or 0) + 2)
        else
          line = tostring(elm)
        end
        s = s .. fmt("%s%2s: %s\n", tabs, i, line)
      end
    end

    return s
  end
  return 'nil'
end

---@return number
---@param name string
function Entity:indexof_attr(name)
  for i, attr in ipairs(self.inv) do
    if attr:getName() == name then
      return i
    end
  end
  return -1
end

--
---@param pos table{x1,x2,y1,y2, ...} mesh to align words
---@param name string
---@param atype string?
---@param props string?
---@param primary_key boolean?
---@return env.draw.notation.db.Attribute?
function Entity:add_attribute_to(pos, name, atype, props, primary_key)
  log_debug("add_attribute_to %s %s %s", name, atype, pos)
  name = name or 'entity_id'
  if not atype and name == 'qty' then atype = 'qty' end
  atype = atype or DEF.ENTITY_ATTR_TYPE

  local attributes = self:inventory()

  if self:indexof_attr(name) == -1 then
    local attr = Attribute:new(nil, name, atype, props, primary_key)
    attr:set_coords(pos.x1, pos.y1, pos.x2, pos.y2, pos)
    attributes[#attributes + 1] = attr
    return attr
  end

  return nil
end

--
--
--
---@param name string
---@param atype string?
---@param props string?
---@param primary_key boolean?
---@return env.draw.notation.db.Attribute?
function Entity:add_attribute(name, atype, props, primary_key)
  log_debug("add_attribute %s %s", name, atype)
  local pos = mk_attr_mesh(self, name, atype, props)

  local attr = self:add_attribute_to(pos, name, atype, props, primary_key)
  if attr then
    self:align_attrs(pos)
  end
  return attr
end

--
---@param name string
---@return env.draw.notation.db.Attribute?
function Entity:remove_attribute(name)
  local i = self:indexof_attr(name)
  if i > 0 then
    return table.remove(self:inventory(), i)
  end
  return nil
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layer number
---@param canvas env.draw.Canvas
function Entity:draw(canvas, layer)
  self.name:draw(canvas, layer)
  self.body:draw(canvas, layer)
  if self.line then
    self.line:draw(canvas, layer)
  end

  local attrs = self:inventory()

  if canvas and attrs then
    for _, elm in ipairs(attrs) do
      elm:draw(canvas, layer)
    end
  end
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------

function Entity:orderedkeys() return ORDERED_KEY_LIST end

function Entity:get_coords()
  local x1, y1, x2, y2
  if self.body then
    x1, y1, x2, y2 = self.body:get_coords()
  end
  return x1 or 0, y1 or 0, x2 or 0, y2 or 0
end

---@param offy number
---@param offx number
function Entity:move(offx, offy)
  assert(type(offy) == 'number', 'offy')
  assert(type(offx) == 'number', 'offx')
  if offx == 0 and offy == 0 then
    return
  end

  local x1, y1, x2, y2 = self:get_coords()
  log_trace('move offset(%s:%s) before(%s:%s %s:%s)', offx, offy, x1, y1, x2, y2)

  self.name:move(offx, offy)
  self.body:move(offx, offy)

  if self.line then
    self.line:move(offx, offy)
  end

  local attrs = self:inventory()
  for _, elm in ipairs(attrs) do
    elm:move(offx, offy)
  end
end

---@param patch table
---@return boolean
---@return table box with details
function Entity:edit(patch)
  log_trace("edit t:%s", patch)
  local Patch = cli.Patch
  local new_name, new_type, changed

  IEditable.apply_patch_to_coords_4v(self, patch)

  new_name, changed = Patch.update_ex_s(patch, 'name', self:getName())
  if changed then
    self:setName(new_name)
    Patch.set_updated_risky(patch, 'name')
  end

  new_type, changed = Patch.update_ex_s(patch, 'etype', self:getType())
  if changed then
    self:setType(new_type)
    Patch.set_updated_risky(patch, 'etype')
  end

  -- attributes
  local new_attrs, old_attrs
  new_attrs, changed, old_attrs = Patch.get_new_value(patch, 'attributes')
  if changed then
    local t = {}
    -- complete rebuild from scratch
    -- to be able to delete, add and swap attributes
    for _, serialized_attr in ipairs(new_attrs) do
      local attr = IEditable.deserialize(serialized_attr)
      assert(instanceof(attr, Attribute), 'expected Attribute')
      t[#t + 1] = attr
    end
    self.inv = t
    -- recalc align
    self:align_attrs(mk_attr_mesh(self))
    Patch.report_set_updated(patch, 'attributes', old_attrs, new_attrs)
  end


  return Patch.has_changes(patch), patch
end

--
---@diagnostic disable-next-line: unused-local
function Entity:cutoff(x1, y1, x2, y2, t)
  log_debug("cutoff")

  error('Not implemented yet')
end

function Entity:serialize(opts)
  local x1, y1, x2, y2 = self:get_coords()

  -- orderedkeys
  local t = {
    x1 = x1,
    y1 = y1,
    x2 = x2,
    y2 = y2,
    name = self:getName(),
    etype = self:getType(),
    attributes = {}
  }

  local attributes = self:inventory()
  log_trace('serialize attributes:%s opts:%s', #(attributes or E), opts)

  if (opts or E).cli then
    -- extra stuff only in cli-mod to edit coords by size(w&h)
    t.w, t.h = IEditable.get_width_n_height(x1, y1, x2, y2)
  end

  for _, attr in ipairs(attributes) do
    t.attributes[#t.attributes + 1] = attr:serialize(opts)
  end

  self:addClassRef(t, (opts or E).cname2id)
  return t
end

--
-- static factory not a dynamic method of instance!
--
---@param t table
---@param id2cname table?
---@diagnostic disable-next-line: unused-local
function Entity.deserialize(t, id2cname)
  -- avoid wrong usage Rectangle:deserialize insteds dot notation
  assert(type(t) == 'table' and not getmetatable(t), 'simple table expected')

  local attrs = {}
  for _, attr in ipairs(t.attributes or E) do
    attrs[#attrs + 1] = Attribute.deserialize(attr, id2cname)
  end

  return Entity:new(nil, t.x1, t.y1, t.x2, t.y2, t.name, t.etype, attrs)
end

--
---@param t table
---@param deep number
---@param opts table?
function Entity:fmtSerialized(t, deep, opts)
  opts = opts or {}

  local prev = opts.handlers
  opts.handlers = {
    attributes = Container.mk_inv_handler('attributes')
  }
  local res = IEditable.fmtSerialized(self, t, deep, opts)
  opts.handlers = prev

  return res
end

--------------------------------------------------------------------------------

---@param canvas env.draw.Canvas
---@param pos table{x:number, y:number}
---@return env.draw.notation.db.Entity?
function Entity.find_closest(canvas, pos)
  local dlist = canvas:get_distsq_elms_to_pos(Entity, pos.x, pos.y, 2)
  return (dlist[1] or E).elm
end

class.build(Entity)
return Entity

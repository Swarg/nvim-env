--  30-03-2024  @author Swarg
--

local class = require 'oop.class'
local log = require 'alogger'
-- local u8 = require("env.util.utf8")
local Line = require("env.draw.Line");
local Point = require("env.draw.Point");
local IDrawable = require 'env.draw.IDrawable'
local IEditable = require 'env.draw.IEditable'
local usymbols = require 'env.draw.utils.unicode_symbols'
local Patch = IEditable.cli().Patch

local E, v2s, fmt = {}, tostring, string.format
local log_trace = log.trace

class.package 'env.draw'
---@class env.draw.Rectangle : oop.Object, env.draw.IDrawable, env.draw.IEditable
---@field new fun(self, o:table?, x1:number, y1:number, x2:number, y2:number, hline_c:string?, vline_c:string?, corners_c:string|table?, bg_color:string?) : env.draw.Rectangle
---@field x1 number
---@field y1 number
---@field x2 number
---@field y2 number
---@field bg string
---@field corners table -- verteces color_corners
-- IEditable:
---@field setStyle fun(self, style:table) -- {corner:string|table, hl:string, vl:string, bottom:string?, right:string?})
---@field getStyle fun(self): table{hl, vl, corners, rl, bl}
---@field serialize fun(self, cname2id:table?, opts:table?): table
---@field deserialize fun(self, table, id2cname:table?): env.draw.Rectangle
---@field cutoff fun(self, x1, y1, x2, y2, t:table?): boolean, table?
---@field _tag string
---@field tagged fun(self, string): self  -- setter
---@field tag    fun(self): string        -- getter
local Rectangle = class.new_class(nil, 'Rectangle', {
}, --[[ implements ]] IDrawable, IEditable)

-- See Rectangle.serialize
local ORDERED_KEY_LIST = {
  { x1 = 'number' },
  { y1 = 'number' },
  { x2 = 'number' },
  { y2 = 'number' },
  { hline = 'string' },
  { vline = 'string' },
  { corners = 'string' },
  { bg = 'string' },
  { w = 'number',      _def = IEditable.get_width }, -- phantom
  { h = 'number',      _def = IEditable.get_height },
}


-- plain ascii chars
Rectangle.DEFAULT_CORNERS = { '+', '+', '+', '+' }
Rectangle.DEFAULT_VLINE = '|'
Rectangle.DEFAULT_HLINE = '-'


-- constructor used in Rectangle:new()
function Rectangle:_init(x1, y1, x2, y2, hline_c, vline_c, corners_c, bg_color)
  self.bg = self.bg or bg_color or ''
  self:extractCorners(corners_c)
  -- self.color_corners = self.color_corners or corners_c or '+'
  local color_hline = hline_c or Rectangle.DEFAULT_HLINE
  local color_vline = vline_c or Rectangle.DEFAULT_VLINE

  -- normalize for isIntersects
  x1, y1, x2, y2 = IEditable.normalize(x1, y1, x2, y2)

  self.lline = Line:new(nil, x1, y1 + 1, x1, y2 - 1, color_vline)
  self.rline = Line:new(nil, x2, y1 + 1, x2, y2 - 1, color_vline)

  self.tline = Line:new(nil, x1, y1, x2, y1, color_hline)
  self.bline = Line:new(nil, x1, y2, x2, y2, color_hline)
end

--
---@param corners string|table
function Rectangle:extractCorners(corners)
  local typ = type(corners)

  if not corners then
    self.corners = Rectangle.DEFAULT_CORNERS
    --
  elseif typ == 'table' then
    self.corners = usymbols.corners_rect_normolize(corners)
    --
  elseif typ == 'string' then
    self.corners = usymbols.corners_rect_parse(corners)
  end
end

local function get_edges_colors(self)
  local tl, rl = (self or E).tline, (self or E).rline
  local hc, vc = (tl or E).color, (rl or E).color
  return hc, vc
end

--
---@return string
function Rectangle:__tostring()
  if self then
    local x1, y1, x2, y2 = self:get_coords()
    local w, h = IEditable.get_width_n_height(x1, y1, x2, y2)
    local bg = self.bg
    if bg == '' then bg = '""' end
    local tag = self._tag and (' ' .. v2s(self._tag)) or ''

    return fmt('Rectangle (%s:%s %s:%s) (%sx%s) bg: %s%s',
      v2s(x1), v2s(y1), v2s(x2), v2s(y2), v2s(w), v2s(h), v2s(bg), tag)
  end
  return 'nil'
end

--------------------------------------------------------------------------------
--                       IDrawable Implementations
--------------------------------------------------------------------------------

---@param layer number?
---@param canvas env.draw.Canvas
function Rectangle:draw(canvas, layer)
  log.trace('Rectangle.draw has_canvas: ', canvas ~= nil)

  if canvas then
    self.tline:draw(canvas, layer)
    self.bline:draw(canvas, layer)
    self.lline:draw(canvas, layer)
    self.rline:draw(canvas, layer)
    -- corners
    local x1, y1, x2, y2 = self:get_coords()
    assert(x1 and y1 and x2 and y2, 'has coords')
    local t = self.corners
    local tl, tr, br, bl = t[1], t[2], t[3], t[4]

    canvas:draw_point(layer, x1, y1, tl)
    canvas:draw_point(layer, x1, y2, bl)
    canvas:draw_point(layer, x2, y1, tr)
    canvas:draw_point(layer, x2, y2, br)
  end
end

--------------------------------------------------------------------------------
--                        IEditable Implementations
--------------------------------------------------------------------------------


-- 'x1', 'y1', 'x2', 'y2', 'hline', 'vline', 'corners', 'bg'
function Rectangle:orderedkeys() return ORDERED_KEY_LIST end

---@param patch table  -- mkpatch(obj,{x1, y1, x2, y2, hline, vline, corners, bg})
---@return boolean
---@return table box with details
function Rectangle:edit(patch)
  local has_changes = Patch.has_changes(patch)
  log_trace("Rectangle:edit has_changes:%s patch:%s", has_changes, patch)
  if not has_changes then
    return false, patch
  end

  IEditable.apply_patch_to_coords_4v(self, patch)

  Patch.update_in(patch, 'bg', self)

  local new_corners, changed = Patch.get_new_value(patch, 'corners')
  if changed then
    self:extractCorners(assert(new_corners, 'corners'))
    Patch.set_updated_risky(patch, 'corners')
  end

  local hline_c, vline_c = get_edges_colors(self)

  if Patch.is_changed(patch, 'hline') then
    local hln_c = Patch.update_ex_s(patch, 'hline', hline_c)
    self.tline:setStyle(hln_c)
    self.bline:setStyle(hln_c)
    Patch.set_updated_risky(patch, 'hline')
  end

  if Patch.is_changed(patch, 'vline') then
    local vln_c = Patch.update_ex_s(patch, 'vline', vline_c)
    self.rline:setStyle(vln_c)
    self.lline:setStyle(vln_c)
    Patch.set_updated_risky(patch, 'vline')
  end

  return Patch.has_changes(patch), patch
end

--
-- to change lines style(chars to draw)
--
---@param style table{hl, vl, corners, rl?, bl?}
function Rectangle:setStyle(style) -- corners, top, left, bottom, right)
  self:extractCorners(style.corners)
  local top = style.hl or self.tline:getStyle() or Rectangle.DEFAULT_HLINE
  local left = style.vl or self.lline:getStyle() or Rectangle.DEFAULT_VLINE
  local right = style.rl or self.rline:getStyle() or left
  local bottom = style.bl or self.bline:getStyle() or top

  self.lline:setStyle(left)
  self.rline:setStyle(right)
  self.tline:setStyle(top)
  self.bline:setStyle(bottom)
end

--
---@return table
function Rectangle:getStyle()
  local corners = table.concat(self.corners, '')
  local top = self.tline:getStyle()
  local left = self.lline:getStyle()
  local right = self.rline:getStyle()
  local bottom = self.bline:getStyle()
  return { corners = corners, hl = top, vl = left, rl = right, bl = bottom }
end

---@param offx number
---@param offy number
---@return self
function Rectangle:move(offx, offy)
  assert(type(offy) == 'number', 'offy')
  assert(type(offx) == 'number', 'offx')
  local x1, y1, x2, y2 = self:get_coords()
  log_trace('move offset(%s:%s) before(%s:%s %s:%s)', offx, offy, x1, y1, x2, y2)
  self:set_coords(x1 + offx, y1 + offy, x2 + offx, y2 + offy)
  return self
end

-- topleft in canvas
---@return number x left
---@return number y top
function Rectangle:pos()
  local x1, y1, _, _ = self:get_coords()
  return x1 or 0, y1 or 0
end

---@param self self
---@return number x1
---@return number y1
---@return number x2
---@return number y2
function Rectangle:get_coords()
  local bl, tl = (self or E).bline, (self or E).tline
  if bl and tl then
    local x1, y1, x2, y2 = tl.x1, tl.y1, bl.x2, bl.y2
    return x1, y1, x2, y2
  else
    -- error('rectangle without sides?')
  end
  return 0, 0, 0, 0
end

---@return self
function Rectangle:set_coords(x1, y1, x2, y2)
  if self.rline then
    self.lline:set_coords(x1, y1 + 1, x1, y2 - 1)
    self.rline:set_coords(x2, y1 + 1, x2, y2 - 1)

    self.tline:set_coords(x1, y1, x2, y1)
    self.bline:set_coords(x1, y2, x2, y2)
  end
  return self
end

-- Rectangle:offsets_on_drag(cx, cy, direction) from IEditable

--
-- Resize an element based on specified offsets along the coordinate axes
--
---@param up number
---@param right number
---@param down number
---@param left number
function Rectangle:resize(up, right, down, left)
  if up ~= 0 or right ~= 0 or down ~= 0 or left ~= 0 then
    local x1, y1, x2, y2 = self:get_coords()

    local nx1 = x1 + left
    local ny1 = y1 + up
    local nx2 = x2 + right
    local ny2 = y2 + down

    if nx1 == nx2 or ny1 == ny2 then
      return false -- block attempts to shrink to line width
    end

    self:set_coords(nx1, ny1, nx2, ny2)
  end
end

-- strategy: do not save each Lines as objec
--
---@param opts table?{cname2id, cli, x, y}
---@return table
function Rectangle:serialize(opts)
  local x1, y1, x2, y2 = self:get_coords()
  local hline_c, vline_c = get_edges_colors(self)

  -- orderedkeys
  local t = {
    x1 = x1,
    y1 = y1,
    x2 = x2,
    y2 = y2,
    bg = self.bg,
    hline = hline_c,
    vline = vline_c,
    corners = table.concat(self.corners, ''),
  }

  log_trace('serialize opts:%s', opts)
  if (opts or E).cli then
    -- extra stuff only in cli-mod to edit coords by size(w&h)
    t.w, t.h = IEditable.get_width_n_height(x1, y1, x2, y2)
  end

  self:addClassRef(t, (opts or E).cname2id)
  return t
end

--
-- static factory not a dynamic method of instance!
--
---@param t table
---@param id2cname table?
---@diagnostic disable-next-line: unused-local
function Rectangle.deserialize(t, id2cname)
  -- avoid wrong usage Rectangle:deserialize insteds dot notation
  assert(type(t) == 'table' and not getmetatable(t), 'simple table expected')

  return Rectangle:new(nil, t.x1, t.y1, t.x2, t.y2,
    t.hline, t.vline, t.corners, t.bg):tagged(t._tag)
end

--
--
---@param x1 number
---@param y1 number
---@param x2 number
---@param y2 number
---@param t table?
---@return boolean, table?
function Rectangle:cutoff(x1, y1, x2, y2, t)
  if self:isIntersects(x1, y1, x2, y2) then
    local xx1, yy1, xx2, yy2 = self:get_coords()
    assert(xx1 and yy1 and xx2 and yy2, 'coords')
    t = t or {}
    local c = self.corners
    local tl, tr, br, bl = c[1], c[2], c[3], c[4]

    local function cutline(line)
      if not line:cutoff(x1, y1, x2, y2, t) then
        t[#t + 1] = line:copy()
      end
    end
    local function cutpoint(x, y, color)
      local is_intersects = x >= x1 and x <= x2 and y >= y1 and y <= y2
      if not is_intersects then
        t[#t + 1] = Point:new(nil, x, y, color)
      end
    end

    cutline(self.tline)
    cutline(self.rline)
    cutline(self.bline)
    cutline(self.lline)

    cutpoint(xx1, yy1, tl)
    cutpoint(xx1, yy2, bl)
    cutpoint(xx2, yy1, tr)
    cutpoint(xx2, yy2, br)

    return true, t
  end

  return false, t
end

class.build(Rectangle)
return Rectangle

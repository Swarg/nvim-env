-- 23-05-2023 @author Swarg
local M = {}
--

function M.hex_to_char(x)
  return string.char(tonumber(x, 16))
end

function M.char_to_hex(c)
  return string.format("%%%02X", string.byte(c))
end

-- escape
function M.url_encode(url)
  if url == nil then
    return
  end
  url = url:gsub("\n", "\r\n")
  url = url:gsub("([^%w ])", M.char_to_hex)
  url = url:gsub(" ", "+")
  return url
end

-- unescape
-- Example usage:
-- local url = "/test/some%20string?foo=bar"
-- print(unescape(url)) -- /test/some string?foo=bar
function M.url_decode(url)
  if url == nil then
    return
  end
  url = url:gsub("+", " ")
  url = url:gsub("%%(%x%x)", M.hex_to_char)
  return url
end

return M

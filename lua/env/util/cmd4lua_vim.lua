-- 26-10-2023 @author Swarg
--
-- UseCase tooling for nvim
--  - find the handler function by the command name
--  - generate code for command handler
--  - factory to build instance of the Cmd4Lua "with dependencies":
--     - token_parser    - to override a simple parse_line
--     - generate_handle - to implements extended built command for inspect thelp
--
local M = {}


local log = require('alogger')
local Cmd4Lua = require('cmd4lua')
local c4lbase = require('cmd4lua.base')
local cb = require('env.util.clipboard')
local tp = require("env.util.tokenizer.parser")

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
-- local uv = R.require("luv", vim, "loop")             -- vim.loop

local E = {}

---@param line string
function M.find_cmdnames(line)
  local name, sn
  -- :cmd('name', 'short-name')  or :cmd('name', 'short-name', 'n3')
  name, sn = line:match(":?cmd%(['\"](.-)['\"],%s*['\"](.-)['\"],?.*%)")
  if not name then
    name = line:match(":?cmd%(['\"](.-)['\"]%)") -- :cmd('name')
    sn = name
  end

  if name and sn then
    name, sn = c4lbase.fullname_first(name, sn)
  end
  return name, sn
end

-- :desc('some text')  --> "some text"
function M.find_desc(line, def)
  local desc = (line or ''):match(":desc%(['\"](.-)['\"]%)") or def
  return desc
end

--
-- UseCase: GotoSourceOf -- jump to function from line with :cmd('name')
--
-- Cmd4Lua:  handlers(M):cmd('fullname', 'shortname') -> M.cmd_fullname
--
---@param ctx table{bufnr, bufname}
---@param line string
---@param callback function
function M.find_function_by_cmdname(ctx, line, callback)
  local name, sn = M.find_cmdnames(line)
  if not name then
    return false
  end
  log.debug('find a line with Cmd4Lua cmd definition')

  local bufnr = ctx.bufnr or 0
  local curr_lnum = (vim.api.nvim_win_get_cursor(0) or {})[1]
  local lines = vim.api.nvim_buf_get_lines(bufnr, curr_lnum, -1, false)
  local func_name = c4lbase.get_cmdhandler_funcname(name)
  local ptrn = '^%s*function [%w_%.]+%.' .. func_name .. '%(.+%).*$'
  for n, line0 in pairs(lines) do
    if line0:match(ptrn) then
      -- print("[DEBUG] ctx:", inspect(ctx))
      local line_nr = curr_lnum + n
      log.debug('find the handler function: "%s" at lnum: %s', line_nr, name)
      assert(type(callback) == 'function')
      return callback(ctx.bufname, line_nr)
    end
  end
  log.debug('Not found function for names: "%s", "%s"', name, sn)
  print('Cannot find the func:', func_name, 'for name:', name)

  return true -- stop another find-handlers
end

--------------------------------------------------------------------------------
--                               GenCode
--------------------------------------------------------------------------------

--
function M.gen_code_cmd_handler(fname, body, comment)
  body = body or "\n"
  comment = comment or ''
  if #comment > 0 then comment = " " .. tostring(comment) end
  return
      "--\n--" .. comment .. "\n--\n" ..
      "---@param w Cmd4Lua\n" ..
      "function M." .. fname .. "(w)\n" ..
      "  if not w:is_input_valid() then return end\n" ..
      body ..
      "end\n"
end

-- generate cmd handler the node with subcommands
-- tsubcmds - is a table for collect subcommands (for nested subs in same file)
---@param fname string
---@param subcommands table
function M.gen_code_cmd_node(fname, subcommands) --, root)
  local tsubcmds = string.format("M._%s_subs", fname:match("cmd_(.+)") or fname)
  local head =
  -- tsubcmds .. " = {}\n\n" ..
      "--@param w Cmd4Lua\n" ..
      "function M." .. fname .. "(w)\n" ..
      "  w:handlers(" .. tsubcmds .. ")\n"

  local body = ''
  local def_cmdname = ''
  if type(subcommands) == 'string' then
    def_cmdname = subcommands
  end
  --
  if type(subcommands) == 'table' then
    for _, cmd in ipairs(subcommands) do
      body =
          "      :desc('')\n" ..
          "      :cmd('" .. cmd .. "')\n"
    end
  else -- stub
    body =
        "      :desc('')\n" ..
        "      :cmd('" .. def_cmdname .. "')\n"
  end
  local footer =
      "\n" ..
      "      :run()\n" ..
      "end\n"

  return head .. body .. footer
end

--------------------------------------------------------------------------------
--         Cmd4Lua Factory to build instance with dependencies
--------------------------------------------------------------------------------

local I = {} -- implementations (dependencies)
M.Implementations = I

--
-- Create instance of Cmd4Lua with "dependencies"
-- so that they are accessible through
-- чтобы они были доступны через
-- UseCase:
--   - to pass nvim opts into command handler
--   so that they are accessible through  w.vars.vim_opts
--
--   - add the extension "generate_handle" -- by overriding the default stub func
--   so that you can interact with the thelp object directly through commands:
--   :YourCommand -_G help
--
---@param opts table|nil
---@param impls table|nil
---@return Cmd4Lua
function M.newCmd4Lua(opts, impls)
  -- for nvim
  opts = opts or E
  local args = opts.args or M.join_grouped_args(opts.fargs)
  local vars = { vim_opts = opts }
  log.debug('newCmd4Lua args', args)
  ---@diagnostic disable-next-line: inject-field
  -- c4l.debug = true
  -- c4l.autofind_src_root()
  return Cmd4Lua.of(args, I, impls)
      :self_print_callback_userdata()
      :set_vars(vars)
end

-- join args back to line to parse by parser.Tokenizer passed to Cmd4Lua
---@param args table|string
---@return table|string
function M.join_grouped_args(args)
  if type(args) == 'table' and #args > 0 then
    -- check is string contains groups then - join args to a line to reparsing
    for _, arg in ipairs(args) do
      local c = arg:sub(1, 1)
      if c == "'" or c == '"' or c == '[' or c == '{' or c == '\\' then
        return table.concat(args, ' ') -- list to string to reparce
      end
    end
  end
  return args
end

--------------------------------------------------------------------------------

--
-- Own implementation to override standart Cmd4Lua.I.parse_line
-- add features to rich line parseing as:
--
--   x'abc c'X"f d"  -->  "xabc Xf d"      quoted groups
--   [ab c]          -->  {"ab", "c"}      string array
--   {key = 1}       -->  {key = 1}        text with lua-tables to tables itself
--
---@param line string
---@return table -- args
function I.parse_line(line)
  if not line or line == '' then
    return {} -- empty args
  end
  local t = tp.Tokenizer:new(line):tokenize()
  return t:buildObject()
end

I.print_userdata = nil

--
--- fix input(command) on fly
--  fires when a command is expected but the argument entered
--  is not one of the known commands
--
---@param obj Cmd4Lua
---@return string|nil
function I.fix_input_callback(obj)
  log.debug('fix_input_callback')

  local help = obj:build_help()
  local completion = nil
  local new_input = nil
  ---@diagnostic disable-next-line: undefined-field
  local prev_input, orig = '', obj.original_input

  --[[
  if type(orig) == 'table' then
    prev_input = table.concat(orig, ' ')
  elseif type(orig) == 'string' then
    prev_input = orig
  end

  -- reduce input only to latest command context
  -- root subcmd_depth1 subcmd2 arg1 arg2 --key  --> arg1 arg2
  ---@diagnostic disable-next-line: undefined-field
  local path = obj:get_cmd_path()
  if #path > 0 then
    local last_cmd = path[#path]
    local i = prev_input:find(last_cmd)
    if i then
      prev_input = prev_input:sub(i + #last_cmd)
    end
  end
  ]]

  vim.ui.input({ prompt = help, default = prev_input, completion = completion },
    function(input)
      new_input = input
    end)

  if new_input and new_input ~= '' then
    return new_input
  end
end

--------------------------------------------------------------------------------

local GH = {} -- handlers for cmds from generate_handle

--------------------------------------------------------------------------------
--                             Generate
--------------------------------------------------------------------------------

--
-- hook called in Cmd4Lua before actual work in method
-- :run() and :is_input_valid()
--
-- UseCase:
--   - generate content based a data from prarsed command handler (complete)
--   (research and inspect the inner data structure with built thelp data)
--
-- UsageExmaples:
-- :EnvLog -_G help
-- :EnvLog -_G complete
--   ^
--   any command for handling to which this method is passed via w:implements()
--
---@param args table
---@param obj Cmd4Lua
function I.generate_handle(args, obj)
  Cmd4Lua.of(args)
      :about('Generate stuff')
      :handlers(GH)
      :set_var('obj', obj)

      :desc('Generate lua code for nvim complete with all commands')
      :cmd('complete')

  --  research and inspect helpers
      :desc('Show list of the available handlerfunctions')
      :cmd('handlers-list', 'hi')

      :desc('Inspect the Cmd4Lua instance (With tviewer)')
      :cmd('inspect', 'i')

      :run()

  return true
end

---@param w Cmd4Lua
function GH.cmd_complete(w)
  local t = {}
  local obj = w:get_var('obj') -- w.vars.obj
  if c4lbase.is_instanceof_cmd4lua(obj) then
    -- if obj.thelp and obj.thelp.cmds then

    local cmds = (obj.collector.thelp or {}).cmds
    if not cmds then
      obj:print('No commands')
      return
    end

    for _, cmd in pairs(cmds) do
      t[#t + 1] = cmd.name
    end

    local str = 'local _commands = ' .. inspect(t) .. "\n" ..
        "M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }\n"

    obj:print(str)
    cb.copy_to_clipboard(str)
  else
    print('no inspect')
  end
end

-- same options for frequent use
local function define_opt_view(w)
  local view = w:desc('open result in table-viewer')
      :has_opt('--view', '-v')
  local open_node = w:desc('open a node path(cd)')
      :optl('--open-node', '-o')

  return view, open_node
end

local function open_in_tviewer(obj, name, open_node)
  local ok, tviewer = pcall(require, "env.util.tbl_viewer")
  if not ok then
    print('Error:', tviewer)
    return false
  end
  name = name or 'Cmd4Lua'
  return tviewer.open(obj, name, open_node)
end

---@param w Cmd4Lua
function GH.cmd_handlers_list(w)
  local view, open_node = define_opt_view(w)

  if w:is_input_valid() then
    local obj = w:get_var('obj') -- w.vars.obj
    if c4lbase.is_instanceof_cmd4lua(obj) then
      if view then
        open_in_tviewer(obj.thelp.handlers, 'Cmd4Lua:handlers', open_node)
      else
        local line = ''
        for k, v in pairs(obj.thelp.handlers) do
          line = line .. k .. ':' .. tostring(v) .. "\n"
        end
        print(line)
      end
    else
      print('No Cmd4Lua instance.')
    end
  end
end

---@param w Cmd4Lua
function GH.cmd_inspect(w)
  local view, open_node = define_opt_view(w)

  if w:is_input_valid() then
    local obj = w:get_var('obj')
    if view then
      open_in_tviewer(obj, 'Cmd4Lua', open_node)
    else
      print(inspect(obj))
    end
  end
end

return M

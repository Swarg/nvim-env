-- 19-01-2024 @author Swarg
-- Goal: create command to run single bats test
local M = {}
-- local log = require('alogger')

---@param bufname string
---@param line string
function M.get_cmd_to_run_single_test(bufname, line)
  local t = { cmd = nil, args = nil }
  if line then
    local test_name = string.match(line, '^%s*@test .*[\'"](.*)[\'"]%s*{?%s*$')
    if not test_name then
      t.err = 'Cannot find test name'
    else
      t.cmd, t.args = 'bats', { '-f', '^' .. test_name .. '$', bufname }
      t.envs = { 'BATS_RUN_SKIPPED=true' }
      t.cwd = bufname:match("(.*[/\\])") -- extract dir
      -- log.debug('bats cmd', t)
    end
  end
  return t
end

return M

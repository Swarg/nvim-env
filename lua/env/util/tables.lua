-- 28-05-2023 @author Swarg
local M = {}

---@param list table
---@return number
function M.indexof(list, elm)
  for i, v in ipairs(list) do
    if v == elm then
      return i
    end
  end
  return -1
end

---@param list table
---@param variants table
---@return any
---@return number -1 not found
function M.next_after_anyof(list, variants)
  for i, v in ipairs(list) do
    local variant = M.indexof(variants, v)
    if variant > -1 then
      return list[i + 1], variants[variant]
    end
  end
  return nil, -1
end

---@return any
---@return any
function M.prevtwo_before_elm(list, elm)
  for i, v in ipairs(list) do
    if elm == v then
      return list[i - 2], list[i - 1]
    end
  end
  return nil, nil
end

-- Return Value from table path specified by key names
---@param tbl table
function M.tbl_value_at_path(tbl, ...)
  if type(tbl) == 'table' then
    local e = tbl
    for i = 1, select('#', ...) do
      local name = select(i, ...)
      if not name then break end
      e = e[name]
      if not e then return nil end
    end
    return e
  end
end

-- Find in table-list the table entry with given key-value pair
---@param tbl_list table
---@param key any
---@param value any
---@return table|nil
function M.tbl_entry_with(tbl_list, key, value)
  if key and value and type(tbl_list) == 'table' then
    for _, e in pairs(tbl_list) do
      if type(e) == 'table' then
        for k, v in pairs(e) do
          if k == key and v == value then
            return e
          end
        end
      end
    end
  end
end

-- deepcopy function that works with recursive tables.
-- This is done by creating a table of tables that have been copied and feeding
-- it as a second argument to the deepcopy function.
-- Save copied tables in `copies`, indexed by original table.
function M.deep_copy(orig, copies)
  copies = copies or {}
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
    if copies[orig] then
      copy = copies[orig]
    else
      copy = {}
      copies[orig] = copy
      for orig_key, orig_value in next, orig, nil do
        copy[M.deep_copy(orig_key, copies)] = M.deep_copy(orig_value, copies)
      end
      setmetatable(copy, M.deep_copy(getmetatable(orig), copies))
    end
  else -- number, string, boolean, etc
    copy = orig
  end
  return copy
end

-- calculate the string length needed to convert table to string
---@param t table|any
---@param passed table|nil
function M.length_as_string(t, passed)
  passed = passed or {}
  local t_type, len, keys, elms = type(t), 0, 0, 0

  if t_type == 'table' then
    if passed[t] then
      -- copy = passed[t]
    else
      passed[t] = true
      for key, value in next, t, nil do
        local ktype = type(key)
        if ktype ~= 'number' then
          local len0k, keys0k, elms0k = M.length_as_string(key, passed)
          len = len + len0k + 3 -- ' = '
          keys = keys + 1 + keys0k
          elms = elms + elms0k
          if ktype == 'string' then -- no quotes for key by default
            len = len - 2           -- "key" -> key
          end
        else
          elms = elms + 1
        end
        -- len = len + M.length_as_string(value, passed) + 2 -- ', '
        local len0v, keys0v, elms0v = M.length_as_string(value, passed)
        len = len + len0v + 2 -- ', '
        keys = keys + 1 + keys0v
        elms = elms + elms0v
      end
      len = len + 2 -- {#...#} ( last ', ' => '{' + '}'
    end
  elseif t == nil then
    len = 3
  elseif t_type == 'string' then
    len = #t + 2 -- quotes
  elseif t == true then
    len = 4
  elseif t == false then
    len = 5
  else -- number, string, boolean, etc
    len = #tostring(t)
  end

  return len, keys, elms
end

---@param s string
function M.str2code(s)
  local has_new_lines = s:find("\n", 1, true) ~= nil
  local has_squote = s:find('\'', 1, true)
  local has_dquote = s:find('"', 1, true)

  if not has_new_lines then
    if has_squote and not has_dquote then
      s = '"' .. s .. '"'
    elseif not has_squote and has_dquote then
      s = "'" .. s .. "'"
    else -- both
      local f = s:sub(1, 1)
      if f == "'" then
        s = '"' .. s:gsub('"', '\\"') .. '"'
      else
        s = "'" .. s:gsub("'", "\\'") .. "'"
      end
    end
    --
  elseif not has_dquote then
    s = '"' .. s:gsub("\n", '\\n') .. '"'
  else
    s = '"' .. s:gsub('"', '\\"'):gsub("\n", '\\n') .. '"'
  end
  return s
end

---@param t table
---@param sort_func function?
---@return table
function M.sorted_keys(t, sort_func)
  assert(type(t) == 'table', 'table expected')
  local kt = {}
  for k, _ in pairs(t) do
    kt[#kt + 1] = k
  end

  table.sort(kt, sort_func)

  return kt
end

M.func_compare_descending = function(a, b)
  if a ~= nil and b ~= nil then
    return a > b
  else
    return false
  end
end -- descending


---@param t table
function M.split_to_keys_and_indexes(t, nosort)
  local nums, strs, another = {}, {}, {}
  for k, _ in pairs(t) do
    local kt = type(k)
    if kt == 'number' then
      nums[#nums + 1] = k
    elseif kt == 'string' then
      strs[#strs + 1] = k
    else
      another[#another + 1] = k
    end
  end
  if not nosort then
    -- table.sort(nums)
    table.sort(strs)
  end
  return nums, strs, another
end

---@param t table
---@return table
---@param entry_value any?
function M.list_to_map(t, entry_value)
  local m = {}
  assert(type(t) == 'table', 't')
  for i, v in ipairs(t) do
    m[v] = entry_value ~= nil and entry_value or i
  end
  return m
end

--
-- Apply desired order to sorted string keys
--
-- skeys: {'a', 'b', 'c'}  desired order: {'b', 'a'} --> { 'b', 'a', 'c' },
--
---@param skeys table
---@param desired_order table
function M.apply_order(skeys, desired_order)
  local new_order = {}
  local passed = {}
  local map = M.list_to_map(skeys, true)

  for _, k in ipairs(desired_order) do
    if map[k] then
      new_order[#new_order + 1] = k
      passed[k] = true
    end
  end

  for _, k in ipairs(skeys) do
    if not passed[k] then
      new_order[#new_order + 1] = k
    end
  end

  return new_order
end

-- Fisher-Yates shuffle:
---@param t table - list to be shuffled
---@return table
function M.shuffle2new(t)
  if not t then return {} end -- ?
  if #t == 1 then return { t[1] } end

  local tbl = {}
  for i = 1, #t do tbl[i] = t[i] end -- flat copy

  for i = #tbl, 2, -1 do
    local j = math.random(i)
    tbl[i], tbl[j] = tbl[j], tbl[i]
  end
  return tbl
end

---@param list table
---@param s number?
---@param e number?
---@return table
function M.get_sublist(list, s, e)
  local t = {};
  s = s or 1
  e = e or #list
  if e > #list then e = #list end
  if s < 1 then s = 1 end

  if s < e then
    for i = s, e do t[#t + 1] = list[i] end
  elseif s >= e then
    for i = s, e, -1 do
      t[#t + 1] = list[i]
    end
  end
  return t
end

-- without protection against cyclic references
---@param map table
---@param t table? output
function M.flatten_map_to_list(map, t)
  t = t or {}
  for _, v in pairs(map) do
    if type(v) == 'table' then
      M.flatten_map_to_list(v, t)
    else
      t[#t + 1] = v
    end
  end
  return t
end

--
-- {k=v, b=9} ---> {{"k", "v"}, "b", 9}
--
function M.map_to_list(map, t)
  t = t or {}
  for k, v in pairs(map) do
    t[#t + 1] = { k, v }
  end
  return t
end

-- table to string
---@param t table
---@param tab string|nil
---@param passed table|nil
---@param order table? desired order of string keys
---@param each_key_new_line boolean?
function M.table2code(t, tab, passed, order, each_key_new_line)
  if type(t) ~= 'table' then error('expected table t got:' .. type(t)) end
  assert(not tab or type(tab) == 'string', 'tab')

  tab = tab or ''
  local nl = tab == '' and ' ' or "\n"

  local function v2s(v, tab0)
    if type(v) == 'string' then
      return M.str2code(v) -- "'" .. line .. "'"
      --
    elseif type(v) == 'number' or type(v) == 'boolean' then
      return tostring(v)
      --
    elseif type(v) == 'table' then
      if tab0 ~= nil and tab0 ~= '' then tab0 = tostring(tab0) .. '  ' end
      return M.table2code(v, tab0, passed, nil, each_key_new_line)
    else
      return M.str2code(type(v))
    end
  end

  local s = '{'
  local len, keys, elms = M.length_as_string(t, passed)
  if each_key_new_line then
    nl = "\n"
    -- tab = '    '
  elseif len < 60 and nl == "\n" then
    nl = ' '
    tab = ''
  end
  if keys == 0 and elms == 0 then
    return '{}'
  end

  local indexes, str_keys, another = M.split_to_keys_and_indexes(t)
  if order and next(order) then
    str_keys = M.apply_order(str_keys, order)
  end

  -- keys of table as map

  for _, key in ipairs(str_keys) do
    local v = t[key]
    if (key:match('[^%w_]+') or key:match('^%d')) then
      key = "['" .. key .. "']"
    end
    s = s .. nl .. tab .. key .. ' = ' .. v2s(v, tab) .. ','
  end

  -- indexes of table as arraylist
  -- todo gaps: 1,2,3,10,11
  for _, i in ipairs(indexes) do
    local v = t[i]
    s = s .. nl .. tab .. v2s(v, tab) .. ','
  end

  -- not an number or string keys
  for _, key in ipairs(another) do
    local v = t[key]
    key = "[" .. v2s(key) .. "]"
    s = s .. nl .. tab .. key .. ' = ' .. v2s(v, tab) .. ','
  end

  if #s > 3 then
    s = s:sub(1, -2) --  remove ','
  end

  if nl ~= '' then                       --if #s + #tab > 60 then
    s = s .. nl .. tab:sub(1, -3) .. '}' -- add last tab only for multilines
  else
    s = s .. nl .. '}'
  end


  return s
end

--
---@param t table
---@param start number? defualt is 1 can be negative (from end)
---@param pend number? defualt is #t  can be negative (from end)
---@return table?
function M.flat_copy(t, start, pend, out)
  if t then
    if start and start < 0 then start = #t + start + 1 end
    if pend and pend < 0 then pend = #t + pend + 1 end
    start = start or 1
    pend = pend or #t

    out = out or {}
    for i = start, pend do
      out[#out + 1] = t[i]
    end

    return out
  end
  return nil
end

---@return table
function M.flat_copy_or_empty(t, start, pend, out)
  return M.flat_copy(t, start, pend, out) or out or {}
end

---@return number
function M.tbl_keys_count(t)
  if type(t) == 'table' then
    local c = 0
    for _, _ in pairs(t) do c = c + 1 end
    return c
  end
  return -1
end

local function sure_not_table(v, k)
  if type(v) == 'table' then
    error('only for flat tables! has table key: ' .. tostring(k))
  end
  return v
end

--
-- merge values from two tables into one new table
--
-- copy all values from oldt and replace it by redefined values from newt
-- if newt is empty or nil just copy oldt to new table and return it
-- merging two nils will give an empty table
--
-- Does not support nested tables!
--
---@param oldt table?
---@param newt table?
function M.tbl_merge_flat(oldt, newt)
  local t = {}
  if type(oldt) == 'table' then
    for k, v in pairs(oldt) do t[k] = sure_not_table(v, k) end
  end
  if type(newt) == 'table' then
    for k, v in pairs(newt) do t[k] = sure_not_table(v, k) end
  end
  return t
end

-- apppedn given list to dst-list
-- {1,2,3,4 } + {5,6,7} -> {1,2,3,4,5,6,7}
---@param dst table list
---@param list table list
---@return table dst
---@param offset number?
function M.append_all_elms(dst, list, offset)
  offset = offset or 1
  for i = offset, #list do
    local elm = list[i]
    dst[#dst + 1] = elm
  end
  return dst
end

---@param original_lines table
---@param pos number
---@param insert table - list to be insterted
function M.insert_all_to_new(original_lines, pos, insert)
  local lines = M.flat_copy_or_empty(original_lines, 1, pos - 1)
  M.append_all_elms(lines, insert, 1)
  M.append_all_elms(lines, original_lines, pos)
  return lines
end

--------------------------------------------------------------------------------

--
-- remove all onle-line comments from given string with lua-code
--
-- "-- comment \n {lua-table-code} -- comment \n" -> "{lua-table-code}"
--
---@param str string
---@return string
function M.remove_comments(str)
  local s = ''

  local function append(p, i)
    local line = string.sub(str, p, i)
    if line and line ~= '' and string.match(line, "^%s*%-%-") == nil then
      if #s > 0 then s = s .. "\n" end
      -- case:  key = 'value', -- comment with description
      local code, _ = string.match(line, '^(.-,)%s*%-%-(.*)$')
      s = s .. (code ~= nil and code or line)
    end
  end

  local p = 1
  while true do
    local i = string.find(str, "\n", p, true)
    if not i then
      break
    end
    append(p, i)
    p = i + 1
  end
  append(p, #str)

  return s
end

-- experimental
--
-- warn: can be not safe! works via loadstring
-- not recommended for use in hostile environments
-- {} and bad_func();
-- {} --[[]]and bad_func();
--
---@param line string
---@return boolean
---@return table|string?
function M.luacode2table(line)
  if type(line) ~= 'string' then
    return false, 'expected string got:' .. type(line)
  end
  local v2s = tostring
  line = M.remove_comments(line)

  local fc = string.match(line, "^%s*(%{).*") -- return {...
  if not fc or fc == nil then
    return false, 'expected line starts with "{" got: "' ..
        line:sub(1, 10):gsub("\n", '.') .. '"'
  end
  if string.match(line, "}%s*and ") or                    -- {} and bad();
      string.match(line, "}%s*%-%-[%[%=%]]+%s*and ") then -- {} --[[]] and bad()
    return false, 'cannot run any functions'
  end

  local okf, func = pcall(loadstring, 'return ' .. line, 'chunkname')
  if not okf or type(func) ~= 'function' then
    return false, 'Cannot parse line as lua-table: ' .. v2s(func)
  end
  local ok_call, t = pcall(func)
  if not ok_call or type(t) ~= 'table' then
    return false, 'Error on build lua-table: ' .. tostring(t)
  end

  return true, t
end

---@param s string
---@return string
local function trim(s)
  return string.match(s or '', '^%s*(.-)%s*$')
end

---@param line string
---@return table{{cat:string, ps:number, pe:number}, {}}
function M.parse_title_caption(line)
  local t = {}
  local pc, ppc = nil, nil
  local word_start, word_end = 1, nil

  local function add_word(i)
    t[#t + 1] = {
      name = trim(string.sub(line, word_start, word_end)),
      ps = word_start,
      pe = i - 1 -- word_end
    }
  end

  for i = 1, #line, 1 do
    local c = line:sub(i, i)
    if string.match(c, '^%s$') == nil then
      if pc == ' ' and ppc == ' ' then -- new title
        add_word(i)
        word_start = i                 -- for next word
      end
      word_end = i
    end
    ppc = pc
    pc = c
  end

  add_word(#line + 1)

  return t
end

--
-- parse lines with formated table into object(rows with columns)
--
---@param lines table
---@param opts table?
---@return table
function M.lines_to_table_obj(lines, opts)
  assert(type(lines) == 'table', 'lines')
  assert(type(lines[1]) == 'string' and lines[1] ~= '', 'first line - caption')
  opts = opts or {}

  local caption = M.parse_title_caption(lines[1])
  local columns_names = {}
  for _, e in ipairs(caption) do
    columns_names[#columns_names + 1] = e.name
  end
  local t = { columns_names }

  for i = 2, #lines do
    local line = lines[i]
    local row = {}
    for _, ce in ipairs(caption) do
      row[#row + 1] = string.match(line:sub(ce.ps, ce.pe), '^%s*(.-)%s*$')
    end
    t[#t + 1] = row
  end

  return t
end

local function str_trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function str_split(s, sep, do_foreach)
  local t = {}
  for str in string.gmatch(s, '([^' .. sep .. ']+)') do
    if do_foreach then
      str = do_foreach(str)
    end
    t[#t + 1] = str
  end
  return t
end

--
-- parse lines with raw text table tab-separated
--
---@param lines table
---@param sep string? default is tab '\t'
---@return table
function M.parse_raw_lines_to_table_obj(lines, sep)
  assert(type(lines) == 'table', 'lines')
  assert(type(lines[1]) == 'string' and lines[1] ~= '', 'first line - caption')
  sep = sep or "\t"
  local columns_names = str_split(lines[1], sep, str_trim) -- caption

  local t = { columns_names }

  for i = 2, #lines do
    local line = lines[i]
    t[#t + 1] = str_split(line, sep, str_trim)
  end

  return t
end

return M

-- 14-01-2024 @author Swarg
local M = {}
--

-- RFC 3629
--
-- UTF8-octets = *( UTF8-char )
-- UTF8-char      UTF8-1 | UTF8-2 | UTF8-3 | UTF8-4
--
-- UTF8-1    %x00-7F
-- UTF8-2    %xC2-DF  UTF8-tail
--
-- UTF8-3    %xE0     %xA0-BF     UTF8-tail    or
--           %xE1-EC           2x(UTF8-tail)   or
--           %xED     %x80-9F     UTF8-tail    or
--           %xEE-EF           2x(UTF8-tail)
--
-- UTF8-4    %xF0     %x90-BF  2x(UTF8-tail)   or
--           %xF1-F3           3x(UTF8-tail)   or
--           %xF4     %x80-8F  2x(UTF8-tail)
--
-- UTF8-tail is %x80-BF
--
--


-- 0xxxxxxx                            | 007F   (127)
-- 110xxxxx	10xxxxxx                   | 07FF   (2047)
-- 1110xxxx	10xxxxxx 10xxxxxx          | FFFF   (65535)
-- 11110xxx	10xxxxxx 10xxxxxx 10xxxxxx | 10FFFF (1114111)
--local pattern = '[%z\1-\127\194-\244][\128-\191]*'

--
-- traverse a given string containing UTF8 characters,
-- passing one character at a time to the callback
--
---@param line string
---@param callback function
---@param userdata any? (aka acc)
function M.foreach_letter(line, callback, userdata)
  local i = 0
  local byte, sub = string.byte, string.sub
  while i <= #line do
    i = i + 1
    local b1, b2 = byte(line, i, i + 1)
    if not b1 then
      break
    end

    if b1 <= 0x7F then -- One ASCII char
      callback(sub(line, i, i), userdata)
      --
    elseif b2 then
      if b1 >= 0xC2 and b1 <= 0xDF then -- UTF8-2
        callback(sub(line, i, i + 1), userdata)
        i = i + 1

        -- UTF8-3
      elseif (b1 == 0xE0 and (b2 >= 0xA0 and b2 <= 0xBF)) or
          (b1 >= 0xE1 and b1 <= 0xEC) or
          (b1 == 0xED and (b2 >= 0x80 and b2 <= 0x9F)) or
          (b1 >= 0xEE and b1 <= 0xEF) then
        callback(sub(line, i, i + 2), userdata)
        i = i + 2

        -- UTF8-4
      elseif (b1 == 0xF0 and (b2 >= 0x90 and b2 <= 0xBF)) or
          (b1 >= 0xF1 and b1 <= 0xF3) or
          (b1 == 0xF4 and (b2 >= 0x80 and b2 <= 0x8F)) then
        callback(sub(line, i, i + 4), userdata)

        i = i + 3
      end
    end
  end

  return userdata
end

--
-- return utf8 symbol from a given position
--
---@param line string
---@param pos number
---@return string?
function M.letter_at(line, pos)
  local i, n = 0, 0
  local byte, sub = string.byte, string.sub

  while i <= #line do
    i = i + 1
    n = n + 1 -- letter number in given line
    local b1, b2 = byte(line, i, i + 1)
    if not b1 then break end
    local extra_bytes = 0

    if b1 <= 0x7F then -- One ASCII char
      extra_bytes = 0
      --
    elseif b2 then
      if b1 >= 0xC2 and b1 <= 0xDF then -- UTF8-2
        extra_bytes = 1

        -- UTF8-3
      elseif (b1 == 0xE0 and (b2 >= 0xA0 and b2 <= 0xBF)) or
          (b1 >= 0xE1 and b1 <= 0xEC) or
          (b1 == 0xED and (b2 >= 0x80 and b2 <= 0x9F)) or
          (b1 >= 0xEE and b1 <= 0xEF) then
        extra_bytes = 2

        -- UTF8-4
      elseif (b1 == 0xF0 and (b2 >= 0x90 and b2 <= 0xBF)) or
          (b1 >= 0xF1 and b1 <= 0xF3) or
          (b1 == 0xF4 and (b2 >= 0x80 and b2 <= 0x8F)) then
        extra_bytes = 3
      end
    end

    if n == pos then
      return sub(line, i, i + extra_bytes)
    end

    i = i + extra_bytes
  end

  return nil
end

--
-- RFC 3629
-- length in bytes of the utf8 character by its first two bytes
--
-- len means:
--   1 - regular ASCII char
--   2 - UTF8-2
--   3 - UTF8-3
--   4 - UTF8-4
--
---@param b1 number
---@param b2 number
function M.utf8_char_len(b1, b2)
  local len = 1

  if b1 <= 0x7F then -- One ASCII char
    len = 1
    --
  elseif b2 then
    -- UTF8-2
    if b1 >= 0xC2 and b1 <= 0xDF then
      len = 2

      -- UTF8-3
    elseif (b1 == 0xE0 and (b2 >= 0xA0 and b2 <= 0xBF)) or
        (b1 >= 0xE1 and b1 <= 0xEC) or
        (b1 == 0xED and (b2 >= 0x80 and b2 <= 0x9F)) or
        (b1 >= 0xEE and b1 <= 0xEF) then
      len = 3

      -- UTF8-4
    elseif (b1 == 0xF0 and (b2 >= 0x90 and b2 <= 0xBF)) or
        (b1 >= 0xF1 and b1 <= 0xF3) or
        (b1 == 0xF4 and (b2 >= 0x80 and b2 <= 0x8F)) then
      len = 4
    else
      error('Unknown seq: ' .. tostring(b1) .. ' ' .. tostring(b2))
    end
  end

  return len
end

function M.get_utf8char_len_at_byte(line, col)
  if line and col then
    local b1, b2 = string.byte(line, col, col + 1)
    return M.utf8_char_len(b1, b2)
  end
end

--
-- convert byte_index_in_string to utf8char_index
--
---@param line string
---@param col number
---@return number starts from 0 (like return api.nvim_win_get_cursor)
---@return number length of utf8-char
function M.letter_pos(line, col)
  local i, n, len, byte = 1, 0, 0, string.byte
  -- in nvim col starts from 0, and chars in luastrings from 1
  if col > #line then col = #line end

  while i <= col do
    local b1, b2 = byte(line, i, i + 1)
    if not b1 then
      break
    end
    len = M.utf8_char_len(b1, b2)
    n, i = (n + 1), (i + len)
  end

  return n, len
end

--
-- reverse operation of letter_pos
-- get absolute byte position of utf-8 letter in given string with for
--
---@param line string
---@param letternr number
function M.byte_pos_of_letter(line, letternr)
  local i, n, byte = 1, 0, string.byte

  while i <= #line do
    local b1, b2 = byte(line, i, i + 1)
    if not b1 then break end
    local clen = M.utf8_char_len(b1, b2)
    n = n + 1

    if n >= letternr then
      return i, clen
    end

    i = i + clen
  end

  -- the case when the given string contains fewer characters than requested
  return #line, 0
end

local space_code = string.byte(' ', 1)

---@param c1 string
---@param c2 string
---@param lowercase boolean?
function M.is_utf8_ru(c1, c2, lowercase)
  if not c1 or not c2 then
    return false
  end

  local b1 = string.byte(c1)
  local b2 = string.byte(c2)

  -- print('b1:', b1, 'b2:', b2)

  if lowercase then
    return (b1 == 208 and (b2 >= 176 and b2 <= 191)) or
        (b1 == 209 and (b2 >= 128 and b2 <= 143 or b2 == 145))
  end

  return (b1 == 208 and (b2 >= 144 and b2 <= 191 or b2 == 001)) or
      (b1 == 209 and (b2 >= 128 and b2 <= 143 or b2 == 145))
end

--[[

Символ  UNICODE HEX-16  DEC 8x2
А       0410    D090    208 144
Б       0411    D091    208 145
Ю       042E    D0AE    208 174
Я       042F    D0AF    208 175

а       0430    D0B0    208 176
б       0431    D0B1    208 177
н       043D    D0BD    208 189
о       043E    D0BE    208 190
п       043F    D0BF    208 191
р       0440    D180    209 128
с       0441    D181    209 129
т       0442    D182    209 130
у       0443    D183    209 131
э       044D    D18D    209 141
ю       044E    D18E    209 142
я       044F    D18F    209 143

Ё       0401    D001    208 001
ё       0451    D191    209 145

]]

function M.is_ascii_letter(b)
  return b and ((b >= 65 and b <= 90) or (b >= 97 and b <= 122))
end

---@param b1 number(byte)
---@param b2 number(byte)
function M.utf8_is_ru_bytes(b1, b2)
  return b1 and b2 and (
    (b1 == 208 and (b2 >= 144 and b2 <= 191 or b2 == 001)) or
    (b1 == 209 and (b2 >= 128 and b2 <= 143 or b2 == 145))
  )
end

-- length of give string with utf8-ru
---@param s string
---@return number
---@param offset number?
function M.utf8_slen(s, offset)
  offset = offset or 1
  local i, sz = offset, 0
  while i <= #s do
    local b1, b2 = string.byte(s, i, i + 1)
    local len = M.utf8_char_len(b1, b2)
    i = i + len
    sz = sz + 1
  end

  return sz
end

--
---@param line string
---@param width number
---@return table
function M.utf8_fold(line, width, t)
  t = t or {}
  -- width = width or M.WIDTH
  local sz, i, prev_sp, last_p = 0, 1, 1, 1

  while i <= #line do
    if sz >= width then
      t[#t + 1] = line:sub(last_p, prev_sp)
      last_p = prev_sp + 1
      sz = 0
    end

    local b1, b2 = string.byte(line, i, i + 1)
    -- utf-8 ru-chars
    if M.utf8_is_ru_bytes(b1, b2) then
      i = i + 2
    else
      if b1 == space_code then
        prev_sp = i
      end
      i = i + 1
    end

    sz = sz + 1
  end

  t[#t + 1] = line:sub(last_p, #line)
  return t
end

function M.show_all_bytes(s)
  local t = { string.byte(s, 1, #s) }
  local x = ''
  for _, v in ipairs(t) do
    x = x .. ' 0x' .. string.format("%X", v)
  end
  return x
end

-- mapping to replace utf8 chars by ascii chars
local punctuations_0xE2_0x80_xx = {
  [0x9C] = '"',   -- “
  [0x9D] = '"',   -- ”
  [0x98] = "'",   -- ‘
  [0x99] = "'",   -- ’
  [0x93] = '-',   -- –
  [0x94] = '-',   -- —
  [0xA6] = '...', -- …
  [0xA2] = '*',   -- •
}

--
-- replace all utf-8 punctuation characters with the corresponding ascii
-- gsub doesnt work correctly with utf8 chars!
-- so there is low-level work with the string by checking the bytes
--
---@param s string
function M.mk_simple_punctuation(s)
  -- return s:gsub('[«»]', '"')

  local i, t = 0, nil
  while i < #s do
    i = i + 1
    local b1, b2, b3 = s.byte(s, i, i + 2)

    -- hardcoded utf8 byte sequences
    -- 2 bytes
    if b1 == 0xC2 then
      if b2 == 0xAB or b2 == 0xBB then -- «»
        t = t or {}
        t[#t + 1] = { i, 2, '"' }
        i = i + 1
      end
    end
    -- 3 bytes
    if b1 == 0xE2 and b2 == 0x80 then
      local repl = punctuations_0xE2_0x80_xx[b3]
      if repl then
        t = t or {}
        t[#t + 1] = { i, 3, repl }
        i = i + 2
      end
    end
  end


  -- apply patches to replace utf8 chars by ascii
  if t then -- has patches to replace utf8-chars
    local line, off = '', 1
    for _, patch in ipairs(t) do
      local pos, len, repl = patch[1], patch[2], patch[3]
      line = line .. s:sub(off, pos - 1) .. repl
      off = pos + len
    end
    s = line .. s:sub(off, #s)
  end

  return s
end

-- sep can be only ascii char
---@param line string
---@param ps number? pos_start (range in line)
---@param pe number? pos_end
---@param sep_pattern string? default is spaces
---@param withrange boolean?
function M.split_words(line, withrange, ps, pe, sep_pattern)
  local i, len, previ, prevcn, words = 1, 0, 1, 1, {}
  sep_pattern = sep_pattern or '[ \n\t,;.]'
  ps = ps or 1
  pe = pe or 0

  local function add_word(s, e)
    if s <= e then
      local word = string.sub(line, s, e)
      -- print('b1:', b1, 'word:','|'.. word..'|', previ, i)
      words[#words + 1] = word
      if withrange then
        words.brange = words.brange or {}
        words.crange = words.crange or {}
        -- bytenumber in the source string(line) and in utf8 char number
        words.brange[#words] = { s, e }        -- in bytes
        words.crange[#words] = { prevcn, len } -- in utf8-chars
      end
    end
  end

  while i <= #line do
    local b1, b2 = string.byte(line, i, i + 1)
    if M.utf8_is_ru_bytes(b1, b2) then
      i = i + 2
    else
      if b1 == 32 or string.char(b1):match(sep_pattern) then
        if len >= ps then
          add_word(previ, i - 1)
          previ = i + 1
          prevcn = len + 2
          if pe > 0 and pe >= len then break end
        end
      end
      i = i + 1
    end
    len = len + 1
  end

  add_word(previ, #line)

  return words, len
end

---@param words table {range={bn={s,e}, cn={s,e}}}
---@param pos number
---@param as_char_pos boolean? (false means in bytes not in utf8 chars)
---@return string?
---@return number
function M.get_word_at(words, pos, as_char_pos)
  assert(type(pos) == 'number', 'pos')
  assert(type(words) == 'table', 'words')
  assert(type(words.crange) == 'table', 'words')
  assert(type(words.brange) == 'table', 'words')

  local ranges = words.crange
  if as_char_pos == false then
    ranges = words.brange
  end

  for i, range in pairs(ranges) do
    local s, e = range[1], range[2]
    -- print(s, e, pos)
    if pos >= s and pos <= e then
      return words[i], i
    end
  end

  return nil, -1
end

--------------------------------------------------------------------------------
-- keyboard languages layouts

---@format disable-next
M.layout_lattin2ru = {
  A = "Ф", B = "И", C = "С", D = "В", E = "У", F = "А", G = "П", H = "Р",
  I = "Ш", J = "О", K = "Л", L = "Д", M = "Ь", N = "Т", O = "Щ", P = "З",
  Q = "Й", R = "К", S = "Ы", T = "Е", U = "Г", V = "М", W = "Ц", X = "Ч",
  Y = "Н", Z = "Я", ['{'] = 'Х', ['}'] = 'Ъ', [':'] = 'Ж',
  ['<'] = 'Б', ['>'] = 'Ю', ['~'] = 'Ё', ['?'] = ',',
  --
  a = "ф", b = "и", c = "с", d = "в", e = "у", f = "а", g = "п", h = "р",
  i = "ш", j = "о", k = "л", l = "д", m = "ь", n = "т", o = "щ", p = "з",
  q = "й", r = "к", s = "ы", t = "е", u = "г", v = "м", w = "ц", x = "ч",
  y = "н", z = "я", ['['] = 'х', [']'] = 'ъ', [';'] = 'ж',
  [','] = 'б', ['.'] = 'ю', ['`'] = 'ё', ['/'] = '.',
}

return M

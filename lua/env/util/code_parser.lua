--
local M = {}
local su = require('env.sutil')
local log = require('alogger')
local fs = require('env.files')


local D = require("dprint")
local dprint = D.mk_dprint_for(M)

local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
local LEXEME_NAMES = lapi_consts.LEXEME_NAMES


-- Project properties of current processed source file
--  own for earch buffer
local default_project = {
  project_root = '', --
  src = 'src',       -- dir with sources inside project_root
  test = 'test',     -- dir with tests for sources
  build = 'build',   --
  ext = 'lua',       -- extension of source file language specific
}

-- make config (append defaults fileds if they not defined)
---@param project table
---@return  table
function M.new_project(project)
  project = project or {}
  -- TODO how to copy a table entries without vim ?
  project.project_root = project.project_root or ""
  project.src = project.src or default_project.src
  project.test = project.test or default_project.test
  project.build = project.build or default_project.build
  project.ext = project.ext or default_project.ext
  return project
end

-- extract full module(class) name based on project root
---@param project table {project_root, src}
---@param path string full path to currently opened source
---@return string|nil
function M.get_module(project, path)
  if path and project and project.project_root and project.src then
    -- local src_path = project.project_root .. src --'/' .. project.src .. '/'
    local _, j = string.find(path, project.project_root, 1, true)
    if j and j > 1 then
      local k = string.find(path, '/', j + 2, true) -- +2 -- to skip first '/'
      if k then
        local dir0 = string.sub(path, j + 2, k - 1) -- src or test
        if dir0 == project.src or dir0 == 'test' then
          j = k
        end
      end
      local module = path:sub(j + 1)
      if module then
        module = module:gsub('%/', '.')           -- the.package.to.file
        if project.ext and project.ext ~= '' then -- remove file extension
          module = module:gsub('%.' .. project.ext .. '$', '')
        end
      end
      return module
    end
  end
  return nil
end

-- extract from given line(from source code) function name
-- function M.func() --> func
-- function  func2() --> func2
---@param line string
---@return string|nil
function M.get_function_name(line)
  if line then
    -- lua has no optional items in pattern therefore first extract full-name
    local fullname = string.match(line, "function%s*([%w_%.]+)") -- 'M.name'
    if fullname then
      --print('fullname: '..(fullname or 'nil'))
      -- short name without module
      if fullname:find('%.') then
        return string.match(fullname, "%.+([%w_]+)")
      else
        return fullname
      end
    end
  end
  return nil
end

--
-- everything inside the first block of parentheses()
-- to the right of the current word
--
-- self.obj:method(a, b)  -> "a, b"
--            ^ cursor
--
---@param line string
---@return string?
function M.get_function_params(line, col)
  if line and col then
    local s = line:find('(', col, true)
    if s then
      local e = line:find(')', s, true)
      if e then
        return line:sub(s + 1, e - 1)
      end
    end
    -- return string.match(line, "%((.+)%)") -- 'M.name'
  end
end

-- Extract word from line for given cursor-[p]osition
---@param line string
---@param p number cursor pos in line
---@param allowed_chars_pattern string?|nil [%a_%d]+   any letters,_,digits
---@return string|nil, number|nil, number|nil  -- word, start_pos, end_pos
-- [NOTE] valid patter for whole word is is '^(%a+[%a_%d]*)$' for varnames:
-- Strats with any letter + any letters, digits, and underscore
-- here  chars_pattern - used to check each character in line
-- some left_side some_word_to_be_returned right_side
--                ^<-- ^p(CursorPos)  -->^
---@param offset number? word offset: 0 - none, -1 prev word, 1 - next word
function M.get_word_at(line, p, offset, allowed_chars_pattern)
  dprint('get_word_at line:', line, p)
  offset = offset or 0
  allowed_chars_pattern = allowed_chars_pattern or '[%a_%d]+' -- "[%w_]+"
  if line and p > 0 and p <= #line + 1 then
    local s, e = p, p - 1
    for i = p, 1, -1 do -- find word sep from left
      if not line:sub(i, i):match(allowed_chars_pattern) then
        break
      end
      s = i
    end
    for i = p, #line do -- find word sep from right
      if not line:sub(i, i):match(allowed_chars_pattern) then
        break
      end
      e = i
    end
    if s <= e then
      if offset > 0 and e + 2 < #line then -- next word
        local s0 = M.indexofmatch(line, e + 2, allowed_chars_pattern)
        return M.get_word_at(line, s0, (offset - 1), allowed_chars_pattern)
      elseif offset < 0 and s > 2 then -- prev word
        local s0 = M.indexofmatch_back(line, s - 2, allowed_chars_pattern)
        return M.get_word_at(line, s0, (offset + 1), allowed_chars_pattern)
      end
      return line:sub(s, e), s, e
    end
  end
  return nil
end

--
-- with support to return two words if cursor under the space between two words
-- 'word_left word_right'  -->  "word_left word_right"
--           ^p
function M.get_word_at_ex(line, p, offset, chars_pattern)
  local wl, wr, ls, le, rs, re, word, s, e
  wl, ls, le = M.get_word_at(line, p - 1, offset, chars_pattern)
  wr, rs, re = M.get_word_at(line, p + 1, offset, chars_pattern)

  if wl and wr then
    word, s, e = wl .. ' ' .. wr, ls, re
  elseif wl then
    word, s, e = wl, ls, le
  elseif wr then
    word, s, e = wr, rs, re
  end
  local c = e and line:sub(e, e)
  if c == ',' or c == '.' or c == ';' or c == ':' or c == ')' then
    e = e - 1
    word = word:sub(1, -2)
  end
  return word, s, e
end

--
function M.indexofmatch(line, p, chars_pattern)
  for i = p, #line do
    if line:sub(i, i):match(chars_pattern) then
      return i
    end
  end
  return -1
end

function M.indexofmatch_back(line, p, chars_pattern)
  for i = p, 1, -1 do
    if line:sub(i, i):match(chars_pattern) then
      return i
    end
  end
  return -1
end

-- var, const, class ...

function M.get_lexeme_type(line, word, s, e)
  dprint("get_lexeme_type line: ", line, 'word:', word, s, e)

  if line and type(s) == 'number' and type(e) == 'number' then
    if word == " " then return LEXEME_TYPE.SPACE end

    if e > 0 and e < #line then
      local nch = line:sub(e + 1, e + 1) -- next char after word
      if nch == ' ' then
        nch = line:sub(e + 2, e + 2)     -- case: "func (" not "func("
      end
      local pch = line:sub(s - 1, s - 1) -- prev char before word
      if (nch == '"' and pch == '"') or (nch == "'" and pch == "'") then
        return LEXEME_TYPE.STRING
      end

      -- case: "  method();"  not ".. x = new ClassName();"
      -- case: "  method'arg1' - call function without ()
      if nch == '(' or nch == '"' or nch == "'" then
        dprint('next-char is (')
        -- ?? method if statrs only from lower letter?
        if s > 4 and line:sub(s - 4, s - 1) == 'new ' then
          return LEXEME_TYPE.CLASS
        end
        if pch == ':' then -- lua self.obj:method(param)
          return LEXEME_TYPE.METHOD
        end
        return LEXEME_TYPE.FUNCTION
        --
      elseif nch == ':' then -- lua MyClass:new()
        dprint('next-char is :')
        local wc1 = word:sub(1, 1)
        if string.upper(wc1) == wc1 then
          return LEXEME_TYPE.CLASS
        end
      elseif nch == '[' then
        return LEXEME_TYPE.ARRAY
      elseif pch ~= ':' and nch ~= '(' and nch ~= '[' and nch ~= ':' and
          M.is_simple_varname(word) then
        return LEXEME_TYPE.VARNAME
      end
    end
  end

  if tonumber(word) then return LEXEME_TYPE.NUMBER end
  return LEXEME_TYPE.UNKNOWN
end

---@param s string?
---@return boolean
function M.is_simple_varname(s)
  return s ~= nil and s ~= 'true' and s ~= 'false' and s:match("^[%a_][%w_]*$") ~= nil
end

function M.lexeme_name(n)
  n = n or LEXEME_TYPE.UNKNOWN
  return LEXEME_NAMES[n] or LEXEME_NAMES[LEXEME_TYPE.UNKNOWN]
end

-- Extract from line the container name start from position(p)
-- (Constanst.URL)
--      ^     ^^-word under cursot given from get_word_at
--      |     `-p the first Char of word under cursor (second ret from get_word_at)
--      `container name
--
-- (io.pkg.app.ClassName)
--  `--------' ^  ^ cursor
--       ^     `-p
--       `- container_name
--
---@param line string current line from source code under cursor
---@param p number? position in line
---@param chars_pattern string?   for match words
---@return string? container_name
function M.get_container_name(line, p, chars_pattern)
  chars_pattern = chars_pattern or '[%a_%d]+'
  local name = nil

  while line and p and p > 2 and p <= #line + 1 do
    local ch = line:sub(p - 1, p - 1)

    if ch == '.' or ch == ':' then
      local w
      if line:sub(p - 2, p - 2) == ')' then
        p = su.back_indexof(line, '(', 0, p - 2)
      end
      w, p = M.get_word_at(line, p - 2, 0, chars_pattern)
      if not name then
        name = w
      else
        name = w .. ch .. name
      end
    else
      break
    end
  end
  return name
end

-- get words under cursor and add it into ctx:
-- word_container[.:]word_element
-- if word_element is function or method has after self '(' add method_name
--
-- word_container is the word that comes before the word under cursors
-- followed by a dot or colon (can be nil)
--
-- word_element - the word uder cursor in current line
--
---@param ctx table
---@param line string? if nil then auto fetch current line from buffer
function M.resolve_words(ctx, line)
  if ctx then
    local api = vim.api
    line = line or api.nvim_get_current_line()
    local cursor_pos = api.nvim_win_get_cursor(0)
    if cursor_pos then
      local cursor_col = cursor_pos[2]
      local s, e
      ctx.current_line = line
      ctx.word_element, s, e = M.get_word_at(line, cursor_col + 1)
      ctx.word_container = M.get_container_name(line, s)
      -- is method|function?
      ctx.word_lexeme_type = M.get_lexeme_type(line, ctx.word_element, s, e)
      if ctx.word_lexeme_type == LEXEME_TYPE.FUNCTION then
        ctx.method_name = ctx.word_element
      end
      --
      if log.is_debug() then
        log.debug('resolve words: %s %s (%s:%s) line |%s|',
          ctx.word_container,
          ctx.word_element,
          ctx.word_lexeme_type,
          M.lexeme_name(ctx.word_lexeme_type),
          line
        )
      end
      return true
    end
  end
end

-- local s = "some\ntext here" --> local s = "some\n" .. (\n) .. "text here"
---@return  table?
function M.split_code_string_to_multilines(line)
  if line then
    -- check is here has some correct block with ""
    local p_start = string.find(line, '"', 1, true)
    if not p_start then
      return
    end
    local p_end = su.get_group_end(line, p_start)
    if not p_end then
      return
    end

    -- work
    p_start = 1 -- from begining of line
    local sep, pos, line_start, lines, pref = "\\n", p_start, 0, {}, ''
    while true do
      local p = string.find(line, sep, pos, true)
      if not p or p > p_end then
        break
      end
      line_start = pos
      pos = p + #sep --1
      p = p - 1
      if line:sub(p, p) == '\r' then
        p = p - 1
      end
      lines[#lines + 1] = pref .. string.sub(line, line_start, p) .. '\\n" ..'
      if pref == '' then pref = '"' end
    end
    lines[#lines + 1] = pref .. string.sub(line, pos, p_end)

    -- extra code or comments after forst "" block
    if p_end + 1 < #line then
      lines[#lines + 1] = string.sub(line, p_end + 1, #line)
    end
    return lines
  end
end

--
-- Deprecated
-- TODO rewrite to Editor Context and Lang
--
-- find or get already found project properties for given buffer infor
-- _env_project
function M.project_of_buf(cbi)
  local ok, project = pcall(vim.api.nvim_buf_get_var, cbi.bufnr, "_env_project")
  if not ok then
    local project_root = fs.find_root({ '.git' }, cbi.bufname)
    -- TODO detect lang and init project, default - for lua-lang
    project = M.new_project({
      project_root = project_root,
      src = 'lua'
    })
    vim.api.nvim_buf_set_var(cbi.bufnr, "_env_project", project)
  end
  return project
end

return M

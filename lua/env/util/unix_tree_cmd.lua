-- 02-02-2024 @author Swarg
--
-- Goal: parse output of the `tree` unix command
--
local M = {}

-- local D = require('dprint')


-- https://www.fileformat.info/info/unicode/char/2500/index.htm

-- BOX DRAWINGS LIGHT:                        UTF-8       UTF-16   255 byte
local BDL_H        = '─' -- HORIZONTAL           e2 94 80    0x2500   226 148 128
local BDL_V        = '│' -- VERTICAL             e2 94 82    0x2502   226 148 130
local BDL_V_N_R    = '├' -- VERTICAL AND RIGHT   e2 94 9c    0x251C   226 148 156
local BDL_U_N_R    = '└' -- UP AND RIGHT         e2 94 94    0x2514   226 148 148
local NBSP         = ' ' -- NO-BREAK SPACE       c2 a0       0x00A0   194 160
-- local SPACE     = ' ' -- ASCII 32
local SPACE_BYTE   = string.byte(' ', 1, 1)
local COMMENT_BYTE = string.byte('#', 1, 1)


-- 194 160 is the UTF-8 encoding of a NO-BREAK SPACE codepoint
-- (the same codepoint that HTML calls &nbsp;).
-- So it's really not a space, even though it looks like one.
-- (You'll see it won't word-wrap, for instance.)
-- A regular expression match for \s would match it,  --?
-- but a plain comparison with a space won't.

-- To simply replace NO-BREAK spaces you can do the following:
-- src = src.Replace('\u00A0', ' ');

-- Example:
-- UTF-8: 0xE29494  UTF-16: 0x2514 (2514)  C/C++/Java source code "\u2514"

--
-- make the tree output more compact
--
---@param s string|table{string}
---@return table?
function M.parse_output(s)
  -- local c = NBSP print(c, string.byte(c, 1, #c)) if 0 == 0 then return end
  if not s then return nil end

  local lines = {}

  local function simplify(line)
    return line:gsub(NBSP, ' ')
        :gsub(BDL_H, '~')
        :gsub(BDL_V, '|')
        :gsub(BDL_V_N_R, '>')
        :gsub(BDL_U_N_R, '`')
  end

  local function restore_back(line)
    return line:gsub('~', BDL_H)
        :gsub('|', BDL_V)
        :gsub('>', BDL_V_N_R)
        :gsub('`', BDL_U_N_R)
  end

  if type(s) == 'string' then
    for line in string.gmatch(s, '([^\n]+)') do
      lines[#lines + 1] = simplify(line)
    end
    --
  elseif type(s) == 'table' then
    for i = 1, #s do lines[i] = simplify(s[i]) end
  end

  local XX = '' -- debugging

  for i = 1, #lines do
    local line = lines[i]
    local p = string.find(line, '~~ ', 1, true)
    if p then
      local joint = string.sub(line, p - 1, p - 1) -- continue or last
      if joint == '>' or joint == '`' then
        -- replace `|---`  -->  `|-`
        line = line:sub(1, p - 1) .. XX .. line:sub(p + 2)
        lines[i] = line

        for j = i + 1, #lines do
          line = lines[j]
          local ident = string.sub(line, p, p + 1)
          if ident == '  ' then
            lines[j] = line:sub(1, p - 1) .. XX .. line:sub(p + 2)
          elseif ident ~= '~~' then
            break
          end
        end
      end
    end
  end

  for i = 1, #lines do lines[i] = restore_back(lines[i]) end

  return lines
end

--
-- check is given string contains unicode chars: BDL_*
--
---@param line string?
---@return boolean
function M.has_tree_nodes(line)
  if line then
    local i = 0
    while i < #line do
      i = i + 1
      if string.byte(line, i, i) == 226 then -- unicode
        i = i + 1
        if string.byte(line, i, i) == 148 then
          i = i + 1
          local b = string.byte(line, i, i)
          if b == 128 or b == 130 or b == 156 or b == 148 then
            return true
          end
        end
      end
    end
  end
  return false
end

--  BDL_H     = '─' -- HORIZONTAL           e2 94 80    0x2500   226 148 128
--  BDL_V     = '│' -- VERTICAL             e2 94 82    0x2502   226 148 130
--  BDL_V_N_R = '├' -- VERTICAL AND RIGHT   e2 94 9c    0x251C   226 148 156
--  BDL_U_N_R = '└' -- UP AND RIGHT         e2 94 94    0x2514   226 148 148
--  NBSP      = ' ' -- NO-BREAK SPACE       c2 a0       0x00A0   194 160

-- UTF8-octets = *( UTF8-char )
-- UTF8-char      UTF8-1 | UTF8-2 | UTF8-3 | UTF8-4
--
-- UTF8-1    %x00-7F
-- UTF8-2    %xC2-DF  UTF8-tail
--
-- UTF8-3    %xE0     %xA0-BF     UTF8-tail    or
--           %xE1-EC           2x(UTF8-tail)   or
--           %xED     %x80-9F     UTF8-tail    or
--           %xEE-EF           2x(UTF8-tail)
--
-- UTF8-4    %xF0     %x90-BF  2x(UTF8-tail)   or
--           %xF1-F3           3x(UTF8-tail)   or
--           %xF4     %x80-8F  2x(UTF8-tail)
--
-- UTF8-tail %x80-BF
--

--
-- supports unicode string
-- " |--- node name  # comment" ->  "node name"
--
---@param line string
---@return number? start_pos
---@return number? end_pos
function M.pos_of_node_name(line)
  if not line or line == '' then return nil, nil end

  local i, sp, ep = 0, nil, #line
  local u, j, rem = { 0, 0, 0, 0 }, 1, 0 -- rem is a remaining unicode parts
  local wait_start_name = false          -- ascii

  while i < #line do
    i = i + 1
    local b = string.byte(line, i, i)
    if b < 128 then
      -- ASCII chars -- ignore
      j = 0                            -- ?
      if sp and b == COMMENT_BYTE then --'#'
        ep = i - 1
        -- right trim whitespaces
        while ep > 0 do
          if string.byte(line, ep, ep) == SPACE_BYTE then
            ep = ep - 1
          else
            break
          end
        end
        break
      end
      if wait_start_name and b ~= 32 then -- first not space char
        wait_start_name, sp = false, i
      end
      --
      -- x80 - xBF  UTF8-tail
    elseif b >= 128 and b <= 191 then
      if rem > 0 then
        u[j] = b
        j, rem = j + 1, rem - 1
      end
      --
      -- xC2 - xDF   UTF8-2
    elseif b >= 194 and b <= 223 then
      u[1] = b      -- j = 1
      rem, j = 1, 2 -- j++
      --
      -- UTF8-3    %xE0     %xA0-BF     UTF8-tail    or
      --           %xE1-EC           2x(UTF8-tail)   or
      --           %xED     %x80-9F     UTF8-tail    or
      --           %xEE-EF           2x(UTF8-tail)
    elseif b >= 224 and b <= 239 then
      u[1] = b      -- j = 1
      rem, j = 2, 2 -- j++
      --
      -- UTF8-4    %xF0     %x90-BF  2x(UTF8-tail)   or
      --           %xF1-F3           3x(UTF8-tail)   or
      --           %xF4     %x80-8F  2x(UTF8-tail)
    elseif b >= 224 and b <= 239 then
      u[1] = b      -- j = 1
      rem, j = 3, 2 -- j++
    end

    -- all unicode-bytes in u
    if j > 0 and rem == 0 then
      if (u[1] == 226 and u[2] == 148) and (u[3] == 148 or u[3] == 156) then
        wait_start_name = true
      end
    end
  end

  return sp, ep
end

--
-- build full path in tree started from file in the given lnum
--
---@param lines table
---@param lnum number
---@return string?
function M.build_path(lines, lnum)
  assert(type(lines) == 'table', 'lines')
  assert(type(lnum) == 'number', 'lnum')

  -- used to join parts of path
  local function sep(s)
    return (s ~= '' and s:sub(-1, -1) == '/') and '' or '/'
  end

  local name, path, ip = nil, nil, nil

  -- from leaf to root
  for i = lnum, 2, -1 do
    local line = lines[i] or ''
    local sp, ep = M.pos_of_node_name(line)
    if sp then
      name = string.sub(line, sp, ep)

      if not path then -- init
        ip, path = sp, name
      elseif sp < ip then
        path = name .. sep(path) .. path
        ip = sp
      end
    end
  end

  -- root of the tree
  path = lines[1] .. sep(lines[1]) .. path

  return path
end

return M

-- 10-01-2024 @author Swarg
local M = {}

local su = require('env.sutil')
local fs = require('env.files')
local log = require('alogger')

local D = require("dprint");
---@diagnostic disable-next-line: unused-local
local dprint, dvisualize = D.mk_dprint_for(M, false), D.mk_dvisualize_for(M, false)


local ANNOTATION_MAX_HEIGHT = 16


---@param line string
function M.get_funcname(line)
  if line then
    -- function abc() {} -> "abc"
    local func_name = line:match('^%s*function%s+([^%s%(]+)%s*%(')
    return func_name
  end
end

-- is given string starts with '$'
-- used with get_bash_command_one_line to run "```\n $ date \n ```" in md-files
function M.is_dollar_first(line)
  if line then
    return string.match(line, '^%s*%$.*')
  end
  return false
end

-- simpe one-line only
function M.get_bash_command_one_line(line, res)
  res = res or {}
  if line then
    local args
    if line:match('.*\\$') then
      res.err = 'multiline commands are not supported yet'
      return res
    end
    res.cmd, args = string.match(line, '%s*%$%s*([^%s]+)%s*(.*)%s*$')
    res.args = args and args ~= '' and su.split_groups(args, ' ') or {}
  end
  if not res.cmd or res.cmd == '' then
    res.err = 'Command not found from ' .. line:sub(1, math.max(#line, 12))
  end
  return res
end

--[[
'#',
'# envs: KEY=VALUE',
"# args: a 'b c' 'd e'",
'function callme() {',
'}',
]]
-- pick from comment above current line(ln) annotation (fargs, fenvs, etc)
-- it will be used when calling the function
---@return table
function M.pick_annotations_fr_docblock(bn, ln, out)
  out = out or {}
  local s, e = math.min(ln - ANNOTATION_MAX_HEIGHT, 0), ln
  local lines = vim.api.nvim_buf_get_lines(bn, s, e, false)

  if lines then
    for i = #lines, 1, -1 do
      local line = lines[i]
      if line then
        -- print('[DEBUG]',i, line)
        if line == '' then break end
        local k, v = line:match('^%s*#%s*([^%s:]+):%s*(.*)%s*$')
        if k and v then
          if k == 'args' or k == 'ARGS' then
            out.fargs = su.split_groups(v, ' ', 1, #v, false, su.unwrap_quotes)
          elseif k == 'envs' or k == 'ENVS' then
            out.fenvs = su.split_groups(v, ' ', 1, #v, false, su.unwrap_quotes)
          end
        end
      end
    end
  end
  return out
end

-- for run via spawner.run  "/bin/bash", { "-c", bash_cmd }
---@param t table{file, funcname}
function M.build_cmd_to_call_bash_func(t, quote)
  log.debug("build_cmd_to_call_bash_func")
  if not t.file or not fs.file_exists(t.file) then
    return false, 'Not found file: "' .. tostring(t.file) .. '"'
  end

  quote = quote or '"'

  local envs, params = '', ''

  if t.fenvs then
    for _, env in ipairs(t.fenvs) do
      local key, value = string.match(env, "^([^=]+)%s*[=:]%s*(.*)%s*$")
      log.debug('kv: %s:%s', key, value)
      if value and string.find(value, ' ') then
        value = string.match(value, '^[\'"](.*)[\'"]$') or value
        log.debug('unquoted  v: %s', value)
        value = quote .. value .. quote
      end
      envs = envs .. ' export ' .. key .. '=' .. value .. ' && '
    end
  end

  if t.fargs then
    for _, arg0 in ipairs(t.fargs) do
      if string.find(arg0, ' ') then
        arg0 = quote .. arg0 .. quote
        --   -- print(arg)
      end
      params = params .. ' ' .. arg0
    end
  end

  log.debug("params: ", params)
  return true, "source " .. t.file .. ' && ' .. envs .. t.funcname .. params
end

--
-- Parse condition block with input routing
-- parse line:  [el]if [.*]; then
-- [el]if [ "$1" == "fullname" -o "$1" == "sn" ]; then   --> {'fullname', 'sn'}
--        ^ test
--
-- result is a list of command names
---@return table?
function M.parse_cmd_route_via_test_cond(line)
  if line then
    local t = nil
    local f = -2 -- state of state machine
    --  'if' '[' '"$1"' '==' '"val1"' '-o' '"$1"' '==' '"v2"' '];' 'then'
    --  -2    -1   0     1      2      3           1     2     3
    for s in string.gmatch(line, '([^%s]+)') do
      if f == -2 then
        if s == 'if' or s == 'elif' then
          f = -1
        else
          break
        end
      elseif f == -1 then
        if s == '[' then
          f = 0
        else
          break
        end
      elseif f == 3 then
        if s == "-o" then                 -- "or" between variants
          f = 0
        elseif s == ']' or s == '];' then -- or end of cond
          break
          --
        else         -- support only "or" - variants
          return nil -- "$1" == "cmd" -a "$1" == "short-cmd"
        end
      elseif f == 2 then
        local value = string.match(s, '[\'"](.*)[\'"]')
        if value then
          t = t or {}
          t[#t + 1] = value
        end
        f = 3
      elseif f == 1 then
        if s == '==' then
          f = 2
        else
          return nil -- not support case: '"$1" != "cmd-name"'
        end
      elseif f == 0 and s and string.match(s, '"%$%d"') then
        f = 1
      end
    end

    return t
  end
  return nil
end

---@param bn number buff num
---@param ln number line num
function M.get_full_bash_command(bn, ln)
  log.debug('get_full_bash_command', bn, ln)
  bn = bn or 0
  local res = { cmd = nil, args = nil, not_finished_line = nil }

  local function res_with_err(msg)
    log.debug(msg)
    res.err = msg
    return res
  end

  if type(ln) ~= 'number' then
    return res_with_err('lnum not defined!')
  end

  local lines = vim.api.nvim_buf_get_lines(bn, ln - 2, ln + 4, false)
  if not lines or #lines < 1 then
    return res_with_err('too few lines to recognize command')
  end

  local pline = lines[1] -- prev line under the cursor
  -- is prev line ends with \
  if pline and pline:sub(-1, -1) == '\\' then
    return res_with_err('Its not a begin of the shell command. ' ..
      'Previous line ends with \\')
  end

  -- case: native os command under then cond
  --   | if [ cond ] ; then
  -- > |   cd 123 && ls -l
  if not pline:match('%s+;?then$') then --^%s*e?l?if')
    res.err = 'Can run only commands after "then" of "if cond; then" '
    return res
  end

  if lines[2] and lines[2] == '' then
    return res_with_err('Empty string')
  end

  local cmd, raw_args = '', nil
  local i, off, lines_cnt = 1, 0, #lines
  local not_finished_line = false

  while (i <= lines_cnt) do
    i = i + 1
    local line = lines[i]
    log.debug('lnum i: %s/%s[off:%s]: %s', i, lines_cnt, off, line)
    if not line or line == '' then break end -- else elif

    if line:sub(1, 1) ~= '#' then
      -- to support multiline command
      if line:sub(-1, -1) == '\\' then
        cmd = cmd .. line:sub(1, -2)
        not_finished_line = true
        -- is current index last in the part of lines - fetch more lines if has
        if i == lines_cnt then
          off = off + i
          i = 0
          lines = vim.api.nvim_buf_get_lines(bn, ln + off - 2, ln + off + 1, false)
          lines_cnt = #lines
          log.debug('off:', off, 'lnum:', ln, 'lines_cnt:', lines_cnt, vim.inspect(lines))
        end
      else
        cmd = cmd .. line
        not_finished_line = false
        break
      end
    elseif #cmd == 0 then
      return res_with_err('Its a comment not a shell command.')
    end
  end

  if string.find(cmd, ' ', 1, true) then
    res.cmd, raw_args = string.match(cmd, "^([^%s]*)%s+(.*)%s*$");
    res.args = su.split_groups(raw_args, ' ')
  else
    res.cmd, res.args = cmd, {}
  end

  if not_finished_line then -- command in buffer ends with "\"
    res.not_finished_line = true
  end

  log.debug('result: %s', res)
  return res
end

--
-- get sub-commands names from command-route condition:
--
-- if [ "$1" == "name" -o "$1" == "sort" ]; then .\n.. func "$2"
--
---@param cbi table{bufnr, current_line, bufname}
---@param res table out
function M.get_command_from_cmd_route_cond(cbi, res)
  assert(type(res) == 'table', 'res')

  local test_cond = M.parse_cmd_route_via_test_cond(cbi.current_line)
  if type(test_cond) == 'table' and test_cond[1] then
    res.cmd = cbi.bufname
    res.args = { test_cond[1] }
    local t = M.pick_annotations_fr_docblock(cbi.bufnr, cbi.cursor_row)
    if t.fargs then
      for _, arg in ipairs(t.fargs) do
        if string.find(arg, ' ') then
          arg = '"' .. arg .. '"'
        end
        res.args[#res.args + 1] = arg
      end
    end
    -- TODO t.fenvs
    return true
  end
  return false
end

-- check is given args has combined bash command, like `echo '{}' | jq`
---@param args table?
function M.is_piped_cmd(args)
  if args then
    for _, a in ipairs(args) do
      if a == '|' or a == '&&' or a == '||' or a == '{' or a == '(' or
          a == '>' or a == '>>' or a == '<' or a == '<<' then
        return true
      end
    end
  end
  return false
end

local function unquote(s)
  if s == '"&&"' or s == '"|"' or s == '"||"' then
    return s
  end
  return su.unwrap_quotes(s)
end

--
-- TODO findout what is an issue with keep quotes for md
--
-- substitute home-dir, unquote
function M.arg_decorator(arg)
  if arg then
    -- '~/dir-a' - ok,  '"~/dir b/"' - bad,  '~/"dir b/"' - ok
    if string.find(arg, '~/') then
      arg = arg:gsub('^~/', (os.getenv('HOME') or '~') .. '/')

      -- '~/"dir with space/"file' --> '~/dir with space/file'
      -- TODO:
      -- '~/"dir with space/"file\"name' --> '~/dir with space/file"name'
      local qi = string.find(arg, '[\'"]', 1, false)
      if qi and arg:sub(qi - 1, qi - 1) ~= '\\' then
        arg = arg:gsub(arg:sub(qi, qi), '')
      end
      -- spaces
      -- if string.find(arg, '[^\\]\\ ', 1, false) then
      --   arg = arg:gsub(arg:sub('\\ '), ' ') -- ?
      -- end

      -- envvars?
    else
      arg = unquote(arg)
    end
  end
  return arg
end

--
-- parse cmd-line to cmd + args to run
-- if cmd-line is has pipes or output redirections then wrap it to `bash -c cmd`
--
---@param cmd_line string
---@return string  command - executable file
---@return table   arguments
function M.parse_line(cmd_line)
  -- dprint('parse_line', cmd_line)
  local cmd, args, raw_args
  if string.find(cmd_line, ' ', 1, true) then
    cmd, raw_args = string.match(cmd_line, "^([^%s]*)%s+(.*)%s*$");
    args = su.split_groups(raw_args, ' ', 1, #raw_args, false, M.arg_decorator)
    if M.is_piped_cmd(args) then
      cmd, args = 'bash', { '-c', cmd_line }
    end
  else
    cmd, args = cmd_line, {}
  end
  args = args or {}
  -- dprint('parse_line', cmd, #args)
  return cmd, args
end

--
-- {'a', 'b c'} -> 'a "b c"'
---@param args table?
---@return string
function M.join_args(args)
  local line = ''
  if args then
    for _, arg in ipairs(args) do
      if string.find(arg, ' ') then
        -- todo escape already existed "
        -- if string.find(arg, '"') then
        --   arg = string.gsub(arg, '"','\\"')
        -- end
        local has_dq, has_sq = string.find(arg, '"'), string.find(arg, '"')
        if (has_sq and not has_dq) or (not has_sq) then
          arg = '"' .. arg .. '"' -- ?
        elseif not has_sq and has_dq then
          arg = "'" .. arg .. "'"
        end
      end
      if #line > 0 then
        line = line .. ' '
      end
      line = line .. arg
    end
  end
  return line
end

--
return M

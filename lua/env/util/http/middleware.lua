-- 27-05-2024 @author Swarg
-- Goal: decode json|xml into luatable

local log = require 'alogger'
local cjson = require 'cjson'

local M = {}
local E, log_debug = {}, log.debug


--
---@param raw_response string
---@return table|string
function M.decode(raw_response, opts, uri)
  log_debug("decode")
  local ok, decoded = false, nil

  if (opts or E).is_json then
    ok, decoded = pcall(M.decode_json, raw_response)
    if not ok or type(decoded) ~= 'table' then
      log_debug('Cannot decode json response for: "%s"', uri)
      return 'error on decode json'
    end
    --
  elseif (opts or E).is_xml then
    ok, decoded = pcall(M.decode_xml, raw_response)
    if not ok or type(decoded) ~= 'table' then
      log_debug('Cannot decode xml response for: "%s"', uri)
      return 'error on decode xml'
    end
  else
    decoded = raw_response -- as is
  end

  return decoded
end

---@param raw_resp string
-- string with json into lua-table
---@return table
function M.decode_json(raw_resp)
  return cjson.decode(raw_resp)
end

---@param raw_resp string
---@return table
---@diagnostic disable-next-line: unused-local
function M.decode_xml(raw_resp)
  error('Not implemented yet')
end

return M

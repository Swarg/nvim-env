-- 27-05-2024 @author Swarg
--
local log = require 'alogger'

local M = {}
local log_debug = log.debug
local has_socket_http = nil
-- posix?
local has_wget = nil
local has_curl = nil

-- check is socket package installed and available on system
function M.is_socket_http_available()
  if has_socket_http == nil then
    local ok, modt = pcall(require, 'socket.http')
    has_socket_http = ok ~= nil and type(modt) == 'table'
  end
  return has_socket_http == true
end

function M.is_wget_available()
  return has_wget ~= true -- todo
end

function M.is_curl_available()
  return has_curl == true
end

---@return env.util.http.Transport
function M.choose_transport()
  if M.is_socket_http_available() then
    log_debug('available socket_http')
    return require "env.util.http.socket_transport"
    --
  elseif M.is_wget_available() then
    log_debug('available wget')
    return require "env.util.http.wget_transport"
    --
  elseif M.is_curl_available() then
    log_debug('available curl')
    return require "env.util.http.curl_transport"
  end

  error('all supported transports no available')
end

return M

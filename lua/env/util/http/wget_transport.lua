-- 27-05-2024 @author Swarg
--
local log = require 'alogger'
local spawner = require "env.spawner"
local uhttp = require("env.util.http")

local M = {}

---@param t env.util.http.Request
---@param opts table?{verbose:boolean?, callback:function?}
function M.send(t, opts)
  log.debug("wget-transport: send req %s %s%s", t.method, t.host, t.uri)

  local req_url = uhttp.request_to_url(t)
  local args = { '-U', "Mozilla/5.0", '-qO', '-', req_url }

  local pp = spawner.new_process_props(-1, 'wget', args, '/tmp/')
  local callback = (opts or {}).callback
  -- handler wget outputs
  pp.handler_on_close = function(output)
    -- local ret = parse_response(req_url, output) -- TranslatedAnswer
    if callback then
      callback(output, opts, t.uri)
    else
      return output
    end
  end

  pp.hide_start = true -- not show cmd in "status line"
  spawner.run(pp)
  return nil
end

---@diagnostic disable-next-line: unused-local
function M.decode(raw_response, opts, uri)
  return raw_response
end

return M

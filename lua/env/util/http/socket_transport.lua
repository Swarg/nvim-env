-- 27-05-2024 @author Swarg
--
-- Goal: fetch data from given HttpRequest
--
-- Dependency:
--  - luasocket
--  - cjson or vim.json

local log = require 'alogger'
local http = require 'socket.http'
local ltn12 = require 'ltn12'
local inspect = require 'inspect'

local DEF_SCHEME = 'http'
local DEF_METHOD = 'GET'

---@class env.util.http.SocketTransport: env.util.http.Transport
local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_debug, is_log_debug = log.debug, log.is_debug()

--
-- send _http_request via socket.html
--
---@param t env.util.http.Request
---@param opts table?{callback:function?}
function M.send(t, opts)
  log_debug("socket-transport: send req %s %s%s", t.method, t.host, t.uri)

  -- vim.schedule(function()
  local t_response = {}

  local reqt = {
    scheme = t.scheme or DEF_SCHEME, -- luasock supports https(TLS) via LuaSec package
    method = t.method or DEF_METHOD,
    uri = t.uri or '/',
    host = t.host or t.headers.host,
    headers = t.headers,
    proxy = t.proxy or nil, -- "http://127.0.0.1:80"
    sink = ltn12.sink.table(t_response),
  }

  local r, code, headers, status = http.request(reqt) -- 1 200 {} "HTTP/1.1 200 OK"
  if is_log_debug then
    log_debug('code: %s status:%s Headers:%s', code, status, inspect(headers))
    log_debug('RESPONSE:%s', inspect(t_response))
  end

  -- local r, code = http.request(reqt) -- 1 200 {} "HTTP/1.1 200 OK"
  if not r then
    log_debug('reqt:', reqt)
    log.error('[ERROR]: %s "%s"', v2s(code), v2s(status))
    return false
  end

  -- middleware text from json|xml -> luatable (by opts)
  local response = M.decode((t_response or E)[1], opts, reqt.uri)

  local callback = (opts or E).callback
  if callback then
    log_debug('run callback with', response)
    callback(response, opts, t.uri) -- > api
  else
    log_debug('Response: ', response)
    return response
  end
  -- end)
end

---@param raw_response string
---@diagnostic disable-next-line: unused-local
function M.decode(raw_response, opts, uri)
  return raw_response
end

return M

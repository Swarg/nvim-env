-- 24-09-2024 @author Swarg
local M = {}

local E = {}

function M.get_comment_content(s)
  local comment = string.match(s or '', '^%s*/%*%s*(.-)%s*%*/%s*')
  return comment
end

--
-- parse lines from style.css
-- parses simple flat CSS code, extract class names and rule contents from it
--
-- example of use:
-- - to generate html tags for these classes
--
---@param lines table
---@param off number?
---@param opts table?
---@return table{classes{name, body, comment}}
function M.parse_css(lines, off, opts)
  opts = opts or E
  local t = {
    classes = {}
  }
  local i = off or 0

  local comment = M.get_comment_content(lines[1])
  if comment and comment ~= '' then
    t.comment = comment
    i = 1
  end

  while i < #lines do
    i = i + 1
    local line = lines[i]
    if line:sub(1, 1) == '.' then -- css class
      -- `:` to support pseudo css-classes and elements
      local css_class = string.match(line, '^%.([%w%-_:]+)%s*{%s*$')
      local rule_body = {}
      if css_class then
        while i < #lines do
          i = i + 1
          line = lines[i]
          if string.match(line, '^%s*}%s*$') then
            t.classes[#t.classes + 1] = { css_class, rule_body }
            break
          else
            local s = string.match(line, '^%s*(.-)%s*$')
            if s:sub(1, 2) ~= '/*' and s:sub(-2, -1) ~= '*/' then
              rule_body[#rule_body + 1] = s
            end
          end
        end
      end
    end
  end
  return t
end

---@param s string
local function split(s, sep, out)
  out = out or {}
  sep = sep or "\n"
  local p = 1
  while true do
    local n = string.find(s, sep, p, true)
    if not n then
      break
    end
    out[#out + 1] = string.sub(s, p, n - 1)
    p = n + 1
  end
  out[#out + 1] = string.sub(s, p, #s)

  return out
end

--
---@param s string?
---@return table
function M.parse_kvpairs(s)
  local map = {}
  if type(s) == 'string' and s ~= '' then
    local pairs = split(s, ';', {})
    for _, pair in ipairs(pairs) do
      local k, v = string.match(pair, '^%s*([^:]+)%s*:%s*(.-)%s*$')
      if k and k ~= '' and v then
        map[k] = tonumber(v) or v
      end
    end
  end
  return map
end

--
return M

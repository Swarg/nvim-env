-- 03-06-2024 @author Swarg

local ustr = require 'env.sutil'
local u8 = require 'env.util.utf8'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


---@param lines table
---@param sep string?
---@param opts table?
---@return table
function M.lines_to_table_obj(lines, sep, opts)
  assert(type(lines) == 'table', 'lines')
  opts = opts or {}
  sep = sep or '|'

  local t = {}
  local columns = nil
  for _, line in ipairs(lines) do
    -- if string.match(line, '^%s*%-+%+%-+[%-%+]+%s*$') == nil then
    local row = ustr.split_range(line, sep, 1, #line)

    if type(row) == 'table' then
      if not columns then columns = #row end

      -- skip not a rows(like header sep --+--+-- or comments or ...
      if columns and #row >= columns then
        for i, cell in ipairs(row) do
          row[i] = ustr.trim(cell)
        end
        t[#t + 1] = row
      end
    end
    -- end
  end

  return t
end

-- format an object containing a title and lines into the text of a psql table
--
---@param t table{row1{cell1, cell2}, row2,...}
---@param sep string?
---@param opts table?{markdown}
---@param lines table? output
---@return table
function M.fmt_table_obj_to_lines(t, sep, opts, lines)
  assert(type(t) == 'table', 't')
  sep = sep or '|'
  opts = opts or {}
  if sep == "\t" or sep == "\n" or sep == "\r" then sep = '|' end

  local tmax = {}

  -- get max columns width
  for _, row in ipairs(t) do
    for i, cell in ipairs(row) do
      -- tmax[i] = tmax[i] or 0
      local len = u8.utf8_slen(cell)
      if not tmax[i] or len > tmax[i] then tmax[i] = len end
    end
  end

  -- format
  lines = lines or {}
  assert(t[1], 'expected not empty table with columns names')

  -- build columns(names) and line separator
  local col, lsep = '', ''
  local column_sep = opts.markdown == true and '|' or '+'
  local pref = opts.markdown == true and '|' or ''

  for i, cell in ipairs(t[1]) do
    local max = (tmax[i] or #cell)
    col = col .. ' ' .. ustr.align_str_to(cell, max, 'center') .. ' ' .. sep
    lsep = lsep .. '-' .. string.rep('-', max, '') .. '-' .. column_sep
  end
  lines[#lines + 1] = pref .. col:sub(1, -2)
  lines[#lines + 1] = pref .. lsep:sub(1, -2)
  -- --
  local colnum = #(t[1] or E)

  for r = 2, #t do
    local row = t[r] or E
    local line = ''

    for i = 1, colnum do
      local cell = row[i] or ''
      local len = u8.utf8_slen(cell)
      local max = (tmax[i] or len)
      -- line = line .. fmt(' %-' .. max .. 's %s', cell, sep)
      local aligned_cell = cell .. string.rep(' ', max - len, '')
      line = line .. ' ' .. aligned_cell .. ' ' .. sep
    end

    lines[#lines + 1] = pref .. line:sub(1, #line - 1)
  end

  return lines
end

return M

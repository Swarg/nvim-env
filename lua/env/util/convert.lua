-- 03-02-2024 @author Swarg
-- Goal: Convert nums
--

local M = {}
-- local input = 0.5
-- local output = string.format("%x", input * 255) -- "7F"

local HEX_CHARS = "0123456789ABCDEF"

---@param num number
---@return string
function M.decimal_to_hex(num)
  if num == 0 then return '0' end

  local neg = false
  if num < 0 then
    neg = true
    num = num * -1
  end

  local result = ""
  while num > 0 do
    local n = num % 16 --math.mod(num, 16)
    result = string.sub(HEX_CHARS, n + 1, n + 1) .. result
    num = math.floor(num / 16)
  end
  if neg then
    result = '-' .. result
  end
  return result
end

-- Use tonumber to convert from strings to numbers.
-- print(tonumber("0x7DE"))
-- print(tonumber("7DE",16))
-- You can also use hexadecimal constants directly: print(0x7DE)

--
---@param hex string
---@return number?
function M.hex_to_decimal(hex)
  if hex then
    if hex:sub(1, 2) ~= '0x' then
      hex = '0x' .. hex
    end
    return tonumber(hex, 16)
  end
end

-- if s:sub(1, 1) == '{' then
--   return loadstring('return ' .. s)()
-- end

--
-- string of array of ints into array itself
-- "{1, 2}" -->  {1, 2}  or "4,16" -> {4,16}
function M.srt2numarr(s)
  if s then
    s = s:gsub('{', ''):gsub('}', ''):gsub(' ', '')
    local t = {}
    for n in string.gmatch(s, '([^,]+)') do
      t[#t + 1] = tonumber(n)
    end
    return t
  end
  return {}
end

local function fn_digits_to_char(digits)
  return string.char(tonumber(digits, 16))
end

---@param offset number?
function M.hex_decode(hex, offset)
  return (string.gsub(hex, "%x%x", fn_digits_to_char, offset))
end

local function char_to_digits(char)
  return string.format("%02x", char:byte())
end

---@param str string
---@param offset number?
function M.hex_encode(str, offset)
  return (string.gsub(str, ".", char_to_digits, offset))
end

return M

-- 01-12-2024 @author Swarg
--
-- utils and implementation layer of a text-to-speech integration
--

local fs = require 'env.files'
local su = require 'env.sutil'
local cjson = require 'cjson'

local mpv = require 'env.bridges.mpv'
local uvtt = require "env.langs.vtt.utils"


local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local find = string.find

-- todo ask via api
M.known_speakers = {
  "aidar", "baya", "kseniya", "xenia", "eugene", "random"
}

---@return table
function M.get_tts_config(config)
  if type(config) ~= 'table' then
    local ok, setup = pcall(require, 'env.setup') -- for testing
    if ok then
      config = setup._get_config()
    end
  end
  local conf = assert(config, 'has config of env.setup')
  conf.speech = conf.speech or {}
  conf.speech.tts = conf.speech.tts or {
    api_url = nil,
    speaker = 'xenia',
    sample_rate = 48000,
    ssml = nil,
    outdir = './audio'
  }
  return conf.speech.tts
end

local replace_shorthand_tags, process_input_text
local get_current_subt_entry, check_subt_range_duration


local play_vtt_audio

-- tts stuff
---@param opts table?{speaker, api_url, ssml, sample_rate, ...}
---@param play boolean?
---@return boolean
---@return string?
function M.do_generate_text_to_speech(opts, play)
  opts = opts or M.get_tts_config()
  local e, err = get_current_subt_entry(opts)
  if not e then return false, err end ---@cast e table

  local ok, errmsg, path = M.generate(e.text, opts)
  if not ok then return false, errmsg end
  assert(type(path) == 'string', 'path')

  local outdir = opts.outdir or './audio'
  local start_timestemp
  ok, errmsg, start_timestemp = check_subt_range_duration(e, outdir, path)
  if not ok then
    return false, errmsg
  end

  if (play or opts.play) and start_timestemp then
    play_vtt_audio(path, start_timestemp)
  end

  return true
end

--
-- check the duration of the generated audio to see if it will fit within the
-- specified boundaries of the current subtitle recording
--
---@return boolean
---@return string? errmsg
---@return string? message
---@param play boolean?
function M.do_check_audio_duration(opts, play)
  opts = opts or M.get_tts_config()
  local e, err = get_current_subt_entry(opts)
  if not e then return false, err end ---@cast e table
  local outdir = assert(opts.outdir, 'outdir')
  local path = fs.join_path(outdir, assert(opts.output_filename))

  local _, msg, start_timestemp = check_subt_range_duration(e, outdir, path)

  if (play or opts.play) and start_timestemp then
    if not fs.file_exists(path) then
      return false, 'cannot play audio - file not found: ' .. v2s(path)
    end
    play_vtt_audio(path, start_timestemp)
  end
  return true, nil, msg
end

--
--
---@param opts table
---@return table?
---@return string?
function get_current_subt_entry(opts)
  local e, err = uvtt.get_current_subt_entry()

  if not e then
    return nil, 'cannot detect current vtt entry: ' .. err
  end
  if not e.index then
    return nil, 'no index of the entry from vtt file'
  end
  if type(e.text) ~= 'table' then
    return nil, 'no text to generate'
  end

  if #e.text == 0 then
    return nil, 'empty text to generate'
  end

  opts.output_filename = M.gen_filename(e.index)
  opts.outdir = opts.outdir or './audio'

  return e
end

---@param e table{index, tstart, tend, ...}
---@param path string?
---@return boolean
---@return string?
---@return number? start_timestemp
function check_subt_range_duration(e, outdir, path)
  path = path or M.get_audio_entry_path(e, outdir)

  local generated_duration = M.get_mediafile_duration(path)
  if not generated_duration or generated_duration < 0 then
    return false, 'cannot get duration of ' .. v2s(path)
  end

  local duration, start_timestemp = uvtt.entry_get_duration(e)
  if not duration or duration < 0 then
    return false, 'cannot get duration of vtt entry: ' .. v2s(e.tstart)
  end
  -- how much time in seconds remains free for this recording
  local free_time = duration - generated_duration -- remains

  -- check is generated auto fit to this vtt entry
  local msg = fmt('%s: duration: %s, generated: %s, remains_free: %s (sec)',
    v2s(path), duration, generated_duration, free_time)

  return (generated_duration <= duration), msg, start_timestemp
end

-- echo '{ "command": ["script-message", "script_audio_tracks",
--           "add_audio_with_delay", "./audio.mp3", "4"] }' | \
--
---@param path string
---@param avdelay number
function play_vtt_audio(path, avdelay)
  mpv.mpv_pause()
  mpv.mpv_set_time_pos(avdelay)

  mpv.mpv_script_message(
    "script_audio_tracks", "add_audio_with_delay", path, v2s(avdelay), nil)

  --vim.loop.sleep(1)
  -- vim.defer_fn(function()
  print("[DEBUG] avdelay:", avdelay)
  mpv.mpv_set_time_pos(avdelay)
  mpv.mpv_play()
  -- end, 500)
end

--
-- send request to REST API to generate text-to-speech  (tts)
--
---@param lines table
---@param params table{api_url, speaker, ssml, sample_rate, outdir, output_filename}
---@return boolean
---@return string? errmsg
---@return string? path
function M.generate(lines, params)
  assert(type(params) == 'table', 'params')

  local speaker = assert(params.speaker, 'speaker')
  local host = assert(params.api_url, 'api_url')
  local ssml = params.ssml
  --
  local s = table.concat(process_input_text(lines, ssml), ' ')

  if ssml and not su.starts_with(s, "<speak>") then
    s = '<speak>' .. s .. '</speak>' -- autowrap to valid xml
    params.ssml = true
  end

  local req = {
    speaker = assert(speaker),
    text = s,
    ssml = ssml,
    sample_rate = params.sample_rate or 48000,
  }
  return M.download_file_via_post(host .. '/api/tts', req, params)
end

function M.gen_filename(idx)
  return string.format('a_%04d.mp3', v2s(idx))
end

function M.get_audio_entry_path(e, outdir)
  local output_filename = M.gen_filename(e.index)
  return fs.join_path(outdir, output_filename)
end

--
--
---@param url string
---@param payload string|table?
---@param opts table?{outdir, output_filename}
---@return boolean
---@return string? errmsg
---@return string? path
function M.download_file_via_post(url, payload, opts)
  local io = io
  local headers = {}
  local source = nil
  opts = opts or {}
  local outdir = opts.outdir or './'
  local output_filename = opts.output_filename or nil

  if outdir ~= './' and not fs.dir_exists(outdir) then
    if not fs.mkdir(outdir) then
      return false, 'cannot mkdir ' .. v2s(outdir)
    end
  end

  -- asume that this is REST API
  if type(payload) == 'table' then
    payload = cjson.encode(payload)
    headers = {
      accept = 'application/json',
      ['content-type'] = 'application/json',
      connection = 'close',
    }
  end
  assert(type(payload) == 'string', 'POST payload')
  source = ltn12.source.string(payload);

  local http = require "socket.http"
  local ltn12 = require "ltn12"

  local rawoutput = {}

  local r, code, resp_headers, status = http.request {
    method = "POST",
    url = url,
    headers = headers,
    sink = ltn12.sink.table(rawoutput),
    source = source, -- to send post data (nullable) (post payload)
  }

  if not r or not code == 200 then
    return false, 'code:' .. v2s(code) .. ' ' .. v2s(status)
  end

  local ftype = resp_headers['content-type'] or ''
  if ftype == 'application/json' then
    local _, resp = pcall(cjson.decode, table.concat(rawoutput, ''))
    local errmsg = type(resp) == 'table' and resp.error or '?'
    return false, errmsg
  end
  -- ["content-disposition"] = "inline; filename=audio_1733049009085.mp3",
  -- ["content-length"] = "95373",
  -- ["content-type"] = "audio/mpeg",
  -- local size = resp_headers['content-length'] or 0
  if not output_filename or output_filename == '' then
    local disp = resp_headers['content-disposition'] or ''
    output_filename = string.match(disp, 'filename=([^=]+)$')
    if not output_filename then
      return false, 'unknown filename from content-disposition: ' .. v2s(disp)
    end
  end
  local fn = fs.join_path(outdir, output_filename)
  local h, err = io.open(fn, 'wb')
  if not h then
    return false, err or 'on open file'
  end
  for i = 1, #rawoutput, 1 do
    assert(h:write(rawoutput[i]))
  end
  assert(h:close())

  return true, nil, fn
end

---@param fn string
---@return number?
---@return string?
function M.get_mediafile_duration(fn)
  if not fs.file_exists(fn) then
    return nil, 'not found ' .. v2s(fn)
  end
  local cmd = "ffprobe -v error -show_entries format=duration " ..
      "-of default=noprint_wrappers=1:nokey=1 " .. v2s(fn)
  local h, err = io.popen(cmd, 'r')
  if not h then
    return nil, 'error: ' .. v2s(err)
  end
  local duration = h:read("*a")
  h:close()

  return tonumber(duration)
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--
--
--
---@param lines table
---@param ssml boolean?
---@return table
function process_input_text(lines, ssml)
  if #lines > 0 then
    lines[1] = match(lines[1], '^%s*(.-)%s*$') -- trim
  end
  if #lines > 1 then
    lines[#lines] = match(lines[#lines], '^%s*(.-)%s*$') -- trim
  end

  if not ssml then
    return lines
  end

  for i = 1, #lines do
    lines[i] = replace_shorthand_tags(lines[i])
  end

  return lines
end

local short_tag_to_full

---@param line string
---@param ssml boolean?
function replace_shorthand_tags(line, ssml)
  ssml = ssml
  local p, res = 1, nil

  while p < #line do
    local s, e, cap = find(line, "<!([^<>]+)>", p, false)
    if not s or not e then
      if not res then
        return line -- original
      end
      break
    end
    if res == nil then res = '' end
    res = res .. line:sub(p, s - 1)
    res = res .. short_tag_to_full(cap)

    p = e + 1
  end
  if res and p < #line then
    res = res .. line:sub(p)
  end
  return res or line
end

local prosody_map = {
  pitch = { L = 'x-low', l = 'low', m = 'medium', h = 'high', H = 'x-high' },
  rate = { S = 'x-slow', s = 'slow', m = 'medium', f = 'fast', F = 'x-fast' },
}
--
-- This is my own addition to the standard syntax, shorter
-- shorter, in order to get rid of text bloat.
-- https://github.com/snakers4/silero-models/wiki/SSML#prosody
--
function short_tag_to_full(s)
  local tag = s:sub(1, 1)

  if tag == 'b' then -- <!b05> -- mean <break time="500ms"/>
    local sec = assert(tonumber(s:sub(2, 2)), '!b sec')
    local ms_x100 = assert(tonumber(s:sub(3, 3)), '!b ms')
    local v = sec * 1000 + ms_x100 * 100
    return '<break time="' .. v .. 'ms"/>'
  end

  local close_pair = false
  local i = 1
  if tag == '/' then
    tag = s:sub(i + 1, i + 1)
    close_pair = true
    i = i + 2
  end

  -- <!ppl> Word <!/p>  ->  <prosody pitch="low"> Word </prosody>
  -- <!ppL> Word <!/p>  ->  <prosody pitch="x-low"> Word </prosody>
  if tag == 'p' then -- prosody
    if close_pair then
      return '</prosody>'
    end

    local typ = s:sub(i + 1, i + 1)
    local value = s:sub(i + 2, i + 2)
    if typ == 'p' then
      -- pitch - Raise or lower the tone (pitch) of the speech:
      -- x-low, low, medium, high, x-high: Set the pitch to a predefined value.
      typ = 'pitch'
    elseif typ == 'r' then
      -- rate - Modify the rate of the speech:
      -- x-slow, slow, medium, fast, x-fast: Set the rate to a predefined value.
      typ = 'rate'
    else
      error('unsupported type fro prosody: ' .. v2s(typ))
    end
    local svalue = prosody_map[typ][value]
    if not svalue then
      error('unsupported value of ' .. v2s(typ) .. ' got: ' .. v2s(value))
    end
    return fmt('<prosody %s="%s">', typ, svalue)
  end

  error('unknown shorhand: ' .. v2s(s) .. ' tag:' .. v2s(tag))
end

--------------------------------------------------------------------------------

--
-- Join generated audio into one audio track
--
---@param subtfn string
---@param dir string
---@param opts table? {outputfn, sample_rate, channels}
---@return false|string
---@return string? errmsg
---@return table? entries
function M.join_audio_parts(dir, subtfn, opts)
  assert(type(dir) == 'string', 'dir')
  assert(type(subtfn) == 'string' and subtfn ~= '', 'substfn')
  opts = opts or {}
  local input_fn = opts.input or 'silence.wav'
  local output_fn = opts.output or 'out.wav'
  ---@diagnostic disable-next-line: unused-local
  local sample_rate = opts.sample_rate or 44100 -- 24000
  ---@diagnostic disable-next-line: unused-local
  local channels = opts.channels or 2
  local skip_missing = opts.ignore_gaps -- ignore missing number index in fn

  if not fs.file_exists(subtfn) then
    return false, 'not found file with subtitles: ' .. v2s(subtfn)
  end
  if not fs.dir_exists(dir) then
    return false, 'not found directory: ' .. v2s(dir)
  end

  local input_path = fs.join_path(dir, input_fn)
  if not fs.file_exists(input_path) then
    return false, 'not found input audio file: ' .. v2s(input_path) .. "\n"
        .. "generate silent audio file first:\n"
        .. "sox -n -r 44100 -c 2 silence.wav trim 0.0 20.0"
    -- -r is sample_rate
    -- -c is channels (2 is sterio)
    -- 20.0 is duration of the silence.wav
  end

  -- parse file with subtitles
  local entries, err = uvtt.parse_file(subtfn)
  if type(entries) ~= 'table' then
    return false, err
  end ---@cast entries table

  local t = { 'sox -m ' .. input_fn } -- -m
  local tm = {}
  -- sox -m silence.wav "|sox sound1.mp3 -p pad 0" "|sox sound2.mp3 -p pad 2" out.wav
  for i = 1, #entries do
    local e = entries[i]
    local n = e.index
    local offset_sec = uvtt.str_timestamp_to_num(e.tstart)
    local audio_fn = fs.join_path('audio', M.gen_filename(n))
    local fn = fs.join_path(dir, audio_fn)
    local ignore = false
    if not fs.file_exists(fn) then
      if skip_missing then
        local msg = fmt('# [WARN] no audio#%s %s: %s', v2s(n), v2s(e.tstart), v2s(fn))
        tm[#tm + 1] = msg
        ignore = true
      else
        return false, 'stop at audio part #' .. v2s(n) .. ' not found: ' .. v2s(fn)
      end
    end
    if not ignore then
      t[#t + 1] = fmt(' "|sox %s -p pad %0.2f"', audio_fn, offset_sec)
    end
  end

  t[#t + 1] = output_fn
  local cmd = table.concat(t, ' ')

  local script_fn = fs.join_path(dir, 'join_audio_parts.sh')
  local h, errw = io.open(script_fn, 'wb')
  if not h then
    return false, 'on write ' .. v2s(script_fn) .. ': ' .. v2s(errw)
  end
  local body = "#!/bin/sh\n" .. table.concat(tm, "\n") .. "\n\n"
      .. "echo sox --combine mix ... # autioparts: " .. v2s(#entries) .. "\n"
      .. "date\n"
      .. cmd
      .. "\ndate\n"

  assert(h:write(body))
  h:close()

  return script_fn, nil, entries
end

--
-- Note: before use this command
-- you need to replace old or just add a new audiotrack into given video file
--
---@param entries table
function M.gen_ffmpeg_cut_parts_cmd(entries)
  local t = {}
  local offset = '0'

  for i = 1, #entries do
    local e = entries[i]
    local line = (e.text or E)[1] or ''
    local remove_typ = match(line, '^%-%-!remove%s*(.-)$')
    if remove_typ == '' or remove_typ == 'full' then
      local offset_sec_b = assert(uvtt.str_timestamp_to_num(e.tstart), 'tstart')
      local offset_sec_e = assert(uvtt.str_timestamp_to_num(e.tend), 'tend')
      -- local part = fmt('not(between(t,%s,%s))', offset_sec_b, offset_sec_e)
      t[#t + 1] = 'between(t,' .. offset .. ',' .. offset_sec_b .. ')'
      offset = tostring(offset_sec_e)
      -- -vf "select='not(between(t,521.7,540))',  setpts=N/FRAME_RATE/TB" \
    else
      -- to cover certain parts of the screen with pictures
      -- to remove some information from video
      -- todo
    end
  end

  -- preparing a command for cutting out unnecessary parts in the output video
  -- what marked in vtt file with timestamp range with --!remove
  local ffmpeg_remove_parts_cmd = ''
  if #t > 0 then
    local sv, sa = '', ''

    local last_offset_sec_e = uvtt.str_timestamp_to_num(entries[#entries].tend)
    t[#t + 1] = 'between(t,' .. offset .. ',' .. last_offset_sec_e .. ')'

    for i = 1, #t do
      local part = t[i]
      if sv ~= '' then sv = sv .. '+' end
      if sa ~= '' then sa = sa .. '+' end
      sv = sv .. part
      sa = sa .. part
    end

    ffmpeg_remove_parts_cmd = "ffmpeg -i out2.mp4\\\n"
        .. " -vf \"select='" .. sv .. "',setpts=N/FRAME_RATE/TB\" \\\n"
        .. " -af \"aselect='" .. sa .. "',asetpts=N/SR/TB\" \\\n"
        .. " output.mp4"
  end

  return ffmpeg_remove_parts_cmd
end

--------------------------------------------------------------------------------

if _G.TEST then
  M.test = {
    process_input_text = process_input_text,
    replace_shorthand_tags = replace_shorthand_tags,
    short_tag_to_full = short_tag_to_full,
  }
end
return M

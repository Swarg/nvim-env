-- 27-09-2023 @author Swarg
local M = {}
--

--------------------------------------------------------------------------------

M.debug = false
local dlog = {}
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match


---@diagnostic disable-next-line: unused-function, unused-local
local function dprint(...)
  if M.debug then
    local s = ''
    for i = 1, select('#', ...) do
      local el = select(i, ...)
      s = s .. ' ' .. tostring(el)
    end
    dlog[#dlog + 1] = s
  end
end

--------------------------------------------------------------------------------

function M.get_executable(cmd)
  local path = nil
  if cmd and cmd ~= nil then
    path = (M.os_run('which ' .. cmd) or ''):match("^([^\n]+)")
  end
  return path
end

-- via popen
function M.os_run(cmd)
  local handle = io.popen(cmd)
  if handle then
    local result = handle:read('*all')
    handle:close()
    return result
  else
    return false
  end
end

--  SIZE    VSZ   RSS CMD
-- 1308924
-- 4488256
--  645720 firefox-esr
--
-- default cmd is  "ps -Ao pid,ppid,user,rss,vsz,pcpu,command"
--
-- `man ps/SIZE` say:
-- vsz - virtual memory size of the process in KiB (1024-byte units).
--       Device mappings are currently excluded; this is subject to change.
--
-- size - is approximate amount of swap space that would be required if
-- the process were to dirty all writable pages and then be swapped out.
-- This number is very rough!
-- size - is the virtual size of the process (code+data+stack)
--
-- rss - resident set size, the non-swapped physical memory that a task has
--       used (in kilobytes). (+ shared lib mem?)
--
---@param cmd string?
---@param sort string? -size
function M.get_ps_output(cmd, sort)
  -- cmd = cmd or "ps -Ao pid,ppid,user,size,pcpu,command" --sort -size"
  cmd = cmd or "ps -Ao pid,ppid,user,vsz,size,rss,pcpu,command" --sort -size"
  if sort then                                                  -- in "-size" ..
    cmd = cmd .. '--sort ' .. tostring(sort)
  end
  return M.os_run(cmd) or ''
end

--
-- add cwd of the process by its PID, readed from the /proc/pid/cwd
--
---@param t table - list of the entries
function M.enrich_ps_state_with_cwd(t)
  local readlink = ((vim or E).loop or E).fs_readlink
  if type(t) == 'table' and readlink then
    for _, e in ipairs(t) do
      if type(e) == 'table' and e.pid and not e.cwd then
        -- 'readlink /proc/$pid/cwd'
        e.cwd = readlink("/proc/" .. e.pid .. "/cwd")
      end
    end
  end
end

--
-- parsing output of this command:
--  ps -Ao size,pid,user,command --sort -size
---@param output string
function M.parse_ps_output(output)
  local t = {}
  for s in string.gmatch(output, '([^\n]+)') do
    --            pid,    ppid,    user,     vsz     size     rss     pcpu      cmd
    local p = "%s*(%w+)%s+(%w+)%s+([^%s]+)%s+(%d+)%s+(%d+)%s+(%d+)%s+([%w%.]+)%s+(.*)%s*"
    local pid, ppid, user, vsz, size, rss, pcpu, cmd = string.match(s, p)

    pid = tonumber(pid)
    if pid and cmd then
      t[pid] = {
        pid = pid,
        ppid = tonumber(ppid),
        user = user,
        vsz = tonumber(vsz),   -- mem
        size = tonumber(size), -- mem
        rss = tonumber(rss),   -- mem
        pcpu = tonumber(pcpu), -- % 0.0
        cmd = cmd,
      }
    end
  end

  return t
end

M.ps_state_title = "   RSS   SIZE    VSZ      PID     PPID     USER  PCPU  COMMAND"

---@param sz number in Kb
function M.kb2readable(sz, mult)
  if mult then
    sz = sz / mult
  end
  if sz > 1024 * 1024 then
    return string.format("%.2fG", (sz / (1024 * 1024)))
  elseif sz > 1024 then
    return math.ceil(sz / 1024) .. 'M'
  elseif sz > 0 then
    return sz .. 'K'
  else
    return tostring(sz)
  end
end

-- default is bytes
-- string to kb
---@param s string|number
---@param mult number? if no KMG then use defualt mult
function M.str_size2num(s, mult)
  mult = mult or 1

  if type(s) == 'string' then
    local suff = s:sub(-1, -1)
    if suff == 'K' or suff == 'k' then
      return tonumber(s:sub(1, -2)) * 1024
    elseif suff == 'M' or suff == 'm' then
      return tonumber(s:sub(1, -2)) * 1024 * 1024
    elseif suff == 'G' or suff == 'g' then
      return tonumber(s:sub(1, -2)) * 1024 * 1024 * 1024
    end
  end
  return (tonumber(s) or 0) * mult
end

---@param t table?
---@param pid number?
function M.ps_state2str(t, pid)
  if t then
    --   RSS   SIZE    VSZ      PID     PPID     USER  PCPU  COMMAND
    local sfmt = "%6s %6s %6s %8s %8s %8s %5s %s"
    local vsz = M.kb2readable(t.vsz)
    local rss = M.kb2readable(t.rss)
    local sz = M.kb2readable(t.size)
    local cmd = t.cmd
    -- M.ps_state_title
    if t.cwd then
      cmd = t.cwd .. "   " .. cmd
    end
    return fmt(sfmt, rss, sz, vsz, t.pid, t.ppid, t.user, t.pcpu, cmd)
  elseif pid then
    return 'not found for pid: ' .. tostring(pid)
  end
  return 'nil'
end

---@param flatlist table{pid->{pid,ppid,cmd,..}}
---@param pid0 number -- parent pid for which needs to find all it childs
---@param recursive0 boolean?
function M.build_child_list(flatlist, pid0, recursive0)
  local list = {}

  for _, v in pairs(flatlist) do
    if v.ppid == pid0 then
      if recursive0 then
        local childs = M.build_child_list(flatlist, v.pid, recursive0)
        list[v.pid] = { v, childs = childs }
        if not next(childs) then
          list[v.pid].childs = nil
        end
      else
        list[#list + 1] = v
      end
    end
  end

  return list
end

---@param flat_list table
---@return table
function M.build_tree(flat_list)
  local tree, map = {}, {}

  local max_pid = 0
  for k, _ in pairs(flat_list) do if k > max_pid then max_pid = k end end

  for i = 1, max_pid do
    local t = flat_list[i]
    if t then
      if not t.ppid or t.ppid == 0 then -- root nodes without parents
        tree[t.pid] = { t, childs = {} }
      else
        local pa = tree[t.ppid] or map[t.ppid]
        if not pa then
          error('should taken from map. cannot find ppid:' .. tostring(t.ppid))
        end
        pa.childs = pa.childs or {}
        local node = { t, child = nil }
        pa.childs[t.pid] = node
        map[t.pid] = node -- flat
      end
    end
  end

  return tree
end

--------------------------------------------------------------------------------
--                               for tviewer
--------------------------------------------------------------------------------


---@return number size
---@return number childs count
function M.get_childs_mem(t)
  if t then
    local sz, c = 0, 0
    for _, v in pairs(t) do
      local sz1 = (v[1] or E).size
      if sz1 then
        sz = sz + sz1
        c = c + 1
      end
      if v.childs then -- table
        local sz0, c0 = M.get_childs_mem(v.childs)
        sz = sz + sz0
        c = c + c0
      end
    end
    return sz, c
  end
  return 0, 0
end

--
-- make a node name of the given table: (readable for tviewer)
--   symbol
--   range
---@param node table
---@param pref number?
function M.mk_node_name(node, pref)
  if node and type(node[1]) == 'table' and node[1].cmd then
    local t = node[1]
    local sz, childs_cnt = M.get_childs_mem(node.childs)
    local kb2h = M.kb2readable
    sz = sz + (t.size or 0)
    local n = 0
    if type(pref) == 'number' and pref > 0 then
      n = pref
    end
    local ptrn = "%-" .. n .. "s%2s %5s  %s"
    return string.format(ptrn, "", childs_cnt, kb2h(sz), t.cmd)
  end
  return ''
end

--[[

ps -eo size,pid,user,command --sort -size | \
  awk '{ hr=$1/1024 ; printf("%13.2f Mb ",hr) } { for ( x=4 ; x<=NF ; x++ ) { printf("%s ",$x) } print "" }' |\
  cut -d "" -f2 | cut -d "-" -f1


get the pids of all child processes of a given parent process <pid>
/proc/<pid>/task/<tid>/children

ps -A --forest -o pid=,tty=,stat=,time=,cmd=

]]

return M

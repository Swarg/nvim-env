-- 16-05-2023 @author Swarg
-- Goal: Commons for command handlers

local log = require('alogger')
local state_registry = require("env.state_registry");

local M = {}

-- list of registered commands
M.registered = {}

-- command name --> module_name of command handler
M.registered_handlers = {}

-- Store all names of self registered commands in list
function M.reg_cmd(name, command, opts)
  table.insert(M.registered, name)
  opts = opts or {}
  vim.api.nvim_create_user_command(name, command, opts)
end

--
-- register the command handler for given command name and module
-- (bind the require("module').handle to a given cmd name)
-- modules are:
--   - must has entry point function "handle"
--   - based on the Cmd4Lua (ArgWrapper)
--   - the dependencies passed in the entry-point function ("handle")
--   - can has complete to the nvim via module.opts
--
-- See for example: ./lua/env/command/call_func.lua
--
---@param cmd_name string
---@param module_name string
function M.reg_handler(cmd_name, module_name, config)
  assert(type(cmd_name) == 'string' and cmd_name ~= '', 'command name')
  assert(type(module_name) == 'string' and module_name ~= '', 'module name')

  local ok, module = pcall(require, module_name)
  if not ok then
    print('Cannot reqire: "' .. tostring(module_name) .. '"')
    print(module)
    return
  end

  module.config = config

  local command = module.handle
  if type(command) ~= 'function' then
    print('Not Found the interface function "handle" in the Module "' ..
      tostring(module_name) .. '"')
    return
  end

  local opts = module.opts or {}
  assert(type(opts) == 'table', 'opts')

  vim.api.nvim_create_user_command(cmd_name, command, opts)
  table.insert(M.registered, cmd_name)
  M.registered_handlers[cmd_name] = module_name

  -- auto register statefull module to keep its state on reload code in runtime
  if state_registry.is_statefull_module(module) then
    log.debug('registering statefull module %s ...', module_name)

    if state_registry.is_registered(module) then
      local f = string.format
      error(f('Module % alredy registered as statefull (4reload)', module_name))
    end
    local b = state_registry.register(module)
    log.debug('statefull module %s registered: %s', module_name, b)
  end
end

function M.indexof_registered_cmd(name)
  for i, cmd in ipairs(M.registered) do
    if cmd == name then
      return i
    end
  end
  return nil
end

-- remove registered nvim command
---@param cmd_name string?
function M.unreg_handler(cmd_name)
  assert(type(cmd_name) == 'string', 'cmd_name')

  local ok, err = pcall(vim.api.nvim_del_user_command, cmd_name)
  if ok then
    M.registered_handlers[cmd_name] = nil
    local n = M.indexof_registered_cmd(cmd_name)
    if n then
      table.remove(M.registered, n)
    end
    return true
  else
    print(err)
    return false
  end
  -- lua vim.api.nvim_del_user_command('EnvLineInsert')
  -- lua package.loaded["env.command.line_insert"] = nil
  -- lua require("env.util.commands").reg_handler('EnvLineInsert', "env.command.line_insert")
  --
  -- lua vim.api.nvim_del_user_command('EnvLineInsert'); package.loaded["env.command.line_insert"] = nil; require("env.util.commands").reg_handler('EnvLineInsert', "env.command.line_insert")
end

--
-- return full module name for given command name
--  Example:
--    "EnvReload" -> "env.command.reload"
--
---@param cmdname string
---@return string?
function M.get_cmd_handler(cmdname)
  return M.registered_handlers[cmdname or false]
end

function M.show_registered_cmds()
  if vim then
    print(vim.inspect(M.registered))
  end
end

function M.unregister_all_cmds(skip)
  local new_reg = {}
  for _, name in pairs(M.registered) do
    -- print('Unreg: ' .. name)
    if not skip or not skip[name] then
      pcall(vim.api.nvim_del_user_command, name)
      M.registered_handlers[name] = nil
    else
      new_reg[#new_reg + 1] = name
    end
  end
  -- todo keeped commands(not deleted
  M.registered = new_reg
end

-- wrapper to ensure no errors on get opts.fargs
function M.fargs_safely(opts)
  if opts then
    return opts.fargs or {}
  end
  return {}
end

function M.mk_completion_filter(arg)
  return function(s)
    return s and s:sub(1, #arg) == arg
  end
end

function M.completion_sort(items)
  table.sort(items)
  return items
end

--
-- create function to complete command input in nvim
--
---@param commands table<string>
---@return function
function M.mk_complete(commands)
  assert(type(commands) == 'table', 'command names')

  local handler_commands = commands or {}

  return function(arg, cmdline, cursor_pos)
    local words = vim.split(cmdline, ' ', { trimempty = true })
    if not words then
      return {}
    end
    local n = #words;
    -- case: 'cmd arg |'
    if cursor_pos and cmdline:sub(cursor_pos, cursor_pos) == '' then
      --su.substr(cmdline, cursor_pos, cursor_pos) == " " then
      n = n + 1
    end
    local filter = M.mk_completion_filter(arg)
    if n == 2 then -- select sub-command
      return M.completion_sort(vim.tbl_filter(filter, handler_commands))
    end
  end
end

---@param str string
function M.cp2clipboard(str)
  -- copy to clipboard
  if vim and vim.fn then
    vim.fn.setreg("+", str)
    vim.fn.setreg('"', str)
    vim.fn.setreg("*", str) -- to primary
    vim.fn.setreg('"', str)
  else
    print('Cannot copy to clipboard vim not found')
    print(str)
  end
end

return M

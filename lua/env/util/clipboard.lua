local M = {}

M.use_system_clipboard = true

---@param str string
function M.copy_to_clipboard(str)
  if type(str) == 'table' then
    local s = ''
    for _, l in pairs(str) do s = s .. "\n" .. l end
  end

  if vim and vim.fn then
    if M.use_system_clipboard == true then -- to system clipboard
      vim.fn.setreg("+", str)
      vim.fn.setreg('"', str)
      vim.fn.setreg("*", str) -- to primary?
      vim.fn.setreg('"', str)
    else                      -- to nvim clipboard
      vim.fn.setreg('"', str)
      vim.fn.setreg("1", str)
    end
    return true
  end
  return false
end

--
--
---@param patt string pattern to match content
---@return string?
---@return string? errmsg
function M.get_from_clipboard(patt)
  local s = vim.fn.getreg("+")
  patt = patt or '^[^%s]+$' -- for words only without spaces
  if not s or string.match(s, patt) == nil then
    s = vim.fn.getreg("*")
    if not s or string.match(s, patt) == nil then
      return nil, 'no word in system clipboard'
    end
  end

  return s
end

--------------------------------------------------------------------------------

---@return table?
function M.get_text_html_unix()
  local cmd = 'xclip -selection clipboard -o -t text/html'
  local h = io.popen(cmd, "r") -- no error raised when file not exists
  if h then
    local t = {}
    while true do
      local line = h:read("*l")
      if not line then break end

      t[#t + 1] = line
    end

    h:close()
    return t
  end
  return nil
end

return M

-- 01-03-2025 @author Swarg
--

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
--  [[  "key": "value", ]] --> "key", "value"
--
---@param s string
---@return string? key
---@return string? value
function M.parse_kvpair(s)
  if not s or #s == 5 then
    return nil, nil
  end
  local k, v = match(s, '^%s*("[%w_%-]+")%s*:%s*(.-)%s*,?%s*$')

  if v and v:sub(1, 1) == '"' then
    v = v:sub(2, -2)
  else
    if v == "null" then
      v = 'null' -- ?
    elseif v == "true" then
      v = true
    elseif v == 'false' then
      v = false
    else
      v = tonumber(v) or v
    end
  end

  return k, v
end

return M

-- 09-01-2024 @author Swarg
local M = {}
--
local log = require('alogger')
local httpu = require('env.util.http')
local ubash = require('env.util.bash')


local D = require("dprint")
local dprint = D.mk_dprint_for(M, false)
M.dprint = dprint

function M.is_syntax_block_head(cbi)
  if cbi and cbi.current_line and #cbi.current_line > 3 then
    local pref = cbi.current_line:sub(1, 3)
    if pref == '```' then
      cbi.md_block_syntax = cbi.current_line:sub(4)
      return cbi.md_block_syntax and cbi.md_block_syntax ~= ''
    end
  end
  return false
end

--
--- ```syntax -> syntax
--  ```       -> ''
---@param line string
---@return string?
function M.get_syntax_of_block_head(line)
  log.debug("get_syntax_of_block_head")
  local syntax = nil
  if line and #line >= 3 and line:sub(1, 3) == '```' then
    if #line == 3 then -- end of ```-``` block
      syntax = ''
    else
      local e = string.find(line, ' ', 3, true) -- '"```x " -> "x" '
      e = e and (e - 1) or #line
      syntax = line:sub(4, e)
    end
  end
  log.debug('syntax of block: ', syntax)
  return syntax
end

---@param cbi table
function M.is_inside_block(cbi)
  local inside, syntax = false, nil
  -- if not cbi or cbi.current_line == '```' then
  -- end
  if cbi and cbi.current_line ~= '```' then
    local bn, ln, bunch_size = cbi.bufnr, cbi.cursor_row, 32
    local s = math.max(0, ln - bunch_size)
    local lines = vim.api.nvim_buf_get_lines(bn, s, ln - 1, false)
    cbi.md_block_syntax = nil

    if lines then
      for i = #lines, 1, -1 do
        -- dprint('i:', i, line)
        syntax = M.get_syntax_of_block_head(lines[i])
        if syntax == '' then -- consider ```
          cbi.md_block_syntax = ''
          inside = false
          break
        elseif syntax then
          -- dprint('syntax:', syntax)
          cbi.md_block_syntax = syntax
          inside = true
          break
        end
      end
    end
  end

  log.debug('is_inside_block: %s syntax: |%s|', inside, syntax)
  return inside -- false
end

---@param cbi table?
---@return string?
function M.get_block_syntax(cbi)
  return cbi and cbi.md_block_syntax or nil
end

---@param cbi table
function M.is_shell_block(cbi)
  local s = M.get_block_syntax(cbi)
  local f = s == 'sh' or s == 'bash' -- TODO add another os executable
  log.debug('is_shell_block: %s', f)
  return f
end

function M.is_http_block(cbi)
  local s = M.get_block_syntax(cbi)
  return s == 'http' or s == 'https'
end

local function res_with_err(msg, res)
  res = res or {}
  log.debug(msg)
  res.err = msg
  return res
end

---@param line string
---@return string?    -- nil or ( ' or " )
function M.get_not_closed_quote(line)
  if line then
    local group, prev_c = nil, nil
    for i = 1, #line do
      local c = line:sub(i, i)
      if (c == '"' or c == "'") and prev_c ~= '\\' then
        if not group then
          group = c
        elseif group == c then
          group = nil
        end
      end
      prev_c = c
    end

    return group
  end
  return nil
end

--
-- get bash command from Markdown
-- cursor should stay under os-command inside inlined md-block ```sh or ```bash
-- this function should pick full command from the current line in buffer
-- with multiline supporting (splited lines by '\ ' will be concatenated)
--
-- autowrap
-- echo arg1 | echo art2  --> bash -c "echo arg1 | echo arg2"
--
---@param bufnr number
---@param lnum number
---@return table {cmd, args, not_finished_line}
function M.get_full_bash_command(bufnr, lnum)
  log.debug('get_full_bash_command', bufnr, lnum)
  bufnr = bufnr or 0

  if type(lnum) ~= 'number' then
    return res_with_err('lnum not defined!')
  end

  local A = vim.api

  local lines = A.nvim_buf_get_lines(bufnr, lnum - 2, lnum + 4, false)
  if not lines or #lines < 1 then
    return res_with_err('too few lines to recognize command')
  end

  -- is prev line ends with \
  if lines[1] and lines[1]:sub(-1, -1) == '\\' then
    return res_with_err('Its not a begin of the shell command. ' ..
      'Previous line ends with \\')
  end

  if lines[2] and lines[2] == '' then
    return res_with_err('Empty string')
  end

  local cmd, i, off = '', 1, 0
  local ml_quote, not_finished_line = nil, false

  while (i <= #lines) do
    i = i + 1
    local line = lines[i]
    -- log.debug('lnum i: %s/%s[off:%s]: %s', i, #lines, off, line)
    -- dprint('i:', i, '/:', lines_cnt, 'off:', off, line)
    if not line or line == '' or line == '```' then break end

    if ml_quote then
      -- support multiline string like:  jq ' line1 \n line 2 '
      cmd = cmd .. " " .. line -- ? "\n"
      local q = M.get_not_closed_quote(line)
      if q == ml_quote then
        ml_quote, not_finished_line = nil, false
        break -- found end of quoted multiline
      end
      --
    elseif line:sub(1, 1) ~= '#' then
      -- to support multiline command
      if line:sub(-1, -1) == '\\' then
        cmd = cmd .. line:sub(1, -2)
        not_finished_line = true
        -- is current index last in the part of lines - fetch more lines if has
      else
        cmd = cmd .. line
        -- support multiline string like:  jq ' line1 \n line 2 '
        ml_quote = M.get_not_closed_quote(line)
        if ml_quote then
          not_finished_line = true
        else
          not_finished_line = false
          break
        end
      end
      --
    elseif #cmd == 0 then
      return res_with_err('Its a comment not a shell command.')
    end

    -- get more lines from buffer for multiline commands
    -- ends with \\ and quoted
    if not_finished_line and i == #lines then
      off = off + i
      i = 0
      lines = A.nvim_buf_get_lines(bufnr, lnum + off - 2, lnum + off + 1, false)
      -- log.debug('off:', off, 'lnum:', lnum, 'lines_cnt:', #lines) --, lines)
    end
  end

  local res = { cmd = nil, args = nil, not_finished_line = nil }

  res.cmd, res.args = ubash.parse_line(cmd)

  if not_finished_line then -- command in buffer ends with "\"
    res.not_finished_line = true
  end

  log.debug('result: %s', res)
  return res
end

--
--
--
function M.get_http_request(bufnr, lnum, curr_line)
  log.debug('get_http_request', bufnr, lnum)
  bufnr = bufnr or 0
  curr_line = curr_line or vim.api.nvim_get_current_line()
  --
  local lines = vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum + 16, false)
  if not lines or #lines < 1 then
    return res_with_err('too few lines to recognize http-reguest')
  end
  local syntax = M.get_syntax_of_block_head(lines[1])
  log.debug('syntax: ', syntax, lines[1])
  if syntax ~= 'http' and syntax ~= 'https' then
    return res_with_err('unsupported syntax block ' .. tostring(syntax))
  end

  local i, lines_cnt, headers, body = 2, #lines, {}, nil
  local method, uri, proto, host

  method, uri, proto = httpu.parse_reguest_method_uri_proto(lines[2])
  if not httpu.is_valid_http_method(method) then
    return res_with_err('unkwnon http method: ' .. tostring(method))
  end
  if not uri or uri == '' then
    return res_with_err('illegal uri: ' .. tostring(uri))
  end
  if not proto or proto == '' then -- HTTP/1.1
    return res_with_err('illegal proto: ' .. tostring(proto))
  end

  while (i <= lines_cnt) do
    i = i + 1
    local line = lines[i]
    -- log.debug('lnum i: %s/%s: %s', i, lines_cnt, line)
    if not line or line == '```' then break end

    if not body then
      if line == '' then -- empty line -- sign of ends of header
        body = ''
      else
        local name, value = httpu.parse_header_entry(line)
        if name then
          -- headers[string.lower(name):gsub('-', '_')] = value
          headers[string.lower(name)] = value
        end
      end
    else
      if body ~= '' then
        body = body .. "\n"
      end
      if line ~= '' then
        --   body = body .. "\n" -- ??
        -- else
        body = body .. line
      end
    end
  end

  -- auto varing Content-Length by body size
  if headers['content-length'] == '*' and body then
    headers['content-length'] = tostring(string.len(body))
  end

  local reqt = {
    scheme = syntax, -- http or https  via Markdown-inline syntax block
    method = method,
    uri = uri,
    proto = proto,
    host = host,
    headers = headers,
    body = body,

    -- basic-auth:
    user = headers.user,
    password = headers.password,
    --
    proxy = headers.proxy, -- custom used by luasocket
    -- my custom header for send request to specified ip
    -- usefull then need to check server in ip with given host
    ip = headers.ip,
  }

  local h = reqt.headers
  h.ip, h.proxy, h.user, h.password = nil, nil, nil, nil

  return reqt
end

-- parse given  lines with Markdown table to table object
--
---@param lines table
---@return table
function M.lines_to_table_obj(lines)
  local t = {}
  for _, line in ipairs(lines) do
    local row = {}
    if line ~= '' and string.match(line, '^[%s|%-]+$') == nil then
      for s in string.gmatch(line, '([^|]+)') do
        row[#row + 1] = string.match(s, '^%s*(.-)%s*$')
      end
      t[#t + 1] = row
    end
  end
  return t
end

return M

-- 04-11-2023 @author Swarg
local M = {}
--
local ui = require("env.ui")
local fs = require 'env.files'
local tu = require("env.util.tables")
local log = require('alogger')
local clipboard = require("env.util.clipboard")
local devhelpers = require("env.util.tokenizer.devhelpers")

local D = require("dprint")
---@diagnostic disable-next-line: unused-local
local dprint = D.mk_dprint_for(M)
local R = require("env.require_util")
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local cjson = R.require("cjson", vim, "json")        -- vim.json

-- local api = vim.api -- in testing use require('env.ui.testhelper').mk_vim_api(..)

M.LINE_WIDTH = 80

local E = {}
--------------------------------------------------------------------------------

--
-- open object-table in the table-viewer buffer(UI)
--
---@param obj table
---@param name string
---@param open_node table|nil
---@param node_name_cb function|nil
function M.open_in_buf(obj, name, open_node, node_name_cb)
  if not obj then
    print(tostring(name) .. ' Object is: ' .. type(obj))
    return
  end

  local ctx = { state = M.wrap2state(obj, name, node_name_cb) }
  local state, bn = M.create_buf(ctx)
  if open_node then
    M.open_path(ctx.o, bn, open_node)
  end
  return state, bn
end

---@param obj_table table
---@param src string?
---@param node_name_callback function?
function M.wrap2state(obj_table, src, node_name_callback)
  -- mk deep copy?
  local state = {
    root = obj_table,
    cwd = obj_table,
    path = {},
    src = src,
    node_name_cb = node_name_callback,
  }
  return state
end

local function to_luatable(bin, filename)
  if bin:sub(1, 1) == '{' then
    bin = 'return ' .. bin
  end

  local load0 = loadstring(bin)
  if type(load0) ~= 'function' then
    error('cannot load ' .. tostring(filename) ..
      ' type:' .. type(load0) ..
      ' size: ' .. #bin
    )
  end
  ---@diagnostic disable-next-line: need-check-nil
  return load0()
end

local function decode_json(bin, filename)
  local ok, decoded = pcall(cjson.decode, bin)
  if not ok then
    error('cannot parse json: ' .. tostring(filename) .. ' size: ' .. #bin)
  end
  return decoded
end


-- load lua-table from file to "state"
---@param o table {root, cwd, path, src}
function M.load(o, filename)
  assert(type(filename) == 'string', 'filename')
  o = o or {}
  local bin = fs.read_all_bytes_from(filename)
  local bytes = -1
  if bin and bin ~= '' then
    bytes = #bin
    o = o or {}
    if fs.extract_extension(filename) == 'json' then
      o.root = decode_json(bin, filename)
    else
      o.root = to_luatable(bin, filename)
    end
    o.src = filename
    o.cwd = o.root
    o.path = {}
    dprint('[load] loaded bytes:', bytes, ' root:', o.root ~= nil)
  else
    M.clear(o)
  end
  return o, bytes
end

---@param o table {root, cwd, path, src}
function M.clear(o)
  assert(type(o) == 'table', 'o table state')
  o.src = nil
  o.root = nil
  o.cwd = nil
  o.path = nil
  ---
  o.opened = nil
end

---@param o table {root, cwd, path, src}
function M.cd(o, key)
  assert(type(o) == 'table', 'o table state')
  assert(type(key) == 'string' or type(key) == 'number', 'key')
  if type(o.cwd) ~= 'table' or type(o.root) ~= 'table' then
    return false
  end

  -- dprint('cd key:', key)
  if key == '/' then
    o.cwd = o.root
    o.path = {}
    --
  elseif key and key:sub(1, 2) == '..' then
    if o.path and #o.path > 0 then
      o.path[#o.path] = nil -- reduce full path to one up
      local cwd = o.root
      if #o.path == 0 then
        o.cwd = o.root
      else
        -- open parent of current "dir" bu retraverse by path
        M.get_value_in_path(o, o.path)
        for _, dir in pairs(o.path) do
          cwd = cwd[dir]
          if not cwd then
            print('Not Existed Key: ' .. tostring(dir))
            return false
          end
        end
      end
      return o.cwd ~= nil
      --
    elseif o.root then -- can't go deeper than the root
      print('/')
      return false
    end
    --
  else -- go deeper to subtable by the keyname
    o.path = o.path or {}
    local cwd = o.cwd[key]
    if not cwd then
      key = tonumber(key)
      if key then
        cwd = o.cwd[key]
      end
    end
    o.cwd = cwd

    if o.cwd ~= nil then
      o.path[#o.path + 1] = key
      return true
    else
      -- print('Not Found Entry(Dir) with key:', key)
      return false
    end
  end
end

---@param o table {root, cwd, path, src}
function M.pwd(o)
  assert(type(o) == 'table', 'o table state')

  local s
  if o.root == o.cwd and o.cwd ~= nil then
    s = '/'
  elseif o.root then
    o.path = o.path or {}
    s = '/ ' .. table.concat(o.path, ' ')
  end
  return s or ''
end

---@param o table {root, cwd, path, src}
function M.ls(o, list)
  assert(type(o) == 'table', 'o table state')

  local s = ''
  if type(o.cwd) == 'table' then
    local sep = ' '
    if list then sep = "\n" end
    ---@diagnostic disable-next-line: param-type-mismatch
    for k, v in pairs(o.cwd) do
      s = s .. sep .. tostring(k)
      if list then
        s = ' -- (' .. type(v) .. ')'
      end
    end
    if list then
      s = s .. "\n"
      ---@diagnostic disable-next-line: cast-local-type
      s = devhelpers.format_comments(s)
    end
  end
  return s
end

---@param o table {root, cwd, path, src}
function M.tree(o, max_depth)
  assert(type(o) == 'table', 'o table state')
  if not o.cwd then
    return 'nil'
  end
  max_depth = max_depth or 1
  --
  local function draw_node(t, depth, limit)
    if not type(t) == 'table' then
      return ''
    end
    if depth > limit then
      return ' -- (TABLE) [+]'
    end

    local s, tab = '', nil
    tab = string.format("%-" .. (depth * 4) .. "s", ' ')

    -- "dirs" with string names
    for k, v in pairs(t) do
      if type(k) ~= 'number' then
        local t0 = type(v)
        if t0 == 'table' then
          s = s .. "\n" .. tab .. tostring(k) .. draw_node(v, depth + 1, limit)
        end
      end
    end
    -- "dirs" with number indexes
    for k, v in pairs(t) do
      if type(k) == 'number' then
        local t0 = type(v)
        if t0 == 'table' then
          s = s .. "\n" .. tab .. tostring(k) .. draw_node(v, depth + 1, limit)
        end
      end
    end
    -- "files" with string names
    for k, v in pairs(t) do
      if type(k) ~= 'number' then
        local t0 = type(v)
        if t0 ~= 'table' then
          s = s .. "\n" .. tab .. tostring(k) .. ' -- (' .. t0 .. ')'
        end
      end
    end
    -- files with indexes names
    for k, v in pairs(t) do
      if type(k) == 'number' then
        local t0 = type(v)
        if t0 ~= 'table' then
          s = s .. "\n" .. tab .. tostring(k) .. ' -- (' .. t0 .. ')'
        end
      end
    end

    s = s .. "\n"
    return s
  end

  return M.pwd(o) .. "\n" ..
      devhelpers.format_comments(draw_node(o.cwd, 1, max_depth))
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------

-- the mappings bufnr -> state with table-object
M.tables = {}

--
-- create buffer with keybinding to view table
-- see help by g?
--
---@param ctx table
---@param bname string|nil
function M.create_buf(ctx, bname)
  assert(type(ctx) == 'table', 'ctx')
  assert(type(ctx.state) == 'table', 'ctx.state')
  local api = vim.api or {}

  local win = api.nvim_get_current_win()
  bname = bname or ctx.state.src or 'tviewer'
  -- reuse already exists buf
  if bname then
    local oldbufnr = ui.get_buf_with_name(bname)
    if oldbufnr and oldbufnr ~= -1 then
      ctx.out_bufnr = oldbufnr -- reuse
    end
  end

  if not ctx.bufnr or ctx.bufnr == 0 then
    ctx.bufnr = api.nvim_get_current_buf()
  end
  -- if oldbufnr ~= -1 then
  --   vim.cmd(string.format('silent! confirm bd %d', oldbufnr))
  -- end
  if not ctx.out_bufnr or ctx.out_bufnr == -1 then
    ctx.out_bufnr = api.nvim_create_buf(true, true)
  end
  api.nvim_win_set_buf(win, ctx.out_bufnr) -- mk foreground
  pcall(api.nvim_buf_set_name, ctx.out_bufnr, bname)

  api.nvim_buf_set_option(ctx.out_bufnr, 'modifiable', true) -- clear old
  api.nvim_buf_set_lines(ctx.out_bufnr, 0, -1, true, {})
  api.nvim_buf_set_option(ctx.out_bufnr, 'buftype', 'nofile')
  api.nvim_buf_set_option(ctx.out_bufnr, 'swapfile', false)
  api.nvim_buf_set_option(ctx.out_bufnr, 'filetype', 'tbl_viewer')
  api.nvim_win_set_option(win, 'cursorline', true)

  api.nvim_buf_set_var(ctx.out_bufnr, 'prev_buf', ctx.prev_bufnr or ctx.bufrn)

  local bufnr = ctx.out_bufnr
  local map = vim.api.nvim_buf_set_keymap
  local function mk_opts(callback)
    return {
      silent = true,
      noremap = true,
      nowait = true,
      callback = callback
    }
  end

  -- actions via keybinding
  map(bufnr, 'n', 'q', '', mk_opts(M.do_close_and_clean_buf))
  map(bufnr, 'n', 'o', '', mk_opts(M.do_toggle_open_close))
  map(bufnr, 'n', '<space>', '', mk_opts(M.do_toggle_open_close))
  map(bufnr, 'n', '<enter>', '', mk_opts(M.do_toggle_open_close))
  map(bufnr, 'n', 'g?', '', mk_opts(M.do_toggle_help_page))
  -- todo modes to open/close nodes or move in buffer for keys h l
  map(bufnr, 'n', 'h', '', mk_opts(M.do_node_close))
  map(bufnr, 'n', 'l', '', mk_opts(M.do_node_open))

  -- change value
  map(bufnr, 'n', 'i', '', mk_opts(M.do_change_value))
  map(bufnr, 'n', 's', '', mk_opts(M.do_change_value))
  map(bufnr, 'n', 'a', '', mk_opts(M.do_nothing))
  map(bufnr, 'n', 'p', '', mk_opts(M.do_nothing))

  map(bufnr, 'n', 'y', '', mk_opts(M.do_copy_name))
  map(bufnr, 'n', 'Y', '', mk_opts(M.do_copy_path))
  map(bufnr, 'n', 'c', '', mk_opts(M.do_copy_value))

  M.bind_state(ctx.state, bufnr)
  M.draw2buf(ctx.state, bufnr)
  return ctx.state, bufnr
end

M.help_logo = '====  Table-Viewer Help ===='
function M.draw_help(bufnr)
  bufnr = bufnr or 0
  if not M.is_valid_buf(bufnr) then
    return
  end
  local lines = {
    M.help_logo,
    "",
    "  g?         show/hide the help_page",
    "  q          close_and_clean_buf",
    "",
    "  o,<space>  toggle_open_close",
    "  h          node_close))",
    "  l          node_open",
    "",
    "  y          copy_name",
    "  Y          copy_path",
    "  c          copy_value",
    "",
    "  i          change_value",
    -- TODO a add key-value
  }
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, lines)
end

---@private
function M.bind_state(state, bufnr)
  assert(type(state) == 'table', 'state')
  assert(type(bufnr) == 'number', 'bufnr')
  -- table with state
  vim.api.nvim_buf_set_var(bufnr, 'state', true) --ctx.state)
  M.tables[bufnr] = state
  return state
end

function M.is_valid_buf(bufnr)
  local ok, _ = pcall(vim.api.nvim_buf_get_var, bufnr, 'state')
  if not ok then
    log.debug('Stop! Not found state in buff: %s', bufnr)
    return false
  end
  return true
end

function M.do_close_and_clean_buf()
  log.debug("close_and_clean_buf")
  local bufnr = vim.api.nvim_get_current_buf()
  if bufnr then
    local was = M.tables[bufnr]
    M.tables[bufnr] = nil
    if was then
      log.debug('Cleaned for bufnr', bufnr)
    end
  end
  -- standart cmd bdelete - can close nvim
  -- Bdelete cmd from moll/vim-bbye
  if vim then
    vim.cmd(':EnvBufClose') -- '<cmd>Bdelete<cr>',
  end
end

--------------------------------------------------------------------------------
--                          Actions Helpers
--------------------------------------------------------------------------------
--

---@param bufnr number
function M.get_state(bufnr)
  assert(type(bufnr) == 'number', 'bufnr')

  local o = M.tables[bufnr]
  if not o then
    log.debug('Not found table state for buf: %s', bufnr)
    -- error('no state in buf: ' .. tostring(bufnr))
    return
  end
  return o
end

---@return number?, table?, table?, table?  -- bufnr, state, cwd, relative-path2node
function M.get_node_under_cursor()
  local bufnr = vim.api.nvim_get_current_buf()
  log.debug("get_node_under_cursor bufnr: %s", bufnr)

  if not M.is_valid_buf(bufnr) then
    return
  end

  local o = M.get_state(bufnr)
  if not o then return end

  local cursor_pos, lnum = vim.api.nvim_win_get_cursor(0), 0
  if cursor_pos then
    lnum = cursor_pos[1]
  end
  if type(lnum) ~= 'number' then
    log.debug('No lnum: %s', lnum)
    return
  end
  -- not needed to convert from nvim indexes from 0 here lnum starts from 1
  lnum = lnum

  -- relative from cwd path to node under the cursor
  local rel_path = (o.mappings or E)[lnum] -- line-num --> relpath from cwd
  if not rel_path then
    log.debug('Not Found relative path to node for lnum: %s', lnum)
    return
  end

  o.path = o.path or {} -- "cwd path" is a path from o.root to o.cwd

  -- take cwd of the o.cwd(the current "work dir" - opened subtable)
  local opened_cwd = M.get_opened_for(o, o.path)

  return bufnr, o, opened_cwd, rel_path
end

local function node_close(cwd, rel_path)
  local parent = M.get_parent(cwd, rel_path)
  local node = (parent or E)[rel_path[#rel_path]]
  if node then
    node.__closed = true -- = nil -- close
  else
    error('empty node in try to toggle to close')
  end
end

local function node_open(o, rel_path)
  local opened = M.get_opened_for(o, rel_path) -- open
  if opened then
    opened.__closed = false
  else
    error('empty node in try to toggle to open')
  end
end

--------------------------------------------------------------------------------
--                             Actions
--------------------------------------------------------------------------------

--
-- callback to keybinding
--
function M.do_toggle_open_close()
  log.debug('toggle open/close bufnr:')
  local bufnr, o, ocwd, rel_path = M.get_node_under_cursor()
  if not o or not ocwd or not rel_path then return end

  ---- do open/close toggle ----

  if M.is_opened(ocwd, rel_path) then
    node_close(ocwd, rel_path)
  else
    node_open(o, rel_path)
  end

  M.draw2buf(o, bufnr)
  return true
end

function M.do_node_open()
  local bufnr, o, ocwd, rel_path = M.get_node_under_cursor()
  if not o or not ocwd or not rel_path then return end
  if not M.is_opened(ocwd, rel_path) then
    print('open')
    node_open(o, rel_path)
    M.draw2buf(o, bufnr)
  end
end

function M.do_node_close()
  local bufnr, o, ocwd, rel_path = M.get_node_under_cursor()
  if not o or not ocwd or not rel_path then return end
  if M.is_opened(ocwd, rel_path) then
    print('close')
    node_close(ocwd, rel_path)
    M.draw2buf(o, bufnr)
  else
    -- todo jump to line with parent node
  end
end

function M.do_toggle_help_page()
  local bufnr = vim.api.nvim_get_current_buf()
  local ok, val = pcall(vim.api.nvim_buf_get_var, bufnr, 'help_page')
  if ok and val == true then
    vim.api.nvim_buf_set_var(bufnr, 'help_page', nil)
    M.draw2buf(M.get_state(bufnr), bufnr)
  else
    vim.api.nvim_buf_set_var(bufnr, 'help_page', true)
    M.draw_help(bufnr)
  end
end

---@param val any
local function copy2cb(name, val)
  name = name or ''
  if type(val) == 'table' then
    val = inspect(val)
  else
    val = tostring(val)
  end
  clipboard.copy_to_clipboard(val)
  if #val > 60 then
    val = val:sub(1, 60) .. '...'
  end
  if val:find("\n") then
    val = val:gsub("\n", ' ')
  end
  print('Copied to system clipboard ' .. name .. ': ' .. val)
end

function M.do_copy_name()
  local _, o, _, rel_path = M.get_node_under_cursor()
  if o and rel_path then
    copy2cb('keyname', rel_path[#rel_path])
  end
end

---@param o table
---@param rel_path table|nil
function M.get_absolute_path_s(o, rel_path, sep)
  assert(type(o) == 'table', 'o')
  assert(not rel_path or type(rel_path) == 'table', 'rel_path')
  sep = sep or ' '

  local path = ''
  if sep == '/' then path = sep end -- ?

  if o.path and #o.path > 0 then
    path = path .. table.concat(o.path, sep)
  end

  if rel_path and #rel_path > 0 then
    if #path > 0 and path:sub(-1, -1) ~= sep then
      path = path .. sep
    end

    path = path .. table.concat(rel_path, sep)
  end
  return path
end

function M.do_copy_path()
  local _, o, _, rel_path = M.get_node_under_cursor()
  if o and rel_path then
    copy2cb('path', M.get_absolute_path_s(o, rel_path))
  end
end

function M.do_copy_value()
  local _, o, _, rel_path = M.get_node_under_cursor()
  if o and rel_path then
    copy2cb('value', M.get_value_in_path(o, rel_path))
  end
end

-- edit value of the table key under the cursor
function M.do_change_value()
  local bufnr, o, ocwd, rel_path = M.get_node_under_cursor()
  if o and ocwd and rel_path then
    local val = M.get_value_in_path(o, rel_path)
    local vt = type(val)

    if vt == 'table' then
      print('Not implement yet. TODO edit table-value in the new buffer')
      return
      -- change value via vim.input
    else
      local completion, new_val = nil, nil
      local path = M.get_absolute_path_s(o, rel_path, '/')
      path = path or rel_path[#rel_path] or '?'

      val = tostring(val)
      new_val = ui.ask_value('Change value of ' .. path .. ': ', val, completion)
      if new_val and new_val ~= val then
        local key = rel_path[#rel_path]
        local parent = M.get_parent(o.cwd, rel_path)
        if not parent or not key then
          print('Cannot find parent of the key: ' .. tostring(key))
        else
          if vt == 'number' then
            new_val = tonumber(new_val)
          elseif vt == 'boolean' then
            new_val = new_val == 'true'
          end
          if new_val == nil then
            print('Cannot convert string to ' .. vt)
            return
          end
          parent[key] = new_val
          -- TODO redraw only one updated line
          M.draw2buf(o, bufnr)
        end
      end
    end
  end
end

function M.do_change_key()
  local _, o, ocwd, rel_path = M.get_node_under_cursor()
  if o and ocwd and rel_path then
    error('Not implement yet')
  end
end

function M.do_nothing()
end

--
-- get the viewed object-table by bufnr
--
---@param bufnr number|nil
function M.get_current_state(bufnr)
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  log.debug("get_current_state bufnr:", bufnr)

  if M.is_valid_buf(bufnr) then
    return M.get_state(bufnr), bufnr
  end
  return nil, nil
end

-- open
---@param o table
---@param path table -- sequence of keys to open the subtable
function M.open_path(o, bufnr, path)
  if o then
    local opened = M.get_opened_for(o, path)
    log.debug("has opened:", opened ~= nil)
    if opened then
      opened.__closed = false
      if bufnr then
        M.draw2buf(o, bufnr)
      end
    end
  end
end

---@param o table
function M.grep(o, s)
  if not o then
    return false, 'not state'
  end
  if not s or s == '' then
    return false, 'expected string to search got:|' .. tonumber(s) .. '|'
  end
  if type(o.root) ~= 'table' then
    return false, 'expected table root got:' .. type(o.root)
  end

  ---@param t table
  ---@param path string
  ---@param out table
  local function grep0(t, path, out)
    for k, v in pairs(t) do
      local typ = type(v)
      if typ == 'table' then
        grep0(v, path .. ' ' .. tostring(k), out)
      elseif typ == 'string' then
        local ps = string.find(v, s, 1, true) -- start position
        if ps then
          local line
          local sp0 = math.max(1, ps - 20)
          if ps == 1 and #v <= 40 then
            line = ' :' .. ' |' .. v .. '|'
          else
            local pe = math.min(ps + 20, #v) -- end position
            local pref, suff = '|', '|'

            if pe < #v then
              suff = '..|(' .. pe .. '/' .. #v .. ')'
            end
            if sp0 < ps and sp0 > 1 then
              pref = '(' .. sp0 .. ')|..'
            end
            line = ' : ' .. pref .. string.sub(v, sp0, pe) .. suff
          end
          out[#out + 1] = path .. ' ' .. tostring(k) .. line
        end
      end
    end
  end

  local res = {}
  grep0(o.root, '', res)

  return res, nil
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------


function M.draw2buf(o, bufnr)
  log.debug("draw2buf ", bufnr)
  assert(type(o) == 'table', 'o table state has:' .. tostring(o))
  bufnr = bufnr or 0
  if not M.is_valid_buf(bufnr) then
    return
  end

  local lines = M.render_lines(o)

  log.debug('renderd to lines', #(lines or E))
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, lines)
end

--
--
function M.get_opened_for(o, path)
  assert(type(o) == 'table', 'o')
  assert(type(path) == 'table', 'path')
  o.opened = o.opened or {}

  local cwd = o.opened
  for i = 1, #path do
    local key = path[i]
    if not cwd[key] then
      cwd[key] = {}
    end
    cwd = cwd[key]
  end
  -- one opened
  return cwd
end

-- get subtable(or value) content by relative or absolute(/) "path"
-- secuensec of keys specified in the path(list of the keys)
function M.get_value_in_path(o, path)
  assert(type(o) == 'table', 'o')
  assert(type(path) == 'table', 'path')

  local cwd = o.cwd
  local start = 1
  if path[1] == '/' then
    cwd = o.root
    start = 2
  end
  for i = start, #path do
    local key = path[i]
    if not cwd[key] then
      log.debug('Not found key:%s at path-i: %s', key, i)
      return nil
    end
    cwd = cwd[key]
  end
  -- one opened
  return cwd
end

---@param cwd table -- subtable in the o.opened
function M.is_opened(cwd, rel_path)
  assert(type(cwd) == 'table', 'o')
  assert(type(rel_path) == 'table', 'rel-path')
  for i = 1, #rel_path do
    local key = rel_path[i]
    if not cwd[key] then
      return false
    end
    cwd = cwd[key]
  end
  return cwd ~= nil and not cwd.__closed
  -- return true
end

-- is relative-path exist on the path from cwd.point then return it parent
function M.get_parent(cwd, rel_path)
  assert(type(cwd) == 'table', 'o')
  assert(type(rel_path) == 'table', 'rel_path')
  local parent = cwd
  for i = 1, #rel_path do
    local key = rel_path[i]
    if not cwd[key] then
      return nil
    end
    parent = cwd
    -- name = key
    cwd = cwd[key]
  end
  return parent --, name
end

-- sort keys in "dir like order
function M.sort_keys(tbl)
  assert(type(tbl) == 'table', 'tbl')
  local max_dir, max_file = 0, 0

  -- string keys with table values ("dirs" names)
  local tsk = {} -- key list
  for k, v in pairs(tbl) do
    local kt, vt = type(k), type(v)
    if vt == 'table' and kt ~= 'number' then
      tsk[#tsk + 1] = k
      if kt == 'string' then
        if #k > max_dir then max_dir = #k end
      end
    end
  end
  -- sort by string value
  table.sort(tsk, function(a, b)
    return a:upper() < b:upper()
  end)

  -- number keys with table values ("dirs" names)
  local tnk = {}
  for k, v in pairs(tbl) do
    local kt, vt = type(k), type(v)
    if vt == 'table' and kt == 'number' then
      tnk[#tnk + 1] = k
      local ks = tostring(k) -- todo get num len without tostring
      if #ks > max_dir then max_dir = #ks end
    end
  end
  -- sort by number value
  table.sort(tnk, function(a, b)
    return a < b
  end)

  -- string keys without table ("files")
  local tskv = {}
  for k, v in pairs(tbl) do
    local kt, vt = type(k), type(v)
    if vt ~= 'table' and kt ~= 'number' then
      tskv[#tskv + 1] = k
      if kt == 'string' then
        if #k > max_file then max_file = #k end
      end
    end
  end
  table.sort(tskv, function(a, b)
    return a:upper() < b:upper()
  end)

  -- number keys without table ("files")
  local tnkv = {}
  for k, v in pairs(tbl) do
    local kt, vt = type(k), type(v)
    if vt ~= 'table' and kt == 'number' then
      tnkv[#tnkv + 1] = k
      local ks = tostring(k)
      if #ks > max_file then max_file = #ks end
    end
  end

  table.sort(tskv, function(a, b)
    return a < b
  end)

  local t = {} -- sorted keys
  for i = 1, #tsk do t[#t + 1] = tsk[i] end
  for i = 1, #tnk do t[#t + 1] = tnk[i] end
  for i = 1, #tskv do t[#t + 1] = tskv[i] end
  for i = 1, #tnkv do t[#t + 1] = tnkv[i] end

  return t, max_dir, max_file
end

---@param o table {root, cwd, path, src}
function M.render_lines(o)
  log.debug("render_lines")
  assert(type(o) == 'table', 'o table state')

  if type(o.cwd) ~= 'table' then
    return { tostring(o.cwd) }
  end
  --

  o.draw = {}
  o.draw.lines = {}
  o.draw.info = {}
  o.mappings = {} -- line number to path from cwd
  o.opened = o.opened or {}
  local max_len, max2 = 0, 0

  local node_name_cb = nil
  if type(o.node_name_cb) == 'function' then
    node_name_cb = o.node_name_cb
  end
  local do_sort_keys = nil
  if type(o.sort_keys_cb) == 'function' then
    do_sort_keys = o.sort_keys_cb
  else
    do_sort_keys = M.sort_keys
  end
  --
  local function render0(depth, enter_path)
    local cwd = M.get_value_in_path(o, enter_path)
    if cwd == nil then return 0 end

    local tabn, tab = (depth * 4 - 2), ''
    if tabn > 0 then
      tab = string.format("%-" .. tabn .. "s", ' ')
    end
    ---@diagnostic disable-next-line: unused-local
    local sorted_keys, max_dir, max_file = do_sort_keys(cwd)

    -- for k, v in pairs(cwd) do -- cwd
    for i = 1, #sorted_keys do
      local k = sorted_keys[i]
      local v = cwd[k]

      local c_path = tu.deep_copy(enter_path)
      c_path[#c_path + 1] = k
      local n = #o.draw.lines + 1
      o.mappings[n] = c_path

      local t0, is_opened, line = type(v), false, nil
      if t0 == 'table' then
        local sign
        is_opened = M.is_opened(o.opened, c_path)
        if is_opened then sign = '-' else sign = '+' end
        local dirname = tostring(k)
        line = tab .. sign .. ' ' .. '[' .. dirname .. ']'
        if node_name_cb then
          local dname = node_name_cb(v, max_dir - #dirname)
          if dname then
            line = line .. ' ' .. dname
          end
        end
      else
        line = M.build_line0(tab, k, v, max_file)
      end
      -- local line = tab .. sign .. ' ' .. q .. tostring(k) .. p
      local info = ' (' .. t0 .. ')' -- coment
      if #line > max_len then max_len = #line end
      if #info > max2 then max2 = #info end
      o.draw.lines[n], o.draw.info[n] = line, info

      if is_opened then
        render0(depth + 1, c_path)
      end
    end
  end

  render0(0, {})
  --
  -- stage 2 render to text

  -- build lines to output
  local t = {} -- lines for text output
  for i, line in ipairs(o.draw.lines) do
    -- local rem = M.LINE_WIDTH - (max_len + 3) - #o.draw.info[i]
    local rem = M.LINE_WIDTH - #line - max2 --#o.draw.info[i]
    -- local rem = (max_len + 3) - #line
    rem = rem > 0 and rem or 0
    local pad = string.format("%-" .. (rem) .. "s", ' ')
    t[#t + 1] = line .. pad .. o.draw.info[i]
  end

  return t
end

-- leaf of the tree with value (file)
function M.build_line0(tab, key, v, max_file)
  tab = tab or ''

  local vtype, skey, sval, pad, q, free_space, rem

  vtype = type(v)
  free_space = M.LINE_WIDTH - (#tab + 2) - max_file - 3 - #vtype - 4
  if vtype ~= 'string' then
    v = tostring(v)
    q = ''
  else
    q = "'"
  end
  skey, sval = tostring(key), v:sub(1, math.min(free_space, #v))
  if skey:find("\n", 1) then skey = skey:gsub("\n", '\\n') end
  if sval:find("\n", 1) then sval = sval:gsub("\n", '\\n') end

  rem = (max_file + 2) - #skey --line
  if rem > 0 then
    pad = string.format("%-" .. (rem) .. "s", ' ')
  else
    pad = ''
  end
  -- print('key:', key, 'rem:', rem, 'max_file:', max_file, 'tab:', #tab)
  return tab .. '  ' .. skey .. pad .. ' : ' .. q .. sval .. q
  --             ^ space for signs for dirs(table values)
end

return M

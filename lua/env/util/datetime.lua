-- 26-08-2024 @author Swarg
local M = {}
--
local def_datetime_format = "%Y-%m-%dT%H:%M:%S.000Z"
local def_datetime_pattern = "^(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)%.(%d+)Z"
local uni_datetime_pattern = "^%s*(%d+)%-(%d+)%-(%d+)%s+(%d+):(%d+):(%d+)"
local uni_date_pattern = "^%s*(%d+)%-(%d+)%-(%d+)"

---@param ts number
---@param format string?
---@return string
function M.timestamp2readable(ts, format)
  format = format or def_datetime_format
  return tostring(os.date(format, ts))
end

-- parse string to timestamp
---@param datetime string
---@param pattern string?
---@return number?
function M.datetime2timestamp(datetime, pattern)
  pattern = pattern or def_datetime_pattern
  local y, m, d, h, min, s, _ = string.match(datetime, pattern)
  if not y or not m or not h then
    y, m, d, h, min, s, _ = string.match(datetime, uni_datetime_pattern)
  end
  if not y or not m or not h then
    y, m, d = string.match(datetime, uni_date_pattern)
    h, min, s = 0, 0, 0
  end

  local timestamp = os.time({
    year = y, month = m, day = d, hour = h, min = min, sec = s
  })

  return timestamp
end


function M.gen_random_timestamp(seed, format, min, max)
  format = format or "%Y-%m-%dT%H:%M:%S.000000Z"

  if seed ~= "old" then
    seed = seed or os.time()
    math.randomseed(seed)
  end
  min = min or os.time() - 60*60*24*365*30
  max = max or os.time()

    -- index = index + 1
  local time = math.random(min, max)

  return tostring(os.date(format, time))
end

return M

-- 05-11-2023 @author Swarg
local M = {}

local lsp = (vim or {}).lsp
local log = require('alogger')

M.kinds = {
  'File',          --  1
  'Module',        --  2
  'Namespace',     --  3
  'Package',       --  4
  'Class',         --  5
  'Method',        --  6
  'Property',      --  7
  'Field',         --  8
  'Constructor',   --  9
  'Enum',          -- 10
  'Interface',     -- 11
  'Function',      -- 12
  'Variable',      -- 13
  'Constant',      -- 14
  'String',        -- 15
  'Number',        -- 16
  'Boolean',       -- 17
  'Array',         -- 18
  'Object',        -- 19
  'Key',           -- 20
  'Null',          -- 21
  'EnumMember',    -- 22
  'Struct',        -- 23
  'Event',         -- 24
  'Operator',      -- 25
  'TypeParameter', -- 26
  'Component',     -- 27
  'Fragment',      -- 28
}

local E = {}

-- name to id
---@param kind string|nil
function M.kind2id(kind)
  if type(kind) == 'number' then
    return kind
  elseif type(kind) == 'string' then
    for i, kind0 in pairs(M.kinds) do
      if string.lower(kind0) == string.lower(kind) then
        return i
      end
    end
  end
  return 0
end

function M.postprocess_symbols(response)
  return response
end

-- all methods(names of requests see :h lsp-method)


--
-- Lists all symbols in the current buffer (async)
--
-- command to show this list in quitckfix window:
-- :lua vim.lsp.buf.document_symbol()
--
-- request_symbols(on_symbols)
function M.request_doc_symbols(bufnr, handler)
  bufnr = bufnr or 0
  local params = {
    textDocument = vim.lsp.util.make_text_document_params()
    -- url: file:// ... .lua
  }
  vim.lsp.buf_request_all(
    bufnr,
    'textDocument/documentSymbol', -- method
    params,
    function(response)             -- callback
      handler(M.postprocess_symbols(response))
    end
  )
end

--
-- Lists all the references to the symbol under the cursor.
--
-- Command to show it in the quickfix window:
-- :lua vim.lsp.buf.references()
--
---@param bufnr number
---@param position table|nil to override the current cursor position
function M.request_references(bufnr, position)
  bufnr = bufnr or 0
  local method = 'textDocument/references'
  local params = vim.lsp.util.make_position_params()

  -- override default if defined
  if position and position.line then -- lnum
    params.position.line = position.line
    if position.character then       -- col
      params.position.character = position.character
    end
  end
  params.context = { includeDeclaration = true }

  -- Sends a request to all server and waits for the response of all of them.
  -- vim.lsp.buf_request_sync(bufnr, method, params, timeout_ms)
  -- ret: map client_id:request_result or nil, err
  local response = lsp.buf_request_sync(bufnr, method, params, 2000)

  -- if response == nil or tbl_isempty(response) then
  --   print('No location found: ' .. method)
  --   return
  -- end
  return response
end

local function get_line0(t, def)
  if t and type(t.line) == 'number' then
    return t.line + 1
  end
  return def or '0'
end

--
-- make a node name of the given table: (readable for tviewer)
--   symbol
--   range
---@param t table
function M.mk_node_name(t)
  if not t then
    return ''
  end
  -- symbol
  if t.kind then
    local skind = M.kinds[t.kind] or '?'
    local line = ''
    if type(t.range) == 'table' then
      line = get_line0(t.range.start) .. ':' .. get_line0(t.range['end'])
    end
    return string.format("%-10s %-7s %s", skind, line, tostring(t.name))
    --
  elseif type(t.range) == 'table' and t.range.start then
    return get_line0(t.range.start) .. ':' .. get_line0(t.range['end'])
  end
end

--
-- filter symbols by kind
--
---@param response table -- nvim-lsp response
---@param kind number|nil
function M.get_root_symbols(response, lnum, kind)
  log.debug("get_root_symbols lnum: %s kind: %s", lnum, kind)
  local res = {}
  ---@diagnostic disable-next-line: unused-local
  for client_id, t in pairs(response) do
    local result = t.result or E
    for _, symbol in pairs(result) do
      if symbol.range and (not kind or kind == symbol.kind) then
        local start = (symbol.range.start or E).line or math.huge
        local lend = (symbol.range['end'] or E).line or math.huge
        if lnum >= start and lnum <= lend then
          res[#res + 1] = symbol
        end
      end
    end
  end
  return res
end

--
-- request to lsp
--
---@param bufnr number
---@param cursor table {1 lnum, 2 - col}
---@param handler function
function M.symbol_under_cursor(bufnr, cursor, handler)
  assert(type(cursor) == 'table', 'cursor')
  bufnr = bufnr or 0

  M.request_doc_symbols(bufnr, function(response)
    assert(type(response) == 'table', 'response')
    -- find function range from nvim-lsp
    ---@diagnostic disable-next-line: unused-local
    local lnum, col = cursor[1] - 1, cursor[2]
    local res = M.get_root_symbols(response, lnum)
    handler(res, response)
  end)
end

-- lsp request
--
---@param bufnr number?
---@param cursor table
---@param kind string|nil
function M.find_block_range(bufnr, cursor, kind, handler)
  assert(type(cursor) == 'table', 'cursor')
  bufnr = bufnr or 0
  log.debug('find_block_range %s %s %s', bufnr, cursor, kind)

  M.request_doc_symbols(bufnr, function(response)
    assert(type(response) == 'table', 'response')
    local lnum = cursor[1] - 1
    local kindn = M.kind2id(kind)
    local res = M.get_root_symbols(response, lnum, kindn)

    -- mk lnum(start) and lnum_end
    if res and res[1] then
      local r = res[1].range or res[1].selectionRange
      if r then
        log.debug('range: %s', r)
        res.lnum = (r.start or E).line
        res.lnum_end = (r['end'] or E).line
      end
    end

    handler(res, response)
  end)
end

local function index_of(t, n)
  if type(t) == 'table' then
    for index, v in ipairs(t) do
      if v == n then
        return index
      end
    end
  end
  return -1
end

---@param bufnr number
---@param lsp_name string
---@return false|string   lsp-name
---@return string? errmsg
function M.stop_lsp(bufnr, lsp_name)
  local opts = { name = lsp_name, bufnr = bufnr }
  local ok, clients = pcall(vim.lsp.get_active_clients, opts)
  if not ok then
    return false, 'cannot get acive lsp.clients'
  end

  for _, e in ipairs(clients) do
    if type(e) == 'table' then
      -- print("[DEBUG] e:", require "inspect" (e))
      local buffs = e.attached_buffers or E
      -- local ws = (e.workspaceFolders or E)[1] or E
      -- print(i, e.name, ws.name, ws.uri, log.format("buffs: %s", buffs))

      if index_of(buffs, bufnr) then
        if type(e.stop) == 'function' then
          e.stop()
          return e.name
        end
      end
    end
  end
  return false, 'not found lsp with buffer ' .. tostring(bufnr)
end

---@param lsp_name string ("jdtls")
---@param bufnr number
function M.wait_to_stop(bufnr, lsp_name, timeout_ms)
  timeout_ms = timeout_ms or 30000
  local opts = { name = lsp_name, bufnr = bufnr }
  vim.wait(timeout_ms, function()
    return next(vim.lsp.get_active_clients(opts)) == nil
  end)
end

---@param message string
function M.echohl(message)
  if message then
    vim.api.nvim_command(':echohl Function | echo "' ..
      string.sub(message, 1, vim.v.echospace) .. '" | echohl None')
  end
end

return M

--[[
:h lsp

To learn what capabilities are available you can run the following command in
a buffer with a started LSP client:

:lua =vim.lsp.get_active_clients()[1].server_capabilities
]]

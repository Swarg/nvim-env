-- 28-10-2023 @author Swarg
local M = {}
--
-- tokenizer.parser
--
-- UseCase:
--  - extended cli string parsing

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local D = require("dprint")
local U = require("env.util.tokenizer.util")
local T = require("env.util.tokenizer.devhelpers")
local M_token = require("env.util.tokenizer.token")

local dprint = D.mk_dprint_for(M)
local dvisualize = D.visualize
-- to check is debug-enabled use: D.is_enabled(M)
-- to disable debug output only for this module use: D.enable_module(M, false)

_TEST = _G._TEST
M.DEEP_LIMIT = 4096

local H = {} -- Debugging Helpers dshow

local class = require 'oop.class'
--
--------------------------------------------------------------------------------
--                              Tokenizer
--------------------------------------------------------------------------------

class.package 'parser' -- env.util.tokenizer
--
---@class parser.Tokenizer
--
---@field line string
---@field p_start number
---@field p_end number
---@field root table (token) -- parser.Token
---@field token table (token) -- parser.Token
--
-- Settings
---@field throw_errors   boolean        - stop parsing in input error
---@field tokens_with_id boolean|nil    - for testing
--
M.Tokenizer = class.new_class(nil, 'Tokenizer', {
})

-- constructor
---@return self
function M.Tokenizer:new(obj)
  -- usecase: :new(line)
  if type(obj) == 'string' then
    local line = obj
    obj = {}
    M.Tokenizer.source(obj, line)
  end
  obj = obj or {}
  obj.separators = { ' ', ',', "\n" }

  -- on invalid input
  obj.throw_errors = false

  if _TEST then
    obj.next_token_id = obj.next_token_id or 0
    obj.tokens_with_id = true
  end

  -- usecase: :new({line = line})
  if obj.line and (not obj.pstart or not obj.pend) then
    M.Tokenizer.source(obj, obj.line)
  end

  self.__index = self
  setmetatable(obj, self)
  return obj
end

-- set source to parse
---@param line string
---@param pstart number|nil
---@param pend number|nil
function M.Tokenizer:source(line, pstart, pend)
  assert(type(line) == 'string', 'line must be a string has: ' .. tostring(line))
  assert(not pstart or type(pstart) == 'number', 'pstart')
  assert(not pend or type(pend) == 'number', 'pend')

  self.line = line
  self.pstart = pstart or 1
  self.pend = pend or #line

  return self
end

---@param enable boolean|nil - by default set to true(throw errors)
function M.Tokenizer:setThrowErrors(enable)
  if enable == nil then enable = true end
  self.throw_errors = enable
  return self
end

---@param separators table<string>
function M.Tokenizer:set_separators(separators)
  assert(type(separators) == 'table' and #separators > 0, 'separators')
  self.separators = separators -- or {' ', ','}
  return self
end

--
-- id are added to token only in testing env  (tokens_with_id)
function M.Tokenizer:_next_token_id()
  self.next_token_id = self.next_token_id + 1
  return self.next_token_id
end

--
-- Create new opened token only with start position(pe) and parent (branch)
-- This token will be added to tree(parent) only on stage "on token_close"
-- This is done in order not to add invalid - empty tokens to the tree(parent).
--
-- this token is a leaf (not contains field "open" with char of group
--
-- token-leaf is a token-value by design it cannot containes any childrens
-- but it can be turned into token-branch (mk_branch) when in parsing text
-- starts a subgroup
--
-- a token-branch is a token-container is intended to contain at least one
-- token-leaf (a child)
--
-- ps - start position in the line
-- pe - end position
--
---@param parent table|false|nil    -- token to which links a this one
--                                     use nil or false to create a root token
---@param start_pos number|nil      -- nil used only for root token
-- (It is required to define the starting position for all child tokens)
--
-- Extra args to use in testing:
---@param end_pos number|nil     by design starts with nil
---@param open string|nil        char of current opened group(default - nil)
function M.Tokenizer:new_token(parent, start_pos, end_pos, open)
  parent = parent or false
  local token = { ps = start_pos, pe = nil, parent = parent }
  ---- for testing
  if end_pos then
    token.pe = end_pos
  end
  token.open = open

  if self.tokens_with_id then
    token.id = self:_next_token_id()
    self:dprint_token('[NEW] token', token, 1)
  end

  ----
  -- by design linking to the parent happens only in token_close() via:
  -- parent[#parent + 1] = token
  return token
end

--
-- Linking to the tree & set the end position of the token(Optional)
--
-- Complite(Close) already opened token
-- add the given token to the list of childs in the parent
--
-- the added token must have a ps field, otherwise will be thrown error
--
---@param token table
---@param p_end number|nil
---@return table -- token
function M.Tokenizer:token_close(token, p_end)
  self:dprint_token('token_close', token, 1)

  assert(type(token) == 'table', 'token')
  assert(type(token.parent) == 'table', 'token.parent has: ' .. tostring(token.parent))
  assert(not p_end or type(p_end) == 'number', 'p_end must be nil or number')

  if p_end and token.pe ~= p_end then
    -- not update the single empty token - a child of root
    if not (token.parent == self.root and token.ps == 0 and token.open == '') then
      token.pe = p_end
      self:dprint_token('     update', token)
    end
  end
  local ps, pe, e = token.ps, token.pe, token.open == ''

  assert(ps ~= nil and (ps > 0 or e), 'validate token.ps has:' .. tostring(ps))
  assert(pe ~= nil and (pe > 0 or e), 'validate token.pe has:' ..
    tostring(pe) .. ' pend:' .. tostring(p_end))
  assert(ps <= pe, 'validate range ps<=pe ' .. tostring(ps) .. ' ' .. tostring(pe))

  if M_token.token_get_link_index(token) then
    self:error(string.format('token already in list! %s',
      T.token_info(token, self.line)))
  end

  -- link to the parent
  token.parent[#token.parent + 1] = token

  self:dshow('linked(closed) token', token)

  -- is key-value-entry then check to needs colse token-key
  -- if token.parent.open == '=' then
  if token.parent.entry_key then
    local token_key, token_value = token.parent, token
    dprint('kv-entry is detected in token_value id:', token_value.id)

    if not M_token.token_get_link_index(token_key) then
      dprint('try to close unlinked token_key id:', token_key.id)
      return self:token_close(token_key)
    end

    return token_key -- instead of token_value
  end

  return token
end

--
-- close current already opened token, add it to the parent(tree), and
-- create a new token for the next content if not last char in line
-- if "last==true" and self.token is empty - discard it and
-- select from the same parent of discarded token the last added token
-- (select previos "word" in the same context level
--  - to keep in cursor valid token before exit)
--
function M.Tokenizer:token_ends(i)
  dprint("\n|---- Token Ends ---->>", i)

  if not self.token.ps or self.token.parent == false then
    self:dprint_token('broken-token: ', self.token)
    self:error('Something went wrong. Did the root get here?')
  end

  self.token.pe = i

  if self.token.pe >= self.token.ps then
    local prev_token = self:token_close(self.token, i)
    self.token = self:new_token(prev_token.parent, i + 1 + 1) -- i + sep_len + 1
  else
    -- Skip empty token (UseCase: trim spaces)
    -- instead of adding an empty token, changes its start position
    self.token.ps = i + 1 + 1 -- i + sep_len + 1
    self:dshow('Skip empty token via update token.ps (trim)', self.token)
  end

  dprint("<<---- Token Ends ----|\n")
end

-- case: 'abc)de' --  when has group-close char without openeing
-- keep this char in the separate token
-- showed as one string but but under the hood these are two different tokens
function M.Tokenizer:add_single_char(i, c)
  dprint("add_single_char i:", i, 'c:', c)
  self:dprint_token('add_single_char self.token:', self.token)
  -- closing previou token that may have some range
  self.token.pe = i - 1
  if self.token.pe >= self.token.ps then
    local prev_token = self:token_close(self.token)
    self.token = self:new_token(prev_token.parent, i)
  end
  -- close one-char-token and starts a new one
  self.token.ps = i
  local prev_token = self:token_close(self.token, i)
  self.token = self:new_token(prev_token.parent, i + 1)
end

--
-- Extract the chars before the upcomming group into a separate token
-- when there is no separator between a sequence of character and the group
--
-- extra'ab cd'  split
-- *['1']        (busted)
--
function M.Tokenizer:check_extra_chars(i, c, prev_c)
  dprint('check_extra_chars')
  local length = i - (self.token.ps or 0)
  if length > 0 then
    --
    -- to supports the busted output: "{ *[1] = {...} }"
    if c == '[' and prev_c == '*' and self.close == '}' and length == 1 then
      self.token.mark = '*' -- for future use
      self.token.ps = i
      dprint('current token marked with *', self.token.id)
      return
    end

    -- a mark indicating that the token contains a simple value (string?)
    self.token.open = ''
    local token0 = self.token

    local prev_token = self:token_close(self.token, i - 1)
    self.token = self:new_token(prev_token.parent, i)

    self:dshow('add token from extra chars', token0, 'next group:', c)
  end
end

local function token_is_key_has_linked_value(tk)
  if tk and tk.entry_key and tk[1] ~= nil then
    return type(tk[1]) == 'table' and tk[1].parent == tk
  end
end
--
-- 'key="value"'
-- 'key = "value"'
-- TODO:
-- ['key'] = "value"'
--
-- key is a container for value
-- key = 'value'
function M.Tokenizer:open_kv_entry(i)
  dprint("open_kv_entry i:", i)
  local token_key, token_value

  -- case: 'key="value"'
  --           ^
  -- set current token as token_key + create new token_value for this token_key
  if self.token.ps < i then
    dprint("make token_key from the current token id:#", self.token.id)
    token_key = self.token

    if not self:is_token_key_valid(token_key) then -- case:  "0='value'"
      return i
    end

    self:token_close(token_key, i - 1) -- i - 1 --  without '='
    --?? token_key.entry_key = true -- mark as entry key (container for value)

    -- link token_value -> token_key, but not token_key -> token_value(on close)
    token_value = self:new_token(token_key, i + 1) -- sep len for '=' is 1

    -- case:  'key' '=' '"value"'
    --               ^
    -- find already linked(added) to the tree token_key for current token_value
  elseif self.token.ps == i then
    token_value = self.token
    token_value.ps = i + 1 -- next after '='
    self:dshow('try to open kv-entry for token-value', token_value)
    token_key = self:find_token_key_for(token_value)
  else -- ps > i
    self:error('token.ps:' .. tostring(self.token.ps) .. ' i:' .. tostring(i))
  end

  -- case: "key = 'value' = 'value2'"
  if token_is_key_has_linked_value(token_key) then
    self:dprint_token('(existed) token-key', token_key)
    self:dprint_token('(existed) token-value', token_key[1])
    self:error('[Incorrect Input]. This token_key already has linked token-value')
    token_value.ps = i --?
    return i
  end

  if not self:is_token_key_valid(token_key) then
    dprint('Not valid. Can\'t be a key of kv-entry) id:#', token_key.id)
    token_key.entry_key = false
    token_value.ps = i
    return i
  end

  -- case: {[1] = 'value'}
  if token_key.open == '[' then
    token_key = self:convert_token_array_to_token_key(token_key)
  end

  token_key.entry_key = true
  token_value.parent = token_key
  -- linking will occur on closing token_value:  token_key[1] = token_value
  self:dshow('token-key(updated)', token_key)
  self:dshow('token-value', token_value)

  self.token = token_value
  return i -- for sep longest than one
end

--
--
---@private
function M.Tokenizer:is_token_key_valid(token_key)
  assert(type(token_key) == 'table', 'token_key')

  if not token_key.open or token_key.open == '' then
    if self.line and token_key.ps then
      if self.line:sub(token_key.ps, token_key.ps):match('%d') then
        self:error('[Incorrect Input] Prev token - is a number. Skip kv-entry')
        return false
      end
    end
  end

  if token_key.open == '{' then -- case:   { {1} = 'value'}
    self:error('[Incorrect Input] Prev token - is a table. Skip kv-entry')
    return false
  end

  -- case: {['key'] = 'value'}  convert already linked array into value of key
  if token_key.open == '[' then
    if not token_key[1] then -- first element from the array - the value of key
      -- assert(type(tk) == 'table', 'token-element-1 in [] has:' .. tostring(tk))
      self:error('[Incorrect Input] Empty array without keyname. Skip kv-entry')
      return false
    end
  end
  -- elseif U.is_quotes(token_key.open) then
  --   TODO "key": "value"
  --   error('Quoted keys not supported in lua')

  return true
end

--
-- case: {[1] = 'value'}
--
-- then token_key is a already existed token of string-array with one element
-- action: remove token_array by his first-element with relinking tree
--
--   relinkng:  tree->array->element  >>  tree->element
--   here the element will be token_key, array - is a removed token
--
function M.Tokenizer:convert_token_array_to_token_key(token_key)
  if token_key[1] then -- first element from the array - the value of key
    -- relink tokens in the tree:  tree->array->element >> tree->element
    token_key[1].parent = token_key.parent
    local idx = M_token.token_get_link_index(token_key)
    if idx then
      token_key[1].parent[idx] = token_key[1]
    else
      token_key[1].parent[#token_key[1].parent] = token_key[1]
      self:error('string-array with key of kv-entry not linked to the parent.')
    end
    token_key = token_key[1]
  end
  assert(type(token_key) == 'table', 'found token_key')

  return token_key
end

--
function M.Tokenizer:find_token_key_for(token_value)
  local token_key
  -- get previos token - is a token_key of kv-entry
  if token_value.parent then
    if token_value.parent.entry_key then
      -- case key== value -- wrong input with dup equals
      token_key = token_value.parent
    else
      -- pick from the context of the same parent
      -- case when token_key was already added to tree 'key = value'
      token_key = token_value.parent[#token_value.parent]
    end
  end

  assert(type(token_key) == 'table',
    'extract valid token_key from same parent has:' .. tostring(token_key))
  return token_key
end

--
-- add token with string value in the quotes
--
-- runs when a free quote character appears;
-- this function aimed to find a position with a closing quote
-- and add a token based on the found position
--
---@param i number
---@param c string " or ' design only for quotes
function M.Tokenizer:add_string_token(i, c)
  self:dshow('add_string_token i', i, 'c:', c, 'open:', self.token.open, 'close:', self.close)
  -- Find the end-position of the quote-char
  local new_i = U.indexof_unescaped_char(self.line, i + 1, self.pend, c, self.pend)
  self.token.open = c -- ' "  for specify the string type

  local prev_token = self:token_close(self.token, new_i)
  self.token = self:new_token(prev_token.parent, new_i + 1)

  return new_i
end

-- turn the current token(leaf) into a token-branch (container|tree)
function M.Tokenizer:mk_branch(i, c)
  assert(self.token.ps == i, 'token.ps == i has: ' .. tostring(i)) -- ??
  -- a new child itself contains the open-char of the selt group
  self.token.open = c
  self.close = U.get_pair(self.token.open) -- c
  return self.token
end

--
-- create a subtree to store elements nested in a group like {{}{}}
-- token-branch is a container for elements in this context level
--
-- the following happens here:
--  - "convert" current token to "branch" to store grouped elements(subtokens)
--  - create new token-leaf and link to it parent the converded branch token
--    in this case, the child token itself is not added to the branch list here
--    it was appears only on group_close() token_close() with range validate
--
function M.Tokenizer:open_group(i, c)
  dprint("\n|---- Open_Group ---->>")

  local token_branch = self:mk_branch(i, c)

  -- new child has own parent but this child not yet linked to its parent
  -- bind to parent will be on close_group via token_close()
  local token_leaf = self:new_token(token_branch, i + 1)

  self:dshow('token-group(branch)', token_branch)
  self:dshow('token-elm-of-group(leaf)(new)', token_leaf)

  -- switch to the new context
  self.token = token_leaf
  dprint("<<---- Open_Group ----|\n")
end

-- dev
-- is token valid and that is not a root
local function validate_branch(token_branch)
  assert(type(token_branch) == 'table', 'group conteiner(token_branch)')
  -- assert(not token_branch.entry_key,
  --   'token-key in kv-entry-container must be closed with token-value')

  if token_branch and token_branch.parent == false then -- root   ??????
    dprint(1, 'Something broke')
    error('Closed-group is a Root')
  end
end

--
-- actions:
--   -- close self.token(cursor) - the last element in the group
--   -- close self.token.parent  - token-group-container for an elements
--
function M.Tokenizer:close_group(i)
  dprint("\n|---- Close_Group ---->>")
  -- now self.token is the latest element in the current group (leaf)

  -- pass to tree only valid tokens and discard empty tokens
  if not M_token.token_has_valid_range(self.token, i - 1) then
    dprint('discard empty token-leaf id:', self.token.id)
    self.token.discarded = true

    -- check kv-entry  for case: has token-key but no input for token-value
    -- if self.token.parent and self.token.parent.open == '=' then
    if self.token.parent and self.token.parent.entry_key then
      dprint('discarded token is token-value of kv-entry')
      local token_key = self.token.parent
      token_key.pe = token_key.pe or self.token.ps - 2 -- ?
      if not M_token.token_get_link_index(token_key) then
        self.token = self:token_close(token_key)
        -- now self.token is the one that just closed token-key
      else
        self.token = token_key
      end
    end
  else
    dprint('close token-element-of-group(leaf)')
    self.token = self:token_close(self.token, i - 1) -- link to tree
    -- now self.token is the one that just closed (or token-key for kv-entry)
  end

  dprint('close token-group(branch)')
  -- here self.token.parent is a group that needs be closed
  self.token = self:token_close(self.token.parent, i)
  -- now self.token is the closed group
  validate_branch(self.token) -- make sure is not the root

  -- switch back to the context of the parent of the token-group
  self.close = U.get_pair((self.token.parent or {}).open)

  -- create new token in the same context (as a "brother" of the closed group)
  -- brother means an element of the same level(table deep or tree height)
  self.token = self:new_token(self.token.parent, i + 1)

  dprint("<<---- Close_Group ----| now wait-close:", self.close, "\n")
end

-- is_separator + get-length
-- return nil if char c is not one of the known separators
---@return boolean
function M.Tokenizer:is_separator(c)
  for _, sep in ipairs(self.separators) do
    if c == sep then
      return true
    end
  end
  return false
end

--
-- Parse self.line in range[self.pstart:self.pend] to tree of tokens
--
---@param source string|nil (Optional if not defined here - take from self)
---@param pstart number|nil
---@param pend number|nil
---@return self
function M.Tokenizer:tokenize(source, pstart, pend)
  if source then
    self.next_token_id = 0
    self:source(source, pstart, pend)
  end
  dprint("tokenize ps:", self.pstart, 'pe:', self.pend, 'line:', self.line, "\n",
    'throw_errors:', self.throw_errors,
    'tokens_with_id:', self.tokens_with_id,
    'next_token_id:', self.next_token_id
  )

  assert(type(self.line) == 'string', 'self.line has:' .. tostring(self.line))
  assert(type(self.pstart) == 'number', 'pstart')
  assert(type(self.pend) == 'number', 'pend')

  self.close = nil
  self.root = self:new_token(false)
  self.token = self:new_token(self.root, self.pstart)

  local line, i, prev_c = self.line, self.pstart - 1, nil
  while i <= self.pend do
    i = i + 1
    local c = line:sub(i, i)
    if prev_c ~= '\\' then
      dprint('---- i:', i, 'c:', c, 'wait-close:', self.close, ' ----')

      if (c == '=' or c == ':') and self.close == '}' then
        i = self:open_kv_entry(i)
        --
      elseif c == self.close then
        self:close_group(i)
        --
      elseif U.is_opening_pair(c) then
        self:check_extra_chars(i, c, prev_c)
        if U.is_quotes(c) then
          i = self:add_string_token(i, c)
        else
          self:open_group(i, c)
        end
        --
      elseif U.is_closing_pair(c) then -- })] without opened pair
        self:add_single_char(i, c)
        --
      elseif self:is_separator(c) then
        self:token_ends(i - 1) -- - sep_length
      end
    end
    prev_c = c
  end

  dprint('---- finish last token ----')
  self:token_ends(self.pend) -- #line
  self:seal(self.pend)       -- close and link all unlinked tokens

  return self
end

--
-- in the end of tokenized line -- close and bind to the tree all unlinked tokens
--
---@param pend number
function M.Tokenizer:seal(pend)
  dprint('Seal - Close all unlinked tokens end-pos:', pend)

  assert(type(pend) == 'number', 'self.pend has:' .. tostring(self.pend))
  local ps, pe = self.token.ps, self.token.pe

  if (not pe or not ps) or pe and ps and pe < ps then -- empty
    if self.token.parent == self.root and #self.root == 0 then
      -- case: tokenize empty string ''
      self.token.ps = 0
      self.token.pe = 0
      self.token.open = ''      -- ??
      self.root[1] = self.token -- bind to parent
      self:dprint_token('update to empty token', self.token)
      return
    else
      self.token = self:discard_token(self.token)
    end
  end

  if pend > 0 then
    local token = self.token

    while token do
      self:dprint_token('try to close token:', token)
      -- is not root and not already linked
      if token.parent ~= false and token ~= token.parent[#token.parent] then
        self:token_close(token, pend)
      else
        dprint('token is already linked (closed)')
      end
      token = token.parent
    end
  end

  dprint('root sealed with branches: ', #self.root)
end

-- usecase: - links to latest valid token after exit from tokenizer
-- discrard current temporary self.token (not linked to its parent)
---@param token table
function M.Tokenizer:discard_token(token)
  local last_token = nil -- latest actual(already linked) token
  token.discarded = true
  dprint(1, 'discard token id:', token.id)

  -- check kv-entry  for case has token-key but no input for token-value
  -- if token.parent and token.parent.open == '=' then
  if token.parent and token.parent.entry_key then
    dprint('discarded token is token-value of kv-entry')
    local token_key = token.parent
    token_key.pe = token_key.pe or token.ps - 2
    if not M_token.token_get_link_index(token_key) then
      self:token_close(token_key) -- link to tree token-key without token-value
    end
    last_token = token_key
  else
    -- select previous added token in the same parent to current token(cursor)
    local pa = token.parent
    if pa and pa[#pa] then
      last_token = pa[#pa]
      self:dshow('discard empty token and select the latest added', last_token)
      assert(last_token ~= token, 'discarded not already added to a parent')
    else
      last_token = token.parent
      self:dshow('discard empty token and select parent', last_token)
    end
  end

  self:dprint_token('after discard selected token', last_token)
  return last_token
end

-------------------------------------------------------------------------------

-- root of the tokens tree
---@return table -- Token
function M.Tokenizer:getRoot()
  return self.root
end

-- the "cursor" token - the lats valid word-range(token) after tokenize
---@return table -- Token
function M.Tokenizer:getToken()
  return self.token
end

-- return substring of the range defined in the token [ps:pe]
function M.Tokenizer:getStrValue(token)
  assert(type(token) == 'table', 'token expected has:' .. tostring(token))
  local ps, pe, e = token.ps, token.pe, token.open == ''

  assert(type(self.line) == 'string', 'self.line')
  assert(ps ~= nil and (ps > 0 or e), 'token.p_start has' .. tostring(ps))
  assert(pe ~= nil and (pe > 0 or e), 'token.p_end has' .. tostring(pe))
  assert(ps <= pe, 'token must be not empty (ps<=pe)') -- import!

  -- empty token used to reflect the tokenized empty string
  if ps == 0 and pe == 0 and e then return '' end

  if U.is_quotes(token.open) then
    ps = ps + 1
    if self.line:sub(pe, pe) == token.open then -- case with broken input: |abc'no-q-end|
      pe = pe - 1
    end
  end

  local value = self.line:sub(ps, pe)
  return value
end

--
-- build the value of the given token
--  - simple(primitive)
--  - object(lua-table)
--
---@param token table
---@param group string|nil
function M.Tokenizer:buildValue(token, group)
  local value
  if M_token.token_is_value(token) then
    value = self:getStrValue(token)
    if not U.is_quotes(token.open) then
      -- castring
      value = U.str2value(value)
    end
  else
    value = self:buildObject0(token, group)
  end
  return value
end

--
-- build string value from the token range [ps:pe]
-- thrown error if the token has invalid or empty range
-- concatenate tokens with string value coming sequentially one after another
-- without separators                          case: 'ab c'X'd e' -> 'ab cXd f'
--
---@param t table
---@param token table
---@diagnostic disable-next-line: unused-local
function M.Tokenizer:build_str_token(token, t, prev_str_e, group)
  local value = self:getStrValue(token)
  -- todo case to prim types for child of {
  dprint('token value:', value, 'token.ps:', token.ps, 'prev_str_e:', prev_str_e)

  -- check whether it is necessary to merge two consecutive str-tokens into one
  if prev_str_e and prev_str_e + 1 == token.ps then
    -- value and type(prev_value) == 'string' and token.after_sep then
    t[#t] = t[#t] .. value

    dprint('append to string-token prev-e:', prev_str_e, ' new value:', t[#t])
  else
    t[#t + 1] = value
    dprint('add to args as a new string to idx:', (#t + 1))
  end

  return token.pe
end

--
-- with catsing to type for numbers and booleans
-- grouped token as raw string
--
-- [ 8 'ab' 'c' ]
function M.Tokenizer:buildFlatArray0(root)
  dprint('StringArray Token:', T.get_token_name(root))

  local sarray, pse, cnt = {}, nil, 0 -- pse - prev-str-end-pos
  -- childs
  for k, v in ipairs(root) do
    if type(k) == 'number' and type(v) == 'table' then
      local token = v
      ---@diagnostic disable-next-line: unused-local
      if M_token.token_is_value(token) then
        if not U.is_quotes(token.open) and (not pse or pse + 1 ~= token.ps) then
          sarray[#sarray + 1] = U.str2value(self:getStrValue(token))
          pse = token.pe
        else
          pse = self:build_str_token(token, sarray, pse)
        end
        -- dprint('child_token: ', T.get_token_name(token))
        cnt = cnt + 1
      else
        pse = nil
        -- self:error('build not string-token(object) as a raw string')
        sarray[#sarray + 1] = self:getStrValue(token)
      end
    end
  end
  return sarray, cnt
end

function M.Tokenizer:addKeyObjectRef(token_key, key_object)
  if _TEST then
    if type(key_object) == 'table' then
      self.key_objects = self.key_objects or {}
      self.key_objects[token_key] = key_object
    end
  end
end

---@param root table
---@param group string|nil one of the { [ (
function M.Tokenizer:buildObject0(root, group)
  assert(type(root) == 'table', 'root table expected has:' .. tostring(root))

  local list, cnt, prev_str_e = {}, 0, nil
  -- cnt a number of processed tokens
  -- prev_str_e used to concat(merge) string tokens to one case: 'ab c'X'de f'

  -- the "pairs" does not guarantee traversal order
  for n, token in ipairs(root) do
    self:dshow('build value from token', token, 'k:', n, 'group:', group)
    cnt = cnt + 1

    -- if token.open == '=' then           -- kv-entry
    if token.entry_key then -- kv-entry
      local token_key, token_value, key, value
      token_key = token
      token_value = token[1]
      key = self:buildValue(token_key, group)

      if not token_value then
        self:error('token-key without token-value has:' .. tostring(token_value))
        value = '' -- if specify nil then key will be removed from table
      else
        value = self:buildValue(token_value, group)
      end

      assert(key ~= nil, 'key ~= nil')
      list[key] = value
      self:addKeyObjectRef(token_key, key)

      --
    elseif M_token.token_is_value(token) then -- a simple text
      self:dprint_token('String Token:', token)
      if group == '{' then
        list[#list + 1] = self:buildValue(token)
      else
        -- with merging several consecutive string tokens into one str-value
        prev_str_e = self:build_str_token(token, list, prev_str_e, group)
      end
      --
    elseif token.open == '{' or token.open == '(' then -- {} and ()
      -- convert tokens to lua table with type casting
      local t, cnt0 = self:buildObject0(token, '{')
      list[#list + 1] = t
      prev_str_e = nil
      cnt = cnt + cnt0
      --
    elseif token.open == '[' then --or token.open == '(' then
      local array, cnt0 = self:buildFlatArray0(token)
      list[#list + 1] = array
      prev_str_e = nil
      cnt = cnt + cnt0
      -- idea: apply tonumber for () ?
    end
  end
  return list, cnt
end

-- Convert tree of tokens into object with values
function M.Tokenizer:buildObject()
  dprint("\n---- Build Tree to Object ---->>")
  assert(type(self.root) == 'table', 'has self.root')

  local res, cnt = self:buildObject0(self.root)

  dprint('processed tokens: ', cnt)
  return res
end

function M.Tokenizer:error(msg)
  if self.throw_errors then
    error(tostring(msg))
  else
    dprint(1, '[ERROR] ' .. tostring(msg))
  end
end

---- Debug helpers

function M.Tokenizer:dprint_token(msg, token, trace_offset)
  if D.on and D.is_enabled(M) then
    trace_offset = trace_offset or 0
    return dprint(1 + trace_offset, msg or '', T.token_info(token, self.line))
  end
end

-- visualise verbose token info with arrows in the line
function M.Tokenizer:dshow(msg, token, ...)
  if D.on and D.is_enabled(M) then
    return H.dshow(2, msg, self.line, token, ...)
  end
end

-- debug helper
function M.Tokenizer:toString()
  return T.tree2str(self.root, self.line, false, '  ', false)
end

---@param token table|nil default is self.token (current selected)
function M.Tokenizer:tokenInfo(token)
  token = token or self.token
  if type(token) == 'table' then
    return T.token_info(token, self.line)
  end
  return 'nil'
end

--------------------------------------------------------------------------------

--
-- factory to one line parsing line into object-value
--
-- Parse line to the tree of the tokens
-- and convert this tockens into values (string, arrays, tables)
--
---@param line string
---@param seps table<string>|nil  default is space
---@return table, table, table -- ResultObject, Root, Tokenizer
---@diagnostic disable-next-line: unused-local
function M.parse_line(line, seps)
  local t = M.Tokenizer:new():source(line)
  if seps then
    t:set_separators(seps)
  end
  t:tokenize()
  local res = t:buildObject()
  local root = t:getRoot()
  return res, root, t
end

--
--------------------------------------------------------------------------------
--                       Debugging Helpers
--------------------------------------------------------------------------------


--

function H.dprint_token(msg, token, line)
  return dprint(1, msg or '', T.token_info(token, line))
end

-- debug helper
---@param token table|number
function H.dshow(traceoff, msg, line, token, ...)
  if not D.on or not D.is_enabled(M) then -- if debug is off
    return false
  end
  if not msg and not line then
    return dprint(1, 'Empty message and line')
  end
  msg = tostring(msg)

  local msg0, ps, pe

  if type(token) == 'number' then
    ps = token -- istead of object(table with token) a number(start-pos) passed
    pe = 0     --  the end position not yet known passed only start instead a token
    --
    msg0 = string.format("%s: pos:%s", msg, ps)
    --
  elseif type(token) == 'table' then
    ps = token.ps or 0 -- in name [ps:pe]
    pe = token.pe or 0
    msg0 = msg .. ' ' .. T.token_info(token, line)
  end

  return dvisualize(line, ps, pe, traceoff, msg0, ...)
end

--@diagnostic disable-next-line: undefined-global
-- if _TEST then
--   -- T._token_is_value = token_is_value
--   -- M.T = T
--
--   -- M.token_get_link_index = M_token.token_get_link_index
--   for k,v in pairs(M_token) do
--     if type(v) == 'function' then
--       M[k] = M_token[k]
--     end
--   end
-- end

class.build(M.Tokenizer)

return M

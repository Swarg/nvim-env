-- 02-11-2023 @author Swarg
local M = {}

local U = require("env.util.tokenizer.util")

M.DEEP_LIMIT = 4096

--
-- for testing
--
---@return table -- token
---@param parent table|false|nil
---@param start_pos number
---@param end_pos number
---@param grp_open string|nil
---@param entry_key boolean|nil
---@param id number|nil
function M.new_token(parent, start_pos, end_pos, grp_open, entry_key, id)
  parent = parent or false
  return {
    parent = parent,
    ps = start_pos,
    pe = end_pos,
    open = grp_open,
    entry_key = entry_key,
    id = id, -- used only in testing and debugging mode
  }
end

--
-- check is a given token has valid range [ps:pe]
-- if you need to check with exteranl pe you can pass it via second arg(optional)
--
-- WARN: p_end has more priority than token.pe
-- so if the p_end are given, then check will occur using the p_end not token.pe
--
-- works same for token-value and token-container(grouped)
-- group - token_branch must has at least 2-string range
-- string-token can has 0-string range but with token.open = '' or ',"
--
---@param p_end number|nil -- then needs to check to given end-position
--
function M.token_has_valid_range(token, p_end)
  assert(not p_end or type(p_end) == 'number', 'p_end must be nil or number')

  local valid = false
  if type(token) == 'table' then
    -- open - is a char of opened group - is token has this field it's container
    -- if not token.open then end --?
    local pe = token.pe
    if p_end ~= nil then
      pe = p_end
    end
    valid = token.ps ~= nil and pe ~= nil and token.ps <= pe and pe > 0
  end
  -- dprint(1, 'token-is-valid-range:' .. tostring(valid))
  return valid
end

--
-- check is a token range is a simple value (like string, boolean, number)
-- any group token for {}[] - is not value
-- but if the group containers any string group like '' "" - its the value.
--
-- t.open = '' (empty string) using to specify that token was created from
-- an extra chars before some group (case: abc'x' or abc{d,e}
--
-- token with entry_key field is also checked according to this rules.
-- this is done so that the key can be determined whether it is a primitive or
-- an object value
--
---@param t table -- token
---@return boolean
function M.token_is_value(t)
  if t then -- and not t.entry_key then
    local quoted = t.open ~= nil and U.is_quotes(t.open)
    return (not t.open or quoted or t.open == '')
  end
  -- to make sure and #t == 0 (no childs) but not for t.entry_key
  -- (group-block(token) with string cannot contains a childs tokens)
  return false
end

--
-- get link index in the parent of given token (child index)
--
---@param token table
---@param only_last boolean|nil
function M.token_get_link_index(token, only_last)
  if type(token) == 'table' and type(token.parent) == 'table' then
    local sz = #token.parent
    -- fast
    if sz > 0 and token.parent[sz] == token then
      return sz
    end
    if sz > 1 and not only_last then
      for i, v in ipairs(token.parent) do
        if v == token then
          return i
        end
      end
    end
  end
  return nil
end

function M.get_root(token)
  local deep, limit = 0, M.DEEP_LIMIT

  while token and type(token) == 'table' and token.parent and deep <= limit do
    token = token.parent
    deep = deep + 1
  end -- while
  return token
end

return M

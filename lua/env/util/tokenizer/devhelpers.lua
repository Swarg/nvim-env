-- 02-11-2023 @author Swarg
-- Test and Dev Helpers for tokenizer.parser and tree

-- local M = require("env.util.tokenizer.parser")
local M_token = require("env.util.tokenizer.token")

local T = {}
T.DEEP_LIMIT = 8096

--
--------------------------------------------------------------------------------
--                             Test Helpers
--------------------------------------------------------------------------------

function T.token_info(token, line)
  if type(token) == 'table' then
    local deep, tcnt, tid, name

    tid = T.get_token_id(token)

    -- token_count - a number of the tokens in same parent is useful because
    -- it allows you to estimate how many tokens are in the current context
    tcnt = T.get_token_childs(token.parent)

    deep = T.get_token_deep(token)
    name = T.get_token_name(token) -- [ps:pe:open:]

    if token.discarded then
      name = name .. ' [Discarded]'
    end
    if token.entry_key then
      name = name .. ' [EntryKey]'
    end
    local extra = ''
    if M_token.token_has_valid_range(token) then
      if type(line) == 'string' then
        extra = extra .. ' |' .. line:sub(token.ps, token.pe) .. '|'
      end
    end
    return string.format("id:%s(%s) deep:%s %s%s", tid, tcnt, deep, name, extra)
  end
  return tostring(token)
end

--
-- [1:2:[:] - ps:pe:open  here open is a group-char:
-- [3:4]    - for case then no open
--
-- token to readable for testing
---@param token table|nil
function T.get_token_name(token)
  if type(token) == 'table' then
    if token.parent == false then -- not token.pe and not token.ps then
      return '[root]'
    else
      local name = '[' .. tostring(token.ps) .. ':' .. tostring(token.pe)
      if token.open then
        name = name .. ':' .. token.open .. ':'
      end
      -- if token.id then
      --   name = '#' .. tostring(token.id) .. ' ' .. name
      -- end
      return name .. ']'
    end
  end
  return tostring(token)
end

function T.get_token_id0(token)
  if token and type(token) == 'table' then
    return token.id
  end
end

--
-- verbose token identity: #id + /path/in/tree
--
-- The token identifier is built based on the in-tree path to it.
-- the new_token function creates only a temporary token(leaf), and will not
-- immediately add it to the tree itself. addition to the tree occurs only if
-- the token is validly closed.
-- Therefore, for temporary tokens, the id will be the next index in the parent
-- * - means approximate (next) index for this tokens like: 1-2-3*
--
-- reverse order: from leaf to root in the tokens tree
-- based on the index in all nested parents
--
function T.get_token_id(token)
  -- dprint('get-token-id of token:',T.get_token_name(token))

  if token and type(token) == 'table' then
    local parent, tid, deep, limit = token.parent, '', 0, T.DEEP_LIMIT
    local token_id = token.id

    while parent and type(parent) == 'table' and deep < limit do
      -- dprint('pa:',T.get_token_name(pa), '#pa:', #pa)
      local link_idx
      link_idx = M_token.token_get_link_index(token)

      if not link_idx then
        link_idx = '*' .. tostring(#parent + 1) -- next id for temporary token(leaf)
      end

      if link_idx then
        if tid ~= '' then tid = '/' .. tid end
        -- reverse order: from leaf to root in the tokens tree

        if parent.id then
          link_idx = tostring(link_idx) .. '#' .. parent.id
        end

        tid = tostring(link_idx) .. tid
      end
      token = parent
      parent = token.parent
      deep = deep + 1
    end -- while

    if parent == false then
      tid = '/' .. tid -- root
    end

    if token_id then
      tid = '#' .. token_id .. ' ' .. tid
    end

    if tid == '' then tid = '?' end
    -- dprint('get-token-id of token:',T.get_token_name(token), 'ret:', tid)
    return tid --, deep
  end

  return '?'
end

-- deepcopy function that works with recursive tables.
-- This is done by creating a table of tables that have been copied and feeding
-- it as a second argument to the deepcopy function.
-- Save copied tables in `copies`, indexed by original table.
function T.deep_copy(orig, copies)
  copies = copies or {}
  local orig_type = type(orig)
  local copy
  if orig_type == 'table' then
    if copies[orig] then
      copy = copies[orig]
    else
      copy = {}
      copies[orig] = copy
      for orig_key, orig_value in next, orig, nil do
        copy[T.deep_copy(orig_key, copies)] = T.deep_copy(orig_value, copies)
      end
      setmetatable(copy, T.deep_copy(getmetatable(orig), copies))
    end
  else -- number, string, boolean, etc
    copy = orig
  end
  return copy
end

-- to convert tree with parets id to parent links
---@param token table -- root of the tree
function T.bind_parents(token)
  assert(type(token) == 'table', 'token must be a table')

  for _, child in ipairs(token) do
    T.bind_parents(child)
    child.parent = token
  end

  return token -- leaf
end

-- bind(link) the token to its parent. to list of parent childs
-- link child to parent
function T.link(token)
  assert(type(token) == 'table', 'token must be a table')
  if type(token.parent) == 'table' then
    token.parent[#token.parent + 1] = token
  else
    error('Cannot link. No Parent!')
  end
  return token
end

-- deep in table or height of the given token in the tree
function T.get_token_deep(token)
  local deep = 0
  while token and deep < T.DEEP_LIMIT do
    token = token.parent
    if token == false then -- root
      break
    end
    deep = deep + 1
  end
  return deep
end

-- the number of direct childs(heirs) in this token, excluding nested
function T.get_token_childs(token)
  if token and type(token) == 'table' then
    local c = 0
    for _, child in ipairs(token) do
      if type(child) == 'table' and child.parent == token then
        c = c + 1
      end
    end
    return c
  end
  return 0
end

--  get the token object itself from the tree by its path
-- Examplle of path: /1/2/3
--                    ^----index in root
--                      ^--index in child of root and so on
-- from root to leaf by index id
function T.token_find_by_path(root, path)
  assert(type(root) == 'table', 'root')
  assert(type(path) == 'string', 'path')

  local token = root
  for s in string.gmatch(path, '([%d]+)') do
    local i = tonumber(s)
    if i and token then
      token = token[i]
    else
      return nil
    end
  end
  return token
end

-- function T.token_find_by_path(root, path) end

-- for testing
function T.remove_all_parents(root)
  local i = 0
  for _, v in pairs(root) do
    if type(v) == 'table' then
      if type(v) == 'table' and v.parent then
        v.parent = nil
        i = i + T.remove_all_parents(v) + 1
      end
    end
  end
  return i
end

--
-- replace links to table by readable string with start_pos-end_pos-open-char
--
---@param root table
function T.mk_parents_names_readable(root)
  local i = 0

  local function change_parent(token)
    if token and type(token.parent) == 'table' then
      if token.parent and token.parent.id then
        token.parent = T.get_token_id0(token.parent) -- number
      elseif token.parent ~= false then
        ---@diagnostic disable-next-line: param-type-mismatch
        token.parent = T.get_token_name(token.parent)
      end
    end
  end

  if type(root) == 'table' then
    ---@diagnostic disable-next-line: unused-local
    for n, v in pairs(root) do
      if type(v) == 'table' then
        -- replace link to id(number) or readable name(string)
        change_parent(v)
        i = i + T.mk_parents_names_readable(v) + 1
      end
    end
    change_parent(root)
  end
  return i
end

local function val2str(v)
  if type(v) == 'string' then
    local q = "'"
    local has_sq = v:find("'")
    local has_dq = v:find('"')
    if has_sq and not has_dq then
      q = '"'
    end
    return q .. v .. q
  elseif type(v) == 'number' then
    return tostring(v)
  end
  return tostring(v)
end

-- helper for tree2str "fancy(formated) inspect"
---@param source string|false
---@param tab string|nil
local function mk_tokens_list(parent, source, tab)
  local s = ''
  tab = tab or '  '

  for k, child in ipairs(parent) do
    local line, id, extra, name = '', '', '', ''

    if type(child) == 'table' and k ~= 'parent' then
      local ps, pe, parent_name
      ps = tostring(child.ps)
      pe = tostring(child.pe)
      parent_name = val2str(T.get_token_id0(child.parent) or
        T.get_token_name(child.parent))

      if child.id then
        id = 'id = ' .. child.id .. ', '
      end
      if child.entry_key then
        extra = extra .. ' entry_key = ' .. tostring(child.entry_key) .. ','
      end
      if child.open then
        extra = extra .. ' open = ' .. val2str(child.open) .. ','
      end

      local comment = ' -- ' .. T.get_token_id(child)
      if source and M_token.token_has_valid_range(child) then
        local val = source:sub(child.ps, child.pe)
        comment = comment .. ' |' .. val .. '|'
      end

      -- has subtokens(childs)
      if type(child[1]) == 'table' then
        extra = extra .. comment .. "\n" ..
            mk_tokens_list(child, source, tab .. '  ') .. tab
        comment = ''
      end
      if extra == '' then extra = ' ' end
      -- H.dprint_token('mk_tokens_list:', child)

      line = line .. string.format(
        "%s{ %sparent = %s,%s ps = %s, pe = %s,%s},%s\n",
        tab, id, parent_name, name, ps, pe, extra, comment
      )
    end
    s = s .. line
  end
  return s
end

--
-- simple formatting the comments in the multilines source
-- see better implementation in the lua_parser
--
--   i = 1, -- commentA              =>     i = 1,         -- commentA
--   key = 'ssdhf'  -- commentB      =>     key = 'ssdhf'  -- commentB
--
---@param source string
function T.format_comments(source)
  local max = 0
  local t = {}
  if not source then
    return t
  end
  if source:sub(-1, -1) ~= "\n" then
    source = source .. "\n"
  end
  -- for str in string.gmatch(source, '([^\n]+)') do
  for str in string.gmatch(source, "(.-)\n") do -- keep empty lines
    local i = str:find(' -- ')
    local entry = {}
    if i then
      if i > max then max = i - 1 end
      entry = { str:sub(1, i - 2), str:sub(i - 2, #str) }
    else -- only code without comments
      if #str > max then max = #str end
      entry = { str }
    end
    t[#t + 1] = entry
  end
  local res = ''
  for _, v in ipairs(t) do
    local code, comment = v[1], v[2]
    if not comment then
      res = res .. code .. "\n"
    else
      res = res .. string.format("%-" .. max .. "s%s\n", code, comment)
    end
  end
  return res
end

-- helper for tests
-- side effect -- apply mk_parents_names_readable
---@param source string|false -- the source line to show values of tokens
function T.tree2str(root, source, modify, tab, addexp)
  tab = tab or '  '
  modify = modify or false
  addexp = addexp or true
  if type(root) == 'table' then
    if modify then
      T.mk_parents_names_readable(root)
    end
    local p_name = tostring(root.parent)
    local s = "{\n" .. tab
    if root.id then
      s = s .. 'id = ' .. tostring(root.id) .. ', '
    end
    s = s .. "parent = " .. p_name .. ", -- " .. T.get_token_id(root) .. "\n"

    s = s .. mk_tokens_list(root, source, tab) .. "}\n"
    -- format comments

    s = T.format_comments(s)

    if addexp then
      s = 'local exp = ' .. s
    end
    return s
  end
  return nil
end

--
-- create deep copy of given tree and replace links to parent to names
--
---@param root table -- Token
function T.readable_copy(root)
  local copy = T.deep_copy(root)
  T.mk_parents_names_readable(copy)
  return copy
end

return T

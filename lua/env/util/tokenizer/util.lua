-- 02-11-2023 @author Swarg
local U = {}
--
--------------------------------------------------------------------------------
--                            Utilites
--------------------------------------------------------------------------------

U.pairs_group = {
  ["'"] = '\'',
  ['"'] = '"',
  ["("] = ')',
  ["{"] = '}',
  ["["] = ']',
  [")"] = '(',
  ["}"] = '{',
  ["]"] = '[',
}

---@param c string
---@return string|nil
function U.get_pair(c)
  if c then return U.pairs_group[c] end
end

function U.is_paired(c)
  return c == '\'' or c == '"' or
      c == '(' or c == ')' or
      c == '{' or c == ']' or
      c == '[' or c == ']'
end

---@param c string
---@return boolean
function U.is_opening_pair(c)
  return c == '\'' or c == '"' or c == '{' or c == '[' or c == '('
end

function U.is_closing_pair(c)
  return c == '}' or c == ']' or c == ')'
end

function U.is_quotes(c)
  return c == '\'' or c == '"'
end

--
-- find the index of a first not escaped char in the line with a given range
-- [start-pend]
--
---@param line string
---@param start number
---@param pend number     - if <=0 take #line
---@param find_char string
---@param def number      - value to return then find_char not found(def #line)
function U.indexof_unescaped_char(line, start, pend, find_char, def)
  assert(type(line) == 'string', 'line')
  assert(type(start) == 'number', 'start pos')
  assert(type(pend) == 'number', 'end pos')
  if pend <= 0 then
    pend = #line
  end
  if start <= pend and find_char and find_char ~= '' then
    local prev_c = nil
    for i = start, pend do
      local c = line:sub(i, i)
      if prev_c ~= '\\' then
        if c == find_char then
          return i
        end
      end
      prev_c = c
    end
  end
  return def or pend
end

-- simple castring from string
function U.str2value(s)
  if s then
    if s == 'true' then
      return true
    elseif s == 'false' then
      return false
    else
      return tonumber(s) or s
    end
  end
  return tostring(s)
end

return U

-- 02-11-2023 @author Swarg

-- Goal: Interact with token tree
--  - like file system

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local D = require("dprint")
local U = require("env.util.tokenizer.util")
local T = require("env.util.tokenizer.devhelpers")
local M_tokenizer = require("env.util.tokenizer.parser")
local M_token = require("env.util.tokenizer.token")

---@diagnostic disable-next-line: unused-local
local dvisualize = D.visualize

_TEST = _G._TEST

local class = require 'oop.class'

class.package 'parser.tokenizer'
---@class parser.tokenizer.Tree
--
---@field line string
---@field p_start number
---@field p_end number
---@field root table (token) -- parser.Token
---@field token table (token) -- parser.Token
--
local Tree = class.new_class(nil, 'Tree', {
})
local dprint = D.mk_dprint_for(Tree)

--
-- Factory to create Tree
-- from already existed Tokenizer or from given input (string)
--
---@param obj table|string -- tokenizer|line  -- tokenizer.Parser
---@return table
function Tree.of(obj)
  local tree = Tree:new()

  if type(obj) == 'string' then
    local line = obj
    obj = M_tokenizer.Tokenizer:new(line):tokenize()
  end

  tree:sync(obj)
  return tree
end

---@param tokenizer table
---@return self
function Tree:sync(tokenizer)
  if type(tokenizer) == 'table' and tokenizer.root and tokenizer.line then
    self.line = tokenizer.line
    self.pstart = tokenizer.pstart
    self.pend = tokenizer.pend
    self.root = tokenizer.root
    self.token = tokenizer.token
    self.throw_errors = tokenizer.throw_errors
  end
  return self
end

--
function Tree:new(obj)
  obj = obj or {}

  -- on invalid input
  obj.throw_errors = false

  -- usecase: :new({line = line})
  if obj.line and (not obj.pstart or not obj.pend) then
    Tree.source(obj, obj.root, obj.line)
  end

  self.__index = self
  setmetatable(obj, self)
  return obj
end

function Tree:source(root, line, pstart, pend)
  assert(type(root) == 'table', 'root')

  assert(type(line) == 'string', 'line must be a string has: ' .. tostring(line))
  assert(not pstart or type(pstart) == 'number', 'pstart')
  assert(not pend or type(pend) == 'number', 'pend')

  self.root = root

  self.line = line
  self.pstart = pstart or 1
  self.pend = pend or #line

  return self
end

-- root of the tokens tree
---@return table -- Token
function Tree:getRoot()
  return self.root
end

-- the "cursor" token - the lats valid word-range(token) after tokenize
---@return table -- Token
function Tree:getCursor()
  return self.token
end

--------------------------------------------------------------------------------
--      Interact with tokens tree as a file system (by fimilar commands)
--------------------------------------------------------------------------------
-- ct -> cd, ls, pwd

--
-- change tocken - like cd(change-dir) by given absolute or relative path
-- in the current tree with self.root
--
-- path specified by numbers of the index in which tokens as childs linked in
-- its parents
--
-- Examples:
-- /1/2   -->  0 1 2  -- absolute path from root starts from 0 (zero)
-- ^root---^
-- ./1/2  -->  1 2    -- relative path from current self.token(cursor)
-- ..     -->  -1     -- up to parent (go back from leaf to root)
-- ../..  -->  -2     -- up to parent of parent
--
function Tree:cd(...) -- change-token
  local tree = self.token
  for i = 1, select('#', ...) do
    local idx = select(i, ...)
    assert(type(idx) == 'number', 'index must be a number has:' .. type(idx))
    if idx == 0 then
      tree = self.root
    elseif idx < 0 then -- back to root from leaf
      while idx < 0 and tree.parent ~= false do
        tree = tree.parent
        idx = idx + 1
      end
    else
      tree = tree[idx]
    end
  end
  self.token = tree
  return self
end

-- select last branch in the level of the current subtree
---@param token table|nil
function Tree:last(token)
  token = token or self.token
  if self.token then
    self.token = self.token[#self.token]
  end
  return self
end

-- how many childs in the given or a current token
function Tree:branches(token)
  token = token or self.token
  if type(self.token) == 'table' then
    return #self.token
  end
  return -1
end

-- list of childs in given or current self.token
---@param token table|nil
function Tree:ls(token)
  token = token or self.token
  if type(token) == 'table' then
    local line = ''
    for i, t in ipairs(token) do
      local open, ps, pe, content, range = t.open, t.ps, t.pe, '', ''
      if not open then
        open = ' '
      elseif open == '' then
        open = 'e' -- empty
      end
      if ps and pe and pe >= ps then
        local len = pe - ps
        if len > 40 then len = 40 end
        content = self.line:sub(ps, ps + len)
      end
      range = '[' .. tostring(ps) .. ':' .. tostring(pe) .. ']'
      local t1, t2, t3, t4 = '-', '-', '-', '-'
      if #t > 0 then t1 = 'd' end -- branch or directory
      if t.entry_key then t2 = 'K' end
      if t.discarded then t4 = 'D' end

      line = line .. string.format("%s%s%s%s %4s  %s  %-16s  %s\n",
        t1, t2, t3, t4, i, open, range, content)
    end
    return line
  end
  return ''
end

-- the full path from root to the given or current self.token
---@param token table|nil
function Tree:pwd(token)
  token = token or self.token
  if type(token) == 'table' then
    if token.parent == false then
      return '0'
    end
    local path, limit = '', M_tokenizer.DEEP_LIMIT
    while limit > 0 do
      local idx = M_token.token_get_link_index(token)
      if idx then
        if path ~= '' then
          path = ' ' .. path
        end
        path = idx .. path
        token = token.parent
      else
        break
      end
      limit = limit - 1
    end
    path = '0 ' .. path
    return path
  end
  return ''
end

--
-- content of the given or the current self.token
-- if token does not exists return nil
--
---@param token table|nil
---@return string?, number?, number?
function Tree:cat(token)
  assert(type(self.line) == 'string', 'self.line')

  token = token or self.token
  -- assert(type(token) == 'table', 'token expected, has:' .. tostring(token))

  if token then
    local ps, pe = token.ps, token.pe
    dprint('value ps pe:', ps, pe)
    if pe and ps and ps <= pe then
      local value
      if pe == 0 and ps == 0 then -- empty token
        value = ''
      else
        if U.is_quotes(token.open) then
          ps = ps + 1
          -- case with broken input: |abc'no-q-end|
          if self.line:sub(pe, pe) == token.open then
            pe = pe - 1
          end
        end
        value = self.line:sub(ps, pe)
      end
      dprint('value:', value)
      return value, token.ps, token.pe
    end
  end

  return nil, 0, 0
end

-- tokenInfo
function Tree:stat(token)
  token = token or self.token
  if type(token) == 'table' then
    return T.token_info(token, self.line)
  end
  return 'not exists'
end

-- is cursor not empty
function Tree:has() -- hasToken
  return self.token ~= nil
end

---@param open_char string|nil
---@param token table|nil
function Tree:isGroup(open_char, token)
  token = token or self.token
  return token and token.open == open_char
end

function Tree:hasValidRange(token)
  token = token or self.token
  if token then
    local ps, pe = token.ps, token.pe
    return ps and pe and ps <= pe and ps > 0 and pe <= self.pend
  end

  return false
end

-- the range length of the string in the source line that
-- represets the current token
function Tree:rangeLength(token)
  token = token or self.token
  local len = 0
  if token and token.ps and token.pe and token.ps <= token.pe then
    len = token.pe - token.ps
    if len > 0 then
      len = len + 1 -- range 1:1 - represent one char(len=1)
    end
  end
  return len -- 0 for nil token??
end

function Tree:isQuotedString(token)
  token = token or self.token
  return self:hasValidRange(token) and not token.entry_key
      and U.is_quotes(token.open)
end

function Tree:isEntryKey(token)
  token = token or self.token
  return token.entry_key
end

function Tree:isLeaf(token)
  token = token or self.token
  return #token == 0 and not token.entry_key
end

function Tree:isBranch(token)
  token = token or self.token
  return #token > 0 -- and token.entry_key
end

--------------------------------------------------------------------------------

--- word - string without quotes
---@param word string
-- function Tree:isContainsWord(word, token)
function Tree:isWord(word, token)
  assert(type(word) == 'string', 'word')
  token = token or self.token

  if word and self:hasValidRange(token) and not self:isQuotedString(token) then
    if token.pe - token.ps + 1 == #word then
      local value = self:cat(token)
      return value ~= nil and value == word
    end
  end
  return false
end

-- if the token contains word(not string in quoted) - return it word or nil
---@return string?
function Tree:getWord(token)
  token = token or self.token

  if self:hasValidRange(token) and not self:isQuotedString(token) then
    return self:cat(token)
  end
  return nil
end

-- if the token contains one char - return it char or nil
function Tree:isOneChar(token)
  token = token or self.token
  local t = token
  if t and t.ps ~= nil and t.pe ~= nil and t.ps > 0 and t.ps == t.pe then
    return self.line:sub(t.ps, t.pe)
  end
  return nil
end

--
--------------------------------------------------------------------------------
--                            Lua Syntax
--------------------------------------------------------------------------------

-- build from current token the lua strblock
-- both from starts part or from the ends
-- StrBlock [[]]  [==[]==]
---@param token table subtree
function Tree:getLuaSBlock(token)
  token = token or self.token
  dprint("getLuaSBlock has token", token ~= nil)
  if token then
    -- starts part like [[  or [==[
    if token.open == '[' and token[1] then
      local inner = self:getWord(token[1])
      -- dprint('inner:', inner)
      if inner then
        local c = string.sub(inner, 1, 1)
        if c == '=' and token[2] then
          local ends = self:getWord(token[2])
          if ends then
            return '[' .. inner .. '['
          end
        elseif c == '[' then
          return '[['
        end
      end
      --
      -- ends part like ]] or ]==]
    elseif not token.open and token.parent and self:isOneChar(token) == ']' then
      local idx = M_token.token_get_link_index(token)
      if idx and idx > 1 then
        local inner = self:getWord(token.parent[idx - 1])
        -- dprint('idx:', idx, 'n:', #token.parent, 'w:', inner)
        if inner then
          if inner == ']' then
            return ']]'
          elseif inner then
            local w2 = self:getWord(token.parent[idx - 2])
            if w2 == ']' and inner:sub(1, 1) == '=' then
              return ']' .. inner .. ']'
            end
          end
        end
      end
    end
  end
  return nil
end

---@param block string
function Tree.getLuaSBlockPair(block)
  -- assert(type(block) == 'stirng', 'block')
  if block and #block >= 2 then
    local fc, c = block:sub(1, 1), nil
    if fc == ']' then
      c = '['
    elseif fc == '[' then
      c = ']'
    end
    if c then
      return c .. block:sub(2, -2) .. c
    end
  end
end

--
--------------------------------------------------------------------------------
--                        Testing Helpers
--------------------------------------------------------------------------------

--
-- Example:
--   assert.same('[3:3] |=|', treeOf('x = '):cd(0):last():cat0())
--   cd(0) - start from root of the tree
--   last - select last branch in the current(root) level
function Tree:cat0(...)
  self:cd(...)
  local s, ps, pe = self:cat()
  return string.format('[%s:%s] |%s|', tostring(ps), tostring(pe), tostring(s))
end

class.build(Tree)
return Tree

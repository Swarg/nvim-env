-- 02-10-2023 @author Swarg
local M = {}
-- Goal: helper for vim.diagnostic
--
M.severity = {
  HINT = 1,
  INFO = 2,
  WARN = 3,
  ERROR = 4,
}
if vim and (vim.diagnostic or {}).severity then
  M.severity.HINT = vim.diagnostic.severity.HINT
  M.severity.INFO = vim.diagnostic.severity.INFO
  M.severity.WARN = vim.diagnostic.severity.WARN
  M.severity.ERROR = vim.diagnostic.severity.ERROR
end

-- mkDiagnosticEntry
---@param severity number
---@param message string
---@param lnum number
---@param col number
---@param end_col number
function M.mkDiagnosticEntry(severity, message, lnum, col, end_col)
  assert(type(severity) == 'number')
  return {
    severity = severity or M.severity.HINT,
    lnum = lnum - 1,
    end_lnum = lnum - 1,
    col = col,
    end_col = end_col,
    message = message,
  }
end

function M.hint(message, lnum, col, end_col)
  return M.mkDiagnosticEntry(M.severity.HINT, message, lnum, col, end_col)
end

function M.info(message, lnum, col, end_col)
  return M.mkDiagnosticEntry(M.severity.INFO, message, lnum, col, end_col)
end

function M.warn(message, lnum, col, end_col)
  return M.mkDiagnosticEntry(M.severity.WARN, message, lnum, col, end_col)
end

function M.error(message, lnum, col, end_col)
  return M.mkDiagnosticEntry(M.severity.ERROR, message, lnum, col, end_col)
end

-- return key(index) for find-object in table-list or nil
function M.tbl_indexof(list, find)
  if list and find then
    for i, e in pairs(list) do
      if find == e then
        return i
      end
    end
  end
  return nil
end

-- return a list of all diagnostics(dlist) by a given namespace
-- except those that contains in the excludes list
--
---@param dlist table
---@param ns number
---@param excludes table|nil
function M.filter_by_namespace(dlist, ns, excludes)
  local t = {}
  if dlist and ns then
    for _, d in pairs(dlist) do
      if d.namespace == ns and M.tbl_indexof(excludes, d) == nil then
        t[#t + 1] = d
      end
    end
  end
  return t
end

--
---@param dlist table
---@param ln number
---@param bufnr number
---@param col number|nil
---@return table<table>|nil -- list of the diagnostic entries or nil
function M.filter_by_position(dlist, bufnr, ln, col)
  assert(type(ln) == 'number', 'line-nr')
  assert(not col or type(col) == 'number', 'col')

  local t, i = {}, 0
  -- vim specific
  ln = ln - 1
  if col then
    col = col - 1
  end
  if col and col < 0 then -- not check by columns
    col = nil
  end

  for _, d in pairs(dlist) do
    if d.bufnr == bufnr then
      if ln == d.lnum then
        if not col or (col >= d.col and col <= d.end_col) then
          t[#t + 1] = d
          i = i + 1
        end
      end
    end
  end

  if i > 0 then
    return t
  end
  return nil
end

--
-- if ns_include specified, then take only those records whose namespace numbers match from the ns_includes list
-- ns_includes - list of the numbers or nil
-- When the ns_includes list is not specified, then simply separate all found
-- entries by namespace  the key is namespace number.
--
-- keeps the namespace even for empty lists (used to clear a diagnostic via set)
--
---@param dlist table
---@param ns_includes table<number>|nil|false  -- list of namespaces nums(id) or nil
---@param d_excludes table<table>|nil          -- list of diagnostic to exclude
--                                                fromt the result list
---@return table
function M.split_by_namespace(dlist, ns_includes, d_excludes)
  local t = {}
  for _, d in pairs(dlist) do
    local ls = t[d.namespace] or {} -- keep the namespace even for empty lists
    if not ns_includes or M.tbl_indexof(ns_includes, d.namespace) then
      if not d_excludes or M.tbl_indexof(d_excludes, d) == nil then
        ls[#ls + 1] = d
      end
    end
    t[d.namespace] = ls -- keep the namespace even for empty lists
  end
  return t
end

---@param bufnr number
---@param all table?     -- the list(table) of the diagnostic entries to remove
---@param to_remove table? -- the list(table) of the diagnostic entries to remove
function M.remove_all(bufnr, all, to_remove)
  if all and to_remove then
    local da_byns = M.split_by_namespace(all, false, to_remove)
    for ns, ns_dlist in pairs(da_byns) do
      vim.diagnostic.set(ns, bufnr, ns_dlist)     -- bufnr == rd.bufnr
    end
  end
end

return M

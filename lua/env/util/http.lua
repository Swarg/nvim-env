-- 10-01-2024 @author Swarg
-- Goals:
--   parsing things related to HTTP
--

local M = {}

local su = require('env.sutil')

---@class env.util.http.Request
---@field scheme string
---@field method string
---@field host string
---@field uri string
---@field headers table?
---@field proxy string?


---@class env.util.http.Transport
---@field send fun(request: env.util.http.Request, callback)
---@field decode fun(raw_response:string): string -- string to lua-table(decode json)

local DEF_SCHEME = 'http'

---@param request env.util.http.Request
function M.request_to_url(request)
  assert(type(request) == 'table', 'request')
  return string.format('%s://%s%s',
    (request.scheme or DEF_SCHEME), request.host, request.uri)
end

--

---@param line string
function M.parse_reguest_method_uri_proto(line)
  -- GET /v2/library/alpine/tags/list HTTP/1.1
  local method, uri, proto
  method, uri, proto = string.match(line, '^(%a+)%s+([^%s]+)%s+([^%s]+)%s*$')
  return method, uri, proto
end

local http_methods = {
  HEAD = 1,
  GET = 1,
  POST = 1,
  PUT = 1,
  DELETE = 1,
  CONNECT = 1,
  OPTIONS = 1,
  TRACE = 1,
  PATCH = 1
}

function M.is_valid_http_method(method)
  if method then
    method = string.upper(method)
    return http_methods[method] == 1
  end
  return false
end

---@param line string
function M.parse_header_entry(line)
  if line then
    local name, value = string.match(line, "^%s*([^:]+)%s*:%s*(.*)%s*$")
    return name, value
  end
end

--------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------


---@param status string?
---@param headers table|string
---@param lines table output
function M.headers_to_lines(status, headers, lines)
  if status then
    lines[#lines + 1] = status
  end
  if type(headers) == 'table' then
    for h, v in pairs(headers) do
      lines[#lines + 1] = h .. ': ' .. tostring(v)
    end
  elseif type(headers) == 'string' then
    su.split(headers, "\n", lines)
  end

  lines[#lines + 1] = ''
end

--
---@param response_body string|table
---@param lines table output
function M.response_to_lines(response_body, lines)
  if type(response_body) == 'table' then
    for _, line in pairs(response_body) do
      line = line:gsub("\r", "")
      if line:find("\n") then
        su.split(line, "\n", lines)
      else
        lines[#lines + 1] = line
      end
    end
  elseif type(response_body) == 'string' then
    su.split(response_body, "\n", lines)
  end
end

--
---@param t table{scheme, method, ut, proxy, etc}
---@param proto string
---@param lines table output
function M.reqt_to_lines(t, proto, lines)
  if t.proxy then
    lines[#lines + 1] = tostring(t.scheme) .. ' ' .. tostring(t.proxy)
    lines[#lines + 1] = ''
  end
  local uri = t.uri or '/'
  proto = proto or 'HTTP/1.1'
  lines[#lines + 1] = tostring(t.method) .. ' ' .. uri .. ' ' .. proto

  -- workaround to show request header with auth data
  if t.user and t.password then
    -- luasocket
    local mime = require("mime")
    local url = require("socket.url")
    t.headers["authorization"] =
        "Basic " .. (mime.b64(t.user .. ":" .. url.unescape(t.password)))
  end
  M.headers_to_lines(nil, t.headers, lines)
end

--[==[

snippets:

local https = require("ssl.https")
local one, code, headers, status = https.request{
       url = "https://www.test.com",
       key = "/root/client.key",
       certificate="/root/client.crt",
       cafile="/root/ca.crt"
}

-- local result, b, c, h = http.request{
--   url = "myurl", headers = header, method="POST" }
-- for k,v in pairs(c) do print(k,v) end

-- local https = require("ssl.https")
-- local one, code, headers, status = https.request{
--        url = "https://www.test.com",
--        key = "/root/client.key",
--        certificate="/root/client.crt",
--        cafile="/root/ca.crt"
-- }

--  os.getenv("http_proxy") how its work?

]==]

return M

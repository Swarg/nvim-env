-- 01-03-2025 @author Swarg
--


local log = require 'alogger'
local Editor = require 'env.ui.Editor'

local M = {}

local cache = {}

local DEFAULT_TAB = 4
local log_debug, log_trace = log.debug, log.trace

function M.get_parsed_yml(bufnr)
  if bufnr == 0 then bufnr = nil end
  bufnr = bufnr or vim.api.nvim_get_current_buf()
  if not cache[bufnr] then
    cache[bufnr] = {}
    -- parse...
  end
  return cache
end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

--
---@param line string?
function M.parse_line_with_key(line)
  local ind, key = match(line or '', "^(%s*)([%w_%-/{}]+)%s*:%s*")
  return ind, key
end

local parse_line_with_key = M.parse_line_with_key

--
--
---@return string?
---@return string? errmsg
function M.get_full_path()
  log.debug("get_full_path")
  local ctx = Editor:new():getContext()
  local lnum = ctx:getCurrentLineNumber();
  local lines = ctx:getLines(1, lnum)

  local curr_line = lines[lnum]
  local ind, key = parse_line_with_key(curr_line);
  if not key then
    return nil, 'expected line with key'
  end
  local path = { key }
  local prev_ind = ind

  for i = lnum - 1, 1, -1 do
    local line = lines[i]
    ind, key = parse_line_with_key(line)
    if ind and #ind < #prev_ind then
      path[#path + 1] = key
      prev_ind = ind
    end
  end
  local path0 = '#'

  for i = #path, 1, -1 do
    local key0 = path[i]
    path0 = path0 .. '/' .. key0
  end
  return path0
end

-- $ref: '#/components/schemas/zones_identifier'
function M.get_ref_from_cursor(line)
  line = line or Editor.getCurrentLine()
  local ref = match(line, "^%s*%-?%s*%$ref%s*:%s*'([^'%s]+)'%s*$")
  return ref, line
end

--
---@param ref string?
---@return boolean?
---@return string? errmsg
function M.jump_to_ref(ref)
  local line = nil
  if not ref or ref == '' or ref == '.' then
    ref, line = M.get_ref_from_cursor()
  end
  if not ref then
    return nil, 'not found $ref in current line: ' .. v2s(line)
  end

  local ctx = Editor:new():getContext()
  local lines = ctx:getLines(1, -1)
  local lnum = M.find_lnum_of_key(lines, ref)

  if not lnum then
    return nil, 'not found ref: ' .. v2s(ref)
  end
  ctx:setCursorPos(lnum, 4)
  return true
end

---@param path string
function M.parse_key_path(path)
  local key_names = {}
  for key in string.gmatch(path, '([^/]+)') do
    if key ~= '#' or #key_names > 0 then -- ignore first # char
      key_names[#key_names + 1] = key
    end
  end
  return key_names
end

--
---@param ref string #/root/sub/...
---@return number? lnum
function M.find_lnum_of_key(lines, ref, tab)
  log_debug("find_lnum_of_key lines:%s ref:%s", #(lines or E), ref)
  tab = tab or DEFAULT_TAB
  local path = M.parse_key_path(ref)

  local next_ind, key_depth = 0, 1

  for lnum = 1, #lines, 1 do
    local line = lines[lnum]
    local ind, key = parse_line_with_key(line)
    if ind and key then
      if #ind == next_ind and path[key_depth] == key then
        key_depth = key_depth + 1
        next_ind = next_ind + tab
        if key_depth > #path then
          return lnum
        end
      end
    end
  end
  return nil
end

--- three action
--  1. lines
--  2. lnum of key head
--  3. indentation level of the key
---@return table? lines
---@return number? lnum
---@return number? indn
---@return string? errmsg
local function prepare_key_head(keypath, tab, lines, lnum)
  local err = nil

  -- pick lines or use defined
  if not lines or not lnum then -- for testing
    local ctx = Editor:new():getContext()
    lines = lines or ctx:getLines(1, -1)
    lnum = lnum or ctx:getCurrentLineNumber();
  end
  assert(lines, 'lines')
  assert(lnum, 'lnum')

  -- if keypath is specified find lnum of line with it key(starting of key body)
  if keypath ~= nil and keypath ~= '.' then
    lnum, err = M.find_lnum_of_key(lines, keypath, tab)
    if not lnum then
      return nil, nil, nil, err or 'key not found'
    end
  end

  -- pick indentation in line with key to compare in future
  local ind0, key0 = parse_line_with_key(lines[lnum])
  log_debug("current ind:|%s| key: %s ", ind0, key0)
  if not ind0 or not key0 then
    return nil, nil, nil, 'cannot parse line with key: ' .. v2s(lnum)
  end

  return lines, lnum, #ind0
end

---@param keypath? string full name (path to key)
---@param tab number?
---@param lines table?
---@param lnum number?
---@return number? lnum
---@return number? lnum_end
---@return string? errmsg
function M.find_key_body_range(keypath, tab, lines, lnum)
  log_debug("find_key_body_range", keypath)
  local indn, err
  lines, lnum, indn, err = prepare_key_head(keypath, tab, lines, lnum)
  if err or not lines then return nil, nil, err end

  local lnum_end = #lines

  for i = lnum + 1, #lines, 1 do
    local line = lines[i]
    local ind, key = parse_line_with_key(line)
    if ind and key then
      if indn == #ind then
        lnum_end = i - 1
        break;
      end
    end
  end

  log_debug("lnum:%s lnum_end:%s", lnum, lnum_end)
  return lnum, lnum_end
end

---@param keypath? string full name (path to key)
---@param tab number?
---@param lines table?
---@param lnum number?
---@return number? lnum
---@return number? lnum_end
---@return string? errmsg
function M.find_sibling_keys_range(keypath, tab, lines, lnum)
  log_debug("find_sibling_keys_range", keypath)
  tab = tab or DEFAULT_TAB
  local indn, err
  lines, lnum, indn, err = prepare_key_head(keypath, tab, lines, lnum)
  if err or not lines then return nil, nil, err end

  indn = indn - tab
  if indn < 0 then
    return 1, #lines
  end

  local lnum_end = #lines

  -- find the end of the parent body
  for i = lnum + 1, #lines, 1 do
    local line = lines[i]
    local ind, key = parse_line_with_key(line)
    if ind and key then
      if indn == #ind then
        lnum_end = i - 1
        break;
      end
    end
  end

  -- find the head of the parent body
  for i = lnum - 1, 1, -1 do
    local line = lines[i]
    local ind, key = parse_line_with_key(line)
    if ind and key then
      if indn == #ind then
        lnum = i
        break;
      end
    end
  end

  log_debug("found sibling keys range: lnum:%s lnum_end:%s", lnum, lnum_end)
  return lnum, lnum_end
end

--
-- get list of key names from range of sibling keys
---@param lnum number
---@param lnum_end number
---@param lines table?
function M.get_sibling_key_names(lnum, lnum_end, lines)
  log_debug('get_sibling_key_names range: lnum:%s lnum_end%s ', lnum, lnum_end)
  if not lines then
    local ctx = Editor:new():getContext()
    lines = ctx:getLines(lnum, lnum_end - 1)
  end

  local ind1, key1 = parse_line_with_key(lines[1])
  log_debug('first line: ', lines[1])
  if not ind1 or not key1 then
    return nil, 'expected first line with key got:' .. v2s(lines[1])
  end

  local keys = {}
  local indn = #ind1

  for i = 1, #lines, 1 do
    local line = lines[i]
    local ind, key = parse_line_with_key(line)
    -- log_debug('key: %s %s', indn, #(ind or ''), key)
    if ind and key then
      if indn == #ind then
        keys[#keys + 1] = key
      end
    end
  end

  return keys
end

--

---@return string
local function mk_err_msg_on_open(prefix, path, depth, node)
  local key0 = path[depth]
  local keys = ''
  if type(node) == 'table' then
    for k, _ in pairs(node) do
      keys = keys .. ' ' .. v2s(k)
    end
  end

  local err = prefix .. ' node with key: "' .. v2s(key0) ..
      '" at depth: ' .. v2s(depth)
  if depth > 1 then
    err = err .. ' passed path: [' .. table.concat(path, " ", 1, depth - 1) .. ']'
  end
  err = err .. " available keys in prev level: [" .. keys .. ']'
  return err
end

--
--
---@param yml table parsed yml file
---@param key string path of the node to open
---@return table? node
---@return string? errmsg
---@return table? path
function M.open_node(yml, key)
  log_trace("open_node", type(yml), key)
  if type(yml) ~= 'table' then
    return nil, 'yml: expected table got: ' .. type(yml)
  end
  local path = M.parse_key_path(key)
  local depth, node, err = 1, nil, nil
  node = yml -- root

  local rootkey = path[1] or false
  local nextkey = path[2] or false
  -- to skip root key
  -- print('has-root:' .. v2s(yml[rootkey] == nil) .. 'has next:' ..
  -- v2s(yml[nextkey] ~= nil) .. ' | ' .. v2s(nextkey))
  if yml[rootkey] == nil and yml[nextkey] ~= nil then
    depth = depth + 1
  end

  while depth <= #path do
    local key0 = path[depth]
    assert(key0, 'has key at depth: ' .. v2s(depth))
    local prev_node = node
    if type(node) ~= 'table' then
      err = mk_err_msg_on_open('not a object value', path, depth, prev_node)
      node = nil
      break
    end
    node = node[key0]
    if not node then
      err = mk_err_msg_on_open('not found', path, depth, prev_node)
      node = nil
      break
    end
    depth = depth + 1
  end

  log_debug("open_node:%s err:%s path:%s depth:%s", node ~= nil, err, path, depth)
  return node, err, path
end

return M

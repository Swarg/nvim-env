-- 08-05-2023
-- @author Swarg
-- Goal: run app/tests for projects: java, c, lua ...
-- debugging:

local M = {}

local log = require('alogger')
local c4lv = require("env.util.cmd4lua_vim")
local cu = require("env.util.commands")

-- platforms
local lua = require('env.lua')
local umake = require("env.bridges.make")
local nvim_jdtls = require 'env.bridges.nvim-jdtls'
local btelescope = require 'env.bridges.telescope'

local class = require("oop.class")

local Editor = require 'env.ui.Editor'
local Lang = require('env.lang.Lang')
-- register in enc.lang.Lang
require('env.langs.java.JLang')
require 'env.langs.svelte.SLang'
local LuaLang = require('env.langs.lua.LuaLang')
require 'env.langs.markdown.MDLang'
require('env.langs.elixir.ExLang')
require('env.langs.php.PhpLang')

local ui = require('env.ui')
local bu = require('env.bufutil')
local su = require('env.sutil')
local setup = require('env.setup')
local cb = require("env.util.clipboard")
local cmd_gradle = require 'env.langs.java.command.gradle'

local E = {}

local api = vim.api
---@cast api table


function M.setup(config)
  setup.init(config)

  -- autocommands
  vim.api.nvim_create_autocmd({ "BufWritePost" }, {
    pattern = {
      "*.rockspec",
      -- TODO build.gralde, pom.xml, mix.exs, etc
    },
    callback = M.refresh_buildscript
  })
end

local function is_file(ctx, ext)
  if ctx and ctx.uri and ext then
    if type(ctx.uri) == "string" then
      local s = ctx.uri
      if su.starts_with(s, "file://") and su.ends_with(s, ext) then
        return true
      end
    end
  end
  return false;
end

-- Create ctx table for common fields.
-- if bufnr not defined take current buffer
---@param bufnr? number
local function make_context(bufnr)
  bufnr = (bufnr == nil or bufnr == 0) and api.nvim_get_current_buf() or bufnr
  return {
    bufnr = bufnr,                          -- buf num for each run cmd
    out_bufnr = -1,                         -- buffer for output staout of runned cmd
    bufname = api.nvim_buf_get_name(bufnr), -- full file name
    uri = vim.uri_from_bufnr(bufnr),        -- file:// + buffname
    project_root = nil,
    class_name = nil,
    src = nil,     -- dirname -sub path from project root to source code
    test = nil,    -- sub path to tests files
    builder = nil, -- System Builder like make, mvn, gradle, etc
  }
end


local UNSUPPORTED_FT = "Unsupported file type"

-- run the main method in current file
function M.run_main_in_file(opts)
  log.debug('Run Main in File')
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)
  bu.save_current_buffer()

  if is_file(ctx, ".lua") then
    lua.run_main_in_file(ctx, opts)
  else
    local lhandler = Lang.getLang(ctx.bufname)
    if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
      lhandler:resolve(ctx.bufname):runSingleSrcFile(ctx.bufname)
      return
    end
  end
  log.info(UNSUPPORTED_FT)
end

function M.debug_main_in_file(opts)
  log.debug('Debug Main in File')
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)
  bu.save_current_buffer()

  local lhandler = Lang.getLang(ctx.bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(ctx.bufname):debugSingleSrcFile(ctx.bufname)
    return
  end

  log.info(UNSUPPORTED_FT)
end

-- run test via project build system for current (buffer) opened test-file
-- uri file:///home/user/dev/toolset/src/main/java/org/pkg/utils/Utils.java
-- bufname       /home/user/dev/proj/src/main/java/org/pkg/utils/Utils.java
-- project_root  /home/user/dev/proj
---@diagnostic disable-next-line: unused-local
function M.run_test_on_file(opts)
  log.debug('Run Single Test File')
  -- opts = setup.get_extended_config(opts)
  local bufname = api.nvim_buf_get_name(0)
  -- local ctx = make_context(opts.bufnr)
  bu.save_current_buffer()

  -- if is_file(ctx, ".lua") then
  --   return lua.run_test_file(ctx, opts)
  -- else
  local lhandler = Lang.getLang(bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(bufname):runSingleTestFile(bufname)
    return
  end
  -- end

  log.info(UNSUPPORTED_FT)
end

-- run the test file in debugging mode
function M.debugging_test_for_file(opts)
  log.debug('Debugging Single Test File')
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)
  bu.save_current_buffer()

  local lhandler = Lang.getLang(ctx.bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(ctx.bufname):debugSingleTestFile(ctx.bufname)
    return
  end

  log.info(UNSUPPORTED_FT)
end

-- toggling between test|sources files with quick jump to the coresponding pair
-- find and open test for opened source
function M.goto_test_or_source(opts)
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)
  if ui.has_back_src(ctx) then -- jump back from a report of the runned testfile
    ui.open_back_src(ctx)
    return
  end
  -- lua.open_test_or_src(ctx, opts) -- TODO remove
  local lhandler = Lang.getLang(ctx.bufname)
  log.debug('Found Lang for buf %s %s', ctx.bufnr, lhandler ~= nil)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(ctx.bufname):openTestOrSource(ctx.bufname)
  else
    log.info(UNSUPPORTED_FT)
  end
end

-- quickly writing tests helpers
function M.t_use_passedin()
  local bufname = api.nvim_buf_get_name(0)

  local lang = Lang.getLang(bufname)
  if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
    local ok, err = lang:resolve(bufname):usePassedIn()
    if not ok and err then print(err) end
  else
    log.info(UNSUPPORTED_FT)
  end
end

--
-- :EnvTestUpdateDiagnostic
--
-- for selt debugging. update test diagnostic message from the test-file-report
--
function M.t_update_diagnostic(opts)
  local bufname = api.nvim_buf_get_name(0)

  local lang = Lang.getLang(bufname)
  if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
    local w = c4lv.newCmd4Lua(opts)
    w.vars.bufname = bufname
    w.vars.bufnr = api.nvim_get_current_buf()
    local ok, err = lang:resolve(bufname):updateTestDiagnostic(w)
    if not ok and err then print(err) end
  else
    log.info(UNSUPPORTED_FT)
  end
end

-- :EnvGrepCurrentWord
-- arg1 - dir defualt is cwd
-- arg2 - word defualt auto pick from current context(under the cursor)
---@param opts table?
function M.grepCurrentWord(opts)
  local args = (opts or E).fargs or E
  local cwd = args[1] or vim.loop.cwd()
  local word = args[2] or Editor:new():getContext():resolveWords().word_element
  local has_plugin = btelescope.has_telescope()

  log.debug("grepCurrentWord dir:%s word_to_find:'%s'", cwd, word, has_plugin)

  if not word then return false end

  if not has_plugin then
    return print('not found plugin: telescope')
  end

  btelescope.live_grep(cwd, word)
end

-- check to EmbededLuaTester inside another Lang
---@param lang env.lang.Lang
---@param resolvedLang env.lang.Lang
---@return env.lang.Lang
local function checkEmbededLuaTestsCase(server, lang, resolvedLang, bufname)
  local f = false
  local cntl, cnrl
  if resolvedLang ~= lang and server == 'lua_ls' then
    cntl = class.name(lang)         -- temp
    cnrl = class.name(resolvedLang) -- resolved
    if cntl ~= cnrl and cntl == class.name(LuaLang) then
      -- class.name(resolvedLang) ~= class.name(lang)
      lang:findProjectBuilder(bufname)
      ---@diagnostic disable-next-line: param-type-mismatch
      Editor.echoInStatus(nil, 'Delected EmbededLuaTester' +
        tostring(lang.project_root) + " " + tostring(bufname))

      resolvedLang = lang -- back to LuaLang
      f = true
    end
  end
  log.debug("checkEmbededLuaTestsCase ", server, f, cntl, cnrl, bufname)
  return resolvedLang
end

--
-- update a settings on attach to a lsp server
--
-- Goal: transfer the settings specified in the project root to the LSP server
-- for example, to specify paths to all libraries used in the project
-- (i.g. paths for modules specified in the rockspec-file in dependencies-block)
--
---@param opts table - all options merged into one table for attach to lsp server
---@param server string -- name of the lsp-server
function M.update_lsp_settings_on_attach(server, opts)
  local bufname = api.nvim_buf_get_name(0)
  log.debug("update_lsp_settings_on_attach for %s %s", server, bufname)
  assert(type(opts) == 'table', 'conf')

  -- See: doc/dev-notes/nvim.md
  if log.is_debug() then
    local varname = 'LSP_' .. tostring(server)
    log.debug('put options into ', varname)
    _G[varname] = opts
  end

  local lang = Lang.getLang(bufname)
  if class.instanceof(lang, Lang) then ---@cast lang env.lang.Lang
    local resolvedLang = lang:resolve(bufname)
    resolvedLang = checkEmbededLuaTestsCase(server, lang, resolvedLang, bufname)
    resolvedLang:updateLspSettings(opts)
  else
    log.debug('WARN not found handler for buf', bufname, lang)
  end

  return opts
end

function M.refresh_buildscript()
  log.debug("refresh_buildscript")
  local path = vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf())
  local lang = Lang.findOpenProject(path)
  if lang then
    lang:refreshBuildscript(path)
  end
end

-- When you run the test file, a new window with a report opens
-- this window will automatically be attached to the buffer for which it was
-- called with the ability to quickly jump-back from repot into test
-- For quick navigation between file buffers:   src<->test<->report
--
function M.goto_attached_buf(opts)
  local ctx = make_context(opts.bufnr)
  if ui.has_attached_buf(ctx) then
    ui.open_attached_buf(ctx)
  end
end

-- Just Attach to already started external Debugger
function M.attach_to_debugger(opts)
  log.debug('Attach to Debugger')
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)

  local lhandler = Lang.getLang(ctx.bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(ctx.bufname):attachToDebugger(ctx.bufname)
  else
    log.info(UNSUPPORTED_FT)
  end
end

function M.validate_debugger(opts)
  log.debug('Validate Debugger')
  opts = setup.get_extended_config(opts)
  local bufname = api.nvim_buf_get_name(0)

  local lhandler = Lang.getLang(bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(bufname):validateDebugger(bufname)
  else
    log.info(UNSUPPORTED_FT)
  end
end

function M.sync_project_settings(opts)
  log.debug('Sync Project Settings')
  opts = setup.get_extended_config(opts)
  local bufname = api.nvim_buf_get_name(0)

  local lhandler = Lang.getLang(bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    lhandler:resolve(bufname):syncProjectSettings(bufname)
  else
    log.info(UNSUPPORTED_FT)
  end
end

function M.code_check(opts)
  log.debug('Code Check')
  opts = setup.get_extended_config(opts)
  local bufname = api.nvim_buf_get_name(0)

  local lhandler = Lang.getLang(bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    local w = c4lv.newCmd4Lua(opts)

    w:root(':EnvCodeCheck')
    lhandler:resolve(bufname):codeCheck(bufname, w)
  else
    log.info(UNSUPPORTED_FT)
  end
end

-- For Project of the currently opened file
-- Run Debbugger via the BuildSystem and attach to it
function M.debugging_project(opts)
  log.debug('Debugging Project')
  error('Not implemented yet ' .. tostring(opts))
  -- opts = setup.get_extended_config(opts)
  -- local ctx = make_context(opts.bufnr)
  -- if java.is_java_project(ctx) then
  --   java.debugging_project(ctx, opts)
  -- else
  --   log.info(UNSUPPORTED_FT)
  -- end
end

---@param opts table{args, fargs}
function M.goto_source_file(opts)
  local w = c4lv.newCmd4Lua(opts)
  w:about('GotoSource jump to source file from current context')

  w:desc('by input from clipboard but not from a current line in the opened file')
      :v_has_opt('--from-clipboard', '-c')

  local line = w:desc('by given input'):opt('--input', '-i')

  if not w:is_input_valid() then return false end

  if not line and w.vars.from_clipboard then
    line = vim.fn.getreg("*") or vim.fn.getreg("+") or false
    log.debug('goto_source_file via clipboard: ', line)
  end
  if not line or line == '' then line = nil end

  local bufname = Editor.getCurrentFile()
  -- custom for yaml files
  if su.ends_with(bufname, '.yml') or su.ends_with(bufname, '.yaml') then
    require 'env.util.yaml'.jump_to_ref('.')
    return
  end


  Lang.performGotoDefinition(line)
end

-- Get Class(Module) name for opened or given full-path to source file
-- print and copy to clipboard
function M.get_source_name(opts)
  log.debug('Source Name')
  opts = setup.get_extended_config(opts)
  local ctx = make_context(opts.bufnr)
  if opts.fargs and opts.fargs[1] then
    ctx.bufname = opts.fargs[1]
  end

  local lhandler = Lang.getLang(ctx.bufname)
  if class.instanceof(lhandler, Lang) then ---@cast lhandler env.lang.Lang
    ctx.class_name = lhandler:resolve(ctx.bufname):sourceNameOf(ctx.bufname)
  end

  if ctx.class_name then
    cb.copy_to_clipboard(ctx.class_name)
  end
  print(ctx.class_name or 'Cannot determine the source name')
end

-- ":EnvNew :EnvProject[new|info|build|run|test|debug]" in the env.setup
function M.add_commands()
  -- , function(opts) M.cmd_new(opts) end, {nargs = '*' })
  cu.reg_cmd("EnvMake", umake.run, { nargs = "*" })
  cu.reg_cmd("EnvGradle", cmd_gradle.handle, { nargs = "*" })
  cu.reg_cmd("EnvRunTestFile", M.run_test_on_file, { nargs = '*' })
  cu.reg_cmd("EnvDebugTestFile", M.debugging_test_for_file)
  cu.reg_cmd("EnvRunMainInFile", M.run_main_in_file)
  cu.reg_cmd("EnvDebugMainInFile", M.debug_main_in_file)
  cu.reg_cmd("EnvAttachToDebugger", M.attach_to_debugger)
  cu.reg_cmd("EnvValidateDebugger", M.validate_debugger, {})
  cu.reg_cmd("EnvCodeCheck", M.code_check, { nargs = "*" }) -- static analizy, lint, etc

  -- Open File by given classname (via arg) if no args - goto file from curline
  -- for 'gs' key bilding. Quick jump to source of some definition like trace
  -- Aka key gd -- vim.lsp.buf.definition but fro
  cu.reg_cmd("EnvGotoSource", M.goto_source_file, { nargs = "*" })
  cu.reg_cmd("EnvGotoAttachedBuf", M.goto_attached_buf, { nargs = "*" })
  -- jump to source-file from test-file or to test from source
  cu.reg_cmd("EnvToggleTestSourceJump", M.goto_test_or_source)
  -- Full Class Name of Opened or given File -- sourcefile to classname(module)
  cu.reg_cmd("EnvSourceNameOf", M.get_source_name, { nargs = "*" })
  cu.reg_cmd("EnvSyncProjectSettings", M.sync_project_settings, { nargs = "*" })

  cu.reg_cmd("EnvJdtlsGenConstructors", nvim_jdtls.genContructorsPrompt)

  --
  -- dev & debug
  cu.reg_cmd("EnvExperiment", require('env.research').pick_one_action, { nargs = "*" })

  -- helpers for writing tests - substiture actual results from testing framework
  cu.reg_cmd("EnvHUsePassedIn", M.t_use_passedin, { nargs = "*" })
  cu.reg_cmd("EnvTestUpdateDiagnostic", M.t_update_diagnostic, { nargs = "*" })

  cu.reg_cmd("EnvGrepCurrentWord", M.grepCurrentWord, { nargs = "*" })
  --
end

return M

-- 08-08-2024 @author Swarg
--
--
local class = require 'oop.class'
local interface = class.interface -- or require 'oop.interface'

class.package 'olua.util'
---@class olua.util.Iterator : oop.Object
---@field new fun(self, o:table): olua.util.Iterator
---@field hasNext fun(self): boolean
---@field next fun(self): any
---@field free fun(self): any -- to free resources (e.g. for FileIterator)
local I = interface.Iterator {

  ---@return boolean
  hasNext = interface.method('self', ':', 'boolean'),
  next = interface.method('self', ':', 'any'),
  free = function() end,
}

interface.build(I)
return I

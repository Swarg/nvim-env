-- 30-09-2024 @author Swarg

local log = require 'alogger'
local class = require 'oop.class'

local Iterator = require 'olua.util.Iterator'

local log_debug = log.debug

--
--
-- "inner" class
class.package 'olua.util'
---@class olua.util.FileIterator: olua.util.Iterator
---@field new fun(self, o:table?, path:string, out:table?): olua.util.FileIterator
---@field hfile userdata
---@field next_line string?
---@field lnum number
local C = class.FileIterator:implements(Iterator) {
  -- constructor
  ---@param path string
  ---@param out table
  _init = function(self, path, out)
    log_debug("Iter._init path:%s", path)
    local hfile, err = io.open(path, 'rb')
    assert(hfile, 'error on open: ' .. tostring(path) .. ' ' .. tostring(err))
    self.hfile = hfile
    self.next_line = self.hfile:read('*l')
    self.lnum = 0
    if type(out) == 'table' then
      self.out = out
      out[#out + 1] = self.next_line
    end
  end,

  ---@return boolean
  hasNext = function(self)
    return self.next_line ~= nil
  end,

  next = function(self)
    if self.hfile == nil then
      local next = self.next_line
      self.next_line = nil
      return next
    end

    local next = self.next_line
    self.next_line = self.hfile:read('*l')
    if self.out then self.out[#self.out + 1] = self.next_line end

    if self.next_line == nil then
      self.hfile:close()
      self.hfile = nil
    end

    self.lnum = self.lnum + 1
    return next
  end,

  -- close opened file descriptor - free resources
  free = function(self)
    if self.hfile then
      assert(self.hfile.close(), 'on close')
      self.hfile = nil
    end
  end
}

class.build(C) -- check interface impl
return C

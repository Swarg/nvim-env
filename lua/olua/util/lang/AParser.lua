-- 14-08-2024 @author Swarg
-- abstract class of stream parser aimed at line-by-line parsing of source codes
-- Goals:
--   - interface
--   - encapsulation of the common low-level logic of
--     line-by-line parsing of source codes
--
-- As usage example see implementations:
--   - env.langs.java.JParser
--

local log = require 'alogger'
local class = require 'oop.class'
local ErrLogger = require 'olua.util.ErrLogger'
local Iterator = require 'olua.util.Iterator'

class.package 'olua.util.lang'
---@class olua.util.lang.AParser : oop.Object
---@field new fun(self, o:table?, iter:olua.util.Iterator?, offset:number?): olua.util.lang.AParser
---@field iter olua.util.Iterator
---@field elog olua.util.ErrLogger
---@field remainder_line string?
---@field remainder_line_col number?
---@field last_elm table? -- last added element in parsting process
local C = class.new_class(nil, 'AParser', {})

--
-- constructor
--
-- to call from inherided class use:
--   self:super(iter, offset)
--
---@param iter olua.util.Iterator
function C:_init(iter, offset)
  log.debug("_init iter:%s offset:%s", iter, offset)

  self.remainder_line, self.remainder_line_col = nil, nil
  self.curr_line = nil -- for continue
  self.last_elm = nil

  if iter then
    self:setIterator(iter, offset)
  end
end

--
-- parsing the entire file or up to the point at which the predicate return true
--
---@param predicate function?
---@return self
---@diagnostic disable-next-line: unused-local
function C:prepare(predicate) error('abstract ') end

---@return table
function C:factory() error('abstract ') end

-- parsing result
---@return table
function C:result() error('abstract ') end

---@param find string
---@return table{match} - userdata lpeg patt
---@diagnostic disable-next-line: unused-local
function C:getPatternFor(find) error('abstract') end

---@param line string
---@return table?
---@diagnostic disable-next-line: unused-local
function C.splitLineToLexemes(line) error('abstract') end

---@return table
function C.lexer() error('abstract') end

---@param line string
---@diagnostic disable-next-line: unused-local
function C.parseLineStacktrace(line) error('abstract') end

---@param line string
---@diagnostic disable-next-line: unused-local
function C.parseLineCompilerError(line) error('abstract') end

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug, log_trace = log.debug, log.trace
log_debug = log_debug


---@param iter olua.util.Iterator
---@param offset number?
---@return self
function C:setIterator(iter, offset)
  assert(class.instanceof(iter, Iterator), 'expected Iterator got:' ..
    v2s(class.name(iter)))
  self.iter = iter
  self.lnum = offset or 0
  return self
end

---@return olua.util.ErrLogger
function C:getErrLogger()
  self.elog = self.elog or ErrLogger:new()
  return self.elog
end

---@return boolean
function C:hasErrors()
  return self.elog ~= nil and self.elog:hasErrors()
end

function C:error(fmsg, ...)
  self:getErrLogger():error(fmsg, ...)
end

---@param line string?
---@return boolean
---@diagnostic disable-next-line: unused-local
function C.isCommented(line) return false end

---@param s string?
---@return boolean
function C.isEmpty(s)
  return s == nil or s == '' or match(s, '^%s*$')
end

---@param s string?
---@return boolean
function C:isEmptyOrComment(s)
  return C.isEmpty(s) or self.isCommented(s)
end

--------------------------------------------------------------------------------
----              Stuff to improve builtin Iterator
--------------------------------------------------------------------------------

local MAX_INT = 2 ^ 32 / 2 -- ? -2... 0 +2...

---@protected
---@return boolean
function C:_has_next()
  return self.remainder_line ~= nil or self.iter:hasNext() == true
end

---@private
---@return boolean
function C:_has_remainder_line()
  local len = #(self.remainder_line or '')
  local pos = self.remainder_line_col or MAX_INT
  return len > 0 and pos <= len
end

local function ensure_remainder_line_processed(self)
  if self:_has_remainder_line() then
    local s = v2s(self.remainder_line:sub(self.remainder_line_col))
    if s ~= "\r" then
      error(fmt('has remainder:|%s| len:%s byte:%s', s, #s, v2s(string.byte(s))))
    end
  end
  return true -- sure empty
end

--
-- move remainder from current line to self.remainder_line
-- for _next_line_up_to
--
---@private
---@param col number? -- continue_col position in current line
function C:_set_remainder(col)
  local cline = self.curr_line
  local f = false
  ensure_remainder_line_processed(self)

  if type(col) == 'number' and cline ~= nil and col > 0 and col <= #cline then
    -- self.remainder_line = cline:sub(col, #cline)
    self.remainder_line = cline --
    self.remainder_line_col = col
    self.curr_line = nil        -- ?
    f = true
  end
  if log.is_debug() then
    local rems = self:_get_remainder_sub(1, -1)
    log_trace("_set_remainder continue_col:%s is-set:%s new rems:|%s| cline:|%s| ",
      col, f, rems, cline)
  end

  return f
end

---@private
---@return string?
function C:_get_remainder()
  return self.remainder_line
end

---@private
---@param pstart number
---@param pend number
---@return string?
function C:_get_remainder_sub(pstart, pend)
  local sub = nil
  pstart, pend = pstart or 1, pend or 1

  if pstart > 0 and pend and self:_has_remainder_line() then
    local offset = self.remainder_line_col
    if pend < 0 then -- to support from end of the line
      pend = (#self.remainder_line - offset) + (pend + 2)
    end
    sub = self.remainder_line:sub(offset + (pstart - 1), offset + (pend - 1))
  end
  local f = self:_has_remainder_line()
  log_trace("_get_remainder_sub(%s:%s) got:|%s| from rems:|%s| offset:%s ",
    pstart, pend, sub, self.remainder_line, self.remainder_line_col, f)
  return sub
end

---@param pstart number
---@param pend number
function C:_get_curr_line_sub(pstart, pend)
  if self.curr_line and pstart and pend then
    return self.curr_line:sub(pstart, pend)
  end
  return nil
end

--
---@protected
---@return string?
---@return number?
function C:_next_line()
  if self.remainder_line and self.remainder_line_col then -- remainder from last time
    local line = self:_get_remainder_sub(1, -1)
    local col = self.remainder_line_col
    self.remainder_line, self.remainder_line_col = nil, nil -- clear
    self.curr_line = line                                   -- ?
    log_trace("_next_line from remainder without itet:next() is:|%s|", line)
    return line, col
  end
  -- skipp all empty lines
  while true do
    self.lnum = self.lnum + 1
    local line = self.iter:next()
    -- self.prev_line =
    if line == nil then
      break
    end
    if not self:isEmptyOrComment(line) then
      self.curr_line = line
      return line, 0
    end
  end

  return nil, -1 -- EOF
end

--
-- return a substring from the beginning to a given position(col) and
-- remembering the remainder of the string
--
---@param self self
---@param pos number
---@param line string
---@return string, number
local function get_sub_and_remember_remainder(self, line, pos)
  self.remainder_line = line:sub(pos + 1, #line)
  self.remainder_line_col = 0

  if self.remainder_line == '' then
    self.remainder_line, self.remainder_line_col = nil, nil
  end
  log_trace("set remainder_line col:%s sz:%s rems:|%s|",
    pos, #line, self.remainder_line)

  local next_line = line:sub(1, pos)

  log_trace("found next_line is:|%s| lnum:%s", next_line, self.lnum)
  return next_line, self.lnum
end

--
-- return concatenation of lines up to first match of given pattern
--
---@private
---@param find string ) } ;
---@param continue_pos number? - curr_line_remains_offset (column)
function C:_next_line_up_to(find, continue_pos)
  log_trace("_next_line_up_to |%s| continue:%s cline:|%s| lnum:%s rems:|%s|",
    find, continue_pos, self.curr_line, self.lnum, self.remainder_line) --, '::BACKTRACE::')

  assert(type(find) == 'string' and find ~= '', 'pattern')

  local patt = self:getPatternFor(find)

  local s = ''
  while (self.iter:hasNext()) do
    -- Note: if there is a remainder_line then it will be returned without
    -- actually calling self.iter:next() to get the next line
    -- So the remainder of the current line will be checked automatically if
    -- the function self:_set_remainder is applied to the remainder (self.curr_line
    local line = self:_next_line()
    if line ~= nil and line ~= '' then
      if s ~= '' then s = s .. "\n" end
      s = s .. line
      -- Note that the lpeg pattern is always matched not just for one current
      -- line, but for all accumulated lines, this is important, for example,
      -- when you need to find the end of a parenthesis in a complex nested
      -- expression spread over several lines
      local pos = patt:match(s) -- lpeg.match(patt, s)
      -- log_debug("%s line-to-match:|%s| matched-to-pos:%s", self.lnum, s, pos)

      if pos then
        return get_sub_and_remember_remainder(self, s, pos)
      end
    end
  end

  log_trace("_next_line_up_to ret lnum:%s line:|%s|", self.lnum, s)
  return s, self.lnum
end

--
-- continue_col is a position of the char in the current line from Iterator
-- in witch one of the patterns stop matching
-- continue means what pattern need more input to resolve some syntax construction
--
---@protected
---@return number?
function C:_pull_continue(e)
  local continue_col = nil
  if type(e) == 'table' then
    continue_col = e.continue
    e.continue = nil
  end
  if log.is_debug() then
    local col_char = nil
    if self.curr_line and continue_col ~= nil then
      col_char = (self.curr_line):sub(continue_col, continue_col)
    end
    log_trace("pulled continue_col:%s char:|%s|", continue_col, col_char)
  end

  self:_set_remainder(continue_col) -- for case like "(\n ...)"

  return continue_col
end

--------------------------------------------------------------------------------

--
-- when the parsing stop signal is triggered by predicate callback,
-- it calls this method to fill in the position at which it stopped
--
---@protected
---@param line string?
function C:setStoppedAt(line)
  log_trace("setStoppedAt", self.lnum, line)
  self.stopped_line, self.stopped_at_lnum = line, self.lnum
  return self
end

--
-- line number and contents of the line at which source code parsing was stopped
--
---@protected
---@return number? stoppedAt lineNum
---@return string?
---@return number? current line number
function C:getStoppedAt()
  return self.stopped_at_lnum, self.stopped_line
end

-- line number in the source code that the parser has currently reached
---@return number
function C:getLnum()
  return self.lnum
end

-- true - when parsing was stopped by a predicate
---@return boolean
function C:isStopped()
  log_trace("isStopped", self.stopped_at_lnum)
  return self.stopped_at_lnum ~= nil
end

--
-- to continue parsing from the line number where the parser was stopped
--
---@return self
function C:clearStopped()
  self.stopped_line, self.stopped_at_lnum = nil, nil
  return self
end

--
-- create StopParsingInfo
--
---@param stop_predicate function?
---@return table
function C:getStopInfo(stop_predicate)
  return {
    -- a condition that led to stopping parsing of the entire source code
    predicate = stop_predicate,
    lnum = self.stopped_at_lnum,
    line = self.stopped_line,
    -- last parsed element (lexeme) from the source
    elm = self.last_elm,
  }
end

--
-- return the result of the parsing  (e.g. clazz)
--
---@return table
function C:getResult() error 'abstract' end

--------------------------------------------------------------------------------

C.protected = {}
local CP = C.protected

function CP.has_next_token(tokens)
  if tokens then
    tokens.i = tokens.i or 0
    return tokens.i < #tokens
  end
  return false
end

---@return string?
---@return table?
function CP.next_token(tokens)
  tokens.i = (tokens.i or 0) + 1 -- next
  local token = (tokens or E)[tokens.i]

  if type(token) == 'table' then
    if type(token) == 'number' then
      error("next_token is number:", token, require "inspect" (tokens))
    end
    local tag = token[1]
    -- tag one of grammar.tags
    -- items just next element - for package it will be packagename,
    -- for import [static] or full classname
    return tag, token
  end

  return nil, nil -- no more tokens
end

class.build(C)
return C

-- 14-08-2024 @author Swarg
-- Iterator over simple table

local class = require 'oop.class'
local Iterator = require 'olua.util.Iterator'

class.package 'olua.util'
---@class olua.util.IteratorImpl: olua.util.Iterator
---@field new fun(self, o:table?, list:table?, lnum:number?): olua.util.IteratorImpl
---@field o table
local C = class.IteratorImpl :implements(Iterator) {
  -- constructor
  ---@param lines table
  ---@param offset number?
  _init = function(self, lines, offset)
    assert(type(lines) == 'table', 'lines')

    self.o = { -- state
      lines = lines,
      n = offset or 0,
    }
  end,

  ---@return boolean
  hasNext = function(self)
    return self.o.n < #self.o.lines
  end,

  next = function(self)
    local o = self.o
    o.n = o.n + 1
    return o.lines[o.n]
  end,
}

class.build(C) -- check interface impl
return C


-- 17-07-2024 @author Swarg
-- Goal:
-- - save generated content to specified files with logging of how
--   the file was written


local log = require 'alogger'
local class = require 'oop.class'

class.package 'olua.util'
---@class olua.util.ErrLogger : oop.Object
---@field new fun(self, o:table?): olua.util.ErrLogger
---@field entries table?
local C = class.new_class(nil, 'ErrLogger', {
})


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
-- local log_debug, log_trace = log.debug, log.trace

-- Constructor
function C:_init()
end

local function is_empty_str(s)
  return not (type(s) == 'string' and s ~= '' and string.match(s, "^%s*$") == nil)
end

---@param fmsg string
---@return boolean
---@return string
function C:error(fmsg, ...)
  self.entries = self.entries or {}
  local msg = log.format(fmsg, ...)
  self.entries[#self.entries + 1] = msg
  return false, msg
end

---@return string
function C:getReadableReport(prefix)
  local s = ''
  if self.entries and #self.entries > 0 then
    for _, err in ipairs(self.entries) do
      if s ~= '' then s = s .. "\n" end
      s = s .. err
    end
    if not is_empty_str(prefix) then
      s = prefix .. "\n" .. s
    end
  end
  return s
end

function C:clean()
  self.entries = nil
end

---@return boolean
function C:hasErrors()
  return #(self.entries or E) > 0
end

class.build(C)
return C

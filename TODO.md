## tasks

To Rewrite

- env.util.code_parser.project_of_buf         - to use Lang Editor Context
- env.sourcecode: call_func_and_print_to_buf  - to use new print_wrapper

- Cmd4Lua build tree of all commands and subcommands

```lua
    local exp = 'some-value'         -- 1
    assert.same(exp, res)            -- 2 <<  used here

    local res = do_stuff("123")      -- 3
    assert.same('wrong-value', res)  -- << for this line it broke exp in line:1
```

tokienizer:
- add custom handler to support comments block -- \n  and --[[ ]] [[]] [==[]==]

tbl-viewer:
- save changed table to the file hook

- Command to clear library cache
(Luarocks dependencies passed into lua-ls in each lua-file opening)

- Command "close project" (clear project cache and library cache(dependencies))
For now, a complete reload of the nvim-env plugin is suitable for this purpose:
:EnvReload

TODO
to clean up EnvCallFunc run and verbose in work with quoted strings
the difficulty is that in the second case(EnvCallFunc verbose) the transfer of
quotes becomes too complicated:  `xterm -c sh '".."'`
run external terminal and run os-cmd in new buff issue with quoted lines
examples:
```sh
# todo fix issue with auotes in nvim-env
jq -nc '"WHAT'\''" | test("^[[:upper:][:punct:]]+$")'
jq -nc '"WHAT IS" | test("^[[:upper:][:space:][:punct:]]+$")'
```



todo impl:
elixir/comman/new_project cmd_mix_exs

- todo auto add or ask from user to add new flat project into per-dir-settings
  on :EnvProject new <lang> flat --new-project

- :EnvProject new java mag
  pass to generated pom.xml:
  - java version
  - main class
  - dependencies?

on :EnvProject close clear cache in env.bridge.eclipse

It looks like this can already be deleted as unnecessary
`argsTestSingleClass(class_name, opts, method_name)`

[x] rewrite env.goto_source_file to use env.lang.Lang (java)

[-] toggle src->test auto jump by method/func name (4lua)


[-] JParser and grammars multiline class definition with extends, implements


- [+] fixed :EnvLineInsert sample for 0 item works only with :sp before
      and added subcmd `:EnvLineInsert samples`
- [-] wizard for Samples
- [+] decomp_manager raise error on java.lang.UnsupportedClassVersionError
      (as remained that you foget to switch from the java8 to the java17+)


- [-] env.langs.java.command.decompile implement interact with jvm

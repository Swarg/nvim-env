## luarocks - deployment and management system for Lua modules

https://github.com/luarocks/luarocks/wiki/

## See description
```sh
apt show luarocks
```

## How to configure lua-ls to support packages from luarocks

~/.dotfiles/nvim/.config/nvim/lua/user/lsp/settings/lua_ls.lua
```lua
return {
  settings = {
    Lua = {
      -- ...
      workspace = {
        checkThirdParty = false, -- Solve issue 'Do you need to conf.. luv ?'
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.stdpath "config" .. "/lua"] = true,
          ["${3rd}/busted/library"] = true,
          ["${3rd}/luassert/library"] = true,

          -- to support the pakages installed via luarocks
          -- to see actual path to needed package use: "luarocks show cmd4lua"
          ['/usr/local/share/lua/5.1/'] = true,
        },
      },
      -- ..
    },
  },
}
```

This can also be modified when you configure lua_lsp with lspconfig:

```lua
require('lspconfig').lua_ls.setup {
  settings = {
    Lua = {
      -- ...
      workspace = {
        -- ...
        library = { ... }
      },
    },
  },
}
```


For example:
```sh
luarocks show cmd4lua
```
Output
```
Modules:
	cmd4lua (/usr/local/share/lua/5.1/cmd4lua.lua)
	cmd4lua.base (/usr/local/share/lua/5.1/cmd4lua/base.lua)
  ...
```


```sh
cat /etc/luarocks/config.lua
```
```
rocks_trees = {
   home..[[/.luarocks]],
   [[/usr/local]]
}
```

```sh
luarocks
CONFIGURATION
	Lua version: 5.1
	Configuration files:
		System: /etc/luarocks/config.lua (ok)
		User  : /home/user/.luarocks/config-5.1.lua (not found)

	Rocks trees in use:
		/home/swarg/.luarocks
		/usr/local
```


Another way is to modify the .luarc.json like:
```json
{
    "Lua": {
        "workspace.checkThirdParty": false
    }
}
```


## How to get path to installed rockspec file

list of installed rocks(packages) (with filter)

```sh
luarocks list --porcelain cmd4lua
```
```
cmd4lua	0.4.3-1	installed	/usr/local/lib/luarocks/rocks
```

the full path of the rockspec file:
```sh
luarocks show --rockspec cmd4lua
/usr/local/lib/luarocks/rocks/cmd4lua/0.4.3-1/cmd4lua-0.4.3-1.rockspec
```

data directory of the installed rock:
```sh
luarocks show --rock-dir cmd4lua
/usr/local/lib/luarocks/rocks/cmd4lua/0.4.3-1
```

/usr/local/lib/luarocks/rocks/ROCKNAME/ROCKVER/ROCKNAME-ROCKVER.rockspec


## Write rockspec for own rock-package

```lua
package = "cmd4lua"
version = "0.4.3-1"
--         ^^^^^ ^.
--           \     `rockspec_revision
--            package_version
-- (package_version = "0.4.3")
-- (rockspec_revision = "1")

source = {
  url = "git+https://github.com/account/project.git",
  tag = "v0.4.3",
  -- branch = "master",
  -- ...
}
```


> What are rockspec revisions for?

The rockspec revision is the versioning of the `rockspec` file itself.
Suppose you release Foo version 1.0; you create a rockspec `foo-1.0-1.rockspec`.
Later you learn that to get Foo 1.0 to compile in FreeBSD, you need to pass an
extra `-D` flag; the source code needs no changes at all.
You edit the rockspec adding a
[platform override section](https://github.com/luarocks/luarocks/wiki/Platform-overrides)
and re-submit it to [luarocks.org](https://luarocks.org) as `foo-1.0-2.rockspec`.

### what does scm stand for instead of version number

Keeping the latest `scm` rockspec at the root has the slight advantage that
it is automatically picked up by `luarocks make`
if one wants to make a build from a local checked-out tree.
i.g. `pakage-scm-1.rockspec`


### For what rockspecs/ directory in the root of the project

the `rockspecs/` directiry in the repo containing the rockspecs for specific
releases: users can then use those files directly if they wish so,
by using commands such as
`luarocks install https://raw.github.com/account/projname/v0.2/rockspecs/pkg-0.2-1.rockspec`
(that is using the raw.github.com pathname).

Therefore, in the rockspec files of past releases, a version tag must be
specified, and the same tag must be specified in the git repository itself.
```lua
version = "0.4.3-1"
source = {
  url = "git+https://github.com/account/project.git",
  tag = "v0.4.3",
}
```

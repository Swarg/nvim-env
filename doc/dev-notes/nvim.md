## Lua-ls and NVim

how it configured in nvim

```lua
local conf_opts = {
  settings = {
    Lua = {
      runtime = {
        version = 'LuaJIT',
        -- path = runtime_path,
      },
      -- ...
      workspace = {
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.stdpath "config" .. "/lua"] = true,
          ["${3rd}/busted/library"] = true,
          ["${3rd}/luassert/library"] = true,
        },
      },
      --...
    }
  }
}

  --  ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/mason.lua
  lspconfig = require("lspconfig")
	lspconfig['lua_ls'].setup(conf_opts)
  --        In fact, this table ^ is loaded via require from file:
  -- ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/settings/lua_ls.lua
```

Note:
Here are the basic settings that will be applied to all opened files of a given
language (Lua) That is, this place is not suitable for specifying settings for
a specific project. In the example above, we are specifying settings for
convenient work with NVim configs and NVim plugins(written in Lua).

example with real values:

```lua
-- ...
workspace = {
  checkThirdParty = false,
  library = {
    ["/usr/share/nvim/runtime/lua"] = true
    ["/home/user/.config/nvim/lua"] = true,
    ["${3rd}/busted/library"] = true,
    ["${3rd}/luassert/library"] = true,
  }
}
```


at the first stage I found a simple way to register the lua-ls server to
support the new package installed via luarocks.
Just add the path in with all luarocks packages is installed:
```lua
      workspace = {
        library = {
          [vim.fn.expand "$VIMRUNTIME/lua"] = true,
          [vim.fn.stdpath "config" .. "/lua"] = true,
          ["${3rd}/busted/library"] = true,
          ["${3rd}/luassert/library"] = true,
          -- to support pakages installed via luarocks
          ['/usr/local/share/lua/5.1/'] = true,
      }
      }
```
to see the path of a needed package use:
```sh
luarocks show pkg_name
```
But this approach has a big drawback
When you trying to edit a project that is installed in the system through the
luarocks, warnings about duplication appear.
This is happening because Lsp is going crazy. Since it loads the same package
both from the local repository where the package is installed via luarocks and
from the current project. But in reality it is one and the same project.

To solve this problem, I added the ability to add paths of the necessary modules
on the fly. Dependencies(modules) are taken from specified in the rockspec-file
data.
Then, using the `luarocks show <package>` command, it request the paths to all
dependency modules and add them to workspace.library.
These paths will then be picked up by the LSP server.

This way, only the necessary dependencies are connected and not all at once.
And for Projects in which the installed rock-package is located, there is no
duplication of code for the LSP server

approximately how this can be done through the nvim config:

```lua
-- ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/handlers.lua:
M.on_attach = function(client, bufnr)

  local ok_env, env = pcall(require, 'env')
  if ok_env then env.update_lsp_settings_on_attach(client.name, client) end
  -- ...
end
```


```lua
-- List of all installed and used LSP-servers
local servers = { "lua_ls", "clangd", "bashls", ... }
-- ...
for _, server in pairs(servers) do
	opts = {
		on_attach = handlers.on_attach,
    --                   ^^^^^^^^^   <--  callback function --^
	}

  -- ~/config/nvim/lua/user/lsp/settings/ + server + .lua
	local require_ok, conf_opts = pcall(require, "user.lsp.settings." .. server)
	if require_ok then
		opts = vim.tbl_deep_extend("force", conf_opts, opts)
	end

	lspconfig[server].setup(opts)
  -- ...
end
```

Note:
on_attach is triggered every time a file is opened and placed in the vim buffer
That's why I wrote a cache to store libraries
otherwise, for every new open file in the project, a full cycle of searching
for paths to dependency libraries will occur.



## How to get all-options for lsp-server (with workspace.libraries):

```sh
:EnvLog set level debug
# open a new lua file to trigger attach to lsp and update_lsp_settings_on_attach
:EnvInspectTable view -G -open LSP_lua_ls config settings Lua workspace library
:EnvLog open
# turn off debug mode
:EnvLog set level info
```


Another way to specify dependencies to the lsp server for a specific project is
to place the .luarc.json file in the project root:

```json
{
  "Lua": {
    "workspace": {
      "library": ["/usr/local/share/lua/5.1/"]
    }
  }
}
```

But this manual method is not convenient for vim plugins and configs.
Since you need to manually write the full absolute paths both to the files of
NVim itself and to the library files of the project.


Notes:

When opening a source file inside a project, handlers.on_attach is called
and this triggers calling `env.update_lsp_settings_on_attach(client.name, client)`
here client - is a merged bit-table - options configured for lsp-server
and this table is not created new every time, but the one already created
earlier for this LSP server is reused.
Therefore, I added a check to the settings update method to check that the
settings have already been made earlier - by remembering the time the settings
were created specifically for this project. (LuaLand.last_lsp_update)
And also added remembering the reading time of the rockspec-config file so
that it can automatically recreate the lsp-settings when the rockspec-file is
changed (LuaRocksBuilder.buildscript_lm)


## Notes about lua-ls anotations:

Dev/src-lua/a_docs/lua-ls-notations/


## Back from visual mode to normal

```lua
-- current mode
print('mode:', vim.api.nvim_get_mode().mode, 'vm:', vim.fn.visualmode())

ln1 = (vim.fn.getpos("'<") or E)[2] or opts.line1
ln2 = (vim.fn.getpos("'>") or E)[2] or opts.line2
vim.cmd[[exe "normal \<c-c>"]]
```

Note:
unlike getpos('v'), if no lines are selected, it will return the previous
selection; for cases when you need to work with the current selection, getpos('v') is
more suitable.
Even if there is no selection, getpos('v') will return the current line number:



## Notes about this plugin development outside the git-repository

../docs-and-notes/nvim-env/

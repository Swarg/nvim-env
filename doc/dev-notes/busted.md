## busted - testing framework


- .busted - a config file in the project root
- Notes how to run single test file
- How a Lua looks for modules
- Example how to configure busted via .busted file


## .busted - a config file in the project root

remove prev used preloading a `package.path` in each test file:
```sh
package.path = package.path .. ';./lua/?.lua'
```

and introduce .busted file with lpath:

```lua
return {
  _all = {
    coverage = false,
  },
  default = {
    coverage = false,
    verbose = true,
    ROOT = {"test"},
    lpath = "lua/?.lua;lua/env/?.lua;spec/?.lua;",
    -- ["exclude-pattern"] = "only-for-files-without-dir",
  }
}
```

to run all tests just run `busted`


Note: exclude-pattern allow to filter(remove) only by basename
see sources: /usr/local/share/lua/5.1/busted/modules/test_file_loader.lua
function `getTestFiles` and `options.excludes`

for testing resources use this workaround:

```lua
package.preload['mypkg'] = loadfile('test/env/resources/lua/luarocks/src/mypkg/init.lua')
```


## Notes how to run single test file

-- exec '/usr/bin/lua5.1'
-- -e 'package.path="/root/.luarocks/share/lua/5.1/?.lua;/root/.luarocks/share/lua/5.1/?/init.lua;/usr/local/share/lua/5.1/?.lua;/usr/local/share/lua/5.1/?/init.lua;"..package.path; package.cpath="/root/.luarocks/lib/lua/5.1/?.so;/usr/local/lib/lua/5.1/?.so;"..package.cpath'
-- -e 'local k,l,_=pcall(require,"luarocks.loader") _=k and l.add_context("busted","2.1.2-3")'
-- '/usr/local/lib/luarocks/rocks/busted/2.1.2-3/bin/busted' "$@"

-- exec '/usr/bin/lua5.1'
-- -e 'package.path="/root/.luarocks/share/lua/5.1/?.lua
-- /root/.luarocks/share/lua/5.1/?/init:.lua
-- /usr/local/share/lua/5.1/?.lua
-- /usr/local/share/lua/5.1/?/init.lua
-- "..package.path
--  package.cpath="/root/.luarocks/lib/lua/5.1/?.so
-- /usr/local/lib/lua/5.1/?.so
-- "..package.cpath' -e 'local k,l,_=pcall(require,"luarocks.loader") _=k and l.add_context("busted","2.1.2-3")' '/usr/local/lib/luarocks/rocks/busted/2.1.2-3/bin/busted' "$@"



## How a Lua looks for modules

By Default lua search modules(packages) in :
```
/usr/share/lua/VER/
/usr/local/share/lua/VER/
/usr/local/lib/lua/VER/
./?.lua;
./src/?.lua;
./src/?/?.lua;
./src/?/init.lua;
```

./lua/?.lua  << for my case add directry
Another found way to solve issue with passing `LUA_PATH` to resolve env.sutil
is set the CWD to path with source for this test file and run from there

-- Why evn LUA_PATH don't work?
-- See: luarocks doc busted
-- package.path = package.path .. ';./lua/?.lua'
-- print(package.path)


--package.path = package.path .. ';/usr/share/nvim/runtime/lua/?.lua'
--local vim = require('vim.shared')
## Example how to configure busted via .busted file

.busted file in project root:
```lua
return {
  _all = {
    coverage = false
  },
  default = {
    coverage = false,
    verbose = true,
    ROOT = { "spec" },
    lpath = "src/?.lua;src/cmd4lua/?.lua;spec/?.lua;"
  }
}
```


old method that I used to work around this problem:
code in the firts line of each lua-test-module:

```lua
package.path = package.path .. ';./src/cmd4lua/?.lua'
```

if ".busted" file exists in the cwd then this file is automaticly picked up
by busted itself and the lpath settings are taken from it.
for run test via my tool just use this args without --lpath

args = { '-v', path2test }

lpath busted picked up from .busted default.lpath=...




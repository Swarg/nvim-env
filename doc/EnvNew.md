# EnvNew

Create some new thing(files/project/config) from a template
First Usage to provide templates to quickly create a lightweight project and
classes (without gradle|mvn init)

Goal:
faster and easier generation of the necessary pieces of code or simple projects

### Example:

Generate pom.xml file to current directory::
```vim
:EnvNew java pom.xml
```

Generate maven project with MainClass::
```vim
:EnvProject new java pom.xml -g pkg.MainClass -a app -new-project
```

```html
Generate pom.xml for given (All keys are optional):
  optkey            [default-value]
  -g --group_id     [pkg]
  -a --artifact_id  [app]
  -v --version      [0.0.1]
  -n --name         [prog]
  -c --main-class   [pkg.MainClass]
  -j --java         [1.8] - version of java compiler
  -p --new-project  Create The Maven Project (Directory Structure)
  -q --quiet        Not Ask confirmation
```

#### For java-project

```vim
:EnvNew java
```
PrintHelp:
  class | pom.xml | build.gradle

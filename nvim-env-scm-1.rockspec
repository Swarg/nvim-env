---@diagnostic disable: lowercase-global
package = "nvim-env"
version = "scm-1"
source = {
  url = "git@gitlab.com:Swarg/nvim-env.git"
}

description = {
  detailed = [[
Convenient environment for development and testing the code for nvim.
]],
  homepage = "http://gitlab.com/Swarg/nvim-env",
  license = "",
}

dependencies = {
  "lua >= 5.1",
  'alogger >= 0.5',
  'cmd4lua >= 0.8',
  'dprint >= 0.4',
  'oop >= 0.6',
  'clieos >= 0.6.1',
  'lua-cjson >= 2.1',
  'lyaml >= 6.2',
  'xml2lua >= 1.5',
  'luasocket >= 3.1',
  'luasec >= 1.3',
  'ex-parser >= 0.3', -- elixir
  'www-auth >= 0.1',
  'db-env >= 0.1',    -- luasql.*
  'stub >= 0.10',     -- scope:test
  'bit32 >= 5.3',
  'hdgen >= 0.10.0',
  -- "busted 2.2", -- scope:test
}

build = {
  type = "builtin",
  modules = {}
}

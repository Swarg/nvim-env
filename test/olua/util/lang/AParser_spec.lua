-- 14-08-2024 @author Swarg
---@diagnostic disable: invisible
require("busted.runner")()
local assert = require("luassert")
local AParser = require 'olua.util.lang.AParser'
local IteratorImpl = require 'olua.util.IteratorImpl'

describe("olua.util.lang.AParser", function()
  it("_get_remainder_sub", function()
    ---@diagnostic disable-next-line:  missing-parameter
    local p = AParser:new()
    p.remainder_line = "public MyClass(A a, B b) {}"
    --                  12345678901234567890
    p.remainder_line_col = 15

    assert.is_nil(p:_get_remainder_sub(0, 0))
    assert.same('(', p:_get_remainder_sub(1, 1))
    assert.same('(A a, B b) {}', p:_get_remainder_sub(1, -1))
    assert.same('(A a, B b) ', p:_get_remainder_sub(1, -3))
    assert.same('(', p:_get_remainder_sub(1, 1))
    assert.same('(A', p:_get_remainder_sub(1, 2))
  end)


  local function newParser(lines, offset)
    offset = offset or 0
    return AParser:new(nil, IteratorImpl:new(nil, lines, offset), offset)
  end


  it("_next_line_up_to", function()
    local lines = {
      'public class MyClass', 'extends Object', 'implement Iterable',
      '{ /* comment */',
      ' method1(){}',
      '}'
    }
    local p = newParser(lines, 0)
    ---@diagnostic disable-next-line: duplicate-set-field
    p.getPatternFor = function()
      return {
        match = function(_, s) -- lpeg.match stub
          local pos = string.find(s, '{')
          if pos then pos = pos + 1 end
          return pos
        end
      }
    end
    assert.same(true, p:isEmptyOrComment(""))
    local res = p:_next_line_up_to('{')
    local exp = "public class MyClass\nextends Object\nimplement Iterable\n{ "
    assert.same(exp, res)
    -- ignore
    assert.same('/* comment */', p:_next_line())
  end)
end)

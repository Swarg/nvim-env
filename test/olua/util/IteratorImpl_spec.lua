-- 14-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local IteratorImpl = require 'olua.util.IteratorImpl'

describe("olua.util.IteratorImpl", function()
  it("new instance", function()
    local iter = IteratorImpl:new(nil, { 1, 2, 3, 4, 5 }, 0)
    local t = {}
    while (iter:hasNext()) do
      t[#t + 1] = iter:next()
    end
    assert.same({ 1, 2, 3, 4, 5 }, t)
    assert.same(false, iter:hasNext())
    assert.same(nil, iter:next())
  end)
end)

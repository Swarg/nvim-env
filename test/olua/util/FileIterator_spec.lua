-- 30-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local FileIterator = require 'olua.util.FileIterator'

describe("env.ui.nvim.FileIterator", function()
  it("read file", function()
    local fn = os.getenv('PWD') .. '/test/env/resources/about_lua.txt'
    local iter = FileIterator:new(nil, fn)
    assert.same('olua.util.FileIterator', iter:getClassName())

    local t = {}
    while iter:hasNext() do
      t[#t + 1] = iter:next()
    end
    local exp = {
      '# This data used in tests. Do not change line ordering.',
      '',
      '  Lua is a powerful, efficient, lightweight, embeddable scripting language. It supports procedural programming, object-oriented programming, functional programming, data-driven programming, and data description. Lua combines simple procedural syntax with powerful data description constructs based on associative arrays and extensible semantics.',
      '',
      '  Lua is a powerful, efficient, lightweight, embeddable scripting language.',
      'It supports procedural programming, object-oriented programming,',
      'functional programming, data-driven programming, and data description.',
      'Lua combines simple procedural syntax with powerful data description constructs',
      'based on associative arrays and extensible semantics.',
      ''
    }
    assert.same(exp, t)
  end)
end)

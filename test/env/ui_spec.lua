require 'busted.runner' ()

local fs = require 'env.ui'
local M = fs
local R = require 'env.require_util'
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local assert = require('luassert')                   -- used by busted has in nvim +
-- local utils = require('pl.utils') -- used by busted no in nvim ?

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

describe('env.ui', function()
  before_each(function()
    nvim:clear_state()
  end)

  it("get_interpreter", function()
    local f = function(line)
      local bufnr = nvim:new_buf({ line }, 1, 0)
      -- local lines = vim.api.nvim_buf_get_lines(bufnr, 0, 1, false)
      return M.get_interpreter(bufnr)
    end
    assert.same('bash', f('#!/bin/bash'))
    assert.same('', f('# !/bin/bash'))
    assert.same('bash', f('#!/usr/bin/bash'))
    assert.same('bash', f('#!/usr/bin/bash '))
    assert.same('bash', f('#!/usr/bin/env bash'))
    assert.same('bash', f('#!/usr/bin/env  bash '))
    assert.same('sh', f('#!/usr/bin/env /bin/sh'))
    assert.same('', f('#!/usr/bin/env '))
    assert.same('envx', f('#!/usr/bin/envx '))
  end)

  it("format", function()
    local line = 'abc'
    local max = 9
    local n = max - #line
    local res = string.format("%-"..n.."s", line)
    assert.same('abc   ', res)
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')

local bu = require('env.bufutil')
---@diagnostic disable-next-line: unused-local
local log = require('alogger')
local Cmd4Lua = require("cmd4lua")
-- local c4lv = require("env.util.cmd4lua_vim")

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

_G.TEST = true
local M = require("env.command.lines_modify")

describe("env.command.lines_modify", function()
  after_each(function()
    nvim:clear_state()
  end)
  ------------------------------------------------------------------------------


  it("match", function()
    local line = '  elseif w:desc(\'Change the logger settings\')'
    assert.same('  elseif', line:match('^%s+elseif'))
  end)

  it("check helper tooling", function()
    local lines = {
      -------- nvim lnum
      '1', --  0     1
      '2', --  1     2
      '3',
    }

    local bufnr = nvim:new_buf(lines, 1, 1)
    -- get all buffer lines                                   v
    assert.same(lines, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- insert two lines into lnum = 1
    vim.api.nvim_buf_set_lines(bufnr, 1, 1, true, { 'a', 'b' })

    local exp = { '1', 'a', 'b', '2', '3' }
    --                 ^^^^^^^ inserted
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- make sure line1 -- is a lnum (started from 1) but not nvim (from 0)
    local vim_opt = { line1 = 1, line2 = 2 } --- < lines has lnum indexes(1+
    assert.same({ '1', 'a' }, bu.get_selected_lines(vim_opt, false))
  end)

  it("cmd_fix_comments_nums", function()
    local w = Cmd4Lua.of('')
    local lines = {                 -- nvim lnum
      " local lines = {",           -- 0     1
      "      k0 = 1, -- 0",         -- 1     2   <-- line1 = 2
      "      { k = v }, -- 3 note", -- 2     3     selection
      "      '--', --",             -- 3     4     selection
      "      { key = value, },",    -- 4     5   <-- line2 = 5
      "    }"
    }
    local bufnr = nvim:new_buf(lines, 1, 1)

    w.vars.ext = 'lua'
    w.vars.vim_opts = { line1 = 2, line2 = 5 } -- lnum index(from1) not nvim

    M.cmd_fix_comments_nums(w)

    local exp = {
      ' local lines = {',
      '      k0 = 1,           --  0',
      '      { k = v },        --  1 note',
      "      '--',             --  2",
      '      { key = value, }, --  3',
      '    }'
    }
    -- get all lines is 0:-1
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("cmd_swap_pair wraps_strs", function()
    local w = Cmd4Lua.of("--separator = --wrap [AB_ _CD EF_ _JK]")
    local lines = {    -- nvim  lnum
      '--',            -- 0     1   <- line1=1
      'key1 = value1', -- 1     2    selection
      'key2 = value2', -- 2     3   <- line2=3
      'key3 = value3', -- 3     4
    }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 3 }
    -- check helper tooling and bufutil
    local exp1 = { '--', 'key1 = value1', 'key2 = value2' }
    assert.same(exp1, bu.get_selected_lines(w.vars.vim_opts, false))
    --
    -- D.enable()
    -- TH.logger_setup()
    M.cmd_swap_pair(w)

    assert.same(false, w:has_input_errors())
    -- assert.same('', w:build_input_error_msg())
    local exp = {
      '--',
      'AB_value1_CD=EF_key1_JK',
      'AB_value2_CD=EF_key2_JK',
      'key3 = value3'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  --
  it("cmd_swap_pair wraps_strs", function()
    local w = Cmd4Lua.of("--separator = --wrap ['' @ '' #]") --, c4lv.Implementations)
    local lines = {                                          -- nvim  lnum
      '--',                                                  -- 0     1   <- line1=1
      'key1 = value1',                                       -- 1     2    selection
      'key2 = value2',                                       -- 2     3   <- line2=3
    }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 2 }
    -- D.enable() TH.logger_setup()
    M.cmd_swap_pair(w)

    assert.same(false, w:has_input_errors())
    -- assert.same('', w:build_input_error_msg())
    local exp = { '--',
      'value1@=key1#',
      'key2 = value2'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  --
  --
  it("cmd_columns", function()
    local w = Cmd4Lua.of('')
    local lines = {
      "36K	api/vendor/doctrine/lexer",
      "188K	api/vendor/doctrine/common",
      "264K	api/vendor/doctrine/inflector",
      "8.3M	api/vendor/doctrine/"
    }
    local bufnr = nvim:new_buf(lines, 1, 1)

    w.vars.vim_opts = { line1 = 1, line2 = 4 } -- lnum index(from 1) not nvim

    M.cmd_columns(w)

    local exp = {
      '36K   api/vendor/doctrine/lexer      ',
      '188K  api/vendor/doctrine/common     ',
      '264K  api/vendor/doctrine/inflector  ',
      '8.3M  api/vendor/doctrine/           '
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  --
  --
  it("cmd_columns sort", function()
    local w = Cmd4Lua.of('--sort 2')
    local lines = {
      "36K	api/vendor/doctrine/lexer",
      "188K	api/vendor/doctrine/common",
      "264K	api/vendor/doctrine/inflector",
      "8.3M	api/vendor/doctrine/"
    }
    local bufnr = nvim:new_buf(lines, 1, 1)

    w.vars.vim_opts = { line1 = 1, line2 = 4 } -- lnum index(from 1) not nvim

    M.cmd_columns(w)

    local exp = {
      '8.3M  api/vendor/doctrine/           ',
      '188K  api/vendor/doctrine/common     ',
      '264K  api/vendor/doctrine/inflector  ',
      '36K   api/vendor/doctrine/lexer      '
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("build_wrap_map_from_list", function()
    local f = M.build_colums_wrapper
    assert.is_function(f)
    local exp = { [2] = { '<', '>' }, [3] = { '`' } }
    assert.same(exp, f({ '2', '<', '>', '3', '`', 4 }))

    local exp2 = { [2] = { '<', '>' }, [4] = { ':', '_' } }
    assert.same(exp2, f({ '2', '<', '>', 'ignore', 'extra', '3', '4', ':', '_' }))
  end)


  it("cmd_columns columns wrapper", function()
    local w = Cmd4Lua.of('-s " " --wrapper [2 < > 3 `]')
    local lines = {
      'a b__c d_e',
      'f g__h i_j',
      'x xxxx xxx',
    }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 3 }

    M.cmd_columns(w)

    local exp = {
      'a  <b__c>  `d_e`  ',
      'f  <g__h>  `i_j`  ',
      'x  <xxxx>  `xxx`  '
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  -- skip index without lines
  it("cmd_columns columns wrapper", function()
    local w = Cmd4Lua.of('-s " " --wrapper [ 1 2 3 ` 4]')
    local lines = { 'a b__c d_e', 'f g__h i_j', }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 2 }

    M.cmd_columns(w)

    local exp = { 'a  b__c  `d_e`  ', 'f  g__h  `i_j`  ' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("cmd_columns columns wrapper", function()
    local w = Cmd4Lua.of('-s " " --wrapper [ 1 \\" 2 \\[ \\] ]')
    local lines = {
      'a b__c d_e',
      'f g__h i_j',
    }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 2 }

    M.cmd_columns(w)

    local exp = {
      '"a"  [b__c]  d_e  ',
      '"f"  [g__h]  i_j  '
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("cmd_replace_lines", function()
    local w = Cmd4Lua.of('[ "^start_with", "ends_with" ]')
    local lines = {
      'start_with line1',
      'line 2',
      'line3 ends_with',
    }
    local bufnr = nvim:new_buf(lines, 1, 1)
    w.vars.vim_opts = { line1 = 1, line2 = 3 }

    M.cmd_replace_lines(w)

    local exp = { 'line 2' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("cmd_rows_to_table", function()
    local w = Cmd4Lua.of('')
    local lines = {
      's[',           -- 1
      'abc1',         -- 2 <<
      'de fg hj2',    -- 3
      'code 3',       -- 4
      '-- comment 1', -- 5
      '-- comment 2', -- 6
      '-- comment 3', -- 7 <<
      'e]'            -- 8
    }
    local line1, line2 = 2, 7
    local bufnr = nvim:new_buf(lines, line2, 1)
    w.vars.vim_opts = { line1 = line1, line2 = line2 }
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    M.cmd_rows_to_table(w)

    local exp = {
      's[',
      'abc1         -- comment 1',
      'de fg hj2    -- comment 2',
      'code 3       -- comment 3',
      'e]'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("cmd_rows_to_table", function()
    local w = Cmd4Lua.of('')
    local lines = {
      's[',           -- 1
      'abc1',         -- 2
      'de fg hj2',    -- 3
      'code 3',       -- 4  <<
      '-- comment 1', -- 5  <<
      '-- comment 2', -- 6
      '-- comment 3', -- 7
      'e]'
    }
    local line1, line2 = 4, 5
    local bufnr = nvim:new_buf(lines, line2, 1)
    w.vars.vim_opts = { line1 = line1, line2 = line2 }
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    M.cmd_rows_to_table(w)

    local exp = {
      's[',
      'abc1',
      'de fg hj2',
      'code 3    -- comment 1',
      '-- comment 2',
      '-- comment 3',
      'e]'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("cmd_rows_to_table", function()
    local w = Cmd4Lua.of('-c 3')
    local lines = {
      's[',           --  1
      'abc1',         --  2 <<
      'de fg hj2',    --  3
      'code 3',       --  4
      'come code 4',  --  5
      '-- comment 1', --  6
      '-- comment 2', --  7
      '-- comment 3', --  8 <<
      '-- comment 4', --  9 <<
      'e]'            -- 10
    }
    local line1, line2 = 2, 9
    local bufnr = nvim:new_buf(lines, line2, 1)
    w.vars.vim_opts = { line1 = line1, line2 = line2 }
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    M.cmd_rows_to_table(w)

    local exp = {
      's[',
      'abc1         code 3         -- comment 1    -- comment 3',
      'de fg hj2    come code 4    -- comment 2    -- comment 4',
      'e]'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("cmd_simple_punctuation", function()
    local w = Cmd4Lua.of('')
    local lines = {
      '0', -- 0
      '1', --  1
      'abc“de “f”g ‘h’2–—…•«8»z', --  2
      '3', --3
    }
    local line1, line2 = 1, 3
    local bufnr = nvim:new_buf(lines, line2, 1)
    w.vars.vim_opts = { line1 = line1, line2 = line2 }

    M.cmd_simple_punctuation(w)

    local exp = { '0', '1', 'abc\"de \"f\"g \'h\'2--...*\"8\"z', '3' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("cmd_wrap", function()
    local w = Cmd4Lua.of('--number 10')
    local lines = {
      'one',   -- 0
      'two',   -- 1
      'three', -- 2
      'four',  -- 3
    }
    local line1, line2 = 1, 4
    local bufnr = nvim:new_buf(lines, line2, 1)
    w.vars.vim_opts = { line1 = line1, line2 = line2 }

    M.cmd_wrap(w)

    local exp = {
      "(10, 'one'),",
      "(11, 'two'),",
      "(12, 'three'),",
      "(13, 'four'),"
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("cmd_extract_column first column", function()
    local lines = {
      'one     01  a', -- 0
      'two     02  b', -- 1
      'three    3  c', -- 2
      'four    04  d', -- 3
    }
    local line1, line2 = 1, 4
    nvim:new_buf(lines, line2, 0)
    local w = Cmd4Lua.of('-q')
    w.vars.vim_opts = { line1 = line1, line2 = line2 }

    local res = M.cmd_extract_column(w)

    local exp = { 'one', 'two', 'three', 'four' }
    assert.same(exp, res)
  end)

  it("cmd_extract_column second column", function()
    local lines = {
      --234567890123
      'one     01  a', -- 0
      'two     02  b', -- 1
      'three    3  c', -- 2
      'four    04  d', -- 3
    }
    local line1, line2 = 1, 4
    nvim:new_buf(lines, line2, 8)
    local w = Cmd4Lua.of('-q')
    w.vars.vim_opts = { line1 = line1, line2 = line2 }

    local res = M.cmd_extract_column(w)

    local exp = { '01', '02', '3', '04' }
    assert.same(exp, res)
  end)

  it("cmd_extract_column third column", function()
    local lines = {
      --234567890123
      'one     01  a',
      'two     02  bcde',
      'three    3  c',
      'four    04  def',
    }
    local line1, line2 = 1, 4
    nvim:new_buf(lines, line2, 12)
    local w = Cmd4Lua.of('-q')
    w.vars.vim_opts = { line1 = line1, line2 = line2 }

    local res = M.cmd_extract_column(w)

    local exp = { 'a', 'bcde', 'c', 'def' }
    assert.same(exp, res)
  end)
end)

describe("env.command.lines_modify", function()
  it("readable_size_to_bytes", function()
    local f = M.test.readable_size_to_bytes
    assert.same(128974848, f("123M"))
    assert.same(1048576, f("1M"))
    assert.same(524288, f("0.5M"))
    assert.same(512, f("0.5K"))
    assert.same(1536, f("1.5K"))
    assert.same(960, f("960"))
  end)
end)

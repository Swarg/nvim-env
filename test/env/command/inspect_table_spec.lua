--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.command.inspect_table")

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
---@diagnostic disable-next-line
local D = require("dprint")
local Cmd4Lua = require("cmd4lua")
local tviewer = require("env.util.tbl_viewer")

local print_wrapper = require("env.util.print_wrapper")

describe("env.command.inspect_table", function()
  local object_table = {
    sub_1 = {
      file_in_sub1_1 = 'ab',
      sub_2 = {
        sub_3 = {
          file_in_sub3_1 = 'ab',
          sub_4_1 = {
            file_in_sub4_1__1 = 11,
            file_in_sub4_1__2 = 's1',
          },
          sub_4_2 = {
            file_in_sub4_2__1 = 22,
            file_in_sub4_2__2 = 's2',
          },
          sub_4_3 = {
            file_in_sub4_3__1 = 33,
            file_in_sub4_3__2 = 's3',
          },
          file_in_sub3_2 = 's3',
        }
      },
      file_in_sub1_2 = 'ab',
      [1] = 'number-index-1',
      [2] = 'number-index-2',
    },
    file_in_root_1 = 1,
    file_in_root_2 = '',
    file_in_root_3 = true,
  }

  it("cmd_tree root", function()
    print_wrapper.wrap()
    M.tools.set_object(object_table)
    local w = Cmd4Lua.of('2')

    M.cmd_tree(w)
    local exp = {
      '/',
      '',
      '    sub_1',
      '        sub_2            -- (TABLE) [+]',
      '        file_in_sub1_1   -- (string)',
      '        file_in_sub1_2   -- (string)',
      '        2                -- (string)',
      '        1                -- (string)',
      '',
      '    file_in_root_1       -- (number)',
      '    file_in_root_2       -- (string)',
      '    file_in_root_3       -- (boolean)',
      ''
    }
    assert.same(exp, print_wrapper.get_output())
    print_wrapper.restore()
    print_wrapper.clear_output()
  end)

  it("cmd_tree sub_2", function()
    print_wrapper.wrap()
    M.tools.set_object(object_table)
    tviewer.cd(M.state, 'sub_1')
    tviewer.cd(M.state, 'sub_2')
    local w = Cmd4Lua.of('2')

    M.cmd_tree(w)
    local exp = {
      '/ sub_1 sub_2',
      '',
      '    sub_3',
      '        sub_4_2          -- (TABLE) [+]',
      '        sub_4_1          -- (TABLE) [+]',
      '        sub_4_3          -- (TABLE) [+]',
      '        file_in_sub3_1   -- (string)',
      '        file_in_sub3_2   -- (string)',
      '',
      ''
    }
    assert.same(exp, print_wrapper.get_output())
    print_wrapper.restore()
    print_wrapper.clear_output()
  end)

  it("cmd_cd", function()
    print_wrapper.wrap()
    M.tools.set_object(object_table)
    local w = Cmd4Lua.of({ 'sub_1', 'sub_2' })
    M.cmd_cd(w)
    assert.same({ 'sub_1', 'sub_2' }, M.get_path())
    -- M.cmd_pwd(w)
    local exp = {}
    assert.same(exp, print_wrapper.get_output())
    print_wrapper.restore()
    print_wrapper.clear_output()
  end)
end)

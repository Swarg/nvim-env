--
require 'busted.runner' ()
local assert = require('luassert')

local Cmd4Lua = require("cmd4lua")
local M = require("env.command.lines_modify.lua")

---@diagnostic disable-next-line: unused-local
local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
---@diagnostic disable-next-line: unused-local
local D = require("dprint")

local lua_parser = require("env.lua_parser")
local cp = require("env.util.code_parser")

local Editor = require("env.ui.Editor")
-- local Context = require("env.ui.Context")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

describe("env.command.lines_modify.lua", function()
  after_each(function()
    nvim:clear_state()
  end)
  ------------------------------------------------------------------------------

  --
  it("cmd_add_id2table", function()
    local w = Cmd4Lua.of('')
    local lines = { -- selected
      "    local table = {",
      "      k0 = 1,",
      "      { k1 = v1, k2 = v2 },",
      "      { k4 = v1, k4 = v2 },",
      "      { k5 = v1, k6 = v2, },",
      "    }"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.is_true(M.cmd_add_id2table(w))

    local exp
    exp = { '    local table = {',
      "      k0 = 1, id = 1,",
      '      { k1 = v1, k2 = v2, id = 2 },',
      '      { k4 = v1, k4 = v2, id = 3 },',
      '      { k5 = v1, k6 = v2, id = 4 },',
      '    }'
    }
    local res = nvim:get_lines()
    assert.same(exp, res) -- changed_lines
    assert.same(exp, res)
  end)

  it("cmd_sort_table_keys", function()
    local w = Cmd4Lua.of('')
    local lines = { -- selected
      "    local table = {",
      "      k0 = 1,",
      "      { v = v1, k = 'x' },",
      "      { v = v1, k = 'y' },",
      "      { v = v1, k = 'z' },",
      "    }"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.same(true, M.cmd_sort_table_keys(w))
    local exp = {
      '    local table = {',
      '      k0 = 1,',
      "      { k = 'x', v = 'v1' },",
      "      { k = 'y', v = 'v1' },",
      "      { k = 'z', v = 'v1' },",
      '    }'
    }
    assert.same(exp, nvim:get_lines())
  end)

  it("cmd_sort_table_keys with order", function()
    local w = Cmd4Lua.of('--order [d b a]')
    local lines = { -- selected
      "    local table = {",
      "      k0 = 1,",
      "      { a = 3, b = 4, d = '9'}",
      "      { a = 3, b = 4, d = '8', c = 88}",
      "      { a = 3, b = 4, d = '7', c = 8, 42}",
      "      { a = 2, b = 3, d = '6', k = { 1, 2 }, 8,  42 }",
      "      { a = 3, b = 4, d = '5', k = { 1, 2 }}",
      "    }"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.same(true, M.cmd_sort_table_keys(w))
    local exp = {
      '    local table = {',
      '      k0 = 1,',
      "      { d = '9', b = 4, a = 3 }",
      "      { d = '8', b = 4, a = 3, c = 88 }",
      "      { d = '7', b = 4, a = 3, c = 8, 42 }",
      "      { d = '6', b = 3, a = 2, k = { 1, 2 }, 8, 42 }",
      "      { d = '5', b = 4, a = 3, k = { 1, 2 } }",
      '    }'
    }
    assert.same(exp, nvim:get_lines())
  end)

  --

  it("cmd_reorder_multiline_keys", function()
    local w = Cmd4Lua.of('--order [description name type]')
    local lines = { -- selected
      "      {",
      "        description = 'Identifier',",
      "        example = '2d0c3a83153',",
      "        maxLength = 32,",
      "        type = 'String',",
      "        name = 'id',",
      "      },",
      "      {",
      "        type = 'Owner'",
      "        name = 'X owner',",
      "        description = 'The owner of the X',",
      "      },"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.same(true, M.cmd_reorder_multiline_keys(w))
    local exp = {
      '      {',
      "        description = 'Identifier',",
      "        name = 'id',",
      "        type = 'String',",
      "        example = '2d0c3a83153',",
      '        maxLength = 32,',
      '      },',
      '      {',
      "        description = 'The owner of the X',",
      "        name = 'X owner',",
      "        type = 'Owner'",
      '      },'
    }
    assert.same(exp, nvim:get_lines())
  end)

  it("cmd_reorder_multiline_keys order from the first line", function()
    local w = Cmd4Lua.of('--order .')
    local lines = { -- selected
      "      -- order: description name type",
      "      {",
      "        description = 'Identifier',",
      "        example = '2d0c3a83153',",
      "        maxLength = 32,",
      "        type = 'String',",
      "        name = 'id',",
      "      },",
      "      {",
      "        type = 'Owner'",
      "        name = 'X owner',",
      "        description = 'The owner of the X',",
      "      },"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.same(true, M.cmd_reorder_multiline_keys(w))
    local exp = {
      '      -- order: description name type',
      '      {',
      "        description = 'Identifier',",
      "        name = 'id',",
      "        type = 'String',",
      "        example = '2d0c3a83153',",
      '        maxLength = 32,',
      '      },',
      '      {',
      "        description = 'The owner of the X',",
      "        name = 'X owner',",
      "        type = 'Owner'",
      '      },'
    }
    assert.same(exp, nvim:get_lines())
  end)
end)

describe("env.command.lines_modify.lua 2", function()
  it("cmd_gen_method_body", function()
    local lines = {
      '--',                                                --  1
      "function Cmd4Lua:root(cmd_name)",                   --  2
      "  return self.collector:annotation_root(cmd_name)", --  3
      "  if cmd_name then",                                --  4
      "    self.collector._cmds_path = { cmd_name }",      --  5
      "  end",                                             --  6
      "  return self",                                     --  7
      "end",                                               --  8
      ""                                                   --  9
    }
    local bufnr, lnum, col = 8, 3, 33
    nvim:new_buf(lines, lnum, col, bufnr)

    local curr_line = nvim.api.nvim_get_current_line()
    assert.same(lines[lnum], curr_line)
    assert.same('annotation_root', cp.get_word_at(curr_line, col + 1))

    local editor = Editor:new()
    local ctx = editor:resolveWords().ctx
    assert.same(lines[3], ctx.current_line)
    assert.same(col, ctx.cursor_col)
    assert.same({ lnum, col }, ctx.cursor_pos)
    assert.same(lnum, ctx.cursor_row) --line num
    assert.same(LEXEME_TYPE.METHOD, ctx.lexeme_type)

    -- words under cursor
    assert.same('self.collector', ctx.word_container)
    assert.same('annotation_root', ctx.word_element)

    D.enable(false)
    local w = Cmd4Lua.of('')
    local rs, re = lua_parser.find_function_block_range(bufnr, lnum)
    assert.same(1, rs)
    assert.same(7, re)

    assert.same(true, M.cmd_gen_method_body(w))
    local exp = {
      '--',
      'function Cmd4Lua:root(cmd_name)',
      '  return self.collector:annotation_root(cmd_name)',
      'function Collector:annotation_root(cmd_name)',
      '  if cmd_name then',
      '    self._cmds_path = { cmd_name }',
      '  end',
      '  return self.parent',
      'end',
      '',
      'end',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1)) -- editor:getLines(bufnr, 0, -2))
  end)

  it("cmd_require", function()
    local w = Cmd4Lua.of('')
    local lines = { -- selected
      [[local Cmd4Lua = require("cmd4lua")                    -- KEEP comment]],
      [[local M = require("env.command.lines_modify.lua");]],
      "",
      [[---@diagnostic disable-next-line: unused-local]],
      [[local R = require 'env.require_util']],
      [[---@diagnostic disable-next-line]],
      [[local inspect = R.require("inspect", vim, "inspect") -- vim.inspect]],
      [[---@diagnostic disable-next-line: unused-local]],
      [[local D = require("dprint")]],
      "",
      [[local lua_parser = require("env.lua_parser")]],
      [[local cp = require("env.util.code_parser")]],
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    assert.same(true, M.cmd_format_require(w))
    local exp = {
      "local Cmd4Lua = require 'cmd4lua'                    -- KEEP comment",
      "local M = require 'env.command.lines_modify.lua'",
      '',
      '---@diagnostic disable-next-line: unused-local',
      "local R = require 'env.require_util'",
      '---@diagnostic disable-next-line',
      'local inspect = R.require("inspect", vim, "inspect") -- vim.inspect',
      '---@diagnostic disable-next-line: unused-local',
      "local D = require 'dprint'",
      '',
      "local lua_parser = require 'env.lua_parser'",
      "local cp = require 'env.util.code_parser'"
    }
    assert.same(exp, nvim:get_lines())
  end)

  it("cmd_extract_table_keys", function()
    local w = Cmd4Lua.of('-d')
    local lines = { -- selected
      "      ID = '136-dash',",
      "      FPS = '15',",
      "      CH = nil,",
      "      TBR = '304k',",
      "      VCODEC = 'avc1.4d401f',",
      "      VBR = '304k',",
      "      ACODEC = 'video only',",
      "      ABR = nil,",
      "      MORE = 'DASH video, mp4_dash',"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    M.cmd_extract_table_keys(w)
    local exp = {
      '"ID", "FPS", "CH", "TBR", "VCODEC", "VBR", "ACODEC", "ABR", "MORE"',
      "      ID = '136-dash',",
      "      FPS = '15',",
      '      CH = nil,',
      "      TBR = '304k',",
      "      VCODEC = 'avc1.4d401f',",
      "      VBR = '304k',",
      "      ACODEC = 'video only',",
      '      ABR = nil,',
      "      MORE = 'DASH video, mp4_dash',"
    }
    assert.same(exp, nvim:get_lines())
  end)


  it("cmd_extract_table_keys", function()
    local w = Cmd4Lua.of('')
    local lines = {
      "      ID = '136-dash',",
      "      FPS = '15',",
      "      CH = nil,",
      "      TBR = '304k',",
      "      VCODEC = 'avc1.4d401f',",
      "      VBR = '304k',",
      "      ACODEC = 'video only',",
      "      ABR = nil,",
      "      MORE = 'DASH video, mp4_dash',"
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 1, line2 = #lines }
    M.cmd_extract_table_keys(w)
    local exp = {
      'ID FPS CH TBR VCODEC VBR ACODEC ABR MORE',
      "      ID = '136-dash',",
      "      FPS = '15',",
      '      CH = nil,',
      "      TBR = '304k',",
      "      VCODEC = 'avc1.4d401f',",
      "      VBR = '304k',",
      "      ACODEC = 'video only',",
      '      ABR = nil,',
      "      MORE = 'DASH video, mp4_dash',"
    }
    assert.same(exp, nvim:get_lines())
  end)
end)

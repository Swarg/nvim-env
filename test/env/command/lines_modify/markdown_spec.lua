-- 14-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.command.lines_modify.markdown");

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local Cmd4Lua = require("cmd4lua")

describe("env.command.lines_modify.markdown", function()
  it("to-list unordered", function()
    local lines = {
      'this is some text in markdown document', -- 1
      "    What X is",                          -- 2
      "    Look at examples of it",             -- 3
      "    Introduce key terms",                -- 4
      "-- "                                     -- 5
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('')
        :set_vars({ vim_opts = { line1 = 2, line2 = 4 } })

    assert.same(true, M.cmd_to_list(w))
    local exp2 = {
      'this is some text in markdown document',
      '- What X is',
      '- Look at examples of it',
      '- Introduce key terms',
      '-- '
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("to-list ordered", function()
    local lines = {
      'this is some text in markdown document', -- 1
      "    What X is",                          -- 2
      "    Look at examples of it",             -- 3
      "    Introduce key terms",                -- 4
      "-- "                                     -- 5
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('--ordered')
        :set_vars({ vim_opts = { line1 = 2, line2 = 4 } })

    assert.same(true, M.cmd_to_list(w))
    local exp2 = {
      'this is some text in markdown document',
      '1. What X is',
      '2. Look at examples of it',
      '3. Introduce key terms',
      '-- '
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("to-list ordered", function()
    local lines = {
      'this is some text in markdown document',
      "    Section A",
      "      subsection a.1",
      "      subsection a.2",
      "    Section B",
      "      subsection b.1",
      "      subsection b.2",
      "      subsection b.3",
      "    Section C",
      "    Section D",
      "    Section E",
      "      subsection e.1",
      "-- "
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('--ordered')
        :set_vars({ vim_opts = { line1 = 2, line2 = 12 } })

    assert.same(true, M.cmd_to_list(w))
    local exp2 = {
      'this is some text in markdown document',
      '1. Section A',
      '  1. subsection a.1',
      '  2. subsection a.2',
      '2. Section B',
      '  1. subsection b.1',
      '  2. subsection b.2',
      '  3. subsection b.3',
      '3. Section C',
      '4. Section D',
      '5. Section E',
      '  1. subsection e.1',
      '-- '
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("match", function()
    local function f(line)
      local tab, title = string.match(line, '^##([#]*)%s+(.*)$')
      return tostring(tab) .. '|' .. tostring(title)
    end
    assert.same('|1', f('## 1'))
    assert.same('#|1', f('### 1'))
    assert.same('##|1', f('#### 1'))
  end)


  it("cmd_build_content", function()
    local lines = {
      '## title 1',
      "### subtitle 1.1",
      "some thext here",
      " # this is not a title",
      "https://some.ref",
      "(https://some.ref2)",
      '## title 2',
      "text (https://some.ref3)",
      '## title 3',
      "text (https://some.ref4) text",
      'text (http://some.ref5) text',
      "-- "
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('--urls')
        :set_vars({ vim_opts = { line1 = 1, line2 = 1 } })

    M.cmd_build_content(w)
    local exp2 = {
      '## title 1',
      '### subtitle 1.1',
      'some thext here',
      ' # this is not a title',
      'https://some.ref',
      '(https://some.ref2)',
      '## title 2',
      'text (https://some.ref3)',
      '## title 3',
      'text (https://some.ref4) text',
      'text (http://some.ref5) text',
      '-- ',
      '',
      '- title 1',
      '  - subtitle 1.1',
      '- title 2',
      '- title 3',
      '',
      'https://some.ref',
      'https://some.ref2',
      'https://some.ref3',
      'https://some.ref4',
      'http://some.ref5'
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_align_anchor", function()
    local line = '## title anchor'
    local anchor = string.match(line, '%s+([^%s]+)%s*$')
    assert.same('anchor', anchor)

    local lines = {
      -- 34567890123456789012345678
      '## this is a title   --anchor',
      '## строка с utf8 текстом anchor',
      "-- "
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('-w 40')
        :set_vars({ vim_opts = { line1 = 1, line2 = 3 } })

    M.cmd_align_anchor(w)

    local exp2 = {
      '## this is a title             --anchor',
      '## строка с utf8 текстом         anchor',
      '-- '
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_align_anchor", function()
    local lines = {
      '## this is a title',
      "-- "
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('anchor_from_arg -w 40')
        :set_vars({ vim_opts = { line1 = 1, line2 = 1 } })

    M.cmd_align_anchor(w)

    local exp2 = {
      '## this is a title      anchor_from_arg',
      '-- '
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_extract_urls", function()
    local lines = {
      '## title 1',
      "### subtitle 1.1",
      "some thext here",
      " # this is not a title",
      "https://some.ref",
      "(https://some.ref2)",
      '## title 2',
      "text (https://some.ref3)",
      '## title 3',
      "text (https://some.ref4) text",
      'text (http://some.ref5) text',
      "-- "
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of('-q'):set_vars({ vim_opts = { line1 = 1, line2 = 1 } })

    M.cmd_extract_urls(w)
    local exp2 = {
      '## title 1',
      '### subtitle 1.1',
      'some thext here',
      ' # this is not a title',
      'https://some.ref',
      '(https://some.ref2)',
      '## title 2',
      'text (https://some.ref3)',
      '## title 3',
      'text (https://some.ref4) text',
      'text (http://some.ref5) text',
      '-- ',
      '',
      'https://some.ref',
      'https://some.ref2',
      'https://some.ref3',
      'https://some.ref4',
      'http://some.ref5'
    }
    assert.same(exp2, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_extract_url_and_title", function()
    local lines = {
      '<div data-link="/en/blogpost/" class="archive-post archive-post-item link-item">',
      '<p class="archive-post-meta">',
      '<span class="archive-post-type">Type</span>',
      '<span class="archive-post-date">Date</span>',
      '</p>',
      '<div class="archive-post-container-web">',
      '<div class="archive-post-detail">',
      '<h1 class="archive-post-title">The Title of the Blogpost</h1>',
      '<div class="archive-post-image">',
      '<picture>',
      '<source type="image/webp" srcset="/img/someimg.webp">',
      '<img src="/img/someimg.jpg" loading="lazy" decoding="async" width="2448" height="1632">',
      '</picture>',
      '</div>',
      '</div>',
      '<div class="archive-post-summary"></div>',
      '</div>'
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of(''):set_vars({ vim_opts = { line1 = 1, line2 = #lines } })
    M.cmd_extract_url_and_title(w)
    local exp = { 'The Title of the Blogpost', '/en/blogpost/' }
    assert.same(exp, nvim:get_lines(bn, 0, -1))
  end)

  it("match", function()
    local f = function(line)
      local index, url = string.match(line, '^%[([%w]+)%]:%s*([^%s]+)')
      return index, url
    end
    assert.same({ '1', 'http://www.host.edu/' }, { f('[1]: http://www.host.edu/') })
    assert.same({ '1', 'http://www.host.edu/' }, { f('[1]:http://www.host.edu/') })
    assert.same({ '1', 'https://www.host.edu/' }, { f('[1]:https://www.host.edu/') })
    assert.same({ '1', 'https://w' }, { f('[1]:https://w') })
    assert.same({ '1', 'https://' }, { f('[1]:https://') })
  end)
end)

describe("env.command.lines_modify.markdown", function()
  it("cmd_from_formated_text", function()
    local lines = {
      'Method  domain          file        initiator  type',
      'GET     localhost:5173  /           document   html',
      'GET     localhost:5173  main.js?t=  script      js',
      'GET     localhost:5173  07.sass     script     js  '
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of(''):set_vars({ vim_opts = { line1 = 1, line2 = 4 } })
    M.table.cmd_from_formated_text(w)

    local exp = {
      '| Method |     domain     |    file    | initiator | type ',
      '|--------|----------------|------------|-----------|------',
      '| GET    | localhost:5173 | /          | document  | html ',
      '| GET    | localhost:5173 | main.js?t= | script    | js   ',
      '| GET    | localhost:5173 | 07.sass    | script    | js   '
    }
    assert.same(exp, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_reformat", function()
    local lines = {
      ' Method |     domain     |    file    | initiator | type ',
      '--------|----------------|------------|-----------|------',
      ' GET    | localhost:5173 | / | document  | html ',
      ' GET    | localhost:5173 | main.js?t= | script | js   ',
      ' GET    | localhost:5173 | 07.sass | script | js   ',
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of(''):set_vars({ vim_opts = { line1 = 1, line2 = 5 } })
    M.table.cmd_reformat(w)

    local exp = {
      '| Method |     domain     |    file    | initiator | type ',
      '|--------|----------------|------------|-----------|------',
      '| GET    | localhost:5173 | /          | document  | html ',
      '| GET    | localhost:5173 | main.js?t= | script    | js   ',
      '| GET    | localhost:5173 | 07.sass    | script    | js   '
    }
    assert.same(exp, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_reformat markdown", function()
    local lines = {
      '| Method |     domain     |    file    | initiator | type ',
      '|--------|----------------|------------|-----------|------',
      '| GET    | localhost:5173 | / | document  | html ',
      '| GET    | localhost:5173 | main.js?t= | script | js   ',
      '| GET    | localhost:5173 | 07.sass | script | js   ',
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of(''):set_vars({ vim_opts = { line1 = 1, line2 = 5 } })
    M.table.cmd_reformat(w)

    local exp = {
      '| Method |     domain     |    file    | initiator | type ',
      '|--------|----------------|------------|-----------|------',
      '| GET    | localhost:5173 | /          | document  | html ',
      '| GET    | localhost:5173 | main.js?t= | script    | js   ',
      '| GET    | localhost:5173 | 07.sass    | script    | js   '
    }
    assert.same(exp, nvim:get_lines(bn, 0, -1))
  end)

  it("cmd_from_raw_text", function()
    local lines = {
      'Claim 	Type 	Description 	ID Token 	endpoint',
      'sub 	string 	The ID of the user 	Yes 	Yes',
      'auth_time 	integer 	The timestamp 	Yes 	No',
      'name 	string 	The user’s full name 	Yes 	Yes'
    }
    local bn = nvim:new_buf(lines)
    local w = Cmd4Lua.of(''):set_vars({ vim_opts = { line1 = 1, line2 = 4 } })
    M.table.cmd_from_raw_text(w)

    local exp = {
      '|   Claim   |  Type   |     Description      | ID Token | endpoint ',
      '|-----------|---------|----------------------|----------|----------',
      '| sub       | string  | The ID of the user   | Yes      | Yes      ',
      '| auth_time | integer | The timestamp        | Yes      | No       ',
      '| name      | string  | The user’s full name | Yes      | Yes      '
    }
    assert.same(exp, nvim:get_lines(bn, 0, -1))
  end)
end)

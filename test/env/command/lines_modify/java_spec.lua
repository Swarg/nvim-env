-- 04-10-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local Cmd4Lua = require("cmd4lua")
local M = require 'env.command.lines_modify.java'

describe("env.command.lines_modify.java", function()
  after_each(function()
    nvim:clear_state()
  end)

  it("cmd_wrap_lines_to_string", function()
    local w = Cmd4Lua.of('')
    local lines = { -- selected
      '// --',
      'input errors',
      'No Required Argument#2',
      'Usage:',
      '    first-command <Arg0> <Arg1>',
      'Arguments:',
      '0   -  First Argument',
      '1   -  Second Argument',
      '// --',
    }
    nvim:new_buf(lines, 1, 0)
    w.vars.vim_opts = { line1 = 2, line2 = #lines - 1 }
    M.cmd_wrap_lines_to_string(w)

    local res = nvim:get_lines()
    local exp = {
      '// --',
      'String exp = ',
      '"input errors\\n" +',
      '"No Required Argument#2\\n" +',
      '"Usage:\\n" +',
      '"    first-command <Arg0> <Arg1>\\n" +',
      '"Arguments:\\n" +',
      '"0   -  First Argument\\n" +',
      '"1   -  Second Argument\\n";',
      '// --'
    }
    assert.same(exp, res) -- changed_lines
  end)
end)

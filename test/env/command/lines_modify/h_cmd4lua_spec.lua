--
require 'busted.runner' ()
local assert = require('luassert')
---@diagnostic disable-next-line: unused-local
local log = require('alogger')
local Cmd4Lua = require("cmd4lua")
local M = require("env.command.lines_modify.h_cmd4lua")

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
---@diagnostic disable-next-line: unused-local
local D = require("dprint")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim


describe("env.command.lines_modify.cmd4lua", function()
  after_each(function()
    nvim:clear_state()
  end)
  ------------------------------------------------------------------------------

  local function set_selected_lines(w, lines, off1, off2)
    assert(type(lines) == 'table', 'lines')
    nvim:new_buf(lines, 1, 0)
    off1 = off1 or 0
    off2 = off2 or 0
    w.vars.vim_opts = { line1 = 1 + off1, line2 = #lines + off2 }
  end

  it("cmd_to_new", function()
    -- if 0 == 0 then return end
    local w = Cmd4Lua.of('')
    set_selected_lines(w, {
      "  elseif w:desc('the description')",                -- 1
      "      :is_cmd('ebt', 'extract-between-tags') then", -- 2
      "    M.subcmd_extract_between_tags(w, opts)",        -- 3
    })

    M.cmd_convert_to_new(w)

    local exp = {
      '',
      "    :desc('the description')",
      "    :cmd('ebt', 'extract-between-tags')",
      '--    M.subcmd_extract_between_tags(w, opts)',
      '    :run()',
      'end',
      '',
      '',
      '--',
      '-- the description',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_extract_between_tags(w)',
      '  if not w:is_input_valid() then return end',
      '    M.subcmd_extract_between_tags(w, opts)',
      'end',
      ''
    }
    assert.same(exp, nvim:get_lines())
  end)


  it("cmd_to_new", function()
    local w = Cmd4Lua.of('')
    set_selected_lines(w, {
      "  if w:desc('show directory with a log file')",              -- 1
      "      :is_cmd('dir', 'path') then",                          -- 2
      "    print(log.get_logdir())",                                -- 3
      "    --",                                                     -- 4
      "  elseif w:desc('Open the current latest log file')",        -- 5
      "      :is_cmd('open', 'o') then",                            -- 6
      "    M.open()",                                               -- 7
      "    --",                                                     -- 8
      "  elseif w:desc('Change the logger settings')",              -- 13
      "      :is_cmd('set', 's') then",                             -- 14
      "    M.cmd_logger_set(w)",                                    -- 15
      "    --",                                                     -- 16
      "  elseif w:desc('Issue the log message with given params')", -- 17
      "      :is_cmd('message', 'm') then",                         -- 18
      "    print(M.cmd_logger_issue_message(w))",                   -- 27
      "    --",                                                     -- 28
      "  end",                                                      -- 29
      "  w:show_help_or_errors()",                                  -- 30
    })
    M.cmd_convert_to_new(w)
    local exp = {
      '',
      "    :desc('show directory with a log file')",
      "    :cmd('dir', 'path')",
      '--    print(log.get_logdir())',
      '',
      "    :desc('Open the current latest log file')",
      "    :cmd('open', 'o')",
      '--    M.open()',
      '',
      "    :desc('Change the logger settings')",
      "    :cmd('set', 's')",
      '--    M.cmd_logger_set(w)',
      '',
      "    :desc('Issue the log message with given params')",
      "    :cmd('message', 'm')",
      '--    print(M.cmd_logger_issue_message(w))',
      '--  end',
      '--  w:show_help_or_errors()',
      '    :run()',
      'end',
      '',
      '',
      '--',
      '-- show directory with a log file',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_path(w)',
      '  if not w:is_input_valid() then return end',
      '    print(log.get_logdir())',
      'end',
      '',
      '--',
      '-- Open the current latest log file',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_open(w)',
      '  if not w:is_input_valid() then return end',
      '    M.open()',
      'end',
      '',
      '--',
      '-- Change the logger settings',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_set(w)',
      '  if not w:is_input_valid() then return end',
      '    M.cmd_logger_set(w)',
      'end',
      '',
      '--',
      '-- Issue the log message with given params',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_message(w)',
      '  if not w:is_input_valid() then return end',
      '    print(M.cmd_logger_issue_message(w))',
      'end',
      ''
    }
    assert.same(exp, nvim:get_lines())
  end)


  it("cmd_gen_cmd_func", function()
    local w = Cmd4Lua.of('')
    set_selected_lines(w, {
      '',
      '      :desc("list of services in the ./docker-compose.yml")',
      '      :cmd("ls")',
      '',
      '      :desc("show info about service")',
      '      :cmd("info")',
      '',
    }, 0, -1)
    M.cmd_gen_cmd_func(w)
    local exp = {
      '',
      '      :desc("list of services in the ./docker-compose.yml")',
      '      :cmd("ls")',
      '',
      '      :desc("show info about service")',
      '      :cmd("info")',
      '',
      '--',
      '-- list of services in the ./docker-compose.yml',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_ls(w)',
      '  if not w:is_input_valid() then return end',
      'end',
      '',
      '',
      '--',
      '-- show info about service',
      '--',
      '---@param w Cmd4Lua',
      'function M.cmd_info(w)',
      '  if not w:is_input_valid() then return end',
      'end',
      ''
    }
    assert.same(exp, nvim:get_lines())
  end)
end)

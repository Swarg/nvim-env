-- 10-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.http");

describe("env.util.http", function()

  it("parse_reguest_method_uri_proto", function()
    local function f(s)
      local m, u, p = M.parse_reguest_method_uri_proto(s)
      return tostring(m) .. '|' .. tostring(u) .. '|' .. tostring(p)
    end
    assert.same('GET|/v2/lib/tags|HTTP/1.1', f('GET /v2/lib/tags HTTP/1.1'))
    assert.same('PUT|/|HTTP/1.1', f('PUT / HTTP/1.1'))
    assert.same('get|/|http/1.1', f('get / http/1.1'))
    assert.same('nil|nil|nil', f('get  http/1.1'))
    assert.same('get|/|1', f('get / 1'))
    assert.same('nil|nil|nil', f('get / '))
    assert.same('nil|nil|nil', f('0 / http/1.1'))
    assert.same('nil|nil|nil', f('. / http/1.1'))
    assert.same('nil|nil|nil', f('- / http/1.1'))
    assert.same('nil|nil|nil', f('_ / http/1.1'))
    assert.same('a|/|http/1.1', f('a / http/1.1'))
  end)

  it("parse_header_entry", function()
    local function f(s)
      local k, v = M.parse_header_entry(s)
      return tostring(k) .. '|' .. tostring(v)
    end

    assert.same('k|v', f('k:v'))
    assert.same('Hots|domain.net', f('Hots: domain.net'))
    assert.same('Hots|d', f('Hots: d'))
    assert.same('Hots|0', f('Hots: 0'))
    assert.same('Hots|a', f('Hots:a'))
    assert.same('Hots|', f('Hots:'))

    assert.same('nil|nil', f(':'))
    assert.same(' |', f(' : '))
    assert.same(' |', f('  : '))
  end)
end)

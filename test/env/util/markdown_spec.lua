-- 09-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.markdown");

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local D = require("dprint")
local R = require 'env.require_util'
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
vim.inspect = inspect

local bu = require('env.bufutil')

describe("env.util.markdown", function()
  before_each(function()
    nvim:clear_state()
    D.enable_all_modules(false)
  end)

  it("get_block_syntax", function()
    assert.same('bash', M.get_block_syntax({ md_block_syntax = 'bash' }))
    assert.is_nil(M.get_block_syntax({ md_block_syntax = false }))
    assert.is_nil(M.get_block_syntax({}))
    assert.is_nil(M.get_block_syntax(nil))
  end)

  it("is_inside_block success", function()
    local lines, lnum = { '--', '```sh', "date", '```' }, 3
    nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same('date', vim.api.nvim_get_current_line())
    assert.same(true, M.is_inside_block(cbi))
    assert.same(true, M.is_shell_block(cbi))
  end)

  it("is_inside_block fail", function()
    local lines, lnum = { '--', '```sh', "date", '```' }, 2
    nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same('```sh', vim.api.nvim_get_current_line())
    assert.same(false, M.is_inside_block(cbi))
    assert.same(false, M.is_shell_block(cbi))
  end)

  it("is_inside_block fail 2", function()
    local lines, lnum = { '--', '```sh', "date", '```' }, 4
    nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same('```', vim.api.nvim_get_current_line())
    assert.same(false, M.is_inside_block(cbi))
    assert.same(false, M.is_shell_block(cbi))
  end)

  it("is_inside_block fail 3", function()
    local lines, lnum = { '--', '```sh', "date", '```', 'out-of', 'md' }, 6
    nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same('md', vim.api.nvim_get_current_line())
    -- D.enable_module(M)
    assert.same(false, M.is_inside_block(cbi))
    assert.same('', cbi.md_block_syntax)
    assert.same(false, M.is_shell_block(cbi))
  end)

  it("is_inside_block fail 4", function()
    local lines, lnum = { '--', '```sh', "", "date", '```', 'out-of' }, 3
    local bn = nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same('', vim.api.nvim_get_current_line())
    -- D.enable_module(M)
    assert.same(true, M.is_inside_block(cbi))
    assert.same('sh', cbi.md_block_syntax)
    assert.same(true, M.is_shell_block(cbi))
    local res = M.get_full_bash_command(bn, lnum)
    assert.same({ err = 'Empty string' }, res)
  end)

  --

  -- require'dprint'.enable_module(require("env.util.bash")) -- DEBUG_PRINT

  it("get_full_bash_command 1", function()
    local lines, lnum = { '--', '```sh', "date", '```' }, 3
    local bn = nvim:new_buf(lines, lnum)
    -- nvim.logger_setup()
    -- vim.inspect = inspect
    local cbi = bu.current_buf_info()
    assert.same(true, M.is_inside_block(cbi))
    local res = M.get_full_bash_command(bn, lnum)
    assert.same({ cmd = 'date', args = {} }, res)
  end)

  it("get_full_bash_command err out of block", function()
    local lines, lnum = { '--', '```sh', "date +%s", '```' }, 1
    local bn = nvim:new_buf(lines, lnum)
    local cbi = bu.current_buf_info()
    assert.same(false, M.is_inside_block(cbi))
    local exp = { err = 'too few lines to recognize command' }
    assert.same(exp, M.get_full_bash_command(bn, lnum))
  end)

  it("get_full_bash_command err too few lines", function()
    local lines, lnum = { '```sh', }, 1
    local res = M.get_full_bash_command(nvim:new_buf(lines, lnum), lnum)
    local exp = { err = 'too few lines to recognize command' }
    assert.same(exp, res)
  end)

  it("get_full_bash_command args with space", function()
    local lines, lnum = { '--', '```sh', "echo 'a b' 'c d' e", '```' }, 3
    local res = M.get_full_bash_command(nvim:new_buf(lines, lnum), lnum)
    local exp = { cmd = 'echo', args = { 'a b', 'c d', 'e' } }
    assert.same(exp, res) -- ??
  end)

  it("get_full_bash_command multiline + quotes", function()
    local lnum, lines = 2, {
      '```sh',
      "echo 1 | echo '", --2
      'multi',
      ' "li ne"',
      "'",
      '```',
    }
    local res = M.get_full_bash_command(nvim:new_buf(lines, lnum), lnum)
    local exp = {
      cmd = 'bash', args = { '-c', 'echo 1 | echo \' multi  \"li ne\" \'' }
    }
    assert.same(exp, res)
  end)

  it("has_not_closed_quote", function()
    local f = M.get_not_closed_quote
    assert.same(nil, f('""'))
    assert.same('"', f('"""'))
    assert.same(nil, f('\'"""\''))
    assert.same(nil, f('"\'"'))
    assert.same('"', f('"'))
    assert.same("'", f("'"))
  end)

  it("get_full_bash_command multiline", function()
    local lines, lnum = { '```sh', "cmd arg1 \\", "--key value", '```' }, 2
    local res = M.get_full_bash_command(nvim:new_buf(lines, lnum), lnum)
    local exp = { cmd = 'cmd', args = { 'arg1', '--key', 'value' } }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 2", function()
    local lines = { -- 2        3         4          5     6     7      8
      '```sh', "cmd a1 \\", "-k v \\", "-h v2 \\", 'a2', '```', '111', '222'
      --                               ^first load  ^second
    }
    local res = M.get_full_bash_command(nvim:new_buf(lines, 2), 2)
    local exp = { cmd = 'cmd', args = { 'a1', '-k', 'v', '-h', 'v2', 'a2' } }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 3", function()
    local lines = { '```sh', "cmd 2 \\", "3 \\", "4 \\", '5 \\', '```' }
    local ln = 2
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = {
      cmd = 'cmd',
      args = { '2', '3', '4', '5' },
      not_finished_line = true
    }
    assert.same(exp, res)
  end)

  -- issue: if there is no additional line(```) at the end then
  -- the last line will not be picked up (will be without f)
  it("get_full_bash_command multiline 4", function()
    local lines = {
      '```sh', "c 2 \\", "3 \\", "4 \\", '5 \\', '6 \\', '7 \\', '8 \\',
      "9 \\", "a \\", 'b \\', 'c \\', 'd \\', 'e \\', 'f', '```'
    }
    local ln = 2
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = {
      cmd = 'c',
      args = {
        '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
      }
    }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 5", function()
    local lines = { '```sh', "cmd 2 \\", "3 \\", "4 \\", '5 \\', '```', 'x' }
    local ln = 2
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = {
      cmd = 'cmd',
      args = { '2', '3', '4', '5' },
      not_finished_line = true
    }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 6", function()
    local lines = { '```sh', "cmd 2 \\", "3 \\", "4 \\", '5 \\', '```', 'x' }
    local ln = 3
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = {
      err = 'Its not a begin of the shell command. Previous line ends with \\'
    }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 7", function()
    local ln, lines = 2, { '```sh', "cmd 2 \\", "# 3 \\", "4", '```', 'x' }
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = { cmd = 'cmd', args = { '2', '4' }, }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 8", function()
    local ln, lines = 2, { '```sh', "cmd 2 \\", "3 \\", "# 4", '```', 'x' }
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = { cmd = 'cmd', args = { '2', '3' }, not_finished_line = true }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 9", function()
    local ln, lines = 2, { '```sh', "# cmd 2 \\", "3 \\", "4", '```', 'x' }
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = { err = 'Its a comment not a shell command.' }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 10", function()
    local ln, lines = 2, { '```sh', "#", "cmd 3 \\", "4", '```', 'x' }
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = { err = 'Its a comment not a shell command.' }
    assert.same(exp, res)
  end)

  it("get_full_bash_command multiline 11 empty line", function()
    local ln, lines = 2, { '```sh', "cmd 2 \\", "", "4 \\", '```', 'x' }
    local res = M.get_full_bash_command(nvim:new_buf(lines, ln), ln)
    local exp = { not_finished_line = true, cmd = 'cmd', args = { '2' } }
    assert.same(exp, res)
  end)

  it("is_syntax_block_head yes", function()
    local ln, lines = 2, { '--', '```sh', "cd my-dir", "ls", '```', ' ' }
    nvim:new_buf(lines, ln)
    local cbi = bu.current_buf_info()
    assert.same(true, M.is_syntax_block_head(cbi))
  end)

  it("is_syntax_block_head yes", function()
    local ln, lines = 3, { '--', '```sh', "cd my-dir", "ls", '```', ' ' }
    nvim:new_buf(lines, ln)
    local cbi = bu.current_buf_info()
    assert.same(false, M.is_syntax_block_head(cbi))
  end)

  it("is_syntax_block_head yes", function()
    local ln, lines = 99, { '--', '```sh', "cd my-dir", "ls", '```', ' ' }
    nvim:new_buf(lines, ln)
    local cbi = bu.current_buf_info()
    assert.same(false, M.is_syntax_block_head(cbi))
  end)

  --
  it("get_syntax_of_block_head", function()
    local f = M.get_syntax_of_block_head
    assert.same(nil, f(''))
    assert.same(nil, f('`'))
    assert.same('', f('```'))
    assert.same('x', f('```x'))
    assert.same('x', f('```x  '))
    assert.same('', f('``` x')) -- ?
    assert.same('http', f('```http'))
    assert.same('http', f('```http s'))
  end)

  it("get_http_request ip:port", function()
    local ln, lines = 2, {
      '--',
      '```http',
      "GET / HTTP/1.1",
      "PROXY: http://127.0.0.1:8080",
      'Host: domain.io',
      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    local exp = {
      method = 'GET',
      headers = { host = 'domain.io' },
      proxy = 'http://127.0.0.1:8080',
      proto = 'HTTP/1.1',
      uri = '/',
      scheme = 'http',
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  --
  it("get_http_request get", function()
    local ln, lines = 2, {
      '--',
      '```http',
      "GET /url HTTP/1.1",
      "PROXY: http://127.0.0.1:80", -- my custom header
      'Host: registry.domain.io',
      'User-Agent: curl',
      'Accept: */*',

      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    -- nvim.logger_setup()
    local exp = {
      scheme = 'http',
      method = 'GET',
      uri = '/url',
      proto = 'HTTP/1.1',
      headers = {
        host = 'registry.domain.io',
        ['user-agent'] = 'curl',
        accept = '*/*'
      },
      proxy = 'http://127.0.0.1:80',
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  it("get_http_request delete", function()
    local ln, lines = 2, {
      '--',
      '```http',
      "DELETE /users/3 HTTP/1.1",
      'Host: localhost',
      'User-Agent: curl',
      'Accept: application.json',

      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    local exp = {
      scheme = 'http',
      method = 'DELETE',
      uri = '/users/3',
      proto = 'HTTP/1.1',
      headers = {
        accept = 'application.json',
        host = 'localhost',
        ['user-agent'] = 'curl',
      },
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  it("get_http_request post + json body", function()
    local ln, lines = 2, {
      '--',
      '```http',
      "GET /url HTTP/1.1",
      "PROXY: http://127.0.0.1:80", -- my custom header
      'Host: registry.domain.io',
      'User-Agent: curl',
      'Accept: */*',

      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    local exp = {
      scheme = 'http',
      method = 'GET',
      uri = '/url',
      proto = 'HTTP/1.1',
      headers = {
        host = 'registry.domain.io',
        ['user-agent'] = 'curl',
        accept = '*/*'
      },
      proxy = 'http://127.0.0.1:80',
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  it("get_http_request post app/json", function()
    local ln, lines = 2, {
      '--',
      '```http',
      'POST /test HTTP/1.1',
      'Host: localhost:8080',
      "Accept: application/json",
      "Content-Type: application/json",
      'Content-Length: *',
      '',
      '{',
      '  "field1"="value1","field2"="value2"',
      '}',
      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    local exp = {
      scheme = 'http',
      method = 'POST',
      uri = '/test',
      proto = 'HTTP/1.1',
      headers = {
        accept = 'application/json',
        host = 'localhost:8080',
        ['content-length'] = '41',
        ['content-type'] = 'application/json',
      },
      body = "{\n  \"field1\"=\"value1\",\"field2\"=\"value2\"\n}",
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  --
  it("get_http_request post urlencoded", function()
    local ln, lines = 2, {
      '--',
      '```http',
      'POST /test HTTP/1.1',
      'Host: foo.example',
      'Content-Type: application/x-www-form-urlencoded',
      'Content-Length: 27',
      '',
      'field1=value1&field2=value2',
      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    -- nvim.logger_setup()
    local exp = {
      scheme = 'http',
      method = 'POST',
      body = 'field1=value1&field2=value2',
      uri = '/test',
      proto = 'HTTP/1.1',
      headers = {
        host = 'foo.example',
        ['content-type'] = 'application/x-www-form-urlencoded',
        ['content-length'] = '27'
      }
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)


  --
  it("get_http_request post urlencoded", function()
    local ln, lines = 2, {
      '--',
      '```http',
      'POST /test HTTP/1.1',
      'Host: foo.example',
      'Content-Type: multipart/form-data;boundary="boundary"',
      '',
      '--boundary',
      'Content-Disposition: form-data; name="field1"',
      '',
      'value1',
      '--boundary',
      'Content-Disposition: form-data; name="field2"; filename="example.txt"',
      '',
      'value2',
      '--boundary--',
      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    -- nvim.logger_setup()
    local exp = {
      scheme = 'http',
      method = 'POST',
      uri = '/test',
      proto = 'HTTP/1.1',
      headers = {
        host = 'foo.example',
        ['content-type'] = 'multipart/form-data;boundary="boundary"'
      },
      body = [[
--boundary
Content-Disposition: form-data; name="field1"

value1
--boundary
Content-Disposition: form-data; name="field2"; filename="example.txt"

value2
--boundary--]]
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)

  --
  it("get_http_request post urlencoded", function()
    local ln, lines = 2, {
      '--',
      '```http',
      'POST /test HTTP/1.1',
      'Host: foo.example',
      'Content-Length: *', --- asterisk means automatic length calculation
      '',
      '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
      '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
      '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
      '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
      '```',
    }
    local bn = nvim:new_buf(lines, ln)
    -- nvim.logger_setup()
    local exp = {
      scheme = 'http',
      method = 'POST',
      uri = '/test',
      proto = 'HTTP/1.1',
      headers = { host = 'foo.example', ['content-length'] = '25' },
      body = "1\n2\n3\n4\n5\n6\n7\n8\n9\n0\n11\n12",
    }
    assert.same(exp, M.get_http_request(bn, ln))
  end)


  it("lines_to_table_obj", function()
    local f = M.lines_to_table_obj
    local lines = {
      '| Method | domain | file | initiator | type',
      '|--------|--------|------------|-----------|------',
      '| GET | localhost:5173 | / | document | html',
      '| GET | localhost:5173 | main.js?t= | script | js'
    }
    local exp = {
      { 'Method', 'domain',         'file',       'initiator', 'type' },
      { 'GET',    'localhost:5173', '/',          'document',  'html' },
      { 'GET',    'localhost:5173', 'main.js?t=', 'script',    'js' }
    }
    assert.same(exp, f(lines))
  end)
end)

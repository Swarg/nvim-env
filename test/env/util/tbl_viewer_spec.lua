--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")

local R = require("env.require_util")
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect


local nvim = NVim:new() ---@type stub.vim.NVim

describe("env.util.tbl_viewer", function()
  setup(function()
    -- expose private function for testing
    _G._TEST = true
    M = require("env.util.tbl_viewer")
  end)

  teardown(function()
    _G._TEST = nil
  end)

  after_each(function()
    nvim:clear_state()
  end)

  it("get_opened_for", function()
    local o = { opened = {} }
    local path = { 'sub1', 'sub2', 'sub3', 'sub4' }
    local res = M.get_opened_for(o, path)

    local exp = { sub1 = { sub2 = { sub3 = { sub4 = {} } } } }
    assert.same(exp, o.opened)
    assert.is_table(o.opened.sub1)
    assert.is_table(o.opened.sub1.sub2)
    assert.is_table(o.opened.sub1.sub2.sub3)
    assert.is_table(o.opened.sub1.sub2.sub3.sub4)
    -- one instance
    assert.is_true(o.opened.sub1.sub2.sub3.sub4 == res)
    assert.same(tostring(o.opened.sub1.sub2.sub3.sub4), tostring(res))
    assert.is_false({} == res) -- compare by instance not by value
    assert.same({}, res)
  end)

  it("is_opened", function()
    local o = { opened = {} }
    local current_opened_path = { 'a', 'b', 'c', 'd', 'e' }
    -- mk opened path
    local _ = M.get_opened_for(o, current_opened_path)
    assert.is_table(o.opened.a.b.c.d)
    local cwd = o.opened.a.b.c.d.e
    local rel_path = {}
    assert.same(true, M.is_opened(cwd, rel_path))
    --                                \ same  ^-------v
    assert.same(true, M.is_opened(o.opened.a.b.c.d.e, {}))

    assert.same(false, M.is_opened(o.opened.a.b.c.d.e, { 'f' }))
    assert.same(true, M.is_opened(o.opened.a.b.c.d, { 'e' }))
    assert.same(true, M.is_opened(o.opened, { 'a' }))
    assert.same(true, M.is_opened(o.opened, { 'a', 'b' }))
    assert.same(true, M.is_opened(o.opened, { 'a', 'b', 'c', 'd', 'e' }))
    assert.same(false, M.is_opened(o.opened, { 'a', 'b', 'X', 'd', 'e' }))
    assert.same(false, M.is_opened(o.opened, { 'X', 'b', 'c', 'd', 'e' }))
    assert.same(false, M.is_opened(o.opened, { 'a', 'b', 'c', 'd', 'X' }))
    assert.same(false, M.is_opened(o.opened, { 'a', 'b', 'c', 'd', 'e', 'f' }))
    assert.same(true, M.is_opened(o.opened.a, { 'b', 'c', 'd', 'e' }))
    assert.same(true, M.is_opened(o.opened.a.b, { 'c', 'd', 'e' }))
    assert.same(true, M.is_opened(o.opened.a.b.c, { 'd', 'e' }))
    assert.same(false, M.is_opened(o.opened.a.b.c.d, { 'd', 'e' }))
    assert.same(true, M.is_opened(o.opened.a.b.c.d, { 'e' }))
    assert.same(false, M.is_opened(o.opened.a.b.c.d.e, { 'e' }))
    assert.same(true, M.is_opened(o.opened.a.b.c.d.e, {}))
  end)

  it("get_parent", function()
    local o = { opened = {} }
    local current_opened_path = { 'a', 'b', 'c', 'd', 'e' }
    -- mk opened path
    local _ = M.get_opened_for(o, current_opened_path)
    assert.is_table(o.opened.a.b.c.d)
    local root = o.opened
    assert.same(root.a.b.c.d.e, M.get_parent(root.a.b.c.d.e, {}))
    assert.same(root.a.b.c.d, M.get_parent(root.a.b.c.d, { 'e' }))
    assert.same(root.a.b.c.d, M.get_parent(root.a.b.c, { 'd', 'e' }))
    assert.same(root.a.b.c, M.get_parent(root.a.b.c, { 'd' }))
    assert.same(root.a.b.c.d, M.get_parent(root.a.b, { 'c', 'd', 'e' }))
    assert.same(root.a.b.c.d, M.get_parent(root.a.b, { 'c', 'd', 'e' }))
    assert.same(root.a.b.c, M.get_parent(root.a.b, { 'c', 'd' }))
    assert.same(root.a, M.get_parent(root, { 'a', 'b' }))
    assert.same(root.a.b, M.get_parent(root, { 'a', 'b', 'c' }))
    assert.same(root.a.b.c.d, M.get_parent(root, { 'a', 'b', 'c', 'd', 'e' }))
  end)

  -- helper
  -- ret o - the obj wrapped to the state and stored at M.table[burnr]
  local function set_to_view(obj, bufnr, src)
    ---@diagnostic disable-next-line: inject-field
    M.LINE_WIDTH = 40 -- def is 80
    return M.bind_state(M.wrap2state(obj, src or 'mem'), bufnr)
  end

  it("check vim.api stub", function()
    local bufnr = nvim:new_buf({}, 1, 0)
    assert(bufnr, vim.api.nvim_get_current_buf())
  end)

  it("toggle_open_close without obj", function()
    nvim:new_buf({}, 1, 0, 4)
    assert.is_nil(M.do_toggle_open_close())
  end)

  it("set_to_view state wrapper", function()
    local bufnr = nvim:new_buf({})
    local obj_table = { a = 'x', b = {}, c = 8 }

    local o = set_to_view(obj_table, bufnr)

    assert.is_not_nil(o)
    assert.same(obj_table, o.root)
    assert.same(obj_table, o.cwd)
    assert.is_nil(o.draw)
    assert.is_nil(o.mappings)
  end)

  --
  it("toggle_open_close", function()
    local obj_table = {
      a0 = 'x',
      a1 = { a1_b1 = { a1_b1_c1 = 'x', a1_b1_c2 = 1, }, a1_b2 = {} },
      a2 = { a2_b1 = {} },
      a3 = { a3_b1 = {}, a2_b2 = {} },
    }
    local bufnr = nvim:new_buf({}, 2, 1)

    local o = set_to_view(obj_table, bufnr)
    M.draw2buf(o, bufnr)

    local exp_draw = {
      info = { ' (table)', ' (table)', ' (table)', ' (string)' },
      lines = { '+ [a1]', '+ [a2]', '+ [a3]', "  a0   : 'x'" }
    }
    assert.same(exp_draw, o.draw)

    local exp_mappings = { { 'a1' }, { 'a2' }, { 'a3' }, { 'a0' } }
    assert.same(exp_mappings, o.mappings)

    -- before toggle - initial render
    local exp = {
      '+ [a1]                          (table)',
      '+ [a2]                          (table)',
      '+ [a3]                          (table)',
      "  a0   : 'x'                    (string)"
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- toggle from close to open
    assert.is_not_nil(M.do_toggle_open_close())
    local exp2 = {
      '+ [a1]                          (table)',
      '- [a2]                          (table)',
      '  + [a2_b1]                     (table)',
      '+ [a3]                          (table)',
      "  a0   : 'x'                    (string)"
    }
    assert.same(exp2, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- toggle from open to close
    assert.is_not_nil(M.do_toggle_open_close())
    local exp3 = {
      '+ [a1]                          (table)',
      '+ [a2]                          (table)',
      '+ [a3]                          (table)',
      "  a0   : 'x'                    (string)"
    }
    assert.same(exp3, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("sort_keys", function()
    local obj = {
      [1] = { t1 = 't1' },
      [2] = { t2 = 't2' },
      [3] = { t3 = 't3' },
      file = 'sval',
      dir = {}
    }
    local keys, max_dir, max_file = M.sort_keys(obj)
    assert.same({ 'dir', 1, 2, 3, 'file' }, keys)
    assert.same(3, max_dir)
    assert.same(4, max_file)
  end)

  it("append_value", function()
    local max = #'key-fghk'
    local f = M.build_line0
    assert.same('  k          : 8', f('', 'k', 8, max))
    assert.same("  key        : 'value'", f('', 'key', 'value', max))
    assert.same("  key-b      : 'value-3'", f('', 'key-b', 'value-3', max))
    assert.same("  key-cde    : 'long-val4'", f('', 'key-cde', 'long-val4', max))
    assert.same("  key-fghk   : 'the-long-value-4'", f('', 'key-fghk', 'the-long-value-4', max))
  end)

  it("render values", function()
    local obj_table = {
      a0_1 = 'x',
      key_a0_2 = 'value2',
      k_a0_3 = 'value - 3',
      a1 = {},
      dir_a2 = {},
    }
    local bufnr = nvim:new_buf({}, 2, 1)
    local o = set_to_view(obj_table, bufnr)
    M.draw2buf(o, bufnr)

    local exp_draw_lines = {
      '+ [a1]',
      '+ [dir_a2]',
      "  a0_1       : 'x'",
      "  k_a0_3     : 'value - 3'",
      "  key_a0_2   : 'value2'"
    }
    assert.same(exp_draw_lines, o.draw.lines)
  end)

  it("get_absolute_path_s", function()
    local f = M.get_absolute_path_s
    local o = { path = { 'a', 'b', 'c' } }
    local rel = { 'd', 'e' }
    assert.same('/a/b/c/d/e', f(o, rel, '/'))

    assert.same('/d/e', f({ path = nil }, { 'd', 'e' }, '/'))
    assert.same('/../e', f({ path = nil }, { '..', 'e' }, '/')) -- todo up by ..
  end)

  it("node_name_cb", function()
    local obj = {
      node1 = { key = 'value1' },
      node2 = { key = 'value2' },
    }
    local bufnr = nvim:new_buf({}, 2, 1)
    local o = set_to_view(obj, bufnr)
    o.node_name_cb = function(t)
      return t.key
    end
    M.draw2buf(o, bufnr)
    local exp = { '+ [node1] value1', '+ [node2] value2' }
    assert.same(exp, o.draw.lines)
  end)


  it("grep", function()
    local state = {
      root = {
        a = {
          subkey = "SUBstring"
        },
        b = {
          subkey2 = "somethign"
        },
        c = "abcSUBdef",
        d = "SUBabdsdhsjdhjshdjshdjsdlllcddshjdhjsdhjsdhjshdjshdjshdjshdjshdef",
        e = "abdsdhsjdhjshdjshdjsdlllcSUBddshjdhjsdhjsdhjshdjshdjshdjshdjshdef",
        f = "abdsdhsjdhjshdjshdjsdlllcddshjdhjsdhjsdhjshdjshdjshdjshdjshdefSUB",
      }
    }
    local res = M.grep(state, 'SUB')
    local exp = {
      ' a subkey : |SUBstring|',
      ' c : |abcSUBdef|',
      ' e : (6)|..hsjdhjshdjshdjsdlllcSUBddshjdhjsdhjsdhjsh..|(46/65)',
      ' d : |SUBabdsdhsjdhjshdjshd..|(21/65)',
      ' f : (43)|..hdjshdjshdjshdjshdefSUB|'
    }
    assert.same(exp, res)
  end)
end)

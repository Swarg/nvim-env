require 'busted.runner' ()
local assert = require('luassert')
local M = require 'env.util.url'

describe('env.util.url', function()
  it("urlencode", function()
    assert.same("/some string?bar=foo", M.url_decode("/some%20string?bar=foo"))
  end)

  it("urldecode", function()
    assert.same('%2Fsome+str%3Fbar%3Dfoo', M.url_encode("/some str?bar=foo"))
  end)
end)

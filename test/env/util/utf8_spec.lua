-- 14-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.utf8");

describe("env.util.utf8", function()
  --
  it("show_char - manual", function()
    if 0 == 0 then return end
    local function show_char(c)
      local b1, b2, b3, b4 = string.byte(c, 1, 4)
      print(c, string.byte(c), string.len(c), b1, b2, b3, b4)
    end
    show_char(0)
    show_char(9)
    show_char('а')
    show_char('я')
    show_char('a')
    show_char('z')
    show_char('A')
    show_char('Z')
    show_char('∙')
  end)

  it("utf8_slen", function()
    assert.same(1, M.utf8_slen('я'))
    assert.same(4, M.utf8_slen('abcd'))
    assert.same(4, M.utf8_slen('ЯяФф'))
    assert.same(5, M.utf8_slen('abcdЯ'))
    assert.same(5, M.utf8_slen('Яabcd'))
    assert.same(7, M.utf8_slen('ЯУЩabcd'))
  end)

  it("utf8_fold", function()
    local line = 'способность быстрого восприятия; понимание полное,' ..
        ' глубокое понимание, понимание на интуитивном уровне'
    -- ..'  --------- -------- ----- ------------------------------ ----------'
    local exp = {
      'способность быстрого восприятия; понимание полное, глубокое понимание, ',
      'понимание на интуитивном уровне'
    }
    assert.same(exp, M.utf8_fold(line, 80))
  end)

  it("is_utf8_ru", function()
    local function f(letter, lower)
      return M.is_utf8_ru(letter:sub(1, 1), letter:sub(2, 2), lower)
    end

    assert.same(true, f('а'))
    assert.same(true, f('А'))
    assert.same(true, f('р'))
    assert.same(true, f('с'))
    assert.same(true, f('я'))
    assert.same(true, f('Я'))

    assert.same(true, f('Р'))
    assert.same(true, f('С'))

    local lower = true
    assert.same(true, f('я', lower))
    assert.same(false, f('Я', lower))
    assert.same(true, f('а', lower))
    assert.same(false, f('А', lower))
  end)

  it("utf8_fold", function()
    assert.same({ 'abc' }, M.utf8_fold('abc', 10))
    assert.same({ '1', '234567890abc de' }, M.utf8_fold('1234567890abc de', 10))
  end)

  it("is_ascii_letter", function()
    assert.same(true, M.is_ascii_letter(string.byte('A', 1, 1))) -- A -> 65
    assert.same(true, M.is_ascii_letter(string.byte('Z', 1, 1)))
    assert.same(true, M.is_ascii_letter(string.byte('a', 1, 1)))
    assert.same(true, M.is_ascii_letter(string.byte('z', 1, 1)))
    assert.same(false, M.is_ascii_letter(string.byte('!', 1, 1)))
    assert.same(false, M.is_ascii_letter(string.byte(' ', 1, 1)))
    assert.same(false, M.is_ascii_letter(string.byte('0', 1, 1)))
    assert.same(false, M.is_ascii_letter(string.byte('Я', 1, 1)))
  end)

  it("show_all_bytes", function()
    local f = M.show_all_bytes
    assert.same(' 0x61', f('a'))
    assert.same(' 0x61 0x41', f('aA'))
    assert.same(' 0xD1 0x8F', f('я'))
    assert.same(' 0xD0 0xAF', f('Я'))
    assert.same(' 0xD1 0x80', f('р')) -- ru

    assert.same(' 0xC2 0xAB', f('«'))
    assert.same(' 0xC2 0xBB', f('»'))

    assert.same(' 0xE2 0x80 0x9C', f('“'))
    assert.same(' 0xE2 0x80 0x9C', f('“'))
    assert.same(' 0xE2 0x80 0x9D', f('”'))
    assert.same(' 0xE2 0x80 0x98', f('‘'))
    assert.same(' 0xE2 0x80 0x99', f('’'))
    assert.same(' 0xE2 0x80 0x93', f('–'))
    assert.same(' 0xE2 0x80 0x94', f('—'))
    assert.same(' 0xE2 0x80 0xA6', f('…'))
    assert.same(' 0xE2 0x80 0xA2', f('•'))
  end)


  it("mk_simple_punctuation", function()
    local f = M.mk_simple_punctuation
    assert.same('абвг"деф"кл abc"de"', f('абвг«деф»кл abc«de»'))
    assert.same('""', f('«»'))
    assert.same('"', f('«'))
    assert.same('0"0', f('0“0'))
    assert.same('ф"ф', f('ф”ф'))
    assert.same("ф'ф", f('ф‘ф'))
    assert.same("ф'ф", f('ф’ф'))
    assert.same('ф-ф', f('ф–ф'))
    assert.same('ф-ф', f('ф—ф'))
    assert.same('ф*ф', f('ф•ф'))
    assert.same('ф...ф', f('ф…ф'))

    assert.same('i"z', f('i“z'))
    assert.same('i"z', f('i”z'))
    assert.same("i'z", f('i‘z'))
    assert.same("i'z", f('i’z'))
    assert.same('i-z', f('i–z'))
    assert.same('i-z', f('i—z'))
    assert.same('i*z', f('i•z'))
    assert.same('i...z', f('i…z'))

    assert.same('i"z"', f('i“z“'))
    assert.same('i"z"', f('i”z”'))
    assert.same("i'z'", f('i‘z‘'))
    assert.same("i'z'", f('i’z’'))
    assert.same('i-z-', f('i–z–'))
    assert.same('i-z-', f('i—z—'))
    assert.same('i*z*', f('i•z•'))
    assert.same('i...z...', f('i…z…'))

    assert.same('""', f('““'))
    assert.same('""', f('””'))
    assert.same("''", f('‘‘'))
    assert.same("''", f('’’'))
    assert.same('--', f('––'))
    assert.same('--', f('——'))
    assert.same('**', f('••'))
    assert.same('......', f('……'))
  end)

  it("split_words", function()
    local f = M.split_words
    assert.same({ 'abc', 'de' }, f('abc de'))
    assert.same({ 'абв', 'гд' }, f('абв гд'))
    assert.same({ 'абв', 'гд', 'ежз', 'клмн' }, f('абв гд ежз клмн'))

    local exp = {
      brange = { { 1, 3 }, { 5, 6 }, { 8, 8 } },
      crange = { { 1, 3 }, { 5, 6 }, { 8, 8 } },
      'abc',
      'de',
      'f'
    }
    local res = f('abc de f', true)
    assert.same(exp, res)

    local exp2 = {
      brange = { { 1, 6 }, { 8, 11 }, { 13, 18 }, { 20, 27 } },
      crange = { { 1, 3 }, { 5, 6 }, { 8, 10 }, { 12, 15 } },
      'абв',
      'гд',
      'ежз',
      'клмн'
    }
    local res2 = f('абв гд ежз клмн', true)
    assert.same(exp2, res2)

    local exp3 = {
      brange = { { 1, 6 }, { 8, 11 }, { 13, 18 }, { 21, 28 } },
      crange = { { 1, 3 }, { 5, 6 }, { 8, 10 }, { 13, 16 } },
      'абв',
      'гд',
      'ежз',
      'клмн'
    }
    local res3 = f('абв гд ежз  клмн', true)
    assert.same(exp3, res3)
  end)


  it("get_word_at crange", function()
    local words = {
      brange = { { 1, 6 }, { 8, 11 }, { 13, 18 }, { 21, 28 } },
      crange = { { 1, 3 }, { 5, 6 }, { 8, 10 }, { 13, 16 } },
      'абв',
      'гд',
      'ежз',
      'клмн'
    }
    local f = function(w, pos, as_char_pos)
      local word, idx = M.get_word_at(w, pos, as_char_pos)
      return tostring(word) .. '|' .. tostring(idx)
    end

    assert.same('nil|-1', f(words, 0, true))
    assert.same('абв|1', f(words, 1, true))
    assert.same('абв|1', f(words, 2, true))
    assert.same('абв|1', f(words, 3, true))
    assert.same('гд|2', f(words, 5, true))
    assert.same('гд|2', f(words, 6, true))
    assert.same('nil|-1', f(words, 7, true))
    assert.same('ежз|3', f(words, 8, true))
    assert.same('ежз|3', f(words, 10, true))
    assert.same('nil|-1', f(words, 11, true))
    assert.same('nil|-1', f(words, 12, true))
    assert.same('клмн|4', f(words, 13, true))
    assert.same('клмн|4', f(words, 16, true))
    assert.same('nil|-1', f(words, 17, true))
  end)

  it("get_word_at brange", function()
    local words = {
      brange = { { 1, 6 }, { 8, 11 }, { 13, 18 }, { 21, 28 } },
      crange = { { 1, 3 }, { 5, 6 }, { 8, 10 }, { 13, 16 } },
      'абв',
      'гд',
      'ежз',
      'клмн'
    }
    local f = function(w, pos, as_char_pos)
      local word, idx = M.get_word_at(w, pos, as_char_pos)
      return tostring(word) .. '|' .. tostring(idx)
    end

    assert.same('nil|-1', f(words, 0, false))
    assert.same('абв|1', f(words, 1, false))
    assert.same('абв|1', f(words, 2, false))
    assert.same('абв|1', f(words, 6, false))
    assert.same('гд|2', f(words, 9, false))
    assert.same('ежз|3', f(words, 14, false))
    assert.same('клмн|4', f(words, 21, false))
    assert.same('клмн|4', f(words, 22, false))
    assert.same('клмн|4', f(words, 28, false))
    assert.same('nil|-1', f(words, 29, false))
  end)

  it("foreach_letter", function()
    local f = M.foreach_letter
    local cb = function(letter, acc)
      acc[#acc + 1] = letter
    end
    assert.same({ 'a', 's', 'c', 'i', 'i' }, f('ascii', cb, {}))
    assert.same({}, f('', cb, {}))
    assert.same({ 'F' }, f('F', cb, {}))
    assert.same({ 'b', 'Я', 'Ф', 'z', 'П', '1' }, f('bЯФzП1', cb, {}))

    -- utf8-3
    assert.same({ 'b', '—', 'Я', 'Ф', 'z', '—', 'z' }, f('b—ЯФz—z', cb, {}))
  end)

  -- convert byte_index_in_string to utf8char_index
  it("letter_pos", function()
    assert.same(0, M.letter_pos('b—ЯФz—z', 0)) -- b
    assert.same(1, M.letter_pos('b—ЯФz—z', 1)) -- 2letter
    assert.same(2, M.letter_pos('b—ЯФz—z', 2)) -- 2letter
    assert.same(2, M.letter_pos('b—ЯФz—z', 4)) -- Я
    assert.same(3, M.letter_pos('b—ЯФz—z', 6)) -- Ф
    assert.same(6, M.letter_pos('b—ЯФz—z', 12)) -- Ф
  end)

  it("byte_pos_of_letter", function()
    local f = function(line, n)
      local bn, sz = M.byte_pos_of_letter(line, n)
      return tostring(bn) .. "|" .. tostring(line:sub(bn, bn + sz - 1))
    end
    local line = 'b—ЯФa—b“”‘’–—…•zZ'

    assert.same('1|b', f(line, 1))
    assert.same('2|—', f(line, 2))
    assert.same('5|Я', f(line, 3))
    assert.same('7|Ф', f(line, 4))
    assert.same('9|a', f(line, 5))
    assert.same('10|—', f(line, 6))
    assert.same('14|“', f(line, 8))
    assert.same('17|”', f(line, 9))
    assert.same('20|‘', f(line, 10))
    assert.same('23|’', f(line, 11))
    assert.same('26|–', f(line, 12))
    assert.same('29|—', f(line, 13))
    assert.same('32|…', f(line, 14))
    assert.same('35|•', f(line, 15))
    assert.same('38|z', f(line, 16))
    assert.same('39|Z', f(line, 17))
  end)

  it("letter_at", function()
    local f = M.letter_at
    assert.same('1', f('1234567890', 1))
    assert.same('0', f('1234567890', 10))
    assert.same('5', f('1234567890', 5))

    assert.same('─', f('─│┼┌┐┘└', 1))
    assert.same('│', f('─│┼┌┐┘└', 2))
    assert.same('┼', f('─│┼┌┐┘└', 3))
    assert.same('┌', f('─│┼┌┐┘└', 4))
    assert.same('┐', f('─│┼┌┐┘└', 5))
    assert.same('┘', f('─│┼┌┐┘└', 6))
    assert.same('└', f('─│┼┌┐┘└', 7))
    assert.same(nil, f('─│┼┌┐┘└', 8))
  end)
end)

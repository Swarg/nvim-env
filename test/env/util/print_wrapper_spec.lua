--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.util.print_wrapper")

describe("env.util.print_wrapper", function()
  it("func", function()
    -- print('Before wrap')

    M.wrap()
    print('wrapped message')
    _G.print('second wrapped message')
    M.restore()

    -- print('After restore')

    local exp = { 'wrapped message', 'second wrapped message' }
    assert.same(exp, M.get_output())
  end)
end)

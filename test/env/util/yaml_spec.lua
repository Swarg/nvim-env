-- 01-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.util.yaml'

local lines_sample_1 = {
  "components:",
  "    parameters:",
  "        api-shield_api_discovery_origin_parameter:", -- 3
  "            description: |",
  "                Filter results to only include discovery results ...",
  "                  * `ML` - Discovered operations that were sourced ...",
  "                  * `SessionIdentifier` - Discovered operations that ...",
  "            in: query", -- 8
  "            name: origin",
  "            schema:",
  "                $ref: '#/components/schemas/api-shield_api_discovery_origin'",
  "        api-shield_direction_parameter:", -- 12
  "            in: query",
  "            name: direction",             -- 14
  "            schema:",
  "                description: Direction to order results.",
  "                enum:", -- 17
  "                    - asc",
  "                    - desc",
  "                example: desc",
  "                type: string" -- 21
}

describe("env.util.yaml", function()
  it("parse_line_with_key", function()
    local f = M.parse_line_with_key
    assert.same({ '', 'key' }, { f("key:") })
    assert.same({ '', '/zones/{id}' }, { f("/zones/{id}:") })
  end)

  it("get_ref_from_cursor", function()
    local f = M.get_ref_from_cursor
    local line = [[  $ref: '#/components/schemas/zones_identifier']]
    assert.same('#/components/schemas/zones_identifier', f(line))

    local line2 = [[$ref:'#/components/schemas/zones_identifier']]
    assert.same('#/components/schemas/zones_identifier', f(line2))

    local line3 = [[ - $ref:'#/components/schemas/zones_id']]
    assert.same('#/components/schemas/zones_id', f(line3))
  end)


  it("find_lnum_of_key", function()
    local path = '#/components/parameters/api-shield_direction_parameter'
    assert.same(12, M.find_lnum_of_key(lines_sample_1, path))

    path = '#/components/parameters/api-shield_direction_parameter/schema/enum'
    assert.same(17, M.find_lnum_of_key(lines_sample_1, path))
  end)

  it("find_key_body_range", function()
    local f = M.find_key_body_range
    assert.same({ 3, 11 }, { f(nil, nil, lines_sample_1, 3) })
    assert.same({ 3, 11 }, { f(nil, 4, lines_sample_1, 3) })

    assert.same({ 12, 21 }, { f(nil, 4, lines_sample_1, 12) })
    local path = '#/components/parameters/api-shield_direction_parameter/schema/enum'
    assert.same({ 17, 19 }, { f(path, 4, lines_sample_1, 12) })

    local path2 = '#/components/parameters/api-shield_direction_parameter/in'
    assert.same({ 13, 13 }, { f(path2, 4, lines_sample_1, 0) })

    local path3 = '#/components/parameters/api-shield_api_discovery_origin_parameter/in'
    assert.same({ 8, 8 }, { f(path3, 4, lines_sample_1, 0) })
  end)

  it("find_sibling_keys_range", function()
    local f = M.find_sibling_keys_range
    assert.same({ 3, 11 }, { f(nil, 4, lines_sample_1, 8) })
    assert.same({ 12, 21 }, { f(nil, 4, lines_sample_1, 14) })
  end)

  local function sub_lines(lines, s, e)
    local t = {}
    for i = s, e, 1 do
      t[#t + 1] = lines[i]
    end
    return t
  end

  it("get_sibling_key_names", function()
    local f = M.get_sibling_key_names
    local exp = { 'description', 'in', 'name', 'schema' }
    assert.same(exp, f(4, 11, sub_lines(lines_sample_1, 4, 11)))
    local exp2 = {
      'api-shield_api_discovery_origin_parameter',
      'api-shield_direction_parameter'
    }
    assert.same(exp2, f(3, 21, sub_lines(lines_sample_1, 3, 21)))
    local exp3 = { 'components' }
    assert.same(exp3, f(1, 21, sub_lines(lines_sample_1, 1, 21)))
  end)
end)

describe("env.util.yaml", function()
  local lyaml = require "lyaml"
  it("open_node with lyaml", function()
    local path = '#/components/parameters/api-shield_direction_parameter/in'
    local yml = lyaml.load(table.concat(lines_sample_1, "\n"), { all = false })
    local exp = {
      'query',
      nil, -- no errors
      {
        'components',
        'parameters',
        'api-shield_direction_parameter',
        'in'
      }
    }
    assert.same(exp, { M.open_node(yml, path) })
  end)
end)

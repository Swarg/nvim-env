require 'busted.runner' ()

local assert = require('luassert') -- used by busted has in nvim +

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

---@diagnostic disable-next-line: unused-local
local D = require("dprint")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local api = _G.vim.api

local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
local LEXEME_NAMES = lapi_consts.LEXEME_NAMES

describe('env.code_parser', function()
  setup(function()
    M = require 'env.util.code_parser'
  end)

  teardown(function()
  end)

  before_each(function()
    nvim:clear_state()
  end)
  after_each(function() end)


  it("extract function pattern", function()
    local str = "function  M.name (x"
    local pattern = "function%s*([%w_%.]+)"
    local fullname = string.match(str, pattern) -- 'M.name'
    -- print('fullname: ' .. (fullname or 'nil'))
    assert.same("M.name", fullname)
    local shortname = string.match(fullname, "%.+([%w_]+)")
    -- print('shortname: ' .. (shortname or 'nil'))
    assert.same("name", shortname)
  end)

  it("get_function_name", function()
    local line = 'function M.func_name(a, b)'
    assert.equal("func_name", M.get_function_name(line))
    line = 'function  M.func_name (a, b)'
    assert.equal("func_name", M.get_function_name(line))

    line = 'function  min (a, b)'
    assert.equal("min", M.get_function_name(line))
  end)

  -- for get_module
  local function mk_project()
    local proot = "/home/user/path/to/some/project"
    local src = 'lua' -- 'lua' used in nvim plugins, in standalone app - 'src'
    return M.new_project({ project_root = proot, src = src, test = 'test' })
  end

  it('get_module-package', function()
    local p = mk_project()
    local bufname = p.project_root .. "/lua/the/package/module.lua"
    local res = M.get_module(p, bufname)
    local exp = "the.package.module"
    assert.equal(exp, res)
  end)

  it('get_module-file', function()
    local p = mk_project()
    assert.equal("file", M.get_module(p, p.project_root .. "/lua/file.lua"))
    assert.equal("f", M.get_module(p, p.project_root .. "/lua/f.lua"))
    assert.equal("", M.get_module(p, p.project_root .. "/lua/.lua"))
    assert.equal("", M.get_module(p, p.project_root .. "/lua/"))
    assert.equal("x", M.get_module(p, p.project_root .. "/lua/x"))
    assert.equal("x.txt", M.get_module(p, p.project_root .. "/lua/x.txt"))
    p.ext = 'txt'
    assert.equal("file", M.get_module(p, p.project_root .. "/lua/file.txt"))
    p.ext = 'c'
    assert.equal("main", M.get_module(p, p.project_root .. "/lua/main.c"))
  end)

  -- for findout word under cursor from line and column-value
  it("get_word_at", function()
    assert.same("x", M.get_word_at("x", 1))
    assert.same("b", M.get_word_at("a b c", 3))
    assert.same("a", M.get_word_at("a b c", 1))
    assert.same("c", M.get_word_at("a b c", 5))
    assert.same(nil, M.get_word_at("a b c", 2))
    assert.same(nil, M.get_word_at("a b c", 4))
    assert.same(nil, M.get_word_at("a b c ", 6))
    assert.same(nil, M.get_word_at("a ", 4))
    assert.same(nil, M.get_word_at("a ", -1))
    assert.same(nil, M.get_word_at("", 0))
    --                                1234567890
    assert.same("abc", M.get_word_at("abc def ghi", 2))
    assert.same("abc", M.get_word_at("abc def ghi", 1))
    assert.same("abc", M.get_word_at("abc def ghi", 3))
    assert.same("def", M.get_word_at("abc def ghi", 5))
    assert.same("def", M.get_word_at("abc def ghi", 6))
    assert.same("def", M.get_word_at("abc def ghi", 7))
    assert.same("ghi", M.get_word_at("abc def ghi", 9))
    assert.same("ghi", M.get_word_at("abc def ghi", 10))
    assert.same("ghi", M.get_word_at("abc def ghi", 11))

    assert.same("e", M.get_word_at("abc (e) ghi", 6))
    assert.same(nil, M.get_word_at("abc (e) ghi", 22))
    assert.same("i", M.get_word_at("  local i = 0", 9))
    assert.same("ib0", M.get_word_at(" ib0 ", 2))
    assert.same("ib0", M.get_word_at(" ib0 ", 2))
    assert.same("0i", M.get_word_at(" 0i ", 3)) -- issue invalid varname
  end)

  it("get_word_at", function()
    --                                 12345678901
    assert.same("def", M.get_word_at("abc def ghi", 7, 0))
    assert.same("ghi", M.get_word_at("abc def ghi", 7, 1))
    assert.same("abc", M.get_word_at("abc def ghi", 7, -1))
    assert.same("ghi", M.get_word_at("abc def ghi", 1, 2))
    assert.same("def", M.get_word_at("abc def ghi", 1, 1))
    assert.same("ghi", M.get_word_at("abc def ghi", 11, 0))
    assert.same("def", M.get_word_at("abc def ghi", 11, -1))
    assert.same("abc", M.get_word_at("abc def ghi", 11, -2))
  end)

  it("var-name", function()
    assert.same("v", string.match("v", '^(%a+[%a_%d]?)$'))
    assert.same(nil, string.match("0", '^(%a+[%a_%d]?)$'))
    assert.same("a0", string.match("a0", '^(%a+[%a_%d]?)$'))
    assert.same(nil, string.match("a0 b", '^(%a+[%a_%d]?)$'))
  end)

  it("get_word_at & get_container_name", function()
    --                     10        20        30
    --            1234567890123456789012345678901234567890
    local line = "  task.setUrl(delayed(Constants.RES_URL0));"
    local res, s = M.get_word_at(line, 36)
    assert.same("RES_URL0", res)
    assert.same(33, s)
    assert.same("Constants", M.get_container_name(line, s))
    --
    res, s = M.get_word_at(line, 5)
    assert.same("task", res)
    assert.same(3, s)
    assert.is_nil(M.get_container_name(line, s))
    --
    res, s = M.get_word_at(line, 10)
    assert.same("setUrl", res)
    assert.same(8, s)
    assert.same("task", M.get_container_name(line, s))
    --
    res, s = M.get_word_at(line, 18)
    assert.same("delayed", res)
    assert.same(15, s)
    assert.same(nil, M.get_container_name(line, s))
  end)

  it("get_word_at & get_container_name 2", function()
    --                     10        20        30
    --             1234567890123456789012345678901234567890
    local line0 = "  mainClass = 'io.pkg.app.Core'"
    local line2 = "  mainClass = 'io.CoreApp'"
    local res, s = M.get_word_at(line0, 29)
    assert.same("Core", res)
    assert.same(27, s)
    assert.same("io.pkg.app", M.get_container_name(line0, s))

    res, s = M.get_word_at(line2, 24)
    assert.same("CoreApp", res)
    assert.same(19, s)
    assert.same("io", M.get_container_name(line2, s))

    assert.same(nil, M.get_container_name("", 0))
    assert.same(nil, M.get_container_name("", 2))
    assert.same('a.b.c', M.get_container_name("a.b.c.E", 7))
    assert.same('a:b:c', M.get_container_name("a:b:c:E", 7))
    assert.same('a.b.c', M.get_container_name("a.b.c:E", 7))
    assert.is_nil(M.get_container_name("a.b.c::E", 8))
  end)

  it("get_word_at & get_container_name 2", function()
    --                     10        20        30
    --             1234567890123456789012345678901234567890
    local line0 = "import pkg.nested.SomeClass;"
    local res, s = M.get_word_at(line0, 21)
    assert.same('SomeClass', res)
    assert.same(19, s)
    assert.same('pkg.nested', M.get_container_name(line0, s))
  end)

  it("lexeme_name", function()
    assert.same('FUNCTION', M.lexeme_name(LEXEME_TYPE.FUNCTION))
    assert.same('UNKNOWN', M.lexeme_name(nil))
    assert.same('EMPTY', M.lexeme_name(0))
    assert.same('UNKNOWN', M.lexeme_name(-1))
    assert.same('UNKNOWN', M.lexeme_name('bad-input'))
  end)

  it("sub", function()
    local s, line = 1, ""
    assert.same('', line:sub(s - 2, s - 1))
    assert.same('', line:sub(4, 6))
  end)
end)

describe('get_lexeme_type', function()
  local M = require 'env.util.code_parser'
  local lt = LEXEME_TYPE
  local ln = LEXEME_NAMES
  local cases = {
    -- input   exp1, exp2
    -- line word s,e
    { 'x = 123456 ',       '123456',  5, 10, ln[lt.NUMBER] },
    { 'x = "abcd" ',       'abcd',    6, 9,  ln[lt.STRING] },
    { "x = 'abcd' ",       'abcd',    6, 9,  ln[lt.STRING] },
    { 'x = "1234" ',       '1234',    6, 9,  ln[lt.STRING] },
    { 'x = ""',            '',        6, 5,  ln[lt.STRING] },
    { 'x = "a"',           'a',       6, 6,  ln[lt.STRING] },
    { 'x = "0"',           '0',       6, 6,  ln[lt.STRING] },
    -- 1234567890123456789012345678901234567890
    { 'x = method()',      'method',  5, 10, ln[lt.FUNCTION] },
    --     ^5   ^10
    { 'x = method ()',     'method',  5, 10, ln[lt.FUNCTION] },
    { 'x = array[]',       'array',   5, 9,  ln[lt.ARRAY] },
    { 'x =   b',           ' ',       5, 5,  ln[lt.SPACE] },
    { 'x = MyClass:new()', 'MyClass', 5, 11, ln[lt.CLASS] },   -- lua mk new instance
    { 'x = new MyClass()', 'MyClass', 9, 15, ln[lt.CLASS] },   -- oop lang(java)
    { ':desc(word)',       'word',    7, 10, ln[lt.VARNAME] }, -- oop lang(java)
    { ':desc(word())',     'word',    7, 10, ln[lt.FUNCTION] },
    { ':desc(a:word())',   'word',    9, 12, ln[lt.METHOD] },
    { ':desc(a:word "")',  'word',    9, 12, ln[lt.METHOD] },
    { ':desc(word "")',    'word',    7, 10, ln[lt.FUNCTION] },
    { ':desc(word, "")',   'word',    7, 10, ln[lt.VARNAME] },
    { ':desc(wor1, "")',   'wor1',    7, 10, ln[lt.VARNAME] },
    { ':desc(w1rd, "")',   'w1rd',    7, 10, ln[lt.VARNAME] },
    { ':desc(_wrd, "")',   '_wrd',    7, 10, ln[lt.VARNAME] },
    { ':desc(word; "")',   'word',    7, 10, ln[lt.VARNAME] },
    { ':desc(w-rd; "")',   'w-rd',    7, 10, ln[lt.UNKNOWN] },
    { ':desc(0ord)',       '0ord',    7, 10, ln[lt.UNKNOWN] },
  }
  for i, case in pairs(cases) do
    describe('case: ' .. i, function()
      it("in line: [" .. case[1] .. "]", function()
        local line, word, s, e, exp = case[1], case[2], case[3], case[4], case[5]
        local t = M.get_lexeme_type(line, word, s, e)
        assert.same(word, line:sub(s, e))
        assert.same(exp, ln[t])
      end)
    end)
  end

  it("end", function()
    assert.same('wor', string.match("wor-d", "^[a-zA-Z_][%w_]*"))
  end)

  it("get_lexeme_type", function()
    local f = M.get_lexeme_type
    local line = "obj:method(arg1)"
    --            12345678901234567890123
    local word, s, e = "arg1", 12, 15
    assert.same(word, line:sub(s, e))
    assert.same(LEXEME_TYPE.VARNAME, f(line, word, s, e))
  end)
end)

describe('parsing', function()
  local M = require 'env.util.code_parser'
  it("split_code_string_to_multilines", function()
    local line = 'local s = "some\\ntext here\\nlast line" -- comment'
    -- print(line)
    local exp = {
      'local s = "some\\n" ..',
      '"text here\\n" ..',
      '"last line"',
      ' -- comment' }
    assert.same(exp, M.split_code_string_to_multilines(line))
  end)

  it("get_function_params", function()
    --            1234567890123456789012345678901234567890
    local line = 'self.handler:handle(a, b):flush()'
    assert.same("a, b", M.get_function_params(line, 1))
    assert.same("a, b", M.get_function_params(line, 19))
    assert.same('', M.get_function_params(line, 28))
  end)

  it("resolve_words", function()
    local lines = {
      "--",
      --        10        20        30        40
      --234567890123456789012345678901234567890
      "local ll = LuaLang:new():noCache():resolve(luarocks_root)",
    }
    nvim:new_buf(lines, 2, 40)
    local ctx = {}
    assert.same(true, M.resolve_words(ctx))

    assert.same(lines[2], ctx.current_line) --api.nvim_get_current_line())
    assert.same('resolve', ctx.word_element)
    assert.same(LEXEME_TYPE.METHOD, ctx.word_lexeme_type)
    assert.same('LuaLang:new:noCache', ctx.word_container)

    assert.same({ 2, 40 }, api.nvim_win_get_cursor(0))
  end)

  it("get_container_name chain lua object", function()
    --                     10        20        30
    --            1234567890123456789012345678901234567890
    local line = "  ll = LuaLang:new():noCache():resolve(luarocks_root)"
    local res, s = M.get_word_at(line, 36)
    assert.same("resolve", res)
    -- D.enable()
    assert.same("LuaLang:new:noCache", M.get_container_name(line, s))
  end)

  it("get_word_at_ex", function()
    --                     10        20
    --            12345678901234567890123
    local line = "  w1 w2. w3 w4; w5 w6,"
    assert.same('w1 w2', M.get_word_at_ex(line, 5))
    assert.same('w3', M.get_word_at_ex(line, 9))
    assert.same('w3 w4', M.get_word_at_ex(line, 12))
    assert.same('w5 w6', M.get_word_at_ex(line, 19))
    assert.same('w6', M.get_word_at_ex(line, 21))
    assert.is_nil(M.get_word_at_ex(line, 1))
    assert.same('w1', M.get_word_at_ex(line, 2))
  end)
end)

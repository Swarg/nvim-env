--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.util.cmd4lua_vim")
-- local Cmd4Lua = require("cmd4lua")
local c4l_conf = require('cmd4lua.settings')

describe("env.util.cmd4lua_vim", function()
  local res_lines = nil

  setup(function()
    _G._TEST = true
    c4l_conf.show_hints = false -- Disable display hints about built-in help
    c4l_conf.devmode = true     -- throw errors
  end)

  teardown(function()
    _G._TEST = nil
  end)

  before_each(function()
    res_lines = nil
  end)

  after_each(function()
    M.debug = false
    res_lines = nil
  end)

  local print0 = function(line)
    res_lines = res_lines or {}
    res_lines[#res_lines + 1] = line
  end

  ------------------------------------------------------------------------------

  it("match", function()
    local line = "function M.cmd_path(w)"
    local func_name = 'cmd_path'
    local ptrn = '^%s*function [%w]+%.' .. func_name .. '%(.+%).*$'
    assert.same('function M.cmd_path(w)', line:match(ptrn))
  end)


  it("newCmd4Lua", function()
    if 0 == 0 then return end
    local opts = { fargs = '-_G complete', line1 = 1, line2 = 2 }
    local called = 'no'
    local impls = {
      print = print0
    }

    -- c4l.debug = true
    local w = M.newCmd4Lua(opts, impls)
        :handlers({
          ---@diagnostic disable-next-line: unused-local
          cmd_do_stuff = function(w) called = 'do-stuff' end,
          ---@diagnostic disable-next-line: unused-local
          cmd_another = function(w) called = 'another' end
        })
        :cmd('do-stuff')
        :cmd('another')
        :run()

    assert.is_not_nil(w)
    assert.same('no', called)
    assert.same({ vim_opts = opts }, w.vars)
    assert.same(1, w:get_var('vim_opts').line1)
    assert.same(2, w:get_var('vim_opts').line2)
    local exp = {
      "local _commands = { \"do-stuff\", \"another\" }\n" ..
      "M.opts = { nargs = '*', complete = cu.mk_complete(_commands) }\n"
    }
    assert.same(exp, res_lines)
  end)


  it("newCmd4Lua own parse_line implementation", function()
    local I = M.Implementations
    assert.is_function(I.parse_line)
    assert.same({ 'xab cX' }, I.parse_line('x"ab c"X'))
    assert.same({ { 'a', 'b', 'c' } }, I.parse_line('[a b c]'))
    assert.same({ { '', 'x', '' } }, I.parse_line("{'','x','',,}"))
    local line = '"ab c" [d e] {x, a, k=v} --key {debug = true, lvl = 1}'
    local opts = { fargs = line, line1 = 1, line2 = 2 }
    -- require('dprint').enable()
    local w = M.newCmd4Lua(opts, I)
    -- require('dprint').disable()
    -- sure overriden
    assert.same(I.parse_line, w.interface.parse_line)

    assert.same('ab c', w:arg(0))
    assert.match_error(function() w:arg(1) end, 'Type missmatch')
    assert.match_error(function() w:arg(2) end, 'Type missmatch')

    assert.same({ 'd', 'e' }, w:argl(1))
    assert.same({ 'x', 'a', k = 'v' }, w:argl(2))
    assert.same({ debug = true, lvl = 1, }, w:optl('--key'))
  end)

  it("find_desc", function()
    assert.same('', M.find_desc(nil, ''))
    assert.same('', M.find_desc("x", ''))
    assert.same('abc', M.find_desc(":desc('abc')"))
    assert.same('def', M.find_desc(':desc("def")'))
    assert.same('some desc', M.find_desc('  :desc("some desc")'))
    assert.same('some desc 2', M.find_desc('  :desc("some desc 2"):'))
    assert.same('another.one', M.find_desc('  :desc("another.one"):'))
    assert.same("another'one", M.find_desc('  :desc("another\'one"):'))
  end)
end)

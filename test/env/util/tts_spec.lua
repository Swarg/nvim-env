-- 02-12-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local M = require 'env.util.tts'

describe("env.util.tts", function()
  after_each(function()
    nvim:clear_state()
  end)

  it("gen_filename", function()
    local f = M.gen_filename
    assert.same('a_0001.mp3', f(1))
  end)

  it("get_audio_entry_path", function()
    local f = M.get_audio_entry_path
    local e = { index = 123 }
    assert.same('./audio/a_0123.mp3', f(e, './audio'))
  end)

  it("replace_shorthand_tags", function()
    local f = M.test.replace_shorthand_tags
    assert.same('just a text', f("just a text"))
    assert.same('just<break time="500ms"/> a text', f("just<!b05> a text"))

    local exp = 'just<break time="500ms"/> a<break time="200ms"/>text'
    assert.same(exp, f("just<!b05> a<!b02>text"))

    -- standart xml-tags not changed:
    local exp2 = 'just<break time="5ms"/> a<break time="2ms"/>'
    assert.same(exp2, f('just<break time="5ms"/> a<break time="2ms"/>'))
  end)

  -- <!b05>
  it("short_tag_to_full break time", function()
    local f = M.test.short_tag_to_full
    assert.same('<break time="500ms"/>', f('b05'))
    assert.same('<break time="200ms"/>', f('b02'))
    assert.same('<break time="0ms"/>', f('b00'))
    assert.same('<break time="1000ms"/>', f('b10')) -- 1 sec
  end)

  -- <!ppl>, </p>
  it("short_tag_to_full prosody", function()
    local f = M.test.short_tag_to_full
    assert.same('<prosody pitch="low">', f('ppl'))
    assert.same('<prosody pitch="x-low">', f('ppL'))
    assert.same('<prosody pitch="medium">', f('ppm'))
    assert.same('<prosody pitch="high">', f('pph'))
    assert.same('<prosody pitch="x-high">', f('ppH'))
    -- rate
    assert.same('<prosody rate="x-slow">', f('prS'))
    assert.same('<prosody rate="slow">', f('prs'))
    assert.same('<prosody rate="medium">', f('prm'))
    assert.same('<prosody rate="fast">', f('prf'))
    assert.same('<prosody rate="x-fast">', f('prF'))

    assert.same('</prosody>', f('/p')) -- close

    assert.match_error(function()
      f('ppX')
    end, 'unsupported value of pitch got: X')
  end)


  it("process_input_text ssml = false", function()
    local f = M.test.process_input_text
    local exp = { 'text', 'just<!b06> a', 'another<!b01>text' }
    assert.same(exp, f({ " text", "just<!b06> a", "another<!b01>text" }))
  end)

  it("process_input_text ssml = true", function()
    local f = M.test.process_input_text
    local exp = {
      'text',
      'just<break time="600ms"/> a',
      'another<break time="100ms"/>text'
    }
    assert.same(exp, f({ " text", "just<!b06> a", "another<!b01>text" }, true))
  end)

  it("process_input_text ssml = true", function()
    local f = M.test.process_input_text
    local lines = {
      'This is what <!b05> we want to do today.',
      'That is, <!b05> interests of us.<!b10>',
    }

    local exp = {
      'This is what <break time="500ms"/> we want to do today.',
      'That is, <break time="500ms"/> interests of us.<break time="1000ms"/>'
    }
    assert.same(exp, f(lines, true))
  end)

  it("gen_ffmpeg_cut_parts_cmd", function()
    local entries = {
      {
        index = 1,
        lnum = 3,
        lnum_end = 5,
        tstart = '00:00:00.440',
        tend = '00:00:03.900',
        text = { '--!remove' }, -- marked to remove this video part
      },
      {
        index = 2,
        lnum = 6,
        tstart = '00:00:00.440',
        tend = '00:00:03.900',
        text = { '--!remove name' },

      },
      {
        index = 2,
        lnum = 6,
        tstart = '00:00:00.440',
        tend = '00:00:03.900',
        text = { 'simple text' },

      }
    }
    local exp = [[
ffmpeg -i out2.mp4\
 -vf "select='between(t,0,0.44)+between(t,3.9,3.9)',setpts=N/FRAME_RATE/TB" \
 -af "aselect='between(t,0,0.44)+between(t,3.9,3.9)',asetpts=N/SR/TB" \
 output.mp4]]
    assert.same(exp, M.gen_ffmpeg_cut_parts_cmd(entries))
  end)
end)

-- 03-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.util.psql'

describe("env.util.psql", function()
  it("lines_to_table_obj", function()
    local f = M.lines_to_table_obj
    local lines = {
      'PartNumber | Description | Supplier | Price',
      ' 10010 | abc| description1 | 101',
      ' 10010 | def| d2| 91',
      ' 10010 | h| desc3| 92',
      ' 10220 | jk ln| description4| 120'
    }
    local exp = {
      { 'PartNumber', 'Description', 'Supplier',     'Price' },
      { '10010',      'abc',         'description1', '101' },
      { '10010',      'def',         'd2',           '91' },
      { '10010',      'h',           'desc3',        '92' },
      { '10220',      'jk ln',       'description4', '120' }
    }
    assert.same(exp, f(lines))
  end)


  it("lines_to_table_obj", function()
    local f = M.lines_to_table_obj
    local lines = {
      'PartNumber | Description | Supplier | Price',
      '-----------+-------------+----------+-------',
      ' 10010     |     abc     | sup      |  101  ',
    }
    local exp = {
      { 'PartNumber', 'Description', 'Supplier', 'Price' },
      { '10010',      'abc',         'sup',      '101' }
    }
    assert.same(exp, f(lines))
  end)

  it("fmt_table_obj_to_lines", function()
    local f = M.fmt_table_obj_to_lines
    local table_obj = {
      { 'PartNumber', 'Description', 'Supplier',     'Price' },
      { '10010',      'abc',         'description1', '101' },
      { '10010',      'def',         'd2',           '91' },
      { '10010',      'h',           'desc3',        '92' },
      { '10220',      'jk ln',       'description4', '120' }
    }
    local exp = {
      ' PartNumber | Description |   Supplier   | Price ',
      '------------+-------------+--------------+-------',
      ' 10010      | abc         | description1 | 101   ',
      ' 10010      | def         | d2           | 91    ',
      ' 10010      | h           | desc3        | 92    ',
      ' 10220      | jk ln       | description4 | 120   '
    }
    assert.same(exp, f(table_obj))
  end)

  it("fmt_table_obj_to_lines markdown", function()
    local table_obj = {
      { 'PartNumber', 'Description', 'Supplier',     'Price' },
      { '10010',      'abc',         'description1', '101' },
      { '10010',      'def',         'd2',           '91' },
      { '10010',      'h',           'desc3',        '92' },
    }
    local exp = {
      '| PartNumber | Description |   Supplier   | Price ',
      '|------------|-------------|--------------|-------',
      '| 10010      | abc         | description1 | 101   ',
      '| 10010      | def         | d2           | 91    ',
      '| 10010      | h           | desc3        | 92    ',
    }
    assert.same(exp, M.fmt_table_obj_to_lines(table_obj, nil, { markdown = true }))
  end)

  it("fmt_table_obj_to_lines NULLS", function()
    local f = M.fmt_table_obj_to_lines
    local table_obj = {
      { 'PartNumber', 'Description', 'Supplier', 'Price' },
      { '10010',      'abc',         [4] = '101' },
      { '10010',      'def',         'd2',       '91' },
    }
    local exp = {
      ' PartNumber | Description | Supplier | Price ',
      '------------+-------------+----------+-------',
      ' 10010      | abc         |          | 101   ',
      ' 10010      | def         | d2       | 91    '
    }
    assert.same(exp, f(table_obj))
  end)

  it("reformat", function()
    local input_lines = {
      ' PartNumber   |   Description   |  Supplier      | Price ',
      '--------------+-----------------+----------------+-------',
      ' 10010        |   abc           |  description1  | 101   ',
      ' 10010        |   def           |  d2            | 91    ',
      ' 10010        |   h             |  desc3         | 92    ',
      ' 10220        |   jk ln         |  description4  | 120   '
    }
    local t = M.lines_to_table_obj(input_lines)
    local exp = {
      ' PartNumber | Description |   Supplier   | Price ',
      '------------+-------------+--------------+-------',
      ' 10010      | abc         | description1 | 101   ',
      ' 10010      | def         | d2           | 91    ',
      ' 10010      | h           | desc3        | 92    ',
      ' 10220      | jk ln       | description4 | 120   '
    }
    assert.same(exp, M.fmt_table_obj_to_lines(t))
  end)

  it("reformat 2", function()
    local input_lines = {
      ' PartNumber   |   Description   |  Supplier      | Price ',
      '--------------+-----------------+----------------+-------',
      ' 10010        |   abc  |  description1  | 101   ',
      ' 10010        |   def  |  d2            | 91    ',
      ' 10010        |   h             |  desc3         | 92    ',
      ' 10220        |   jk ln            |  description4  | 120   '
    }
    local t = M.lines_to_table_obj(input_lines)
    local exp = {
      ' PartNumber | Description |   Supplier   | Price ',
      '------------+-------------+--------------+-------',
      ' 10010      | abc         | description1 | 101   ',
      ' 10010      | def         | d2           | 91    ',
      ' 10010      | h           | desc3        | 92    ',
      ' 10220      | jk ln       | description4 | 120   '
    }
    assert.same(exp, M.fmt_table_obj_to_lines(t))
  end)
end)

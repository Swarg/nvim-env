-- 01-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.util.json'

describe("env.util.json", function()
  it("parse_kvpair", function()
    local f = M.parse_kvpair
    assert.same({ '"key"', 'value' }, { f([["key": "value",]]) })
    assert.same({ '"key"', 'value' }, { f([["key": "value"]]) })
    assert.same({ '"key"', 'value' }, { f([[  "key" : "value"  ]]) })
    assert.same({ '"key"', 'value' }, { f([[  "key" : "value",  ]]) })
    assert.same({ '"key_a"', true }, { f([[  "key_a" : true,  ]]) })
    assert.same({ '"key-a"', false }, { f([[  "key-a" : false,  ]]) }) -- ?
    assert.same({ '"key"', 1 }, { f([[  "key" : 1,  ]]) })
    assert.same({ '"key"', -1 }, { f([[  "key" : -1,  ]]) })
    assert.same({ '"key"', 0 }, { f([[  "key" : 0,  ]]) })
    assert.same({ '"key"', 0.5 }, { f([[  "key" : 0.5,  ]]) })
  end)
end)

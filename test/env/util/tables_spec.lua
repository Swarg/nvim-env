--
require 'busted.runner' ()
local assert = require('luassert')
local M = require 'env.util.tables'

describe('env.util.tables', function()
  it("indexof", function()
    local f = M.indexof
    assert.same(4, f({ 5, 6, 7, 8, 9 }, 8))
    assert.same(1, f({ 5, 6, 7, 8, 9 }, 5))
    assert.same(-1, f({ 5, 6, 7, 8, 9 }, 99))
  end)

  it("next_after_anyof", function()
    local f = M.next_after_anyof
    local v = { 'class', 'interface' }
    assert.same({ 'cn', 'class' }, { f({ 'public', 'class', 'cn' }, v) })
    assert.same({ 'in', 'interface' }, { f({ 'public', 'interface', 'in' }, v) })
  end)

  it("prevtwo_before_elm", function()
    local f = M.prevtwo_before_elm
    assert.same({ 'String', 'method' }, { f({ 'public', 'String', 'method', '(' }, '(') })
    assert.same({}, { f({ 'public', 'String', 'method', '(' }, 'x') })
    assert.same({ [2] = 'public' }, { f({ 'public', 'String', 'method', '(' }, 'String') })
  end)

  it("tbl_value_at_path", function()
    local f = M.tbl_value_at_path
    assert.same(nil, f({}, ""))
    assert.same({ 'b' }, f({ a = { 'b' } }, "a"))
    assert.same({ '+' }, f({ a = { b = { c = { '+' } } } }, "a", "b", "c"))
    assert.same('s', f({ a = { b = { c = 's' } } }, "a", "b", "c"))
    local tbl = { a = { b = { c = 's' } } }
    assert.same(nil, f(tbl, "a", "c", "b"))
    -- print(vim.inspect(res))
    assert.same('s', tbl['a']['b']['c'])
    assert.error(function() -- attempt to index field 'c' (a nil value)
      assert.same('-', tbl['a']['c']['b'])
    end)
  end)

  it("tbl_entry_with", function()
    local tbl = { { id = 'v1' }, { id = 'v2', data = '0' }, { id = "v3" } }
    assert.same({ id = 'v2', data = '0' }, M.tbl_entry_with(tbl, 'id', 'v2'))
  end)

  it("length_as_string", function()
    local f = M.length_as_string
    assert.same(5, f('abc'))
    assert.same(9, f({ "abc" }))
    assert.same(15, f({ "abc", 'de' }))
    assert.same(18, f({ "abc", 'de', 0 }))
    assert.same(22, f({ "abc", 'de', k = 0 }))
    assert.same(39, f({ "abc", 'de', key = 'value', a = 'b' }))
    assert.same(47, f({ key = { subkey = 'value', a = 'b', k = {} } }))
  end)

  it("table2code", function()
    local t = { key = { subkey = 'value', a = 'b', k = { 1, 2 }, 3 }, 4 }
    local t2s = M.table2code
    local tlen = M.length_as_string
    local res = t2s(t)
    assert.same(#res, tlen(t))
    local exp = "{ key = { a = 'b', k = { 1, 2 }, subkey = 'value', 3 }, 4 }"
    assert.same(exp, t2s(t))
    local exp2 = "{ key = { a = 'b', k = { 1, 2 }, subkey = 'value', 3 }, 4 }"
    assert.same(exp2, t2s(t, '  ')) -- smart by length of stred table
  end)

  it("table2code", function()
    local t = {
      parent = 1,
      id = 1,
      opt = 'abc',
      { sub = 'value', a = 'b', k = { 1, 2 }, 3 },
      { sub = 'value', b = 'c', k = { 1, 2 }, 3 },
    }
    local t2s = M.table2code
    local tlen = M.length_as_string
    local res = t2s(t)
    assert.same(#res, tlen(t))
    local exp = "{ id = 1, opt = 'abc', parent = 1, { a = 'b', k = { 1, 2 }," ..
        " sub = 'value', 3 }, { b = 'c', k = { 1, 2 }, sub = 'value', 3 } }"
    assert.same(exp, t2s(t))
    local exp2 = [[
{
  id = 1,
  opt = 'abc',
  parent = 1,
  { a = 'b', k = { 1, 2 }, sub = 'value', 3 },
  { b = 'c', k = { 1, 2 }, sub = 'value', 3 }
}]]
    assert.same(exp2, t2s(t, '  '))
  end)

  it("table2code", function()
    local t = { pa = 1, id = 1, opt = 'ac', sub = 'val', a = 'b', k = 9 }
    local t2s = M.table2code
    local tlen = M.length_as_string
    local res = t2s(t)
    assert.same(#res, tlen(t))
    local exp = "{ a = 'b', id = 1, k = 9, opt = 'ac', pa = 1, sub = 'val' }"
    assert.same(exp, t2s(t))

    local exp2 = "{ a = 'b', id = 1, k = 9, opt = 'ac', pa = 1, sub = 'val' }"
    assert.same(exp2, t2s(t, '  '))
  end)

  it("table2code each key in new line", function()
    local t2s = M.table2code
    local t = { pa = 42, id = 1, opt = 'ac', sub = 'val' }
    local res = t2s(t, '  ', nil, nil, true)
    local exp = [[
{
  id = 1,
  opt = 'ac',
  pa = 42,
  sub = 'val'
}]]
    assert.same(exp, res)
  end)

  it("table2code2", function()
    local t = { part = 1, id = 1, opt = 'ac', sub = 'val', a = 'b', k = 9 }
    local t2s = M.table2code
    local tlen = M.length_as_string
    local res = t2s(t)
    assert.same(#res, tlen(t))
    local exp = "{ a = 'b', id = 1, k = 9, opt = 'ac', part = 1, sub = 'val' }"
    assert.same(exp, t2s(t, nil)) -- online style
    local exp2 = [[
{
  a = 'b',
  id = 1,
  k = 9,
  opt = 'ac',
  part = 1,
  sub = 'val'
}]]
    assert.same(exp2, t2s(t, '  ')) -- auto new line for a long elements
  end)

  it("table2code keys", function()
    local t2s = M.table2code

    local exp = "{ ['complex.key'] = 1, simplekey = 2 }"
    assert.same(exp, t2s({ ['complex.key'] = 1, simplekey = 2 }))
    assert.same("{ ['/tmp/file'] = 1 }", t2s({ ['/tmp/file'] = 1 }))
    assert.same("{ ['-key'] = 1 }", t2s({ ['-key'] = 1 }))
    assert.same("{ ['0key'] = 1 }", t2s({ ['0key'] = 1 }))
  end)

  it("table2code", function()
    local t = { { {
      k = 'a',
      'bxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
      'cfsdjhfskdhfsjdhf',
      {},
      'e'
    }, 'f' }, 'g' }

    local exp =
        "{\n" ..
        "  {\n" ..
        "    {\n" ..
        "      k = 'a',\n" ..
        "      'bxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',\n" ..
        "      'cfsdjhfskdhfsjdhf',\n" ..
        "      {},\n" ..
        "      'e'\n" ..
        "    },\n" ..
        "    'f'\n" ..
        "  },\n" ..
        "  'g'\n" ..
        "}"
    assert.same(exp, M.table2code(t, '  '))
  end)

  it("flat_copy", function()
    local f = M.flat_copy
    local t = { 'ab', 'cd', 'ef', 'gh' }
    assert.same({ 'ab', 'cd', 'ef', 'gh' }, f(t))
    assert.same({ 'ab' }, f(t, 1, 1))
    assert.same({ 'ab', 'cd', 'ef', 'gh' }, f(t, 1))
    assert.same({ 'cd', 'ef', 'gh' }, f(t, 2))

    assert.same({ 'gh' }, f(t, -1, -1))
    assert.same({ 'ef', 'gh' }, f(t, -2, -1))
    assert.same({ 'cd', 'ef', 'gh' }, f(t, -3, -1))
    assert.same({ 'cd', 'ef', 'gh' }, f(t, -3))
    assert.same({}, f(t, -2, -3))
  end)

  it("flat_copy + out", function()
    local f = M.flat_copy
    local t = { 'ab', 'cd', 'ef', 'gh' }
    local nt = { '01', '02' }
    assert.same({ '01', '02', 'ab', 'cd', 'ef', 'gh' }, f(t, nil, nil, nt))
  end)

  it("luacode2table", function()
    local f = function(line)
      local ok, t = M.luacode2table(line)
      return { ok, t }
    end

    assert.same({ true, {} }, f('{}'))
    assert.same({ true, { 1, 2 } }, f('{1,2}'))
    assert.same({ true, { a = 1, b = 2 } }, f('{a=1,b=2}'))
    assert.same({ true, { a = 1, b = 2 } }, f('{["a"]=1,["b"]=2}'))
    local exp = 'expected line starts with "{" got: '
    assert.same({ false, exp .. '""' }, f(''))
    assert.same({ false, exp .. '"1"' }, f('1'))
    assert.same({ false, exp .. '"a"' }, f('a'))
    assert.same({ false, exp .. '"bad_func()"' }, f('bad_func();'))

    assert.same({ false, 'cannot run any functions' }, f('{} and call_bad_code();'))
    assert.same({ false, 'cannot run any functions' }, f('{} --[[]]and bad();'))

    local line = [[ { ['function'] = { "callback", "handler" }, } ]]
    local exp2 = { true, { ['function'] = { 'callback', 'handler' } } }
    assert.same(exp2, f(line))

    -- keword without [''] gives error:
    local line2 = [[ { function = { "callback", "handler" }, } ]]
    local exp3 = { false, 'Cannot parse line as lua-table: nil' }
    assert.same(exp3, f(line2))

    local line3 = [[ run_bad_code() ]]
    local exp4 = { false, 'expected line starts with "{" got: " run_bad_c"' }
    assert.same(exp4, f(line3))

    local line4 = [[ {} and run_bad_code(); --} ]]
    assert.same({ false, 'cannot run any functions' }, f(line4))
  end)

  it("luacode2table", function()
    local f = function(line)
      local ok, t = M.luacode2table(line)
      return { ok, t }
    end
    assert.same({ true, { 3, 4 } }, f("-- comment 1\n{3,4}"))
    assert.same({ true, { 5, 6 } }, f("-- comment 1\n-- comment 2\n{5,6}"))
    assert.same({ true, { 7, 8 } }, f("-- ?\n { 7, \n--commend2 \n 8}\n--x"))
  end)

  it("remove_comments", function()
    local f = M.remove_comments
    assert.same('{1,2}', f("-- comment 1\n-- comment 2\n{1,2}"))
    local lines = [[
    {
      -- comment above the code
      key1 = 'value1', -- comment in same line
      key2 = 'value2', -- let another comment in same line
      -- comment at the end
    }
    ]]
    local exp = [[
    {

      key1 = 'value1',
      key2 = 'value2',
    }

    ]]
    assert.same(exp, f(lines))
  end)


  it("split_to_keys_and_indexes", function()
    local f = M.split_to_keys_and_indexes
    assert.same({ { 1, 2 }, { 'b', 'c' }, {} }, { f({ c = 1, 8, 3, b = 4 }) })
  end)

  it("apply_order", function()
    local f = M.apply_order
    assert.same({ 'b', 'a', 'c' }, f({ 'a', 'b', 'c' }, { 'b', 'a' }))
  end)

  it("shuffle2new", function()
    math.randomseed(88)
    local f = M.shuffle2new
    local list = { "Apple", "Apricot", "Banana", "Fig", "Pear", "Orange" }
    local copy = M.deep_copy({ "Apple", "Apricot", "Banana", "Fig", "Pear", "Orange" })
    local exp = { 'Banana', 'Pear', 'Fig', 'Orange', 'Apricot', 'Apple' }
    assert.same(exp, f(list))
    assert.are_not_equal(exp, list)
    assert.same(list, copy) -- sure not affected

    local exp2 = { 'Apple', 'Apricot', 'Orange', 'Pear', 'Fig', 'Banana' }
    assert.same(exp2, f(list))
    local exp3 = { 'Banana', 'Apple', 'Fig', 'Orange', 'Apricot', 'Pear' }
    assert.same(exp3, f(list))

    local exp4 = { 'Fig', 'Apricot', 'Banana', 'Apple', 'Pear', 'Orange' }
    assert.same(exp4, f(list))
  end)

  it("shuffle2new", function()
    math.randomseed(88)
    local f = M.shuffle2new
    assert.same({ 'Apple' }, f({ "Apple" }))
    assert.same({}, f({}))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same({}, f(nil))
  end)

  it("flatten_map_to_list", function()
    local f = M.flatten_map_to_list
    local vegetable = {
      root = { "Beetroot", "Carrot", "Turnip", "Radish", "Celeriac" },
      leafy = { "Cabbage", "Spinach", "Mint", "Onion" },
    }
    local exp = {
      'Beetroot', 'Carrot', 'Turnip', 'Radish', 'Celeriac',
      'Cabbage', 'Spinach', 'Mint', 'Onion'
    }
    assert.same(exp, f(vegetable))
    -- assert.same('', table.concat(vegetable, '')) -- not works with map
  end)

  it("map_to_list", function()
    local f = M.map_to_list
    assert.same({ { 'k', 4 }, { 'b', 9 } }, f({ k = 4, b = 9 }))
  end)

  it("get_sublist", function()
    local f = M.get_sublist
    local root_vegs = { "Beetroot", "Carrot", "Turnip", "Radish", "Celeriac" }
    local exp2 = { 'Beetroot', 'Carrot', 'Turnip', 'Radish', 'Celeriac' }
    assert.same(exp2, f(root_vegs))

    local exp3 = { 'Beetroot', 'Carrot' }
    assert.same(exp3, f(root_vegs, 1, 2))

    local exp4 = { 'Turnip', 'Radish', 'Celeriac' }
    assert.same(exp4, f(root_vegs, 3, 5))

    local exp5 = { 'Celeriac', 'Radish', 'Turnip' }
    assert.same(exp5, f(root_vegs, 5, 3))
  end)

  it("table2code", function()
    local f = M.table2code
    local t = { a = 2, b = 3, d = 'b', k = { 1, 2 }, 8, 42 }
    local order = { 'b', 'd', 'a' }

    local exp = "{ b = 3, d = 'b', a = 2, k = { 1, 2 }, 8, 42 }"
    assert.same(exp, f(t, nil, nil, order))
  end)

  it("tbl_merge_flat", function()
    local f = M.tbl_merge_flat
    local oldt = { key1 = 1, key2 = 42, key3 = 'abc', t1key = 1 }
    local newt = { key1 = 10, key2 = 24, key3 = 'ABC', t2key = 2 }
    local exp = {
      key1 = 10,    -- updated
      key2 = 24,    -- updated
      key3 = 'ABC', -- updated
      t1key = 1,    -- same from table1
      t2key = 2     -- same from table2
    }
    assert.same(exp, f(oldt, newt))

    local exp2 = { key1 = 1, key2 = 42, key3 = 'abc', t1key = 1 }
    assert.same(exp2, f(oldt, nil))
    assert.are_not_equal(exp2, oldt)

    local exp3 = { key1 = 10, key2 = 24, key3 = 'ABC', t2key = 2 }
    assert.same(exp3, f(nil, newt))
    assert.are_not_equal(exp3, newt)


    local exp4 = {}
    assert.same(exp4, f(nil, nil))
  end)

  it("append_all_elms", function()
    local f = M.append_all_elms
    local dst = { 1, 2, 3 }
    assert.same({ 1, 2, 3, 4, 5 }, f(dst, { 4, 5 }))
  end)

  it("insert_all_to_new", function()
    local olines = { 1, 2, 7, 8, 9 }
    local res = M.insert_all_to_new(olines, 3, { 3, 4, 5, 6 })
    assert.same({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, res)
    assert.same({ 1, 2, 7, 8, 9 }, olines) -- sure not affected
  end)
end)

describe('env.util.tables format text table', function()
  it("parse_title_caption", function()
    local f = M.parse_title_caption

    local exp = {
      { name = 'Status',    pe = 8,  ps = 1 },
      { name = 'Method',    pe = 16, ps = 9 },
      { name = 'domain',    pe = 26, ps = 17 },
      { name = 'file',      pe = 38, ps = 27 },
      { name = 'initiator', pe = 49, ps = 39 },
      { name = 'type',      pe = 53, ps = 50 }
    }
    assert.same(exp, f("Status  Method  domain    file        initiator  type"))
  end)

  it("lines_to_table_obj", function()
    local f = M.lines_to_table_obj
    local lines = {
      'Status  Method  domain          file        initiator  type',
      ' 304    GET     localhost:5173  /           document   html',
      ' 304    GET     localhost:5173  main.js?t=  script      js',
      ' 304    GET     localhost:5173  06.sass     script     js  '
    }
    local exp = {
      { 'Status', 'Method', 'domain',         'file',       'initiator', 'type' },
      { '304',    'GET',    'localhost:5173', '/',          'document',  'html' },
      { '304',    'GET',    'localhost:5173', 'main.js?t=', 'script',    'js' },
      { '304',    'GET',    'localhost:5173', '06.sass',    'script',    'js' }
    }
    assert.same(exp, f(lines))
  end)

  it("parse_raw_lines_to_table_obj", function()
    local lines = {
      'Claim 	Type 	Description 	Included in ID Token 	Included in userinfo endpoint',
      'sub 	string 	The ID of the user 	Yes 	Yes',
      'auth_time 	integer 	The timestamp 	Yes 	No',
      'name 	string 	The user’s full name 	Yes 	Yes'
    }
    local exp = {
      {
        'Claim',
        'Type',
        'Description',
        'Included in ID Token',
        'Included in userinfo endpoint'
      },
      { 'sub', 'string', 'The ID of the user', 'Yes', 'Yes' },
      { 'auth_time', 'integer', 'The timestamp', 'Yes', 'No' },
      { 'name', 'string', 'The user’s full name', 'Yes', 'Yes' }
    }
    assert.same(exp, M.parse_raw_lines_to_table_obj(lines))
  end)
end)

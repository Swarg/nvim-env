-- 26-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.util.datetime'

describe("env.util.datetime", function()
  it("timestamp2readable", function()
    local f = M.timestamp2readable
    assert.same('1970-01-01T03:00:00.000Z', f(0))
    assert.same('2001-09-09T05:46:40.000Z', f(1000000000))
    assert.same('2033-05-18T06:33:20.000Z', f(2000000000))
    assert.same('2024-08-26T08:39:09.000Z', f(1724650749))
    -- -> 2024-08-26 08:39:09
  end)

  it("datetime2timestamp", function()
    local f = M.datetime2timestamp
    assert.same(1724650749, f('2024-08-26T08:39:09.000Z'))
    assert.same(1724650749, f('2024-08-26 08:39:09'))
    assert.same(1724619600, f('2024-08-26 08:39'))
    assert.same(1722459600, f('2024-08-01'))
    assert.same(1724533200, f('2024-08-25T21:42:35.808+03:00'))
    assert.same(1724533200, f('2024-08-25T21:42:35.808'))
  end)

  it("gen_random_timestamp", function()
    local f = M.gen_random_timestamp
    assert.same('2016-05-07T10:05:49.000000Z', f(1, nil, 0, 1740807106))
    assert.same('2005-09-12T16:54:22.000000Z', f(1, nil, 0, 1340807106))
  end)
end)

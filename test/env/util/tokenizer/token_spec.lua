--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.util.tokenizer.token")

describe("env.util.tokenizer.token", function()
  -- TODO remove part from parser_spec
  it("token_get_root", function()
    local root = { parent = false }
    local t1 = { parent = root, ps = 1, pe = 1 }
    local t2 = { parent = t1, ps = 2, pe = 2 }
    local t3 = { parent = t2, ps = 3, pe = 3 }
    root[#root + 1] = t1
    t1[#t1 + 1] = t2
    t2[#t2 + 1] = t3
    assert.same(root, M.get_root(root))
    assert.same(root, M.get_root(t1))
    assert.same(root, M.get_root(t2))
    assert.same(root, M.get_root(t3))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local D = require("dprint")
local M = require("env.util.tokenizer.parser")
-- token
local MM = require("env.util.tokenizer.token")

local T = require("env.util.tokenizer.devhelpers")

-- for T.s2o
T.throw_errors = nil

--
-- tokenize given line and build to object
--
---@param line string
---@param seps table|nil default is ' ', ',', "\n"
function T.s2o(line, throw_errors, seps)
  assert(not seps or type(seps) == 'table' and #seps > 0, 'separators')
  if throw_errors == nil then
    throw_errors = T.throw_errors
  end
  local t = M.Tokenizer:new(line):setThrowErrors(throw_errors)
  if seps then
    t:set_separators(seps)
  end
  t:tokenize()
  local tree2str = function()
    return T.tree2str(t:getRoot(), line, false, '  ', true)
  end
  return t:buildObject(), line, tree2str, t:getRoot()
end

local tokenizer = M.Tokenizer:new()

T.new_token = function(parent, start_pos, end_pos, open)
  return tokenizer:new_token(parent, start_pos, end_pos, open)
end
T.token_close = function(token, p_end)
  return tokenizer:token_close(token, p_end)
end
T.reset_next_id = function()
  tokenizer.next_token_id = 0
end

-- bind function from a new module token to parser (testing)
for k, v in pairs(MM) do
  if type(v) == 'function' then
    M[k] = MM[k]
  end
end

--------------------------------------------------------------------------------

describe("env.util.tokenizer.parser", function()
  setup(function()
    _G._TEST = true
    -- M = require("env.util.tokenizer.parser")
    -- T = M.T
  end)

  teardown(function()
    _G._TEST = nil
  end)
  before_each(function()
  end)

  after_each(function()
    D.on = false
  end)

  it("token_close", function()
    local root = T.new_token()
    local token = T.new_token(root, 4)

    assert.error_match(function()
      T.token_close(token)
    end, 'token.pe has:nil')

    assert.error_match(function()
      T.token_close(token, 0)
    end, 'token.pe has:0')

    assert.error_match(function()
      T.token_close(token, 3)
    end, 'validate range ps<=pe 4 3')

    assert.same(token, T.token_close(token, 4))

    token.ps = nil
    assert.error_match(function()
      T.token_close(token, 4)
    end, 'validate token.ps has:nil')

    token.ps = 0
    assert.error_match(function()
      T.token_close(token, 4)
    end, 'validate token.ps has:0')
  end)


  it("parse_line empty", function()
    local res, tree, t = M.parse_line('')
    assert.same({ '' }, res)
    assert.same(1, #tree)
    assert.same(tree, tree[1].parent)
    assert.same('id:#2 /1#1(1) deep:1 [0:0::]', t:tokenInfo(tree[1]))
  end)

  -- the single empty token at the root is left to indicate that the input is empty
  it("parse_line empty", function()
    local res, tree = M.parse_line(' ')
    assert.same({ '' }, res)
    local exp_root = { parent = false, id = 1 }
    local empty_token = { parent = exp_root, id = 2, ps = 0, pe = 0, open = '' }
    exp_root[#exp_root + 1] = empty_token
    assert.same(exp_root, tree)

    local res2, tree2 = M.parse_line('   ')
    assert.same({ '' }, res2)
    assert.same(exp_root, tree2)
  end)

  it("parse_line", function()
    local res, tree = M.parse_line('ab cde')

    local root = { parent = false, id = 1 } -- expected
    local token1 = { ps = 1, pe = 2, parent = root, id = 2 }
    local token2 = { ps = 4, pe = 6, parent = root, id = 3 }
    root[#root + 1] = token1
    root[#root + 1] = token2

    assert.same({ 'ab', 'cde' }, res)
    assert.same(root, tree)
  end)

  it("parse_line 1", function()
    local res, tree = M.parse_line(' x ')

    local exp_tree = { parent = false, id = 1 }
    local token1 = { ps = 2, pe = 2, parent = exp_tree, id = 2 }
    exp_tree[#exp_tree + 1] = token1

    assert.same({ 'x' }, res)
    assert.same(exp_tree, tree)
  end)

  it("parse_line 2", function()
    local res, tree = M.parse_line('   ab   c   de     ')
    --                              1234567890123456789

    local root = { parent = false, id = 1 } -- expected
    local token1 = { ps = 4, pe = 5, parent = root, id = 2 }
    local token2 = { ps = 9, pe = 9, parent = root, id = 3 }
    local token3 = { ps = 13, pe = 14, parent = root, id = 4 }
    root[#root + 1] = token1
    root[#root + 1] = token2
    root[#root + 1] = token3

    assert.same(root, tree)
    assert.same({ 'ab', 'c', 'de' }, res)
  end)

  ------ groups - quotes

  it("parse_line single quotes", function()
    --                                token1
    local res, tree = M.parse_line("  'abc de'    fj  ")
    --                              1234567890123456
    --                                ^------^ token1
    --                                 ^-^ ^^  token2,3
    --                                           ^^ token4

    local root = { parent = false, id = 1 }                               -- expected
    local token1 = { ps = 3, pe = 10, parent = root, open = "'", id = 2 } -- abc de
    local token2 = { ps = 15, pe = 16, parent = root, id = 3 }
    root[#root + 1] = token1
    root[#root + 1] = token2
    -- print(M.tree2str(tree))

    assert.same({ 'abc de', 'fj' }, res)
    assert.same(root, tree)
  end)


  it("parse_line single quotes", function()
    --                                token1
    local res, tree = M.parse_line("  'abc' 'de'  fj  ")
    --                              1234567890123456
    --                                ^---^ |  |        token1
    --                                 ^-^  |  |        token2
    --                                      ^--^        token3
    --                                       ^^         token4
    --                                            ^^    token5

    -- real expected table with result of parsing
    local root = { parent = false, id = 1 }                           -- expected
    local t1 = { ps = 3, pe = 7, parent = root, open = "'", id = 2 }  -- 'abc'
    local t2 = { ps = 9, pe = 12, parent = root, open = "'", id = 3 } -- 'de'
    local t3 = { ps = 15, pe = 16, parent = root, id = 4 }            --  fj
    root[#root + 1] = t1
    root[#root + 1] = t2
    root[#root + 1] = t3
    assert.same({ 'abc', 'de', 'fj' }, res)
    assert.same(root, tree)

    -- simplified version to define in tests -- instead of links - parent names
    T.mk_parents_names_readable(tree)
    local exp = {
      parent = false,
      id = 1,
      { parent = 1, ps = 3,  pe = 7,  open = "'", id = 2 },
      { parent = 1, ps = 9,  pe = 12, open = "'", id = 3 },
      { parent = 1, ps = 15, pe = 16, id = 4 }
    }
    assert.same(exp, tree)
  end)

  -- concatenate string-token if no sep
  it("parse_line single quotes without separator", function()
    local res, tree = M.parse_line("  'abc' xX'de'  fj  ")
    --                              1234567890123456
    local exp = {
      parent = false,
      id = 1,
      { parent = 1, ps = 3,  pe = 7,  open = "'", id = 2 },
      { parent = 1, ps = 9,  pe = 10, open = '',  id = 3 },
      { parent = 1, ps = 11, pe = 14, open = "'", id = 4 },
      { parent = 1, ps = 17, pe = 18, id = 5 },
    }
    assert.same({ 'abc', 'xXde', 'fj' }, res)
    T.mk_parents_names_readable(tree)
    assert.same(exp, tree)
  end)

  -- NOTE: how to on errors :
  -- T.mk_parents_names_readable(tree)
  -- print(M.tree2str(tree))

  it("parse_line single quotes", function()
    local res, tree = M.parse_line("  'abc'X'de'  fj  ")
    assert.same({ 'abcXde', 'fj' }, res)
    assert.is_not_nil(tree)
  end)

  it("parse_line single quotes", function()
    local res, tree = M.parse_line("  X 0'de'  fj  ")
    assert.same({ 'X', '0de', 'fj' }, res)
    assert.is_not_nil(tree)
  end)

  it("parse_line single quotes - wrong", function()
    local res = M.parse_line("  X 0'de  fj  ")
    -- then no end of quote - go to the end of line
    assert.same({ 'X', '0de  fj  ' }, res)
  end)


  it("parse_line single quotes - escape ", function()
    local res = M.parse_line("  X \\'de  fj  ")
    assert.same({ 'X', "\\'de", 'fj' }, res)
  end)

  it("parse_line double quotes ", function()
    local res = M.parse_line('  X "de  fj"  ')
    assert.same({ 'X', 'de  fj' }, res)
  end)

  it("parse_line single quotes - not closed", function()
    local res = M.parse_line('  X "de \'x \'y  fj"  ')
    assert.same({ 'X', "de 'x 'y  fj" }, res)
  end)

  it("parse_line single quotes - not closed", function()
    local res = M.parse_line('ab "cd ef"0\'x \'y   ')
    assert.same({ 'ab', 'cd ef0x y' }, res)
  end)

  it("parse_line single quotes - not closed", function()
    local res = M.parse_line('ab "cd ef"0\'x \'y"z  ')
    assert.same({ 'ab', 'cd ef0x yz  ' }, res)
  end)

  -----------------------------------------------------------------------------

  -- generate expected text for testing lua-parser
  -- it("parse_line []", function()
  --   local t = {
  --     { { 'a', 'b', 'c', { 'd', 'e' } } }
  --   }
  --   assert.same({}, t)
  -- end)

  -- make sure group token contains substr from pos with open and close chars
  it("open_group", function()
    local line = '[]'
    local t = M.Tokenizer:new(line):tokenize()

    local root = t:getRoot()
    local t1 = T.token_find_by_path(root, '1')
    assert.is_not_nil(t1)
    ---@cast t1 table
    assert.same(root, t1.parent)
    t1.parent = nil
    local exp = { open = '[', ps = 1, pe = 2, id = 2 }
    assert.same(exp, t1)
    assert.same('[]', t:getStrValue(t1))
  end)


  it("parse_line []", function()
    local res, tree = M.parse_line('[ab cd]')
    assert.same({ { 'ab', 'cd' } }, res)
    assert.is_not_nil(tree)
  end)

  it("parse_line []", function()
    local res, _ = M.parse_line("['ab' 'cd']")
    assert.same({ { 'ab', 'cd' } }, res)
  end)


  it("parse_line []", function()
    assert.same({ {} }, M.parse_line("[]"))
    assert.same({ {} }, M.parse_line(" []"))
    assert.same({ {} }, M.parse_line("[] "))
    assert.same({ {} }, M.parse_line("[ ] "))
    assert.same({ {} }, M.parse_line(" [ ] "))
    assert.same({ { 'ab', 'cd' } }, M.parse_line("[  'ab' 'cd']"))
    assert.same({ { 'ab', 'cd' } }, M.parse_line("[  'ab' 'cd' ]"))
    assert.same({ { 'ab', 'x', 'cd' } }, M.parse_line("[  'ab' x 'cd' ]"))
    assert.same({ { 'ab', 'x', 'cd' }, 'z' }, M.parse_line("[  'ab' x 'cd' ] z"))
    assert.same({ { 'ab', 'x', 'cd' }, 'y' }, M.parse_line("[  'ab' x 'cd' ] y"))
  end)

  it("parse_line [] numbers", function()
    local f = M.parse_line
    assert.same({ { 8 } }, f("[8]"))
    assert.same({ { '8a' } }, f("[8a]"))
    assert.same({ { 'a b8' } }, f("['a b'8]"))
    assert.same({ { 'a b', 8 } }, f("['a b' 8]"))
    assert.same({ { '8' } }, f("[''8]"))
    assert.same({ { '8' } }, f("[8'']"))
    assert.same({ { 8, '' } }, f("[8 '']"))
    assert.same({ { 8, '' } }, f("[ 8 '']"))
    assert.same({ '8' }, f("8")) -- no cast outside []
    assert.same({ { 8 } }, f("{8}"))
    assert.same({ { 'a', 8 } }, f("{'a'8}"))
    assert.same({ { 'a', 8 } }, f("{'a' 8}"))
    assert.same({ { 8, 'b' } }, f("{8'b'}"))
    assert.same({ { 8, 'b' } }, f("{8 'b'}"))
  end)

  it("parse_line [] missing end-close char of group", function()
    local line = "[  'ab' x 'cd' ']' x"
    local exp = { { 'ab', 'x', 'cd', ']', 'x' } }
    local res, root = M.parse_line(line)
    assert.same(exp, res)

    exp = { { 'abxcd', ']', 'x' } }
    assert.same(exp, M.parse_line("[  'ab'x'cd' ']' x"))

    exp = { { 'abxcd', ']x' } }
    assert.same(exp, M.parse_line("[  'ab'x'cd' ']'x"))

    exp = { { 'abxcd]x' } }
    assert.same(exp, M.parse_line("[  'ab'x'cd]'x"))

    exp = { { 'abxcd]', 'x' } }
    assert.same(exp, M.parse_line(" [  'ab'x'cd]'  x"))

    local exp_readable = [[
local exp = {
  id = 1, parent = false,                                   -- #1 /
  { id = 2, parent = 1, ps = 1, pe = 20, open = '[',        -- #2 /1#1 |[  'ab' x 'cd' ']' x|
    { id = 3, parent = 2, ps = 4, pe = 7, open = "'",},     -- #3 /1#1/1#2 |'ab'|
    { id = 4, parent = 2, ps = 9, pe = 9, },                -- #4 /1#1/2#2 |x|
    { id = 5, parent = 2, ps = 11, pe = 14, open = "'",},   -- #5 /1#1/3#2 |'cd'|
    { id = 6, parent = 2, ps = 16, pe = 18, open = "'",},   -- #6 /1#1/4#2 |']'|
    { id = 7, parent = 2, ps = 20, pe = 20, },              -- #7 /1#1/5#2 |x|
  },
}
]]
    assert.same(exp_readable, T.tree2str(root, line))
  end)

  it("parse_line ()", function()
    assert.same({ {} }, M.parse_line("()"))
    assert.same({ {} }, M.parse_line(" ()"))
    assert.same({ {} }, M.parse_line("() "))
    assert.same({ {} }, M.parse_line("( ) "))
    assert.same({ {} }, M.parse_line(" ( ) "))
    assert.same({ { 1, 2 } }, M.parse_line("(1 2)"))
    assert.same({ { 1, 2 } }, M.parse_line("( 1 2)"))
    assert.same({ { 1, 2 } }, M.parse_line("  (1 2  )"))
    assert.same({ { 1, 3, 2 } }, M.parse_line("(1 3 2)"))
    assert.same({ { 1, 3, 2 }, ']]abc' }, M.parse_line("(1 3 2)]]abc"))
  end)

  -- showed as one string but but under the hood these are two different tokens
  it("parse_line () wrong input", function()
    local res, root, t = M.parse_line("()'ab' 'cd']]z")
    -- buildObject merge two string token without separator to the one
    assert.same({ {}, 'ab', 'cd]]z' }, res)
    -- but in fact in the tree these are two separate tokens
    assert.same(6, #root)
    assert.same("id:#5 /3#1(6) deep:1 [8:11:':] |'cd'|", t:tokenInfo(root[3]))
    assert.same('id:#6 /4#1(6) deep:1 [12:12] |]|', t:tokenInfo(root[4]))
    assert.same('id:#7 /5#1(6) deep:1 [13:13] |]|', t:tokenInfo(root[5]))
    assert.same('id:#8 /6#1(6) deep:1 [14:14] |z|', t:tokenInfo(root[6]))
  end)

  it("token_has_valid_range", function()
    local line = "' ' ''"
    -- local res, root = M.parse_line(line)
    local t = M.Tokenizer:new(line):tokenize()
    local res = t:buildObject()
    local root = t:getRoot()
    local t1 = T.token_find_by_path(root, '/1')
    local t2 = T.token_find_by_path(root, '/2')

    assert.same(' ', t:getStrValue(t1))
    assert.same("[1:3:':]", T.get_token_name(t1))
    assert.same('', t:getStrValue(t2))

    assert.same("'", (t2 or {}).open) --- works based on quotes
    assert.same("[5:6:':]", T.get_token_name(t2))

    assert.same(true, M.token_has_valid_range(t1))
    assert.same(true, M.token_has_valid_range(t2))

    assert.same({ ' ', '' }, res)
  end)

  it("token_has_valid_range", function()
    local root = T.new_token()
    local t1 = T.new_token(root, 8, nil)

    assert.same(false, M.token_has_valid_range(t1))
    assert.same(true, M.token_has_valid_range(t1, 9))
    assert.same(false, M.token_has_valid_range(t1, 7))

    t1.pe = 9 -- [8:9] ok
    assert.same(true, M.token_has_valid_range(t1))

    t1.pe = 7 -- [8:7] bad
    assert.same(false, M.token_has_valid_range(t1))

    -- if given the p_end has more priority than token.pe

    t1.pe = 8                                          -- [8:8] ok
    assert.same(true, M.token_has_valid_range(t1))
    assert.same(false, M.token_has_valid_range(t1, 0)) -- sure p_end not t1.pe

    t1.pe = 7                                          -- [8:7] bad
    assert.same(false, M.token_has_valid_range(t1))
    assert.same(true, M.token_has_valid_range(t1, 9))  -- used p_end not t1.pe
  end)

  -----------------------------------------------------------------------------
  --                            OOP Style
  -----------------------------------------------------------------------------

  it("Tokenizer constructor short", function()
    local line = 'abc'
    local t = M.Tokenizer:new(line)
    assert.same(line, t.line)
    assert.same(1, t.pstart)
    assert.same(#line, t.pend)
  end)

  it("new_token", function()
    local t = M.Tokenizer:new()
    local root = t:new_token(false)
    local t1 = t:new_token(root, 4)
    local t2 = t:new_token(root, 5)
    assert.same(1, root.id)
    assert.same(2, t1.id)
    assert.same(3, t2.id)
  end)

  it("Tokenizer constructor source", function()
    local line = 'abc'
    local t = M.Tokenizer:new():source(line)
    assert.same(line, t.line)
    assert.same(1, t.pstart)
    assert.same(#line, t.pend)
  end)

  it("Tokenizer constructor table", function()
    local line = 'abc'
    local t = M.Tokenizer:new({ line = line })
    assert.same(line, t.line)
    assert.same(1, t.pstart)
    assert.same(#line, t.pend)
  end)

  it("Tokenizer", function()
    local line = '[]'
    local t = M.Tokenizer:new():source(line)
    assert.same(line, t.line)
    assert.same(1, t.pstart)
    assert.same(#line, t.pend)
    t:tokenize()

    local res_root = t:getRoot()
    local res_token = t:getToken()

    T.reset_next_id()
    local exp_root = T.new_token(false)
    local exp_token = T.new_token(exp_root, 1, 2, '[')
    exp_root.id = 1
    exp_token.id = 2
    T.token_close(exp_token)
    assert.same(exp_root, res_root)
    local res_info = T.token_info(res_token, line)
    assert.is_function(T.token_info)
    assert.is_not_nil(res_info)
    assert.same('id:#2 /1#1(1) deep:1 [1:2:[:] |[]|', res_info)
    assert.same('#2 /1#1', T.get_token_id(res_token))
    assert.same(exp_token, res_token)
    assert.same('[]', t:getStrValue(res_token))

    assert.match_error(function()
      t:getStrValue(res_root)
    end, 'token.p_start')

    local res = t:buildObject()
    assert.same({ {} }, res)
  end)
  -----------------------------------------------------------------------------
  --                            Lua Tables
  -----------------------------------------------------------------------------

  it("tokinize {} verbose", function()
    local t = M.Tokenizer:new("{1, 2, 3}"):tokenize()
    local res_root = t:getRoot()

    local exp_root_named = { -- named parent instead of links (gen bytree2str)
      parent = false,
      id = 1,
      {
        parent = 1,
        id = 2,
        ps = 1,
        pe = 9,
        open = '{',
        { parent = 2, ps = 2, pe = 2, id = 3 },
        { parent = 2, ps = 5, pe = 5, id = 4 },
        { parent = 2, ps = 8, pe = 8, id = 5 },
      },
    }

    local root = { parent = false, id = 1 }
    local block = { open = "{", parent = root, ps = 1, pe = 9, id = 2 }
    block[#block + 1] = { parent = block, ps = 2, pe = 2, id = 3 }
    block[#block + 1] = { parent = block, ps = 5, pe = 5, id = 4 }
    block[#block + 1] = { parent = block, ps = 8, pe = 8, id = 5 }
    root[#root + 1] = block

    assert.same(root, res_root)
    local res_root_named = T.readable_copy(res_root)
    assert.same(exp_root_named, res_root_named)

    local branch1 = res_root_named[1]
    assert.same(1, branch1.parent) -- [root]
    assert.same('{', branch1.open)
    assert.same('1-9', branch1.ps .. '-' .. branch1.pe)

    local leaf_1_1 = res_root_named[1][1]
    assert.same(2, leaf_1_1.parent)
    assert.same('1', t:getStrValue(leaf_1_1))
  end)

  it("tokinize {} list", function()
    local line = "{1,2,3}"
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('1', t:getStrValue(res_root[1][1]))
    assert.same('2', t:getStrValue(res_root[1][2]))
    assert.same('3', t:getStrValue(res_root[1][3]))
  end)

  it("tokinize {} list 2", function()
    local line = 'abcde'
    local t = M.Tokenizer:new(line)
    local root = t:new_token(false)
    assert.same(1, root.id)
    t.token = t:new_token(root, 2, 2)
    local t3 = t:new_token(t.token, 4, 4)
    assert.same(2, t.token.id)

    assert.same('#1 /', T.get_token_id(root))
    assert.same('#2 /*1#1', T.get_token_id(t.token))
    assert.same('#3 /*1#1/*1#2', T.get_token_id(t3))
    t:dprint_token('Created [NEW] token', t.token)
    t:dprint_token('Created [NEW] token', t3)
  end)

  it("tokinize {} list 2", function()
    local line = "{  1, 2 , 3  }"
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('1', t:getStrValue(res_root[1][1]))
    assert.same('2', t:getStrValue(res_root[1][2]))
    assert.same('3', t:getStrValue(res_root[1][3]))
  end)

  it("tokinize {} mixed k=v + list", function()
    local line = "{ k = 1,2 , 3  }"
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('k', t:getStrValue(res_root[1][1]))
    assert.same('2', t:getStrValue(res_root[1][2]))
    assert.same('3', t:getStrValue(res_root[1][3]))
    assert.is_nil(res_root[1][4])
  end)

  it("tokinize {} mixed k=v + list", function()
    local line = "{ k = 1, {2 , 3} } ab"
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('k', t:getStrValue(res_root[1][1]))
    assert.same('{2 , 3}', t:getStrValue(res_root[1][2]))
    assert.is_nil(res_root[1][3])
    assert.same('2', t:getStrValue(res_root[1][2][1]))
    assert.same('3', t:getStrValue(res_root[1][2][2]))
    assert.same('ab', t:getStrValue(res_root[2]))
  end)

  it("tokinize {} quote", function()
    local line = "{ a b ' } ' c }"
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('a', t:getStrValue(res_root[1][1]))
    assert.same('b', t:getStrValue(res_root[1][2]))
    assert.same(' } ', t:getStrValue(res_root[1][3]))
    assert.same('c', t:getStrValue(res_root[1][4]))
  end)

  it("tokinize {} qquote", function()
    local line = '{ a b " } " c }'
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('a', t:getStrValue(res_root[1][1]))
    assert.same('b', t:getStrValue(res_root[1][2]))
    assert.same(' } ', t:getStrValue(res_root[1][3]))
    assert.same('c', t:getStrValue(res_root[1][4]))
  end)

  it("tokinize {} nested", function()
    local line = '{{{a}{b}}}{c}'
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('{{{a}{b}}}', t:getStrValue(res_root[1]))
    assert.same('{{a}{b}}', t:getStrValue(res_root[1][1]))
    assert.same('{a}', t:getStrValue(res_root[1][1][1]))
    assert.same('a', t:getStrValue(res_root[1][1][1][1]))
    assert.same('{b}', t:getStrValue(res_root[1][1][2]))
    assert.same('b', t:getStrValue(res_root[1][1][2][1]))
    assert.same('{c}', t:getStrValue(res_root[2]))
    assert.same('c', t:getStrValue(res_root[2][1]))
  end)

  it("tokinize {} nested", function()
    local line = '{{{a},{b},}},{c}'
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('{{{a},{b},}}', t:getStrValue(res_root[1]))
    assert.same('{{a},{b},}', t:getStrValue(res_root[1][1]))
    assert.same('{a}', t:getStrValue(res_root[1][1][1]))
    assert.same('a', t:getStrValue(res_root[1][1][1][1]))
    assert.same('{b}', t:getStrValue(res_root[1][1][2]))
    assert.same('b', t:getStrValue(res_root[1][1][2][1]))
    assert.same('{c}', t:getStrValue(res_root[2]))
    assert.same('c', t:getStrValue(res_root[2][1]))
  end)

  it("tokinize {} nested", function()
    local line = '{  {  {  a  },{b} ,}   }  ,{  c  }  '
    local t = M.Tokenizer:new(line):tokenize()
    local res_root = t:getRoot()
    assert.same('{  {  {  a  },{b} ,}   }', t:getStrValue(res_root[1]))
    assert.same('{  {  a  },{b} ,}', t:getStrValue(res_root[1][1]))
    assert.same('{  a  }', t:getStrValue(res_root[1][1][1]))
    assert.same('a', t:getStrValue(res_root[1][1][1][1]))
    assert.same('{b}', t:getStrValue(res_root[1][1][2]))
    assert.same('b', t:getStrValue(res_root[1][1][2][1]))
    assert.same('{  c  }', t:getStrValue(res_root[2]))
    assert.same('c', t:getStrValue(res_root[2][1]))
  end)

  -----------------------------------------------------------------------------
  --     T.s2o() == M.Tokenizer:new("{}"):tokenize():buildObject()

  it("buildObject empty lua table", function()
    assert.same({ {} }, T.s2o("{,,}"))
    assert.same({ { '', '' } }, T.s2o("{'','',}"))
  end)

  it("buildObject empty lua table", function()
    local res = M.Tokenizer:new("{}"):tokenize():buildObject()
    assert.same({ {} }, res)
    assert.same({ {} }, T.s2o("{}"))
    --
    assert.same({ {}, {} }, T.s2o("{}{}"))
    assert.same({ {}, {} }, T.s2o("{},{}"))
    assert.same({ { { { { { {} } } } } } }, T.s2o("{{{{{{}}}}}}"))
  end)

  it("buildObject lua tables nested", function()
    local res = T.s2o("{{{{}}},{}}")
    assert.same({ { { { {} } }, {} } }, res)
  end)

  it("buildObject lua tables nested", function()
    local res = T.s2o("{{{{},{},{}}},{}}")
    assert.same({ { { { {}, {}, {} } }, {} } }, res)
  end)

  it("buildObject lua table-list", function()
    local res = T.s2o("{'abc', 'de', 'fg'}")
    assert.same({ { 'abc', 'de', 'fg' } }, res)
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{key='value'}"):tokenize()
    local root = t:getRoot()
    assert.same("{key='value'}", t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('value', t:getStrValue(root[1][1][1]))
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{key=''}"):tokenize()
    local root = t:getRoot()
    assert.same("{key=''}", t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('', t:getStrValue(root[1][1][1]))
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{key=88}"):tokenize()
    local root = t:getRoot()
    assert.same('{key=88}', t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('88', t:getStrValue(root[1][1][1]))
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{ key=0 }"):tokenize()
    local root = t:getRoot()
    assert.same('{ key=0 }', t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('0', t:getStrValue(root[1][1][1]))
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{ key=. }"):tokenize()
    local root = t:getRoot()
    assert.same('{ key=. }', t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('.', t:getStrValue(root[1][1][1]))
  end)

  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{ key=88 }"):tokenize()
    local root = t:getRoot()
    assert.same('{ key=88 }', t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.same('88', t:getStrValue(root[1][1][1]))
  end)


  it("open_kv-entry", function()
    local t = M.Tokenizer:new("{ key = 88 }"):tokenize()
    local root = t:getRoot()
    assert.same('{ key = 88 }', t:getStrValue(root[1]))
    assert.same('key', t:getStrValue(root[1][1]))
    assert.is_nil(root[1][2])
    assert.same('88', t:getStrValue(root[1][1][1]))
  end)

  it("kv-entry fail without value of the kv-entry)", function()
    local line = '{key=}'
    local t = M.Tokenizer:new(line):setThrowErrors(true):tokenize()
    t.throw_errors = true
    assert.match_error(function()
      t:buildObject()
    end, "token-key without token-value has:nil", 1, true)

    local root = t:getRoot()
    assert.same('{key=}', t:getStrValue(root[1])) -- group {}
    assert.same('key', t:getStrValue(root[1][1])) -- token-key in group {}
    local exp = { parent = 2, id = 3, ps = 2, pe = 4, entry_key = true }
    assert.same(exp, T.readable_copy(root[1][1]))
  end)

  it("kv-entry fail no end of group and value of the kv-entry)", function()
    local line = '{key= '
    local t = M.Tokenizer:new(line):setThrowErrors(true):tokenize()

    assert.match_error(function()
      t:buildObject()
    end, "token-key without token-value has:nil", 1, true)
    local root = t:getRoot()
    assert.same('{key= ', t:getStrValue(root[1])) -- group{}
    assert.same('key', t:getStrValue(root[1][1])) -- token-key in group {}
    assert.same(nil, root[1][1][1])               -- sure no token-value
    local exp = { parent = 2, id = 3, ps = 2, pe = 4, entry_key = true }
    assert.same(exp, T.readable_copy(root[1][1])) -- token-key
  end)

  it("buildObject lua table k = v", function()
    local res, line, t2s, root = T.s2o("{ key = 'value' } key2 = 'X'")
    local exp = { { key = 'value', }, 'key2', '=', 'X' }

    assert.same(exp, res)
    assert.is_string(line)
    assert.is_function(t2s)
    assert.is_table(root)
  end)


  it("buildObject lua table k = v 2", function()
    local line = "{ key='value' } key2='X'"
    assert.same({ { key = 'value', }, 'key2=X' }, T.s2o(line))

    line = "{key='value'}key2='X'"
    assert.same({ { key = 'value', }, 'key2=X' }, T.s2o(line))
  end)

  it("buildObject lua table k = v 3", function()
    local line = "{ key='value' } key2='X'"
    assert.same({ { key = 'value', }, 'key2=X' }, T.s2o(line))

    line = "{key='value'}key2='X'"
    assert.same({ { key = 'value', }, 'key2=X' }, T.s2o(line))

    line = "{   key    =    'value'   },   key2   =   'X'   "
    assert.same({ { key = 'value', }, 'key2', '=', 'X' }, T.s2o(line))
  end)

  it("buildObject lua table k = v 3", function()
    local line = "{ k = {'value'} } key2='X'"
    assert.same({ { k = { 'value' }, }, 'key2=X' }, T.s2o(line))
  end)

  it("buildObject lua table k = v 4", function()
    local line = "{ k = {{'value'}} } k2='V'"
    assert.same({ { k = { { 'value' } }, }, 'k2=V' }, T.s2o(line))
  end)

  it("buildObject lua table k = v 5", function()
    local line = "{ k = {{{{'value'}}}} } k2='V'"
    assert.same({ { k = { { { { 'value' } } } }, }, 'k2=V' }, T.s2o(line))
  end)

  --TODO fix numbers
  it("buildObject lua table k = v 5", function()
    local line = "{ k = {1,{2,{3,{'value'}}}} } k2=88"
    local exp = { { k = { 1, { 2, { 3, { "value" } } } } }, 'k2=88' }
    assert.same(exp, T.s2o(line))
  end)

  ---- kv-entry
  --
  it("close_group of kv-entry", function()
    local line = '{key=8}'
    local t = M.Tokenizer:new(line)
    t:tokenize()
    local res = t:buildObject()
    assert.same({ { key = 8, } }, res)
  end)

  it("close_group of kv-entry", function()
    local line = '{k_a1={b1={c1=8},b2=1,b3=2},k_a2=8}'
    local t = M.Tokenizer:new(line)
    t:tokenize()
    local res = t:buildObject()
    local exp = { { k_a1 = { b1 = { c1 = 8 }, b2 = 1, b3 = 2 }, k_a2 = 8 } }
    assert.same(exp, res)
  end)

  it("close_group of kv-entry", function()
    local line = '{k_a1=1}, "abc d", {b1={c1=8},"de f"}, k_a2=8'
    local t = M.Tokenizer:new(line)
    t:tokenize()
    local res = t:buildObject()
    local exp = { { k_a1 = 1 }, "abc d", { b1 = { c1 = 8 }, 'de f' }, 'k_a2=8' }
    assert.same(exp, res)
  end)


  it("add_string_token in kv-entry", function()
    local rc = T.readable_copy
    local line, root, t2, t3, t4 = '{k="a b"}', nil, nil, nil, nil
    local t = M.Tokenizer:new(line)
    if 0 == 1 then
      t:tokenize()
      root = t:getRoot()
      print(T.tree2str(root, line))
    else
      root = t:new_token(false)           -- container for all values
      t2 = t:new_token(root, 1, nil, '{') -- group-container(branch) for elements in {}
      t3 = t:new_token(t2, 2)             -- k token_key in kv-entry
      assert.same({ parent = 1, id = 2, ps = 1, open = '{' }, rc(t2))

      t.token = t3                                        -- before open_kv_entry it just regular-empty token(with ps)
      -- token-key is a kv-entry container for token-value(single-value)
      assert.same({ parent = 2, id = 3, ps = 2 }, rc(t3)) -- sure regular(empty)
      t:open_kv_entry(3)
      -- after open_kv_entry this is the token-key
      assert.same({ parent = 2, id = 3, ps = 2, pe = 2, entry_key = true }, rc(t3))

      t4 = t.token -- just empty opened token ready to feed -- token-value
      assert.same({ parent = 3, id = 4, ps = 4, }, rc(t4))
      local new_i = t:add_string_token(4, '"')
      assert.same(8, new_i)
      -- assert.same(8, t.token)
      -- TODO validate close group after
    end
  end)

  -----------------------------------------------------------------------------

  it("kv-entry {}-group  -- keynames in [key]", function()
    -- local res, line, t2s, root = T.s2o('{[1] = 1}')

    assert.same({ { [1] = 1 } }, T.s2o('{[1] = 1}'))
    assert.same({ { ['1'] = 1 } }, T.s2o("{['1'] = 1}"))
    -- print(inspect( T.s2o('{["1"] = 1}')))
    assert.same({ { ['1'] = 1 } }, T.s2o('{["1"] = 1}'))
    assert.same({ { [''] = 1 } }, T.s2o('{[""] = 1}'))
    assert.same({ { [''] = 'abc' } }, T.s2o('{[""] = abc}'))

    assert.match_error(function()
      T.s2o('{[""] = }')
    end, 'token-key without token-value has:nil', 1, true)

    assert.match_error(function()
      T.s2o('{[""] =')
    end, 'token-key without token-value has:nil', 1, true)

    assert.match_error(function()
      T.s2o('{[""]=')
    end, 'token-key without token-value has:nil', 1, true)

    assert.match_error(function()
      T.s2o('{["x"] = ["x"] = 4}')
    end, '[Incorrect Input]. This token_key already has linked token-value', 1, true)

    assert.same({ { { '' } } }, T.s2o('{[""]}'))

    assert.same({ { ['x'] = 'a', b = 4 } }, T.s2o('{["x"] = a, b = 4}'))
    assert.same({ { { ['x'] = 'a' }, b = 4 } }, T.s2o('{{["x"] = a}, b = 4}'))
    assert.same({ { ['x'] = { 'x' } } }, T.s2o('{["x"] = ["x"]}'))


    local exp = { { ['x'] = { 'x' }, ['y'] = 4 } }
    assert.same(exp, T.s2o('{["x"] = ["x"] ["y"] = 4}'))
    assert.same(exp, T.s2o('{["x"] = ["x"], ["y"] = 4}'))

    T.throw_errors = false
    assert.same({ { ['x'] = { 'x' }, '=', 4 } }, T.s2o('{["x"] = ["x"] = 4}'))
    T.throw_errors = true
  end)

  it("kv-entry {}-group  -- incorrect input", function()
    T.throw_errors = false
    assert.same({ { { 1 }, '=', 2 } }, T.s2o('{{1} = 2}'))
    assert.same({ { key = 'value', '=', 1 } }, T.s2o('{key =  "value" = 1}'))
    assert.same({ { 8, '=', 'b' } }, T.s2o('{8 = "b"}'))
    assert.same({ { 4, '=', 'value' } }, T.s2o('{4 = "value"}'))
    assert.same({ { k = 'value', } }, T.s2o('{k = "value"}'))
    assert.same({ { k4 = 'value', } }, T.s2o('{k4 = "value"}'))
    assert.same({ { 0, '=', 'value' } }, T.s2o('{0 = "value"}'))
    assert.same({ { '0=', 'value' } }, T.s2o('{0="value"}'))
    assert.same({ { a = 'value', } }, T.s2o('{a="value"}'))
    assert.same({ { '0=', { 'value' } } }, T.s2o('{0=[value]}'))
    assert.same({ { key = '' } }, T.s2o('{[key]=}'))
    assert.same({ 'end)' }, T.s2o('end)'))
  end)

  it("kv-entry {}-group  -- keynames in [key]", function()
    local TKEY = {}
    local line = '{[{}] = 8}'
    local t = M.Tokenizer:new(line):tokenize()
    local root = t:getRoot()
    local res = t:buildObject()
    assert.same('{[{}] = 8}', t:getStrValue(root[1]))
    assert.same('{}', t:getStrValue(root[1][1])) -- key is an empty table

    -- try to access to value of the entry by another instance of empty table
    assert.same(nil, res[1][TKEY]) -- fails!

    -- get instance of empty table actualy used in built object
    assert.is_not_nil(t.key_objects)
    local key = t.key_objects[root[1][1]]
    assert.is_not_nil(key)

    -- access to result object(table) by used key instance
    assert.same(8, res[1][key]) -- success
  end)


  it("{} busted output", function()
    assert.same({ { ['x'] = 'a', b = 4 } }, T.s2o('{*["x"] = a, b = 4}'))
    assert.same({ { '**', ['x'] = 'a', b = 4 } }, T.s2o('{**["x"] = a, b = 4}'))
    local input1 =
        "{" ..
        " *[1] = {" ..
        "   *[b] = 4" ..
        "    [x] = 'a' } }"
    local input2 = "\n{\n *[1] = {\n  *[b] = 4\n    [x] = 'a' } }\n"
    local exp = { { { b = 4, x = 'a' } } }
    assert.same(exp, T.s2o(input1))
    assert.same(exp, T.s2o(input2))
  end)

  -- TODO table as a key
  it("{} busted output custom var for table as key", function()
    local input =
        "(table: 0x5612f009cf40) {" ..
        " *[1] = {" ..
        "   *[table: 0x5612f00f3050] = 1 } }"
    local exp = { { 'table:', '0x5612f009cf40' }, { {} } }
    -- (table: 0x55aa3576cdb0) {
    --   [1] = {
    --     [1] = 'table:'
    --     [2] = '0x5612f009cf40' }
    --  *[2] = {
    --    *[1] = {
    --      *[table:] = 1 } } }
    -- assert.same(exp, T.s2o(input))
    assert.is_not_nil(T.s2o(input))
    assert.is_not_nil(exp)
  end)

  it("quoted key json", function()
    assert.same({ { key = 'value' } }, T.s2o('{"key" = "value"}'))
    assert.same({ { key = 'value' } }, T.s2o('{"key"="value"}'))
    -- wrong
    assert.same({ { key = 'value' } }, T.s2o('{key== "value"}'))
    assert.same({ { key = 'value' } }, T.s2o('{"key"::: "value"}'))
  end)

  -- TODO FIXME
  it("{} group keynames in [key]", function()
    assert.same({ { {}, '=', 'b' } }, T.s2o('{[] = "b"}'))
    assert.same({ { {}, '=', 'b' } }, T.s2o('{[] ="b"}'))
    assert.same({ { {}, '=2' } }, T.s2o('{[] =2}'))
    assert.same({ { x = 'b', '=', 4 } }, T.s2o('{[x] = "b" = 4}'))
    assert.same({ { ['x'] = 'abc' } }, T.s2o('{["x", "y"] = abc}')) -- !!
    assert.same({ { key = 'value' } }, T.s2o('{"key" , ="value"}')) -- ,=,
    -- D.enable()
    -- D.enable_module(M, false)
    assert.same({ { key = 'value' } }, T.s2o('{"key"==  = "value"}'))
  end)
end)

--[[
NOTES:
The token identifier is built based on the path to it in the tree
open-group(token-branch): id:?(0) deep:1 [1:nil]o[ open:[ type:nil
id: ? - because a new token creates as a tempora
]]

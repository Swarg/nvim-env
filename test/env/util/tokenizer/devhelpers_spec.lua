--
require 'busted.runner' ()
local assert = require('luassert')

local D = require("dprint")
local T = require("env.util.tokenizer.devhelpers")
local MM = require("env.util.tokenizer.token")

describe("env.util.tokenizer.devhelpers", function()
  setup(function()
    _G._TEST = true
    M = require("env.util.tokenizer.parser")
    -- T = M.T
    T.tokenizer = M.Tokenizer:new()
    T.new_token = function(parent, start_pos, end_pos, open)
      return T.tokenizer:new_token(parent, start_pos, end_pos, open)
    end
    T.token_close = function(token, p_end)
      T.tokenizer.root = MM.get_root(token)
      return T.tokenizer:token_close(token, p_end)
    end
    T.seal = function(token, p_end)
      T.tokenizer.token = token
      T.tokenizer.root = MM.get_root(token)
      ---@diagnostic disable-next-line: inject-field
      return T.tokenizer:seal(p_end)
    end
    T.reset_next_id = function()
      T.tokenizer.next_token_id = 0
    end
  end)

  teardown(function()
    _G._TEST = nil
  end)

  -- to get "cooked" tree call T.bind_parents(deep_copy(tree_3h_2branch_6leaf))
  local tree_3h_2branch_6leaf = {
    parent = false,
    id = 1,
    {                                               -- id: #2 /1#1 |abcdef|
      id = 2,                                       --
      parent = 1,                                   -- parent id is 1 - [root]
      ps = 1,                                       --
      pe = 6,                                       --
      {                                             -- id: #3 /1#1/1#2 |b|
        id = 3,                                     --
        parent = 2,                                 --
        ps = 2,                                     --
        pe = 2,                                     --
        { parent = 3, ps = 3, pe = 3, id = 4, },    -- id: #4 /1#1/1#2/1#3 |c|
        { parent = 3, ps = 4, pe = 4, id = 5, },    -- id: #5 /1#1/1#2/2#3 |d|
      },                                            --
      { parent = 2, ps = 5, pe = 5, id = 6, },      -- id: #6 /1#1/1#2/3#3 |e|
    },                                              --
    {                                               -- id: #7 /2#1 |ghijklm|
      id = 7,                                       --
      parent = 1,                                   -- root
      ps = 7,                                       --
      pe = 13,                                      --
      {                                             -- id: #8 /2#1/1#7 |hijkl|
        id = 8,                                     --
        parent = 7,                                 --
        ps = 8,                                     --
        pe = 12,                                    --
        { parent = 8, ps = 9,  pe = 9,  id = 9, },  -- id:  #9 /2#1/1#7/1#8 |i|
        { parent = 8, ps = 10, pe = 10, id = 10, }, -- id: #10 /2#1/1#7/2#8
        { parent = 8, ps = 11, pe = 11, id = 11, }, -- id: #11 /2#1/1#7/3#8 |k|
      },
    },
  }
  --[[
 (/1/1/1l)    (/1/1/2l)         (/2/1/1l)  (/2/1/2l)    (/2/1/3l)   (token path)
  #4[3:3]      #5[4:4]           #9[9:9]   #10[10:10]  #11[11:11]    height: 3
     \l1      /l2                       \l1     |l2    /l3            link
   #3[2:2](/1/1b)   #6[5:5](/1/2l)      #8[8:12](/2/1b)              height: 2
            \b1       /l2                  /b1
            #2[1:6](/1b)              #7[7:13](/2b)                  height: 1
                   \b1____            _____/b2
                            root id:1                                height: 0

             #id [ps:pe](path) b -baranch l - leaf
  ]]
  before_each(function()
    T.reset_next_id()
  end)

  local function new_linked_token(t, parent, ps, pe, open)
    local token = t:new_token(parent, ps, pe, open)
    return T.link(token)
  end

  local function build_tree_3h_2branch_6leaf()
    local line = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    local t = M.Tokenizer:new(line)
    ---@diagnostic disable-next-line: inject-field
    t.tokens_with_id = true
    local new = function(parent, ps, pe, open)
      return new_linked_token(t, parent, ps, pe, open)
    end
    --                          -- path     id     varname
    local root = t:new_token()  -- /         1
    local t1 = new(root, 1, 6)  -- /1        2
    local t1_1 = new(t1, 2, 2)  -- /1/1      3
    new(t1_1, 3, 3)             -- /1/1/1    4     t1_1_1
    new(t1_1, 4, 4)             -- /1/1/2    5     t1_1_2
    new(t1, 5, 5)               -- /1/2      6     t1_2
    local t2 = new(root, 7, 13) -- /2        7     t2
    local t2_1 = new(t2, 8, 12) -- /2/1      8     t2_1
    new(t2_1, 9, 9)             -- /2/1/1    9     t2_1_1
    new(t2_1, 10, 10)           -- /2/1/2   10     t2_1_2
    new(t2_1, 11, 11)           -- /2/1/3   11     t2_1_3
    return root, line
  end

  after_each(function()
    D.on = false
  end)

  it("tree_3h_2branch_6leaf & builder", function()
    ---@diagnostic disable-next-line: unused-local
    local root, line = build_tree_3h_2branch_6leaf()
    local exp_root = T.bind_parents(T.deep_copy(tree_3h_2branch_6leaf))
    -- print(T.tree2str(root, line))
    -- print(T.tree2str(tree_3h_2branch_6leaf, line))
    assert.same(1, tree_3h_2branch_6leaf[1].parent) -- id instead link
    assert.is_table(exp_root[1].parent)             -- link to table
    T.remove_all_parents(exp_root)
    T.remove_all_parents(root)
    assert.same(exp_root[2], root[2])
    assert.same(exp_root, root)
    -- assert.same(exp_root[1][1][1], root[1][1][1])
  end)


  it("tree_3h_2branch_6leaf", function()
    local root = tree_3h_2branch_6leaf
    assert.same(1, root.id)
    assert.same(2, root[1].id)
    assert.same(3, root[1][1].id)
    assert.same(4, root[1][1][1].id)
    assert.same(5, root[1][1][2].id)
    assert.same(6, root[1][2].id)
    assert.same(7, root[2].id)
    assert.same(8, root[2][1].id)
    assert.same(9, root[2][1][1].id)
    assert.same(10, root[2][1][2].id)
    assert.same(11, root[2][1][3].id)
  end)

  it("deep_copy", function()
    local tree = build_tree_3h_2branch_6leaf()
    local exp = T.deep_copy(tree) -- tree_3h_2branch_6leaf
    assert.same(1, exp[1].ps)
    assert.same(2, exp[1][1].ps)
    assert.same(3, exp[1][1][1].ps)
    assert.same(4, exp[1][1][2].ps)
    assert.same(5, exp[1][2].ps)
    assert.same(7, exp[2].ps)
    assert.same(8, exp[2][1].ps)
    assert.same(9, exp[2][1][1].ps)
    assert.same(10, exp[2][1][2].ps)
    assert.same(11, exp[2][1][3].ps)
    local res = T.deep_copy(tree) --tree_3h_2branch_6leaf
    assert.same(exp, res)
    -- change original
    exp[1].ps = 10
    exp[1][1].ps = 11
    exp[1][1][1].ps = 33
    exp[1][1][2].ps = 44
    exp[2][1][1].ps = 99
    exp[2][1][2].ps = 1010
    exp[2][1][3].ps = 1111
    -- make sure that the values in the table are copies and not references
    -- to the original values
    assert.same(1, res[1].ps)
    assert.same(3, res[1][1][1].ps)
    assert.same(4, res[1][1][2].ps)
    assert.same(9, res[2][1][1].ps)
    assert.same(10, res[2][1][2].ps)
    assert.same(11, res[2][1][3].ps)
  end)

  it("bind_parents", function()
    local orig = tree_3h_2branch_6leaf
    local copy = T.deep_copy(orig)
    local root = T.bind_parents(copy)

    local t_1 = root[1]
    local t_1_1 = root[1][1] -- branch
    local t_1_2 = root[1][2] -- leaf
    local t_1_1_1 = root[1][1][1]
    local t_1_1_2 = root[1][1][2]
    local t_2 = root[2]
    local t_2_1 = root[2][1]
    local t_2_1_1 = root[2][1][1]
    local t_2_1_2 = root[2][1][2]
    local t_2_1_3 = root[2][1][3]
    assert.same(root, t_1.parent)
    assert.same(t_1, t_1_1.parent)
    assert.same(t_1, t_1_1.parent)
    assert.same(t_1, t_1_2.parent)
    assert.same(t_1_1, t_1_1_1.parent)
    assert.same(t_1_1, t_1_1_2.parent)
    assert.same(root, t_2.parent)
    assert.same(t_2, t_2_1.parent)
    assert.same(t_2_1, t_2_1_1.parent)
    assert.same(t_2_1, t_2_1_2.parent)
    assert.same(t_2_1, t_2_1_3.parent)
  end)


  it("token_bind_index_in_parent", function()
    local root = T.new_token()
    local t1 = T.new_token(root, 1, 1)
    assert.is_nil(MM.token_get_link_index(t1))
    root[#root + 1] = t1 -- same than T.token_bind_to_parent(t1)
    assert.same(1, MM.token_get_link_index(t1))

    local t2 = T.new_token(root, 2, 2)
    assert.is_nil(MM.token_get_link_index(t2))
    T.link(t2) -- bind token to its parent
    assert.same(2, MM.token_get_link_index(t2))
  end)


  -- * - means approximate (next) index
  it("get_token_id", function()
    local root = { parent = false }
    local branch = { parent = root, ps = 1, pe = 1 }
    local leaf = { parent = branch, ps = 5, pe = 5 }
    assert.same('/*1/*1', T.get_token_id(leaf))
    root[#root + 1] = branch                                  -- now t1 is branch - a child of root
    assert.same('/1/*1', T.get_token_id(leaf))
    branch[#branch + 1] = { parent = branch, ps = 2, pe = 2 } -- new leaf
    assert.same('/1/*2', T.get_token_id(leaf))
    branch[#branch + 1] = { parent = branch, ps = 2, pe = 2 } -- leaf
    assert.same('/1/*3', T.get_token_id(leaf))
    branch[#branch + 1] = leaf
    assert.same('/1/3', T.get_token_id(leaf))
  end)

  it("get_token_id", function()
    local root = T.bind_parents(T.deep_copy(tree_3h_2branch_6leaf))
    local t_1 = root[1]
    local t_1_1 = root[1][1] -- branch
    local t_1_2 = root[1][2] -- leaf
    local t_1_1_1 = root[1][1][1]
    local t_1_1_2 = root[1][1][2]
    local t_2 = root[2]
    local t_2_1 = root[2][1]
    local t_2_1_1 = root[2][1][1]
    local t_2_1_2 = root[2][1][2]
    local t_2_1_3 = root[2][1][3]
    -- #id path
    assert.same('#2 /1#1', T.get_token_id(t_1))
    assert.same('#7 /2#1', T.get_token_id(t_2))
    assert.same('#3 /1#1/1#2', T.get_token_id(t_1_1))
    assert.same('#4 /1#1/1#2/1#3', T.get_token_id(t_1_1_1))
    assert.same('#5 /1#1/1#2/2#3', T.get_token_id(t_1_1_2))
    assert.same('#6 /1#1/2#2', T.get_token_id(t_1_2))
    assert.same('#8 /2#1/1#7', T.get_token_id(t_2_1))
    assert.same('#9 /2#1/1#7/1#8', T.get_token_id(t_2_1_1))
    assert.same('#10 /2#1/1#7/2#8', T.get_token_id(t_2_1_2))
    assert.same('#11 /2#1/1#7/3#8', T.get_token_id(t_2_1_3))
  end)

  it("token_find_by_path", function()
    local root = T.deep_copy(tree_3h_2branch_6leaf)

    local exp_2_1_1 = { parent = 8, ps = 9, pe = 9, id = 9 }
    assert.same(exp_2_1_1, T.token_find_by_path(root, '/2/1/1'))

    local exp_2_1_2 = { parent = 8, pe = 10, ps = 10, id = 10 }
    assert.same(exp_2_1_2, T.token_find_by_path(root, '/2/1/2'))

    local exp_2_1_3 = { parent = 8, pe = 11, ps = 11, id = 11, }
    assert.same(exp_2_1_3, T.token_find_by_path(root, '/2/1/3'))

    local exp_1_1_1 = { parent = 3, pe = 3, ps = 3, id = 4 }
    assert.same(exp_1_1_1, T.token_find_by_path(root, '/1/1/1'))

    assert.same(root[1][2], T.token_find_by_path(root, '/1/2'))
    assert.same(root[1], T.token_find_by_path(root, '1'))
    assert.same(root[2], T.token_find_by_path(root, '2'))
  end)


  it("get_token_deep h4", function()
    local root = { parent = false }
    local t1 = { parent = root }
    local t2 = { parent = t1 }
    local t3 = { parent = t2 }
    local t4 = { parent = t3 }
    root[#root + 1] = t1
    t1[#t1 + 1] = t2
    t2[#t2 + 1] = t3
    t3[#t3 + 1] = t4
    assert.same(0, T.get_token_deep(root))
    assert.same(1, T.get_token_deep(t1))
    assert.same(2, T.get_token_deep(t2))
    assert.same(3, T.get_token_deep(t3))
    assert.same(4, T.get_token_deep(t4))
  end)

  it("get_token_deep 2", function()
    -- root of tree
    local root = T.bind_parents(T.deep_copy(tree_3h_2branch_6leaf))
    local t_1 = root[1]
    local t_1_1 = root[1][1] -- branch
    local t_1_2 = root[1][2] -- leaf
    local t_1_1_1 = root[1][1][1]
    local t_1_1_2 = root[1][1][2]
    local t_2 = root[2]
    local t_2_1 = root[2][1]
    local t_2_1_1 = root[2][1][1]
    local t_2_1_2 = root[2][1][2]
    local t_2_1_3 = root[2][1][3]
    assert.same(0, T.get_token_deep(root))
    assert.same(1, T.get_token_deep(t_1))
    assert.same(1, T.get_token_deep(t_2))
    assert.same(2, T.get_token_deep(t_1_1))
    assert.same(2, T.get_token_deep(t_1_2))
    assert.same(3, T.get_token_deep(t_1_1_1))
    assert.same(3, T.get_token_deep(t_1_1_2))
    assert.same(1, T.get_token_deep(t_2))
    assert.same(2, T.get_token_deep(t_2_1))
    assert.same(3, T.get_token_deep(t_2_1_1))
    assert.same(3, T.get_token_deep(t_2_1_2))
    assert.same(3, T.get_token_deep(t_2_1_3))
  end)

  it("remove_all_parents", function()
    local root = { parent = false }
    local t1 = { parent = root }
    local t2 = { parent = t1 }
    local t3 = { parent = t2 }
    local t4 = { parent = t3 }
    root[#root + 1] = t1
    t1[#t1 + 1] = t2
    t2[#t2 + 1] = t3
    t3[#t3 + 1] = t4
    assert.same(4, T.remove_all_parents(root))
    local exp = {
      parent = false, -- root
      {               -- fiset element in list same that [1] = {...}
        {             -- t2
          {           -- t3
            {         -- t4 root[1][1][1]
            },
          },
        },
      },
    }
    assert.same(exp, root)
    assert.same(t1, root[1])
    assert.same(t2, root[1][1])
    assert.same(t3, root[1][1][1])
    assert.same(t4, root[1][1][1][1])
  end)

  it("get_token_childs", function()
    local root = { parent = false }
    local t1 = { parent = root, ps = 1, pe = 1 }
    local t2 = { parent = root, ps = 2, pe = 2 }
    local t3 = { parent = root, ps = 3, pe = 3 }
    assert.same(0, T.get_token_childs(root))
    root[#root + 1] = t1
    assert.same(1, T.get_token_childs(root))
    root[#root + 1] = t2
    assert.same(2, T.get_token_childs(root))
    root[#root + 1] = t3
    assert.same(3, T.get_token_childs(root))
  end)

  it("get_token_childs", function()
    local tree = T.bind_parents(T.deep_copy(tree_3h_2branch_6leaf))
    local t_1 = tree[1]
    local t_1_1 = tree[1][1] -- branch
    local t_1_2 = tree[1][2] -- leaf
    local t_1_1_1 = tree[1][1][1]
    local t_1_1_2 = tree[1][1][2]
    local t_2 = tree[2]
    local t_2_1 = tree[2][1]
    local t_2_1_1 = tree[2][1][1]
    local t_2_1_2 = tree[2][1][2]
    local t_2_1_3 = tree[2][1][3]
    assert.same(2, T.get_token_childs(tree))
    assert.same(2, T.get_token_childs(t_1))
    assert.same(2, T.get_token_childs(t_1_1))
    assert.same(0, T.get_token_childs(t_1_1_1)) -- leaf 0-childs
    assert.same(0, T.get_token_childs(t_1_1_2)) -- leaf
    assert.same(0, T.get_token_childs(t_1_2))   -- leaf
    assert.same(1, T.get_token_childs(t_2))
    assert.same(3, T.get_token_childs(t_2_1))
    assert.same(0, T.get_token_childs(t_2_1_1)) -- leaf
    assert.same(0, T.get_token_childs(t_2_1_2))
    assert.same(0, T.get_token_childs(t_2_1_3))
  end)

  it("mk_parents_names_readable", function()
    local root = { parent = false }
    local t1 = { parent = root, ps = 1, pe = 1 }
    local t2 = { parent = t1, ps = 2, pe = 2 }
    local t3 = { parent = t2, ps = 3, pe = 3 }
    root[#root + 1] = t1
    t1[#t1 + 1] = t2
    t2[#t2 + 1] = t3

    assert.same(3, T.mk_parents_names_readable(root))
    local exp = {
      parent = false,
      {
        parent = "[root]",
        pe = 1,
        ps = 1,
        {
          parent = "[1:1]",
          pe = 2,
          ps = 2,
          {
            parent = "[2:2]", pe = 3, ps = 3
          },
        },
      },
    }
    assert.same(exp, root)
  end)

  -- test helper
  it("tree2str", function()
    local root = { parent = false }
    local t1 = { parent = root, ps = 1, pe = 1 }
    local t2 = { parent = root, ps = 2, pe = 2 }
    local t3 = { parent = root, ps = 3, pe = 3 }
    local t4 = { parent = t3, ps = 4, pe = 4 }
    root[#root + 1] = t1
    root[#root + 1] = t2
    root[#root + 1] = t3
    t3[#t3 + 1] = t4
    local exp = [[
local exp = {
  parent = false,                            -- /
  { parent = '[root]', ps = 1, pe = 1, },    -- ?
  { parent = '[root]', ps = 2, pe = 2, },    -- ?
  { parent = '[root]', ps = 3, pe = 3,       -- ?
    { parent = '[3:3]', ps = 4, pe = 4, },   -- ?
  },
}
]]
    T.mk_parents_names_readable(root)
    assert.same(exp, T.tree2str(root, false))
    local back = loadstring(exp .. ';return exp')()
    assert.same(root, back)
  end)

  it("token_close + seal", function()
    local root = T.new_token()
    local t1 = T.new_token(root, 1)
    local t2 = T.new_token(t1, 2)
    local t3 = T.new_token(t2, 3)
    local t4 = T.new_token(t3, 4)
    -- the star(*) means that only child has parent but it parent has NO this child
    -- by design the child is bind to the parent on calling token_close()
    assert.same('#1 /', T.get_token_id(root))
    assert.same('#5 /*1#1/*1#2/*1#3/*1#4', T.get_token_id(t4))
    ---- emulate token_ends
    assert.same(nil, t4.pe)
    T.token_close(t4, 8)
    assert.same(8, t4.pe)
    ----
    T.seal(t4, 8)
    assert.same('#5 /1#1/1#2/1#3/1#4', T.get_token_id(t4))
  end)


  it("token_close + seal", function()
    local root = T.new_token()
    local t1 = T.new_token(root, 1)
    local t2 = T.new_token(t1, 2)
    local t3 = T.new_token(t2, 3)
    local t4 = T.new_token(t3, 4)
    assert.same('#5 /*1#1/*1#2/*1#3/*1#4', T.get_token_id(t4))

    assert.same(t1, T.token_close(t1, 1))
    assert.same(t1, root[1])
    assert.same(t2, T.token_close(t2, 2))
    assert.same(t2, t1[1])

    assert.same('#5 /1#1/1#2/*1#3/*1#4', T.get_token_id(t4))
    assert.same(t3, t4.parent)
    assert.same(0, #t3)
    T.token_close(t4, 8)
    T.seal(t4, 8)
    assert.same('#5 /1#1/1#2/1#3/1#4', T.get_token_id(t4))
  end)

  it("format_comments", function()
    local exp =
        ".\n" ..
        "abc de   -- 1\n" ..
        "abc      -- 2\n"
    assert.same(exp, T.format_comments(".\nabc de -- 1\nabc -- 2"))
    assert.same(exp, T.format_comments(".\nabc de -- 1\nabc -- 2\n"))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')

local R = require 'env.require_util'
---@diagnostic disable-next-line
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local D = require("dprint")

local class = require 'oop.class'

describe("env.util.tokenizer.token_tree", function()
  setup(function()
    _G._TEST = true
    M = require("env.util.tokenizer.tree")
    TP = require("env.util.tokenizer.parser")
    -- Tokenizer = TP.Tokenizer
    T = TP.T
  end)

  teardown(function() _G._TEST = nil end)
  before_each(function() end)
  after_each(function() D.on = false end)

  local function treeOf(line)
    local tree = M.of(TP.Tokenizer:new(line):tokenize())
    return tree
  end

  it("Tree Factory from Tokenizer Parser", function()
    local line = "function(x) return x * x end"
    local t = treeOf(line)
    ---@diagnostic disable-next-line: undefined-field
    assert.same('parser.tokenizer.Tree', class.name(t))
    assert.same('parser.tokenizer.Tree', t:getClassName())
    -- print("[DEBUG] t:", inspect(t))
    local root = t:getRoot()
    -- assert.same('x', root[1])
    -- D.enable()
    assert.same('function', t:cat(root[1]))
    assert.same(true, t:isWord('function', root[1]))
  end)

  it("is_token_contains_word no (word must be without quotes)", function()
    local line = " 'function'(x)"
    local t = treeOf(line)
    -- print(t:toString())
    local root = t:getRoot()
    assert.same('function', t:cat(root[1]))
    assert.same(false, t:isWord('function', root[1]))
  end)


  -- token tree as a file system

  it("change-token cd", function() -- like the cd - change directory
    -- local rc = T.readable_copy
    local line = "{k1 = {k2 = 'v', 'str', num} }"
    --            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^  /1       or   0 1
    --             ^^                            /1/1          0 1 1
    --                  ^^^^^^^^^^^^^^^^^^^^^^   /1/1/1        0 1 1 1
    --                   ^^                      /1/1/1/1      0 1 1 1
    local t = treeOf(line)
    local root = t:getRoot()
    local t0_1 = root[1]
    assert.is_not.same(root, t.token)
    assert.same(root, t:cd(0).token)
    assert.same(t0_1, t:cd(0, 1).token)

    assert.same(line, t:cat(t0_1))
    assert.same(line, t:cd(0, 1):cat())    -- of t.token
    assert.same('k1', t:cd(0, 1, 1):cat()) -- t.token
    assert.same(nil, t:cd(0, 1, 2):cat())  -- no

    local exp = "{k2 = 'v', 'str', num}"
    assert.same(exp, t:cd(0, 1, 1, 1):cat())
    -- relative from prev
    assert.same(root[1][1][1][1], t:cd(1).token) -- k2
    assert.same('k2', t:cat())                   -- k2
    assert.same("{k2 = 'v', 'str', num}", t:cd(-1):cat())
    assert.same("{k1 = {k2 = 'v', 'str', num} }", t:cd(-2):cat())
  end)

  -- token tree as a file system
  it("ls list of tokens in the current(self.token) context", function()
    local line = "a b c {k = 'v', 'str', 9, word, extra(1,2) } d"
    local t = treeOf(line)
    local exp = [[
----    1     [1:1]             a
----    2     [3:3]             b
----    3     [5:5]             c
d---    4  {  [7:44]            {k = 'v', 'str', 9, word, extra(1,2) }
----    5     [46:46]           d
]]
    assert.same(exp, t:cd(0):ls())

    local exp2 = [[
dK--    1     [8:8]             k
----    2  '  [17:21]           'str'
----    3     [24:24]           9
----    4     [27:30]           word
----    5  e  [33:37]           extra
d---    6  (  [38:42]           (1,2)
]]
    assert.same(exp2, t:cd(0, 4):ls())

    local exp3 = [[
----    1     [39:39]           1
----    2     [41:41]           2
]]
    assert.same(exp3, t:cd(0, 4, 6):ls())
  end)

  -- todo add support for lua multiline strings in the block [[ ]] and [==[ ]==]
  it("ls list of tokens in the current(self.token) context", function()
    local line = "a b  {k = 'v' extra(1,2) } [str arr] [[ the string]]"
    local t = treeOf(line)
    local exp = [=[
----    1     [1:1]             a
----    2     [3:3]             b
d---    3  {  [6:26]            {k = 'v' extra(1,2) }
d---    4  [  [28:36]           [str arr]
d---    5  [  [38:52]           [[ the string]]
]=]
    assert.same(exp, t:cd(0):ls())
    local exp2 = "d---    1  [  [39:51]           [ the string]\n"
    assert.same(exp2, t:cd(0, 5):ls())
    local exp3 = [[
----    1     [41:43]           the
----    2     [45:50]           string
]]
    assert.same(exp3, t:cd(0, 5, 1):ls())
  end)

  it("pwd", function()
    local line = "a b c {k = 'v', 'str', 9, word, extra(f,g) } d"
    local t = treeOf(line)
    -- here zero is a root like /
    assert.same('0 4 6', t:cd(0, 4, 6):pwd()) -- /4/6
    assert.same('0 4', t:cd(0, 4):pwd())
    assert.same('0', t:cd(0):pwd())
    assert.same('0 4 6 1', t:cd(0, 4, 6, 1):pwd()) -- f
    assert.same('f', t:cat())
    assert.same('', t:cd(0, 4, 6, 1, 1):pwd())     -- not exists
    assert.same(nil, t.token)
    assert.same('0 4 6 1', t:cd(0, 4, 6, 1):pwd())
    assert.same('0 4 6 1', t:pwd())
    assert.same('0 4', t:cd(-2):pwd())
    assert.same('0', t:cd(-99):pwd()) -- always stop on root - don't go deeper.
  end)

  local function mk_cat_for(tree)
    local t = tree
    assert(type(t) == 'table', 't')
    return function(...)
      t:cd(...)
      local s, ps, pe = t:cat()
      return string.format('[%s:%s] |%s|', tostring(ps), tostring(pe), tostring(s))
    end
  end

  it("cat", function()
    --            1234567890123456
    local line = "    word1 'ab c' {k = 'de f'} d [the string array] [[xyz]]"
    --                 1      2    ^---- 3 ---^ 4 ^------ 5 -------^ ^--6--^
    --                             /3/1  /3/1/1
    local t = treeOf(line)
    local cat = mk_cat_for(t)
    assert.same('[5:9] |word1|', cat(0, 1))
    assert.same("    w", line:sub(1, 5))     -- tab_n = ps - 1
    assert.same('[11:16] |ab c|', cat(0, 2)) -- show without quotes, but range with
    assert.same("'ab c'", line:sub(11, 16))
    assert.same("[18:29] |{k = 'de f'}|", cat(0, 3))
    assert.same('[19:19] |k|', cat(0, 3, 1))       -- entry-key
    assert.same('[23:28] |de f|', cat(0, 3, 1, 1)) -- entry-value
    assert.same('[31:31] |d|', cat(0, 4))
    assert.same('[33:50] |[the string array]|', cat(0, 5))
    assert.same('[34:36] |the|', cat(0, 5, 1))
    assert.same('[45:49] |array|', cat(0, 5, 3))
    assert.same('[52:58] |[[xyz]]|', cat(0, 6))
    -- D.enable()
    t = treeOf('')
    assert.same("----    1  e  [0:0]             \n", t:cd(0):ls())
    assert.same('[0:0] ||', t:cat0(0, 1))
  end)


  it("last", function()
    assert.same('[13:14] |[[|', treeOf('  local x = [['):cd(0):last():cat0())
    assert.same('[0:0] ||', treeOf(''):cd(0):last():cat0())
    assert.same('[1:1] |x|', treeOf('x'):cd(0):last():cat0())
    assert.same('[3:3] |=|', treeOf('x = '):cd(0):last():cat0())
    local t = treeOf('x = [[')
    assert.same('[5:6] |[[|', t:cd(0):last():cat0())
    assert.same('[[', t:cd(0):last():cat()) -- it token branch(/3) + leaf( /3/1)
    local exp = [==[
----    1     [1:1]             x
----    2     [3:3]             =
d---    3  [  [5:6]             [[
]==]
    assert.same(exp, t:cd(0):ls())
    local exp2 = "----    1  [  [6:6]             [\n"
    assert.same(exp2, t:cd(0, 3):ls())
    assert.same('id:#5 /3#1/1#4(1) deep:2 [6:6:[:] |[|', t:cd(0, 3, 1):stat())
    assert.same('id:#4 /3#1(3) deep:1 [5:6:[:] |[[|', t:cd(0):last():stat())
    assert.same(true, t:cd(0):last():isGroup('['))
    assert.same(2, t:cd(0):last():rangeLength())
    assert.same('[[', t:cd(0):last():cat())
    --
    t = treeOf('  x = [==[')
    assert.same('[7:10] |[==[|', t:cd(0):last():cat0())
    local exp3 = [[
----    1     [3:3]             x
----    2     [5:5]             =
d---    3  [  [7:10]            [==[
]]
    assert.same(exp3, t:cd(0):ls())
    local exp4 = [[
----    1  e  [8:9]             ==
----    2  [  [10:10]           [
]]
    assert.same(exp4, t:cd(0, 3):ls())
    -- group '[':
    --          \-  string='==', group='['
  end)

  it("last", function()
    assert.same('[2:2] |]|', treeOf(']]'):cd(0):last():cat0())
    assert.same(']', treeOf(']]'):cd(0, 1):cat())
    local exp = [[
----    1     [1:1]             ]
----    2     [2:2]             =
----    3     [3:3]             ]
]]
    assert.same(exp, treeOf(']=]'):cd(0):ls())
  end)

  it("getLuaSBlock start", function()
    assert.same('[[', treeOf('[['):cd(0):last():getLuaSBlock())
    assert.same('[==[', treeOf('[==['):cd(0):last():getLuaSBlock())
    local t = treeOf('[==[ abc')
    local exp = [[
----    1  e  [2:3]             ==
d---    2  [  [4:8]             [ abc
]]
    assert.same(exp, t:cd(0, 1):ls())
    assert.same('[==[', t:cd(0, 1):getLuaSBlock())
  end)

  it("getLuaSBlock end", function()
    assert.same(']]', treeOf(']]'):cd(0):last():getLuaSBlock())
    -- D.enable() D.disable_modules(TP)
    assert.same(']==]', treeOf(']==]'):cd(0):last():getLuaSBlock())
    assert.same(']==]', treeOf('x ]==]'):cd(0):last():getLuaSBlock())
    assert.same(']==]', treeOf('ab]==]'):cd(0):last():getLuaSBlock())
    assert.same('[==[', treeOf('[==[ab]==]'):cd(0):last():getLuaSBlock())
    assert.same('[[', treeOf('[[ab]]'):cd(0):last():getLuaSBlock())
  end)

  it("getLuaSBlockPair", function()
    assert.same("]]", M.getLuaSBlockPair('[['))
    assert.is_nil(M.getLuaSBlockPair('['))
    assert.same(']=]', M.getLuaSBlockPair('[=['))
    assert.same(']==]', M.getLuaSBlockPair('[==['))
    assert.same(']=xxx=]', M.getLuaSBlockPair('[=xxx=['))
    assert.same('[==[', M.getLuaSBlockPair(']==]'))
    assert.same('[=[', M.getLuaSBlockPair(']=]'))
    assert.same('[[', M.getLuaSBlockPair(']]'))
  end)

  it("isOneChar", function()
    assert.same('x', treeOf('x'):cd(0):last():isOneChar())
    assert.same(nil, treeOf('xx'):cd(0):last():isOneChar())
    -- not empty container is not a one char - it represent range of its entire content
    assert.same(nil, treeOf('[['):cd(0):last():isOneChar())
    assert.same('[', treeOf('['):cd(0):last():isOneChar())   -- empty container passed
    assert.same(nil, treeOf('[ x'):cd(0):last():isOneChar()) -- x inside opened [
    -- each close-group-char saved as separate token
    assert.same(']', treeOf(']]'):cd(0):last():isOneChar())
    assert.same(2, treeOf(']]'):cd(0):branches())
  end)
  -- it("rangeLength", function()
  --   assert.same("", M.rangeLength())
  -- end)
end)

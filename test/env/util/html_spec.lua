-- 24-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.util.html'

describe("env.util.html", function()
  it("parse_css", function()
    local lines = {
      '.keyword-color {',
      '  color: red;',
      '}',
      '.rgb-color {',
      '  color: rgb(0, 0, 255);',
      '}',
      '.hex-color {',
      '  /* comment */',
      '  color: #63C20E;',
      '}'
    }
    local exp = {
      classes = {
        { 'keyword-color', { 'color: red;' } },
        { 'rgb-color',     { 'color: rgb(0, 0, 255);' } },
        { 'hex-color',     { 'color: #63C20E;' } }
      }
    }
    assert.same(exp, M.parse_css(lines))
  end)


  it("parse_css not supports inline defintion", function()
    local lines = {
      '.keyword-color {  color: red; }',
      '.rgb-color { color: rgb(0, 0, 255); }',
      '.hex-color { /* comment */ color: #63C20E; }'
    }
    local exp = { classes = {} }
    assert.same(exp, M.parse_css(lines))
  end)


  it("parse_css", function()
    local lines = {
      '.box-1 {',
      '  background-color: cornflowerblue;',
      '  position: static;',
      '}',
      '.box-2 {',
      '  background-color: lightpink;',
      '  position: relative;',
      '  top: 50px;',
      '  left: 100px;',
      '}'
    }
    local exp = {
      classes = {
        {
          'box-1',
          {
            'background-color: cornflowerblue;',
            'position: static;'
          }
        },
        {
          'box-2',
          {
            'background-color: lightpink;',
            'position: relative;',
            'top: 50px;',
            'left: 100px;'
          }
        }
      }
    }
    assert.same(exp, M.parse_css(lines, 0))
  end)


  it("parse_css with comment", function()
    local lines = {
      '/* div */',
      '.box-1 {',
      '  color: #63C20E;',
      '}'
    }
    local exp = {
      comment = 'div',
      classes = {
        { 'box-1', { 'color: #63C20E;' } }
      },
    }
    assert.same(exp, M.parse_css(lines))
  end)


  it("parse_kvpairs", function()
    local f = M.parse_kvpairs
    assert.same({ tag = 'div' }, f('tag:div'))

    local exp = { no = 'background-color', tag = 'div' }
    assert.same(exp, f('tag:div; no:background-color'))

    assert.same({}, f(''))
    assert.same({}, f(nil))

    assert.same({ lns = 1 }, f('lns:1'))
  end)
end)

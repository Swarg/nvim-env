--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.util.nvim_lsp")

describe("env.util.nvim_lsp", function()
  local token = {
    detail = "",
    kind = 13,
    name = "M.obj",
    range = {
      start = { character = 2, line = 2 },
      ["end"] = { character = 23, line = 2 },
    },
    selectionRange = {
      start = { character = 2, line = 2 },
      ["end"] = { character = 5, line = 2 },
    }
  }

  it("mk_node_name", function()
    assert.same('Variable   3:3     M.obj', M.mk_node_name(token))
    -- assert.same('', M.mk_node_name(token.range))
  end)

  it("kind2id", function()
    assert.same(0, M.kind2id(nil))
    assert.same(0, M.kind2id(''))
    assert.same(12, M.kind2id('function'))
    assert.same(28, M.kind2id('Fragment'))
  end)
end)

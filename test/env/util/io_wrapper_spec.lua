require 'busted.runner' ()

require('env.outnvim') -- to support calling this code outside nvim
local iow = require 'env.util.io_wrapper'


-- local describeX = function(...) end

describe('env.io_wrapper', function()
  it("io_restore-throw-error", function()
    -- errorness way to use restore before wrap -- throw error
    assert.error(function() iow.restore() end)
  end)

  it("io_wrapper-in-the-middle", function()
    local w = iow.wrap() -- wrap io-funcs and return wrapper state
    local file = io.open('/tmp/123', 'w')
    assert.same('/tmp/123', w.filename)
    assert.same('w', w.mode)
    assert.same('{ "userdata" }', vim.inspect(file))
    io.write('1a', '2b', '3c')
    io.close(file)
    iow.restore()
    assert.same('{ "1a 2b 3c" }', vim.inspect(w.output))
    assert.same('{ "userdata", "closed" }', vim.inspect(file))
    iow.clear_state()
  end)
end)

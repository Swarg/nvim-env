--
require 'busted.runner' ()
local assert = require('luassert')
local M = require 'env.util.os'

describe('env.util.os', function()
  it("os_run", function()
    assert.same("/usr/bin/echo\n", M.os_run('which echo'))
  end)

  it("getExecutable", function()
    assert.same("/usr/bin/echo", M.get_executable('echo'))
    assert.is_nil(M.get_executable('not-existed-in-system-command'))
  end)

  --[[
  it("get_ps_output", function()
    local output = M.get_ps_output()
    print(output)
  end)
]]


  it("parse_ps_output", function()
    local output = [[
    PID    PPID USER        VSZ  SIZE   RSS %CPU COMMAND
      1       0 root     166272 21152 10760  0.0 /sbin/init
      2       0 root          0     0     0  0.0 [kthreadd]
    320       1 root      64932 19468 28860  0.0 /lib/systemd/systemd-journald
    351       1 root      24340  3220  6416  0.0 /lib/systemd/systemd-udevd
   1659       1 user     383944 45272  8420  0.1 /usr/bin/ibus-daemon --daemonize --xim
  80318    2449 user     12408  5944  3536  0.0 bash
 302041  202709 user    2775224 290976 372404 1.8 /usr/lib/browser -contentproc -childID 180 -isForBrowser -p
 303189  250458 user       6952   448  3108  0.0 /usr/bin/xclip -quiet -i -selection clipboard
]]
    local exp = {
      [1] = {
        cmd = "/sbin/init",
        pcpu = 0,
        pid = 1,
        ppid = 0,
        vsz = 166272,
        size = 21152,
        rss = 10760,
        user = "root",
      },
      [2] = {
        cmd = "[kthreadd]",
        pcpu = 0,
        pid = 2,
        ppid = 0,
        vsz = 0,
        size = 0,
        rss = 0,
        user = "root",
      },
      [320] = {
        cmd = "/lib/systemd/systemd-journald",
        pcpu = 0,
        pid = 320,
        ppid = 1,
        vsz = 64932,
        size = 19468,
        rss = 28860,
        user = "root",
      },
      [351] = {
        cmd = "/lib/systemd/systemd-udevd",
        pcpu = 0,
        pid = 351,
        ppid = 1,
        vsz = 24340,
        size = 3220,
        rss = 6416,
        user = "root",
      },
      [1659] = {
        cmd = "/usr/bin/ibus-daemon --daemonize --xim",
        pcpu = 0.1,
        pid = 1659,
        ppid = 1,
        vsz = 383944,
        size = 45272,
        rss = 8420,
        user = "user",
      },
      [80318] = {
        cmd = 'bash',
        pcpu = 0,
        pid = 80318,
        ppid = 2449,
        vsz = 12408,
        rss = 3536,
        size = 5944,
        user = 'user',
      },
      [302041] = {
        cmd = "/usr/lib/browser -contentproc -childID 180 -isForBrowser -p",
        pcpu = 1.8,
        pid = 302041,
        ppid = 202709,
        vsz = 2775224,
        size = 290976,
        rss = 372404,
        user = "user",
      },
      [303189] = {
        cmd = "/usr/bin/xclip -quiet -i -selection clipboard",
        pcpu = 0,
        pid = 303189,
        ppid = 250458,
        vsz = 6952,
        size = 448,
        rss = 3108,
        user = "user",
      }
    }
    local res = M.parse_ps_output(output)
    assert.same(exp, res)
  end)

  it("ps_state2str", function()
    local t = {
      cmd = 'bash',
      pcpu = 0,
      pid = 80318,
      ppid = 2449,
      vsz = 12408,
      rss = 3536,
      size = 5944,
      user = 'user',
    }
    --               rss   size   vsz     pid     ppid     user   cpu cmd
    local exp = '    4M     6M    13M    80318     2449     user     0 bash'
    assert.same(exp, M.ps_state2str(t))
    assert.same('not found for pid: 123', M.ps_state2str(nil, 123))
    assert.same('nil', M.ps_state2str(nil))
  end)

  it("kb2readable", function()
    assert.same('16K', M.kb2readable(16))
    assert.same('11M', M.kb2readable(10.46 * 1024))
    assert.same('1.50G', M.kb2readable(1024 * 1024 * 1.5))
  end)

  it("human2kb", function()
    local f = M.str_size2num
    assert.same(16, f('16'))
    assert.same(16384, f('16K'))
    assert.same(11534336, f('11M'))
    assert.same(1610612736, f('1.50G'))
  end)

  it("build_tree", function()
    local flat = {
      { pid = 1, ppid = 0, cmd = "init", pcpu = 0, size = 1, user = "root" },
      -- gap for trigger inspect show indexes
      { pid = 3, ppid = 1, cmd = "c3",   pcpu = 0, size = 1, user = "u" },
      { pid = 4, ppid = 1, cmd = "c4",   pcpu = 0, size = 1, user = "u" },
      { pid = 5, ppid = 4, cmd = "c5",   pcpu = 0, size = 1, user = "u" },
    }
    local res = M.build_tree(flat)

    local exp = {
      [1] = {
        { pid = 1, ppid = 0, cmd = "init", pcpu = 0, size = 1, user = "root" },
        childs = {
          [3] = {
            { pid = 3, ppid = 1, cmd = "c3", pcpu = 0, size = 1, user = "u" },
          },
          [4] = {
            { pid = 4, ppid = 1, cmd = "c4", pcpu = 0, size = 1, user = "u" },
            childs = {
              [5] = {
                { pid = 5, ppid = 4, cmd = "c5", pcpu = 0, size = 1, user = "u" },
              }
            }
          }
        }
      },
    }
    assert.same(exp, res)
  end)

  it("get_childs_mem", function()
    local tree = {
      [1] = {
        { pid = 1, ppid = 0, cmd = "init", pcpu = 0, size = 11, user = "root" },
        childs = {
          [3] = {
            { pid = 3, ppid = 1, cmd = "c3", pcpu = 0, size = 22, user = "u" },
          },
          [4] = {
            { pid = 4, ppid = 1, cmd = "c4", pcpu = 0, size = 33, user = "u" },
            childs = {
              [5] = {
                { pid = 5, ppid = 4, cmd = "c5", pcpu = 0, size = 44, user = "u" },
              }
            }
          }
        }
      },
    }
    local f = function(tree0)
      local sz, c = M.get_childs_mem(tree0)
      return tostring(sz) .. '|' .. tostring(c)
    end
    assert.same('99|3', f(tree[1].childs))
    assert.same('0|0', f(tree[1].childs[3]))
    assert.same('0|0', f(tree[1].childs[4]))
    assert.same('44|1', f(tree[1].childs[4].childs))
  end)


  it("build_child_list", function()
    local flat = {
      { pid = 1, ppid = 0, cmd = "init", pcpu = 0, size = 1, user = "root" },
      -- gap for trigger inspect show indexes
      { pid = 3, ppid = 1, cmd = "c3",   pcpu = 0, size = 1, user = "u" },
      { pid = 4, ppid = 1, cmd = "c4",   pcpu = 0, size = 1, user = "u" },
      { pid = 5, ppid = 4, cmd = "c5",   pcpu = 0, size = 1, user = "u" },
    }
    local exp = {
      { pid = 3, ppid = 1, cmd = "c3", pcpu = 0, size = 1, user = "u" },
      { pid = 4, ppid = 1, cmd = "c4", pcpu = 0, size = 1, user = "u" },
    }
    assert.same(exp, M.build_child_list(flat, 1, false))

    local exp2 = {
      [3] = {
        { cmd = "c3", pcpu = 0, pid = 3, ppid = 1, size = 1, user = "u" },
        childs = nil,
      },
      [4] = {
        { cmd = "c4", pcpu = 0, pid = 4, ppid = 1, size = 1, user = "u" },
        childs = {
          [5] = {
            { cmd = "c5", pcpu = 0, pid = 5, ppid = 4, size = 1, user = "u" },
            childs = nil
          }
        }
      }
    }
    local res = M.build_child_list(flat, 1, true)
    -- print("[DEBUG] res:", require "inspect" (res))
    assert.same(exp2, res)
  end)

  it("mk_node_name", function()
    local node = {
      { cmd = "c3", pcpu = 0, pid = 3, ppid = 1, size = 1, user = "u" },
      childs = nil,
    }
    assert.same('     0    1K  c3', M.mk_node_name(node, 4))
    assert.same(' 0    1K  c3', M.mk_node_name(node, 0))
    assert.same('  0    1K  c3', M.mk_node_name(node, 1))
  end)
end)

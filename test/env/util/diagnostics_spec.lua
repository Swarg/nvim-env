--
require 'busted.runner' ()
local assert = require('luassert')
local M = require 'env.util.diagnostics'

describe('env.util.diagnostics', function()
  it("tbl_indexof", function()
    local el1 = { ns = 1 }
    local el4 = { ns = 4 }
    local list = { el1, { ns = 2 }, { ns = 3 }, el4 }
    assert.same(1, M.tbl_indexof(list, el1))
    assert.same(4, M.tbl_indexof(list, el4))
  end)

  it("filter_by_namespace", function()
    local e1 = { namespace = 1, i = 1 }
    local e2 = { namespace = 2, i = 2 }
    local e3 = { namespace = 1, i = 3 }
    local e4 = { namespace = 2, i = 4 }
    local e5 = { namespace = 1, i = 5 }
    local da = { e1, e2, e3, e4, e5 }
    assert.same({ e2, e4 }, M.filter_by_namespace(da, 2))
    assert.same({ e2, e4 }, M.filter_by_namespace(da, 2, {}))
    assert.same({ e2, e4 }, M.filter_by_namespace(da, 2, { e1 }))
    assert.same({ e4 }, M.filter_by_namespace(da, 2, { e2 }))
    assert.same({ e2 }, M.filter_by_namespace(da, 2, { e4 }))

    assert.same({ e3 }, M.filter_by_namespace(da, 1, { e1, e5 }))
    assert.same({ e1, e3, e5 }, M.filter_by_namespace(da, 1, {}))
  end)

  -- Note: in vim linenr and columns starts from 0 in diagnostics from 1
  it("filter_by_position", function()
    local e1 = { bufnr = 16, namespace = 1, lnum = 9, col = 20, end_col = 22 }
    local e2 = { bufnr = 16, namespace = 1, lnum = 9, col = 26, end_col = 29 }
    local e3 = { bufnr = 16, namespace = 1, lnum = 17, col = 20, end_col = 22 }
    local da = { e1, e2, e3 }
    assert.is_nil(M.filter_by_position(da, 16, 10, 16))
    assert.same({ e1, e2 }, M.filter_by_position(da, 16, 10)) -- 9 maps to 10
    assert.same({ e1 }, M.filter_by_position(da, 16, 10, 21))
    assert.is_nil(M.filter_by_position(da, 16, 10, 24))
    --
    -- ignore column in line
    assert.same({ e1, e2 }, M.filter_by_position(da, 16, 10, 0)) -- 0 --> nil
    assert.same({ e1, e2 }, M.filter_by_position(da, 16, 10))
  end)

  it("split_by_namespace no_exclude", function()
    local e1 = { bufnr = 16, namespace = 2, lnum = 10, col = 20, end_col = 22 }
    local e2 = { bufnr = 16, namespace = 2, lnum = 10, col = 26, end_col = 29 }
    local e3 = { bufnr = 16, namespace = 4, lnum = 18, col = 20, end_col = 22 }
    local da = { e1, e2, e3 }
    local exp = { [2] = { e1, e2 }, [4] = { e3 } }
    assert.same(exp, M.split_by_namespace(da))

    exp = { [2] = { e1, e2 }, [4] = { e3 } }
    assert.same(exp, M.split_by_namespace(da, { 2, 4 }))

    exp = { [2] = { e1, e2 }, [4] = {} }
    assert.same(exp, M.split_by_namespace(da, { 2 }))

    exp = { [2] = {}, [4] = { e3 } }
    assert.same(exp, M.split_by_namespace(da, { 4 }))

    exp = { [2] = {}, [4] = {} }
    assert.same(exp, M.split_by_namespace(da, { 1 }))
    assert.same(exp, M.split_by_namespace(da, { 1, 8, 9 }))
    assert.same({}, M.split_by_namespace({}, { 1, 8, 9 }))
  end)

  it("split_by_namespace with exclude", function()
    local e1 = { bufnr = 16, namespace = 2, lnum = 10 }
    local e2 = { bufnr = 16, namespace = 2, lnum = 24 }
    local e3 = { bufnr = 16, namespace = 4, lnum = 18 }
    local e4 = { bufnr = 16, namespace = 4, lnum = 24 }
    local da = { e1, e2, e3, e4 }
    local ns_filter = false

    local exp = { [2] = { e1, e2 }, [4] = { e3, e4 } }
    -- no excludes
    assert.same(exp, M.split_by_namespace(da, ns_filter, {}))

    -- with excludes e4, e2
    exp = { [2] = { e1 }, [4] = { e3 } }
    assert.same(exp, M.split_by_namespace(da, ns_filter, { e4, e2 }))
    exp = { [2] = { e1 }, [4] = {} }
    assert.same(exp, M.split_by_namespace(da, ns_filter, { e4, e2, e3 }))
  end)
end)

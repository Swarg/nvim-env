-- 19-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.bats");

--[==[
bats -f '^shouting$' test-bob.bats

@test 'shouting' {
    [[ $BATS_RUN_SKIPPED == "true" ]] || skip

    run jq -r -f bob.jq << 'END_INPUT'
        {
          "heyBob": "WATCH OUT!"
        }
END_INPUT

    assert_success
    expected='Whoa, chill out!'
    assert_equal "$output" "$expected"
}
]==]

describe("env.util.bats", function()
  it("get_cmd_to_run_single_test", function()
    local f = M.get_cmd_to_run_single_test

    local exp = {
      cmd = 'bats',
      args = { '-f', '^test-name$', 'file' },
      envs = { 'BATS_RUN_SKIPPED=true' }
    }
    assert.same(exp, f('file', "@test 'test-name' {"))
    assert.same(exp, f('file', '@test "test-name" {'))
    assert.same(exp, f('file', ' @test  "test-name"  {'))
    assert.same(exp, f('file', ' @test  "test-name"'))
  end)
end)

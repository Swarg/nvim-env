-- 02-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.unix_tree_cmd");

local tree0 = [[
/mnt/registry/
├── docker
│   └── registry
│       └── v2
│           ├── blobs
│           │   └── sha256
│           │       ├── 34
│           │       │   └── 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0
│           │       │       └── data
│           │       ├── b5
│           │       │   └── b541f2080109ab7b6bf2c06b28184fb750cdd17836c809211127717f48809858
│           │       │       └── data
│           │       └── d6
│           │           └── d695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389
│           │               └── data
│           └── repositories
│               └── library
│                   └── alpine
│                       ├── _layers
│                       │   └── sha256
│                       │       └── b541f2080109ab7b6bf2c06b28184fb750cdd17836c809211127717f48809858
│                       │           └── link
│                       ├── _manifests
│                       │   ├── revisions
│                       │   │   └── sha256
│                       │   │       ├── 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0
│                       │   │       │   └── link
│                       │   │       └── d695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389
│                       │   │           └── link
│                       │   └── tags
│                       │       └── 3.18.5
│                       │           ├── current
│                       │           │   └── link
│                       │           └── index
│                       │               └── sha256
│                       │                   └── 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0
│                       │                       └── link
│                       └── _uploads
└── scheduler-state.json
]]

local compact_tree0 = {
  '/mnt/registry/',
  '├ docker',
  '│ └ registry',
  '│   └ v2',
  '│     ├ blobs',
  '│     │ └ sha256',
  '│     │   ├ 34',
  '│     │   │ └ 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0',
  '│     │   │   └ data', -- 9
  '│     │   ├ b5',
  '│     │   │ └ b541f2080109ab7b6bf2c06b28184fb750cdd17836c809211127717f48809858',
  '│     │   │   └ data', -- 12
  '│     │   └ d6',
  '│     │     └ d695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389',
  '│     │       └ data', -- 15
  '│     └ repositories',
  '│       └ library',
  '│         └ alpine',
  '│           ├ _layers',
  '│           │ └ sha256',
  '│           │   └ b541f2080109ab7b6bf2c06b28184fb750cdd17836c809211127717f48809858',
  '│           │     └ link', -- 22
  '│           ├ _manifests',
  '│           │ ├ revisions',
  '│           │ │ └ sha256',
  '│           │ │   ├ 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0',
  '│           │ │   │ └ link', -- 27
  '│           │ │   └ d695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389',
  '│           │ │     └ link', -- 29
  '│           │ └ tags',
  '│           │   └ 3.18.5',
  '│           │     ├ current',
  '│           │     │ └ link', -- 33
  '│           │     └ index',
  '│           │       └ sha256',
  '│           │         └ 34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0',
  '│           │           └ link', -- 37
  '│           └ _uploads',
  '└ scheduler-state.json'
}

describe("env.util.tree_parser", function()
  it("parse_output", function()
    assert.same(compact_tree0, M.parse_output(tree0))
  end)

  it("build_path", function()
    local lines = {
      'registry',
      '└ v2',
      '  ├ blobs',
      '  │ └ sha256',
      '  │   ├ 34',
      '  │   │ └ 34871e729050',
      '  │   │   └ data',
    }
    local exp = 'registry/v2/blobs/sha256/34/34871e729050/data'
    assert.same(exp, M.build_path(lines, #lines))
  end)

  it("build_path 2", function()
    local exp = '/mnt/registry/docker/registry/v2/blobs/sha256/34/' ..
        '34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0/data'
    assert.same(exp, M.build_path(compact_tree0, 9))

    local exp2 = '/mnt/registry/docker/registry/v2/blobs/sha256/b5/' ..
        'b541f2080109ab7b6bf2c06b28184fb750cdd17836c809211127717f48809858/data'
    assert.same(exp2, M.build_path(compact_tree0, 12))

    local exp3 = '/mnt/registry/docker/registry/v2/repositories/library/alpine/'
        .. '_manifests/revisions/sha256/' ..
        '34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0/link'
    assert.same(exp3, M.build_path(compact_tree0, 27))

    local exp4 = '/mnt/registry/docker/registry/v2/repositories/library/alpine/'
        .. '_manifests/revisions/sha256/' ..
        'd695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389/link'
    assert.same(exp4, M.build_path(compact_tree0, 29))

    local exp5 = '/mnt/registry/docker/registry/v2/repositories/library/alpine/'
        .. '_manifests/tags/3.18.5/index/sha256/' ..
        '34871e7290500828b39e22294660bee86d966bc0017544e848dd9a255cdf59e0/link'
    assert.same(exp5, M.build_path(compact_tree0, 37))
  end)

  it("build_path", function()
    local lines = {
      '/mnt/registry/',
      '├ docker',
      '│ └ registry',
      '│   └ v2'
    }
    assert.same('/mnt/registry/docker/registry/v2', M.build_path(lines, 4))
  end)

  it("build_path comments", function()
    local lines = {
      '/mnt/registry/',
      '├ docker         # comment 1',
      '│ └ registry     # comment 2',
      '│   └ v2'
    }
    assert.same('/mnt/registry/docker/registry/v2', M.build_path(lines, 4))
  end)


  it("build_path original", function()
    local lines = {
      '/mnt/registry/',
      '├── docker',
      '│   └── registry',
      '│       └── v2'
    }
    assert.same('/mnt/registry/docker/registry/v2', M.build_path(lines, 4))
  end)

  it("build_path original + comments", function()
    local lines = {
      '/mnt/registry/',
      '├── docker           # comment 1',
      '│   └── registry',
      '│       └── v2       # comment 2'
    }
    assert.same('/mnt/registry/docker/registry/v2', M.build_path(lines, 4))
  end)

  it("index_of_node_name", function()
    local f = function(line)
      local s
      local sp, ep = M.pos_of_node_name(line)
      if sp and ep then s = string.sub(line, sp, ep) end
      return tostring(sp) .. '|' .. tostring(ep) .. '|' .. tostring(s)
    end

    assert.same('11|19|node name', f('├── node name'))
    assert.same('11|19|node name', f('├── node name  # comment'))
    assert.same('5|13|node name', f('├ node name  # comment'))
    assert.same('9|17|node name', f(' ├─ node name  # comment'))
    assert.same('12|20|node name', f(' └── node name  # comment'))
    assert.same('13|13|x', f('  └── x # comment'))
    assert.same('15|23|# comment', f('  └──   # comment')) -- ?? todo fix?
    assert.same('nil|16|nil', f('  └──     '))
    assert.same('11|18|Makefile', f('├── Makefile'))
    assert.same('19|34|digraph_spec.lua', f('│   └── digraph_spec.lua'))
  end)

  it("has_tree_nodes", function()
    local f = M.has_tree_nodes
    assert.same(false, f(nil))
    assert.same(false, f(''))
    assert.same(true, f('─'))
    assert.same(true, f('─ '))
    assert.same(true, f(' ─'))
    assert.same(true, f('  ─'))
    assert.same(true, f('  ─ '))
    assert.same(true, f('   ─ '))
    assert.same(true, f('│'))
    assert.same(true, f('├'))
    assert.same(true, f('└'))
    assert.same(true, f(' │     │   │ └ b5419211127717f48809858  # (1)ImgID'))
  end)
end)

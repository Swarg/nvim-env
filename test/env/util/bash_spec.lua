-- 11-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.bash");

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local bu = require('env.bufutil')


describe("env.util.bash", function()
  before_each(function()
    nvim:clear_state()
  end)

  it("get_funcname", function()
    assert.same('abc', M.get_funcname('function abc(){'))
  end)

  it("get_funcname", function()
    assert.same('a b', string.match("'a b'", '^[\'"](.*)[\'"]$'))
    assert.same('a b', string.match('"a b"', '^[\'"](.*)[\'"]$'))
    assert.is_nil(string.match('a b', '^[\'"](.*)[\'"]$'))
  end)


  it("is_dollar_first", function()
    local f = M.is_dollar_first
    local line = '$ git clone https://git...'
    assert.same('$ git clone https://git...', f(line))
  end)

  it("get_bash_command_one_line", function()
    local f = M.get_bash_command_one_line
    local line = '$ git clone --depth 1 https://git...'
    local exp = {
      cmd = 'git',
      args = { 'clone', '--depth', '1', 'https://git...' }
    }
    assert.same(exp, f(line))
  end)

  it("get_bash_command_one_line 2", function()
    local f = M.get_bash_command_one_line
    local line = '$ git status'
    local exp = { cmd = 'git', args = { 'status' } }
    assert.same(exp, f(line))
  end)

  it("get_bash_command_one_line fail", function()
    local f = M.get_bash_command_one_line
    local line = '$ git status \\'
    local exp = { err = 'multiline commands are not supported yet' }
    assert.same(exp, f(line))
  end)

  it("test-cond regex", function()
    local line = [[if [ "$1" == "make-sections" -o "$1" == "ssn" ]; then]]
    local res = line:match('%s*e?l?if%s+%[(.*)%];?%s*then%s*$')
    assert.same(' "$1" == "make-sections" -o "$1" == "ssn" ', res)
  end)

  it("parse_test_cond", function()
    local f = M.parse_cmd_route_via_test_cond
    local line = [[if [ "$1" == "cmd" -o "$1" == "sn" ]; then]]
    assert.same({ 'cmd', 'sn' }, f(line))
  end)

  it("parse_test_cond one", function()
    local f = M.parse_cmd_route_via_test_cond
    assert.same({ 'cmd' }, f('if [ "$2" == "cmd" ]'))
    assert.same({ 'cmd' }, f('elif [ "$2" == "cmd" ]'))
    assert.same({ 'cmd' }, f(' if    [ "$2" == "cmd" ]'))
    assert.same({ 'cmd' }, f(' elif  [ "$2" == "cmd" ]'))

    assert.is_nil(f('if x[ "$2" == "cmd" ]'))
    assert.is_nil(f('if [ "$x" == "cmd" ]'))
    assert.same({ 'cmd' }, f('if [ "$0" == "cmd" ]'))

    assert.is_nil(f('if x[ "$2" != "cmd" ]'))
    assert.is_nil(f('if x[ "$2" = "cmd" ]'))
    assert.is_nil(f('if x[ "$2" != "cmd" -o "$2" == "cmd" ]'))

    assert.same({ 'cmd', 'x' }, f('if [ "$1" == "cmd" -o "$1" == "x" ]; then'))
    assert.same({ 'cmd', 'x' }, f('if [ "$1" == "cmd" -o "$1" == "x" ]'))
    assert.same({ 'cmd' }, f('if [ "$1" == "cmd" ]; then'))
    assert.same({ 'cmd' }, f('if [ "$2" == "cmd" ]; then'))
    assert.same({ 'cmd' }, f('if [ "$1" == \'cmd\' ]; then'))

    -- only or-variants
    assert.is_nil(f('if [ "$1" == "cmd" -a "$1" == "x" ]'))
  end)

  it("get_bash_command 3", function()
    local f = M.get_bash_command_one_line
    local line = '$ date'
    local exp = { cmd = 'date', args = {} }
    assert.same(exp, f(line))
  end)

  it("pick_annotations_fr_docblock", function()
    local ln, lines = 4, {
      '#',
      '# envs: KEY=VALUE',
      "# args: a 'b c' 'd e'",
      'function callme() {',
      '}',
    }
    local bn = nvim:new_buf(lines, ln)
    assert.same('function callme() {', vim.api.nvim_get_current_line())
    assert.same('callme', M.get_funcname(vim.api.nvim_get_current_line()))
    local exp = { fenvs = { 'KEY=VALUE' }, fargs = { 'a', 'b c', 'd e' } }
    assert.same(exp, M.pick_annotations_fr_docblock(bn, ln, {}))
  end)

  it("pick_annotations_fr_docblock", function()
    local ln, lines = 3, {
      "",
      "# args: 2024",
      "  function foo() {",
      ""
    }
    local bn = nvim:new_buf(lines, ln)
    assert.same('  function foo() {', vim.api.nvim_get_current_line())
    assert.same('foo', M.get_funcname(vim.api.nvim_get_current_line()))
    local exp = { fargs = { '2024' } }
    assert.same(exp, M.pick_annotations_fr_docblock(bn, ln, {}))
  end)

  it("get_command_from_cmd_route_cond", function()
    local ln, lines = 4, {
      '', -- 1
      '', -- '# envs: KEY=VALUE', -- not support yet
      '# args: "ab c" d',
      'elif [ "$1" == "mk-sect" -o "$1" == "ssn" ]; then',
      '  shift; mk_sections "$@"'
    }
    nvim:new_buf(lines, ln, 1, 1, '/home/u/my.sh')
    local cbi = bu.current_buf_info()
    local res = {}
    assert.same(true, M.get_command_from_cmd_route_cond(cbi, res))
    local exp = { cmd = '/home/u/my.sh', args = { 'mk-sect', '"ab c"', 'd' } }
    assert.same(exp, res)
  end)

  it("is_pipe_cmd", function()
    local f = M.is_piped_cmd
    assert.same(true, f({ '{}', '|', 'jq' }))
    assert.same(true, f({ '{}', '&&', 'echo', 'abc' }))
    assert.same(true, f({ '{}', '(', 'jq' }))
    assert.same(false, f({ '{}', '', 'jq' }))
  end)

  it("parse_line", function()
    local f = function(line)
      local cmd, args = M.parse_line(line)
      return tostring(cmd) .. '^' .. table.concat(args, ' ')
    end
    local cmd, args = M.parse_line('echo {} | jq')
    assert.same("bash", cmd)
    assert.same({ '-c', 'echo {} | jq' }, args)
    assert.same('bash^-c echo {} | jq', f('echo {} | jq'))
    assert.same('bash^-c echo 123 && echo abc', f('echo 123 && echo abc'))
    assert.same('echo^123 "&&" echo abc', f('echo 123 "&&" echo abc'))
  end)

  --[[
#!/bin/bash

# envs: KEY="VALUE YES!"
# args: '2020 x' second
function callme() {
  echo "|$1|"
  echo "|$2|"
  echo "|$3|"
  echo $KEY
  bind_projects_form_last_years "$1" "$2"
}
output:
~/.dotfiles$ /bin/bash -c
  "
    source ~/.dotfiles/tools/tools/dev-tools
    &&
    export KEY=\"VALUE YES!\"
    &&
    call_LOWEST_YEAR \"2020 x\"
  "
|2020 x|
|second|
VALUE YES!
]]
end)

-- 27-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require "env.util.http.socket_transport"

describe("env.util.http.socket_transport", function()
  -- manyally
  it("send", function()
    if 1 == 1 then return end -- off

    local request = {
      host = 'api.agify.io',
      uri = '/?name=env'
    }
    require 'alogger'.fast_setup(nil, 1)
    -- local exp = '{"count":11,"name":"env","age":54}'
    -- assert.same(exp, M.send(request, { verbose = true }))

    local exp = { age = 54, count = 11, name = 'env' }
    assert.same(exp, M.send(request, { verbose = true, is_json = true }))
  end)

  it("decode", function()
    assert.same('{"key": 42}', M.decode('{"key": 42}'))
  end)
end)

-- 03-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.util.convert");

describe("env.command.convert", function()
  it("decimal_to_hex", function()
    local f = M.decimal_to_hex
    assert.same('1', f(1))
    assert.same('F', f(15))
    assert.same('80', f(128))
    assert.same('FF', f(255))
    assert.same('F000000A', f(4026531850))
    assert.same('FFFFFFFF', f(4294967295))
  end)

  it("hex_to_decimal", function()
    local f = M.hex_to_decimal
    assert.is_nil(f('x'))
    assert.same(0, f('0'))
    assert.same(255, f('FF'))
    assert.same(4294967295, f('FFFFFFFF'))
    assert.same(10, f('0A'))
    assert.same(4026531850, f('F000000A'))
  end)

  it("srt2numarr", function()
    local f = M.srt2numarr
    assert.same({ 1 }, f('{1}'))
    assert.same({ 1, 170 }, f('{1, 0xAA}'))
    assert.same({ 1, 2, 3, 4026531850 }, f('1,2,3,4026531850'))
  end)

  it("hex_encode", function()
    local f = M.hex_encode
    assert.same('736563726574', f("secret"))
  end)

  it("hex_decode", function()
    local f = M.hex_decode
    assert.same('secret', f("736563726574"))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')
local M = require 'env.lua'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

describe('env.lua', function()
  after_each(function()
    nvim:clear_state()
  end)

  local function mk_ctx(project_root, src, bufname)
    return { project_root = project_root, src = src, bufname = bufname }
  end

  it("is_src_path", function()
    assert.same(true, M.is_src_path(mk_ctx('/pr/', 'src/', '/pr/src/f.lua')))
    assert.same(false, M.is_src_path(mk_ctx('/pr/', 'src/', '/a/pr/src/f.lua')))
  end)

  it("is_in_project", function()
    assert.same(true, M.is_in_project(mk_ctx('/pr/', 'src/', '/pr/src/f.lua')))
    assert.same(true, M.is_in_project(mk_ctx('/pr/', 'src/', '/pr/test/f.lua')))
    assert.same(false, M.is_in_project(mk_ctx('/pr/', 'src/', '/d/test/f.lua')))
  end)

  -- EnvGotoSource
  describe("find_path_to_source_for", function()
    local lines = {
      "local ClassGen = require('env.lang.oop.ClassGen')", --1
      "  local cg = ClassGen:new({",                       --2
      "    classinfo = ci,",                               --3
      "    })",                                            --4
      "  assert.same(exp, cg:toArray())",                  --5
      "  assert.same('x', cg:genClassBody())",             --6 case:1
      "  assert.same('x', ClassGen:staticMethod())",       --7 case:2
      --23456789012345678901234567890
      --        10        20        30
    }
    local path, linenr = nil, nil
    local callback = function(p, ln)
      path = p; linenr = ln
      return p ~= nil and ln ~= nil
    end

    local ctx = {
      project_root = '/tmp/', -- dir must exists
      bufnr = 1,
      bufname = '/tmp/lua/source.lua'
    }

    after_each(function() path, linenr = nil, nil end)

--[[ todo move to LuaLang:gotoDefinition
    it("find_path_to_source_for trace in debug", function()
      local path0 = "lua/env/lang/ClassResolver.lua"
      local line = "[2023-10-10 10:10:10] [DEBUG] @./" .. path0 .. ":45: Cannot resolve class"
      assert.is_true(M.find_path_to_source_for(ctx, line, callback))
      assert.same(45, linenr)
      assert.is_not_nil(path)
      ---@cast path string
      assert.same(uv.cwd() .. '/' .. path0, path)
    end)

    it("find_path_to_source_for trace", function()
      local path0 = "test/env/lua_spec.lua"
      local line = "./" .. path0 .. ":45: Expected objects to be the same."
      assert.is_true(M.find_path_to_source_for(ctx, line, callback))
      assert.same(45, linenr)
      assert.is_not_nil(path)
      ---@cast path string
      assert.same(uv.cwd() .. '/' .. path0, path)
    end)

    -- aka jump via import
    it("parse_line_require", function()
      local line = "local ClassGen = require('oop.Class')"
      nvim:new_buf({ line }, 0, 30) -- cursor under the method 'genClassBody'
      assert.same(true, M.find_module_by_require(ctx, line, callback))
      assert.same('/tmp/oop/Class.lua', path)
      path = nil
      local res, n = M.find_path_to_source_for(ctx, line, callback)
      assert.is_true(res)
      assert.same(3, n)
      assert.same('/tmp/oop/Class.lua', path)
      assert.same(1, linenr)
      -- finally(function() TH.logger_off() end)
    end)


    it("find_source_of_words case-1 from Dynamic Method of Class", function()
      nvim:new_buf(lines, 6, 30) -- cursor under the method 'genClassBody'

      -- via callback results passed into path and linenr
      assert.is_true(M.find_path_to_source_for(ctx, lines[6], callback))
      assert.same("/tmp/env/lang/oop/ClassGen.lua", path)
      assert.same("ClassGen:genClassBody(", linenr)
    end)

    it("find_source_of_words case-2 from Static Method of Class", function()
      -- via callback results passed into path and linenr
      nvim:new_buf(lines, 7, 30)

      assert.is_true(M.find_path_to_source_for(ctx, lines[7], callback))
      assert.same("/tmp/env/lang/oop/ClassGen.lua", path)
      assert.same("ClassGen:staticMethod(", linenr)
    end)
    ]]

    describe("find_source_of_words", function()
      ctx = mk_ctx('/pr/', 'src', '/pr/src/file.lua')
      ctx.current_line = lines[5]
      ctx.word_element = "genConstructorCode"
      ctx.word_container = "MyClass"

      it("before", function()
        local curr_line_nr = 5
        lines = {
          "function MyClass:genConstructorCode(m)", -- 1 < destination
          "  --body",                               -- 2
          "end",                                    -- 3
          "function MyClass:toCode(m)",             -- 4
          "  return MyClass:genConstructorCode(m)", -- 5  <<< current_line
          "end",                                    -- 6
        }
        nvim:new_buf(lines, curr_line_nr, 30)
        ctx.current_line = lines[curr_line_nr]
        ctx.bufnr = 1
        local p, lnr = M.find_source_of_words(ctx)
        assert.same("/pr/src/file.lua", p)
        assert.same(1 - 1, lnr)
      end)

      it("after", function()
        local curr_line_nr = 2
        lines = {
          "function MyClass:toCode(m)",             -- 1
          "  return MyClass:genConstructorCode(m)", -- 2  <<< current_line
          "end",                                    -- 3
          "function MyClass:genConstructorCode(m)", -- 4  < destination
          "  --body",                               -- 5
          "end",                                    -- 6
        }
        nvim:new_buf(lines, curr_line_nr, 30)
        ctx.current_line = lines[curr_line_nr]

        local p, lnr = M.find_source_of_words(ctx)
        assert.same("/pr/src/file.lua", p)
        assert.same(4 - 1, lnr)
      end)

      it("already", function()
        local curr_line_nr = 4
        lines = {
          "function MyClass:toCode(m)",             -- 1
          "  return MyClass:genConstructorCode(m)", -- 2
          "end",                                    -- 3
          "function MyClass:genConstructorCode(m)", -- 4  <- curline & dst
          "  --body",                               -- 5
          "end",                                    -- 6
        }
        nvim:new_buf(lines, curr_line_nr, 30)
        ctx.current_line = lines[curr_line_nr]

        local path0, lnr = M.find_source_of_words(ctx)
        assert.is_nil(path0) -- not ret - already in the destination
        assert.is_nil(lnr)
      end)

      it("find_source_by_tracemsg", function()
        local res_path, res_lnum
        local cb = function(path0, lnum)
          res_path = path0
          res_lnum = lnum
          return true
        end
        local line = "Error -> ./test/env/util/cmd4lua/parse_line.lua @ 63"
        assert.same(true, M.find_source_by_tracemsg(line, cb))
        local exp = uv.cwd() .. '/test/env/util/cmd4lua/parse_line.lua'
        assert.same(exp, res_path)
        assert.same(63, res_lnum)
      end)

      it("find_source_by_tracemsg from exunit", function()
        local line = "     examples/example.exs:55"
        local res, resn = nil, nil
        local cb = function(s, n) res, resn = s, n end

        M.find_source_by_tracemsg(line, cb)
        local exp = uv.cwd() .. '/examples/example.exs'
        assert.same(exp, res)
        assert.same(55, resn)

        line = "  └─ examples_b/example_b.exs:19:8: ModuleName (module)"
        M.find_source_by_tracemsg(line, cb)
        assert.same(uv.cwd() .. '/examples_b/example_b.exs', res)
        assert.same(19, resn)
      end)
    end)
  end)
end)

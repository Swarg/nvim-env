-- 16-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local FileWriter = require 'env.lang.FileWriter'

local OK_SAVED = FileWriter.OK_SAVED

describe("env.lang.FileWriter", function()
  it("getFullPathForInnerPath", function()
    local dir = '/tmp/dev/project/'
    local fw = FileWriter:new(nil, dir)
    local exp = '/tmp/dev/project/inner.ext'
    assert.same(exp, fw:getFullPathForInnerPath('inner', 'ext'))

    local exp2 = '/tmp/dev/project/fullpath.md'
    assert.same(exp2, fw:getFullPathForInnerPath(dir .. 'fullpath.md'))

    local exp3 = '/tmp/dev/project/fullpath'
    assert.same(exp3, fw:getFullPathForInnerPath(dir .. 'fullpath', 'md'))

    -- it is considered that this is not a full path from the root,
    -- but a relative one from the project
    local exp4 = '/tmp/dev/project//etc/ssh'
    assert.same(exp4, fw:getFullPathForInnerPath(dir .. '/etc/ssh'))
  end)


  it("getReadableSavedState", function()
    local fw = FileWriter:new(nil, '/tmp/dev')
    fw:addEntry('/tmp/dev/pr0j/buildscript', OK_SAVED)
    fw:addEntry('/tmp/dev/pr0j/file2', OK_SAVED)
    local exp = [[
ok: /tmp/dev/pr0j/buildscript
ok: /tmp/dev/pr0j/file2
]]
    assert.same(exp, fw:getReadableReport())
    local exp2 = [[
Prefix for Files:
ok: /tmp/dev/pr0j/buildscript
ok: /tmp/dev/pr0j/file2
]]
    assert.same(exp2, fw:getReadableReport("Prefix for Files:"))
  end)

  it("getReadableSavedState one", function()
    local fw = FileWriter:new(nil, '/tmp/dev')
    fw:addEntry('/tmp/dev/pr0j/buildscript', OK_SAVED)
    local exp = 'ok: /tmp/dev/pr0j/buildscript'
    assert.same(exp, fw:getReadableReport())
    local exp2 = "Prefix for Files:\nok: /tmp/dev/pr0j/buildscript\n"
    assert.same(exp2, fw:getReadableReport("Prefix for Files:"))
  end)

  it("getReadableReport", function()
    local fw = FileWriter:new(nil, '/tmp/dev')
    local details = 'something went wrong'
    fw:addEntry('/tmp/dev/file', FileWriter.NO_CONTENT, details)
    local exp_entries = {
      { '/tmp/dev/file', FileWriter.NO_CONTENT, 'something went wrong' }
    }
    assert.same(exp_entries, fw.entries)
    local exp2 = [[
Prefix:
no content: /tmp/dev/file : something went wrong
]]
    assert.same(exp2, fw:getReadableReport("Prefix:"))
  end)

  it("getLastEntryCodeAndPath", function()
    local fw = FileWriter:new(nil, '/tmp/dev')
    fw:addEntry('/tmp/file1', OK_SAVED)
    fw:addEntry('/tmp/file2', OK_SAVED)

    assert.same({ '/tmp/file1', OK_SAVED }, { fw:getEntry(1) })
    assert.same({ '/tmp/file2', OK_SAVED }, { fw:getEntry(2) })
    assert.same({ '/tmp/file2', OK_SAVED }, { fw:getEntry(-1) }) -- last
    assert.same({ '/tmp/file1', OK_SAVED }, { fw:getEntry(-2) })
    assert.same({ OK_SAVED, '/tmp/file2' }, { fw:getLastEntryCodeAndPath() })
  end)
end)

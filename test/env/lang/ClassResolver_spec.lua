--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os() -- activate before another modules

local class = require("oop.class")

-- stubs
local StubLang = require("env.lang.oop.stub.StubLang")
local H = require("env.lang.oop.stub.ATestHelper")

local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")

local rnf = H.root_and_file
local mk_ClassResolver = H.mk_ClassResolver
local CT = ClassInfo.CT


describe("env.lang.ClassResolver", function()
  after_each(function()
    nvim.D.disable()
    nvim.logger_off()
    nvim:clear_all_state_with_os()
    -- nvim:restore_original_os()
  end)
  --
  it("_init constructor", function()
    assert.match_error(function()
      ClassResolver:new()
    end, 'lang strictly required')

    local lang = StubLang:new({ project_root = '/tmp/' })
    local cr = ClassResolver:new({ lang = lang, type = CT.CURRENT })
    assert.same(lang, cr.lang)
    assert.same('/tmp/', cr.project_root)
  end)

  it("isPathInProjectRoot", function()
    local project_root = "/home/app/project/"
    local lang = StubLang:new({ project_root = project_root }) -- mk_lang
    local cr = ClassResolver:new({ lang = lang })              -- CT.CURRENT is default

    assert.is_true(cr:isPathInProjectRoot(project_root .. '/src/file'))
    assert.is_true(cr:isPathInProjectRoot(project_root .. '/src'))
    assert.is_false(cr:isPathInProjectRoot('/home/app/'))
    assert.is_false(cr:isPathInProjectRoot('/'))
    assert.is_false(cr:isPathInProjectRoot(nil))
    assert.is_false(cr:isPathInProjectRoot(''))
  end)

  -- same via helper tooling
  it("isPathInProjectRoot tooling", function()
    local project_root = "/home/app/project/"
    local cr = mk_ClassResolver(project_root)
    assert.same('env.lang.oop.stub.StubLang', class.name(cr.lang))

    assert.is_true(cr:isPathInProjectRoot(project_root .. '/src/file'))
    assert.is_true(cr:isPathInProjectRoot(project_root .. '/src'))
    assert.is_false(cr:isPathInProjectRoot('/home/app/'))
    assert.is_false(cr:isPathInProjectRoot('/'))
    assert.is_false(cr:isPathInProjectRoot(nil))
    assert.is_false(cr:isPathInProjectRoot(''))
  end)

  it("getClassInfo exist", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:get_os():set_file(fn, {})

    local cr = mk_ClassResolver({ pr = pr, path = fn })
    -- cr.path -- target to search

    cr:run()                      -- workload

    assert.is_table(cr.classinfo) -- search result
    assert.is_true(cr.lang:isPathExists(cr.classinfo))
    assert.is_true(cr.lang:isPathExists(cr.classinfo:getPath()))

    local onlyExistOrNil = true
    assert.is_not_nil(cr:getClassInfo(onlyExistOrNil)) --
  end)

  --
  -- ensureFoundExisted
  it("getClassInfo onlyExistOrNil and not exists file", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:get_os():set_file(pr, {})

    local cr = mk_ClassResolver({ pr = pr, path = fn })

    cr:run()                      -- workload

    assert.is_table(cr.classinfo) -- search result
    assert.is_false(cr.lang:isPathExists(cr.classinfo))
    assert.is_false(cr.lang:isPathExists(cr.classinfo:getPath()))

    local onlyExistOrNil = true
    assert.is_nil(cr:getClassInfo(onlyExistOrNil)) --

    assert.is_not_nil(cr:getClassInfo(false))
    assert.same(fn, cr:getClassInfo(false):getPath())
  end)



  it("mkTestCaseCI directory", function()
    local pr, fn = rnf('/home/user/proj/', 'spec/app/module/')
    local cn, ip, pair = 'app.module.', 'spec/app/module/', { 'ci' }
    local tt = ClassInfo.CT.TESTDIR

    local cr = mk_ClassResolver(pr)
    local tci = ClassInfo:new({
      path = fn, inner_path = ip, classname = cn, type = tt, pair = pair
    })
    local tcd = cr:mkTestCaseCI(tci, 'sub', CT.TESTDIR)

    assert.is_not_nil(tcd) ---@cast tcd env.lang.ClassInfo
    assert.same(CT.TESTDIR, tcd.type)
    assert.same(tci, tcd.parent)

    assert.same('/home/user/proj/spec/app/module/sub/', tcd.path)
    assert.same('spec/app/module/sub/', tcd.inner_path)
    assert.same('app.module.sub.', tcd.classname)
    assert.same(pair, tcd.pair)
  end)

  it("mkTestCaseCI directory", function()
    local pr, fn = rnf('/home/user/proj/', 'spec/app/module/')
    local cn, ip, pair = 'app.module.', 'spec/app/module/', { 'ci' }
    local tt = ClassInfo.CT.TESTDIR

    local cr = mk_ClassResolver(pr)
    local tci = ClassInfo:new({
      path = fn, inner_path = ip, classname = cn, type = tt, pair = pair
    })
    local tcd = cr:mkTestCaseCI(tci, 'case_spec.lua', CT.TEST)

    assert.is_not_nil(tcd) ---@cast tcd env.lang.ClassInfo
    assert.same(CT.TEST, tcd.type)
    assert.same(tci, tcd.parent)

    assert.same('/home/user/proj/spec/app/module/case_spec.lua', tcd.path)
    assert.same('spec/app/module/case_spec.lua', tcd.inner_path)
    assert.same('app.module.case_spec.lua', tcd.classname)
    assert.same(pair, tcd.pair)
  end)

  --
  -- resolve src class -> find test class(many-to-one)
  it("TestCasesDir", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/app/MyClass.stub')
    local _, tfn = rnf(pr, 'test/app/MyClass/Case1Test.stub')

    local fs = nvim:get_os():set_dir(pr, { 'test/', 'src/' })
    fs:mk(sfn, tfn) -- existed testcase dir MyClass/Case1Test

    local tcdir = '/home/user/proj/test/app/MyClass'
    assert.same('directory', fs:file_type(tcdir))
    assert.same('file', fs:file_type(tfn))

    -- nvim.logger_setup(nil, 0) nvim.D.enable()

    local cr = mk_ClassResolver({ pr = pr, path = sfn, type = CT.TEST })
    local tci = cr:run():getClassInfo() ---@cast tci env.lang.ClassInfo

    -- assert.is_not_nil(lang:getTestClassForSrcClass(ci:getClassName()))

    assert.same(true, tci:isTestDir())
    assert.same(false, tci:isTestClass())
    assert.same(true, tci:isPathExists())
    assert.same('/home/user/proj/test/app/MyClass/', tci:getPath())
    local res = tci:getTestCases()
    assert.is_table(res) ---@cast res table
    local res_ci = res['Case1Test.stub']
    assert.same('env.lang.ClassInfo', class.name(res_ci))
    assert.same('app.MyClass.Case1Test', res_ci.classname)

    assert.same('file', nvim:get_os():file_type(sfn))

    assert.same(false, cr.isNotExistsTestClass(tci))
  end)


  it("lookupSourceInSubDir", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/app/MyClass.stub')
    local _, tfn = rnf(pr, 'test/app/MyClass/CasesSuite/SuccessTest.stub')
    local tcdir = '/home/user/proj/test/app/MyClass'

    local fs = nvim:get_os()
    fs:mk(pr, 'src/', 'test/', sfn, tfn)
    fs:assert_has(tcdir, tfn) -- ensure dir and file exists

    local cr = mk_ClassResolver({ pr = pr, path = tfn, type = CT.SOURCE })
    local sci = cr:run():getClassInfo()
    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo
    assert.same(true, sci:isSourceClass())
    assert.same('app.MyClass', sci.classname)
    assert.same(sfn, sci:getPath())

    local tci = sci.pair
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(true, tci:isTestClass())
    assert.same('app.MyClass.CasesSuite.SuccessTest', tci.classname)
    assert.same(tfn, tci:getPath())
  end)


  it("getTestCaseDir success", function()
    local tci = ClassInfo:new({
      classname = 'org.pkg.MyClassTest',
      path = '/home/dev/project/test/org/pkg/MyClassTest.file',
      type = ClassInfo.CT.TEST,
    })
    local cr = ClassResolver.of(StubLang:new({ project_root = '/tmp/' }))
    assert.same('/home/dev/project/test/org/pkg/MyClass/', cr:getTestCaseDir(tci))
  end)

  it("getTestCaseDir fail no path", function()
    local cr = mk_ClassResolver('/tmp/')
    local tci = ClassInfo:new({
      classname = 'org.pkg.MyClassTest',
      type = ClassInfo.CT.TEST,
    })
    local exp_err = 'expected resolved Test ClassInfo with path'
    assert.match_error(function() cr:getTestCaseDir(tci) end, exp_err)
  end)

  --
  it("getTestCaseDir fail bad type", function()
    local cr = mk_ClassResolver('/tmp/')
    local tci = ClassInfo:new({
      classname = 'org.pkg.MyClass',
      type = ClassInfo.CT.SOURCE,
      path = 'fake'
    })

    local exp_err = 'expected classinfo.type == Test or TestDir, got:'
        .. ClassInfo.CT.SOURCE
    assert.match_error(function() cr:getTestCaseDir(tci) end, exp_err)
  end)

  it("getTestCaseDir extrace from simple flat test-path", function()
    local pr, fn = rnf('/home/user/proj/', 'spec/app/moduleTest.lua')
    local cr = mk_ClassResolver(pr)
    local ci = ClassInfo:new({ path = fn, exists = true, type = CT.TEST })
    assert.same('/home/user/proj/spec/app/module/', cr:getTestCaseDir(ci))
  end)

  it("getTestCaseDir already extracted", function()
    local pr, fn = rnf('/home/user/proj/', 'spec/app/module')
    local cr = mk_ClassResolver(pr)
    local ci = ClassInfo:new({ path = fn, exists = true, type = CT.TEST })
    assert.same('/home/user/proj/spec/app/module/', cr:getTestCaseDir(ci))
  end)

  --
  it("lang stub for lua adjust tooling", function()
    local pr, dn0 = rnf('/home/user/proj/', 'spec/app/module/')
    local _, fn = rnf(pr, 'spec/app/module_spec.lua') -- suggested path

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    assert.same('spec/', cr.lang:getTestPath())
    local tci = cr:lookupByPath(fn, CT.TEST)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(fn, tci.path)
    assert.same('spec/app/module_spec', tci.inner_path)
    assert.same('app.module_spec', tci.classname)
    assert.same(CT.TEST, tci.type)
    assert.same(dn0, cr:getTestCaseDir(tci))
  end)

  --
  --
  it("lookupTestCases recursive", function()
    local pr, dn = rnf('/home/user/proj/', 'spec/app/module/')

    local _, fn = rnf(pr, 'spec/app/module_spec.lua') -- suggested path

    local _, fn1 = rnf(pr, 'spec/app/module/sub-case-dir/')
    local _, fn2 = rnf(pr, 'spec/app/module/sub-case-dir/a_spec.lua')
    local _, fn3 = rnf(pr, 'spec/app/module/sub-case-dir/b_spec.lua')
    local _, fn4 = rnf(pr, 'spec/app/module/sub-case-dir/sub2/a_spec.lua')
    local _, fn5 = rnf(pr, 'spec/app/module/case1_spec.lua')
    local _, fn6 = rnf(pr, 'spec/app/module/case2_spec.lua')
    nvim:get_os():mk(dn, fn1, fn2, fn3, fn4, fn5, fn6)
    assert.is_nil(nvim:get_os():file_type(fn)) -- not exists

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    local tci = cr:lookupByPath(fn, CT.TEST)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(CT.TEST, tci.type)
    assert.same(dn, cr:getTestCaseDir(tci))

    -- nvim.logger_setup(nil, 0)

    local ret, dir = cr:lookupTestCases(tci, true) -- workload

    assert.is_not_nil(ret)
    assert.same(dn, dir)
    assert.same(dn, tci.path)
    local t = tci.tcases
    assert.is_not_nil(t) ---@cast t table

    local scd = t['sub-case-dir']
    assert.is_not_nil(scd) ---@cast scd env.lang.ClassInfo
    assert.same(fn1, scd.path)
    assert.same(CT.TESTDIR, scd.type)
    assert.same(tci, scd.parent)

    assert.same(fn2, scd.tcases['a_spec.lua'].path)
    assert.same(fn3, scd.tcases['b_spec.lua'].path)
    assert.same(scd, scd.tcases['b_spec.lua'].parent)

    local scd_sub2 = scd.tcases['sub2']
    assert.is_not_nil(scd_sub2) ---@cast scd_sub2 env.lang.ClassInfo

    local exp4 = 'spec/app/module/sub-case-dir/sub2/a_spec'
    assert.same(fn4, scd_sub2.tcases['a_spec.lua'].path)
    assert.same(exp4, scd_sub2.tcases['a_spec.lua'].inner_path)
    assert.same(scd_sub2, scd.tcases['sub2'].tcases['a_spec.lua'].parent)

    local exp_cn5 = 'app.module.sub-case-dir.sub2.a_spec'
    assert.same(exp_cn5, scd.tcases['sub2'].tcases['a_spec.lua'].classname)

    assert.same(fn5, t['case1_spec.lua'].path)

    local exp_ip6 = 'spec/app/module/case2_spec'
    local exp_cn6 = 'app.module.case2_spec'
    assert.same(fn6, t['case2_spec.lua'].path)
    assert.same(exp_ip6, t['case2_spec.lua'].inner_path)
    assert.same(exp_cn6, t['case2_spec.lua'].classname)
  end)

  --
  it("lookupTestCases not recursice", function()
    local pr, dn = rnf('/home/user/proj/', 'spec/app/module/')

    local _, fn = rnf(pr, 'spec/app/module_spec.lua') -- suggested path

    local _, fn1 = rnf(pr, 'spec/app/module/sub-case-dir/')
    local _, fn2 = rnf(pr, 'spec/app/module/sub-case-dir/a_spec.lua')
    local _, fn3 = rnf(pr, 'spec/app/module/sub-case-dir/b_spec.lua')
    local _, fn4 = rnf(pr, 'spec/app/module/sub-case-dir/sub2/a_spec.lua')
    local _, fn5 = rnf(pr, 'spec/app/module/case1_spec.lua')
    local _, fn6 = rnf(pr, 'spec/app/module/case2_spec.lua')
    nvim:get_os():mk(dn, fn1, fn2, fn3, fn4, fn5, fn6)
    assert.is_nil(nvim:get_os():file_type(fn)) -- not exists

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    local tci = cr:lookupByPath(fn, CT.TEST)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo
    assert.same(fn, tci.path)


    local ret, dir = cr:lookupTestCases(tci, false) -- workload

    assert.is_not_nil(ret)
    assert.same(dn, dir)
    assert.same(dn, tci.path)
    local t = tci.tcases
    assert.is_not_nil(t) ---@cast t table

    local scd = t['sub-case-dir']
    assert.is_not_nil(scd) ---@cast scd env.lang.ClassInfo
    assert.same(fn1, scd.path)
    assert.same(CT.TESTDIR, scd.type)
    assert.same(tci, scd.parent)
    assert.is_nil(scd.tcases) -- not a recursive

    assert.same(fn5, t['case1_spec.lua'].path)

    local exp_ip6 = 'spec/app/module/case2_spec'
    local exp_cn6 = 'app.module.case2_spec'
    assert.same(fn6, t['case2_spec.lua'].path)
    assert.same(exp_ip6, t['case2_spec.lua'].inner_path)
    assert.same(exp_cn6, t['case2_spec.lua'].classname)
  end)

  -- why not add new ci inside lookupByPath|lookupByClassName?
  -- this to function return suggested ci not checked is it real existed in fs
  it("lookupByPath find new empty cimap", function()
    local pr, fn = rnf('/tmp/app/', 'spec/app/module_spec.lua')
    nvim:get_os():mk(fn)

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    assert.is_nil(cr.lang:getCIbyPath(fn))  -- before
    local ci = cr:lookupByPath(fn, CT.TEST) -- workload
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo found

    -- make sure cimap is empty
    assert.is_nil(cr.lang:getCIbyPath(fn))
    assert.is_nil(cr.lang:getCIbyClassName('app.module_spec'))
  end)

  it("lookupByClassName find new empty cimap", function()
    local pr, fn = rnf('/tmp/app/', 'spec/app/module_spec.lua')
    local cn = 'app.module_spec'
    nvim:get_os():mk(fn)
    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    assert.is_nil(cr.lang:getCIbyPath(fn))        -- before

    local tci = cr:lookupByClassName(cn, CT.TEST) -- workload
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo  found

    assert.same('spec/app/module_spec', tci:getInnerPath())
    assert.same('app.module_spec', tci:getClassName())

    -- make sure cimap is empty
    assert.is_nil(cr.lang:getCIbyPath(fn))
    assert.is_nil(cr.lang:getCIbyClassName('app.module_spec'))
  end)

  it("resolve new CI ByPath and save to cimap", function()
    local pr, fn = rnf('/tmp/app/', 'spec/app/module_spec.lua')
    nvim:get_os():mk(fn)

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    assert.is_nil(cr.lang:getCIbyPath(fn)) -- before
    cr.path, cr.type = fn, CT.TEST         -- lookupByPath
    local ci = cr:run():getClassInfo()     -- workload

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo found

    -- make sure saved into cimap.path2ci and cimap.cn2ci
    assert.same(ci, cr.lang:getCIbyPath(fn))
    assert.same(ci, cr.lang:getCIbyClassName('app.module_spec'))
  end)

  it("resolve new CI ByClassName and save to cimap", function()
    local pr, fn = rnf('/tmp/app/', 'spec/app/module_spec.lua')
    local cn = 'app.module_spec'
    nvim:get_os():mk(fn)
    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    assert.is_nil(cr.lang:getCIbyPath(fn)) -- before
    cr.classname, cr.type = cn, CT.TEST    -- lookupByClassName
    local ci = cr:run():getClassInfo()     -- workload

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo  found

    assert.same('spec/app/module_spec', ci:getInnerPath())
    assert.same('app.module_spec', ci:getClassName())

    -- make sure cimap is empty
    -- make sure saved into cimap.path2ci and cimap.cn2ci
    assert.same(ci, cr.lang:getCIbyPath(fn))
    assert.same(ci, cr.lang:getCIbyClassName('app.module_spec'))
  end)

  --
  -- In general, by design, a many-to-one relationship assumes that]
  -- a simple direct one-to-one relation will not be used.
  -- he we test is the existed simple one-to-one relation not break many-to-one
  --
  -- one-to-one is:
  --  src/app/module.lua  -->  spec/app/module_spec.lua
  --
  -- many-to-one is:
  --  src/app/module.lua  -->  spec/app/module/a_spec.lua
  --          source       TestCasesDir ^      ^ test-case-file(can be many)
  --
  it("lookupTestCases TESTDIR with existed one-to-one relation", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/app/module.lua')

    local _, tfn = rnf(pr, 'spec/app/module_spec.lua')   -- one-to-one relation
    local _, tdn = rnf(pr, 'spec/app/module/')           -- test case dir
    local _, tdc = rnf(pr, 'spec/app/module/a_spec.lua') -- test case file

    nvim:get_os():mk(sfn, tfn, tdn, tdc)

    local cr = mk_ClassResolver(pr, { 'lua', 'spec/', '_spec' })

    -- need testdir(tdn) for given source file(sfn)
    cr.path, cr.type = sfn, CT.TESTDIR
    local tci = cr:run():getClassInfo()
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo
    local sci = tci.pair
    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo

    -- source of search target (from which we jump)
    assert.same(CT.SOURCE, sci.type)
    assert.same('app.module', sci.classname)
    assert.same('src/app/module', sci.inner_path)
    assert.same('/home/user/proj/src/app/module.lua', sci.path)

    -- target
    assert.same(CT.TESTDIR, tci.type)
    assert.same('app.module.', tci.classname)
    assert.same('spec/app/module/', tci.inner_path)
    assert.same('/home/user/proj/spec/app/module/', tci.path)
    assert.same(tci, sci.pair) -- initialy the source related with testdir

    local tcc = tci:getTestCases()['a_spec.lua']
    assert.is_not_nil(tcc) ---@cast tcc env.lang.ClassInfo
    assert.same(sci, tcc.pair) -- each test-case-file related with source
    assert.same(tci, tcc.parent)

    -- just first test-case-file to open
    assert.equal(tcc, tci:getFirstCase())
  end)
end)

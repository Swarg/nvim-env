--
require 'busted.runner' ()
local assert = require('luassert')

local ClassInfo = require('env.lang.ClassInfo')
local CT = ClassInfo.CT

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect


describe('env.lang.ClassInfo', function()
  it("isSourceClass isTestClass", function()
    local path = '/home/dev/project/src/org/comp/app/App.java'
    local classname = 'org.comp.app.App'

    local ci = ClassInfo:new({
      path = path, classname = classname, type = ClassInfo.CT.SOURCE
    })

    assert.same(true, ci:isSourceClass())
    assert.same(false, ci:isTestClass())
    assert.same(path, ci:getPath())
    assert.same(classname, ci:getClassName())
  end)


  it("isValidType", function()
    local ci = ClassInfo:new({
      path = 'pkg/class', classname = 'pkg.class', type = ClassInfo.CT.UKNOWN
    })
    assert.same(false, ClassInfo.isValidType(ci))

    ci.type = nil
    assert.same(false, ClassInfo.isValidType(ci))

    ci.type = ClassInfo.CT.SOURCE
    assert.same(true, ClassInfo.isValidType(ci))

    ci.type = ClassInfo.CT.TEST
    assert.same(true, ClassInfo.isValidType(ci))

    ci.type = ClassInfo.CT.TESTDIR
    assert.same(true, ClassInfo.isValidType(ci))
  end)


  it("isSourceClass isTestClass", function()
    local ci = ClassInfo:new()

    ci.type = ClassInfo.CT.SOURCE
    assert.same('Source', ci:getTypeName())

    ci.type = ClassInfo.CT.TEST
    assert.same('Test', ci:getTypeName())

    ci.type = ClassInfo.CT.UKNOWN
    assert.same('Unknown', ci:getTypeName())

    ci.type = nil
    assert.same('Unknown', ci:getTypeName())
  end)

  it("getShortClassName", function()
    local ci = ClassInfo:new()
    ci.classname = 'org.comp.app.ShortClassName'
    assert.same("ShortClassName", ci:getShortClassName())

    ci.classname = 'org.comp.app.A'
    assert.same("A", ci:getShortClassName())

    ci.classname = 'org.comp.app.'
    assert.same(nil, ci:getShortClassName())

    ci.classname = [[App\Auth\Entity\User]]
    assert.same("User", ci:getShortClassName())
  end)

  it("getPackage", function()
    local ci = ClassInfo:new()
    ci.classname = 'org.comp.app.ShortClassName'
    assert.same('org.comp.app', ci:getPackage())

    ci.classname = 'org.comp.app.A'
    assert.same('org.comp.app', ci:getPackage())

    ci.classname = 'org.comp.app.'
    assert.same(nil, ci:getPackage())

    ci.classname = 'org.comp.app'
    assert.same('org.comp', ci:getPackage())
  end)

  it("isClassName", function()
    assert.same(false, ClassInfo.isClassName(nil))
    assert.same(false, ClassInfo.isClassName(''))
    assert.same(false, ClassInfo.isClassName('a'))
    assert.same(false, ClassInfo.isClassName('aA'))
    assert.same(true, ClassInfo.isClassName('A'))
    assert.same(true, ClassInfo.isClassName('Abc'))
    assert.same(true, ClassInfo.isClassName('package.Class'))
    assert.same(true, ClassInfo.isClassName('package\\Class'))
    assert.same(false, ClassInfo.isClassName('package.path.not-a-class'))
    assert.same(false, ClassInfo.isClassName('package\\path\\not-a-class'))
  end)

  it("bindPair & toArray", function()
    local sci = ClassInfo:new({
      classname = 'org.pkg.MyClass',
      path = '/home/dev/project/src/org/pkg/MyClass.file',
    })
    local tci = ClassInfo:new({
      classname = 'org.pkg.MyClassTest',
      path = '/home/dev/project/test/org/pkg/MyClassTest.file',
    })
    tci:bindPair(sci)
    assert.same(sci, tci:getPair())
    assert.same(tci, sci:getPair())
    local exp_sci = {
      classname = 'org.pkg.MyClass',
      path = '/home/dev/project/src/org/pkg/MyClass.file',
      type = 0,
      pair = {
        classname = 'org.pkg.MyClassTest',
        path = '/home/dev/project/test/org/pkg/MyClassTest.file',
        type = 0,
        pair = 'self',
      }
    }

    local exp_tci = {
      classname = 'org.pkg.MyClassTest',
      path = '/home/dev/project/test/org/pkg/MyClassTest.file',
      type = 0,
      pair = {
        type = 0,
        classname = 'org.pkg.MyClass',
        path = '/home/dev/project/src/org/pkg/MyClass.file',
        pair = 'self',
      }
    }
    assert.same(exp_tci, tci:toArray())
    assert.same(exp_sci, sci:toArray())
  end)

  it("getPairClassName success", function()
    local sci = ClassInfo:new({ classname = 'org.pkg.MyClass', })
    local tci = ClassInfo:new({ classname = 'org.pkg.MyClassTest', })
    tci:bindPair(sci)

    assert.same('org.pkg.MyClassTest', sci:getPairClassName())
    assert.same('org.pkg.MyClass', tci:getPairClassName())
  end)

  it("getPairClassName no pair", function()
    local sci = ClassInfo:new({ classname = 'org.pkg.MyClass', })
    local tci = ClassInfo:new({ classname = 'org.pkg.MyClassTest', })

    assert.is_nil(sci:getPairClassName())
    assert.is_nil(tci:getPairClassName())
  end)


  it("getPath", function()
    local ci = ClassInfo:new({ path = '/tmp/file' })
    assert.same('/tmp/file', ci:getPath())
    assert.same('/tmp/file', ClassInfo.getPath(ci))
    -- assert.is_nil(ClassInfo.getPath(nil))
  end)

  -- only return field not check itself
  it("isPathExists", function()
    local ci = ClassInfo:new({ path = '/tmp/file', exists = true })

    assert.same(true, ci:isPathExists())
    assert.same(true, ClassInfo.isPathExists(ci))
    -- assert.is_nil(ClassInfo.isPathExists(nil))

    ci:setPathExists(false)
    assert.same(false, ci:isPathExists())
    assert.same(false, ClassInfo.isPathExists(ci))

    ci:setPathExists(nil)
    assert.is_nil(ci:isPathExists())
    assert.is_nil(ClassInfo.isPathExists(ci))
  end)

  -- many-tests-for-one-source
  --
  it("bind many-to-one", function()
    local sci = ClassInfo:new({ classname = 'io.App', type = CT.SOURCE })
    -- suggested one-to-one
    local tci0 = ClassInfo:new({ classname = 'io.AppTest', type = CT.TEST })

    -- actual many-to-one
    local tcid = ClassInfo:new({ classname = 'io.App', type = CT.TESTDIR })
    local tci1 = ClassInfo:new({ classname = 'io.App.Case1Test', type = CT.TEST })
    local tci2 = ClassInfo:new({ classname = 'io.App.Case2Test', type = CT.TEST })
    local tci3 = ClassInfo:new({ classname = 'io.App.Case3Test', type = CT.TEST })

    sci:bindPair(tci0) -- bind src with suggested   one-to-one relation
    assert.same(tci0, sci.pair)
    assert.same(sci.pair, tci0)

    assert.are_not_same(sci.pair, tcid) -- before

    -- todo testcase files for testcasesdir
    tcid.tcases = { tci1, tci2, tci3 }
    ClassInfo.bind(tcid, sci) -- share tci0.pair with actual tcid

    assert.same(sci, tcid.pair)
    assert.same(sci, tci1.pair)
    assert.same(sci, tci2.pair)
    assert.same(sci, tci3.pair)
  end)

  it("getTopCasesDir", function()
    local a = ClassInfo:new({ parent = nil, classname = 'A' })
    local b = ClassInfo:new({ parent = a, classname = 'B' })
    local c = ClassInfo:new({ parent = b, classname = 'C' })
    local d = ClassInfo:new({ parent = c, classname = 'D' })
    assert.same(a, d:getTopCasesDir())
    assert.same(a, c:getTopCasesDir())
    assert.same(a, b:getTopCasesDir())
    assert.is_nil(a:getTopCasesDir())
  end)

  it("getTopCasesDir", function()
    local a = ClassInfo:new({ parent = nil, classname = 'A' })
    local b = ClassInfo:new({ parent = a, classname = 'B' })
    assert.same(a, b:getTopCasesDir())
    assert.is_nil(a:getTopCasesDir())
  end)

  it("getTopCasesDir skip infinity loop", function()
    local a = ClassInfo:new({ parent = nil, classname = 'A' })
    local b = ClassInfo:new({ parent = a, classname = 'B' })
    a.parent = b
    assert.same(a, b:getTopCasesDir())
    assert.same(b, a:getTopCasesDir())
  end)

  it("getVarNameForClassName", function()
    local f = ClassInfo.getVarNameForClassName
    assert.same('user', f("User"))
    assert.same('userId', f("UserId"))
    assert.same('jwtHelper', f("JWTHelper"))
    assert.same('jwth', f("JWTH"))
    assert.same('dao', f("DAO"))
    assert.same('personDAO', f("PersonDAO"))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')
local ComponentGen = require("env.lang.oop.ComponentGen")

describe("env.lang.oop.ComponentGen", function()

  it("notEmptyOrFalse", function()
    assert.same('abc', ComponentGen.notEmptyOrFalse('abc'))
    assert.same('x', ComponentGen.notEmptyOrFalse('x'))
    assert.same(false, ComponentGen.notEmptyOrFalse(''))
    assert.same(false, ComponentGen.notEmptyOrFalse(false))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same(false, ComponentGen.notEmptyOrFalse(nil))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same({}, ComponentGen.notEmptyOrFalse({}))
  end)

end)

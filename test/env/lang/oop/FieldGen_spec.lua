--
require 'busted.runner' ()
local assert = require('luassert')
local H = require("env.lang.oop.stub.ATestHelper")
---@diagnostic disable-next-line: unused-local
local FieldGen = require("env.lang.oop.FieldGen")
local M = require('env.lang.oop.FieldGen')

describe("env.lang.oop.FieldGen", function()
  describe('parseCliStr', function()
    it("fails", function()
      assert.same(nil, M.parseCliStr('public'))
      assert.same(nil, M.parseCliStr('public_static'))
      assert.same(nil, M.parseCliStr('private'))
      assert.same(nil, M.parseCliStr('private_static'))
      assert.same(nil, M.parseCliStr('protected'))
      assert.same(nil, M.parseCliStr('protected_static'))
    end)

    it("one arg", function()
      local defamod, deftype = nil, nil
      -- Field Type Only
      local exp = { type = 'Id', name = 'id', amod = defamod }
      assert.same(exp, M.parseCliStr('Id'):toArray())
      -- Filed Name
      exp = { type = deftype, name = 'str', amod = defamod }
      assert.same(exp, M.parseCliStr('str'):toArray())
      -- Error Only mod
      assert.same(nil, M.parseCliStr('private'))
    end)

    it("two args Mod+Type", function()
      local strdef = 'private-Id' -- Mod+Type
      local exp = { type = 'Id', name = 'id', amod = H.AM.PRIVATE }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'public-Id' -- Mod+Type
      exp = { type = 'Id', name = 'id', amod = H.AM.PUBLIC }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'public_static-UserId' -- Mod+Type
      exp = { type = 'UserId', name = 'userId', amod = H.AM.PUBLIC_STATIC }
      assert.same(exp, M.parseCliStr(strdef):toArray())
    end)

    it("two args Mod+Name", function()
      local deftype = nil

      local strdef = 'private-id' -- Mod+Name
      local exp = { type = deftype, name = 'id', amod = H.AM.PRIVATE }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'public-id' -- Mod+Name
      exp = { type = deftype, name = 'id', amod = H.AM.PUBLIC }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'public_static-userId' -- Mod+Name
      exp = { type = deftype, name = 'userId', amod = H.AM.PUBLIC_STATIC }
      assert.same(exp, M.parseCliStr(strdef):toArray())
    end)

    it("two args Type+Name", function()
      local defamod = nil
      local strdef = 'Id-id' -- Type+Name
      local exp = { type = 'Id', name = 'id', amod = defamod }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'string-id' -- Type+Name
      exp = { type = 'string', name = 'id', amod = defamod }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'bool-userId'
      exp = { type = 'bool', name = 'userId', amod = defamod }
      assert.same(exp, M.parseCliStr(strdef):toArray())
    end)

    it("three args Mod+Type+Name", function()
      local strdef = 'private-Id-id'
      local exp = { type = 'Id', name = 'id', amod = H.AM.PRIVATE }
      assert.same(exp, M.parseCliStr(strdef):toArray())

      strdef = 'public-UUID-token'
      exp = { type = 'UUID', name = 'token', amod = H.AM.PUBLIC }
      assert.same(exp, M.parseCliStr(strdef):toArray())
    end)
  end) -- parseCliStr

  it("toCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.same('String', gen:getDefaultFieldType())

    local field = H.mkField(gen, 'id', 'Id', H.AM.PRIVATE)
    local fg = gen:getFieldGen(ci)
    assert.same('    private Id id;', fg:toCode(field))

    field = H.mkField(gen, 'id')
    assert.same('    private String id;', fg:toCode(field))

    ---@diagnostic disable-next-line: param-type-mismatch
    field = H.mkField(gen, nil)
    assert.same('    private String field;', fg:toCode(field))
  end)


  it("toCode default value", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.same('String', gen:getDefaultFieldType())

    local field = H.mkField(gen, 'id', 'Id', H.AM.PRIVATE)
    field.defval = '123';
    local fg = gen:getFieldGen(ci)
    assert.same('    private Id id = "123";', fg:toCode(field))
  end)

  it("toCode doc block", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.same('String', gen:getDefaultFieldType())

    local field = H.mkField(gen, 'id', 'Id', H.AM.PRIVATE)
    field.title = 'Identificator'
    field.desc = 'this is a comment for this filed';
    field.enum = { '1', '2', '3' }
    field.example = "88"
    local fg = gen:getFieldGen(ci)
    local exp = [[
    /**
     * Identificator
     * this is a comment for this filed
     * enum: 1 2 3
     * example: 88
     */
    private Id id;]]
    assert.same(exp, fg:toCode(field))
  end)
end)

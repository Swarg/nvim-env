-- 10-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local ClassDefinition = require 'env.lang.oop.ClassDefinition'

describe("env.lang.oop.ClassDefinition", function()
  it("ends_with", function()
    local f = ClassDefinition.test.ends_with
    assert.same(true, f('pkg.some.Class', 'Class', '.'))
  end)

  -- helper to create instance of the parsed class
  local function newRepositoryBasedVersionList()
    local clazz = {
      pkg = "org.app.updater",
      name = "RepositoryBasedVersionList",
      typ = "class",
      extends = "RemoteVersionList",
      imports = {
        "java.io.IOException",
        "org.app.versions.Version",
        "org.app.repository.Repo",
      },
      mods = 1, -- public
      -- ln = 11,
    }
    local path = 'path/to/project/src/org/app/updater/RepositoryBasedVersionList'
    local stop_info = nil
    return ClassDefinition:new(nil, clazz, path, stop_info)
  end

  it("_init - new instance", function()
    local instance = newRepositoryBasedVersionList()
    local exp = {
      pkg = 'org.app.updater',
      name = 'RepositoryBasedVersionList',
      mods = 1,
      extends = 'RemoteVersionList',
      imports = {
        'java.io.IOException',
        'org.app.versions.Version',
        'org.app.repository.Repo'
      },
      path = 'path/to/project/src/org/app/updater/RepositoryBasedVersionList',
      mtime = 0,
      test = {}
    }
    assert.same(exp, instance:toArray())
    assert.same('org.app.updater', instance:getPackage())
    assert.same('RepositoryBasedVersionList', instance:getName())
    assert.same('RemoteVersionList', instance:getParentName())
  end)


  it("findClassInImport", function()
    local cdef = newRepositoryBasedVersionList()
    assert.same('java.io.IOException', cdef:findClassInImport('IOException'))
    assert.same('org.app.repository.Repo', cdef:findClassInImport('Repo'))
    assert.same('org.app.versions.Version', cdef:findClassInImport('Version'))

    assert.is_nil(cdef:findClassInImport('Rep'))
    assert.is_nil(cdef:findClassInImport('.Repo'))
    -- full name is ok
    assert.same('java.io.IOException', cdef:findClassInImport('java.io.IOException'))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')
local Docblock = require("env.lang.oop.Docblock")
---@diagnostic disable-next-line: unused-local
local H = require("env.lang.oop.stub.ATestHelper")

-- local Editor = require("env.ui.Editor")
-- local Context = require("env.ui.Context")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local api = _G.vim.api -- nvim.api

describe("env.lang.oop.Docblock", function()
  before_each(function()
    nvim:clear_state()
  end)

  it("addLine", function()
    local ci, gen, _ = H.mkClassInfoAndLangGen0('test.Entity.EmailTest', '/tmp')
    local current_bufname = "/tmp/test/Entity/EmailTest.code"
    local lines = {
      "", -- 1
      "/**",
      " * @covers Email",
      " */",
      "class EmailTest extends TestCase",
      "{",
    }
    local bufnr = nvim:new_buf(lines, 1, 1, nil, current_bufname)

    local docs = Docblock:new({ gen = gen, classinfo = ci })
        :find(Docblock.ID.CLASS, 'EmailTest')

    assert.same(true, docs:isValid())
    local exp = {
      '/**',
      ' * @covers Email',
      ' */',
    }
    assert.same(exp, docs:getLines())
    assert.same(true, docs:addLine('a first new line'))
    local exp2 = {
      '',
      '/**',
      ' * @covers Email',
      ' * a first new line',
      ' */',
      'class EmailTest extends TestCase',
      '{'
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    assert.same(true, docs:addLine('a second new line'))
    local exp3 = {
      '',
      '/**',
      ' * @covers Email',
      ' * a first new line',
      ' * a second new line',
      ' */',
      'class EmailTest extends TestCase',
      '{'
    }
    assert.same(exp3, api.nvim_buf_get_lines(0, 0, -1, false))
  end)


  it("addLine 2", function()
    local ci, gen, _ = H.mkClassInfoAndLangGen0('test.Entity.EmailTest', '/tmp')
    local current_bufname = "/tmp/test/Entity/EmailTest.code"
    local lines = {
      "", -- 1
      "class EmailTest extends TestCase",
      "{",
    }
    local bufnr = nvim:new_buf(lines, 1, 1, nil, current_bufname) -- stub

    local docs = Docblock:new({ gen = gen, classinfo = ci })
        :find(Docblock.ID.CLASS, 'EmailTest')

    assert.same(true, docs:isValid())
    local exp = {}
    assert.same(exp, docs:getLines())
    assert.same(true, docs:addLine('a first new line'))
    local exp2 = {
      '/**',
      ' * a first new line',
      ' */',
      'class EmailTest extends TestCase',
      '{'
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    assert.same(true, docs:addLine('a second new line'))
    local exp3 = {
      '/**',
      ' * a first new line',
      ' * a second new line',
      ' */',
      'class EmailTest extends TestCase',
      '{'
    }
    assert.same(exp3, api.nvim_buf_get_lines(0, 0, -1, false))
  end)
end)

describe("env.lang.oop.Docblock for codegen", function()
  it("newMultiline + add", function()
    local doc = Docblock:new({}):newMultiline()
    assert.same({ '/**', ' */' }, doc.lines)
    assert.same({ '/**', ' * first-line', ' */' }, doc:add("first-line").lines)
    local exp = {
      '/**',
      ' * first-line',
      ' * second-line',
      ' */' }
    assert.same(exp, doc:add("second-line").lines)
  end)

  local multi_line_1 = [[
Indicates whether the zone is only using DNS services.
A true value means the zone will not receive security or performance
benefits.
]]

  it("multiline fold", function()
    local doc = Docblock:new({}):newMultiline():add(multi_line_1)
    local exp = {
      '/**',
      ' * Indicates whether the zone is only using DNS services.',
      ' * A true value means the zone will not receive security or performance',
      ' * benefits.',
      ' */'
    }
    assert.same(exp, doc.lines)
  end)

  it("multiline fold", function()
    local doc = Docblock:new({ max_length = 40 }):newMultiline():add(multi_line_1)
    local exp = {
      '/**',
      ' * Indicates whether the zone is only',
      ' * using DNS services.',
      ' * A true value means the zone will not',
      ' * receive security or performance',
      ' * benefits.',
      ' */'
    }
    assert.same(exp, doc.lines)
  end)

  it("multiline add empty line ", function()
    local doc = Docblock:new({ max_length = 40 }):newMultiline()
        :add("first line of the comment\n\nsecond line")
        :add("")
        :add("third line")
    local exp = {
      '/**',
      ' * first line of the comment',
      ' *', -- <-- without a trailing whitespace
      ' * second line',
      ' *',
      ' * third line',
      ' */'
    }
    assert.same(exp, doc.lines)
  end)

  it("zip multiline into onliner ", function()
    local doc = Docblock:new():newMultiline():add("oneliner")
    local exp = { '/**', ' * oneliner', ' */' }
    assert.same(exp, doc.lines)
    doc:zip()
    local exp_after_zip = { '/** oneliner */' }
    assert.same(exp_after_zip, doc.lines)
  end)

  it("zip multiline into onliner ", function()
    local lines = {
      '    /** this is a comment of the some generated code/',
      '    public static String name;'
    }
    local doc = Docblock:new({ tab = '    ' }):newMultiline(lines)
        :add("one-line-comment")

    local exp_before_zip = {
      '    /** this is a comment of the some generated code/',
      '    public static String name;',
      '    /**',
      '     * one-line-comment',
      '     */'
    }
    assert.same(exp_before_zip, doc.lines)

    doc:zip()
    local exp = {
      '    /** this is a comment of the some generated code/',
      '    public static String name;',
      '    /** one-line-comment */'
    }
    assert.same(exp, doc.lines)
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')
local LangGen = require 'env.lang.oop.LangGen'
local ClassInfo = require("env.lang.ClassInfo")
---@diagnostic disable-next-line: unused-local
local ComponentGen = require("env.lang.oop.ComponentGen")
local ClassGen = require("env.lang.oop.ClassGen")
local MethodGen = require("env.lang.oop.MethodGen")
local FieldGen = require("env.lang.oop.FieldGen")

describe('env.lang.oop.LangGen', function()
  describe('get', function()
    local lg = LangGen:new()
    local ci = ClassInfo:new({ classname = 'pkg.MyClass' })

    it("ClassGen", function()
      local cg = lg:get(ClassGen, ci)
      assert.is_not_nil(cg)
      assert.same(ClassGen, cg:getClass())
      assert.same(ci, cg:getClassInfo()) -- passed classinfo via get
      assert.same('env.lang.oop.ClassGen', cg:getClassName())
      --
      assert.same(cg, lg:getClassGen(ci))
    end)

    it("MethodGen", function()
      local mg = lg:get(MethodGen, ci)
      assert.is_not_nil(mg)
      assert.same(MethodGen, mg:getClass())
      assert.same(ci, mg:getClassInfo())
      assert.same('env.lang.oop.MethodGen', mg:getClassName())
      --
      assert.same(mg, lg:getMethodGen(ci))
    end)

    it("FieldGen", function()
      local fg = lg:get(FieldGen, ci)
      assert.is_not_nil(fg)
      assert.same(FieldGen, fg:getClass())
      assert.same(ci, fg:getClassInfo())
      assert.same('env.lang.oop.FieldGen', fg:getClassName())
      --
      assert.same(fg, lg:getFieldGen(ci))
    end)
  end)
end)

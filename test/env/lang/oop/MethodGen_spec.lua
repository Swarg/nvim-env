--
require 'busted.runner' ()
local assert = require('luassert')
local H = require("env.lang.oop.stub.ATestHelper")

local AMods = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line: unused-local
local ClassInfo = require("env.lang.ClassInfo")
---@diagnostic disable-next-line: unused-local
local MethodGen = require("env.lang.oop.MethodGen")
local Field = require("env.lang.oop.Field")
local Method = require("env.lang.oop.Method")
--
---@diagnostic disable-next-line: unused-local
local StubLangGen = require("env.lang.oop.stub.StubLangGen")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

--- HELPERS

describe('helpers', function()
  it("myClass ", function()
    local ci, gen, lang = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.is_not_nil(gen)
    assert.is_not_nil(lang)
    assert.same("env.lang.oop.stub.StubLangGen", gen:getClassName())
    assert.same('MyClass', ci:getShortClassName())
    assert.same('src/org/pkg/MyClass', ci:getInnerPath())
    assert.same('/tmp/app/src/org/pkg/MyClass.stub', ci:getPath())
    assert.same('/tmp/app/', lang:getProjectRoot())
  end)
end)

describe('env.lang.oop.MethodGen', function()
  it("buildParams", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    local fields = { name = H.mkField(gen, 'name'), id = H.mkField(gen, 'id') }
    local m = Method:new({ params = fields })
    assert.same("String name, String id", mg:buildParams(m))
  end)

  it("genGetter", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    assert.same(ci, mg:getClassInfo())

    local getter1 = mg:genGetter(H.mkField(gen, 'name'))
    local exp = {
      amod = H.AM.PUBLIC,
      mtype = H.MT.GETTER,
      name = "getName",
      rettype = "String"
    }
    assert.is_not_nil(getter1)
    ---@diagnostic disable-next-line: need-check-nil
    assert.same(exp, getter1:toArray())

    local getter2 = mg:genGetter(H.mkField(gen, 'userId', 'Id'))
    exp = {
      amod = H.AM.PUBLIC,
      mtype = H.MT.GETTER,
      name = "getUserId",
      rettype = "Id"
    }
    ---@diagnostic disable-next-line: need-check-nil
    assert.same(exp, getter2:toArray())
  end)

  it("genGetters", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)

    local fields = { name = H.mkField(gen, 'name'), id = H.mkField(gen, 'id') }
    local res = Method.toListOfArrays(mg:genGetters(fields))
    local exp = {
      { amod = H.AM.PUBLIC, mtype = H.MT.GETTER, name = "getName", rettype = "String" },
      { amod = H.AM.PUBLIC, mtype = H.MT.GETTER, name = "getId",   rettype = "String" },
    }
    assert.same(exp, res)

    res = mg:genGetter(H.mkField(gen, 'userId', 'Id')):toArray()
    exp = { amod = AMods.ID.PUBLIC, mtype = H.MT.GETTER, name = "getUserId", rettype = "Id" }
    assert.same(exp, res)
  end)

  describe('Constructor', function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)

    local fields = {
      Field:new({ amod = AMods.ID.PRIVATE, type = 'String', name = 'id' }),
      Field:new({ amod = AMods.ID.PRIVATE, type = 'String', name = 'name' })
    }
    local methodGen = gen:getMethodGen(ci)
    --
    it("ensure ClassInfo:getShortClassName works", function()
      assert.same('MyClass', ci:getShortClassName())
    end)

    it("genConstructor and genConstructors", function()
      local constructor_exp = {
        amod = AMods.ID.PUBLIC,
        mtype = Method.TYPE.CONSTRUCTOR,
        name = "MyClass",
        params = {
          { amod = 1, name = "id",   type = "String" },
          { amod = 1, name = "name", type = "String" }
        },
        rettype = "void"
      }
      -- opts to generate
      local opts = { params = fields }
      -- one
      local res = methodGen:genConstructor(opts)
      assert.is_not_nil(res)
      assert.same(constructor_exp, res:toArray())
      -- many
      local constructors = methodGen:genConstructors({ opts, opts })
      local many_exp = { constructor_exp, constructor_exp }
      assert.same(many_exp, Method.toListOfArrays(constructors))
    end)
  end)

  it("genGetter toCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    -- will take the default type(String) since
    -- the type is not explicitly specified here
    local field = H.mkField(gen, 'name')
    local fexp = { type = 'String', name = 'name', amod = H.AM.PRIVATE }
    assert.same(fexp, field:toArray())

    local m = mg:genGetter(field)
    local exp = [[
    public String getName()
    {
        return this.name;
    }
]]
    assert.same(exp, mg:toCode(m))
  end)

  it("genGetter toCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    local m = mg:genGetter(H.mkField(gen, 'id', 'Id'))
    local exp = [[
    public Id getId()
    {
        return this.id;
    }
]]
    assert.same(exp, mg:toCode(m))
  end)

  it("genSetterCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    local m = mg:genSetter(H.mkField(gen, 'name', 'UserName'))
    local exp = [[
    public void setName(UserName name)
    {
        this.name = name;
    }
]]
    assert.same(exp, mg:toCode(m))
  end)

  it("genIsEquals toCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    assert.is_not_nil(mg:getClassInfo())

    local fields = { H.mkField(gen, 'name', 'Name'), H.mkField(gen, "id", "Id") }
    local m = mg:genIsEqualTo(fields)
    local exp = [[
    public boolean isEqualTo(MyClass myClass)
    {
        return
            this.name == myClass.name &&
            this.id == myClass.id;
    }
]]
    assert.same(exp, mg:toCode(m))
  end)

  it("genTestCode", function()
    local ci, gen = H.mkClassInfoAndLangGen0(H.myClassName)
    local mg = gen:getMethodGen(ci)
    local m = mg:genTest('testSuccess', false, 'body')
    local exp = "    public void testSuccess()\n    {\nbody\n    }\n"
    assert.same(exp, mg:toCode(m))
  end)
end)

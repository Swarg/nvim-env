--
require 'busted.runner' ()
local assert = require('luassert')
local H = require("env.lang.oop.stub.ATestHelper")
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local class = require("oop.class")

local AMods = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line: unused-local
local Class = require("env.lang.oop.Class")
local Method = require("env.lang.oop.Method")

describe('env.lang.oop.ClassGen', function()
  it("is helpers ok", function()
    local ci, gen, lang = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.is_not_nil(ci)
    assert.is_not_nil(gen)
    assert.is_not_nil(lang)
    assert.same(lang, gen:getLang())
    -- assert.same(gen, lang:getLangGen()) -- each time new instance
    --                       ^^^ for now it issues a new instance for every call
    assert.same("env.lang.oop.stub.StubLangGen", gen:getClassName())
    assert.same('MyClass', ci:getShortClassName())
    assert.same('src/org/pkg/MyClass', ci:getInnerPath())
    assert.same('/tmp/app/src/org/pkg/MyClass.stub', ci:getPath())
    -- pr is /app
  end)

  it("newClass", function()
    local ci, gen, _ = H.mkClassInfoAndLangGen0(H.myClassName)
    local cg = gen:getClassGen(ci)
    local klass = cg:newClass()
    local exp = { name = 'MyClass', package = 'org.pkg' }
    assert.same(exp, klass:toArray())
  end)

  it("gsub no nl", function()
    local templ = "WORD"
    local t = { WORD = "123" }
    local res = string.gsub(templ, "[%w%._]+", t)
    assert.same('123', res)
  end)

  it("gsub with nl", function()
    local templ = "WORD"
    local t = { WORD = "\n\nabc\ndef" }
    local res = string.gsub(templ, "[%w%._]+", t)
    assert.same("\n\nabc\ndef", res)
  end)

  --
  -- Generate Class from given params:
  --   - fields, constructor, methods
  --
  describe('Gen MyClass', function()
    local ci, gen, lang = H.mkClassInfoAndLangGen0(H.myClassName)
    assert.is_not_nil(lang)

    -- opts to define class
    local opts = { no_date = true, author = 'A' }
    opts.fields = { name = H.mkField(gen, 'name'), id = H.mkField(gen, 'id') }
    opts.constructor = true
    opts.object_value = true
    --
    local cg = gen:getClassGen(ci):setOpts(opts)
    local PRIVATE, PUB = AMods.ID.PRIVATE, AMods.ID.PUBLIC
    local GETTER = Method.TYPE.GETTER
    --

    it("check helpers & getClassGen", function()
      assert.is_not_nil(cg:getLangGen())
      assert.same('env.lang.oop.stub.StubLangGen', class.name(cg:getLangGen()))
      assert.same('env.lang.oop.stub.StubLangGen', cg:getLangGen():getClassName())
    end)

    it("genClass 1", function()
      local klass = cg:genClass(opts)
      assert.is_not_nil(klass)

      local res = klass:toArray()
      -- print("[DEBUG] res:", inspect(res))
      local exp =
      {
        name = 'MyClass',
        package = 'org.pkg',
        fields = {
          name = { amod = PRIVATE, name = "name", type = "String" },
          id = { amod = PRIVATE, name = "id", type = "String" }
        },
        methods = {
          {
            amod = AMods.ID.PUBLIC,
            mtype = Method.TYPE.CONSTRUCTOR,
            name = "MyClass",
            params = {
              id = { amod = PRIVATE, name = "id", type = "String" },
              name = { amod = PRIVATE, name = "name", type = "String" }
            },
            rettype = "void"
          },
          -- isEqualTo
          {
            amod = AMods.ID.PUBLIC,
            mtype = Method.TYPE.EQUALS,
            name = "isEqualTo",
            params = {
              { amod = PRIVATE, name = "myClass", type = "MyClass" }
            },
            rettype = "boolean"
          },
          -- getters
          { amod = GETTER, mtype = PUB, name = "getName", rettype = "String" },
          { amod = GETTER, mtype = PUB, name = "getId",   rettype = "String" }
        }
      }
      assert.same(exp, res)
    end)

    it("class toCode", function()
      local klass = cg:genClass(opts)
      assert.is_not_nil(klass)

      -- print("[DEBUG] res:", inspect(res))
      local res = cg:toCode(klass)
      -- print(res2)
      local exp = [[
package org.pkg;

/**
 *
 *
 * @author A
 */
class MyClass
{

    private String name;
    private String id;

    public MyClass(String name, String id)
    {
        this.name = name;
        this.id = id;
    }

    public boolean isEqualTo(MyClass myClass)
    {
        return
            this.name == myClass.name &&
            this.id == myClass.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getId()
    {
        return this.id;
    }

}
]]
      assert.same(exp, res)
    end)
  end)
end)

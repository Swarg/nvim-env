--
require 'busted.runner' ()
local assert = require('luassert')
local DockerCompose = require 'env.lang.DockerCompose'

local R = require 'env.require_util'
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

describe('env.lang.DockerCompose', function()
  it("getArgsToRunService", function()
    local dc = DockerCompose:new()
    local exp = { "compose", "run", "--rm", "api-php-cli", "phpunit", "Test.php" }
    local res = dc:getArgsToRunService('api-php-cli', 'phpunit', { 'Test.php' })
    assert.same(exp, res)
  end)

  it("_joinArgs", function()
    local out, name, envs = { 'compose', 'run' }, 'service', { 'A=1', 'B=9' }
    local cmd, args = 'ls', { '-l', '-h' }
    local exp = { "compose", "run", "-e", "A=1", "-e", "B=9", "service",
      "ls", "-l", "-h"
    }
    assert.same(exp, DockerCompose._joinArgs(out, name, envs, cmd, args))
  end)
end)

describe('env.lang.DockerCompose auction', function()
  local auction_root = uv.cwd() .. '/test/env/resources/php/auction/'
  local api_root = uv.cwd() .. '/test/env/resources/php/auction/api/'
  -----------------------------------------------------------------------------

  describe('env.lang.DockerCompose auction', function()
    local ymlfile = auction_root .. 'docker-compose.yml'
    local dc = DockerCompose:new()

    it("parseYaml0", function()
      assert.is_table(DockerCompose.parseYaml0(ymlfile))
      assert.is_nil(DockerCompose.parseYaml0('not-existed-file'))
    end)

    it("parseYaml", function()
      assert.same(true, dc:parseYaml(ymlfile))
      dc.workdir = auction_root -- from lookup
      assert.is_not_nil(dc.yml)
    end)

    it("getService", function()
      local api = dc:getService('api-php-cli') --dc.yml[1].services['api-php-cli']
      assert.is_not_nil(api)
      assert.same({ "./api:/app" }, api.volumes)
    end)

    it("getServicesNames", function()
      local exp = { 'frontend', 'api', 'api-php-fpm', 'api-php-cli', 'gateway' }
      assert.same(exp, dc:getServicesNames())
    end)

    it("findServiceName", function()
      assert.same("api-php-cli", dc:findServiceName('api', 'php', 'cli'))
      assert.same("api", dc:findServiceName('api'))
      assert.same("frontend", dc:findServiceName('frontend'))
      assert.is_nil(dc:findServiceName('frontend', 'no!'))
      assert.is_nil(dc:findServiceName('frontend', 'no!', 'no!'))
      assert.same("api-php-fpm", dc:findServiceName('api', 'php'))
      assert.same("api-php-fpm", dc:findServiceName('api-php-fpm'))
      assert.same("api-php-fpm", dc:findServiceName('api-php', 'fpm'))
      assert.same("api-php-fpm", dc:findServiceName('api', 'php', 'fpm'))
      assert.same("gateway", dc:findServiceName('gat', 'way'))
      -- need one char between words:
      assert.is_nil(dc:findServiceName('gate', 'way'))
      assert.is_nil(dc:findServiceName('gat', 'eway'))
      -- no overlap
      assert.is_nil(dc:findServiceName('gatew', 'way'))
    end)

    it("getVolumesPathMappings", function()
      local service_name = 'api-php-cli'
      assert.same(auction_root, dc.workdir)
      -- inside-container  --> realpath
      local exp = { ['/app'] = auction_root .. 'api' }
      assert.same(exp, dc:getVolumesPathMappings(service_name))
    end)
  end)

  it("lookup", function()
    local dc = DockerCompose:new()
    assert.same(dc, dc:lookup(api_root))
  end)

  --[[ all=true
  {
    {
      services = {
        api = {
          build = {
            context = "api/docker",
            dockerfile = "development/nginx/Dockerfile"
          },
          depends_on = { "api-php-fpm" },
          volumes = { "./api:/app" }
        },
        ["api-php-cli"] = {
          build = {
            context = "api/docker",
            dockerfile = "development/php-cli/Dockerfile"
          },
          volumes = { "./api:/app" }
        },
        ["api-php-fpm"] = {
          build = {
            context = "api/docker",
            dockerfile = "development/php-fpm/Dockerfile"
          },
          environment = {
            APP_DEBUG = 1,
            APP_ENV = "dev",
            PHP_IDE_CONFIG = "serverName=API"
          },
          volumes = { "./api:/app" }
        },
        frontend = {
          build = {
            context = "frontend/docker",
            dockerfile = "development/nginx/Dockerfile"
          },
          volumes = { "./frontend:/app" }
        },
        gateway = {
          build = {
            context = "gateway/docker",
            dockerfile = "development/nginx/Dockerfile"
          },
          depends_on = { "frontend", "api" },
          ports = { "${PORT_FRONTEND}:8080", "${PORT_API}:8081" }
        }
      },
      version = "3.7"
    }
  }
]]
end)

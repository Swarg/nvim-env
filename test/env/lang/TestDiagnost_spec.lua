-- 06-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local TestDiagnost = require 'env.lang.TestDiagnost'

describe("env.lang.TestDiagnost", function()
  it("new instance", function()
    -- given
    local lang = { project_root = '/home/dev/' } ---@cast lang env.lang.Lang
    local obj = TestDiagnost:new(nil, lang, 'X')

    assert.is_table(obj)
    assert.same(2, obj.namespace_id)
    assert.same('NVIM_ENV_X', obj.namespace)
    assert.same(lang, obj.lang)
  end)
end)

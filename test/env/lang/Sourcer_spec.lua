-- 18-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local fs = require 'env.files'
local Sourcer = require 'env.lang.Sourcer'
local StubLang = require("env.lang.oop.stub.StubLang")

describe("env.lang.Sourcer", function()
  it("apply_pkg_mapping", function()
    local f = Sourcer.test.apply_pkg_mapping
    local map = {
      ['/pkg/app/'] = '/pkg/cursomapp/'
    }

    local inner_path = 'src/main/java/pkg/app/entity/User.java'
    local exp_i_path = 'src/main/java/pkg/cursomapp/entity/User.java'
    assert.same(exp_i_path, f(map, inner_path))
  end)

  it("getInnerPath", function()
    local lang = StubLang:new(nil, '/path/to/project/')
    local sourcer = Sourcer:new(nil, lang)
    local path = '/path/to/project/src/pkg/File.ext'
    assert.equal(lang, sourcer.lang)
    assert.same({ 'src/pkg/File', nil, 'ext' }, { sourcer:getInnerPath(path) })
  end)

  it("updateFileContent", function()
    local sourcer = Sourcer:new(nil, StubLang:new(nil, '/path/to/project/'))
    local fn = os.tmpname()
    local lines = {
      'line 1',
      'line 2',
      'line 3',
      'line 4',
      'line 5',
    }
    fs.write(fn, table.concat(lines, "\n") .. "\n")
    local mapping = {
      [2] = { 'LINE 2!' },
      [3] = { 'line 3', 'effect of insert new line' },
      [5] = false, -- to delete the line
      append = { 'new line to append ', 'to the end of the file' }
    }
    assert.same(true, sourcer:updateFileContent(fn, mapping))
    local exp = {
      'line 1',
      'LINE 2!',
      'line 3',
      'effect of insert new line',
      'line 4',
      'new line to append ',
      'to the end of the file'
    }
    assert.same(exp, fs.read_lines(fn))
    os.remove(fn)
  end)

  it("saveRenamePackageMapping", function()
    local sourcer = Sourcer:new(nil, StubLang:new(nil, '/path/to/project/'))
    local fn = os.tmpname()
    local map = sourcer:getRenamePackageMapping()
    map["some.old.package.ClassA"] = "some.new.package.ClassA"
    map["pkg.old.ClassB"] = "pkg.old.ClassB"
    map["pkg.old.ClassC"] = "pkg.old.ClassC"
    local res, err = sourcer:saveRenamePackageMapping(fn, true)
    assert.same(nil, err)
    assert.same(3, res)
    local savedResult = fs.read_lines(fn)
    local exp = {
      'pkg.old.ClassB:pkg.old.ClassB',
      'pkg.old.ClassC:pkg.old.ClassC',
      'some.old.package.ClassA:some.new.package.ClassA'
    }
    assert.same(exp, savedResult)

    sourcer:clearRenamePackageMapping()
    map = sourcer:getRenamePackageMapping()
    assert.same(nil, map["pkg.old.ClassC"])
    local merge_with_existed = true
    local res2, err2 = sourcer:loadRenamePackageMapping(fn, merge_with_existed)
    assert.same(nil, err2)
    assert.same(3, res2)
  end)

  it("parse_git_status_rename_line", function()
    local f = Sourcer.test.parse_git_status_rename_line
    local line = " renamed:    src/main/java/org/old/MyClass.java -> src/main/java/org/new/pkg/MyClass.java"
    local exp = {
      'src/main/java/org/old/MyClass.java',
      'src/main/java/org/new/pkg/MyClass.java'
    }
    assert.same(exp, { f(line) })
  end)
end)

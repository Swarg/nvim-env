-- 20-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim

local M = require 'env.lang.utils.samples'

describe("env.lang.utils.samples nvim", function()
  before_each(function()
    nvim:clear_state()
  end)

  it("handler_add_sample", function()
    local lines = {
      '.',
      'planets::2-4', -- 2 cursor here
      '.',
    }
    local lnum = 2
    local bufnr = nvim:new_buf(lines, lnum, 1)

    local _, err = M.handler_add_sample()
    assert.is_nil(err)

    local exp = {
      '.',
      'Venus',
      'Earth',
      'Mars',
      '.'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)
end)

describe("env.lang.utils.samples", function()
  local words = { "Aspen", "Birch", "Cedar", "Maple", "Oak" }
  it("wrap_to_html list", function()
    local exp = {
      '  <ol>',
      '    <li>Aspen</li>',
      '    <li>Birch</li>',
      '    <li>Cedar</li>',
      '    <li>Maple</li>',
      '    <li>Oak</li>',
      '  </ol>'
    }
    assert.same(exp, M.wrap_to_html(words, 'ol'))

    local exp2 = {
      '  <ul>',
      '    <li>Aspen</li>',
      '    <li>Birch</li>',
      '    <li>Cedar</li>',
      '    <li>Maple</li>',
      '    <li>Oak</li>',
      '  </ul>'
    }
    assert.same(exp2, M.wrap_to_html(words, 'ul'))
  end)


  it("wrap_to_html link", function()
    local exp = {
      '  <div>',
      '    <a hred="/Aspen">Aspen</a>',
      '    <a hred="/Birch">Birch</a>',
      '    <a hred="/Cedar">Cedar</a>',
      '    <a hred="/Maple">Maple</a>',
      '    <a hred="/Oak">Oak</a>',
      '  </div>'
    }
    assert.same(exp, M.wrap_to_html(words, 'a'))
  end)

  it("wrap_to_html p", function()
    local exp = {
      '  <div>',
      '    <p>Aspen</p>',
      '    <p>Birch</p>',
      '    <p>Cedar</p>',
      '    <p>Maple</p>',
      '    <p>Oak</p>',
      '  </div>'
    }
    assert.same(exp, M.wrap_to_html(words, 'p'))
  end)

  it("wrap_to_html span", function()
    local exp = {
      '  <div>',
      '    <span>Aspen</span>',
      '    <span>Birch</span>',
      '    <span>Cedar</span>',
      '    <span>Maple</span>',
      '    <span>Oak</span>',
      '  </div>'
    }
    assert.same(exp, M.wrap_to_html(words, 'span'))
  end)
end)

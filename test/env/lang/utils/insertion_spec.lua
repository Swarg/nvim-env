-- 15-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.lang.utils.insertion");

describe("env.lang.utils.insertion", function()
  it("update_mappings for lua param", function()
    local mappings = { -- M.insert_line_mappings.lua[2]
      -- @param type name
      table = { 'obj' },
      ['function'] = { 'callback' },
      number = { 'n', ' i', 'bufnr', 'lnum', 'lnum_end', 'winnr', 'offset',
        'off', 'fd',
      },
    }

    assert.same('table', M.map_word(mappings, 'obj'))
    assert.same(true, M.update_mappings(mappings, 'obj', 'Cmd4Lua'))
    assert.same('Cmd4Lua', M.map_word(mappings, 'obj'))

    assert.is_nil(M.map_word(mappings, 'not-existed-key'))
    assert.same(true, M.update_mappings(mappings, 'not-existed-key', 'X'))
    assert.same('X', M.map_word(mappings, 'not-existed-key'))

    assert.same(true, M.update_mappings(mappings, 'not-existed-key', 'nil'))
    assert.is_nil(M.map_word(mappings, 'not-existed-key'))
  end)

  it("get_subscriber_index", function()
    local f = M.get_subscriber_index
    local f1 = function() return 1 end
    local f2 = function() return 2 end
    local f3 = function() return 3 end
    local t = { f1, f2, f3 }
    assert.same(1, f(f1, t))
    assert.same(2, f(f2, t))
    assert.same(3, f(f3, t))

    assert.is_nil(f(function() end, t))
  end)

  it("append_line", function()
    local f = M.append_line
    assert.same({ '' }, f({}, ""))
    assert.same({ 'abc' }, f({}, "abc"))
    assert.same({ 'abc', 'de' }, f({}, "abc\nde"))
    assert.same({ 'abc', 'de', '' }, f({}, "abc\nde\n"))
    assert.same({ 'abc', '', 'de', '' }, f({}, "abc\n\nde\n"))
  end)

  it("MD_TITLE", function()
    local line = 'ex:Some Word'
    local title = line:lower():gsub('[%s:]', '-')
    assert.same('ex-some-word', title)
  end)
end)

--
--
describe("env.lang.utils.insertion stub nvim", function()
  local NVim = require("stub.vim.NVim")
  local nvim = NVim:new() ---@type stub.vim.NVim

  after_each(function()
    nvim:clear_state()
  end)

  it("handle_full_line_insertion", function()
    local line1, line2, lines = 2, 5, {
      '1',            --  0
      '2',            --  1
      'local x = 8',  --  2 <<
      'local y = 10', --  3
      'print(x + y)', --  4 <<
      '6',            --  5
      '7',            --  6
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '```lua\n$LINE\n```'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      selection = { line1 = line1, line2 = line2 }, can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    local exp = {
      '1',
      '2',
      '```lua',
      'local x = 8',
      'local y = 10',
      'print(x + y)',
      '```',
      '6',
      '7'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("handle_full_line_insertion 2", function()
    local line1, line2, lines = 2, 5, {
      '1',            --  0
      '2',            --  1
      'local x = 8',  --  2 <<
      'local y = 10', --  3
      'print(x + y)', --  4 <<
      '6',            --  5
      '7',            --  6
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '<LEFT>$LINE<RIGHT>'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      selection = { line1 = line1, line2 = line2 }, can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    -- todo '<LEFT>local y = 10' ... print<RIGHT>
    local exp = {
      '1',
      '2',
      '<LEFT>',
      'local x = 8',
      'local y = 10',
      'print(x + y)',
      '<RIGHT>',
      '6',
      '7'
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)


  it("handle_full_line_insertion one-line r+l", function()
    local line1, line2, lines = 3, 3, {
      '1',     --  1
      '2',     --  2
      'x = 8', --  3 <<
      '4',     --  4
      '5',     --  5
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '<LEFT>$LINE<RIGHT>'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      current_line = lines[line1],
      selection = { line1 = line1, line2 = line2 },
      can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    local exp = { '1', '2', '<LEFT>x = 8<RIGHT>', '4', '5' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("handle_full_line_insertion one-line l", function()
    local line1, line2, lines = 3, 3, {
      '1',     --  1
      '2',     --  2
      'x = 8', --  3 <<
      '4',     --  4
      '5',     --  5
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '<LEFT>$LINE'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      current_line = lines[line1],
      selection = { line1 = line1, line2 = line2 },
      can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    local exp = { '1', '2', '<LEFT>x = 8', '4', '5' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("handle_full_line_insertion one-line r", function()
    local line1, line2, lines = 3, 3, {
      '1',     --  1
      '2',     --  2
      'x = 8', --  3 <<
      '4',     --  4
      '5',     --  5
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '$LINE<RIGHT>'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      current_line = lines[line1],
      selection = { line1 = line1, line2 = line2 },
      can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    local exp = { '1', '2', 'x = 8<RIGHT>', '4', '5' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("handle_full_line_insertion one-line", function()
    local line1, line2, lines = 3, 3, {
      '1',     --  1
      '2',     --  2
      'x = 8', --  3 <<
      '4',     --  4
      '5',     --  5
    }
    local bufnr = nvim:new_buf(lines, line2, 1)
    vim.api.nvim_set_mode0(0, bufnr, 'v')
    vim.fn.setpos('v', { 0, line1, 0, 0 })

    local insertion = '$LINE'
    local pos = { string.find(insertion, "$LINE") }
    local cbi = {
      current_line = lines[line1],
      selection = { line1 = line1, line2 = line2 },
      can_edit = true
    }

    M.handle_full_line_insertion(cbi, pos, insertion)

    local exp = { '1', '2', 'x = 8', '4', '5' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("mk_capitalized_word", function()
    local f = M.mk_capitalized_word
    assert.same('HelloWord', f('hello word'))
    assert.same('OneTwo tree', f('one two tree'))

    assert.same('Hello_Word', f('hello word', '_'))
    assert.same('Hello Word', f('hello word', ' '))
  end)

  it("get_vim_command", function()
    local f = M.get_vim_command
    assert.same({ 'EnvNew', 'test-method .' }, { f(':EnvNew test-method .<CR>') })
    assert.same({ 'EnvNew', '' }, { f(':EnvNew<CR>') })
    assert.same({}, { f(':EnvNew') })
    assert.same({}, { f(':envNew<CR>') })
  end)
end)

-- 16-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.lang.utils.templater'

describe("env.lang.utils.template", function()
  it("substitute", function()
    -- local c = ComponentGen():new()
    local templ, subs = "${KEY0}${key1}${2}", nil
    subs = { KEY0 = "A", key1 = "B", ['2'] = 'C' }
    assert.same('ABC', M.substitute(templ, subs))

    subs = { KEY0 = "A", ['2'] = 'C' }
    assert.same('A${key1}C', M.substitute(templ, subs))

    subs = { KEY0 = "A", key1 = false, ['2'] = 'C' }
    assert.same('AC', M.substitute(templ, subs))

    subs = { KEY0 = "A", key1 = '', ['2'] = 'C' }
    assert.same('AC', M.substitute(templ, subs))
  end)

  it("match + gsub to replace whole line for empty key", function()
    local p = "(% *%${VN}\n)"
    local line = "a\n  ${VN}\nNext"
    assert.same("  ${VN}\n", string.match(line, p))
    assert.same("a\n-Next", string.gsub(line, p, '-'))
    assert.same("  A\n  B\n", string.gsub("  A\n   \n  B\n", "% +\n", ''))
    assert.same("  A\n  B\n ", string.gsub("  A\n   \n  B\n ", "% +\n", ''))
    assert.same("  A\n   .\n  B", string.gsub("  A\n   .\n  B", "% +\n", ''))
  end)

  -- remove "empty" key with line (to avoid leaving empty lines)
  it("substitute", function()
    local templ = "start\n ${FIELDS}\n ${MET}\nend${E}${A}"
    local t = { ["FIELDS"] = false, ["MET"] = false, ["E"] = "", ["A"] = "X" }
    assert.same("start\nendX", M.substitute(templ, t))

    local templ2 = "start\n${FIELDS}\n${MET}\nend${E}${A}"
    assert.same("start\nendX", M.substitute(templ2, t))
  end)

  it("substitute upperkey", function()
    local templ = "app: ${APP_NAME} with version: ${VERSION}"
    local t = { app_name = 'my_app', version = '0.1.0' }
    local exp = 'app: my_app with version: 0.1.0'
    assert.same(exp, M.substitute(templ, t, true))

    local t2 = { APP_NAME = 'my_app', VERSION = '0.1.0' }
    assert.same(exp, M.substitute(templ, t2, true))

    local t3 = { APP_NAME = 'my_app', VERSION = '0.1.0' }
    assert.same(exp, M.substitute(templ, t3, false))

    local exp2 = 'app: ${APP_NAME} with version: ${VERSION}'
    assert.same(exp2, M.substitute(templ, t, false))
  end)

  it("substitute remove empty lines", function()
    local templ = "  ${APP_NAME}\n  ${VERSION}\n"
    local t = { app_name = 'my_app', version = '0.1.0' }
    assert.same("  my_app\n  0.1.0\n", M.substitute(templ, t, true))

    t = { app_name = '', version = '0.1.0' }
    assert.same("  0.1.0\n", M.substitute(templ, t, true))

    t = { app_name = '', version = '' }
    assert.same('', M.substitute(templ, t, true))
  end)

  it("substitute deep-1 (flat recursion)", function()
    local f = M.substitute
    local templ = "  ${MAINCLASS}\n  ${PLUGINS}\n"
    local t = { mainclass = 'pkg.Main', plugins = '<plug>${MAINCLASS}</plug>' }
    assert.same("  pkg.Main\n  <plug>pkg.Main</plug>\n", f(templ, t, true))
  end)

  it("append", function()
    local f = M.append
    local t = { key1 = 'abc', key2 = '' }
    assert.same('abc def', f(t, 'key1', 'def', ' '))
    assert.same("abc def\nghi", f(t, 'key1', 'ghi'))
    assert.same('123', f(t, 'key2', '123'))
    assert.same("123\n45", f(t, 'key2', '45'))
    assert.same("123\n45x67", f(t, 'key2', '67', 'x'))
  end)

  it("set_if_empty", function()
    local f = M.set_if_empty
    local t = { a = '', c = nil }
    assert.same({ a = '123' }, f(t, 'a', '123'))
    assert.same({ a = '123', c = '4' }, f(t, 'c', '4'))
    assert.same({ a = '123', c = '4' }, f(t, 'c', '8'))
    assert.same({ a = '123', c = '4' }, f(t, 'a', '-'))
  end)
end)

-- 05-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.lang.utils.common'

describe("env.lang.utils.common", function()
  it("short_dep_name", function()
    local f = M.short_dep_name
    assert.same('sbd', f('spring-boot-devtools'))
    assert.same('tes5', f('thymeleaf-extras-springsecurity5'))
    assert.same('ae5', f('abc-extras-5'))
    assert.same('ae51', f('abc-extras-51'))
    assert.same('aea51', f('abc-extras-a51'))
    assert.same('aea', f('abc-extras-a51z'))
  end)

  it("resolve_deps_names", function()
    local map = {
      ['spring-boot-devtools'] = {
        groupId = "org.springframework.boot",
        artifactId = "spring-boot-devtools",
      }
    }
    local exp = {
      {
        groupId = 'org.springframework.boot',
        artifactId = 'spring-boot-devtools'
      }
    }
    assert.same(exp, M.pick_deps_by_names(map, { 'sbd' }))
    assert.same(exp, M.pick_deps_by_names(map, { 'spring-boot-devtools' }))
  end)
end)

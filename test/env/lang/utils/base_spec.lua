-- 12-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

describe("env.lang.utils.base and stub.os.OSystem", function()
  local OS = require 'stub.os.OSystem'
  local sys = OS:new()
  local M = require("env.lang.utils.base");
  local dir, bn = '/home/user/project_a/', 'README.md'
  sys:mk(dir, bn)
  assert.same('file', sys:file_type(dir .. bn))
  local fn1 = dir .. 'new.txt'
  local code = M.saveFile(fn1, 'content')
  assert.same(M.OK_SAVED, code)
  assert.same("content\n", sys:get_file_content(fn1))
  -- print(sys:tree())
end)


describe("env.lang.utils.base", function()
  local M = require("env.lang.utils.base");

  it("is_empty_str", function()
    local f = M.is_empty_str
    assert.same(true, f(''))
    assert.same(true, f(' '))
    assert.same(true, f('  '))
    assert.same(true, f("\n"))
    assert.same(false, f(" x"))
    assert.same(false, f("."))
    assert.same(false, f("abc"))
  end)

  it("getTodayDayMonthYear", function()
    assert.same({ 18, 11, 2024 }, { M.getTodayDayMonthYear('18-11-2024') })
    local ndate = function(part)
      return tonumber(os.date(part)) -- '%d'
    end
    local d, m, y = ndate('%d'), ndate('%m'), ndate('%Y')
    assert.is_number(d)
    assert.is_number(m)
    assert.is_number(y)
    assert.same({ d, m, y }, { M.getTodayDayMonthYear(nil) })
  end)

  it("toPascalCase", function()
    assert.same('Module', M.toPascalCase('module'))
    assert.same('Module', M.toPascalCase('_module'))
    assert.same('AModule', M.toPascalCase('a_module'))
    assert.same('A', M.toPascalCase('a_'))
    assert.same('ABC', M.toPascalCase('a_b_c'))
    assert.same('SomeModule', M.toPascalCase('some_module'))
    assert.same('SomeModuleOne', M.toPascalCase('some_module_one'))
    assert.same('SomeModuleTwoFree', M.toPascalCase('some_module_two_free'))

    assert.same('ModuleI1', M.toPascalCase('module_i1'))
    assert.same('ModuleI1', M.toPascalCase('module_i_1'))

    -- todo:
    assert.same('Ns/someModule', M.toPascalCase('ns/some_module'))
  end)

  it("toPascalCase no dup _", function()
    assert.same('SomeModule', M.toPascalCase('some__module'))
    assert.same('SomeModule', M.toPascalCase('some___module'))

    assert.same('some_module', M.pascalCaseToSnake('Some_Module'))
    assert.same('some__module', M.pascalCaseToSnake('Some__Module'))
  end)

  it("pascalCaseToSnake", function()
    local f = M.pascalCaseToSnake
    assert.same('module', f('module'))
    assert.same('mo_dule', f('moDule'))
    assert.same('module', f('Module'))
    assert.same('modul_e', f('ModulE'))
    assert.same('a_module', f('AModule'))
    assert.same('name_space.module', f('NameSpace.Module'))
    assert.same('name_space_module', f('NameSpace_Module'))
    assert.same('a', f('A'))
    assert.same('a_b_c', f('ABC'))
    assert.same('some_module', f('SomeModule'))
    assert.same('some_module_one', f('SomeModuleOne'))
    assert.same('some_module_two_free', f('SomeModuleTwoFree'))

    assert.same('module_i1', f('ModuleI1'))

    -- issues:
    assert.same('mo_ _dule', f('mo Dule'))
  end)

  it("get_value_from_args", function()
    local f = M.get_value_from_args
    local cmd = { 'java', '-Dlog.level=ALL', '-data', '~/workspace/myproject' }
    assert.same('ALL', f(cmd, '-Dlog.level'))
    assert.same('~/workspace/myproject', f(cmd, '-data'))
  end)
end)

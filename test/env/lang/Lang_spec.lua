--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os()

local Lang = require 'env.lang.Lang'
local Builder = require 'env.lang.Builder'
local ClassInfo = require 'env.lang.ClassInfo'
local ClassResolver = require('env.lang.ClassResolver')
local StubLang = require 'env.lang.oop.stub.StubLang'

local pcache = require 'env.cache.projects'


describe('env.lang.Lang', function()
  it("_init pass project_root only", function()
    assert.same('/tmp/project/', Lang:new(nil, '/tmp/project'):getProjectRoot())
  end)

  it("_init pass project_root & builder", function()
    local builder = Builder:new({ buildscript = 'no' })
    local lang = Lang:new(nil, '/tmp/project', builder)
    assert.same('/tmp/project/', lang:getProjectRoot())
    assert.equal(builder, lang.builder)
    assert.same('/tmp/project/', lang.builder:getProjectRoot())
    assert.same('src/', lang.builder.src)
    assert.same('test/', lang.builder.test)
  end)

  it("getLang", function()
    local f = Lang.getLang

    assert.same({}, Lang.getRegisteredLangs()) -- ensure empty
    Lang.register(StubLang)
    -- stub is a extension for this StubLang
    assert.same({ stub = StubLang }, Lang.getRegisteredLangs()) -- ensure empty

    assert.same(StubLang, f('source.stub'))
    assert.same(StubLang, f('', 'stub'))
    assert.same(StubLang, f('some.conf', 'stub'))
    assert.same(nil, f('some.conf', 'not-registered-ext'))
    local err = 'expected not empty string filename or extension'
    assert.match_error(function() f('', '') end, err)
    assert.match_error(function() f('', nil) end, err)
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.match_error(function() f(nil, nil) end, err)
  end)

  it("getInstanceType", function()
    local obj = StubLang:new()
    assert.same('temporary', Lang.getInstanceType(obj))

    Lang.getActiveProjects()['/tmp/proj'] = obj

    assert.same('active', Lang.getInstanceType(obj))
    assert.same('class env.lang.oop.stub.StubLang', Lang.getInstanceType(StubLang))
  end)

  it("getOpenedProject", function()
    local root0 = '/tmp/proj/'
    local obj = StubLang:new(nil, root0)
    Lang.getActiveProjects()[root0] = obj
    assert.same(obj, Lang.getOpenedProject(root0))

    assert.same(nil, Lang.getOpenedProject(root0 .. 'subdir1'))
    assert.same(obj, Lang.getOpenedProject(root0 .. 'subdir1', true))
  end)

  it("validateUniqueProject", function()
    local dir = '/tmp/project'
    local lang = Lang:new(nil, dir)
    assert.same(true, lang:validateUniqueProject())
    local t = lang:getActiveProjects()
    t[dir .. '/'] = { 'another' }
    assert.match_error(function()
      lang:validateUniqueProject()
    end, 'Attempt to override already existed Project')
  end)

  it("register and getLang", function()
    local LuaLang = require("env.langs.lua.LuaLang")
    Lang.register(LuaLang)
    assert.is_not_nil(LuaLang)
    assert.same(LuaLang, Lang.getLang('file.lua'))
  end)

  it("joinNs", function()
    assert.same("pkg.cn", Lang.joinNs('pkg', 'cn'))
    assert.same("pkg.cn", Lang.joinNs('pkg.', 'cn'))
    assert.same("pkg.cn", Lang.joinNs('pkg', '.cn'))
    assert.same("pkg.cn", Lang.joinNs('pkg.', '.cn'))
    assert.same("pkg.sub.cn", Lang.joinNs('pkg', 'sub.cn'))
  end)

  -- Note: by design Lang must keep project_root ends with /
  it("isPathInProjectRoot", function()
    local pr, fn = "/root/dir/", "/root/dir/inner/subdir/file.e"
    local l = Lang:new({ project_root = pr })
    assert.same(true, l:isPathInProjectRoot(fn))

    l.project_root, fn = "/root/dir", "/root/dir/inner/subdir/file.e"
    assert.same(true, l:isPathInProjectRoot(fn))

    l.project_root, fn = "/root/dir", "/root/dir" -- !
    assert.same(false, l:isPathInProjectRoot(fn))

    l.project_root, fn = "/root/dir", "/root/dir/"
    assert.same(true, l:isPathInProjectRoot(fn))

    l.project_root, fn = "/root/dir", "/root/dira"
    assert.same(false, l:isPathInProjectRoot(fn))

    l.project_root, fn = "/root/di", "/root/dir/a"
    assert.same(false, l:isPathInProjectRoot(fn))
  end)

  it("getInnerPath -- with / in the end", function()
    local l = Lang:new({ project_root = '/dev/project/', ext = 'cpp' })
    local filename = l.project_root .. 'src/org/comp/domain/Class.cpp'
    assert.same("src/org/comp/domain/Class", l:getInnerPath(filename))
    -- Note:
    -- in java src/main/java is not namespace(package) !
    -- in php with PSR-4 src is part of the namespace(APP)
  end)

  it("getInnerPath -- autofix dup slashes", function()
    local l = Lang:new({ project_root = '/dev/project/', ext = 'cpp' })
    local filename = l.project_root .. '/' .. 'src/org/comp/domain/Class.cpp'
    assert.same("src/org/comp/domain/Class", l:getInnerPath(filename))
  end)

  it("getInnerPath -- no / in the end", function()
    local l = Lang:new({ project_root = '/dev/project/', ext = 'cpp' })
    local filename = l.project_root .. '/src/org/comp/domain/Class.cpp'
    assert.same("src/org/comp/domain/Class", l:getInnerPath(filename))
  end)
end)

-------------------------------------------------------------------------------

--
-- Goal CI-cache - reuse already resolved ClassInfo - maps path(classname) to CI
--
describe('env.lang.Lang CIcache', function()
  --
  it("Lang.classes getCI setCI popCI", function()
    local pr, ip = "/home/user/proj/", "src/subdir/file.e"
    local cn, fn = 'subdir.file', pr .. ip

    local lang = Lang:new({ project_root = pr })
    local ci = ClassInfo:new({ path = fn, inner_path = ip, classname = cn })

    assert.is_nil(lang:getCIbyPath(fn))

    assert.is_nil(lang:setCI(ci)) -- no prev ci for given path
    assert.is_table(lang.cimap)
    assert.is_table(lang.cimap.path2ci)
    assert.is_table(lang.cimap.cn2ci)

    assert.same(ci, lang:getCIbyClassName(cn))
    assert.same(ci, lang:getCIbyPath(fn)) -- no prev ci for given path
    -- already do not return self if not relaced by a new one
    assert.is_nil(lang:setCI(ci))

    assert.same(ci, lang:popCIbyPath(fn)) -- remove
    assert.is_nil(lang:getCIbyPath(fn))
    assert.is_nil(lang:getCIbyClassName(cn))
  end)

  it("removeCI one-to-one relation", function()
    local pr = "/home/user/proj/"
    local cn, ip = 'subdir.module', "src/subdir/module.lua"
    local tcn, tip = 'subdir.module_spec', "src/subdir/module_spec.lua"
    local fn, tfn = pr .. ip, pr .. tip


    local lang = Lang:new({ project_root = pr })
    local ci = ClassInfo:new({ path = fn, inner_path = ip, classname = cn })
    local tci = ClassInfo:new({ path = tfn, inner_path = tip, classname = tcn })
    ci:bindPair(tci)
    assert(tci, ci:getPair())
    assert(ci, tci:getPair())

    assert.same(0, lang:removeCI(ci)) -- cache is empty nothing is deleted

    lang:setCI(ci)                    -- add to cache
    lang:setCI(tci)

    assert.same(ci, lang:getCIbyPath(ci.path))   -- fn
    assert.same(tci, lang:getCIbyPath(tci.path)) -- tfn

    assert.same(ci, lang:getCIbyClassName(ci.classname))
    assert.same(tci, lang:getCIbyClassName(tci.classname))

    assert.same(2, lang:removeCI(ci)) -- cache is empty nothing is deleted
  end)

  it("removeCI many-to-one relation, cascade deletion from case", function()
    local pr = "/home/user/proj/"
    local cn, ip = 'subdir.module', "src/subdir/module.lua"
    local tcn, tip = 'subdir.module.', "src/subdir/module/"
    local fn, tfn = pr .. ip, pr .. tip

    local lang = Lang:new({ project_root = pr })

    local sci = ClassInfo:new({ path = fn, inner_path = ip, classname = cn })
    local tcd = ClassInfo:new({ path = tfn, inner_path = tip, classname = tcn })
    sci.type, tcd.type = ClassInfo.CT.SOURCE, ClassInfo.CT.TESTDIR

    sci:bindPair(tcd)
    assert(tcd, sci:getPair())
    assert(sci, tcd:getPair())

    local tcs, cases = {}, {}
    local cr = ClassResolver:new({ lang = lang })
    for i = 1, 4 do
      local name = 'file' .. i
      local tcin = cr:mkTestCaseCI(tcd, name, ClassInfo.CT.TEST, true)
      cases[name] = tcin
      tcs[i] = tcin
    end
    tcd:setTestCases(cases)

    assert.same('subdir.module.file1', tcs[1].classname)
    assert.same('src/subdir/module/file1', tcs[1].inner_path)
    assert.same('/home/user/proj/src/subdir/module/file1', tcs[1].path)
    assert.same(tcd, tcs[1].parent)
    assert.same(sci, tcs[1].pair)      -- all testcases (childs) related with source

    assert.same(0, lang:removeCI(sci)) -- cache is empty nothing is deleted

    lang:setCI(sci)                    -- add to cache
    lang:setCI(tcd)
    for i = 1, 4 do lang:setCI(tcs[i]) end

    assert.same(sci, lang:getCIbyPath(sci.path)) -- fn
    assert.same(tcd, lang:getCIbyPath(tcd.path)) -- tfn

    assert.same(sci, lang:getCIbyClassName(sci.classname))
    assert.same(tcd, lang:getCIbyClassName(tcd.classname))

    assert.same(tcs[1], lang:getCIbyClassName(tcs[1].classname))
    assert.same(tcs[1], lang:getCIbyPath(tcs[1].path))

    -- workload
    assert.same(6, lang:removeCI(sci))
    assert.same({ cn2ci = {}, path2ci = {} }, lang:getCIMap())
  end)
end)

-- with FS emulation
describe('env.lang.Lang FS', function()
  after_each(function()
    NVim.D.disable()
    NVim.logger_off()
    nvim:clear_all_state_with_os()
  end)

  it("isFlatDirPart", function()
    assert.same(true, Lang.isFlatDirPart('file_spec.lua', './', './'))
    assert.same(true, Lang.isFlatDirPart('spec/file_spec.lua', './', './'))
    assert.same(false, Lang.isFlatDirPart('spec/file_spec.lua', './', 'spec'))
    assert.same(true, Lang.isFlatDirPart('pkg/Main.java', './', './'))
    assert.same(false, Lang.isFlatDirPart('./pkg/Main.java', './', './'))
    -- known issue:
    assert.same(false, Lang.isFlatDirPart('./file_spec.lua', './', './'))
  end)

  it("isPathExists and ClassInfo.isPathExists", function()
    local fn, sys = '/home/user/proj/file', nvim:get_os()
    sys:set_file(fn)
    assert.same('file', sys:file_type(fn)) -- sure existed

    local lang = Lang:new({}) ---@cast lang env.lang.Lang

    local ci = ClassInfo:new({ path = fn })
    assert.same(fn, ci:getPath())

    assert.same(0, sys.fs:access_cnt('io.open'))
    assert.same(true, lang:isPathExists(ci))
    -- flag exists added to ci:
    assert.same({ path = fn, exists = true }, ci)
    assert.same(1, sys.fs:access_cnt('io.open'))

    assert.same(true, lang:isPathExists(ci))
    assert.same(2, sys.fs:access_cnt('io.open')) -- not reuse exists flag, ask io

    assert.same(true, lang:isPathExists(ci))
    assert.same(3, sys.fs:access_cnt('io.open'))
  end)

  it("getInnerPathFor", function()
    local pr, fn = "/root/dir/", "/root/dir/inner/subdir/file.e"
    assert.same('inner/subdir/file.e', Lang.getInnerPathFor(pr, fn))

    pr, fn = '/root/dir', "/root/dir/inner/subdir/file.e"
    assert.same('inner/subdir/file.e', Lang.getInnerPathFor(pr, fn))

    pr, fn = '/root/dXXir', "/root/dir/inner/subdir/file.e"
    assert.is_nil(Lang.getInnerPathFor(pr, fn))

    pr, fn = '/root/dI', "/root/dI/inner/subdir/file.e"
    assert.same('inner/subdir/file.e', Lang.getInnerPathFor(pr, fn))

    -- not a full root name!
    pr, fn = '/root/d', "/root/dIR/inner/subdir/file.e"
    assert.is_nil(Lang.getInnerPathFor(pr, fn))

    pr, fn = '/root/dI', "/root/dIR/inner/subdir/file.e"
    assert.is_nil(Lang.getInnerPathFor(pr, fn))
  end)

  it("getPathWithoutSuffix", function()
    local fn, s = "/root/dir/file.e", '.e'
    assert.same('/root/dir/file', Lang.getPathWithoutSuffix(fn, s))

    fn, s = "/root/dir/file_spec.lua", '_spec.lua'
    assert.same('/root/dir/file', Lang.getPathWithoutSuffix(fn, s))

    fn, s = "/root/dir/file_spXec.lua", '_spec.lua'
    assert.same('/root/dir/file_spXec.lua', Lang.getPathWithoutSuffix(fn, s))
  end)
  --------------------------------------------------------------------------------
end)

describe('env.lang.Lang per-directory configs', function()
  after_each(function()
    NVim.D.disable()
    NVim.logger_off()
    nvim:clear_all_state_with_os()
    Lang:clearCache()
  end)
  local per_dir_confs_basename = pcache.get_dir_conf_basename()
  local yml01 = [[
project_roots:
  - sub1
  - sub2
]]

  local yml01t = {
    {
      project_roots = { "sub1", "sub2" }
    }
  }


  it("checkDirConf", function()
    local f = pcache.checkDirConf
    local map = pcache.getDirConfings()
    local dir = '/tmp/project/'
    map[dir] = {
      lmtime = 0,
      conf = { -- yml specific root in separate {}
        {
          project_roots = { "sub1", "sub2" }
        }
      }
    }
    assert.same({ '/tmp/project/', 'Makefile' }, { f('/tmp/project/sub3', dir, 'Makefile') })
    assert.same({ '/tmp/project/sub2' }, { f('/tmp/project/sub2', dir, 'Makefile') })
    assert.same({ '/tmp/project/sub1' }, { f('/tmp/project/sub1', dir, 'Makefile') })
  end)


  it("check FS-emulation tooling", function()
    local dir, sys = '/tmp/workspace/', nvim:get_os()
    local fn = dir .. per_dir_confs_basename
    sys:set_file_content(fn, yml01)
    -- check emulation tooling...
    local h, _ = io.open(fn, "rb")
    local bin
    if h then
      bin = h:read("*a")
      h:close()
    end
    assert.same("project_roots:\n  - sub1\n  - sub2", bin)
  end)

  it("readDirConf", function()
    local dir, sys = '/tmp/workspace/', nvim:get_os()

    local fn = dir .. per_dir_confs_basename
    sys:set_file_content(fn, yml01)

    local exp = {
      lmtime = 1700808888,
      conf = yml01t,
    }

    assert.same(exp, pcache.readDirConf(dir))
  end)


  it("checkDirConf with emulated FS", function()
    local dir, sys = '/tmp/workspace/', nvim:get_os()
    local fn = dir .. per_dir_confs_basename
    sys:set_file_content(fn, yml01)

    local res = pcache.readDirConf(dir)
    local exp = {
      conf = { {
        project_roots = { "sub1", "sub2" }
      } },
      lmtime = 1700808888
    }
    assert.same(exp, res)

    local f = pcache.checkDirConf
    assert.same({ '/tmp/workspace/', 'Makefile' }, { f('/tmp/project/sub3', dir, 'Makefile') })
    assert.same({ '/tmp/workspace/', 'Makefile' }, { f('/tmp/workspace/sub3', dir, 'Makefile') })
    assert.same({ '/tmp/workspace/sub2' }, { f('/tmp/workspace/sub2', dir, 'Makefile') })
    assert.same({ '/tmp/workspace/sub1' }, { f('/tmp/workspace/sub1', dir, 'Makefile') })
  end)


  it("findProjectRoot with emulated FS and per-dir conf", function()
    local dir, sys = '/tmp/workspace/', nvim:get_os()

    local fn = dir .. per_dir_confs_basename
    sys:mk(dir .. '.git')
    sys:set_file_content(fn, yml01)
    local exp = [[
/tmp/workspace/
    .git
    .nvim-env.yml
0 directories, 2 files
]]
    assert.same(exp, sys:tree(dir))
    local lang = Lang:new()
    -- lang.no_cache = true
    assert.same({ '.git' }, lang.root_markers)
    -- require 'alogger'.fast_setup()

    assert.same({}, pcache.getDirConfings())

    local lang0, marker = lang:findProjectRoot('/tmp/workspace/sub1/source.ext')

    -- assert.is_table(exp, Lang.getDirConfings())
    assert.same('/tmp/workspace/sub1/', lang0.project_root)
    assert.is_nil(marker)
    assert.same({ 'sub1', 'sub2' }, pcache.getProjectsWithoutMarkers(dir))
  end)

  it("isProjectWithoutMarkers", function()
    local dir, sys = '/tmp/workspace/', nvim:get_os()
    local pr = dir .. 'sub1/'
    local fn = dir .. per_dir_confs_basename
    sys:mk(dir .. '.git')
    sys:set_file_content(fn, yml01)

    local lang = Lang:new(nil, pr)
    assert.same(false, pcache.isProjectWithoutMarkers(lang.project_root))
    -- lang:findProjectRoot('/tmp/workspace/sub1/source.ext') -- needs emulate dirs
    pcache.readDirConf(dir)
    assert.same('/tmp/workspace/', next(pcache.getDirConfings()))
    assert.same(true, pcache.isProjectWithoutMarkers(lang.project_root))
  end)

  it("findOpenedProjectByPath", function()
    local pr1 = '/tmp/project/'
    local pr2 = '/tmp/projectdir/'
    local projects = Lang.getActiveProjects()
    projects[pr1] = { project_root = pr1 }
    projects[pr2] = { project_root = pr2 }
    local f = Lang.findOpenedProjectByPath

    assert.same({ project_root = pr1 }, f("/tmp/project/build.script"))
    assert.same({ project_root = pr2 }, f("/tmp/projectdir/build.script"))
    assert.is_nil(f("/tmp/projectX/build.script"))

    local pr3 = '/tmp/project/childproj/'
    projects[pr3] = { project_root = pr3 }
    assert.same({ project_root = pr3 }, f("/tmp/project/childproj/build.script"))

    -- clear
    projects[pr1], projects[pr2], projects[pr3] = nil, nil, nil
  end)
end)

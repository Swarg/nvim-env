-- 06-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.lang.ExecParams'

describe("env.lang.ExecParams", function()
  it("canExecute", function()
    local isValid = M.isValid -- old canExecute
    local new = function(cwd, cmd, args) return M:new(nil, cwd, cmd, args) end

    assert.match_error(function()
      isValid(nil, 'msg') --Lang.canExecute('msg')
    end, "expected params:table{cwd, cmd, args} got:nil")

    assert.match_error(function()
      isValid(new('/tmp'), 'msg')
    end, 'expected cmd:string or callback:function got: cmd:"nil" cb:nil')

    assert.match_error(function()
      isValid(new('/tmp', ''), 'msg')
    end, 'expected cmd:string or callback:function got: cmd:"" cb:nil')

    assert.same(false, isValid(new('/tmp', 'ls'), 'msg'))
    assert.same(true, isValid(new('/tmp', 'ls', {})))
    assert.same(true, isValid(new('/tmp', 'ls', '')))
    assert.same(false, isValid(new('/tmp'):withCallback(function() end)))
    assert.same(true, isValid(new('/tmp', nil, { arg = 1 }):withCallback(function() end)))
  end)

  it("__tostring", function()
    local exp = 'cwd:/tmp cmd:ls args:[-a -h] envs: desc: opts: callback:nil'
    assert.same(exp, tostring(M.of('/tmp', 'ls', { '-a', '-h' })))

    local exp2 = 'cwd:/tmp cmd:ls args:[-a -h] envs:{k = 8} desc:Desc opts: callback:nil'
    assert.same(exp2, tostring(M.of('/tmp', 'ls', { '-a', '-h' }, { k = 8 }, 'Desc')))

    local params = M.of('/tmp', 'cmd', { 'arg1' }, { k = 8, b = 'str' }, 'Desc')
        :withOpts({ opt = 4 })
        :withCallback(function() end)

    local exp3 = 'cwd:/tmp cmd:cmd args:[arg1] envs:{k = 8, b = "str"}' ..
        ' desc:Desc opts:{opt = 4} callback:function'
    assert.same(exp3, tostring(params))
  end)
end)

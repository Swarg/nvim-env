--
require 'busted.runner' ()
local assert = require('luassert')
local Report = require 'env.lang.Report'

describe('env.lang.Report', function()
  it("add line", function()
    local report = ''
    local r = Report:new({
      writer = function(s)
        report = report .. s .. "\n"
      end
    })
    assert.same('line', r:add('line'))
    assert.same("line\n", report)
    assert.same('def gh', r:add('def %s', 'gh'))
    assert.same("line\ndef gh\n", report)
  end)
end)

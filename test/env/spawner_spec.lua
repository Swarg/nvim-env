--
require 'busted.runner' ()
local assert = require('luassert')
-- local log = require('alogger')

local M = require 'env.spawner'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
-- local uv = (vim or {}).loop
local uv = R.require("luv", vim, "loop")             -- vim.loop

describe('env.spawner', function()
  it("echo", function()
    -- log.set_level(log.levels.DEBUG)

    local cwd = uv.cwd()
    local pp = M.new_process_props(
      -1,                         -- out_bufnr not used
      '/usr/bin/echo',            -- cmd
      { 'its work', '$ENV_VAR' }, -- args
      cwd
    )
    pp.envs = { "ENV_VAR=Value" }

    local output, exit_ok, signal, res_pp
    -- override default handler
    pp.handler_on_close = function(output0, pp0, exit_ok0, signal0)
      output = output0 -- all accomulate input from process stdout|stderr
      exit_ok = exit_ok0
      signal = signal0
      res_pp = pp0
    end

    M.run(pp)
    uv.run() -- like wait() or process.join() wait for done

    local exp_cmdline = "ENV_VAR=Value /usr/bin/echo its work $ENV_VAR"
    local exp_output = "its work $ENV_VAR\n" -- ??
    -- print(inspect(pp))
    assert.same(exp_cmdline, pp.cmdline)
    assert.same(exp_output, pp.output)

    -- from callback
    assert.same(exp_output, output)
    assert.same(true, exit_ok)
    assert.same(0, signal)
    assert.same(pp, res_pp)
  end)

  -- experimental how to work with uv.loop ?? to pass values
  it("run_async", function()
    local cmd, args, envs = 'ls', { '-l' }, {}
    -- closure
    ---@diagnostic disable-next-line: unused-local
    local ondone_callback = function(output, pp, exit_ok, signal)
      print(output)
      assert.same('x', output)
      -- _self.phpinfo = output
    end
    args = args or {}
    envs = envs or {}
    assert.is_not_nil(cmd)
    assert.is_not_nil(ondone_callback)
    ---@diagnostic disable-next-line: invisible
    -- M.runAsync('/tmp', cmd, args, envs, ondone_callback)
    -- here you most use vim.schedule_wrap
  end)
end)

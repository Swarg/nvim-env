-- 05-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local M = require("env.state_registry");

local function loaded_packages_count()
  local cnt = 0
  if package.loaded then
    for _ in pairs(package.loaded) do
      cnt = cnt + 1
    end
  end
  return cnt
end


describe("env.packages", function()
  before_each(function()
    M.clear_registry()
  end)

  it("assert_IStatefullModule -- 'interface' validator", function()
    local err = 'expected module %(table%) got: nil'
    assert.match_error(function() M.assert_IStatefullModule(nil) end, err)

    err = 'expected function dump_state got: nil'
    assert.match_error(function() M.assert_IStatefullModule({}) end, err)

    local modt = { dump_state = function() end }
    err = 'expected function restore_state got: nil'
    assert.match_error(function() M.assert_IStatefullModule(modt) end, err)

    modt.restore_state = function() end
    assert.same(modt, M.assert_IStatefullModule(modt))
  end)

  it("is_statefull_mod", function()
    assert.same(false, M.is_statefull_module(nil))
    assert.same(false, M.is_statefull_module(42))
    assert.same(false, M.is_statefull_module({}))

    local modt = {
      dump_state = function() end,
      restore_state = function() end
    }
    assert.same(true, M.is_statefull_module(modt))
  end)


  it("get_loaded_package_name", function()
    assert.same('env.state_registry', M.get_loaded_package_name(M))
  end)

  it("register & unregister", function()
    assert.same(true, M.register(M))
    assert.same({ ['env.state_registry'] = true }, M.get_registry())
    assert.same(true, M.un_register(M))
    assert.same({}, M.get_registry())
  end)

  local stateful_module1 = [[
local M = {}
local state = {'init'}

function M.dump_state()
  _G.__env_testing_statefull_module_state = state
end

function M.restore_state()
  state = _G.__env_testing_statefull_module_state
  _G.__env_testing_statefull_module_state = nil
end

function M.set_state(new_state) state = new_state end
function M.get_state() return state end

return M
]]

  it("tooling mk statefull module from string", function()
    local cnt1 = loaded_packages_count()
    -- create module on the fly
    local modt = loadstring(stateful_module1, 'stateful_module1')()
    assert.is_not_nil(modt)
    assert.same(cnt1, loaded_packages_count())

    assert(modt)
    assert(type(modt.get_state) == 'function', 'get_state')
    assert(type(modt.set_state) == 'function', 'set_state')

    -- make sure its work as IStatefullModule
    assert.same({ 'init' }, modt.get_state())
    modt.set_state({ 'new_state' })
    assert.same({ 'new_state' }, modt.get_state())
    local mod_name = 'testing.stateful_module'
    M.add_package_to_loaded(mod_name, modt)
    assert.same(cnt1 + 1, loaded_packages_count())
    assert.same(modt, require(mod_name))
    M.del_package_from_loaded(mod_name)
    assert.same(cnt1, loaded_packages_count())
  end)

  it("statefull_modules: dump_state & reload_state", function()
    -- assert.same("", M.statefull_modules_dump_state())
    local key = '__env_testing_statefull_module_state'
    assert.same(nil, _G[key])

    local function load_module()
      local modt = loadstring(stateful_module1, 'stateful_module1')()
      local mod_name = 'testing.stateful_module'
      M.add_package_to_loaded(mod_name, modt)
      return modt, mod_name
    end

    local modt, mod_name = load_module()

    assert.same(true, M.register(modt))
    assert.same({ ['testing.stateful_module'] = true }, M.get_registry())
    modt.set_state({ 'new_state_2' })
    assert.same({ 'new_state_2' }, modt.get_state())

    M.dump_all_state()
    -- ensure state of the module goes into global field
    assert.same({ 'new_state_2' }, _G[key])

    -- emulate reloading
    M.del_package_from_loaded(mod_name)
    modt, mod_name = nil, ''

    modt, mod_name = load_module()
    -- before restore state
    assert.same({ 'init' }, modt.get_state())

    M.restore_all_state()
    assert.same({ 'new_state_2' }, modt.get_state())
  end)
end)

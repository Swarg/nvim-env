require 'busted.runner' ()

local assert = require('luassert') -- used by busted has in nvim +
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect


describe('lua-lang-how-its-work', function()
  it('table_insertion', function()
    local t = { "a", "b", "c" }
    assert.same(3, #t)
    assert.same("a", t[1]) -- index from 1 not from 0
    assert.same("b", t[2])
    assert.same("c", t[3])
    table.insert(t, "d")
    assert.same(4, #t)
    assert.same("d", t[4])
    table.insert(t, 1, "A") -- add to head
    assert.same("A", t[1])
    assert.same("a", t[2])
    assert.same("d", t[5])
  end)

  it('table_insertion', function()
    local v = nil
    local res = 'a-' .. tostring(v)
    assert.same('a-nil', res)
  end)

  it('table_remove', function()
    local t = { 'abc', 'def' }
    table.remove(t, #t)
    assert.same({ 'abc' }, t)
    table.remove(t, #t)
    assert.same({}, t)
    assert.no_error(function()
      table.remove(t, #t)
    end)
  end)

  it("tostring0", function()
    assert.matches("table: .*", tostring({ a = 0, b = 1 }))
    assert.same("1", tostring(1))
    assert.same("nil", tostring(nil))
    assert.same("false", tostring(false))
  end)

  it("tonumber", function()
    assert.same(nil, tonumber('abc'))
    assert.same(nil, tonumber('0abc'))
    assert.same(2, tonumber('2'))
    assert.same(-8, tonumber('-8'))
    assert.same(nil, tonumber(nil))
  end)

  it("gmatch pairs", function()
    local line = '1 2'
    local pairs = {}
    for n in string.gmatch(line, '([^ ]+)') do
      table.insert(pairs, tonumber(n))
    end
    assert.same({ 1, 2 }, pairs)
  end)

  it("string.format", function()
    assert.match_error(function()
      local _ = string.format('(%s) %s', 1, nil)
    end, "bad argument #3 to 'format' .string expected, got nil.")

    assert.match_error(function()
      local _ = string.format('(%s) %s ', 1, true)
    end, "bad argument #3 to 'format' .string expected, got boolean.")
  end)

  it('split path', function()
    local t = {}
    local line = '/root/path/to/file.txt'
    for str in string.gmatch(line, '([^/\\]+)') do -- '\\' - for win path-sep
      table.insert(t, str)
    end
    assert.same({ 'root', 'path', 'to', 'file.txt' }, t)
  end)

  it('split path2', function()
    local t = {}
    local line = 'c:\\path\\to\\file.txt'
    for str in string.gmatch(line, '([^/\\]+)') do
      table.insert(t, str)
    end
    assert.same({ 'c:', 'path', 'to', 'file.txt' }, t)
    --print(inspect(t))
  end)

  it("match", function()
    assert.same(".txt", string.match("some.txt", ".txt"))
    assert.same(nil, string.match("some@txt", "%.txt"))
    assert.same(".txt", string.match("some.txt", "%.txt"))
    assert.same(".txt", string.match("some.txtmore", "%.txt"))
    assert.same(nil, string.match("some.txtmore", "%.txt$"))
    assert.same(".txt", string.match("some.txt", "%.txt$"))
  end)

  it("match-2", function()
    local path = "/home/user/dev/space/proj_a/src/main/java/org/pkg/Main.java"
    local exp = "/home/user/dev/space/proj_a"
    assert.same(exp, string.match(path, "^(.+)/src/main/.*$"))
  end)

  it("match-3", function()
    local line = "	at org.utils.ParserTest.test_build(ParserTest.java:750) "
    local exp = 'at org.utils.ParserTest.test_build(ParserTest.java:750)'
    assert.same(exp, string.match(line, "at .*%(.+%.java:%d+%)"))
  end)

  it("falte in table", function()
    local t = { key = false }
    assert.same({ key = false }, t)

    t = { key = false, key2 = nil, key3 = false }
    t.key4 = true
    assert.same({ key = false, key3 = false, key4 = true }, t)
    t.key4 = false
    assert.same({ key = false, key3 = false, key4 = false }, t)
    t.key4 = nil
    assert.same({ key = false, key3 = false }, t)
  end)

  it("regex", function()
    local line =
    [[./lua/env/lang/oop/ClassGen.lua:106: attempt to concatenate a nil value]]

    local pattern = "^%.?/[^%s]+%:[%d]+%: "
    local pattern2 = "^(%.?/[^%s]+)%:([%d]+)%: "
    -- local pattern = "./[^%s]+%:[%d]+%: "
    local exp = './lua/env/lang/oop/ClassGen.lua:106: '
    assert.same(exp, line:match(pattern))
    local path, ln = line:match(pattern2)
    assert.same('./lua/env/lang/oop/ClassGen.lua', path)
    assert.same('106', ln)
  end)

  it("add nil to lua-table does not change the length of this table", function()
    local t, v = { 'a' }, nil
    assert.same(1, #t)
    t[#t + 1] = v
    assert.same(1, #t)
    t[#t + 1] = 'b'
    assert.same(2, #t)
    assert.same('b', t[2])
  end)

  it("table", function()
    local t = { 1, 2, 3, 4 }
    assert.same(4, #t)
    local sum = 0
    for k, v in ipairs(t) do
      assert.same('number', type(k))
      sum = sum + v
    end
    assert.same(10, sum)

    sum = 0
    local t2 = { 1, 2, 3, 4, k = 5, k2 = 6 }
    assert.same(4, #t2)      -- NOTE 4 not 6! table entries with keys not shown
    for k, v in ipairs(t) do -- traverse only integer keys, skip named
      assert.same('number', type(k))
      sum = sum + v
    end
    assert.same(10, sum)
  end)

  local function check(x, y, z)
    return tostring(x) .. ':' .. tostring(y) .. ':' .. tostring(z)
  end

  it("check arg passing", function()
    assert.same('nil:nil:nil', check())
    assert.same('1:nil:nil', check(1))
    assert.same('a:nil:b', check('a', nil, 'b'))
  end)

  it("check ret 2xArgs first nil ", function()
    local function pass_nil_one() return nil, 1 end
    local v, has = pass_nil_one()
    assert.same(nil, v)
    assert.same(1, has)

    local function pass_false_nil_one() return false, nil, 1 end
    local b, val, has2 = pass_false_nil_one()
    assert.same(false, b)
    assert.same(nil, val)
    assert.same(1, has2)
  end)

  it("exclude", function()
    local fn = "test/env/resources/lua/luarocks/spec/mypkg/init_spec.lua"
    local exclude = "env/resources/"
    assert.same(6, fn:find(exclude))
  end)

  it("pkg", function()
    local line = "class.package 'io.pkg'"
    local res = line:match("^class%.package%s*%(?['\"]([%w%.]+)['\"]%)?")
    assert.same("io.pkg", res)
  end)

  it("process two results from function", function()
    local function a(t, out)
      -- print(t, out)
      local x, y = t[1], t[2]
      if out then
        out[x] = y
      end
      return out
    end
    local function producetwice()
      return 'key', 'value'
    end

    assert.same({ 'key', 'value' }, { producetwice() })
    assert.same({ key = 'value' }, a({ producetwice() }, {}))
  end)

  -- In case anyone's wondering
  -- '=' expected near 'continue'
  -- happens because you're running goto in an old version of lua
  -- before goto was introduced, e.g. 5.1.
  -- To solve this you can rewrite the control flow to avoid goto,
  -- or use a newer version of lua.
  --
  --[[ this code not works in Lua 5.1
function M.check()
  local arr = { 1, 2, 3, 45, 6, 7, 8 }
  local sum = 0
  for key, val in ipairs(arr) do
    if val > 6 then
      goto continue
    end
    sum = sum + val
    -- perform some calculation
    ::continue::
  end
  return sum
end

how to rewrite:

Or if you want both break and continue functionality,
have the local function perform the test, e.g.

local a = 1
while (function()
  if a > 99 then
    return false; -- break
  end
  if a % 2 == 0 then
    return true; -- continue
  end
  print(a)
  return true; -- continue
end)() do
  a = a + 1
end

  ]]

  -- https://www.lua.org/manual/5.1/manual.html
  -- 5.4.1 – Patterns: (for string.match)
  -- %u: represents all uppercase letters.
  -- %a: represents all letters.
  -- %d: represents all digits.
  -- %l: represents all lowercase letters.
  -- %p: represents all punctuation characters.
  -- %s: represents all space characters.
  -- %w: represents all alphanumeric characters.
  -- %x: represents all hexadecimal digits.
  -- %z: represents the character with representation 0.
  -- %x: (where x is any non-alphanumeric character) represents the character x. This is the standard way to escape the magic characters. Any punctuation character (even the non magic) can be preceded by a '%' when used to represent itself in a pattern.
end)

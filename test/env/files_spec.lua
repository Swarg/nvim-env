require 'busted.runner' ()

local fs = require 'env.files'
local M = fs
local su = require 'env.sutil'
local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop")             -- vim.loop
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local assert = require('luassert')                   -- used by busted has in nvim +
-- local utils = require('pl.utils') -- used by busted no in nvim ?

local home = os.getenv("HOME")
local NOT_EXISTS_FILENAME = 'some-not-exists-files.txt.tag.gz'


describe('env.files', function()
  it('sure has home dir', function()
    assert.is_string(home) -- is_true don't work
  end)

  it("is_basename", function()
    assert.same(true, M.is_basename("readme.md"))
    assert.same(false, M.is_basename("dir/readme.md"))
    assert.same(false, M.is_basename(""))
    assert.same(false, M.is_basename("/"))
    assert.same(false, M.is_basename("."))
    assert.same(false, M.is_basename(".."))
    assert.same(true, M.is_basename(".config"))
    assert.same(false, M.is_basename(".config/"))
  end)

  it('basename', function()
    assert.same("/", fs.basename("/"))
    assert.same("file", fs.basename("/file"))
    assert.same("file", fs.basename("path/to/file"))
    assert.same("file", fs.basename("/path/to/file"))
    assert.same('file.ext', fs.basename("/path/to/file.ext"))
    assert.same("/path/to/dir/", fs.basename("/path/to/dir/"))
    assert.same("", fs.basename(""))
    assert.is_nil(fs.basename(nil))
  end)

  it("extract-filename", function()
    assert.same("File", fs.extract_filename("/tmp/path/to/File"))
    assert.same("FileName", fs.extract_filename("/tmp/path/to/FileName.ext"))
    assert.same("File.Name", fs.extract_filename("/tmp/path/to/File.Name.ext"))
    assert.same('.dotfile', fs.extract_filename("/tmp/path/to/.dotfile"))
    assert.same('.dotfile', fs.extract_filename("/tmp/path/to/.dotfile.ext"))
    assert.same("FileName", fs.extract_filename("/tmp/path/to/FileName"))
    assert.is_nil(fs.extract_filename("/tmp/path/to/"))
    assert.same('a', fs.extract_filename("/tmp/path/to/a"))
    assert.same('env bash', fs.extract_filename("#!/usr/bin/env bash"))
  end)

  it("extract_extension", function()
    assert.same("", M.extract_extension("/tmp/path/to/File"))
    assert.same("md", M.extract_extension("/tmp/path/to/readme.md"))
    assert.same("txt", M.extract_extension("/tmp/path/to/readme.txt"))
    assert.same("txt", M.extract_extension("/tmp/path/to/readme.1.txt"))
    assert.same("txt", M.extract_extension("readme.1.txt"))
    assert.same("txt", M.extract_extension("readme/.txt"))
    assert.same("a", M.extract_extension("readme/1.a"))
    assert.same("git", M.extract_extension(".git"))
    assert.same("git", M.extract_extension("/.git"))
    assert.same("e", M.extract_extension("/.git.e"))
    assert.same("ext", M.extract_extension("/.git.e/file.ext"))
    local f = M.extract_extension
    assert.same("", f("/home/user/.dotfiles/tools/tools/tool"))
    assert.same('sh', f("/home/user/.dotfiles/tools/tools/tool.sh"))
    assert.same('', f("ex"))
    assert.same('ex', f(".ex"))
  end)

  it("extract_path", function()
    local s1 = "/home/user/.local/share/app/some_file"
    local s2 = "C:\\Program Files\\app\\some_file"
    assert.same("/home/user/.local/share/app/", M.extract_path(s1))
    assert.same("C:\\Program Files\\app\\", M.extract_path(s2))
    assert.same("C:\\", M.extract_path("C:\\"))
    assert.same("/", M.extract_path("/"))
    assert.same("/", M.extract_path("/dir-or-file"))
    assert.same("/dir/", M.extract_path("/dir/"))
    assert.same("/dir/", M.extract_path("/dir/f"))
    assert.is_nil(M.extract_path("text"))
  end)

  it('fixpath4os', function()
    fs.is_win = false
    assert.same("/path/to/file", fs.fixpath4os("/path/to/file"))
    fs.is_win = true
    assert.same("\\path\\to\\file", fs.fixpath4os("/path/to/file"))
    fs.is_win = fs.is_os_windows() -- return back
  end)

  -- for ensure same random file name generation
  su.set_randomseed(0) -- with one seed will do the same result for each call

  it('tmpfilename', function()
    su.set_randomseed(0) -- with one seed will do the same result for each call
    local rnd = "qOmnuCKl"
    local fn = fs.tmpfilename("test_")
    assert.is_true(su.contanis(fn, "test_" .. rnd))
    -- assert.array(arr).has.no.holes()
    -- assert.has.errors(function() error("this should fail") end)
  end)

  it('file_exists-no', function()
    assert.is_string(home) -- is_true don't work
    assert.is_true(fs.file_exists(home))
    assert.is_false(fs.file_exists(NOT_EXISTS_FILENAME))
  end)

  it('must has error on open not exists file', function()
    local fn = NOT_EXISTS_FILENAME
    local file
    ---@diagnostic disable-next-line: undefined-field
    assert.has.errors(function()
      file = fs.open_file(fn, "r")
    end)
    if file then file:close() end
  end)

  -- create new tmp file with random name, check is_exists, and delete
  -- expected no file before creating, delete it after test
  it('file_exists-has', function()
    local has_fd
    su.set_randomseed(0)
    local fn = fs.tmpfilename("test_") -- io.tmpfile
    assert.is_false(fs.file_exists(fn))

    local fd = fs.open_file(fn, "w")
    has_fd = fd ~= nil
    io.close(fd)
    ---@diagnostic disable-next-line: undefined-field
    assert.not_nil(has_fd)
    assert.is_true(fs.file_exists(fn))
    os.remove(fn)
    assert.is_false(fs.file_exists(fn))
  end)

  it("file_type", function()
    if fs.path_sep == '/' then -- unix
      assert.same(nil, fs.file_type('/tmxxp'))
      assert.same("directory", fs.file_type('/tmp/'))
      assert.same("file", fs.file_type('/bin/bash'))
    end
  end)

  it('read_lines-no', function()
    local file = NOT_EXISTS_FILENAME
    local lines = fs.read_lines(file)
    assert.same({}, lines)
  end)

  it('read_lines_has', function()
    su.set_randomseed(0) -- with one seed will do the same result for each call
    local fn = fs.tmpfilename('lines-')
    local file
    ---@diagnostic disable-next-line: undefined-field
    assert.no.errors(function()
      file = fs.open_file(fn, "w")
      -- sudo chown root:root /tmp/lines-qOmnuCKl
      -- Error opening file: /tmp/lines-qOmnuCKl: Permission denied' ++
    end)
    -- fill file by lines
    if file then
      for i = 1, 10 do
        file:write("Line " .. i .. "\n")
      end
      file:close()
    end
    local lines = fs.read_lines(fn)
    local i = 0
    for _, line in pairs(lines) do
      i = i + 1
      assert.is_true(su.starts_with(line, "Line "))
    end
    assert.same(10, i)
    -- print all line numbers and their contents
    -- for k, v in pairs(lines) do
    --   print('line[' .. k .. ']', v)
    -- end
  end)
end)


describe('env.files-dirs', function()
  local pmode = 448 -- 0700 -> decimal -- permissions
  local rdir = "/tmp/--test--ensure-dir-exists-tmp-dir--"
  it('ensure uv.fs_rmdir cannot remove dirs with files', function()
    assert.is_true(M.ensure_dir_exists(rdir))
    assert.is_true(uv.fs_stat(rdir) ~= nil) -- dir sure created
    local tmp_name = rdir .. '/tmp-file-for-testings.txt'
    -- create file inside dir
    assert.is_true(M.str2file(tmp_name, 'test-ctx-in-file'))
    assert.is_nil(uv.fs_rmdir(rdir)) -- error: ENOTEMPTY: directory not empty
    assert.is_true(M.file_exists(tmp_name))
    os.remove(tmp_name)
    assert.is_true(uv.fs_rmdir(rdir)) -- no erros, empty-dir deleted
    assert.is_nil(uv.fs_stat(rdir))
  end)

  -- create full deep path structure check is exists and delete it
  it("ensure_dir_exists--no-collisions-with-files", function()
    if not fs.is_os_windows() then -- Unix OS
      assert.is_true(M.ensure_dir_exists("/"))
      assert.is_true(M.ensure_dir_exists("/tmp"))
      local path = rdir .. '/ab/deep2/deep3'
      assert.is_true(M.ensure_dir_exists(path))
      -- clear
      assert.is_true(M.ensure_no_path_with_rm('/tmp', path))
      assert.is_nil(uv.fs_stat(rdir))
    end
  end)


  it("ensure_dir_exists--collision-with-file", function()
    if not fs.is_os_windows() then -- Unix OS
      local path = rdir .. '/ab/deep2'
      assert.is_true(uv.fs_mkdir(rdir, pmode))
      local collision_fn = rdir .. '/ab'
      -- create collision for new path
      assert.is_true(M.str2file(collision_fn, 'to-file'))
      assert.is_true(M.file_exists(collision_fn)) -- sure created
      -- cannot create full path, does not allow 'ab' is a file
      assert.error(function() M.ensure_dir_exists(path) end)
      os.remove(collision_fn)
      -- clear
      assert.is_true(M.ensure_no_path_with_rm(rdir, path))
      assert.is_nil(uv.fs_stat(rdir))
    end
  end)

  it("ensure_dir_exists--collision-with-file-2", function()
    if not fs.is_os_windows() then -- Unix OS
      local path = rdir .. '/ab/deep2'
      assert.is_true(uv.fs_mkdir(rdir, pmode))
      local collision_fn = rdir .. '/ab/deep2'
      -- create collision for new path
      assert.is_true(uv.fs_mkdir(rdir .. '/ab/', pmode))
      assert.is_true(M.str2file(collision_fn, 'to-file'))
      assert.is_true(M.file_exists(collision_fn)) -- sure created
      -- cannot create full path, does not allow 'deep2' - is a file
      assert.error(function() M.ensure_dir_exists(path) end)
      os.remove(collision_fn)
      -- clear
      assert.is_true(M.ensure_no_path_with_rm(rdir, path))
      assert.is_nil(uv.fs_stat(rdir))
    end
  end)

  it("ensure_no_path_with_rm--relative-path", function()
    if not fs.is_os_windows() then -- Unix OS
      assert.is_true(M.ensure_dir_exists('/tmp/ab/c'))
      -- clear
      assert.is_true(M.ensure_no_path_with_rm('/tmp', 'ab/c'))
      assert.is_nil(uv.fs_stat(rdir))
    end
  end)

  it("ensure_no_path_with_rm--relative-path-2", function()
    if not fs.is_os_windows() then -- Unix OS
      assert.is_true(M.ensure_dir_exists('/tmp/ab/c'))
      -- clear
      assert.is_true(M.ensure_no_path_with_rm('/tmp', './ab/c'))
      assert.is_nil(uv.fs_stat(rdir))
    end
  end)

  it("ensure_no_path_with_rm--permissions", function()
    if not fs.is_os_windows() then        -- Unix OS
      local dir = '/-----tmp---dir----/c' -- try to mkdir in root
      -- Cannot Create Directory /-----tmp---dir----
      assert.error(function() assert.is_true(M.ensure_dir_exists(dir)) end)
    end
  end)

  -- experimental, check how its woks
  -- io.open(existed-dir-name) give its io.type == 'file'!
  it('check-dir-can-open-dir-as-file?', function()
    if not fs.is_os_windows() then -- Unix OS
      local f = io.open('/tmp/x', 'r')
      assert.is_nil(f)

      local f2 = io.open('/tmp', 'r')
      if f2 then
        assert.same('file', io.type(f2)) --> file,  type(f) --> userdata
        io.close(f2)
      end
    end
  end)

  it("last_modified", function()
    local path = './test/env/resources/eclipse/.classpath'
    assert.same('number', type(M.last_modified(path)))
    assert.same('nil', type(M.last_modified('/some/not/exists/path/')))
  end)

  it("join_path", function()
    assert.same("", M.join_path())
    assert.same("", M.join_path(nil))
    assert.same("/", M.join_path('/', '/'))
    assert.same("/tmp", M.join_path('/', '/tmp'))
    assert.same("/tmp/tmp", M.join_path('/tmp', '/tmp'))
    assert.same("/tmp/tmp", M.join_path('/tmp/', '/tmp'))
    assert.same("/tmp//tmp", M.join_path('/tmp//', '/tmp'))
    assert.same("/tmp", M.join_path('/', 'tmp'))
    assert.same("/tmp/file", M.join_path('/tmp', 'file'))
    assert.same("/tmp/file", M.join_path('/tmp', '/file'))
    assert.same("/tmp/file", M.join_path('/tmp/', '/file'))
    assert.same("/tmp/file", M.join_path('/', 'tmp/', 'file'))
    assert.same('tmp/file', M.join_path('./', 'tmp/', 'file'))
    assert.same('/file', M.join_path('/', './', 'file'))
    assert.same('/proj/dir/to/file', M.join_path('/proj', 'dir', 'to/file'))
  end)

  it("join_path", function()
    assert.same("/dev/project/file", M.join_path('/dev/project', nil, 'file'))
    assert.same("/dev/project/file", M.join_path('/dev/project', '', 'file'))
    assert.same("/dev/project/file", M.join_path('/dev/project', '', '', 'file'))
    assert.same("/dev/pro/sub/file", M.join_path('/dev/pro', 'sub', 'file'))
  end)

  it("is_absolute_path", function()
    local f = M.is_absolute_path
    assert.same(true, f("/"))
    assert.same(true, f("/tmp"))
    assert.same(true, f("/tmp"))
    assert.same(false, f("tmp/"))
    assert.same(false, f("tmp/some/"))
  end)

  it("build_path", function()
    assert.same("/root", M.build_path("/home/dev/pro/", "/root"))
    assert.same("/home/dev/pro/api", M.build_path("/home/dev/pro/", "./api"))
    assert.same("/home/dev/api", M.build_path("/home/dev/pro/", "../api"))
    assert.same("/home/api", M.build_path("/home/dev/pro/", "../../api"))
    assert.same("/api", M.build_path("/home/dev/", "../../api"))
    assert.same("/api", M.build_path("/home/", "../../api"))
    assert.same("/home/", M.build_path("/home/", nil))
    assert.same("/", M.build_path("/home/", '/'))
    assert.same("/home/", M.build_path("/home/", './'))
    assert.same("/home/", M.build_path("/home", './'))
    assert.same("/home/dev", M.build_path("/home", 'dev'))
    assert.same("//dev", M.build_path("/home", '//dev')) -- double slash issue
    assert.same("/", M.build_path("/home/", '../'))
    assert.same("/", M.build_path("/home/", '../.././'))
    assert.same("/home/dev/pro/api", M.build_path("/home/dev/pro/", "././api"))
    assert.same("/ab/cd", M.build_path("/ab/cd/ef/", '../../cd'))
    assert.same("/a/b/c", M.build_path("/a/b/c/d/e", '../../../c'))
  end)

  it("find_marker", function()
    assert.same("env", M.find_marker('./test/', { 'a', 'env' }))
    assert.is_nil(M.find_marker('./test/', { 'a', 'x' }))
  end)

  -- Create all subdirs analog mkdir -p
  it("all-subdirs", function()
    if not fs.is_os_windows() then -- Unix OS
      local path = './dir1/2/dir3/D'
      local fullpath = '/tmp/' .. path
      assert.is_true(M.ensure_no_path_with_rm('/tmp', path))
      assert.is_false(M.dir_exists(fullpath))
      assert.is_true(M.mkdir(fullpath))
      assert.is_true(M.dir_exists(fullpath))
      assert.is_true(M.ensure_no_path_with_rm('/tmp', path))
    end
  end)

  it("exec[r|w]", function()
    local fn = M.tmpfilename('test-exec')
    local text = 'some text\n'
    M.execw('/usr/bin/cat > ' .. fn, text)
    assert.same(text, fs.read_all_bytes_from(fn))
    assert.same(text, M.execr('/usr/bin/cat ' .. fn))
    os.remove(fn)
  end)

  it("get_parent_dirpath", function()
    assert.is_nil(M.get_parent_dirpath("/"))
    assert.is_nil(M.get_parent_dirpath("//"))
    assert.is_nil(M.get_parent_dirpath("///"))
    assert.same("//", M.get_parent_dirpath("//a/"))
    assert.same("/", M.get_parent_dirpath("/dir"))
    assert.same("/", M.get_parent_dirpath("/dir/"))
    assert.same("/dir/", M.get_parent_dirpath("/dir/sub"))
    assert.same("/dir/", M.get_parent_dirpath("/dir/sub/"))
    assert.same("/dir/", M.get_parent_dirpath("/dir/sub//"))
    assert.same("/dir/sub/", M.get_parent_dirpath("/dir/sub/a"))
    assert.same("/dir/sub/", M.get_parent_dirpath("/dir/sub/a/"))
  end)

  it("get_parent_dirname", function()
    assert.same("sub", M.get_parent_dirname("/dir/sub"))
    assert.same("sub", M.get_parent_dirname("/dir/sub/"))
    assert.same("dir", M.get_parent_dirname("/dir"))
    assert.same("dir", M.get_parent_dirname("/dir/"))
    assert.same("d", M.get_parent_dirname("/d"))
    assert.same("d", M.get_parent_dirname("/d/"))
    assert.same("", M.get_parent_dirname("/"))
  end)

  it("get_dirname_with_parentdir", function()
    local f = M.get_dirname_with_parentdir
    assert.same('parent/child', f('/tmp/dev/parent/child'))
    assert.same('parent/child', f('/tmp/dev/parent/child/'))
    assert.same('parent/chi_ld', f('/tmp/dev/parent/chi_ld/'))
    assert.same('parent/chi_ld', f('/tmp/dev/parent/chi_ld/'))
    assert.same('.parent/chi_ld', f('/tmp/dev/.parent/chi_ld/'))
    assert.same('tmp/dev', f('/tmp/dev/'))
    assert.same('tmp/dev', f('/tmp/dev'))
    assert.is_nil(f('/tmp/'))
  end)

  it("get_next_dirname_after_root", function()
    local f = M.get_next_dirname_after_root
    assert.same('subdir', f('/tmp/', '/tmp/subdir/'))
    assert.is_nil(f('/tmp/', '/tmp/file'))
    assert.is_nil(f('/tmp/dev/', '/tmp/subdir/file'))
    assert.is_nil(f('/tmp/subdir/', '/tmp/subdir/file'))
    assert.same('dir', f('/tmp/subdir/', '/tmp/subdir/dir/'))
    assert.same('dir', f('/tmp/subdir', '/tmp/subdir/dir/'))
    assert.same({ 'parent' }, { f('/', '/parent/child/') })
  end)

  it("ensure_dir", function()
    assert.same("/", M.ensure_dir('/'))
    assert.same("/dir/", M.ensure_dir('/dir'))
    assert.same("/dir/", M.ensure_dir('/dir/'))
  end)

  it("ensure_no_dir_slash", function()
    assert.same("/", M.ensure_no_dir_slash('/'))
    assert.same("/dir", M.ensure_no_dir_slash('/dir'))
    assert.same("/dir", M.ensure_no_dir_slash('/dir/'))
    assert.same("/dir/", M.ensure_no_dir_slash('/dir//'))
    assert.same("/a/b/c", M.ensure_no_dir_slash('/a/b/c/'))
  end)

  it("get_child_dirname", function()
    local f = M.get_child_dirname
    assert.same('child-dir', f('/tmp/parent-dir/', '/tmp/parent-dir/child-dir/file'))
    assert.same('child-dir', f('/tmp/parent-dir/', '/tmp/parent-dir/child-dir'))
    assert.is_nil(f('/tmp/parent-dir/', '/tmp/dir/child-dir'))
    assert.is_nil(f('/tmp/parent/', '/tmp/parentdir'))
    assert.is_nil(f('/tmp/parent', '/tmp/parentdir'))
    assert.same('1', f('/tmp/parent', '/tmp/parent/1'))
  end)

  it("get_inner_path_or_full", function()
    local f = M.get_inner_path_or_full
    assert.same('file', f("/tmp", "/tmp/file"))
    assert.same('subdir/file', f("/tmp", "/tmp/subdir/file"))
    assert.same('subdir/file', f("/tmp/", "/tmp/subdir/file"))
    assert.same('child-dir', f('/tmp/parent-dir/', '/tmp/parent-dir/child-dir'))
    assert.same('/another/file', f('/tmp', '/another/file')) -- full
  end)

  it("list_of_files", function()
    -- local path = uv.cwd() .. '/test/env/resources/fs_dir'
    local path = './test/env/resources/fs_dir'
    assert.same({ "abc" }, M.list_of_files(path))
  end)

  it("list_of_files", function()
    local path = './test/env/resources/'
    local exp = {
      'about_lua.txt',
    }
    assert.same(exp, M.list_of_files(path, { ext = 'txt' }))
  end)

  it("path_to_classname", function()
    assert.same('a.b.C', M.path_to_classname('a/b/C'))
    assert.same('a/b/C', M.classname_to_path('a.b.C'))
  end)

  -- it("touch", function()
  --   local fn = fs.tmpfilename('timestamp')
  --   assert.same(true, M.touch(fn))
  --   local mtime1 = fs.last_modified(fn)
  --   assert.is_number(mtime1)
  --   wait(1)
  --   assert.same(true, M.touch(fn))
  --   local mtime2 = fs.last_modified(fn)
  --   assert.are_not_same(mtime1, mtime2)
  -- end)

  -- TODO fs.read_all_bytes_from

  it("read_lines_in_range", function()
    local fn = './test/env/resources/about_lua.txt'
    local f = M.read_lines_in_range

    local exp = '# This data used in tests. Do not change line ordering.'
    assert.same(exp, f(fn, 1, 1)[1])

    local exp2 = {
      '  Lua is a powerful, efficient, lightweight, embeddable scripting language.',
      'It supports procedural programming, object-oriented programming,',
      'functional programming, data-driven programming, and data description.',
      'Lua combines simple procedural syntax with powerful data description constructs',
      'based on associative arrays and extensible semantics.'
    }
    assert.same(exp2, f(fn, 5, 9))
  end)
end)

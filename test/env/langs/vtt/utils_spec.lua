-- 23-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local M = require "env.langs.vtt.utils"


describe("env.langs.vtt.utils", function()
  local vvt_input_2e = {
    "00:19:11.160 --> 00:19:14.420",
    "why one of which is that later on in the",
    "",
    "00:19:14.519 --> 00:19:16.519",
    "database management system",
    ""
  }
  it("join_to_one", function()
    local f = M.join_two_entry_to_one
    local exp = {
      '00:19:11.160 --> 00:19:16.519',
      "why one of which is that later on in the database management system",
      ''
    }
    assert.same(exp, f(vvt_input_2e))
  end)

  it("vvt_pattern", function()
    local f = function(s)
      return { string.match(s, M.vvt_pattern) }
    end
    local exp = { '00:19:11.160', '00:19:14.420' }
    assert.same(exp, f("00:19:11.160 --> 00:19:14.420"))
  end)

  it("sync_timestamp_end_with_next_entry", function()
    local f = M.sync_timestamp_end_with_next_entry
    local exp = {
      '00:19:11.160 --> 00:19:14.420',
      'why one of which is that later on in the',
      '',
      '00:19:14.420 --> 00:19:16.519'
    }
    assert.same(exp, f(vvt_input_2e))
  end)

  it("str_timestamp_to_num", function()
    local f = M.str_timestamp_to_num
    assert.same(1154.125, f('00:19:14.125'))
    assert.same(3600.125, f('01:00:00.125'))
    assert.same(3660.125, f('01:01:00.125'))
    assert.same(3661.125, f('01:01:01.125'))
    assert.same(0.125, f('00:00:00.125'))
    assert.same(-1, f('00:00:00.5'))
    assert.same(0.5, f('00:00:00.500'))
    assert.same(1.5, f('00:00:01.500'))
  end)
end)

describe("env.langs.vtt.utils nvim", function()
  after_each(function()
    nvim:clear_state()
  end)

  it("find_lnum_for_timepos", function()
    local lines = {
      "    WEBVTT",                    -- 1
      "",                              -- 2
      "00:00:01.860 --> 00:00:03.439", -- 3
      "line-4",                        -- 4
      "",                              -- 5
      "00:00:03.540 --> 00:00:06.020", -- 6
      "line-7",                        -- 7
      "",                              -- 8
      "00:00:06.120 --> 00:00:08.780", -- 9
      "line-10",                       -- 10
      "",
    }
    local bn = nvim:new_buf(lines, 1, 0)

    local f = M.find_lnum_for_timepos
    assert.same({ true, 3 }, { f(bn, 0.000) })
    assert.same({ true, 3 }, { f(bn, 2.000) })
    assert.same({ true, 6 }, { f(bn, 3.540) })
    assert.same({ true, 6 }, { f(bn, 3.539) })
    assert.same({ true, 9 }, { f(bn, 8.780) })
    assert.same({ true, 9 }, { f(bn, 6.025) })
  end)

  it("build_timestamp_line", function()
    local f = M.build_timestamp_line
    assert.same('00:00:01.000 --> 00:00:02.000', f(1, 2))
    assert.same('00:00:01.500 --> 00:00:02.000', f('00:00:01.500', 2))
    assert.same('00:00:01.000 --> 00:00:02.500', f(1, '00:00:02.500'))
  end)

  it("move_left_substr_to_prev_entry", function()
    local lines = {
      "    WEBVTT",                    -- 1
      "",                              -- 2
      "00:00:01.860 --> 00:00:03.439", -- 3
      "abc line 4",                    -- 4
      "",                              -- 5
      "00:00:03.540 --> 00:00:06.020", -- 6
      "def line 7",                    -- 7
      "",                              -- 8
      "00:00:06.120 --> 00:00:08.780", -- 9
      "LEFT_TO_PREV line 10",          -- 10
      "",
    }
    nvim:new_buf(lines, 10, 12)
    assert.same({ true, -1 }, { M.move_left_substr_to_prev_entry() })
    local res = nvim:get_lines()
    local exp = {
      '    WEBVTT',
      '',
      '00:00:01.860 --> 00:00:03.439',
      'abc line 4',
      '',
      '00:00:03.540 --> 00:00:06.020',
      'def line 7 LEFT_TO_PREV',
      '',
      '00:00:06.120 --> 00:00:08.780',
      'line 10',
      ''
    }
    assert.same(exp, res)
  end)

  it("is_entry_metaline", function()
    local f = M.is_entry_metaline
    assert.same(false, f(nil))
    assert.same(true, f(''))
    assert.same(false, f('x'))
    assert.same(true, f("00:00:01.860 --> 00:00:03.439"))
    assert.same(true, f("00:00:01.860 --> 00:00:03.439."))
  end)

  local lines_auto_capitalize_move_left_to_prev = {
    "    WEBVTT",                    -- 1
    "",                              -- 2
    "00:00:01.860 --> 00:00:03.439", -- 3
    "This is a first sencence.",     -- 4
    "",                              -- 5
    "00:00:03.540 --> 00:00:06.020", -- 6
    "and this is the second",        -- 7
    "",                              -- 8
    "00:00:06.120 --> 00:00:08.780", -- 9
    "sentence. next sentence here!",
    "",
  }

  it("recognize_sentence_start", function()
    local f = M.recognize_sentence_start
    local exp = {
      '    WEBVTT',
      '',
      '00:00:01.860 --> 00:00:03.439',
      'This is a first sencence.',
      '',
      '00:00:03.540 --> 00:00:06.020',
      'And this is the second',
      '',
      '00:00:06.120 --> 00:00:08.780',
      'sentence. next sentence here!',
      ''
    }
    assert.same(exp, f(lines_auto_capitalize_move_left_to_prev, 7))
  end)


  it("move_left_substr_to_prev_entry", function()
    nvim:new_buf(lines_auto_capitalize_move_left_to_prev, 10, 9)
    assert.same({ true, -1 }, { M.move_left_substr_to_prev_entry() })
    local res = nvim:get_lines()
    local exp = {
      '    WEBVTT',
      '',
      '00:00:01.860 --> 00:00:03.439',
      'This is a first sencence.',
      '',
      '00:00:03.540 --> 00:00:06.020',
      'And this is the second sentence.',
      '',
      '00:00:06.120 --> 00:00:08.780',
      'Next sentence here!',
      ''
    }
    assert.same(exp, res)
  end)


  it("get_current_sentence", function()
    local lines = {
      "    WEBVTT",                      -- 1
      "",                                -- 2
      "00:00:01.860 --> 00:00:03.439",   -- 3
      "this is a begin of the sentence", -- 4
      "",                                -- 5
      "00:00:03.540 --> 00:00:06.020",   -- 6
      "and this is the end.",            -- 7
      "",                                -- 8
      "00:00:06.120 --> 00:00:08.780",   -- 9
      "next sentence",                   -- 10
      "",
    }

    nvim:new_buf(lines, 4, 0)
    local exp = 'this is a begin of the sentence and this is the end.'
    assert.same(exp, M.get_current_sentence())

    vim.api.nvim_win_set_cursor(0, { 3, 0 })
    local exp1 = 'this is a begin of the sentence and this is the end.'
    assert.same(exp1, M.get_current_sentence())

    vim.api.nvim_win_set_cursor(0, { 7, 0 })
    local exp2 = 'and this is the end.'
    assert.same(exp2, M.get_current_sentence())
  end)

  it("is_punctuation_mark", function()
    local f = M.is_punctuation_mark
    assert.same(false, f('..'))
    assert.same(true, f('.'))
    assert.same(true, f('!'))
    assert.same(true, f('?'))
    assert.same(true, f(','))
    assert.same(true, f(';'))
    assert.same(true, f(':'))
  end)
end)


describe("env.langs.vtt.utils get_current_entry", function()
  after_each(function()
    nvim:clear_state()
  end)
  local f = M.get_current_subt_entry

  local lines = {
    "    WEBVTT",                      -- 1
    "",                                -- 2
    "00:00:01.860 --> 00:00:03.439",   -- 3
    "this is a begin of the sentence", -- 4
    "",                                -- 5
    "00:00:03.540 --> 00:00:06.020",   -- 6
    "and this is the end.",            -- 7
    "",                                -- 8
    "00:00:06.120 --> 00:00:08.780",   -- 9
    "next sentence",                   -- 10
    "with multiple",                   -- 11
    "lines",                           -- 12
    "",
  }

  it("get_current_entry first lnum 4 from text", function()
    nvim:new_buf(lines, 4, 0)
    local exp = {
      index = 1,
      lnum = 3,
      lnum_end = 4,
      tstart = '00:00:01.860',
      tend = '00:00:03.439',
      text = { 'this is a begin of the sentence' },
    }
    assert.same(exp, f())
  end)

  it("get_current_entry first lnum 3 from time range", function()
    nvim:new_buf(lines, 3, 0)
    local exp = {
      index = 1,
      lnum = 3,
      lnum_end = 4,
      tstart = '00:00:01.860',
      tend = '00:00:03.439',
      text = { 'this is a begin of the sentence' },
    }
    assert.same(exp, f())
  end)

  it("get_current_entry first lnum 5 from last line", function()
    nvim:new_buf(lines, 5, 0)
    local exp = {
      index = 1,
      lnum = 3,
      lnum_end = 4,
      tstart = '00:00:01.860',
      tend = '00:00:03.439',
      text = { 'this is a begin of the sentence' },
    }
    assert.same(exp, f())
  end)


  it("get_current_entry first lnum 5 from last line", function()
    nvim:new_buf(lines, 10, 0)
    local exp = {
      index = 3,
      lnum = 9,
      lnum_end = 12,
      tstart = '00:00:06.120',
      tend = '00:00:08.780',
      text = { 'next sentence', 'with multiple', 'lines' },
    }
    assert.same(exp, f())
  end)

  it("entry_get_duration", function()
    local entry = {
      index = 3,
      lnum = 9,
      lnum_end = 12,
      tstart = '00:00:06.120',
      tend = '00:00:08.780',
      text = { 'next sentence', 'with multiple', 'lines' },
    }
    assert.same(2.6599999999999992539, M.entry_get_duration(entry))
  end)
end)


describe("env.langs.vtt.utils", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    -- D.on = false
  end)
  it("parse_file", function()
    local fn = '/tmp/main.vtt'
    sys:set_file_content(fn, [[
WEBVTT

00:00:00.440 --> 00:00:03.900
In this video we will talk about subtitles

00:00:00.440 --> 00:00:03.900
second subtitles
with multiple lines

]])
    local exp = {
      {
        index = 1,
        lnum = 3,
        lnum_end = 5,
        tstart = '00:00:00.440',
        tend = '00:00:03.900',
        text = { 'In this video we will talk about subtitles' },
      },
      {
        index = 2,
        lnum = 6,
        tstart = '00:00:00.440',
        tend = '00:00:03.900',
        text = { 'second subtitles', 'with multiple lines' },

      }
    }
    assert.same(exp, M.parse_file(fn))
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')
local fs = require('env.files')
local M = require 'env.langs.php.PhpInfoParser'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local php_root = uv.cwd() .. '/test/env/resources/php/'

describe('env.langs.php.PhpInfoParser', function()
  it("sure no empty line from gmatch", function()
    local txt = "abc\n\ndef"
    local lines = {}
    for line in txt:gmatch('([^\n]+)') do
      table.insert(lines, line)
    end
    assert.same({ 'abc', 'def' }, lines)
  end)

  it("splitKeyValue escape", function()
    local line = "\27[0mVersion"
    assert.same('Version', line:match('\27%[%d%a(.+)'))
  end)

  it("splitKeyValue", function()
    local line = 'Configuration File (php.ini) Path => /usr/local/etc/php'
    local k, v = M.splitKeyValue(line)
    assert.same('Configuration File (php.ini) Path', k)
    assert.same('/usr/local/etc/php', v)
  end)

  it("parseDirectiveLocalMasterValue 1", function()
    local line = 'xdebug.client_port => 9004 => 9003'
    local k, lv, mv = M.parseDirectiveLocalMasterValue(line)
    assert.same('xdebug.client_port', k)
    assert.same('9004', lv)
    assert.same('9003', mv)
  end)

  it("parseDirectiveLocalMasterValue 2", function()
    local line = 'xdebug.client_port => 9003'
    local k, lv, mv = M.parseDirectiveLocalMasterValue(line)
    assert.same('xdebug.client_port', k)
    assert.same('9003', lv)
    assert.is_nil(mv)
  end)

  it("parsePhpVariable 0", function()
    local line = [[$_SERVER['DOCUMENT_ROOT'] =>]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same('$_SERVER', a)
    assert.same('DOCUMENT_ROOT', k)
    assert.same('', v)
  end)

  it("parsePhpVariable 1", function()
    local line = [[$_SERVER['XDEBUG_TRIGGER'] => 1]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same('$_SERVER', a)
    assert.same('XDEBUG_TRIGGER', k)
    assert.same('1', v)
  end)

  it("parsePhpVariable 2", function()
    local line = [[$_SERVER['PWD'] => /app]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same('$_SERVER', a)
    assert.same('PWD', k)
    assert.same('/app', v)
  end)

  it("parsePhpVariable 3", function()
    local line = [[xdebug]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same(false, a)
    assert.same('xdebug', k)
    assert.is_nil(v)
  end)

  it("parsePhpVariable 4", function()
    local line = [[$_SERVER['argv'] => Array]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same('$_SERVER', a)
    assert.same('argv', k)
    assert.same('Array', v) -- + new lines '(\n' ')\n\n'
  end)

  it("parsePhpVariable 4", function()
    local line = [[$_SERVER['argv'] => Array]]
    local a, k, v = M.parsePhpVariable(line)
    assert.same('$_SERVER', a)
    assert.same('argv', k)
    assert.same('Array', v) -- + new lines '(\n' ')\n\n'
  end)
end)

local phpinfo_output_multiline_ini_files = [[
phpinfo()
PHP Version => 8.1.23

Virtual Directory Support => disabled
Configuration File (php.ini) Path => /usr/local/etc/php
Loaded Configuration File => /usr/local/etc/php/php.ini
Scan this dir for additional .ini files => /usr/local/etc/php/conf.d
Additional .ini files parsed => /usr/local/etc/php/conf.d/docker-php-ext-sodium.ini,
/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini,
/usr/local/etc/php/conf.d/xdebug.ini

PHP API => 20210902
Debug Build => no
]]

describe('env.langs.php.PhpInfoParser multi line definition of a .ini files', function()
  it("parse multilines to array", function()
    local info = M:new():parse(phpinfo_output_multiline_ini_files)
    assert.is_not_nil(info)
    -- print(inspect(info))
    local exp =
    {
      configs = {},
      phpinfo = {
        conf = "/usr/local/etc/php",
        debug_build = "no",
        ini_files = {
          "/usr/local/etc/php/conf.d/docker-php-ext-sodium.ini",
          "/usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini",
          "/usr/local/etc/php/conf.d/xdebug.ini"
        },
        loaded_conf = "/usr/local/etc/php/php.ini",
        php_api = "20210902",
        php_ver = "8.1.23"
      }
    }
    assert.same(exp, info)
  end)
end)

local phpinfo_output_envs_and_php_vars = [[
zlib

ZLib Support => enabled
Stream Wrapper => compress.zlib://
Stream Filter => zlib.inflate, zlib.deflate
Compiled Version => 1.2.13
Linked Version => 1.2.13

zlib.output_handler => no value => no value

Additional Modules

Module Name

Environment

Variable => Value
PHP_INI_DIR => /usr/local/etc/php
HOME => /root
COMPOSER_ALLOW_SUPERUSER => 1
XDEBUG_TRIGGER => 1
PWD => /app

PHP Variables

Variable => Value
$_SERVER['PHP_INI_DIR'] => /usr/local/etc/php
$_SERVER['HOME'] => /root
$_SERVER['XDEBUG_TRIGGER'] => 1
$_SERVER['PWD'] => /app
$_SERVER['SCRIPT_FILENAME'] =>
$_SERVER['DOCUMENT_ROOT'] =>
$_SERVER['argv'] => Array
(
    [0] => Standard input code
    [1] => abc
    [2] => def
)

$_SERVER['argc'] => 0

PHP License
This program is free software; you can redistribute it and/or modify
]]

describe('env.langs.php.PhpInfoParser Envs and PhpVars', function()
  it("parse part with envs and php vars", function()
    local info = M:new():parse(phpinfo_output_envs_and_php_vars,
      -- start not from the very beginning but from a configuration point
      M.configuration_handler
    )
    assert.is_not_nil(info)

    -- print(inspect(info))
    local exp =
    {
      configs = {
        Environment = {
          COMPOSER_ALLOW_SUPERUSER = "1",
          HOME = "/root",
          PHP_INI_DIR = "/usr/local/etc/php",
          PWD = "/app",
          Variable = "Value",
          XDEBUG_TRIGGER = "1"
        },
        ["PHP Variables"] = {
          ["$_SERVER"] = {
            DOCUMENT_ROOT = "",
            HOME = "/root",
            PHP_INI_DIR = "/usr/local/etc/php",
            PWD = "/app",
            SCRIPT_FILENAME = "",
            XDEBUG_TRIGGER = "1",
            argc = "0",
            -- TODO
            argv = {
              "    [0] => Standard input code",
              "    [1] => abc",
              "    [2] => def"
            }
          },
          Variable = "Value"
        }
      },
      phpinfo = {}
    }
    assert.same(exp, info)
  end)
end)

local phpinfo_php_vars_empty_array = [[
$_SERVER['HOME'] => /root
$_SERVER['argv'] => Array
(
)

$_SERVER['argc'] => 0

PHP License
]]

describe('env.langs.php.PhpInfoParser Envs and PhpVars', function()
  it("parse part with envs and php vars", function()
    local info = M:new():parse(phpinfo_php_vars_empty_array,
      M.php_variables_handler,
      'PHP Variables'
    )
    assert.is_not_nil(info)
    -- print(inspect(info))
    local exp =
    {
      configs = {
        ["PHP Variables"] = {
          ["$_SERVER"] = {
            HOME = "/root",
            argc = "0",
            argv = {}
          }
        }
      },
      phpinfo = {}
    }
    assert.same(exp, info)
  end)
end)

describe('env.langs.php.PhpInfoParser Xdebug', function()
  it("parse", function()
    local body = fs.read_all_bytes_from(php_root .. '/xdebug_info.log')

    local php_info_xdebug_part = "\nxdebug\n" .. body .. '\nzlib\n'
    local info = M:new():parse(php_info_xdebug_part,
      M.configuration_handler
    )
    assert.is_not_nil(info)
    -- print(inspect(info))
    assert.is_not_nil(info.configs)

    local xdebug = info.configs.xdebug
    assert.is_not_nil(xdebug)
    assert.same('3.2.2', info.configs.xdebug['Version'])

    assert.same('enabled', (xdebug.features or {}).Debugger)
    assert.same("✔ enabled", (xdebug.features or {})["Step Debugger"])

    -- assert.same("✔ enabled", xdebug.["Step Debugger"])
    assert.same("no value", xdebug["xdebug.idekey"])
    assert.same("/tmp/xdebug.log", xdebug["xdebug.log"])
    assert.same("7", xdebug["xdebug.log_level"])
    assert.same("debug", xdebug["xdebug.mode"])

    assert.same("9003", xdebug["xdebug.client_port"])
    assert.same("host.docker.internal", xdebug["xdebug.client_host"])
  end)
end)

describe('env.langs.php.PhpInfoParser Auction', function()
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  ---@diagnostic disable-next-line: unused-local
  local api_auc_root = php_root .. 'auction/api/'
  it("parse", function()
    -- local log = fs.read_all_bytes_from(api_auc_root .. '../php-i-xdebug.log')
    local log = ''
    assert.is_not_nil(log)
    local info = M:new():parse(log)
    assert.is_not_nil(info)

    -- print(inspect(info))
    assert.is_not_nil(info)
  end)

  -- it("parsePhpInfo c 1", function()
  --   local line = 'Configuration File (php.ini) Path => /usr/local/etc/php'
  --   local config = line:match('^Configuration File %(php.ini%) Path => ([^%s]+)$')
  --   assert.same('x', config)
  -- end)
end)

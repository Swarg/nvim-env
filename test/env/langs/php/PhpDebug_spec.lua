--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local ClassInfo = require('env.lang.ClassInfo')
local PhpLang = require 'env.langs.php.PhpLang'
local PhpDebug = require 'env.langs.php.PhpDebug'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop


local php_root = uv.cwd() .. '/test/env/resources/php/'
local Helpers = {}

describe('env.langs.php.PhpDebug', function()
  before_each(function()
    nvim:clear_state()
  end)

  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  local api_auc_root = php_root .. 'auction/api/'
  local pl = PhpLang:new():noCache():resolve(api_auc_root)
  -- frequentry reused path
  local user_id_src_path = api_auc_root .. '/src/Auth/Entity/User/Id.php'
  local user_id_src_ipath = 'src/Auth/Entity/User/Id.php'
  local user_id_src_cn = [[App\Auth\Entity\User\Id]]
  ---@diagnostic disable-next-line: unused-local
  local user_id_ci = ClassInfo:new({
    lang = pl,
    classname = user_id_src_cn,
    inner_path = user_id_src_ipath,
    path = user_id_src_path
  })

  local function new_php_debug()
    vim.schedule_wrap = false
    local pd = PhpDebug:new({
      lang = pl,
      report = pl:newReport('Validate Debugger')
    }) --:validateConfig(user_id_ci)
    return pd
  end

  -----------------------------------------------------------------------------
  it("new PhpDebug for testing", function()
    local pd = new_php_debug()
    assert.is_not_nil(pd)
    assert.is_not_nil(pd.report)
    assert.is_not_nil(pd.errors)
  end)


  -- it("run docker-compose", function()
  --   -- local cwd = uv.cwd()
  --   local cwd = "/home/swarg/dev0/php/auction/"
  --   --local t = PhpDebug.runAndWait(cwd, 'echo', { '123', '$VAR' }, { 'VAR=456' })
  --   local cmd = "docker"
  --   local args = { "compose", "run", "--rm", "-e", "XDEBUG_TRIGGER=1", "api-php-cli", "php", "-i"}
  --   local t = PhpDebug.runAndWait(cwd, cmd, args, { 'VAR=456' })
  --   print(t)
  -- end)

  it("chk", function()
    local pd = new_php_debug()
    -- assert.same('Installed: [OK]', pd:chk('Installed', true))
    -- assert.same('Installed: [yes]', pd:chk('Installed', 'yes'))
    -- assert.same('Installed: [1]', pd:chk('Installed', 1))
    -- assert.same('A healthy: [FAIL]', pd:chk('A healthy', false))
    -- assert.same('B healthy: [FAIL]', pd:chk('B healthy', 0))
    -- assert.same('C healthy: [FAIL]', pd:chk('C healthy', nil))
    assert.same(true, pd:chk('Installed', true))
    assert.same(true, pd:chk('Installed', 'yes'))
    assert.same(true, pd:chk('Installed', 1))
    assert.same(false, pd:chk('A healthy', false))
    assert.same(false, pd:chk('B healthy', 0))
    assert.same(false, pd:chk('C healthy', nil))
    assert.is_not_nil(pd.errors[1])
    -- accomulated errors from chk:
    assert.same('A healthy', pd.errors[1])
    assert.same('B healthy', pd.errors[2])
    assert.same('C healthy', pd.errors[3])
  end)
end)


describe('env.langs.php.PhpDebug Static', function()
  it("getXdebugDirective", function()
    local pd = PhpDebug:new()
    Helpers.setPhpInfoConfiguration(pd)
    assert.same({}, pd:getPhpInfoConfiguration())
    assert.same(false, pd:hasPhpInfoConfiguration())

    Helpers.setPhpInfoConfiguration(pd, {
      xdebug = {
        ['xdebug.client_port'] = '9003',
      }
    })
    assert.same(true, pd:hasPhpInfoConfiguration())
    assert.same(nil, pd:getXdebugDirective())                 --
    assert.same('9003', pd:getXdebugDirective('client_port')) -- auto prefix
    assert.same('9003', pd:getXdebugDirective('xdebug.client_port'))
    assert.same(nil, pd:getPhpInfoConfiguration().xdebug['client_port'])
    assert.same('9003', pd:getPhpInfoConfiguration().xdebug['xdebug.client_port'])
  end)
end)

-- for testing without parsing
function Helpers.setPhpInfoConfiguration(pd, conf)
  pd.info = pd.info or {}
  pd.info.configs = conf or {}
  return pd.info.configs
end

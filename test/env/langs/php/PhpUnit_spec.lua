--
require 'busted.runner' ()
local fs = require('env.files')

local assert = require('luassert')
local PhpUnit = require 'env.langs.php.PhpUnit'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local conf = "./test/env/resources/php/auction/api/phpunit.xml"
local phpunitxml = assert(fs.read_all_bytes_from(conf))

describe('env.langs.php.phpunit.PhpUnit', function()
  it("xml to object", function()
    ---@diagnostic disable-next-line: undefined-field
    local pu = PhpUnit:new():parse(phpunitxml)
    assert.same('vendor/autoload.php', pu.attr.bootstrap)

    local exp_testsuites = {
      functional = { "tests/Functional" },
      unit = { "src/Http/Test", "src/Auth/Test/Unit" }
    }
    assert.same(exp_testsuites, pu.testsuites)

    local exp_source = {
      attr = {
        restrictDeprecations = "true",
        restrictNotices = "true",
        restrictWarnings = "true"
      },
      includes = { "src" }
    }
    assert.same(exp_source, pu.source)

    local exp_php = {
      envs = {
        APP_DEBUG = "1",
        APP_ENV = "test"
      }
    }
    assert.same(exp_php, pu.php)
  end)

  it("findTestSuiteNameFor", function()
    ---@diagnostic disable-next-line: undefined-field
    local pu = PhpUnit:new():parse(phpunitxml)

    assert.same("unit", pu:findTestSuiteNameFor('src/Http/Test/Unit/'))
    assert.same("unit", pu:findTestSuiteNameFor('src/Auth/Test/Unit/'))
    assert.same("functional", pu:findTestSuiteNameFor('tests/Functional/'))
  end)

  it("getTestDirs", function()
    ---@diagnostic disable-next-line: undefined-field
    local pu = PhpUnit:new():parse(phpunitxml)
    local exp = { 'src/Http/Test', 'src/Auth/Test/Unit' }
    assert.same(exp, pu:getTestDirs(nil))
  end)

  --    -- true (Module Auth)
  --   Test\Unit\Entity\TokinTest -- false
  --@deprecated
  -- it("is_module_test_class", function()
  --   local pu = PhpUnit:new():parse(phpunitxml)
  --
  --   local cn1 = [[Test\Unit\Entity\TokinTest]]
  --   assert.same(false, pu:is_module_test_class(cn1))
  --
  --   local cn2 = [[App\Auth\Test\Unit\Service\TokinizerTest]]
  --   assert.same(true, pu:is_module_test_class(cn2))
  --
  --   cn2 = [[App\Auth\Test\Unit\Service\Tokinizer]] -- no suffix
  --   assert.same(false, pu:is_module_test_class(cn2))
  --
  --   cn2 = [[App\Test\Unit\Service\Tokinizer]] -- no module
  --   assert.same(false, pu:is_module_test_class(cn2))
  --
  --   cn2 = [[App\Service\Tokinizer]] -- source
  --   assert.same(false, pu:is_module_test_class(cn2))
  -- end)
end)

describe('xml2lua', function()
  it("parse_xml", function()
    local xml2lua = require("xml2lua")
    local xmlhandler = require("xmlhandler.tree")

    local xml = [[
      <people>
        <person type="natural">
          <name>ABC</name>
          <city>0123</city>
        </person>
        <person type="legal">
          <name>DEF</name>
          <city>4567</city>
        </person>
      </people>
      ]]

    --Instantiates the XML parser
    local handler = xmlhandler:new()
    local parser = xml2lua.parser(handler)
    parser:parse(xml)
    local exp = {
      { name = 'ABC', city = '0123', type = 'natural' },
      { name = 'DEF', city = '4567', type = 'legal' },
    }
    for i, p in pairs(handler.root.people.person) do
      assert.same(exp[i].name, p.name)
      assert.same(exp[i].city, p.city)
      assert.same(exp[i].type, p._attr.type)
    end
  end)
end)

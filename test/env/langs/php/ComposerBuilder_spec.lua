--
require 'busted.runner' ()
local assert = require('luassert')

-- local class = require 'oop.class'
_G._TEST = true

local fs = require('env.files')
local ComposerBuilder = require 'env.langs.php.ComposerBuilder'
local PhpUnit = require('env.langs.php.PhpUnit')

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop


local php_root = uv.cwd() .. '/test/env/resources/php/'
local api_auc_root = php_root .. 'auction/api/'
local phpunit_conf = api_auc_root .. "phpunit.xml"
local phpunit_conf_ctx = fs.read_all_bytes_from(phpunit_conf)

describe('env.langs.php.ComposerBuilder', function()
  it("getModulesTestNamespaceMap", function()
    local c = ComposerBuilder:new()
    local pu = PhpUnit:new():parse(phpunit_conf_ctx)
    c.phpunit = pu
    local exp = {
      -- module ns    --> test path in phpunit testsuites for
      ['App\\Auth\\'] = 'src/Auth/Test/Unit',
      ['App\\Http\\'] = 'src/Http/Test'
    }
    assert.same(exp, c:getModulesTestNamespaceMap())
  end)

  it("parse composer.json", function()
    local c = ComposerBuilder:new()
    c.project_root = api_auc_root
    ---@diagnostic disable-next-line: invisible
    local conf = c:parseJson()
    -- print(inspect(conf))

    type = "project"
    assert.is_not_nil(conf)
    assert.is_not_nil(conf.autoload)
    assert.is_not_nil(conf['autoload-dev'])
    assert.is_not_nil(conf.require)
    assert.is_not_nil(conf.require.php)
    assert.is_not_nil(conf['require-dev'])
    assert.is_not_nil(conf['require-dev']['vimeo/psalm'])
    assert.is_not_nil(conf.scripts)
    assert.is_not_nil(conf.scripts.psalm)
    assert.is_not_nil(conf.scripts.lint)
    assert.is_not_nil(conf.scripts.test)
  end)
end)

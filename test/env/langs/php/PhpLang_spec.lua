--
require 'busted.runner' ()
local assert = require('luassert')

local Object = require('oop.Object')
local Lang = require('env.lang.Lang')
local ClassInfo = require('env.lang.ClassInfo')
local ClassResolver = require('env.lang.ClassResolver')

local PhpLang = require 'env.langs.php.PhpLang'
---@diagnostic disable-next-line: unused-local
local PhpGen = require('env.langs.php.PhpGen')
local FieldGen = require("env.lang.oop.FieldGen")
local ComposerBuilder = require('env.langs.php.ComposerBuilder')
local PhpUnit = require('env.langs.php.PhpUnit')
local DockerCompose = require 'env.lang.DockerCompose'

-- local NVim = require("stub.vim.NVim")
-- local nvim = NVim:new() ---@type stub.vim.NVim

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local CT = ClassInfo.CT

--------------------------------------------------------------------------------
--        Tests Classes:  PhpLang + Lang + ClassResolver + ClassInfo          --
--------------------------------------------------------------------------------

-- this test file use resources with part of php-project at:
local php_root = uv.cwd() .. '/test/env/resources/php/'
--[[
  This files used for this tests:

    test/env/resources/php/
    └── auction
        └── api
            ├── composer.json
            ├── phpunit.xml
            ├── src
            │   ├── Auth
            │   │   ├── Entity
            │   │   │   └── User
            │   │   │       ├── Id.php
            │   │   │       └── Token.php
            │   │   ├── Service
            │   │   │   └── Tokinizer.php
            │   │   └── Test
            │   │       └── Unit
            │   │           ├── Entity
            │   │           │   └── User
            │   │           │       ├── IdTest.php
            │   │           │       └── Token
            │   │           │           ├── CreateTest.php
            │   │           │           └── ValidateTest.php
            │   │           └── Service
            │   │               └── TokinizerTest.php
            │   └── Flusher.php
            └── tests
                └── Functional
                    └── NotFoundTest.php
]]

describe('env.langs.php.PhpLang 1', function()
  it("new getClass instanceof", function()
    local pl = PhpLang:new();
    assert.same(PhpLang, pl:getClass())
    assert.same(true, pl:instanceof(PhpLang))
    assert.same(true, pl:instanceof(Lang))

    assert.same("php", pl:getExt())
  end)

  it("isLang", function()
    assert.same(true, PhpLang.isLang('auction/api/src/Auth/Entity/Id.php'))
    assert.same(false, PhpLang.isLang('auction/api/src/Auth/Entity/Id.txt'))
    assert.same(false, PhpLang.isLang('auction/api/src/Auth/Entity/Id.php.2'))
    assert.same(false, PhpLang.isLang(''))
    ---@diagnostic disable-next-line: missing-parameter
    assert.same(false, PhpLang.isLang())
  end)
end)

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
--              checking support for simple mapping:
--                one source file to one test file
--                    one test to one source
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

describe('env.langs.php.PhpLang -- base', function()
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  -- here Auth - is module name of whole project(scr)
  -- this module-dir holds sources and tests-files in src/Auth/Tests
  local api_auc_root = php_root .. 'auction/api/'
  local pl = PhpLang:new():noCache():resolve(api_auc_root)
  ---@cast pl env.langs.php.PhpLang
  -- frequentry reused path
  local user_id_src_path = api_auc_root .. 'src/Auth/Entity/User/Id.php'
  local user_id_src_exp_cn = [[App\Auth\Entity\User\Id]]
  local user_id_src_innerpath = 'src/Auth/Entity/User/Id'

  -----------------------------------------------------------------------------

  it("getProjectRoot", function()
    assert.same(api_auc_root, pl:getProjectRoot())

    assert.is_not_nil(pl:getProjectBuilder())
    assert.is_true(pl:getProjectBuilder():getClass() == ComposerBuilder)

    assert.is_not_nil(pl:getTestFramework())
    assert.is_true(pl:getTestFramework():getClass() == PhpUnit)
  end)

  it("Namespace_map", function()
    -- pl:findProjectBuilder()
    assert.is_not_nil(pl:getProjectBuilder())
    local exp = { ['src/'] = 'App\\', ['tests/'] = 'Test\\' }
    assert.same(exp, pl.namespace_map)
  end)

  -- check path issue
  it("getInnerPath", function()
    assert.same(user_id_src_innerpath, pl:getInnerPath(user_id_src_path))
  end)

  it("composer namespace_map PRS-4", function()
    local exp = {
      -- path into first part of namespace
      ['src/'] = 'App\\',
      ['tests/'] = 'Test\\'
    }
    assert.is_not_nil(pl:getTestFramework())
    assert.same(exp, pl.namespace_map)
  end)

  it("getClassNameForInnerPath & getInnerPathForClassName", function()
    local ns_path = "src/Auth/Entity/User/Id" -- inner path
    local exp_cn = [[App\Auth\Entity\User\Id]]

    assert.same(exp_cn, pl:getClassNameForInnerPath(ns_path))

    ns_path = "tests/Auth/Entity/User/Id" -- inner path
    exp_cn = [[Test\Auth\Entity\User\Id]]

    assert.same(exp_cn, pl:getClassNameForInnerPath(ns_path))
    assert.same(ns_path, pl:getInnerPathForClassName(exp_cn))
  end)

  it("getClassName src", function()
    local src_path = user_id_src_path
    local exp_cn = user_id_src_exp_cn

    local pl0 = PhpLang:new()
    -- then no project_root it cannot work]
    -- it("should throw an error", function()
    local in_project, classname = false, nil
    assert.has_error(function()
      error("Yup,  it errored")
      in_project = pl0:isPathInProjectRoot(src_path)
    end)
    assert.is_false(in_project)

    assert.has_error(function()
      classname = pl0:getClassName(src_path)
    end)
    assert.is_nil(classname)

    -- resolve project properties - builder testframework, testsuite ns-mappings
    pl0:resolve(src_path)
    assert.same(exp_cn, pl0:getClassName(src_path))
  end)


  it("getClassName test", function()
    local path = api_auc_root .. 'tests/Functional/NotFoundTest.php'
    local exp_ns = [[Test\Functional\NotFoundTest]]
    assert.same(exp_ns, pl:getClassName(path))
    assert.is_true(pl:isTestPath(path))
  end)

  it("isSrcPath", function()
    assert.is_true(pl:isSrcPath(user_id_src_path))
  end)

  it("isTestPath", function()
    assert.is_false(pl:isTestPath(user_id_src_path))
  end)

  it("getTestModuleMapping", function()
    -- path -> class
    -- App/Auth/Entity/User/Id
    -- App/Auth/Test/Unit/Entity/User/Id
    local classname = pl:getClassName(user_id_src_path)
    assert.same(user_id_src_exp_cn, classname)

    local module_ns, tpath = pl:getTestModuleMapping(classname)
    assert.same("App\\Auth\\", module_ns)
    assert.same("src/Auth/Test/Unit", tpath)
  end)

  -- a simple and straightforward classname mappings
  -- without searching in subdir (one source -- multiple tests)

  it("getSrcClassForTestClass", function()
    local tcn = [[App\Auth\Test\Unit\Entity\User\IdTest]]
    local scn = [[App\Auth\Entity\User\Id]]
    assert.same(scn, pl:getSrcClassForTestClass(tcn))
  end)

  it("getTestClassForSrc", function()
    local tcn = [[App\Auth\Test\Unit\Entity\User\IdTest]]
    local scn = [[App\Auth\Entity\User\Id]]
    assert.same(tcn, pl:getTestClassForSrcClass(scn))
  end)
end)


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
--               Checking support for mapping:
--             many tests files to one source file
--                 one source to many tests
--                  with tests in submodule
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

describe('env.lang.ClassResolver -- ClassResolver', function()
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  local api_auc_root = php_root .. 'auction/api/'
  local pl = PhpLang:new():noCache():resolve(api_auc_root)
  -- frequentry reused path
  local user_id_src_path = api_auc_root .. '/src/Auth/Entity/User/Id.php'
  local user_id_src_exp_cn = [[App\Auth\Entity\User\Id]]
  -----------------------------------------------------------------------------

  it("lookupByClassName -- ClassResolver", function()
    local class_name = 'App\\Auth\\Service\\Tokinizer'
    local exp_ip = 'src/Auth/Service/Tokinizer'
    local exp_path = api_auc_root .. 'src/Auth/Service/Tokinizer.php'
    local cr, ci
    -- directly
    assert.same(exp_ip, pl:getInnerPathForClassName(class_name))

    cr = ClassResolver:new({ lang = pl, type = CT.CURRENT })
    ci = cr:lookupByClassName(class_name)

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(exp_ip, ci:getInnerPath())
    assert.same(exp_path, ci:getPath())
  end)

  it("lookupByClassName -- ClassResolver 2", function()
    local class_name = 'App\\Auth\\Service\\Tokinizer'
    local exp_ip = 'src/Auth/Service/Tokinizer'
    local exp_path = api_auc_root .. 'src/Auth/Service/Tokinizer.php'
    local cr, ci

    cr = pl:getClassResolver(CT.CURRENT, nil, class_name)
    ci = cr:lookupByClassName(class_name)

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(exp_ip, ci:getInnerPath())
    assert.same(exp_path, ci:getPath())
  end)

  it("lookupByClassName -- ClassResolver inside run", function()
    local class_name = 'App\\Auth\\Service\\Tokinizer'
    local exp_ip = 'src/Auth/Service/Tokinizer'
    local exp_path = api_auc_root .. 'src/Auth/Service/Tokinizer.php'
    local ci

    ci = pl:getClassResolver(CT.CURRENT, nil, class_name):run():getClassInfo()

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(exp_ip, ci:getInnerPath())
    assert.same(exp_path, ci:getPath())
  end)

  it("lookupByPath -- ClassResolver", function()
    local path = api_auc_root .. 'src/Auth/Service/Tokinizer.php'
    local exp_cn = 'App\\Auth\\Service\\Tokinizer'
    local exp_ip, ci = 'src/Auth/Service/Tokinizer', nil

    ci = ClassResolver:new({ lang = pl, type = CT.CURRENT }):lookupByPath(path)

    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(exp_cn, ci:getClassName())
    assert.same(exp_ip, ci:getInnerPath())
  end)

  it("lookupByPath -- ClassInfo 2", function()
    local path = user_id_src_path
    local exp_cn = user_id_src_exp_cn
    local exp_ip = 'src/Auth/Entity/User/Id'

    local ci = ClassResolver:new({ lang = pl }):lookupByPath(path)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(exp_cn, ci:getClassName())
    assert.same(exp_ip, ci:getInnerPath())
  end)

  -- Testsuites used to check is path or class is a test-class
  it("lookupTestSuite", function()
    local tpath = api_auc_root .. 'src/Auth/Test/Unit/Entity/User/TokenTest.php'

    local ci = ClassResolver:new({ lang = pl }):lookupByPath(tpath)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(true, pl:lookupTestSuite(ci))

    local tsname, tspath = ci:getTestSuite()
    assert.same('unit', tsname)               -- from phpunit.xml
    assert.same('src/Auth/Test/Unit', tspath) -- from phpunit.xml
  end)

  it("ClassInfo opposite", function()
    local path = api_auc_root .. 'src/Auth/Entity/User/Id.php'
    local exp_cn = [[App\Auth\Entity\User\Id]]

    local cr = ClassResolver:new({ lang = pl })
    local ci = cr:lookup(path, nil) -- by path
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    assert.same(true, ci:isSourceClass())
    assert.same(false, ci:isTestClass())
    assert.same(exp_cn, ci:getClassName())
    local oci = cr:getOppositeClass(ci)
    assert.is_not_nil(oci)
  end)

  it("lookupSourceInSubDir -- ClassResolver", function()
    local path = api_auc_root .. 'src/Auth/Entity/User/Token/Create.php'
    local exp = api_auc_root .. 'src/Auth/Entity/User/Token.php'
    -- fix suggest path into realy existed:
    -- Token/CreateTest.php -> Token.php

    local cr = ClassResolver:new({ lang = pl })
    assert.same(exp, cr:lookupSourceInSubDir(path))
  end)

  it("lookupTestInSubDir -- ClassResolver", function()
    local tpath = api_auc_root .. 'src/Auth/Test/Unit/Entity/User/TokenTest.php'
    local exp_dir = api_auc_root .. "src/Auth/Test/Unit/Entity/User/Token/"
    local exp_files = {
      exp_dir .. 'CreateTest.php', exp_dir .. "ValidateTest.php"
    }

    local cr = ClassResolver:new({ lang = pl, path = tpath, type = CT.TEST })
    local tci = cr:run():getClassInfo(false)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    local tcd = cr:lookupTestInSubDir(tci)
    assert.is_not_nil(tcd) ---@cast tcd env.lang.ClassInfo
    assert.same(CT.TESTDIR, tcd.type)
    assert.same(exp_dir, tcd.path)
    assert.is_table(tcd:getTestCases())
    assert.same(exp_files[1], tcd:getTestCases()['CreateTest.php']:getPath())
    assert.same(exp_files[2], tcd:getTestCases()['ValidateTest.php']:getPath())
  end)


  it("findTestForSource -- via ClassResolver", function()
    local spath = api_auc_root .. 'src/Auth/Entity/User/Id.php'
    local exp = api_auc_root .. 'src/Auth/Test/Unit/Entity/User/IdTest.php'
    -- source-class: App/Auth/Entity/User/Id
    -- expected    : App/Auth/Test/Unit/Entity/User/IdTest
    assert.same(exp, pl:findTestForSource(spath)) -- works via ClassResolver
  end)

  it("findSourceForTest -- via ClassResolver", function()
    local tpath = api_auc_root .. 'src/Auth/Test/Unit/Entity/User/IdTest.php'
    local exp = api_auc_root .. 'src/Auth/Entity/User/Id.php'
    -- test-class: App/Auth/Test/Unit/Entity/User/IdTest
    -- expected  : App/Auth/Entity/User/Id
    assert.same(exp, pl:findSourceForTest(tpath)) -- works via ClassResolver
  end)

  -- based modular structure
  -- Convert class_name(source) into full test_class_name
  -- Goal: support generate test ns for classes in modulary structure defined
  --       in phpunit.xml
  --       src/module/ns/Class  ->  src/module/Test/ns/ClassTest
  --       App\module\ns\Class  ->  App\module\Test\ns\ClassTest
  --
  --       and regular test-source mapping: src/ns/Class -> tests/ns/ClassTest
  --       App\ns\Class         ->  Test\ns\ClassTest
  -- Note: in php 'src' mapped as the 'App' word in the namespace(package)
  -- (See composer.json psr4 - autoloading)
  --
  -- in java this is simple directory for sources
  --
  -- input:
  --    App\Auth\Service\Tokinizer
  --    composer App -> src
  -- config:(phpunit testsuite)
  --    src/Auth/Test/Unit/
  -- result:
  --    App\Auth\Test\Unit\Service\TokinizerTest
  it("src->test 1 -- ClassResolver lookup -- with Auth Module", function()
    local class_name = [[App\Auth\Service\Tokinizer]]

    -- arg1 nil - CT_CURRENT arg2-nil no by path but by classname
    local cr = pl:getClassResolver(CT.CURRENT, nil, class_name)
    local ci = cr:lookup(nil, class_name)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    local tci = cr:getOppositeClass(ci) -- map src -> test
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    local exp_tcn = [[App\Auth\Test\Unit\Service\TokinizerTest]]
    assert.same(exp_tcn, tci:getClassName())

    -- check pair binds
    assert.same(ci, tci:getPair())
    assert.same(tci, ci:getPair())
  end)

  --
  it("src->test 1 -- ClassResolver run manual -- with Auth Module", function()
    local class_name = [[App\Auth\Service\Tokinizer]]

    local cr = pl:getClassResolver(CT.CURRENT, nil, class_name):run()
    local ci = cr:getClassInfo(false)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    local tci = cr:getOppositeClass(ci) -- map src -> test
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    local exp_tcn = [[App\Auth\Test\Unit\Service\TokinizerTest]]
    assert.same(exp_tcn, tci:getClassName())

    assert.same(ci, tci:getPair())
    assert.same(tci, ci:getPair())
  end)

  it("src->test 1 -- ClassResolver run OPPOSITE -- with Auth Module", function()
    local class_name = [[App\Auth\Service\Tokinizer]]

    -- OPPOSITE to get ci already mapped src->text
    local cr = pl:getClassResolver(CT.OPPOSITE, nil, class_name):run()
    local tci = cr:getClassInfo(false)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    local exp_tcn = [[App\Auth\Test\Unit\Service\TokinizerTest]]
    assert.same(exp_tcn, tci:getClassName())

    assert.is_not_nil(tci:getPair())
    assert.same(class_name, tci:getPair():getClassName())
  end)

  --
  it("src->test 2 (No Module)", function()
    local scn = 'App\\Entity\\Email'          -- source class name
    local exp_tcn = 'Test\\Entity\\EmailTest' -- test class name

    local cr = pl:getClassResolver(CT.OPPOSITE, nil, scn):run()
    local tci = cr:getClassInfo(false)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(exp_tcn, pl:getTestClassForSrcClass(scn))
    assert.same(exp_tcn, tci:getClassName())

    assert.is_not_nil(tci:getPair())
    assert.same(scn, tci:getPair():getClassName())
  end)

  --
  it("src->test 3 (No Module)", function()
    local scn = 'App\\Flusher'
    local exp_tcn = 'Test\\FlusherTest'

    local cr = pl:getClassResolver(CT.OPPOSITE, nil, scn):run()
    local tci = cr:getClassInfo(false)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(exp_tcn, pl:getTestClassForSrcClass(scn))
    assert.is_not_nil(tci)
    assert.same(exp_tcn, tci:getClassName())
    assert.same(scn, tci:getPair():getClassName())
  end)


  -- convert given src_path into test path (Support modular system)
  -- input:
  --    /home/dev/project/api/src/Auth/Service/Tokinizer.php
  -- config:(phpunit testsuite)
  --    src/Auth/Test/Unit/
  -- result:
  --    /home/dev/project/api/src/Auth/Test/Unit/Service/TokinizerTest.php
  --
  -- convert given test_path into src path (Support modular system)
  -- input:
  --    /home/dev/project/api/src/Auth/Test/Unit/Service/TokinizerTest.php
  -- config:
  --    src/Auth/Test/Unit/
  -- result:
  --    /home/dev/project/api/src/Auth/Service/Tokinizer.php
  --
  it("findTestForSource & findSourceForTest -- simple", function()
    -- src/Auth/Test/Unit/
    local src_path = api_auc_root .. 'src/Auth/Service/Tokinizer.php'
    local test_path_exp = api_auc_root ..
        'src/Auth/Test/Unit/Service/TokinizerTest.php'

    -- test -> src
    assert.same(test_path_exp, pl:findTestForSource(src_path))
    assert.same(test_path_exp, pl:findTestForSource(test_path_exp)) --already

    -- src -> test
    assert.same(src_path, pl:findSourceForTest(test_path_exp))
    assert.same(src_path, pl:findSourceForTest(src_path)) -- already src
  end)

  -- when instead of one test class there is a whole directory with tests
  -- this case resolved by ClassResolver
  it("findTestForSource & findSourceForTest -- subdir", function()
    -- src/Auth/Test/Unit/
    local src_path = api_auc_root .. 'src/Auth/Entity/User/Token.php'
    local test_path_exp = api_auc_root ..
        'src/Auth/Test/Unit/Entity/User/Token/CreateTest.php'

    local test_dir_exp = api_auc_root ..
        'src/Auth/Test/Unit/Entity/User/Token/'
    local test_cases_exp = {
      test_dir_exp .. 'CreateTest.php', test_dir_exp .. 'ValidateTest.php'
    }

    -- test -> src
    local dir, tcases = pl:findTestForSource(src_path)
    assert.same(test_dir_exp, dir)
    assert.is_table(tcases) ---@cast tcases table
    assert.same(test_cases_exp[1], tcases['CreateTest.php'].path)
    assert.same(test_cases_exp[2], tcases['ValidateTest.php'].path)
    -- req_type then required test from src/test pair and test already given
    assert.same(test_path_exp, pl:findTestForSource(test_path_exp))

    -- src -> test
    assert.same(src_path, pl:findSourceForTest(test_path_exp))
    assert.same(src_path, pl:findSourceForTest(src_path))
  end)

  --
  it("findSourceForTest subdir", function()
    local path = php_root ..
        'auction/api/src/Auth/Test/Unit/Entity/User/Token/CreateTest.php'
    local exp = php_root .. 'auction/api/src/Auth/Entity/User/Token.php'
    -- test-class: App/Auth/Test/Unit/Entity/User/IdTest
    -- expected  : App/Auth/Entity/User/Id
    assert.same(exp, pl:findSourceForTest(path))
  end)

  it("resolveClass simple", function()
    local tp = php_root .. 'auction/api/src/Auth/Test/Unit/Entity/User/IdTest.php'
    local sp = php_root .. 'auction/api/src/Auth/Entity/User/Id.php'

    -- src -> test
    local task = pl:resolveClass(sp, ClassInfo.CT.OPPOSITE);
    assert.is_not_nil(task)
    assert.is_true(Object.instanceof(task, ClassResolver))

    local classinfo = task:getClassInfo()
    assert.is_not_nil(classinfo) ---@cast classinfo env.lang.ClassInfo
    assert.same(true, classinfo:isTestClass())
    assert.same(false, classinfo:isSourceClass())
    assert.same(tp, classinfo:getPath())

    -- test -> src
    task = pl:resolveClass(tp, ClassInfo.CT.OPPOSITE);
    classinfo = task:getClassInfo()
    -- print(classinfo:toString())
    assert.is_not_nil(classinfo) ---@cast classinfo env.lang.ClassInfo
    assert.same(false, classinfo:isTestClass())
    assert.same(true, classinfo:isSourceClass())
    assert.same(sp, classinfo:getPath())
  end)

  --[[
     src
     ├── Auth
        ├── Entity
        │   └── User
        │       └── Token.php                        << source-class
        └── Test
            └── Unit
                ├── Entity
                    └── User
                        └── Token                    << test-subdir
                            ├── CreateTest.php          variant-1
                            └── ValidateTest.php        variant-2
    ]]
  it("resolveClass test-sub-dir", function()
    local scn = php_root .. 'auction/api/src/Auth/Entity/User/Token.php'
    local tp_dir = php_root .. 'auction/api/src/Auth/Test/Unit/Entity/User/Token/'

    local exp_files = {
      tp_dir .. 'CreateTest.php', tp_dir .. "ValidateTest.php"
    }
    local tp = tp_dir .. '/' .. exp_files[1]

    -- source --> test
    -- local dst0, res_dir, res_files = pl:resolveClass(sp);
    local task = pl:resolveClass(scn, CT.OPPOSITE);
    assert.is_true(Object.instanceof(task, ClassResolver))
    local ci = task:getClassInfo()
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(true, ci:isTestDir())
    assert.same(false, ci:isTestClass())
    assert.same(false, ci:isSourceClass())
    assert.same(tp_dir, ci:getPath())
    local tcases = ci:getTestCases()
    assert.is_not_nil(tcases) ---@cast tcases table

    assert.same(exp_files[1], tcases['CreateTest.php'].path)
    assert.same(exp_files[2], tcases['ValidateTest.php'].path)

    -- test --> source
    -- local dst, res = pl:resolveClass(tp);
    ci = pl:resolveClass(tp, CT.OPPOSITE):getClassInfo();
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    assert.same(true, ci:isSourceClass())
    assert.same(scn, ci:getPath())
  end)

  it("check regex", function()
    local module = string.match('src/Auth/Test/Unit', "^src/([%w]+)/Test/")
    assert.same("Auth", module)
  end)
end)

describe('env.langs.php.PhpGen', function()
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  local api_auc_root = php_root .. 'auction/api/'
  local pl = PhpLang:new():noCache():resolve(api_auc_root)
  -- frequentry reused path
  -----------------------------------------------------------------------------
  it("createSourceFile", function()
    local opts = {} -- the opts from a cli to generate new Class
    opts.no_date, opts.author = true, 'A'
    opts.object_value = true
    opts.constructor = true
    opts.fields = FieldGen.parseFieldsFromStrings({ 'Id', 'Token' })
    opts.test_class = false
    opts.extends = nil
    local new_classname = 'App\\Entity\\User\\NetworkId'

    local cr = pl:getClassResolver(nil, nil, new_classname)
    local new_ci = cr:lookupByClassName(new_classname)

    assert.is_not_nil(new_ci) ---@cast new_ci env.lang.ClassInfo
    local pg = pl:getLangGen() ---@cast pg env.langs.php.PhpGen

    -- TH.logger_setup()
    local path = new_ci:getPath()

    local path_exp = api_auc_root .. 'src/Entity/User/NetworkId.php'
    assert.same(path_exp, path) -- path filled afret call saveFile

    local body = pg:createClassBody(new_ci, opts)
    assert.is_not_nil(body)
    local class_body_exp = [[
<?php

declare(strict_types=1);

namespace App\Entity\User;

/**
 *
 *
 * @author A
 */
class NetworkId
{

    private Id $id;
    private Token $token;

    public function __construct(Id $id, Token $token)
    {
        $this->id = $id;
        $this->token = $token;
    }

    public function isEqualTo(NetworkId $networkId): bool
    {
        return
            $this->id === $networkId->id &&
            $this->token === $networkId->token;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

}
]]
    assert.same(class_body_exp, body)
  end)
end)

describe('env.lang.DockerCompose', function()
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  local api_auc_root = php_root .. 'auction/api/'
  local pl = PhpLang:new():noCache():resolve(api_auc_root)
  ---@cast pl env.langs.php.PhpLang
  -- frequentry reused path
  local user_id_src_cn = [[App\Auth\Entity\User\Id]]
  local user_id_src_ipath = 'src/Auth/Entity/User/Id.php'
  local user_id_src_path = api_auc_root .. '/' .. user_id_src_ipath
  ---@diagnostic disable-next-line: unused-local
  local user_id_ci = ClassInfo:new({
    lang = pl,
    classname = user_id_src_cn,
    inner_path = user_id_src_ipath,
    path = user_id_src_path
  })
  -----------------------------------------------------------------------------

  it("lookup", function()
    local dc = DockerCompose:new()
    assert.same(dc, dc:lookup(api_auc_root))
  end)

  it("getDockerComposeServiceName", function()
    assert.same("api-php-cli", pl:getDockerComposeServiceName())
  end)

  it("getArgsToRunSingleTestFile", function()
    local exp_args = { "compose", "run", "--rm",
      "api-php-cli", -- service name
      "vendor/bin/phpunit", 'src/Auth/Test/Unit/Entity/User/IdTest.php'
    }
    local cr = pl:getClassResolver(nil, user_id_src_path, nil)
    local ci = cr:lookupByPath(user_id_src_path)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    local tci = cr:getOppositeClass(ci)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    local params = pl:getArgsToRunSingleTestFile(tci)

    assert.is_not_nil(pl:getDockerCompose())

    local exp_params = { cwd = api_auc_root, cmd = 'docker', args = exp_args }
    assert.same(exp_params, params)
  end)

  -- it("doValidateDebugger", function()
  --   local title = 'Validate Debugger'
  --   pl:doValidateDebugger(user_id_ci)
  --   assert.is_not_nil(pl.editor.reports)
  --   local report = pl.editor.reports[title]
  --   assert.is_not_nil(report)
  --   print(report)
  --   -- update project_root from ci.lang.project_root to pl.dc.project_root
  --   assert.is_not_nil(report:find('Sync ProjectRoot to ', 1, true))
  --   assert.is_not_nil(report:find('Found Docker Compose configuration.', 1, true))
  --   assert.is_not_nil(report:find("DockerComposeServiceName: [api-php-cli]", 1, true))
  -- end)
end)

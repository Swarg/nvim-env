-- 29-11-2023 @author Swarg
local M = {}
--

-- local ClassInfo = require("env.lang.ClassInfo")
-- local ClassResolver = require("env.lang.ClassResolver")
local PhpLang = require 'env.langs.php.PhpLang'
local PhpUnit = require 'env.langs.php.PhpUnit'
-- local PhpGen = require 'env.langs.php.PhpGen'


--------------------------------------------------------------------------------
--                   Helper implementation for specific lang
--------------------------------------------------------------------------------
--
local H = require("env.lang.oop.stub.ATestHelper")
H.inherit(M) -- inherit functions from abstract-lang helper

--
-- factory to create ClassInfo LangGen Lang
--
---@param classname string
---@param project_root string?
--
---@return env.lang.ClassInfo
---@return env.langs.php.PhpGen
---@return env.langs.php.PhpLang
function M.mkClassInfoAndPhpGen(classname, project_root)
  local ci, gen, lang = H.mkClassInfoAndLangGen(PhpLang, classname, project_root)
  ---@cast gen env.langs.php.PhpGen
  ---@cast lang env.langs.php.PhpLang
  return ci, gen, lang
end

-- factory to create PhpLang for given project_root
--
---@return env.langs.php.PhpLang
---@return string
function M.mkPhpLang(project_root)
  local lang, pr = H.mkLang(PhpLang, project_root)
  ---@cast lang env.langs.php.PhpLang
  return lang, pr
end

--------------------------------------------------------------------------------

-- local M.Auc = {}

-- local tables = require 'env.util.tables'

--
-- make PhpLang from resources/php/auction
--
function M.mkAucProj(cwd, t)
  assert(type(cwd) == 'string', 'cwd')
  t = t or {}

  -- this test file use resources with part of php-project at:
  local php_root = cwd .. '/test/env/resources/php/'
  -----------------------------------------------------------------------------
  --              One PhpLang object Reused in this describe                 --
  -----------------------------------------------------------------------------
  -- here Auth - is module name of whole project(scr)
  -- this module-dir holds sources and tests-files in src/Auth/Tests
  t.project_root = php_root .. 'auction/api/'
  --  ^ api_auc_root

  -- if not M.plang_auc then
  --   local plang = PhpLang:new():noCache():resolve(t.api_auc_root)
  --   ---@cast plang env.langs.php.PhpLang
  --   M.plang_auc = plang
  -- end
  -- frequentry reused path

  t.user_id_src_path = t.project_root .. 'src/Auth/Entity/User/Id.php'
  t.user_id_src_exp_cn = [[App\Auth\Entity\User\Id]]
  t.user_id_src_innerpath = 'src/Auth/Entity/User/Id'

  t.lang = PhpLang:new():noCache():resolve(t.project_root)

  -- t.lang = tables.deep_copy(M.plang_auc)
  return t
end

--
-- deserialize PhpLang from state for Auction project
--
---@param project_root string?
---@return env.langs.php.PhpLang
function M.mkLangForAucProj(project_root)
  local pr = project_root or '/home/user/proj/'

  local testframework = {
    _class = "env.langs.php.PhpUnit",
    attr = {
      bootstrap = "vendor/autoload.php",
    },
    executable = "vendor/bin/phpunit",
    php = {
      envs = {
        APP_DEBUG = "1",
        APP_ENV = "test"
      }
    },
    source = {
      attr = {
      },
      includes = { "src" }
    },
    testsuites = {
      functional = { "tests/Functional" },
      unit = { "src/Http/Test", "src/Auth/Test/Unit" }
    }
  }

  local t = {
    _class = "env.langs.php.PhpLang",
    ext = "php",
    executable = "php",
    root_markers = { ".git", "composer.json" },
    project_root = pr,
    src = "src/",
    test = "tests/",
    test_suffix = "Test",
    no_cache = true,
    already_existed = false,

    -- testframework = testframework

    builder = {
      _class = "env.langs.php.ComposerBuilder",
      name = "composer",
      project_root = pr, -- "test/env/resources/php/auction/api/",
      src = "src/",
      test = "tests/",
      buildscript = "composer.json",

      -- phpunit = testframework,
      -- modules_test_namespaces = ,
      -- namespaces = ,
    },
  }

  local lang = PhpLang:fromArray(t)
  lang.testframework = PhpUnit:fromArray(testframework)
  lang.builder.phpunit = lang.testframework

  lang.modules_test_namespaces = {
    ["App\\Auth\\"] = "src/Auth/Test/Unit",
    ["App\\Http\\"] = "src/Http/Test"
  }
  lang.namespace_map = {
    ["src/"] = "App\\",
    ["tests/"] = "Test\\"
  }
  lang.builder.modules_test_namespaces = lang.modules_test_namespaces
  lang.builder.namespaces = lang.namespace_map

  return lang
end

return M

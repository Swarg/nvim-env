--
require 'busted.runner' ()
local assert = require('luassert')

_G._TEST = true
-- local class = require("oop.class")

local M = require("env.langs.php.command.new_stuff")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local ClassInfo = require("env.lang.ClassInfo")
-- local ClassResolver = require("env.lang.ClassResolver")
local PhpLang = require 'env.langs.php.PhpLang'
local PhpGen = require 'env.langs.php.PhpGen'
---@diagnostic disable-next-line: unused-local
local Editor = require("env.ui.Editor")
---@diagnostic disable-next-line: unused-local
local Context = require("env.ui.Context")
local FieldGen = require("env.lang.oop.FieldGen")


local H = require("env.langs.php.ahelper")
local mkClassInfoAndPhpGen = H.mkClassInfoAndPhpGen
local mkPhpLang = H.mkPhpLang
---@diagnostic disable-next-line: unused-local
local rnf, new_cli = H.root_and_file, H.new_cli



describe("env.langs.php.command.new_stuff", function()
  after_each(function()
    nvim:clear_state()
  end)
  teardown(function()
    nvim:restore_original_os()
  end)

  -- opts for testing
  local test_opts = ' --author A --no-date --dry-run --quiet'

  it("parseFieldsFromStrings str", function()
    local w = new_cli(nil, ' --fields Id')
    local fields_cli = w:optl('--fields', '-f')
    assert.same({ 'Id' }, fields_cli)
    ---@diagnostic disable-next-line: param-type-mismatch
    local fields = FieldGen.parseFieldsFromStrings(fields_cli)
    assert.same({ id = { name = 'id', type = 'Id' } }, fields)
  end)

  it("parseFieldsFromStrings list", function()
    local w = new_cli(nil, ' --fields [Id]')
    local fields_cli = w:optl('--fields', '-f')
    assert.same({ 'Id' }, fields_cli)
    ---@diagnostic disable-next-line: param-type-mismatch
    local fields = FieldGen.parseFieldsFromStrings(fields_cli)
    assert.same({ id = { name = 'id', type = 'Id' } }, fields)
  end)

  it("parseFieldsFromStrings list 2", function()
    local w = new_cli(nil, ' --fields [Name]')
    local fields_cli = w:optl('--fields', '-f')
    assert.same({ 'Name' }, fields_cli)
    ---@diagnostic disable-next-line: param-type-mismatch
    local fields = FieldGen.parseFieldsFromStrings(fields_cli)
    local exp = { name = { name = 'name', type = 'Name' } }
    assert.same(exp, fields)
  end)


  it("cmd_new_class empty", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "Id"
    local pkg = "App\\Entity\\User"
    local exp_class_file = "/tmp/src/Entity/User/Id.php"

    -- require('dprint').enable()
    local w = new_cli(gen, classname .. ' --package ' .. pkg .. test_opts)
    local path, errmsg = M.cmd_class(w)
    assert.is_nil(errmsg)
    local opts = w.vars

    assert.is_not_nil(path)
    assert.same(exp_class_file, path) -- place to save body into this file

    assert.is_true(opts.dry_run)      -- for testing without saving result to file

    local body, klass = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(klass)
    assert.same(classname, klass.name)
    assert.same(pkg, klass.package)

    assert.same(exp_class_file, opts.class_file)
    assert.is_nil(opts.extends)
    assert.same(false, opts.fields)
    assert.same(false, opts.methods)
    assert.same(false, opts.object_value)
    assert.same(false, opts.test_class)
    -- check tooling
    assert.same("A", opts.author)
    assert.same(true, opts.no_date)
    assert.same(true, opts.quiet)
    -- print("[DEBUG] opts:", inspect(opts))
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity\User;

/**
 *
 *
 * @author A
 */
class Id
{
}
]]
    assert.same(exp, body)
  end)

  --
  --
  it("cmd_new_class fields", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "User"
    local pkg = "App\\Entity"
    local exp_class_file = "/tmp/src/Entity/User.php"

    local fields_cli = "[ Id Token ]"
    local w = new_cli(gen, classname .. ' --package ' .. pkg ..
      ' --fields ' .. fields_cli ..
      test_opts)

    local path, errmsg = M.cmd_class(w)
    assert.is_not_nil(path)
    assert.is_nil(errmsg)
    local opts = w.vars

    assert.same(exp_class_file, path)
    assert.is_not_nil(opts)

    local body, class = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(class)
    assert.same(classname, class.name)
    assert.same(pkg, class.package)
    assert.same(exp_class_file, opts.class_file)

    assert.is_nil(opts.extends)
    assert.same("\n    private Id $id;\n    private Token $token;", opts.fields)
    assert.same(false, opts.methods)
    assert.same(false, opts.object_value)
    assert.same(false, opts.test_class)
    -- print("[DEBUG] opts:", inspect(opts))
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity;

/**
 *
 *
 * @author A
 */
class User
{

    private Id $id;
    private Token $token;
}
]]
    assert.same(exp, body)
  end)

  --
  --
  --
  it("cmd_new_class constructor", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "User"
    local pkg = "App\\Entity"
    local exp_class_file = "/tmp/src/Entity/User.php"

    local fields_cli = "[ Id Token ]"
    local w = new_cli(gen, classname .. ' --package ' .. pkg ..
      ' --fields ' .. fields_cli .. ' --constructor' ..
      test_opts)

    local path, errmsg = M.cmd_class(w)
    assert.is_not_nil(path)
    assert.is_nil(errmsg)
    local opts = w.vars

    assert.same(exp_class_file, path)
    assert.is_not_nil(opts)

    local body, class = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(class)
    assert.same(classname, class.name)
    assert.same(pkg, class.package)
    assert.same(exp_class_file, opts.class_file)

    assert.is_nil(opts.extends)
    assert.same("\n    private Id $id;\n    private Token $token;", opts.fields)
    local exp_methods = [[

    public function __construct(Id $id, Token $token)
    {
        $this->id = $id;
        $this->token = $token;
    }
]]
    assert.same(exp_methods, opts.methods)
    assert.same(false, opts.object_value)
    assert.same(false, opts.test_class)
    -- print("[DEBUG] opts:", inspect(opts))
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity;

/**
 *
 *
 * @author A
 */
class User
{

    private Id $id;
    private Token $token;

    public function __construct(Id $id, Token $token)
    {
        $this->id = $id;
        $this->token = $token;
    }

}
]]
    assert.same(exp, body)
  end)

  --
  --
  it("cmd_new_class object-value no constructor", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "User"
    local pkg = "App\\Entity"
    local exp_class_file = "/tmp/src/Entity/User.php"

    local fields_cli = "[ Id Token ]"
    local w = new_cli(gen, classname .. ' --package ' .. pkg ..
      ' --fields ' .. fields_cli .. ' --object-value' ..
      test_opts)

    local path, errmsg = M.cmd_class(w)
    assert.is_not_nil(path)
    assert.is_nil(errmsg)
    local opts = w.vars

    assert.same(exp_class_file, path)
    assert.is_not_nil(opts) ---@cast opts table

    local body, class = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(class)
    assert.same(classname, class.name)
    assert.same(pkg, class.package)
    assert.same(exp_class_file, opts.class_file)

    assert.same(true, opts.object_value)
    assert.same(false, opts.test_class)
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity;

/**
 *
 *
 * @author A
 */
class User
{

    private Id $id;
    private Token $token;

    public function isEqualTo(User $user): bool
    {
        return
            $this->id === $user->id &&
            $this->token === $user->token;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

}
]]
    assert.same(exp, body)
  end)


  --
  --
  it("cmd_new_class object-value", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "User"
    local pkg = "App\\Entity"
    local exp_class_file = "/tmp/src/Entity/User.php"

    local fields_cli = "[Id Token]"
    local w = new_cli(gen, classname .. ' --package ' .. pkg ..
      ' --fields ' .. fields_cli .. ' --object-value' .. ' --constructor' ..
      test_opts)

    local path, errmsg = M.cmd_class(w)
    assert.is_not_nil(path)
    assert.is_nil(errmsg)
    assert.same(exp_class_file, path)
    local opts = w.vars
    assert.is_not_nil(opts)

    local body, class = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(class)
    assert.same(classname, class.name)
    assert.same(pkg, class.package)
    assert.same(exp_class_file, opts.class_file)

    assert.same(true, opts.object_value)
    assert.same(false, opts.test_class)
    -- print("[DEBUG] opts:", inspect(opts))
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity;

/**
 *
 *
 * @author A
 */
class User
{

    private Id $id;
    private Token $token;

    public function __construct(Id $id, Token $token)
    {
        $this->id = $id;
        $this->token = $token;
    }

    public function isEqualTo(User $user): bool
    {
        return
            $this->id === $user->id &&
            $this->token === $user->token;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getToken(): Token
    {
        return $this->token;
    }

}
]]
    assert.same(exp, body)
  end)

  -- take package from current bufname
  it("cmd_new_class find package", function()
    local gen = mkPhpLang('/tmp'):getLangGen()
    local classname = "Token" -- class name to generate
    -- the file from which the package will be extracted:
    local current_bufname = "/tmp/src/Entity/User/Id.php"

    local exp_pkg = "App\\Entity\\User" -- resolved from "current" bufname
    local exp_class_file = "/tmp/src/Entity/User/Token.php"

    -- UI Stubs emulate
    -- emulate the desired file is open in the nvim in the current buffer
    local lines = { '--' }
    nvim:new_buf(lines, 1, 1, nil, current_bufname)

    local exp_ctx = {
      current_line = '--',
      bufnr = 1,
      bufname = current_bufname,
      cursor_col = 1,
      cursor_row = 1,
      can_edit = true,
      cursor_pos = { 1, 1 },
    }
    -- sure UI Stubs works
    local res_ctx = gen:getContext()
    assert.same(exp_ctx, res_ctx)

    -- emulate a response from the file system - so that it shows that
    -- the file current_bufname exists
    nvim:get_os():set_file(current_bufname)

    -- cli command to generate new classfile for cmd_new_class
    local w = new_cli(gen, classname .. test_opts)

    -- TH.logger_setup()
    local path, errmsg = M.cmd_class(w)

    assert.is_not_nil(path)
    assert.is_nil(errmsg)
    assert.same(exp_class_file, path) -- place to save body into this file
    local opts = w.vars

    assert.is_table(opts) ---@cast opts table
    assert.is_true(opts.dry_run) -- for testing without saving result to file

    local body, class = opts.class_body, opts.class_obj
    assert.is_not_nil(body)
    assert.is_not_nil(class)
    assert.same(classname, class.name)
    assert.same(exp_pkg, class.package)

    assert.same(exp_class_file, opts.class_file)
    local exp = [[
<?php

declare(strict_types=1);

namespace App\Entity\User;

/**
 *
 *
 * @author A
 */
class Token
{
}
]]
    assert.same(exp, body)
  end)
  --
end)

describe('Gen Test', function()
  --
  -- old simple way
  it("createClassBody", function()
    local gen = PhpGen:new({ lang = PhpLang:new() })
    local sci = ClassInfo:new({
      classname = 'org\\pkg\\MyClass',
      path = '/home/dev/project/src/org/pkg/MyClass.php',
    })
    local tci = ClassInfo:new({
      classname = 'org\\pkg\\MyClassTest',
      path = '/home/dev/project/test/org/pkg/MyClassTest.php',
    })
    tci:bindPair(sci)

    local opts = {}
    local body = gen:createClassBody(tci, opts)

    assert.is_string(body) ---@cast body string
    assert.match('class MyClassTest', body)
    assert.match('namespace org.pkg;', body)
  end)

  -- the old one rewrited to new
  it("createClassBody 2", function()
    local projroot = '/home/dev/project/'
    local sci, gen, _ = mkClassInfoAndPhpGen('App\\pkg\\MyClass', projroot)
    assert.same('/home/dev/project/src/pkg/MyClass.php', sci:getPath())
    local tci = ClassInfo:new({
      classname = 'org\\pkg\\MyClassTest',
      path = '/home/dev/project/test/pkg/MyClassTest.php',
    })
    tci:bindPair(sci)

    local opts = {}
    local body = gen:createClassBody(tci, opts)

    assert.is_string(body) ---@cast body string
    assert.match('class MyClassTest', body)
    assert.match('namespace org.pkg;', body)
  end)

  it("genOptsForTestFile", function()
    local lang = mkPhpLang('/tmp')
    local curr_bufname = "/tmp/src/Entity/User.php"
    local lines = { '--' }

    nvim:new_buf(lines, 1, 1, nil, curr_bufname)
    nvim:get_os():set_file(curr_bufname)

    -- code piece from openTestOrSource  sci -> tci
    local cr = lang:getClassResolver(H.CT.OPPOSITE, curr_bufname):run()
    local tci = cr:getClassInfo(false)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo
    local opts = { no_date = true, author = "A" }
    local res_opts = lang:getLangGen():genOptsForTestFile(tci, opts)

    assert.is_not_nil(res_opts) ---@cast res_opts table
    assert.is_not_nil(res_opts.methods)
    local tm = res_opts.methods[1]
    assert.is_not_nil(tm)

    local exp_tm = {
      amod = 2,
      body = "        $user = new User();\n" ..
          "        self::assertEquals(1, $user);",
      mtype = 6,
      name = "testSuccess",
      rettype = "void"
    }
    assert.same(exp_tm, tm:toArray())

    assert.is_not_nil(res_opts.methods[1].classinfo)
    res_opts.methods[1].classinfo = res_opts.methods[1].classinfo:toArray()

    -- the tci is define classname and paths
    -- the res_opts is define body of the new test class file
    local body = lang:getLangGen():createClassBody(tci, res_opts)

    local exp = [[
<?php

declare(strict_types=1);

namespace Test\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * @covers User
 *
 * @author A
 */
class UserTest extends TestCase
{

    public function testSuccess(): void
    {
        $user = new User();
        self::assertEquals(1, $user);
    }

}
]]
    assert.same(exp, body)

    finally(function()
    end)
  end)
end)

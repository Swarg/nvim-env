-- 29-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os() -- activate before another modules


-- local ClassInfo = require("env.lang.ClassInfo")
-- local ClassResolver = require("env.lang.ClassResolver")
local PhpLang = require 'env.langs.php.PhpLang'
-- local PhpGen = require 'env.langs.php.PhpGen'
local class = require 'oop.class'

local M = require("env.langs.php.command.new_stuff");

local H = require("env.langs.php.ahelper")
local mkClassInfoAndLangGen = H.mkClassInfoAndPhpGen
local rnf, new_cli = H.root_and_file, H.new_cli


-- how to setup trace output to stdout:
-- nvim.logger_setup(nil, 0) nvim.D.enable_module(require'cmd4lua.settings', true)


describe("env.langs.php.command.new_stuff", function()
  after_each(function()
    nvim.D.disable()
    nvim:logger_off()
    nvim:clear_all_state_with_os()
  end)
  teardown(function()
    nvim:restore_original_os()
  end)

  --
  -- flat mirror src->test without modular structure
  -- NOTE in this case PhpLang without actual config
  -- (not setuped testcase mapping for module inside src path)
  --
  -- src/App.php --> test/AppTest.php
  --
  it("cmd_test_case", function()
    local pr, fn = rnf('/home/user/proj/', 'src/Auth/Service/Tokinizer.php')

    nvim:new_buf({ '--' }, 1, 16, nil, fn, true)
    local sys = nvim:get_os()
    sys:mk(pr, 'spec/', '.git')

    local _, gen = mkClassInfoAndLangGen('app.MyClass', pr)

    -- :EnvNew test-case
    local w = new_cli(gen, '--name Case --dry-run --author A --no-date')
    -- nvim.logger_setup(nil, 0) nvim.D.enable_module(require'cmd4lua.settings', true)
    local res = M.cmd_test_case(w)

    local opts = w.vars
    local exp_tcpath = pr .. 'tests/Auth/Service/Tokinizer/CaseTest.php'
    assert.same(exp_tcpath, res)
    assert.is_table(opts.tci)
    assert.same(exp_tcpath, opts.tcpath)
    assert.same(exp_tcpath, opts.tcpath)

    -- updated path and class for new testcase
    assert.same(exp_tcpath, opts.tci.path)
    assert.same([[Test\Auth\Service\Tokinizer\CaseTest]], opts.tci.classname)
    local exp = [[
<?php

declare(strict_types=1);

namespace Test\Auth\Service\Tokinizer;

use App\Auth\Service\Tokinizer;
use PHPUnit\Framework\TestCase;

/**
 * @covers Tokinizer
 *
 * @author A
 */
class CaseTest extends TestCase
{

    public function testSuccess(): void
    {
        $tokinizer = new Tokinizer();
        self::assertEquals(1, $tokinizer);
    }

}
]]
    assert.same(exp, opts.class_body)
    assert.same(exp_tcpath, opts.class_file)
  end)

  --
  -- check tooling
  --
  it("check deserialized PhpLang for Auction Project", function()
    -- nvim:get_fs():restore_original()
    -- local a = t.lang:toArray(nil, true) -- how to get state

    local pr, sfn = rnf('/home/user/proj/', 'src/Auth/Service/Tokinizer.php')

    local lang = H.mkLangForAucProj(pr) -- build ready to work from a table

    assert.same('env.langs.php.PhpLang', class.name(lang))
    -- make sure desirialized instans is works
    assert.same(PhpLang, getmetatable(lang))
    assert.same(true, class.is_object(lang))
    assert.same('env.langs.php.PhpLang', class.Object.getClassName(lang))
    assert.is_not_nil(lang:getProjectBuilder())

    --
    local tcn = [[App\Auth\Test\Unit\Entity\User\IdTest]]
    local scn = [[App\Auth\Entity\User\Id]]
    assert.same(scn, lang:getSrcClassForTestClass(tcn))

    scn = [[App\Auth\Entity\User\Id]]
    tcn = [[App\Auth\Test\Unit\Entity\User\IdTest]]
    assert.same(tcn, lang:getTestClassForSrcClass(scn))

    assert.same([[App\Auth\Service\Tokinizer]], lang:getClassName(sfn))
  end)

  --
  --
  -- tree mirror src->test with configured modular structure
  --
  --  src/Auth/Entity/User/Token.php -->
  --  src/Auth/Test/Unit/User/Token/CreateTest.php
  --
  it("cmd_test_case for module test-src mapping success", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/Auth/Entity/User/Token.php')
    local _, tfn = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/CreateTest.php')
    local _, tcdir = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/')
    local lang = H.mkLangForAucProj(pr)

    nvim:new_buf({ '--' }, 1, 16, nil, sfn, true)
    local sys = nvim:get_os()
    sys:mk(pr, 'spec/', '.git', tcdir)
    sys:assert_has(pr, sfn, tcdir)
    assert.is_nil(sys:file_type(tfn)) -- no file

    -- :EnvNew test-case
    local gen = lang:getLangGen()
    local w = new_cli(gen, '--name Create --author A --no-date')
    local res = M.cmd_test_case(w) -- workload

    local opts = w.vars
    local exp_tcpath = tfn
    assert.same(exp_tcpath, res)
    assert.same(exp_tcpath, opts.tcpath)

    -- updated path and class for new testcase
    assert.same(exp_tcpath, opts.tci.path)
    local exp_cn = [[App\Auth\Test\Unit\Entity\User\Token\CreateTest]]
    assert.same(exp_cn, opts.tci.classname)

    assert.same('file', sys:file_type(tfn))
    local body = sys:get_file_content(tfn) or ''
    -- print(body)
    assert.match([[namespace App\Auth\Test\Unit\Entity\User\Token;]], body)
    assert.match([[use App\Auth\Entity\User\Token;]], body)
    assert.match([[class CreateTest extends TestCase]], body)
  end)

  --
  --
  -- tree mirror src->test with configured modular structure
  --
  --  src/Auth/Entity/User/Token.php -->
  --  src/Auth/Test/Unit/User/Token/CreateTest.php
  --
  it("cmd_test_case for module test-src mapping fail - file exists", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/Auth/Entity/User/Token.php')
    local _, tfn = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/CreateTest.php')
    local _, tcdir = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/')
    local lang = H.mkLangForAucProj(pr)

    nvim:new_buf({ '--' }, 1, 16, nil, sfn, true)
    local sys = nvim:get_os()
    sys:mk(pr, 'spec/', '.git', tcdir, tfn)
    sys:assert_has(pr, sfn, tcdir, tfn)
    assert.same('file', sys:file_type(tfn)) -- already exists
    -- nvim.logger_setup()
    -- :EnvNew test-case
    local gen = lang:getLangGen()
    local w = new_cli(gen, '--name Create --author A --no-date')

    local res = M.cmd_test_case(w) -- workload

    assert.same(false, res) -- not generated
    assert.same('file', sys:file_type(tfn))
    -- why? file already exists and user not define another name
    local opts = w.vars
    assert.is_nil(opts.name)
    assert.is_nil(opts.tcpath)
  end)


  --
  --
  --  tree mirror src->test with configured modular structure
  --
  --  src/Auth/Entity/User/Token.php -->
  --  src/Auth/Test/Unit/User/Token/CreateTest.php
  --
  it("cmd_test_case for module test-src mapping fail - file exists", function()
    local pr, sfn = rnf('/home/user/proj/', 'src/Auth/Entity/User/Token.php')
    local _, tfn = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/CreateTest.php')
    local _, tcdir = rnf(pr, 'src/Auth/Test/Unit/Entity/User/Token/')
    local lang = H.mkLangForAucProj(pr)

    nvim:new_buf({ '--' }, 1, 16, nil, sfn, true)
    -- user input for new test-case name
    nvim:add_ui_input('Success')

    local sys = nvim:get_os()
    sys:mk(pr, 'spec/', '.git', tcdir, tfn)
    sys:assert_has(pr, sfn, tcdir, tfn)
    assert.same('file', sys:file_type(tfn)) -- already exists

    -- nvim.logger_setup()
    -- :EnvNew test-case
    --
    local gen = lang:getLangGen()
    local w = new_cli(gen, '--name Create --author A --no-date')

    local res = M.cmd_test_case(w) -- workload

    local exp = pr .. 'src/Auth/Test/Unit/Entity/User/Token/SuccessTest.php'
    assert.same(exp, res) ---@cast res string
    assert.same('file', sys:file_type(tfn)) -- generated a new one! from Input

    local opts = w.vars
    assert.same('Success', opts.name)
    assert.same(exp, opts.tcpath)
    local body = sys:get_file_content(res) or ''
    -- print(body)
    assert.match([[namespace App\Auth\Test\Unit\Entity\User\Token;]], body)
    assert.match([[use App\Auth\Entity\User\Token;]], body)
    assert.match([[class SuccessTest extends TestCase]], body)
  end)
end)

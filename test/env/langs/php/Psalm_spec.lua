--
require 'busted.runner' ()
local assert = require('luassert')
local fs = require('env.files')
local M = require 'env.langs.php.Psalm'

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop
-- local cjson = R.require("cjson", vim, "json")        -- vim.json


local php_root = uv.cwd() .. '/test/env/resources/php/'
local psalm_json_output = fs.read_all_bytes_from(php_root .. '/psalm.json')

describe('env.langs.php.Psalm', function()
  describe('From resources/psalm.json', function()
    local lang = {}
    local p = M:new({ lang = lang })
    p.useDocker = true
    -- p.serviceName = 'api-fpm-cli'
    p.docker_volumes = { ['/app'] = php_root .. 'auction/api' }
    p.output_raw = psalm_json_output
    local resDiagnostics = p:parseOutput() -- diagnostic multiple-files in one big table

    it("parseOutput is success", function()
      -- print(inspect(res))
      assert.is_not_nil(resDiagnostics)
      assert.same(10, #resDiagnostics)
    end)

    it("parseOutput is copy of original json was created", function()
      assert.is_not_nil(p.rawDiagnostics)
    end)

    it("record 1 buildDiagnosticMessage", function()
      local exp1 = {
        bufnr = 1,     -- syntetic for test
        code = 12,
        col = 34,      -- -1
        end_col = 42,  -- -1
        end_lnum = 22, -- -1
        lnum = 22,     -- -1
        message = [==[
[12:InvalidScalarArgument] https://psalm.dev/012

Argument 1 of array_replace_recursive expects array<array-key, mixed>,
but possibly undefined array<array-key, mixed> provided

return array_replace_recursive(...$configs);
----------------------------------^^^^^^^^]==],
        namespace = 1,
        severity = 1, -- "error"
        -- source = "psalm"
      }
      assert.same(exp1, resDiagnostics[1])
    end)

    it("record 9 Other References in message", function()
      local exp =
      [[
[30:MixedArgument] https://psalm.dev/030

Argument 1 of App\Auth\Entity\User\UserRepository::add cannot be mixed,
expecting App\Auth\Entity\User\User

        $this->users->add($user);
--------------------------^^^^^
Other References:
The type of $user is sourced from here
src/Auth/Command/JoinByEmail/Request/Handler.php:49
        $user = new User(]]
      local cooked = p:cookDiagnosticEntryForVim(p.rawDiagnostics[9])
      assert.same(exp, cooked.message)
    end)
  end)

  it("record 1 buildDiagnosticMessage", function()
    local line = [[[30:MixedArgument] https://psalm.dev/030]]
    local err = string.match(line, '%[[%d]+:([%w]+)%]', 1)
    assert.same('MixedArgument', err)
  end)
end)

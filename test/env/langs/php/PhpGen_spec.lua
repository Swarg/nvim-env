--
require 'busted.runner' ()
local assert = require('luassert')


local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

-- debug
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

-- local PhpLang = require 'env.langs.php.PhpLang'

local AMods = require("env.lang.oop.AccessModifiers")
---@diagnostic disable-next-line unused-local
local Constructor = require("env.lang.oop.Constructor")
---@diagnostic disable-next-line unused-local
local Method = require("env.lang.oop.Method")
---@diagnostic disable-next-line unused-local
local Field = require("env.lang.oop.Field")
---@diagnostic disable-next-line unused-local
local MethodGen = require("env.lang.oop.MethodGen")


-- local H = require("env.lang.oop.stub.ATestHelper")
local H = require("env.langs.php.ahelper")
local mkClassInfoAndPhpGen = H.mkClassInfoAndPhpGen
-- local rnf = H.H.root_and_file
-- local new_cli = H.H.new_cli
-- helpers
local mkField, mkFields = H.mkField, H.mkFields


--------------------------------------------------------------------------------

describe('env.langs.php.PhpGen', function()
  before_each(function()
    nvim:clear_state()
  end)

  it("mkClassInfoAndPhpGen", function()
    local ci, gen, lang = mkClassInfoAndPhpGen('App\\Entity\\User')
    assert.is_not_nil(gen)
    assert.same(lang, gen:getLang())
    -- assert.same(gen, ci:getLang().gen) ??
    assert.same('/tmp/app/', gen:getLang():getProjectRoot())
    assert.same('/tmp/app/src/Entity/User.php', ci:getPath())
    assert.same('src/Entity/User', ci:getInnerPath())
    -- assert.same("x", ci:getPath())
  end)

  it("getConstructorCode", function()
    local ci, gen, _ = mkClassInfoAndPhpGen('App\\Entity\\User')
    local params = mkFields(gen, { { 'id', 'Id' }, { 'token', 'Token' } })

    local opts = { params = params, amod = H.AM.PRIVATE }
    local exp = [[
    private function __construct(Id $id, Token $token)
    {
        $this->id = $id;
        $this->token = $token;
    }
]]
    assert.same(exp, gen:getConstructorCode(ci, opts))
    --'name', 'body', 'params', 'type'))
  end)

  it("getGetterCode", function()
    local ci, gen = mkClassInfoAndPhpGen('App\\Entity\\User')
    local field = mkField(gen, 'id', 'Id')
    local exp = [[
    public function getId(): Id
    {
        return $this->id;
    }
]]
    assert.same(exp, gen:getGetterCode(ci, field))
  end)

  it("getSetterCode", function()
    local ci, gen = mkClassInfoAndPhpGen('App\\Entity\\User')
    local field = mkField(gen, 'id', 'Id')
    local exp = [[
    public function setId(Id $id): void
    {
        $this->id = $id;
    }
]]
    assert.same(exp, gen:getSetterCode(ci, field))
  end)

  it("genereateConstructor", function()
    local ci, gen, _ = mkClassInfoAndPhpGen('App\\Entity\\User')
    local fields = { mkField(gen, 'id', 'Id'), mkField(gen, 'name') }
    local opts = { params = fields, amod = AMods.ID.PRIVATE }

    local mg = gen:getMethodGen(ci)
    assert.same(gen, mg:getLangGen())

    local constructor = mg:genConstructor(opts)
    local exp = [[
    private function __construct(Id $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
]]
    assert.same(exp, mg:genConstructorCode(constructor))
  end)

  it("genereateConstructor", function()
    local ci, gen = mkClassInfoAndPhpGen('App\\Entity\\User')
    local fields = { mkField(gen, 'id', 'Id'), mkField(gen, 'token') }
    local opts = { params = fields, amod = AMods.ID.PUBLIC }

    local mg = gen:getMethodGen(ci)
    local constructor = mg:genConstructor(opts)
    local exp = [[
    public function __construct(Id $id, string $token)
    {
        $this->id = $id;
        $this->token = $token;
    }
]]
    assert.same(exp, mg:genConstructorCode(constructor))
  end)
  -- todo with additional opts with Asserts inside constructor body and tolower


  it("is_equals_to", function()
    local ci, gen = mkClassInfoAndPhpGen('App\\Entity\\User')
    local fields = { mkField(gen, 'id', 'Id'), mkField(gen, 'token') }
    local mg = gen:getMethodGen(ci)
    local eq = mg:genIsEqualTo(fields)
    local exp = [[
    public function isEqualTo(User $user): bool
    {
        return
            $this->id === $user->id &&
            $this->token === $user->token;
    }
]]
    assert.same(exp, mg:genIsEqualToCode(eq))
  end)

  describe('Field', function()
    local ci, gen = mkClassInfoAndPhpGen('App\\Entity\\User')
    it("toCode", function()
      assert.same('string', gen:getDefaultFieldType())
      -- local fields = { mkField('Id'), mkField('Name') }

      ---@diagnostic disable-next-line: param-type-mismatch
      local field = mkField(gen, 'id', false, H.AM.PRIVATE)
      local fg = gen:getFieldGen(ci)
      assert.same('    private string $id;', fg:toCode(field))

      field = mkField(gen, 'id')
      assert.same('    private string $id;', fg:toCode(field))

      ---@diagnostic disable-next-line: param-type-mismatch
      field = mkField(gen, nil)
      assert.same('    private string $field;', fg:toCode(field))
    end)
  end)
end)

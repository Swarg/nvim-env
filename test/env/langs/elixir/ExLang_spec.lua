--
-- 12-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local D = require("dprint")

-- ex-parser debugging config:
-- D.on = true -- ex-parser see parsing
---@diagnostic disable-next-line: unused-local
local ex_parser = require("ex_parser.grammar");
-- D.enable_module(ex_parser, true)  -- to enable dprint
D.on = false
--

local class = require 'oop.class'
local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local ClassInfo = require('env.lang.ClassInfo')
local H = require("env.langs.elixir.ahelper")
local ExLang = require("env.langs.elixir.ExLang");
local MixBuilder = require("env.langs.elixir.MixBuilder");

-- disable all load modules by mask
D.enable_module('stub.os.*', false)
D.enable_module('stub.vim.*', false)


describe("env.langs.elixir.ExLang", function()
  it("isLang", function()
    assert.is_true(ExLang.isLang('source.ex'))
    assert.is_true(ExLang.isLang('source.ex'))
    assert.is_true(ExLang.isLang('mix.exs'))
    assert.is_true(ExLang.isLang('mix.ex'))
    assert.is_false(ExLang.isLang('mix.e'))
    assert.is_false(ExLang.isLang('ex'))
    assert.is_false(ExLang.isLang('exs'))
  end)
end)

describe("env.langs.elixir.ExLang FlatDir", function()
  it("getClassNameForInnerPath", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local lang = ExLang:new({ project_root = proj_dir })

    local innerpath = 'lib/some_module'
    assert.same('SomeModule', lang:getClassNameForInnerPath(innerpath))
  end)

  it("getInnerPathForClassName", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'

    local lang = ExLang:new({ project_root = proj_dir })
    local cn = 'SomeModule'
    assert.same('lib/some_module', lang:getInnerPathForClassName(cn))
  end)
end)

describe("env.langs.elixir.ExLang FlatDir (with Enumlated FS)", function()
  after_each(function()
    sys:clear_state()
    sys.fs:clear_state()
  end)


  it("findProjectBuilder with Mix", function()
    local proj_dir = '/tmp/dev/workspace/proj_a/'
    sys.fs:mk(proj_dir, 'mix.exs')
    local proj_buildscript = proj_dir .. 'mix.exs'

    assert.same(true, sys:set_file_content(proj_buildscript, H.mix_conf_00))
    assert.same(H.mix_conf_00, sys:get_file_content(proj_buildscript))

    local lang = ExLang:new({ project_root = proj_dir })
    local mix = lang:findProjectBuilder().builder

    assert.is_not_nil(mix) --@cast mix env.langs.elixir.MixBuilder
    assert.same('/tmp/dev/workspace/proj_a/mix.exs', mix:getBuildScriptPath())
    assert.same('lib/', lang.src)
    assert.same('test/', lang.test)
  end)


  -- used to jump from source-file to corresponded test file
  it("resolve src > test (both existed in fs)", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local mixpath = proj_dir .. 'mix.exs'
    local spath = proj_dir .. 'lib/module.ex'
    local tpath = proj_dir .. 'test/module_test.exs'

    sys:set_file_content(mixpath, H.mix_conf_01)
    sys.fs:mk(proj_dir, './lib/', 'module.ex', '../test/', 'module_test.exs')

    local lang = nil
    lang = ExLang:new():resolve(spath)
    -- under the hood
    -- local lang = M:new({ project_root = proj_dir })
    -- lang:findProjectRoot(spath)
    -- instance = self:findProjectRoot(filename)
    -- instance:findProjectBuilder()
    -- instance:findTestFramework()
    assert.same(proj_dir, lang.project_root)
    assert.is_not_nil(lang.builder)
    assert.same('mix', lang.builder.name)

    -- lang:openTestOrSource(spath):
    local cr = lang:resolveClass(spath, ClassInfo.CT.OPPOSITE) -- src -> test
    local tci = cr:getClassInfo()
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo
    local sci = tci.pair

    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo
    assert.same('Module', sci.classname)
    assert.same(spath, sci:getPath())
    assert.same('lib/module', sci:getInnerPath())
    assert.same(true, sci:isPathExists())

    assert.same('ModuleTest', tci.classname)
    assert.same(tpath, tci:getPath())
    assert.same('test/module_test', tci:getInnerPath())
    assert.same(true, tci:isPathExists())
  end)


  -- used to jump from test-file to corresponded source file
  it("resolve test > src (flat src+test)(both existed in fs)", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local mixpath = proj_dir .. 'mix.exs'
    local spath = proj_dir .. 'module.ex'
    local tpath = proj_dir .. 'module_test.exs'

    local mix_exs = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_app,
      version: "0.1.0",
      # elixir: "~> 1.16",
      elixirc_paths: [ "./" ], # ./lib
      start_permanent: Mix.env() == :prod,
    ]
  end

  def application do
    [ ]
  end

end
]]
    sys:set_file_content(mixpath, mix_exs)
    sys.fs:mk(proj_dir, 'module.ex', 'module_test.exs')

    local lang = nil
    lang = ExLang:new(nil, proj_dir, nil)
    lang:findTestFramework()

    -- D.enable_module(ex_parser, true)
    -- require'alogger'.fast_setup(nil, 0)

    lang:findProjectBuilder()
    local mix = lang:getProjectBuilder()
    assert.same('env.langs.elixir.MixBuilder', class.name(mix))

    assert.same(proj_dir, lang.project_root)
    assert.is_not_nil(lang.builder)
    assert.same('mix', lang.builder.name)


    assert.same('./', mix.src)
    assert.same('./', mix.test)
    assert.same('./', lang:getSrcPath())
    assert.same('./', lang:getTestPath())

    -- lang:openTestOrSource(spath)
    local cr = lang:resolveClass(tpath, ClassInfo.CT.OPPOSITE) -- test -> src
    local sci = cr:getClassInfo()
    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo
    local tci = sci.pair
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same('ModuleTest', tci.classname)
    assert.same(tpath, tci:getPath())
    assert.same('module_test', tci:getInnerPath())
    assert.same(true, tci:isPathExists())

    assert.same('Module', sci.classname)
    assert.same(spath, sci:getPath())
    assert.same('module', sci:getInnerPath())
    assert.same(true, sci:isPathExists())
  end)


  it("onResolveCINoExistedPath match", function()
    local match0 = function(line)
      return string.match(line, '^(.+)_i(%d+)_test.exs$')
    end

    local fn, itern = match0('/tmp/dev/proj_a/module_i4_test.exs')
    assert.same('/tmp/dev/proj_a/module', fn)
    assert.same('4', itern)

    fn, itern = match0('/home/u/elixir/code-snippets/datebase_i2_test.exs')
    assert.same('/home/u/elixir/code-snippets/datebase', fn)
    assert.same('2', itern)
  end)

  it("RawSnippets: onResolveCINoExistedPath mod_i4 -> mod_i1_test.exs", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local spath = proj_dir .. 'module_i4.ex'
    local tpath = proj_dir .. 'module_i1_test.exs'
    sys.fs:mk(proj_dir, spath, tpath)

    -- RawSnippets
    local lang = ExLang:new({})
    lang.builder = lang:createBuilder(proj_dir, { flat = true, inmem = true })
    local mix = lang.builder
    assert.is_not_nil(mix) ---@cast mix env.langs.elixir.MixBuilder
    assert.is_true(MixBuilder.isRawSnippets(mix))

    local path = proj_dir .. 'module_i4_test.exs'

    -- supposed _i4_test.exs which does not exist and needs maps to _i1_test.exs
    local tci = ClassInfo:new({
      classname = 'ModuleTestI4',
      inner_path = 'module_i4_test',
      path = path, -- i4
      type = ClassInfo.CT.TEST,
    })

    assert.same(path, tci:getPath())
    assert.same(true, tci:isTestClass())
    local utci = lang:onResolveCINoExistedPath(tci)
    assert.is_not_nil(utci)
    assert.same(tpath, utci:getPath())
  end)

  -- one test for multiple iterations
  it("RawSnippets: onResolveCINoExistedPath mod_i4 -> mod_test.exs", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local spath = proj_dir .. 'module_i4.ex'
    local tpath = proj_dir .. 'module_test.exs'
    sys.fs:mk(proj_dir, spath, tpath)

    -- RawSnippets
    local lang = ExLang:new({})
    lang.builder = lang:createBuilder(proj_dir, { flat = true, inmem = true })
    assert.is_not_nil(lang.builder)
    assert.is_true(MixBuilder.isRawSnippets(lang.builder))

    local path = proj_dir .. 'module_i4_test.exs'

    -- supposed _i4_test.exs which does not exist and needs maps to _test.exs
    local tci = ClassInfo:new({
      classname = 'ModuleTestI4',
      inner_path = 'module_i4_test',
      path = path, -- i4
      type = ClassInfo.CT.TEST,
    })

    assert.same(path, tci:getPath())
    assert.same(true, tci:isTestClass())
    local utci = lang:onResolveCINoExistedPath(tci)
    assert.is_not_nil(utci)
    assert.same(tpath, utci:getPath())
  end)
end)


describe("env.langs.elixir.ExLang", function()
  it("createMixExsConfig flat_project", function()
    local proj_root = '/tmp/dev/workspace/proj_a/'
    sys:mk(proj_root)
    local lang = ExLang:new(nil, proj_root, MixBuilder:new())
    local opts = { flat_project = true }
    lang:createProjectConfigs(opts) --MixExsConfig(opts)
    local fw = lang:getProjectBuilder():getFileWriter()
    local exp_files =
        "ok: /tmp/dev/workspace/proj_a/mix.exs\n" ..         -- 1
        "ok: /tmp/dev/workspace/proj_a/test_helper.exs\n" .. -- 2
        "ok: /tmp/dev/workspace/proj_a/.formatter.exs\n" ..  -- 3
        "ok: /tmp/dev/workspace/proj_a/.gitignore\n"         -- 4

    assert.same(exp_files, lang.builder:getFileWriter():getReadableReport())
    assert.same('/tmp/dev/workspace/proj_a/mix.exs', fw:getEntry(1))
    local exp = [[
defmodule Project.MixProject do
  use Mix.Project

  def project do
    [
      app: :app,
      version: "0.1.0",
      elixir: "~> 1.16",
      elixirc_paths: ["./"],
      test_paths: ["./"],
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: []
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/repo/lib.git", tag: "0.1.0"}
    ]
  end
end
]]
    assert.same(exp, sys:get_file_content(fw:getEntry(1) or ''))
    local exp4 = [[
# Used by "mix format"
[
  inputs: ["{mix,.formatter}.exs", "{config,.,.}/**/*.{ex,exs}"]
]
]]
    assert.same(exp4, sys:get_file_content(fw:getEntry(3) or ''))
  end)
end)

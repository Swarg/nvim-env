-- 13-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.langs.elixir.ExUnit");

--[[

MyModuleTest.MyModuleTest [my_module_test.exs]
  * test MyModule sum 1 + 2 = 3 [L#8][32m  * test MyModule sum 1 + 2 = 3 (0.00ms) [L#8][0m
  * test MyModule sum 1 + 3 = 4 [L#12][32m  * test MyModule sum 1 + 3 = 4 (0.00ms) [L#12][0m
  * test MyModule boom! [L#16][31m  * test MyModule boom! (3.1ms) [L#16][0m

  1) test MyModule boom! (MyModuleTest.MyModuleTest)
     [1m[30mmy_module_test.exs:16[0m
     [31mAssertion with == failed[0m
     [36mcode:  [0massert sum(1, 3) == 0
     [36mleft:  [0m[31m4[0m
     [36mright: [0m[32m0[0m
     [36mstacktrace:[0m
       my_module_test.exs:17: (test)

  * test MyModule boom map! empty [L#20][31m  * test MyModule boom map! empty (0.01ms) [L#20][0m

  2) test MyModule boom map! empty (MyModuleTest.MyModuleTest)
     [1m[30mmy_module_test.exs:20[0m
     [31mAssertion with == failed[0m
     [36mcode:  [0massert amap == %{}
     [36mleft:  [0m%{[31mb: "value"[0m, [31mc: ["a", "list"][0m, [31mkey: 8[0m}
     [36mright: [0m%{}
     [36mstacktrace:[0m
       my_module_test.exs:22: (test)


Finished in 0.02 seconds (0.02s on load, 0.00s async, 0.00s sync)
[31m4 tests, 2 failures[0m

Randomized with seed 0
]]
describe("env.langs.elixir.ExUnit", function()

  it("success", function()
  --  assert.same(1, 'TODO')
  end)

end)

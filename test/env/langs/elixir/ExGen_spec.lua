-- 12-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

-- debug
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local ClassResolver = require("env.lang.ClassResolver")

local H = require("env.langs.elixir.ahelper")
-- local ExGen = require("env.langs.elixir.ExGen");

describe("env.langs.elixir.ExGen", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
  end)

  it("tooling", function()
    local pr = '/tmp/dev/workspace/proj_a/'
    sys:mk(pr, 'Makefile')
    local lang = H.mkExLang(pr)
    local gen = lang:getLangGen()
    assert.is_not_nil(lang)
    assert.is_not_nil(gen)
  end)



  it("generate module", function()
    local pr = '/tmp/dev/workspace/proj_a/'
    sys:mk(pr, './lib/', 'moduel.ex', '../test/', 'module_test.exs')
    local modulename = 'Package.SomeModule'
    local ci, gen, _ = H.mkClassInfoAndExGen(modulename, pr)
    -- local ci = ClassResolver.of(lang):lookupByClassName(modulename)
    --
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    local exp_fn = '/tmp/dev/workspace/proj_a/lib/package/some_module.ex'
    assert.same(exp_fn, ci:getPath())
    local opts = {}
    opts.no_date, opts.no_author = true, true
    assert.same(exp_fn, gen:createClassFile(ci, opts))

    local exp = [[
# @author AuthorName

defmodule Package.SomeModule do
  @moduledoc """
  """
end
]]
    assert.same(exp, sys:get_file_content(exp_fn))
  end)

  it("generate test module", function()
    local pr = '/tmp/dev/workspace/proj_a/'
    sys:mk(pr, './lib/', 'moduel.ex', '../test/', 'module_test.exs')
    local modulename = 'Package.SomeModuleTest'
    local ci, gen, lang = H.mkClassInfoAndExGen(modulename, pr)
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo
    local exp_fn = '/tmp/dev/workspace/proj_a/lib/package/some_module_test.exs'
    assert.same(exp_fn, ci:getPath())
    assert.is_nil(ci:getPair())

    local opts = {}
    opts.test_class, opts.no_date, opts.no_author = true, true, true
    assert.same(exp_fn, gen:createClassFile(ci, opts))

    local exp = [[
# @author AuthorName
defmodule Package.SomeModuleTest do

  describe "${PAIR_CLASSNAME}" do

    # @tag :pending
    test "success" do
      assert 1 + 1 == 2
    end
  end
end
]]
    assert.same(exp, sys:get_file_content(exp_fn))
    sys.fs:rm(exp_fn) -- free to recreate a new one

    -- resolve pair
    assert.is_not_nil(ClassResolver.of(lang):getOppositeClass(ci))
    assert.is_not_nil(ci:getPair())

    assert.same(exp_fn, gen:createClassFile(ci, opts))
    local exp2 = [[
# @author AuthorName
defmodule Package.SomeModuleTest do

  describe "Package.SomeModule" do

    # @tag :pending
    test "success" do
      assert 1 + 1 == 2
    end
  end
end
]]
    assert.same(exp2, sys:get_file_content(exp_fn))
  end)
end)

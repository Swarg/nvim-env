-- 12-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
-- local su = require 'env.sutil'

-- local NVim = require("stub.vim.NVim")
-- local nvim = NVim:new() ---@type stub.vim.NVim
local OS = require 'stub.os.OSystem'
local sys = OS:new() -- it also creates a stub for the FileSystem too

-- local ExLang = require("env.langs.elixir.ExLang");
local MixBuilder = require("env.langs.elixir.MixBuilder");

local H = require("env.langs.elixir.ahelper")

-- source code of mix ~/dev/src/elixir/lang_elixir/lib/mix/lib/mix/project.ex

describe("env.langs.elixir.MixBuilder", function()
  after_each(function()
    require 'dprint'.disable()
    sys:clear_state()
    sys.fs:clear_state()
  end)

  it("isProjectRoot", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local proj_buildscript = '/tmp/dev/workspace/projects/proj_a/mix.exs'
    -- note if name is dir(with /) auto when cd into it after mkdir
    sys.fs:mk(proj_dir, 'mix.exs', './lib/', 'm.ex', '../test/', 'm_test.exs')

    assert.same(true, sys:set_file_content(proj_buildscript, H.mix_conf_01))
    -- print(sys:tree(proj_dir))
    --[[
    /tmp/dev/workspace/projects/proj_a
      mix.exs
      lib/
          src.ex
      test/
          src_test.exs
    ]]
    assert.same('directory', sys.fs:file_type(proj_dir))
    assert.same('directory', sys.fs:file_type(proj_dir .. "lib/"))
    assert.same('directory', sys.fs:file_type(proj_dir .. "test/"))

    assert.same('file', sys.fs:file_type(proj_dir .. "mix.exs"))
    assert.same('file', sys.fs:file_type(proj_dir .. "lib/m.ex"))
    assert.same('file', sys.fs:file_type(proj_dir .. "test/m_test.exs"))
    local exp = {
      project_root = '/tmp/dev/workspace/projects/proj_a/',
      buildscript = 'mix.exs',
      test = 'test/',
      src = 'lib/'
    }
    local userdata = {}
    local path = MixBuilder.isProjectRoot(proj_dir, userdata)
    assert.same(proj_buildscript, path)
    assert.same(exp, userdata)
    assert.same(H.mix_conf_01, sys.fs:get_file_content(proj_buildscript))
    -- next step loadMixExs
  end)

  --
  it("loadMixExs (part of MixBuilder:getSettings())", function()
    local proj_dir = '/tmp/dev/workspace/projects/proj_a/'
    local mix_fn = '/tmp/dev/workspace/projects/proj_a/mix.exs'
    sys.fs:mk(proj_dir, 'mix.exs', './lib/', 'm.ex', '../test/', 'm_test.exs')

    assert.same(true, sys:set_file_content(mix_fn, H.mix_conf_01))

    local exp_userdata = {
      project_root = "/tmp/dev/workspace/projects/proj_a/",
      buildscript = "mix.exs",
      src = "lib/",
      test = "test/"
    }
    local userdata = {}
    -- mix = ExLang:new({ project_root = proj_dir }):findprojectbuilder().builder
    assert.same(mix_fn, MixBuilder.isProjectRoot(proj_dir, userdata))
    assert.same(exp_userdata, userdata)

    local mix = MixBuilder:new(userdata) ---@cast mix env.langs.elixir.MixBuilder
    assert.is_not_nil(mix)
    local exp = {
      application = { extra_applications = { ':logger' } },
      project_name = 'Some.MixProject',
      project = {
        app = ':my_app',
        version = '0.1.0',
        deps = { ecto = '~> 3.0' },
        start_permanent = ':false'
      }
    }
    -- require 'alogger'.fast_setup()
    -- require'dprint'.enable_module(require('ex_parser.grammar'))
    local res = mix:loadBuildScript() -- mix:getSettings()
    assert.same(exp, res)
  end)

  it("parseBuildScript", function()
    local mix_exs = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_app,
      version: "0.1.0",
      elixirc_paths: [ "./" ],
      start_permanent: Mix.env() == :prod,
    ]
  end

  def application do [] end

end
]]
    local userdata = {
      project_root = "/tmp/dev/workspace/projects/proj_a/",
      buildscript = "mix.exs",
      src = "lib/",
      test = "test/"
    }
    local mix = MixBuilder:new(userdata) ---@cast mix env.langs.elixir.MixBuilder
    local exp = {
      application = {},
      project_name = 'Some.MixProject',
      project = {
        app = ':my_app',
        elixirc_paths = { './' },
        version = '0.1.0',
        start_permanent = ':false'
      }
    }
    local res = mix:parseBuildScript('mix.exs', mix_exs, 0)
    assert.same(exp, res)
  end)

  it("isFlatStructure", function()
    local userdata = {
      project_root = "/tmp/dev/workspace/projects/proj_a/",
      buildscript = "mix.exs",
    }
    local mix = MixBuilder:new(userdata) ---@cast mix env.langs.elixir.MixBuilder
    mix.settings = {
      project_name = 'Some.MixProject',
      project = {
        app = ':my_app',
        elixirc_paths = { './' },
        test_paths = { './' },
      }
    }
    assert.same(true, mix:isFlatStructure())

    mix.settings = {
      project_name = 'Some.MixProject',
      project = {
        app = ':my_app',
        elixirc_paths = { './lib' },
        test_paths = { './' },
      }
    }
    assert.same(false, mix:isFlatStructure())

    mix.settings = {
      project_name = 'Some.MixProject',
      project = {
        app = ':my_app',
        elixirc_paths = { './' },
        test_paths = { './test' },
      }
    }
    assert.same(false, mix:isFlatStructure())

    mix.settings = {
      project_name = 'Some.MixProject',
      project = { app = ':my_app' }
    }
    assert.same(false, mix:isFlatStructure())

    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same(false, MixBuilder.isFlatStructure(nil))
  end)
end)


-- lookup test_helper.exs for FlatProject
describe("env.langs.elixir.MixBuilder FlatProject getTestHelperPath", function()
  local ExLang = require("env.langs.elixir.ExLang");

  after_each(function()
    require 'dprint'.disable()
    sys:clear_state()
    sys.fs:clear_state()
  end)

  local function mkLangAndMixBuilder(pr)
    local lang = ExLang:new({})
    lang.builder = lang:createBuilder(pr, { flat = true, inmem = true })
    return lang, lang.builder
  end

  it("tooling mkLangAndMixBuilder", function()
    local dir = '/tmp/dev/workspace/exercises/10/some/'
    local lang, mix = mkLangAndMixBuilder(dir)

    assert.is_not_nil(lang.builder)
    assert.equal(lang.builder, mix)
    assert.is_true(MixBuilder.isRawSnippets(lang.builder))
    assert.is_true(MixBuilder.isRawSnippets(mix))
  end)

  it("deep 0", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/solution_1/'
    local th_fn = '/tmp/dev/workspace/exercises/10/solution_1/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same('test_helper.exs', mix:getTestHelperPath())
  end)

  it("deep 1", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/solution_1/'
    local th_fn = '/tmp/dev/workspace/exercises/10/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(th_fn, mix:getTestHelperPath())
  end)

  it("deep 2", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/solution_1/'
    local th_fn = '/tmp/dev/workspace/exercises/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(th_fn, mix:getTestHelperPath())
  end)

  it("deep 3", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/simple_registry/'
    local th_fn = '/tmp/dev/workspace/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(th_fn, mix:getTestHelperPath())
  end)

  it("deep 4", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/simple_registry/'
    local th_fn = '/tmp/dev/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(th_fn, mix:getTestHelperPath())
  end)

  it("deep 5", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/simple_registry/'
    local th_fn = '/tmp/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(nil, mix:getTestHelperPath())
  end)

  it("deep 6", function()
    local c_dir = '/tmp/dev/workspace/exercises/10/simple_registry/'
    local th_fn = '/test_helper.exs'
    sys.fs:mk(c_dir, th_fn)
    assert.is_true(sys:is_file_exits(th_fn))
    local _, mix = mkLangAndMixBuilder(c_dir)
    assert.same(nil, mix:getTestHelperPath())
  end)
end)

-- 12-03-2024 @author Swarg
local M = {}
--

local ExLang = require 'env.langs.elixir.ExLang'
-- local ExUnit =


--------------------------------------------------------------------------------
--                   Helper implementation for specific lang
--------------------------------------------------------------------------------
--
local H = require("env.lang.oop.stub.ATestHelper")
H.inherit(M) -- inherit functions from abstract-lang helper

--
-- factory to create ClassInfo LangGen Lang
--
---@param classname string
---@param project_root string?
--
---@return env.lang.ClassInfo
---@return env.langs.elixir.ExGen
---@return env.langs.elixir.ExLang
function M.mkClassInfoAndExGen(classname, project_root)
  local ci, gen, lang = H.mkClassInfoAndLangGen(ExLang, classname, project_root)
  ---@cast gen env.langs.elixir.ExGen
  ---@cast lang env.langs.elixir.ExLang
  return ci, gen, lang
end

--
-- factory to create ExLang for given project_root
--
---@return env.langs.elixir.ExLang
---@return string
function M.mkExLang(project_root)
  local lang, pr = H.mkLang(ExLang, project_root)
  ---@cast lang env.langs.elixir.ExLang
  return lang, pr
end

M.mix_conf_01 = [[
defmodule Some.MixProject do
  use Mix.Project

  def project do
    [
      app: :my_app,
      version: "0.1.0",
      # elixir: "~> 1.16",
      # elixirc_paths: ["./lib"]
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ecto, "~> 3.0"},
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
]]

-- minimal stub for fast parsing
M.mix_conf_00 = [[
defmodule Some.MixProject do
  use Mix.Project
  def project do
    [
      app: :my_app,
      version: "0.1.0",
      start_permanent: false,
      deps: []
    ]
  end
  def application do
    [
      extra_applications: false
    ]
  end
end
]]

return M

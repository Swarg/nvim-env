-- 27-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os();

local JSourcer = require 'env.langs.java.JSourcer'
local H = require 'env.langs.java.ahelper'
local fs = require 'env.files'

describe("env.langs.java.JSourcer", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    -- require 'alogger'.fast_off()
  end)

  it("fixPackagesInImports", function()
    local lang = H.mkJLang('/tmp/proj')
    local obj = JSourcer:new(nil, lang)

    local lines = {
      'package org.app;',              -- 1
      'import pkg.util.A;',            -- 2
      'import static pkg.util.A.*;',   -- 3
      'import static pkg.util.B.*;',   -- 3
      'public class A {',              -- 4
      '    public A(int x, int y) {}', -- 5
      '}'                              -- 6
    }
    local source_file = '/tmp/proj/src/main/java/org/app/A.java'
    nvim:get_os():set_file(source_file, lines)
    local t = {}
    local tmp_lines = {}
    local cd, _ = obj:loadClassDef(source_file, function() end, tmp_lines)
    assert.is_table(cd) ---@cast cd table
    local map = obj:getRenamePackageMapping()
    map['pkg.util.A'] = 'pkg.new.util.A'
    map['pkg.util.B'] = 'pkg.new.util.B'

    obj:fixPackagesInImports(cd, t)
    assert.same(lines, tmp_lines)
    local exp = {
      [2] = 'import pkg.new.util.A;',
      [3] = 'import static pkg.new.util.A.*;',
      [4] = 'import static pkg.new.util.B.*;',
    }

    assert.same(exp, t)
  end)


  local lines_org_app_ClassB = {
    'package org.app;',
    '',
    'import org.something.ClassA;',
    'import org.common.MyClass;',
    'import org.common.AnotherClass;',
    'import static org.common.MyClass.methodA;',
    'import static org.common.MyClass.methodB;',
    'import static org.common.AnotherClass.methodC;',
    '',
    'public class ClassB {',
    '    public ClassB(int x, int y) {}',
    '}'
  }
  it("fixPackagesInImports", function()
    -- given
    local lang = H.mkJLang('/tmp/proj')
    local obj = JSourcer:new(nil, lang)

    local source_file = '/tmp/proj/src/main/java/org/app/ClassB.java'
    nvim:get_os():set_file(source_file, lines_org_app_ClassB)

    local map = obj:getRenamePackageMapping()
    map['org.common.MyClass'] = 'org.mylib.app.MyClass'

    -- when
    local t, tmp_lines = {}, {}
    local cd, _ = obj:loadClassDef(source_file, function() end, tmp_lines)
    assert.is_table(cd) ---@cast cd table

    obj:fixPackagesInImports(cd, t)
    assert.same(lines_org_app_ClassB, tmp_lines)

    -- then
    local expLinesFix = { -- update lines mapping
      [4] = 'import org.mylib.app.MyClass;',
      [6] = 'import static org.mylib.app.MyClass.methodA;',
      [7] = 'import static org.mylib.app.MyClass.methodB;'
    }
    assert.same(expLinesFix, t)
  end)

  it("fixImportsInClassFile", function()
    local lang = H.mkJLang('/tmp/proj')
    local obj = JSourcer:new(nil, lang)

    local source_file = '/tmp/proj/src/main/java/org/app/ClassB.java'
    sys:set_file(source_file, lines_org_app_ClassB)

    local update_line_mapping = { -- update lines mapping
      [4] = 'import org.mylib.app.MyClass;',
      [6] = 'import static org.mylib.app.MyClass.methodA;',
      [7] = 'import static org.mylib.app.MyClass.methodB;'
    }
    -- when
    obj:updateFileContent(source_file, update_line_mapping);

    local exp = {
      'package org.app;',
      '',
      'import org.something.ClassA;',
      'import org.mylib.app.MyClass;',
      'import org.common.AnotherClass;',
      'import static org.mylib.app.MyClass.methodA;',
      'import static org.mylib.app.MyClass.methodB;',
      'import static org.common.AnotherClass.methodC;',
      '',
      'public class ClassB {',
      '    public ClassB(int x, int y) {}',
      '}'
    }
    assert.same(exp, sys:get_file_lines(source_file))
  end)


  it("getInnerPathFromSource", function()
    -- given
    local lang = H.mkJLang('/tmp/proj')
    local obj = JSourcer:new(nil, lang)

    local lines = {
      'package org.app;',              -- 1
      'import pkg.util.A;',            -- 2
      'import static pkg.util.A.*;',   -- 3
      'import static pkg.util.B.*;',   -- 3
      'public class A {',              -- 4
      '    public A(int x, int y) {}', -- 5
      '}'                              -- 6
    }
    local source_file = '/tmp/A.java'
    nvim:get_os():set_file(source_file, lines)
    -- then
    local res, err = obj:getInnerPathFromSource(source_file)
    assert.is_nil(err)
    assert.same('org/app/A', res)
  end)

  -- use case: class-file saved without package subdirs, restory from source-file
  it("addExternalSourceFile", function()
    -- given
    local lang = H.mkJLang('/home/user/project/')
    local obj = JSourcer:new(nil, lang)
    local external_source_lines = {
      'package org.some.app;',         -- 1 will pick subdirs from this line
      'import pkg.util.A;',            -- 2
      'import static pkg.util.A.*;',   -- 3
      'import static pkg.util.B.*;',   -- 3
      'public class A {',              -- 4
      '    public A(int x, int y) {}', -- 5
      '}'                              -- 6
    }
    local source_file = '/tmp/decompiled/A_ab_cdef_01.java'
    nvim:get_os():set_file(source_file, external_source_lines)

    local opts = {
      quiet = true,
      fix_pkg = true,
      source_root = '/tmp/decompiled/'
    }
    -- when
    local new_path, err = obj:addExternalSourceFile(source_file, opts)
    -- then
    assert.is_true(fs.dir_exists(opts.source_root))
    assert.is_nil(err)
    local exp_new_path = '/home/user/project/src/main/java/org/some/app/A.java'
    assert.same(exp_new_path, new_path)
    local exp_content = external_source_lines
    assert.same(exp_content, nvim:get_os():get_file_lines(exp_new_path))
  end)

  it("addRenamePkgMappingFromGitStatus", function()
    local lang = H.mkJLang('/home/user/project/')
    local sourcer = JSourcer:new(nil, lang)

    local exp = 'org.lib.Prop'
    assert.same(exp, lang:getClassNameForInnerPath("src/main/java/org/lib/Prop"))

    local output = {
      "On branch main",
      "Your branch is ahead of 'origin/main' by 1 commit.",
      "  (use \"git push\" to publish your local commits)",
      "",
      "Changes to be committed:",
      "  (use \"git restore --staged <file>...\" to unstage)",
      "	renamed:    src/main/java/org/lib/Prop.java -> src/main/java/org/lib/conf/Prop.java",
      "	renamed:    src/main/java/org/common/StreamUtil.java -> src/main/java/org/lib/io/StreamUtil.java",
      "	renamed:    src/main/java/org/jvmutil/JvmUtil.java -> src/main/java/org/swarg/lib/jvm/JvmUtil.java",
      "	modified:   src/main/java/org/lib/research/ObjResearch.java"
    }
    assert.same(3, sourcer:addRenamePkgMappingFromGitStatus(output))
    local actualResult = sourcer:getRenamePackageMapping()
    local expMapping = {
      ['org.lib.Prop'] = 'org.lib.conf.Prop',
      ['org.common.StreamUtil'] = 'org.lib.io.StreamUtil',
      ['org.jvmutil.JvmUtil'] = 'org.swarg.lib.jvm.JvmUtil'
    }
    assert.same(expMapping, actualResult)
  end)
end)

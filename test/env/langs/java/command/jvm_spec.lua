-- 22-02-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.command.jvm'

describe("env.langs.java.command.jvm", function()
  it("find_last_agent_output_message", function()
    local output = {
      "some outdated and non-relevant data...",
      "20:26:45 [INFO] Setup output log file: /tmp/agent-report.log",
      "20:26:45 [INFO] [Agent] PID:[1103127] via agentmain, args:'hotswap pkg.MyClass /home/user/dev/project/build/classes/pkg/MyClass.class'",
      "redefineClassBytecode pkg.MyClass /home/user/dev/project/build/classes/pkg/MyClass.class",
      "",
      "found class pkg.MyClass @828447060 CL: sun.misc.Launcher$AppClassLoader@18b4aac2",
      "",
      "redefined: true"
    }
    local f = M.find_last_agent_output_message
    local exp = {
      "20:26:45 [INFO] [Agent] PID:[1103127] via agentmain, args:'hotswap pkg.MyClass /home/user/dev/project/build/classes/pkg/MyClass.class'",
      'redefineClassBytecode pkg.MyClass /home/user/dev/project/build/classes/pkg/MyClass.class',
      '',
      'found class pkg.MyClass @828447060 CL: sun.misc.Launcher$AppClassLoader@18b4aac2',
      '',
      'redefined: true'
    }
    assert.same(exp, f(output, 1103127, "hotswap"))
  end)
end)

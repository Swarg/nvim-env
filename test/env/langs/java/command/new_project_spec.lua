-- 16-07-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

-- local fs = require('env.files')
-- local uv = require("luv")

local Lang = require 'env.lang.Lang'
local M = require 'env.langs.java.command.new_project'

------------------------ impl for specific lang --------------------------------
local H = require("env.langs.java.ahelper")
-- local mkClassInfoAndGen = H.mkClassInfoAndGen
local rnf, new_cli = H.root_and_file, H.new_cli
--------------------------------------------------------------------------------
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


describe("env.langs.java.command.new_project", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)

  it("cmd_pom_xml only pom.xml without project-files itself", function()
    local cwd, pom_xml = rnf('/home/user/dev/project_a', 'pom.xml')
    local cmd_line = '--yes --quiet' -- confirmation
    --
    assert.same('/home/user/', os.getenv('PWD'))
    sys:mkcdir(cwd)
    assert.same(cwd, os.getenv('PWD'))

    local exp = "/home/user/dev/project_a\n0 directories, 0 files\n"
    assert.same(exp, sys:tree())

    local w = new_cli(nil, cmd_line)

    -- require'alogger'.fast_setup()
    M.cmd_pom_xml(w) -- payload
    local exp2 = [[
/home/user/dev/project_a
    pom.xml
0 directories, 1 file
]]
    assert.same(exp2, sys:tree())
    local exp3 = [[<?xml version="1.0" encoding="UTF-8"?>]]
    assert.same(exp3, (sys:get_file_lines(pom_xml) or E)[1])
  end)


  it("cmd_pom_xml", function()
    local cwd, pom_xml = rnf('/home/user/dev/project_a', 'pom.xml')
    local cmd_line = '--new-project --yes --quiet'
    -- local cmd_line = '--quiet --yes'
    -- --yes to confirm defualt proj props - to skip interactive mode with ObjEditor
    --
    sys:mkcdir(cwd)

    local exp = "/home/user/dev/project_a\n0 directories, 0 files\n"
    assert.same(exp, sys:tree())

    local w = new_cli(nil, cmd_line)

    -- require'alogger'.fast_setup(false, 0)
    M.cmd_pom_xml(w) -- payload
    local exp2 = [[/home/user/dev/project_a
    pom.xml
    src/
        main/
            java/
                pkg/
                    MainClass.java
        test/
            java/
                pkg/
                    MainClassTest.java
7 directories, 3 files
]]
    assert.same(exp2, sys:tree())
    local exp3 = [[<?xml version="1.0" encoding="UTF-8"?>]]
    assert.same(exp3, (sys:get_file_lines(pom_xml) or E)[1])
  end)

  it("cmd_flat", function()
    local cwd, _ = rnf('/home/user/dev/flat', 'pom.xml')
    local cmd_line = '--quiet --yes'

    sys:mkcdir(cwd)

    local exp = "/home/user/dev/flat\n0 directories, 0 files\n"
    assert.same(exp, sys:tree())

    local w = new_cli(nil, cmd_line)

    M.cmd_flat(w)
    assert.same(exp, sys:tree())
    -- assert.same(0, (Lang.getActiveProjects()))

    local lang = Lang.getOpenedProject(cwd)
    local exp_lang = {
      already_existed = false,
      project_root = '/home/user/dev/flat/',
      src = './',
      test = './',
      builder = {
        buildscript = "", -- no
        project_root = "/home/user/dev/flat/",
        src = './',
        test = './',
        -- settings = {
        --   project = {
        --     build = {
        --       sourceDirectory = "./"
        --     },
        --     raw_snippets = true
        --   }
        -- }
      }
    }
    assert.same(exp_lang, lang)
    assert.same(true, lang.builder:isFlatStructure())
    assert.same('env.langs.java.JBuilder', lang.builder:getClassName())
  end)

  it("cmd_flat with files", function()
    local cwd, _ = rnf('/home/user/dev/flat', 'pom.xml')
    local cmd_line = '--quiet --yes --new-project -c pkg.App'

    sys:mkcdir(cwd)

    local exp_before = "/home/user/dev/flat\n0 directories, 0 files\n"
    assert.same(exp_before, sys:tree())

    local w = new_cli(nil, cmd_line)

    M.cmd_flat(w) -- payload

    local exp_after = [[
/home/user/dev/flat
    pkg/
        App.java
        AppTest.java
1 directory, 2 files
]]
    assert.same(exp_after, sys:tree())

    local lang = Lang.getOpenedProject(cwd)
    assert.same({ './', './', cwd }, { lang.src, lang.test, lang.project_root })
    assert.same(true, lang.builder:isFlatStructure())
    assert.same('env.langs.java.JBuilder', lang.builder:getClassName())
  end)
end)

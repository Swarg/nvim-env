-- 07-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local M = require 'env.langs.java.command.new_stuff'

local Lang = require 'env.lang.Lang'
------------------------ impl for specific lang --------------------------------
local H = require("env.langs.java.ahelper")
local setupProject, new_cli = H.setupProject, H.new_cli
--------------------------------------------------------------------------------
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


describe("env.langs.java.command.new_stuff", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
  end)

  -- checking file creation and protection against overwriting an existing file
  it("cmd_webapp_lua", function()
    local setup, gen = H.get_springboot3_app1_setup_mvn()
    setupProject(sys, setup)

    local w = new_cli(gen, '--quiet --no-date --author A')
    M.integration_test.cmd_webapp_lua(w) -- payload


    local path = (w.vars._res or E).path
    local exp_path = '/home/me/dev/sb_app1/src/test/_lua/webapp_spec.lua'
    assert.same(exp_path, path)
    local exp_st = 'ok: /home/me/dev/sb_app1/src/test/_lua/webapp_spec.lua'
    assert.same(exp_st, gen:getReadableSaveReport())

    -- assert.same(exp, sys:tree(setup.project_root))
    local lines = sys:get_file_lines(path)
    lines[#lines] = 'end) -- with sign' -- to check rewrite in retry cmd

    assert.is_table(lines) ---@cast lines table
    local exp_slice = {
      '--  @author A',
      '-- dependencies:',
      '--   - luarocks: busted, lhttp, db-env',
      'end) -- with sign'
    }
    assert.same(exp_slice, { lines[1], lines[4], lines[5], lines[#lines] })

    -- ensure last line in the file content was changed
    local lines2 = sys:get_file_lines(path)
    assert.same('end) -- with sign', (lines2 or E)[#(lines2 or E)])

    -- repeating the command should not overwrite an existing file

    w = new_cli(gen, '--quiet --no-date --author A')
    gen:getFileWriter():clearEntries()

    M.integration_test.cmd_webapp_lua(w) -- payload
    local exp = 'already exists: /home/me/dev/sb_app1/src/test/_lua/webapp_spec.lua'
    assert.same(exp, gen:getReadableSaveReport())

    lines = sys:get_file_lines(path)
    lines[#lines] = 'end) -- with sign'
  end)


  it("cmd_makefile", function()
    local setup, gen = H.get_springboot3_app1_setup_mvn(true)
    local files = {
      [setup.project_root .. 'src/test/_lua/a_spec.lua'] = '-- empty'
    }
    setupProject(sys, setup, files)
    -- assert.same(1,  sys:tree())

    local w = new_cli(gen, '--quiet')

    M.cmd_makefile(w) -- payload

    assert.same(nil, w.errors)
    assert.same('ok: /home/me/dev/sb_app1/Makefile', gen:getReadableSaveReport())

    local path = (w.vars._res or E).path

    local exp = [[
ARTIFACT=app
VERSION=0.1.0

#rellative path in servlet-container and url
WEBAPP_ROOT=
PORT=8080

# for debugging
JPDA_TRANSPORT=dt_socket
JPDA_ADDRESS=127.0.0.1:5005
JPDA_SUSPEND=n
JDWP_OPTS=transport=$(JPDA_TRANSPORT),address=$(JPDA_ADDRESS),server=y,suspend=$(JPDA_SUSPEND)
JPDA_ARG=-agentlib:jdwp=$(JDWP_OPTS)

# this script used qdb cli tool what can be downloaded from (see install-qdb)
QDB_URL=https://gitlab.com/Swarg/dotfiles/-/raw/main/tools/tools/qdb?ref_type=heads
QDB_DST=${HOME}/.local/bin/qdb


.PHONY: build clean-build run run-jar debug debug-jar test

install-lua-deps:
	sudo apt update && sudo apt install luarocks
	sudo luarocks install busted
	sudo luarocks install luasql-postgres
	echo todo db-env lhttp

install-qdb:
	curl -o $(QDB_DST) $(QDB_URL)


# configure permission to connect to db from java-app
create-dbuser-and-db:
	qdb create-user-and-db

# drop existed tables and refill with test datas
# based on ./sql/scheme-ddl.sql & ./sql/insert-dml.sql
create-test-tables:
	qdb create-tables populate-tables



# Command Interface

build:
	mvn package

clean-build:
	mvn clean package

run:
	mvn spring-boot:run

run-jar:
	java -jar ./target/$(ARTIFACT)-$(VERSION).jar

debug:
	mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Xdebug -Xrunjdwp:$(JDWP_OPTS)"


debug-jar:
	java $(JPDA_ARG) -jar ./target/$(ARTIFACT)-$(VERSION).jar

# Before running the tests, you need to start the application itself
test:
	busted src/test/_lua/


]]
    assert.same(exp, sys:get_file_content(path))
  end)
end)

-- 30-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.name_mapping'

local mappind_dir = os.getenv('PWD') .. '/test/env/resources/java/mapping/'

local mapping = {
  fields = {
    field_100001_g = 'readableFieldName1',
    field_100002_T = 'readableFieldName2',
    field_123456_g = 'readableFieldName3',
  },
  methods = {
    func_100000_b = 'methodName0',
    func_123456_g = 'methodName1',
    func_100011_g = 'methodName2',
    func_100012_b = 'methodName2',
  },
  params = {
    p_100000_1_ = 'param0',
    p_100002_1_ = 'param1',
    p_100002_2_ = 'param2',
    p_100002_3_ = 'param3',
  }
}

-- clazz = {
--     lne = 21,
--     fields = { {
--         ln = 10,
--         mods = 2,
--         typ = "double"
--         name = "field_100001_g",
--       }, {
--         ln = 11,
--         mods = 2,
--         typ = "int"
--         name = "field_100002_T",
--       }
--     },
--     methods = {
--       {
--         ln = 13,
--         lne = 15,
--         mods = 1,
--         typ = "void"
--         name = "func_100011_g",
--         params = { { "boolean", "p_100000_1_" } },
--         throws = { { "IOException" } },
--       },
--       {
--         ln = 17,
--         lne = 19,
--         mods = 0,
--         typ = "void",
--         name = "func_100012_b",
--         params = {
--           { "String", "p_100002_1_" },
--           { "int", "p_100002_2_" },
--           { "boolean", "p_100002_3_" }
--         }
--       }
--     }
--   }

describe("env.langs.java.util.name_mapping", function()
  it("apply_name_mapping_to_file", function()
    local fn = mappind_dir .. 'MyClass.java'
    local exp = {
      'package org.comp;',
      '',
      '// field_123456_g',
      '// func_123456_g',
      '/**',
      ' * func_100000_b',
      ' */',
      'public class MyClass {',
      '',
      '    private double readableFieldName1;',
      '    private int readableFieldName2;',
      '',
      '    public void methodName2(boolean param0) throws IOException {',
      '        System.out.println(param0);',
      '    }',
      '',
      '    void methodName2(String param1, int param2, boolean param3) {',
      '        System.out.println(param1 + param2 + param3);',
      '    }',
      '',
      '}'
    }
    local report = {}
    assert.same(exp, M.apply_name_mapping_to_file(fn, mapping, report))
    assert.same({ fields = 2, methods = 2, params = 8 }, report)
  end)

  it("apply_name_mapping_to_line", function()
    local f = M.apply_name_mapping_to_line
    local lines = { 'ab func_100012_b (p_100000_1_, p_100002_3_)' }
    -- 	{ { "public", 5, 10 }, { "void", 12, 15 }, { "methodName2", 17, 27 }, ..
    local report = {}
    assert.same('ab methodName2 (param0, param3)', f(lines, 1, mapping, report))
    assert.same({ methods = 1, params = 2 }, report)
  end)
end)

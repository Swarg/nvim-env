-- 14-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)
_G.TEST = true
local M = require 'env.langs.java.util.junit'

describe("env.langs.java.util.junit", function()
  --
  it("normalized_xml_str", function()
    local f = M.normalized_xml_str
    local line = 'expected:&lt;1&gt; but was:&lt;0&gt;"'
    assert.same('expected:<1> but was:<0>"', f(line))
  end)

  it("normalized_xml_str amp quot apos #10", function()
    local f = M.normalized_xml_str
    local line = 'expected:&lt;&amp;&quot;&apos;&#10;&gt; but was:&lt;0&gt;"'
    local exp = "expected:<&\"'\n> but was:<0>\""
    assert.same(exp, f(line))
  end)

  it("normalized_xml_str amp quot apos #10", function()
    local f = M.normalized_xml_str
    local line = [[
Expected size: 2 but was: 3 in:
[&quot;[ERROR] message with without args&quot;,
    &quot;java.lang.Throwable&quot;,
    &quot;	at org.app.ATest.method(ATest.java:12)&quot;]
]]
    local exp = [[
Expected size: 2 but was: 3 in:
["[ERROR] message with without args",
    "java.lang.Throwable",
    "	at org.app.ATest.method(ATest.java:12)"]
]]
    assert.same(exp, f(line))
  end)


  --
  it("parse_open_xml_tag", function()
    local line = '<testsuite name="org.common.SomeTest" tests="2" skipped="0"'
        .. ' failures="2" errors="0" timestamp="2025-01-14T11:05:59" '
        .. 'hostname="debian" time="0.011">'

    local exp = {
      tag = 'testsuite',
      attrs = {
        name = 'org.common.SomeTest',
        tests = '2',
        skipped = '0',
        failures = '2',
        errors = '0',
        timestamp = '2025-01-14T11:05:59',
        hostname = 'debian',
        time = '0.011',
      }
    }
    assert.same(exp, M.test.parse_open_xml_tag(line))

    local exp2 = { tag = 'properties', close = true }
    assert.same(exp2, M.test.parse_open_xml_tag("<properties/>"))

    local exp3 = { tag = 'system-out' }
    assert.same(exp3, M.test.parse_open_xml_tag("<system-out>"))

    local exp4 = { tag = 'properties' }
    assert.same(exp4, M.test.parse_open_xml_tag("<properties>"))

    local exp5 = { tag = 'properties', close = true, ind = '' }
    assert.same(exp5, M.test.parse_open_xml_tag("</properties>"))

    local exp6 = { tag = 'properties', text = 'inner text' }
    assert.same(exp6, M.test.parse_open_xml_tag("<properties>inner text"))
  end)

  local sample_report_0_path = './test/env/resources/gradle/app/build/test-results/test/TEST-org.common.SomeTest.xml'
  local sample_testcase_0_1_trace = {
    "java.lang.AssertionError: expected:&lt;1&gt; but was:&lt;0&gt;",
    "\tat org.junit.Assert.fail(Assert.java:89)",
    "\tat org.junit.Assert.failNotEquals(Assert.java:835)",
    "\tat org.junit.Assert.assertEquals(Assert.java:647)",
    "\tat org.junit.Assert.assertEquals(Assert.java:633)",
    "\tat org.common.SomeTest.test_2(SomeTest.java:41)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "\tat java.lang.reflect.Method.invoke(Method.java:498)",
    "\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:59)",
    "\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)",
    "\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:56)",
    "\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)",
    "\tat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)",
    "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner$1.evaluate(BlockJUnit4ClassRunner.java:100)",
    "\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:366)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:103)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:63)",
    "\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)",
    "\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)",
    "\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)",
    "\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)",
    "\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)",
    "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
    "\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.runTestClass(JUnitTestClassExecutor.java:112)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(JUnitTestClassExecutor.java:58)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(JUnitTestClassExecutor.java:40)",
    "\tat org.gradle.api.internal.tasks.testing.junit.AbstractJUnitTestClassProcessor.processTestClass(AbstractJUnitTestClassProcessor.java:54)",
    "\tat org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(SuiteTestClassProcessor.java:53)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "\tat java.lang.reflect.Method.invoke(Method.java:498)",
    "\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:36)",
    "\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)",
    "\tat org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(ContextClassLoaderDispatch.java:33)",
    "\tat org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:92)",
    "\tat com.sun.proxy.$Proxy4.processTestClass(Unknown Source)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker$2.run(TestWorker.java:181)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.executeAndMaintainThreadName(TestWorker.java:130)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:101)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:61)",
    "\tat org.gradle.process.internal.worker.child.ActionExecutionWorker.execute(ActionExecutionWorker.java:56)",
    "\tat org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:122)",
    "\tat org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:69)",
    "\tat worker.org.gradle.process.internal.worker.GradleWorkerMain.run(GradleWorkerMain.java:69)",
    "\tat worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)"
  }
  local sample_testcase_1_0_trace = {
    "org.junit.ComparisonFailure: expected:&lt;[]&gt; but was:&lt;[Bind Class for {0} - Recognized java.lang.Object]&gt;",
    "\tat org.junit.Assert.assertEquals(Assert.java:117)",
    "\tat org.junit.Assert.assertEquals(Assert.java:146)",
    "\tat org.common.SomeTest.test_MessageFormat(SomeTest.java:35)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "\tat java.lang.reflect.Method.invoke(Method.java:498)",
    "\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:59)",
    "\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)",
    "\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:56)",
    "\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)",
    "\tat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)",
    "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner$1.evaluate(BlockJUnit4ClassRunner.java:100)",
    "\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:366)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:103)",
    "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:63)",
    "\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)",
    "\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)",
    "\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)",
    "\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)",
    "\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)",
    "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
    "\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.runTestClass(JUnitTestClassExecutor.java:112)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(JUnitTestClassExecutor.java:58)",
    "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.execute(JUnitTestClassExecutor.java:40)",
    "\tat org.gradle.api.internal.tasks.testing.junit.AbstractJUnitTestClassProcessor.processTestClass(AbstractJUnitTestClassProcessor.java:54)",
    "\tat org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.processTestClass(SuiteTestClassProcessor.java:53)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "\tat java.lang.reflect.Method.invoke(Method.java:498)",
    "\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:36)",
    "\tat org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:24)",
    "\tat org.gradle.internal.dispatch.ContextClassLoaderDispatch.dispatch(ContextClassLoaderDispatch.java:33)",
    "\tat org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:92)",
    "\tat com.sun.proxy.$Proxy4.processTestClass(Unknown Source)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker$2.run(TestWorker.java:181)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.executeAndMaintainThreadName(TestWorker.java:130)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:101)",
    "\tat org.gradle.api.internal.tasks.testing.worker.TestWorker.execute(TestWorker.java:61)",
    "\tat org.gradle.process.internal.worker.child.ActionExecutionWorker.execute(ActionExecutionWorker.java:56)",
    "\tat org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:122)",
    "\tat org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:69)",
    "\tat worker.org.gradle.process.internal.worker.GradleWorkerMain.run(GradleWorkerMain.java:69)",
    "\tat worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)"
  }
  local sample_testcase_2_0_trace = {
    "java.lang.IllegalArgumentException: can't parse argument number: \" type=\"java.lang.IllegalArgumentException\">java.lang.IllegalArgumentException: can't parse argument number: ",
    "	at java.text.MessageFormat.makeFormat(MessageFormat.java:1429)",
    "	at java.text.MessageFormat.applyPattern(MessageFormat.java:479)",
    "	at java.text.MessageFormat.&lt;init&gt;(MessageFormat.java:362)",
    "	at java.text.MessageFormat.format(MessageFormat.java:840)",
    "	at org.app.Helper.log(ReflectionHelper.java:47)",
    "	at org.app.Helper.getClassByName(ReflectionHelper.java:102)",
    "	at org.app.HelperTest.test_getClassByName(HelperTest.java:30)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "	at java.lang.reflect.Method.invoke(Method.java:498)",
    "	at org.junit.platform.commons.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:767)",
    "	at org.junit.jupiter.engine.execution.MethodInvocation.proceed(MethodInvocation.java:60)",
    "	at org.junit.jupiter.engine.execution.InvocationInterceptorChain$ValidatingInvocation.proceed(InvocationInterceptorChain.java:131)",
    "	at org.junit.jupiter.engine.execution.InterceptingExecutableInvoker.invoke(InterceptingExecutableInvoker.java:86)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.lambda$invokeTestMethod$8(TestMethodTestDescriptor.java:217)",
    "	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.invokeTestMethod(TestMethodTestDescriptor.java:213)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.execute(TestMethodTestDescriptor.java:138)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.execute(TestMethodTestDescriptor.java:68)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$6(NodeTestTask.java:156)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:100)",
    "	at java.util.ArrayList.forEach(ArrayList.java:1259)",
    "	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:41)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$6(NodeTestTask.java:160)",
    "	at org.junit.platform.launcher.core.DefaultLauncherSession$DelegatingLauncher.execute(DefaultLauncherSession.java:86)",
    "	at org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.stop(SuiteTestClassProcessor.java:63)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "	at java.lang.reflect.Method.invoke(Method.java:498)",
    "	at org.gradle.internal.dispatch.ReflectionDispatch.dispatch(ReflectionDispatch.java:36)",
    "	at org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:92)",
    "	at com.sun.proxy.$Proxy4.stop(Unknown Source)",
    "	at org.gradle.api.internal.tasks.testing.worker.TestWorker$3.run(TestWorker.java:198)",
    "	at org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:69)",
    "	at worker.org.gradle.process.internal.worker.GradleWorkerMain.run(GradleWorkerMain.java:69)",
    "	at worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)",
    "Caused by: java.lang.NumberFormatException: For input string: &quot;&quot;",
    "	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)",
    "	at java.lang.Integer.parseInt(Integer.java:592)",
    "	at java.lang.Integer.parseInt(Integer.java:615)",
    "	at java.text.MessageFormat.makeFormat(MessageFormat.java:1427)",
    "	... 90 more"
  }

  local sample_testcase_3_0_trace = {
    "org.opentest4j.AssertionFailedError: expected: &lt;ABC&gt; but was: &lt;CBA&gt;",
    "	at org.junit.jupiter.api.AssertionFailureBuilder.build(AssertionFailureBuilder.java:151)",
    "	at org.junit.jupiter.api.AssertionFailureBuilder.buildAndThrow(AssertionFailureBuilder.java:132)",
    "	at org.junit.jupiter.api.AssertEquals.failNotEqual(AssertEquals.java:197)",
    "	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:182)",
    "	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:177)",
    "	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:1145)",
    "	at org.swarg.common.MessageFormatTest.test_MessageFormat(MessageFormatTest.java:46)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "	at java.lang.reflect.Method.invoke(Method.java:498)",
    "	at org.junit.platform.commons.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:767)",
    "	at org.junit.jupiter.engine.execution.MethodInvocation.proceed(MethodInvocation.java:60)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.lambda$invokeTestMethod$8(TestMethodTestDescriptor.java:217)",
    "	at org.junit.platform.engine.support.hierarchical.ThrowableCollector.execute(ThrowableCollector.java:73)",
    "	at org.junit.jupiter.engine.descriptor.TestMethodTestDescriptor.execute(TestMethodTestDescriptor.java:68)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.lambda$executeRecursively$6(NodeTestTask.java:156)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:100)",
    "	at java.util.ArrayList.forEach(ArrayList.java:1259)",
    "	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:41)",
    "	at org.junit.platform.engine.support.hierarchical.NodeTestTask.execute(NodeTestTask.java:100)",
    "	at java.util.ArrayList.forEach(ArrayList.java:1259)",
    "	at org.junit.platform.engine.support.hierarchical.SameThreadHierarchicalTestExecutorService.invokeAll(SameThreadHierarchicalTestExecutorService.java:41)",
    "	at org.junit.platform.engine.support.hierarchical.HierarchicalTestEngine.execute(HierarchicalTestEngine.java:54)",
    "	at org.junit.platform.launcher.core.DefaultLauncherSession$DelegatingLauncher.execute(DefaultLauncherSession.java:86)",
    "	at org.gradle.api.internal.tasks.testing.junitplatform.JUnitPlatformTestClassProcessor$CollectAllTestClassesExecutor.processAllTestClasses(JUnitPlatformTestClassProcessor.java:124)",
    "	at org.gradle.api.internal.tasks.testing.SuiteTestClassProcessor.stop(SuiteTestClassProcessor.java:63)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "	at java.lang.reflect.Method.invoke(Method.java:498)",
    "	at org.gradle.internal.dispatch.ProxyDispatchAdapter$DispatchingInvocationHandler.invoke(ProxyDispatchAdapter.java:92)",
    "	at com.sun.proxy.$Proxy4.stop(Unknown Source)",
    "	at org.gradle.api.internal.tasks.testing.worker.TestWorker$3.run(TestWorker.java:198)",
    "	at org.gradle.process.internal.worker.child.SystemApplicationClassLoaderWorker.call(SystemApplicationClassLoaderWorker.java:69)",
    "	at worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)"
  }

  -- similar trace in the multiline message
  local sample_testcase_4_0_trace = {
    "org.opentest4j.AssertionFailedError: expected: &lt;&gt; but was: &lt;[ERROR] prefix Unable to set newValue for fieldName fieldA",
    "java.lang.NullPointerException",
    "	at sun.reflect.UnsafeFieldAccessorImpl.ensureObj(UnsafeFieldAccessorImpl.java:57)",
    "	at sun.reflect.UnsafeObjectFieldAccessorImpl.set(UnsafeObjectFieldAccessorImpl.java:75)",
    "	at java.lang.reflect.Field.set(Field.java:764)",
    "	at org.app.Helper.setFieldValueByFieldName(ReflectionHelper.java:88)",
    "	at org.app.HelperTest.givenNull_whenSetFieldValueByFieldName_thenFalseIsReturned(HelperTest.java:66)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at org.gradle.api.internal.tasks.testing.worker.TestWorker$3.run(TestWorker.java:198)",
    "	at worker.org.gradle.process.internal.worker.GradleWorkerMain.main(GradleWorkerMain.java:74)",
    "&gt;",
    "	at org.junit.jupiter.api.AssertionFailureBuilder.build(AssertionFailureBuilder.java:151)",
    "	at org.junit.jupiter.api.AssertionFailureBuilder.buildAndThrow(AssertionFailureBuilder.java:132)",
    "	at org.junit.jupiter.api.AssertEquals.failNotEqual(AssertEquals.java:197)",
    "	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:182)",
    "	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:177)",
    "	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:1145)",
    "	at org.app.HelperTest.givenA_whenB_thenC(HelperTest.java:70)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
    "	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
    "	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
    "	at java.lang.reflect.Method.invoke(Method.java:498)",
    "	at org.junit.platform.commons.util.ReflectionUtils.invokeMethod(ReflectionUtils.java:767)",
    "	at org.junit.jupiter.engine.execution.MethodInvocation.proceed(MethodInvocation.java:60)"
  }
  local sample_testsuite_0 = {
    tag = "testsuite",
    attrs = {
      errors = "0",
      failures = "2",
      hostname = "debian",
      name = "org.common.SomeTest",
      skipped = "0",
      tests = "2",
      time = "0.011",
      timestamp = "2025-01-14T11:05:59"
    },
    childs = {
      { tag = "properties" },
      {
        tag = "testcase",
        attrs = {
          classname = "org.common.SomeTest",
          name = "test_2",
          time = "0.007"
        },
        childs = {
          {
            tag = "failure",
            attrs = {
              message = "java.lang.AssertionError: expected:&lt;1&gt; but was:&lt;0&gt;",
              type = "java.lang.AssertionError"
            },
            text = sample_testcase_0_1_trace,
          }
        },
      },
      {
        tag = "testcase",
        attrs = {
          classname = "org.common.SomeTest",
          name = "test_MessageFormat",
          time = "0.003"
        },
        childs = {
          {
            tag = "failure",
            attrs = {
              message =
              "org.junit.ComparisonFailure: expected:&lt;[]&gt; but was:&lt;[Bind Class for {0} - Recognized java.lang.Object]&gt;",
              type = "org.junit.ComparisonFailure"
            },
            text = sample_testcase_1_0_trace
          }
        }
      },
      { tag = "system-out", text = { "testMessageFormat", "" } },
      { tag = "system-err", text = "" }
    }
  }

  it("parse_xml_test_report", function()
    local res = M.parse_xml_test_report(sample_report_0_path)
    local exp = sample_testsuite_0
    assert.same(exp, res)
  end)

  it("get_lt_gt_balance", function()
    local f = M.get_lt_gt_balance
    assert.same(0, f("", 0))
    assert.same(0, f("&lt;[]&gt;", 0))
    assert.same(1, f("&lt;&gt;&lt;", 0))
    assert.same(2, f("&lt;&gt;&lt;&lt;", 0)) -- <> <<
    assert.same(0, f("&lt;&gt;&lt;&gt;", 0)) -- <> <>
  end)

  --
  it("get_meaningful_stacktrace", function()
    local f = M.get_meaningful_stacktrace
    local failure_node = {
      tag = 'failure',
      text = sample_testcase_0_1_trace
    }
    local testcase_node = {
      tag = 'testcase',
      attrs = {
        classname = "org.common.SomeTest",
        name = "test_2",
      }
    }
    local exp = {
      {
        '...',
        '	at org.junit.Assert.assertEquals(Assert.java:633)',
        '	at org.common.SomeTest.test_2(SomeTest.java:41)',
        '	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)',
        '...'
      },
      41,
      'SomeTest.java'
    }
    assert.same(exp, { f(failure_node, testcase_node) })
  end)

  it("get_meaningful_stacktrace 2", function()
    local f = M.get_meaningful_stacktrace
    local failure_node = {
      tag = 'failure',
      text = sample_testcase_2_0_trace
    }
    local testcase_node = {
      tag = 'testcase',
      attrs = {
        classname = "org.app.HelperTest",
        name = "test_getClassByName",
      }
    }
    local exp = {
      {
        '...',
        '	at java.text.MessageFormat.makeFormat(MessageFormat.java:1429)',
        '	at java.text.MessageFormat.applyPattern(MessageFormat.java:479)',
        '	at java.text.MessageFormat.&lt;init&gt;(MessageFormat.java:362)',
        '	at java.text.MessageFormat.format(MessageFormat.java:840)',
        '	at org.app.Helper.log(ReflectionHelper.java:47)',
        '	at org.app.Helper.getClassByName(ReflectionHelper.java:102)',
        '	at org.app.HelperTest.test_getClassByName(HelperTest.java:30)',
        '	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)',
        '...',
        'Caused by: java.lang.NumberFormatException: For input string: &quot;&quot;',
        '	at java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)',
        '	at java.lang.Integer.parseInt(Integer.java:592)',
        '	at java.lang.Integer.parseInt(Integer.java:615)',
        '	at java.text.MessageFormat.makeFormat(MessageFormat.java:1427)',
        '	... 90 more'
      },
      30,
      'HelperTest.java'
    }
    assert.same(exp, { f(failure_node, testcase_node) })
  end)

  it("get_meaningful_stacktrace 3", function()
    local f = M.get_meaningful_stacktrace
    local failure_node = {
      tag = 'failure',
      text = sample_testcase_3_0_trace
    }
    local testcase_node = {
      tag = 'testcase',
      attrs = {
        classname = "org.swarg.common.MessageFormatTest",
        name = "test_MessageFormat",
      }
    }
    local exp = {
      {
        '...',
        '	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:1145)',
        '	at org.swarg.common.MessageFormatTest.test_MessageFormat(MessageFormatTest.java:46)',
        '	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)',
        '...'
      },
      46,
      'MessageFormatTest.java'
    }
    assert.same(exp, { f(failure_node, testcase_node) })
  end)

  it("get_meaningful_stacktrace 4", function()
    local f = M.get_meaningful_stacktrace
    local failure_node = {
      tag = 'failure',
      text = sample_testcase_4_0_trace
    }
    local testcase_node = {
      tag = 'testcase',
      attrs = {
        classname = "org.app.HelperTest",
        name = "givenA_whenB_thenC",
      }
    }
    local exp = {
      {
        '...',
        '	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:1145)',
        '	at org.app.HelperTest.givenA_whenB_thenC(HelperTest.java:70)',
        '	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)',
        '...'
      },
      70,
      'HelperTest.java'
    }
    assert.same(exp, { f(failure_node, testcase_node) })
  end)

  local sample_2_failure_text = {
    "java.lang.AssertionError: No value at JSON path &quot;$.id&quot;",
    "	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:351)",
    "	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:106)",
    "	at org.springframework.test.web.servlet.result.JsonPathResultMatchers.lambda$value$0(JsonPathResultMatchers.java:88)",
    "	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:214)",
    "	at pkg.app.rest.UserControllerV1Test.givenDevDto_whenCreateDev_thenSuccessResponse(UserControllerV1Test.java:76)",
    "	at java.base/java.lang.reflect.Method.invoke(Method.java:569)",
    "	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)",
    "	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)",
    "Caused by: com.jayway.jsonpath.PathNotFoundException: No results for path: $['id']"
  }

  it("get_meaningful_stacktrace", function()
    local f = M.get_meaningful_stacktrace
    local failure_node = {
      tag = 'failure',
      text = sample_2_failure_text
    }
    local testcase_node = {
      tag = 'testcase',
      attrs = {
        classname = "pkg.app.rest.UserControllerV1Test",
        name = "Test create developer functionality", -- DisplayName
      }
    }
    local exp = {
      {
        '...',
        '	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:351)',
        '	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:106)',
        '	at org.springframework.test.web.servlet.result.JsonPathResultMatchers.lambda$value$0(JsonPathResultMatchers.java:88)',
        '	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:214)',
        '	at pkg.app.rest.UserControllerV1Test.givenDevDto_whenCreateDev_thenSuccessResponse(UserControllerV1Test.java:76)',
        '	at java.base/java.lang.reflect.Method.invoke(Method.java:569)',
        '...',
        "Caused by: com.jayway.jsonpath.PathNotFoundException: No results for path: $['id']"
      },
      76,
      'UserControllerV1Test.java'
    }
    assert.same(exp, { f(failure_node, testcase_node) })
  end)
end)

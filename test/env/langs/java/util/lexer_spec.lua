-- 14-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.lexer'
local LT = M.LT

describe("env.langs.java.util.lexer", function()
  it("indexOfByPos", function()
    local f = M.indexOfByPos
    local t = {
      { 'List<Object>', 1,  12 },
      { 'obj',          14, 16 },
      { '=',            18, 18 },
      { 'new',          20, 22 },
      { 'ArrayList',    24, 32 }, -- 5
      { '<>',           33, 34 },
      { '(',            35, 35 },
      { ')',            36, 36 },
      { ';',            37, 37 }
    }
    assert.same(-1, f(t, -1))
    assert.same(5, f(t, 24))
    assert.same(9, f(t, 37))
    assert.same(-1, f(t, 38))
  end)

  it("indexOfByPos", function()
    local f = M.indexOfByPos
    local t = {
      { "RegistrationService", 27, 45 },
      { "registrationService", 47, 65 },
      { ",",                   66, 66 }
    }
    assert.same({ 1, 'RegistrationService' }, { f(t, 39) })
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local t = {
      { 'Object', 1,  6 },
      { 'o',      8,  8 },
      { '=',      10, 10 },
      { 'new',    12, 14 },
      { 'Object', 16, 21 },
      { '()',     22, 23 },
      { ';',      24, 24 }
    }
    assert.same(LT.CLASS, f(t, 16))
    assert.same(LT.VARNAME, f(t, 8))
    assert.same(LT.KEYWORD, f(t, 12))
    assert.same(LT.CLASS, f(t, 16))
    assert.same(LT.OPERATOR, f(t, 22))
    assert.same(LT.OPERATOR, f(t, 10))
    assert.same(LT.OPSEP, f(t, 24))
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local t = { { 'Container<String>', 5, 20 } }
    assert.same(LT.TYPE, f(t, 5))

    local t2 = { { 'Parent.Container<String>', 5, 28 } }
    assert.same(LT.TYPE, f(t2, 5))
  end)

  it("is_type", function()
    local f = M.t.is_type
    assert.is_true(f("T"))
    assert.is_true(f("<T>"))
    assert.is_true(f("<K, V>"))
    assert.is_true(f("Entry"))
    assert.is_true(f("Entry<>"))
    assert.is_true(f("Entry<T>"))
    assert.is_true(f("Entry<T>>")) -- bad
    assert.is_true(f("Map<Entry<K,T>>"))
    assert.is_true(f("Map<? extends String>"))
    assert.is_true(f("Map<? super String>"))
  end)
end)

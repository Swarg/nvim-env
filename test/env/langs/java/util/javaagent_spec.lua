-- 08-02-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local jvm = require 'env.langs.java.util.jvm'
local M = require 'env.langs.java.util.javaagent'

describe("env.langs.java.util.javaagent", function()
  it("get_agent_for_jvm", function()
    -- given
    local jvm_version = 21
    jvm.get_version_mapping()['java'] = jvm_version
    -- when
    local result = M.get_agent_for_jvm("java")
    -- then
    local agentpath = M.AGENTS[jvm_version].agent
    assert.same(agentpath, result)
  end)

  it("run_agent cmd version", function()
    -- given
    M.change_home() -- /home/user/ -> ~/
    jvm.get_version_mapping()['java'] = 21
    local agentJarLoader, err, jvm_version = M.get_agent_for_jvm("java")
    assert.is_nil(err)
    assert.same(21, jvm_version)
    assert.is_not_nil(agentJarLoader) ---@cast agentJarLoader string
    local opts = { dry_run = true }
    local agentcmd = 'version'
    -- when
    local res = M.run_agent(agentJarLoader, 'self', agentcmd, 1234, opts)
    -- then
    local exp = {
      "java -jar ~/tmp-agent/ragent.jar attach-javaagent 1234 self -c 'version'"
    }
    assert.same(exp, res)
  end)
end)

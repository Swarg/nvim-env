-- 26-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)

-- local fs = require "env.files"
local M = require 'env.langs.java.util.gradle.groovy_dsl_syntax'

describe("env.langs.java.util.gradle.dsl_syntax", function()
  -- before_each(function() require 'alogger'.fast_off() end)

  it("handle_node_methodcall method call with one param", function()
    local node = { 'MC', 'id', "'java'" }
    local parent = {}
    M.handle_node_methodcall(parent, node)

    local exp2 = { id = { 'java' } }
    assert.same(exp2, parent)
  end)

  it("handle node of closure with statements", function()
    local node = {
      'C', -- closure
      {
        { 'MC', 'id', "'java'" },
        { 'MC', 'id', "'application'" }
      }
    }
    local parent = { plugin = {} }
    M.handle_node_closure(parent.plugin, node)

    local exp2 = {
      plugin = { id = { 'java', 'application' } }
    }
    assert.same(exp2, parent)
  end)

  -- plugins {
  --     id 'java'
  --     id 'application'
  -- }
  it("handle_node", function()
    local node = {
      'MC',      -- methodcall
      'plugins', -- methodname
      {          -- method-params
        'C',     -- closure
        {
          { 'MC', 'id', "'java'" },
          { 'MC', 'id', "'application'" }
        }
      }
    }
    local parent = {}
    M.handle_node(parent, node)
    local exp = { plugins = { id = { 'java', 'application' } } }
    assert.same(exp, parent)
  end)

  it("handle_node assignment", function()
    local node = { '=', 'group', "'org.app'" }
    local parent = {}
    M.handle_node(parent, node)
    local exp = { group = 'org.app' }
    assert.same(exp, parent)
  end)

  -- plugins {
  --     id 'java'
  --     id 'application'
  -- }
  -- group = 'org.app'
  -- version = '0.1.0'
  it("ast_to_dsl", function()
    local ast = {
      {
        'MC',
        'plugins',
        {
          'C',
          {
            { 'MC', 'id', "'java'" },
            { 'MC', 'id', "'application'" }
          }
        }
      },
      { '=', 'group',   "'org.app'" },
      { '=', 'version', "'0.1.0'" },
    }
    local exp = {
      plugins = {
        id = { 'java', 'application' }
      },
      group = 'org.app',
      version = '0.1.0'
    }
    assert.same(exp, M.ast_to_dsl(ast))
  end)

  -- java {
  --     toolchain {
  --         languageVersion = JavaLanguageVersion.of(21)
  --     }
  -- }
  it("handle_node nested closure with assignment of methodcall", function()
    local node = {
      'MC',
      'java',
      {
        'C',
        {
          {
            'MC',
            'toolchain',
            {
              'C',
              {
                {
                  '=',
                  'languageVersion',
                  { 'MC', 'JavaLanguageVersion.of', '21' }
                }
              }
            }
          }
        }
      }
    }
    local exp = {
      java = {
        toolchain = {
          languageVersion = { ['JavaLanguageVersion.of'] = { '21' } }
        }
      }
    }
    assert.same(exp, M.handle_node({}, node))
  end)

  it("handle_node dependencies", function()
    local node = {
      'MC',
      'dependencies',
      {
        'C',
        {
          { 'MC', 'implementation', "'org.app:util:0.2.0'" },
          {
            'MC',
            'testImplementation',
            "'org.assertj:assertj-core:3.27.3'"
          },
          {
            'MC',
            'testImplementation',
            "'org.junit.jupiter:junit-jupiter-engine:5.11.4'"
          },
          {
            'MC',
            'testImplementation',
            "'org.mockito:mockito-junit-jupiter:5.15.2'"
          },
          {
            'MC',
            'testImplementation',
            "'com.h2database:h2:2.3.232'"
          },
          {
            'MC',
            'runtimeOnly',
            "'org.postgresql:postgresql:42.7.5'"
          }
        }
      }
    }
    local exp = {
      dependencies = {
        implementation = { 'org.app:util:0.2.0' },
        runtimeOnly = { 'org.postgresql:postgresql:42.7.5' },
        testImplementation = {
          'org.assertj:assertj-core:3.27.3',
          'org.junit.jupiter:junit-jupiter-engine:5.11.4',
          'org.mockito:mockito-junit-jupiter:5.15.2',
          'com.h2database:h2:2.3.232'
        }
      }
    }
    assert.same(exp, M.handle_node({}, node))
  end)

  it("handle_node attrs", function()
    local node = {
      'MC',
      'attributes',
      {
        'KVL',
        { 'KV', "'Main-Class'", 'application.mainClass' }
      }
    }
    local exp = {
      attributes = {
        ['Main-Class'] = 'application.mainClass'
      }
    }
    assert.same(exp, M.handle_node({}, node))
  end)

  -- from {
  --     configurations.runtimeClasspath.findAll {
  --         it.name.endsWith('jar')
  --     }.collect {
  --         zipTree(it)
  --     }
  -- }
  it("handle_node attrs", function()
    local node = {
      'MC',
      'configurations.runtimeClasspath.findAll',
      { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } },
      {
        'CMC',
        'collect',
        { 'C', { { 'MC', 'zipTree', 'it' } } }
      }
    }
    local exp = {
      ['configurations.runtimeClasspath.findAll'] = {
        ['it.name.endsWith'] = { 'jar' },
        collect = { zipTree = { 'it' } },
      }
    }

    assert.same(exp, M.handle_node({}, node))
  end)


  -- parsed via parse_dsl 'test/env/resources/gradle/misc/build_gradle_1'
  local build_gradle_1_ast = {
    {
      'MC',
      'plugins',
      {
        'C',
        {
          { 'MC', 'id', "'java'" },
          { 'MC', 'id', "'application'" }
        }
      }
    },
    { '=', 'group',   "'org.app'" },
    { '=', 'version', "'0.1.0'" },
    {
      'MC',
      'java',
      {
        'C',
        {
          {
            'MC',
            'toolchain',
            {
              'C',
              {
                {
                  '=',
                  'languageVersion',
                  { 'MC', 'JavaLanguageVersion.of', '21' }
                }
              }
            }
          }
        }
      }
    },
    {
      'MC',
      'application',
      { 'C', { { '=', 'mainClass', "'org.app.Application'" } } }
    },
    {
      'MC',
      'repositories',
      {
        'C',
        { { 'MC', 'mavenLocal' }, { 'MC', 'mavenCentral' } }
      }
    },
    {
      'MC',
      'dependencies',
      {
        'C',
        {
          { 'MC', 'implementation', "'org.app:util:0.2.0'" },
          {
            'MC',
            'testImplementation',
            "'org.assertj:assertj-core:3.27.3'"
          },
          {
            'MC',
            'testImplementation',
            "'org.junit.jupiter:junit-jupiter-engine:5.11.4'"
          },
          {
            'MC',
            'testImplementation',
            "'org.mockito:mockito-junit-jupiter:5.15.2'"
          },
          {
            'MC',
            'testImplementation',
            "'com.h2database:h2:2.3.232'"
          },
          {
            'MC',
            'runtimeOnly',
            "'org.postgresql:postgresql:42.7.5'"
          }
        }
      }
    },
    {
      'MC',
      'task',
      {
        'MC',
        'fatJar',
        { 'KVL', { 'KV', 'type', 'Jar' } },
        {
          'C',
          {
            {
              'MC',
              'manifest',
              {
                'C',
                {
                  {
                    'MC',
                    'attributes',
                    {
                      'KVL',
                      { 'KV', "'Main-Class'", 'application.mainClass' }
                    }
                  }
                }
              }
            },
            { '=',  'archiveClassifier', '"all"' },
            { 'MC', 'from',              'sourceSets.main.output' },
            { 'MC', 'dependsOn',         'configurations.runtimeClasspath' },
            {
              'MC',
              'from',
              {
                'C',
                {
                  {
                    'MC',
                    'configurations.runtimeClasspath.findAll',
                    { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } },
                    {
                      'CMC',
                      'collect',
                      { 'C', { { 'MC', 'zipTree', 'it' } } }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    {
      'MC',
      'tasks.named',
      "'test'",
      {
        'C',
        {
          {
            'IF',
            '(project.hasProperty("noHtmlReports"))',
            "{\n        reports.html.required = false\n    }"
          },
          { 'MC', 'useJUnitPlatform' }
        }
      }
    }
  }

  it("parse_dsl", function()
    local res = M.ast_to_dsl(build_gradle_1_ast)
    local exp = {
      plugins = { id = { 'java', 'application' } },
      group = 'org.app',
      version = '0.1.0',
      java = {
        toolchain = {
          languageVersion = { ['JavaLanguageVersion.of'] = { '21' } }
        }
      },
      application = {
        mainClass = 'org.app.Application'
      },
      repositories = {
        mavenCentral = {},
        mavenLocal = {}
      },
      dependencies = {
        implementation = { 'org.app:util:0.2.0' },
        testImplementation = {
          'org.assertj:assertj-core:3.27.3',
          'org.junit.jupiter:junit-jupiter-engine:5.11.4',
          'org.mockito:mockito-junit-jupiter:5.15.2',
          'com.h2database:h2:2.3.232'
        },
        runtimeOnly = { 'org.postgresql:postgresql:42.7.5' },
      },
      task = {
        fatJar = {
          ['type'] = 'Jar',

          manifest = {
            attributes = {
              ['Main-Class'] = 'application.mainClass'
            }
          },

          archiveClassifier = 'all',
          from = {
            'sourceSets.main.output',
            ['configurations.runtimeClasspath.findAll'] = {
              ['it.name.endsWith'] = { 'jar' },
              collect = { zipTree = { 'it' } }
            }
          },
          dependsOn = { 'configurations.runtimeClasspath' },
        }
      },
      ['tasks.named'] = { 'test', useJUnitPlatform = {} },
    }
    assert.same(exp, res)
  end)

  it("parse_dsl", function()
    local ast = {
      { '=', 'version', 'project.version' },
      { '=', 'group',   'project.group' }
    }
    local exp = {
      version = 'project.version',
      group = 'project.group'
    }
    assert.same(exp, M.ast_to_dsl(ast))
  end)


  it("handle_node_closure", function()
    local node = { 'C', { { 'NI', 'extraLibs' } } }
    local parent = {}
    local exp = { extraLibs = {} }
    assert.same(exp, M.handle_node_closure(parent, node))
  end)


  it("parse_dsl new id in closure (configurations-extraLibs", function()
    local ast = {
      { 'MC', 'plugins', { 'C', { { 'MC', 'id', "'java'" } } } },
      { 'MC', 'configurations', { 'C', { { 'NI', 'extraLibs' } } } },
      {
        'MC',
        'dependencies',
        {
          'C',
          {
            { 'MC', 'extraLibs',      "'org.some:lib:0.3.0'" },
            { 'MC', 'implementation', "'org.some:lib:0.3.0'" },
            {
              'MC',
              'testImplementation',
              "'org.junit.jupiter:junit-jupiter:5.11.4'"
            }
          }
        }
      }
    }
    local exp = {
      plugins = { id = { 'java' } },
      configurations = { extraLibs = {} },
      dependencies = {
        extraLibs = { 'org.some:lib:0.3.0' },
        implementation = { 'org.some:lib:0.3.0' },
        testImplementation = { 'org.junit.jupiter:junit-jupiter:5.11.4' }
      }
    }
    assert.same(exp, M.ast_to_dsl(ast))
  end)


  it("parse_dsl ternary op", function()
    local ast = {
      {
        'MC',
        'from',
        {
          'C',
          {
            {
              'MC',
              'configurations.extraLibs.collect',
              {
                'C',
                {
                  {
                    'TO', { 'MC', 'it.isDirectory' }, 'it', { 'MC', 'zipTree', 'it' }
                  }
                }
              }
            }
          }
        }
      }
    }
    local exp = {
      from = {
        ['configurations.extraLibs.collect'] = {
          {
            condition = { 'MC', 'it.isDirectory' },
            btrue = 'it',
            bfalse = { 'MC', 'zipTree', 'it' },
          }
        }
      }
    }
    assert.same(exp, M.ast_to_dsl(ast))
  end)

  -- local path = 'test/env/resources/gradle/misc/build_gradle_1'
  -- local body = fs.read_all_bytes_from(path)
  -- local res = M.parse_dsl(body)
end)

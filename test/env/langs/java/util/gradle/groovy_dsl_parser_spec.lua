-- 26-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)
_G.TEST = true
local M = require 'env.langs.java.util.gradle.groovy_dsl_parser'

describe("env.langs.java.util.gradle.groovy_dsl_parser lpeg behavior", function()
  local lpeg = require 'lpeg'
  local P = lpeg.P

  it("NL", function()
    local NL = P '\r' ^ -1 * P '\n' -- line end \r\n
    local f = function(s) return NL:match(s) end
    assert.same(nil, f(""))
    assert.same(2, f("\n"))
    assert.same(2, f("\n\n"))
    assert.same(3, f("\r\n"))
    assert.same(nil, f("\r\r\n"))
    assert.same(2, f("\n\r"))
  end)

  it("pCommentMLInline", function()
    local f = function(s) return M.p.CommentMLInline:match(s) end
    assert.same(5, f("/**/"))
    assert.same(14, f("/* comment */"))
    assert.is_nil(f("/* multiline \n comment */"))
    assert.is_nil(f("/* "))
  end)
end)

--
--
--
describe("env.langs.java.util.gradle.groovy_dsl_parser", function()
  it("parse_to_ast Assignment one kv-pair", function()
    local f = M.parse_to_ast
    -- 1 means assignment
    assert.same({ { '=', 'key', '123' } }, f("key=123"))
    assert.same({ { '=', 'key', '-12' } }, f("key=-12"))
    local exp = { { '=', 'a', { '=', 'b', '123' } } }
    assert.same(exp, f("a=b=123"))

    assert.same({ { '=', 'a', { '=', 'b', '123' } } }, f("a=b=123\n"))
    assert.same({ { '=', 'a', '123' } }, f("a=123//"))
    assert.same({ { '=', 'a', '123' } }, f("a=123 //"))
    assert.same({ { '=', 'a', '123' } }, f("a=123 //\n"))
    assert.same({ { '=', 'a', "'abc'" } }, f("a='abc' //\n"))
    assert.same({ { '=', 'a', '123' } }, f("a=123 // comment\n"))
    assert.same({ { '=', 'a', '123' } }, f("a=123 /* comment */\n"))
  end)

  it("parse_to_ast Assignment one kv-pair", function()
    local f = M.parse_to_ast
    local exp = { { '=', 'key', '123' }, { '=', 'b', '99' } }
    assert.same(exp, f("key=123\nb=99"))
  end)

  it("parse_to_ast Assignment: limitation of current implementation", function()
    local f = M.parse_to_ast
    assert.same({ { '=', 'a', '123' } }, f("a=123 /* com \n ment */\n"))
  end)


  it("parse_to_ast MathodCall simple params", function()
    local f = M.parse_to_ast
    local exp = { { 'MC', 'methodName', 'param1' } }
    assert.same(exp, f("methodName param1"))
    assert.same(exp, f("methodName(param1)"))
    -- assert.same(exp2, f("methodName param1: key"))

    local exp2 = { { 'MC', 'methodName', "'value'" } }
    assert.same(exp2, f("methodName('value')"))
    local exp3 = { { 'MC', 'methodName', "'param1'", "'pa2'", "'pa3'" } }
    assert.same(exp3, f("methodName('param1', 'pa2', 'pa3')"))
    assert.same(exp3, f("methodName('param1','pa2','pa3')"))
    assert.same(exp3, f("methodName('param1' ,'pa2' ,'pa3')"))

    local exp4 = { { 'MC', 'from', 'sourceSets.main.output' } }
    assert.same(exp4, f('from sourceSets.main.output'))
  end)

  it("parse_to_ast MathodCall with map", function()
    local f = M.parse_to_ast
    local exp = {
      { 'MC', 'methodName', { 'KVL', { 'KV', 'key', "'pa2'" } } }
    }
    assert.same(exp, f("methodName(key: 'pa2')"))

    local exp2 = {
      {
        'MC',
        'methodName',
        { 'KVL', { 'KV', 'key', "'pa2'" }, { 'KV', 'b', '88' } }
      }
    }
    assert.same(exp2, f("methodName(key: 'pa2', b: 88)"))

    local exp3 = {
      {
        'MC',
        'methodName',
        { 'KVL', { 'KV', 'key', "'pa2'" }, { 'KV', 'b', '88' } }
      }
    }
    assert.same(exp3, f("methodName([key: 'pa2', b: 88])"))

    local exp4 = {
      {
        'MC',
        'methodName',
        { 'KVL', { 'KV', 'key', "'pa2'" }, { 'KV', 'b', '88' } }
      }
    }
    assert.same(exp4, f("methodName [key: 'pa2', b: 88] "))

    local exp5 = {
      { 'MC', 'methodName', { 'KVL', { 'KV', 'key', "'pa2'" } } }
    }
    assert.same(exp5, f("methodName key: 'pa2'"))

    local exp6 = { 1 }
    assert.same(exp6, f("methodName key: 'pa2' xx"))
    --                                         ^^
  end)

  it("parse_to_ast MathodCall with closure as single param", function()
    local f = M.parse_to_ast
    local exp = { { 'MC', 'methodName', { 'C', {} } } }
    assert.same(exp, f("methodName {}"))

    local exp2 = {
      { 'MC', 'methodName', { 'C', { { '=', 'key', '42' } } } }
    }
    assert.same(exp2, f("methodName {key=42}")) -- only with new line sep

    local exp3 = {
      { 'MC', 'methodName', { 'C', { { '=', 'key', '42' } } } }
    }
    assert.same(exp3, f("methodName {key=42\n}"))
    assert.same(exp3, f("methodName {key=42;}"))

    local exp4 = { { 'MC', 'methodName', { 'C', { { 'MC', 'method' } } } } }
    assert.same(exp4, f("methodName {\n  method()\n}"))

    local exp5 = {
      {
        'MC',
        'methodName',
        { 'C',
          {
            { 'MC', 'method', '4' }
          }
        }
      }
    }
    assert.same(exp5, f("methodName {\n  method(4)\n}"))

    local exp6 = {
      {
        'MC',
        'methodName',
        { 'C', { { 'MC', 'method', '4', '5' } } }
        --                         ^p1  ^p2
      }
    }
    assert.same(exp6, f("methodName {\n  method(4, 5)\n}"))
  end)


  it("parse_to_ast MathodCall with closure as single param", function()
    local f = M.parse_to_ast
    local exp = { { 'MC', 'task', { 'MC', 'fatJar', 'param1' } } }
    assert.same(exp, f("task fatJar(param1)"))

    local exp2 = {
      {
        'MC',
        'task',
        {
          'MC',
          'fatJar',
          'param1', -- param1
          { 'C', { { '=', 'classifier', '"all"' } } }
        }
      }
    }
    assert.same(exp2, f("task fatJar(param1) {\n  classifier = \"all\"\n}"))

    local exp2_1 = {
      {
        'MC',
        'task',
        {
          'MC',
          'fatJar',
          'param1',
          { 'C', { { '=', 'classifier', '"all"' } } }
        }
      }
    }
    assert.same(exp2_1, f("task fatJar(param1) {\n  classifier = \"all\"}"))

    local exp3 = {
      {
        'MC',
        'task',
        {
          'MC',
          'fatJar',
          { 'KVL', { 'KV', 'type', 'Jar' } },
          { 'C',   { { '=', 'classifier', '"all"' } } }
        }
      }
    }
    assert.same(exp3, f("task fatJar(type: Jar) {\n  classifier = \"all\"\n}"))
  end)


  it("parse_to_ast MathodCall with closure as single param", function()
    local s = " reports.html.required = false"
    local res = M.parse_to_ast(s)
    local exp = { { '=', 'reports.html.required', 'false' } }
    assert.same(exp, res)
  end)

  local s = [[
    attributes (
      'Main-Class': application.mainClass
    )
  ]]
  local res = M.parse_to_ast(s)
  local exp = {
    { "MC",                                                        -- method call
      "attributes",                                                -- method name
      { "KVL", { "KV", "'Main-Class'", "application.mainClass" } } -- param1
    }
  }
  assert.same(exp, res)
end)


describe("env.langs.java.util.gradle.groovy_dsl_parser", function()
  before_each(function()
    M.debug_clear()
  end)
  it("parse_to_ast MathodCall with closure and if", function()
    local input = [[
      tasks.named('test') {
          if (project.hasProperty("noHtmlReports")) {
              reports.html.required = false
          }
          useJUnitPlatform()
      }
    ]]
    local exp = {
      {
        'MC',
        'tasks.named',
        "'test'",
        {
          'C',
          {
            {
              'IF',
              '(project.hasProperty("noHtmlReports"))',
              "{\n              reports.html.required = false\n          }"
            },
            { 'MC', 'useJUnitPlatform' }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(input))
  end)

  -- task - is a dsl method name
  -- fatJar(['type':Jar], {closure}) - 1th param of the task name
  it("parse_to_ast MathodCall with closure as single param", function()
    local input = [[
    task fatJar(type: Jar) {
        manifest {
            attributes (
                'Main-Class': application.mainClass
            )
        }
        archiveClassifier = "all"

        from sourceSets.main.output
        dependsOn configurations.runtimeClasspath
    }
    ]]
    local exp = {
      {
        'MC',
        'task',
        {
          'MC',
          'fatJar',
          { 'KVL', { 'KV', 'type', 'Jar' } },
          {
            'C',
            {
              {
                'MC',
                'manifest',
                {
                  'C',
                  {
                    {
                      'MC',
                      'attributes',
                      {
                        'KVL',
                        { 'KV', "'Main-Class'", 'application.mainClass' }
                      }
                    }
                  }
                }
              },
              { '=',  'archiveClassifier', '"all"' },
              { 'MC', 'from',              'sourceSets.main.output' },
              { 'MC', 'dependsOn',         'configurations.runtimeClasspath' }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(input))
  end)


  it("parse_to_ast nested struct with closure", function()
    local input = [[
      from {
          configurations.runtimeClasspath.findAll {
              it.name.endsWith('jar')
          }
      }
    ]]
    local exp = {
      {
        'MC',
        'from',
        {
          'C',
          {
            {
              'MC',
              'configurations.runtimeClasspath.findAll',
              { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(input))
  end)

  it("parse_to_ast chained MathodCall", function()
    local input4 = [[
      method1{}.method2{}
    ]]
    local exp = {
      {
        'MC',
        'method1',
        { 'C',   {} },
        { 'CMC', 'method2', { 'C', {} } }
      }
    }
    assert.same(exp, M.parse_to_ast(input4))
  end)

  it("parse_to_ast MathodCall with closure as single param", function()
    local exp = {
      {
        'MC',
        'method1',
        { 'C',   {} },
        { 'CMC', 'method2', { 'C', {} } }
      }
    }
    assert.same(exp, M.parse_to_ast(" method1{}.method2{} "))
    assert.same(exp, M.parse_to_ast(" method1{}.method2({}) "))
    assert.same(exp, M.parse_to_ast(" method1({}).method2{} "))
    assert.same(exp, M.parse_to_ast(" method1({}).method2({})"))
  end)

  -- given {} when {} then
  it("parse_to_ast MathodCall with closure as single param", function()
    local input4 = [[
      method1 {
        a = 1
      }.method2 {
        b = 2
      }
    ]]
    local exp = {
      {
        'MC',
        'method1',
        { 'C',   { { '=', 'a', '1' } } },
        { 'CMC', 'method2',            { 'C', { { '=', 'b', '2' } } } }
      }
    }
    assert.same(exp, M.parse_to_ast(input4))
  end)

  -- given({}).when({}).then({})
  it("parse_to_ast MathodCall with closure as single param", function()
    local input4 = [[
      given {
        a = 1
      } when {
        b = 2
      } then {
        c = 3
      }
    ]]
    local exp = {
      {
        'MC',
        'given',
        { 'C', { { '=', 'a', '1' } } },
        {
          'CMC',
          'when',
          { 'C',   { { '=', 'b', '2' } } },
          { 'CMC', 'then',               { 'C', { { '=', 'c', '3' } } } }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(input4))
  end)

  -- // equivalent to: please(show).the(square_root).of(100)
  it("parse_to_ast chained MathodCall", function()
    local line = 'please(show).the(square_root).of(100)'
    local exp = {
      {
        'MC',
        'please',
        'show',
        { 'CMC', 'the', 'square_root', { 'CMC', 'of', '100' } }
      }
    }
    assert.same(exp, M.parse_to_ast(line))
  end)


  --     configurations.runtimeClasspath.findAll {
  --         it.name.endsWith('jar')
  --     }.collect { zipTree(it) }
  -- }
  it("parse_to_ast chained MathodCall", function()
    local s = "findAll {\n call() \n}.clt {\n call2() }\n"
    local exp = {
      {
        'MC',
        'findAll',
        { 'C',   { { 'MC', 'call' } } },
        { 'CMC', 'clt',               { 'C', { { 'MC', 'call2' } } } }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)


  it("parse_to_ast chained MathodCall", function()
    local s = [[
      from {
          configurations.runtimeClasspath.findAll {
              it.name.endsWith('jar')
          }

          collect { zipTree(it) }
      }
    ]]
    local exp = {
      {
        'MC', -- from
        'from',
        {
          'C', -- closure
          {
            {  -- 1th line of code in closure
              'MC',
              'configurations.runtimeClasspath.findAll',
              { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } }
            },
            { 'MC', 'collect', { 'C', { { 'MC', 'zipTree', 'it' } } } }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)


  it("parse_to_ast chained MathodCall", function()
    local s = [[
      from {
          configurations.runtimeClasspath.findAll {
              it.name.endsWith('jar')
          }.collect { zipTree(it) }
      }
    ]]
    local exp = {
      {
        'MC',
        'from',
        {
          'C',
          {
            {
              'MC',
              'configurations.runtimeClasspath.findAll',
              { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } },
              {
                'CMC',
                'collect',
                { 'C', { { 'MC', 'zipTree', 'it' } } }
              }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast chained MathodCall", function()
    local s = [[
      from {
          configurations.runtimeClasspath.findAll {
              it.name.endsWith('jar') }.collect { zipTree(it)
          }
      }
    ]]
    local exp = {
      {
        'MC',
        'from',
        {
          'C',
          {
            {
              'MC',
              'configurations.runtimeClasspath.findAll',
              { 'C', { { 'MC', 'it.name.endsWith', "'jar'" } } },
              {
                'CMC',
                'collect',
                { 'C', { { 'MC', 'zipTree', 'it' } } }
              }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)


  it("parse_to_ast", function()
    local bin = [[
      version = project.version
      group = project.group
    ]]
    local exp = {
      { '=', 'version', 'project.version' },
      { '=', 'group',   'project.group' }
    }
    assert.same(exp, M.parse_to_ast(bin))
  end)

  it("parse_to_ast", function()
    local s = [[
    section {
        version = project.app_version + "-" + project.tool_version
        runDir = "run"
    }
    ]]
    local exp = {
      {
        'MC',
        'section',
        {
          'C',
          {
            {
              '=',
              'version',
              'project.app_version + "-" + project.tool_version'
            },
            { '=', 'runDir', '"run"' }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
task deobfJar(type: Jar) {
    from sourceSets.main.output
    archiveClassifier = 'dev' // appendix = 'deobf'
}
]]
    local exp = {
      {
        'MC',
        'task',
        {
          'MC',
          'deobfJar',
          { 'KVL', { 'KV', 'type', 'Jar' } },
          {
            'C',
            {
              { 'MC', 'from',              'sourceSets.main.output' },
              { '=',  'archiveClassifier', "'dev'" }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
processResources {
    duplicatesStrategy(DuplicatesStrategy.EXCLUDE)

    inputs.property "version", project.version
}
  ]]
    local exp = {
      {
        'MC',
        'processResources',
        {
          'C',
          {
            {
              'MC',
              'duplicatesStrategy',
              'DuplicatesStrategy.EXCLUDE'
            },
            { 'MC', 'inputs.property', '"version"', 'project.version' }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
processResources {

    from(sourceSets.main.resources.srcDirs) {
        include 'some.info'

        expand 'version':project.version, 'toolversion':project.tool.version
    }
}
]]
    local exp = {
      {
        'MC',
        'processResources',
        {
          'C',
          {
            {
              'MC',
              'from',
              'sourceSets.main.resources.srcDirs',
              {
                'C',
                {
                  { 'MC', 'include', "'some.info'" },
                  {
                    'MC',
                    'expand',
                    {
                      'KVL',
                      { 'KV', "'version'",     'project.version' },
                      { 'KV', "'toolversion'", 'project.tool.version' }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
// comment 1
// comment 2
// comment 3
tasks.withType(GenerateModuleMetadata) {
    enabled = false
}
]]
    local exp = {
      {
        'MC',
        'tasks.withType',
        'GenerateModuleMetadata',
        { 'C', { { '=', 'enabled', 'false' } } }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
    methodCall {
        Node pomNode = asNode()
    }
    next_value = 42
    ]]
    local exp = {
      {
        'MC',
        'methodCall',
        { 'C', { { '=', 'pomNode', { 'MC', 'asNode' } } } }
      },
      { '=', 'next_value', '42' }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("pGIdentifierWithWildCard", function()
    local f = function(s) return M.p.GIdentifierWithWildCard:match(s) end
    assert.same(25, f("pomNode.dependencies.'*'"))
    assert.same(30, f("pomNode.dependencies.'*'.more"))
    assert.same(33, f("pomNode.dependencies.'*'.findAll()"))
  end)

  it("bool expr", function()
    local f = function(s) return M.p.grammar:match(s) end

    local exp = { 'BE', 'true', '!=', 'false' }
    assert.same(exp, f("true != false"))

    local exp2 = { 'BE', { 'MC', 'it.artifactId.text' }, '!=', "''" }
    assert.same(exp2, f("it.artifactId.text() != ''"))
  end)

  it("assignment expression with access to the map by literal key", function()
    local s = [[DIR = System.properties['user.home'] + "/" + project.DIR]]
    local exp3 = {
      { '=', 'DIR', 'System.properties[\'user.home\'] + \"/\" + project.DIR' }
    }
    assert.same(exp3, M.parse_to_ast(s))
  end)

  it("assignment expression with access to the map by literal key", function()
    local s = [[def natives = new File(userhome, NATIVES)]]
    local exp3 = {
      {
        '=', 'natives', { 'NEW', 'File', 'userhome', 'NATIVES' } }
    }
    assert.same(exp3, M.parse_to_ast(s))
  end)

  it("parse_to_ast assignment with type, wildcard, bool expr", function()
    local s = [[
    pom.withXml {
        Node pomNode = asNode()
        pomNode.dependencies.'*'.findAll() {
            it.artifactId.text() != ''
        }.each() {
            it.parent().remove(it)
        }
    }
    next_value = 42
    ]]
    local exp = {
      {
        'MC',
        'pom.withXml',
        {
          'C',
          {
            { '=', 'pomNode', { 'MC', 'asNode' } },
            {
              'MC',
              "pomNode.dependencies.'*'.findAll",
              {
                'C',
                { { 'BE', { 'MC', 'it.artifactId.text' }, '!=', "''" } }
              },
              {
                'CMC',
                'each',
                {
                  'C',
                  { { 'MC', 'it.parent', { 'CMC', 'remove', 'it' } } }
                }
              }
            }
          }
        }
      },
      { '=', 'next_value', '42' }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast", function()
    local s = [[
test {
    jvmArgs (
        "-Xmn128M",   // comment 1
        "-Xmx1024M",
        "-Xms1024M",
        "-Dg",
    )

    exclude '**/suite/*.class'

    // dependsOn 'cleanTest'
}
]]

    local exp = {
      {
        'MC',
        'test',
        {
          'C',
          {
            {
              'MC',
              'jvmArgs',
              '"-Xmn128M"',
              '"-Xmx1024M"',
              '"-Xms1024M"',
              '"-Dg"'
            },
            { 'MC', 'exclude', "'**/suite/*.class'" }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  -- https://docs.gradle.org/current/dsl/org.gradle.api.artifacts.ConfigurationContainer.html
  --[[
      configurations.create('myConfiguration')
      configurations.myConfiguration.transitive = false

  A dynamic method is added for each configuration which takes a configuration closure.
  This is equivalent to calling
  ConfigurationContainer.getByName(java.lang.String, groovy.lang.Closure).
  For example:

      configurations.create('myConfiguration')
      configurations.myConfiguration {
          transitive = false
      }
  ]]

  it("parse_to_ast", function()
    local s = [[
plugins {
    id 'java' // so that I can use 'implementation', 'testImplementation' configurations
}

configurations {
    // configuration that holds jars to include in the jar
    extraLibs
}

dependencies {
    // to assembile into the jar
    extraLibs 'org.some:lib:0.3.0'
    implementation 'org.some:lib:0.3.0'

    testImplementation 'org.junit.jupiter:junit-jupiter:5.11.4'
}
 ]]
    local exp = {
      { 'MC', 'plugins', { 'C', { { 'MC', 'id', "'java'" } } } },
      {
        'MC',
        'configurations', { 'C', { { 'NI', 'extraLibs' } } }
      },
      {
        'MC',
        'dependencies',
        {
          'C',
          {
            { 'MC', 'extraLibs',      "'org.some:lib:0.3.0'" },
            { 'MC', 'implementation', "'org.some:lib:0.3.0'" },
            {
              'MC',
              'testImplementation',
              "'org.junit.jupiter:junit-jupiter:5.11.4'"
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast map as a param of the method (attributes)", function()
    local s = [[
jar {
    archiveFileName = "agent.jar"
    manifest {
        attributes(
            "Main-Class": 'org.agent.App',
            "Agent-Class": 'org.agent.Agent',
            "Premain-Class": 'org.agent.Agent',
            'Can-Redefine-Classes': 'true'
        )
    }
}
  ]]
    local exp = {
      {
        'MC',
        'jar',
        {
          'C',
          {
            { '=', 'archiveFileName', '"agent.jar"' },
            {
              'MC',
              'manifest',
              {
                'C',
                {
                  {
                    'MC',
                    'attributes',
                    {
                      'KVL',
                      { 'KV', '"Main-Class"',           "'org.agent.App'" },
                      { 'KV', '"Agent-Class"',          "'org.agent.Agent'" },
                      { 'KV', '"Premain-Class"',        "'org.agent.Agent'" },
                      { 'KV', "'Can-Redefine-Classes'", "'true'" }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  it("parse_to_ast ternary operator in closure", function()
    local s = [[
    from {
        configurations.extraLibs.collect { it.isDirectory() ? it : zipTree(it) }
    }
]]
    local exp = {
      {
        'MC',
        'from',
        {
          'C',
          {
            {
              'MC',
              'configurations.extraLibs.collect',
              {
                'C',
                {
                  {
                    'TO', { 'MC', 'it.isDirectory' }, 'it', { 'MC', 'zipTree', 'it' }
                  }
                }
              }
            }
          }
        }
      }
    }
    assert.same(exp, M.parse_to_ast(s))
  end)

  --[[
  it("to parse prod build.gradle", function()
    local fs = require 'env.files'
    local path = ...
    local bin = fs.read_all_bytes_from(path)
    assert.is_string(bin) ---@cast bin string
    local ast = M.parse_to_ast(bin)
    local exp = {}
    assert.same(exp, ast)
    assert.is_table(ast)
    local syntax = require 'env.langs.java.util.gradle.groovy_dsl_syntax'
    local exp_dsl = 1
    assert.same(exp_dsl, syntax.ast_to_dsl(ast))
  end)
]]
end)

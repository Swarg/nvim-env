require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)

local M = require 'env.langs.java.util.gradle.gradle_sources'

local gradle_src_root_dir = os.getenv('HOME') ..
    "/.gradle/wrapper/dists/gradle-8.9-all/6m0mbzute7p0zdleavqlib88a/gradle-8.9/src/"

describe("env.langs.java.util.gradle.gradle_sources", function()
  -- skip
  it("find_all_plugin_dirs_for_pkg_path", function()
    if 0 == 0 then return end -- skip
    local f = M.find_all_plugin_dirs_for_pkg_path

    local inner_pkg_path = "org/gradle/api/tasks/testing/"
    local exp = {
      'testing-base',
      'testing-base-infrastructure',
      'testing-jvm'
    }
    assert.same(exp, f(gradle_src_root_dir, inner_pkg_path))
  end)
end)

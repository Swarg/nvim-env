-- 23-02-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local M = require 'env.langs.java.util.gradle.gradle_sources'

-- samples

local gradle_wrapper_dists = "/home/user/.gradle/wrapper/dists/"
local gradle_src_root = gradle_wrapper_dists .. "gradle-8.9-all/" ..
    "6m0mbzute7p0zdleavqlib88a/gradle-8.9/src/"

-- just for example, how many Gradle architecture components
-- this is a list of subdirs with sources of gradle plugins|modules|core etc.
local plugin_subdirs = {
  "antlr",
  "base-asm",
  "base-ide-plugins",
  "base-services",
  "base-services-groovy",
  "bean-serialization-services",
  "build-cache",
  "build-cache-base",
  "build-cache-http",
  "build-cache-local",
  "build-cache-packaging",
  "build-cache-spi",
  "build-configuration",
  "build-events",
  "build-init",
  "build-operations",
  "build-option",
  "build-process-services",
  "build-profile",
  "build-state",
  "cli",
  "client-services",
  "code-quality",
  "composite-builds",
  "concurrent",
  "configuration-cache",
  "configuration-cache-base",
  "configuration-problems-base",
  "core",
  "core-api",
  "core-kotlin-extensions",
  "core-serialization-codecs",
  "daemon-main",
  "daemon-protocol",
  "daemon-server",
  "daemon-services",
  "declarative-dsl-api",
  "declarative-dsl-core",
  "declarative-dsl-evaluator",
  "declarative-dsl-provider",
  "declarative-dsl-tooling-builders",
  "declarative-dsl-tooling-models",
  "dependency-management",
  "dependency-management-serialization-codecs",
  "diagnostics",
  "ear",
  "enterprise",
  "enterprise-logging",
  "enterprise-operations",
  "enterprise-workers",
  "execution",
  "file-collections",
  "files",
  "file-temp",
  "file-watching",
  "flow-services",
  "functional",
  "gradle-cli-main",
  "graph-serialization",
  "guava-serialization-codecs",
  "hashing",
  "ide",
  "ide-native",
  "ide-plugins",
  "input-tracking",
  "installation-beacon",
  "instrumentation-declarations",
  "internal-instrumentation-api",
  "io",
  "ivy",
  "jacoco",
  "java-language-extensions",
  "java-platform",
  "jvm-services",
  "kotlin-dsl",
  "kotlin-dsl-provider-plugins",
  "kotlin-dsl-tooling-builders",
  "kotlin-dsl-tooling-models",
  "language-groovy",
  "language-java",
  "language-jvm",
  "language-native",
  "launcher",
  "logging",
  "logging-api",
  "maven",
  "messaging",
  "model-core",
  "model-groovy",
  "native",
  "normalization-java",
  "persistent-cache",
  "platform-base",
  "platform-jvm",
  "platform-native",
  "plugin-development",
  "plugins-application",
  "plugins-distribution",
  "plugins-groovy",
  "plugins-java",
  "plugins-java-base",
  "plugins-java-library",
  "plugins-jvm-test-fixtures",
  "plugins-jvm-test-suite",
  "plugins-test-report-aggregation",
  "plugins-version-catalog",
  "plugin-use",
  "problems-api",
  "process-services",
  "publish",
  "reporting",
  "resources",
  "resources-gcs",
  "resources-http",
  "resources-s3",
  "resources-sftp",
  "scala",
  "security",
  "serialization",
  "service-provider",
  "signing",
  "snapshots",
  "stdlib-kotlin-extensions",
  "stdlib-serialization-codecs",
  "testing-base",
  "testing-base-infrastructure",
  "testing-junit-platform",
  "testing-jvm",
  "testing-jvm-infrastructure",
  "testing-native",
  "test-kit",
  "test-suites-base",
  "time",
  "toolchains-jvm",
  "toolchains-jvm-shared",
  "tooling-api",
  "tooling-api-builders",
  "tooling-api-provider",
  "tooling-native",
  "unit-test-fixtures",
  "version-control",
  "war",
  "worker-main",
  "workers",
  "wrapper-main",
  "wrapper-shared"
}

local function mk_dirs_name(names)
  local t = {}
  for _, name in ipairs(names) do
    t[#t + 1] = name .. '/'
  end
  return t
end

describe("env.langs.java.util.gradle.gradle_sources", function()
  before_each(function()
    nvim.logger_off()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    M.clear_plugin_dir_cache()
  end)

  it("get_gradle_version_from_path", function()
    local path = "/home/user/.gradle/wrapper/dists/" ..
        "gradle-8.9-all/6m0mbzute7p0zdleavqlib88a/gradle-8.9/" ..
        "src/testing-junit-platform/org/gradle/api/internal/tasks/testing/" ..
        "junitplatform/JUnitPlatformTestExecutionListener.java"
    assert.same('8.9', M.parse_path_to_gradle_source_file(path))
  end)

  it("parse_gradle_version", function()
    local f = M.parse_gradle_version
    assert.same({ 8, 9 }, { f("8.9") })
    assert.same({ 8, 12, 1 }, { f("8.12.1") })
  end)

  it("find_gradle_version_of_distr_with_sources", function()
    local f = M.find_gradle_version_of_distr_with_sources
    local dists = {
      "gradle-3.5.1-all", "gradle-8.9-all", "gradle-8.9-bin",
      "gradle-8.12-all", "gradle-8.12.1-all",
    }

    sys:set_dir(gradle_wrapper_dists, mk_dirs_name(dists))
    local versions_with_sources = {}
    assert.same('8.12.1', f(versions_with_sources))
    local exp2 = { '3.5.1', '8.12', '8.12.1', '8.9' }
    assert.same(exp2, versions_with_sources)
  end)


  it("findRootDirOfGradleSources", function()
    sys:set_dir(gradle_src_root, { "testing-junit-platform/", "testing-base/" })
    local exp = '/home/user/.gradle/wrapper/dists/gradle-8.9-all/6m0mbzute7p0zdleavqlib88a/gradle-8.9/src/'
    assert.same(exp, M.find_root_dir_of_gradle_sources('8.9'))

    assert.same({ nil, 'no version' }, { M.find_root_dir_of_gradle_sources(nil) })

    local exp2 = { nil, 'not found "all"-distr for gradle version: "1.1"' }
    assert.same(exp2, { M.find_root_dir_of_gradle_sources('1.1') })
  end)


  -- only for unix, todo skip for win
  it("findPathToSourceOfClass diff plugin_subdir", function()
    local path = gradle_src_root .. "testing-junit-platform/" ..
        "org/gradle/api/internal/tasks/testing/junitplatform/JUnitPlatformTestExecutionListener.java"
    -- class to find
    local fqcn = "org.gradle.api.internal.tasks.testing.TestDescriptorInternal"

    -- sys:set_dir(gradle_src_root, { "testing-junit-platform/", "testing-base/" })
    sys:set_dir(gradle_src_root, mk_dirs_name(plugin_subdirs))

    local TestDescriptorInternal = gradle_src_root .. "testing-base-infrastructure/" ..
        "org/gradle/api/internal/tasks/testing/TestDescriptorInternal.java"
    sys:set_file_content(TestDescriptorInternal, { "dummy-class-content" })

    local exp = TestDescriptorInternal
    assert.same(exp, M.findPathToSourceOfClass(path, fqcn))
  end)


  it("findPathToSourceOfClass diff plugin_subdir with caching", function()
    local path = gradle_src_root .. "testing-junit-platform/" ..
        "org/gradle/api/internal/tasks/testing/junitplatform/JUnitPlatformTestExecutionListener.java"
    -- class to find
    local fqcn1 = "org.gradle.api.internal.tasks.testing.TestDescriptorInternal"
    local fqcn2 = "org.gradle.api.internal.tasks.testing.DefaultJUnitXmlReport"
    local fqcn3 = "org.gradle.api.internal.tasks.testing.TestFramework"
    -- sys:set_dir(gradle_src_root, { "testing-junit-platform/", "testing-base/" })
    sys:set_dir(gradle_src_root, mk_dirs_name(plugin_subdirs))

    local TestDescriptorInternal = gradle_src_root .. "testing-base-infrastructure/" ..
        "org/gradle/api/internal/tasks/testing/TestDescriptorInternal.java"
    local DefaultJUnitXmlReport = gradle_src_root .. "testing-base/" ..
        "org/gradle/api/internal/tasks/testing/DefaultJUnitXmlReport.java"
    local TestFramework = gradle_src_root .. "testing-jvm/" ..
        "org/gradle/api/internal/tasks/testing/TestFramework.java"

    sys:set_file_content(TestDescriptorInternal, { "dummy-class-content1" })
    sys:set_file_content(DefaultJUnitXmlReport, { "dummy-class-content2" })
    sys:set_file_content(TestFramework, { "dummy-class-content3" })

    -- require 'alogger'.fast_setup()
    local exp1 = TestDescriptorInternal
    local exp2 = DefaultJUnitXmlReport
    local exp3 = TestFramework
    assert.same(exp1, M.findPathToSourceOfClass(path, fqcn1))
    assert.same(exp2, M.findPathToSourceOfClass(path, fqcn2))
    assert.same(exp3, M.findPathToSourceOfClass(path, fqcn3))
    local cache = M.get_plugin_dir_cache()
    local exp = {
      ['org/gradle/api/internal/tasks/testing/'] = {
        'testing-base-infrastructure',
        'testing-base',
        'testing-jvm'
      }
    }
    assert.same(exp, cache)
  end)


  it("findPathToSourceOfClass same plugin_subdir", function()
    -- The name of the buffer from which we jump to definition
    local jump_path = gradle_src_root .. "testing-base-infrastructure/" ..
        "org/gradle/api/tasks/testing/TestFailure.java"
    -- class to find
    local fqcn = "org.gradle.api.internal.tasks.testing.DefaultTestFailure"

    sys:set_dir(gradle_src_root, { "testing-junit-platform/", "testing-base/" })

    local definition_path = gradle_src_root .. "testing-base-infrastructure/" ..
        "org/gradle/api/internal/tasks/testing/DefaultTestFailure.java"
    sys:set_file_content(definition_path, { "dummy" })

    local exp = definition_path
    assert.same(exp, M.findPathToSourceOfClass(jump_path, fqcn))
  end)
end)

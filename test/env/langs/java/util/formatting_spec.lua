-- 13-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.formatting'

describe("env.langs.java.util.formatting", function()
  it("format one-line-comment", function()
    local lines = {
      '//comment',
      '//comment with spaces',
      'http://host.com',
      '//c',
      'a//b',
      'a // b',
      'a // b //c',
      '    code   ', -- trailing spaces
      '    ',
    }
    M.format(lines) -- alter existed lines
    local exp = {
      '// comment',
      '// comment with spaces',
      'http://host.com',
      '// c',
      'a// b',
      'a // b',
      'a // b // c',
      '    code',
      ''
    }
    assert.same(exp, lines)
  end)

  it("format multi-line-comment", function()
    local lines = {
      '/*comment*/',
      '/*comment with spaces*/',
      '/*c*/',
      'a/*b*/',
      'a /* b*/',
      'a /* b */ //c',
      'a /*b */ //c',
      '/*b*/',
      '/*b */',
      '/*',
      '/* ',
      '/**',
    }
    M.format(lines) -- alter existed lines
    local exp = {
      '/* comment */',
      '/* comment with spaces */',
      '/* c */',
      'a/* b */',
      'a /* b */',
      'a /* b */ // c',
      'a /* b */ // c',
      '/* b */',
      '/* b */',
      '/*',
      '/*',
      '/**'
    }
    assert.same(exp, lines)
  end)

  it("format one-line-comment", function()
    local lines = {
      'for(x=1; x < 10; x++){',
      'for (b=1; b < 10; b++){',
      'if (true) {',
      'if(true) {',
      'if(true){',
    }
    M.format(lines) -- alter existed lines
    local exp = {
      'for (x=1; x < 10; x++) {',
      'for (b=1; b < 10; b++) {',
      'if (true) {',
      'if (true) {',
      'if (true) {'
    }
    assert.same(exp, lines)
  end)

  it("fmt_method_signature", function()
    local f = M.fmt_method_signature
    local line = '    public static MethodHandle lfindStaticGetter(' ..
        'Class<?> refc, String fieldname, Class<?> fieldtype, PrintStream err) {'
    local exp = {
      '    public static MethodHandle lfindStaticGetter(',
      '        Class<?> refc, String fieldname, Class<?> fieldtype, PrintStream err',
      '    ) {'
    }
    assert.same(exp, f(line))
  end)

  --

  it("fix_raw_types", function()
    local f = M.fix_raw_types
    assert.same('(Class<?> clazz, Class<?> c2)', f('(Class clazz, Class c2)'))
    assert.same('Class', f('Class'))
    assert.same('Class<?>', f('Class<?>'))
    assert.same('Class[]', f('Class[]'))
    assert.same('public Class<?>[] ', f('public Class[] '))
    assert.same('Class c;', f('Class c;'))
    assert.same('obj = new MyClass();', f('obj = new MyClass();'))
    assert.same('obj = new Class();', f('obj = new Class();'))
    assert.same('obj = new ArrayList<>();abc', f('obj = new ArrayList();abc'))
    assert.same('obj = new HashMap<>();abc', f('obj = new HashMap();abc'))
  end)
end)

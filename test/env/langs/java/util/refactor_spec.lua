-- 14-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
-- local sys = nvim:get_os()

local M = require 'env.langs.java.util.refactor'

describe("env.langs.java.util.refactor", function()
  it("invert_condition", function()
    local f = M.invert_condition
    local line = 'if (a != null && bin != null && bin.length > 0) {'
    local exp = 'if (a == null || bin == null || bin.length <= 0) {'
    assert.same(exp, f(line))

    line = 'if (a == null || bin == null || bin.length <= 0) {'
    exp = 'if (a != null && bin != null && bin.length > 0) {'
    assert.same(exp, f(line))

    line = 'if (logger!=null&&bin!=null&&bin.length>0) {'
    exp = 'if (logger == null || bin == null || bin.length <= 0) {'
    assert.same(exp, f(line))

    line = '    if (logger!=null&&bin!=null&&bin.length>0) {'
    exp = '    if (logger == null || bin == null || bin.length <= 0) {'
    assert.same(exp, f(line))

    line = 'if (s != null && !s.isEmpty()) {'
    exp = 'if (s == null ||   s.isEmpty()) {'
    assert.same(exp, f(line))
  end)

  it("fold_one_string_to_many", function()
    local line = '    String s = "line-1\\nline-2\\nline-3\\n";'
    local exp = {
      '    String s =',
      '        "line-1\\n" +',
      '        "line-2\\n" +',
      '        "line-3\\n";'
    }
    assert.same(exp, M.fold_one_string_to_many(line))
  end)

  it("fold_one_string_to_many", function()
    local line = '    String s = "this is a long-long realy long too long string'
        .. ' abcdefghijklhpoqrstuvwxuz";'
    local exp = {
      '    String s =',
      '        "this is a " +',
      '        "long-long " +',
      '        "realy long " +',
      '        "too long " +',
      '        "string " +',
      '        "abcdefghijk" +',
      '        "lhpoqrstuvw" +',
      '        "xuz";'
    }
    -- require 'dprint'.enable()
    assert.same(exp, M.fold_one_string_to_many(line, 10))
  end)

  it("beautify_tostring_output", function()
    local line = '    String exp = "DnsRecordResponse[id=dc97cfd44, comment=null,' ..
        ' comment_modified_on=null, created_on=2024-08-22T14:31:31.000000Z,' ..
        ' meta={}, modified_on=2024-11-12T05:19:41.000000Z, name=vm.mydomain.com,' ..
        ' proxiable=true, proxied=false, settings=DnsRecordSettings[ipv4_only=false,' ..
        ' ipv6_only=false], tags=[], tags_modified_on=null, ttl=1]";'
    local exp = {
      '    String exp = "DnsRecordResponse[" +',
      '        "id=dc97cfd44," +',
      '        " comment=null," +',
      '        " comment_modified_on=null," +',
      '        " created_on=2024-08-22T14:31:31.000000Z," +',
      '        " meta={}," +',
      '        " modified_on=2024-11-12T05:19:41.000000Z," +',
      '        " name=vm.mydomain.com," +',
      '        " proxiable=true," +',
      '        " proxied=false," +',
      '        " settings=DnsRecordSettings[" +',
      '        "ipv4_only=false," +',
      '        " ipv6_only=false]," +',
      '        " tags=[]," +',
      '        " tags_modified_on=null," +',
      '        " ttl=1]";'
    }
    assert.same(exp, M.beautify_tostring_output(line))
  end)
end)

--

describe("env.langs.java.util.refactor", function()
  it("swap_method_args", function()
    local line = '  assertEquals("Pull 123 from box", 123, unBox(ibox, 0));'
    local mn = 'assertEquals'
    local params = 'int, int, String'
    local p1, p2 = 1, 3
    local exp = '  assertEquals(unBox(ibox, 0), 123, "Pull 123 from box");'
    assert.same(exp, M.swap_method_args(line, mn, params, p1, p2))
  end)

  it("move_method_arg", function()
    local line = '  assertEquals("Pull 123 from box", 123, unBox(ibox, 0));'
    local mn = 'assertEquals'
    local params = 'int, int, String'
    local from, to = 1, 3
    local exp = '  assertEquals(123, unBox(ibox, 0), "Pull 123 from box");'
    assert.same(exp, M.move_method_arg(line, mn, params, from, to))
  end)

  it("set_method_arg", function()
    local line = '    assertEquals(1, 0); // comment'
    local sub_ast_of_method = {
      { 'METHOD',   'assertEquals', 5,  16 },
      { 'ARGUMENT', '1',            18, 18 },
      { 'ARGUMENT', '0',            21, 21 },
    }
    local exp = '    assertEquals("new", 0); // comment'
    assert.same(exp, M.set_method_arg(line, sub_ast_of_method, 1, "new"))

    local exp2 = '    assertEquals(1, "new"); // comment'
    assert.same(exp2, M.set_method_arg(line, sub_ast_of_method, 2, "new"))

    local exp3 = nil
    assert.same(exp3, M.set_method_arg(line, sub_ast_of_method, 3, "new"))
  end)

  it("set_method_name", function()
    local line = '    assertEquals(1, 0); // comment'
    local sub_ast_of_method = {
      { 'METHOD',   'assertEquals', 5,  16 },
      { 'ARGUMENT', '1',            18, 18 },
      { 'ARGUMENT', '0',            21, 21 },
    }
    local res, err = M.set_method_name(line, sub_ast_of_method, 'newName')
    assert.same(nil, err)
    local exp = '    newName(1, 0); // comment'
    assert.same(exp, res)
  end)

  it("prepare_value_to_insert", function()
    local f = M.prepare_value_to_insert
    assert.same('a\\nb', f('var s=""', 8, 9, "a\nb"))
  end)
end)



describe("env.langs.java.util.refactor", function()
  local Context = require 'env.ui.Context'
  local test_class_lines = {
    'package org.swarg.common;', -- 1
    '',
    'import org.junit.jupiter.api.Test;',
    'import static org.junit.jupiter.api.Assertions.*;',
    '',
    'public class SomeTest {',   -- 6
    '    @Test',
    '    void test_strings() {', -- 8
    '        String res = "AB";',
    '        String exp = "ABCDE";',
    '        assertEquals(exp, res);', -- 11
    '    }',
    '}'
  }
  it("find_variable_assigment", function()
    local lnum, col = 11, 24
    local bufnr = nvim:new_buf(test_class_lines, lnum, col)
    local ctx = Context.of(bufnr, lnum, col, nil)
    local res, err = M.find_variable_assigment(ctx, 'exp', 'ABCDE')

    assert.is_nil(err)
    local exp = {
      varname = 'exp',
      old_value = 'ABCDE',
      lnum = 10,
      lnum_end = 10,
      col = 22,
      col_end = 28,
    }
    assert.same(exp, res) ---@cast res table
    -- check plan to insert
    local line = test_class_lines[res.lnum]
    local old_value = string.sub(line, res.col, res.col_end)
    assert.same('"ABCDE"', old_value)
  end)
end)

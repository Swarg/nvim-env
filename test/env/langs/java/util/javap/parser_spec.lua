-- 02-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local M = require 'env.langs.java.util.javap.parser'

local fs = require 'env.files'
local read_lines = fs.read_lines


local root_dir = os.getenv('PWD') .. '/test/env/resources/java/realign'

local function get_javap_output_file(classname)
  return root_dir .. '/decompiled/' .. classname .. '.javap'
end


describe("env.langs.java.util.javap.line_number_parser", function()
  it("split constructor", function()
    local exp = { 'public', 'pkg.TestClass01', '(', ')', ';' }
    assert.same(exp, M.split('public pkg.TestClass01();'))

    local exp2 = { 'public', 'pkg.TestClass01', '(', 'int', 'int', ')', ';' }
    assert.same(exp2, M.split('public pkg.TestClass01(int, int);'))
  end)

  it("split method", function()
    local exp = { 'public', 'static', 'int[]', 'getSizes', '(', ')', ';' }
    assert.same(exp, M.split('public static int[] getSizes();'))
  end)


  it("is_constructor_def_line", function()
    local f = M.is_constructor_def_line
    local def = { 'public', 'pkg.MyClass', '(', ')', ';' }
    assert.same(true, f('pkg.MyClass', def))

    local def2 = { 'private', 'pkg.MyClass', '(', ')', ';' }
    assert.same(true, f('pkg.MyClass', def2))

    local def3 = { 'protected', 'pkg.MyClass', '(', ')', ';' }
    assert.same(true, f('pkg.MyClass', def3))

    local def4 = { 'pkg.MyClass', '(', ')', ';' }
    assert.same(true, f('pkg.MyClass', def4))
  end)

  it("is_method_def_line", function()
    local def = { 'public', 'static', 'int[]', 'getSizes', '(', ')', ';' }
    assert.same(true, M.is_method_def_line(def))

    local def2 = { 'public', 'static', 'void', 'method', '(', ')', ';' }
    assert.same(true, M.is_method_def_line(def2))

    local def3 = { 'public', 'static', 'void', 'method', '(', 'int', ')', ';' }
    assert.same(true, M.is_method_def_line(def3))
  end)

  it("get_constructor_params", function()
    local f = M.get_constructor_params
    assert.same({}, f('pkg.MyCls', { 'pkg.MyCls', '(', ')', ';' }))

    assert.same({ 'int' }, f('pkg.MyCls', { 'pkg.MyCls', '(', 'int', ')', ';' }))

    local exp = { 'int', 'int' }
    assert.same(exp, f('pkg.A', { 'pkg.A', '(', 'int', 'int', ')', ';' }))
  end)

  it("parse_line_table_entry", function()
    local f = M.parse_line_table_entry
    assert.same({ 17, 33 }, { f('line 17: 33') })
    assert.same({ 1, 3 }, { f('line 1: 3') })
    assert.same({ 4, 5 }, { f('  line 4: 5') })
    assert.same({ 444, 555 }, { f('  line 444: 555') })
    assert.same({ 444, 555 }, { f('  line 444: 555  // comment') })
  end)


  it("parseLineNumberTables", function()
    local lines = read_lines(get_javap_output_file('TestClass01'))
    assert.is_table(lines)
    local mapping = M.parseLineNumberTables(lines)

    local exp_mapping = {
      classname = "pkg.TestClass01",
      compiled = "TestClass01.java",
      constructors = {
        {
          visibility = "public",
          params = {},
          lnum = 11,
          lnum_end = 11,
        }
      },
      methods = {
        {
          visibility = "public",
          params = { "java.lang.String[]" },
          lnum = 30,
          lnum_end = 34,
          name = "main",
        },
        {
          visibility = "public",
          params = {},
          lnum = 41,
          lnum_end = 41,
          name = "getSizes",
        },
        {
          visibility = "public",
          params = { "int", "int" },
          lnum = 45,
          lnum_end = 45,
          name = "sum",
        },
        {
          visibility = "public",
          params = { "java.lang.String" },
          lnum = 50,
          lnum_end = 50,
          name = "getKey",
        },
        {
          visibility = "public",
          params = { "java.lang.String" },
          lnum = 54,
          lnum_end = 54,
          name = "addItem",
        }
      },
      static = {
        lnum = 21,
        lnum_end = 24
      }
    }
    assert.same(exp_mapping, mapping)
  end)

  --[[
Compiled from "TestClass01.java"
public class pkg.TestClass01 {
  public pkg.TestClass01();                       -- first default constructor
    LineNumberTable:
      line 6: 0

  public static void main(java.lang.String[]);    -- method
    LineNumberTable:
      line 23: 0
      ...

  public static int[] getSizes();
    LineNumberTable:
      line 32: 0

  public static int sum(int, int);
    LineNumberTable:
      line 36: 0

  static {};                                       -- statick block
    LineNumberTable:
      line 14: 0
      ..
}
]]
end)

-- 01-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.util.openapi.pojo'
local S = require 'env.langs.java.util.openapi.samples'

local fs = require 'env.files'
local lyaml = require "lyaml"

local path_zones_zone = 'test/env/resources/open_api/cloudflare_zones_zone.yml'
local body_zones_zone = fs.read_all_bytes_from(path_zones_zone)

local yml_sample_zones_zone = lyaml.load(body_zones_zone, { all = false })

describe("env.langs.java.util.openapi.pojo", function()
  before_each(function()
    require 'alogger'.fast_off()
  end)

  it("is_plural", function()
    local f = M.is_plural
    assert.same(true, f("zones", "zone"))
    assert.same(false, f("zone", "zones"))
  end)

  -- abuse-reports_BadActError string constant?
  it("gen_fully_qualifed_class_name", function()
    local f = M.gen_pkg_and_class_name
    local path = { "components", "schemas", "zones_zone" }
    local pkg = 'org.cloudflare'
    local exp = { 'org.cloudflare.zones', 'Zone' }

    assert.same(exp, { f({}, path, pkg) })

    path[3] = 'api-shield_api_discovery_origin_parameter'
    local exp3 = {
      'org.cloudflare.api.shield',
      'ApiDiscoveryOriginParameter'
    }
    assert.same(exp3, { f({}, path, pkg) })


    path[3] = 'aaa_components-schemas-api-response-common-failure'
    local exp4 = {
      'org.cloudflare.aaa',
      'ComponentsSchemasApiResponseCommonFailure'
    }
    assert.same(exp4, { f({}, path, pkg) })

    path[3] = 'aaa_alert-types'
    local exp5 = { 'org.cloudflare.aaa', 'AlertTypes' }
    assert.same(exp5, { f({}, path, pkg) })

    -- require 'alogger'.fast_setup(false, 0)
    path[3] = 'aaa_created_at'
    local exp6 = { 'org.cloudflare', 'AaaCreatedAt' }
    assert.same(exp6, { f({}, path, pkg) })

    path[3] = 'aaa_description'
    local exp7 = { 'org.cloudflare.aaa', 'Description' }
    assert.same(exp7, { f({}, path, pkg) })

    path[3] = 'aaa_integration-token'
    local exp8 = { 'org.cloudflare.aaa', 'IntegrationToken' }
    assert.same(exp8, { f({}, path, pkg) })

    path[3] = 'aaa_policies_components-schemas-response_collection'
    local exp9 = {
      'org.cloudflare.aaa.policies',
      'ComponentsSchemasResponseCollection'
    }
    assert.same(exp9, { f({}, path, pkg) })

    path[3] = 'aaa_schemas-identifier'
    local exp10 = { 'org.cloudflare.aaa', 'SchemasIdentifier' }
    assert.same(exp10, { f({}, path, pkg) })

    path[3] = 'zones_api-response-common'
    local exp11 = { 'org.cloudflare.zones', 'ApiResponseCommon' }
    assert.same(exp11, { f({}, path, pkg) })

    path[3] = 'dns-records_dns_response_collection'
    local exp12 = { 'org.cloudflare.dns.records', 'DnsResponseCollection' }
    assert.same(exp12, { f({}, path, pkg) })
  end)
end)


describe("env.langs.java.util.openapi.pojo", function()
  --[[

        zones_h2_prioritization:
            allOf:
                - $ref: '#/components/schemas/zones_base'
                - properties:
                    id:
                        description: ID of the zone setting.
                        enum:
                            - h2_prioritization
                        example: h2_prioritization
                    value:
                        $ref: '#/components/schemas/zones_h2_prioritization_value'
            description: HTTP/2 Edge Prioritization optimises the delivery of ...
            title: HTTP/2 Edge Prioritization

        zones_host_header_override:
            properties:
                id:
                    description: Apply a specific host header.
                    enum:
                        - host_header_override
                    type: string
                    x-auditable: true
                value:
                    description: The hostname to use in the `Host` header
                    example: example.com
                    minLength: 1
                    type: string
                    x-auditable: true
            title: Host Header Override
            type: object
]]
end)


describe("env.langs.java.util.openapi.pojo", function()
  before_each(function()
    require 'alogger'.fast_off()
  end)

  it("resolve_array_field_type", function()
    local yml_content = [[
    name_servers:
        description: The name servers assigns to a zone
        example:
            - bob.ns.dns.com
            - lola.ns.dns.com
        items:
            format: hostname
            type: string
        readOnly: true
        type: array
    ]]
    local yml = lyaml.load(yml_content, { all = false })
    local node = assert(yml.name_servers, 'yml.name_servers')
    local clazz = {
      fqcn = 'org.app.api.model.MyCompoment'
    }
    local field = {
      name = 'name_servers',
      type = 'array',
    }
    local res = M.resolve_array_field_type({}, clazz, field, node, nil)
    assert.same('List<String>', res)
  end)


  -- It can also be an example of inheritance
  -- when allOf + first element is $ref-to-type:object
  it("generate_pojo_class case: allOf and no type", function()
    local component_schema = [[
components:
    schemas:
        dns-records_dns_response_collection:
            allOf:
                - $ref: '#/components/schemas/dns-records_api-response-collection'
                - properties:
                    result:
                        field_b:
                        type: string
                  type: object
        dns-records_api-response-collection:
            properties:
                field_a:
                    type: string
            type: object
]]
    local yml = lyaml.load(component_schema, { all = false })
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local ref = "#/components/schemas/dns-records_dns_response_collection"
    local state = {
      yml = yml,
      lang = lang,
    }
    local pkg = 'org.app.api.model'

    local clazz, err = M.generate_pojo_class(state, ref, pkg)
    assert.same(nil, err)
    local exp = {
      api_component = '#/components/schemas/dns-records_dns_response_collection',
      parent_key = '#/components/schemas/dns-records_api-response-collection',
      pkg = 'org.app.api.model.dns.records',
      name = 'DnsResponseCollection',
      fqcn = 'org.app.api.model.dns.records.DnsResponseCollection',
      fields = {
        { name = 'field_a', type = 'String' },
        { name = 'result',  type = 'String' }
      }
    }
    assert.same(exp, clazz)
    local exp2 = { 'org.app.api.model.dns.records.DnsResponseCollection' }
    assert.same(exp2, M.get_tbl_keys(state.classes))
  end)


  -- This could be an example of OOP- inheritance and
  -- not with simple copying of the properties-fields into one class
  -- when allOf + first element is $ref-to-type:object
  -- todo: opts to generate multiple classes with inheritance
  it("generate_pojo_class case: allOf and no type", function()
    local component_schema = [[
components:
    schemas:
        dns-records_dns_response_collection:
            allOf:
                - $ref: '#/components/schemas/dns-records_api-response-collection'
                - properties:
                    result:
                        field_with_list_result:
                        type: string
                  type: object
        dns-records_api-response-collection:
            allOf:
                - $ref: '#/components/schemas/dns-records_api-response-common'
                - properties:
                    result_info:
                        $ref: '#/components/schemas/dns-records_result_info'
            type: object
        dns-records_api-response-common:
            properties:
                errors:
                    $ref: '#/components/schemas/dns-records_messages'
                messages:
                    $ref: '#/components/schemas/dns-records_messages'
                success:
                    description: Whether the API call was successful
                    type: boolean
            required:
                - success
                - errors
                - messages
            type: object
        dns-records_messages:
            example: []
            items:
                properties:
                    code:
                        type: integer
                    message:
                        type: string
                type: object
            type: array
        dns-records_result_info:
            properties:
                page:
                    type: number
            type: object
]]
    local yml = lyaml.load(component_schema, { all = false })
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local ref = "#/components/schemas/dns-records_dns_response_collection"
    local pkg = 'org.app.api.model'
    local state = {
      yml = yml,
      lang = lang,
      pkg = pkg,
    }
    -- require 'alogger'.fast_setup(false, 0)
    local clazz, err = M.generate_pojo_class(state, ref, pkg)
    assert.same(nil, err)
    local exp = {
      api_component = '#/components/schemas/dns-records_dns_response_collection',
      parent_key = '#/components/schemas/dns-records_api-response-common', -- 1th parent
      fqcn = 'org.app.api.model.dns.records.DnsResponseCollection',
      name = 'DnsResponseCollection',
      pkg = 'org.app.api.model.dns.records',
      imports = { ['java.util.List'] = true },
      fields = {
        { name = 'errors',      type = 'List<Message>', example = {} },
        { name = 'messages',    type = 'List<Message>', example = {} },
        { name = 'result',      type = 'String' },
        { name = 'result_info', type = 'ResultInfo' },
        {
          description = 'Whether the API call was successful',
          name = 'success',
          type = 'boolean'
        }
      }
    }
    assert.same(exp, clazz)
    local exp2 = {
      'org.app.api.model.dns.records.DnsResponseCollection',
      'org.app.api.model.dns.records.Message',
      'org.app.api.model.dns.records.ResultInfo'
    }
    assert.same(exp2, M.get_tbl_keys(state.classes))
  end)


  it("generate_pojo_class", function()
    local keypath = "#/components/schemas/zones_websockets" -- aka $ref
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    -- require'alogger'.fast_setup(false, 0)
    local o = {
      yml = yml_sample_zones_zone,
      lang = lang,
    }
    local clazz, err = M.generate_pojo_class(o, keypath, pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.zones', clazz.pkg)
    assert.same('Websockets', clazz.name)
    local exp = 'id editable modified_on value'
    assert.same(exp, S.get_fields_names(clazz))

    local exp_fields = {
      {
        description = 'ID of the zone setting.',
        enum = { 'websockets' },
        example = 'websockets',
        name = 'id',
        type = 'String'
      },
      {
        default = true,
        description = 'Whether or not this setting can be modified ..',
        name = 'editable',
        type = 'boolean',
        readOnly = true,
        enum = { true, false },
      },
      {
        description = 'last time this setting was modified.',
        name = 'modified_on',
        type = 'String',
        nullable = true,
        readOnly = true,
        example = '2014-01-01T05:20:00.12345Z',
        format = 'date-time',
      },
      {
        default = 'off',
        description = 'Value of the zone setting.',
        name = 'value',
        type = 'String',
        enum = { 'off', 'on' },
        example = 'on'
      }
    }
    assert.same(exp_fields, clazz.fields)
    assert.same(exp_fields, clazz.fields)
  end)

  -- todo fix class names and generate getter-setters
  it("generate_pojo_class", function()
    local keypath = "#/components/schemas/zones_zone"
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local o = {
      yml = yml_sample_zones_zone,
      lang = lang,
    }

    -- require'alogger'.fast_setup(false, 0)
    local clazz, err = M.generate_pojo_class(o, keypath, pkg)
    -- then
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.zones', clazz.pkg)
    assert.same('Zone', clazz.name)
    local exp_required_fields = { -- used as order of the fields
      'id', 'name', 'development_mode', 'owner', 'account', 'meta',
      'name_servers', 'original_name_servers', 'original_registrar', 'original_dnshost',
      'created_on', 'modified_on', 'activated_on'
    }
    assert.same(exp_required_fields, clazz.required)
    assert.is_nil(err)
    local exp = 'id name development_mode owner account meta name_servers ' ..
        'original_name_servers original_registrar original_dnshost created_on ' ..
        'modified_on activated_on ' ..
        -- optional (nullable)
        'paused status type vanity_name_servers verification_key'
    assert.same(exp, S.get_fields_names(clazz))

    local exp_fields = {
      -- order: description name type
      {
        description = 'Identifier',
        name = 'id',
        type = 'String',
        example = '023e105f4ecef8ad9ca31a8372d0c353',
        maxLength = 32,
      },
      {
        description = 'The domain name',
        name = 'name',
        type = 'String',
        example = 'example.com',
        maxLength = 253,
        pattern = [[^([a-zA-Z0-9][\-a-zA-Z0-9]*\.)+[\-a-zA-Z0-9]{2,20}$]],
      },
      {
        description = "The interval (in seconds) from when development mode expires\n" ..
            "(positive integer) or last expired (negative integer) for the\n" ..
            "domain. If development mode has never been enabled, this value is 0.",
        name = 'development_mode',
        type = 'int',
        example = 7200,
        readOnly = true,
      },
      {
        description = 'The owner of the zone',
        name = 'owner',
        type = 'ZoneOwner'
      },
      {
        description = 'The account the zone belongs to',
        name = 'account',
        type = 'ZoneAccount',
      },
      {
        description = 'Metadata about the zone',
        name = 'meta',
        type = 'ZoneMeta',
      },
      {
        description = 'The name servers Cloudflare assigns to a zone',
        name = 'name_servers',
        type = 'List<String>',
        example = { 'bob.ns.cloudflare.com', 'lola.ns.cloudflare.com' },
        readOnly = true,
      },
      {
        description = 'Original name servers before moving to Cloudflare',
        name = 'original_name_servers',
        type = 'List<String>',
        example = { 'ns1.originaldnshost.com', 'ns2.originaldnshost.com' },
        nullable = true,
        readOnly = true,
      },
      {
        description = 'Registrar for the domain at the time of switching to Cloudflare',
        name = 'original_registrar',
        type = 'String',
        example = 'GoDaddy',
        nullable = true,
        readOnly = true,
      },
      {
        description = 'DNS host at the time of switching to Cloudflare',
        name = 'original_dnshost',
        type = 'String',
        example = 'NameCheap',
        maxLength = 50,
        nullable = true,
        readOnly = true,
      },
      {
        description = 'When the zone was created',
        name = 'created_on',
        type = 'String',
        example = '2014-01-01T05:20:00.12345Z',
        format = 'date-time',
        readOnly = true,
      },
      {
        description = 'When the zone was last modified',
        name = 'modified_on',
        type = 'String',
        example = '2014-01-01T05:20:00.12345Z',
        format = 'date-time',
        readOnly = true,
      },
      {
        description = "The last time proof of ownership was detected and the zone was made\nactive",
        name = 'activated_on',
        type = 'String',
        example = '2014-01-02T00:01:00.12345Z',
        format = 'date-time',
        nullable = true,
        readOnly = true,
      },
      {
        description = "Indicates whether the zone is only using Cloudflare DNS services. A\n" ..
            "true value means the zone will not receive security or performance\nbenefits.\n",
        name = 'paused',
        type = 'boolean',
        default = false,
        readOnly = true,
      },
      {
        description = 'The zone status on Cloudflare.',
        name = 'status',
        type = 'String',
        enum = { 'initializing', 'pending', 'active', 'moved' },
        example = 'active',
        readOnly = true,
      },
      {
        description = "A full zone implies that DNS is hosted with Cloudflare. " ..
            "A partial zone is\ntypically a partner-hosted zone or a CNAME setup.\n",
        name = 'type',
        type = 'String',
        default = 'full',
        enum = { 'full', 'partial', 'secondary' },
        example = 'full',
      },
      {
        description = 'An array of domains used for custom name servers.',
        name = 'vanity_name_servers',
        type = 'List<String>',
        example = { 'ns1.example.com', 'ns2.example.com' },
      },
      {
        description = 'Verification key for partial zone setup.',
        name = 'verification_key',
        type = 'String',
        example = '284344499-1084221259',
        readOnly = true,
      }
    }
    assert.same(exp_fields, clazz.fields)

    assert.is_not_nil(o.classes['org.cloudflare.zones.Zone'])
    assert.same(exp_fields, o.classes['org.cloudflare.zones.Zone'].fields)

    local exp_class_1 = {
      api_component = '#/components/schemas/zones_zone/properties/account',
      pkg = 'org.cloudflare.zones',
      name = 'ZoneAccount',
      fqcn = 'org.cloudflare.zones.ZoneAccount',
      description = 'The account the zone belongs to',
      fields = {
        {
          description = 'Identifier',
          example = '023e105f4ecef8ad9ca31a8372d0c353',
          maxLength = 32,
          name = 'id',
          type = 'String'
        },
        {
          description = 'The name of the account',
          example = 'Example Account Name',
          name = 'name',
          type = 'String'
        }
      },
    }
    assert.same(exp_class_1, o.classes['org.cloudflare.zones.ZoneAccount'])

    local exp_class2 = {
      api_component = '#/components/schemas/zones_zone/properties/meta',

      description = 'Metadata about the zone',
      pkg = 'org.cloudflare.zones',
      name = 'ZoneMeta',
      fqcn = 'org.cloudflare.zones.ZoneMeta',
      fields = {
        {
          description = 'The zone is only configured for CDN',
          name = 'cdn_only',
          type = 'boolean',
          example = true
        },
        {
          description = 'Number of Custom Certificates the zone can have',
          name = 'custom_certificate_quota',
          type = 'int',
          example = 1
        },
        {
          description = 'The zone is only configured for DNS',
          name = 'dns_only',
          type = 'boolean',
          example = true
        },
        {
          description = 'The zone is setup with Foundation DNS',
          name = 'foundation_dns',
          type = 'boolean',
          example = true
        },
        {
          description = 'Number of Page Rules a zone can have',
          name = 'page_rule_quota',
          type = 'int',
          example = 100,
        },
        {
          description = 'The zone has been flagged for phishing',
          name = 'phishing_detected',
          type = 'boolean',
          example = false
        },
        { name = 'step', type = 'int', example = 2 }
      }
    }
    assert.same(exp_class2, o.classes['org.cloudflare.zones.ZoneMeta'])

    local exp_class3 = {
      api_component = '#/components/schemas/zones_zone/properties/owner',
      pkg = 'org.cloudflare.zones',
      name = 'ZoneOwner',
      fqcn = 'org.cloudflare.zones.ZoneOwner',
      description = 'The owner of the zone',
      fields = {
        {
          description = 'Identifier',
          example = '023e105f4ecef8ad9ca31a8372d0c353',
          maxLength = 32,
          name = 'id',
          type = 'String'
        },
        {
          description = 'The type of owner',
          example = 'organization',
          name = 'type',
          type = 'String'
        },
        {
          description = 'Name of the owner',
          example = 'Example Org',
          name = 'name',
          type = 'String'
        }
      }
    }
    assert.same(exp_class3, o.classes['org.cloudflare.zones.ZoneOwner'])
  end)
end)



describe("env.langs.java.util.openapi.pojo complex types", function()
  local yml_zones_api_response_common_path = [[
components:
    schemas:
        zones_api-response-common:
            properties:
                errors:
                    $ref: '#/components/schemas/zones_messages'
                messages:
                    $ref: '#/components/schemas/zones_messages'
                success:
                    description: Whether the API call was successful
                    example: true
                    type: boolean
            required:
                - success
                - errors
                - messages
            type: object
        zones_messages:
            example: []
            items:
                properties:
                    code:
                        minimum: 1000
                        type: integer
                    message:
                        type: string
                required:
                    - code
                    - message
                type: object
                uniqueItems: true
            type: array
]]
  local yml_zones_api_response_common = lyaml.load(yml_zones_api_response_common_path, { all = false })

  before_each(function()
    require 'alogger'.fast_off()
  end)


  it("gen_pkg_and_class_name", function()
    -- local key = '#/components/schemas/zones_api-response-common'
    local path = { 'components', 'schemas', 'zones_api-response-common' }
    local pkg = 'org.app.api.model'

    local pkg0, classname = M.gen_pkg_and_class_name({}, path, pkg)
    assert.same('org.app.api.model.zones', pkg0)
    assert.same('ApiResponseCommon', classname)
  end)

  it("build_primitive_field_type", function()
    -- given
    local key = '#/components/schemas/zones_api-response-common'
    local original_build_pojo_class = M.build_pojo_class
    local spy = {}
    local state, node = nil, nil

    local mock_build_pojo_class = function(state0, pkg0, cn0, node0, keypath0)
      state, node = state0, node0
      spy[#spy + 1] = { pkg = pkg0, classname = cn0, keypath = keypath0 }
      return nil, 'stop'
    end
    M.build_pojo_class = mock_build_pojo_class
    local yml = yml_zones_api_response_common
    local pkg = 'org.app.api.model'
    local opts = {
      as_records = true,
    }
    -- when
    local _, err = M.generate_classes(yml, { key }, pkg, opts)
    -- then
    assert.same("stop", err)
    assert.is_not_nil(state)

    local exp = {
      pkg = 'org.app.api.model.zones',
      classname = 'ApiResponseCommon',
      keypath = '#/components/schemas/zones_api-response-common',
    }
    assert.same(exp, spy[1])

    local exp_node = {
      properties = {
        errors = { ['$ref'] = '#/components/schemas/zones_messages' },
        messages = { ['$ref'] = '#/components/schemas/zones_messages' },
        success = {
          description = 'Whether the API call was successful',
          example = true,
          type = 'boolean'
        }
      },
      required = { 'success', 'errors', 'messages' },
      type = 'object'
    }
    assert.same(exp_node, node)

    -- restore original at final (cleanup)
    M.build_pojo_class = original_build_pojo_class
  end)


  -- for case collections of items(array)
  -- zones_messages:
  --     items:
  --         properties:
  --             ...
  --         type: object
  --     type: array
  it("gen_type_for_anon_object_of_items", function()
    local state = {
      pkg = 'org.app.api.model',
      yml = yml_zones_api_response_common,
      classes = {}
    }
    local clazz = {
      fqcn = 'pkg.app.api.model.zones.ApiResponseCommon',
      pkg = 'pkg.app.api.model.zones',
      name = 'ApiResponseCommon',
    }
    local fieldname = 'zones_messages'
    local node = state.yml.components.schemas.zones_messages
    assert.is_not_nil(node)

    local typ = M.gen_type_for_anon_item_object(state, fieldname, node, clazz, nil)
    assert.same('List<Message>', typ)
    local exp = {
      name = 'Message',
      pkg = 'pkg.app.api.model.zones',
      fqcn = 'pkg.app.api.model.zones.Message',
      fields = {
        { name = 'code',    type = 'int' },
        { name = 'message', type = 'String' }
      },
      required = { 'code', 'message' }
    }
    assert.same(exp, state.classes[clazz.pkg .. '.Message'])
    local exp2 = { ['java.util.List'] = true }
    assert.same(exp2, clazz.imports)
  end)


  --
  -- check specified usecase of merging parse inside allOf with $ref and
  --  $ref + properties. with nested $ref
  -- merging fields allOf:{ $ref + properties} with nested $ref's
  --[[
components:
    schema:
        dns-records_ARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    content:
                        description: A valid IPv4 address.
                        example: 198.51.100.4
                        format: ipv4
                        type: string
                        x-auditable: true
                    type:
                        description: Record type.
                        enum:
                            - A
                        example: A
                        type: string
                        x-auditable: true
                  type: object
            title: A Record
            type: object
]]
  it("assembly_obj_parts_from_allOf", function()
    -- given
    local key = '#/components/schemas/dns-records_ARecord' -- in building
    -- parts of the state
    local ref_shared_fields = "#/components/schemas/dns-records_dns-record-shared-fields"
    local raw_node_shared_fields = { -- aka "superclass|interface" for ARecord
      properties = {
        comment = { ["$ref"] = "#/components/schemas/dns-records_comment" },
        name = { ["$ref"] = "#/components/schemas/dns-records_name" },
        proxied = { ["$ref"] = "#/components/schemas/dns-records_proxied" },
        settings = { ["$ref"] = "#/components/schemas/dns-records_settings" },
        tags = { ["$ref"] = "#/components/schemas/dns-records_tags" },
        ttl = { ["$ref"] = "#/components/schemas/dns-records_ttl" }
      }
    }
    local raw_node_comment = {
      description = 'Comments or notes about the DNS record. ..',
      example = 'Domain verification record',
      type = 'string',
    }
    local raw_node_string_stub = { type = 'string' }
    local raw_node_int_stub = { type = 'integer' }

    local state = {
      yml = {
        components = {
          schemas = {
            ['dns-records_dns-record-shared-fields'] = raw_node_shared_fields,
            ['dns-records_comment'] = raw_node_comment,
            ['dns-records_name'] = raw_node_string_stub,
            ['dns-records_proxied'] = raw_node_string_stub,
            ['dns-records_settings'] = raw_node_string_stub,
            ['dns-records_tags'] = raw_node_string_stub,
            ['dns-records_ttl'] = raw_node_int_stub,
          }
        },
        classes = {} -- cached(alredy created) classes
      }
    }
    local allOf = { -- intut for assembly_obj_parts_from_allOf
      { ["$ref"] = ref_shared_fields },
      {
        properties = {
          content = { description = "A valid IPv4 address.", type = "string" },
          type = { description = "Record type.", type = "string" }
        },
        type = "object"
      }
    }

    -- require 'alogger'.fast_setup()
    -- prepare - check intermediate tooling
    local res = M.assembly_object_parts(state, raw_node_shared_fields, ref_shared_fields)
    local exp_resolved_parts = {
      name = { type = 'string' },
      comment = {
        description = 'Comments or notes about the DNS record. ..',
        example = 'Domain verification record',
        type = 'string'
      },
      proxied = { type = 'string' },
      settings = { type = 'string' },
      tags = { type = 'string' },
      ttl = { type = 'integer' }
    }
    assert.same(exp_resolved_parts, res)

    -- when
    local props, err, pk = M.assembly_obj_parts_from_allOf(state, allOf, key)
    -- then
    local exp = nil
    assert.same(exp, err)
    local exp_parent_key = ref_shared_fields
    assert.same(exp_parent_key, pk)
    local exp_props = {
      name = { type = 'string' },
      comment = {
        description = 'Comments or notes about the DNS record. ..',
        example = 'Domain verification record',
        type = 'string'
      },
      proxied = { type = 'string' },
      settings = { type = 'string' },
      tags = { type = 'string' },
      ttl = { type = 'integer' },
      --
      type = { description = 'Record type.', type = 'string' },
      content = { description = 'A valid IPv4 address.', type = 'string' },
    }
    assert.same(exp_props, props)
  end)
end)

describe("env.langs.java.util.openapi.pojo tooling", function()
  it("is_primitive_type", function()
    local f = M.is_primitive_type
    assert.same(true, f({ type = 'string' }))
    assert.same(true, f({ type = 'integer' }))
    assert.same(false, f({ type = 'object' }))
    assert.same(false, f({ type = 'array' }))
  end)

  it("get_implementations_of", function()
    local o = {}
    local add = M.add_class_to_cache
    add(o, S.clazz_dns_records_ARecord, S.clazz_dns_records_ARecord.api_component)
    add(o, S.clazz_dns_records_AAAARecord, S.clazz_dns_records_AAAARecord.api_component)

    local exp = {
      'org.cloudflare.dns.records.AAAARecord',
      'org.cloudflare.dns.records.ARecord'
    }
    assert.same(exp, M.get_implementations_of(o, 'org.cloudflare.dns.records.DnsRecord'))
  end)

  it("add_all_names", function()
    local common = {}

    local clazz1_fields = {
      { name = 'field_a' }, { name = 'field_b' }, { name = 'typ' }
    }
    M.add_all_names(common, clazz1_fields)
    local exp = {
      field_a = { name = 'field_a' },
      field_b = { name = 'field_b' },
      typ = { name = 'typ' }
    }
    assert.same(exp, common)

    local clazz2_fields = {
      { name = 'field_c' }, { name = 'field_d' }, { name = 'typ' }
    }

    M.remove_all_unshared_names(common, clazz2_fields)
    assert.same({ typ = { name = 'typ' } }, common)
  end)

  it("get_sorted_field_names", function()
    local f = M.get_sorted_field_names
    local names = { id = 1, a = 2, x = 1, b = 1 }
    assert.same({ 'id', 'a', 'b', 'x' }, f({}, nil, names))

    names = { id = 1, a = 2, x = 1, b = 1, ['type'] = 'int', name = 'X' }
    local exp = { 'id', 'type', 'name', 'a', 'b', 'x' }
    assert.same(exp, f({}, nil, names))

    local names2 = { a = 2, x = 1, b = 1, ['type'] = 'int', name = 'X' }
    local exp2 = { 'type', 'name', 'a', 'b', 'x' }
    assert.same(exp2, f({}, nil, names2))

    local names3 = { a = 2, x = 1, b = 1 }
    local exp3 = { 'a', 'b', 'x' }
    assert.same(exp3, f({}, nil, names3))
  end)

  -- given order + syntetic + alphabeth
  it("get_sorted_field_names", function()
    local f = M.get_sorted_field_names
    local fields_map = {
      meta = { name = 'meta', type = 'int' },
      id = { name = 'id', type = 'int' },
      dnsRecord = { name = 'dnsRecord', type = 'DnsRecord', syntetic = 'common_fields' },
      key_a = { name = 'key_a', type = 'int' },
      key_x = { name = 'key_x', type = 'int' },
    }
    local st, order = {}, { 'key_x', 'key_a' }
    local exp = { 'dnsRecord', 'key_x', 'key_a', 'id', 'meta' }
    assert.same(exp, f(st, order, fields_map))
  end)


  it("merge_enum_elms", function()
    local f = M.merge_enum_elms
    local enum1 = { 'A' }
    local enum2 = { 'A' }
    assert.same({ 'A' }, f(enum1, enum2))
    local enum3 = { 'AAAA' }
    assert.same({ 'A', 'AAAA' }, f(enum1, enum3))
  end)
end)

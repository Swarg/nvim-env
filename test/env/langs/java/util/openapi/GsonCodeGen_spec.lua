-- 07-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
-- vos - virtual operation system. test with file system emulation to speed up

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os()
local sys = nvim:get_os()

local project_cache = require 'env.cache.projects'

local log = require 'alogger'
local H = require 'env.langs.java.ahelper'
local upojo = require 'env.langs.java.util.openapi.pojo'
local CodeGen = require 'env.langs.java.util.openapi.CodeGen'
local GsonCodeGen = require 'env.langs.java.util.openapi.GsonCodeGen'

local lyaml = require "lyaml"

local res_dir = 'test/env/resources/'

local path_dns_records = res_dir .. 'open_api/cloudflare_dns_records.yml'
local body_dns_records = table.concat(sys:host_read_lines(path_dns_records), "\n")
local yml_dns_records = lyaml.load(body_dns_records, { all = false })

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

describe("env.langs.java.util.openapi.GsonCodeGen", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    project_cache.full_cleanup()
    log.fast_off()
  end)

  it("genTypeAdapterForEnum with common field agregator", function()
    local keys = { '#/components/schemas/dns-records_dns-record-response' }
    local pkg, proot = 'app.api.model', '/tmp/project/'
    -- pkg = 'org.swarg.cloudflare.api.model'
    local yml = yml_dns_records
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
      json_lib = GsonCodeGen,
    }
    local state, err = upojo.generate_classes(yml, keys, pkg, opts)
    assert.is_nil(err)
    assert.is_not_nil(state) ---@cast state table

    local codegen = CodeGen:new(nil, lang, state)
    local gson_cg = GsonCodeGen:new(nil, codegen)
    local fqin = pkg .. '.dns.records.DnsRecord'
    local enum_field_name = 'type'
    local clazz = codegen:get_clazz(fqin)
    assert.is_not_nil(clazz) ---@cast clazz table
    local type_field = codegen.get_elm_by_name(clazz.fields, enum_field_name)
    local enum_values = (type_field or E).enum
    local exp = {
      'A', 'AAAA', 'CAA', 'CERT', 'CNAME', 'DNSKEY', 'DS', 'HTTPS',
      'LOC', 'MX', 'NAPTR', 'NS', 'OPENPGPKEY', 'PTR', 'SMIMEA', 'SRV',
      'SSHFP', 'SVCB', 'TLSA', 'TXT', 'URI'
    }
    assert.same(exp, enum_values)

    -- require 'alogger'.fast_setup()
    local res = gson_cg:genTypeAdapterForEnum(enum_values, fqin, enum_field_name)
    local exp_source_code = [[
package app.api.model.dns.records.adapter;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import app.api.model.dns.records.AAAARecord;
import app.api.model.dns.records.ARecord;
import app.api.model.dns.records.CAARecord;
import app.api.model.dns.records.CERTRecord;
import app.api.model.dns.records.CNAMERecord;
import app.api.model.dns.records.DNSKEYRecord;
import app.api.model.dns.records.DSRecord;
import app.api.model.dns.records.DnsRecordSharedFields;
import app.api.model.dns.records.HTTPSRecord;
import app.api.model.dns.records.LOCRecord;
import app.api.model.dns.records.MXRecord;
import app.api.model.dns.records.NAPTRRecord;
import app.api.model.dns.records.NSRecord;
import app.api.model.dns.records.OPENPGPKEYRecord;
import app.api.model.dns.records.PTRRecord;
import app.api.model.dns.records.SMIMEARecord;
import app.api.model.dns.records.SRVRecord;
import app.api.model.dns.records.SSHFPRecord;
import app.api.model.dns.records.SVCBRecord;
import app.api.model.dns.records.TLSARecord;
import app.api.model.dns.records.TXTRecord;
import app.api.model.dns.records.URIRecord;

public class DnsRecordAdapter extends TypeAdapter<DnsRecord> {
    @Override
    public DnsRecord read(JsonReader in) throws IOException {
        JsonElement jsonElement = JsonParser.parseReader(in);
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        String type = jsonObject.get("type").getAsString();
        DnsRecordType dnsRecordType = DnsRecordType.valueOf(type); // Str2Enum

        switch (dnsRecordType) {
            case A:
                return gson.fromJson(jsonElement, ARecord.class);
            case AAAA:
                return gson.fromJson(jsonElement, AAAARecord.class);
            case CAA:
                return gson.fromJson(jsonElement, CAARecord.class);
            case CERT:
                return gson.fromJson(jsonElement, CERTRecord.class);
            case CNAME:
                return gson.fromJson(jsonElement, CNAMERecord.class);
            case DNSKEY:
                return gson.fromJson(jsonElement, DNSKEYRecord.class);
            case DS:
                return gson.fromJson(jsonElement, DSRecord.class);
            case HTTPS:
                return gson.fromJson(jsonElement, HTTPSRecord.class);
            case LOC:
                return gson.fromJson(jsonElement, LOCRecord.class);
            case MX:
                return gson.fromJson(jsonElement, MXRecord.class);
            case NAPTR:
                return gson.fromJson(jsonElement, NAPTRRecord.class);
            case NS:
                return gson.fromJson(jsonElement, NSRecord.class);
            case OPENPGPKEY:
                return gson.fromJson(jsonElement, OPENPGPKEYRecord.class);
            case PTR:
                return gson.fromJson(jsonElement, PTRRecord.class);
            case SMIMEA:
                return gson.fromJson(jsonElement, SMIMEARecord.class);
            case SRV:
                return gson.fromJson(jsonElement, SRVRecord.class);
            case SSHFP:
                return gson.fromJson(jsonElement, SSHFPRecord.class);
            case SVCB:
                return gson.fromJson(jsonElement, SVCBRecord.class);
            case TLSA:
                return gson.fromJson(jsonElement, TLSARecord.class);
            case TXT:
                return gson.fromJson(jsonElement, TXTRecord.class);
            case URI:
                return gson.fromJson(jsonElement, URIRecord.class);
            default:
                return gson.fromJson(jsonElement, DnsRecordSharedFields.class);
        }
    }
}]]
    assert.same(exp_source_code, table.concat(res, "\n"))
  end)
end)

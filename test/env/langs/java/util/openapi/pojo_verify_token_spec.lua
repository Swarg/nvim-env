require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.util.openapi.pojo'
local S = require 'env.langs.java.util.openapi.samples'

local fs = require 'env.files'
local lyaml = require "lyaml"

local path_verify_token = 'test/env/resources/open_api/cloudflare_verify_token.yml'
local body_verify_token = fs.read_all_bytes_from(path_verify_token)

local yml_verify_token = lyaml.load(body_verify_token, { all = false })

describe("env.langs.java.util.openapi.pojo", function()
  it("generate_pojo_class", function()
    local key = '#/components/schemas/iam_response_single_segment'
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local o = {
      pkg = pkg,
      yml = yml_verify_token,
      lang = lang,
    }
    local clazz, err = M.generate_pojo_class(o, key, pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare', clazz.pkg)
    assert.same('IamResponseSingleSegment', clazz.name)
    local exp = 'errors messages result success'
    assert.same(exp, S.get_fields_names(clazz))
    local exp_fields = {
      { name = 'errors',   type = 'List<Message>',                 example = {} },
      { name = 'messages', type = 'List<Message>',                 example = {} },
      { name = 'result',   type = 'IamResponseSingleSegmentResult' },
      {
        description = 'Whether the API call was successful',
        name = 'success',
        type = 'boolean',
        enum = { true },
        example = true
      }
    }
    assert.same(exp_fields, clazz.fields)
    local exp_generated_classes = {
      'org.cloudflare.IamResponseSingleSegment',
      'org.cloudflare.iam.Message',
      'org.cloudflare.IamResponseSingleSegmentResult'
    }
    assert.same(exp_generated_classes, S.get_classes_names(o))

    local clazz0 = o.classes['org.cloudflare.IamResponseSingleSegmentResult']
    local exp_Result_clazz = {
      api_component = '#/components/schemas/iam_response_single_segment/properties/result',
      fqcn = 'org.cloudflare.IamResponseSingleSegmentResult',
      name = 'IamResponseSingleSegmentResult',
      pkg = 'org.cloudflare',
      required = { 'id', 'status' },
      fields = {
        {
          description = 'Token identifier tag.',
          name = 'id',
          type = 'String',
          maxLength = 32,
          readOnly = true,
          example = 'ed17574386854bf78a67040be0a770b0',
        },
        {
          description = 'Status of the token.',
          name = 'status',
          type = 'String',
          enum = { 'active', 'disabled', 'expired' },
          example = 'active',
        },
        {
          description = 'The expiration time on or after which the JWT MUST NOT be accepted for processing.',
          name = 'expires_on',
          type = 'String',
          example = '2020-01-01T00:00:00Z',
          format = 'date-time',
        },
        {
          description = 'The time before which the token MUST NOT be accepted for processing.',
          name = 'not_before',
          type = 'String',
          example = '2018-07-01T05:20:00Z',
          format = 'date-time',
        }
      }
    }
    assert.same(exp_Result_clazz, clazz0)
  end)
end)

-- 02-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)

-- vos - virtual operation system. test with file system emulation to speed up

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os()
local sys = nvim:get_os()

local project_cache = require 'env.cache.projects'

local log = require 'alogger'
local H = require 'env.langs.java.ahelper'
local M = require 'env.langs.java.util.openapi.CodeGen'
local upojo = require 'env.langs.java.util.openapi.pojo'

local lyaml = require "lyaml"

local res_dir = 'test/env/resources/'
local path_zones_zone = res_dir .. 'open_api/cloudflare_zones_zone.yml'
local body_zones_zone = table.concat(sys:host_read_lines(path_zones_zone), "\n")

local path_Zone_records = res_dir .. 'open_api/generated/records/Zone.java.0'
local body_Zone_java = table.concat(sys:host_read_lines(path_Zone_records), "\n")

local yml_sample_01 = lyaml.load(body_zones_zone, { all = false })


describe("env.langs.java.util.openapi.pojo with emulated filesystem", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    project_cache.full_cleanup()
    log.fast_off()
  end)

  it("generate_classes with regular classes", function()
    local keys = { '#/components/schemas/zones_websockets' }
    local pkg, proot = 'org.example.api.model', '/tmp/project/'
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      quiet = true,
    }

    -- require'alogger'.fast_setup(false, 0)
    local state, err = M.generate_classes(yml_sample_01, keys, pkg, lang, opts)
    assert.same(nil, err)
    local exp = {
      {
        'org.example.api.model.zones.Websockets',
        '/tmp/project/src/main/java/org/example/api/model/zones/Websockets.java'
      }
    }
    assert.same(exp, (state or {}).report)

    local path = exp[1][2]
    local exp_class_body = [[
package org.example.api.model.zones;

/**
 *
 * @author AuthorName
 */
public class Websockets {

    /**
     * ID of the zone setting.
     * enum: websockets
     * example: websockets
     */
    private String id;
    /**
     * Whether or not this setting can be modified ..
     * enum: true false
     */
    private boolean editable = true;
    /**
     * last time this setting was modified.
     * format: date-time
     * example: 2014-01-01T05:20:00.12345Z
     */
    private String modified_on;
    /**
     * Value of the zone setting.
     * enum: off on
     * example: on
     */
    private String value = "off";

    public Websockets() {
    }

    public Websockets(String id, boolean editable, String modified_on, String value) {
        this.id = id;
        this.editable = editable;
        this.modified_on = modified_on;
        this.value = value;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean getEditable() {
        return this.editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getModified_on() {
        return this.modified_on;
    }

    public void setModified_on(String modified_on) {
        this.modified_on = modified_on;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean equals(Websockets websockets) {
        return
            this.id == websockets.id &&
            this.editable == websockets.editable &&
            this.modified_on == websockets.modified_on &&
            this.value == websockets.value;
    }

}
]]
    assert.same(exp_class_body, sys:get_file_content(path))
  end)


  it("generate_classes as record zones_zone", function()
    local keys = { '#/components/schemas/zones_zone' }
    local pkg, proot = 'org.example.api.model', '/tmp/project/'
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
    }

    -- require'alogger'.fast_setup(false, 0)
    local state, err = M.generate_classes(yml_sample_01, keys, pkg, lang, opts)
    assert.same(nil, err)
    local exp = {
      {
        'org.example.api.model.zones.Zone',
        '/tmp/project/src/main/java/org/example/api/model/zones/Zone.java'
      }
    }
    assert.same(exp, (state or {}).report)

    local path2java = exp[1][2]
    local exp_class_body = body_Zone_java
    local actual_result = sys:get_file_content(path2java)
    assert.same(exp_class_body, actual_result)
  end)

  --
  it("generate_classes as record zones_zone", function()
    local path = res_dir .. 'open_api/cloudflare_api_response_common.yml'
    local content = table.concat(sys:host_read_lines(path), "\n")
    local yml = lyaml.load(content, { all = false })

    local keys = { '#/components/schemas/zones_api-response-common' }
    local pkg, proot = 'org.example.api.model', '/tmp/project/'
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
    }
    -- require'alogger'.fast_setup(false, 0)
    local state, err = M.generate_classes(yml, keys, pkg, lang, opts)
    assert.same(nil, err)
    local exp_report = {
      {
        'org.example.api.model.zones.Message',
        '/tmp/project/src/main/java/org/example/api/model/zones/Message.java'
      },
      {
        'org.example.api.model.zones.ApiResponseCommon',
        '/tmp/project/src/main/java/org/example/api/model/zones/ApiResponseCommon.java'
      }
    }
    assert.same(exp_report, (state or {}).report)

    local path2java = exp_report[1][2]

    local exp1 = [[
package org.example.api.model.zones;

public record Message(
        int code,
        String message) {
}
]]
    assert.same(exp1, sys:get_file_content(path2java))

    local exp2 = [[
package org.example.api.model.zones;

import java.util.List;

/**
 * #/components/schemas/zones_api-response-common
 * required: success errors messages
 */
public record ApiResponseCommon(
        /**
         * Whether the API call was successful
         * example: true
         */
        boolean success,
        /** example: [] */
        List<Message> errors,
        /** example: [] */
        List<Message> messages) {
}
]]
    assert.same(exp2, sys:get_file_content(exp_report[2][2]))
  end)


  --
  -- example of a complex logic of interface class generation with oneOf
  -- When the creation of an interface class is going on with a direct request
  -- first created a regular clazz, then it is detected that it is an interface.
  -- There is a creation of another instance of the class-interface inside the
  -- logic of the field assembly_obj_parts_from_oneOf logic
  -- This is done in order to bind all another impls-classes from oneOf to this
  -- new clazz-interface in clazz.implements of its implementation classes.
  -- Then, while adding to the cache, the first dummy-clazz(interface) is
  -- replaced with the correct working class-interface (from state.classes)
  it("generate_classes", function()
    local schema = [[
components:
    schemas:
        dns-records_dns-record:
            oneOf:
                - $ref: '#/components/schemas/dns-records_ARecord'
                - $ref: '#/components/schemas/dns-records_AAAARecord'
            type: object
        dns-records_ARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    type:
                        enum:
                            - A
                        type: string
                  type: object
            type: object
        dns-records_AAAARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    type:
                        enum:
                            - AAAA
                        type: string
                  type: object
            type: object
        dns-records_dns-record-shared-fields:
            properties:
                name:
                    type: string
                ttl:
                    type: string
            type: object
]]
    local keys = { '#/components/schemas/dns-records_dns-record' }
    local pkg, proot = 'org.app.api.model', '/tmp/project/'
    local yml = lyaml.load(schema, { all = false })
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
    }

    -- require 'alogger'.fast_setup(false, 0)
    local state, err = M.generate_classes(yml, keys, pkg, lang, opts)
    assert.is_nil(err) ---@cast state table
    assert.is_not_nil(state.report)
    local exp = {
      'org.app.api.model.dns.records.DnsRecordSharedFields', -- glue (abstract)
      'org.app.api.model.dns.records.ARecord',
      'org.app.api.model.dns.records.AAAARecord',
      'org.app.api.model.dns.records.DnsRecord' -- interface
    }
    assert.same(exp, upojo.get_tbl_keys(state.classes))
    local models_dir = "/tmp/project/src/main/java/org/app/api/model"
    local exp_tree = [[
/tmp/project/src/main/java/org/app/api/model
    dns/
        records/
            AAAARecord.java
            ARecord.java
            DnsRecord.java
            DnsRecordSharedFields.java
2 directories, 4 files
]]
    assert.same(exp_tree, sys:tree(models_dir))
    local function get(relpath)
      return sys:get_file_lines(models_dir .. relpath)
    end
    -- print("local clazz = "..require"inspect"(state.classes['org.app.api.model.dns.records.DnsRecordSharedFields']))

    local exp1 = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * #/components/schemas/dns-records_dns-record',
      ' */',
      'public interface DnsRecord {',
      '    /** enum: A AAAA */',
      '    String type();',
      '',
      '    String name();',
      '',
      '    String ttl();',
      '',
      '}'
    }
    assert.same(exp1, get('/dns/records/DnsRecord.java'))

    local exp2 = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * default implementation of org.app.api.model.dns.records.DnsRecord',
      ' * #/components/schemas/dns-records_dns-record-shared-fields',
      ' */',
      'public record DnsRecordSharedFields(',
      '        /** enum: A AAAA */',
      '        String type,',
      '        String name,',
      '        String ttl) implements DnsRecord {',
      '}'
    }
    assert.same(exp2, get('/dns/records/DnsRecordSharedFields.java'))

    local exp3 = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * #/components/schemas/dns-records_ARecord',
      ' * parent scheme: #/components/schemas/dns-records_dns-record-shared-fields',
      ' */',
      'public record ARecord(',
      '        /** enum: A */',
      '        String type,',
      '        String name,',
      '        String ttl) implements DnsRecord {',
      '}'
    }
    assert.same(exp3, get('/dns/records/ARecord.java'))

    local exp4 = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * #/components/schemas/dns-records_AAAARecord',
      ' * parent scheme: #/components/schemas/dns-records_dns-record-shared-fields',
      ' */',
      'public record AAAARecord(',
      '        /** enum: AAAA */',
      '        String type,',
      '        String name,',
      '        String ttl) implements DnsRecord {',
      '}'
    }
    assert.same(exp4, get('/dns/records/AAAARecord.java'))
  end)
end)


--


describe("env.langs.java.util.openapi.pojo with emulated filesystem", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    project_cache.full_cleanup()
    log.fast_off()
  end)
  local path_dns_records = res_dir .. 'open_api/cloudflare_dns_records.yml'
  local body_dns_records = table.concat(sys:host_read_lines(path_dns_records), "\n")
  local yml_dns_records = lyaml.load(body_dns_records, { all = false })

  -- local path_Zone_records = res_dir .. 'open_api/generated/records/Zone.java.0'
  -- local body_Zone_java = table.concat(sys:host_read_lines(path_Zone_records), "\n")
  it("generate_classes with common field agregator", function()
    local keys = { '#/components/schemas/dns-records_dns-record-response' }
    local pkg, proot = 'app.api.model', '/tmp/project/'
    local yml = yml_dns_records
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
    }
    local state, err = M.generate_classes(yml, keys, pkg, lang, opts)
    assert.is_nil(err) ---@cast state table
    assert.is_not_nil(state) ---@cast state table
    assert.is_not_nil(state.report)
    local exp = {
      'app.api.model.dns.records.LOCRecord',
      'app.api.model.dns.records.SMIMEARecord',
      'app.api.model.dns.records.URIRecord',
      'app.api.model.dns.records.DNSKEYRecord',
      'app.api.model.dns.records.DnsRecordResponse',
      'app.api.model.dns.records.CNAMERecord',
      'app.api.model.dns.records.OPENPGPKEYRecord',
      'app.api.model.dns.records.ARecord',
      'app.api.model.dns.records.CAARecord',
      'app.api.model.dns.records.DnsRecordSharedFields',
      'app.api.model.dns.records.NAPTRRecord',
      'app.api.model.dns.records.TXTRecord',
      'app.api.model.dns.records.AAAARecord',
      'app.api.model.dns.records.Settings',
      'app.api.model.dns.records.DnsRecord',
      'app.api.model.dns.records.TLSARecord',
      'app.api.model.dns.records.SVCBRecord',
      'app.api.model.dns.records.SSHFPRecord',
      'app.api.model.dns.records.PTRRecord',
      'app.api.model.dns.records.SRVRecord',
      'app.api.model.dns.records.HTTPSRecord',
      'app.api.model.dns.records.DSRecord',
      'app.api.model.dns.records.NSRecord',
      'app.api.model.dns.records.MXRecord',
      'app.api.model.dns.records.CERTRecord'
    }
    assert.same(exp, upojo.get_tbl_keys(state.classes))

    local models_dir = "/tmp/project/src/main/java/app/api/model"
    local function get(relpath)
      return table.concat(sys:get_file_lines(models_dir .. relpath) or {}, "\n")
    end

    local exp_tree = [[
/tmp/project/src/main/java/app/api/model
    dns/
        records/
            AAAARecord.java
            ARecord.java
            CAARecord.java
            CERTRecord.java
            CNAMERecord.java
            DNSKEYRecord.java
            DnsRecord.java
            DnsRecordResponse.java
            DnsRecordSharedFields.java
            DSRecord.java
            HTTPSRecord.java
            LOCRecord.java
            MXRecord.java
            NAPTRRecord.java
            NSRecord.java
            OPENPGPKEYRecord.java
            PTRRecord.java
            Settings.java
            SMIMEARecord.java
            SRVRecord.java
            SSHFPRecord.java
            SVCBRecord.java
            TLSARecord.java
            TXTRecord.java
            URIRecord.java
2 directories, 25 files
]]
    assert.same(exp_tree, sys:tree(models_dir))

    local exp2 = [[
package app.api.model.dns.records;

/**
 * #/components/schemas/dns-records_dns-record-response
 * parent scheme: #/components/schemas/dns-records_dns-record
 * required: name type content data proxied ttl settings comment tags
 */
public record DnsRecordResponse(
        /** agregation of a common fields from #/components/schemas/dns-records_dns-record */
        DnsRecord dnsRecord,
        /**
         * When the record comment was last modified..
         * format: date-time
         * example: 2024-01-01T05:20:00.12345Z
         */
        String comment_modified_on,
        /**
         * When the record was created.
         * format: date-time
         * example: 2014-01-01T05:20:00.12345Z
         */
        String created_on,
        /**
         * Identifier
         * maxLength: 32
         * example: 023e105f4ecef8ad9ca31a8372d0c353
         */
        String id,
        /** Extra Cloudflare-specific information about the record. */
        Object meta,
        /**
         * When the record was last modified.
         * format: date-time
         * example: 2014-01-01T05:20:00.12345Z
         */
        String modified_on,
        /**
         * Whether the record can be proxied by Cloudflare or not.
         * example: true
         */
        boolean proxiable,
        /**
         * When the record tags were last modified...
         * format: date-time
         * example: 2025-01-01T05:20:00.12345Z
         */
        String tags_modified_on) {

    /**
     * DNS record name (or @ for the zone apex) in Punycode.
     * maxLength: 255
     * example: example.com
     */
    public String name() { return this.dnsRecord.name(); }

    /**
     * Record type.
     * enum: A AAAA CAA CERT CNAME DNSKEY DS HTTPS LOC MX NAPTR NS OPENPGPKEY PTR
     * SMIMEA SRV SSHFP SVCB TLSA TXT URI
     * example: A
     */
    public String type() { return this.dnsRecord.type(); }

    /**
     * A valid IPv4 address.
     * format: ipv4
     * example: 198.51.100.4
     */
    public String content() { return this.dnsRecord.content(); }

    /** this field is not found in the DnsRecord (a class with common_fields) */
    public Object data() { return this.dnsRecord.data(); }

    /**
     * Whether the record is receiving the performance and ..
     * example: true
     */
    public boolean proxied() { return this.dnsRecord.proxied(); }

    /**
     * Time To Live (TTL) of the DNS record in seconds...
     * example: 3600
     */
    public int ttl() { return this.dnsRecord.ttl(); }

    /** Settings for the DNS record. */
    public Settings settings() { return this.dnsRecord.settings(); }

    /**
     * Comments or notes about the DNS record. ..
     * example: Domain verification record
     */
    public String comment() { return this.dnsRecord.comment(); }

    /** Custom tags for the DNS record... */
    public List<String> tags() { return this.dnsRecord.tags(); }

    public DnsRecord getDnsRecord() { return this.dnsRecord; }
}]]
    assert.same(exp2, get('/dns/records/DnsRecordResponse.java'))
    -- local clazz = S.clazz_dns_records_DnsRecordResponse;
    --
  end)



  it("generate_classes with common field agregator", function()
    local keys = { '#/components/schemas/dns-records_dns_response_single' }
    local pkg, proot = 'app.api.model', '/tmp/project/'

    local yml = yml_dns_records
    local lang = H.mkJLang(proot)
    local opts = {
      no_date = true,
      as_records = true,
      quiet = true,
    }
    local state, err = M.generate_classes(yml, keys, pkg, lang, opts)
    assert.is_nil(err) ---@cast state table
    assert.is_not_nil(state) ---@cast state table
    assert.is_not_nil(state.report)

    local models_dir = "/tmp/project/src/main/java/app/api/model"
    local function get(relpath)
      return table.concat(sys:get_file_lines(models_dir .. relpath) or {}, "\n")
    end

    local exp_tree = [[
/tmp/project/src/main/java/app/api/model
    dns/
        records/
            AAAARecord.java
            ARecord.java
            CAARecord.java
            CERTRecord.java
            CNAMERecord.java
            DNSKEYRecord.java
            DnsRecord.java
            DnsRecordResponse.java
            DnsRecordSharedFields.java
            DnsResponseSingle.java
            DSRecord.java
            HTTPSRecord.java
            LOCRecord.java
            Message.java
            MXRecord.java
            NAPTRRecord.java
            NSRecord.java
            OPENPGPKEYRecord.java
            PTRRecord.java
            Settings.java
            SMIMEARecord.java
            SRVRecord.java
            SSHFPRecord.java
            SVCBRecord.java
            TLSARecord.java
            TXTRecord.java
            URIRecord.java
2 directories, 27 files
]]
    assert.same(exp_tree, sys:tree(models_dir))

    local exp_DnsResponseSingle_code = [[
package app.api.model.dns.records;

import java.util.List;

/**
 * #/components/schemas/dns-records_dns_response_single
 * parent scheme: #/components/schemas/dns-records_api-response-common
 */
public record DnsResponseSingle(
        /** example: [] */
        List<Message> errors,
        /** example: [] */
        List<Message> messages,
        DnsRecordResponse result,
        /**
         * Whether the API call was successful
         * enum: true
         * example: true
         */
        boolean success) {
}]]
    assert.same(exp_DnsResponseSingle_code, get('/dns/records/DnsResponseSingle.java'))
  end)
end)

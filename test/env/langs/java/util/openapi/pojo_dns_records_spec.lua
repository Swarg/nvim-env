-- 04-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.util.openapi.pojo'
local S = require 'env.langs.java.util.openapi.samples'

local fs = require 'env.files'
local lyaml = require "lyaml"


local path_dns_records = 'test/env/resources/open_api/cloudflare_dns_records.yml'
local body_dns_records = fs.read_all_bytes_from(path_dns_records)
local yml_dns_records = lyaml.load(body_dns_records, { all = false })

local function get_keys(t)
  local keys = {}; for k, _ in pairs(t or {}) do keys[#keys + 1] = k end;
  table.sort(keys)
  return keys
end


describe("env.langs.java.util.openapi.pojo", function()
  it("generate_pojo_class  dns-records_dns_response_collection", function()
    local key = "#/components/schemas/dns-records_dns_response_collection"
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local state = {
      yml = yml_dns_records,
      pkg = pkg,
      lang = lang,
    }
    -- require 'alogger'.fast_setup(false, 0)
    local clazz, err = M.generate_pojo_class(state, key, pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.dns.records', clazz.pkg)
    assert.same('DnsResponseCollection', clazz.name)

    local exp = S.dns_records_DnsResponseCollection
    assert.same(exp, clazz)

    local exp_sorted_list_of_fqcn = {
      'org.cloudflare.dns.records.AAAARecord',
      'org.cloudflare.dns.records.ARecord',
      'org.cloudflare.dns.records.CAARecord',
      'org.cloudflare.dns.records.CAARecordData',
      'org.cloudflare.dns.records.CERTRecord',
      'org.cloudflare.dns.records.CERTRecordData',
      'org.cloudflare.dns.records.CNAMERecord',
      'org.cloudflare.dns.records.DNSKEYRecord',
      'org.cloudflare.dns.records.DNSKEYRecordData',
      'org.cloudflare.dns.records.DSRecord',
      'org.cloudflare.dns.records.DSRecordData',
      'org.cloudflare.dns.records.DnsRecord',
      'org.cloudflare.dns.records.DnsRecordResponse',
      -- abstract (like glue to link "DnsRecord" interface with it implementations)
      'org.cloudflare.dns.records.DnsRecordSharedFields',
      'org.cloudflare.dns.records.DnsResponseCollection',
      'org.cloudflare.dns.records.HTTPSRecord',
      'org.cloudflare.dns.records.HTTPSRecordData',
      'org.cloudflare.dns.records.LOCRecord',
      'org.cloudflare.dns.records.LOCRecordData',
      'org.cloudflare.dns.records.MXRecord',
      'org.cloudflare.dns.records.Message',
      'org.cloudflare.dns.records.NAPTRRecord',
      'org.cloudflare.dns.records.NAPTRRecordData',
      'org.cloudflare.dns.records.NSRecord',
      'org.cloudflare.dns.records.OPENPGPKEYRecord',
      'org.cloudflare.dns.records.PTRRecord',
      'org.cloudflare.dns.records.ResultInfo',
      'org.cloudflare.dns.records.SMIMEARecord',
      'org.cloudflare.dns.records.SMIMEARecordData',
      'org.cloudflare.dns.records.SRVRecord',
      'org.cloudflare.dns.records.SRVRecordData',
      'org.cloudflare.dns.records.SSHFPRecord',
      'org.cloudflare.dns.records.SSHFPRecordData',
      'org.cloudflare.dns.records.SVCBRecord',
      'org.cloudflare.dns.records.SVCBRecordData',
      'org.cloudflare.dns.records.Settings',
      'org.cloudflare.dns.records.TLSARecord',
      'org.cloudflare.dns.records.TLSARecordData',
      'org.cloudflare.dns.records.TXTRecord',
      'org.cloudflare.dns.records.URIRecord',
      'org.cloudflare.dns.records.URIRecordData'
    }
    -- dns-records_api-response-collection
    assert.same(exp_sorted_list_of_fqcn, get_keys(state.classes))
    local c1 = state.classes['org.cloudflare.dns.records.DnsRecordSharedFields']
    assert.is_not_nil(c1)
    assert.is_nil(c1.typ) -- regular class|record

    local c2 = state.classes['org.cloudflare.dns.records.DnsRecord']
    assert.is_not_nil(c2)
    assert.same('interface', c2.typ)

    local exp2 = S.interface_dns_records_DnsRecord
    assert.same(exp2, c2)

    local c3 = state.classes['org.cloudflare.dns.records.ARecord']

    assert.is_not_nil(c3)
    local exp3 = S.clazz_dns_records_ARecord
    assert.same(exp3, c3)
    local idx1 = M.index_of_tbl_with_name(c1.fields, 'type')
    local idx2 = M.index_of_tbl_with_name(c2.fields, 'type')
    local idx3 = M.index_of_tbl_with_name(c3.fields, 'type')
    assert.same(1, idx1)
    assert.same(1, idx2)
    assert.same(1, idx3)
    -- diff instances of tables
    assert.are_not_equal(c1.fields[idx1], c3.fields[idx3])

    local c4 = state.classes['org.cloudflare.dns.records.AAAARecord']
    assert.is_not_nil(c4)
    local exp4 = S.clazz_dns_records_AAAARecord
    assert.same(exp4, c4)
  end)



  it("generate_pojo_class  dns-records_dns-record-shared-fields", function()
    local key = '#/components/schemas/dns-records_dns-record-shared-fields'
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local state = {
      yml = yml_dns_records,
      pkg = pkg,
      lang = lang,
    }
    local clazz, err = M.generate_pojo_class(state, key, pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.dns.records', clazz.pkg)
    assert.same('DnsRecordSharedFields', clazz.name)

    local exp = S.dns_records_DnsRecordSharedFields_direct
    assert.same(exp, clazz)

    local exp_sorted_list_of_fqcn = {
      'org.cloudflare.dns.records.DnsRecordSharedFields',
      'org.cloudflare.dns.records.Settings'
    }
    -- dns-records_api-response-collection
    assert.same(exp_sorted_list_of_fqcn, get_keys(state.classes))
    local c1 = state.classes['org.cloudflare.dns.records.DnsRecordSharedFields']
    assert.is_not_nil(c1)
    assert.same(nil, c1.typ)

    local c2 = state.classes['org.cloudflare.dns.records.Settings']
    assert.is_not_nil(c2)
    assert.same(nil, c2.typ)

    local exp2 = S.dns_records_Settings
    assert.same(exp2, c2)
  end)



  it("generate_pojo_class  dns-records_ARecord", function()
    local key = '#/components/schemas/dns-records_ARecord'
    local pkg = "org.cloudflare"
    local lang = { getProjectRoot = function() return "/tmp/project" end }
    local state = {
      yml = yml_dns_records,
      pkg = pkg,
      lang = lang,
    }
    -- require 'alogger'.fast_setup(false, 0)
    local clazz, err = M.generate_pojo_class(state, key, pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.dns.records', clazz.pkg)
    assert.same('ARecord', clazz.name)

    local exp = S.clazz_dns_records_ARecord
    assert.is_nil(clazz.implements)
    clazz.implements = 'org.cloudflare.dns.records.DnsRecord'
    assert.same(exp, clazz)

    local exp_sorted_list_of_fqcn = {
      'org.cloudflare.dns.records.ARecord',
      'org.cloudflare.dns.records.Settings'
    }
    -- dns-records_api-response-collection
    assert.same(exp_sorted_list_of_fqcn, get_keys(state.classes))
    local c1 = state.classes['org.cloudflare.dns.records.ARecord']
    assert.is_not_nil(c1)
    assert.same(nil, c1.typ)

    local c2 = state.classes['org.cloudflare.dns.records.Settings']
    assert.is_not_nil(c2)
    assert.same(nil, c2.typ)

    local exp2 = S.dns_records_Settings
    assert.same(exp2, c2)
  end)

  --[[
        dns-records_dns-record:
            oneOf:
                - $ref: '#/components/schemas/dns-records_ARecord'
                - $ref: '#/components/schemas/dns-records_AAAARecord'
                - $ref: '#/components/schemas/dns-records_CAARecord'
                ...

        dns-records_ARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    content: ...
                    type: ...
                  type: object
            type: object
        dns-records_CAARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    content:
                    data: ...
                    type: ...
                  type: object
            type: object
        dns-records_CERTRecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    content:
                    data: ...
                    type: ...
                  type: object
            type: object
        dns-records_CNAMERecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    content: ...
                    settings: ...
                    type: ...
                  type: object
            type: object

        dns-records_dns-record-shared-fields:
            properties:
                comment:
                    $ref: '#/components/schemas/dns-records_comment'  #(string)
                name:
                    $ref: '#/components/schemas/dns-records_name'     #(string)
                proxied:
                    $ref: '#/components/schemas/dns-records_proxied'  #(bool)
                settings:
                    $ref: '#/components/schemas/dns-records_settings' #(obj)
                tags:
                    $ref: '#/components/schemas/dns-records_tags'     # List<String>
                ttl:
                    $ref: '#/components/schemas/dns-records_ttl'      # number
            type: object

        dns-records_comment:
            description: ...
            type: string
]]

  -- todo:
  --   - [-] pass $ref to interface '#/components/schemas/dns-records_dns-record'
  --         as an inner (transient) field (with Adapter codegen)
  -- Thoughts in a rumor about how to represent this in Java Code:
  -- use inheritance? (not possible for java records) with records can us only
  -- interface
  it("generate_pojo_class  dns-records_dns-record-response", function()
    --[[
        dns-records_dns-record-response:               (class to be generated)
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record' (interface)
                - properties:
                    ...
                  type: object
            required:
                - name
                - type
                ...
            type: object
        dns-records_dns-record:                               (as interface)
            oneOf:
                - $ref: '#/components/schemas/dns-records_ARecord'   (its impls)
                - $ref: '#/components/schemas/dns-records_AAAARecord'
                - #ref: ...
        dns-records_ARecord: (as one of an implementations of the interface DnsRecord)
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                    ...
        dns-records_AAAARecord:
            allOf:
                - $ref: '#/components/schemas/dns-records_dns-record-shared-fields'
                - properties:
                     ...
    ]]
    -- given
    local key = '#/components/schemas/dns-records_dns-record-response'
    local state = { yml = yml_dns_records, pkg = "org.cloudflare" }

    -- require 'alogger'.fast_setup(false, 1) -- TODO add DnsRecord as a field
    -- when
    local clazz, err = M.generate_pojo_class(state, key, state.pkg)
    assert.same(nil, err)
    assert.is_not_nil(clazz) ---@cast clazz table
    assert.same('org.cloudflare.dns.records', clazz.pkg)
    assert.same('DnsRecordResponse', clazz.name)

    local exp_required = {
      'name', 'type', 'content', 'data', 'proxied', 'ttl', 'settings', 'comment', 'tags'
    }
    assert.same(exp_required, clazz.required)

    local exp_fnames = 'dnsRecord comment_modified_on created_on id meta modified_on ' ..
        'proxiable tags_modified_on'
    assert.same(exp_fnames, S.get_fields_names(clazz))

    local exp = S.clazz_dns_records_DnsRecordResponse
    assert.same(exp, clazz)
  end)
end)

-- 05-03-2025 @author Swarg
-- samples for testing
local M = {}
--

-- from resources/open_api/cloudflare_dns_records.yml

--
M.dns_records_DnsResponseCollection = {
  api_component = '#/components/schemas/dns-records_dns_response_collection',
  parent_key = '#/components/schemas/dns-records_api-response-common',
  fqcn = 'org.cloudflare.dns.records.DnsResponseCollection',
  imports = { ['java.util.List'] = true },
  name = 'DnsResponseCollection',
  pkg = 'org.cloudflare.dns.records',
  fields = {
    { name = 'errors',      type = 'List<Message>',          example = {} },
    { name = 'messages',    type = 'List<Message>',          example = {} },
    { name = 'result',      type = 'List<DnsRecordResponse>' },
    { name = 'result_info', type = 'ResultInfo' },
    {
      description = 'Whether the API call was successful',
      enum = { true },
      example = true,
      name = 'success',
      type = 'boolean'
    }
  },
}

-- Direct creation by ref as a regular clazz
M.dns_records_DnsRecordSharedFields_direct = {
  api_component = "#/components/schemas/dns-records_dns-record-shared-fields",
  pkg = "org.cloudflare.dns.records",
  name = "DnsRecordSharedFields",
  fqcn = "org.cloudflare.dns.records.DnsRecordSharedFields",
  imports = {
    ["java.util.List"] = true
  },

  fields = { {
    description = "DNS record name (or @ for the zone apex) in Punycode.",
    name = "name",
    type = "String",
    example = "example.com",
    maxLength = 255,
  }, {
    description = "Comments or notes about the DNS record. ..",
    name = "comment",
    type = "String",
    example = "Domain verification record",
  }, {
    description = "Whether the record is receiving the performance and ..",
    name = "proxied",
    type = "boolean",
    default = false,
    example = true,
  }, {
    description = "Settings for the DNS record.",
    name = "settings",
    type = "Settings"
  }, {
    description = "Custom tags for the DNS record...",
    name = "tags",
    type = "List<String>",
    default = {},
  }, {
    description = "Time To Live (TTL) of the DNS record in seconds...",
    name = "ttl",
    type = "int",
    default = 1,
    example = 3600,
  } }
}

-- Note: when build DnsRecord (#/components/schemas/dns-records_dns-record)
-- defined as an object+oneOf then consider it as an interface (for java records)
-- and consider the DnsRecordSharedFields as an "glue" that used only in the
-- api-component scheme as a way to bind the necessary components in such a
-- relationship
-- as an part of the (interface)DnsRecord creation for oneOf link
M.dns_record_DnsRecordSharedFields_abstract = {
  api_component = "#/components/schemas/dns-records_dns-record-shared-fields",
  typ = "abstract",
  fqcn = "org.cloudflare.dns.records.DnsRecordSharedFields",
  name = "DnsRecordSharedFields",
  pkg = "org.cloudflare.dns.records",
  fields = {
    {
      description = "Comments or notes about the DNS record. ..",
      name = "comment",
      type = "String",
      example = "Domain verification record",
    },
    -- extra field added from all oneOf impls (used in all variants of oneOf block)
    {
      description = "A valid IPv4 address.",
      name = "content",
      type = "String",
      example = "198.51.100.4",
      format = "ipv4",
    },
    {
      description = "DNS record name (or @ for the zone apex) in Punycode.",
      name = "name",
      type = "String",
      example = "example.com",
      maxLength = 255,
    },
    {
      default = false,
      description = "Whether the record is receiving the performance and ..",
      name = "proxied",
      type = "boolean",
      example = true,
    },
    {
      description = "Settings for the DNS record.",
      name = "settings",
      type = "Settings"
    },
    {
      description = "Custom tags for the DNS record...",
      name = "tags",
      type = "List<String>",
      default = {},
    },
    {
      description = "Time To Live (TTL) of the DNS record in seconds...",
      name = "ttl",
      type = "int",
      example = 3600,
      default = 1,
    },
    -- extra field added from collected all impls (all varians defined in the oneOf block)
    {
      description = "Record type.",
      name = "type",
      type = "String",
      enum = { "A", "AAAA", "CAA", "CERT", "CNAME", "DNSKEY", "DS", "HTTPS",
        "LOC", "MX", "NAPTR", "NS", "OPENPGPKEY", "PTR", "SMIMEA", "SRV", "SSHFP",
        "SVCB", "TLSA", "TXT", "URI" },
      example = "A",
    }
  }
}


M.dns_records_Settings = {
  api_component = "#/components/schemas/dns-records_settings",
  fqcn = "org.cloudflare.dns.records.Settings",
  name = "Settings",
  pkg = "org.cloudflare.dns.records",
  fields = { {
    description = "When enabled, only A records will be generated, and..",
    name = "ipv4_only",
    type = "boolean",
    example = true,
    default = false,
  }, {
    description = "When enabled, only AAAA records will be generated, and..",
    name = "ipv6_only",
    type = "boolean",
    example = true,
    default = false,
  } }
}

--
-- from resources/open_api/cloudflare_dns_records.yml
--
M.interface_dns_records_DnsRecord = {
  api_component = "#/components/schemas/dns-records_dns-record",
  typ = "interface",
  fqcn = "org.cloudflare.dns.records.DnsRecord",
  name = "DnsRecord",
  pkg = "org.cloudflare.dns.records",
  imports = {
    ['java.util.List'] = true
  },
  fields = {
    {
      description = "Record type.",
      name = "type",
      type = "String",
      enum = { "A", "AAAA", "CAA", "CERT", "CNAME", "DNSKEY", "DS", "HTTPS",
        "LOC", "MX", "NAPTR", "NS", "OPENPGPKEY", "PTR", "SMIMEA", "SRV",
        "SSHFP", "SVCB", "TLSA", "TXT", "URI" },
      example = "A",
    },
    {
      description = "DNS record name (or @ for the zone apex) in Punycode.",
      example = "example.com",
      maxLength = 255,
      name = "name",
      type = "String"
    },
    {
      description = "Comments or notes about the DNS record. ..",
      example = "Domain verification record",
      name = "comment",
      type = "String"
    },
    {
      description = "A valid IPv4 address.",
      example = "198.51.100.4",
      format = "ipv4",
      name = "content",
      type = "String"
    },
    {
      default = false,
      description = "Whether the record is receiving the performance and ..",
      example = true,
      name = "proxied",
      type = "boolean"
    },
    {
      description = "Settings for the DNS record.",
      name = "settings",
      type = "Settings"
    },
    {
      default = {},
      description = "Custom tags for the DNS record...",
      name = "tags",
      type = "List<String>"
    },
    {
      default = 1,
      description = "Time To Live (TTL) of the DNS record in seconds...",
      example = 3600,
      name = "ttl",
      type = "int"
    },
  },
}


-- created as a part of the DnsRecord (with interface introducing)
M.clazz_dns_records_ARecord = {
  api_component = '#/components/schemas/dns-records_ARecord',
  parent_key = '#/components/schemas/dns-records_dns-record-shared-fields',
  pkg = 'org.cloudflare.dns.records',
  name = 'ARecord',
  fqcn = 'org.cloudflare.dns.records.ARecord',
  imports = { ['java.util.List'] = true },
  implements = 'org.cloudflare.dns.records.DnsRecord',
  fields = {
    {
      description = 'Record type.',
      name = 'type',
      type = 'String',
      enum = { 'A' },
      example = 'A',
    },
    {
      description = 'DNS record name (or @ for the zone apex) in Punycode.',
      name = 'name',
      type = 'String',
      example = 'example.com',
      maxLength = 255,
    },
    {
      description = 'Comments or notes about the DNS record. ..',
      name = 'comment',
      type = 'String',
      example = 'Domain verification record',
    },
    {
      description = 'A valid IPv4 address.',
      example = '198.51.100.4',
      format = 'ipv4',
      name = 'content',
      type = 'String'
    },
    {
      description = 'Whether the record is receiving the performance and ..',
      name = 'proxied',
      type = 'boolean',
      example = true,
      default = false,
    },
    {
      description = 'Settings for the DNS record.',
      name = 'settings',
      type = 'Settings'
    },
    {
      default = {},
      description = 'Custom tags for the DNS record...',
      name = 'tags',
      type = 'List<String>'
    },
    {
      default = 1,
      description = 'Time To Live (TTL) of the DNS record in seconds...',
      name = 'ttl',
      type = 'int',
      example = 3600
    },
  },
}

M.clazz_dns_records_AAAARecord = {
  api_component = "#/components/schemas/dns-records_AAAARecord",
  parent_key = "#/components/schemas/dns-records_dns-record-shared-fields",
  pkg = "org.cloudflare.dns.records",
  name = "AAAARecord",
  fqcn = "org.cloudflare.dns.records.AAAARecord",
  implements = "org.cloudflare.dns.records.DnsRecord",
  imports = { ["java.util.List"] = true },
  fields = {
    {
      description = "Record type.",
      name = "type",
      type = "String",
      enum = { "AAAA" },
      example = "AAAA",
    },
    {
      description = "DNS record name (or @ for the zone apex) in Punycode.",
      example = "example.com",
      maxLength = 255,
      name = "name",
      type = "String"
    },
    {
      description = "Comments or notes about the DNS record. ..",
      example = "Domain verification record",
      name = "comment",
      type = "String"
    },
    {
      description = "A valid IPv6 address.",
      example = "2400:cb00:2049::1",
      format = "ipv6",
      name = "content",
      type = "String"
    },
    {
      default = false,
      description = "Whether the record is receiving the performance and ..",
      example = true,
      name = "proxied",
      type = "boolean"
    },
    {
      description = "Settings for the DNS record.",
      name = "settings",
      type = "Settings"
    },
    {
      default = {},
      description = "Custom tags for the DNS record...",
      name = "tags",
      type = "List<String>"
    },
    {
      default = 1,
      description = "Time To Live (TTL) of the DNS record in seconds...",
      example = 3600,
      name = "ttl",
      type = "int"
    },
  }
}

M.clazz_dns_records_DnsRecordResponse = {
  api_component = '#/components/schemas/dns-records_dns-record-response',
  parent_key = '#/components/schemas/dns-records_dns-record',
  pkg = 'org.cloudflare.dns.records',
  name = 'DnsRecordResponse',
  fqcn = 'org.cloudflare.dns.records.DnsRecordResponse',
  imports = {},
  required = { -- required fields
    'name', 'type', 'content', 'data', 'proxied', 'ttl', 'settings',
    'comment', 'tags'
  },

  -- order: comment_modified_on created_on id meta modified_on proxiable tags_modified_on
  fields = {
    -- order: description name type
    {
      description = 'agregation of a common fields from ' ..
          '#/components/schemas/dns-records_dns-record',
      name = 'dnsRecord',
      type = 'DnsRecord',
      syntetic = 'common_fields',
    },
    {
      description = 'When the record comment was last modified..',
      name = 'comment_modified_on',
      type = 'String',
      example = '2024-01-01T05:20:00.12345Z',
      format = 'date-time',
      readOnly = true,
    },
    {
      description = 'When the record was created.',
      name = 'created_on',
      type = 'String',
      example = '2014-01-01T05:20:00.12345Z',
      format = 'date-time',
      readOnly = true,
    },
    {
      description = 'Identifier',
      name = 'id',
      type = 'String',
      example = '023e105f4ecef8ad9ca31a8372d0c353',
      maxLength = 32,
    },
    {
      description = 'Extra Cloudflare-specific information about the record.',
      name = 'meta',
      type = 'Object',
      readOnly = true,
    },
    {
      description = 'When the record was last modified.',
      name = 'modified_on',
      type = 'String',
      example = '2014-01-01T05:20:00.12345Z',
      format = 'date-time',
      readOnly = true,
    },
    {
      description = 'Whether the record can be proxied by Cloudflare or not.',
      name = 'proxiable',
      type = 'boolean',
      example = true,
    },
    {
      description = 'When the record tags were last modified...',
      name = 'tags_modified_on',
      type = 'String',
      example = '2025-01-01T05:20:00.12345Z',
      format = 'date-time',
      readOnly = true,
    }
  }
}


-- helpers
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match

function M.show_field(clazz, fieldname)
  local idx = M.index_of_tbl_with_name(clazz.fields, fieldname)
  local field = clazz.fields[idx or false]
  print("[DEBUG] filed:", clazz.fqcn, fieldname, ' found:', idx, field,
    require "inspect" (field))
end

function M.get_fields_names(clazz)
  local t = {}
  for _, field in ipairs(clazz.fields or E) do
    t[#t + 1] = v2s(field.name)
  end
  return table.concat(t, " ")
end

function M.get_field(clazz, name)
  for _, field in ipairs(clazz.fields or E) do
    if field.name == name then
      return field
    end
  end
  return nil
end

function M.get_classes_names(state)
  local t = {}
  for fqcn, _ in pairs((state or E).classes or E) do
    t[#t + 1] = v2s(fqcn)
  end
  return t
end

return M

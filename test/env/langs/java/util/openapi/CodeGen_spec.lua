-- 05-03-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local CodeGen = require 'env.langs.java.util.openapi.CodeGen'
local S = require 'env.langs.java.util.openapi.samples'

describe("env.langs.java.util.openapi.CodeGen", function()
  before_each(function()
    require 'alogger'.fast_off()
  end)
  --
  -- generate java16+ code with records
  ---@return env.langs.java.util.openapi.CodeGen
  ---@param lang env.langs.java.JLang?
  local function getCodeGen(lang, state)
    lang = lang or {} -- stub
    ---@diagnostic disable-next-line: param-type-mismatch
    local gen = CodeGen:new(nil, lang, state or { opts = { quiet = true } })
    gen:initDocblock()
    return gen
  end

  local clazz_Zone = {
    api_component = '#/components/schemas/zones_zone',
    pkg = 'org.cloudflare.zones',
    name = 'Zone',
    fqcn = 'org.cloudflare.zones.Zone',
    description = 'the comment under the class definition line',
    fields = {
      { description = 'Identifier',        name = 'id',      type = 'int' },
      { description = 'The type of owner', name = 'type',    type = 'String' },
      { description = 'The owner account', name = 'account', type = 'Account' }
    },
    classes = {
      {
        api_component = '#/components/schemas/zones_zone/account',
        pkg = 'org.cloudflare.zones',
        name = 'Account',
        description = 'comment under the sub record with name "Account"',
        fqcn = 'org.cloudflare.zones.Zone.Account',
        fields = {
          { description = 'Identifier',              name = 'id',   type = 'String' },
          { description = 'The name of the account', name = 'name', type = 'String' }
        }
      }
    }
  }
  it("gen_source_code_with_java_records", function()
    local output = getCodeGen():gen_source_code_with_java_records(clazz_Zone)
    local exp = {
      'package org.cloudflare.zones;',
      '',
      '/**',
      ' * the comment under the class definition line',
      ' * #/components/schemas/zones_zone',
      ' */',
      'public record Zone(',
      '        /** Identifier */',
      '        int id,',
      '        /** The type of owner */',
      '        String type,',
      '        /** The owner account */',
      '        Account account) {',
      '',
      '    /**',
      '     * comment under the sub record with name "Account"',
      '     * #/components/schemas/zones_zone/account',
      '     */',
      '    public record Account(',
      '            /** Identifier */',
      '            String id,',
      '            /** The name of the account */',
      '            String name) {',
      '    }',
      '}',
      ''
    }
    assert.same(exp, output)
  end)

  local clazz_DnsRecord = {
    api_component = "#/components/schemas/dns-records_dns-record",
    typ = "interface",
    pkg = "org.app.api.model.dns.records",
    name = "DnsRecord",
    fqcn = "org.app.api.model.dns.records.DnsRecord",
    fields = {
      { name = "type", type = "String", read_only = true },
      { name = "name", type = "String" },
      { name = "ttl",  type = "String" },
    },
  }

  it("gen_source_code_with_interface_body", function()
    local output = getCodeGen():gen_source_code_of_interface(0, clazz_DnsRecord)
    local exp = {
      'public interface DnsRecord {',
      '    String getType();',
      '',
      '    String getName();',
      '    void setName(String name);',
      '',
      '    String getTtl();',
      '    void setTtl(String ttl);',
      '',
      '}'
    }
    assert.same(exp, output)
  end)

  it("gen_source_code_with_interface_body", function()
    local res = getCodeGen():gen_source_code_with_java_records(clazz_DnsRecord)
    local exp = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * #/components/schemas/dns-records_dns-record',
      ' */',
      'public interface DnsRecord {',
      '    String getType();',
      '',
      '    String getName();',
      '    void setName(String name);',
      '',
      '    String getTtl();',
      '    void setTtl(String ttl);',
      '',
      '}',
      ''
    }
    assert.same(exp, res)
  end)



  local clazz_DnsRecordSharedFields = {
    api_component = "#/components/schemas/dns-records_dns-record-shared-fields",
    typ = "abstract",
    pkg = "org.app.api.model.dns.records",
    name = "DnsRecordSharedFields",
    fqcn = "org.app.api.model.dns.records.DnsRecordSharedFields",
    fields = {
      { name = "name",          type = "String" },
      { name = "ttl",           type = "String" },
      { enum = { "A", "AAAA" }, name = "type",  type = "String", read_only = true } },
  }

  it("gen_source_code_of_abstract_clazz", function()
    local res = getCodeGen():gen_source_code_of_abstract_clazz(0, clazz_DnsRecordSharedFields)
    local exp = {
      'public abstract class DnsRecordSharedFields {',
      '    protected String name;',
      '    protected String ttl;',
      '    /** enum: A AAAA */',
      '    protected String type;',
      '',
      '    public String getName() {',
      '        return this.name;',
      '    }',
      '',
      '    public void setName(String name) {',
      '        this.name = name;',
      '    }',
      '',
      '    public String getTtl() {',
      '        return this.ttl;',
      '    }',
      '',
      '    public void setTtl(String ttl) {',
      '        this.ttl = ttl;',
      '    }',
      '',
      '    public String getType() {',
      '        return this.type;',
      '    }',
      '}'
    }
    assert.same(exp, res)
  end)


  it("gen_source_code_with_java_records abstrac class", function()
    local res = getCodeGen():gen_source_code_with_java_records(clazz_DnsRecordSharedFields)
    local exp = {
      'package org.app.api.model.dns.records;',
      '',
      '/**',
      ' * #/components/schemas/dns-records_dns-record-shared-fields',
      ' */',
      'public abstract class DnsRecordSharedFields {',
      '    protected String name;',
      '    protected String ttl;',
      '    /** enum: A AAAA */',
      '    protected String type;',
      '',
      '    public String getName() {',
      '        return this.name;',
      '    }',
      '',
      '    public void setName(String name) {',
      '        this.name = name;',
      '    }',
      '',
      '    public String getTtl() {',
      '        return this.ttl;',
      '    }',
      '',
      '    public void setTtl(String ttl) {',
      '        this.ttl = ttl;',
      '    }',
      '',
      '    public String getType() {',
      '        return this.type;',
      '    }',
      '}',
      ''
    }
    assert.same(exp, res)
  end)


  it("is_all_impls_are_records", function()
    local o = {
      classes = {
        [S.clazz_dns_records_ARecord.fqcn] = S.clazz_dns_records_ARecord,
        [S.clazz_dns_records_AAAARecord.fqcn] = S.clazz_dns_records_AAAARecord,
      }
    }

    local gen = getCodeGen(nil, o)
    local res = gen:is_all_impls_are_records('org.cloudflare.dns.records.DnsRecord')
    assert.same(true, res)
  end)
end)

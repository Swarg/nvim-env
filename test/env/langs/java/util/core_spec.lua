-- 09-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.core'

describe("env.langs.java.util.core", function()
  it("isVariableName", function()
    local f = M.isVariableName
    assert.is_true(f('a'))
    assert.is_true(f('abc'))
    assert.is_true(f('ab_c'))
    assert.is_true(f('a1b_c0'))
    assert.is_false(f('0bc')) -- class
    assert.is_false(f('Abc')) -- class
    assert.is_false(f('A'))
  end)

  it("isConstantName", function()
    local f = M.isConstantName
    assert.same(true, f('A'))
    assert.same(true, f('CONST_NAME'))
    assert.same(true, f('_CONST_NAME'))
    assert.same(false, f('const_name'))
    assert.same(true, f('VALID_IPV6_PATTERN1'))
  end)


  it("validatedVariableName", function()
    local f = M.validatedVariableName
    local exp = 'expected valid variable name for "%?" got:"while"'
    assert.match_error(function() f('while') end, exp)

    local em = [[expected valid variable name for "name" got:"while" token: {
  name = "while"
}]]
    assert.match_error(function() f('while', { name = 'while' }, 'name') end, em)
  end)

  it("isClassName", function()
    local f = M.isClassName
    assert.same(true, f('Class'))
    assert.same(true, f('A'))
    assert.same(true, f('A0'))
    assert.same(true, f('A$Inner'))
    assert.same(true, f('A$b'))
    assert.same(true, f('A$B'))
    assert.same(false, f('A$$B'))
    assert.same(false, f('not_a_class'))
    assert.same(false, f('not-a-class'))
    assert.same(false, f('org.comp.MyClass'))
  end)

  -- short class name without package First letter UpperCase
  it("isShortClassName", function()
    local f = M.isShortClassName
    assert.is_true(f("Class"))
    assert.is_true(f("Ca"))
    assert.is_true(f("CaCsomething"))
    assert.is_true(f("CCsomething"))
    assert.is_true(f("HttpURLConnection"))
    assert.is_true(f("IOException"))
    assert.is_true(f("ClassName"))
    assert.is_true(f("Cl0ssName"))
    assert.is_true(f("Class_Name"))
    assert.is_true(f("Class_Name$Inner"))
    assert.is_true(f("C0lassName"))
    assert.is_true(f("OS"))
    assert.is_true(f("URI"))
    assert.is_true(f("URL"))

    assert.is_false(f(".Name"))
    assert.is_false(f("Cl0"))
    assert.is_false(f("UR0"))
    assert.is_false(f("0ClassName"))
    assert.is_false(f("$Inner"))
    assert.is_false(f("C0"))
    assert.is_false(f("lC0"))
    assert.is_false(f("var"))
    assert.is_false(f("varName"))
    assert.is_false(f("0"))
    assert.is_false(f("X"))
    assert.is_false(f(""))
    assert.is_false(f("C_A"))
    assert.is_false(f("CONST0"))
    assert.is_false(f("CONST"))
    assert.is_false(f("_Strange"))
  end)

  it("isFullClassNameWithPackage", function()
    local f = M.isFullClassNameWithPackage
    assert.same(true, f('org.Class'))
    assert.same(false, f('Class'))
    assert.same(false, f('App.Class'))
    assert.same(true, f('org.me.Class'))
    assert.same(false, f('org..me.Class'))
    assert.same(false, f('.org.Class'))
    assert.same(false, f('.org.Parent.Inner'))
    assert.same(true, f('org.Parent.Inner'))
    assert.same(true, f('org.ok.Parent.Inner'))
    assert.same(true, f('org.Parent.not.Inner')) -- todo fix
  end)

  it("isStartsWithPackage", function()
    local f = M.isStartsWithPackage
    assert.is_true(f("io.pkg"))
    assert.is_true(f("io.pkg.app"))
    assert.is_true(f("io.pkg.app."))
    assert.is_true(f("io.pkg.Class"))
    assert.is_false(f(".a"))
    assert.is_false(f(".A"))
    assert.is_false(f("a.0"))
    assert.is_false(f("A.B"))
    assert.is_false(f("pkg.Class"))       --?
    assert.is_false(f(" pkg.comp.Class")) --?
  end)

  it("getPackage", function()
    local f = M.getPackage
    assert.same('some.org', f("some.org.Class"))
    assert.same('org', f("org.Class"))
    assert.is_nil(f("Class"))
  end)


  it("isEmptyOrComment", function()
    local f = M.isEmptyOrComment
    assert.same(false, f("logn l = 9;"))
    assert.same(true, f(""))
    assert.same(true, f("//"))
    assert.same(true, f(" // "))
    assert.same(true, f(" // this is comment"))
    assert.same(true, f("/* one line comment*/"))
    -- ignore
    assert.same(false, f(" * this is comment to - part of multiline"))
    -- assert.same(true, f(" * this is comment to - part of multiline"))
    -- assert.same(true, f("*"))
    -- assert.same(true, f(" /* start of multiline"))
    -- assert.same(true, f(" /** start of multiline"))
    -- assert.same(true, f("/** start of multiline"))
    assert.same(true, f(""))
  end)

  it("isKeyWord", function()
    local f = M.isKeyWord
    assert.same(true, f("new"))
    assert.same(false, f("not_a_keyword"))
  end)

  it("isOperator", function()
    local f = M.isOperator
    assert.same(true, f("="))
    assert.same(true, f("<"))
  end)

  it("match", function()
    local path = "/tmp/build/classes/java/main/org/comp/sub/MainClass.class"
    local name = string.match(path, "[/\\]([^/\\]+).class$")
    assert.same('MainClass', name)
  end)

  it("find_mapping_starts_with", function()
    local f = M.find_mapping_starts_with
    local fcn = 'org.myapp.handlers.SomeHandler'
    local mapping = {
      ['org.myapp'] = 'org.myapp.subpkg',
      ['org.myapp.Class'] = 'org.myapp.subpkg.Class'
    }
    local exp = 'org.myapp' -- key in the mapping
    assert.same(exp, f(fcn, mapping))
  end)

  it("find_mapping_starts_with", function()
    local f = M.find_mapping_starts_with
    local mapping = {
      ['org.myapp.sub.Class'] = 'org.myapp.subpkg.Class',
      ['org.junit.Test'] = 'org.junit.jupiter.api.Test',
      ['org.junit.Assert'] = 'org.junit.jupiter.api.Assertions',
    }
    local exp2 = 'org.myapp.sub.Class'
    assert.same(exp2, f('org.myapp.sub.Class', mapping))
    -- add 'org.myapp' and  'org.myapp.sub.Class' will be broken!
    -- create ordered list with long-to-short ordering ?
  end)

  it("parse_import_mapping", function()
    local f = M.parse_import_mapping
    -- old name // new name
    local line = 'import org.common.MyClass; // org.mylib.app.MyClass'

    local exp = { 'org.common.MyClass', 'org.mylib.app.MyClass' }
    assert.same(exp, { f(line) })

    line = 'import org.common.MyClass; // import org.mylib.app.MyClass;'
    assert.same(exp, { f(line) })

    -- need to discard the method and take only the name of the class
    line = 'import static org.old.A.meth; // org.new.A'
    local exp2 = { 'org.old.A', 'org.new.A' }
    assert.same(exp2, { f(line) })

    line = 'import static org.old.A.meth; // import static org.new.A.meth;'
    assert.same(exp2, { f(line) })
  end)
end)

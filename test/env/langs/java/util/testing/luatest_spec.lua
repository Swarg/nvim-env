-- 07-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.testing.luatest'

describe("env.langs.java.util.testing.luatest", function()
  it("add_webapp_n_db_template", function()
    local f = M.add_webapp_n_db_template
    local opts = { no_date = true, author = 'A' }
    local path, body = f('/home/me/dev/proj', opts)
    assert.same('/home/me/dev/proj/src/test/_lua/webapp_spec.lua', path)
    local exp = [[
--  @author A
-- Start the webapp before run this test
--
-- dependencies:
--   - luarocks: busted, lhttp, db-env

require "busted.runner" ()
local assert = require 'luassert'

local H = require 'lhttp.html.helper'
local DB = require 'db_env.helper'

local hostaddr = H.localhost8080
local dbconf = ./src/main/resources/application.properties

-- webapp endpoints
local ep = {
  people = '/people',
  people_id = '/people/{id}',
}

H.check_webapp_health(hostaddr, ${CHECK_HEALTH_ENDPOINT}, {
  msg = use 'make clean-build run' before run this test
})

local client ---@cast client lhttp.Client

describe("rest-api", function()
  setup(function()
    client = H.newClient(hostaddr, H.UserAgent.Linux.Firefox, H.CT.APP_JSON)
    client:addResponseFilter(H.jsonFilterMaskTimestamp)
    -- db connection
    DB.connect_by_config(dbconf)
    DB.refresh_db_state({ __scripts_dir = "./sql/" })
  end)

  before_each(function()
    client:newRequest()
  end)

  teardown(function()
    DB.disconnect()
  end)

  it("/people/{id}", function()
    local exp = { username = 'Jonny', email = 'jonny@mail.com' }
    assert.same(exp, client:getJsonO(ep.people_id, { id = 4 }))
  end)

end)
]]
    assert.same(exp, body)
  end)
end)

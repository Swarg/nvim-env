-- 05-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.spring.resolver'

describe("env.langs.java.util.spring.resolver", function()
  local proot = "/home/dev/proj/"
  local contrpath = proot .. "src/main/java/pkg/app/controller/MController.java"

  it("get_templ_dir", function()
    local exp = '/home/dev/proj/src/main/resources/templates'
    assert.same(exp, M.get_templ_dir_from(contrpath))
  end)

  it("resolveSourceFrom", function()
    local f = M.resolveSourceFrom

    local line = '[THYMELEAF][http-nio-8080-exec-2] ' ..
        'Exception processing template "auth/login": ' ..
        'Exception evaluating SpringEL expression: ' ..
        '"_csrf.parameterName" (template: "auth/login" - line 15, col 28)'
    local exp = { '/home/dev/proj/templates/auth/login.html', '15' }
    assert.same(exp, { f(line, proot) })
  end)
end)

-- 20-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.spring.generator'

describe("env.langs.java.util.spring.generator", function()
  local proot = "/home/dev/proj/"
  local contrpath = proot .. "src/main/java/pkg/app/controller/MController.java"
  local def_templ_body = M.get_templ_body()

  it("get_path_variables", function()
    local f = M.get_path_variables
    assert.same({ 'user', 'post' }, f("/path/{user}/subpath/{post}"))
    assert.same({}, f("/path/subpath/post"))
  end)

  it("new_endpoint empty endpoint(inherited)", function()
    local exp = {
      code = {
        '    @GetMapping',
        '    public String userList(Model model) {',
        '        model.addAttribute("", );',
        '        return "userList";',
        '    }'
      },
      templ_body = def_templ_body,
      templ_path = '/home/dev/proj/src/main/resources/templates/userList.html'
    }
    assert.same(exp, M.new_endpoint("get", ".", "userList", contrpath))
  end)

  it("new_endpoint empty endpoint(inherited)", function()
    local exp = {
      code = {
        '    @GetMapping("{user}")',
        '    public String userEdit(@PathVariable User user, Model model) {',
        '        model.addAttribute("user", user);',
        '        return "userEdit";',
        '    }'
      },
      templ_body = def_templ_body,
      templ_path = '/home/dev/proj/src/main/resources/templates/userEdit.html'
    }
    assert.same(exp, M.new_endpoint("get", "{user}", "userEdit", contrpath))
  end)

  it("new_endpoint", function()
    local exp = { false, '<http_method> <endpoint> <templ_name>' }
    assert.same(exp, { M.new_endpoint("help", "", "", "") })
  end)


  it("new_bean", function()
    local exp = {
      code = {
        '    @Bean',
        '    public RestTemplate restTemplate() {',
        '        return new RestTemplate();',
        '    }'
      }
    }
    assert.same(exp, M.new_bean("RestTemplate"))
  end)

  it("new_bean with builder", function()
    local exp = {
      code = {
        '    @Bean',
        '    public RestTemplate restTemplate(RestTemplateBuilder builder) {',
        '        return builder.build();',
        '    }'
      }
    }
    assert.same(exp, M.new_bean("RestTemplate", { params = { 'builder' } }))
  end)

  it("new_value_property_field", function()
    local exp = {
      '    @Value("${app.prod.url}")',
      '    private String appProdUrl;'
    }
    assert.same(exp, M.new_value_property_field("app.prod.url"))

    local exp2 = {
      '    @Value("${url}")',
      '    private String url;'
    }
    assert.same(exp2, M.new_value_property_field("url"))
  end)
end)

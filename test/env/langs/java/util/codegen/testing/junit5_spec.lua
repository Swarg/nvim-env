-- 30-01-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.util.codegen.testing.junit5'

describe("env.langs.java.util.codegen.testing.junit5", function()
  it("gen_given_when_then_blocks", function()
    local method = {
      mods = 9, -- static
      typ = "PrintStream",
      name = "getNew",
      params = {},
      ln = 16,
      lne = 29,
    }
    local clazz = {
      pkg = "org.swarg.common",
      mods = 1,
      typ = "class",
      name = "InMemPrintStream",
      pkg_ln = 1,
      imports = {
        "java.io.ByteArrayOutputStream",
        "java.io.FilterOutputStream",
        "java.io.OutputStream",
        "java.io.PrintStream",
        "java.nio.charset.StandardCharsets",
        "java.lang.reflect.Field"
      },
      ln = 14,
      import_lns = { 3, 4, 5, 6, 7, 8 },
      methods = { method },
    }
    local opts = { tag = '    ' }
    local exp = {
      '',
      '    PrintStream printStream = InMemPrintStream.getNew();',
      '    assertEquals(null, printStream);'
    }
    assert.same(exp, { M.gen_given_when_then_blocks(clazz, method, opts) })
  end)

  it("gen_given_when_then_blocks", function()
    local clazz = {
      pkg = "org.swarg.common",
      typ = "class",
      mods = 1,
      name = "InMemPrintStream",
      pkg_ln = 1,
      import_lns = { 3, 4, 5, 6, 7, 8 },
      imports = {
        "java.io.ByteArrayOutputStream",
        "java.io.FilterOutputStream", "java.io.OutputStream", "java.io.PrintStream",
        "java.nio.charset.StandardCharsets", "java.lang.reflect.Field"
      },
      ln = 14,
      methods = {
        {
          ln = 19,
          lne = 32,
          mods = 9,
          name = "getNew",
          params = {},
          typ = "PrintStream"
        },
        {
          ln = 34,
          lne = 52,
          mods = 9,
          name = "pullAll",
          params = { { "PrintStream", "out" }, { 'String', 'str' } },
          typ = "String"
        }
      },
    }
    local method = clazz.methods[2]
    local exp = {
      "    PrintStream out = null;\n    String str = null;",
      '    String string = InMemPrintStream.pullAll(out, str);',
      '    assertEquals(null, string);'
    }
    assert.same(exp, { M.gen_given_when_then_blocks(clazz, method, {}) })
  end)
end)

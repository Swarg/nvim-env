-- 23-01-2025 @author app
require("busted.runner")()
local assert = require("luassert")

local M = require 'env.langs.java.util.codegen.generator'

describe("env.langs.java.util.codegen.generator", function()
  it("gen_value_class", function()
    local clazz = {
      pkg = "org.app.dto",
      mods = 1,
      name = "UserDto",
      typ = "class",
      imports = {
        "org.app.entity.Gender",
        "org.app.entity.Role",
        "java.time.LocalDate"
      },
      fields = {
        { ln = 11, mods = 0, name = "id",       typ = "Integer" },
        { ln = 12, mods = 0, name = "name",     typ = "String" },
        { ln = 13, mods = 0, name = "birthday", typ = "LocalDate" },
        { ln = 14, mods = 0, name = "email",    typ = "String" },
        { ln = 15, mods = 0, name = "image",    typ = "String" },
        { ln = 16, mods = 0, name = "role",     typ = "Role" },
        { ln = 17, mods = 0, name = "gender",   typ = "Gender" }
      },
      ln = 10,
      lne = 19,
    }
    local res = M.gen_value_class(clazz)
    local exp = {
      'package org.app.dto;',
      '',
      'import org.app.entity.Gender;',
      'import org.app.entity.Role;',
      'import java.time.LocalDate;',
      'import java.beans.ConstructorProperties;',
      'import java.util.Objects;',
      '',
      'public final class UserDto {',
      '    private final Integer id;',
      '    private final String name;',
      '    private final LocalDate birthday;',
      '    private final String email;',
      '    private final String image;',
      '    private final Role role;',
      '    private final Gender gender;',
      '',
      '    @ConstructorProperties({',
      '        "id", "name", "birthday", "email", "image", "role", "gender"',
      '    })',
      '    public UserDto (Integer id, String name, LocalDate birthday, String email,',
      '            String image, Role role, Gender gender) {',
      '        this.id = id;',
      '        this.name = name;',
      '        this.birthday = birthday;',
      '        this.email = email;',
      '        this.image = image;',
      '        this.role = role;',
      '        this.gender = gender;',
      '    }',
      '',
      '    public Integer getId() {',
      '        return this.id;',
      '    }',
      '',
      '    public String getName() {',
      '        return this.name;',
      '    }',
      '',
      '    public LocalDate getBirthday() {',
      '        return this.birthday;',
      '    }',
      '',
      '    public String getEmail() {',
      '        return this.email;',
      '    }',
      '',
      '    public String getImage() {',
      '        return this.image;',
      '    }',
      '',
      '    public Role getRole() {',
      '        return this.role;',
      '    }',
      '',
      '    public Gender getGender() {',
      '        return this.gender;',
      '    }',
      '',
      '    @Override',
      '    public int hashCode() {',
      '        final int PRIME = 31;',
      '        int result = 17;',
      '        result = PRIME * result + Objects.hashCode(this.id);',
      '        result = PRIME * result + Objects.hashCode(this.name);',
      '        result = PRIME * result + Objects.hashCode(this.birthday);',
      '        result = PRIME * result + Objects.hashCode(this.email);',
      '        result = PRIME * result + Objects.hashCode(this.image);',
      '        result = PRIME * result + Objects.hashCode(this.role);',
      '        result = PRIME * result + Objects.hashCode(this.gender);',
      '        return result;',
      '    }',
      '',
      '    @Override',
      '    public boolean equals(Object obj) {',
      '        if (this == obj) return true;',
      '        if (obj == null || getClass() != obj.getClass()) return false;',
      '        UserDto other = (UserDto) obj;',
      '        Integer tId = this.getId();',
      '        Integer oId = other.getId();',
      '        if (tId == null ? oId!= null : !tId.equals(oId)) return false;',
      '        String tName = this.getName();',
      '        String oName = other.getName();',
      '        if (tName == null ? oName!= null : !tName.equals(oName)) return false;',
      '        LocalDate tBirthday = this.getBirthday();',
      '        LocalDate oBirthday = other.getBirthday();',
      '        if (tBirthday == null ? oBirthday!= null : !tBirthday.equals(oBirthday)) return false;',
      '        String tEmail = this.getEmail();',
      '        String oEmail = other.getEmail();',
      '        if (tEmail == null ? oEmail!= null : !tEmail.equals(oEmail)) return false;',
      '        String tImage = this.getImage();',
      '        String oImage = other.getImage();',
      '        if (tImage == null ? oImage!= null : !tImage.equals(oImage)) return false;',
      '        Role tRole = this.getRole();',
      '        Role oRole = other.getRole();',
      '        if (tRole == null ? oRole!= null : !tRole.equals(oRole)) return false;',
      '        Gender tGender = this.getGender();',
      '        Gender oGender = other.getGender();',
      '        if (tGender == null ? oGender!= null : !tGender.equals(oGender)) return false;',
      '        return true;',
      '    }',
      '',
      '    @Override',
      '    public String toString() {',
      '        return "UserDto(id=" + getId() + ", name=" + getName()',
      '                 + ", email=" + getEmail() + ", image=" + getImage()',
      '                 + ", gender=" + getGender() + ")";',
      '    }',
      '}'
    }
    assert.same(exp, res)
  end)

  it("gen_tostring one field", function()
    local clazz = {
      pkg = "org.app.dto",
      mods = 1,
      name = "UserDto",
      typ = "class",
      imports = {},
      fields = {
        { ln = 12, mods = 0, name = "name", typ = "String" },
      },
      ln = 10,
      lne = 19,
    }
    local t = {}
    M.gen_tostring(clazz, t, {})
    local exp = {
      '    @Override',
      '    public String toString() {',
      '        return "UserDto(name=" + getName() + ")";',
      '    }'
    }
    assert.same(exp, t)
  end)

  it("gen_tostring two fields", function()
    local clazz = {
      pkg = "org.app.dto",
      mods = 1,
      name = "UserDto",
      typ = "class",
      imports = {},
      fields = {
        { ln = 11, mods = 0, name = "id",   typ = "Integer" },
        { ln = 12, mods = 0, name = "name", typ = "String" },
      },
      ln = 10,
      lne = 19,
    }
    local t = {}
    M.gen_tostring(clazz, t, {})
    local exp = {
      '    @Override',
      '    public String toString() {',
      '        return "UserDto(id=" + getId() + ", name=" + getName() + ")";',
      '    }'
    }
    assert.same(exp, t)
  end)

  it("gen_setters", function()
    local clazz = {
      pkg = "org.app.dto",
      mods = 1,
      name = "UserDto",
      typ = "class",
      imports = {
        "org.app.entity.Gender",
        "org.app.entity.Role",
        "java.time.LocalDate"
      },
      fields = {
        { ln = 11, mods = 0, name = "id",       typ = "Integer" },
        { ln = 12, mods = 0, name = "name",     typ = "String" },
        { ln = 13, mods = 0, name = "birthday", typ = "LocalDate" },
        { ln = 14, mods = 0, name = "email",    typ = "String" },
        { ln = 15, mods = 0, name = "image",    typ = "String" },
        { ln = 16, mods = 0, name = "role",     typ = "Role" },
        { ln = 17, mods = 0, name = "gender",   typ = "Gender" }
      },
      ln = 10,
      lne = 19,
    }
    local t = {}
    M.gen_setters(clazz, t, {})
    local exp = {
      '    public void setId(Integer id) {',
      '        this.id = id;',
      '    }',
      '',
      '    public void setName(String name) {',
      '        this.name = name;',
      '    }',
      '',
      '    public void setBirthday(LocalDate birthday) {',
      '        this.birthday = birthday;',
      '    }',
      '',
      '    public void setEmail(String email) {',
      '        this.email = email;',
      '    }',
      '',
      '    public void setImage(String image) {',
      '        this.image = image;',
      '    }',
      '',
      '    public void setRole(Role role) {',
      '        this.role = role;',
      '    }',
      '',
      '    public void setGender(Gender gender) {',
      '        this.gender = gender;',
      '    }',
      ''
    }
    assert.same(exp, t)
  end)
end)

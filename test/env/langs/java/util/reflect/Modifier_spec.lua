-- 08-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.reflect.Modifier'

describe("env.langs.java.util.reflect.Modifier converter from tokens", function()

  it("modifiers to string", function()
    local f = M.mods2str
    assert.same('', f(0))
    assert.same('public', f(1))
    assert.same('private', f(2))
    assert.same('protected', f(4))
    assert.same('public static', f(11))
    assert.same('public static final', f(0x01 + 0x10 + 0x08))
  end)
end)

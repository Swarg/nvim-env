-- 21-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local D, NVim, nvim, sys

-- true to check emulated javac set false to test real inastalled javac
if 0 == 0 then -- to check via stub.os.bin.javac (emulation)
  NVim = require 'stub.vim.NVim'
  nvim = NVim:new() ---@type stub.vim.NVim
  sys = nvim:get_os()
  sys = sys

  D = require 'dprint'
  --
  D.enable_module('stub.os.*', true)
  D.enable_module('stub.vim.*', true)
  D.on = false
end
local SKIP0 = 1


local fs = require 'env.files'
local su = require 'env.sutil'
-- local uv = require 'luv'
-- local spawner = require 'env.spawner'

-- checking and researching how the javac compiler behaves
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local root_dir = "/tmp/nvim-env/javac/"
-- helper
---@param name string
---@param content string
---@return boolean
local function mk_file(name, content)
  assert(type(name) == 'string', 'name')
  assert(type(content) == 'string', 'content')
  local path = fs.build_path(root_dir, name)
  if string.find(name, fs.path_sep) then
    local dir = fs.get_parent_dirpath(path)
    if dir then
      fs.mkdir(dir)
    end
  end
  if sys then -- emulated
    return sys:set_file_content(path, content)
  else        -- real
    return fs.str2file(path, content, "wb")
  end
end

local function normalize_sys_tree(lines)
  local chars = { '└', '─', '├', '│', string.char(0xc2, 0xa0) } -- hiden space
  lines = su.replace_chars_in_lines(lines, chars, " ")
  local t = {}
  for i, line in ipairs(lines or E) do
    if i == 1 and string.find(line, '/') then
      lines = fs.ensure_dir(line)
    end
    if line ~= '' then
      t[#t + 1] = line
    end
  end
  return t
end

local function tree(...)
  local dir = fs.join_path(root_dir, ...)
  local list
  if sys then
    list = sys:treel(dir, { no_trailing_slashes = 1 })
  else
    list = normalize_sys_tree(fs.execrl("tree " .. dir))
    -- print("[DEBUG] list[3]:", require "inspect" (list[3]), su.tohex(list[3]))
  end
  return list
end
--
local function javac(argsline) -- redirect stderr to stdout 2>&1
  -- return fs.execrl("cd " .. root_dir .. " && pwd") -- == spawner run + cwd
  return fs.execrl("cd " .. root_dir .. " && javac " .. argsline .. " 2>&1")
  -- spawner.run({
  --   cwd = "", cmd = "", args = {}, handler_on_close = func() end
  -- }
end

describe("env.langs.java.util.javac", function()
  before_each(function()
    -- cleanup
    if nvim and sys then
      D.on = false
      nvim:clear_state()
      sys:clear_state()
      sys.fs:clear_state()
    else
      os.execute("rm -r /tmp/nvim-env/javac")
    end
    -- prepare
    assert.same(true, fs.mkdir(root_dir))
  end)

  local class_org_swarg_Main = "package org.ns;\n public class Main {\n}\n"


  it("compile clashes with class", function()
    if SKIP0 == 0 then return end -- +
    assert.same(true, mk_file('src/org/ns/Main.java', [[
package org.ns.Main;                                  // bad << package
public class Main {
}
  ]]))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        org',
      '            ns',
      '                Main.java',
      '3 directories, 1 file'
    }
    assert.same(exp_before, tree())
    local exp = {
      'src/org/ns/Main.java:1: error: package org.ns.Main clashes with class of same name',
      'package org.ns.Main;                                  // bad << package',
      '^',
      '1 error'
    }

    assert.same(exp, javac("-sourcepath src -d build/classes src/org/ns/Main.java"))
    local exp_after = exp_before
    assert.same(exp_after, tree())
  end)


  it("compile success -sourcepath", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('src/org/ns/Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        org',
      '            ns',
      '                Main.java',
      '3 directories, 1 file'
    }
    assert.same(exp_before, tree())

    -- require'dprint'.enable()
    assert.same({}, javac("-sourcepath src -d build/classes src/org/ns/Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        classes',
      '            org',
      '                ns',
      '                    Main.class',
      '    src',
      '        org',
      '            ns',
      '                Main.java',
      '7 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success NO -sourcepath", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('src/org/ns/Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        org',
      '            ns',
      '                Main.java',
      '3 directories, 1 file'
    }
    assert.same(exp_before, tree())

    assert.same({}, javac("-d build/classes src/org/ns/Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        classes',
      '            org',
      '                ns',
      '                    Main.class',
      '    src',
      '        org',
      '            ns',
      '                Main.java',
      '7 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success NO -sourcepath", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('src/main/java/org/ns/Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        main',
      '            java',
      '                org',
      '                    ns',
      '                        Main.java',
      '5 directories, 1 file'
    }
    assert.same(exp_before, tree())

    assert.same({}, javac("-d build/classes src/main/java/org/ns/Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        classes',
      '            org',
      '                ns',
      '                    Main.class',
      '    src',
      '        main',
      '            java',
      '                org',
      '                    ns',
      '                        Main.java',
      '9 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success full path to class in not cwd", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('/tmp/org/ns/Main.java', class_org_swarg_Main))
    local exp_before = { '/tmp/nvim-env/javac/', '0 directories, 0 files' }
    assert.same(exp_before, tree())
    assert.same({}, javac("-d build/classes /tmp/org/ns/Main.java"))
    os.remove("/tmp/org/ns/Main.java")

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        classes',
      '            org',
      '                ns',
      '                    Main.class',
      '4 directories, 1 file'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    Main.java',
      '0 directories, 1 file'
    }
    assert.same(exp_before, tree())
    assert.same({}, javac("-d build/classes Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        classes',
      '            org',
      '                ns',
      '                    Main.class',
      '    Main.java',
      '4 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    Main.java',
      '0 directories, 1 file'
    }
    assert.same(exp_before, tree())
    assert.same({}, javac("Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    Main.class',
      '    Main.java',
      '0 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('src/Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        Main.java',
      '1 directory, 1 file'
    }
    assert.same(exp_before, tree())
    assert.same({}, javac("src/Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        Main.class',
      '        Main.java',
      '1 directory, 2 files'
    }
    assert.same(exp_after, tree())
  end)


  it("compile success", function()
    if SKIP0 == 0 then return end -- ++
    assert.same(true, mk_file('src/Main.java', class_org_swarg_Main))
    local exp_before = {
      '/tmp/nvim-env/javac/',
      '    src',
      '        Main.java',
      '1 directory, 1 file'
    }
    assert.same(exp_before, tree())
    local exp = {}
    assert.same(exp, javac("-d build src/Main.java"))

    local exp_after = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        org',
      '            ns',
      '                Main.class',
      '    src',
      '        Main.java',
      '4 directories, 2 files'
    }
    assert.same(exp_after, tree())
  end)
end)

-- 05-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.maven.gen'

describe("env.langs.java.util.maven.gen", function()
  it("generate_dependency", function()
    local f = M.generate_dependency
    local gav = { groupId = 'junit', artifactId = 'junit', version = '4.13.2' }
    local exp = [[
    <dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
      <version>4.13.2</version>
      <scope>test</scope>
		</dependency>]]
    assert.same(exp, f(gav, 'test'))
  end)

  it("dep_to_gralde", function()
    local f = M.dep_to_gralde
    local dep = {
      artifactId = 'spring-boot-starter-data-jpa',
      groupId = 'org.springframework.boot'
    }
    local exp = 'implementation org.springframework.boot:spring-boot-starter-data-jpa'
    assert.same(exp, f(dep))
  end)

  it("dep_to_luamap", function()
    local dep = {
      artifactId = 'spring-boot-starter-data-jpa',
      groupId = 'org.springframework.boot'
    }
    local exp = [[
m['spring-boot-starter-data-jpa'] = {
  groupId = 'org.springframework.boot',
  artifactId = 'spring-boot-starter-data-jpa',
}
]]
    assert.same(exp, M.dep_to_luamap(dep))
  end)

  it("gen_min_pom_xml", function()
    local f = M.gen_min_pom_xml
    local exp = M.POM_HEAD.. [[
  <groupId>group</groupId>
  <artifactId>artifact</artifactId>
  <version>1.0</version>
  <packaging>jar</packaging>
</project>
]]
    assert.same(exp, f(nil))
  end)
end)

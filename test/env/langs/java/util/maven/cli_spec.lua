-- 03-10-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.maven.cli'

describe("env.langs.java.util.maven.cli", function()
  it("is_valid_artifact_name", function()
    local f = M.is_valid_artifact_name
    assert.same(true, f('org.slf4j:slf4j-simple:2.0.16'))
    assert.same(false, f('org.slf4j:slf4j-simple:'))
    assert.same(false, f(':slf4j-simple:2.0.16'))
    assert.same(false, f('org.flywaydb:flywy-core', true))
    assert.same(true, f('org.flywaydb:flywy-core', false))
  end)

  it("parse_artifact_files", function()
    local files = {
      "_remote.repositories",
      "slf4j-simple-2.0.16.pom", "slf4j-simple-2.0.16.pom.sha1",
      "slf4j-simple-2.0.16.jar", "slf4j-simple-2.0.16.jar.sha1",
      "slf4j-simple-2.0.16-sources.jar", "slf4j-simple-2.0.16-sources.jar.sha1",
      "slf4j-simple-2.0.16-javadoc.jar", "slf4j-simple-2.0.16-javadoc.jar.sha1",
    }

    local gav = {
      groupId = 'org.slf4j', artifactId = 'slf4j-simple', version = '2.0.16'
    }
    local exp = {
      pom = 'slf4j-simple-2.0.16.pom',
      jar = 'slf4j-simple-2.0.16.jar',
      javadoc = 'slf4j-simple-2.0.16-javadoc.jar',
      sources = 'slf4j-simple-2.0.16-sources.jar'
    }
    assert.same(exp, M.parse_artifact_files(gav, files))
  end)
end)

-- 28-07-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
local M = require 'env.langs.java.util.maven.parser'

describe("env.langs.java.util.maven.parser", function()
  -- maven output on `mvn -X compile`
  local JAVAC_OPTS = "-d ~/java/webapp/target/classes " ..
      "-classpath ~/java/webapp/target/classes:" ..
      "~/.m2/repository/javax/servlet/javax.servlet-api/4.0.1/javax.servlet-api-4.0.1.jar:" ..
      "~/.m2/repository/org/springframework/spring-core/5.3.31/spring-core-5.3.31.jar:" ..
      "~/.m2/repository/org/postgresql/postgresql/42.7.3/postgresql-42.7.3.jar:" ..
      " -sourcepath ~/java/webapp/src/main/java:" ..
      "~/java/webapp/target/generated-sources/annotations:" ..
      " -s ~/java/webapp/target/generated-sources/annotations" ..
      " -g -nowarn -target 1.8 -source 1.8 -encoding UTF-8"
  local lines = {
    "Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)",
    "Maven home: /opt/maven",
    "Java version: 17.0.11, vendor: X runtime: /usr/lib/jvm/jdk-17",
    "Default locale: en_US, platform encoding: UTF-8",
    -- ...
    "[DEBUG] Created new class realm maven.api",
    "[DEBUG] Importing foreign packages into class realm maven.api",
    "[DEBUG]   Imported: javax.annotation.* < plexus.core",
    "[DEBUG]   Imported: javax.annotation.security.* < plexus.core",
    "[DEBUG]   Imported: javax.inject.* < plexus.core",
    "[DEBUG] Source roots:",
    "[DEBUG]  ~/java/webapp/src/main/java",
    "[DEBUG]  ~/java/webapp/target/generated-sources/annotations",
    "[DEBUG] Command line options:",
    "[DEBUG] " .. JAVAC_OPTS,
    "[DEBUG] incrementalBuildHelper#beforeRebuildExecution",
    "[INFO] Compiling 13 source files to ~/java/webapp/target/classes",
    "[DEBUG] incrementalBuildHelper#afterRebuildExecution",
    "[INFO] ------------------------------------------------------------------------",
    "[INFO] BUILD SUCCESS",
    "[INFO] ------------------------------------------------------------------------"
  }
  it("parse_compile_output", function()
    local f = M.parse_compile_output
    local exp = {
      javac_opts = JAVAC_OPTS,
      src_roots = {
        '~/java/webapp/src/main/java',
        '~/java/webapp/target/generated-sources/annotations',
      }
    }
    assert.same(exp, f(lines))
  end)


  it("unwrapp_in_tag", function()
    local f = M.unwrapp_in_tag

    local line = "<id>maven-tomcat-war-deployme:e nt-server</id>"
    local exp = { 'id', 'maven-tomcat-war-deployme:e nt-server', 'id' }
    assert.same(exp, { f(line) })
  end)

  -- it("get_server_props", function()
  --   local f = M.get_server_props
  --   local exp = { user = 'war-deployer', password = 'secret' }
  --   assert.same(exp, f("/opt/maven/conf/settings.xml", "maven-tomcat-war-deployment-server"))
  -- end)

  it("parse_artifact_coords", function()
    local f = M.parse_artifact_coords
    local exp = { groupId = 'junit', artifactId = 'junit', version = '4.13.2' }
    assert.same(exp, f('junit:junit:4.13.2'))

    exp = {
      groupId = 'org.slf4j', artifactId = 'slf4j-simple', version = '2.0.16'
    }
    assert.same(exp, f('org.slf4j:slf4j-simple:2.0.16'))

    exp = {
      groupId = 'org.junit.jupiter', artifactId = 'junit-jupiter', version = '5.10.3'
    }
    assert.same(exp, f('org.junit.jupiter:junit-jupiter:5.10.3'))
  end)

  it("parse_artifact_coords", function()
    local f = M.parse_artifact_coords
    local exp = { groupId = 'org.flywaydb', artifactId = 'flywy-core' }
    assert.same(exp, f('org.flywaydb:flywy-core'))
  end)

  it("parse_artifact_with_classifier", function()
    local f = M.parse_artifact_with_classifier
    local exp = {
      groupId = 'org.slf4j',
      artifactId = 'slf4j-simple',
      version = '2.0.16',
      classifier = 'sources'
    }
    assert.same(exp, f('org.slf4j:slf4j-simple:2.0.16:sources'))
  end)

  it("parse_dependencies_lines", function()
    local lines0 = {
      '	<dependencies>',
      '		<dependency>',
      '			<groupId>org.springframework.boot</groupId>',
      '			<artifactId>spring-boot-starter-data-jpa</artifactId>',
      '		</dependency>',
      '		<dependency>',
      '			<groupId>org.springframework.boot</groupId>',
      '			<artifactId>spring-boot-starter-security</artifactId>',
      '		</dependency>',
      '</dependencies>',
    }
    local exp = {
      {
        artifactId = 'spring-boot-starter-data-jpa',
        groupId = 'org.springframework.boot'
      },
      {
        artifactId = 'spring-boot-starter-security',
        groupId = 'org.springframework.boot'
      }
    }
    assert.same(exp, M.parse_dependencies_lines(lines0))
  end)

  it("parse_dependencies_lines", function()
    local lines0 = {
      '			<groupId>org.springframework.boot</groupId>',
      '			<artifactId>spring-boot-starter-data-jpa</artifactId>',
    }
    local exp = {
      {
        artifactId = 'spring-boot-starter-data-jpa',
        groupId = 'org.springframework.boot'
      }
    }
    assert.same(exp, M.parse_dependencies_lines(lines0))
  end)
end)

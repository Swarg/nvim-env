-- 20-07-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
local M = require 'env.langs.java.util.maven.init_project'


describe("env.langs.java.util.maven.init_project", function()
  it("gen_cmd_archetype_generate", function()
    local t = {
      ARCHETYPE_ARTIFACT = "maven-archetype-quickstart",
      ARCHETYPE_GROUP = "org.apache.maven.archetypes",
      ARCHETYPE_VERSION = "1.4",
      ARTIFACT_ID = "app",
      CREATE_PROJECT = "yes",
      DEPENDENCIES = "",
      GROUP_ID = "pkg",
    }
    local exp = {
      'archetype:generate',
      '-X',
      '-DarchetypeCatalog=local',
      '-DarchetypeGroupId=org.apache.maven.archetypes',
      '-DarchetypeArtifactId=maven-archetype-quickstart',
      '-DarchetypeVersion=1.4',
      '-DgroupId=pkg',
      '-DartifactId=app',
      '-Dversion=1.0-SNAPSHOT',
      '-DinteractiveMode=false'
    }
    assert.same(exp, M.gen_args_archetype_generate(t))
  end)

  it("gen_cmd_archetype_generate", function()
    local t = {
      ARTIFACT_ID = "app",
      CREATE_PROJECT = "yes",
      DEPENDENCIES = "",
      GROUP_ID = "pkg",
      VERSION = "0.1.0"
    }
    local exp = {
      'archetype:generate',
      '-X',
      '-DarchetypeCatalog=local',
      '-DarchetypeGroupId=org.apache.maven.archetypes',
      '-DarchetypeArtifactId=maven-archetype-quickstart',
      '-DarchetypeVersion=1.4',
      '-DgroupId=pkg',
      '-DartifactId=app',
      '-Dversion=0.1.0',
      '-DinteractiveMode=false'
    }
    assert.same(exp, M.gen_args_archetype_generate(t))
  end)

  it("find_generated_proj_dir", function()
    local f = M.find_generated_proj_dir
    local output = [[

[INFO] Project created from Archetype in dir: /dev/new/project
[INFO] ------------------------------------------------------------------------
    ]]
    local exp = '/dev/new/project'
    assert.same(exp, f(output))
  end)
end)

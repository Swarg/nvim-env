-- 13-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.syntax'
local Modifier = M.Modifier

describe("env.langs.java.util.syntax", function()
  it("token_to_class_entry", function()
    local token = { "CLASSDEF", "public", "class", "AuthController", "{" }

    local exp = { mods = 1, name = 'AuthController', typ = 'class', ln = 1 }
    assert.same(exp, M.token_to_class_entry(token, 1, {}))
    assert.same(true, Modifier.isPublic(exp.mods))
  end)

  it("token_to_class_entry with extends and implements", function()
    local token = {
      'CLASSDEF', 'public', 'class', 'B',
      { 'EXTENDS',    'MyList' },
      { 'IMPLEMENTS', { 'Iterable', 'Collectable' } },
      39
    }
    local exp = {
      mods = 1,
      name = 'B',
      typ = 'class',
      extends = 'MyList',
      implements = { 'Iterable', 'Collectable' },
      ln = 1,
      continue = 39,
    }
    assert.same(exp, M.token_to_class_entry(token, 1, {}))
  end)


  it("token_to_constructor_entry only open", function()
    local token = { "CONSTRUCTOR", "public", "A", 9 }
    local e = M.token_to_constructor_entry(token, 1, {})
    assert.same({ mods = 1, name = 'A', ln = 1, continue = 9 }, e)
  end)

  it("token_to_constructor_entry only open", function()
    local token = { "CONSTRUCTOR", "public", "A", {}, 9 }
    assert.match_error(function()
      M.token_to_constructor_entry(token, 1, {})
    end, 'expected token kind: SIGNPARAMS got:nil')
  end)


  it("token_to_constructor_entry empty params", function()
    local token = {
      "CONSTRUCTOR", "public", "A",
      { "SIGNPARAMS", { "int", "x" }, { "int", "y" } }, 28
    }
    local exp = {
      mods = 1,
      name = 'A',
      params = { { 'int', 'x' }, { 'int', 'y' } },
      continue = 28,
      ln = 4,
    }
    assert.same(exp, M.token_to_constructor_entry(token, 4, {}))
  end)

  it("token_to_constructor_entry empty params", function()
    local token = { "CONSTRUCTOR", "public", "MController", { "SIGNPARAMS" }, 26 }
    local exp = {
      mods = 1,
      name = 'MController',
      params = {},
      ln = 1,
      continue = 26,
    }
    assert.same(exp, M.token_to_constructor_entry(token, 1, {}))
  end)

  it("token_to_annotation", function()
    local tokens = { { "@", "Annot", "(", 8 } }
    local exp = { continue = 8, ln = 1, name = 'Annot' }
    assert.same(exp, M.token_to_annotation(tokens[1], 1))
  end)

  it("token_to_annotation", function()
    local f = M.token_to_annotation
    -- @Import(PostgresTestcontainerConfig.class)
    local token = { "@", "Import", { "PostgresTestcontainerConfig.class" } }
    local exp = {
      ln = 42,
      name = 'Import',
      params = { 'PostgresTestcontainerConfig.class' }
    }
    assert.same(exp, f(token, 42))
  end)

  it("add_free_annotation", function()
    local clazz = {}
    local annot = { continue = 8, ln = 1, name = 'Annot' }
    M.add_free_annotation(clazz, annot)
    local exp = {
      free_annotations = { { continue = 8, ln = 1, name = 'Annot' } }
    }
    assert.same(exp, clazz)
  end)


  it("token_to_method_entry empty params", function()
    local token = {
      'METHOD', 'protected', 'void', 'doFilterInternal',
      {
        'SIGNPARAMS',
        { 'HttpServletRequest',  'request' },
        { 'HttpServletResponse', 'response' },
        { 'FilterChain',         'filterChain' },
      },
      { 'THROWS', { 'ServletException', 'IOException' } },
      181
    }
    local exp = {
      mods = 4,
      typ = 'void',
      name = 'doFilterInternal',
      params = {
        { 'HttpServletRequest',  'request' },
        { 'HttpServletResponse', 'response' },
        { 'FilterChain',         'filterChain' }
      },
      throws = {
        { 'ServletException', 'IOException' }
      },
      ln = 1,
      continue = 181,
    }
    assert.same(exp, M.token_to_method_entry(token, 1, {}))
  end)

  it("token_to_method_entry", function()
    local token = {
      'METHOD', 'protected', 'void', 'doStuff',
      { 'SIGNPARAMS',
        { 'String', 'stuff' },
      },
      { 'THROWS', { 'Exception' } },
      181
    }
    local exp = {
      mods = 4,
      typ = 'void',
      name = 'doStuff',
      params = { { 'String', 'stuff' } },
      throws = { { 'Exception' } },
      ln = 1,
      continue = 181,
    }
    assert.same(exp, M.token_to_method_entry(token, 1, {}))
  end)

  it("token_with_params_to_method_entry one annotation", function()
    local method = {}
    local token_with_params = {
      "SIGNPARAMS",
      {
        {
          "@", "RequestParam", -- annotation
          {
            { "name",         '"name"' },
            { "required",     "false" },
            { "defaultValue", '"World"' }
          }
        },
        "String", "name"   -- param 1
      },
      { "Model", "model" } -- param 2
    }
    local res = M.token_with_params_to_method_entry(token_with_params, method)
    assert.equal(res, method) -- same instance (value is filled to method)

    local exp_method_entry = {
      params = {
        {           -- param1 type+name+annotations
          "String", -- type
          "name",   -- name
          annotations = {
            {       -- annotation 1
              name = "RequestParam",
              params = {
                { "name",         '"name"' },
                { "required",     "false" },
                { "defaultValue", '"World"' }
              }
            }
          }
        },
        { "Model", "model" } -- param2: type+name
      }
    }
    assert.same(exp_method_entry, res)
  end)

  it("token_with_params_to_method_entry one annotation without param", function()
    local method = {}
    local token_with_params = {
      "SIGNPARAMS",
      {
        { "@", "Assisted" },
        "Configuration", "configuration" -- param1
      }
    }
    local res = M.token_with_params_to_method_entry(token_with_params, method)
    assert.equal(res, method) -- filled
    local exp_method_entry = {
      params = {
        {
          annotations = { { name = "Assisted" } }, -- annotation of param
          'Configuration',                         -- param-1 type
          'configuration'                          -- param-1 name
        }
      }
    }
    assert.same(exp_method_entry, res)
  end)

  -- needs fix?
  it("token_with_params_to_method_entry two annotations per param", function()
    local method = {}
    local token_with_params = {
      "SIGNPARAMS",
      {
        {
          "@", "RequestParam", { { "key", '"value"' }, },
          "@", "Valid", {},
        },
        "String", "name"   -- param 1
      },
      { "Model", "model" } -- param 2
    }
    local res = M.token_with_params_to_method_entry(token_with_params, method)
    assert.equal(res, method)

    local exp_method_entry = {
      params = {
        { -- param 1
          "String",
          "name",
          annotations = {
            { name = "RequestParam", params = { { "key", '"value"' } } },
            { name = "Valid",        params = {} }
          }
        },
        { "Model", "model" }
      }
    }
    assert.same(exp_method_entry, res)
  end)

  it("token_with_params_to_method_entry with final prefix", function()
    -- ref: public static String methodName(final Object p1)
    local method = {}
    local token = {
      'SIGNPARAMS', { 'final', 'Object', 'p1' },
    }
    local res = M.token_with_params_to_method_entry(token, method)
    local exp = {
      params = {
        {
          'Object', -- type of the param
          'p1',     -- param name
          'FINAL'   -- final mod
        }
      }
    }
    assert.same(exp, res)
  end)
end)

--

describe("env.langs.java.util.syntax to code", function()
  it("syntax_elm_to_code", function()
    local f = M.syntax_elm_to_code
    local constructor = {
      mods = 1,
      name = 'A',
      params = { { 'int', 'x' }, { 'int', 'y' } },
      continue = 28,
      ln = 4,
    }
    local exp = {
      '    public A(int x, int y) {',
      '        this.x = x;',
      '        this.y = y;',
      '    }'
    }
    assert.same(exp, f(constructor, M.tags.CONSTRUCTOR))
  end)

  -- annotation obj to code
  it("get_code_of_annotations", function()
    local f = M.get_code_of_annotations
    local t1 = { { name = "Autowired", ln = 14, } }
    local t2 = {
      { -- first annotation
        name = "Min",
        params = {
          { "value",   "1900" },
          { "message", '"Year of birth should be greater than 1900"' }
        },
        ln = 14,
      }
    }
    assert.same({ '    @Autowired' }, f(t1))
    local exp = {
      '    @Min(value = 1900, message = "Year of birth should be greater than 1900")'
    }
    assert.same(exp, f(t2))
  end)

  -- find annotation by name in the given element
  it("find_annotation(by_name)", function()
    local f = M.find_annotation
    local elm = {
      annotations = {
        { name = "Override", ln = 16 }
      }
    }
    assert.same({ { ln = 16, name = 'Override' }, 1 }, { f(elm, 'Override') })
  end)

  it("find_annotation_any", function()
    local f = M.find_annotation_any
    local elm = {
      annotations = {
        { name = "Bean" },
        { name = "Component" },
        { name = "Service" },
        { name = "Repository" },
      }
    }
    assert.same({ name = 'Bean' }, f(elm, { 'Bean', 'Compoment' }))
    assert.same({ nil, -1 }, { f(elm, { 'NotExists' }) })
  end)
end)


describe("env.langs.java.util.syntax", function()
  it("is_value", function()
    local f = M.is_value
    assert.same(true, f('1'))
    assert.same(true, f('0'))
    assert.same(true, f('"abc"'))
    assert.same(true, f("'a'"))
    assert.same(false, f("'ab'"))
    assert.same(false, f('varname'))
    assert.same(true, f('true'))
    assert.same(true, f('false'))
    assert.same(true, f('null'))
  end)

  it("is_varname", function()
    local f = M.is_local_variable_name
    assert.same(false, f('1'))
    assert.same(true, f('a1'))
    assert.same(true, f('A1'))
    assert.same(false, f('this')) -- keyword
    assert.same(false, f('new'))  -- keyword
    assert.same(false, f('this.field'))
    assert.same(false, f('obj.prop'))
    assert.same(false, f('obj prop'))
    assert.same(false, f('this.method()'))
    assert.same(true, f('$variable'))
    assert.same(true, f('v$ariable'))
    assert.same(true, f('v$ari_able'))
    assert.same(false, f('v$ari-able'))
  end)

  it("is_same_value", function()
    local f = M.is_same_value
    -- 1th from source code (strings will be quoted)
    -- 2th from giagnostic message
    assert.same(false, f('arg', 'exp'))
    assert.same(true, f('str', 'str'))
    assert.same(true, f('"str"', 'str'))
    assert.same(true, f("a\nb", "a\nb"))
    assert.same(true, f('a\\nb', "a\nb"))
  end)

  it("parse_method_call with three arguments", function()
    local f = M.parse_method_call_to_ast
    local line = '  assertEquals("Message, with spaces", 123, unBox(ibox, 0));'
    local exp = {
      {
        { 'METHOD',   'assertEquals',           3,  14 },
        { 'ARGUMENT', '"Message, with spaces"', 16, 37 },
        { 'ARGUMENT', '123',                    40, 42 },
        { 'ARGUMENT', 'unBox(ibox, 0)',         45, 58 },
        { 'OPSEP',    60 }
      },
      nil, -- errmsg
      61
    }
    assert.same(exp, { f(line) })
  end)

  it("parse_method_call with two arguments", function()
    local f = M.parse_method_call_to_ast
    local line = '    assertEquals(1, 0);'
    local exp_ast = {
      {
        { 'METHOD',   'assertEquals', 5,  16 },
        { 'ARGUMENT', '1',            18, 18 },
        { 'ARGUMENT', '0',            21, 21 },
        { 'OPSEP',    23 }
      },
      nil, -- errmsg
      24
    }
    assert.same(exp_ast, { f(line) })
  end)

  it("parse_method_call with chained call", function()
    local f = M.parse_method_call_to_ast
    local line = '  assertThat(object).isNull();'
    local exp = {
      {
        { 'METHOD',   'assertThat', 3,  12 }, -- 1th call
        { 'ARGUMENT', 'object',     14, 19 },
        { 'METHOD',   'isNull',     22, 27 }, -- 2th method call
        { 'OPSEP',    30 }
      },
      nil, -- errmsg
      31   -- position with end of the method call
    }
    assert.same(exp, { f(line) })
  end)

  it("get_method_name", function()
    local f = M.get_method_name
    local ast = {
      { 'METHOD',   'assertThat', 3,  12 }, -- 1th call
      { 'ARGUMENT', 'object',     14, 19 },
      { 'METHOD',   'isNull',     22, 27 }, -- 2th method call
      { 'OPSEP',    30 }
    }
    assert.same('assertThat', f(ast, 1))
    assert.same('isNull', f(ast, 2))
    assert.is_nil(f(ast, 3))
  end)

  it("get_method_entry", function()
    local ast = {
      { 'METHOD',   'assertThat', 3,  12 }, -- 1th call
      { 'ARGUMENT', 'object',     14, 19 },
      { 'METHOD',   'isNull',     22, 27 }, -- 2th method call
      { 'OPSEP',    30 }
    }
    local exp_sub_ast = {
      { 'METHOD',   'assertThat', 3,  12 },
      { 'ARGUMENT', 'object',     14, 19 }
    }
    assert.same(exp_sub_ast, M.get_method_entry(ast, 1))

    local exp2 = { { 'METHOD', 'isNull', 22, 27 } }
    assert.same(exp2, M.get_method_entry(ast, 2))

    assert.same(nil, M.get_method_entry(ast, 0))
    assert.same(nil, M.get_method_entry(ast, 3))
  end)

  it("get_method_arg", function()
    local sub_ast = {
      { 'METHOD',   'assertThat', 3,  12 },
      { 'ARGUMENT', 'arg1',       14, 19 },
      { 'ARGUMENT', 'arg2',       21, 26 },
      { 'ARGUMENT', 'arg3',       28, 33 },
    }
    assert.same({ 'arg1', 14, 19 }, { M.get_method_arg(sub_ast, 1) })
    assert.same({ 'arg2', 21, 26 }, { M.get_method_arg(sub_ast, 2) })
    assert.same({ 'arg3', 28, 33 }, { M.get_method_arg(sub_ast, 3) })
  end)
end)

-- 08-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)
local M = require 'env.langs.java.util.grammar'

local lpeg = require 'lpeg'
local H = require 'env.langs.java.ahelper'

describe("env.langs.java.util.grammar", function()
  it("pFindOpSep", function()
    assert.same(5, M.pFindOpSep:match("abc;"))
    assert.same(5, M.pFindOpSep:match("a{};"))
    assert.same(7, M.pFindOpSep:match('a";" ;'))
    assert.same(7, M.pFindOpSep:match('a{;} ;'))
    assert.same(9, M.pFindOpSep:match('a/*;*/ ;'))
    assert.same(8, M.pFindOpSep:match("a//;\n ;"))
  end)

  it("pFindEndOfMultilineString", function()
    local f = function(s) return M.pFindEndOfMultilineString:match(s) end
    assert.same(4, f('"""'))
    assert.same(4, f('""";'))
    assert.same(8, f('abc """;'))
  end)

  it("import", function()
    local line = "import org.comp.*;"

    local p = lpeg.R 'az' ^ 1 * (lpeg.P '.' * lpeg.R 'az' ^ 1) ^ 0 * lpeg.P '.*'
    assert.same(11, p:match('org.comp.*'))

    assert.same({ 'IMPORT', 'org.comp.*', ';' }, M.p.cpImport:match(line))
  end)

  it("import static", function()
    local line = "import static org.comp.*;"

    local p = lpeg.R 'az' ^ 1 * (lpeg.P '.' * lpeg.R 'az' ^ 1) ^ 0 * lpeg.P '.*'
    assert.same(11, p:match('org.comp.*'))

    assert.same({ 'IMPORT', 'static org.comp.*', ';' }, M.p.cpImport:match(line))
  end)

  it("pPackageId", function()
    local p0 = M.p.pPackageId
    assert.same(4, p0:match('pkg'))
    assert.same(4, p0:match('pkg'))
    assert.is_nil(p0:match('this'))
    assert.same(5, p0:match('pkg.'))
    assert.same(10, p0:match('pkg.Class'))
    assert.same(10, p0:match('pkg.Class('))
    assert.same(10, p0:match('pkg.Class ('))
    assert.same(12, p0:match('pkg.Class.X'))
  end)

  it("pType", function()
    local f = function(line) return M.p.pType:match(line) end
    assert.same(4, f('int'))
    assert.same(5, f('byte'))
    assert.same(5, f('long'))
    assert.same(6, f('short'))
    assert.same(6, f('int[]'))

    assert.same(7, f('Object'))
    assert.same(7, f('String'))
    assert.same(9, f('String[]'))

    assert.same(17, f('Optional<String>'))
    assert.same(19, f('Optional<String>[]'))
    assert.same(23, f('List<Optional<String>>'))

    assert.same(22, f('Entry<String, Object>'))
    assert.same(26, f('Map.Entry<String, Object>'))
    assert.same(26, f('Map$Entry<String, Object>'))
    assert.same(24, f('MainClass$Inner<String>'))
    assert.same(19, f('MainClass$Inner<?>'))
    --
    assert.same(11, f('MyClass<?>'))
    assert.same(11, f('MyClass<T>'))
    assert.same(13, f('MyClass<K,V>'))
    assert.same(26, f('MyClass<? extends Object>'))
    assert.same(24, f('MyClass<? super String>'))
    -- todo all cases for <? ..
  end)

  it("pTypeVarArgs", function()
    local f = function(line) return M.p.pTypeVarArgs:match(line) end
    assert.same(10, f('String...'))
    assert.same(12, f('String[]...'))
    assert.same(20, f('Optional<String>...'))
  end)


  it("figureout how lpeg works", function()
    local line1 = "ID(pair)"
    local line2 = "ID(pair"
    local P = lpeg.P
    local EOF = P(-1)
    local pat = P('ID') * P('(') * P('pair') * P(')')
    local t = { count = 0 }
    assert.same(9, pat:match(line1))
    assert.same(nil, pat:match(line2))

    local spy = function(line, pos)
      t[#t + 1] = line; t.count = t.count + 1; return pos
    end

    local pat2 = P('ID') * P('(') * P('pair') * EOF * spy
    assert.same(8, pat2:match(line2))

    local pat3 = P('ID') * P('(') * P('pair') * (P(')') + EOF * spy)
    assert.same(8, pat3:match(line2))

    assert.same({ count = 2, 'ID(pair', 'ID(pair' }, t)
  end)

  it("Annotation kv-params with not closed bracket 2", function()
    local p = M.fpClassHeader
    local line1 = '@Autowired(key=value,'
    local line2 = 'key2="abc")'
    local exp_pos = 12

    assert.same({ { '@', 'Autowired', 11 } }, p:match(line1))

    local remainder = line1:sub(exp_pos - 1) .. " " .. line2
    assert.same('(key=value, key2="abc")', remainder)

    -- not finished - annotation params in multiple lines
    assert.is_nil(M.pBalancedParentheses:match(line1:sub(exp_pos - 1)))

    -- finished
    assert.same(24, M.pBalancedParentheses:match(remainder))

    local exp = { { 'key', 'value' }, { 'key2', '"abc"' } }
    assert.same(exp, M.p.cpAnnotationBody:match(remainder))
  end)

  it("cpAnnotationBody KWPairs", function()
    local p = M.p.cpAnnotationBody
    local akvparams = 'keya=12, keyb=34, keyc=5'

    local exp = { { 'keya', '12' }, { 'keyb', '34' }, { 'keyc', '5' } }
    assert.same(nil, p:match(akvparams))
    assert.same(exp, p:match('(' .. akvparams .. ')'))

    assert.same({ { 'keya', '123' } }, p:match('(keya=123)'))

    local p2 = M.p.cpKWPairListNotEmpty
    local exp2 = { { 'keya', '12' }, { 'keyb', '34' }, { 'keyc', '5' } }
    assert.same(exp2, p2:match(akvparams))

    local exp3 = { { 'keya', '12' }, { 'keyb', '34' }, { 'keyc', '5' } }
    assert.same(exp3, M.p.cpAnnotationParam:match(akvparams))
  end)

  it("cpAnnotationBody pGlobalIdentifier", function()
    local p = M.p.cpAnnotationBody
    local abody = 'TestInstance.Lifecycle.PER_METHOD'
    local exp = { 'value', 'TestInstance.Lifecycle.PER_METHOD' }
    assert.same(exp, p:match('(' .. abody .. ')'))
  end)

  it("cpAnnotationBody pGlobalIdentifier", function()
    local p = M.p.cpAnnotationBody
    local abody = 'TestInstance.class'
    local exp = { 'value', 'TestInstance.class' }
    assert.same(exp, p:match('(' .. abody .. ')'))
  end)

  it("cpAnnotationParam", function()
    local p = M.p.cpAnnotationParam
    assert.same({ 'value', 'AManager.class' }, p:match('AManager.class'))

    local abody = 'TestInstance.Lifecycle.PER_METHOD'
    local exp = { 'value', 'TestInstance.Lifecycle.PER_METHOD' }
    assert.same(exp, p:match(abody))
  end)

  it("Annotation list of classes", function()
    local ln = '@CompDep({AManager.class, VerLists.class, BManager.class})'
    local p = M.fpClassHeader
    local res = p:match(ln)
    local exp = {
      { "@",
        "CompDep",
        { 'value', { "AManager.class", "VerLists.class", "BManager.class" } }
      }
    }
    assert.same(exp, res)
    local exp_pos = 10
    local remainder = ln:sub(exp_pos - 1)
    assert.same("({AManager.class, VerLists.class, BManager.class})", remainder)

    local exp_params = {
      'value', { 'AManager.class', 'VerLists.class', 'BManager.class' }
    }
    assert.same(exp_params, M.p.cpAnnotationBody:match(remainder))
  end)

  it("Annotation list of one class", function()
    local ln = '@CompDep({AManager.class})'
    local p = M.fpClassHeader
    local res = p:match(ln)
    local exp = { { '@', 'CompDep', { 'value', { 'AManager.class' } } } }
    assert.same(exp, res)
    local exp_pos = 10
    local remainder = ln:sub(exp_pos - 1)

    local exp2 = { 'value', { 'AManager.class' } }
    assert.same(exp2, M.p.cpAnnotationBody:match(remainder))
  end)

  -- ???
  it("Annotation one class (same as value=AManager.class)", function()
    local ln = '@CompDep(AManager.class)'
    local p = M.fpClassHeader
    local res = p:match(ln)
    local exp = { { '@', 'CompDep', { 'value', 'AManager.class' } } }
    assert.same(exp, res)
    local exp_pos = 10
    local remainder = ln:sub(exp_pos - 1)
    assert.same('(AManager.class)', remainder)

    assert.same(15, M.p.pGlobalIdentifierClass:match('AManager.class')) -- pass
    local exp2 = { 'value', 'AManager' }
    assert.same(exp2, M.p.cpAnnotationBody:match('(AManager)'))

    local exp3 = { 'value', 'AManager.class' }
    assert.same(exp3, M.p.cpAnnotationBody:match('(AManager.class)'))
  end)

  it("Annotation one default param Constant", function()
    local ln = '@TestInstance(TestInstance.Lifecycle.PER_METHOD)'
    local p = M.fpClassHeader
    local res = p:match(ln)
    local exp = {
      {
        '@',
        'TestInstance',
        { 'value', 'TestInstance.Lifecycle.PER_METHOD' }
      }
    }
    assert.same(exp, res)

    local remainder = "(TestInstance.Lifecycle.PER_METHOD)"
    local exp2 = { 'value', 'TestInstance.Lifecycle.PER_METHOD' }
    assert.same(exp2, M.p.cpAnnotationBody:match(remainder))

    local body = '("literal")'
    local exp3 = { 'value', '"literal"' }
    assert.same(exp3, M.p.cpAnnotationBody:match(body))
  end)


  it("cpIdentifier", function()
    assert.same(14, M.p.pIdentifier:match('variable_name'))
    assert.same(11, M.p.pIdentifier:match('variable10'))
  end)

  it("pGlobalIdentifier", function()
    assert.same(19, M.p.pGlobalIdentifier:match('org.some.MainClass'))
  end)

  it("pGlobalIdentifierClass", function()
    assert.same(25, M.p.pGlobalIdentifierClass:match('org.some.MainClass.class'))
    assert.same(16, M.p.pGlobalIdentifierClass:match('MainClass.class'))
    assert.same(8, M.p.pGlobalIdentifierClass:match('A.class'))
    assert.is_nil(M.p.pGlobalIdentifierClass:match('0A.class'))
  end)

  -- issue pClassModifier without AcceccModifiers
  it("cpClassModifiers", function()
    local p0 = M.p.cpClassModifier -- ony one
    assert.same({ 'public' }, { p0:match("public static") })
    assert.is_nil(p0:match("static"))

    assert.same({ 'public' }, { p0:match("public static final") })
    assert.same(nil, p0:match("static public String m(){}"))
    assert.same({ 'abstract' }, { p0:match("abstract public static string m(){}") })

    -- -- todo solve this puzzle
    -- local pMods = (p0 * M.p.whitespace^1 )^1 / M.p.rall --lpeg.P('static') * M.p.EOF
    -- assert.same({ 'public' }, {pMods:match("public public")})
  end)

  -- ClassHeader consists of:  package, import, class-annotation, class-defintion
  -- class-defintion:  AcceccModifiers + classType(class|enum|interface) className
  -- TODO extends and implements
  describe("fpClassHeader", function()
    local lines = H.code_spring_AuthController
    local patt = M.fpClassHeader -- pattern for testing

    it("empty", function()
      assert.same({}, patt:match(""))
      assert.same({}, patt:match(" "))
    end)

    it("comment", function()
      local exp = { { 'COMMENT' } }
      assert.same(exp, patt:match("// comment"))

      local exp2 = { { 'COMMENT' }, { 'COMMENT' } }
      assert.same(exp2, patt:match("/* comment1 */ /*comment2*/"))

      assert.same({ { 'COMMENT_OPEN', 16 } }, patt:match("/* comment open"))
      -- the rest of the work of finding the line with the closing comment also
      -- falls on the parser that requests additional lines from Iterator

      assert.same({}, patt:match("close comment */ "))

      assert.same(16, M.p.ccomment_open:match("/* comment open"))
      assert.is_nil(M.p.ccomment_open:match("/* comment open */"))

      assert.same(3, M.p.ccomment_close:match("*/"))
      assert.same(4, M.p.ccomment_close:match(" */"))
      assert.same(5, M.p.ccomment_close:match(" */ "))

      assert.same(17, M.p.ccomment_close:match("close comment */"))
      assert.is_nil(M.p.ccomment_close:match("/* close comment */"))

      local exp3 = { { 'COMMENT' } }
      assert.same(exp3, patt:match("/* comment */ "))
    end)

    it("package", function()
      local exp = { { 'PACKAGE', 'org.springcourse.fsa.controller', ';' } }
      assert.same(exp, patt:match(lines[1]))
    end)

    it("import", function()
      local exp = {
        { 'IMPORT', 'org.springframework.beans.factory.annotation.Autowired', ';' }
      }
      assert.same(exp, patt:match(lines[3]))
    end)

    it("class one line defintion", function()
      local exp = { { 'CLASSDEF', 'public', 'class', 'CName', '{' } }
      assert.same(exp, patt:match('public class CName {'))
    end)

    it("class one line defintion with end of class", function()
      local exp3 = {
        { 'CLASSDEF', 'public', 'final', 'class', 'CName', '{' },
        '}'
      }
      assert.same(exp3, patt:match('public final class CName {}'))
    end)

    it("class multiline definition", function()
      local exp = { { 'CLASSDEF', 'public', 'final', 'class', 'CName', 25 } }
      -- 25 is a CONTINUE flag
      assert.same(exp, patt:match('public final class CName'))
    end)
  end)

  --

  -- lowlevel
  local pS0 = lpeg.S(' \n\t\v\f') ^ 0
  it("pClassModifier", function()
    local p = (pS0 * M.p.cpClassModifier) ^ 0
    assert.same({ 'public', 'final' }, { p:match('public final class name') })
    assert.same({ 'public', 'abstract' }, { p:match('public abstract class name') })
    -- inner
    assert.same({ 1 }, { p:match('static final class name') })
    -- issue
    local exp = { 'public', 'public', 'public', 'final' }
    assert.same(exp, { p:match('public public public final class name') })
  end)

  it("cpNestedClassModifiers", function()
    local p = (pS0 * M.p.cpInnerClassModifiers) ^ 0
    local exp = { 'static', 'private' }
    assert.same(exp, { p:match('static private class InnerClass name{') })
  end)

  it("cpClassDefinition", function()
    local p = M.p.cpClassDefinition
    local exp = {
      { 'CLASSDEF', 'public', 'abstract', 'class', 'MyClass', '{' }
    }
    assert.same(exp, { p:match("public abstract class MyClass { }") })

    local exp1 = { 'CLASSDEF', 'public', 'class', 'MyClass', '{' }
    assert.same(exp1, p:match("public class MyClass { }"))

    local exp2 = { 'CLASSDEF', 'class', 'MyClass', '{' }
    assert.same(exp2, p:match("class MyClass { }"))

    assert.same(exp2, p:match("class MyClass { }"))
  end)

  it("pClassTypeList", function()
    local p = M.p.pTypeList
    assert.same({ 'Object' }, p:match("Object"))
    assert.same({ 'String', 'Object' }, p:match("String, Object"))
    assert.same({ 'String', 'Object' }, p:match("String,Object"))
    assert.same({ 'A<T>', 'B<K,V>', 'C' }, p:match("A<T>, B<K,V>, C"))
    assert.same({ 'A<T>', 'B<K,V>', 'C<?>' }, p:match("A<T>,B<K,V>,C<?>"))
  end)

  it("pClassExtends", function()
    local p = M.p.pClassExtends
    assert.same({ 'EXTENDS', 'Object' }, p:match("extends Object"))
    -- cannot be inherited from multiple classes:
    assert.is_nil(p:match("extends A, B"))
    assert.is_nil(p:match("extends A, "))
  end)

  it("pClassImplements", function()
    local p = M.p.pClassImplements
    assert.same({ 'IMPLEMENTS', { 'Iterable' } }, p:match('implements Iterable'))
  end)

  it("pClassImplementsAndExtends", function()
    local p = M.p.pClassImplementsAndExtends
    local exp = { { 'EXTENDS', 'A' }, { 'IMPLEMENTS', { 'B' } } }
    assert.same(exp, { p:match("extends A implements B") })

    local exp2 = { { 'EXTENDS', 'A' }, { 'IMPLEMENTS', { 'IA', 'IB' } } }
    assert.same(exp2, { p:match("extends A implements IA, IB") })

    assert.same({ 1 }, { p:match("extends A, B implements IA, IB") }) -- ???
  end)

  it("cpClassDefinition with extends", function()
    local p = M.p.cpClassDefinition
    local exp = {
      'CLASSDEF', 'public', 'class', 'B', { 'EXTENDS', 'A' }, 25
    }
    assert.same(exp, p:match('public class B extends A'))
  end)

  it("cpClassDefinition with implements", function()
    local p = M.p.cpClassDefinition
    local exp = {
      'CLASSDEF', 'public', 'class', 'B', { 'IMPLEMENTS', { 'IA' } }, 29
    }
    assert.same(exp, p:match('public class B implements IA'))
  end)

  it("cpClassDefinition with extends and implements", function()
    local p = M.p.cpClassDefinition
    local exp = {
      'CLASSDEF',
      'public',
      'class',
      'B',
      { 'EXTENDS',    'A' },
      { 'IMPLEMENTS', { 'IA' } },
      39
    }
    assert.same(exp, p:match('public class B extends A implements IA'))
  end)

  it("cpClassDefinition with extends and implements list", function()
    local p = M.p.cpClassDefinition
    local exp = {
      'CLASSDEF',
      'public',
      'class',
      'B',
      { 'EXTENDS',    'A' },
      { 'IMPLEMENTS', { 'IA', 'IC' } },
      43
    }
    assert.same(exp, p:match('public class B extends A implements IA, IC'))
  end)

  it("build_find_outside_string_and_comments", function()
    local p = M.build_patt_find_outside_string_and_comments("abc")
    local line = '123"abc" /*abc*/ ab abc 2'
    --            123456789012345678
    assert.same(24, lpeg.match(p, line))
    assert.same('abc', line:sub(24 - 3, 24 - 1))
  end)



  describe("Annotations", function()
    local patt = M.fpClassHeader

    it("cpAnnotation no params", function()
      local p0 = M.p.cpAnnotation
      assert.same({ '@', 'Autowired' }, p0:match('@Autowired'))
      assert.same({ { '@', 'Autowired' } }, patt:match('@Autowired'))
      --
      assert.same({ { '@', 'Autowired' } }, patt:match('@Autowired()'))
    end)

    it("Annotation params without keys - one defualt value in one line", function()
      -- local p0 = M.p.cpAnnotation
      local exp = { { '@', 'Autowired', { 'value', '"x"' } } }
      assert.same(exp, patt:match('@Autowired("x")'))

      -- need next line(CONTINUE from pos=12)
      local exp2 = { { '@', 'Autowired', 11 } }
      assert.same(exp2, patt:match('@Autowired("x"'))
    end)

    it("Annotation kv-params with not closed bracket", function()
      local exp = { { '@', 'Autowired', 11 } }
      assert.same(exp, patt:match('@Autowired(key=value'))
    end)

    it("Two Annotations with kv-params in one line", function()
      local exp = {
        { '@', 'Anno1', { { 'k', 'v' } } },
        { '@', 'Anno2', { { 'k2', 'v2' } } }
      }
      assert.same(exp, patt:match('@Anno1(k=v) @Anno2(k2=v2)'))
    end)

    it("Annotation with k=v params", function()
      -- local p0 = M.p.cpAnnotation
      local s = [[  @JoinColumn(name = "person_id", referencedColumnName = "id")]]
      local exp = {
        { '@', 'JoinColumn',
          { { 'name', '"person_id"' }, { 'referencedColumnName', '"id"' } } }
      }
      assert.same(exp, patt:match(s))
    end)

    it("Annotation with Class.CONST", function()
      local exp = { { '@', 'Target', { 'value', 'ElementType.METHOD' } } }
      assert.same(exp, patt:match("@Target(ElementType.METHOD)"))

      local exp2 = { { '@', 'Target', { 'value', 'ElementType' } } }
      assert.same(exp2, patt:match("@Target(ElementType)"))
    end)


    it("pAnnoBody", function()
      local p0 = M.p.cpAnnotationBody
      -- M.p.D.enable_module(M)
      assert.same({ { 'k', 'v' } }, p0:match('(k=v)'))

      local exp = { { 'k', 'v' }, { 'b', '3' }, { 'c', '"T"' } }
      assert.same(exp, p0:match('(k=v, b=3, c = "T")')) -- ?

      assert.is_nil(p0:match('"literal"'))
      assert.same({ 'value', '"literal"' }, p0:match('("literal")'))
      assert.same({ 'value', { '"literal"' } }, p0:match('({"literal"})'))
      assert.same({ 'value', { '"ab"', '"cd"', '"e"' } }, p0:match('({"ab", "cd", "e"})'))
      assert.same({ { 'value', '"literal"' } }, p0:match('(value="literal")'))

      local exp2 = { { 'value', '{"ab", "cd"}', { '"ab"', '"cd"' } } }
      assert.same(exp2, p0:match('(value={"ab", "cd"})'))

      local exp3 = {
        {
          'value',
          '{MyClass.class, Some.class}',
          { 'MyClass.class', 'Some.class' }
        }
      }
      assert.same(exp3, p0:match('(value={MyClass.class, Some.class})'))

      -- assert.same({ { 'value', '"literal"' } }, p0:match('({value="literal"})'))
      assert.same(nil, p0:match('({value="literal"})'))
      assert.same({ { 'value', 'varname', } }, p0:match('(value = varname)')) -- ?
      assert.same({ 'value', 'Elm.TYPE' }, p0:match('(Elm.TYPE)'))            -- ?
      assert.same({ { 'k', '"abc"' } }, p0:match('(k="abc")'))                -- ?
      assert.same({ { 'k', '"abc"' } }, p0:match('(k="abc")'))                -- ?
      assert.same({ { 'k', '123' } }, p0:match('(k=123)'))                    -- ?
      local exp4 = { { 'k', 'v' }, { 'k2', '"abc"' }, { 'd', '123' } }
      assert.same(exp4, p0:match('(k=v,k2="abc",d=123)'))                     -- ?
      local exp5 = { { 'k', 'Elm.TYPE' }, { 'k2', '"abc"' }, { 'd', '123' } }
      assert.same(exp5, p0:match('(k=Elm.TYPE,k2="abc",d=123)'))              -- ?
    end)

    it("cpKWPairListNotEmpty", function()
      local p0 = M.p.cpKWPairListNotEmpty

      -- not a map but flat list (table) to keep order of keys
      assert.same({ { 'key', 'val' }, { 'b', '8' } }, p0:match('key=val,b=8'))

      local exp = { { 'k', 'v' }, { 'b', '8' }, { 'c', '"123"' } }
      assert.same(exp, p0:match('k=v, b = 8, c="123"'))
      assert.same({ { 'k', 'v' } }, p0:match('k=v,'))
      assert.same({ { 'k', 'Elm.TYPE' } }, p0:match('k=Elm.TYPE'))
    end)
  end)

  --

  describe("ClassFields", function()
    it("fpClassField", function()
      local exp = { 'FIELD', 'public', 'String', 'name', ';' }
      assert.same(exp, M.fpClassField:match('public String name;'))
    end)

    it("fpClassField", function()
      local line = "    private final PersonValidator personValidator;"
      local exp = {
        'FIELD', 'private', 'final', 'PersonValidator', 'personValidator', ';'
      }
      assert.same(exp, M.fpClassField:match(line))
    end)

    it("fpClassField partial", function()
      local p = M.fpClassField
      local exp = { 'FIELD', 'public', 'String', 'name', 20 } -- CONTINUE
      assert.same(exp, p:match('public String name '))
      --                                          ^ 20
    end)

    it("cpAssigmentRightPart", function()
      local f = function(s) return M.p.cpAssigmentRightPart:match(s) end
      local exp = { '=', 'new Object();' }
      assert.same(exp, { f( --[[public Object fn ]] '= new Object();') })
      --  public String fn = "abc";
      assert.same({ '=', '"abc";' }, { f('= "abc";') })
      assert.same({ '=', '9;' }, { f('= 9;') })
      assert.same({ '=', '-1;' }, { f('= -1;') })
      assert.same({ '=', '-1 + 2;' }, { f('= -1 + 2;') })
      assert.same({ '=', '-2 - 1;' }, { f('= -2 - 1;') })
      assert.same({}, { f('= -2 - -1;') })
      assert.same({ '=', '0;' }, { f('= 0;') })
      -- continue
      assert.same({ '=', '"""', 6 }, { f('= """') })
      -- todo float double exponent

      local exp2 = { '=', 'new Point(x, y);' }
      assert.same(exp2, { f( --[[public Point p ]] '= new Point(x, y);') })

      local exp3 = { '=', '', 4 }
      assert.same(exp3, { f( --[[public Point p ]] ' = ') })
      -- value in the next line

      assert.same({ '=', '"abc"+"bc";' }, { f('= "abc"+"bc";') })
      assert.same({ '=', 'CONST+"bc";' }, { f('= CONST+"bc";') })
      assert.same({ '=', 'CONST + "bc";' }, { f('= CONST + "bc";') })
      assert.same({ '=', 'X + Y;' }, { f('= X + Y;') })
      assert.same({ '=', 'X + Y + Z;' }, { f('= X + Y + Z;') })
      assert.same({ '=', 'X + Y + Z - QQ;' }, { f('= X + Y + Z - QQ;') })
    end)
  end)

  --

  it("cpSignatureMethodParams", function()
    local p = M.p.pSignatureMethodParamsFull
    assert.same({ 'SIGNPARAMS' }, p:match('() '))
    assert.same({ { 'SIGNPARAMS' }, 4 }, { p:match('() ') })

    local exp2 = { { 'SIGNPARAMS', { 'A', 'a' }, { 'B', 'b' } }, 12 }
    assert.same(exp2, { p:match('(A a, B b) ') })

    local exp3 = { { 'SIGNPARAMS', { 'A', 'a' } }, 7 }
    assert.same(exp3, { p:match('(A a) ') })

    assert.same(nil, p:match('(OnlyType) '))

    local exp4 = { { 'SIGNPARAMS', { 'int', 'x' }, { 'int', 'y' } }, 16 }
    assert.same(exp4, { p:match('(int x, int y) ') })
  end)

  it("cpSignatureMethodParams final prefix for ParamType", function()
    local p = M.p.pSignatureMethodParamsFull
    local exp5 = {
      { 'SIGNPARAMS', { 'final', 'int', 'x' }, { 'int', 'y' } }, 22
    }
    assert.same(exp5, { p:match('(final int x, int y) ') })

    local exp6 = {
      { 'SIGNPARAMS', { 'int', 'x' }, { 'final', 'int', 'y' } }, 22
    }
    assert.same(exp6, { p:match('(int x, final int y) ') })
  end)

  it("pSignatureMethodParamsFull", function()
    local p = M.p.pSignatureMethodParamsFull
    local exp = { { 'SIGNPARAMS', { 'A', 'a1' }, { 'B', 'a2' } }, 14 }
    assert.same(exp, { p:match("(A a1, B a2) {") })

    assert.same({ { 'SIGNPARAMS', { 'A', 'a' } }, 8 }, { p:match("(A a)  {") })
    assert.same({ { 'SIGNPARAMS', { 'A', 'a' } }, 9 }, { p:match("(A a)   {") })
  end)

  it("pSignatureMethodParamsFull TypeVarArgs", function()
    local p = M.p.pSignatureMethodParamsFull
    local exp = {
      { 'SIGNPARAMS', { 'A', 'a1' }, { 'Object...', 'a2' } },
      22
    }
    assert.same(exp, { p:match("(A a1, Object... a2) {") })

    local exp2 = { { 'SIGNPARAMS', { 'A...', 'a' } }, 10 }
    assert.same(exp2, { p:match("(A...a)  {") })

    local exp3 = { { 'SIGNPARAMS', { 'A...', 'a' } }, 11 }
    assert.same(exp3, { p:match("(A...a)   {") })

    local exp4 = {
      { 'SIGNPARAMS', { 'A', 'a' }, { 'Object...', 'args' } },
      23
    }
    assert.same(exp4, { p:match("(A a, Object... args) {") })

    -- fails
    assert.same({}, { p:match("(A a, Object... args, int i) {") })
  end)

  it("pSignatureMethodParam", function()
    local f = function(s) return M.p.pSignatureMethodParam:match(s) end
    assert.same({ 'A', 'a' }, f("A a"))
    assert.same({ 'A...', 'a' }, f("A... a"))
    assert.same({ 'A...', 'a' }, f("A...a"))
    assert.is_nil(f("A... a,"))
    assert.is_nil(f("A...a,"))
    assert.is_nil(f("A...a ,"))
    assert.same({ 'A...', 'a' }, f("A...a)"))
  end)

  it("pSignatureMethodParamsFull", function()
    local p = M.p.pSignatureMethodParamsFull
    local line = "(int x, int y) throws Exception {"
    local exp = {
      { 'SIGNPARAMS', { 'int', 'x' }, { 'int', 'y' } },
      { 'THROWS',     { 'Exception' } },
      33
    }
    assert.same(exp, { p:match(line) })
  end)

  it("cpAnnotationFull", function()
    local s = '@RequestParam(name = "name", required = false, defaultValue = "World")'
    local exp = {
      '@', 'RequestParam',
      {
        { 'name',         '"name"' },
        { 'required',     'false' },
        { 'defaultValue', '"World"' }
      }
    }
    assert.same(exp, M.p.cpAnnotationFull:match(s))
  end)

  it("pSignatureMethodParamsFull params with annotations", function()
    local p = M.p.pSignatureMethodParamsFull
    local input = [[(
        @RequestParam(name = "name", required = false, defaultValue = "World")
        String name,
        Model model
    )]]
    local res = { p:match(input) }

    local exp = {
      { "SIGNPARAMS",
        {
          { "@", "RequestParam", -- annotation
            {
              { "name",         '"name"' },
              { "required",     "false" },
              { "defaultValue", '"World"' }
            }
          },
          "String", "name" },   -- param 1

        { "Model", "model" } }, -- param 2
      128                       -- continue_col
    }
    assert.same(exp, res)
  end)

  describe("Constructor", function()
    local patt = M.fpClassConstructorBegin

    it("fpClassConstructorBegin one-line", function()
      local exp = {
        'CONSTRUCTOR', 'public', 'MyClass', { 'SIGNPARAMS' }, 18
      }
      assert.same(exp, patt:match(' public MyClass(){}'))
      --                        12345678901234567890123456
      local exp2 = {
        'CONSTRUCTOR', 'public', 'MyClass', { 'SIGNPARAMS' }, 20
      }
      assert.same(exp2, patt:match(' public MyClass()  {}'))
      --                        12345678901234567890123456
    end)


    it("fpClassConstructorBegin multiline", function()
      local lines = {
        '    public MController(MService msService,',
        '            MValidator mValidator,',
        '            ModelMapper modelMapper) {',
        '        this.msService = msService;',
        '        this.mValidator = mValidator;',
        '        this.modelMapper = modelMapper;',
        '    }',
      }
      local exp = { 'CONSTRUCTOR', 'public', 'MController', 23 }
      assert.same(exp, patt:match(lines[1]))
    end)

    it("fpClassConstructorBegin", function()
      local p = M.fpClassConstructorBegin

      assert.same(nil, p:match(' public MyClass'))

      local exp = { 'CONSTRUCTOR', 'public', 'MyClass', 16 }
      assert.same(exp, p:match(' public MyClass('))

      local exp2 = { 'CONSTRUCTOR', 'public', 'MyClass', 17 }
      assert.same(exp2, p:match(' public MyClass (  '))

      local exp3 = {
        'CONSTRUCTOR', 'public', 'MyClass',
        { "SIGNPARAMS", { "A", "a1" }, { "B", "a2" } }, 30
      }
      local res = p:match(' public MyClass (A a1, B a2) {')
      assert.same(exp3, res)

      local exp4 = { 'CONSTRUCTOR', 'public', 'A', { 'SIGNPARAMS' }, 13 }
      assert.same(exp4, p:match(' public A() {'))

      local exp5 = {
        'CONSTRUCTOR', 'public', 'A', { 'SIGNPARAMS', { 'B', 'a1' } }, 17
      }
      assert.same(exp5, p:match(' public A(B a1) {'))

      local exp6 = {
        'CONSTRUCTOR', 'public', 'A', { 'SIGNPARAMS', { 'B', 'a1' } }, 19
      }
      assert.same(exp6, p:match(' public A(B a1)   {'))

      local exp7 = {
        'CONSTRUCTOR', 'public', 'A',
        { 'SIGNPARAMS', { { '@', 'Valid' }, 'B', 'a1' } }, -- annotated param
        26                                                 -- position of the {
      }
      assert.same(exp7, p:match(' public A(@Valid B a1)   {'))
      --                         12345678901234567890123456
    end)
  end)
end)


describe("env.langs.java.util.grammar Method", function()
  it("pMethodName", function()
    local f = function(l) return M.p.pMethodName:match(l) end
    assert.same(11, f("methodName"))
    assert.same(6, f("_name"))
    assert.same(12, f("method_name"))
    assert.is_nil(f("NotAmethodName"))
    assert.is_nil(f("not.a.method.name"))
  end)

  it("pType", function()
    local f = function(l) return M.p.pType:match(l) end
    assert.same(7, f("String"))
    assert.same(4, f("int"))
    assert.is_nil(f("void")) -- only as return type in signature
  end)

  it("pMethodSignatureBegin", function()
    local f = function(l) return M.p.pMethodSignatureBegin:match(l) end
    local exp = { 'public', 'String', 'methodName' }
    assert.same(exp, { f("public String methodName() {") })
  end)

  it("fpClassMethodBegin", function()
    local f = function(l) return M.fpClassMethodBegin:match(l) end
    local code = [[
    protected String methodName() {
        // code
    }
    ]]
    local exp = {
      'METHOD', 'protected', 'String', 'methodName', { 'SIGNPARAMS' }, 35
    }
    assert.same(exp, f(code))
  end)

  it("fpClassMethodBegin", function()
    local f = function(l) return M.fpClassMethodBegin:match(l) end
    local code = [[
    protected void doFilterInternal(HttpServletRequest request,
            HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        // code
    }
    ]]
    local exp = {
      'METHOD',
      'protected',
      'void',
      'doFilterInternal',
      {
        'SIGNPARAMS',
        { 'HttpServletRequest',  'request' },
        { 'HttpServletResponse', 'response' },
        { 'FilterChain',         'filterChain' },
      },
      { 'THROWS', { 'ServletException', 'IOException' } },
      181
    }
    assert.same(exp, f(code))
  end)

  it("fpClassMethodBegin", function()
    local f = function(l) return M.fpClassMethodBegin:match(l) end
    local code = [[
    public static String methodName(final Object p1) {
         return "";
    }
    ]]
    local exp = {
      'METHOD',
      'public',
      'static',
      'String',
      'methodName',
      { 'SIGNPARAMS', { 'final', 'Object', 'p1' } },
      54
    }
    assert.same(exp, f(code))
  end)
end)


describe("env.langs.java.util.grammar offhand", function()
  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    -- M.debug = true
    -- require'dprint'.enable_module(M)
    local exp0 = {
      { '1', 2, 2 },
      { '+', 3, 3 },
      { ';', 4, 4 },
      { '2', 5, 5 }
    }
    assert.same(exp0, f(" 1+;2"))

    local input = 'abc >= "de f"'
    local exp = { { 'abc', 1, 3 }, { '>=', 5, 6 }, { '"de f"', 8, 13 } }
    assert.same(exp, f(input))
    assert.same('abc', string.sub(input, 1, 3))

    local exp2 = { { 'a', 1, 1 }, { 'b', 3, 3 }, { 'c', 5, 5 }, { 'd', 7, 7 } }
    assert.same(exp2, f("a b c d"))

    local exp3 = {
      { '1', 1, 1 },
      { '+', 2, 2 },
      { '2', 3, 3 },
      { ':', 4, 4 },
      { '3', 5, 5 },
      { '4', 7, 7 },
      { '5', 9, 9 }
    }
    assert.same(exp3, f("1+2:3 4 5"))
    local exp4 = {
      { 'private',       2,  8 },
      { 'final',         10, 14 },
      { 'org.comp.Main', 16, 28 },
      { 'field',         30, 34 },
      { ';',             35, 35 }
    }
    assert.same(exp4, f(" private final org.comp.Main field;"))
  end)

  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'word1',         1,  5 },
      { 'a',             7,  7 },
      { '=',             8,  8 },
      { '123',           9,  11 },
      { ';',             12, 12 },
      { 'b',             14, 14 },
      { '=',             15, 15 },
      { '"ab c"',        16, 21 },
      { '/* comment */', 23, 35 },
      { 'e',             37, 37 },
      { '-',             38, 38 },
      { 'c',             39, 39 },
      { 'last_word',     41, 49 }
    }
    assert.same(exp, f('word1 a=123; b="ab c" /* comment */ e-c last_word'))
  end)


  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'List<Object>', 1,  12 },
      { 'obj',          14, 16 },
      { '=',            18, 18 },
      { 'new',          20, 22 },
      { 'ArrayList',    24, 32 },
      { '<>',           33, 34 },
      { '()',           35, 36 },
      { ';',            37, 37 }
    }
    assert.same(exp, f("List<Object> obj = new ArrayList<>();"))

    local exp2 = { { 'List<?>', 1, 7 }, { 'obj', 9, 11 }, { ';', 12, 12 } }
    assert.same(exp2, f("List<?> obj;"))

    local exp3 = {
      { 'a', 1, 1 }, { ',', 2, 2 }, { 'b', 3, 3 }, { ',', 4, 4 }, { 'c', 5, 5 }
    }
    assert.same(exp3, f("a,b,c"))

    local exp4 = {
      { '(', 1, 1 }, { 'a', 2, 2 }, { ',', 3, 3 }, { 'b', 4, 4 }, { ')', 5, 5 }
    }
    assert.same(exp4, f("(a,b)"))

    local exp5 = { { '@', 1, 1 }, { 'Autowired', 2, 10 } }
    assert.same(exp5, f("@Autowired"))

    local exp6 = {
      { '@',         1,  1 },
      { 'Autowired', 2,  10 },
      { '(',         11, 11 },
      { 'k',         12, 12 },
      { '=',         13, 13 },
      { 'v',         14, 14 },
      { ')',         15, 15 }
    }
    assert.same(exp6, f("@Autowired(k=v)"))
  end)

  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'this.',     1,  5 },
      { 'fieldName', 6,  14 },
      { '=',         16, 16 }
    }
    assert.same(exp, f("this.fieldName = "))
  end)

  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = { { 'abc', 1, 3 }, { '// this is a comment ', 5, 25 } }
    assert.same(exp, f("abc // this is a comment "))

    local exp3 = {
      { 'abc', 1, 3 }, { '/* this is a comment */', 5, 27 }, { 'de', 29, 30 }
    }
    assert.same(exp3, f("abc /* this is a comment */ de"))

    local exp2 = {
      { 'abc',                   1,  3 },
      { '// this is a comment ', 5,  25 },
      { 'next',                  28, 31 },
      { 'line',                  33, 36 }
    }
    assert.same(exp2, f("abc // this is a comment \n next line"))
  end)

  it("cpLexemsWithPos", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'Object', 1,  6 },
      { 'o',      8,  8 },
      { '=',      10, 10 },
      { 'new',    12, 14 },
      { 'Object', 16, 21 },
      { '()',     22, 23 },
      { ';',      24, 24 }
    }
    assert.same(exp, f("Object o = new Object();"))

    local exp2 = {
      { 'e', 1, 1 }, { '=', 3, 3 }, { 'Entry<String, Object>', 5, 25 }
    }
    assert.same(exp2, f("e = Entry<String, Object>"))

    local exp3 = { { 'e', 1, 1 }, { '=', 3, 3 }, { 'Constants.Tag', 5, 17 } }
    assert.same(exp3, f("e = Constants.Tag"))

    local exp4 = {
      { 'e', 1, 1 }, { '=', 3, 3 }, { 'Parent.Container<String>', 5, 28 }
    }
    assert.same(exp4, f("e = Parent.Container<String>"))
  end)

  it("cpLexemsWithPos method call", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'tracker.func', 1,  12 },
      { '(',            13, 13 },
      { '32',           14, 15 },
      { ')',            16, 16 },
      { ';',            17, 17 },
      { '// comment',   19, 28 }
    }
    assert.same(exp, f("tracker.func(32); // comment"))
  end)

  it("cpLexemsWithPos field access", function()
    local f = function(l) return M.offhand.cpLexemsWithPos:match(l) end
    local exp = {
      { 'int',           1,  3 },
      { 'x',             5,  5 },
      { '=',             7,  7 },
      { 'tracker.field', 9,  21 },
      { ';',             22, 22 },
      { '// comment',    24, 33 }
    }
    assert.same(exp, f("int x = tracker.field; // comment"))
  end)

  it("pUntilOpenCurlyBraces", function()
    local p = M.pUntilOpenCurlyBraces
    assert.same(18, p:match("throws Exception {"))
    assert.same('throws Exception ', lpeg.C(p):match("throws Exception {"))
    assert.same('"{" ', lpeg.C(p):match('"{" {'))
    assert.same('/*{*/ ', lpeg.C(p):match('/*{*/ {'))
  end)
end)

--

describe("env.langs.java.util.grammar instruction parsing", function()
  it("pMethodCall", function()
    local f = function(s) return { M.pMethodCall:match(s) } end
    assert.same({ 'mn', { '1', '2', '3' } }, f('mn(1,2,3)'))
    assert.same({ 'm', { '1', '2', '3' } }, f('m(1, 2, 3)'))
    assert.same({ 'm', { '1', '2', '3' } }, f('m( 1 , 2 , 3 )'))
    assert.same({ 'm', { '"abc"', '2', '3' } }, f('m("abc",2,3)'))
    assert.same({ 'm', { '"#"+i', '2', '3' } }, f('m("#"+i,2,3)'))

    assert.same({ 'm', { 'fieldName', '2', '3' } }, f('m(fieldName, 2,3)'))
    assert.same({ 'm', { 'this.fieldNm', '2', '3' } }, f('m(this.fieldNm, 2,3)'))

    assert.same({ 'm', { 'method(1,2)', '3' } }, f('m(method(1,2), 3)'))
    assert.same({ 'm', { 'this.method(1,2)', '3' } }, f('m(this.method(1,2), 3)'))
    local exp = { 'method', { 'call(this.nested(), 2)', '3' } }
    assert.same(exp, f('method(call(this.nested(), 2), 3)'))

    local exp2 = { 'assertEquals', { '"size:#i"+i', 'i', 'size()' } }
    assert.same(exp2, f('  assertEquals("size:#i"+i, i, size());'))

    local exp3 = { 'assertEquals', { '"size:#i"+i', 'i', 'lap10.size()' } }
    assert.same(exp3, f('  assertEquals("size:#i"+i, i, lap10.size());'))

    local exp4 = { 'assertEquals', { '"size:#i"+i', 'i', 'lap10.get()[0]' } }
    assert.same(exp4, f('  assertEquals("size:#i"+i, i, lap10.get()[0]);'))
  end)

  it("pMethodCallRaw", function()
    local f = function(s) return M.p.pMethodCallRaw:match(s) end
    assert.same(13, f('lap10.size()'))
    assert.same(11, f('lap.size()'))
    assert.same(7, f('size()'))
    assert.is_nil(f('lap10.'))
  end)

  it("pSignatureParamTypes", function()
    local f = function(s) return M.pSignatureParamTypes:match(s) end
    assert.same({ 'int', 'int' }, { f('int, int') })
    assert.same({ 'int', 'int[]', 'String' }, { f('int, int[], String') })
    assert.same({ 'List<String>', 'String' }, { f('List<String>, String') })
  end)

  it("pMethodCallWithPos", function()
    local f = function(s) return M.offhand.pMethodCallWithPos:match(s) end
    local exp = {
      {
        { 'METHOD',   'methodName', 2,  11 },
        { 'ARGUMENT', '"arg1"',     13, 18 },
        { 'ARGUMENT', '"arg2"',     21, 26 },
        { 'ARGUMENT', '3',          29, 29 }
      },
      31
    }
    assert.same(exp, { f(' methodName("arg1", "arg2", 3);') })

    local exp2 = { { { 'METHOD', 'methodName', 2, 11 } }, 14 }
    assert.same(exp2, { f(' methodName();') })
  end)

  it("pMethodCallChainedWithPos with opsep(;)", function()
    local f = function(s) return M.offhand.pMethodCallChainedWithPos:match(s) end
    local exp = {
      {
        { 'METHOD',   'method', 2,  7 },
        { 'ARGUMENT', '"arg1"', 9,  14 },
        { 'ARGUMENT', '"arg2"', 17, 22 },
        { 'METHOD',   'with',   25, 28 },
        { 'ARGUMENT', '1',      30, 30 },
        { 'OPSEP',    32 }
      },
      33
    }
    assert.same(exp, { f(' method("arg1", "arg2").with(1);') })
  end)

  it("pMethodCallChainedWithPos", function()
    local f = function(s) return M.offhand.pMethodCallChainedWithPos:match(s) end
    local exp = {
      {
        { 'METHOD',   'object', 2,  7 },
        { 'ARGUMENT', '"arg1"', 9,  14 },
        { 'ARGUMENT', '"arg2"', 17, 22 },
        { 'METHOD',   'with',   25, 28 },
        { 'ARGUMENT', '1',      30, 30 }
        -- no ; in the end
      },
      32
    }
    assert.same(exp, { f(' object("arg1", "arg2").with(1).do()') })
  end)
end)


describe("env.langs.java.util.grammar offhand local variable assignment", function()
  local f = function(s) return M.offhand.pLocalVarAssignmentsWithPos:match(s) end

  it("pLocalVarAssignmentsWithPos one line String", function()
    local line = 'String name = "abc";'
    local exp = {
      {
        typ = { 'String', 1, 6 },
        value = { '"abc"', 15, 19 },
        varname = { 'name', 8, 11 }
      }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos one line int", function()
    local line = 'int x = 1;'
    local exp = {
      {
        typ = { 'int', 1, 3 },
        value = { '1', 9, 9 },
        varname = { 'x', 5, 5 }
      }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarDefinitionWthPos", function()
    local line = 'int x;'
    local exp = {
      { typ = { 'int', 1, 3 }, varname = { 'x', 5, 5 } }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos one line int", function()
    local line = 'x = 1;'
    local exp = {
      { value = { '1', 5, 5 }, varname = { 'x', 1, 1 } }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos two lines int", function()
    local line = "    int x = 1;\n    int y = 2;"
    local exp = {
      {
        typ = { 'int', 5, 7 },
        value = { '1', 13, 13 },
        varname = { 'x', 9, 9 }
      },
      {
        typ = { 'int', 20, 22 },
        value = { '2', 28, 28 },
        varname = { 'y', 24, 24 }
      }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos two lines int", function()
    local line = "    int x = 1;\n    // comment\n    int y = 2;"
    local exp = {
      {
        typ = { 'int', 5, 7 },
        value = { '1', 13, 13 },
        varname = { 'x', 9, 9 }
      },
      {
        typ = { 'int', 35, 37 },
        value = { '2', 43, 43 },
        varname = { 'y', 39, 39 }
      }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos two lines int", function()
    local line = "    int x = 1;\n    // comment\n    x = 2;"
    local exp = {
      {
        typ = { 'int', 5, 7 },
        value = { '1', 13, 13 },
        varname = { 'x', 9, 9 }
      },
      { value = { '2', 39, 39 }, varname = { 'x', 35, 35 } }
    }
    assert.same(exp, f(line))
  end)

  it("pLocalVarAssignmentsWithPos two lines int", function()
    local body = [[
        // given
        mockHttpClientSetRespFromFile(200, "response.json");

        // when
        Response found = updater.getSmthgResonse();
        // then
        assertNotNull(found);
        String exp = "DnsRecordResponse";
        assertEquals(exp, found.toString());
]]
        -- assertEquals(exp, found.toString());
    local exp = {
      {
        typ = { 'String', 202, 207 },
        value = { '"DnsRecordResponse"', 215, 233 },
        varname = { 'exp', 209, 211 }
      }
    }
    assert.same(exp, f(body))
  end)
end)

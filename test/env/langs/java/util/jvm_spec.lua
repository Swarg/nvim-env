-- 02-10-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.java.util.jvm'

describe("env.langs.java.util.jvm", function()
  local jvm_list = {
    -- 'ps --no-headers -eo "pid,%cpu,%mem,etime,cmd"'
    '533   0.2  3.5    03:44:09 /usr/lib/jvm/java8/bin/java -XX:MaxMetaspaceSize=384m -XX:+HeapDumpOnOutOfMemoryError -Xms256m -Xmx512m -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant -cp /opt/gradle/lib/gradle-daemon-main-8.9.jar -javaagent:/opt/gradle/lib/agents/gradle-instrumentation-agent-8.9.jar org.gradle.launcher.daemon.bootstrap.GradleDaemon 8.9',
    '615   0.3  2.5    01:38:27 /usr/lib/jvm/java/bin/java -jar /home/user/Decomp/luyten-0.8.3.jar org.eclipse.jdt.ls.core_1.40.0.202409261450.jar',
    '1291  3.0  3.5    09:31 /usr/lib/jvm/java17/bin/java -Declipse.application=org.eclipse.jdt.ls.core.id1 -Dosgi.bundles.defaultStartLevel=4 -Declipse.product=org.eclipse.jdt.ls.core.product -Dlog.protocol=true -Dlog.level=ALL -Xms1G -XX:+UseG1GC -XX:+UseStringDeduplication --add-modules=ALL-SYSTEM --add-opens java.base/java.util=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED -jar /home/user/.local/share/jdtls//plugins/org.eclipse.equinox.launcher_1.6.900.v20240613-2009.jar -configuration /home/user/.local/share/jdtls/config_linux -data /home/user/workspace/research',
    "4780  1.0  1.7    23:12 /usr/lib/jvm/oracle/jdk8/bin/java -Dg -Djava.library.path=/usr/java/packages/lib:/usr/lib64:/lib64:/lib:/usr/lib//home/user/.gradle/caches/app/net/app/app_natives/ -Djava.security.manager=worker.org.gradle.process.internal.worker.child.BootstrapSecurityManager -Dorg.gradle.internal.worker.tmpdir=/home/user/dev/app/build/tmp/test/work -Xmn128M -Xms1024M -Xmx1024M -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant -ea -cp /home/user/.gradle/caches/8.9/workerMain/gradle-worker.jar worker.org.gradle.process.internal.worker.GradleWorkerMain 'Gradle Test Executor 3'",
  }
  local ps_entry_1 = {
    pid = 533,
    cpu = '0.2',
    mem = '3.5',
    etime = '03:44:09',
    cmd = '/usr/lib/jvm/java8/bin/java',
    args = {
      '-XX:MaxMetaspaceSize=384m',
      '-XX:+HeapDumpOnOutOfMemoryError',
      '-Xms256m',
      '-Xmx512m',
      '-Dfile.encoding=UTF-8',
      '-Duser.country=US',
      '-Duser.language=en',
      '-Duser.variant',
      '-cp',
      '/opt/gradle/lib/gradle-daemon-main-8.9.jar',
      '-javaagent:/opt/gradle/lib/agents/gradle-instrumentation-agent-8.9.jar',
      'org.gradle.launcher.daemon.bootstrap.GradleDaemon',
      '8.9'
    },
  }
  it("parse_jvm_entry", function()
    local exp = ps_entry_1
    assert.same(exp, M.parse_ps_entry(jvm_list[1]))
  end)

  local ps_entry_4 = {
    pid = 4780,
    cpu = '1.0',
    mem = '1.7',
    etime = "23:12",
    cmd = "/usr/lib/jvm/oracle/jdk8/bin/java",
    args = {
      '-Dg',
      '-Djava.library.path=/usr/java/packages/lib:/usr/lib64:/lib64:/lib:/usr/lib//home/user/.gradle/caches/app/net/app/app_natives/',
      '-Djava.security.manager=worker.org.gradle.process.internal.worker.child.BootstrapSecurityManager',
      '-Dorg.gradle.internal.worker.tmpdir=/home/user/dev/app/build/tmp/test/work',
      '-Xmn128M',
      '-Xms1024M',
      '-Xmx1024M',
      '-Dfile.encoding=UTF-8',
      '-Duser.country=US',
      '-Duser.language=en',
      '-Duser.variant',
      '-ea',
      '-cp',
      '/home/user/.gradle/caches/8.9/workerMain/gradle-worker.jar',
      'worker.org.gradle.process.internal.worker.GradleWorkerMain',
      "Gradle Test Executor 3",
    }
  }
  it("parse_jvm_entry 4", function()
    local exp = ps_entry_4 -- JUnit5 test runned in Grade
    assert.same(exp, M.parse_ps_entry(jvm_list[4]))
  end)

  it("match_jvm_name", function()
    local f = M.match_jvm_name
    local t = ps_entry_4
    assert.same(true, f(t.cmd, t.args, 'Test'))
    assert.same(true, f(t.cmd, t.args, 'Gradle Test Executor 3'))
    assert.same(true, f(t.cmd, t.args, 'gradle-worker.jar'))
    assert.same(false, f(t.cmd, t.args, '-cp'))
    assert.same(false, f(t.cmd, t.args, 'user.variant'))
  end)

  it("jvm_entry_to_short_stirng e1", function()
    local f = M.jvm_entry_to_short_string
    local exp = '533      0.2  3.5  03:44:09 /usr/lib/jvm/java8/bin/java ' ..
        'org.gradle.launcher.daemon.bootstrap.GradleDaemon 8.9'
    assert.same(exp, f(ps_entry_1))
  end)

  it("jvm_entry_to_short_stirng e2", function()
    local f = M.jvm_entry_to_short_string
    local exp = '615      0.3  2.5  01:38:27 /usr/lib/jvm/java/bin/java ' ..
        '-jar ~/Decomp/luyten-0.8.3.jar org.eclipse.jdt.ls.core_1.40.0.202409261450.jar'
    assert.same(exp, f(M.parse_ps_entry(jvm_list[2]) or {}, '/home/user'))

    local exp2 =
        '1291     3.0  3.5  09:31    /usr/lib/jvm/java17/bin/java ' ..
        '-jar ~/.local/share/jdtls//plugins/org.eclipse.equinox.launcher_1.6.900.v20240613-2009.jar'
    assert.same(exp2, f(M.parse_ps_entry(jvm_list[3]) or {}, '/home/user'))
  end)


  it("parse_java_version 8,17,21", function()
    local f = M.parse_java_version
    local lines_jdk8 = {
      'java version "1.8.0_351"',
      'Java(TM) SE Runtime Environment (build 1.8.0_351-b10)',
      'Java HotSpot(TM) 64-Bit Server VM (build 25.351-b10, mixed mode)'
    }
    assert.same("8", f(lines_jdk8, true))

    local lines_jdk17 = {
      'openjdk version "17.0.5" 2022-10-18 LTS',
      'OpenJDK Runtime Environment ...Vendor17.38+21-CA (build 17.0.5+8-LTS)',
      'OpenJDK 64-Bit Server VM Vendor17.38+21-CA (build 17.0.5+8-LTS, mixed mode, sharing)'
    }
    assert.same("17", f(lines_jdk17, true))

    local lines_jdk21 = {
      "openjdk 21.0.6 2025-01-21 LTS",
      "OpenJDK Runtime Environment Vendor21.40+17-CA (build 21.0.6+7-LTS)",
      "OpenJDK 64-Bit Server VM Vendor21.40+17-CA (build 21.0.6+7-LTS, mixed mode, sharing)"
    }
    assert.same("21", f(lines_jdk21, true))
  end)
end)

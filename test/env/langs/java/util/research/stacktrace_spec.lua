-- 16-02-2025 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'env.langs.java.util.research.stacktrace'

describe("env.langs.java.util.research.stacktrace", function()
  local stacktrace_lines = {
    'at org.app.launcher.daemon.server.exec.WatchForDisconnection.execute(WatchForDisconnection.java:39)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.ResetDeprecationLogger.execute(ResetDeprecationLogger.java:29)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.RequestStopIfSingleUsedDaemon.execute(RequestStopIfSingleUsedDaemon.java:35)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.ForwardClientInput.lambda$execute$0(ForwardClientInput.java:40)',
    'at org.app.internal.daemon.clientinput.ClientInputForwarder.forwardInput(ClientInputForwarder.java:80)',
    'at org.app.launcher.daemon.server.exec.ForwardClientInput.execute(ForwardClientInput.java:37)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.LogAndCheckHealth.execute(LogAndCheckHealth.java:64)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.LogToClient.doBuild(LogToClient.java:63)',
    'at org.app.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:37)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.EstablishBuildEnvironment.doBuild(EstablishBuildEnvironment.java:84)',
    'at org.app.launcher.daemon.server.exec.BuildCommandOnly.execute(BuildCommandOnly.java:37)',
    'at org.app.launcher.daemon.server.api.DaemonCommandExecution.proceed(DaemonCommandExecution.java:104)',
    'at org.app.launcher.daemon.server.exec.StartBuildOrRespondWithBusy$1.run(StartBuildOrRespondWithBusy.java:52)',
    'at org.app.launcher.daemon.server.DaemonStateCoordinator.lambda$runCommand$0(DaemonStateCoordinator.java:320)',
    'at org.app.internal.concurrent.ExecutorPolicy$CatchAndRecordFailures.onExecute(ExecutorPolicy.java:64)',
    'at org.app.internal.concurrent.AbstractManagedExecutor$1.run(AbstractManagedExecutor.java:48)',
    'at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149)',
    'at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624)',
    'at java.lang.Thread.run(Thread.java:750)'
  }
  local function mk_copy(lines)
    local t = {}
    for n, line in ipairs(lines) do
      t[n] = line
    end
    return t
  end

  it("compare_ufd same", function()
    local f = M.compare_ufd
    local exp = { { equals = true } }
    assert.same(exp, { f(stacktrace_lines, stacktrace_lines, "b2t") })
  end)

  it("compare_ufd b2t with diff at the bottom", function()
    local f = M.compare_ufd

    local st1 = mk_copy(stacktrace_lines)
    local st2 = mk_copy(stacktrace_lines)
    st1[#st1 - 4] = 'at org.app.different.place.Main(Main.java:123)'

    local exp2 = {
      {
        same_at = {
          from1 = 25, to1 = 21, from2 = 25, to2 = 21
        }
      }
    }
    assert.same(exp2, { f(st1, st2, "b2t") })
  end)

  it("compare_ufd b2t with diff at the top", function()
    local f = M.compare_ufd

    local st1 = mk_copy(stacktrace_lines)
    local st2 = mk_copy(stacktrace_lines)
    st1[1] = 'at org.app.different.place.Main(Main.java:123)'

    local exp2 = {
      { same_at = { from1 = 25, from2 = 25, to1 = 1, to2 = 1 } }
    }
    assert.same(exp2, { f(st1, st2, "b2t") })
  end)
end)

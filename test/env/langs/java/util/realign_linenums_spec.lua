-- 02-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local fs = require 'env.files'
local decomp_manager = require 'env.langs.java.util.decompiler.decomp_manager'
_G.TEST = true
local M = require 'env.langs.java.util.realign_linenums'

local file_exists, read_lines = fs.file_exists, fs.read_lines

local root_dir = os.getenv('PWD') .. '/test/env/resources/java/realign'

-- helper
local function decompile(classfile, outdir)
  local path, err = decomp_manager.decompile_classfile(classfile, {
    decompiler = 'jb_fernflower',
    output_dir = outdir,
    inner_path = fs.basename(classfile),
  })
  return path, err
end

local HEADER = M.HEADER
local FIELDS = M.FIELDS
local FIELD_ANNOTATIONS = M.FIELD_ANNOTATIONS

-- prepare:
--   - compile java -> class
--   - decompile class -> java
--   - generate javap output with line-numbers
local function prepare_class_file(classname)
  local javafile = root_dir .. '/' .. classname .. '.java'
  local classfile = root_dir .. '/compiled/pkg/' .. classname .. '.class'
  local decompiled_javafile = root_dir .. '/decompiled/' .. classname .. '.java'
  local outdir = root_dir .. '/decompiled/'

  assert(file_exists(javafile), 'has javafile to process')

  if not file_exists(decompiled_javafile) then
    -- compile
    if not file_exists(classfile) then
      local cmd = 'javac -d ' .. root_dir .. '/compiled/ ' .. javafile
      print(cmd)
      os.execute(cmd)
    end
    -- decompile
    local path, err = decompile(classfile, outdir)
    if not path then
      print(err)
    end

    -- produce line-number bytecode mapping by javap
    local outfile = root_dir .. '/decompiled/' .. classname .. '.javap'
    if not file_exists(outfile) then
      local cmd = 'javap -l ' .. classfile .. ' > ' .. outfile
      os.execute(cmd)
    end
  end
end


local function get_decompiled_javafile(classname)
  return root_dir .. '/decompiled/' .. classname .. '.java'
end
local function get_expected_javafile(classname)
  return root_dir .. '/expected/' .. classname .. '.java'
end


describe("env.langs.java.util.realign_linenums", function()
  --
  it("is_line_with_code", function()
    local f = M.is_line_with_code
    assert.same(false, f(""))
    assert.same(true, f("x"))
    assert.same(false, f("  "))
    assert.same(true, f("//"))   -- ??
    assert.same(true, f("/**/")) -- ??
  end)

  it("match", function()
    local line = '    }// 21'
    local a, b, c = string.match(line, '^(%s*).-(%}?)//%s*(%d+)')
    assert.same({ '    ', '}', '21' }, { a, b, c })
  end)

  it("is_constructor_def_ln", function()
    local f = M.is_constructor_def_ln
    assert.same(true, f('    public TestClass01() {'))
    assert.same(true, f('    private MyClass() {'))
    assert.same(true, f('    protected MyClass() {'))
    assert.same(true, f('    MyClass() {'))
    assert.same(false, f('    bad MyClass() {'))
    assert.same(false, f('    bad MyClass() {'))
    assert.same(false, f('    public static MyClass() {'))
    assert.same(false, f('    public private MyClass() {'))
    assert.same(false, f('    public private protected MyClass() {'))
    assert.same(false, f('    private protected MyClass() {'))
  end)

  it("is_java_type", function()
    local f = M.is_java_type
    assert.same(true, f('void'))
    assert.same(true, f('int'))
    assert.same(true, f('int[]'))
    assert.same(true, f('long'))
    assert.same(false, f('int[] '))
    assert.same(true, f('Map.Entry<String, Object>'))
    assert.same(false, f('Map!Entry<String, Object>'))
    assert.same(false, f('Map$Entry<String, Object>'))
    assert.same(false, f('new'))
    assert.same(false, f('try'))
  end)

  it("is_method_def_line", function()
    local f = M.is_method_def_line
    assert.same(true, f('    public void method() {'))
    assert.same(true, f('    public static void method() {'))
    assert.same(true, f('    public static final void method() {'))
    assert.same(true, f('    public void method() throws IOException {'))
    assert.same(true, f('    public int m() throws Ex1, Ex2 {'))
    assert.same(true, f('    public int[] method() {'))

    assert.same(true, f('    void method() {'))
    assert.same(true, f('    static void method() {'))
    assert.same(true, f('    static final void method() {'))
    assert.same(true, f('    void method() throws IOException {'))
    assert.same(true, f('    int m() throws Ex1, Ex2 {'))

    assert.same(true, f('    int[] m() throws Ex1, Ex2 {'))
    assert.same(true, f('    public int[] m() throws Ex1, Ex2 {'))
    assert.same(true, f('    public Class<?> method() {'))
    assert.same(true, f('    public Map.Entry<String, Object> method() {'))
    assert.same(true, f('    public Car.Engine method() {'))
    assert.same(true, f('    public void method(Car.Engine engine) {'))
    assert.same(true, f('    public void method(int[] engine) {'))
    assert.same(true, f('    public void m(Map.Entry<String, Object> map, int i) {'))
    assert.same(false, f('    public Map!Entry<String, Object> method() {'))

    -- issues: with mods
    assert.same(true, f('    public static final private void method() {'))
    assert.same(true, f('    static final private void method() {'))
  end)

  it("is_method_def_line signature only", function()
    local f = M.is_method_def_line
    assert.same(false, f('    public void method() ;', false))
    assert.same(true, f('    public void method() ;', true))
    assert.same(true, f('    public void method() {', true))
    assert.same(true, f('    public abstract void method() ;', true))
    assert.same(false, f('    new FileExplorer();', true))
  end)

  it("is_field_def", function()
    local f = M.is_field_def
    assert.same(true, f("    int x;"))
    assert.same(true, f("    public int x;"))
    assert.same(true, f("    public int aa;"))
    assert.same(true, f("    public int a$a;"))
    assert.same(true, f("    public int a_a;"))
    assert.same(true, f("    public int fieldname;"))
    assert.same(true, f("    public int fieldName;"))
    assert.same(true, f("    public static int fieldName;"))
    assert.same(true, f("    public static final int SIZE;"))
    assert.same(true, f("    public static final int SIZE = 4;"))
    assert.same(true, f("    public Class map;"))
    assert.same(true, f("    public Class<> map;"))
    assert.same(true, f("    public Class<?> map;"))
    assert.same(true, f("    public Map<String, Object> map;"))
    assert.same(true, f("    PUBLIC int fieldName;")) -- todo fix
    assert.same(false, f("    wrong modiff int fieldName;"))
    assert.same(false, f("   x x x;"))
  end)

  -- calculate "holes"
  it("get_free_lines_cnt", function()
    local f = M.get_free_holes
    local lines = { 'class A {', nil, nil, nil, ' public A {', '}' }
    local down, up = 1, -1
    assert.same(0, f(lines, 1, #lines, down))
    assert.same(3, f(lines, 2, #lines, down))
    assert.same(2, f(lines, 3, #lines, down))
    assert.same(1, f(lines, 4, #lines, down))
    assert.same(0, f(lines, 5, #lines, down))
    assert.same(3, f(lines, 4, 1, up))
    assert.same(0, f(lines, 5, 1, up))
    assert.same(1, f(lines, 2, 1, up))
  end)

  it("is_balanced_paretness", function()
    local f = M.is_balanced_paretness
    assert.same(true, f("()"))
    assert.same(true, f("  ()"))
    assert.same(false, f("("))
    assert.same(false, f('(")'))
    assert.same(true, f('(")")'))
    assert.same(false, f('(")"'))
    assert.same(true, f('(\\")\\"'))
    assert.same(false, f('(\\")\\")'))
    assert.same(true, f('()'))
    assert.same(true, f('(())'))
    assert.same(true, f('(()())'))
    assert.same(false, f('(()()'))
  end)

  it("is_annotation_def multiline and oneline field's annotations", function()
    local lines = {
      'public class A {',
      '    private int someFieldName;',
      '    @Parameter(',                        -- 3 multiline annotation head
      '        names = {"-memory"},',           -- 4
      '        description = "The size of..."', -- 5
      '    )',                                  -- 6
      '    private long minMemorySize;',
      '    @NotNull',                           -- 8 oneline annotation
      '    private Long value1;',
    }
    local t = {
      [FIELDS] = { 2 },
      block_list = {
        lnum_b = 1, lnum_e = nil,
      },
      depth = 1, -- inside class body
    }
    local f = M.is_annotation_def
    assert.same(false, f(t, lines[1], 1))
    assert.same(false, f(t, lines[2], 2))

    -- step 1 detect multiline annotation
    assert.same(true, f(t, lines[3], 3))
    local exp_st = {
      [FIELDS] = { 2 },
      block_list = { lnum_b = 1 },
      depth = 1,
      annotation_lnum_b = 3,
      annotation_open_ind = 4,
    }
    assert.same(exp_st, t)

    -- body of annotation
    assert.same(true, f(t, lines[4], 4))
    assert.same(true, f(t, lines[5], 5))
    -- close annotation
    assert.same(true, f(t, lines[6], 6))
    local exp_st2 = {
      [FIELDS] = { 2 },
      block_list = { lnum_b = 1 },
      depth = 1,
      annotation_lnum_b = nil,

      free_annotations = {
        [1] = { lnum_b = 3, lnum_e = 6 }
      }
    }
    assert.same(exp_st2, t)

    assert.same(false, f(t, lines[7], 7))

    -- one line annotation
    assert.same(true, f(t, lines[8], 8))
    local exp_st3 = {
      [FIELDS] = { 2 },
      block_list = { lnum_b = 1 },
      depth = 1,
      annotation_lnum_b = nil,

      free_annotations = {
        { lnum_b = 3, lnum_e = 6 }, -- multiline
        { lnum_b = 8, lnum_e = 8 }  -- oneline
      }
    }
    assert.same(exp_st3, t)
  end)

  --

  it("dir exists", function()
    assert.is_true(fs.dir_exists(root_dir))
  end)

  it("build_mapping_from_source_lines", function()
    local f = M.build_mapping_from_source_lines
    local exp = {
      [HEADER] = {},
      classdef_ln = -1,
      [FIELDS] = {},
      block_list = {},
      max_lnum = 0,
      map = {},
    }
    assert.same(exp, f({ 'a', 'b', 'c' }))

    exp = {
      [HEADER] = {},
      classdef_ln = -1,
      [FIELDS] = {},
      block_list = {},
      max_lnum = 4,
      map = { [2] = 2 },
    }
    assert.same(exp, f({ 'a', 'b// 2', 'c' }))

    exp = {
      [HEADER] = {},
      classdef_ln = -1,
      [FIELDS] = {},
      block_list = {},
      max_lnum = 5,
      map = { [2] = 3, [3] = 4 },
    }
    assert.same(exp, f({ 'a', 'b// 3', 'c// 4' }))
  end)

  prepare_class_file('TestClass01')

  it("grap mapping and realign TestClass01", function()
    local classname = 'TestClass01'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)

    local lines = read_lines(decompiled_javafile)

    local res_mapping = M.build_mapping_from_source_lines(lines)
    local exp_mapping = {
      [HEADER] = { 1, 3, 4, 5, 6 },
      classdef_ln = 11, -- origlnum
      [FIELDS] = { 9, 10, 11, 12, 13 },
      map = {
        [15] = -1,
        [16] = -1, -- hidden defualt constructor "anchor" to classdef linenum
        [18] = 29, -- calculated from anchor by first known line-num
        [19] = 30,
        [20] = 31,
        [21] = 32,
        [22] = 33,
        [23] = 34,
        [24] = 35,
        [26] = 40, -- calculated
        [27] = 41,
        -- [28] = 42, -- for } - will be calculated on realign
        [30] = 44, -- calculated
        [31] = 45,
        [32] = 46,
        -- [33] = 47, -- for }
        [35] = 49, -- calculated
        [36] = 50,
        -- [37] = 51, -- for }
        [39] = 53, -- calculated
        [40] = 54,
        [41] = 55,
        [43] = 21, -- calculated
        [44] = 22,
        [45] = 23,
        [46] = 24
      },
      max_lnum = 56,
      block_list = {
        { ind = 0, lnum_b = 8,  lnum_e = 47, anchor = nil },
        { ind = 4, lnum_b = 15, lnum_e = 16, anchor = 16 },
        { ind = 4, lnum_b = 18, lnum_e = 24, anchor = 19 },
        { ind = 4, lnum_b = 26, lnum_e = 28, anchor = 27, um = { 28 } },
        { ind = 4, lnum_b = 30, lnum_e = 33, anchor = 31, um = { 33 } },
        { ind = 4, lnum_b = 35, lnum_e = 37, anchor = 36, um = { 37 } },
        { ind = 4, lnum_b = 39, lnum_e = 41, anchor = 40 },
        { ind = 4, lnum_b = 43, lnum_e = 46, anchor = 44 }
      },
    }
    assert.same(exp_mapping, res_mapping)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)


  it("grap mapping and realign TestClass02", function()
    local classname = 'TestClass02'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)

    local lines = read_lines(decompiled_javafile)

    local res_mapping = M.build_mapping_from_source_lines(lines)
    local exp_mapping = {
      [HEADER] = { 1, 2, 3 },
      [FIELDS] = { 6, 7 },
      classdef_ln = 3,
      map = {
        [9] = -1,
        [10] = -1,
        [12] = 9,
        [13] = 10,
        [16] = 13,
        [17] = 14,
        [18] = 15,
        [20] = 17,
        [21] = 18,
        [24] = 21,
        [26] = 23,
      },
      max_lnum = 35,
      block_list = {
        { anchor = nil, ind = 0, lnum_b = 5,  lnum_e = 34, },
        { anchor = 10,  ind = 4, lnum_b = 9,  lnum_e = 10, },
        { anchor = 13,  ind = 4, lnum_b = 12, lnum_e = 14, um = { 14 } },
        { anchor = 17,  ind = 4, lnum_b = 16, lnum_e = 18, },
        { anchor = 21,  ind = 4, lnum_b = 20, lnum_e = 26, um = { 22, 23, 25, } },
        { anchor = nil, ind = 4, lnum_b = 28, lnum_e = 32, um = { 29, 30, 31, 32 } }
      }
    }
    assert.same(exp_mapping, res_mapping)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)


  it('realign ClassStaticConflict', function()
    local classname = 'ClassStaticConflict'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)
    local exp_mapping = {
      [HEADER] = { 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19 },
      [FIELDS] = { 22, 23, 24, 25 },
      block_list = {
        { anchor = nil, ind = 0, lnum_b = 21, lnum_e = 38 },
        { anchor = 28,  ind = 4, lnum_b = 27, lnum_e = 28 },
        { anchor = 31,  ind = 4, lnum_b = 30, lnum_e = 33, um = { 33 } },
        { anchor = 36,  ind = 4, lnum_b = 35, lnum_e = 37, um = { 37 } }
      },
      classdef_ln = 21,
      map = { -- decomp_lnum to orig_lnum
        [27] = -1,
        [28] = -1,
        [30] = 29,
        [31] = 30,
        [32] = 31,
        [35] = 25,
        [36] = 26,
      },
      max_lnum = 39
    }

    assert.same(exp_mapping, res_mapping)
    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)

  it('realign AppConfig', function()
    local classname = 'AppConfig'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)
    local exp_mapping = {
      [HEADER] = { 1, 3, 4, 5, 6, 7, 8 },
      [FIELDS] = { 11, 12, 13, 14, 19, 24, 29, 30 },
      [FIELD_ANNOTATIONS] = {
        [19] = { { lnum_b = 15, lnum_e = 18 } },
        [24] = { { lnum_b = 20, lnum_e = 23 } },
        [29] = { { lnum_b = 25, lnum_e = 28 } }
      },
      block_list = {
        { anchor = nil, ind = 0, lnum_b = 10,  lnum_e = 129, },
        { anchor = 33,  ind = 4, lnum_b = 32,  lnum_e = 34,  um = { 34 } },
        { anchor = nil, ind = 4, lnum_b = 36,  lnum_e = 39,  um = { 37, 38, 39 } },
        { anchor = 42,  ind = 4, lnum_b = 41,  lnum_e = 43,  um = { 43 } },
        { anchor = 46,  ind = 4, lnum_b = 45,  lnum_e = 47,  um = { 47 } },
        { anchor = 50,  ind = 4, lnum_b = 49,  lnum_e = 51,  um = { 51 } },
        { anchor = nil, ind = 4, lnum_b = 53,  lnum_e = 55,  um = { 54, 55 } },
        { anchor = nil, ind = 4, lnum_b = 57,  lnum_e = 59,  um = { 58, 59 } },
        { anchor = nil, ind = 4, lnum_b = 61,  lnum_e = 63,  um = { 62, 63 } },
        { anchor = nil, ind = 4, lnum_b = 65,  lnum_e = 95,  um = { 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 89, 90, 91, 92, 93, 94, 95 } },
        { anchor = nil, ind = 4, lnum_b = 97,  lnum_e = 99,  um = { 98, 99 } },
        { anchor = nil, ind = 4, lnum_b = 101, lnum_e = 111, um = { 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 } },
        { anchor = 114, ind = 4, lnum_b = 113, lnum_e = 115, um = { 115 } },
        { anchor = 118, ind = 4, lnum_b = 117, lnum_e = 118 },
        { anchor = 121, ind = 4, lnum_b = 120, lnum_e = 124, um = { 122, 123, 124 } },
        { anchor = 127, ind = 4, lnum_b = 126, lnum_e = 128, um = { 128 } }

      },
      classdef_ln = 20,
      map = {
        [32] = 36,
        [33] = 37,
        [41] = 22,
        [42] = 23,
        [45] = 45,
        [46] = 46,
        [49] = 47,
        [50] = 48,
        [113] = 14,
        [114] = 15,
        [117] = -1,
        [118] = -1,
        [120] = 16,
        [121] = 17,
        [126] = 53,
        [127] = 54,
        -- [128] = 53  -- conflict broken ln-mapping in static-block
      },
      max_lnum = 130
    }

    assert.same(exp_mapping, res_mapping)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)


  it('realign Class with mehtod Annotations', function()
    local classname = 'GuiceModule'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)


  it('realign Abstract class with abstract mehtod', function()
    local classname = 'AbstractClass'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)


  -- make sure that lines with code are not lost (not overwritten)
  it('realign Version$Metadata max_oln and append lines(no', function()
    local classname = 'Version$Metadata'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)

  -- annotated method with anotated paramentr of the method with trowns
  it('realign Annotation02', function()
    local classname = 'Annotation02'
    local decompiled_javafile = get_decompiled_javafile(classname)
    local expected_javafile = get_expected_javafile(classname)
    local lines = read_lines(decompiled_javafile)
    local res_mapping = M.build_mapping_from_source_lines(lines)

    local res = M.realign(lines, res_mapping)
    -- fs.write(expected_javafile, res) -- produce output
    local exp = fs.read_lines(expected_javafile)
    assert.same(exp, res)
  end)
end)

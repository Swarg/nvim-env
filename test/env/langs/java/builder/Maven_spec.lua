-- 15-07-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

-- local fs = require('env.files')
-- local uv = require("luv")

local class = require 'oop.class'
local Lang = require 'env.lang.Lang'
local MavenBuilder = require 'env.langs.java.builder.Maven'

------------------------ impl for specific lang --------------------------------
local H = require 'env.langs.java.ahelper'
local pkgApp0 = H.pkgApp0
-- local mkClassInfoAndGen = H.mkClassInfoAndGen
-- local rnf, new_cli = H.root_and_file, H.new_cli
--------------------------------------------------------------------------------
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


local default_pom_xmp = [[
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>pkg</groupId>
  <artifactId>app</artifactId>
  <version>0.1.0</version>
  <packaging>jar</packaging>

  <name>app</name>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <exec.mainClass>pkg.MainClass</exec.mainClass>
  </properties>

  <dependencies>
    <dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
      <version>4.13.2</version>
      <scope>test</scope>
		</dependency>
  </dependencies>

  <build>
    <pluginManagement>
    </pluginManagement>

    <plugins>

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.4.2</version>
        <configuration>
          <archive>
            <manifest>
              <addClasspath>true</addClasspath>
              <mainClass>pkg.MainClass</mainClass>
            </manifest>
          </archive>
        </configuration>
      </plugin>


    </plugins>
  </build>

</project>
]]


describe("env.langs.java.builder.Maven", function()
  it("project_root", function()
    local project_root = '/tmp/dir'
    local m = MavenBuilder:new(nil, project_root)
    assert.same('/tmp/dir/', m:getProjectRoot())
  end)

  it("isBuildScript", function()
    assert.same(true, MavenBuilder.isBuildScript("/tmp/proj/pom.xml"))
    assert.same(false, MavenBuilder.isBuildScript("/tmp/proj/build.xml"))
    assert.same(false, MavenBuilder.isBuildScript("/tmp/proj/build.gradle"))
  end)
end)


describe("env.langs.java.builder.Maven with emulated os", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)

  it("createBuildScript", function()
    local project_root = '/tmp/dev/proj_a'
    local pp = H.mkNewProjProps(project_root)
    local exp_default_proj_props = {
      UI_TITLE = 'New Java Project',
      POM_PARENT = '',
      PROJECT_NAME = 'app',
      GROUP_ID = 'pkg',
      ARTIFACT_ID = 'app',
      VERSION = '0.1.0',
      PACKAGING = 'jar',
      MAINCLASS = 'pkg.MainClass',
      JAVA_VERSION = '1.8',
      DEPENDENCIES = '',
      TEST_FRAMEWORK = "junit:junit:4.13.2",
      project_root = '/tmp/dev/proj_a'
    }
    assert.same(exp_default_proj_props, pp)
    local builder = MavenBuilder:new(nil, pp.project_root)

    -- require'alogger'.fast_setup(false, 0) -- trace level
    local cnt = builder:createBuildScript(pp, nil) -- ui.openFileCallback, force)
    assert.same(1, cnt)                            -- one file created
    local exp_created_files = 'ok: /tmp/dev/proj_a/pom.xml'
    assert.same(exp_created_files, builder:getFileWriter():getReadableReport())
    local exp_pom = default_pom_xmp
    local res = sys:get_file_content(project_root .. '/pom.xml')
    assert.same(exp_pom, res)
  end)


  it("parseBuildScript", function()
    local project_root = '/tmp/dev/proj_a'
    local builder = MavenBuilder:new(nil, project_root)

    local res = builder:parseBuildScript(project_root, default_pom_xmp, 0)

    local obj = {
      project = {
        -- _attr = {
        --   xmlns = "http://maven.apache.org/POM/4.0.0",
        --   ["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance",
        --   ["xsi:schemaLocation"] = "http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
        -- },
        -- modelVersion = "4.0.0",
        groupId = "pkg",
        artifactId = "app",
        version = "0.1.0",
        packaging = "jar",
        name = "app",
        properties = {
          ["exec.mainClass"] = "pkg.MainClass",
          ["maven.compiler.source"] = "1.8",
          ["maven.compiler.target"] = "1.8",
          ["project.build.sourceEncoding"] = "UTF-8"
        },
        dependencies = {
          dependency = {
            artifactId = "junit",
            groupId = "junit",
            version = "4.13.2",
            scope = "test"
          }
        },

        build = {
          pluginManagement = {},
          plugins = {
            plugin = {
              groupId = "org.apache.maven.plugins",
              artifactId = "maven-jar-plugin",
              version = "3.4.2",
              configuration = {
                archive = {
                  manifest = {
                    addClasspath = "true",
                    mainClass = "pkg.MainClass"
                  }
                }
              }
            }
          }
        }
      }
    }
    assert.same(obj, res)
  end)


  local function mkApp0Files()
    local a = pkgApp0
    assert.same(true, sys:set_file_content(a.pom_xml_path, a.pom_xml))
    assert.same(true, sys:set_file_content(a.mainclass_path, a.mainclass_body))
    return a.proj_root, a -- .mainclass_path
  end

  it("Maven resolve from source.java", function()
    local proj_root, a = mkApp0Files()

    local lang0 = H.JavaLang:new() -- temporary

    local lang = lang0:resolve(a.mainclass_path)
    assert.same(proj_root .. '/', (lang or E).project_root)
    assert.same(proj_root .. '/', (lang.builder or E).project_root)
    assert.same('env.langs.java.builder.Maven', class.name(lang.builder))

    local exp_lang = {
      ext = 'java',
      executable = 'java',
      project_root = '/tmp/dev/pr0j/',
      already_existed = false,
      no_cache = false,
      pkg_sep = '.',
      root_markers = {
        '.git',
        'pom.xml',
        'build.gradle',
        'build.gradle.kts',
        'build.xml',
        'Makefile'
      },
      src = 'src/main/java/',
      test = 'src/test/java/',
      test_suffix = 'Test',
      builder = {
        name = 'mvn',
        buildscript = 'pom.xml',
        buildscript_lm = 1700808888,
        project_root = '/tmp/dev/pr0j/',
        src = 'src/main/java/',
        test = 'src/test/java/',
        settings = {
          project = {
            artifactId = "app0",
            groupId = "pkg",
            version = "0.1.0"
          }
        },
      },
    }
    local res = lang:toArray()
    assert.same(exp_lang, res)
  end)

  it("JLang+Maven getOppositeClassInfo", function()
    local proj_root, a = mkApp0Files()
    local builder = MavenBuilder:new(nil, proj_root)
    local lang = H.JavaLang:new(nil, proj_root, builder)
    local sci = lang:resolveClass(a.mainclass_path, H.CT.SOURCE):getClassInfo()
    local exp = [[
(env.lang.ClassInfo) classname: pkg.Main
	path: /tmp/dev/pr0j/src/main/java/pkg/Main.java
	ipath: src/main/java/pkg/Main
	type: 1:Source]]
    assert.same(exp, v2s(sci)) ---@cast sci env.lang.ClassInfo

    local exp_path = '/tmp/dev/pr0j/src/main/java/pkg/Main.java'
    assert.same(exp_path, sci:getPath())

    assert.same(true, lang:getLangGen():isClassExists(a.mainclass))

    local tci = lang:getOppositeClassInfo(sci)
    assert.is_table(tci)
    assert.same('pkg.MainTest', tci:getClassName())
    assert.same('/tmp/dev/pr0j/src/test/java/pkg/MainTest.java', tci:getPath())
    assert.same('src/test/java/pkg/MainTest', tci:getInnerPath())
  end)
end)


describe("env.langs.java.builder.Maven", function()
  local spring_app_pom_xml = {
    project = {
      -- _attr = {
      --   xmlns = "http://maven.apache.org/POM/4.0.0",
      --   ["xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance",
      --   ["xsi:schemaLocation"] = "http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd"
      -- },
      -- modelVersion = "4.0.0",
      groupId = "org.swarg.springapp",
      artifactId = "mywebapp",
      packaging = "war",
      properties = {
        ["project.build.sourceEncoding"] = "UTF-8",
        ["maven.compiler.source"] = "1.8",
        ["maven.compiler.target"] = "1.8",
        ["hibernate-core.version"] = "5.6.15.Final",
        ["hibernate-validator.version"] = "6.2.5.Final",
        ["spring-data-jpa.version"] = "2.7.18",
        ["spring.version"] = "5.3.31",
        ["thymeleaf.version"] = "3.1.2.RELEASE",
        postgresql = "42.7.3",
      },
      version = "1.0",
      build = {
        finalName = "mywebapp",
        pluginManagement = {
          plugins = {
            plugin = {
              { artifactId = "maven-clean-plugin",     version = "3.1.0" },
              { artifactId = "maven-resources-plugin", version = "3.0.2" },
              { artifactId = "maven-compiler-plugin",  version = "3.8.0" },
              { artifactId = "maven-surefire-plugin",  version = "2.22.1" },
              { artifactId = "maven-war-plugin",       version = "3.2.2" },
              { artifactId = "maven-install-plugin",   version = "2.5.2" },
              { artifactId = "maven-deploy-plugin",    version = "2.8.2" },
              {
                groupId = "org.apache.tomcat.maven",
                artifactId = "tomcat7-maven-plugin",
                version = "2.2",
                configuration = {
                  path = "/mywebapp",
                  server = "maven-tomcat-war-deployment-server",
                  url = "http://localhost:8080/manager/text"
                }
              }
            }
          }
        }
      },
      dependencies = {
        dependency = { {
          artifactId = "javax.servlet-api",
          groupId = "javax.servlet",
          scope = "provided",
          version = "4.0.1"
        }, {
          artifactId = "spring-core",
          groupId = "org.springframework",
          version = "${spring.version}"
        }, {
          artifactId = "spring-context",
          groupId = "org.springframework",
          version = "${spring.version}"
        }, {
          artifactId = "spring-web",
          groupId = "org.springframework",
          version = "${spring.version}"
        }, {
          artifactId = "spring-webmvc",
          groupId = "org.springframework",
          version = "${spring.version}"
        }, {
          artifactId = "spring-jdbc",
          groupId = "org.springframework",
          version = "${spring.version}"
        }, {
          artifactId = "spring-data-jpa",
          groupId = "org.springframework.data",
          version = "${spring-data-jpa.version}"
        }, {
          artifactId = "thymeleaf-spring5",
          groupId = "org.thymeleaf",
          version = "${thymeleaf.version}"
        }, {
          artifactId = "hibernate-validator",
          groupId = "org.hibernate.validator",
          version = "${hibernate-validator.version}"
        }, {
          artifactId = "hibernate-core",
          groupId = "org.hibernate",
          version = "${hibernate-core.version}"
        }, {
          artifactId = "postgresql",
          groupId = "org.postgresql",
          version = "${postgresql}"
        }, {
          artifactId = "junit",
          groupId = "junit",
          scope = "test",
          version = "4.11"
        } }
      }
    }
  }

  it("isWebApp", function()
    local builder = MavenBuilder:new({ settings = spring_app_pom_xml })
    assert.same(true, builder:isWebApp())
  end)

  it("getPlugin", function()
    local builder = MavenBuilder:new({ settings = spring_app_pom_xml })
    local groupId = "org.apache.tomcat.maven"
    local artifactId = "tomcat7-maven-plugin"
    local exp = {
      groupId = 'org.apache.tomcat.maven',
      artifactId = 'tomcat7-maven-plugin',
      version = '2.2',
      configuration = {
        path = '/mywebapp',
        server = 'maven-tomcat-war-deployment-server',
        url = 'http://localhost:8080/manager/text'
      }
    }
    assert.same(exp, builder:getPlugin(groupId, artifactId))
  end)
end)

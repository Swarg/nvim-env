-- 18-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local Lang = require 'env.lang.Lang'
local Ant = require 'env.langs.java.builder.Ant'

local H = require 'env.langs.java.ahelper'
local pkgApp0 = H.pkgApp0


describe("env.langs.java.builder.Ant with emulated os", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)

  it("createBuildScript", function()
    local project_root = pkgApp0.proj_root
    local pp = H.mkNewProjProps(project_root)
    local exp_default_proj_props = {
      UI_TITLE = 'New Java Project',
      POM_PARENT = '',
      PROJECT_NAME = 'app',
      GROUP_ID = 'pkg',
      ARTIFACT_ID = 'app',
      VERSION = '0.1.0',
      PACKAGING = 'jar',
      MAINCLASS = 'pkg.MainClass',
      JAVA_VERSION = '1.8',
      DEPENDENCIES = '',
      TEST_FRAMEWORK = 'junit:junit:4.13.2',
      project_root = H.pkgApp0.proj_root
    }
    assert.same(exp_default_proj_props, pp)
    local builder = Ant:new(nil, pp.project_root)

    -- require'alogger'.fast_setup(false, 0) -- trace level
    local cnt = builder:createBuildScript(pp, nil) -- ui.openFileCallback, force)
    assert.same(1, cnt)                            -- one file created
    local exp_created_files = 'ok: /tmp/dev/pr0j/build.xml'
    assert.same(exp_created_files, builder:getFileWriter():getReadableReport())
    local exp_build_xml = pkgApp0.build_xml
    assert.same(exp_build_xml, sys:get_file_content(project_root .. '/build.xml'))
  end)


  it("parseBuildScript pkgApp0", function()
    local ant = Ant:new()
    local res = ant:parseBuildScript('build.xml', pkgApp0.build_xml, 0)
    local exp = {
      project = {
        name = 'app',
        property = {
          ['build.dir'] = 'build',
          ['classes.dir'] = '${build.dir}/classes',
          ['jar.dir'] = '${build.dir}/jar',
          ['main-class'] = 'pkg.MainClass',
          ['src.dir'] = 'src'
        }
      }
    }
    assert.same(exp, res)
  end)
end)

describe("env.langs.java.builder.Ant", function()
  it("isBuildScript", function()
    assert.same(true, Ant.isBuildScript("/tmp/proj/build.xml"))
    assert.same(false, Ant.isBuildScript("/tmp/proj/build.gradle"))
    assert.same(false, Ant.isBuildScript("/tmp/proj/pom.xml"))
  end)
end)

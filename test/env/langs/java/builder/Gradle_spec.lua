-- 18-07-2024 @authogr Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local Lang = require 'env.lang.Lang'
local Builder = require 'env.lang.Builder'
local Gradle = require 'env.langs.java.builder.Gradle'
local JavaBuilder = require 'env.langs.java.JBuilder'
local JavaLang = require 'env.langs.java.JLang'

local H = require 'env.langs.java.ahelper'
-- local pkgApp0 = H.pkgApp0



describe("env.langs.java.builder.Gradle oop toolings", function()
  before_each(function()
    nvim.logger_off()
    Lang.clearCache()
  end)

  it("isBuildScript", function()
    assert.same(true, Gradle.isBuildScript("/tmp/proj/build.gradle"))
    assert.same(true, Gradle.isBuildScript("/tmp/proj/build.gradle.kts"))
    assert.same(true, Gradle.isBuildScript("/tmp/proj/settings.gradle"))
  end)

  it("new instance", function()
    local project_root = '/home/dev/project/'
    local g = Gradle:new({ project_root = project_root })
    assert.same('gradle', g:getName())
    assert.same(project_root .. 'build.gradle', g:getBuildScriptPath())
    assert.same(project_root, g:getProjectRoot())
    assert.is_nil(g:getSubProjects())
  end)

  it("get class", function()
    ---@diagnostic disable-next-line: undefined-field
    local g = Gradle:new()
    local class = getmetatable(g) -- getClass()
    assert.same('gradle', g:getName())
    assert.same(Gradle, class)
    assert.same(true, Gradle == class)
    local t1 = { a = 'b' }
    local t2 = { a = 'b' }
    assert.same(t1, t2)
    assert.same(false, t1 == t2)
  end)

  it("instanceof", function()
    ---@diagnostic disable-next-line: undefined-field
    local g = Gradle:new()
    -- local class = getmetatable(g) -- getClass()
    local function instance_of(instance, class)
      if instance and class then
        local i = 1
        while instance and i < 64 do
          local obj_class = getmetatable(instance)
          if obj_class == class then return true end
          instance = obj_class
          i = i + 1
        end
      end
      return false
    end
    assert.is_true(instance_of(g, Gradle))
    assert.is_true(g:instanceof(Gradle))
    assert.is_true(instance_of(g, JavaBuilder))
    assert.is_true(g:instanceof(JavaBuilder))

    assert.is_true(instance_of(g, Builder))
    assert.is_true(g:instanceof(Builder))

    ---@class SomeClass
    local SomeClass = {}
    assert.is_false(instance_of(g, SomeClass))
    assert.is_false(g:instanceof(SomeClass))
  end)

  -- work with test/resources
end)

--

describe("env.langs.java.builder.Gradle with emulated FS", function()
  before_each(function()
    nvim.logger_off()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)


  it("getSubProjects - parse settings.gradle", function()
    local project_root = "/tmp/dev/pr0j/"
    local settings_gradle_path = project_root .. "settings.gradle"
    local settings_gradle_body = {
      "rootProject.name = 'gr_project'",
      "// subprojects",
      "include('app', 'utilities')",
      "//include 'app'",
      "//include 'utilities'"
    }

    sys:set_file_content(settings_gradle_path, settings_gradle_body)
    -- todo subrojects see resources:
    -- local path = './test/env/resources/gradle/settings.gradle'
    -- local project_root = './test/env/resources/gradle/' -- resources

    local g = Gradle:new(nil, project_root)
    assert.same(project_root, g:getProjectRoot())

    assert.same({ 'app', 'utilities' }, g:getSubProjects())
  end)


  it("createTestFile", function()
    local project_root = '/tmp/nvim-env/test/gradle/'
    assert.same(1, sys:mk(project_root))
    local exp = [[
/tmp/nvim-env/test/gradle/
0 directories, 0 files
]]
    assert.same(exp, sys:tree(project_root))
    -- assert.is_true(fs.mkdir(project_root))
    ---@diagnostic disable-next-line: undefined-field
    local g = Gradle:new({ project_root = project_root })
    local lang = JavaLang:new(nil, project_root, g)
    assert.equal(g, lang.builder)
    lang:syncSrcAndTestPathsWithBuilder()

    assert.same(project_root, g:getProjectRoot())
    assert.same(project_root, lang:getProjectRoot())

    local class_name = 'io.pkg.MainTest'
    -- if fs.file_exists(exp) then os.remove(exp) end          --  cleanup
    -- assert.same(exp, g:createTestFile(class_name, {}))      -- old way

    -- resolve inner path  in the project and full path
    local tci = lang:lookupByClassName(class_name) --, H.CT.TEST)
    assert.is_table(tci) ---@cast tci table
    local exp_path = '/tmp/nvim-env/test/gradle/src/test/java/io/pkg/MainTest.java'
    assert.same(exp_path, tci:getPath())
    local expip = 'src/test/java/io/pkg/MainTest'
    assert.same(expip, tci:getInnerPath())
    assert.same('io.pkg.MainTest', tci:getClassName())
    assert.same('Test', tci:getTypeName())

    local opts = { no_date = 1, author = 'A' }
    local path, errmsg = lang:generateTestFile(tci, opts)
    assert.same(nil, errmsg)
    assert.same(exp_path, path)
    local exp_testclass_body = [[
package io.pkg;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author A
 */
public class MainTest {

    @Test
    public void testSuccess() {
        System.out.println("testSuccess");
          = new ();
        assertEquals(1, );
    }

}
]]
    assert.same(exp_testclass_body, sys:get_file_content(exp_path))
  end)
end)



describe('GradleBuilder generation', function()
  it("create build scripts", function()
    local project_root = '/tmp/nvim-env/gradle/'

    local g = Gradle:new(nil, project_root)
    assert.same(project_root, g:getProjectRoot())
    g.USER = 'name'

    local pp = JavaBuilder.getDefaultProjectProperties()
    assert.same('pkg', pp['GROUP_ID'])
    assert.same('app', pp['PROJECT_NAME'])
    -- assert.is_true(fs.dir_exists(project_root) or fs.mkdir(project_root))
    --
    -- clear already exited files if has
    -- clear_build_scripts(g)
    assert.same(3, g.createBuildScript(g, pp)) -- static method of Builder
    local exp = [[
ok: /tmp/nvim-env/gradle/settings.gradle
ok: /tmp/nvim-env/gradle/gradle.properties
ok: /tmp/nvim-env/gradle/build.gradle
]]
    assert.same(exp, g:getFileWriter():getReadableReport())

    local cnt = 0
    local templates = g:getBuildScriptTemplate()

    -- check created build_scripts files
    -- 3 scripts files: build.gradle, settings.gradle, gradle.properties
    for build_script_name, templ in pairs(templates) do
      local fn = g.project_root .. build_script_name
      assert.is_true(sys:is_file_exits(fn))
      -- check created content
      local content_lines = sys:get_file_lines(fn)
      assert.is_not_nil(content_lines)
      local templ_lines = H.split(templ, '\n')
      assert.is_not_nil(templ_lines)
      assert.is_true(#templ_lines <= #content_lines)

      cnt = cnt + 1
      -- finally(function()
      --   -- print('Cleanup ' .. fn)
      --   if fs.file_exists(fn) then os.remove(fn) end
      -- end)
    end
    assert.same(3, cnt)
  end)
end)

-- 07-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local log = require 'alogger'
local ut = require 'env.util.tables'
local Editor = require 'env.ui.Editor'
local JParser = require 'env.langs.java.JParser'
local H = require 'env.langs.java.ahelper'

-- require 'alogger'.fast_setup()
-- require 'dprint'.enable_module(JParser.G)


---@diagnostic disable: invisible
describe("env.langs.java.JParser", function()
  before_each(function()
    log.fast_off()
    nvim:clear_state()
    -- sys:clear_state()
    -- sys.fs:clear_state()
  end)


  local function newParser(lines, lnum, col, offset)
    local bufnr = nvim:new_buf(ut.flat_copy(lines), lnum, col)
    offset = offset or 0
    return JParser:new(nil, Editor.iterator(bufnr, offset), offset), bufnr
  end

  it("parse to the line with package", function()
    local lines = {
      'package org.some.app;',         -- 1
      'import pkg.util.A;',            -- 2
      'public class A {',              -- 3
      '    public A(int x, int y) {}', -- 4
      '}'                              -- 5
    }
    local p = newParser(lines, 0, 1)

    -- require 'alogger'.fast_setup(false, 0)
    local res = p:parseClassHeader(p.predicate_stop_on.package_def)
    local exp = {
      clazz = {
        pkg = 'org.some.app', pkg_ln = 1, imports = {}, import_lns = {}
      },
      curr_line = 'package org.some.app;',
      iter = {
        o = {
          bufnr = 1,
          current_line = 'package org.some.app;',
          line_count = 5,
          lnum = 1
        }
      },
      last_elm = { 'PACKAGE', 'org.some.app', 1 },
      lnum = 1,
      stopped_at_lnum = 1,
      stopped_line = 'package org.some.app;'
    }
    assert.same(exp, res)
  end)

  it("parse with import static", function()
    local lines = {
      'package org.some.app;',         -- 1
      'import pkg.util.A;',            -- 2
      'import static pkg.util.A.*;',   -- 3
      'public class A {',              -- 4
      '    public A(int x, int y) {}', -- 5
      '}'                              -- 6
    }
    local p = newParser(lines, 0, 1)

    local res = (p:parseClassHeader(p.predicate_stop_on.classdef) or {}).clazz
    local exp = {
      import_lns = { 2, 3 },
      imports = { 'pkg.util.A', 'static pkg.util.A.*' },
      ln = 4,
      mods = 1,
      name = 'A',
      pkg = 'org.some.app',
      pkg_ln = 1,
      typ = 'class'
    }
    assert.same(exp, res)
  end)

  it("parseConstructors", function()
    local lines = {
      'package pkg; import pkg.util.A;', -- 1
      'public class A {',                -- 2
      '    public A(int x, int y) {}',   -- 3
      '}'                                -- 4
    }
    local p = newParser(lines, 0, 1)

    local res = p:prepareConstructors().clazz
    local exp = {
      pkg = "pkg",
      imports = { "pkg.util.A" },
      pkg_ln = 1,
      import_lns = { 1 },
      mods = 1,
      name = "A",
      typ = "class",
      ln = 2,
      constructors = {
        {
          mods = 1,
          name = "A",
          params = { { "int", "x" }, { "int", "y" } },
          ln = 3,
          lne = 3,
        }
      }
    }
    assert.same(exp, res)
  end)



  it("parseClassConstructors", function()
    local p = newParser(H.code_spring_MController, 27, 0, 24)
    assert.same('    @Autowired', p:_next_line()) -- 26 line
    -- assert.same('    public MController(MService msService,', p:_next_line())
    p.clazz = { constructors = {} }

    p:parseClassBody(p.predicate_stop_on.fist_method)

    local exp_constructors = {
      { -- first
        mods = 1,
        name = "MController",
        ln = 27,
        params = {
          { "MService",    "msService" },
          { "MValidator",  "mValidator" },
          { "ModelMapper", "modelMapper" },
          lne = 29
        },
        lne = 33, -- end of constructor body
      },

      { --second constructor (Empty)
        mods = 1,
        name = "MController",
        params = {},
        ln = 35,
        lne = 35,
      }
    }
    assert.same(exp_constructors, p.clazz.constructors)
  end)


  describe("tooling", function()
    it("_next_line_up_to Annotation multiline definition", function()
      local lines = {
        'package org.some;',
        '@Annot(key=Elm.TYPE',
        '       kb="str"',
        '       c=88)',
        'public class MyClass {',
        '}'
      }
      local p = newParser(lines, 1, 0, 0)

      local res = p:parseClassHeader().clazz
      local exp = {
        annotations = {
          {
            name = "Annot",
            params = { { "key", "Elm.TYPE" }, { "kb", '"str"' }, { "c", "88" } },
            ln = 2,
            lne = 4,
          }
        },
        imports = {},
        import_lns = {},
        ln = 5,
        mods = 1,
        pkg = 'org.some',
        pkg_ln = 1,
        name = "MyClass",
        typ = "class",
      }
      assert.same(exp, res)
    end)


    it("_next_line_up_to Annotation multiline definition empty", function()
      local lines = {
        'package org.some;',
        '@Annot(',
        ')',
        'public class MyClass {', '}'
      }
      local p = newParser(lines, 1, 0, 0)

      local res = p:parseClassHeader().clazz
      local exp = {
        pkg = 'org.some',
        imports = {},
        typ = 'class',
        name = 'MyClass',
        ln = 4,
        mods = 1,
        annotations = { { ln = 2, lne = 3, name = 'Annot' } },
        pkg_ln = 1,
        import_lns = {},
      }
      assert.same(exp, res)
    end)

    it("_next_line_up_to Annotation multiline definition empty", function()
      local lines = {
        'package org.some;',
        '@Annot(k=v,',
        'b=9)',
        'public class MyClass {', '}'
      }
      local p = newParser(lines, 1, 0, 0)
      local res = p:parseClassHeader().clazz
      local exp = {
        pkg = 'org.some',
        imports = {},
        name = 'MyClass',
        typ = 'class',
        mods = 1,
        ln = 4,
        annotations = {
          {
            name = 'Annot',
            params = { { 'k', "v", }, { "b", "9" } },
            ln = 2,
            lne = 3,
          }
        },
        pkg_ln = 1,
        import_lns = {},
      }
      assert.same(exp, res)
    end)
  end)



  it("parseClassHeader", function()
    local p = newParser(H.code_spring_AuthController, 7, 25)

    local res = p:parseClassHeader().clazz
    local exp = {
      pkg = 'org.springcourse.fsa.controller',
      pkg_ln = 1,
      typ = 'class',
      name = 'AuthController',
      mods = 1,
      ln = 9, -- line number of a class definition
      imports = {
        'org.springframework.beans.factory.annotation.Autowired',
        'org.springframework.validation.BindingResult',
        'org.springframework.web.bind.annotation.*'
      },
      import_lns = { 3, 4, 5 },
      annotations = {
        { ln = 7, name = 'RestController' },
        { ln = 8, name = 'RequestMapping', params = { "value", '"/auth"' } }
      }
    }
    assert.same(exp, res)
  end)



  it("parseClassHeader public MyClass {}", function()
    local lines = {
      'package pkg;', 'import pkg.util.A; import pkg.utilB;',
      'import pkg.c.App;',
      'public class MyClass {}'
    }
    local p = newParser(lines, 7, 25)
    local res = p:parseClassHeader().clazz
    local exp = {
      pkg = 'pkg',
      pkg_ln = 1,
      typ = 'class',
      name = 'MyClass',
      mods = 1,
      ln = 4,
      imports = { 'pkg.util.A', 'pkg.utilB', 'pkg.c.App' },
      import_lns = { 2, 2, 3 }
    }
    assert.same(exp, res)
  end)

  it("parseClassHeader public MyClass", function()
    local lines = {
      'package pkg;',
      'public class MyClass ',
      '{', '}',
    }
    local p = newParser(lines, 7, 25)
    local res = p:parseClassHeader().clazz
    local exp = {
      import_lns = {},
      imports = {},
      ln = 2,
      lne = 3,
      mods = 1,
      name = 'MyClass',
      pkg = 'pkg',
      pkg_ln = 1,
      typ = 'class'
    }
    assert.same(exp, res)
  end)

  it("parseClassHeader public enum", function()
    local lines = {
      'package org.comp;',
      'public enum MyClass {}'
    }
    local p = newParser(lines, 7, 25)
    local res = p:parseClassHeader().clazz
    local exp = {
      pkg = 'org.comp',
      typ = 'enum',
      name = 'MyClass',
      mods = 1,
      ln = 2,
      imports = {},
      pkg_ln = 1,
      import_lns = {},
    }
    assert.same(exp, res)
  end)


  -- Fields are not parsed throughout the entire class,
  -- but only up to the first constructor
  it("parseClassHeader", function()
    local p = newParser(H.code_spring_AuthController, 7, 25)
    local res = p:parseClassHeader().clazz
    local exp = {
      ln = 9,
      mods = 1,
      name = "AuthController",
      pkg = "org.springcourse.fsa.controller",
      typ = "class",
      annotations = { { name = "RestController", ln = 7, },
        { name = "RequestMapping", params = { "value", '"/auth"' }, ln = 8, }
      },
      imports = { "org.springframework.beans.factory.annotation.Autowired", "org.springframework.validation.BindingResult", "org.springframework.web.bind.annotation.*" },
      pkg_ln = 1,
      import_lns = { 3, 4, 5 },
    }
    assert.same(exp, res)
    assert.same(9, p:getLnum())
    assert.same({}, { p:getStoppedAt() })
  end)

  -- Fields are not parsed throughout the entire class,
  -- but only up to the first constructor
  it("parseClassFileds", function()
    local p = newParser(H.code_spring_AuthController, 7, 25)
    local res = p:prepareClassFields().clazz
    local exp = {
      pkg = "org.springcourse.fsa.controller",
      imports = {
        "org.springframework.beans.factory.annotation.Autowired",
        "org.springframework.validation.BindingResult",
        'org.springframework.web.bind.annotation.*'
      },
      annotations = {
        { name = "RestController", ln = 7 },
        { name = "RequestMapping", params = { "value", '"/auth"' }, ln = 8, }
      },
      mods = 1,
      name = "AuthController",
      typ = "class",
      ln = 9,
      pkg_ln = 1,
      import_lns = { 3, 4, 5 },

      fields = {
        { mods = 18, typ = "PersonValidator",     name = "personValidator",     ln = 11, },
        { mods = 18, typ = "RegistrationService", name = "registrationService", ln = 12, }
      },
      constructors = {
        {
          annotations = { { name = "Autowired", ln = 14, } },
          ln = 15,
          lne = 18,
          mods = 1,
          name = "AuthController",
          params = {
            { "PersonValidator", "personValidator" }, { "RegistrationService", "registrationService" }
          }
        }
      }
    }
    assert.same(exp, res)
  end)


  it("prepareClassFields", function()
    local p = newParser(H.code_spring_AuthController, 7, 25)

    local res = p:prepareClassFields().clazz
    local exp = {
      pkg = 'org.springcourse.fsa.controller',
      typ = 'class',
      name = 'AuthController',
      mods = 1,
      ln = 9, -- line number of a class definition
      imports = {
        'org.springframework.beans.factory.annotation.Autowired',
        'org.springframework.validation.BindingResult',
        'org.springframework.web.bind.annotation.*'
      },
      pkg_ln = 1,
      import_lns = { 3, 4, 5 },
      annotations = { -- for class
        { ln = 7, name = 'RestController' },
        { ln = 8, name = 'RequestMapping', params = { "value", '"/auth"' } }
      },
      fields = {
        {
          name = "personValidator",
          typ = "PersonValidator",
          mods = 18,
          ln = 11,
        },
        {
          name = "registrationService",
          typ = "RegistrationService",
          mods = 18,
          ln = 12,
        }
      },
      -- fileds parsed only to first constructor
      constructors = {
        {
          annotations = { { name = "Autowired", ln = 14, } }, -- for constructork
          name = "AuthController",                            -- class name
          params = {
            { "PersonValidator",     "personValidator" },
            { "RegistrationService", "registrationService" }
          },
          mods = 1,
          ln = 15,
          lne = 18,
        }
      }
    }
    assert.same(exp, res)
  end)

  --

  it("parseClassConstructors", function()
    local p = newParser(H.code_spring_MController, 27, 0, 24)
    assert.same('    @Autowired', p:_next_line()) -- 26 line
    -- assert.same('    public MController(MService msService,', p:_next_line())
    p.clazz = { constructors = {} }

    p:parseClassBody(p.predicate_stop_on.fist_method)

    local exp_constructors = {
      {
        mods = 1,
        name = "MController",
        ln = 27,
        params = {
          { "MService",    "msService" },
          { "MValidator",  "mValidator" },
          { "ModelMapper", "modelMapper" },
          lne = 29
        },
        lne = 33, -- end of constructor body
      },
      {
        mods = 1, name = 'MController', params = {}, ln = 35, lne = 35,
      }
    }

    assert.same(exp_constructors, p.clazz.constructors)
  end)


  it("parseConstructors", function()
    local p = newParser(H.code_spring_AuthController, 7, 25)

    local res = p:prepareConstructors().clazz
    local exp = {
      pkg = "org.springcourse.fsa.controller",
      imports = {
        "org.springframework.beans.factory.annotation.Autowired",
        "org.springframework.validation.BindingResult",
        'org.springframework.web.bind.annotation.*',
      },
      pkg_ln = 1,
      import_lns = { 3, 4, 5 },
      annotations = { -- for class
        { name = "RestController", ln = 7 },
        { name = 'RequestMapping', params = { "value", '"/auth"' }, ln = 8 }
      },
      mods = 1,
      name = "AuthController",
      typ = "class",
      ln = 9,

      fields = {
        { mods = 18, typ = "PersonValidator",     name = "personValidator",     ln = 11, },
        { mods = 18, typ = "RegistrationService", name = "registrationService", ln = 12, }
      },
      constructors = {
        {
          annotations = { { name = "Autowired", ln = 14, } }, -- for constructor
          name = "AuthController",
          mods = 1,
          params = {
            { "PersonValidator",     "personValidator" },
            { "RegistrationService", "registrationService" }
            --, lne = 15
          },
          ln = 15,  -- lnum with begining of constructor signature
          lne = 18, -- end of constructor body
        }
      }
    }
    assert.same(exp, res)
  end)


  it("prepareConstructors(parsing)", function()
    local p = newParser(H.code_spring_MController, 0, 1)
    local res = p:prepareConstructors().clazz
    local exp = {
      pkg = "org.springcourse.p3.controller",
      mods = 1,
      name = "MController",
      typ = "class",
      ln = 21,
      imports = {
        "java.util.stream.Collectors",
        "org.modelmapper.ModelMapper",
        "org.springframework.beans.factory.annotation.Autowired",
        "org.springframework.http.HttpStatus",
        "org.springframework.http.ResponseEntity",
        "org.springframework.validation.BindingResult",
        "org.springframework.web.bind.annotation.*",
        'org.springcourse.p3.dto.*',
        "jakarta.validation.Valid"
      },
      pkg_ln = 1,
      import_lns = { 3, 5, 6, 7, 8, 9, 10, 12, 14 },
      -- for class
      annotations = { { name = "RestController", ln = 19, },
        { name = "RequestMapping", params = { "value", '"/ms"' }, ln = 20, }
      },
      fields = {
        { mods = 18, name = "msService",   typ = "MService",    ln = 22, },
        { mods = 18, name = "mValidator",  typ = "MValidator",  ln = 23, },
        { mods = 18, name = "modelMapper", typ = "ModelMapper", ln = 24, }
      },
      constructors = {
        {
          annotations = { { name = "Autowired", ln = 26, } },
          mods = 1,
          name = "MController",
          ln = 27,
          lne = 33,
          params = {
            { "MService",    "msService" },
            { "MValidator",  "mValidator" },
            { "ModelMapper", "modelMapper" },
            lne = 29
          }
        }
      }
    }
    assert.same(exp, res)
  end)

  it("getFirstNotEmptyConstructor", function()
    local lines = {
      'package pkg;',        --  1
      'public class A {',    --  2
      '  private int x;',    --  3
      '  public A() {',      --  4
      '  }',                 --  5
      '  public A(int x) {', --  6
      '    self.x = x;',     --  7
      '  }',                 --  8
      '}'                    --  9
    }
    local p = newParser(lines, 0, 1)
    local predicate = function() return false end
    local clazz = p:prepareMethods(predicate).clazz

    local exp_clazz = {
      pkg = "pkg",
      imports = {},
      mods = 1,
      name = "A",
      typ = "class",
      ln = 2,
      lne = 9,
      pkg_ln = 1,
      import_lns = {},
      fields = {
        { mods = 2, typ = "int", name = "x", ln = 3, }
      },
      constructors = {
        { mods = 1, name = "A", params = {},                 ln = 4, lne = 5, },
        { mods = 1, name = "A", params = { { "int", "x" } }, ln = 6, lne = 8, }
      },
    }
    assert.same(exp_clazz, clazz)
    local e = p:getFirstNotEmptyConstructor()
    local exp = {
      mods = 1, name = 'A', params = { { 'int', 'x' } }, ln = 6, lne = 8,
    }
    assert.same(exp, e)
    -- local f = M.getFirstNotEmptyConstructor
  end)

  -- todo methods
  -- multiparam annotations
  it("getFirstNotEmptyConstructor", function()
    local p = newParser(H.code_spring_PersonDTO, 0, 1)
    local clazz = p:prepareMethods().clazz

    local exp = {
      imports = { "javax.validation.constraints.*" },
      ln = 8,
      lne = 28,
      mods = 1,
      name = "PersonDTO",
      pkg = "org.springcourse.fsa.dto",
      typ = "class",
      pkg_ln = 1,
      import_lns = { 3 },
      --
      fields = {
        { -- >> field1
          annotations = {
            {
              name = "NotEmpty",
              params = { { "message", '"Name should not be empty"' } },
              ln = 10,
            },
            {
              name = "Size",
              params = {
                { "min",     "3" }, { "max", "100" },
                { "message", '"Name should be between 3 and 100 characters"' }
              },
              ln = 11,
            }
          },
          mods = 2,
          name = "username",
          typ = "String",
          ln = 12,
        }, -- << field 1
        {  -- >> field 2
          annotations = {
            {
              ln = 14,
              name = "Min",
              params = { { "value", "1900" },
                { "message", '"Year of birth should be greater than 1900"' }
              }
            }
          },
          mods = 2,
          name = "yearOfBirth",
          typ = "int",
          ln = 15,
        },
        {
          ln = 17,
          mods = 2,
          name = "password",
          typ = "String"
        }
      },

      constructors = {
        -- empty constructor
        { mods = 1, name = "PersonDTO", params = {}, ln = 19, lne = 20, },
        {
          mods = 1,
          name = "PersonDTO",
          params = {
            { "String", "username" }, { "int", "yearOfBirth" },
            { "String", "password" }
          },
          ln = 22,
          lne = 26,
        }
      }
    }
    assert.same(exp, clazz)
  end)
end)

describe("env.langs.java.JParser full class", function()
  local IteratorImpl = require 'olua.util.IteratorImpl'
  local function newParser(lines, offset)
    offset = offset or 0
    return JParser:new(nil, IteratorImpl:new(nil, lines, offset), offset)
  end

  it("code_spring_JwtFilter", function()
    local parser = newParser(H.code_spring_JwtFilter, 0)
    local exp = {
      pkg = "org.swarg.springcourse.fsa.config",
      mods = 1,
      typ = "class",
      name = "JWTFilter",
      extends = "OncePerRequestFilter",
      imports = {
        "java.io.IOException", "javax.servlet.FilterChain",
        "javax.servlet.ServletException",
        "javax.servlet.http.HttpServletRequest",
        "javax.servlet.http.HttpServletResponse",
        "org.springframework.web.filter.OncePerRequestFilter" },
      ln = 14,
      lne = 25,
      pkg_ln = 1,
      import_lns = { 3, 4, 5, 6, 7, 8 },
      methods = {
        {
          annotations = {
            { name = "Override", ln = 16 }
          },
          name = "doFilterInternal",
          mods = 4,
          typ = "void",
          ln = 17,
          lne = 25,
          params = {
            { "HttpServletRequest",  "request" },
            { "HttpServletResponse", "response" },
            { "FilterChain",         "filterChain" },
            lne = 18
          }
        }
      }
    }
    local res = parser:prepare(nil).clazz
    assert.same(exp, res)
  end)


  it("code_spring_GreetingController", function()
    local parser = newParser(H.code_spring_GreetingController, 0)

    local res = parser:prepare(nil).clazz
    local exp = {
      pkg = "com.swarg.sweater.controller",
      imports = { "org.springframework.stereotype.Controller" },
      mods = 1,
      typ = "class",
      name = "GreetingController",
      annotations = { { name = "Controller", ln = 5, } },
      ln = 6,
      lne = 23,
      pkg_ln = 1,
      import_lns = { 3 },
      methods = {
        {
          annotations = {
            { name = "GetMapping", params = { "value", '"/greeting"' }, ln = 8, }
          },
          ln = 9,
          lne = 16,
          mods = 1,
          name = "greeting",
          params = {
            {
              "String",
              "name",
              annotations = {
                {
                  name = "RequestParam",
                  params = {
                    { "name",         '"name"' },
                    { "required",     "false" },
                    { "defaultValue", '"World"' }
                  }
                }
              }
            },
            { "Model", "model" },
            lne = 13
          },
          typ = "String"
        },
        {
          annotations = { {
            ln = 18,
            name = "GetMapping"
          } },
          ln = 19,
          lne = 22,
          mods = 1,
          name = "index",
          params = { { "Model", "model" } },
          typ = "String"
        }
      },
    }
    assert.same(exp, res)
  end)

  it("{ in same line", function()
    local lines = {
      'package com.example;',
      '',
      'public class Service {',
      '',
      '    public void method(String arg) {',
      '                 ',
      '        //...code',
      '    }',
      '',
      '}'
    }
    local parser = newParser(lines, 0)
    local res = parser:prepare(nil).clazz
    local exp = {
      pkg = "com.example",
      pkg_ln = 1,
      mods = 1,
      typ = "class",
      name = "Service",
      imports = {},
      import_lns = {},
      ln = 3,
      lne = 10,
      methods = {
        {
          mods = 1,
          typ = "void",
          name = "method",
          params = { { "String", "arg" } },
          ln = 5,
          lne = 8,
        }
      }
    }
    assert.same(exp, res)
  end)

  it("{ in new lines", function()
    local lines = {
      'package com.example;',
      '',
      'public class Service',
      '{',
      '    public void method(String arg)',
      '    {',
      '        //...code',
      '    }',
      '',
      '}'
    }
    local parser = newParser(lines, 0)
    local res = parser:prepare(nil).clazz
    local exp = {
      pkg = "com.example",
      pkg_ln = 1,
      mods = 1,
      typ = "class",
      name = "Service",
      imports = {},
      import_lns = {},
      ln = 3,
      methods = {
        {
          mods = 1,
          typ = "void",
          name = "method",
          params = { { "String", "arg" } },
          ln = 5,
          lne = 8,
        }
      },
      lne = 10,
    }
    assert.same(exp, res)
  end)

  it("code_spring_GreetingController", function()
    local lines = {
      'package com.example.service;',
      '',
      '@Service',
      'public class UserService implements UserDetailsService {',
      '',
      '    @Override',
      '    public UserDetails loadUserByUsername(String username)',
      '            throws UsernameNotFoundException {',
      '        //...',
      '    }', -- 10
      '}'
    }

    local parser = newParser(lines, 0)
    local res = parser:prepare(nil).clazz
    local exp = {
      pkg = "com.example.service",
      pkg_ln = 1,
      implements = { "UserDetailsService" },
      mods = 1,
      name = "UserService",
      typ = "class",
      annotations = { { name = "Service", ln = 3, } },
      imports = {},
      import_lns = {},
      ln = 4,
      methods = {
        {
          annotations = { { name = "Override", ln = 6, } },
          mods = 1,
          name = "loadUserByUsername",
          params = { { "String", "username" } },
          throws = { { "UsernameNotFoundException" } },
          typ = "UserDetails",
          ln = 7,
          lne = 10,
        }
      },
      lne = 11,
    }
    assert.same(exp, res)
  end)

  it("continue", function()
    local lines = {
      'package org;',
      '',
      'public class ByteArray {',
      '    private byte[] byteArray;',
      '',
      '    ByteArray() {}',
      '',
      '    public ByteArray(byte[] data) {',
      '        this.byteArray = data;',
      '    }',
      '',
      '    void doSomething(DataInput dataInput, int param2,',
      '            SizeTracker sizeTracker) throws IOException {',
      '        sizeTracker.track(32);',
      '        int len = dataInput.readInt();',
      '        sizeTracker.track((long) (8 * len));',
      '        this.byteArray = new byte[len];',
      '        dataInput.readFully(this.byteArray);',
      '    }',
      '}'
    }
    local parser = newParser(lines, 0)
    local res = parser:prepare(nil).clazz

    local exp = {
      pkg = "org",
      pkg_ln = 1,
      mods = 1,
      typ = "class",
      name = "ByteArray",
      imports = {},
      import_lns = {},
      ln = 3,
      lne = 20,
      constructors = {
        { ln = 6, lne = 6, mods = 0, name = "ByteArray", params = {} },
        {
          ln = 8,
          lne = 10,
          mods = 1,
          name = "ByteArray",
          params = {
            { "byte[]", "data" } }
        }
      },
      fields = { { ln = 4, mods = 2, typ = "byte[]", name = "byteArray" } },
      methods = {
        {
          ln = 12,
          lne = 20,
          mods = 0,
          typ = "void",
          name = "doSomething",
          params = {
            { "DataInput",   "dataInput" },
            { "int",         "param2" },
            { "SizeTracker", "sizeTracker" },
            lne = 13
          }
        }
      },
    }
    assert.same(exp, res)
  end)


  it("full-class with multiline fileds", function()
    local lines = {
      'package org.app.dao;', -- 1
      '',
      'import java.sql.PreparedStatement;',
      'import java.sql.SQLException;',
      '',
      'import static java.sql.Statement.RETURN_GENERATED_KEYS;', -- 6
      '',
      'public class UserDao implements Dao<Integer, User> {',
      '',
      '    private static final UserDao INSTANCE = new UserDao();',
      '',
      '    private static final String GET_ALL_SQL = """', -- 12
      '            SELECT',
      '                id,',
      '                name,',
      '                birthday,',
      '                email,',
      '                password,',
      '                role,',
      '                gender',
      '            FROM users', -- 21
      '            """;',       -- 22
      '    private static final String GET_BY_ID_SQL = GET_ALL_SQL + " WHERE id = ?";',
      '    private static final String GET_BY_EMAIL_AND_PASSWORD_SQL = GET_ALL_SQL + " WHERE email = ? AND password = ?";',
      '    private static final String SAVE_SQL =',
      '            "INSERT INTO users (name, birthday, email, password, role, gender) VALUES (?, ?, ?, ?, ?, ?)";',
      '',
      '    }'
    }

    -- require 'alogger'.fast_setup(false, 0)
    local parser = newParser(lines, 0)
    local res = parser:prepare(nil).clazz
    local exp = {
      pkg = 'org.app.dao',
      typ = 'class',
      mods = 1,
      name = 'UserDao',
      implements = { 'Dao<Integer, User>' },
      imports = {
        'java.sql.PreparedStatement', 'java.sql.SQLException',
        'static java.sql.Statement.RETURN_GENERATED_KEYS'
      },
      import_lns = { 3, 4, 6 },
      ln = 8,
      lne = 28,
      pkg_ln = 1,
      fields = {
        { -- 1
          ln = 10,
          mods = 26,
          name = 'INSTANCE',
          typ = 'UserDao',
          value = 'new UserDao();'
        },
        { -- 2
          ln = 12,
          lne = 22,
          mods = 26,
          name = 'GET_ALL_SQL',
          typ = 'String',
          value = [[
"""            SELECT
                id,
                name,
                birthday,
                email,
                password,
                role,
                gender
            FROM users
            """;]]
        },
        { -- 3
          ln = 23,
          mods = 26,
          name = 'GET_BY_ID_SQL',
          typ = 'String',
          value = 'GET_ALL_SQL + " WHERE id = ?";'
        },
        { -- 5
          ln = 24,
          mods = 26,
          name = 'GET_BY_EMAIL_AND_PASSWORD_SQL',
          typ = 'String',
          value = 'GET_ALL_SQL + " WHERE email = ? AND password = ?";'
        },
        { -- 6
          ln = 25,
          lne = 26,
          mods = 26,
          name = 'SAVE_SQL',
          typ = 'String',
          value =
          '            "INSERT INTO users (name, birthday, email, password, role, gender) VALUES (?, ?, ?, ?, ?, ?)";'
        }
      }
    }
    assert.same(exp, res)
  end)
end)

describe("env.langs.java.JParser factory", function()
  local IteratorImpl = require 'olua.util.IteratorImpl'

  local function newParser(lines, offset)
    offset = offset or 0
    return JParser:new(nil, IteratorImpl:new(nil, lines, offset), offset)
  end

  it("newPredicateFindClassName", function()
    local lines = {
      'package pkg;',
      'import org.comp.ClassA;',
      'import org.comp.ClassB;',
      'import org.comp.ClassC;',
      'import org.comp.util.*;',
      'import org.lib.*;',
      'public class A {',
      '    private int x;',
      '}' -- 9
    }
    local p = newParser(lines, 0)
    local res = {}
    local predicate = p:factory().newPredicateFindClassName('ClassC', res)
    p:prepare(predicate)
    assert.same({ found = 'org.comp.ClassC', lnum = 4 }, res)

    p = newParser(lines, 0)
    res = {}
    p:prepare(p:factory().newPredicateFindClassName('ClassF', res))
    assert.same({ variants = { 'org.comp.util.*', 'org.lib.*' } }, res)
    assert.same(7, p.lnum)
  end)
end)

--

describe("env.langs.java.JParser offhand", function()
  it("split2lexems", function()
    local f = JParser.splitLineToLexemes
    local exp = {
      { 'public', 1,  6 },
      { 'final',  8,  12 },
      { 'Object', 14, 19 },
      { 'lock',   21, 24 },
      { ';',      25, 25 }
    }
    assert.same(exp, f("public final Object lock;"))
  end)

  it("lexer", function()
    assert.is_function((JParser.lexer() or {}).indexOfByPos)
  end)
end)

it("local", function()
  local p = JParser:new()
  assert.same(true, p:isEmptyOrComment("//x")) -- todo fix
  assert.same(true, p:isEmptyOrComment(" /* */ "))
end)

--

describe("env.langs.java.JParser offhand", function()
  it("parse_trace_line-0", function()
    local f = JParser.parseLineStacktrace
    local exp = {
      classname = "java.lang.reflect.Method",
      method = "invoke",
      lnum = 498,
    }
    assert.same(exp, f("	at java.lang.reflect.Method.invoke(Method.java:498)"))
  end)

  it("parse_trace_line-1", function()
    local line = "	at org.utils.ParserTest.test_func(ParserTest.java:750) "
    local exp = {
      classname = 'org.utils.ParserTest',
      method = 'test_func',
      lnum = 750,
    }
    assert.same(exp, JParser.parseLineStacktrace(line))
  end)


  it("match line with class name ", function()
    local f = function(s)
      return string.match(s, "^([%l%.]+%.%u[%w_]+)$")
    end
    assert.same('pkg.some.MyClass', f("pkg.some.MyClass"))
    assert.same('pkg.MyClass', f("pkg.MyClass"))
    assert.is_nil(f("pkg.some.notAClass"))
    assert.is_nil(f("pkg."))
    assert.is_nil(f("MyClass"))
  end)

  it("parseImportLineToFQCN", function()
    local f = JParser.parseImportLineToFQCN
    assert.same('pkg.MyClass', f("import pkg.MyClass;"))
    assert.same('MyClass', f("import MyClass;"))
    assert.same('MyClass', f("import static MyClass.method;"))
    assert.same('MyClass', f("import static MyClass.*;"))
    assert.same('org.some.MyClass', f("import static org.some.MyClass.method;"))
    assert.same('org.some.MyClass', f("import static org.some.MyClass.*;"))
  end)

  -- dont work as expected
  it("parse_trace_line-Uknown-Source", function()
    local line = "	at com.sun.proxy.$Proxy2.processTestClass(Unknown Source)"
    assert.is_nil(JParser.parseLineStacktrace(line))
  end)

  it("parse_trace_line-2", function()
    local line = "at org.junit.JUnitRunner$1.evaluate(JUnitRunner.java:100)"
    local exp = {
      classname = 'org.junit.JUnitRunner',
      method = 'evaluate',
      inner = '$1',
      lnum = 100,
    }
    assert.same(exp, JParser.parseLineStacktrace(line))
  end)

  it("parseLineCompilerError", function()
    local line = '[ERROR] /home/user/proj1/src/test/java/pkg/AppTest.java:[16,28] cannot find symbol'
    local f = JParser.parseLineCompilerError
    local exp = { '/home/user/proj1/src/test/java/pkg/AppTest.java', 16 }
    assert.same(exp, { f(line) })
  end)

  it("parseFaildedTestLine", function()
    local line = '     org.junit.ComparisonFailure at SomeTest.java:35'
    local exp = {
      cause = 'org.junit.ComparisonFailure',
      fn = 'SomeTest.java',
      lnum = 35
    }
    assert.same(exp, JParser.parseFaildedTestLine(line))
  end)
end)

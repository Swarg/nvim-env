-- 15-07-2024 @author Swarg

local su = require 'env.sutil'

-- local ClassInfo = require("env.lang.ClassInfo")
-- local ClassResolver = require("env.lang.ClassResolver")
local JLang = require 'env.langs.java.JLang'
local utempl = require 'env.lang.utils.templater'
local mvn_gen = require 'env.langs.java.util.maven.gen'
local JavaBuilder = require 'env.langs.java.JBuilder'
local ClassInfo = require 'env.lang.ClassInfo'

local MavenBuilder = require 'env.langs.java.builder.Maven'
local spring_gen = require 'env.langs.java.util.spring.generator'

local utbl = require 'env.util.tables'

local H = require 'env.lang.oop.stub.ATestHelper'


---@func new_cli(gen:env.lang.oop.LangGen?, cmd_line:string): Cmd4Lua
local M = {}
H.inherit(M) -- inherit functions from abstract-lang helper

M.JavaBuilder = JavaBuilder
M.JavaLang = JLang

-- class types
M.CT = ClassInfo.CT
M.split = su.split

-- shorthands
M.tbl_merge_flat = utbl.tbl_merge_flat
M.templater = utempl

-- common test data from empty maven project

-- to test FlatStructure
M.pkgApp0 = {
  pom_xml = mvn_gen.POM_HEAD .. [[
  <groupId>pkg</groupId>
  <artifactId>app0</artifactId>
  <version>0.1.0</version>
]] .. mvn_gen.POM_FOOTER,

  proj_root = '/tmp/dev/pr0j',
  proj_parent_dir = '/tmp/dev/',
  mainclass = 'pkg.Main',

  -- first varianl - regular structure
  mainclass_path = '/tmp/dev/pr0j/src/main/java/pkg/Main.java',

  -- second variant - flat structure (toy-education-snippets projects)
  flat_mainclass_path = '/tmp/dev/pr0j/pkg/Main.java',
  -- for JBuilder without any buildsystem but with per-dir-conf(.nvim-env.yml)
  build_grade = [[
plugins {
    id 'java'
}

group = 'pkg'
version = '0.1.0'

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(8)
    }
}

application {
    mainClass = 'pkg.Main'
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-engine:5.11.4'
}
]]
}
M.pkgApp0.project_root = M.pkgApp0.proj_root
M.pkgApp0.pom_xml_path = M.pkgApp0.proj_root .. '/pom.xml'
M.pkgApp0.build_gradle_path = M.pkgApp0.proj_root .. '/build.gradle'

-- default on init new project
M.pkgApp0.mainclass_body = [[
package pkg;

/**
 *
 * @author AuthorName
 */
public class Main {
}
]]
-- default on init new project
M.pkgApp0.main_testclass_body = [[
package pkg;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author AuthorName
 */
public class MainTest {

    @Test
    public void testSuccess() {
        System.out.println("testSuccess");
        Main main = new Main();
        assertEquals(1, main);
    }

}
]]

-- note: Ant use same placeholders as my env.lang.utils.templater like ${key}
M.pkgApp0.build_xml_path = M.pkgApp0.proj_root .. '/build.xml'
M.pkgApp0.build_xml = [[
<project name="app" basedir="." default="main">

    <property name="src.dir"     value="src"/>

    <property name="build.dir"   value="build"/>
    <property name="classes.dir" value="${build.dir}/classes"/>
    <property name="jar.dir"     value="${build.dir}/jar"/>

    <property name="main-class"  value="pkg.MainClass"/>


    <target name="clean">
        <delete dir="${build.dir}"/>
    </target>

    <target name="compile">
        <mkdir dir="${classes.dir}"/>
        <javac srcdir="${src.dir}" destdir="${classes.dir}"/>
    </target>

    <target name="jar" depends="compile">
        <mkdir dir="${jar.dir}"/>
        <jar destfile="${jar.dir}/${ant.project.name}.jar" basedir="${classes.dir}">
            <manifest>
                <attribute name="Main-Class" value="${main-class}"/>
            </manifest>
        </jar>
    </target>

    <target name="run" depends="jar">
        <java jar="${jar.dir}/${ant.project.name}.jar" fork="true"/>
    </target>

    <target name="clean-build" depends="clean,jar"/>

    <target name="main" depends="clean,run"/>

</project>
]]
--------------------------------------------------------------------------------


M.App1 = {
  proj_root = '/tmp/dev/proj1/',
  proj_parent_dir = '/tmp/dev/',
  mainclass = 'org.comp.Main',
  mainclass_path = '/tmp/dev/proj1/src/main/java/pkg/Main.java',
}
--------------------------------------------------------------------------------
--                   Helper implementation for specific lang
--------------------------------------------------------------------------------
--

function M.mkNewProjProps(cwd)
  local t = JavaBuilder.getDefaultProjectProperties()
  -- t = utbl.tbl_merge_flat(t, w.vars.props)
  t.project_root = cwd -- os.getenv() get_current_directory()
  return t
end

-- --
-- -- factory to create ClassInfo LangGen Lang
-- --
-- ---@param classname string
-- ---@param project_root string?
-- --
-- ---@return env.lang.ClassInfo
-- ---@return env.langs.java.JGen
-- ---@return env.langs.java.JLang
function M.mkClassInfoAndGen(classname, project_root)
  local ci, gen, lang = H.mkClassInfoAndLangGen(JLang, classname, project_root)
  ---@cast gen env.langs.java.JGen
  ---@cast lang env.langs.java.JLang
  return ci, gen, lang
end

-- factory to create JavaLang for given project_root
--
---@return env.langs.java.JLang
---@return string
function M.mkJLang(project_root)
  local lang, pr = H.mkLang(JLang, project_root)
  ---@cast lang env.langs.java.JLang
  return lang, pr
end

--------------------------------------------------------------------------------
--                          Source Code Samples
--------------------------------------------------------------------------------

-- for JParser and grammars

M.code_spring_AuthController = {
  "package org.springcourse.fsa.controller;", -- 1
  "",
  "import org.springframework.beans.factory.annotation.Autowired;",
  "import org.springframework.validation.BindingResult;",
  "import org.springframework.web.bind.annotation.*;",
  "",
  "@RestController",
  '@RequestMapping("/auth")',                                   -- 8
  "public class AuthController {",                              -- 9
  "",
  "    private final PersonValidator personValidator;",         -- 11
  "    private final RegistrationService registrationService;", -- 12
  "",
  "    @Autowired",
  "    public AuthController(PersonValidator personValidator, RegistrationService registrationService) {", -- 15
  "        this.personValidator = personValidator;",
  "        this.registrationService = registrationService;",
  "    }", -- 18
  "",
  "}"      -- 20

}
M.code_spring_AuthController_empty_constructor = {
  "    public AuthController() {",
  "    }",
  "",
}
M.code_spring_AuthController_2 = {
  "package org.springcourse.fsa.controller;", -- 1
  "",
  "import org.springframework.beans.factory.annotation.Autowired;",
  "import org.springframework.validation.BindingResult;",
  "import org.springframework.web.bind.annotation.*;",
  "",
  "@RestController",
  '@RequestMapping("/auth")',                                   -- 8
  "public class AuthController {",                              -- 9
  "",
  "    private final PersonValidator personValidator;",         -- 11
  "    private final RegistrationService registrationService;", -- 12
  "",
  "    @Autowired",
  "    public AuthController(PersonValidator personValidator,",           -- 15
  "                          RegistrationService registrationService) {", -- 16
  "        this.personValidator = personValidator;",
  "        this.registrationService = registrationService;",
  "    }", -- 19
  "",
  "}"      -- 21

}





M.code_spring_MController = {
  'package org.springcourse.p3.controller;',                                  --  1
  '',                                                                         --  2
  'import java.util.stream.Collectors;',                                      --  3
  '',                                                                         --  4
  'import org.modelmapper.ModelMapper;',                                      --  5
  'import org.springframework.beans.factory.annotation.Autowired;',           --  6
  'import org.springframework.http.HttpStatus;',                              --  7
  'import org.springframework.http.ResponseEntity;',                          --  8
  'import org.springframework.validation.BindingResult;',                     --  9
  'import org.springframework.web.bind.annotation.*;',                        -- 10
  '',                                                                         -- 11
  'import org.springcourse.p3.dto.*;',                                        -- 12
  '',                                                                         -- 13
  'import jakarta.validation.Valid;',                                         -- 14
  '',                                                                         -- 15
  '/**',                                                                      -- 16
  ' * @author Author',                                                        -- 17
  ' */',                                                                      -- 18
  '@RestController',                                                          -- 19
  '@RequestMapping("/ms")',                                                   -- 20
  'public class MController {',                                               -- 21
  '    private final MService msService;',                                    -- 22
  '    private final MValidator mValidator;',                                 -- 23
  '    private final ModelMapper modelMapper;',                               -- 24
  '',                                                                         -- 25
  '    @Autowired',                                                           -- 26
  '    public MController(MService msService,',                               -- 27
  '            MValidator mValidator,',                                       -- 28
  '            ModelMapper modelMapper) {',                                   -- 29
  '        this.msService = msService;',                                      -- 30
  '        this.mValidator = mValidator;',                                    -- 31
  '        this.modelMapper = modelMapper;',                                  -- 32
  '    }',                                                                    -- 33
  '',                                                                         -- 34
  '    public MController() {} ',                                             -- 35
  '',                                                                         -- 36
  '    @GetMapping()',                                                        -- 37
  '    public MResponse getM() {',                                            -- 38 -- first method
  '        return new MResponse(msService.findAll()',                         -- 39
  '                .stream().map(this::convertToMDTO)',                       -- 40
  '                .collect(Collectors.toList()));',                          -- 41
  '    }',                                                                    -- 42
  '',                                                                         -- 43
  '    @GetMapping("/{id}")',                                                 -- 44
  '    public MDTO getM(@PathVariable int id) {',                             -- 45
  '        return convertToMDTO(msService.findOne(id));',                     -- 46
  '    }',                                                                    -- 47
  '',                                                                         -- 48
  '    @PostMapping("/add")',                                                 -- 49
  '    public ResponseEntity<HttpStatus> add(@RequestBody @Valid MDTO mDto,', -- 50
  '            BindingResult bindingResult) {',                               -- 51
  '        if (bindingResult.hasErrors()) {',                                 -- 52
  '            throw new MException(ErrHelper.build(bindingResult));',        -- 53
  '        }',                                                                -- 54
  '        M m = convertToM(mDto);',                                          -- 55
  '        mValidator.validate(m, bindingResult);',                           -- 56
  '',                                                                         -- 57
  '        msService.save(m);',                                               -- 58
  '',                                                                         -- 59
  '        return ResponseEntity.ok(HttpStatus.OK);',                         -- 60
  '    }',                                                                    -- 61
  '',                                                                         -- 62
  '}'                                                                         -- 63
}


M.code_spring_PersonDTO = {
  'package org.springcourse.fsa.dto;',
  '',
  'import javax.validation.constraints.*;',
  '',
  '/**',
  ' *',
  ' */',
  'public class PersonDTO {',
  '',
  '    @NotEmpty(message = "Name should not be empty")',
  '    @Size(min = 3, max = 100, message = "Name should be between 3 and 100 characters")',
  '    private String username; // full name',
  '',
  '    @Min(value = 1900, message = "Year of birth should be greater than 1900")',
  '    private int yearOfBirth;',
  '',
  '    private String password;',
  '',
  '    public PersonDTO() {',
  '    }',
  '',
  '    public PersonDTO(String username, int yearOfBirth, String password) {',
  '        this.username = username;',
  '        this.yearOfBirth = yearOfBirth;',
  '        this.password = password;',
  '    }',
  '',
  '}'
}

-- 'org.springframework.stereotype.Component'
M.code_spring_JwtFilter = {
  'package org.swarg.springcourse.fsa.config;', -- 1
  '',
  'import java.io.IOException;',
  'import javax.servlet.FilterChain;',
  'import javax.servlet.ServletException;',
  'import javax.servlet.http.HttpServletRequest;',
  'import javax.servlet.http.HttpServletResponse;',
  'import org.springframework.web.filter.OncePerRequestFilter;',
  '',
  '/**',
  ' *',
  ' * @author Author',
  ' */',
  'public class JWTFilter extends OncePerRequestFilter {',
  '', --JWTHelper  lnum 15
  '    @Override',
  '    protected void doFilterInternal(HttpServletRequest request,',
  '            HttpServletResponse response, FilterChain filterChain)',
  '            throws ServletException, IOException {',
  '        String auth = request.getHeader("Authorization");',
  '        // ...',
  '    }',
  '}',
  ''
}

M.code_spring_GreetingController = {
  'package com.swarg.sweater.controller;', -- 1
  '',
  'import org.springframework.stereotype.Controller;',
  '',
  '@Controller',
  'public class GreetingController {', -- 6
  '',
  '    @GetMapping("/greeting")',      -- 8
  '    public String greeting(',       -- 9
  '        @RequestParam(name = "name", required = false, defaultValue = "World")',
  '        String name,',
  '        Model model', -- 12
  '    ) {',
  '        model.addAttribute("name", name);',
  '        return "greeting";',
  '    }',
  '',
  '    @GetMapping()',
  '    public String index(Model model) {',
  '        model.addAttribute("messages")',
  '        return "index";',
  '    }',
  '}'
}

--
---@param t table{project_root}
---@param sys stub.os.OSystem
---@param files table?{fn=body}
---@return env.langs.svelte.SLang
function M.setupProject(sys, t, files)
  return H.setupProject(JLang, sys, t, files)
end

-- sample projects

---@param props table{POM_PARENT, GROUP_ID, ARTIFACT_ID, DEPENDENCIES}
function M.gen_pom_xml(props)
  local pp = JavaBuilder.getDefaultProjectProperties()
  local upperkey = {}
  local subs = M.tbl_merge_flat(pp, props)
  return utempl.substitute(mvn_gen.TEMPL_POM_XML, subs, upperkey)
end

---@return table{project_root, _props}
---@return env.langs.java.JGen
---@param withbuilder boolean?
function M.get_springboot3_app1_setup_mvn(withbuilder)
  local g0 = require 'env.langs.java.util.spring.generator'

  local project_root = '/home/me/dev/sb_app1/'
  local t = {
    project_root = project_root,
    mainclass_path = project_root .. 'src/main/java/sb3/App.java',
  }
  t._props = {
    POM_PARENT = spring_gen.TEMPL_MVN_POM_PARENT,
    GROUP_ID = "sb3",
    ARTIFACT_ID = "app",
    MAINCLASS = "sb3.App",
    PACKAGE = 'sb3',
    DEPENDENCIES = "",
    JAVA_VERSION = "21",
    DEPENDENCY_TEST_FRAMEWORK = '',
    PLUGINS = '',
  }
  t.pom_xml_path = project_root .. 'pom.xml'
  t.pom_xml = M.gen_pom_xml(t._props)

  t.mainclass_body = utempl.substitute(g0.TEMPL_SPRINGBOOT_MAINCLASS, t._props)

  local lang = JLang:new(nil, project_root)
  local gen = lang:getLangGen()
  if withbuilder then
    lang.builder = MavenBuilder:new(nil, project_root)
    -- lang:findProjectBuilder(nil) -- MavenBuilder
  end
  return t, gen
end

return M

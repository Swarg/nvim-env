-- 08-10-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

describe("env.langs.java.ui.JarViewer", function()
  local JarViewer = require 'env.langs.java.ui.JarViewer'

  local slf4j_jar_content = {
    'META-INF/MANIFEST.MF',
    'META-INF/',
    'org/',
    'org/slf4j/',
    'org/slf4j/simple/',
    'META-INF/services/',
    'META-INF/maven/',
    'META-INF/maven/org.slf4j/',
    'META-INF/maven/org.slf4j/slf4j-simple/',
    'org/slf4j/simple/OutputChoice.java',
    'org/slf4j/simple/SimpleLogger.java',
    'org/slf4j/simple/SimpleLoggerConfiguration.java',
    'org/slf4j/simple/SimpleLoggerFactory.java',
    'org/slf4j/simple/SimpleServiceProvider.java',
    'META-INF/services/org.slf4j.spi.SLF4JServiceProvider',
    'META-INF/LICENSE.txt',
    'META-INF/maven/org.slf4j/slf4j-simple/pom.xml',
    'META-INF/maven/org.slf4j/slf4j-simple/pom.properties'
  }
  local slf4j_jar_tree = {
    ["META-INF"] = {
      ["LICENSE.txt"] = true,
      maven = {
        ["org.slf4j"] = {
          ["slf4j-simple"] = {
            ["pom.properties"] = true,
            ["pom.xml"] = true
          }
        }
      },
      services = {
        ["org.slf4j.spi.SLF4JServiceProvider"] = true
      }
    },
    org = {
      slf4j = {
        simple = {
          ["OutputChoice.java"] = true,
          ["SimpleLogger.java"] = true,
          ["SimpleLoggerConfiguration.java"] = true,
          ["SimpleLoggerFactory.java"] = true,
          ["SimpleServiceProvider.java"] = true
        }
      }
    }
  }

  it("parseLinesToTree", function()
    local tree = JarViewer.parseLinesToTree(slf4j_jar_content)
    local exp = slf4j_jar_tree
    assert.same(exp, tree)
  end)

  it("renderTreeToLines", function()
    local exp = {
      '[+] META-INF/',
      '[+] org/'
    }
    local lines, mappings = {}, {}
    local dstate = {}
    JarViewer.renderTreeToLines(lines, mappings, dstate, slf4j_jar_tree, 0)
    assert.same(exp, lines)
    local exp2 = { 'META-INF/', 'org/' }
    assert.same(exp2, mappings)
  end)

  it("get_parent_dir", function()
    local f = JarViewer.get_parent_dir
    assert.same('/', f("/tmp"))
    assert.is_nil(f("tmp/"))
    assert.same('/tmp/', f("/tmp/file"))
    assert.same('/tmp/subdir/', f("/tmp/subdir/file"))
    assert.same('/t_m-p/sub-dir/', f("/t_m-p/sub-dir/file.ex"))
    assert.same('/t_m-p/sub-dir/', f("/t_m-p/sub-dir/dir.ex/"))
  end)

  -- todo experimetal
  it("get_jdt_path", function()
    -- given
    local jar = "/home/swarg/.m2/repository/org/slf4j/slf4j-simple/2.0.16/slf4j-simple-2.0.16.jar"
    local inner = "org/slf4j/simple/SimpleLoggerFactory.class"
    local proj_name = "project"
    local scope = 'gradle_used_by_scope=/main,test'
    local jv = JarViewer:new(nil, jar)

    local exp = "jdt://contents/slf4j-simple-2.0.16.jar/org.slf4j.simple/SimpleLoggerFactory.class?=" ..
        "project/%5C/home%5C/swarg%5C/.m2%5C/repository%5C/org%5C/slf4j%5C/slf4j-simple%5C/2.0.16%5C/slf4j-simple-2.0.16.jar=/" ..
        "gradle_used_by_scope=/main,test=/%3C" ..
        "org.slf4j.simple(SimpleLoggerFactory.class"
    assert.same(exp, jv:get_jdt_path(inner, proj_name, scope))
  end)
end)


describe("env.langs.java.ui.JarViewer", function()
  local NVim = require 'stub.vim.NVim'
  local nvim = NVim:new() ---@type stub.vim.NVim
  local JarViewer = require 'env.langs.java.ui.JarViewer'

  local M = JarViewer.code
  it("find_package_for_short_classname", function()
    local lines = {
      'package pkg.some;',
      '',
      'import pkg.util.Tools;',
      'import pkg.model.User;',
      '',
      'public class App {',
      '// code',
      '}'
    }
    local bufnr = nvim:new_buf(lines, 0, 0)
    local f = M.find_package_for_short_classname

    assert.same('pkg.model.User', f(bufnr, 'User'))
    assert.same('pkg.util.Tools', f(bufnr, 'Tools'))
    assert.same('pkg.some.Meta', f(bufnr, 'Meta')) -- pkg + short classname
  end)

  it("find_package_for_short_classname as inner of the current", function()
    local lines = {
      'package pkg.some;',
      '',
      'import pkg.util.Tools;',
      'import pkg.model.User;',
      '',
      'public class Car {',
      '    public Engine engine;',
      '}'
    }
    local bufnr = nvim:new_buf(lines, 0, 0)
    local f = M.find_package_for_short_classname

    assert.same('pkg.some.Car$Engine', f(bufnr, 'Engine', true))
    assert.same('pkg.util.Tools', f(bufnr, 'Tools', true)) -- ?
  end)

  it("hasFile", function()
    local jar_viewer = JarViewer:new(nil, 'fantom.jar')
    jar_viewer.tree = {
      path = {
        to = {
          ['MyClass.class'] = true
        }
      }
    }
    assert.same(false, jar_viewer:hasFile('path/to/MyClass.java'))
    assert.same(true, jar_viewer:hasFile('path/to/MyClass.class'))
  end)

  it("match", function()
    local line = '|    |    +--- org.junit.jupiter:junit-jupiter-api:5.11.4 (c)'
    local patt = '^[/|%-%s%s+]+%s+([^%s]+)%s*[%(cn%*%)]+$'
    local exp = 'org.junit.jupiter:junit-jupiter-api:5.11.4'
    assert.same(exp, string.match(line, patt))
  end)
end)

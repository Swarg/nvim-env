require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local fs = require('env.files')
local uv = require("luv")


local H = require 'env.langs.java.ahelper'
local JLang = require 'env.langs.java.JLang'
-- local JGen = require 'env.langs.java.JGen'

local D = require 'dprint'
D.enable_module('stub.os.*', false)
D.enable_module('stub.vim.*', false)
D.on = false
D.disable()

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


---@return env.langs.java.JGen
local function mkGen(projroot) return JLang:new(nil, projroot):getLangGen() end

local pkgApp0 = H.pkgApp0

describe("env.langs.java.JGen inmem", function()
  it("createClassBody", function()
    -- if 0 == 0 then return end
    local ci, gen = H.mkClassInfoAndGen('pkg.Main', pkgApp0.proj_root)
    local opts = { no_date = true }

    -- require'dprint'.enable()
    -- require 'alogger'.fast_setup()
    local exp = pkgApp0.mainclass_body
    assert.same(exp, gen:createClassBody(ci, opts))
  end)
end)


describe("env.langs.java.JGen", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    D.on = false
  end)

  it("isClassExists", function()
    local cn = pkgApp0.mainclass
    local ci, gen = H.mkClassInfoAndGen(cn, pkgApp0.proj_root)
    local exp_path = '/tmp/dev/pr0j/src/main/java/pkg/Main.java'
    assert.same(exp_path, ci:getPath())
    assert.same(false, gen:isClassExists(cn))

    assert.same(true, sys:set_file_content(exp_path, pkgApp0.mainclass_body))

    assert.same(true, gen:isClassExists(cn))
  end)

  it("Lang getOppositeClassInfo", function()
    local cn = pkgApp0.mainclass
    local ci, gen = H.mkClassInfoAndGen(cn, pkgApp0.proj_root)
    assert.same('java', gen.lang.ext)

    local exp_path = '/tmp/dev/pr0j/src/main/java/pkg/Main.java'
    assert.same(true, sys:set_file_content(exp_path, pkgApp0.mainclass_body))
    assert.same(exp_path, ci:getPath())
    assert.same(true, gen:isClassExists(cn))

    local tci = gen.lang:getOppositeClassInfo(ci)
    assert.is_table(tci)
    assert.same('pkg.MainTest', tci:getClassName())
    assert.same('/tmp/dev/pr0j/src/test/java/pkg/MainTest.java', tci:getPath())
    assert.same('src/test/java/pkg/MainTest', tci:getInnerPath())
  end)

  --
  -- luv is used on env.lang.utils.base.saveFile()
  it("tooling emulated FS (stub.vim.OSystem and IO-libs)", function()
    sys.fs:mk(pkgApp0.pom_xml_path)
    assert.same('directory', (uv.fs_stat(pkgApp0.proj_root) or E).type)
    assert.same('emulated luv module', sys:whatis(uv))
    -- assert.same(1,  sys:tree())
    assert.is_true(fs.dir_exists(pkgApp0.proj_root))
  end)


  it("createProjectFiles MAINCLASS+test", function()
    sys.fs:mk(pkgApp0.pom_xml_path)
    local gen = mkGen(pkgApp0.proj_root)
    local pp = { MAINCLASS = pkgApp0.mainclass, no_date = 1 }
    assert.same(false, gen:isClassExists(pp.MAINCLASS))
    -- local lines = H.split()
    -- nvim:new_buf(lines, 1, 1, nil, current_bufname)

    -- require 'alogger'.fast_setup()
    assert.same(2, gen:createProjectFiles(pp))
    assert.same(true, gen:isClassExists(pp.MAINCLASS))
    local exp_tree = [[
/tmp/dev/pr0j
    pom.xml
    src/
        main/
            java/
                pkg/
                    Main.java
        test/
            java/
                pkg/
                    MainTest.java
7 directories, 3 files
]]
    assert.same(exp_tree, sys:tree(pkgApp0.proj_root))
    local inner_main_file = '/src/main/java/pkg/Main.java'
    local inner_test_file = '/src/test/java/pkg/MainTest.java'
    local expt = pkgApp0.main_testclass_body
    local exps = pkgApp0.mainclass_body
    assert.same(expt, sys:get_file_content(pkgApp0.proj_root .. inner_test_file))
    assert.same(exps, sys:get_file_content(pkgApp0.proj_root .. inner_main_file))
  end)

  it("getClassNameFromLine from new instance", function()
    local gen = mkGen(pkgApp0.proj_root)
    local f = function(line)
      return gen:getClassNameFromLine(line)
    end
    assert.same('MyClass', f("MyClass my = new MyClass();"))
    assert.same('org.MyClass', f("MyClass my = new org.MyClass();"))
    assert.same('MyClass', f("MyClass  my  =  new  MyClass ();"))
    assert.same('MyClass', f("MyClass  my  =  new  MyClass ("))
    assert.same('MyClass', f("MyClass  my  =  new  MyClass (abc"))
    assert.same('MyClass', f("my  =  new  MyClass (abc"))
    assert.same('MyClass', f("my=new MyClass(abc"))
    assert.same('pkg.MyClass', f("my=new pkg.MyClass(abc"))
    assert.is_nil(f("my=new MyClass "))
    assert.is_nil(f("my=new MyClass; "))
  end)

  it("getClassNameFromLine from import", function()
    local gen = mkGen(pkgApp0.proj_root)
    local f = function(line)
      return gen:getClassNameFromLine(line)
    end
    assert.same('org.MyClass', f("import org.MyClass;"))
    assert.same('org.MyClass', f("import org.MyClass"))
    assert.same('org.MyClass', f(" import  org.MyClass;"))
    assert.same('MyClass', f(" import  MyClass;"))
    assert.same('MyClass', f(" import  MyClass"))
    -- import static?
    assert.same('x', f(" import  x;")) --?
    assert.is_nil(f("// import  x;"))
  end)
end)

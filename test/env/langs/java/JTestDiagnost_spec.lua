-- 14-01-2025 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)

_G.TEST = true
local C = require 'env.langs.java.JTestDiagnost'

describe("env.langs.java.JTestDiagnost", function()
  local output_ok_01 = [[
> Task :compileJava UP-TO-DATE
> Task :processResources NO-SOURCE
> Task :classes UP-TO-DATE
> Task :compileTestJava
> Task :processTestResources UP-TO-DATE
> Task :testClasses
> Task :test
BUILD SUCCESSFUL in 1s
4 actionable tasks: 2 executed, 2 up-to-date
]]

  local output_fail_test_01 = [[
> Task :compileJava UP-TO-DATE
> Task :processResources NO-SOURCE
> Task :classes UP-TO-DATE
> Task :compileTestJava UP-TO-DATE
> Task :processTestResources UP-TO-DATE
> Task :testClasses UP-TO-DATE
> Task :test FAILED
SomeTest > test_MessageFormat FAILED
    org.junit.ComparisonFailure at SomeTest.java:35
1 test completed, 1 failed
FAILURE: Build failed with an exception.
* What went wrong:
Execution failed for task ':test'.
> There were failing tests. See the results at: file:///home/user/app/build/test-results/test/
* Try:
> Run with --scan to get full insights.
BUILD FAILED in 1s
4 actionable tasks: 1 executed, 3 up-to-date
]]

  local output_fail_test_02 = [[
> Task :compileJava UP-TO-DATE
> Task :processResources UP-TO-DATE
> Task :classes UP-TO-DATE
> Task :compileTestJava UP-TO-DATE
> Task :processTestResources NO-SOURCE
> Task :testClasses UP-TO-DATE
> Task :test
UserControllerV1Test > Test create developer functionality FAILED
    java.lang.AssertionError at UserControllerV1Test.java:76
        Caused by: com.jayway.jsonpath.PathNotFoundException
1 test completed, 1 failed
FAILURE: Build failed with an exception.
* What went wrong:
Execution failed for task ':test'.
> There were failing tests. See the results at: file:///home/user/app/build/test-results/test/
* Try:
> Run with --scan to get full insights.
BUILD FAILED in 8s
> Task :test FAILED
4 actionable tasks: 1 executed, 3 up-to-date
]]

  local output_fail_test_03 = [[
> Task :compileJava UP-TO-DATE
> Task :processResources NO-SOURCE
> Task :classes UP-TO-DATE
> Task :compileTestJava
> Task :processTestResources UP-TO-DATE
> Task :testClasses

> Task :test FAILED

ReflectionHelperTest > test_getClassByName() FAILED
    java.lang.IllegalArgumentException at ReflectionHelperTest.java:30
        Caused by: java.lang.NumberFormatException at ReflectionHelperTest.java:30

1 test completed, 1 failed

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':test'.
> There were failing tests. See the results at: file:///home/user/app/build/test-results/test/

* Try:
> Run with --scan to get full insights.

BUILD FAILED
4 actionable tasks: 2 executed, 2 up-to-date
]]


  -- there may be a case that the test compilation failed, but
  -- there was already an old report - it will not be deleted or changed
  -- (that is, it will contain outdated data - this can be checked by the
  -- compilation time of the test file and the report time)
  local output_fail_compile_02 = [[
> Task :compileJava UP-TO-DATE
> Task :processResources NO-SOURCE
> Task :classes UP-TO-DATE

> Task :compileTestJava FAILED
/home/user/app/src/test/java/org/common/SomeTest.java:35: error: cannot find symbol
        assertEquals(exp, res);
                     ^
  symbol:   variable exp
  location: class org.common.SomeTest
1 error

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':compileTestJava'.
> Compilation failed; see the compiler error output for details.

* Try:
> Run with --info option to get more log output.
> Run with --scan to get full insights.

BUILD FAILED in 909ms
2 actionable tasks: 1 executed, 1 up-to-date
]]

  it("get_build_state", function()
    local f = C.test.get_build_state
    assert.same({ 'SUCCESSFUL' }, { f(output_ok_01) })
    assert.same({ 'FAILED', 'test' }, { f(output_fail_test_01) })
    assert.same({ 'FAILED', 'test' }, { f(output_fail_test_02) })
    assert.same({ 'FAILED', 'test' }, { f(output_fail_test_03) })
    assert.same({ 'FAILED', 'compileTestJava' }, { f(output_fail_compile_02) })
  end)

  -- > Task :test FAILED
  -- SomeTest > test_MessageFormat FAILED
  --     org.junit.ComparisonFailure at SomeTest.java:35
  -- 1 test completed, 1 failed
  -- FAILURE: Build failed with an exception.
  it("get_failed_test_class_and_reports_dir", function()
    local f = C.test.get_reports_dir_and_failed_tests
    local exp = {
      '/home/user/app/build/test-results/test/',
      {
        {
          name = 'SomeTest',
          method = 'test_MessageFormat',
          cause = 'org.junit.ComparisonFailure',
          lnum = 35,
        }
      }
    }
    assert.same(exp, { f(output_fail_test_01) })
  end)

  -- > Task :test
  -- UserControllerV1Test > Test create developer functionality FAILED
  --     java.lang.AssertionError at UserControllerV1Test.java:76
  --         Caused by: com.jayway.jsonpath.PathNotFoundException
  -- 1 test completed, 1 failed
  -- FAILURE: Build failed with an exception.
  it("get_failed_test_class_and_reports_dir", function()
    local f = C.test.get_reports_dir_and_failed_tests
    local exp = {
      '/home/user/app/build/test-results/test/',
      {
        {
          name = 'UserControllerV1Test',
          method = 'Test create developer functionality', -- DisplayName("a b")
          cause = 'java.lang.AssertionError',
          lnum = 76,
        }
      }
    }
    assert.same(exp, { f(output_fail_test_02) })
  end)

  it("get_failed_test_class_and_reports_dir", function()
    local f = C.test.get_reports_dir_and_failed_tests
    local exp = {
      '/home/user/app/build/test-results/test/',
      {
        {
          name = 'ReflectionHelperTest',
          method = 'test_getClassByName()',
          cause = 'java.lang.IllegalArgumentException',
          lnum = 30,
        }
      }
    }
    assert.same(exp, { f(output_fail_test_03) })
  end)

  it("mk_test_report_files_map", function()
    local f = C.test.mk_test_report_files_map
    local files = { 'TEST-org.common.SomeTest.xml', 'TEST-org.common.ATest.xml' }
    local exp = {
      ATest = 'TEST-org.common.ATest.xml',
      SomeTest = 'TEST-org.common.SomeTest.xml'
    }
    assert.same(exp, f(files))
  end)

  -- app/build/test-results/test/TEST-org.common.SomeTest.xml
  -- Note when in test classes used @DisplayName then testsuite.testcase.name
  -- can be a readable name with spaces not a pure java method name w/o spaces
  ---@diagnostic disable-next-line: unused-local
  local no_errors_01 = [==[
<?xml version="1.0" encoding="UTF-8"?>
<testsuite name="org.common.SomeTest" tests="2" skipped="0" failures="0" errors="0" timestamp="2025-01-14T09:42:00" hostname="debian" time="0.007">
  <properties/>
  <testcase name="test_2" classname="org.common.SomeTest" time="0.006"/>
  <testcase name="test_MessageFormat" classname="org.common.SomeTest" time="0.001"/>
  <system-out><![CDATA[
test_something
]]></system-out>
  <system-err><![CDATA[]]></system-err>
</testsuite>
]==]


  it("mkDiaFailureFromXmlNode", function()
    local failure_node = {
      tag = "failure",
      attrs = {
        message = "java.lang.AssertionError: expected:&lt;1&gt; but was:&lt;0&gt;",
        type = "java.lang.AssertionError"
      },
      text = {
        "java.lang.AssertionError: expected:&lt;1&gt; but was:&lt;0&gt;",
        "\tat org.junit.Assert.fail(Assert.java:89)",
        "\tat org.junit.Assert.failNotEquals(Assert.java:835)",
        "\tat org.junit.Assert.assertEquals(Assert.java:647)",
        "\tat org.junit.Assert.assertEquals(Assert.java:633)",
        "\tat org.common.SomeTest.test_2(ReflectionHelperTest.java:41)",
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)",
        "\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)",
        "\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)",
        "\tat java.lang.reflect.Method.invoke(Method.java:498)",
        "\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:59)",
        "\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)",
        "\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:56)",
        "\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)",
        "\tat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)",
        "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
        "\tat org.junit.runners.BlockJUnit4ClassRunner$1.evaluate(BlockJUnit4ClassRunner.java:100)",
        "\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:366)",
        "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:103)",
        "\tat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:63)",
        "\tat org.junit.runners.ParentRunner$4.run(ParentRunner.java:331)",
        "\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:79)",
        "\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:329)",
        "\tat org.junit.runners.ParentRunner.access$100(ParentRunner.java:66)",
        "\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:293)",
        "\tat org.junit.runners.ParentRunner$3.evaluate(ParentRunner.java:306)",
        "\tat org.junit.runners.ParentRunner.run(ParentRunner.java:413)",
        "\tat org.gradle.api.internal.tasks.testing.junit.JUnitTestClassExecutor.runTestClass(JUnitTestClassExecutor.java:112)",
        "...",
      }
    }
    local testcase_node = {
      tag = "testcase",
      attrs = {
        classname = "org.common.SomeTest", name = "test_2", time = "0.007"
      },
      childs = {
        failure_node
      }
    }

    local exp = {
      fn = 'ReflectionHelperTest.java',
      lnum = 40,
      end_lnum = 40,
      col = 1,
      end_col = 80,
      severity = 3,
      message = [[
java.lang.AssertionError:
Expected: |1|
Actual:   |0|
...
	at org.junit.Assert.assertEquals(Assert.java:633)
	at org.common.SomeTest.test_2(ReflectionHelperTest.java:41)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
...
org.common.SomeTest test_2 41
path
]],
    }
    assert.same(exp, C.test.mkDiaFailureFromXmlNode(failure_node, testcase_node, 'path'))
  end)



  it("mkDiaFailureFromXmlNode", function()
    local failure_node = {
      tag = "failure",
      attrs = {
        message = "java.lang.AssertionError: No value at JSON path &quot;$.id&quot;",
        type = "java.lang.AssertionError"
      },
      text = {
        "java.lang.AssertionError: No value at JSON path &quot;$.id&quot;",
        "	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:351)",
        "	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:106)",
        "	at org.springframework.test.web.servlet.result.JsonPathResultMatchers.lambda$value$0(JsonPathResultMatchers.java:88)",
        "	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:214)",
        "	at pkg.app.rest.UserControllerV1Test.givenDevDto_whenCreateDev_thenSuccessResponse(UserControllerV1Test.java:76)",
        "	at java.base/java.lang.reflect.Method.invoke(Method.java:569)",
        "	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)",
        "	at java.base/java.util.ArrayList.forEach(ArrayList.java:1511)",
        "Caused by: com.jayway.jsonpath.PathNotFoundException: No results for path: $['id']"
      }
    }
    local testcase_node = {
      tag = "testcase",
      attrs = {
        classname = "pkg.app.rest.UserControllerV1Test",
        name = "Test create developer functionality", -- DisplayName
        time = "0.007"
      },
      childs = {
        failure_node
      }
    }

    local exp = {
      fn = 'UserControllerV1Test.java',
      lnum = 75,
      end_lnum = 75,
      col = 1,
      end_col = 80,
      severity = 3,
      message = [[
java.lang.AssertionError:
Expected: |value-at-given-JSON-path|
Actual:   |No value at JSON path "$.id"|
...
	at org.springframework.test.util.JsonPathExpectationsHelper.evaluateJsonPath(JsonPathExpectationsHelper.java:351)
	at org.springframework.test.util.JsonPathExpectationsHelper.assertValue(JsonPathExpectationsHelper.java:106)
	at org.springframework.test.web.servlet.result.JsonPathResultMatchers.lambda$value$0(JsonPathResultMatchers.java:88)
	at org.springframework.test.web.servlet.MockMvc$1.andExpect(MockMvc.java:214)
	at pkg.app.rest.UserControllerV1Test.givenDevDto_whenCreateDev_thenSuccessResponse(UserControllerV1Test.java:76)
	at java.base/java.lang.reflect.Method.invoke(Method.java:569)
...
Caused by: com.jayway.jsonpath.PathNotFoundException: No results for path: $['id']
pkg.app.rest.UserControllerV1Test Test create developer functionality 76
path
]]
    }
    assert.same(exp, C.test.mkDiaFailureFromXmlNode(failure_node, testcase_node, 'path'))
  end)



  it("build_diagnostics_from_report", function()
    local path = './test/env/resources/gradle/app/build/test-results/test/TEST-org.common.SomeTest.xml'
    local report = C.test.parse_xml_report(path)
    local res = C.test.build_diagnostics_from_report(0, report, {}, 'path/to/xml')
    local exp_msg1 = [[
java.lang.AssertionError:
Expected: |1|
Actual:   |0|
...
	at org.junit.Assert.assertEquals(Assert.java:633)
	at org.common.SomeTest.test_2(SomeTest.java:41)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
...
org.common.SomeTest test_2 41
path/to/xml
]]
    local exp_msg2 = [[
org.junit.ComparisonFailure:
Expected: |[]|
Actual:   |[Bind Class for {0} - Recognized java.lang.Object]|
...
	at org.junit.Assert.assertEquals(Assert.java:146)
	at org.common.SomeTest.test_MessageFormat(SomeTest.java:35)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
...
org.common.SomeTest test_MessageFormat 35
path/to/xml
]]
    assert.is_not_nil((res or {})[0])
    assert.same(exp_msg1, (res or {})[0][1].message)
    assert.same(exp_msg2, (res or {})[0][2].message)

    local exp = {
      [0] = {
        {
          fn = 'SomeTest.java',
          lnum = 40,
          end_lnum = 40,
          col = 1,
          end_col = 80,
          severity = 3,
          message = exp_msg1,
        },
        {
          fn = 'SomeTest.java',
          lnum = 34,
          end_lnum = 34,
          col = 1,
          end_col = 80,
          severity = 3,
          message = exp_msg2,
        }
      }
    }
    assert.same(exp, res)
  end)


  it("build_diagnostics_from_compile_fail", function()
    local output = [[
> Task :compileJava UP-TO-DATE
> Task :processResources UP-TO-DATE
> Task :classes UP-TO-DATE

> Task :compileTestJava FAILED
/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java:36: error: cannot find symbol
    @MockitoBean
     ^
  symbol:   class MockitoBean
  location: class UserControllerTest
/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java:43: error: cannot find symbol
        this.webTestMockMvc = webTestMockMvc;
            ^
  symbol: variable webTestMockMvc
/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java:43: error: cannot find symbol
        this.webTestMockMvc = webTestMockMvc;
                              ^
  symbol:   variable webTestMockMvc
  location: class UserControllerTest
3 errors

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':compileTestJava'.
> Compilation failed; see the compiler error output for details.

* Try:
> Run with --info option to get more log output.
> Run with --scan to get full insights.

BUILD FAILED in 1s
3 actionable tasks: 1 executed, 2 up-to-date
]]
    local orig = C.getBufnrWithOpen
    -- mock
    C.getBufnrWithOpen = function() return 1; end
    local res = C.test.build_diagnostics_from_compile_fail(output, 'compileTestJava')
    C.getBufnrWithOpen = orig

    local exp = {
      {
        {
          col = 1,
          end_col = 80,
          end_lnum = 35,
          fn = '/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java',
          lnum = 35,
          message = "error: cannot find symbol\n" ..
              "    @MockitoBean\n     ^\n" ..
              "  symbol:   class MockitoBean\n" ..
              "  location: class UserControllerTest\n",
          severity = 3
        },
        {
          col = 1,
          end_col = 80,
          end_lnum = 42,
          fn = '/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java',
          lnum = 42,
          message =
              "error: cannot find symbol\n" ..
              "        this.webTestMockMvc = webTestMockMvc;\n" ..
              "            ^\n" ..
              "  symbol: variable webTestMockMvc\n",
          severity = 3
        },
        {
          col = 1,
          end_col = 80,
          end_lnum = 42,
          fn = '/home/user/project/src/test/java/pkg/app/rest/UserControllerTest.java',
          lnum = 42,
          message =
              "error: cannot find symbol\n" ..
              "        this.webTestMockMvc = webTestMockMvc;\n" ..
              "                              ^\n" ..
              "  symbol:   variable webTestMockMvc\n" ..
              "  location: class UserControllerTest\n",
          severity = 3
        }
      }
    }
    assert.same(exp, res)
  end)
  local compile_java_fatal_error_in_dependency = [[
> Task :compileJava FAILED
[Fatal Error] artifact-0.2.0.pom:14:7: The element type "dependencies" must be ...

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':compileJava'.
> Could not resolve all files for configuration ':compileClasspath'.
   > Could not resolve org.group:artifact:0.2.0.
     Required by:
         project :
      > Could not resolve org.group:artifact:0.2.0.
         > Could not parse POM /home/user/.m2/repository/org/group/artifact/0.2.0/artifact.2.0.pom
            > The element type "dependencies" must be ...

* Try:
> Run with --stacktrace option to get the stack trace.
> Run with --info or --debug option to get more log output.
> Run with --scan to get full insights.
> Get more help at https://help.gradle.org.

BUILD FAILED in 1s
1 actionable task: 1 executed
]]
  it("build_diagnostics_from_compile_fail 2", function()
    local f = C.test.build_diagnostics_from_compile_fail
    assert.same({}, f(compile_java_fatal_error_in_dependency, ''))
  end)
end)

describe("env.langs.java.JTestDiagnost filter", function()
  it("filter", function()
    local dlist = {
      {
        namespace = 27,
        severity = 1,
        source = "Java",
        bufnr = 1,
        code = "67108979",
        lnum = 213,
        end_lnum = 213,
        col = 8,
        end_col = 20,
        message = "The method assertEquals(int, int, String) ...",
        user_data = { lsp = { code = "67108979" } }
      },
      {
        namespace = 27,
        severity = 1,
        source = "Rust",
        bufnr = 2,
        lnum = 213,
        col = 8,
        message = "Smthg",
      }
    }
    local exp = { dlist[1] }
    assert.same(exp, C.filter(dlist))
  end)

  it("get_error_info", function()
    local msg = "The method assertEquals(int, int, String) in the type Assertions"
        .. " is not applicable for the arguments (String, int, int)"
    local exp = {
      class_name = 'Assertions',
      method_name = 'assertEquals',
      method_params = 'int, int, String',
      bad_args = 'String, int, int',
      errtype = 'WRONG_METHOD_ARGS',
    }
    assert.same(exp, C.get_error_info(msg))
  end)

  it("get_error_info", function()
    local s =
    "The method assertNotNull(Object, String) in the type Assertions is not applicable for the arguments (String, Field)"
    local exp = {
      class_name = 'Assertions',
      method_name = 'assertNotNull',
      method_params = 'Object, String',
      bad_args = 'String, Field',
      errtype = 'WRONG_METHOD_ARGS',
    }
    assert.same(exp, C.get_error_info(s))
  end)
end)

describe("env.langs.java.JTestDiagnost parseRawTestFailMessage", function()
  local f = C.parseRawTestFailMessage
  -- integration with opentest4j
  it("parseRawTestFailMessage", function()
    local m = "org.opentest4j.AssertionFailedError: expected: not <null>"
    local exp = { 'org.opentest4j.AssertionFailedError', '!null', '?' }
    assert.same(exp, { f(m) })
  end)

  it("parseRawTestFailMessage", function()
    local m = "java.lang.AssertionError: Expected size: 2 but was: 3 in:\n" ..
        "[&quot;[ERROR] message with without args&quot;," ..
        "     &quot;java.lang.Throwable&quot;," ..
        "     &quot;	at org.app.ATest.method(ATest.java:88)&quot;]"

    local exp = { 'java.lang.AssertionError', '2', '3' }
    assert.same(exp, { f(m) })
  end)

  it("parseRawTestFailMessage exp, res, message", function()
    local m = "org.opentest4j.AssertionFailedError: " ..
        "has items queued to unload ==> expected: <true> but was: <false>\n" ..
        " at org.junit.jupiter.api.AssertionFailureBuilder.build(AssertionFailureBuilder.java:151)\n" ..
        "	at org.junit.jupiter.api.AssertionFailureBuilder.buildAndThrow(AssertionFailureBuilder.java:132)\n" ..
        "	at org.junit.jupiter.api.Assertions.assertTrue(Assertions.java:214)\n" ..
        "	at org.app.ATest.test_method(ATest.java:47)"
    local exp = { 'org.opentest4j.AssertionFailedError', 'true', 'false' }
    assert.same(exp, { f(m) })
  end)
end)

--

describe("env.langs.java.JTestDiagnost parseNormalisedTestFailMessage", function()
  local fn = C.parseNormalisedTestFailMessage

  it("assertEquals two string value ", function()
    local mgs = [[
org.opentest4j.AssertionFailedError:
Expected: |Bind|
Actual:   |Bind Class for {0} - Recognized java.lang.Object|
...
    ]]
    local exp = {
      'org.opentest4j.AssertionFailedError',             -- exception
      'Bind',                                            -- expected
      'Bind Class for {0} - Recognized java.lang.Object' -- actual
    }
    assert.same(exp, { fn(mgs) })
  end)


  it("assertEquals two string value ", function()
    local msg = [[
org.opentest4j.AssertionFailedError:
Expected: |ABCDE|
Actual:   |AB|
]]
    local exp = { 'org.opentest4j.AssertionFailedError', 'ABCDE', 'AB' }
    assert.same(exp, { fn(msg) })
  end)


  it("assertEquals two digit", function()
    local msg = [[
org.opentest4j.AssertionFailedError:
Expected: |1|
Actual:   |0|
...
	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:531)
]]
    local exp = { 'org.opentest4j.AssertionFailedError', '1', '0' }
    assert.same(exp, { fn(msg) })
  end)


  it("assertNotNull digit", function()
    local msg = [[
org.opentest4j.AssertionFailedError:
Expected: |!null|
Actual:   |?|
...
	at org.junit.jupiter.api.Assertions.assertNotNull(Assertions.java:304)
]]
    local exp = { 'org.opentest4j.AssertionFailedError', '!null', '?' }
    assert.same(exp, { fn(msg) })
  end)
end)

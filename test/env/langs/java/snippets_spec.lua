-- 07-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local H = require 'env.langs.java.ahelper'
local M = require 'env.langs.java.snippets'
local tu = require 'env.util.tables'

describe("env.langs.java.snippets", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    -- Lang.clearCache()
  end)

  ---@param insert table?{pos, lines}
  local function new_buf(orig_lines, lnum, col, insert)
    local lines
    if type(insert) == 'table' then
      assert(type(insert.pos) == 'number', 'insert.pos')
      assert(type(insert.lines) == 'table', 'insert.lines')

      lines = tu.insert_all_to_new(orig_lines, insert.pos, insert.lines)
    else
      lines = tu.flat_copy(orig_lines)
    end
    local bufnr = nvim:new_buf(lines, lnum, col)
    return lines, bufnr
  end

  it("handler_fix_if_parenthesess", function()
    local lines = { "    if this.Map.containsKey(key)" }
    local _, bufnr = new_buf(lines, 1, 1)
    local _, err = M.handler_fix_if_parenthesess()
    assert.same(nil, err)
    local exp = {
      '    if (this.Map.containsKey(key)) {',
      '        ;',
      '    }'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_inject_dependencies", function()
    -- input to inject --
    local i2i = { lines = { "    JWTHelper ModelMaper" }, pos = 13 }
    local _, bufnr = new_buf(H.code_spring_AuthController, i2i.pos, 25, i2i)
    -- require 'alogger'.fast_setup()
    M.handler_inject_dependencies()
    local exp = {
      'package org.springcourse.fsa.controller;',
      '',
      'import org.springframework.beans.factory.annotation.Autowired;',
      'import org.springframework.validation.BindingResult;',
      'import org.springframework.web.bind.annotation.*;',
      '',
      '@RestController',
      '@RequestMapping("/auth")',
      'public class AuthController {',
      '',
      '    private final PersonValidator personValidator;',
      '    private final RegistrationService registrationService;',
      '    private final JWTHelper jwtHelper;',
      '    private final ModelMaper modelMaper;',
      '',
      '    @Autowired',
      '    public AuthController(PersonValidator personValidator,',
      '                          RegistrationService registrationService,',
      '                          JWTHelper jwtHelper,',
      '                          ModelMaper modelMaper) {',
      '        this.personValidator = personValidator;',
      '        this.registrationService = registrationService;',
      '        this.jwtHelper = jwtHelper;',
      '        this.modelMaper = modelMaper;',
      '    }',
      '',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_inject_dependencies", function()
    -- input to inject --
    local i2i = { lines = { "    JWTHelper ModelMaper" }, pos = 13 }
    local _, bufnr = new_buf(H.code_spring_AuthController_2, i2i.pos, 25, i2i)
    M.handler_inject_dependencies()
    local exp = {
      'package org.springcourse.fsa.controller;',
      '',
      'import org.springframework.beans.factory.annotation.Autowired;',
      'import org.springframework.validation.BindingResult;',
      'import org.springframework.web.bind.annotation.*;',
      '',
      '@RestController',
      '@RequestMapping("/auth")',
      'public class AuthController {',
      '',
      '    private final PersonValidator personValidator;',
      '    private final RegistrationService registrationService;',
      '    private final JWTHelper jwtHelper;',
      '    private final ModelMaper modelMaper;',
      '',
      '    @Autowired',
      '    public AuthController(PersonValidator personValidator,',
      '                          RegistrationService registrationService,',
      '                          JWTHelper jwtHelper,',
      '                          ModelMaper modelMaper) {',
      '        this.personValidator = personValidator;',
      '        this.registrationService = registrationService;',
      '        this.jwtHelper = jwtHelper;',
      '        this.modelMaper = modelMaper;',
      '    }',
      '',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_inject_dependencies", function()
    -- input to inject --
    local i2i = { lines = { "    JWTHelper" }, pos = 15 }
    local _, bufnr = new_buf(H.code_spring_JwtFilter, i2i.pos, 25, i2i)
    M.handler_inject_dependencies()
    local exp = {
      'package org.swarg.springcourse.fsa.config;',
      '',
      'import java.io.IOException;',
      'import javax.servlet.FilterChain;',
      'import javax.servlet.ServletException;',
      'import javax.servlet.http.HttpServletRequest;',
      'import javax.servlet.http.HttpServletResponse;',
      'import org.springframework.web.filter.OncePerRequestFilter;',
      '',
      '/**',
      ' *',
      ' * @author Author',
      ' */',
      '@Configuration',
      'public class JWTFilter extends OncePerRequestFilter {',
      '    private final JWTHelper jwtHelper;',
      '',
      '    @Autowired',
      '    public JWTFilter(JWTHelper jwtHelper) {',
      '        this.jwtHelper = jwtHelper;',
      '    }',
      '',
      '',
      '    @Override',
      '    protected void doFilterInternal(HttpServletRequest request,',
      '            HttpServletResponse response, FilterChain filterChain)',
      '            throws ServletException, IOException {',
      '        String auth = request.getHeader("Authorization");',
      '        // ...',
      '    }',
      '}',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_new_endpoint empty endpoint", function()
    local proot = "/home/dev/proj/"
    local src = proot .. "src/main/java/pkg/app/controller/MController.java"
    local lines = {
      'package com.example.controller;', -- 1
      '',
      '@Controller',
      '@RequestMapping("/user")',
      'public class UserController {',
      '',
      '    get . userList', -- 7
      '',
      '}'
    }
    sys:set_file(src, lines)
    sys:set_dir(proot .. 'src/main/resources/templates/')
    local bufnr = nvim:open(src, 7, 5)

    local ok, err = M.handler_new_endpoint()

    assert.same({ true, nil }, { ok, err })

    local exp = {
      'package com.example.controller;',
      '',
      '@Controller',
      '@RequestMapping("/user")',
      'public class UserController {',
      '',
      '    @GetMapping',
      '    public String userList(Model model) {',
      '        model.addAttribute("", );',
      '        return "userList";',
      '    }',
      '',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_new_endpoint empty endpoint", function()
    local proot = "/home/dev/proj/"
    local templs = proot .. 'src/main/resources/templates/'
    local src = proot .. "src/main/java/pkg/app/controller/MController.java"
    local lines = {
      'package com.example.controller;', -- 1
      '',
      '@Controller',
      '@RequestMapping("/user")',
      'public class UserController {',
      '',
      '    get {user} userEdit', -- 7
      '',
      '}'
    }
    sys:set_file(src, lines)
    sys:set_dir(templs)
    local bufnr = nvim:open(src, 7, 5)

    local ok, err = M.handler_new_endpoint()

    assert.same({ true, nil }, { ok, err })
    local exp = {
      'package com.example.controller;',
      '',
      '@Controller',
      '@RequestMapping("/user")',
      'public class UserController {',
      '',
      '    @GetMapping("{user}")',
      '    public String userEdit(@PathVariable User user, Model model) {',
      '        model.addAttribute("user", user);',
      '        return "userEdit";',
      '    }',
      '',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
    local exp_templ = {
      '<!DOCTYPE html>',
      '<html th:replace="~{ layout :: layout(_, ~{::section}) }">',
      '<body>',
      '<section>',
      '',
      '  <!-- page content -->',
      '',
      '</section>',
      '</body>',
      '</html>'
    }
    assert.same(exp_templ, sys:get_file_lines(templs .. "userEdit.html"))
  end)

  --
  it("spring handler_new_bean", function()
    local proot = "/home/dev/proj/"
    local src = proot .. "src/main/java/pkg/app/controller/MController.java"
    local lines = {
      'package com.example.config;', -- 1
      '',
      '@Configuration',
      'public class MvcConfig implements WebMvcConfigurer {',
      '',
      '    RestTemplate', -- 6
      '}'
    }
    sys:set_file(src, lines)
    local bufnr = nvim:open(src, 6, 5)

    local ok, err = M.handler_new_bean()

    assert.same({ true, nil }, { ok, err })
    local exp = {
      'package com.example.config;',
      '',
      '@Configuration',
      'public class MvcConfig implements WebMvcConfigurer {',
      '',
      '    @Bean',
      '    public RestTemplate restTemplate() {',
      '        return new RestTemplate();',
      '    }',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("spring handler_new_bean with builder", function()
    local proot = "/home/dev/proj/"
    local src = proot .. "src/main/java/pkg/app/controller/MController.java"
    local lines = {
      'package com.example.config;', -- 1
      '@Configuration',
      'public class MvcConfig implements WebMvcConfigurer {',
      '',
      '    RestTemplate builder', -- 5
      '}'
    }
    sys:set_file(src, lines)
    local bufnr = nvim:open(src, 5, 1)

    local ok, err = M.handler_new_bean()

    assert.same({ true, nil }, { ok, err })
    local exp = {
      'package com.example.config;',
      '@Configuration',
      'public class MvcConfig implements WebMvcConfigurer {',
      '',
      '    @Bean',
      '    public RestTemplate restTemplate(RestTemplateBuilder builder) {',
      '        return builder.build();',
      '    }',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("spring handler_new_value_property_field", function()
    local proot = "/home/dev/proj/"
    local src = proot .. "src/main/java/pkg/app/controller/MController.java"
    local lines = {
      'package com.example.controller;', -- 1
      '@Controller',
      'public class MController {',
      '',
      '    app.prod.url', -- 5
      '}'
    }
    sys:set_file(src, lines)
    local bufnr = nvim:open(src, 5, 7)

    local ok, err = M.handler_new_value_property_field()

    assert.same({ true, nil }, { ok, err })
    local exp = {
      'package com.example.controller;',
      '@Controller',
      'public class MController {',
      '',
      '    @Value("${app.prod.url}")',
      '    private String appProdUrl;',
      '}'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)
end)

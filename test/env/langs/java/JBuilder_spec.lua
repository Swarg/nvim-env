-- 15-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local D = require 'dprint'

D.enable_module('stub.os.*', true)
D.enable_module('stub.vim.*', true)
D.on = false

local JBuilder = require 'env.langs.java.JBuilder'
local ClassInfo = require 'env.lang.ClassInfo'
local JC = require 'env.langs.java.util.consts'
local H = require 'env.langs.java.ahelper'

local pkgApp0 = H.pkgApp0
-- local dir_conf_basename = pcache.get_dir_conf_basename()


describe("env.langs.java.JBuilder", function()
  it("getPackageAndClassName", function()
    local f = JBuilder.getPackageAndClassName
    assert.same({ 'pkg', 'Main' }, { f('pkg.Main') })
    assert.same({ 'org.company', 'Main' }, { f('org.company.Main') })
    assert.same({}, { f('org.company.') }) -- ?
    assert.same({ 'ok', 'Name' }, { f('ok.Name') })
    assert.same({}, { f('ok.name') })      -- bad class name
    assert.same({ 'ok', 'Name$1' }, { f('ok.Name$1') })
    assert.same({ 'ok', 'Name_1' }, { f('ok.Name_1') })
    assert.same({}, { f('ok.1ame') })
  end)

  it("isWebAppResource", function()
    local path = '/tmp/proj/src/main/webapp/WEB-INF/views/index.html'
    local ci = ClassInfo:new({ path = path })
    local f = JBuilder.isWebAppResource
    assert.same(true, f(ci))
    assert.same(JC.CT_WEBTEMPLATE, ci.type)
    assert.same('WEB-INF/views/index.html', ci.inner_path)
    assert.is_nil(ci.classname)
  end)
end)

describe("env.langs.java.JBuilder", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
  end)

  local function mkApp0Files()
    local a = pkgApp0
    -- assert.same(true, sys:set_file_content(a.pom_xml_path, a.pom_xml))
    assert.same(true, sys:set_file_content(a.flat_mainclass_path, a.mainclass_body))
    return a.proj_root, a -- .mainclass_path
  end

  it("mkCompileCallback flat Struct", function()
    local project_root, a = mkApp0Files()
    -- local builder = JBuilder:new(nil, project_root, './', './') -- flatStruct
    local lang = H.JavaLang:new(nil, project_root)
    local builder = lang:createBuilder(project_root, { flat = true })
    assert.same({ 'src/main/java/', 'src/test/java/' }, { lang.src, lang.test })
    lang:setProjectBuilder(builder)
    assert.same({ './', './' }, { lang.src, lang.test })

    local t = {
      builder = builder,
      classpath = nil,
    }
    local compile_callback, errmsg = JBuilder.factoryBuildCompileCallback(t)
    assert.is_function(compile_callback) ---@cast compile_callback function
    assert.same(nil, errmsg)
    local exp = [[
/tmp/dev/pr0j
    pkg/
        Main.java
1 directory, 1 file
]]
    assert.same(exp, sys:tree(project_root))
    assert.is_number(nvim:open(a.flat_mainclass_path))


    -- require 'dprint'.enable(); require 'alogger'.fast_setup()
    sys:cd(project_root)
    local msg = compile_callback(lang)

    assert.same('Compile...', msg) -- for Editor:echoInStatus()
    local exp_after = [[
/tmp/dev/pr0j
    build/
        pkg/
            Main.class
    pkg/
        Main.java
3 directories, 2 files
]]
    assert.same(exp_after, sys:tree(project_root))
    assert.is_nil(errmsg)
  end)
end)

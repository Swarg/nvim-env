-- 15-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
assert:set_parameter("TableFormatLevel", 33)

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local class = require 'oop.class'
local Lang = require 'env.lang.Lang'
local JavaLang = require 'env.langs.java.JLang'
local JBuilder = require 'env.langs.java.JBuilder'
local pcache = require 'env.cache.projects'

local H = require 'env.langs.java.ahelper'
local pkgApp0 = H.pkgApp0
local dir_conf_basename = pcache.get_dir_conf_basename()
local E = {}



describe("env.langs.java.JLang", function()
  it("_init pass project_root only", function()
    assert.same('/tmp/project/', JavaLang:new(nil, '/tmp/project'):getProjectRoot())
  end)

  it("_init pass project_root & builder", function()
    local builder = JBuilder:new({ buildscript = 'no' })
    local lang = JavaLang:new(nil, '/tmp/project', builder)
    assert.same('/tmp/project/', lang:getProjectRoot())
    assert.equal(builder, lang.builder)
    assert.same('/tmp/project/', lang.builder:getProjectRoot())
    assert.same('src/main/java/', lang.builder.src)
    assert.same('src/test/java/', lang.builder.test)
    assert.same('java', lang.ext)
  end)

  it("getBuilderClassForBuildScript", function()
    local klass = JavaLang.getBuilderClassForBuildScript("/tmp/proj/pom.xml")
    assert.same('env.langs.java.builder.Maven', class.name(klass))

    klass = JavaLang.getBuilderClassForBuildScript("/tmp/proj/build.gradle")
    assert.same('env.langs.java.builder.Gradle', class.name(klass))

    klass = JavaLang.getBuilderClassForBuildScript("/tmp/proj/build.xml")
    assert.same('env.langs.java.builder.Ant', class.name(klass))
  end)
end)

describe("env.langs.java.JGen", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)

  it("findProjectBuilder - Gradle", function()
    local lang = JavaLang:new()
    local a = pkgApp0

    assert.same(true, sys:set_file_content(a.mainclass_path, a.mainclass_body))
    assert.same(true, sys:set_file_content(a.build_gradle_path, a.build_grade))

    assert.is_nil(lang.builder)
    lang.project_root = a.proj_root
    lang:findProjectBuilder('build.gradle') -- 1th part of the Lang.resolve()
    local builder = lang:getProjectBuilder()
    assert.same('env.langs.java.builder.Gradle', class.name(builder))
    local settings = builder:getSettings()
    local exp = {
      plugins = { id = { 'java' } },
      group = 'pkg',
      version = '0.1.0',
      application = { mainClass = 'pkg.Main' },
      repositories = { mavenCentral = {} },
      dependencies = {
        testImplementation = { 'org.junit.jupiter:junit-jupiter-engine:5.11.4' }
      },
      java = {
        toolchain = { languageVersion = { ['JavaLanguageVersion.of'] = { '8' } } }
      },
      -- extra:
      testframework = {
        executable = 'gradle',
        name = 'junit5' -- identified from dependencies.testImplementation
      },
    }
    assert.same(exp, settings)
    assert.is_table(lang:getTestFramework()) -- via synchSrc
    -- lang:findTestFramework() -- 2th part of the Lang.resolve()
  end)


  it("findProjectBuilder - Maven", function()
    local lang = JavaLang:new()
    local a = pkgApp0

    assert.same(true, sys:set_file_content(a.mainclass_path, a.mainclass_body))
    assert.same(true, sys:set_file_content(a.pom_xml_path, a.pom_xml))

    assert.is_nil(lang.builder)
    lang.project_root = a.proj_root
    lang:findProjectBuilder('pom.xml')
    assert.same('env.langs.java.builder.Maven', class.name(lang.builder))
  end)


  it("findProjectBuilder - Ant", function()
    local a = pkgApp0

    assert.same(true, sys:set_file_content(a.mainclass_path, a.mainclass_body))
    assert.same(true, sys:set_file_content(a.build_xml_path, a.build_xml))

    local lang = JavaLang:new()
    assert.is_nil(lang.builder)
    lang.project_root = a.proj_root
    lang:findProjectBuilder('build.xml') -- payload
    assert.same('env.langs.java.builder.Ant', class.name(lang.builder))
  end)


  it("findProjectBuilder - JBuilder(FlatStructure)", function()
    local a = pkgApp0
    local dirconf_path = a.proj_parent_dir .. dir_conf_basename
    local dirconf_yml = "project_roots:\n  - pr0j\n"

    assert.same(true, sys:set_file_content(dirconf_path, dirconf_yml))
    assert.same(true, sys:set_file_content(a.mainclass_path, a.mainclass_body))

    local lang = JavaLang:new()
    assert.is_nil(lang.builder)
    lang.project_root = a.proj_root
    assert.same({}, pcache.getDirConfings())
    lang:findProjectBuilder(nil) -- payload  first attempt
    assert.is_nil(class.name(lang.builder))

    local exp = { { project_roots = { 'pr0j' } } }
    assert.same(exp, (pcache.readDirConf(a.proj_parent_dir) or E).conf)

    lang:findProjectBuilder(nil) -- payload
    assert.same('env.langs.java.JBuilder', class.name(lang.builder))
  end)
end)

describe("env.langs.java.JLang GotoDefintion", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
  end)

  ---@diagnostic disable-next-line: unused-local
  local linesSource1 = {
    'package pkg;',                               --  1
    '',                                           --  2
    'import pkg.nested.FirstClass;',              --  3
    'import pkg.nested.SecondClass ; // comment', --  4
    'import  pkg.nested.ThirdClass; ',            --  5
    ' import static pkg.nested.FourthClass;',     --  6
    '// import static pkg.nested.X1Class;',       --  7
    '/**',                                        --  8
    '/* import static pkg.nested.X1Class;',       --  9
    ' * import static pkg.nested.X2Class;',       -- 10
    '   import static pkg.nested.X2Class;',       -- 11
    ' */',                                        -- 12
    '',                                           -- 13
    'public class App {',                         -- 14
    '    public static main(String[] args) {',    -- 15
    '        new SomeClass();',                   -- 16
    '    }',                                      -- 17
    '}',                                          -- 18
    --                 ^
    --345678901234567890123456789
  }
  -- TODO GotoDefintion byClassName
  --
end)

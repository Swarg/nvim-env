-- 28-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local M = require 'env.langs.svelte.snippets'

describe("env.langs.svelte.snippets", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    -- Lang.clearCache()
  end)

  it("handler_remove_attr_class", function()
    local lines = {
      '', -- 1
      '  <li class="exercise 2ms-hdd">',
      '    <a href="/tutorial/reactive-assignments" class="2mshdd">Assignments</a>',
      '  </li>',
      ' <li class="exercise 2ms-hd_d">',
      '   <a href="/tutorial/reactive-declarations" class="2mshdd" >Declarations</a>',
      ' </li>',
      '',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 1, 7)

    local ok, err = M.handler_remove_attr_class()
    assert.same({ true, nil }, { ok, err })
    local exp = {
      '',
      '  <li>',
      '    <a href="/tutorial/reactive-assignments">Assignments</a>',
      '  </li>',
      ' <li>',
      '   <a href="/tutorial/reactive-declarations" >Declarations</a>',
      ' </li>',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_extract_to_route", function()
    local proot = "/home/dev/proj/"
    local src_routes = proot .. "src/routes/"
    local page_path = src_routes .. "+page.svelte"
    local lines = {
      'my_new_route', -- 1
      '<script>',
      '</script>',
      '<p>to be moved to given route<p>',
    }
    sys:set_file(page_path, lines)
    local bufnr = nvim:open(page_path, 1, 4)

    local ok, err = M.handler_extract_to_route()
    assert.same({ true, nil }, { ok, err })

    assert.same({}, nvim:get_lines(bufnr, 0, -1)) -- cleaned

    local new_page = src_routes .. 'my_new_route/+page.svelte'
    local exp_new_page_content = {
      '<script>',
      '</script>',
      '<p>to be moved to given route<p>'
    }
    assert.same(exp_new_page_content, sys:get_file_lines(new_page))
  end)

  --
  --
  it("handler_new_component", function()
    local proot = "/home/dev/proj/"
    local src_routes = proot .. "src/routes/"
    local page_path = src_routes .. "+page.svelte"
    local lines = {
      '<script>',
      '  import Nested from \'./Nested.svelte\'',
      "  let state = ''",
      '</script>',
      '',
      '<Nested answer="42" />',
      '',
      '<Componet answer=42 attr2={state}/>'
    }
    sys:set_file(page_path, lines)
    local bufnr = nvim:open(page_path, 8, 4)

    local ok, err = M.handler_new_component()

    assert.same({ true, nil }, { ok, err })
    -- todo add import to parent svelte-file
    local exp = {
      '<script>',
      "  import Nested from './Nested.svelte'",
      "  import Componet from './Componet.svelte'",
      "  let state = ''",
      '</script>',
      '',
      '<Nested answer="42" />',
      '',
      '<Componet answer=42 attr2={state}/>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
    local exp_code = [[
<script>
  export let answer;
  export let attr2;
</script>

<p>The answer is: {answer}</p>
<p>The attr2 is: {attr2}</p>

<style>
</style>
]]
    assert.same(exp_code, sys:get_file_content(src_routes .. 'Componet.svelte'))
  end)

  --
  it("handler_build_urls", function()
    local lines = {
      '',        -- 1
      'title-1', -- 2
      'title-2',
      'title-3',
      '',
      'url-1',
      'url-2',
      'url-3', -- 8
      '',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 9)

    local ok, err = M.handler_build_urls()
    assert.same({ true, nil }, { ok, err })
    local exp = {
      '',
      '<a href="/url-1">title-1</a>',
      '<a href="/url-2">title-2</a>',
      '<a href="/url-3">title-3</a>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_build_urls", function()
    local lines = {
      '', -- 1
      ':list',
      'title-1',
      'title-2',
      '',
      'url-1',
      'url-2',
      '', -- 8
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 7)

    local ok, err = M.handler_build_urls()
    assert.same({ true, nil }, { ok, err })
    local exp = {
      '',
      '<lo>',
      '  <li><a href="/url-1">title-1</a></li>',
      '  <li><a href="/url-2">title-2</a></li>',
      '</lo>',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_build_urls", function()
    local lines = {
      '', -- 1
      ':wrap <tag>|</tag>',
      'title-1',
      'title-2',
      '',
      'url-1',
      'url-2',
      '', -- 8
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 7)

    local ok, err = M.handler_build_urls()
    assert.same({ true, nil }, { ok, err })
    local exp = {
      '',
      '<tag><a href="/url-1">title-1</a></tag>',
      '<tag><a href="/url-2">title-2</a></tag>',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_wrapto_a_href only link", function()
    local lines = {
      '  https://example.com/path',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = { '  <a href="https://example.com/path">path</a>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_wrapto_a_href cmd|link", function()
    local lines = {
      'li|https://example.com/path',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = {
      '<li><a href="https://example.com/path">path</a></li>',
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_wrapto_a_href cmd|link", function()
    local lines = {
      'li|https://example.com/path|The Title',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = {
      '<li><a href="https://example.com/path">The Title</a></li>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_wrapto_a_href cmd|link", function()
    local lines = {
      '  li|https://example.com/path|The Title',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = {
      '  <li><a href="https://example.com/path">The Title</a></li>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)
  it("handler_wrapto_a_href cmd|link", function()
    local lines = {
      '  li| https://example.com/path | The Title',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = {
      '  <li><a href="https://example.com/path">The Title</a></li>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_wrapto_a_href", function()
    local lines = {
      '  li|https://example.com/path',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)

    assert.same({ true, nil }, { M.handler_wrapto_a_href() })
    local exp = {
      '  <li><a href="https://example.com/path">path</a></li>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_wrapto_ul_li", function()
    local lines = {
      '',
      '  item 1',
      '  item 2',
      '  item 3',
      '',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 4)

    assert.same({ true, nil }, { M.handler_wrapto_ul_li() })
    local exp = {
      '',
      '<ul>',
      '  <li>item 1</li>',
      '  <li>item 2</li>',
      '  <li>item 3</li>',
      '</ul>',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_wrapto_ul_li", function()
    local lines = {
      '',
      '  - item 1',
      '  - item 2',
      '',
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 3)

    assert.same({ true, nil }, { M.handler_wrapto_ul_li() })
    local exp = {
      '',
      '<ul>',
      '  <li>item 1</li>',
      '  <li>item 2</li>',
      '</ul>',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_gen_new_routes", function()
    local proot = "/home/dev/proj/"
    local src_routes = proot .. "src/routes/"
    local page_path = src_routes .. "+page.svelte"
    local lines = {
      "<script>",
      "  /* layout */",
      "  import Nav from '/src/routes/tutorial/Nav.svelte'",
      "  import '/src/global.css';",
      "  /* layout end */",
      "</script>",
      "",
      "<Nav></Nav>",
      "",
      '<p> Introduction </p>',
      '<ul>',
      '  <li><a href="/tutorial/part-1-basic/welcome-to-svelte">Welcome to Svelte</a> </li>',
      '  <li><a href="/tutorial/part-1-basic/your-first-component">Your first component</a> </li>',
      '  <li><a href="/tutorial/part-1-basic/dynamic-attributes">Dynamic attributes</a> </li>',
      '  <li><a href="/tutorial/part-1-basic/styling">Styling</a> </li>',
      '  <li><a href="/tutorial/part-1-basic/nested-components">Nested components</a> </li>',
      '  <li><a href="/tutorial/part-1-basic/html-tags">HTML tags</a> </li>',
      '</ul>'
    }
    sys:set_file(page_path, lines)
    local bufnr = nvim:open(page_path, 8, 4)
    nvim:set_selection_visual(bufnr, 12, 18)

    -- require'alogger'.fast_setup(false, 0)
    assert.same({ true, nil }, { M.handler_gen_new_routes() })

    local exp = [[
/home/dev/proj/src/routes/
    +page.svelte
    tutorial/
        part-1-basic/
            dynamic-attributes/
                +page.svelte
            html-tags/
                +page.svelte
            nested-components/
                +page.svelte
            styling/
                +page.svelte
            welcome-to-svelte/
                +page.svelte
            your-first-component/
                +page.svelte
8 directories, 7 files
]]
    assert.same(exp, sys:tree(src_routes))
    local dir = src_routes .. 'tutorial/part-1-basic/'
    local exp1 = {
      '<script>',
      '  /* layout */',
      "  import Nav from '/src/routes/tutorial/Nav.svelte'",
      "  import '/src/global.css';",
      '</script>',
      '',
      '<Nav></Nav>',
      '',
      '<h1>Welcome to Svelte</h1>',
      '',
      '<style>',
      '</style>'
    }
    assert.same(exp1, sys:get_file_lines(dir .. 'welcome-to-svelte/+page.svelte'))
    local exp2 = {
      '<script>',
      '  /* layout */',
      "  import Nav from '/src/routes/tutorial/Nav.svelte'",
      "  import '/src/global.css';",
      '</script>',
      '',
      '<Nav></Nav>',
      '',
      '<h1>Styling</h1>',
      '',
      '<style>',
      '</style>'
    }
    assert.same(exp2, sys:get_file_lines(dir .. 'styling/+page.svelte'))
  end)


  it("handler_gen_new_routes with command in comment", function()
    local proot = "/home/dev/proj/"
    local src_routes = proot .. "src/routes/"
    local page_path = src_routes .. "tutorial/first-step/+page.svelte"
    local lines = {
      "<script>",
      "</script>",
      "",
      '<p> Introduction </p>',
      '<ul>',
      '  <!-- / -->', -- from webapp root not
      '  <li><a href="/tutorial/welcome">Welcome</a> </li>',
      '  <li><a href="/tutorial/first-step">First step</a> </li>',
      '</ul>'
    }
    sys:set_file(page_path, lines)
    local bufnr = nvim:open(page_path, 8, 4)
    nvim:set_selection_visual(bufnr, 5, 9)

    assert.same({ true, nil }, { M.handler_gen_new_routes() })

    local exp = [[
/home/dev/proj/src/routes/
    tutorial/
        first-step/
            +page.svelte
        welcome/
            +page.svelte
3 directories, 2 files
]]
    assert.same(exp, sys:tree(src_routes))
  end)
end)

-- 29-08-2024 @author Swarg
--

local ClassInfo = require("env.lang.ClassInfo")
-- local ClassResolver = require("env.lang.ClassResolver")
local SLang = require 'env.langs.svelte.SLang'
local NPMBuilder = require 'env.langs.svelte.builder.NPM'

local H = require 'env.lang.oop.stub.ATestHelper'


---@func new_cli(gen:env.lang.oop.LangGen?, cmd_line:string): Cmd4Lua
local M = {}
H.inherit(M) -- inherit functions from abstract-lang helper

M.NPMBuilder = NPMBuilder
M.SLang = SLang

-- class types
M.CT = ClassInfo.CT
-- M.split = su.split

-- common test data from empty maven project
M.webapp0 = {
  project_root = "/tmp/study-svelte/concepts/"
}


M.webapp0.routes_dir = M.webapp0.project_root .. 'src/routes/'

M.webapp0.package_json = M.webapp0.project_root .. "package.json"
M.webapp0.package_json_body = [[
{
	"name": "sandbox",
	"version": "0.0.1",
	"private": true,
	"type": "module",
	"scripts": {
		"dev": "vite dev",
		"build": "vite build",
		"preview": "vite preview"
	},
	"devDependencies": {
		"svelte": "^4.2.7",
		"@sveltejs/kit": "^2.0.0",
		"@sveltejs/adapter-auto": "^3.0.0",
		"@sveltejs/vite-plugin-svelte": "^3.0.0",
		"vite": "^5.0.3"
	}
}
]]

M.webapp0.main_page = M.webapp0.routes_dir .. "+page.svelte"
M.webapp0.main_page_body = [[
<div data-sveltekit-preload-data="off">

<h1>Welcome SvelteKit Learners</h1>

<p>Main Concepts of the Svelte</p>

<lo>
  <li><a href="/template">- 1.1 Svelte Templates</a></li>
  <li><a href="/templ_logic">- 1.2 Tempate Logic (if-else)</a></li>
  <li><a href="/import_module">- 1.3 Importing Modules</a></li>
  <li><a href="/component_props">- 1.4 Svelte Component Props</a></li>
  <li><a href="/reactivity">- 1.5 Svelte's Reactivity</a></li>
  <li><a href="/slots">- 1.6 Slots</a></li>
  <li><a href="/named-slots">- 1.7 Svelte Stores</a></li>
</lo>

<p>See source code in the src/routes/template/+page.svelte <p>

</div>
]]


function M.mkSLang(project_root)
  local lang = SLang:new(nil, project_root)
  return lang
end

--
---@param t table{project_root}
---@param sys stub.os.OSystem
---@param files table{fn=body}
---@return env.langs.svelte.SLang
function M.setupProject(sys, t, files)
  return H.setupProject(SLang, sys, t, files)
end

return M

-- 28-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.svelte.util.routing'

describe("env.langs.svelte.util.routing", function()
  it("get_new_routes_page", function()
    local f = M.get_new_routes_page
    local exp = '/home/dev/proj/src/routes/subpath/+page.svelte'
    assert.same(exp, f("/home/dev/proj/src/routes/+page.svelte", "subpath"))
  end)

  it("get_app_route", function()
    local f = M.get_app_route
    assert.same('/path/to', f("/tmp/project/src/routes/path/to/"))
    assert.same('/path/to', f("/tmp/project/src/routes/path/to"))
    assert.same('/', f("/tmp/project/src/routes/"))
  end)

  it("get_routes_root", function()
    local f = M.get_routes_root
    assert.same('/tmp/project/src/routes/', f("/tmp/project/src/routes/path/to/"))
  end)

  it("get_routes_from_selected_lines", function()
    local f = M.get_routes_from_selected_lines
    local lines = {
      '  <li>',
      '    <a href="/tutorial/dom-events">DOM events</a>',
      '  </li>',
      '  <li><a href="/tutorial/inline-handlers">Inline handlers</a></li>',
      '  <li><a href="/tutorial/event-modifiers">Event modifiers</a></li>'
    }
    local exp = {
      { href = '/tutorial/dom-events',      lnum = 2 },
      { href = '/tutorial/inline-handlers', lnum = 4 },
      { href = '/tutorial/event-modifiers', lnum = 5 }
    }
    assert.same(exp, f(lines))
  end)

  it("refactor_route0", function()
    local f = M.refactor_route0
    local line = '    <a href="/tutorial/dom-events">DOM events</a>'
    local href = '/tutorial/dom-events'
    local exp = {
      '    <a href="/tutorial/events/dom-events">DOM events</a>',
      '/tutorial/events/dom-events'
    }
    assert.same(exp, { f(line, href, 'events', 2) })
  end)
end)

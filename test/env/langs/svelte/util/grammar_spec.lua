-- 28-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.svelte.util.grammar'

describe("env.langs.svelte.util.grammar", function()
  it("cpLexemsWithPos", function()
    local f = function(line) return M.offhand.cpLexemsWithPos:match(line) end
    local exp = {
      { '1', 2, 2 },
      { '+', 3, 3 },
      { ';', 4, 4 },
      { '2', 5, 5 }
    }
    assert.same(exp, f(" 1+;2"))
    local exp2 = {
      { '<',         3,  3 },
      { 'Compoment', 4,  12 },
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '{',         19, 19 },
      { '42',        20, 21 },
      { '}',         22, 22 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same(exp2, f("  <Compoment attr={42} />"))
  end)

  it("cpLexemsWithPos", function()
    local f = function(line) return M.offhand.cpLexemsWithPos:match(line) end
    local exp =
    {
      { "import",           3,  8 },
      { "Inner",            10, 14 },
      { "from",             16, 19 },
      { "'./Inner.svelte'", 21, 36 },
      { ";",                37, 37 }
    }
    local res = f("  import Inner from './Inner.svelte';")
    assert.same(exp, res)
  end)

  it("cpLexemsWithPos", function()
    local f = function(line) return M.offhand.cpLexemsWithPos:match(line) end
    local res = f("import { createEventDispatcher } from 'svelte';")
    local exp = {
      { "import",                1,  6 },
      { "{",                     8,  8 },
      { "createEventDispatcher", 10, 30 },
      { "}",                     32, 32 },
      { "from",                  34, 37 },
      { "'svelte'",              39, 46 },
      { ";",                     47, 47 }
    }
    assert.same(exp, res)
  end)
end)

-- 30-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.svelte.util.html'

describe("env.langs.svelte.util.html", function()

  it("extract_ahref", function()
    local f = M.extract_ahref
    assert.same('/tutor/props', f('<li><a href="/tutor/props">props</a></li>'))
    assert.same('/', f('<li><a href="/">props</a></li>'))
    assert.same('/main', f('<li><a href="/main">props</a></li>'))
    assert.same('/main', f('<li><a  href="/main">props</a></li>'))
    assert.same('/main', f('<li><a  href = "/main" >props</a></li>'))
  end)

  it("has_html_tag", function()
    local f = M.has_html_tag
    assert.same(false, f(""))
    assert.same(true, f("<a>"))
    assert.same(true, f("</a>"))
    assert.same(false, f("</a"))
  end)

end)

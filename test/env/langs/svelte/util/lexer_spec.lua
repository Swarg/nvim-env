-- 28-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.svelte.util.lexer'
local LT = require 'env.lang.api.constants'.LEXEME_TYPE

describe("env.langs.svelte.util.lexer", function()
  it("indexOfByPos", function()
    local f = M.indexOfByPos
    local lexems = {
      { '<',         3,  3 },
      { 'Component', 4,  12 }, -- 2
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '{',         19, 19 },
      { '42',        20, 21 },
      { '}',         22, 22 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same(2, f(lexems, 8))
    assert.same(6, f(lexems, 20))
    assert.same(6, f(lexems, 21))
    assert.same(9, f(lexems, 25))
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local lexems = {
      { '<',         3,  3 },
      { 'Component', 4,  12 }, -- 2
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '{',         19, 19 },
      { '42',        20, 21 },
      { '}',         22, 22 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same(LT.TAG_SINGLE, f(lexems, 5))
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local lexems = {
      { '<',      3,  3 },
      { 'Button', 4,  12 },
      { '>',      25, 25 }
    }
    assert.same(LT.TAG_PAIRED, f(lexems, 5))
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local lexems = {
      { "import",           3,  8 },
      { "Inner",            10, 14 },
      { "from",             16, 19 },
      { "'./Inner.svelte'", 21, 36 },
      { ";",                37, 37 }
    }
    assert.same(LT.COMPONENT, f(lexems, 11))
  end)

  it("getTypeOfPos", function()
    local f = M.getTypeOfPos
    local lexems = {
      { "import",                1,  6 },
      { "{",                     8,  8 },
      { "createEventDispatcher", 10, 30 },
      { "}",                     32, 32 },
      { "from",                  34, 37 },
      { "'svelte'",              39, 46 },
      { ";",                     47, 47 }
    }
    assert.same(LT.FUNCTION, f(lexems, 11))
  end)

  it("isSingleTag single", function()
    local f = M.isSingleTag
    local lexems = {
      { '<',         3,  3 },
      { 'Component', 4,  12 }, -- 2
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '""',        19, 19 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same(true, f(lexems, 2))
  end)

  it("isSingleTag paired", function()
    local f = M.isSingleTag
    local lexems = {
      { '<',   3,  3 },
      { 'tag', 4,  12 },
      { '>',   25, 25 },
      { '<',   25, 25 },
      { '/',   25, 25 },
      { 'tag', 25, 25 },
      { '>',   25, 25 }
    }
    assert.same(false, f(lexems, 2))
  end)

  it("getAttribures", function()
    local f = M.getAttribures
    local lexems = {
      { '<',         3,  3 },
      { 'Component', 4,  12 }, -- 2
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '""',        19, 19 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same({ 'attr' }, f(lexems, 2))
  end)

  it("indexof_token", function()
    local f = M.indexof_token
    local lexems = {
      { '<',         3,  3 },
      { 'Component', 4,  12 }, -- 2
      { 'attr',      14, 17 },
      { '=',         18, 18 },
      { '""',        19, 19 },
      { '/',         24, 24 },
      { '>',         25, 25 }
    }
    assert.same(2, f(lexems, 'Component'))

    local lexems2 = {
      { "import",           3,  8 },
      { "Inner",            10, 14 },
      { "from",             16, 19 },
      { "'./Inner.svelte'", 21, 36 },
      { ";",                37, 37 }
    }
    assert.same(3, f(lexems2, 'from'))
  end)
end)

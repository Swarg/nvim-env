-- 28-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim

local sys = nvim:get_os()

local M = require 'env.langs.svelte.util.gen'


describe("env.langs.svelte.util.gen", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    -- Lang.clearCache()
    -- D.on = false
  end)

  it("gen_new_compoment by relpath from import", function()
    local f = M.gen_new_compoment
    local params = {
      fn = "/tmp/src/App.svelte",
      module = { path = "./pages/club/SingleClub.svelte" }
    }
    sys:set_file_content('/tmp/src/App.svelte', '.')
    local exp = {
      '/tmp/src/pages/club/SingleClub.svelte',
      "<script>\n</script>\n\n<style>\n</style>\n"
    }

    assert.same(exp, { f('SingleClub', params) })
  end)

  it("gen_new_compoment by abs path from import", function()
    local f = M.gen_new_compoment
    local params = {
      fn = "/tmp/src/routes/+page.svelte",
      module = { path = "/club/SingleClub.svelte" }
    }
    sys:set_file_content(params.fn, '.')
    local exp = {
      '/tmp/src/routes/club/SingleClub.svelte',
      "<script>\n</script>\n\n<style>\n</style>\n"
    }

    assert.same(exp, { f('SingleClub', params) })
  end)

  it("gen_new_compoment by abs path from import", function()
    local f = M.gen_new_compoment
    local params = {
      fn = "/tmp/src/routes/nested/+page.svelte",
      module = { path = "/club/SingleClub.svelte" }
    }
    sys:set_file_content(params.fn, '.')
    local exp = {
      '/tmp/src/routes/club/SingleClub.svelte',
      "<script>\n</script>\n\n<style>\n</style>\n"
    }

    assert.same(exp, { f('SingleClub', params) })
  end)

  it("gen_new_compoment by abs path from import", function()
    local f = M.gen_new_compoment
    local params = {
      fn = "/tmp/src/routes/nested/+page.svelte",
      module = { path = "./club/SingleClub.svelte" }
    }
    sys:set_file_content(params.fn, '.')
    local exp = {
      '/tmp/src/routes/nested/club/SingleClub.svelte',
      "<script>\n</script>\n\n<style>\n</style>\n"
    }

    assert.same(exp, { f('SingleClub', params) })
  end)

  it("gen_new_compoment", function()
    local f = M.gen_new_compoment
    local params = { fn = "/tmp/Parent.svelte", attrs = { "attr1" } }
    sys:set_file_content('/tmp/Parent.svelte', '.')
    local path, code = f('Compoment', params)
    assert.same('/tmp/Compoment.svelte', path)
    local exp = [[
<script>
  export let attr1;
</script>

<p>The attr1 is: {attr1}</p>

<style>
</style>
]]
    assert.same(exp, code)

    local exp_params_with_import = {
      fn = '/tmp/Parent.svelte',
      attrs = { 'attr1' },
      import = { "  import Compoment from './Compoment.svelte'" }
    }
    assert.same(exp_params_with_import, params)
  end)
end)

--
--
--
describe("env.langs.svelte.util.gen", function()
  it("get_comment_content", function()
    local f = M.get_comment_content
    assert.same('comment', f("/*comment*/"))
    assert.same('comment', f("/* comment */"))
    assert.same('this is content', f("/*  this is content */"))
    assert.same('this is content', f("  /*  this is content */  "))
    assert.same('comment', f("<!--comment-->"))
    assert.same('content', f("<!--content -->"))
    assert.same('content', f("  <!--  content -->"))
  end)




  it("gen_new_routes0", function()
    local page_abs_path = '/tmp/proj/src/routes/+page.svelte'
    local urls = { { 'url1', 'title1' }, { 'url2', 'title2' } }
    local exp = {
      {
        path = '/tmp/proj/src/routes/url1/+page.svelte',
        body = "<script>\n</script>\n\n<style>\n</style>\n",
      },
      {
        path = '/tmp/proj/src/routes/url2/+page.svelte',
        body = "<script>\n</script>\n\n<style>\n</style>\n",
      }
    }
    assert.same(exp, M.gen_new_routes0(urls, page_abs_path))
  end)

  it("gen_new_routes0 with desc", function()
    local page_abs_path = '/tmp/proj/src/routes/+page.svelte'
    local urls = { { 'url1', 'title1' } }
    local opts = {
      add_desc = true,
    }
    local exp = {
      {
        body = "<script>\n</script>\n\n<h1>title1</h1>\n\n<style>\n</style>\n",
        path = '/tmp/proj/src/routes/url1/+page.svelte'
      }
    }
    assert.same(exp, M.gen_new_routes0(urls, page_abs_path, opts))
  end)

  it("mk_route_path", function()
    local f = M.mk_route_path
    local exp = '/home/me/dev/webapp/src/routes/parent/child/+page.svelte'
    assert.same(exp, f('/home/me/dev/webapp/src/routes/', 'child', 'parent'))

    local exp2 = '/home/me/dev/webapp/src/routes/not-child/+page.svelte'
    assert.same(exp2, f('/home/me/dev/webapp/src/routes/', '/not-child', 'parent'))
  end)
end)

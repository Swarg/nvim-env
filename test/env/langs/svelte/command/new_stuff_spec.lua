-- 30-08-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local Lang = require 'env.lang.Lang'
local M = require 'env.langs.svelte.command.new_stuff'
local H = require("env.langs.svelte.ahelper")
local new_cli, setupProject = H.new_cli, H.setupProject



describe("env.langs.svelte.command.new_stuff", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
  end)

  --
  -- replace href in selected lines and rename(move) dirs if exists
  -- goal:
  --  - move all childs in tutorial directory into tutorial/event/
  --  in fs and in selected lines (update hrefs)
  --
  it("cmd_refactor_routes", function()
    local lines = {
      '',
      '  <li>',
      '    <a href="/tutorial/reactive-assignments">Assignments</a>',
      '  </li>',
      ' <li>',
      '   <a href="/tutorial/reactive-declarations" >Declarations</a>',
      ' </li>',
      ''
    }
    local bufnr = nvim:new_buf(lines, 1, 4)
    nvim:set_selection_visual(bufnr, 2, 8)

    local cmd_line = 'event 2'
    local w = new_cli(nil, cmd_line)

    local rd = H.webapp0.routes_dir

    local lang = setupProject(sys, H.webapp0, {
      [rd .. '/tutorial/reactive-assignments/+page.svelte'] = '',
      [rd .. '/tutorial/reactive-declarations/+page.svelte'] = '',
    })
    w.vars.gen = lang:getLangGen()
    local exp_project_tree = [[
/tmp/study-svelte/concepts/
    package.json
    src/
        routes/
            +page.svelte
            tutorial/
                reactive-assignments/
                    +page.svelte
                reactive-declarations/
                    +page.svelte
5 directories, 4 files
]]
    assert.same(exp_project_tree, sys:tree(H.webapp0.project_root))

    local t = M.cmd_refactor_routes(w) -- workload
    assert.is_table(t) ---@cast t table
    local exp = {
      {
        { href = '/tutorial/reactive-assignments',  lnum = 2 },
        { href = '/tutorial/reactive-declarations', lnum = 5 }
      },
      {
        '  <li>',
        '    <a href="/tutorial/event/reactive-assignments">Assignments</a>',
        '  </li>',
        ' <li>',
        '   <a href="/tutorial/event/reactive-declarations" >Declarations</a>',
        ' </li>',
        ''
      }
    }
    assert.same(exp, { t.links, t.lines })
    local exp_lines = {
      '',
      '  <li>',
      '    <a href="/tutorial/event/reactive-assignments">Assignments</a>',
      '  </li>',
      ' <li>',
      '   <a href="/tutorial/event/reactive-declarations" >Declarations</a>',
      ' </li>',
      ''
    }
    assert.same(exp_lines, nvim:get_lines(bufnr, 0, -1))


    local exp_moved = [[
/tmp/study-svelte/concepts/
    package.json
    src/
        routes/
            +page.svelte
            tutorial/
                event/
                    reactive-assignments/
                        +page.svelte
                    reactive-declarations/
                        +page.svelte
6 directories, 4 files
]]
    assert.same(exp_moved, sys:tree(H.webapp0.project_root))
  end)
end)

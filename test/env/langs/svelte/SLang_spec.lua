-- 29-08-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local H = require 'env.langs.svelte.ahelper'

-- local logger = require 'logger'
local class = require 'oop.class'
local Lang = require 'env.lang.Lang'
local SLang = require 'env.langs.svelte.SLang'

local setupProject = H.setupProject


describe("env.langs.svelte.SLang", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
    -- D.on = false
  end)

  it("findProjectBuilder - NPM", function()
    local lang = SLang:new()
    local wa = H.webapp0

    assert.same(true, sys:set_file_content(wa.package_json, wa.package_json_body))
    assert.same(true, sys:set_file_content(wa.main_page, wa.main_page_body))

    assert.is_nil(lang.builder)
    lang.project_root = wa.project_root

    lang:findProjectBuilder('package.json')
    assert.same('env.langs.svelte.builder.NPM', class.name(lang.builder))
    local exp = {
      name = 'sandbox',
      private = true,
      type = 'module',
      version = '0.0.1',
      scripts = {
        build = 'vite build',
        dev = 'vite dev',
        preview = 'vite preview'
      },
      devDependencies = {
        ['@sveltejs/adapter-auto'] = '^3.0.0',
        ['@sveltejs/kit'] = '^2.0.0',
        ['@sveltejs/vite-plugin-svelte'] = '^3.0.0',
        svelte = '^4.2.7',
        vite = '^5.0.3'
      }
    }
    assert.same(exp, lang.builder:getSettings())
  end)

  it("refactorRoute", function()
    local files = {
      [H.webapp0.routes_dir .. 'tutor/section1/+page.svelte'] = '',
      [H.webapp0.routes_dir .. 'tutor/section2/+page.svelte'] = '',
    }
    local lang = setupProject(sys, H.webapp0, files)
    local exp = [[
/tmp/study-svelte/concepts/
    package.json
    src/
        routes/
            +page.svelte
            tutor/
                section1/
                    +page.svelte
                section2/
                    +page.svelte
5 directories, 4 files
]]
    assert.same(exp, sys:tree(H.webapp0.project_root))

    local res = { lang:refactorRoute('/tutor/event/section1', '/tutor/section1') }
    local exp_names = {
      '/tmp/study-svelte/concepts/src/routes/tutor/event/section1', -- new
      '/tmp/study-svelte/concepts/src/routes/tutor/section1'        -- old
    }
    assert.same(exp_names, res)

    local exp_new_state = [[
/tmp/study-svelte/concepts/
    package.json
    src/
        routes/
            +page.svelte
            tutor/
                event/
                    section1/
                        +page.svelte
                section2/
                    +page.svelte
6 directories, 4 files
]]
    assert.same(exp_new_state, sys:tree(H.webapp0.project_root))
  end)
end)

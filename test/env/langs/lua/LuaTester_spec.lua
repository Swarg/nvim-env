-- 03-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.langs.lua.LuaTester'
local ExecParams = require 'env.lang.ExecParams'

describe("env.langs.lua.LuaTester", function()
  local specfn = "./src/test/_lang/stuff_spec.lua"
  local lines_of_spec_file_with_deps = {
    "-- @author ...",
    "-- Goals: testing...",
    "-- dependencies:",
    "--   - luarocks: alogger, cli-app-base, lhttp, db-env",
    "--   - something alse may be git-repo",
    "--",
    'require("busted.runner")()',
    'local assert = require "luassert"',
    '',
  }
  local lines_of_spec_file_with_deps_no_commons = {
    "-- @author ...",
    "-- Goals: testing...",
    "-- dependencies:",
    "--   - luarocks: alogger, cli-app-base, lhttp, db-env",
    "--   - something alse may be git-repo",
    "--",
    'require("busted.runner")()',
    'local assert = require "luassert"',
    '',
  }

  it("getRawDependenciesFromComments", function()
    local f = M.loadRawDependenciesFromComments
    local exp = {
      'luarocks: alogger, cli-app-base, lhttp, db-env',
      'something alse may be git-repo'
    }
    assert.same(exp, f(specfn, lines_of_spec_file_with_deps))
  end)

  it("getRawDependenciesFromComments", function()
    local f = M.loadRawDependenciesFromComments
    assert.same(nil, f(specfn, {}))
    assert.same(nil, f(specfn, { '-- dependencies:' }))
    assert.is_nil(f(specfn, { '-- dependencies:', 'luarocks:' }))

    assert.same({ 'luarocks' }, f(specfn, { '-- dependencies:', '-- luarocks:' }))
  end)

  it("loadSettingsFromSpecFile", function()
    local f = M.loadSettingsFromSpecFile
    local exp = {
      dependencies = { 'alogger', 'cli-app-base', 'lhttp', 'db-env' }
    }
    assert.same(exp, f(specfn, lines_of_spec_file_with_deps))
  end)

  it("loadSettingsFromSpecFile", function()
    local f = M.loadSettingsFromSpecFile
    local exp = {
      dependencies = { 'alogger', 'cli-app-base', 'lhttp', 'db-env' }
    }
    assert.same(exp, f(specfn, lines_of_spec_file_with_deps_no_commons))
  end)

  it("fixRootDirInRunParams", function()
    local lt = M:new({
      actual_project_root = "~/dev/web/library/",
      project_root = --[[]] "~/dev/web/library/src/test/_lua/",
      target_lang = "java",
      src = "src/",
      test = "./"
    })
    local params = ExecParams:new({
      cwd = "~/dev/web/library/src/test/_lua/",
      cmd = "busted",
      args = { "rest_endpoints_spec.lua" },
    })
    local exp_fixed = {
      cwd = '~/dev/web/library/',
      cmd = 'busted',
      args = { 'src/test/_lua/rest_endpoints_spec.lua' },
    }
    assert.same(exp_fixed, lt:fixRootDirInRunParams(params):toArray())
  end)

  --[==[ TODO test caching with env.cache.projects via stub.OSystem
  it("add_project_root first luatest", function()
    -- when
    local specfile = '/home/me/dev/proj/src/main/_lua/a_spec.lua'
    local makefile = '/home/me/dev/proj/Makefile'
    sys:set_file_content(specfile, 'some tests for j')
    sys:set_file_content(makefile, 'some build scripts')
    local exp_tree = [[
/home/me/dev/
    proj/
        Makefile
        src/
            main/
                _lua/
                    a_spec.lua
4 directories, 2 files
]]
    assert.same(exp_tree, sys:tree('/home/me/dev/'))

    require'alogger'.fast_setup(false, 0)
    M.find_root({ 'Makefile' }, specfile) -- add_proot('/home/me/dev/proj/src/test/_lua/')
    local exp = { ['/home/me/dev/proj/'] = { subprojects = { src = false } } }
    assert.same(exp, M.projects_cache)

    M.add_project_root('/home/me/dev/proj/')
    local exp2 = { ['/home/me/dev/proj/'] = { subprojects = { src = false } } }
    assert.same(exp2, M.projects_cache)

    local exp3 = '/home/me/dev/proj/'
    assert.same(exp3, M.find_root_in_cache(nil, specfile))
  end)
]==]
end)

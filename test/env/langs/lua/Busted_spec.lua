--
require 'busted.runner' ()
local assert = require('luassert')

local Busted = require("env.langs.lua.Busted")

---@diagnostic disable-next-line unused-local
local D = require("dprint")
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

-- WARN project root must ends with '/'
local luarocks_root = uv.cwd() .. '/test/env/resources/lua/luarocks/'
---@diagnostic disable-next-line unused-local
local busted0 = luarocks_root .. '.busted'

--[[
ROOT = {"spec"},
lpath = "src/?.lua;src/mypkg/?.lua;spec/?.lua;",
]]

describe("env.langs.lua.Busted", function()
  local busted = nil

  it("loadConfig", function()
    -- local fn = fs.join_path(luarocks_root, '.busted')
    -- TH.logger_setup()
    local b = Busted:new():setProjectRoot(luarocks_root)
    local exp = {
      _all = {
        coverage = false
      },
      default = {
        coverage = false,
        ROOT = { "spec" }, -- test dir
        helper = "spec.util.test_env",
        lpath = "src/?.lua;src/mypkg/?.lua;spec/?.lua;",
        verbose = true
      }
    }
    assert.same(exp, b:getConfig())
    busted = b
    -- print("[DEBUG] b:", inspect(b))
  end)

  it("findTestSuiteNameFor", function()
    -- print("[DEBUG] busted:", inspect(busted))
    assert.is_not_nil(busted)
    ---@cast busted table
    assert.is_nil(busted:findTestSuiteNameFor('src/mypkg/init'))
    local tsn, tsp = busted:findTestSuiteNameFor('spec/mypkg/init')
    assert.same('ROOT', tsn)
    assert.same('spec/', tsp)
  end)
end)

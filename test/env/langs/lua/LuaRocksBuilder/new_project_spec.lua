require 'busted.runner' ()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local LuaRocksBuilder = require 'env.langs.lua.LuaRocksBuilder'

local Lang = require 'env.lang.Lang'

describe("env.langs.lua.LuaRocksBuilder", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
  end)

  it("getDefaultProjectProperties", function()
    local f = LuaRocksBuilder.getDefaultProjectProperties
    local exp = {
      UI_TITLE = 'New LuaRocks Project',
      AUTHOR = 'AuthorName',
      YEAR = 2024,
      PROJECT_NAME = 'app',
      GROUP_ID = 'pkg',
      ARTIFACT_ID = 'app',
      VERSION = '0.1.0-1',
      DEPENDENCIES = '',
      TEST_FRAMEWORK = 'busted',
      HOMEPAGE = 'http://gitlab.com/me/app',
      LICENSE = 'MIT',
      LUA_VERSION = '5.1',
      MAINCLASS = 'pkg.Main',
      ROCKSPEC = 'app-0.1.0-1.rockspec',
      CLI_TOOL = false,
      LUA_INTERPRETER = 'lua',
    }
    assert.same(exp, f({ date = '18-11-2024' }))
  end)

  local opts4newpp = { date = '18-11-2024' }

  it("update_project_properties", function()
    local pp = LuaRocksBuilder.getDefaultProjectProperties(opts4newpp)

    assert.same('app-0.1.0-1.rockspec', pp.ROCKSPEC)
    assert.same('pkg.Main', pp.MAINCLASS)

    pp.ARTIFACT_ID = ''
    pp.PROJECT_NAME = 'lib'
    pp.VERSION = 'scm-1'
    pp.GROUP_ID = 'org.me'

    LuaRocksBuilder.update_project_properties(pp)

    assert.same('lib-scm-1.rockspec', pp.ROCKSPEC)
    assert.same('org.me.Main', pp.MAINCLASS)
  end)

  it("of", function()
    local pp = LuaRocksBuilder.getDefaultProjectProperties(opts4newpp)
    pp.project_root = '/tmp/luarocks_project/'
    local builder = LuaRocksBuilder.of(pp)
    assert.same('env.langs.lua.LuaRocksBuilder', builder._cname)
    assert.same('/tmp/luarocks_project/', builder.project_root)
    assert.same('app-0.1.0-1.rockspec', builder.buildscript)

    local exp = {
      package = 'app',
      version = '0.1.0-1',
      source = { url = 'URL for source tarball' },
      description = { homepage = 'a project homepage', license = 'MIT' },
      dependencies = { 'lua >= 5.1' },
      build = {
        modules = { mypkg = 'src/mypkg/init.lua -- FIXME' }, -- todo
        type = 'builtin'
      }
    }
    assert.same(exp, builder.settings)
  end)

  it("buildscript to string", function()
    local pp = LuaRocksBuilder.getDefaultProjectProperties(opts4newpp)
    pp.project_root = '/tmp/luarocks_project/'
    local builder = LuaRocksBuilder.of(pp)
    local res = builder:buildScriptToString()
    local exp = [[
---@diagnostic disable: lowercase-global
package = 'app'
version = '0.1.0-1'
source = {
  url = 'URL for source tarball'
}
description = {
  homepage = 'a project homepage',
  license = 'MIT'
}
dependencies = {
  'lua >= 5.1'
}
build = {
  type = 'builtin',
  modules = {
    mypkg = 'src/mypkg/init.lua -- FIXME'
  }
}
]]
    assert.same(exp, res)
  end)
end)

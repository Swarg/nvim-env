require 'busted.runner' ()
local assert = require 'luassert'

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

_G.TEST = true
local LuaLang = require 'env.langs.lua.LuaLang'
local LuaRocksBuilder = require 'env.langs.lua.LuaRocksBuilder'

local Lang = require 'env.lang.Lang'

describe("env.langs.lua.LuaRocksBuilder", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    Lang.clearCache()
  end)

  local opts4newpp = { date = '18-11-2024' }

  it("genProjectFiles", function()
    local pp = LuaRocksBuilder.getDefaultProjectProperties(opts4newpp)
    pp.project_root = '/tmp/new_proj'
    pp.no_date = true
    local builder = LuaRocksBuilder.of(pp)
    local opts = { dont_open = true }

    -- require 'alogger'.fast_setup()
    local lang, gen = LuaLang.genProjectFiles(pp, builder, opts)
    assert.same('/tmp/new_proj/', lang:getProjectRoot())

    local exp = [[
/tmp
    new_proj/
        spec/
            pkg/
                Main_spec.lua
        src/
            pkg/
                Main.lua
5 directories, 2 files
]]
    assert.same(exp, sys:tree('/tmp'))

    local exp_repo = [[
ok: /tmp/new_proj/src/pkg/Main.lua
ok: /tmp/new_proj/spec/pkg/Main_spec.lua
]]
    assert.same(exp_repo, gen:getReadableSaveReport())

    local exp_main = {
      '--   @author AuthorName',
      '--',
      '',
      "local class = require 'oop.class'",
      '',
      "class.package 'pkg'",
      '',
      '---@class pkg.Main',
      "local Main = class.new_class(nil, 'Main', {",
      '})',
      '',
      '-- constructor used in Main:new()',
      'function Main:_init()',
      'end',
      '',
      '',
      'class.build(Main)',
      'return Main'
    }
    assert.same(exp_main, sys:get_file_lines('/tmp/new_proj/src/pkg/Main.lua'))

    local exp_spec = {
      '-- @author AuthorName',
      'require("busted.runner")()',
      "local assert = require 'luassert'",
      "assert:set_parameter('TableFormatLevel', 33)",
      "local M = require 'pkg.Main'",
      '',
      'describe("pkg.Main", function()',
      '',
      '  it("success", function()',
      '    assert.same(1, 0)',
      '  end)',
      '',
      'end)'
    }
    assert.same(exp_spec, sys:get_file_lines('/tmp/new_proj/spec/pkg/Main_spec.lua'))
  end)

  it("createNewProject", function()
    local pp = LuaRocksBuilder.getDefaultProjectProperties(opts4newpp)
    pp.project_root = '/tmp/projb'
    local lang, gen = LuaLang.createNewProject(pp, opts4newpp)
    assert.same('env.langs.lua.LuaLang', lang._cname)

    local exp = [[
ok: /tmp/projb/src/pkg/Main.lua
ok: /tmp/projb/spec/pkg/Main_spec.lua
ok: /tmp/projb/app-0.1.0-1.rockspec
ok: /tmp/projb/README.md
ok: /tmp/projb/LICENSE
ok: /tmp/projb/.gitignore
ok: /tmp/projb/Makefile
]]
    assert.same(exp, gen:getReadableSaveReport())
  end)
end)

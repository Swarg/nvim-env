require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local class = require 'oop.class'
local fs = require('env.files')
local Lang = require('env.lang.Lang')
local LuaLang = require("env.langs.lua.LuaLang")
local LuaRocksBuilder = require("env.langs.lua.LuaRocksBuilder")
local Busted = require("env.langs.lua.Busted")
local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require('env.lang.ClassResolver')
-- local Editor = require("env.ui.Editor")
-- local Context = require("env.ui.Context")
-- local lua_parser = require("env.lua_parser")
-- local cp = require("env.util.code_parser")

---@diagnostic disable-next-line: unused-local
local D = require("dprint")
local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local H = require("env.langs.lua.ahelper")


-- WARN project root must ends with '/'
local luarocks_root = uv.cwd() .. '/test/env/resources/lua/luarocks/'
local src_init_lua0 = 'src/mypkg/init.lua'
local test_init_lua0 = 'spec/mypkg/init_spec.lua'

local src_init_lua = luarocks_root .. src_init_lua0
local test_init_lua = luarocks_root .. test_init_lua0


describe("env.langs.lua.LuaLang", function()
  it("Lang.getLang lua", function()
    local file = fs.join_path(luarocks_root, 'src/mypkg/init.lua')
    local lhandler = Lang.getLang(file)
    assert.is_not_nil(lhandler)
    ---@cast lhandler table LuaLang
    lhandler:resolve(file)

    assert.same("env.langs.lua.LuaLang", class.name(lhandler))

    assert.same(luarocks_root, lhandler:getProjectRoot())
    assert.is_true(lhandler:getProjectBuilder():getClass() == LuaRocksBuilder)
    assert.is_true(lhandler:getTestFramework():getClass() == Busted)
  end)

  it("getProjectRoot", function()
    assert.same(fs.path_sep, luarocks_root:sub(-1, -1)) -- import! /
    assert.same(luarocks_root:sub(1, -2), fs.find_root({ 'spec' }, luarocks_root))

    local ll = LuaLang:new():noCache():resolve(luarocks_root)
    assert.same(luarocks_root, ll:getProjectRoot())

    assert.is_not_nil(ll:getProjectBuilder())
    assert.is_true(ll:getProjectBuilder():getClass() == LuaRocksBuilder)

    -- NVim.logger_setup()
    assert.is_not_nil(ll:getTestFramework())
    assert.is_true(ll:getTestFramework():getClass() == Busted)
    local busted = ll:getTestFramework()
    assert.is_table(busted:getConfig())
    -- print("[DEBUG] busted:", inspect(busted))
  end)

  -- one reused instance of LuaLang
  local ll = LuaLang:new():noCache():resolve(src_init_lua)
  ---@cast ll env.langs.lua.LuaLang

  it("validate settings", function()
    assert.same(luarocks_root, ll:getProjectRoot())
    assert.same('src/', ll.src)
    assert.same('spec/', ll.test)
    assert.is_true(ll:getProjectBuilder():getClass() == LuaRocksBuilder)
    assert.is_true(ll:getTestFramework():getClass() == Busted)
  end)

  it("isSrcPath", function()
    assert.same(true, ll:isSrcPath(src_init_lua))
    assert.same(true, ll:isTestPath(test_init_lua))

    assert.same(false, ll:isTestPath(src_init_lua))
    assert.same(false, ll:isSrcPath(test_init_lua))
  end)

  it("ClassResolver lookup convert path to class", function()
    local cr = ClassResolver:new({ lang = ll, type = ClassInfo.CT.CURRENT })
    local ci = cr:lookup(src_init_lua, nil) -- by path
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    assert.same("src/mypkg/init", ci:getInnerPath())
    assert.same('Source', ci:getTypeName())

    local src_classname = ci:getClassName()
    assert.same('mypkg.init', src_classname)

    assert.same('mypkg.init_spec', ll:getTestClassForSrcClass(src_classname))
    assert.same('mypkg.init', ll:getSrcClassForTestClass('mypkg.init_spec'))

    assert.same('mypkg.init', ll:getClassNameForInnerPath('src/mypkg/init'))
    assert.same('mypkg.init_spec', ll:getClassNameForInnerPath('spec/mypkg/init_spec'))
  end)

  it("ClassInfo lookup by test classname", function()
    local cr = ClassResolver:new({ lang = ll, type = ClassInfo.CT.CURRENT })
    local ci = cr:lookup(nil, 'mypkg.init_spec') -- by class
    assert.is_not_nil(ci) ---@cast ci env.lang.ClassInfo

    assert.same('spec/mypkg/init_spec', ci:getInnerPath())
    assert.same('Test', ci:getTypeName())

    local src_classname = ci:getClassName()
    assert.same('mypkg.init_spec', src_classname)

    assert.same('mypkg.init', ll:getSrcClassForTestClass(ci:getClassName()))
    assert.same('mypkg.init_spec', ll:getTestClassForSrcClass('mypkg.init'))

    assert.same('mypkg.init', ll:getClassNameForInnerPath('src/mypkg/init'))
    assert.same('mypkg.init_spec', ll:getClassNameForInnerPath('spec/mypkg/init_spec'))
  end)

  it("ClassResolver opposite src->test", function()
    local classResolver = ll:resolveClass(src_init_lua, ClassInfo.CT.OPPOSITE)
    local classinfo = classResolver:getClassInfo()
    local exp = {
      pair = {
        pair = 'self',
        classname = 'mypkg.init',
        inner_path = 'src/mypkg/init',
        path = luarocks_root .. 'src/mypkg/init.lua',
        type = ClassInfo.CT.SOURCE,
        exists = true,
      },
      classname = 'mypkg.init_spec',
      inner_path = 'spec/mypkg/init_spec',
      path = luarocks_root .. 'spec/mypkg/init_spec.lua',
      exists = true,
      testsuite_path = 'spec/',
      testsuite_name = 'ROOT',
      type = ClassInfo.CT.TEST,
    }
    assert.is_not_nil(classinfo)
    ---@cast classinfo env.lang.ClassInfo
    assert.same(exp, classinfo:toArray())
  end)

  it("openTestOrSource", function()
    if 0 == 0 then return end
    local lines = {
      '--',                    --  1
      "local function pow(x)", --  2
      "  return x * x",        --  3
      "end",                   --  4
      ""                       --  5
    }
    local bufnr, lnum, col = 8, 2, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    -- self:getEditor():resolveWords() -- :pickMethodNameTo(classinfo)
    -- TODO
    -- assert.same(test_init_lua, ll:openTestOrSource(src_init_lua))
  end)

  it("match parent package", function()
    local line = 'cmd4lua.help.collector'
    local function prev(l)
      return l:match("(.*)[%.]") or l
    end
    assert.same('cmd4lua.help', prev(line))
    assert.same('cmd4lua', prev(prev(line)))
    assert.same('cmd4lua', prev(prev(prev(line))))
  end)

  it("_findInnerPathForModule", function()
    local rockspec_config = {
      build = {
        modules = {
          cmd4lua                = "src/cmd4lua/cmd4lua.lua",
          ["cmd4lua.vim"]        = "src/cmd4lua_vim.lua",
          ["root-ns.util"]       = "src/root-ns/util.lua",
          ['busted.modules.cli'] = 'busted/modules/cli.lua',
        },
      }
    }
    local ll0 = H.mk_lang(rockspec_config, luarocks_root)

    local f = function(l) return ll0.builder:findInnerPathForModule(l) end
    assert.same('src/cmd4lua/cmd4lua', f('cmd4lua'))
    assert.same('src/cmd4lua/help/collector', f('cmd4lua.help.collector'))
    assert.same('src/cmd4lua/util', f('cmd4lua.util'))
    assert.same('src/root-ns/tools', f('root-ns.tools'))
    assert.same('busted/modules/cli', f('busted.modules.cli'))
    assert.same('busted/modules', f('busted.modules'))
    assert.same('busted/modules/abc', f('busted.modules.abc'))
    assert.same('busted/jkl', f('busted.jkl'))
  end)


  it("updateLspSettings", function()
    local lsp_opts = {}
    local rockspec_config = {
      dependencies = {
        'cmd4lua >= 0.4.3',
      }
    }
    local ll0 = H.mk_lang(rockspec_config)
    assert.is_nil(ll0.last_lsp_update)

    ll0:updateLspSettings(lsp_opts)
    assert.is_number(ll0.last_lsp_update)

    assert.is_not_nil(lsp_opts.config)
    assert.is_not_nil(lsp_opts.config.settings)
    assert.is_not_nil(lsp_opts.config.settings.Lua)
    assert.is_not_nil(lsp_opts.config.settings.Lua.workspace)
    assert.is_not_nil(lsp_opts.config.settings.Lua.workspace.library)
    assert.is_false(lsp_opts.config.settings.Lua.telemetry.enable)
    local wslibs = lsp_opts.config.settings.Lua.workspace.library
    local first_key = next(wslibs)
    -- first key "/usr/local/share/lua/5.1/cmd4lua/base.lua"
    assert.match('cmd4lua.*%.lua', first_key)
    assert.is_true(wslibs[first_key])
  end)

  -- goal: update only if either the values have not been set at all yet, or
  -- if the rockspec-config file has been changed
  it("updateLspSettings update by rockspec last-modified-time ", function()
    local lsp_opts = {}
    local rockspec_config = { dependencies = { 'cmd4lua >= 0.4.3', } }
    local ll0 = H.mk_lang(rockspec_config)

    assert.is_nil(ll0.last_lsp_update)
    ll0.last_lsp_update = nil
    --@cast ll0.builder LuaRocksBuilder
    ll0.builder.buildscript_lm = os.time() - 10

    local backup = ll0.builder.getWorkspaceLibs
    local called = false
    ---@diagnostic disable-next-line: duplicate-set-field
    ll0.builder.getWorkspaceLibs = function()
      called = true
      return {}
    end
    ll0:updateLspSettings(lsp_opts)
    assert.same(true, called)

    assert.is_number(ll0.last_lsp_update)
    assert.is_true(ll0.last_lsp_update > ll0.builder.buildscript_lm)

    called = false
    ll0:updateLspSettings(lsp_opts)
    assert.same(false, called)

    -- touch rockspec config
    ll0.builder.buildscript_lm = ll0.last_lsp_update + 8
    ll0:updateLspSettings(lsp_opts)
    assert.same(true, called)

    called = false
    ll0.last_lsp_update = nil
    ll0:updateLspSettings(lsp_opts)
    assert.same(true, called)

    finally(function() ll0.builder.getWorkspaceLibs = backup end)
  end)

  --[[  works
  it("last-modified-time", function()
    local fn = fs.tmpfilename('check-time')
    local now = os.time()
    assert.is_true(fs.str2file(fn,'test', 'w'))
    local stat = uv.fs_stat(fn)
    assert.is_not_nil(stat)

    -- print(now, stat.mtime.sec) -- on linux its sec from epoch
    assert.is_true(stat.mtime.sec - now < 8) -- approximately equal
    os.remove(fn)
  end)
  ]]
end)


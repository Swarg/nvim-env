-- 15-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os() -- init fs before all other modules
local sys = nvim:get_os()

-- local log = require('alogger')
local tables = require 'env.util.tables'
local base = require("env.lang.utils.base")
local lapi_consts = require 'env.lang.api.constants'

local ClassInfo = require("env.lang.ClassInfo")
local ClassResolver = require("env.lang.ClassResolver")
-- local LuaGen = require("env.langs.lua.LuaGen");
-- local LuaLang = require("env.langs.lua.LuaLang");

local LEXEME_TYPE = lapi_consts.LEXEME_TYPE
local CT = ClassInfo.CT
local H = require("env.langs.lua.ahelper")
local rnf = H.root_and_file
local mkClassInfoAndLuaGen = H.mkClassInfoAndLuaGen
local mkLuaLang = H.mkLuaLang



describe("env.langs.lua.LuaGen", function()
  after_each(function()
    NVim.D.disable()
    NVim.logger_off()
    nvim:clear_all_state_with_os()
  end)

  -- check tooling
  it("ClassResolver + lang tooling", function()
    local lang = mkLuaLang(H.luarocks_root)

    local cr = ClassResolver.of(lang)
    local sci = cr:lookupByClassName(H.mod_core)
    assert.is_not_nil(sci)

    -- #2 same without of-factory:
    local tcur = CT.CURRENT
    cr = ClassResolver:new({ lang = lang, classname = H.mod_core, type = tcur })
    sci = cr:lookupByClassName(H.mod_core)
    assert.is_not_nil(sci)

    -- #3
    cr = H.mk_ClassResolver({ lang = lang, classname = H.mod_core_spec })
    sci = cr:lookupByClassName(H.mod_core)
    assert.is_not_nil(sci)

    -- #4
    cr = H.mk_ClassResolver({ lang = lang, classname = H.mod_core_spec })
    sci = cr:lookup(nil, H.mod_core) -- by classname
    assert.is_not_nil(sci)

    -- #4
    cr = lang:getClassResolver(CT.CURRENT, nil, H.mod_core_spec)
    sci = cr:lookup(nil, H.mod_core) -- by classname
    assert.is_not_nil(sci)
  end)

  --
  it("gen-test-file work under-the-hood", function()
    local lang = mkLuaLang(H.luarocks_root)
    -- resolve by classname
    local cr = lang:getClassResolver(CT.CURRENT, nil, H.mod_core):run()
    local sci = cr:getClassInfo(false)
    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo

    assert.same(H.path_core, sci:getPath())
    local tci = cr:getOppositeClass(sci)
    assert.is_not_nil(tci) ---@cast tci env.lang.ClassInfo

    assert.same(H.path_core_spec, tci:getPath())
    assert.same(H.mod_core_spec, tci:getClassName())

    local opts = {}
    opts.dry_run, opts.no_date, opts.author = true, true, 'A'

    local lines = {
      "--",                 -- 0
      "local M = {}",       -- 1
      "",                   -- 2
      "function M.echo(s)", -- 3
      "  return s",
      "end",
      "",
      "return M"
    }
    local lnum, col = 4, 14
    nvim:new_buf(lines, lnum, col)
    assert.is_not_nil(vim.cmd)

    local c = lang:getEditor():getContext(true):resolveWords()
    assert.same('echo', c.word_element)
    assert.same(LEXEME_TYPE.FUNCTION, c.lexeme_type)

    local path = lang:getLangGen():createTestFile(tci, opts) -- dry-run
    -- assert.same(1, nvim:get_executed_vim_cmd())
    assert.same(H.path_core_spec, path)
    local exp = [[
-- @author A
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'mypkg.core'

describe("mypkg.core", function()

  it("echo", function()
    local res = M.echo()
    local exp = 1
    assert.same(exp, res)
  end)

end)
]]
    assert.same(exp, opts.class_body)
  end)

  --
  --
  --  try to find in currline (method name) -- no
  --  ask from user vim. ui_input -- no
  --
  it("createNewTestCaseFile fail no test-case name", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true) -- emulate existed file
    assert.is_false(nvim:get_next_ui_input())

    local lang = mkLuaLang(pr)
    local cr = lang:getClassResolver(CT.CURRENT, nil, 'app.module'):run()
    local sci = cr:getClassInfo(false)
    -- local sci, gen = mkClassInfoAndLuaGen('app.module', pr)
    local exp = { -- just check generated sci
      classname = 'app.module',
      inner_path = 'src/app/module',
      path = '/home/user/proj/src/app/module.lua',
      type = ClassInfo.CT.SOURCE,
      exists = true,
    }
    assert.same(exp, sci) ---@cast sci env.lang.ClassInfo
    assert.same(true, sci:isSourceClass())

    local opts = { dry_run = true }

    local ret = lang:getLangGen():createNewTestCaseFile(opts) -- payload
    assert.is_false(ret)

    assert.is_table(opts.tci)

    local sci_copy = tables.deep_copy(sci)
    sci_copy.pair = opts.tci

    assert.same(sci_copy, opts.tci.pair)

    -- notice here tci.path is updated to testcasedir -- from simple mapping
    assert.same(pr .. 'spec/app/module/', opts.tci.path)
    assert.same('app.module.', opts.tci.classname)

    -- not generated:
    assert.is_nil(opts.class_file)
    assert.is_nil(opts.class_body)
    -- why: cancel: no test-case name
    assert.is_nil(opts.name)
    assert.is_nil(opts.tcpath)
  end)

  --
  -- dry_run success tc name from input
  --
  it("createNewTestCaseFile tc name from input", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true) -- true to "mk file exists"
    nvim:add_ui_input('tc-name-from-input')

    local lang, sci = mkLuaLang(pr), nil
    sci = lang:getClassResolver(nil, nil, 'app.module'):run():getClassInfo(false)
    assert.is_not_nil(sci) ---@cast sci env.lang.ClassInfo
    assert.same(true, sci:isSourceClass())

    local opts = { dry_run = true, no_date = true, author = 'A' }

    local path = lang:getLangGen():createNewTestCaseFile(opts) -- workload

    local exp = pr .. 'spec/app/module/tc-name-from-input_spec.lua'
    assert.same(exp, path)
    assert.same(exp, opts.tci.path)
    assert.same(exp, opts.class_file) -- dry mode without saveing to file
    local expb = [[
-- @author A
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'app.module'

describe("app.module", function()

  it("success", function()
    assert.same(1, 0)
  end)

end)
]]
    assert.same(expb, opts.class_body)
  end)

  --
  --
  -- check Stub tooling
  it("check stub.os.FileSystem", function()
    local fn = '/home/user/proj/src/app/module.lua'
    sys:set_file(fn, { '--', 'second' })

    assert.same('file', sys:file_type(fn))
    assert.same({ '--', 'second' }, sys:get_file_lines(fn))

    assert.same('emulated luv module', sys:whatis(package.loaded['luv']))
    local code = base.saveFile(fn, "a new lines for file\nsecond line")
    assert.same('ok', base.save_file_codes[code])
    local exp = { 'a new lines for file', 'second line' }
    assert.same(exp, sys:get_file_lines(fn))
  end)

  --
  -- generate to file tc name from input
  --
  it("createNewTestCaseFile tc name from input", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    assert.same('file', sys:file_type(fn))  -- ensure "existed" in "fs"

    nvim:add_ui_input('tc-name-from-input') -- < testcase filename

    -- nvim.D.enable() log.fast_setup() -- log.fast_log_to_mem(1)

    local opts = { no_date = true, author = 'A' }

    local gen = mkLuaLang(pr):getLangGen()
    local path = gen:createNewTestCaseFile(opts) -- workload

    local exp1 = 'ok: /home/user/proj/spec/app/module/tc-name-from-input_spec.lua'
    assert.same(exp1, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/tc-name-from-input_spec.lua'
    --                          ^^^^^^        ^new generated testfile
    --                        TestCase dir
    assert.same(exp, opts.tci.path)
    assert.same(exp, path)
    -- assert.same(1, sys:tree())
    assert.same('file', sys:file_type(exp))
    assert.match("require.*app.module", sys:get_file_content(exp) or '')
  end)

  --
  -- opts.name == "tc-name" is a dir with existed files e.g. name is busy
  -- opts.name refs to already existed directory with test files
  -- ask new name via vim.ui.input from user
  -- - user give unique not existed filename
  --
  it("createNewTestCaseFile gen tc file take new name via ui", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    local tcdir = pr .. 'spec/app/module/tc-name'

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    sys:set_dir(tcdir, { 'success_spec.lua', 'fail_spec.lua' })
    -- user input
    nvim:add_ui_input('new-tc-name') -- ok

    assert.same('directory', sys:file_type(tcdir))
    assert.same('file', sys:file_type(fn))

    local opts = { name = 'tc-name', no_date = true, author = 'A' }

    local gen = mkLuaLang(pr):getLangGen()
    local path = gen:createNewTestCaseFile(opts) -- workload
    -- assert.same(1, nvim:get_ui_input_opts(0))

    local ex = 'ok: /home/user/proj/spec/app/module/new-tc-name_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/new-tc-name_spec.lua'
    assert.same(exp, opts.tci.path)
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp)) -- ensure generated
  end)


  it("createNewTestCaseFile gen tc file take new name via ui", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    local tcdir = pr .. 'spec/app/module/tc-name'

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    sys:set_dir(tcdir, { 'success_spec.lua', 'fail_spec.lua' })

    -- user input
    nvim:add_ui_input('tc-name/success')       -- bad - existed!
    nvim:add_ui_input('tc-name/fail')          -- bad - existed!
    nvim:add_ui_input('tc-name/fail_spec.lua') -- bad - existed!
    nvim:add_ui_input('tc-name/new')           -- ok

    assert.same('directory', sys:file_type(tcdir))
    assert.same('file', sys:file_type(fn))

    local opts = { name = 'tc-name', no_date = true, author = 'A' }

    local gen = mkLuaLang(pr):getLangGen()
    local path = gen:createNewTestCaseFile(opts) -- workload

    local ex = 'ok: /home/user/proj/spec/app/module/tc-name/new_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/tc-name/new_spec.lua'
    assert.same(exp, opts.tci.path)         -- tcpath from specified name
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp)) -- ensure was created
  end)

  --
  -- name asked from user with multiples tryes
  -- generate test-case with subdirs
  --
  it("createNewTestCaseFile gen tc file", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    local tcdir = pr .. 'spec/app/module/tc-name'

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    sys:set_dir(tcdir, { 'case-a/', 'case-b/' })

    -- user input
    nvim:add_ui_input('tc-name')                -- bad
    nvim:add_ui_input('tc-name/case-a')         -- bad
    nvim:add_ui_input('tc-name/case-b')         -- bad
    nvim:add_ui_input('tc-name/case-c/success') -- ok

    assert.same('directory', sys:file_type(tcdir))
    assert.same('file', sys:file_type(fn))

    -- nvim.D.enable() log.fast_setup(nil, 0) -- log.fast_log_to_mem(1)

    local _, gen = mkClassInfoAndLuaGen('app.module', pr)
    local opts = { no_date = true, author = 'A' }

    local path = gen:createNewTestCaseFile(opts) -- payload

    local ex = 'ok: /home/user/proj/spec/app/module/tc-name/case-c/success_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/tc-name/case-c/success_spec.lua'
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp))
  end)

  --
  it("createNewTestCaseFile gen tc file", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    local tcdir = pr .. 'spec/app/module/tc-name'

    sys:set_dir(tcdir, { 'case-a/', 'case-b/' })
    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)

    -- user input
    nvim:add_ui_input('tc-name_spec.lua')        -- bad
    nvim:add_ui_input('tc-name')                 -- bad
    nvim:add_ui_input('tc-name/case-a_spec.lua') -- bad has dir case-a
    nvim:add_ui_input('tc-name/case-b')          -- bad
    nvim:add_ui_input('tc-name/case-b_spec.lua') -- bad
    nvim:add_ui_input('tc-name/case-c/success')  -- ok

    assert.same('directory', sys:file_type(tcdir))
    assert.same('file', sys:file_type(fn))

    -- nvim.D.enable() log.fast_setup(nil, 0) -- log.fast_log_to_mem(1)

    local _, gen = mkClassInfoAndLuaGen('app.module', pr)
    local opts = { no_date = true, author = 'A' }

    local path = gen:createNewTestCaseFile(opts) -- payload

    local ex = 'ok: /home/user/proj/spec/app/module/tc-name/case-c/success_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/tc-name/case-c/success_spec.lua'
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp))
  end)

  --
  it("createNewTestCaseFile gen tc file", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    local tcdir = pr .. 'spec/app/module/tc-name'

    sys:set_dir(tcdir, { 'case-a/', 'case-b/' })
    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)

    -- user input
    nvim:add_ui_input('tc-name_spec.lua')        -- bad
    nvim:add_ui_input('tc-name')                 -- bad
    nvim:add_ui_input('tc-name/case-a_spec.lua') -- bad has dir case-a
    nvim:add_ui_input('tc-name/case-b')          -- bad
    nvim:add_ui_input('tc-name/case-b_spec.lua') -- bad
    nvim:add_ui_input('tc-name/case-c_spec.lua') -- ok

    assert.same('directory', sys:file_type(tcdir))
    assert.same('file', sys:file_type(fn))

    -- nvim.D.enable() log.fast_setup(nil, 0) -- log.fast_log_to_mem(1)

    local _, gen = mkClassInfoAndLuaGen('app.module', pr)
    local opts = { no_date = true, author = 'A' }

    local path = gen:createNewTestCaseFile(opts) -- payload

    local ex = 'ok: /home/user/proj/spec/app/module/tc-name/case-c_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    -- logicaly is ok because here is has no the "case-c" sub-directory but
    local exp = pr .. 'spec/app/module/tc-name/case-c_spec.lua'
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp))
  end)

  --
  --
  -- name with dirs /
  --
  it("createNewTestCaseFile gen tc file", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')

    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)

    assert.same('file', sys:file_type(fn))

    -- nvim.D.enable() log.fast_setup(nil, 0) -- log.fast_log_to_mem(1)

    local _, gen = mkClassInfoAndLuaGen('app.module', pr)
    local opts = { name = 'tc/name/sub', no_date = true, author = 'A' }

    local path = gen:createNewTestCaseFile(opts) -- payload

    local ex = 'ok: /home/user/proj/spec/app/module/tc/name/sub_spec.lua'
    assert.same(ex, gen:getReadableSaveReport())

    local exp = pr .. 'spec/app/module/tc/name/sub_spec.lua'
    assert.same(exp, opts.tci.path)
    assert.same(exp, path)
    assert.same('file', sys:file_type(exp))
  end)


  it("createClassBody", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/module.lua')
    assert.same(nil, sys:file_type(fn))

    local ci, gen = mkClassInfoAndLuaGen('app.module', pr)
    local exp_ci = {
      classname = 'app.module',
      inner_path = 'src/app/module',
      path = '/home/user/proj/src/app/module.lua',
      type = ClassInfo.CT.SOURCE
    }
    assert.same(exp_ci, ci)
    local opts = { no_date = true, extends = 'oop.Object' }
    -- nvim.D.enable() log.fast_setup(nil, 0) -- log.fast_log_to_mem(1)

    local exp = [[
--   @author AuthorName
--

local class = require 'oop.class'

class.package 'app'

---@class app.module : oop.Object
local module = class.new_class(nil, 'module', {
})

-- constructor used in module:new()
function module:_init()
end


class.build(module)
return module
]]
    assert.same(exp, gen:createClassBody(ci, opts))
  end)
end)

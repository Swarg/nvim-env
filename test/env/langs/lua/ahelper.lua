-- 15-11-2023 @author Swarg
--
-- to pickup by lsp and busted  add into  ".busted:-file this lpath:
--
-- default = { ..., lpath = "lua/?.lua;lua/env/?.lua;test/?.lua;", ...}
--                                                   ^^^^^^^^^^^

local LuaLang = require("env.langs.lua.LuaLang")
local LuaRocksBuilder = require("env.langs.lua.LuaRocksBuilder")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local M = {}

--------------------------------------------------------------------------------
--                   Helper implementation for specific lang
--------------------------------------------------------------------------------

--
local H = require("env.lang.oop.stub.ATestHelper")
H.inherit(M)

--
-- factory to create ClassInfo LangGen Lang
--
---@param classname string
---@param project_root string?
--
---@return env.lang.ClassInfo
---@return env.langs.lua.LuaGen
---@return env.langs.lua.LuaLang
function M.mkClassInfoAndLuaGen (classname, project_root)
  local ci, gen, lang = H.mkClassInfoAndLangGen(LuaLang, classname, project_root)
  ---@cast ci env.lang.ClassInfo
  ---@cast gen env.langs.lua.LuaGen
  ---@cast lang env.langs.lua.LuaLang
  return ci, gen, lang
end

function M.mkLuaLang(project_root)
  local lang, pr = H.mkLang(LuaLang, project_root)
  ---@cast lang env.langs.lua.LuaLang
  return lang, pr
end

--------------------------------------------------------------------------------

--
function M.say(s)
  return s
end


M.luarocks_root = uv.cwd() .. '/test/env/resources/lua/luarocks/'
M.mypkg_rockspec = 'mypkg-scm-1.rockspec'

M.mod_core = "mypkg.core"
M.path_core = M.luarocks_root .. "src/mypkg/core.lua"
M.mod_core_spec = "mypkg.core_spec"
M.path_core_spec = M.luarocks_root .. "spec/mypkg/core_spec.lua" -- not exits

---@param settings table?
---@return env.langs.lua.LuaRocksBuilder
---@param project_root string?
---@param rockspec string?
function M.mk_builder(settings, project_root, rockspec)
  return LuaRocksBuilder:new({
    project_root = project_root or M.luarocks_root,
    buildscript = rockspec or M.mypkg_rockspec,
    src = 'src/',
    test = 'spec/',
    settings = settings,
  })
end

---@param settings table? - rockspec config-file (buildscript) for LuaRocksBuilder
---@param project_root string?
---@param rockspec string?
---@return env.langs.lua.LuaLang
function M.mk_lang(settings, project_root, rockspec)
  local ll = LuaLang:new({
    project_root = project_root or M.luarocks_root,

    builder = LuaRocksBuilder:new({
      project_root = project_root or M.luarocks_root,
      buildscript = rockspec or M.mypkg_rockspec,
      settings = settings,
    }),
  })
  return ll
end

return M

-- 06-08-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local LuaTestDiagnost = require 'env.langs.lua.LuaTestDiagnost'
local M = LuaTestDiagnost

local fs = require('env.files')

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") -- vim.loop

local lua_root = uv.cwd() .. '/test/env/resources/lua/'
local busted_output1 = lua_root .. 'busted/output01.lua'

local toutput01 = {}
if fs.file_exists(busted_output1) then
  local text = fs.read_all_bytes_from(busted_output1) or 'return false'
  if text then
    local func, ok = loadstring(text), false
    ---@diagnostic disable-next-line: param-type-mismatch
    ok, toutput01 = pcall(func, '') --loadstring(text)()
    if not ok then
      print('Cannot parse lua file ' .. busted_output1)
    end
  end
else
  print('Not Found File ' .. busted_output1)
end


describe("env.langs.lua.LuaTestDiagnost", function()
  it("new instance", function()
    -- given
    local lang = { project_root = '/home/dev/' } ---@cast lang env.lang.Lang
    local obj = LuaTestDiagnost:new(nil, lang, 'BUSTED')

    assert.is_table(obj)
    assert.same(2, obj.namespace_id)
    assert.same('NVIM_ENV_BUSTED', obj.namespace)
    assert.same(lang, obj.lang)
  end)

  it("aboutResultBriefly", function()
    local e = '1 successes / 1 failures / 1 errors / 0 pending : 0.018939 seconds'
    assert.same({ e }, M.aboutResultBriefly(toutput01))
  end)

  -- before output formated
  it("sort_decoded", function()
    local e1, e2, e3, e4
    e1 = { element = { trace = { currentline = 5 } }, trace = { currentline = 6 } }
    e2 = { element = { trace = { currentline = 2 } }, trace = { currentline = 3 } }
    e3 = { element = { trace = { currentline = 9 } }, trace = { currentline = 10 } }
    e4 = { element = { trace = { currentline = 1 } }, trace = { currentline = 1 } }
    local exp = { failures = { e4, e2, e1, e3 } }
    local decoded = { failures = { e1, e2, e3, e4 } }
    assert.same(exp, M.sort_decoded(decoded))
  end)

  -- for comparison only - not used
  it("sortDiagnostics", function()
    local d1 = { lnum = 36, message = "Failure:...36", }
    local d2 = { lnum = 21, message = "Failure:...21", }
    local d3 = { lnum = 1, message = "Failure:...1", }
    local d4 = { lnum = 45, message = "Failure:...45", }
    local diagnostics = { [1] = { d1, d2, d3, d4 } }
    assert.same({ [1] = { d3, d2, d1, d4 } }, M.sortDiagnostics(diagnostics))
  end)

  it("format_decoded", function()
    local r = M.format_decoded(toutput01)
    local exps = {
      "Error -> ./test/env/lang/oop/ClassGen_spec.lua @ 22",
      "env.lang.oop.ClassGen genClassBody",
      "./lua/env/lang/oop/ComponentGen.lua:22: [DEBUG] No ClassInfo!",
      "",
      "stack traceback:",
      "	./lua/env/lang/oop/ComponentGen.lua:22: in function 'canWork'",
      "	./lua/env/lang/oop/MethodGen.lua:158: in function 'toCode'",
      "	./lua/env/lang/oop/ClassGen.lua:140: in function 'genClassBody'",
      "	./test/env/lang/oop/ClassGen_spec.lua:58: in function <./test/env/lang/oop/ClassGen_spec.lua:22>",
      "",
      "",
      'Failure ->  ./test/env/lang/oop/ClassGen_spec.lua @ 18',
      'env.lang.oop.ClassGen stub fail',
      './test/env/lang/oop/ClassGen_spec.lua:19: Expected objects to be the same.',
      "Passed in:",
      "(string) 'abc'",
      "Expected:",
      "(string) 'x'",
      "", -- 8
      "stack traceback:",
      "	./test/env/lang/oop/ClassGen_spec.lua:19: in function <./test/env/lang/oop/ClassGen_spec.lua:18>",
    }
    assert.is_not_nil(r)
    for i, exp in pairs(exps) do
      -- print('[VERBOSE]', i, exp)
      assert.same(exp, r[i])
    end
  end)

  it("buildDiagnosticsFromDecoded", function()
    -- assert.same('x', toutput01)
    local t = toutput01
    local trace = ((t.successes or {})[1] or {}).trace
    assert.is_not_nil(trace)
    assert.same(14, trace.currentline)
    assert.same('./test/env/lang/oop/ClassGen_spec.lua', trace.short_src)
    --
    local bufnr = 16
    local exp = {
      [bufnr] = {
        { -- 1
          col = M.IN_ROW_COL_START,
          end_col = M.IN_ROW_COL_END,
          end_lnum = 13,
          lnum = 13,
          message = "Success: env.lang.oop.ClassGen stub ok",
          severity = 1
        },
        { -- 2
          col = M.IN_ROW_COL_START,
          end_col = M.IN_ROW_COL_END,
          end_lnum = 18,
          lnum = 18,
          message =
              "Failure:\n" ..
              "./test/env/lang/oop/ClassGen_spec.lua:19: Expected objects to be the same.\n" ..
              "Passed in:\n" ..
              "(string) 'abc'\n" ..
              "Expected:\n" ..
              "(string) 'x'\n\n" ..
              "stack traceback:\n" ..
              "\t./test/env/lang/oop/ClassGen_spec.lua:19: in function <./test/env/lang/oop/ClassGen_spec.lua:18>\n\n" ..
              "env.lang.oop.ClassGen stub fail\n",
          severity = 3
        },
        { -- 3
          col = M.IN_ROW_COL_START,
          end_col = M.IN_ROW_COL_END,
          end_lnum = 58 - 1, -- to nvim
          lnum = 58 - 1,
          message =
              "Error: \n" ..
              "./lua/env/lang/oop/ComponentGen.lua:22: [DEBUG] No ClassInfo!\n\n" ..
              "stack traceback:\n" ..
              "\t./lua/env/lang/oop/ComponentGen.lua:22: in function 'canWork'\n" ..
              "\t./lua/env/lang/oop/MethodGen.lua:158: in function 'toCode'\n" ..
              "\t./lua/env/lang/oop/ClassGen.lua:140: in function 'genClassBody'\n" ..
              "\t./test/env/lang/oop/ClassGen_spec.lua:58: in function <./test/env/lang/oop/ClassGen_spec.lua:22>\n",
          severity = 4
        }
      }
    }
    local res = M.buildDiagnosticsFromDecoded(toutput01, bufnr)
    -- print("[DEBUG] res:", inspect(res))
    assert.is_not_nil(res[bufnr])
    assert.is_not_nil(res[bufnr][1])
    assert.is_not_nil(res[bufnr][2])
    assert.is_not_nil(res[bufnr][3])

    assert.same(exp[bufnr][1], res[bufnr][1])
    assert.same(exp[bufnr][2], res[bufnr][2])
    assert.same(exp[bufnr][3], res[bufnr][3])
    assert.same(exp, res) ----
  end)

  -- newlines - not skip empty lines in messages
  it("format_decoded_element", function()
    local el = {
      element = {
        attributes = {},
        descriptor = "it",
        name = "genClassBody",
        trace = {
          currentline = 22,
          message = "genClassBody",
          short_src = "./test/A_spec.lua",
          source = "@./test/A_spec.lua",
          traceback = "\nstack traceback:\n\n1\n2",
        }
      },
      message = "./test/A_spec.lua:19: Expected.\nPassed in:\n(string) 'a\nb\n\nc'\nExpected:\n(string) 'x'",
      name = "name a\nb",
      trace = {
        currentline = 22,
        message = "msg-trace",
        short_src = "./lua/A.lua",
        source = "@./lua/A.lua",
        traceback = "\nstack traceback:\n\nB",
      }
    }
    local exp = {
      ' ./test/A_spec.lua @ 22',
      'name a\\nb', -- keep without newlines
      './test/A_spec.lua:19: Expected.',
      'Passed in:',
      '(string) \'a',
      'b',
      '', -- empty line
      'c\'',
      'Expected:',
      '(string) \'x\'',
      '',
      'stack traceback:',
      '',
      'B'
    }
    assert.same(exp, M.format_decoded_element('', el, {}))
  end)

  local decoded_json_error_entry_01 =
  {
    element = {
      name = "cmdNewClass",
      trace = {
        currentline = 205,
        message = "cmdNewClass",
        short_src = "./test/A_spec.lua",
        source = "@./test/A_spec.lua", -- << test-file
        traceback =
            "\nstack traceback:\n" ..
            "\t./test/A_spec.lua:205: in function <./test/A_spec.lua:202>\n",
      }
    },
    isError = true,
    message = "./lua/A.lua:97: message_in_error",
    name = "A NewStuff cmdNewClass",
    trace = {
      currentline = 97,          -- line from code not from test file!
      message = "./lua/A.lua:97: message_in_error",
      short_src = "./lua/A.lua", -- << source code
      source = "@./lua/A.lua",
      traceback =
          "\nstack traceback:\n" ..
          "\t./lua/A.lua:97: in function 'cmdNewClass'\n" ..
          "\t./test/A_spec.lua:220: in function <./test/A_spec.lua:205>\n",
    }
  }

  it("buildDiagnosticsFromDecoded", function()
    local decoded_json = {
      successes = {},
      failures = {},
      errors = { decoded_json_error_entry_01 }
    }
    local bufnr = 16
    local d = {
      col = M.IN_ROW_COL_START,
      end_col = M.IN_ROW_COL_END,
      end_lnum = 220 - 1,
      lnum = 220 - 1, -- to nvim
      message =
          "Error: \n./lua/A.lua:97: message_in_error\n" ..
          "\nstack traceback:\n" ..
          "\t./lua/A.lua:97: in function 'cmdNewClass'\n" ..
          "\t./test/A_spec.lua:220: in function <./test/A_spec.lua:205>\n",
      severity = 4
    }
    local dlist = M.buildDiagnosticsFromDecoded(decoded_json, bufnr)
    assert.is_not_nil(dlist)
    local res_derror = dlist[bufnr][1]
    -- print(inspect(res_derror.message))
    assert.is_not_nil(res_derror)
    assert.same(d, res_derror)
  end)

  it("mkDErrorFromDecoded", function()
    local exp = {
      col = 1,
      end_col = 80,
      end_lnum = 220 - 1,
      lnum = 220 - 1, -- to nvim
      message =
          "Error: \n./lua/A.lua:97: message_in_error\n" ..
          "\nstack traceback:\n" ..
          "\t./lua/A.lua:97: in function 'cmdNewClass'\n" ..
          "\t./test/A_spec.lua:220: in function <./test/A_spec.lua:205>\n",
      severity = 4,
      user_data = {
        original_json = decoded_json_error_entry_01,
      }
    }
    local add_original_json = true
    local json = add_original_json or decoded_json_error_entry_01
    -- M.debug = true
    local res = M.mkDErrorFromDecoded(decoded_json_error_entry_01, json)
    -- print("[DEBUG] res:", inspect(res))
    assert.is_not_nil(res)
    assert.same(exp, res)
  end)

  it("mkDErrorFromDecoded no trace", function()
    local decoded_json =
    {
      element = {
        descriptor = "file",
        name = "./test/env/lua_parser_spec.lua",
        trace = {}
      },
      isError = true,
      message = "./test/env/lua_parser_spec.lua:1405: '=' expected near 'skip_to_next'",
      name = "./test/env/lua_parser_spec.lua",
      trace = {}
    }
    local json = false
    local message = decoded_json.message
    local lnum = tonumber(message:match("%a+:([%d]+):%s"))
    assert.same(1405, lnum)
    local res = M.mkDErrorFromDecoded(decoded_json, json)
    local exp = {
      col = 1,
      end_col = 80,
      end_lnum = 1404,
      lnum = 1404, -- from message not from the trace as is usually done
      severity = 4,
      message = "Error: \n" ..
          "./test/env/lua_parser_spec.lua:1405: '=' expected near 'skip_to_next'\nnil"
    }
    assert.same(exp, res)
  end)
end)

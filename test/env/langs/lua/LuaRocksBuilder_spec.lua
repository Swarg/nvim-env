--
require 'busted.runner' ()
local assert = require('luassert')
local fs = require('env.files')
local class = require 'oop.class'
local M = require("env.langs.lua.LuaRocksBuilder")

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop")             -- vim.loop

local H = require("env.langs.lua.ahelper")

local luarocks_root = uv.cwd() .. '/test/env/resources/lua/luarocks/'
local mypkg_rockspec = 'mypkg-scm-1.rockspec'
-- local full_rockspec0 = luarocks_root .. 'mypkg-scm-1.rockspec'


describe("env.langs.lua.LuaRocksBuilder", function()
  it("isProjectRoot", function()
    local userdata = {}
    local exp = fs.join_path(luarocks_root, mypkg_rockspec)
    assert.same(exp, M.isProjectRoot(luarocks_root, userdata))
    assert.same(luarocks_root, userdata.project_root)
    assert.same(mypkg_rockspec, userdata.buildscript)
    assert.same('src/', userdata.src)
    assert.same('spec/', userdata.test)
  end)

  it("getSettings (load rockspe config)", function()
    local userdata = {}
    local exp_buildscript = fs.join_path(luarocks_root, 'mypkg-scm-1.rockspec')
    assert.same(exp_buildscript, M.isProjectRoot(luarocks_root, userdata))
    local exp = {
      src = 'src/',
      test = 'spec/',
      buildscript = mypkg_rockspec,
      project_root = luarocks_root,
    }
    assert.same(exp, userdata)
    local builder = M:new(userdata)
    assert.same('env.langs.lua.LuaRocksBuilder', class.name(builder))
    assert.same(exp_buildscript, builder:getBuildScriptPath())
    local exp_conf = {
      package = 'mypkg',
      version = 'scm-1',
      description = { homepage = 'a project homepage', license = 'MIT' },
      source = { url = 'URL for source tarball' },
      dependencies = {
        'cmd4lua >= 0.4.3',
      },
      build = {
        type = 'builtin',
        modules = { mypkg = 'src/mypkg/init.lua' }
      },
    }
    local t, errmsg = builder:loadBuildScript()
    assert.same(nil, errmsg)
    assert.same(exp_conf, t)
    assert.is_number(builder.buildscript_lm) -- 1700023640

    assert.same("src/mypkg/init.lua", builder:getSettings().build.modules.mypkg)
  end)

  -- it("uv.stat fields", function()
  --   local path = fs.join_path(luarocks_root, 'mypkg-scm-1.rockspec')
  --   local stat = uv.fs_stat(path)
  --   print("[DEBUG] stat:", inspect(stat))
  -- end)

  it("getModulesMap", function()
    local settings = { -- rockspec
      build = {
        modules = {
          cmd4lua = "src/cmd4lua/cmd4lua.lua",
          ["cmd4lua.vim"] = "src/cmd4lua_vim.lua"
        },
      }
    }
    local builder = H.mk_builder(settings)
    assert.same(settings, builder.settings) --, builder:getSettings()
    local exp = {
      cmd4lua = 'src/cmd4lua/cmd4lua.lua',
      ['cmd4lua.vim'] = 'src/cmd4lua_vim.lua',
    }
    assert.same(exp, builder:getModulesMap())
  end)

  it("parse dependency line", function()
    local l = "lua >= 5.1"
    local pkg, ver = l:match("%s*([^%s]+)%s*[<>=]+%s([^%s]+)")
    assert.same("lua", pkg)
    assert.same("5.1", ver)
  end)

  -- by pakage name (rock) name get paths for all modules
  it("getPackageModules", function()
    -- luarocks install cmd4lua   or cd ../cmd4lua/ && luarocks make
    local res = M.getPackageModules('cmd4lua')
    -- print("[DEBUG] res:", inspect(res))
    assert.is_not_nil(res)
    ---@cast res table
    assert.match('/cmd4lua.lua', res['cmd4lua'])
    assert.match('/cmd4lua/parser.lua', res['cmd4lua.parser'])
    assert.match('/cmd4lua/builtin.lua', res['cmd4lua.builtin'])

    -- for cache last-modify time and full path to the installed rockspec
    -- used to check for updates when rockspec opened in nvim are saved
    assert.is_not_nil(res.rockspec)
    assert.match('[%d]+', res.rockspec.mtime)
    -- 1.0.1-1 or scm-1
    assert.match('cmd4lua%-[%w%d%.]+%-%d+%.rockspec', res.rockspec.path)
    -- assert.match('cmd4lua%-%d+%.%d+%.%d+%-%d+.%rockspec', res.rockspec.path)
  end)

  it("resolveDependencies", function()
    local builder = H.mk_builder()
    local res = builder:resolveDependencies():getWorkspaceLibs()
    -- cmd4lua = {
    --   cmd4lua = '/usr/local/share/lua/5.1/cmd4lua.lua',
    --   ...
    -- }
    assert.is_not_nil(res)
    ---@cast res table
    assert.is_not_nil(res['cmd4lua'])
    assert.match('cmd4lua%.lua', res['cmd4lua']['cmd4lua'])
    assert.match('cmd4lua.base%.lua', res['cmd4lua']['cmd4lua.base'])
    assert.match('cmd4lua.parser%.lua', res['cmd4lua']['cmd4lua.parser'])
    assert.is_number(builder.buildscript_lm) -- 1700023640
  end)

  it("isBuildScriptChanged", function()
    local builder = H.mk_builder()
    builder.buildscript_lm = 10 -- last modify time then config was readed

    assert.same(false, builder:isBuildScriptChanged(11))
    assert.same(false, builder:isBuildScriptChanged(10))
    assert.same(true, builder:isBuildScriptChanged(9)) -- before conf readed
  end)
end)

-- 25-11-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new()   -- activate before another modules
local sys = nvim:get_os() -- init emutated OS+FS

local base = require("env.lang.utils.base")
local M = require("env.langs.lua.command.new_stuff");

-- local ClassInfo = require("env.lang.ClassInfo")
-- local Editor = require("env.ui.Editor")
local Context = require("env.ui.Context")

local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

-- local LuaLang = require 'env.langs.lua.LuaLang'
-- local LuaGen = require 'env.langs.lua.LuaGen'

------------------------ impl for specific lang --------------------------------
local H = require("env.langs.lua.ahelper")
local mkClassInfoAndLuaGen = H.mkClassInfoAndLuaGen
local rnf, new_cli = H.root_and_file, H.new_cli
--------------------------------------------------------------------------------



describe("env.langs.lua.command.new_stuff", function()
  after_each(function()
    nvim.D.disable()
    nvim:logger_off()
    nvim:clear_all_state_with_os()
    nvim:restore_original_os()
    sys = nvim:get_os() -- init freash OS
    sys:clear_state(nil, true)
  end)

  -- local test_opts = ' --author A --no-date --dry-run --quiet'

  --
  -- check tooling
  -- show how it works under the hood
  it("resolve words", function()
    local lines = { -- MyClass.lua
      --         10    v   20
      --12345678901234567890123
      'function M.func_name(arg1, arg2)', -- 1
      '-- body',
      'end',
    }
    local lnum, col = 1, 16
    local bufnr = nvim:new_buf(lines, lnum, col) -- emulate buffer with code

    -- check tooling
    assert.same(lines[lnum], nvim.api.nvim_get_current_line())
    local c = Context:new():resolveWords()
    assert.same(bufnr, c.bufnr)

    assert.same(LEXEME_TYPE.FUNCTION, c.lexeme_type)
    assert.same('M', c.word_container)
    assert.same('func_name', c.word_element)

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', '/tmp/proj')

    local ctx = gen:getEditor():getContext(true):resolveWords()
    local LT = LEXEME_TYPE
    local method = ctx:element(LT.METHOD) or ctx:element(LT.FUNCTION)
    assert.same('func_name', method)
  end)


  --
  it("new test-case from input", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    nvim:add_ui_input('name-from-input')

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', pr)
    local cmd_line = '--dry-run'
    local w = new_cli(gen, cmd_line)

    M.cmd_test_case(w)
    local opts = w.vars
    assert.same('name-from-input', opts.name)
  end)

  --
  -- ask testcase name from user
  --
  it("new test-case not found", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:new_buf({ '--' }, 1, 1, nil, fn, true)
    -- user input
    nvim:add_ui_input('name-from-input')

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', pr)
    local cmd_line = '--dry-run --quiet'
    local w = new_cli(gen, cmd_line) ---@type Cmd4Lua

    -- nvim.logger_setup()

    local ret = M.cmd_test_case(w)

    local exp = pr .. 'spec/app/MyClass/name-from-input_spec.lua'
    assert.same(exp, ret) -- not found
    assert.same(false, w:has_input_errors())
    assert.same(exp, w.vars.tci.path)

    -- do not ask value from user
    -- testcase_name == nil  when not found and cannot ask from user
    assert.same('name-from-input', w.vars.name)
    assert.is_table(w.vars)
  end)

  --
  --
  --
  it("new test-case from source line by method name", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    local lines = {
      'function M.func_name(arg1, arg2)', -- 1
      '-- body',
      'end',
    }
    nvim:new_buf(lines, 1, 16, nil, fn, true) -- emulate existed file
    nvim:get_os():set_dir(pr, { 'src/', 'spec/', '.git' })

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', pr)

    local w = new_cli(gen, '--dry-run')
    -- nvim.logger_setup()

    -- :EnvNew test-case
    local res = M.cmd_test_case(w)

    local opts = w.vars
    local exp_tcpath = pr .. 'spec/app/MyClass/func_name_spec.lua'
    assert.same(exp_tcpath, res)
    assert.is_table(opts.tci)
    assert.same(exp_tcpath, opts.tcpath)
    assert.same(exp_tcpath, opts.tcpath)
    -- updated path and class for new testcase
    assert.same(pr .. 'spec/app/MyClass/func_name_spec.lua', opts.tci.path)
    assert.same('app.MyClass.func_name_spec', opts.tci.classname)
  end)

  --
  --
  it("devname popen", function()
    -- emulate answer from prog
    assert.is_not_nil(sys:get_popen('git config user.name'))

    base.clearDevNameCache() -- can use cached name
    assert.same('AuthorName', base.getDeveloperName())
  end)

  --
  --
  it("new test-case given name dry-run", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:new_buf({ '--' }, 1, 16, nil, fn, true)
    nvim:get_os():set_dir(pr, { 'src/', 'spec/', '.git' })

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', pr)

    -- :EnvNew test-case
    local w = new_cli(gen, '--name func_name --dry-run --author A --no-date')
    -- nvim.logger_setup()
    -- nvim.D.enable_module(require'cmd4lua.settings', true)
    local res = M.cmd_test_case(w)

    local opts = w.vars
    local exp_tcpath = pr .. 'spec/app/MyClass/func_name_spec.lua'
    assert.same(exp_tcpath, res)
    assert.is_table(opts.tci)
    assert.same(pr .. 'spec/app/MyClass/func_name_spec.lua', opts.tcpath)
    assert.same(exp_tcpath, opts.tcpath)

    -- updated path and class for new testcase
    assert.same(pr .. 'spec/app/MyClass/func_name_spec.lua', opts.tci.path)
    assert.same('app.MyClass.func_name_spec', opts.tci.classname)
    local exp = [[
-- @author A
require("busted.runner")()
local assert = require 'luassert'
assert:set_parameter('TableFormatLevel', 33)
local M = require 'app.MyClass'

describe("app.MyClass", function()

  it("success", function()
    assert.same(1, 0)
  end)

end)
]]
    -- assert.match('app.MyClass', opts.class_body)
    assert.same(exp, opts.class_body)
    assert.same(exp_tcpath, opts.class_file)
  end)


  it("tooling LangGen.saveFile ", function()
    local pr, fn = rnf('/home/user/proj/', 'MyClass.lua')
    sys:mk(pr)
    assert.same('directory', sys:file_type(pr))
    assert.same(nil, sys:file_type(fn))

    -- require'alogger'.fast_setup(nil, 0) require'dprint'.enable()
    local body = "body\n of\n the file"
    assert.same('emulated io module', sys:whatis(io))
    local code, err = base.saveFile(fn, body)
    assert.same(nil, err)

    assert.same(base.OK_SAVED, code)
    assert.same('file', sys:file_type(fn))
    assert.same("body\n of\n the file\n", sys:get_file_content(fn))
    -- print(sys:tree())
  end)


  -- if 0 == 0 then return end
  it("cmd_class", function()
    local pr, fn = rnf('/home/user/proj/', 'src/app/MyClass.lua')
    nvim:new_buf({ '--' }, 1, 16, nil, fn, true)
    sys:set_dir(pr, { 'src/', 'spec/', '.git' })
    assert.same('file', sys:file_type(fn))

    local _, gen = mkClassInfoAndLuaGen('app.MyClass', pr)

    -- :EnvNew class
    local w = new_cli(gen, 'newClassName --author A --no-date -Q')

    -- require'alogger'.fast_setup(nil, 0) require'dprint'.enable()

    local res, _ = M.cmd_class(w) -- second is opts

    local exp_fn = '/home/user/proj/src/app/newClassName.lua'
    assert.same(exp_fn, res)
    assert.same('file', sys:file_type(exp_fn)) -- exists in enumated fs

    local exp_body = [[
--   @author A
--

local class = require 'oop.class'

class.package 'app'

---@class app.newClassName : oop.Object
local newClassName = class.new_class(nil, 'newClassName', {
})

-- constructor used in newClassName:new()
function newClassName:_init()
end


class.build(newClassName)
return newClassName
]]
    assert.same(exp_body, sys:get_file_content(exp_fn))
  end)
end)

-- 25-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require 'stub.vim.NVim'
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

local M = require 'env.langs.markdown.snippets'


describe("env.langs.markdown.snippets", function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
  end)

  it("handler_emit_html", function()
    local lines = {
      '.',
      'html{body{p"The Text"}}', -- 2 cursor here
      '.',
    }
    local lnum = 2
    local bufnr = nvim:new_buf(lines, lnum, 1)
    -- nvim:set_selection_visual(bufnr, lnum, lnum)

    M.handler_emit_html()

    local exp = {
      '.',
      '<!doctype html>',
      '<html>',
      '  <body>',
      '    <p>The Text</p>',
      '  </body>',
      '</html>',
      '.'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_emit_html", function()
    local lines = {
      '<html>',
      '<body>',
      '   div {', -- 3
      '     p"Text",',
      '     a{href="/", "Home"}',
      '   }', -- 6
      '</body>',
      '</html>.',
    }
    local lnum, lnum_end = 3, 6
    local bufnr = nvim:new_buf(lines, lnum, 1)
    nvim:set_selection_visual(bufnr, lnum, lnum_end)

    M.handler_emit_html()

    local exp = {
      '<html>',
      '<body>',
      '  <div>',
      '    <p>Text</p>',
      '    <a href="/">Home</a>',
      '  </div>',
      '</body>',
      '</html>.'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  --
  -- this will work only for root(first) html-element
  -- div.box-1 --> '<div class="box-1"></div>'
  --
  -- todo for all nested
  it("handler_emit_html shortcut for root elm", function()
    local bufnr = nvim:new_buf({ 'div.box-1' }, 1, 1)

    M.handler_emit_html()

    local exp = { '<div class="box-1"></div>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html shortcut", function()
    local bufnr = nvim:new_buf({ 'div#unique' }, 1, 1)

    M.handler_emit_html()

    local exp = { '<div id="unique"></div>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html shortcut", function()
    local bufnr = nvim:new_buf({ "div.box-1'Text'" }, 1, 1)

    M.handler_emit_html()

    local exp = { '<div class="box-1">Text</div>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html shortcut", function()
    local bufnr = nvim:new_buf({ "div.box-1{p'Text'}" }, 1, 1)

    M.handler_emit_html()

    local exp = { '<div class="box-1">', '  <p>Text</p>', '</div>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html multiply", function()
    local bufnr = nvim:new_buf({ "x3 br''" }, 1, 1)

    M.handler_emit_html()

    local exp = { '<br/><br/><br/>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html multiply", function()
    local bufnr = nvim:new_buf({ "  x3 br''" }, 1, 1)

    M.handler_emit_html()

    local exp = { '  <br/><br/><br/>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html apply_shortcuts", function()
    local bufnr = nvim:new_buf({ "  br" }, 1, 1)
    M.handler_emit_html()
    local exp = { '  <br/>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  it("handler_emit_html multiply with auto add ()", function()
    local bufnr = nvim:new_buf({ "  x3 br" }, 1, 1)
    M.handler_emit_html()
    local exp = { '  <br/><br/><br/>' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_emit_html multiply", function()
    -- local bufnr = nvim:new_buf({ "x3 div.box-1{p'Text-1'}" }, 1, 1)
    local bufnr = nvim:new_buf({ "x3 div.box-1{p'Text-1'}" }, 1, 1)

    M.handler_emit_html()

    local exp = {
      '<div class="box-1">',
      '  <p>Text-1</p>',
      '</div>',
      '<div class="box-1">',
      '  <p>Text-1</p>',
      '</div>',
      '<div class="box-1">',
      '  <p>Text-1</p>',
      '</div>'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)

  -- generate example fro css-rules
  it("handler_gen_html_for_css_classes", function()
    local lines = {
      '', -- 1
      '/* tag:div */',
      '.box-1 {',
      '  background-color: cornflowerblue;',
      '  position: static;',
      '}',
      '.box-2 {',
      '  background-color: lightpink;',
      '  position: relative;',
      '  top: 50px;',
      '  left: 100px;',
      '}',
      '.box-3 {',
      '  background-color: palegreen;',
      '  position: absolute;',
      '  z-index: 100;',
      '  top: 100px;',
      '  left: 250px;',
      '}', -- 19
      '.'
    }
    local lnum, lnum_end = 2, 19
    local bufnr = nvim:new_buf(lines, lnum, 1)
    -- set selection range from lnum 2 to 19 in visualmode
    nvim:set_selection_visual(bufnr, lnum, lnum_end)

    local ok, _ = M.handler_gen_html_for_css_classes()
    assert.same(true, ok)

    local exp = {
      '',
      '/* tag:div */',
      '.box-1 {',
      '  background-color: cornflowerblue;',
      '  position: static;',
      '}',
      '.box-2 {',
      '  background-color: lightpink;',
      '  position: relative;',
      '  top: 50px;',
      '  left: 100px;',
      '}',
      '.box-3 {',
      '  background-color: palegreen;',
      '  position: absolute;',
      '  z-index: 100;',
      '  top: 100px;',
      '  left: 250px;',
      '}',
      '  <div class="box-1">',
      '    background-color: cornflowerblue;',
      '    position: static;',
      '  </div>',
      '',
      '  <div class="box-2">',
      '    background-color: lightpink;',
      '    position: relative;',
      '    top: 50px;',
      '    left: 100px;',
      '  </div>',
      '',
      '  <div class="box-3">',
      '    background-color: palegreen;',
      '    position: absolute;',
      '    z-index: 100;',
      '    top: 100px;',
      '    left: 250px;',
      '  </div>',
      '.'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_gen_multiple_css_classes", function()
    local bufnr = nvim:new_buf({ "css-prop{v1, v2, v3}" }, 1, 1)

    M.handler_gen_multiple_css_classes()

    local exp = {
      '.css-prop-v1 { css-prop: v1; }',
      '.css-prop-v2 { css-prop: v2; }',
      '.css-prop-v3 { css-prop: v3; }'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)


  it("handler_gen_html_for_css_classes", function()
    local lines = {
      '',                               -- 1
      '/* act:use; lns:13; lne:19; */', -- 2
      '.align-items-start {',
      '  align-items: start;',
      '}',
      '.align-items-end {',
      '  align-items: end;',
      '}',
      '.align-items-center {',
      '  align-items: center;',
      '}',                                           -- 11
      '',
      '  <div class="grid-container ${CSS_CLASS}">', -- 13
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '  </div>',
      '  <br>',
      '', -- 19
      '.'
    }
    local lnum, lnum_end = 2, 11
    local bufnr = nvim:new_buf(lines, lnum, 1)
    nvim:set_selection_visual(bufnr, lnum, lnum_end)

    M.handler_gen_html_for_css_classes()

    local exp = {
      '',
      '/* act:use; lns:13; lne:19; */',
      '.align-items-start {',
      '  align-items: start;',
      '}',
      '.align-items-end {',
      '  align-items: end;',
      '}',
      '.align-items-center {',
      '  align-items: center;',
      '}',
      '  <div>align-items: start;</div>',
      '  <div class="grid-container align-items-start">',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '  </div>',
      '  <br>',
      '',
      '  <div>align-items: end;</div>',
      '  <div class="grid-container align-items-end">',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '  </div>',
      '  <br>',
      '',
      '  <div>align-items: center;</div>',
      '  <div class="grid-container align-items-center">',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '  </div>',
      '  <br>',
      '',
      '  <div class="grid-container ${CSS_CLASS}">',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '    <div class="grid-element">Element</div>',
      '  </div>',
      '  <br>',
      '',
      '.'
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1))
  end)
end)

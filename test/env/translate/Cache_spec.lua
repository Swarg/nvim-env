-- 27-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os() -- init fs before all other modules
_G.TEST = true
local log = require 'alogger'
local M = require "env.translate.Cache"
local U = require("env.translate.utils");

local T_WORDS = U.IT.WORDS
local T_PHRASES = U.IT.PHRASES

describe("env.translate.Cache", function()
  after_each(function()
    NVim.D.disable()
    NVim.logger_off()
    nvim:clear_all_state_with_os()
    log.fast_off()
  end)

  ---@return env.translate.ParsedResponse
  local function mk_resp(original, translated)
    return {
      original = original,
      translated = translated
    }
  end


  it("in_mem(persist=false)", function()
    local cache = M:new(nil, '/tmp/cache/', false)

    assert.same(true, cache:put(mk_resp('text', 'текст'), 'en', 'ru', T_WORDS))
    assert.same('текст', cache:find('text', 'en', 'ru', T_WORDS))
  end)


  it("persistent save|load to|from disk)", function()
    local cache_dir = '/tmp/cache'
    local cache = M:new(nil, cache_dir, true)

    assert.same(true, cache:put(mk_resp('text', 'текст'), 'en', 'ru', T_WORDS))
    assert.same('текст', cache:find('text', 'en', 'ru', T_WORDS))

    assert.is_nil(nvim.os:file_type(cache_dir)) -- not exists

    cache:save()                                -- workload
    assert.same('directory', nvim.os:file_type(cache_dir))

    local exp = [[
/tmp/cache
    en/
        ru/
            words/
                1
3 directories, 1 file
]]
    assert.same(exp, nvim.os:tree(cache_dir))
    local exp2 = "text|текст\n"
    assert.same(exp2, nvim.os:get_file_content('/tmp/cache/en/ru/words/1'))

    -- log.fast_setup(false, 0)
    -- emulate restore(load) cache from disk
    local cache2 = M:new(nil, cache_dir, false)
    assert.is_nil(cache2:find('text', 'en', 'ru', T_WORDS))
    assert.same(1, cache:load())
    assert.is_nil(cache2:find('text', 'en', 'ru', T_WORDS))

    local cache3 = M:new(nil, cache_dir, true)
    assert.same('текст', cache3:find('text', 'en', 'ru', T_WORDS))
  end)


  it("load_file_to", function()
    local cache_dir = '/tmp/cache'
    local fn = cache_dir .. '/en/ru/words'
    nvim.os:set_file(fn, { "text|текст", "for|для", "translate|перевода" })
    local t = {}
    assert.same(true, M.load_file_to(t, fn))
    local exp = { text = 'текст', ['for'] = 'для', translate = 'перевода' }
    assert.same(exp, t)
  end)


  it("build_content_of", function()
    local f = M.build_content_of
    local exp = "hellow|привет\nworld|мир\n"
    assert.same(exp, f(U.IT.WORDS, { hellow = 'привет', world = 'мир' }))
  end)

  --
  it("status, save, load", function()
    local cache = M:new(nil, nil, false)
    local exp = [[
In-Memory    : empty
In-Disk      : empty
Cache-Dir    : /home/user/.local/share/env-translator/
Def-Cache-Dir: /home/user/.local/share/env-translator/
persistent   : false
updated      : false
DevMode      : true
]]
    assert.same(exp, cache:status())

    assert.same(true, cache:put(mk_resp('text', 'текст'), 'en', 'ru', T_WORDS))
    assert.same(true, cache:put(mk_resp('world', 'мир'), 'en', 'ru', T_WORDS))
    assert.same(true, cache:put(mk_resp('hellow world', 'привет мир'), 'en', 'ru', T_PHRASES))
    local exp_before_save = [[
In-Memory    :
  en-ru-words: 2 9 16
  en-ru-phrases: 1 12 19
  total: cnt:2 szo:21 szt:35
In-Disk      : empty
Cache-Dir    : /home/user/.local/share/env-translator/
Def-Cache-Dir: /home/user/.local/share/env-translator/
persistent   : false
updated      : true
DevMode      : true
]]
    assert.same(exp_before_save, cache:status())

    local exp_mmap = {
      en = {
        ru = {
          phrases = {
            ["hellow world"] = "привет мир"
          },
          words = {
            text = "текст",
            world = "мир"
          }
        }
      }
    }
    assert.same(exp_mmap, cache.mmap)

    cache:save()

    local exp_after_save = [[
In-Memory    :
  en-ru-words: 0 0 0
  en-ru-phrases: 0 0 0
  total: cnt:2 szo:0 szt:0
In-Disk      :
  en-ru-words: 2 9 16
  en-ru-phrases: 1 12 19
  total: cnt:2 szo:21 szt:35
Cache-Dir    : /home/user/.local/share/env-translator/
Def-Cache-Dir: /home/user/.local/share/env-translator/
persistent   : false
updated      : true
DevMode      : true
]]
    assert.same(exp_after_save, cache:status())
    local exp_tree = [[
/home/user/.local/share/env-translator/
    en/
        ru/
            phrases/
                1
            words/
                1
4 directories, 2 files
]]
    assert.same(exp_tree, nvim.os:tree(cache.dir))
  end)
end)

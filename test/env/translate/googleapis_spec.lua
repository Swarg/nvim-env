-- 27-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.translate.googleapis")
local uhttp = require("env.util.http")

require('env.outnvim') -- to support calling this code outside nvim

describe("env.translate.googleapis", function()
  it("mk_request", function()
    local exp = {
      host = 'translate.googleapis.com',
      method = 'GET',
      scheme = 'http',
      uri = '/translate_a/single?client=gtx&sl=en&tl=ru&dt=t&q=text+to+translate'
    }
    assert.same(exp, M.mk_request('text to translate', 'en', 'ru'))
  end)


  it("mk_url_request", function()
    local req = M.mk_request('text to translate', 'en', 'ru')
    local exp = 'http://translate.googleapis.com/translate_a/single?' ..
        'client=gtx&sl=en&tl=ru&dt=t&q=text+to+translate'
    assert.same(exp, uhttp.request_to_url(req))
  end)

  it("parse-response", function()
    local response =
    '[[["translated-text","original-text",null,null,3,null,null,[[]],[[["c5f104380d2f4c4bf0c587f790a21817","en_en_2021q4.md"]]]]],null,"en",null,null,null,null,[]]'
    local succ, t = pcall(vim.json.decode, response) --local succ, t = pcall(cjson.decode, response)

    assert.is_true(succ)
    local message = t[1][1]
    assert.is_not_nil(message)
    local translated = message[1]
    local original = message[2]
    assert.same('original-text', original)
    assert.same('translated-text', translated)
  end)

  it("parse_response check api response parsing", function()
    local response = [==[
[[["привет мир. ","hellow world.",null,null,3,null,null,[[]],[[["8e6adaf1f9ae06bcb9663531e5521abb","en_ru_2023q1.md"]]]],["Привет, мир. ","hello world.",null,null,10],["привет","hi",null,null,10]],null,"en",null,null,null,null,[]]
]==]
    local exp = {
      translated = 'привет мир. Привет, мир. привет',
      original = 'hellow world.hello world.hi'
    }
    local succ, decoded_resp = pcall(vim.json.decode, response)
    assert.same(exp, M.parse_response(decoded_resp))
  end)
end)

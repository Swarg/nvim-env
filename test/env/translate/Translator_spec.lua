-- 27-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local Translator = require "env.translate.Translator"
local googleapis = require "env.translate.googleapis"
local socket_transport = require "env.util.http.socket_transport"

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

describe("env.translate.Translator", function()
  it("new + translate stub transport", function()
    local log = {}

    -- API --
    local api = {
      mk_request = function(text, src_lang, dst_lang)
        log[#log + 1] = fmt('[api] mk_request: %s %s %s', text, src_lang, dst_lang)
        return text
      end,
      parse_response = function(response)
        log[#log + 1] = '[api] parse-response: ' .. v2s(response)
        local parsed = string.upper(response)
        log[#log + 1] = '[api] parsed: ' .. parsed
        return parsed
      end,
      validated = function(text, src_lang, dst_lang)
        return text, src_lang, dst_lang
      end,
    }
    api.mk_opts = function(cb)
      log[#log + 1] = '[api] mk_opts'
      return {
        is_json = true,
        callback = function(response, opts, uri)
          log[#log + 1] = fmt('[api-callback] uri:%s json:%s',
            v2s(uri), v2s((opts or E).is_json))
          local parsed = api.parse_response(response)
          return cb(parsed)
        end
      }
    end

    -- Transport --
    local socket_html_transport = {
      send = nil,
      decode = function(raw_resp, opts, uri)
        log[#log + 1] = fmt('[transport] decode raw_response uri: %s is_json:%s',
          v2s(uri), v2s((opts or E).is_json))
        -- emulate middleware(json decode)
        local decoded = v2s(raw_resp):gsub(' ', '_')
        log[#log + 1] = '[transport] ' .. decoded
        return decoded
      end
    }
    socket_html_transport.send = function(req, opts) -- (req, callback or self.callback)
      log[#log + 1] = '[transport] send req'

      local response, uri = req, 'URI'
      local parsed_resp = socket_html_transport.decode(response, opts, uri)
      opts.callback(parsed_resp, opts, uri)

      log[#log + 1] = '[transport] finish'
      return 'done.'
    end
    --

    local callback = function(parsed_resp)
      log[#log + 1] = '[render callback] ' .. v2s(parsed_resp)
    end

    -- require 'alogger'.fast_setup(false, 1)

    local t = Translator:new(nil, api, socket_html_transport)

    local ret = t:translate('text to translate', 'en', 'ru', callback) -- workload

    local exp = {
      '[api] mk_request: text to translate en ru',
      '[api] mk_opts',
      '[transport] send req',
      '[transport] decode raw_response uri: URI is_json:true',
      '[transport] text_to_translate',
      '[api-callback] uri:URI json:true',
      '[api] parse-response: text_to_translate',
      '[api] parsed: TEXT_TO_TRANSLATE',
      '[render callback] TEXT_TO_TRANSLATE',
      '[transport] finish'
    }
    assert.same(exp, log)
    assert.same('done.', ret)
  end)

  -- manual
  it("new + translate socket transport", function()
    if 1 == 1 then return end -- off

    local log = {}

    local callback = function(parsed_resp)
      log[#log + 1] = parsed_resp
    end

    -- require 'alogger'.fast_setup(false, 1)

    local t = Translator:new(nil, googleapis, socket_transport)
    t:translate('text to translate', 'en', 'ru', callback) -- workload

    local exp = {
      {
        original = 'text to translate',
        translated = 'текст для перевода'
      }
    }
    assert.same(exp, log)
  end)
end)

-- 27-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.translate.utils");

describe("env.translate.utils", function()
  it("detect_lang", function()
    assert.same("en", M.detect_lang('abc'))
    assert.same("ru", M.detect_lang('абс'))
    assert.same("en", M.detect_lang('it’s said that'))

    -- local m = string.match('it’s said that', "[А-Яа-яЁё]+")
    -- assert.same('x', m)
  end)

  it("indexof_sentence_sep", function()
    local f = M.indexof_sentence_sep
    assert.same(-1, f('Hellow word'))
    assert.same(7, f('Hellow. Word.'))
    assert.same(12, f('Hellow Word.'))

    local line = 'Hellow World.'
    assert.same(true, f(line) == #line)

    line = 'Hellow, World.'
    assert.same(true, f(line) == #line)

    line = 'Hellow, World!'
    assert.same(true, f(line) == #line)

    line = 'Hellow, World?'
    assert.same(true, f(line) == #line)
  end)

  -- paragraphs
  local what_is_dict1 =
      "A dictionary is a listing of lexemes from the lexicon of one " ..
      "or more specific languages, often arranged alphabetically " ..
      "(or by consonantal root for Semitic languages or radical and " ..
      "stroke for logographic languages), which may include information on " ..
      "definitions, usage, etymologies, pronunciations, translation, etc."
  local what_is_dict2 =
  "It is a lexicographical reference that shows inter-relationships among the data."

  it("get_input_type", function()
    local f = M.get_input_type
    assert.same('words', f('home'))
    assert.same('phrases', f('this is a home'))
    assert.same('sentences', f('this is a home.'))
    assert.same('sentences', f('This is a home.'))
    assert.same('paragraphs', f('This is a home. My home'))
    assert.same('sentences', f(what_is_dict1))
    assert.same('paragraphs', f(what_is_dict1 .. ' ' .. what_is_dict2))

    local mess = (what_is_dict1 .. what_is_dict2):gsub('%.', '')
    assert.same('dumps', f(mess))
  end)

  it("get_input_type not a words", function()
    local f = M.get_input_type
    assert.same('empty', f(''))
    assert.same('empty', f('  '))
    assert.same('empty', f(" \n "))
    assert.same('empty', f("1 \n 2"))
    assert.same('empty', f('123'))
    assert.same('empty', f('123.45'))
    assert.same('empty', f('123 45'))
    assert.same('empty', f('123 - -45'))
    assert.same('empty', f('123 - +45'))
    assert.same('empty', f('123 - /45'))
    assert.same('empty', f('123 - /45, 1'))
    assert.same('phrases', f('123 - /45, 1 x'))
    assert.same('empty', f('123 - /45, 1 :'))
    assert.same('empty', f('123 = 321'))
    assert.same('empty', f('123 ^ 321'))
    assert.same('empty', f('123 $ 321'))
    assert.same('phrases', f('123 $ 321 xx'))
    assert.same('empty', f('123 @ ; '))
    assert.same('empty', f('123 ()[]{}<>?~`"\'\\|'))
  end)
end)

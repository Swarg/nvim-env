---
require 'busted.runner' ()
require('env.outnvim') -- to support calling this code outside nvim
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new():init_os()
local sys = nvim:get_os()

local fs = require 'env.files'
local M = require 'env.cache.projects'

local R = require 'env.require_util'
local uv = R.require("luv", vim, "loop") --vim.loop

describe('env.cache.projects', function()
  before_each(function()
    nvim:clear_state()
    sys:clear_state()
    sys.fs:clear_state()
    M.full_cleanup()
    require 'alogger'.fast_off()
    -- D.on = false
  end)

  local root_markers = { ".git", "pom.xml", "build.gradle", "settings.gradle" }

  it("find_root_in_cache", function()
    local f = M.find_root_in_cache

    M.add_project_root('/tmp/dev/proj') -- testing data

    local exp = { ['/tmp/dev/proj/'] = { subprojects = {} } }
    assert.same(exp, M.projects_cache)
    local mkrs = root_markers
    assert.same('/tmp/dev/proj/', f(mkrs, '/tmp/dev/proj')) -- case-1
    assert.same('/tmp/dev/proj/', f(mkrs, '/tmp/dev/proj/'))

    assert.same('/tmp/dev/proj/', f(mkrs, '/tmp/dev/proj/file')) -- case-2
    assert.is_nil(f(mkrs, '/tmp/dev/proj.'))
    assert.same('/tmp/dev/proj/', f(mkrs, '/tmp/dev/proj/.'))
    -- case-3

    assert.same('/tmp/dev/proj/', f(mkrs, '/tmp/dev/proj/subdir/file'))
    local exp2 = { ['/tmp/dev/proj/'] = { subprojects = { subdir = false } } }
    assert.same(exp2, M.projects_cache)
  end)



  it("find_root-simple(project without subprojects)", function()
    local root = '/home/dev/proj/'
    local path = "/home/dev/proj/src/main/java/pkg/App.java"
    local marker = root .. 'build.gradle'

    sys:set_file_content(path, "package pkg;public class App {\n}\n")
    sys:set_file_content(marker, "buildscript-here")
    local exp = [[
/home/dev/proj/
    build.gradle
    src/
        main/
            java/
                pkg/
                    App.java
4 directories, 2 files
]]
    assert.same(exp, sys:tree(root))
    -- search in fs
    assert.same({ calls = 0, fs_access = 0 }, M.statistics)
    assert.same('/home/dev/proj/', M.find_root(root_markers, path))
    assert.same({ calls = 1, fs_access = 1 }, M.statistics)

    -- take from cache (+check subdir "/home/dev/proj/src")
    assert.same('/home/dev/proj/', M.find_root(root_markers, path))
    assert.same({ calls = 2, fs_access = 1 }, M.statistics)
    -- fs_access=2 because find_root check src subdir is it subproj-dir

    -- take all from cache
    assert.same('/home/dev/proj/', M.find_root(root_markers, path))
    assert.same({ calls = 3, fs_access = 1 }, M.statistics)
    -- fs_access not changed because already known that /home/dev/proj/src/ is
    -- not a subproject dir
  end)



  -----------------------------------------------------------------------------
  -- find_root for multi-project (parent project with subprojects)

  -- generate emulated FS with needed files
  local function mk_fs_parent_project_with_subproject_gradle_1()
    local parent_proj_root = "/home/dev/mproj/"
    local paths = {
      "/home/dev/mproj/settings.gradle",                  -- 1
      "/home/dev/mproj/app/src/main/java/pkg/App.java",   -- 2
      "/home/dev/mproj/app/src/main/java/pkg/Util.java",  -- 3
      "/home/dev/mproj/src/main/java/pkg/Main.java",      -- 4
      "/home/dev/mproj/src/main/java/pkg/Util.java",      -- 4
      "/home/dev/mproj/app/src/main/java/pkg/Tool.java",  -- 5
      "/home/dev/mproj/app/build.gradle",                 -- 6
      "/home/dev/mproj/util/build.gradle",                -- 8
      "/home/dev/mproj/util/src/main/java/pkg/Util.java", -- 9
    }
    for _, path in ipairs(paths) do
      sys:set_file_content(path, 'just-no-empty')
    end
    return parent_proj_root, paths, root_markers
  end


  -- check stub tooling
  it("mk_fs_parent_project_with_subproject_gradle_1", function()
    local pr, paths = mk_fs_parent_project_with_subproject_gradle_1()
    local exp = [[
/home/dev/mproj/
    app/
        build.gradle
        src/
            main/
                java/
                    pkg/
                        App.java
                        Tool.java
                        Util.java
    settings.gradle
    src/
        main/
            java/
                pkg/
                    Main.java
                    Util.java
    util/
        build.gradle
        src/
            main/
                java/
                    pkg/
                        Util.java
14 directories, 9 files
]]
    assert.same(exp, sys:tree(pr))
    assert.is_true(sys:is_file_exits(paths[1]))
    assert.is_true(sys:is_file_exits(paths[5]))
  end)

  it("find_root-multiproj get_project_root_marker", function()
    local pr, paths, pm = mk_fs_parent_project_with_subproject_gradle_1()
    M.full_cleanup()

    assert.is_nil(M.get_project_root_marker({}, 'x'))

    local subproj_dir, build_gradle = '/home/dev/mproj/app', 'build.gradle'
    assert.same(true, sys:is_file_exits(subproj_dir))

    local t = uv.fs_stat(fs.join_path(subproj_dir, build_gradle))
    assert.is_table(t) ---@cast t table
    assert.same('file', t.type)

    assert.same('build.gradle', M.get_project_root_marker(pm, subproj_dir))

    assert.same('/home/dev/mproj/', pr)
    assert.same('/home/dev/mproj/settings.gradle', paths[1])
    assert.same(true, sys:is_file_exits(paths[1]))
    assert.same('settings.gradle', M.get_project_root_marker(pm, pr))

    assert.same({ calls = 0, fs_access = 3 }, M.statistics)
  end)

  it("find_root-multiproj", function()
    local pr, paths, mkrs = mk_fs_parent_project_with_subproject_gradle_1()
    M.full_cleanup()

    -- search in fs
    assert.same('/home/dev/mproj/', M.find_root(mkrs, paths[1]))
    assert.same({ calls = 1, fs_access = 1 }, M.statistics)

    local exp = {
      ['/home/dev/mproj/'] = { subprojects = {} }
    }
    assert.same(exp, M.projects_cache)

    -- take from cache
    assert.same('/home/dev/mproj/', M.find_root(mkrs, paths[1]))
    -- sure no access to fs
    assert.same({ calls = 2, fs_access = 1 }, M.statistics)
    assert.same(exp, M.projects_cache) -- not changed


    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, paths[2]))
    -- access to fs to check app subdir
    assert.same({ calls = 3, fs_access = 2 }, M.statistics)
    local exp_cache2 = {
      ['/home/dev/mproj/'] = { subprojects = { app = true } }
    }
    assert.same(exp_cache2, M.projects_cache)


    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, paths[3]))
    assert.same({ calls = 4, fs_access = 2 }, M.statistics) -- sure no access to fs
    assert.same(exp_cache2, M.projects_cache)               -- not changed


    assert.same('/home/dev/mproj/src/main/java/pkg/Util.java', paths[5])
    assert.same('/home/dev/mproj/', M.find_root(mkrs, paths[5]))
    assert.same({ calls = 5, fs_access = 3 }, M.statistics)
    local exp3 = {
      ['/home/dev/mproj/'] = { subprojects = { app = true, src = false } }
    }
    assert.same(exp3, M.projects_cache) -- not changed

    assert.same('/home/dev/mproj/app/src/main/java/pkg/Tool.java', paths[6])
    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, paths[6]))
    assert.same({ calls = 6, fs_access = 3 }, M.statistics) -- sure no access to fs

    -- specific case
    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, paths[6]))
    assert.same('/home/dev/mproj/', M.find_root(mkrs, pr))
  end)

  -- Check issue then first opened subproject and for it create separate table
  -- like: ["/home/dev/mproj/app"] = { subprojects = { src = false } },
  -- insted been like   ["/home/dev/mproj"] = { subprojects = { app = true } }
  it("find_root-multiproj-2", function()
    local pr, _, mkrs = mk_fs_parent_project_with_subproject_gradle_1()
    local get_marker = M.get_project_root_marker
    M.full_cleanup()

    assert.is_nil(get_marker(mkrs, '/home/dev/mproj/src')) -- not a subproj

    assert.same('settings.gradle', get_marker(mkrs, pr))
    assert.same('build.gradle', get_marker(mkrs, '/home/dev/mproj/app'))
    assert.same('build.gradle', get_marker(mkrs, '/home/dev/mproj/util'))
  end)


  it("find_root-multiproj-2", function()
    local pr, paths, mkrs = mk_fs_parent_project_with_subproject_gradle_1()
    M.full_cleanup()

    local path1 = paths[2] -- "/home/dev/mproj/app/src/main/java/pkg/App.java"
    local path2 = paths[4] -- "/home/dev/mproj/src/main/java/pkg/Main.java"
    local path3 = paths[9] -- "/home/dev/mproj/util/src/main/java/pkg/Util.java"

    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, path1))
    assert.same({ calls = 1, fs_access = 1 }, M.statistics)
    -- from cache but checks subdir "src:
    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, path1))
    assert.same({ calls = 2, fs_access = 1 }, M.statistics)
    -- fully from cache
    assert.same('/home/dev/mproj/app/', M.find_root(mkrs, path1))
    assert.same({ calls = 3, fs_access = 1 }, M.statistics)
    local exp = {
      ['/home/dev/mproj/app/'] = { subprojects = { src = false } }
    }
    assert.same(exp, M.projects_cache)


    assert.same('/home/dev/mproj/', M.find_root(root_markers, path2))
    assert.same({ calls = 4, fs_access = 2 }, M.statistics)
    local exp2 = {
      ['/home/dev/mproj/'] = { subprojects = { app = true, src = false } }
    }
    assert.same(exp2, M.projects_cache)

    assert.same('/home/dev/mproj/', M.find_root(root_markers, path2))
    assert.same('/home/dev/mproj/app/', M.find_root(root_markers, path1))
    assert.same('/home/dev/mproj/', M.find_root(root_markers, path2))
    assert.same('/home/dev/mproj/', M.find_root(root_markers, path2))
    assert.same('/home/dev/mproj/util/', M.find_root(root_markers, path3))
    assert.same({ calls = 9, fs_access = 3 }, M.statistics)


    M.full_cleanup()
    assert.same('/home/dev/mproj/', M.find_root(root_markers, pr))
    assert.same('/home/dev/mproj/', M.find_root(root_markers, path2))
    assert.same('/home/dev/mproj/app/', M.find_root(root_markers, path1))
    assert.same('/home/dev/mproj/util/', M.find_root(root_markers, path3))
    assert.same({ calls = 4, fs_access = 4 }, M.statistics)
  end)

  -- The project name is used for example for the Jdtls workspace
  -- this is essentially just a directory name for a single stand-alone project
  -- and a parent/child for a child subproject.
  --
  -- This name will be used inside the workspace:
  --
  -- in disk                          project_name      in lsp workspace(jdtls)
  -- /home/user/dev/mproj/subproj/ -> mproj/subproj -> ~/workspace/mproj/subproj/
  --
  -- m - mean multiproject(with childs
  --
  it("get_full_project_name no .git", function()
    local _, paths, mkrs = mk_fs_parent_project_with_subproject_gradle_1()
    M.full_cleanup()
    local path2 = paths[2]
    assert.same('/home/dev/mproj/app/src/main/java/pkg/App.java', path2)
    local exp_root_dir = '/home/dev/mproj/app/'

    assert.same(exp_root_dir, M.find_root(mkrs, path2)) -- payload

    -- the subtlety here is that having found the project root marker, the
    -- search stops and does not check whether there is anything at a higher
    -- level. although logically this should be done. in order to know for sure
    -- that this project is a subproject for the parent.
    local exp = {
      ['/home/dev/mproj/app/'] = { subprojects = { src = false } }
    }
    assert.same(exp, M.projects_cache)
    assert.same('app', M.get_full_project_name(exp_root_dir))

    assert.same('/home/dev/mproj/settings.gradle', paths[1])

    -- here the call leads to the fact that it is discovered that the project
    -- above is a child and they are combined into one record and now the
    -- project name will be correct.
    assert.same('/home/dev/mproj/', M.find_root(mkrs, paths[1]))
    local exp2 = {
      ['/home/dev/mproj/'] = { subprojects = { app = true } }
    }
    assert.same(exp2, M.projects_cache)
    assert.same('mproj', M.get_full_project_name('/home/dev/mproj/'))
    assert.same('mproj/app', M.get_full_project_name('/home/dev/mproj/app/'))
    assert.same('mproj/app', M.get_full_project_name('/home/dev/mproj/app'))
    assert.same('mproj/appx', M.get_full_project_name('/home/dev/mproj/appx'))
    assert.same('mproj/ap', M.get_full_project_name('/home/dev/mproj/ap'))
  end)

  -- correction of the full name of the subproject by checking the parent
  -- directory for the presence of the .git marker on get_full_project_name
  it("get_full_project_name with .git marker", function()
    local pr, paths, mkrs = mk_fs_parent_project_with_subproject_gradle_1()
    assert.same(true, sys:mk_dir(fs.join_path(pr, '.git'))) -- common marker
    M.full_cleanup()

    local path2 = paths[2]
    assert.same('/home/dev/mproj/app/src/main/java/pkg/App.java', path2)
    local exp_root_dir = '/home/dev/mproj/app/'

    assert.same(exp_root_dir, M.find_root(mkrs, path2)) -- payload

    local exp = {
      ['/home/dev/mproj/app/'] = { subprojects = { src = false } }
    }
    assert.same(exp, M.projects_cache)
    assert.same('mproj/app', M.get_full_project_name(exp_root_dir))
    local exp2 = {
      ['/home/dev/mproj/'] = { subprojects = { app = true } }
    }
    assert.same(exp2, M.projects_cache)

    assert.same('mproj', M.get_full_project_name('/home/dev/mproj/'))
    assert.same('mproj/app', M.get_full_project_name('/home/dev/mproj/app/'))
    assert.same('mproj/app', M.get_full_project_name('/home/dev/mproj/app'))
    assert.same('mproj/appx', M.get_full_project_name('/home/dev/mproj/appx'))
    assert.same('mproj/ap', M.get_full_project_name('/home/dev/mproj/ap'))
  end)


  -- 'Keep Cache clean'
  it("absorbe_subs", function()
    local cache = {
      ["/home/dev/mproj/app/"] = { subprojects = { src = false } },
      ["/home/dev/mproj/api/"] = { subprojects = { src = false } },
      ["/home/dev/mproj/util/"] = { subprojects = { src = false } },
    }
    local exp = {
      ["/home/dev/mproj/"] = {
        subprojects = { app = true, api = true, util = true }
      }
    }
    -- add root_project for subs
    local root = '/home/dev/mproj'
    cache[fs.ensure_dir(root)] = { subprojects = {} }
    M.absorbe_subprojects(cache, root)
    assert.same(exp, cache)
  end)

  it("is_nearest_subdir", function()
    local f = M.is_nearest_subdir
    assert.same(true, f("/tmp/parent/", '/tmp/parent/child/'))
    assert.same(true, f("/tmp/parent/", '/tmp/parent/child'))
    assert.same(false, f("/tmp/parent/", '/tmp/parent/child/sub'))
    assert.same(false, f("/tmp/parent", '/tmp/parent/child/sub'))
    assert.same(false, f("/tmp/parent/", '/tmp/another/child'))
  end)

  -- opening the built-in LuaTest second after cached the main project
  it("add_project_root first:main-proj second:LutTest", function()
    local add = M.add_project_root
    add('/home/dev/proj/')
    local exp = { ['/home/dev/proj/'] = { subprojects = {} } }
    assert.same(exp, M.projects_cache)

    add('/home/dev/proj/src/main/_lua/')
    local exp2 = {
      ['/home/dev/proj/'] = {
        embeded = '/home/dev/proj/src/main/_lua/',
        subprojects = {}
      },
      ['/home/dev/proj/src/main/_lua/'] = { subprojects = {} }
    }

    assert.same(exp2, M.projects_cache)
    local exp3 = '/home/dev/proj/src/main/_lua/'

    assert.same(exp3, M.find_root_in_cache(nil, '/home/dev/proj/src/main/_lua/a_spec.lua'))
  end)

  -- this case is intended to never happen
  -- because even if the project has a built-in Lua test, the root for this file
  -- will be the root of the project, then LuaTester will recognize it and
  -- it will recache itself, setting itself the desired directory
  -- that is, the project_root will always be added first, and only then
  -- the nested dir within it for the LuaTester
  -- here I show that if you break this sequence of adding roots, the embeded
  -- nesting breaks
  -- Note: use `:EnvCache i` to debug projects_cache behavior in alive nvim
  it("add_project_root first:LuaTest second:main-proj", function()
    local add = M.add_project_root

    add('/home/dev/proj/src/main/_lua/')
    local exp = { ['/home/dev/proj/src/main/_lua/'] = { subprojects = {} } }
    assert.same(exp, M.projects_cache)

    add('/home/dev/proj/')
    local exp2 = {
      ['/home/dev/proj/'] = { subprojects = {} },
      ['/home/dev/proj/src/main/_lua/'] = { subprojects = {} }
    }
    assert.same(exp2, M.projects_cache)

    local res = M.find_root_in_cache(nil, '/home/dev/proj/src/main/_lua/a_spec.lua')
    assert.same('/home/dev/proj/src/main/_lua/', res)
  end)


  it("absorbe_subs", function()
    M.add_project_root("/home/dev/proj/")
    local exp = { ['/home/dev/proj/'] = { subprojects = {} } }
    assert.same(exp, M.projects_cache)

    M.add_project_root("/home/dev/proj/src/test/_lua/")

    local exp2 = {
      ['/home/dev/proj/'] = {
        embeded = '/home/dev/proj/src/test/_lua/',
        subprojects = {}
      },
      ['/home/dev/proj/src/test/_lua/'] = { subprojects = {} }
    }
    assert.same(exp2, M.projects_cache)
  end)

  it("absorbe_subs", function()
    local cache = {
      ["/home/dev/proj/"] = { subprojects = {} },
      ["/home/dev/proj/src/test/_lua/"] = { subprojects = {} },
    }
    -- add root_project for subs
    local root = '/home/dev/proj'
    cache[fs.ensure_dir(root)] = { subprojects = {} }
    M.absorbe_subprojects(cache, root)

    local exp = cache -- no changed
    assert.same(exp, cache)
  end)

  it("get_parent_root_dir", function()
    local f = M.get_parent_root_dir
    M.projects_cache = {
      ["/home/dev/mproj/"] = {
        subprojects = { app = true, api = true, util = true }
      }
    }
    assert.same('/home/dev/mproj/', f('/home/dev/mproj/app/'))
    assert.same('/home/dev/mproj/', f('/home/dev/mproj/api/'))
    assert.same('/home/dev/mproj/', f('/home/dev/mproj/util/'))
    assert.same('/home/dev/mproj/', f('/home/dev/mproj/'))
    assert.same('/home/dev/mproj/', f('/home/dev/mproj'))
    assert.is_nil(f('/home/dev/mprojx'))
    assert.same('/home/dev/mproj/', f('/home/dev/mproj/file'))
  end)

  it("add_subproject", function()
    M.projects_cache = {
      ["/tmp/dev/mproj/"] = {
        subprojects = {
          subproj1 = true,
          subproj2 = true,
        }
      }
    }
    assert.same(false, M.add_project_root('/tmp/dev/mproj/subproj1'))
    local exp = {
      ['/tmp/dev/mproj/'] = { subprojects = { subproj1 = true, subproj2 = true } }
    }
    assert.same(exp, M.projects_cache)

    assert.same(true, M.add_project_root('/tmp/dev/mproj/subproj3'))
    local exp2 = {
      ['/tmp/dev/mproj/'] = {
        subprojects = { subproj1 = true, subproj2 = true, subproj3 = true }
      }
    }
    assert.same(exp2, M.projects_cache)
  end)


  it("set_subproj_entry", function()
    assert.same(true, M.set_subproj_entry('parent', 'child', true))
    local exp = { parent = { subprojects = { child = true } } }
    assert.same(exp, M.projects_cache)

    assert.same(true, M.get_subproj_entry('parent', 'child'))

    -- rewrite existed
    assert.same(true, M.set_subproj_entry('parent', 'child', { 'state' }))
    assert.same({ 'state' }, M.get_subproj_entry('parent', 'child'))
  end)


  it("get_root_by_name", function()
    local dirname = "project_c"
    local root1 = "~/dev/workspace/lang/proj_a/"
    local root2 = "~/dev/workspace/lang/proj_b/"
    local root3 = "~/dev/workspace/lang/" .. dirname .. '/'
    M.projects_cache = {
      [root1] = { subprojects = {} },
      [root2] = { subprojects = {} },
      [root3] = { subprojects = {} },
    }
    assert.same(root3, M.get_root_by_name(dirname))
  end)

  -- todo next
  it("get_root_by_name(project_name)", function()
    M.projects_cache = {
      ["/home/dev/mproj/"] = {
        subprojects = { app = true, api = true, util = true }
      }
    }
    assert.is_nil(M.get_root_by_name('app'))
    assert.same('/home/dev/mproj/', M.get_root_by_name('mproj'))
    assert.same('/home/dev/mproj/app/', M.get_root_by_name('mproj/app'))
    assert.same('/home/dev/mproj/app/', M.get_root_by_name('mproj/app/'))
  end)

  it("parse_project_name", function()
    local f = M.parse_project_name
    assert.same({ 'parent', 'child' }, { f('parent/child') })
    assert.same({ 'parent' }, { f('parent/') })
    assert.same({ 'parent' }, { f('parent') })
    assert.same({}, { f('/') })
  end)


  it("get_cached_root_dir_for", function()
    -- local dirname = "project_c"
    local root1 = "~/dev/workspace/lang/proj_a/"
    local root2 = "~/dev/workspace/lang/proj_b/"
    M.projects_cache = {
      [root1] = { subprojects = { src = false } },
      [root2] = { subprojects = { app = true } },
    }
    local f = M.get_cached_root_dir_for
    local exp = '~/dev/workspace/lang/proj_b/app/'
    assert.same(exp, f(root2 .. 'app/src/main/java/pkg/App.java'))
    assert.same(exp, f(root2 .. 'app/'))
    assert.same(root2, f(root2 .. 'app')) -- treats as a file not as a directory

    -- not a subprojects
    assert.same(root2, f(root2 .. 'doc/README.md'))
    assert.same(root1, f(root1 .. 'src/main/java/pkg/App.java'))
  end)


  it("get_project_name_from_jdt", function()
    -- assert.same("bcd", string.match("a=bcd/e", "^.*=([^\\/]+).*")) -- bcde
    -- assert.same("123", string.match("a=bcde/f=123/5", "^.*=([^\\/]+).*")) -- 123
    local s = "jdt://some?=abc/de=jk"
    assert.same("abc", string.match(s, "jdt://[^=]+=([^\\/]+).*"))

    local bufname = "jdt://contents/java.base/java.lang/System.class?" ..
        "=project_a/%5C/usr%5C/lib%5C/jvm%5C/java17%5C/lib%5C/jrt-fs.jar" ..
        "%60java.base=/javadoc_location=" ..
        "/https:%5C/%5C/docs.oracle.com%5C/en%5C/java%5C/javase%5C/17%5C/" ..
        "docs%5C/api%5C/=/%3Cjava.lang(System.class"
    assert.same("project_a", M.get_project_name_from_jdt(bufname))
  end)

  it("get_project_name_from_jdt", function()
    local f = M.get_project_name_from_jdt
    local s = "jdt://contents/rt.jar/java.util.stream/Stream.class?" ..
        "=my-project-name/" ..
        "%5C/usr%5C/lib%5C/jvm%5C/oracle%5C/jdk1.8.0_351%5C/jre%5C/lib%5C/rt.jar"
        ..
        "=/javadoc_location=" ..
        "/https:%5C/%5C/docs.oracle.com%5C/javase%5C/8%5C/docs%5C/api%5C/=/=/" ..
        "maven.pomderived=/true=/%3Cjava.util.stream(Stream.class"
    assert.same('my-project-name', f(s))
  end)
end)

require 'busted.runner' ()
require('env.outnvim') -- to support calling this code outside nvim
local assert = require('luassert')

local M = require 'env.cache.library'

describe('env.cache.projects', function()
  it("get", function()
    local rec, created = M.get('lua', 'pkg', '1.0')
    assert.same({}, rec)
    assert.same(true, created)
    local exp = { lua = { pkg = { ["1.0"] = {} } } }
    assert.same(exp, M.get_cache())

    -- write to cached record
    rec['module.name'] = '/full/path/to/module/name.lua'
    local exp2 = {
      lua = {
        pkg = {
          ["1.0"] = {
            ['module.name'] = '/full/path/to/module/name.lua',
          }
        }
      }
    }
    assert.same(exp2, M.get_cache())
    assert.same(true, M.clean('lua', 'pkg', '1.0'))
    local exp3 = { lua = { pkg = {} } }
    assert.same(exp3, M.get_cache())
  end)

  it("clean", function()
    M.get('lua', 'pkg', '1.0')
    M.get('lua', 'pkg', '1.1')
    M.get('lua', 'pkg', '1.2')

    assert.same(true, M.clean('lua', 'pkg', '1.0'))
    local exp3 = { lua = { pkg = { ['1.2'] = {}, ['1.1'] = {} } } }
    assert.same(exp3, M.get_cache())

    assert.same(true, M.clean('lua', 'pkg'))

    assert.same({ lua = {} }, M.get_cache())
  end)
end)

--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local Context = require("env.ui.Context")
local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE

---@return env.ui.Context
local function mkCtx(lines, cursor_row, cursor_col)
  nvim:new_buf(lines, cursor_row, cursor_col)
  local ctx = Context:new()
  -- ctx.current_line
  return ctx
end

describe("env.ui.Context", function()
  before_each(function()
    nvim:clear_state()
  end)




  it("check-helper", function()
    --                  1234567890
    local lines = { '--', 'MyClass:method(params)' }
    -- nvim.D.enable()
    local ctx = mkCtx(lines, 2, 12)
    -- Context.current_line
    -- cast Context ctx
    assert.same(lines[2], ctx.current_line)
  end)

  -- element is a current word under the cursor in current line
  it("element", function()
    --                   1234567890
    local ctx = mkCtx({ 'MyClass:method(params)' }, 1, 12)
    ctx:resolveWords()

    assert.same("method", ctx:element())                   -- any
    assert.same("method", ctx:element(LEXEME_TYPE.METHOD)) -- onfy if matched
    assert.is_nil(ctx:element(LEXEME_TYPE.ARRAY))          -- not matached - nil
  end)

  it("hasWordElement", function()
    local ctx = mkCtx({ 'MyClass:method(params)' }, 1, 12)
    local exp_lex_type = LEXEME_TYPE.METHOD
    local exp = { false, 'no resolved word (word under the cursor)', -1 }
    assert.same(exp, { ctx:getWordElement(exp_lex_type) })

    ctx:resolveWords()

    local exp2 = { 'method', nil, LEXEME_TYPE.METHOD }
    assert.same(exp2, { ctx:getWordElement(exp_lex_type) })
  end)

  it("is_string_with_new_lines", function()
    local f = Context.is_string_with_new_lines
    assert.is_false(f("abc"))
    assert.is_true(f("a\nbc"))
    assert.is_true(f("\n"))
    assert.is_false(f(""))
    assert.is_false(f(" "))
  end)

  it("__tostring", function()
    local ctx = mkCtx({ 'MyClass:method(params)' }, 1, 12)
    ctx:resolveWords()
    local exp = [[
bufnr:1 Editable:true cursor(lnum:1 col:12) lines: ?
MyClass:method(params)
word_element(9:14): "method" [11:METHOD] word_container: "MyClass"]]
    assert.same(exp, tostring(ctx))
  end)

  it("processLines handler: walk all lines", function()
    local bufferLines =
    {
      '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
      '13', '14', '15', '16', '17', '18',
    }

    local ctx = mkCtx(bufferLines, 1, 1)
    local userdata = {}
    ---@diagnostic disable-next-line: unused-local
    local handler = function(o, lines, lnum, total_lines)
      o.res = o.res or ''
      o.res = o.res .. table.concat(lines, '-') .. '-'
      return true
    end
    local exp = {
      { res = '1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-' }, 18
    }
    assert.same(exp, { ctx:processLines(handler, userdata, { batch = 4 }) })
    assert.same('1-2-3-4-5-6-7-8-9-10-11-12-13-14-15-16-17-18-', userdata.res)
  end)

  --
  it("processLines handler: walk forst 10 lines", function()
    local bufferLines =
    {
      '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12',
      '13', '14', '15', '16', '17', '18',
    }

    local ctx = mkCtx(bufferLines, 1, 1)
    local userdata = {}
    ---@diagnostic disable-next-line: unused-local
    local handler = function(o, lines, lnum, total_lines)
      o.res = o.res or ''
      for i, line in ipairs(lines) do
        o.res = o.res .. line .. '-'
        if line == '10' then
          o.stop_at = i + lnum
          return false -- stop
        end
      end
      return true
    end
    local exp = { res = '1-2-3-4-5-6-7-8-9-10-', stop_at = 10 }
    assert.same(exp, ctx:processLines(handler, userdata, { batch = 4 }))
    assert.same('1-2-3-4-5-6-7-8-9-10-', userdata.res)
  end)

  it("insertAfterCurrentLine string", function()
    local lines = {
      "--",
      "  current_line",
      "  next_line",
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    Context:new():insertAfterCurrentLine("line to insert")
    local exp = {
      '--',
      '  current_line',
      'line to insert',
      '  next_line',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)

  it("insertAfterCurrentLine string", function()
    local lines = {
      "--",
      "  current_line",
      "  next_line",
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    Context:new():insertAfterCurrentLine("line1\nline2")
    local exp = {
      '--',
      '  current_line',
      'line1',
      'line2',
      '  next_line',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)

  it("insertAfterCurrentLine string", function()
    local lines = {
      "--",
      "  current_line",
      "  next_line",
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    Context:new():insertAfterCurrentLine({ "line1", "line2" })
    local exp = {
      '--',
      '  current_line',
      'line1',
      'line2',
      '  next_line',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)

  it("insertBeforeCurrentLine string", function()
    local lines = {
      "--",
      "  current_line",
      "  next_line",
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    Context:new():insertBeforeCurrentLine({ "line1", "line2" })
    local exp = {
      '--',
      'line1',
      'line2',
      '  current_line',
      '  next_line',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)

  it("pullCurrLine", function()
    local lines = {
      "--",
      "  current_line", -- 2
      "  next_line",
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    assert.same('  current_line', vim.api.nvim_get_current_line())

    Context:new():pullCurrLine()

    local exp = {
      '--',
      '  next_line',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)


  it("getLinesBetween", function()
    local lines = {
      "--", -- 1
      "open",
      "close",
      '--', -- 4
    }
    nvim:new_buf(lines, 2, 12)
    local res, lend = Context:new():getLinesBetween("open", "close")
    local exp = { false, 3 }
    assert.same(exp, { res, lend })
  end)

  it("getLinesBetween not found", function()
    local lines = {
      "--", -- 1
      "  open",
      "  close",
      '--', -- 4
    }
    nvim:new_buf(lines, 2, 12)
    local res, lend = Context:new():getLinesBetween("open", "close")
    local exp = { false, -1 }
    assert.same(exp, { res, lend })
  end)

  it("getLinesBetween usetrim", function()
    local lines = {
      "--", -- 1
      "  open",
      "  close",
      '--', -- 4
    }
    nvim:new_buf(lines, 2, 12)
    local res, lend = Context:new():getLinesBetween("open", "close", true)
    local exp = { false, 3 }
    assert.same(exp, { res, lend })
  end)

  it("getLinesBetween usetrim", function()
    local lines = {
      "--", -- 1
      "  open",
      "  content",
      "  close",
      '--', -- 4
    }
    nvim:new_buf(lines, 1, 12)
    local res, lend = Context:new():getLinesBetween("open", "close", true)
    local exp = { { '  content' }, 4 }
    assert.same(exp, { res, lend })
  end)

  it("getLinesBetween", function()
    local lines = {
      "--", -- 1
      "open",
      "  line 3",
      "  line 4",
      "close",
      '--', -- 6
    }
    nvim:new_buf(lines, 2, 12)
    local res, lend = Context:new():getLinesBetween("open", "close")
    local exp = { { '  line 3', '  line 4' }, 5 }
    assert.same(exp, { res, lend })
  end)

  it("replaceCurrentWordByLines", function()
    local lines = {
      "--",
      "word", -- 2
      '--',
    }
    local bufnr = nvim:new_buf(lines, 2, 3)
    local ctx = Context:new()
    ctx:resolveWordSimple('[%w_%.%-:]')
    assert.same(true, ctx:replaceCurrentWordByLines({ 'replacement', 'lines' }))
    local exp = {
      '--',
      'replacement',
      'lines',
      '--'
    }
    assert.same(exp, nvim:get_lines(bufnr))
  end)
end)

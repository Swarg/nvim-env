--
require 'busted.runner' ()
local assert = require('luassert')

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local lapi_consts = require 'env.lang.api.constants'
local LEXEME_TYPE = lapi_consts.LEXEME_TYPE


describe('env.ui.Editor', function()
  setup(function()
    Editor = require("env.ui.Editor")
    Context = require("env.ui.Context")
  end)

  teardown(function()
  end)

  before_each(function()
    nvim:clear_state()
  end)
  after_each(function() end)

  it("resolveWords", function()
    ---1234567890123456789012345678901234567890
    local lines = {
      "--",
      "  task.setUrl(delayed(Constants.RES_URL0));",
      '--',
    }
    nvim:new_buf(lines, 2, 12)
    local e = Editor:new()

    local ctx = e:resolveWords().ctx
    -- print("[DEBUG] ctx:", inspect(ctx))
    assert.same('No Name', ctx.bufname)
    assert.same(1, ctx.bufnr)
    assert.same(true, ctx.can_edit)
    assert.same('  task.setUrl(delayed(Constants.RES_URL0));', ctx.current_line)
    assert.same(12, ctx.cursor_col)
    assert.same({ 2, 12 }, ctx.cursor_pos)
    assert.same(2, ctx.cursor_row) --line num
    assert.same(LEXEME_TYPE.FUNCTION, ctx.lexeme_type)
    -- words under cursor
    assert.same('task', ctx.word_container)
    assert.same('setUrl', ctx.word_element)
  end)

  it("newReport", function()
    local editor = Editor:new()
    ---@cast editor env.ui.Editor
    local title = 'abc'
    vim.schedule_wrap = false
    local report = editor:newReport(title)
    assert.is_not_nil(report)
    assert.is_not_nil(editor.reports)
    assert.is_not_nil(editor.reports[title])

    report:add('Its works %s', "!")
    assert.same("Its works !\n", editor.reports[title])

    report:add('A new line.')
    assert.same("Its works !\nA new line.\n", editor.reports[title])
  end)

  it("join table", function()
    local line, lines0 = 'abc', { 'def', 'klm' }
    local lines = table.pack(line, table.unpack(lines0))
    assert.same(3, lines.n)
    lines.n = nil
    assert.same({ 'abc', 'def', 'klm' }, lines)
  end)
end)

describe('env.ui.Editor BufIterator', function()
  local Editor ---@cast Editor env.ui.Editor
  setup(function()
    Editor = require 'env.ui.Editor'
  end)

  teardown(function()
  end)

  before_each(function()
    nvim:clear_state()
  end)
  after_each(function() end)

  it("BufIterator", function()
    local lines = {
      "a", 'b', 'c', 'd'
    }
    local bufnr = nvim:new_buf(lines, 2, 12)
    local iter = Editor.iterator(bufnr, 0)
    assert.same(true, iter:hasNext())
    assert.same('a', iter:next())
    assert.same('b', iter:next())
    assert.same('c', iter:next())
    assert.same(true, iter:hasNext())
    assert.same('d', iter:next())
    assert.is_nil(iter:next())
    assert.same(false, iter:hasNext())
  end)
end)

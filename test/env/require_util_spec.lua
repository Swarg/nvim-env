require 'busted.runner' ()

local R = require 'env.require_util'

describe('env.require_util', function()
  it('take from table', function()
    local t = { sub = { deep1 = "abc" } }
    local m = R.require("here-tacke-from-table", t, "sub", "deep1")
    assert.same("abc", m)
  end)

  it('tack from module-name', function()
    local bad = nil
    local m = R.require("env.require_util", bad, "sub")
    assert.truthy(type(m) == "table")
  end)


  -- experimental: check the versions of luv built into the nvim and the system
  -- If this code will be runned from nvim this show builtin version
  -- owerthise if run it via buster or lua -- show system scope ver
  -- to install to system use: 'apt install lua-luv' or 'luarocks install luv'
  it('check luv', function()
    local ok, uv = pcall(require, "luv")
    if ok then
      local line = ("luv " .. (uv.version_string() or "?") .. ' '
        .. (uv.version() or "?"))
      -- print(line)
      assert.is_not_nil(line)
    end
  end)

  -- inspect lib builtin in nvim as vim.inspect
  it('check inspect', function()
    local ok, m = pcall(require, "inspect")
    if ok then
      -- print("inspect " .. (m._VERSION or "?"))
      assert.is_not_nil(m._VERSION)
    end
  end)

  -- used by busted, has in nvim builtin
  -- luarocks doc luassert
  it('check luassert', function()
    local ok, m = pcall(require, "luassert")
    if ok then
      ---@diagnostic disable-next-line: undefined-field
      assert.is_not_nil(m._VERSION)
      -- print("luassert " .. (m._VERSION or "?"))
    end
  end)
end)

-- 02-12-2023 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local M_tokenizer = require("env.util.tokenizer.parser")
local M_token_tree = require("env.util.tokenizer.tree")

local D = require("dprint")
local M = require("env.lua_parser");


describe("env.lua_parser", function()
  it("find_function_block_range pre", function()
    local line =
    '  it("description of do-stuff", function()'
    -- ^token-1 path 0 1  or  /1
    --   ^--- token-2-branch with subtree------^ to the end of line(not finished)
    local t = M_token_tree.of(line)
    local exp = [[
----    1  e  [3:4]             it
d---    2  (  [5:42]            ("description of do-stuff", function()
]]
    assert.same(exp, t:cd(0):ls())
    local exp2 = [[
----    1  "  [6:30]            "description of do-stuff"
----    2  e  [33:40]           function
----    3  (  [41:42]           ()
]]
    assert.same(exp2, t:cd(2):ls())
    -- e means the extra chars before the group  like func() where () - is a group
  end)

  -- assert.samev = assert.samev

  -- without wrapper:
  -- l_start, l_end = M.get_function_block_range(bufnr, 2) -- 2 is the lnum
  -- assert.same('1-3', tostring(l_start) .. '-' .. tostring(l_end))
  -- a wrapper to easy asserts
  -- proxy helper for testing one-line assert for two values
  local find_function_block_range0 = function(bufnr0, lnum0)
    local l_start, l_end = M.find_function_block_range(bufnr0, lnum0)
    return tostring(l_start) .. '-' .. tostring(l_end)
  end

  -- local exp = TH.cut_lines(lines, 3, 8, 1)
  it("find_function_block_range 1", function()
    local f = find_function_block_range0
    local lines = {
      '--',                                              --  0
      'function M.prev_func(bufnr, lnum)',               --  1
      '',                                                --  2
      'end',                                             --  3
      '',                                                --  4
      'function M.func_parent(bufnr, lnum)',             --  5    +
      '  local y = z',                                   --  6    |
      '  it("description of the function ", function()', --  7 +  |
      '     -- line 3',                                  --  8 |  |
      '     for k,v in pairs(t) do',                     --  9 |  |
      '       work(k,v)',                                -- 10 |  |
      '     end',                                        -- 11 |  |
      '  end)',                                          -- 12 +  |
      '  local x = 0',                                   -- 13    |
      'end',                                             -- 14    +
      '',                                                -- 15
      'function M.next_func(bufnr, lnum)',               -- 16
      '',                                                -- 17
      'end',                                             -- 18
      '  function M.foo(bufnr, lnum)',                   -- 19
      '',                                                -- 20
      '  end',                                           -- 21
      '',                                                -- 22
    }
    local bufnr, lnum, col = 4, 3, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    -- D.enable() D.disable_modules(M_tokenizer, M_token_tree)
    assert.same('1-3', f(bufnr, 2))
    assert.same('19-21', f(bufnr, 20))
    assert.same('5-14', f(bufnr, 5))
    assert.same('1-3', f(bufnr, 1))
    assert.same('1-3', f(bufnr, 2))
    assert.same('1-3', f(bufnr, 3))
    assert.same('7-12', f(bufnr, 8))
    assert.same('7-12', f(bufnr, 10))
    assert.same('7-12', f(bufnr, 11))
    assert.same('7-12', f(bufnr, 12))
    assert.same('5-14', f(bufnr, 14))
    assert.same('19-21', f(bufnr, 19))
    assert.same('19-21', f(bufnr, 21))
    assert.same('nil-nil', f(bufnr, 0))
    assert.same('nil-nil', f(bufnr, 4))
    assert.same('nil-nil', f(bufnr, 15))
    assert.same('nil-nil', f(bufnr, 22))
  end)

  it("find_function_block_range ", function()
    -- proxy helper for testing one-line assert for two values
    local f = find_function_block_range0
    local lines = {
      '--',                                --  0
      '  function M.foo(bufnr, lnum)',     --  1
      'function M.next_func(bufnr, lnum)', --  2
      'end',                               --  3
      '  -function M.foo(bufnr, lnum)',    --  4
      '',                                  --  5
      '  end',                             --  6  <<
      '',                                  --  7
    }
    local bufnr, lnum, col = 0, 6, 16
    nvim:new_buf(lines, lnum, col)
    -- D.enable() D.disable_modules(M_tokenizer, M_token_tree)
    assert.same('1-6', f(bufnr, 1)) -- is fix needed? (bad idents only)
    assert.same('nil-nil', f(bufnr, 5))
    assert.same('nil-nil', f(bufnr, 6))
    assert.same('nil-nil', f(bufnr, 7))
    assert.same('2-3', f(bufnr, 2))
    assert.same('2-3', f(bufnr, 3))
  end)

  it("find_function_block_range ", function()
    local f = find_function_block_range0

    local lines = {
      '--',                                --  0
      'function M.next_func(bufnr, lnum)', --  1
      '  --',                              --  2
      '  function M.foo(bufnr, lnum)',     --  3
      '    local v = [[',                  --  4 +
      '  function bar(x)',                 --  5 |
      '    return x * x',                  --  6 |
      '  end',                             --  7 |
      '  function zbar(x)',                --  8 |
      ']]',                                --  9 +
      '',                                  -- 10
      '  end',                             -- 11
      'end',                               -- 12
    }
    local bufnr, lnum, col = 0, 6, 16
    nvim:new_buf(lines, lnum, col)
    -- D.enable() D.disable_modules(M_tokenizer, M_token_tree)

    -- if 0 == 0 then return end
    assert.same('5-7', f(bufnr, 7)) -- inside string block not check inside [[
    assert.same('3-11', f(bufnr, 3))
    -- 4 start from line with sblock - remember sblock to pass to the end-find
    assert.same('3-11', f(bufnr, 4))
    assert.same('nil-nil', f(bufnr, 8)) -- inside sblock
    assert.same('3-11', f(bufnr, 9))
    assert.same('3-11', f(bufnr, 10))   -- start empty line + sblock before
    assert.same('3-11', f(bufnr, 11))
  end)

  it("find_function_block_range lua sblock", function()
    -- proxy helper for testing one-line assert for two values
    local f = function(bufnr0, lnum0)
      local l_start, l_end = M.find_function_block_range(bufnr0, lnum0)
      return tostring(l_start) .. '-' .. tostring(l_end)
    end

    local lines = {
      '--',                                --  0
      'function M.next_func(bufnr, lnum)', --  1
      '  --',                              --  2
      '  function M.foo(bufnr, lnum)',     --  3
      '    local v = [==[',                --  4 +
      '  function bar(x)',                 --  5 |
      '    return x * x',                  --  6 |
      '  end',                             --  7 |
      '  function zbar(x)',                --  8 |
      '    local x = [[',                  --  9 |
      '  end',                             -- 10 |
      ']]',                                -- 11 |
      '  end',                             -- 12 |
      ']==]',                              -- 13 +
      '',                                  -- 14
      '  end',                             -- 15
      'end',                               -- 16
    }
    local bufnr, lnum, col = 0, 6, 16
    nvim:new_buf(lines, lnum, col)
    -- D.enable()
    D.disable_modules(M_tokenizer, M_token_tree)
    assert.same('8-12', f(bufnr, 10)) -- todo fix 3-15
    -- if 0 == 0 then return end
    assert.same('5-7', f(bufnr, 7))
    assert.same('3-15', f(bufnr, 3))
    assert.same('3-15', f(bufnr, 4))
    assert.same('8-12', f(bufnr, 8))  -- inside [==[]==]
    assert.same('8-12', f(bufnr, 9))
    assert.same('8-12', f(bufnr, 10)) -- todo fix with str-block
    assert.same('8-12', f(bufnr, 11))
  end)
end)

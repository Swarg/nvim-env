--
require 'busted.runner' ()
local assert = require('luassert')
local M = require("env.lua_parser")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local R = require("env.require_util")
---@diagnostic disable-next-line: unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect
local uv = R.require("luv", vim, "loop")             -- vim.loop

local D = require("dprint")
local api = _G.vim.api

-- todo move to env.lang.lua.consts
local busted0 = {
  NAMESPACE = 2, -- in testing
  IN_ROW_COL_START = 1,
  IN_ROW_COL_END = 80,
}
local ns_id = busted0.NAMESPACE

---@diagnostic disable-next-line: unused-local
local dprint, dvisualize = D.print, D.visualize

describe('env.lua_parser is_simple_varname', function()
  after_each(function()
    nvim:clear_state()
    D.disable()
  end)

  local cases = {
    -- exp, input
    { true,  'a' },
    { true,  'abc' },
    { true,  'a_bc' },
    { true,  '_a' },
    { true,  'A' },
    { false, '.abc' },
    { false, ' abc' },
    { false, 'abc ' },
    { false, 'a.bc' },
    { false, 'a/bc' },
    { false, 'a\\bc' },
    { false, 'a,bc' },
    { false, 'a bc' },
    { false, 'a-bc' },
    { false, 'a^bc' },
    { true,  'Abc' },
    { true,  'abc0' },
    { false, '0abc' },
    { false, '0' },
    { false, ' 0' },
    { false, 'a 0' },
  }
  for i, case in pairs(cases) do
    describe('case: ' .. i, function()
      it("input: [" .. case[2] .. "]", function()
        assert.same(case[1], M.is_simple_varname(case[2]))
      end)
    end)
  end
end)

describe('env.lua_parser parse_to_type_value', function()
  local cases = {
    -- input            exp
    { "(string) 'abc'",             { 'string', 'abc' } },
    { "(boolean) true",             { 'boolean', 'true' } },
    { "(number) 12345",             { 'number', '12345' } },
    { "(table: 0x55c53b216b80) {}", { 'table', "{}" } },
  }
  for i, case in pairs(cases) do
    describe('case: ' .. i, function()
      local input, exps = case[1], case[2]
      it("input: [" .. input .. "]", function()
        local t, v = M.parse_to_type_value(input)
        assert.same(exps[1], t)
        assert.same(exps[2], v)
      end)
    end)
  end
end)


describe('env.lua_parser', function()
  it("is_commented", function()
    assert.same(true, M.is_commented('--'))
    assert.same(true, M.is_commented(' --'))
    assert.same(true, M.is_commented(' -- abc'))
    assert.same(true, M.is_commented(' ----abc'))
    assert.same(true, M.is_commented(' ----[['))
    assert.same(true, M.is_commented(' ----[[]]'))
    assert.same(false, M.is_commented('local var = "value" -- extra'))
  end)

  it("is_class_name", function()
    assert.same(false, M.is_class_name('a'))
    assert.same(false, M.is_class_name('A'))
    assert.same(true, M.is_class_name('Ab'))
    assert.same(true, M.is_class_name('MyClass'))
    assert.same(true, M.is_class_name('My_Class'))
    assert.same(true, M.is_class_name('My_Class0'))
    assert.same(false, M.is_class_name('Not.Class'))
    assert.same(false, M.is_class_name('Not-Class'))
    assert.same(false, M.is_class_name('Not/Class'))
    assert.same(false, M.is_class_name('Not Class'))
    assert.same(false, M.is_class_name('Not$Class'))
  end)

  it("regex", function()
    assert.same('\'', string.match("'", "['\"]"))
    assert.same('\'abc', string.match("'abc", "['\"][%w]+"))
    assert.same('\'abc\'', string.match("'abc'", "['\"][%w]+['\"]"))
    assert.same('\'a/bc\'', string.match("'a/bc'", "['\"][%w_/]+['\"]"))
    assert.same('a/b', string.match(" .. 'a/b'", "[^'\"]['\"]([%w_/]+)['\"]"))
    assert.same('a/b', string.match("local .. 'a/b'", "[^'\"]['\"]([%w_/]+)['\"]"))

    local line1 = [[  local lua_root = uv.cwd() .. '/test/env/resources/lua/']]
    local line2 = [[  local busted_output1 = lua_root .. 'busted/output01.lua']]

    local pattern = "local [%w_]* = [^'\"]+['\"][%w%._/]+['\"]"
    local exp2 = [[local busted_output1 = lua_root .. 'busted/output01.lua']]
    assert.same(exp2, string.match(line2, pattern))

    local pattern2 = "local ([%w_%.]+) = [^'\"]+['\"]([%w%._/]+)['\"]"
    local var, relpath = string.match(line2, pattern2)
    assert.same('busted_output1', var)
    assert.same('busted/output01.lua', relpath)

    var = 'lua_root'
    local pattern_val_of_var = "^[%s]*local " .. var .. ' = ([^=]+)$'
    local exp1 = [[uv.cwd() .. '/test/env/resources/lua/']]
    assert.same(exp1, string.match(line1, pattern_val_of_var))
  end)

  it("parse_line_require", function()
    local cases = {
      -- exp,             input
      { 'env',            [[local M = require 'env' ]] },
      { 'env',            [[local M = require('env')]] },
      { 'env.lua_parser', [[local M = require 'env.lua_parser']] },
      { 'env.lua_parser', [[local M = require('env.lua_parser')]] },
      { 'env.lua_parser', [[local M = require("env.lua_parser")]] },
      { 'env.lua_parser', [[M = require('env.lua_parser')]] },
    }
    local i = 1
    for _, case in pairs(cases) do
      local vn, mn = M.parse_line_require(case[2])
      assert.same('M', vn)
      assert.same(case[1], mn)
      i = i + 1
    end
  end)


  it("parse_line_require 2", function()
    local f = M.parse_line_require
    local exp = { 'mod', 'ns.mod', '' }
    assert.same(exp, { f([[local mod = require("ns.mod")]]) })
    assert.same(exp, { f([[local mod = require('ns.mod')]]) })
    assert.same(exp, { f([[local mod = require"ns.mod"]]) })
    assert.same(exp, { f([[local mod = require'ns.mod']]) })
    assert.same(exp, { f([[local mod = require "ns.mod"]]) })
    assert.same(exp, { f([[local mod = require 'ns.mod']]) })
    assert.same(exp, { f([[local mod = require  'ns.mod']]) })
    assert.same(exp, { f([[local mod=require  'ns.mod']]) })
    assert.same(exp, { f([[local mod=require'ns.mod']]) })
    assert.same(exp, { f([[local mod  =   require   'ns.mod']]) })
    local exp1 = { 'mod', 'ns.mod', '.func()' }
    assert.same(exp1, { f([[local mod = require 'ns.mod'.func()]]) })
    local exp2 = { 'mod', 'ns.mod', '.func()' }
    assert.same(exp2, { f([[local mod = require('ns.mod').func()]]) })
    local exp3 = { 'mod', 'ns.mod', ' -- comment' }
    assert.same(exp3, { f([[local mod = require("ns.mod") -- comment]]) })

    local exp4 = { 'm', '', '' }
    assert.same(exp4, { f([[local m  =   require '']]) })
    assert.same(exp4, { f([[local m  =   require '']]) })
    assert.same(exp4, { f([[local m  =   require("")]]) })
    assert.same(exp4, { f([[local m  =   require '';]]) })
    assert.same({ 'm', 'x', '' }, { f([[local m  =   require 'x';]]) })
    assert.same({ 'm', 'x', '' }, { f([[local m  =   require 'x' ; ]]) })
    assert.same({ 'm', 'x', '' }, { f([[local m  =   require 'x' ;  ]]) })

    assert.same({ 'm', 'x', ';  -- comment' }, { f([[local m  =   require 'x';  -- comment]]) })
    assert.same({ 'm', 'x', '.call();' }, { f([[local m  =   require 'x'.call();]]) })

    assert.same({ 'm', 'x', '.call();' }, { f([[m  =   require 'x'.call();]]) })
    assert.same({ 'm', 'x', '.call();' }, { f([[m=require 'x'.call();]]) })
    assert.same({ 'm', 'x', '.call();' }, { f([[m=require'x'.call();]]) })
  end)

  it("parse_line_require 2", function()
    local f = M.parse_line_require
    local line = [[local fix_ok, FIX = pcall(require, 'env.bridges.smthg-fix')]]

    local exp = { 'FIX', 'env.bridges.smthg-fix', '' }
    assert.same(exp, { f(line) })

    local line2 = [[local ok, mod = pcall(require, 'ns.module') -- comment]]
    local exp2 = { 'mod', 'ns.module', '-- comment' }
    assert.same(exp2, { f(line2) })
  end)


  it("parse_assign_line 1", function()
    local line = [[  local var_name1 = lua_root .. 'subdir/file.lua']]

    local vn, exp, append = M.parse_line_assign_string(line)
    assert.same('var_name1', vn)
    assert.same('lua_root .. ', exp)
    assert.same('subdir/file.lua', append)
  end)

  it("parse_assign_line 2", function()
    local line = [[  local var_name1 = 'subdir/file.lua']]

    local vn, exp, append = M.parse_line_assign_string(line)
    assert.same('var_name1', vn)
    assert.same('', exp)
    assert.same('subdir/file.lua', append)
  end)

  it("parse_assign_line 3", function()
    local line = [[local path = uv.cwd() .. '/test/env/resources/lua/']]
    local vn, exp, append = M.parse_line_assign_string(line)
    assert.same('path', vn)
    assert.same('uv.cwd() .. ', exp)
    assert.same('/test/env/resources/lua/', append)
  end)

  it("parse_line_expr_append_string 1", function()
    local line = [[lua_root .. 'subdir/file.lua']]
    local exp, append = M.parse_line_expr_append_string(line)
    assert.same('lua_root .. ', exp)
    assert.same('subdir/file.lua', append)
  end)

  it("parse_line_expr_append_string 2", function()
    local line = [['subdir/file.lua']]
    local exp, append = M.parse_line_expr_append_string(line)
    assert.same('', exp)
    assert.same('subdir/file.lua', append)
  end)

  it("find_path_from_code with uv.cwd()", function()
    local line1 = [[  local lua_root = uv.cwd() .. '/test/env/resources/lua/']]
    local lines = { line1, '-- comment 1', '  --- comment 2' }
    local ctx, path, linenr = {}, nil, nil
    local callback = function(p, ln)
      path = p; linenr = ln
      return p ~= nil and ln ~= nil
    end

    nvim:new_buf(lines, 16, 1)

    assert.same(true, M.find_path_from_code(ctx, line1, callback))
    local exp = uv.cwd() .. '/test/env/resources/lua/'
    assert.same(exp, path)
    assert.same(1, linenr)
  end)

  it("find_path_from_code with uv.cwd()", function()
    local lines = {
      "--",
      "local luarocks_root = uv.cwd() .. '/test/env/resources/lua/luarocks/'",
      "local rockspec0 = 'mypkg-scm-1.rockspec'",
      "local full_rockspec0 = luarocks_root .. 'mypkg-scm-1.rockspec'", -- 4
    }
    local ctx, res_path, linenr = {}, nil, nil
    local callback = function(p, ln)
      res_path = p; linenr = ln
      return p ~= nil and ln ~= nil
    end

    nvim:new_buf(lines, 16, 1)

    local vn_path, expr, path0 = M.parse_line_assign_string(lines[4])
    assert.same('full_rockspec0', vn_path)
    assert.same('luarocks_root .. ', expr)
    assert.same('mypkg-scm-1.rockspec', path0)

    assert.same(true, M.find_path_from_code(ctx, lines[4], callback))
    local exp = uv.cwd() .. '/test/env/resources/lua/luarocks/mypkg-scm-1.rockspec'
    assert.same(exp, res_path)
    assert.same(1, linenr)
  end)

  it("find_path_from_code vn-ref", function()
    local line1 = [[  local lua_root = uv.cwd() .. '/test/env/resources/lua/']]
    local line2 = [[  local busted_output1 = lua_root .. 'busted/output01.lua']]
    local lines = { line1, line2, '-- comment 1', '  --- comment 2' }
    local ctx, path, linenr = {}, nil, nil
    local callback = function(p, ln)
      path = p; linenr = ln
      return p ~= nil and ln ~= nil
    end

    nvim:new_buf(lines, 16, 1)

    assert.same(true, M.find_path_from_code(ctx, line2, callback))
    local exp = uv.cwd() .. '/test/env/resources/lua/busted/output01.lua'
    assert.same(exp, path)
    assert.same(1, linenr)
  end)

  --
  it("resolve_strvar_value", function()
    local lines = {
      "local root = uv.cwd() .. '/test/env/resources/'",
      "  -- comment 1",
      "  local api_root = root .. 'auction/api/'",
      "  local pl = Lang:new():noCache():resolve(api_root)",
      "  -- comment 2",
      "  local user_id_src_path = api_root .. 'src/Auth/Entity/User/Id.ext'",
    }
    local exp = uv.cwd() .. '/test/env/resources/auction/api/'
    assert.same(exp, M.resolve_strvar_value(lines, 'api_root', 1))
  end)

  it("resolve_strvar_value 2", function()
    local lines = {
      "M.root = uv.cwd() .. '/test/env/resources/'",
      "  -- comment 1",
      "  M.api_root = M.root .. 'auction/api/'",
      "  M.pl = Lang:new():noCache():resolve(api_root)",
      "  -- comment 2",
      "  M.user_id_src_path = M.api_root .. 'src/Auth/Entity/User/Id.ext'",
    }
    local exp = uv.cwd() .. '/test/env/resources/auction/api/'
    assert.same(exp, M.resolve_strvar_value(lines, 'M.api_root', 1))
  end)

  it("parse_line_expr_class_method success", function()
    local line = " Lang:new():noCache():resolve(api_root)"
    local c, m = M.parse_line_expr_class_method(line)
    assert.same("Lang", c)
    assert.same("new", m)
  end)

  it("parse_line_expr_class_method success-2", function()
    local line = " Lang.staticMethod(params)"
    local c, m = M.parse_line_expr_class_method(line)
    assert.same("Lang", c)
    assert.same('staticMethod', m)
  end)

  it("parse_line_expr_class_method fail", function()
    local line = " lang:factory()" -- class name only from Upper letters
    local c, m = M.parse_line_expr_class_method(line)
    assert.is_nil(c)
    assert.is_nil(m)
  end)

  it("parse_line_expr_class_method", function()
    local line = "H.mkField('id','Id', H.AM.PRIVATE)"
    local c, m = M.parse_line_expr_class_method(line)
    assert.same('H', c)
    assert.same('mkField', m)
  end)

  it("find_module_namespace", function()
    local lines = { '--1', '--2',
      "local ClassGen = require('env.lang.oop.ClassGen')", --3
      "local cg = ClassGen:new({})"
    }
    local bufnr = nvim:new_buf(lines, #lines, 1)
    local ns, ln = M.find_module_namespace('ClassGen', bufnr, 0, #lines)
    assert.same("env.lang.oop.ClassGen", ns)
    assert.same(3 - 1, ln) -- (nvim index from 0)
  end)

  it("find_in_class_method_definition", function()
    local lines = {
      "---@class MehtodGen: ComponentGen",                       -- 1
      "---@field fields table<string, Field>  -- name -> Filed", -- 2
      "local MethodGen = ComponentGen:new({",                    -- 3
      " fields = nil,",                                          -- 4
      "})",                                                      -- 5
      "",                                                        -- 6
      "MethodGen.new = Object.new",                              -- 7
      "---@param m Method",                                      -- 8
      "---@return string",                                       -- 9
      "function MethodGen:genConstructorCode(m)",                -- 10
      "-- method body here",                                     -- 11
      "end",                                                     -- 12
      "...",                                                     -- 13
      "--- method to code",                                      -- 14
      "---@param m Method",                                      -- 15
      "function MethodGen:toCode(m)",                            -- 16
      "  return MethodGen:genConstructorCode(m)",                -- 17
      "end ",                                                    -- 18
      "return MethodGen",                                        -- 19
    }
    local bn, lnum, col = 32, #lines, 1
    nvim:new_buf(lines, lnum, col, bn)
    local scn, mn = "MethodGen", "genConstructorCode"
    local path, ln = M.find_in_class_method_definition(scn, mn, bn, 0, lnum)
    assert.same("function MethodGen:genConstructorCode(", path)
    assert.same(10 - 1, ln) -- ln from 0 (nvim) not from 1
  end)

  it("parse_function_definition 1", function()
    local m, f = M.parse_function_definition("function MethodGen:toCode(m)")
    assert.same("MethodGen", m)
    assert.same("toCode", f)
  end)

  it("parse_function_definition 2", function()
    local m, f = M.parse_function_definition("function M.func(m)")
    assert.same("M", m)
    assert.same("func", f)
  end)

  describe('env.lua_parser', function()
    -- local lines = {
    --   "local ClassGen = require('env.lang.oop.ClassGen')",   -- 1
    --   "local methodGen = MethodGen:new({ classinfo = ci })", -- 2
    --   "...",                                                 -- 3
    --   "  local cg = ClassGen:new({",                         -- 4
    --   "        classinfo = ci,",                             -- 5
    --   "        fields = fields,",                            -- 6
    --   "      })",                                            -- 7
    --   "      :addMethods(methodGen:genGetters(fields))",     -- 8
    --   "      :setOpts({ nodate = true, author = '' })",      -- 9
    --   --2345678901234567890123456789012345678901234567890
    --   --        10        20        30        40        50
    -- }
    -- todo for find_source_of_words
    -- extract class name for method cursor under 'addMethods'
    -- it("parse_method_call - case 3", function()
    --   local api = H.mk_vim_api(lines, 0, 0)
    --   -- assert.is_true(M.find_path_to_source_for(ctx, lines[7], callback))
    --   api.nvim_win_set_cursor(0, { 7, 36 })
    --   assert.same(lines[7], api.nvim_get_current_line())
    --
    --   local line = 'local a = A:new({b=c})'
    --   assert.same(true, string.match(line, "^[%s]*local a[%s]*=[%s]*(.*)$"))
    --
    --   local varname = 'methodGen'
    --   local p_find_value_of_var = "^[%s]*local " .. varname .. '[%s]*=[%s]*([.]+)$'
    --   local expr = M.find_var_definition(lines, varname)
    --   assert.same(true, string.match(lines[2], p_find_value_of_var))
    --
    --   assert.same("x", expr)
    --   -- M.find
    --   -- assert.same("/tmp/env/lang/oop/ClassGen.lua, path)
    --   -- assert.same("ClassGen:staticMethod(", linenr)
    -- end)
    --
    it("find_var_definition", function()
      local lines = {
        "local ClassGen = require('env.lang.oop.ClassGen')", -- 1
        "...",                                               -- 2
        "  local cg = ClassGen:new({",                       -- 3
        "        classinfo = ci,",                           -- 4
      }
      local bufnr, lnum, col = 4, #lines, 1
      nvim:new_buf(lines, lnum, col, bufnr)

      local vn = "cg" -- varname
      local res, s, e = M.find_var_definition(vn, bufnr, 0, lnum)
      assert.same('ClassGen:new({', res)
      assert.same(2, s)
      assert.same(2, e)

      vn = "ClassGen"
      local res2, s2, e2 = M.find_var_definition(vn, bufnr, 0, lnum)
      assert.same('require(\'env.lang.oop.ClassGen\')', res2)
      assert.same(0, s2)
      assert.same(0, e2)
    end)

    it("find_var_definition multiline", function()
      local pattern = "^[%s]*local[%s]+([%w_%.]+)[%s]*=[%s]*(.+)$"
      local line = "  local line = [["
      assert.same('line', line:match(pattern))
    end)

    it("find_var_definition multiline", function()
      local lines = {
        "  --",              -- 1
        "  local a = [[",    -- 2
        "  abc",             -- 3
        "]]",                -- 4
        "  local b = 'zzz'",
        "  local line = [[", -- 6
        "  zzz",             -- 7
        "]]",                -- 8
        "  func(a)"          -- 9
      }
      local lnum, col = #lines - 1, 1
      local bufnr = nvim:new_buf(lines, lnum, col)
      local varname = "a"
      local res, ls, le = M.find_var_definition(varname, bufnr, 0, lnum, true)
      assert.same(1, ls) -- line number where definition starts
      assert.same(3, le) -- lnum is where the variable declaration ends
      assert.same("[[\n  abc\n]]", res)
    end)

    it("find_var_definition fail diff scope", function()
      local lines = {
        "  it('name-1', function()",   -- 1
        "    local exp = 'abc'",       -- 2  -- not passed - other scope!
        "    assert.same(exp, 'abc')", -- 3
        "  end)",                      -- 4  -- stop! out of scope
        "",                            -- 5
        "  it('name-2', function()",   -- 6
        "    local res = 'cba'",       -- 7
        "    assert.same(exp, res)",   -- 8  -- ^^ start searching point. go up
        "  end)",                      -- 9
      }
      local lnum, col = 7, 1
      local bufnr = nvim:new_buf(lines, lnum, col)
      local varname = "exp"
      local res, ls, le = M.find_var_definition(varname, bufnr, 0, lnum, true)
      assert.is_nil(ls)
      assert.is_nil(le)
      assert.is_nil(res)
    end)

    it("find_var_definition success - scope", function()
      local lines = {
        "  it('name-1', function()",   -- 1
        "    local exp = 'abc'",       -- 2
        "    assert.same(exp, 'abc')", -- 3
        "  end)",                      -- 4
        "",                            -- 5
        "  it('name-2', function()",   -- 6
        "    local exp = 'abc'",       -- 7  -- stop -> passed - same scope
        "    local res = 'cba'",       -- 8
        "    assert.same(exp, res)",   -- 9  -- ^^ start searching point. go up
        "  end)",                      -- 10
      }
      local lnum, col = 8, 1
      local bufnr = nvim:new_buf(lines, lnum, col)
      local varname = "exp"
      local res, ls, le = M.find_var_definition(varname, bufnr, 0, lnum, true)
      assert.same(6, ls)
      assert.same(6, le)
      assert.same("'abc'", res)
    end)

    it("find_var_definition success - scope", function()
      local lines = {
        "  it('name-1', function()",   -- 1
        "    local exp = 'abc'",       -- 2
        "    assert.same(exp, 'abc')", -- 3
        "  end)",                      -- 4
        "",                            -- 5
        "  it('name-2', function()",   -- 6
        "    local exp = 'abc'",       -- 7  -- stop -> passed - same scope
        "    local res = [[",          -- 8
        "  end)",                      -- 9 string not code!
        "]]",                          -- 10
        "    assert.same(exp, res)",   -- 11  -- ^^ start searching point. go up
        "  end)",                      -- 12
      }
      local lnum, col = #lines - 2, 1
      local bufnr = nvim:new_buf(lines, lnum, col)
      local varname = "exp"
      local res, ls, le = M.find_var_definition(varname, bufnr, 0, lnum, true)
      assert.same(6, ls)
      assert.same(6, le)
      assert.same("'abc'", res)
    end)
  end)
end)


local failure_message_1 = [[
Failure:
  ./test/env/lua_parser_spec.lua:238: Expected objects to be the same.
Passed in:
(string) 'A:new({b=c})'
Expected:
(boolean) true

stack traceback:
	./test/env/lua_parser_spec.lua:238: in function <./test/env/lua_parser_spec.lua:231>
]]

describe('env.lua_parser', function()
  it("regext 1", function()
    local line = "Failure: Passed in: abc\nExpected: true\nstack traceback"
    local pattern = "^Failure:.*Passed in:(.*)Expected:(.*)stack traceback"
    local passedin, expected = line:match(pattern)
    assert.same(" abc\n", passedin)
    assert.same(" true\n", expected)
  end)

  it("parse_failure_pass_exp", function()
    local p, e = M.parse_failure_pass_exp(failure_message_1)
    local exp1 = "(string) 'A:new({b=c})'"
    local exp2 = "(boolean) true"
    assert.same(exp1, p)
    assert.same(exp2, e)
  end)

  it("parse_test_assert - 1", function()
    local line = "  assert.same(exp, res)"
    local f, a1, a2 = M.parse_test_assert(line)
    assert.same("same", f)
    assert.same("exp", a1)
    assert.same("res", a2)
  end)

  it("parse_test_assert - 2", function()
    local line = "  assert.same({'a','b'}, res)"
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same("same", func)
    assert.same("{'a','b'}", a1)
    assert.same("res", a2)
  end)

  it("parse_test_assert - 3", function()
    local line = "  assert.same('a,b', res_ult)"
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same("same", func)
    assert.same("'a,b'", a1)
    assert.same("res_ult", a2)
  end)

  it("parse_test_assert - 4", function()
    local line = '  assert.same("a,b", res.val)'
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same("same", func)
    assert.same('"a,b"', a1)
    assert.same("res.val", a2)
  end)

  it("parse_test_assert - 5", function()
    local line = '  assert.is_true(res)'
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same("is_true", func)
    assert.same('res', a1)
    assert.is_false(a2)
  end)

  it("parse_test_assert - 6", function()
    local line = '  assert.is_nil(res)'
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same("is_nil", func)
    assert.same('res', a1)
    assert.is_false(a2)
  end)

  it("parse_test_assert - 7", function()
    local line = '  assert(res)'
    local func, a1, a2 = M.parse_test_assert(line)
    assert.is_nil(func)
    assert.is_nil(a1)
    assert.is_nil(a2)
  end)

  it("parse_test_assert - 8", function()
    local line = "  assert.has_no.errors(func_arg)"
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same('has_no.errors', func)
    assert.same('func_arg', a1)
    assert.is_false(a2)
  end)

  it("parse_test_assert - 9", function()
    local line = [[assert.same(exp, pg:getConstructorCode(ci, opts))]]
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same('same', func)
    assert.same('exp', a1)
    assert.same('pg:getConstructorCode(ci, opts)', a2)
  end)

  it("parse_test_assert - 10", function()
    -- local line = "  assert.has_no.errors(function() end)"
    local line = [[assert.is_nil(pg:getConstructorCode(ci, opts))]]
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same('is_nil', func)
    assert.same('pg:getConstructorCode(ci, opts)', a1)
    assert.same(false, a2)
  end)

  it("parse_test_assert - 11", function()
    local line = "  assert.same(3, le)"
    local func, a1, a2 = M.parse_test_assert(line)
    assert.same('same', func)
    assert.same('3', a1)
    assert.same('le', a2)
  end)

  it("parse_test_assert - 12", function()
    local line = "  assert.same(3, le) -- comment"
    local func, a1, a2, comment = M.parse_test_assert(line)
    assert.same('same', func)
    assert.same('3', a1)
    assert.same('le', a2)
    assert.same(' -- comment', comment)
  end)

  --

  it("parse_line_call_dyn_method", function()
    local instance, method = M.parse_line_call_dyn_method('pl:getLangGen()')
    assert.same('pl', instance)
    assert.same('getLangGen', method)
  end)

  it("parse_line_call_dyn_method", function()
    local instance, method = M.parse_line_call_dyn_method('pl:m1():m2()')
    assert.same('pl', instance)
    assert.same('m1', method)
  end)

  it("parse_line_call_dyn_method fail", function()
    local instance, method = M.parse_line_call_dyn_method('pl:getLangGen')
    assert.is_nil(instance)
    assert.is_nil(method)
  end)
end)

describe('env.lua_parser update_line_from_passedin', function()
  local function mkDiag(bufnr, lnum, message)
    return {
      namespace = busted0.NAMESPACE,
      severity = 2,
      bufnr = bufnr,
      lnum = lnum - 1, -- to nvim inner lines indexes from 0
      end_lnum = lnum - 1,
      col = busted0.IN_ROW_COL_START,
      end_col = busted0.IN_ROW_COL_END,
      message = message,
    }
  end

  local function mkFailMsg(passed, expected)
    return
        "Failure:\n./test/A_spec.lua:16: Expected objects to be the same.\n" ..
        "Passed in:\n" .. passed .. "\n" ..
        "Expected:\n" .. expected .. "\n\n" ..
        "stack traceback:\n" ..
        "\t./test/env/A_spec.lua:12: in function <./test/env/A_spec.lua:12>"
  end

  it("same", function()
    local lines = {
      "",                                                      --1
      [[  local line = 'local a = A:new({b=c})']],             --2
      [[  assert.same(true, string.match(line, "^([%w])$"))]], --3 << currenct
      --012345678901234567890
      --          10        20
      "", --4
    }
    local lnum, col = 3, 16
    local bufnr = nvim:new_buf(lines, lnum, col) -- TH.logger_setup()

    local d = mkDiag(bufnr, lnum, failure_message_1)
    assert.same(lines[lnum], api.nvim_get_current_line())

    local func, arg1, arg2 = M.parse_test_assert(lines[lnum])
    assert.same('same', func)
    assert.same('true', arg1)
    assert.same('string.match(line, "^([%w])$")', arg2)

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    -- local exp = '  assert.same(true, string.match(line, "^[%s]*local a[%s]*=[%s]*(.*)$"))'
    local exp = [[  assert.same('A:new({b=c})', string.match(line, "^([%w])$"))]]
    -- NOTE:                     ^was true, but the match returns a specific string value
    assert.same(exp, res)
    assert.same(exp, api.nvim_get_current_line())
  end)

  --
  it("is_nil->same", function()
    local lines = { "  assert.is_nil(1)" }
    local bufnr, lnum, col = 4, 1, 16
    nvim:new_buf(lines, lnum, col) -- TH.logger_setup()
    local d = mkDiag(bufnr, lnum, mkFailMsg('(number) 1', 'type nil'))

    local func, arg1, arg2, comment = M.parse_test_assert(lines[lnum])
    assert.same('is_nil', func)
    assert.same('1', arg1)
    assert.is_false(arg2)
    assert.is_nil(comment)

    assert.is_not_nil(d.message)
    local pass, exp = M.parse_failure_pass_exp(d.message)
    assert.same('(number) 1', pass)
    assert.same('type nil', exp)

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    assert.same('  assert.same(1, 1)', res)
  end)

  --
  it("same->is_nil", function()
    local lines = { "  assert.same('abc', res2)" } -- res2 == nil
    local bufnr, lnum, col = 4, 1, 16
    nvim:new_buf(lines, lnum, col)
    local d = mkDiag(bufnr, lnum, mkFailMsg('(nil)', '(string) \'abc\''))

    local func, arg1, arg2 = M.parse_test_assert(lines[lnum])
    assert.same('same', func)
    assert.same("'abc'", arg1)
    assert.same('res2', arg2)

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    assert.same('  assert.is_nil(res2)', res)
  end)

  --
  it("is_nil->is_false", function()
    local lines = { "  assert.is_nil(res)" } -- res == false
    local bufnr, lnum, col = 4, 1, 16
    nvim:new_buf(lines, lnum, col)
    -- TH.logger_setup()
    local d = mkDiag(bufnr, lnum, mkFailMsg('(boolean) false', 'type nil'))

    local func, arg1, arg2 = M.parse_test_assert(lines[lnum])
    assert.same('is_nil', func)
    assert.same('res', arg1)
    assert.is_false(arg2)

    local pass, exp = M.parse_failure_pass_exp(d.message)
    assert.same('(boolean) false', pass)
    assert.same('type nil', exp)

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    assert.same('  assert.is_false(res)', res)
  end)

  it("is_nil->is_false", function()
    local lines = { "  assert.is_nil(res)" } -- res == false
    local bufnr, lnum, col = 4, 1, 16
    nvim:new_buf(lines, lnum, col)
    -- TH.logger_setup()
    local d = mkDiag(bufnr, lnum, mkFailMsg('(boolean) true', 'type nil'))
    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    assert.same('  assert.is_true(res)', res)
  end)

  it("is_nil->same + value", function()
    local lines = { "assert.is_nil(M._get_right_order('name', nil))" }
    local bufnr, lnum, col = 4, 1, 16
    nvim:new_buf(lines, lnum, col)
    --
    local func, arg1, arg2 = M.parse_test_assert(lines[lnum])
    assert.same('is_nil', func)
    assert.same('M._get_right_order(\'name\', nil)', arg1)
    assert.is_false(arg2)

    local d = mkDiag(bufnr, lnum, mkFailMsg("(string) 'name'", 'type nil'))
    local pass, exp = M.parse_failure_pass_exp(d.message)
    assert.same('(string) \'name\'', pass)
    assert.same('type nil', exp)
    local p_type, p_value = M.parse_to_type_value(pass)
    assert.same("string", p_type)
    assert.same("name", p_value)
    -- TH.logger_setup()

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    exp = [[assert.same('name', M._get_right_order('name', nil))]]
    assert.same(exp, res)
  end)

  it("patch multiline to multiline", function()
    local bufnr, lnum, col = 44, 7, 16
    local lines = {
      ' --',                        -- 1
      '    local exp = [[',         -- 2
      'Usage:',                     -- 3
      '    2 scmd - desc1',         -- 4
      '   sn fname - desc2',        -- 5
      ']]',                         -- 6
      '    assert.same(exp, help)', -- 7
      '--',                         -- 8
    }

    local passedin = "(string) 'Usage:\n" ..
        "    2 scmd    -  desc 1\n   sn fname   -  desc 2\n'"
    local d = mkDiag(bufnr, lnum, mkFailMsg(passedin, 'type nil'))
    nvim:new_buf(lines, lnum, col, bufnr)

    -- TH.logger_setup()
    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    local exp = "    assert.same(exp, help)"
    assert.same(exp, res)
    local exp2 = {
      ' --',
      '    local exp = [[',
      'Usage:',
      '    2 scmd    -  desc 1',
      '   sn fname   -  desc 2',
      ']]',
      '    assert.same(exp, help)',
      '--',
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("patch one-line to multiline exp definition", function()
    local bufnr, lnum, col = 44, 3, 16
    local lines = {
      ' --',                        -- 1
      '    local exp = "abc"',      -- 2
      '    assert.same(exp, help)', -- 3
      '--',                         -- 4
    }

    local passedin = "(string) 'Usage:\n" ..
        "    2 scmd    -  desc 1\n   sn fname   -  desc 2\n'"
    local d = mkDiag(bufnr, lnum, mkFailMsg(passedin, 'type nil'))
    nvim:new_buf(lines, lnum, col, bufnr)

    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    local exp = "    assert.same(exp, help)"
    assert.same(exp, res)
    local exp2 = {
      ' --',
      '    local exp = [[',
      'Usage:',
      '    2 scmd    -  desc 1',
      '   sn fname   -  desc 2',
      ']]',
      '    assert.same(exp, help)',
      '--',
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("patch multiline to one-line", function()
    local bufnr, lnum, col = 44, 7, 16
    local lines = {
      ' --',                        -- 1
      '    local exp = [[',         -- 2
      'Usage:',                     -- 3
      '    2 scmd - desc1',         -- 4
      '   sn fname - desc2',        -- 5
      ']]',                         -- 6
      '    assert.same(exp, help)', -- 7
      '--',                         -- 8
    }

    local d = mkDiag(bufnr, lnum, mkFailMsg("(string) 'abc'", 'type nil'))
    nvim:new_buf(lines, lnum, col, bufnr)

    -- TH.logger_setup()     -- print debug messages to stdout
    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    local exp = "    assert.same(exp, help)"
    assert.same(exp, res)
    local exp2 = {
      ' --',
      "    local exp = 'abc'",
      '    assert.same(exp, help)',
      '--',
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("patch multiline to one-line with middle code lines", function()
    local bufnr, lnum, col = 44, 8, 16
    local lines = {
      ' --',                        -- 1
      '    local exp = [[',         -- 2
      'Usage:',                     -- 3
      '    2 scmd - desc1',         -- 4
      '   sn fname - desc2',        -- 5
      ']]',                         -- 6
      '    some code here',         -- 7 -- middle
      '    assert.same(exp, help)', -- 8
      '--',                         -- 9
    }

    local d = mkDiag(bufnr, lnum, mkFailMsg("(string) 'abc'", 'type nil'))
    nvim:new_buf(lines, lnum, col, bufnr)

    -- TH.logger_setup()
    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    local exp = "    assert.same(exp, help)"
    assert.same(exp, res)
    local exp2 = {
      ' --',
      "    local exp = 'abc'",
      '    some code here',
      '    assert.same(exp, help)',
      '--',
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("patch multiline to one-line FIXME", function()
    local bufnr, lnum, col = 44, 8, 16
    local lines = {
      '    local exp = [[',        -- 1
      'Usage:',                    -- 2
      '    2 scmd - desc1',        -- 3
      '   sn fname - desc2',       -- 4
      ']]',                        -- 5
      'end)',                      -- 6
      '    res = func_b()',        -- 7
      '    assert.same(exp, res)', -- 8
      '--',                        -- 9
    }

    local d = mkDiag(bufnr, lnum, mkFailMsg("(string) 'abc'", 'type nil'))
    nvim:new_buf(lines, lnum, col, bufnr)

    -- TH.logger_setup()
    local res = M.update_line_from_passedin(bufnr, lnum, col, d, ns_id)
    local exp = '    assert.same(exp, res)'
    assert.same(exp, res)
    local exp2 = {
      '    local exp = [[',
      'Usage:',
      '    2 scmd - desc1',
      '   sn fname - desc2',
      ']]',
      'end)',
      '    res = func_b()',
      '    local exp = \'abc\'',
      '    assert.same(exp, res)',
      '--'
    }
    assert.same(exp2, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)
end)


describe('change_var_definition: ', function()
  --
  it("change_var_definition", function()
    local lines = { '--', 'local exp = [[', 'ab', 'c', ']]', 'func(exp,res)' }
    local bufnr, lnum, col = 4, 6, 1
    nvim:new_buf(lines, lnum, col, bufnr)
    assert.same(lines, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- works
    local update = { 'local exp = [[', 'AB', ']]' } -- data to patch lines

    local ls, le = M.change_var_definition(bufnr, #lines, 'exp', update)
    assert.same(1, ls)
    assert.same(4, le)
    local exp = { '--', 'local exp = [[', 'AB', ']]', 'func(exp,res)' }
    assert.same(exp, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("change_var_definition 2", function()
    local lines = { '--', 'local exp = [[', 'ab', 'c', ']]', 'func(exp,res)' }
    local bufnr, lnum, col = 4, 2, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    assert.same(lines, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    local patch = { 'local exp = [[', 'A', 'B', 'C', ']]' }
    assert.same(1, M.change_var_definition(bufnr, #lines, 'exp', patch))
    local exp = { '--', 'local exp = [[', 'A', 'B', 'C', ']]', 'func(exp,res)' }
    assert.same(exp, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("change_var_definition 3", function()
    local lines = { '--', 'local exp = [[', 'ab', 'c', ']]', 'func(exp,res)' }
    local bufnr, lnum, col = 4, 2, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    assert.same(lines, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    local patch = { 'local exp = "one"' }
    assert.same(1, M.change_var_definition(bufnr, #lines, 'exp', patch))
    local exp = { '--', 'local exp = "one"', 'func(exp,res)' }
    assert.same(exp, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("change_var_definition 4", function()
    local lines = { '--', '', 'local exp = "one"', 'func(exp,res)' }
    local bufnr, lnum, col = 4, 2, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    assert.same(lines, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    local patch = { 'local exp = "patched!"' }
    assert.same(2, M.change_var_definition(bufnr, #lines, 'exp', patch))
    local exp = { '--', '', 'local exp = "patched!"', 'func(exp,res)' }
    assert.same(exp, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  it("change_var_definition 5", function()
    local lines = { '--', '', 'local exp = "wrong"', 'f(exp,r)' }
    local bufnr, lnum, col = 4, 2, 16
    nvim:new_buf(lines, lnum, col, bufnr)
    assert.same(lines, api.nvim_buf_get_lines(bufnr, 0, -1, false))

    local patch = { 'local exp = [[', 'A', 'B', 'C', ']]' }
    assert.same(2, M.change_var_definition(bufnr, #lines, 'exp', patch))
    local exp = { '--', '', 'local exp = [[', 'A', 'B', 'C', ']]', 'f(exp,r)' }
    assert.same(exp, api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  -- it("find_var_definition", function()
  --   assert.same("", M.find_var_definition0())
  -- end)
  -- todo PassedIn + Did not expect:
  -- assert.is_not_nil(nil)
  -- assert.is_not_true(true)
  --[[
./test/env/lua_parser_spec.lua:509: Expected objects to be the same.
Passed in:
(nil)
Expected:
(string) 'abc'

    assert.same('abc', arg2) -- arg2 == nil
]]

  describe('str2code: ', function()
    after_each(function()
      D.disable()
    end)

    local cases = {
      -- input     exp1,   exp2-add_exp_var
      { '',       { "''", false } },
      { 'abc',    { "'abc'", false } },
      { 'a\nbc',  { '"a\\nbc"', false } },
      { 'a\n\nb', { '"a\\n\\nb"', false } }, -- keep empty line
      { '--------------------------------------------------',
        { "'--------------------------------------------------'", true } },
      --
      { '-\'-------------------------------------------------',
        { "\"-\'-------------------------------------------------\"", true } },
      --
      { '-"-------------------------------------------------',
        { "'-\"-------------------------------------------------'", true } },
      --
      { "---------------------------\n-----------------------",
        { "[[\n---------------------------\n-----------------------]]", true } },
      --
      { '"a"',              { '\'"a"\'', false } },
      { '"a\nb"',           { '"\\"a\\nb\\""', false } },
      --
      { "  :desc('Issue')", { "\"  :desc('Issue')\"", false } },
      { '\'a "b\'',         { "\"'a \\\"b'\"", false } },

    }
    for i, case in pairs(cases) do
      describe('case: ' .. i, function()
        it("input: [" .. case[1] .. "]", function()
          local input, exp = case[1], case[2]
          local value, add_exp_var = M.str2code(input)
          assert.same(exp[1], value)
          assert.same(exp[2], add_exp_var)
        end)
      end)
    end
  end)

  -- based on parse.Tokenizer
  describe('parse_busted_readable_table: ', function()
    -- busted passedin to code
    it("parse_busted_readable_table", function()
      local line = "{\n" ..
          " *[1] = 'ab'\n" ..
          "  [2] = 'cde'\n" ..
          "  [3] = 'fg' }\n"
      local texp = { 'ab', 'cde', 'fg' }
      local res = M.parse_busted_readable_table(line)
      assert.same(texp, res)
    end)

    it("parse_busted_readable_table 2", function()
      local line = "[1] = false "
      local k, v = line:match("^%s*%*?%[([%w_]+)%] = (.-)%s*$")
      assert.same("1", k)
      assert.same("false", v)

      assert.same({ 1 }, M.parse_busted_readable_table("{\n *[1] = 1 }"))
      assert.same({ 'a' }, M.parse_busted_readable_table("{\n *[1] = 'a' }"))
      assert.same({ false }, M.parse_busted_readable_table("{\n *[1] = false }"))
      assert.same({ true }, M.parse_busted_readable_table("{\n *[1] = true }"))

      local i = "{\n *[1] = 'abc'\n  [2] = 123\n  [3] = false\n  [4] = true }"
      assert.same({ "abc", 123, false, true }, M.parse_busted_readable_table(i))
    end)

    it("parse_busted_readable_table k=v", function()
      local line = "{\n" ..
          " *[name] = 'MyClass'\n" ..
          "  [package] = 'org.pkg' }\n"
      local texp = { name = 'MyClass', package = 'org.pkg' }
      local res = M.parse_busted_readable_table(line)
      assert.same(texp, res)
      ---@cast res table
      local s = M.table2code(res)
      local exp2 = "{ name = 'MyClass', package = 'org.pkg' }"
      assert.same(exp2, s)
    end)


    it("parse_busted_readable_table k=v nested", function()
      local stbl = [[
{
  [bufname] = '/tmp/src/Entity/User/Id.php'
  [bufnr] = 1
  [can_edit] = true
  [current_line] = '--'
  [cursor_col] = 1
  [cursor_pos] = {
    [1] = 1
    [2] = 1 }
  [cursor_row] = 1 }
]]

      local res = M.parse_busted_readable_table(stbl)
      D.disable()
      local exp = {
        bufname = '/tmp/src/Entity/User/Id.php',
        bufnr = 1,
        can_edit = true,
        current_line = '--',
        cursor_col = 1,
        cursor_pos = { 1, 1 },
        cursor_row = 1,
      }
      assert.same(exp, res)
      local exp2 = [[
{
  bufname = '/tmp/src/Entity/User/Id.php',
  bufnr = 1,
  can_edit = true,
  current_line = '--',
  cursor_col = 1,
  cursor_pos = { 1, 1 },
  cursor_row = 1
}]]
      assert.same(exp2, M.table2code(exp))
    end)

    it("busted_message_fix_quotes", function()
      local stbl =
          "{\n" ..
          "  [1] = ''\n" ..
          " *[2] = 'local x = y'\n" ..
          " *[2] = 'desc('show')'\n" ..
          " *[2] = 'desc(\"a\")'\n" ..
          " *[2] = 'desc(\"a'b\")'\n" ..
          "  [3] = 'cmd('dir', 'path')' }"
      local exp = [[
{
  [1] = ''
 *[2] = 'local x = y'
 *[2] = "desc('show')"
 *[2] = 'desc("a")'
 *[2] = "desc(\"a'b\")"
  [3] = "cmd('dir', 'path')" }
]]
      assert.same(exp, M.busted_message_fix_quotes(stbl))
    end)

    it("busted_message_fix_quotes userdate: (nil) (from cjson)", function()
      local s = [[
{
 *[1] = {
   *[createdAt] = userdata: (nil)
    [createdWho] = userdata: (nil)
    [id] = 1 } }
]]
      local exp = [[
{
 *[1] = {
   *[createdAt] = null
    [createdWho] = null
    [id] = 1 } }
]]
      assert.same(exp, M.busted_message_fix_quotes(s))
    end)

    it("parse_busted_readable_table", function()
      local stbl =
          "{\n" ..                                     -- 1
          "  [1] = ''\n" ..                            -- 2
          " *[2] = '    :desc('show directory')'\n" .. -- 3
          "  [3] = '    :cmd('dir', 'path')' }"        -- 4
      local res = M.parse_busted_readable_table(stbl)
      local exp = {
        '',
        "    :desc('show directory')",
        "    :cmd('dir', 'path')"
      }
      assert.same(exp, res)
    end)

    it("table2code", function()
      local t = { 'ab', 'cde', 'fg' }
      assert.same('{ \'ab\', \'cde\', \'fg\' }', M.table2code(t))

      assert.same('{}', M.table2code({}))
      assert.same('{ 1 }', M.table2code({ 1 }))
      assert.same('{ { 1 }, { \'a\' } }', M.table2code({ { 1 }, { 'a' } }))
    end)

    it("table2code", function()
      local t = { '1111111111111111111', '222222222222', '33333333333333333' }
      local exp = [[
{
  '1111111111111111111',
  '222222222222',
  '33333333333333333'
}]]
      assert.same(exp, M.table2code(t))
    end)
  end)


  it("parse_busted_readable_table string array list", function()
    local stbl =
        "{\n" ..
        " *[1] = {\n" ..
        "    [1] = 'ab'\n" ..
        "   *[2] = 'cd' } }\n"

    local res = M.parse_busted_readable_table(stbl)
    local exp = { { 'ab', 'cd' } }
    assert.same(exp, res)
  end)

  it("parse_busted_readable_table string array list", function()
    local exp = { { { 'a', 'b' } } }
    -- generate assert.same({}, exp)
    local stbl = [[
{
 *[1] = {
    [1] = {
      [1] = 'a'
      [2] = 'b' } } }
]]
    --      [3] = { ... more } } } }  -- on 3th level always hide to "... more" ?
    local res = M.parse_busted_readable_table(stbl)
    assert.same(exp, res)
  end)

  it("parse_busted_readable_table string array list", function()
    local stbl = [[
{
 *[1] = {
    [1] = 'ab'
   *[2] = 'cd' } }
]]
    local res = M.parse_busted_readable_table(stbl)
    assert.same({ { 'ab', 'cd' } }, res)
  end)

  it("parse_busted_readable_table string array list", function()
    local stbl = [[
{
 *[1] = {
    [1] = userdata: (nil)
    [2] = userdata: (nil) } }
]]
    local res = M.parse_busted_readable_table(stbl)
    assert.same({ { 'null', 'null' } }, res)
  end)


  it("parse_busted_readable_table string array list", function()
    local exp = { { { 'a', 'b', 'c', {}, 'e' }, 'f' }, 'g' }
    local stbl = [[
{
 *[1] = {
    [1] = {
      [1] = 'a'
      [2] = 'b'
      [3] = 'c'
      [4] = { }
      [5] = 'e' }
    [2] = 'f' }
  [2] = 'g' }
]]
    local res = M.parse_busted_readable_table(stbl)
    assert.same(exp, res)
  end)

  -- -- to generate busted output message with nested table
  -- it("end", function()
  --   local res = { { { 'xa', 'b', 'c', {}, 'e' }, 'f' }, 'g' }
  --   local exp = { { { 'a', 'b', 'c', {}, 'e' }, 'f' }, 'g' }
  --   assert.same(exp, res)
  -- end)

  it("get_tab_len", function()
    local len = M.get_tab_len
    assert.same(0, len(''))
    assert.same(1, len(' '))
    assert.same(2, len('  x'))
    assert.same(0, len(nil))
  end)

  it("find_comment_pos lua", function()
    assert.same(9, M.find_comment_pos("' -- ', --"))
    assert.same(4, M.find_comment_pos("\\' -- \\', --"))
    assert.same(nil, M.find_comment_pos("' -- '"))
    assert.same(3, M.find_comment_pos("- --' -- '"))
    assert.same(10, M.find_comment_pos('" \\" --" -- '))
    assert.same(10, M.find_comment_pos('[[ -- ]] -- '))
    assert.same(5, M.find_comment_pos('[=[ -- ]=] -- ')) -- todo fix
  end)

  it("find_comment_pos diff", function()
    assert.same(9, M.find_comment_pos("' -- ', --"))
    assert.same(9, M.find_comment_pos("' // ', //", "//"))
    assert.same(7, M.find_comment_pos("' # ', #", "#"))
    assert.same(9, M.find_comment_pos("' /* ', /*", "/*"))
  end)

  it("format_comments", function()
    local exp = { '.',
      'abc de -- 1',
      'abc    -- 2'
    }
    assert.same(exp, M.format_comments({ ".", "abc de -- 1", "abc -- 2" }))
  end)

  -----------------------------------------------------------------------------


  it("get_signature_of_method", function()
    local lines = {
      "--",                                   -- 0
      "function Class:method(a, b)",          -- 1
      "  return self.collector:has_one() or", -- 2
      "      or self.collector:has_two()",    -- 3
      "      or self.collector:has_three()",  -- 4
      "end", "",
    }
    local lnum, col = 3, 16
    local bufnr = nvim:new_buf(lines, lnum, col)
    -- D.enable()
    local class, method, params = M.get_signature_of_method(bufnr, lnum, col)
    assert.same('Class', class)
    assert.same('method', method)
    assert.same('a, b', params)
  end)
end)

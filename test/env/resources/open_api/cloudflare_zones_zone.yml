components:
    schemas:
        zones_identifier:
            description: Identifier
            example: 023e105f4ecef8ad9ca31a8372d0c353
            maxLength: 32
            type: string
        zones_paused:
            default: false
            description: |
                Indicates whether the zone is only using Cloudflare DNS services. A
                true value means the zone will not receive security or performance
                benefits.
            readOnly: true
            type: boolean
        zones_type:
            default: full
            description: |
                A full zone implies that DNS is hosted with Cloudflare. A partial zone is
                typically a partner-hosted zone or a CNAME setup.
            enum:
                - full
                - partial
                - secondary
            example: full
            type: string
        zones_zone:
            properties:
                account:
                    description: The account the zone belongs to
                    properties:
                        id:
                            $ref: '#/components/schemas/zones_identifier'
                        name:
                            description: The name of the account
                            example: Example Account Name
                            type: string
                    type: object
                activated_on:
                    description: |-
                        The last time proof of ownership was detected and the zone was made
                        active
                    example: "2014-01-02T00:01:00.12345Z"
                    format: date-time
                    nullable: true
                    readOnly: true
                    type: string
                created_on:
                    description: When the zone was created
                    example: "2014-01-01T05:20:00.12345Z"
                    format: date-time
                    readOnly: true
                    type: string
                development_mode:
                    description: |-
                        The interval (in seconds) from when development mode expires
                        (positive integer) or last expired (negative integer) for the
                        domain. If development mode has never been enabled, this value is 0.
                    example: 7200
                    readOnly: true
                    type: number
                id:
                    $ref: '#/components/schemas/zones_identifier'
                meta:
                    description: Metadata about the zone
                    properties:
                        cdn_only:
                            description: The zone is only configured for CDN
                            example: true
                            type: boolean
                        custom_certificate_quota:
                            description: Number of Custom Certificates the zone can have
                            example: 1
                            type: integer
                        dns_only:
                            description: The zone is only configured for DNS
                            example: true
                            type: boolean
                        foundation_dns:
                            description: The zone is setup with Foundation DNS
                            example: true
                            type: boolean
                        page_rule_quota:
                            description: Number of Page Rules a zone can have
                            example: 100
                            type: integer
                        phishing_detected:
                            description: The zone has been flagged for phishing
                            example: false
                            type: boolean
                        step:
                            example: 2
                            type: integer
                    type: object
                modified_on:
                    description: When the zone was last modified
                    example: "2014-01-01T05:20:00.12345Z"
                    format: date-time
                    readOnly: true
                    type: string
                name:
                    description: The domain name
                    example: example.com
                    maxLength: 253
                    pattern: ^([a-zA-Z0-9][\-a-zA-Z0-9]*\.)+[\-a-zA-Z0-9]{2,20}$
                    type: string
                name_servers:
                    description: The name servers Cloudflare assigns to a zone
                    example:
                        - bob.ns.cloudflare.com
                        - lola.ns.cloudflare.com
                    items:
                        format: hostname
                        type: string
                    readOnly: true
                    type: array
                original_dnshost:
                    description: DNS host at the time of switching to Cloudflare
                    example: NameCheap
                    maxLength: 50
                    nullable: true
                    readOnly: true
                    type: string
                original_name_servers:
                    description: Original name servers before moving to Cloudflare
                    example:
                        - ns1.originaldnshost.com
                        - ns2.originaldnshost.com
                    items:
                        format: hostname
                        type: string
                    nullable: true
                    readOnly: true
                    type: array
                original_registrar:
                    description: Registrar for the domain at the time of switching to Cloudflare
                    example: GoDaddy
                    nullable: true
                    readOnly: true
                    type: string
                owner:
                    description: The owner of the zone
                    properties:
                        id:
                            $ref: '#/components/schemas/zones_identifier'
                        name:
                            description: Name of the owner
                            example: Example Org
                            type: string
                        type:
                            description: The type of owner
                            example: organization
                            type: string
                    type: object
                paused:
                    $ref: '#/components/schemas/zones_paused'
                status:
                    description: The zone status on Cloudflare.
                    enum:
                        - initializing
                        - pending
                        - active
                        - moved
                    example: active
                    readOnly: true
                    type: string
                type:
                    $ref: '#/components/schemas/zones_type'
                vanity_name_servers:
                    description: An array of domains used for custom name servers.
                    example:
                        - ns1.example.com
                        - ns2.example.com
                    items:
                        format: hostname
                        maxLength: 253
                        type: string
                    type: array
                verification_key:
                    description: Verification key for partial zone setup.
                    example: 284344499-1084221259
                    readOnly: true
                    type: string
            required:
                - id
                - name
                - development_mode
                - owner
                - account
                - meta
                - name_servers
                - original_name_servers
                - original_registrar
                - original_dnshost
                - created_on
                - modified_on
                - activated_on
            type: object
        zones_websockets:
            allOf:
                - $ref: '#/components/schemas/zones_base'
                - properties:
                    id:
                        description: ID of the zone setting.
                        enum:
                            - websockets
                        example: websockets
                    value:
                        $ref: '#/components/schemas/zones_websockets_value'
            description: WebSockets are open connections sustained between ..
            title: WebSockets
            type: object
        zones_websockets_value:
            default: "off"
            description: Value of the zone setting.
            enum:
                - "off"
                - "on"
            type: string
        zones_base:
            properties:
                editable:
                    default: true
                    description: Whether or not this setting can be modified ..
                    enum:
                        - true
                        - false
                    readOnly: true
                    type: boolean
                id:
                    description: Identifier of the zone setting.
                    example: development_mode
                    type: string
                modified_on:
                    description: last time this setting was modified.
                    example: "2014-01-01T05:20:00.12345Z"
                    format: date-time
                    nullable: true
                    readOnly: true
                    type: string
                value:
                    description: Current value of the zone setting.
                    example: "on"
            required:
                - id
                - value

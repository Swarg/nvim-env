require 'busted.runner' ()
local assert = require('luassert')

-- package.path = package.path .. ';test/env/resources/lua/luarocks/src/?.lua' -
package.preload['mypkg'] = loadfile('test/env/resources/lua/luarocks/src/mypkg/init.lua')

local M = require("mypkg")

describe("env.langs.lua.LuaRocksBuilder", function()
  it("func", function()
    assert.same('0.1.0', M._VERSION)
  end)
end)

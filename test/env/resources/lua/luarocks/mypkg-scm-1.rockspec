---@diagnostic disable: lowercase-global
package = "mypkg"
version = "scm-1"
source = {
   url = "URL for source tarball"
}
description = {
   homepage = "a project homepage",
   license = "MIT"
}
dependencies = {
  'cmd4lua >= 0.4.3',
}
build = {
   type = "builtin",
   modules = {
      mypkg = "src/mypkg/init.lua"
   }
}

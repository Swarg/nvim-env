<?php

declare(strict_types=1);

namespace App\Auth\Test\Unit\Service;

use App\Auth\Service\Tokinizer;
use DateInterval;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

/**
 * @covers Tokinizer
 */
class TokinizerTest extends TestCase
{
    public function testSuccess(): void
    {
        $interval = new DateInterval('PT1H');
        $date = new DateTimeImmutable('+1 day');

        $tokinizer = new Tokinizer($interval);

        $token = $tokinizer->generate($date);

        self::assertEquals($date->add($interval), $token->getExpires());
    }
}

package app.versions;





public class Version$Metadata {
    private String id;
    private int totalSize;
    protected String name;
    protected long size;
    protected String path;
    protected String url;

    public String toString() {
        String var10000 = this.getId();// 16
        return "Version.Metadata(id=" + var10000 + ", totalSize=" + this.getTotalSize() + ", name=" + this.getName() + ", size=" + this.getSize() + ", path=" + this.getPath() + ", url=" + this.getUrl() + ")";
    }
    public String getId() {        return this.id;/*19*/     }
    public int getTotalSize() {         return this.totalSize;/*20*/     }
    public String getName() {         return this.name;/*21*/     }
    public long getSize() {         return this.size;/*22*/     }
    public String getPath() {         return this.path;/*23*/     }
    public String getUrl() {         return this.url;// 24
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean equals(Object o) {
        Version$Metadata other = (Version$Metadata)o;
        if (this.getId() == null) {
        } else if (!this.getId().equals(other.getId())) {
            return false;
        }
        if (this.getName() == null) {
        } else if (!this.getName().equals(other.getName())) {
            return false;
        }
        if (this.getPath() == null) {
        } else if (!this.getPath().equals(other.getPath())) {
            return false;
        }
        if (this.getUrl() == null) {
        } else if (!this.getUrl().equals(other.getUrl())) {
            return false;
        }
        return true;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Version$Metadata;
    }

    public int hashCode() {
        int result = this.getTotalSize() * 59 + (int)($size >>> 32 ^ $size);
        Object id = this.getId();
        result = result * 59 + (id == null ? 43 : id.hashCode());
        Object $name = this.getName();
        result = result * 59 + (name == null ? 43 : name.hashCode());
        Object $path = this.getPath();
        result = result * 59 + (path == null ? 43 : path.hashCode());
        Object $url = this.getUrl();
        result = result * 59 + (url == null ? 43 : url.hashCode());
        return result;
    }

}
package pkg.model;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import pkg.jcmd.Parameter;
public class AppConfig {
    private static final String APP_CONFIG = "appConfig.json";
    public static final String APP_CHANGES_LOG = "changes.log";
    public static final List<String> URI_APP_CONFIG = Lists.newArrayList(new String[]{"https://repo.example.org/sources/prod/","https://repo.exmaple.com/sources/prod/"});
    private final boolean prod = true;
    @Parameter(names = {"-memory"},description = "The size of the required free disk space to download the application")
    private long minMemorySize;     @Parameter(names = {"-logLevel"},description = "Change log level, TRACE, DEBUG, INFO")     private String logLevel;     @Parameter(names = {"-stop"},description = "Argument to stop the application")     private boolean stop;     public static final AppConfig DEFAULT_CONFIG;
    public String toString() {        return "AppConfig(prod=" + this.isProd() + ", minMemorySize=" + this.getMinMemorySize() + ", logLevel=" + this.getLogLevel() + ", stop=" + this.isStop() + ")";/*15*/     }
    public AppConfig(long minMemorySize, String logLevel, boolean stop) {
        this.minMemorySize = minMemorySize;// 17
        this.logLevel = logLevel;
        this.stop = stop;
    }

    public long getMinMemorySize() {
        return this.minMemorySize;// 23
    }











    public String getServerFileConfig(AppConfig config, String version) {
        return Objects.isNull(version) ? String.join("/", "appConfig.json") : String.join("/", version, "appConfig.json");// 37
    }







    public String getLogLevel() {        return this.logLevel;/*46*/     }
    public boolean isStop() {
        return this.stop;// 48
    }



    static {
        DEFAULT_CONFIG = new AppConfig(500L, "INFO", false); // 54 55 58
    }// 53

    public boolean isProd() {
        this.getClass();
        return prod;
    }

    public void setMinMemorySize(long minMemorySize) {
        this.minMemorySize = minMemorySize;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof AppConfig)) {
            return false;
        } else {
            AppConfig other = (AppConfig)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.isProd() != other.isProd()) {
                return false;
            } else if (this.getMinMemorySize() != other.getMinMemorySize()) {
                return false;
            } else {
                Object this$logLevel = this.getLogLevel();
                Object other$logLevel = other.getLogLevel();
                if (this$logLevel == null) {
                    if (other$logLevel != null) {
                        return false;
                    }
                } else if (!this$logLevel.equals(other$logLevel)) {
                    return false;
                }
                if (this.isStop() != other.isStop()) {
                    return false;
                }
                return true;
            }
        }
    }

    protected boolean canEqual(Object other) {
        return other instanceof AppConfig;
    }

    public int hashCode() {
        int PRIME = 59;
        int result = 1;
        result = result * 59 + (this.isProd() ? 79 : 97);
        long $minMemorySize = this.getMinMemorySize();
        result = result * 59 + (int)($minMemorySize >>> 32 ^ $minMemorySize);
        Object $logLevel = this.getLogLevel();
        result = result * 59 + ($logLevel == null ? 43 : $logLevel.hashCode());
        result = result * 59 + (this.isStop() ? 79 : 97);
        return result;
    }

}

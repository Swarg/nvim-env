package app.launcher.;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;











@Compoment
public class Launcher {
    private static volatile Launcher INSTANCE;
    private static File directory;
    private static Gson gson;
    private static InnerConfiguration innerSettings;
    private static String family;
    private static Injector injector;
    @Named("console")
    @Inject
    private Console console;
    private final AdditionalAssetsComponent additionalAssetsComponent;
    @Inject
    private LauncherFactory launcherFactory;     private boolean ready;
    @Inject     public Launcher(@Assisted Configuration configuration) throws Exception {
        this.configuration = configuration;// 34
        INSTANCE = this;// 35
        gson = new Gson();// 36
        this.manager = new ComponentManager(this);// 37
    }
    public static String getFamily() {
        return family;// 40
    }

    public static void main(String[] args) {         try {
            OptionSet set = ArgumentParser.parseArgs(args);// 44
            Configuration configuration = Configuration.createConfiguration(set);// 45
            launch(configuration);// 46
            Configuration c = getInstance().getConfiguration();// 47
            c.set("memory.problem.message", null, false);// 48
        } catch (Throwable ex) {// 49
            U.log(ex);/*50*/         }
    }// 51

    private static void launch(Configuration configuration) {
        GuiceModule guiceModule = new GuiceModule(configuration, innerSettings);// 54
        injector = Guice.createInjector(new Module[]{guiceModule});// 55
        guiceModule.setInjector(injector);// 56
        Launcher t = (Launcher)injector.getInstance(Launcher.class);// 57
        t.init();// 58
    }// 59



}
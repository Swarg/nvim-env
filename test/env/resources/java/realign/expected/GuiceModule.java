package org.app.util.guice;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.regex.Pattern;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Named;
import com.google.inject.name.Names;






@Compoment
public class GuiceModule extends AbstractModule {
    private final Configuration configuration;
    private Injector injector;
    private boolean usedDefaultChooser = true;
    @FieldAnnotation
    private int field;

    public Injector getInjector() {
        return this.injector;// 32
    }

    public void setInjector(Injector injector) {
        this.injector = injector;// 36
    }// 37


    public GuiceModule(Configuration configuration, InnerConfiguration inner) {
        this.configuration = configuration;// 41
        this.inner = inner;// 42
        try {
            new FileExplorer();

        } catch (Throwable var4) {// 46
            U.log(new Object[]{"problem with standard FileExplorer"});// 47
            this.usedDefaultChooser = false;// 48
        }
    }// 50

    protected void configure() {
        this.bindListener(Matchers.any(), new $1(this));// 53
        this.bind(Gson.class).annotatedWith(Names.named("GsonCompleteVersion")).toInstance(this.createGsonCompleteVersion());// 54
        this.bind(Gson.class).annotatedWith(Names.named("GsonAdditionalFile")).toInstance(this.createAddittionalFileGson());// 55
        this.bind(InnerMinecraftServer.class).to(InnerMinecraftServersImpl.class);// 56
        this.bind(Console.class).annotatedWith(Names.named("console")).toInstance(new Console());// 57
        Class<? extends FileChooser> c = FileExplorer.class;// 58
        if (!this.usedDefaultChooser) {// 59
            c = FileWrapper.class;// 60
        }
        this.bind(FileChooser.class).to(c);// 62
        this.install((new FactoryModuleBuilder()).implement(SoundAssist.class, SoundAssist.class).build(SoundAssistFactory.class));// 63 64

        this.install((new FactoryModuleBuilder()).implement(AdditionalFileAssistance.class, AdditionalFileAssistance.class).build(AdditionalFileAssistanceFactory.class));// 65 66

    }// 67


    @Provides
    @Singleton
    public FileCacheService getFileCacheService() {
        return new FileCacheServiceImpl(this.getHttpService(), this.getGson(), StandardCharsets.UTF_8, getLauncherFile("cache").toPath(), this.inner.getInteger("file.cache.service.time.to.life"), this.internetServerMap);// 73 74
    }

    @Provides
    @Singleton
    public Gson getGson() {
        GsonBuilder builder = (new GsonBuilder()).setPrettyPrinting();// 79
        return builder.create();// 80
    }
    @Provides
    @Singleton
    public FileMapperService createFileMapperService() {
        return new FileMapperService(this.getGson(), StandardCharsets.UTF_8, MinecraftUtil.getWorkingDirectory().getAbsolutePath());// 85 86
    }










    private Gson createGsonCompleteVersion() {
        GsonBuilder builder = new GsonBuilder();// 98
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());// 99
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());// 100



        builder.registerTypeAdapter(MapDTO.class, new MapDTOTypeAdapter());// 104

        builder.enableComplexMapKeySerialization();// 106
        builder.setPrettyPrinting();// 107
        builder.disableHtmlEscaping();// 108
        return builder.create();// 109
    }

    private Gson createAddittionalFileGson() {
        GsonBuilder builder = new GsonBuilder();// 113
        builder.registerTypeAdapter(Pattern.class, new PatternTypeAdapter());// 114
        builder.setPrettyPrinting();// 115
        return builder.create();// 116
    }

    private Gson createProfileGson() {
        GsonBuilder builder = new GsonBuilder();// 120
        builder.registerTypeAdapterFactory(new LowerCaseEnumTypeAdapterFactory());// 121
        builder.registerTypeAdapter(Date.class, new DateTypeAdapter());// 122
        builder.registerTypeAdapter(File.class, new FileTypeAdapter());// 123
        builder.registerTypeAdapter(AuthenticatorDatabase.class, new AuthenticatorDatabase.Serializer());// 124
        builder.registerTypeAdapter(UUIDTypeAdapter.class, new UUIDTypeAdapter());// 125
        builder.setPrettyPrinting();// 126
        return builder.create();// 127
    }

    @Provides
    @Singleton
    private RequestConfig getRequestConfig() {
        return RequestConfig.custom().setConnectTimeout(60000).setSocketTimeout(60000).build();// 133
    }

    @Provides
    @Singleton
    CloseableHttpClient getHttpClient() {
        return HttpClientConfig.getInstanceHttpClient();// 139
    }

    @Provides
    @Singleton
    public HttpService getHttpService() {
        return new HttpServiceImpl((String)null, this.getHttpClient(), this.getRequestConfig(), this.internetServerMap.values().stream().anyMatch((e) -> e) ? this.inner.getInteger("max.attempts.per.request") : 1);// 145 146 147
    }



    @Provides
    @Singleton
    public GsonService getGsonService() {
        return new GsonServiceImpl(this.createGsonCompleteVersion(), this.getFileCacheService(), this.getHttpService(), this.internetServerMap);// 153
    }








    @Named("profileFileMapperService")
    @Provides
    @Singleton
    public FileMapperService createFileMapperService1() {
        return new FileMapperService(this.createProfileGson(), StandardCharsets.UTF_8, MinecraftUtil.getWorkingDirectory().getAbsolutePath());// 167 168
    }


    @Named("anyVersionType")
    @Provides
    @Singleton
    public NameIdDTO createAnyVersionType() {
        return new NameIdDTO(0L, "any");// 175
    }
    public void lastMethod() {
    }// 178


    @Named("anyObject")
    @Singleton
    public NameIdDTO methodWithOutMapping() {
        return new Object();
    }

}

package org.app.util.async;

public abstract class LoopedThread extends ExtendedThread {
    protected static final String LOOPED_BLOCK = "iteration";

    public LoopedThread(String name) {
        super(name);// 7
    }// 8

    public LoopedThread() {
        this("LoopedThread");// 11
    }// 12

    protected final void lockThread(String reason) {
        if (reason == null) {// 16
            throw new NullPointerException();// 17
        } else if (!reason.equals("iteration")) {// 19
            throw new IllegalArgumentException("Illegal block reason. Expected: iteration, got: " + reason);// 20
        } else {
            super.lockThread(reason);// 22
        }
    }// 23

    public final boolean isIterating() {
        return !this.isThreadLocked();// 26
    }

    public final void iterate() {
        if (!this.isIterating()) {// 30
            this.unlockThread("iteration");// 31
        }

    }// 32

    public final void run() {
        while(true) {
            this.lockThread("iteration");// 37
            this.iterateOnce();// 38
        }
    }

    protected abstract void iterateOnce();
}

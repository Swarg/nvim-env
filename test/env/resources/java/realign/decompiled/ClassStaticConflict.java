package pkg;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemException;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import org.apache.http.impl.client.CloseableHttpClient;
import com.google.gson.Gson;
/* here the static block overlaps the "client" filed */
public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);
    public static Gson GSON;
    public static Charset charset;
    public static CloseableHttpClient client;

    public Main() {
    }// 21

    public static void main(String[] args) throws Exception {
        boolean flag = true;// 30
        System.setProperty("java.net.preferIPv4Stack", String.valueOf(flag));// 31
    }

    static {
        charset = StandardCharsets.UTF_8;// 26
    }
}

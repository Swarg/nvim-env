package pkg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestClass01 {
    private static int[] sizes;
    private static String[] names = new String[]{"name1", "name2"};
    private static Map<String, Object> map;
    private static List<String> list;
    private static final int SIZE;

    public TestClass01() {
    }// 11

    public static void main(String[] var0) {
        System.out.println(sizes);// 30
        System.out.println(names);// 31
        System.out.println(SIZE);// 32
        map = new HashMap();// 33
        list = new ArrayList();// 34
    }// 35

    public static int[] getSizes() {
        return sizes;// 41
    }

    public static int sum(int var0, int var1) {
        int var2 = var0 + var1;// 45
        return var2;// 46
    }

    public static Object getKey(String var0) {
        return map.get(var0);// 50
    }

    public static void addItem(String var0) {
        list.add(var0);// 54
    }// 55

    static {
        sizes = new int[names.length];// 22
        SIZE = names.length;// 23
    }// 24
}

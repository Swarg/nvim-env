package pkg;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 */
public class TestClass01 {

    private static int[] sizes;
    private static String[] names;
    private static Map<String, Object> map;
    private static List<String> list;

    private static final int SIZE;

    static {
        names = new String[]{"name1", "name2"};
        sizes = new int[names.length];
        SIZE = names.length;
    }

    /**
     * Comment
     */
    public static void main(String[] args) {
        System.out.println(sizes);
        System.out.println(names);
        System.out.println(SIZE);
        map = new HashMap<>();
        list = new ArrayList<>();
    }

    /**
     *
     */
    public static int[] getSizes() {
        return sizes;
    }

    public static int sum(int a, int b) {
        final int sum = a + b;
        return sum;
    }

    public static Object getKey(String key) {
        return map.get(key);
    }

    public static void addItem(String item) {
        list.add(item);
    }
}

package org.comp;

// field_123456_g
// func_123456_g
/**
 * func_100000_b
 */
public class MyClass {

    private double field_100001_g;
    private int field_100002_T;

    public void func_100011_g(boolean p_100000_1_) throws IOException {
        System.out.println(p_100000_1_);
    }

    void func_100012_b(String p_100002_1_, int p_100002_2_, boolean p_100002_3_) {
        System.out.println(p_100002_1_ + p_100002_2_ + p_100002_3_);
    }

}

require 'busted.runner' ()

-- share nvim libs to outside using
require('env.outnvim') -- to support calling this code outside nvim
local su = require 'env.sutil'
local Cmd4Lua = require("cmd4lua")

local R = require 'env.require_util'
---@diagnostic disable-next-line unused-local
local inspect = R.require("inspect", vim, "inspect") -- vim.inspect

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local api = _G.vim.api

local M = require 'env.sourcecode'


local context1 = {
  funcname = "callme",
  module = "env.sourcecode",
  project = {
    build = "build",
    ext = "lua",
    project_root = "/home/dev/nvim/nvim-env",
    src = "lua",
    test = "test"
  }
}

local traceback1 = [[
stack traceback:
	...ack/packer/start/nvim-env/lua/env/app.lua:60: in function <...ack/packer/start/nvim-env/lua/env/app.lua:59>
	[C]: in function 'xpcall'
	...ack/packer/start/nvim-env/lua/env/util.lua:110: in function 'call_func_and_print_to_buf'
	...ack/packer/start/nvim-env/lua/env/sourcecode.lua:132: in function <...ack/packer/start/nvim-env/lua/env/sourcecode.lua:124>
]]
local fancy_trace1 = [[
stack traceback:
	env/app.lua:60: in function <env/app.lua:59>
	[C]: in function 'xpcall'
	env/util.lua:110: in function 'call_func_and_print_to_buf'
	env/sourcecode.lua:132: in function <env/sourcecode.lua:124>
]]

local ln1r =
[[	...ack/packer/start/nvim-env/lua/env/sourcecode.lua:60: in function <...ack/packer/start/nvim-env/lua/env/sourcecode.lua:59>]]
local ln1c = [[	env/sourcecode.lua:60: in function <env/sourcecode.lua:59>]]

describe('sourcecode', function()
  it("match-path", function()
    local line = context1.project.project_root
    local res = line:match('.*(/[^/]+)')
    assert.same('/nvim-env', res)
  end)

  it("replace_path_part", function()
    local t = context1
    local line = t.project.project_root
    local proj_src = line:match('.*(/[^/]+)') .. '/' .. t.project.src .. '/'
    local pattern = '%.%.%.[%w_%-%/]+' .. su.regex_escape(proj_src)
    local res = string.gsub(ln1r, pattern, '')
    assert.same(ln1c, res)
  end)

  it('fancy_traceback', function()
    local t = context1
    local res = M.fancy_traceback(t, traceback1)
    assert.same(vim.split(fancy_trace1, '\n', { trimempty = false }), res)
  end)

  it("match", function()
    local res = traceback1:match('(%.%.%.[%a_%-%./]+)env/sourcecode%s*')
    assert.same('...ack/packer/start/nvim-env/lua/', res)
  end)

  it("table.insert", function()
    local t = { 'abc', 'e' }
    local mline = 'def\nght'
    local lines = vim.split(mline, "\n\r?", { trimeply = false })
    table.insert(t, lines)
    assert.same('{ "abc", "e", { "def", "ght" } }', vim.inspect(t))
  end)

  -- See sourcecode here /usr/share/nvim/runtime/lua/vim/shared.lua
  it("vim.list_extend", function()
    local t = { 'abc', 'e' }
    local mline = 'def\nght'
    local lines = vim.split(mline, "\n\r?", { trimeply = false })
    vim.list_extend(t, lines, 1, #lines)
    assert.same('{ "abc", "e", "def", "ght" }', vim.inspect(t))
  end)

  it("mk_tbl_key", function()
    assert.same("[1]", M.mk_tbl_key('1'))
    assert.same("['abc']", M.mk_tbl_key("'abc'"))
    assert.same("abc", M.mk_tbl_key('abc'))
    assert.same("['ab cd']", M.mk_tbl_key('ab cd'))
  end)

  it("wrap_to_quotes", function()
    assert.same("1", M.mk_tbl_value('1'))
    assert.same("'a1'", M.mk_tbl_value('a1'))
    assert.same('"1"', M.mk_tbl_value('"1"'))
  end)

  it("current_line_split", function()
    local line = 'local s = "one\\ntwo\\nthree" -- X'
    local bufnr = nvim:new_buf({ line }, 1, 1)
    M.current_line_split(Cmd4Lua.of({ "code-string" }))

    local res = api.nvim_buf_get_lines(bufnr, 0, -1, false)
    local exp = { 'local s = "one\\n" ..', '"two\\n" ..', '"three"', ' -- X' }
    assert.same(exp, res)
  end)

  it("current_line_debug_print", function()
    local line = 'a,b,c,d, e'
    local bufnr = nvim:new_buf({ line }, 1, 1)
    M.current_line_debug_print(Cmd4Lua.of({}))

    local res = api.nvim_buf_get_lines(bufnr, 0, -1, false)
    local exp = {
      [[print('[DEBUG] ','a:', a, 'b:', b, 'c:', c, 'd:', d, ' e:',  e)]]
    }
    assert.same(exp, res)
  end)


  it("cmd_split_liny_by_anchor_word", function()
    local f = M.cmd_split_liny_by_anchor_word
    local line = 'anchor{k=v, b=42, c=88}, anchor{a=2, b=43}, anchor{b=9}'
    local exp = {
      'anchor{k=v, b=42, c=88}, ',
      'anchor{a=2, b=43}, ',
      'anchor{b=9}'
    }
    assert.same(exp, f(Cmd4Lua.of({ 'anchor' }), line))
  end)
end)

-- first attempt: [NOTE] in lua '-' is spec char! use regex_esq
-- local mpath = t.module:gsub('%.', '/')
-- local pattern = '(%.%.%.[%a_%-%./]-)' .. mpath
-- local starts = traceback:match(pattern)
-- traceback = traceback:gsub(su.regex_escape(starts), '')

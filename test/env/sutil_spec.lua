-- For Run test use busted directly cmd
-- busted <filename_spec.lua>
-- Don't used PlenaryBustedFile due to issues with test output rendering

--[[ By Default lua search modules(packages) in :
/usr/share/lua/VER/
/usr/local/share/lua/VER/
/usr/local/lib/lua/VER/
./?.lua;
./src/?.lua;
./src/?/?.lua;
./src/?/init.lua;

./lua/?.lua  << for my case add directry
Another found way to solve issue with passing LUA_PATH to resolve runner.sutil
is set the CWD to path with source for this test file and run from there
]]
-- TODO Why evn LUA_PATH don't work?
-- See: luarocks doc busted
-- package.path = package.path .. ';./lua/?.lua'
-- print(package.path)

require 'busted.runner' ()
local assert = require('luassert')
local su = require 'env.sutil'
local fs = require 'env.files'

require 'env.outnvim'

--- to remove annoying diagnostics output for global busted functions
--- define 3rd lib path for lua_ls in your config
-- local describe, it = describe, it
---@diagnostic disable-next-line
assert.is_equals = assert.is_equals

describe('env.sutil', function()
  it('get_last_line', function()
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_equals(nil, su.get_last_line(nil))
    assert.is_equals('', su.get_last_line(""))
    assert.is_equals('', su.get_last_line("\n"))
    assert.is_equals('1', su.get_last_line("1"))
    assert.is_equals('ax', su.get_last_line("ax"))
    assert.is_equals('abc', su.get_last_line("abc\n"))
    assert.is_equals('JKL', su.get_last_line("abc\nDEF\nJKL\n"))
    assert.is_equals('JKL', su.get_last_line("abc\nDEF\nJKL"))
    assert.is_equals('A', su.get_last_line("abc\nDEF\nJKL\nA"))
    assert.is_equals('AB', su.get_last_line("abc\nDEF\nJKL\nAB"))
    assert.is_equals('ABC', su.get_last_line("abc\nDEF\nJKL\nABC"))
    assert.is_equals('ABC', su.get_last_line("abc\nDEF\nJKL\nABC\n"))
    assert.is_equals('E', su.get_last_line("abc\nDEF\nJKL\nABC\nE"))
  end)

  it('substr - substr from given range without errors', function()
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_nil(su.substr())
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_nil(su.substr(nil))
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_nil(su.substr(nil, nil))
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_nil(su.substr(""))
    assert.same("", su.substr("", 0)) -- s:0 e:0 str starts with 1 but s==e
    assert.same("", su.substr("", 1))
    assert.same("a", su.substr("a", 1))
    assert.same("abc", su.substr("abc", 1))
    assert.same("a", su.substr("abc", 1, 1))
    assert.same("b", su.substr("abc", 2, 2))
    assert.same("c", su.substr("abc", 3)) -- endi to end of line
    -- then out of range - return inrange substring no error
    assert.same("c", su.substr("abc", 3, 9))
    -- range from line ends
    local s = "abcdefgh"
    assert.same("cde", s:sub(3, 5))
    assert.same("cdef", su.substr(s, 3, -3))
    assert.same("cdefgh", su.substr(s, 3, -1))
    assert.same("abcdefgh", su.substr(s, 0, -1))
    assert.same("abcdefgh", su.substr(s, 1, -1))
  end)

  it('last_index_of', function()
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_equals(-1, su.last_indexof(nil, nil))
    assert.is_equals(-1, su.last_indexof("", ""))
    assert.is_equals(1, su.last_indexof("A", "A"))
    assert.is_equals(4, su.last_indexof("ABCD", "D"))
    assert.is_equals(1, su.last_indexof("ABCD", "ABCD"))
    assert.is_equals(-1, su.last_indexof("ABCD", "ABCDE"))
    assert.is_equals(-1, su.last_indexof("ABCD", ""))
  end)

  it('rand_str', function()
    su.set_randomseed(0) -- for one seed return same pseudo ranom value
    assert.same("qOmnuCKl", su.rand_str(8))
    su.set_randomseed(2) -- another seed give anoter value of rnd string
    assert.same("ho57LQh4", su.rand_str(8))
  end)

  it('contains', function()
    local show_err = false -- to output
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_false(su.contanis(nil, nil, show_err))
    ---@diagnostic disable-next-line cannot assign nil
    assert.is_false(su.contanis("abc", nil, show_err))
    assert.is_false(su.contanis("abc", "", show_err))
    assert.is_false(su.contanis("", "abc", show_err))
    assert.is_false(su.contanis("abc", "xxx", show_err))
    assert.is_true(su.contanis("abc", "a", show_err))
    assert.is_true(su.contanis("abc", "c", show_err))
    assert.is_true(su.contanis("abc", "b", show_err))
    assert.is_true(su.contanis("abc", "abc", show_err))
  end)

  it("fold_about_lua", function()
    local lines = fs.read_lines('./test/env/resources/about_lua.txt')
    assert.is_not_nil(lines)
    local res = su.fold_line(lines[3], 80)
    local exp = {
      '  Lua is a powerful, efficient, lightweight, embeddable scripting language. It',
      'supports procedural programming, object-oriented programming, functional',
      'programming, data-driven programming, and data description. Lua combines simple',
      'procedural syntax with powerful data description constructs based on',
      'associative arrays and extensible semantics.' }
    -- for i = 5, 9 do
    --   table.insert(exp, lines[i])
    -- end
    -- print('\n') for _,v in pairs(res) do print(v) end
    assert.same(exp, res)
  end)

  it("fold_line", function()
    local line = "a b c d e f j h i j k l m o p q r s t u v w x y z"
    local exp  = { "a b c d", "e f j h", "i j k l", "m o p q", "r s t u", "v w x y", "z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-2", function()
    local line = "ab cd ef jh ij kl mo pq rs tu vw xy z"
    local exp = { "ab cd", "ef jh", "ij kl", "mo pq", "rs tu", "vw xy z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-3", function()
    local line = "abcd efjh ijkl mopq rstu vwxy z"
    local exp = { "abcd", "efjh", "ijkl", "mopq", "rstu", "vwxy z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-4", function()
    local line = "abcdefjh ijklmopq rstuvwxy z"
    local exp = { "abcdefjh", "ijklmopq", "rstuvwxy", "z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-5", function()
    local line = "abcdefjhijklmopqrstuvwxy z"
    local exp = { "abcdefjhijklmopqrstuvwxy", "z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-6", function()
    local line = "abcdefjhijklmopqrstuvwxyz"
    assert.same({ line }, su.fold_line(line, 8))
  end)

  it("fold_line-7", function()
    assert.same({ 'abc' }, su.fold_line('abc', 8))
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same({}, su.fold_line(nil))
  end)

  it("fold_line-8", function()
    local line = "a1b2c3d4e5f6j7h8i9jakbl m1o2p 3q4r5s t1u2v3w4x5y6z"
    local exp = { "a1b2c3d4e5f6j7h8i9jakbl", "m1o2p", "3q4r5s", "t1u2v3w4x5y6z" }
    assert.same(exp, su.fold_line(line, 4))
  end)

  it("fold_line-9", function()
    local line = "--abcdefjh ijklmopq rstuvwxy z"
    local exp = { "-- abcdefjh", "-- ijklmopq", "-- rstuvwxy", "-- z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-10", function()
    local line = "// abcdefjh ijklmopq rstuvwxy z"
    local exp = { "//  abcdefjh", "// ijklmopq", "// rstuvwxy", "// z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("fold_line-10", function()
    local line = "# abcdefjh ijklmopq rstuvwxy z"
    local exp = { "# abcdefjh", "# ijklmopq", "# rstuvwxy", "# z" }
    assert.same(exp, su.fold_line(line, 8))
  end)

  it("split", function()
    assert.same({ 'aa', 'b' }, su.split(":aa::b:", ':'))
    assert.same({ 'x', 'y' }, su.split("axaby", 'ab?'))
    assert.same({ 'x', 'yz', 'o' }, su.split("x*yz*o", '%*'))
    assert.same({ 'x', 'y', 'z' }, su.split("|x|y|z|", '|'))
  end)

  it("split", function()
    local t = { '0', }
    assert.same({ '0', '1', '2', '3' }, su.split('1,2,3', ',', t))
  end)

  it("split with foreach callback", function()
    local f = su.split
    assert.same({ 'a', ' b', ' c', ' d' }, f('a, b, c, d', ','))
    assert.same({ 'a', 'b', 'c', 'd' }, f('a, b, c, d', ',', nil, su.trim))
  end)

  local a_eq = assert.same

  it("split_range", function()
    a_eq({ 'abc', 'def', 'g' }, su.split_range("abc\ndef\ng"))
    a_eq({ 'abc', 'def', 'g' }, su.split_range("abc\r\ndef\r\ng"))
    a_eq({ 'abc', '\rdef', '\rg' }, su.split_range("abc\n\rdef\n\rg"))
    a_eq({ 'abc', 'def', '' }, su.split_range("abc\ndef\n"))
    a_eq({ 'a', 'b', '', 'c' }, su.split_range("a\nb\n\nc"))
    a_eq({ '', '', '', 'c' }, su.split_range("\n\n\nc"))
    a_eq({ '', '', '', 'c', '' }, su.split_range("\n\n\nc\n"))
    a_eq({ 'de', '', 'c', '' }, su.split_range("abc\nde\n\nc\n", '\n', 5))
    --            123 456 7 89 0
    local line = "abc\nde\n\nc\n"
    a_eq({ 'de' }, su.split_range(line, '\n', 5, 6))
    a_eq({ 'de', '' }, su.split_range(line, '\n', 5, 7))
    a_eq({ 'de', '', '' }, su.split_range(line, '\n', 5, 8))
    a_eq({ 'bc', 'de' }, su.split_range(line, '\n', 2, 6))
  end)

  it("split_range to old", function()
    local old = { 'a', 'b' }
    local exp = { 'a', 'b', 'c', 'd', 'e' }
    assert.same(exp, su.split_range('c,d,e', ',', 1, 5, false, old))
  end)

  -- last-sep-pos+1 used to keep offset
  it("split_range-last-sep-pos", function()
    local out = { pos = 0 } --keep offset
    --            123 4567 890
    local line = 'ABC\nDEF\nGH'
    a_eq({ 'ABC', 'DEF', 'GH' }, su.split_range(line, '\n', 1, #line, out))
    a_eq({ pos = 9 }, out)

    --      123 4567 8
    line = 'ABC\nDEF\n'
    a_eq({ 'ABC', 'DEF', '' }, su.split_range(line, '\n', 1, #line, out))
    a_eq({ pos = 9 }, out) -- 8 + sep_len(1)

    line = 'ABC\nDEF'
    a_eq({ 'ABC', 'DEF' }, su.split_range(line, '\n', 1, #line, out))
    a_eq({ pos = 5 }, out) -- 4 + sep_len(1)

    a_eq({ 'abc' }, su.split_range('abc', '\n', 1, string.len('abc'), out))
    a_eq({ pos = 1 }, out)
  end)

  it("trim_empty_strs", function()
    local t = { 'a', 'b', '', 'c', 'e', '', '' }
    local exp = { 'a', 'b', 'c', 'e' }
    assert.same(exp, su.rm_empty_elm(t))
  end)

  it("indexof_contains_str", function()
    local list = { 'abc', 'def', 'ghk', 'e', 'abc' }
    a_eq(1, su.indexof(list, 'abc'))
    a_eq(-1, su.indexof(list, 'abc', true, 2, 3))
    a_eq(5, su.indexof(list, 'abc', true, 2))
  end)

  it("indexof regex ", function()
    local list = { 'Found 10 items, similar to abc ', 'def', }
    a_eq(1, su.indexof(list, '^Found %d+ items, similar to', false))
  end)

  it("regex ", function()
    --         12345678 901
    local s = "abcd ef\n   Found 16 items, similar to abc "
    a_eq('16', string.match(s, '^%s*Found (%d+) items, similar to', 10))

    local s2 = "abcd ef\nFound 8 items, similar to abc "
    a_eq('8', string.match(s2, '^Found (%d+) items, similar to', 9))
  end)

  it("format", function()
    -- places to insert (%s) not defined, add to end of line
    a_eq(" abc", su.format('', 'abc'))
    a_eq(" abc de", su.format('', 'abc', 'de'))
    a_eq(" abc de 8", su.format('', 'abc', 'de', 8))
    a_eq("msg abc de 8", su.format('msg', 'abc', 'de', 8))

    a_eq('msg abc de 8', su.format("msg %s %s %d", 'abc', 'de', 8))
    a_eq('msg  d 8', su.format("msg %s %s %d", '', 'd', 8))
    a_eq('msg d', su.format("msg %s%s%s", '', 'd', ''))
    a_eq('msg d extra', su.format("msg %s%s%s", '', 'd', '', 'extra'))
    a_eq('msg %s', su.format("msg %s"))
    a_eq('msg abc %s', su.format("msg %s %s", 'abc'))
    a_eq('msg {} %s', su.format("msg %s %s", {}))
    a_eq('msg {} {}', su.format("msg %s %s", {}, {}))

    local res = su.format("msg %s %s", { a = 9 }, { b = 7 })
    a_eq('msg {a = 9} {b = 7}', res)

    res = su.format("msg %s %s", { 1, 2, a = 8, b = 'txt' }, { c = 7 })
    a_eq('msg {[1] = 1, [2] = 2, a = 8, b = "txt"} {c = 7}', res)

    local t = { 1, 3, 5 };
    t["4"] = 4
    -- t[function() return '5' end] = 5 +
    -- t[nil] = 5 -- error
    a_eq('msg {[1] = 1, [2] = 3, [3] = 5, 4 = 4} %s', su.format("msg %s %s", t))

    res = su.format("msg %s %s", { a = 9 }, { b = 7, c = { sc = 1, 2 } })
    a_eq('msg {a = 9} {c = {[1] = 2, sc = 1}, b = 7}', res)

    a_eq('msg 0.125', su.format('msg %f', 0.125))
    a_eq('msg 125', su.format('msg %f', 125))
    a_eq('msg 125', su.format('msg %d', 125))
    a_eq('msg 125', su.format('msg %s', 125))
    a_eq('msg 125', su.format('msg %x', 125))
    a_eq('msg %888', su.format('msg %%%x', 888))
    a_eq('msg %%abc', su.format('msg %%%%%s', 'abc'))
    a_eq('msg abc', su.format('msg %', 'abc')) -- errornes single % -> ''
    a_eq('msg % abc', su.format('msg %%', 'abc'))

    local t2 = { 1, 2, 3, key0 = 'value0' }
    t2[function() return 'key' end] = 'val' -- key in table is function!
    t2["key1"] = 'value1'
    t2["i5"] = 6
    t2["6"] = 7  -- inspect show it as '["6"] = 7' dum as '6 = 7'
    t2["7a"] = 8 -- inspect show it as '["6"] = 7' dum as '6 = 7'
    assert.no_error(function() su.format('m %s', t2) end)
    -- print(require 'inspect' (t2))
    -- print(su.format('dump:', t2))
    -- print(require 'inspect' (t))
  end)

  it("line:find", function()
    --            1234567890
    local line = 'abcdefghij'
    local i, j = line:find('defg')
    assert.same(4, i)
    assert.same(7, j)
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same('defg', line:sub(i, j))
    assert.same('hij', line:sub(j + 1))
    local x, y = line:find('abcde')
    assert.same(1, x)
    assert.same(5, y)
  end)

  it("string.find", function()
    local line = 'abcdefghij'
    local i, j = string.find(line, 'defg', 1, true)
    assert.same(4, i)
    assert.same(7, j)
    local x, y = string.find(line, 'abcde', 1, true)
    assert.same(1, x)
    assert.same(5, y)
  end)

  -- \n -> newlines
  it("apply_escapesfold_line", function()
    local f = su.apply_escapes
    assert.same({ 'abc', 'def' }, f('abc\\ndef'))
    assert.same({ 'abc' }, f('abc'))
    assert.same({ 'a' }, f('a'))
    assert.same({}, f(nil))
    assert.same({}, f(""))
    assert.same({ '', '' }, f("\\n"))
    assert.same({ '', '', '' }, f("\\n\\n"))
    assert.same({ ' ', ' ', '' }, f(" \\n \\n"))
    assert.same({ 'a', '' }, f("a\\n"))
    assert.same({ 'a', ' ' }, f("a\\n "))
  end)

  it("trim", function()
    assert.same("abc", su.trim('  abc  '))
    assert.same("a", su.trim('  a  '))
    assert.same("", su.trim('    '))
    assert.same("", su.trim(' '))
    assert.same("ab", su.trim('  ab'))
  end)

  it("is_yes", function()
    assert.same(true, su.is_yes('yes'))
    assert.same(true, su.is_yes('Yes'))
    assert.same(true, su.is_yes('YES'))
    assert.same(true, su.is_yes('Y'))
    assert.same(true, su.is_yes('y'))
    assert.same(true, su.is_yes('+'))
    assert.same(true, su.is_yes('1'))
    assert.is_false(su.is_yes('No'))
    assert.is_false(su.is_yes('n'))
    assert.is_false(su.is_yes('0'))
    assert.is_false(su.is_yes(''))
    assert.is_false(su.is_yes(nil))
  end)

  -- create ouput based on template and substitutions
  it("templ_substitution", function()
    local template = "a=V; b=VAL.2; c=V_3; <V4>, 'V4' abcV4 a,V4 _V4 (V4) [V4]"
    local subs = { V = 8, ['VAL.2'] = 16, V_3 = 4, V4 = "8" }
    local output = string.gsub(template, "[%w%._]+", subs)
    assert.same("a=8; b=16; c=4; <8>, '8' abcV4 a,8 _V4 (8) [8]", output)
  end)

  it("check_templ_substitution", function()
    local lines = { '1', '2', '3' }
    local templ = { '1', '2', '3' }
    local subs = {}
    assert.no_error(function()
      su.check_templ_substitution(lines, templ, subs)
    end)

    assert.error_matches(function()
      su.check_templ_substitution(lines, {}, subs)
    end, "Not Found templ_line %d", 1, false)

    assert.error_matches(function()
      ---@diagnostic disable-next-line: param-type-mismatch
      su.check_templ_substitution(lines, nil, subs)
    end, "templ_lines must be a table with strings", 1, true)

    assert.error_matches(function()
      ---@diagnostic disable-next-line: param-type-mismatch
      su.check_templ_substitution({}, {}, nil)
    end, "substitutions must .*", 1, false)

    assert.no_error(function()
      ---@diagnostic disable-next-line: param-type-mismatch
      lines = { '# comment', 'key1=val1', 'k2=val2', 'the line' }
      templ = { '# comment', 'key1=KEY1', 'k2=KEY2', 'LINE' }
      subs = { KEY1 = "val1", KEY2 = "val2", LINE = "the line" }
      su.check_templ_substitution(lines, templ, subs)
    end)

    assert.error_matches(function()
      lines = { '# comment', 'key1=val1', 'k2=val2', 'LINE' }
      templ = { '# comment', 'key1=KEY1', 'k2=KEY2', 'LINE' }
      subs = { KEY1 = "val1", KEY2 = "val2", LINE = "the line" }
      su.check_templ_substitution(lines, templ, subs)
    end, "Found Not Replaced Key: [LINE]", 1, true)

    assert.error_matches(function()
      lines = { '# comment', 'key1=val1', 'k2=KEY2', 'LINE' }
      templ = { '# comment', 'key1=KEY1', 'k2=KEY2', 'LINE' }
      subs = { KEY1 = "val1", KEY2 = "val2", LINE = "the line" }
      su.check_templ_substitution(lines, templ, subs)
    end, "Found Not Replaced Key: [KEY2]", 1, true)
  end)

  it("rand_rasswd", function()
    assert.same(8, su.rand_rasswd(8):len())
    assert.same(16, su.rand_rasswd(16):len())
    assert.same(32, su.rand_rasswd(32):len())
    -- local seed = 1 -- one seed -- one pseudo random generation results
    -- assert.same("1D6yJ4hIuV9a", su.rand_rasswd(12, false, seed))
    -- assert.same("QKp3pSc3S9HyV8w1", su.rand_rasswd(16, false, 2))
  end)

  it("rand_id", function()
    assert.same(8, su.rand_id(8):len())
    assert.same(16, su.rand_id(16):len())
    assert.same(32, su.rand_id(32):len())
    assert.is_not_nil(su.rand_id(32):match('^[abcdef%d]+$'))
  end)

  it("table_size", function()
    assert.same(-1, su.table_size(nil))
    assert.same(-1, su.table_size(''))
    assert.same(0, su.table_size({}))
    assert.same(1, su.table_size({ 1 }))
    assert.same(4, su.table_size({ 1, 2, 3, 4 }))
    assert.same(3, su.table_size({ a = 1, b = 2, 3 }))
    assert.same(1, su.table_size({ key = 8 }))
    assert.same(0, su.table_size({ key = nil }))
  end)

  it("dup", function()
    assert.same("----", su.dup('-', 4))
    assert.same("abababab", su.dup('ab', 4))
  end)

  describe('get_group_end', function()
    local cases = {
      -- input offset, exp
      { '{} ',                    1, 2 },
      { '{{}} ',                  1, 4 },
      { '{{}{} ',                 1, -1 }, -- error
      { '() ()',                  1, 2 },
      { '(()) ()',                1, 4 },
      { '(\\)) ()',               1, 4 },
      { '(\\\\\\)) ()',           1, 6 }, -- (\\\))
      { '"\\"a"  "b c"',          1, 5 },
      { '{"a", "b"}  ',           1, 10 },
      -- 12345678901234567890
      { '{"a", "b", {"c"}}  ',    1, 17 },
      { '{"a", "{", {"}" }}  ',   1, 18 },
      { '"{{(}}", ")"',           1, 7 },
      { '"\'" abc ',              1, 3 }, -- quotes group skipp all another
      { '{("a", "b"),(\'c\')}  ', 1, 18 },
      { "' \"abc ' ",             1, 8 }, -- quotes group skipp all another
      { "{\\{}  }  ",             1, 4 }, -- secod { escaped!
    }
    for i, case in pairs(cases) do
      describe('case: ' .. i, function()
        it("input: [" .. case[1] .. "]", function()
          local input, offset, exp = case[1], case[2], case[3]
          local r = su.get_group_end(input, offset)
          assert.same(exp, r)
        end)
      end)
    end
  end)

  it("get_group_end escape", function()
    local f = function(line, off)
      local ge = su.get_group_end(line, off)
      if type(ge) == 'number' then
        return line:sub(off, ge)
      end
    end
    assert.same("'x y'", f("'x y\'", 1))
    assert.same("'x y\\'", f("'x y\\'", 1))
    assert.same("'x y\\\'", f("'x y\\\'", 1))
    assert.same('[x y\\] ]', f("[x y\\] ]", 1))
    assert.same('[x y\\\\]', f("[x y\\\\] ]", 1))         -- [x y\\] (self escaped
    assert.same('[x y\\\\\\] ]', f("[x y\\\\\\] ]", 1))   -- [x y\\\] -> \]
    assert.same('[x y\\\\\\\\]', f("[x y\\\\\\\\] ]", 1)) -- [x y\\\\] self esc
    -- require'dprint'.enable_module(su, true)
  end)

  it("split_by_groups 0", function()
    assert.same({ 'exp', 'res' }, su.split_groups('exp, res'))
    assert.same({ 'a', 'bcd', 'e' }, su.split_groups('a,bcd,e'))
    assert.same({ '0', '(a,b,c)' }, su.split_groups('0,(a,b,c)'))
    assert.same({ 'pref(a,b,c)' }, su.split_groups('pref(a,b,c)'))
    assert.same({ '(a,b,c)' }, su.split_groups('(a,b,c)'))
    assert.same({ '(a,b,c)' }, su.split_groups(' (a,b,c)'))
    assert.same({ '(a,b,c) -- extra' }, su.split_groups(' (a,b,c) -- extra'))
    assert.same({ '" -- "', '-- x' }, su.split_groups(' " -- ", -- x'))
    assert.same({ '"a,b,c" -- extra' }, su.split_groups(' "a,b,c" -- extra'))
  end)

  it("split_by_groups 1", function()
    --            12345678901234567890
    local line = ' foo(a,b,c) -- extra'
    local exp = { '(a,b,c)' }
    assert.same(exp, su.split_groups(line, ',', 5, 11))
  end)

  it("split_by_groups 2", function()
    --           12345678901234567890123456789012
    local line = "  assert.same(3, le) -- comment"
    local exp = { '3', 'le' }
    assert.same(exp, su.split_groups(line, ',', 15, 19, false))
  end)

  it("split_by_groups 2", function()
    local exp = { 'abc(a,b,c)', 'd', 'e' }
    assert.same(exp, su.split_groups('abc(a,b,c),d,e'))
  end)

  it("split_by_groups 3", function()
    local exp = { 'abc(a,b,c)', '{d,e}', 'j' }
    assert.same(exp, su.split_groups('abc(a,b,c), {d,e} ,   j'))
  end)

  it("split_by_groups 4 fail - no )", function()
    local exp = { 'a', 'func', }
    assert.same(exp, su.split_groups('a, func(a,b,c'))
  end)

  it("split_groups with decorator unwrap", function()
    local f = su.split_groups
    local decorator = function(e) -- unwrap decorator
      if e and e:sub(1, 1) == '"' then
        e = e:sub(2, -2)
      end
      -- print(e)
      return e
    end
    local exp = { 'a', 'b c', 'd' }
    assert.same(exp, f('"a", "b c", d', false, nil, nil, false, decorator))
  end)

  it("split_groups with decorator wrap", function()
    local f = su.split_groups
    local decorator = function(e) -- unwrap decorator
      return '->' .. tostring(e) .. '<-'
    end
    local exp = { '->a<-', "->'b c'<-", '->d<-' }
    assert.same(exp, f("a, 'b c', d", false, nil, nil, false, decorator))
  end)

  it("unwrap_quotes", function()
    local f = su.unwrap_quotes
    assert.same("a b", f('"a b"'))
    assert.same("a b", f("'a b'"))
    assert.same(' a b ', f("' a b '"))
  end)

  it("split_groups with decorator wrap", function()
    local f, decorator = su.split_groups, su.unwrap_quotes
    local exp = { '(a b)', 'c d', 'e' }
    assert.same(exp, f("(a b), 'c d', \"e\"", false, nil, nil, false, decorator))
  end)


  it("back_indexof", function()
    --            1234 56789 01234567890
    local line = "one\nthow\nthree"
    assert.same(9, su.back_indexof(line, "\n"))
    assert.same(9, su.back_indexof(line, "\n", 9)) --[s,e]
    assert.same(-1, su.back_indexof(line, "\n", 10))
    assert.same(4, su.back_indexof(line, "\n", 1, 6))
    assert.same(1, su.back_indexof("abc", "a"))
    assert.same(4, su.back_indexof("abca", "a"))
  end)

  it("back_indexof fails", function()
    assert.same(-1, su.back_indexof("abc", ""))
    assert.same(-1, su.back_indexof("abc", "abc"))
    assert.same(-1, su.back_indexof("abc", "ab"))
    assert.same(-1, su.back_indexof("", "a"))
  end)

  it("gsub", function()
    local templ = "a(${ABC}${DE})${F}${G}"
    local t = { ABC = '', DE = 'de', F = '0', G = '8' }
    -- for s in string.gmatch(templ, "%${([%w_]+)}") do
    --   print('@@@@',s)
    -- end
    assert.same('a(de)08', string.gsub(templ, "%${([%w_]+)}", t))
  end)

  it("get_char_count", function()
    assert.same(3, su.get_char_count('aaabc', 'a', 2))
    assert.same(3, su.get_char_count('aaabcaa', 'a', 2))
    assert.same(5, su.get_char_count('aaabcaa', 'a'))
    assert.same(1, su.get_char_count('aaabcaa', 'b'))
    assert.same(0, su.get_char_count('aaabcaa', 'x'))
  end)

  -- for _, s in ipairs {
  --   "line0", "line1"
  -- } do
  --   it(' test for [' + s + '] ', function()
  --     assert.is_not_nul(s)
  --   end)
  -- end

  it("align_str_to", function()
    local f = su.align_str_to
    assert.same('abc       ', f('abc', 10, '- '))
    assert.same('       abc', f('abc', 10, ' -'))
    assert.same('   abc    ', f('abc', 10, ' - '))
    assert.same('a-ver', f('a-very-long-line', 5, ' - '))
  end)

  it("replace_fairst", function()
    local f = su.replace_first
    assert.same('a new_sub b', f("a old_sub b", "old_sub", "new_sub"))
    assert.same('first_only abc abc ', f("abc abc abc ", "abc", "first_only"))
    assert.same('first_only abc abc ', f("a-c abc abc ", "a-c", "first_only"))
  end)

  it("replace_in_range", function()
    local f = su.replace_in_range
    local exp = '1234ABC90'
    assert.same(exp, f("1234567890", 5, 8, 'ABC'))
    local s, e = string.find(exp, 'ABC', 1, true) ---@cast s number
    assert.same('1234567890', f(exp, s, e or 0, '5678'))
  end)

  it("replace_chars_in_lines", function()
    local f = su.replace_chars_in_lines

    local lines = {
      '/tmp/nvim-env/javac/',
      '├── build',
      '│   └── org',
      '│       └── ns',
      '│           └── Main.class',
      '└── src',
      '    └── Main.java',
      '',
      '4 directories, 2 files'
    }
    local exp = {
      '/tmp/nvim-env/javac/',
      '    build',
      '        org',
      '            ns',
      '                Main.class',
      '    src',
      '        Main.java',
      '',
      '4 directories, 2 files'
    }
    assert.same(exp, f(lines, { '└', '─', '├', '│' }, " "))
  end)

  it("tohex & fromhex", function()
    assert.same('4f72616e6765', su.tohex("Orange"))
    assert.same('Orange', su.fromhex("4f72616e6765"))

    assert.same('31323334353637383930', su.tohex("1234567890"))
    assert.same('1234567890', su.fromhex("31323334353637383930"))
  end)
end)

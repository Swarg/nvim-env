-- 24-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local M = require "env.bridges.mpv"

describe("env.bridges.mpv", function()
  it("build_cmd", function()
    local f = M.build_cmd_json
    local exp = [[
{"command":["get_property","filename/no-ext"],"request_id":1}
]]
    assert.same(exp, f(1, 'get_property', 'filename/no-ext'))
  end)

  it("append_buf_to_lines", function()
    local f = M.append_buf_to_lines
    local t = {}
    assert.same('', f(t, '', "123\n456\n"))
    assert.same({ '123', '456' }, t)

    t = {}
    assert.same('', f(t, 'BeforeRead', "Readed\n"))
    assert.same({ 'BeforeReadReaded' }, t)

    t = {}
    assert.same('', f(t, '123', "\n"))
    assert.same({ '123' }, t)

    t = {}
    assert.same('123456', f(t, '', "abc\nde\n\n123456"))
    assert.same({ 'abc', 'de', '' }, t)

    t = {}
    assert.same('abc', f(t, '', "abc"))
    assert.same({}, t)

    t = {}
    assert.same('', f(t, '', ""))
    assert.same({}, t)

    t = {}
    assert.same('', f(t, '', "\n"))
    assert.same({ '' }, t)

    t = {}
    assert.same('remains', f(t, '', "\nremains"))
    assert.same({ '' }, t)
  end)

  it("get_requestid", function()
    local f = M.get_requestid
    assert.same(789, f('{"request_id":789,"error":"success"}'))
    assert.same(123, f('{"error":"success","request_id":123}'))
    assert.same(-1, f('{"error":"success","request_id":123abc}'))
    assert.same(-1, f('{"error":"success","request_id":123'))
  end)

  it("append_buf_to_lines with wait_req_id", function()
    local f = M.append_buf_to_lines

    local lines = { 'already readed data' }
    local buf, readed, wreq_id = '{"requ', 'est_id":123}' .. "\n", 123
    assert.same({ '', 2 }, { f(lines, buf, readed, wreq_id) })

    buf, readed, wreq_id = '', '{"request_id":777}', 777
    assert.same({ '{"request_id":777}', -1 }, { f(lines, buf, readed, wreq_id) })

    buf, readed, wreq_id = '', '{"request_id":777}' .. "\n", 777
    assert.same({ '', 3 }, { f(lines, buf, readed, wreq_id) })
    --                ^ index with needed req_id in t
    --            ^ rems in buf
    local exp = {
      'already readed data',
      '{"request_id":123}', -- 2
      '{"request_id":777}'  -- 3
    }
    assert.same(exp, lines)
  end)

  it("is_success_response_for", function()
    local f = M.is_success_for
    local lines = {
      '{"request_id":123}',                  -- 1
      '{"request_id":789,"error":"success"}' -- 2
    }
    local resp_idx, req_id, hint = 2, 789, 'hint for log.debug'
    assert.same(true, f(lines, resp_idx, req_id, hint))

    -- bad: at resp_idx no reponse
    lines = { '{"request_id":789,"error":"success"}' }
    resp_idx, req_id, hint = 2, 789, 'hint for log.debug'
    assert.same(false, f(lines, resp_idx, req_id, hint))

    -- bad: not synched req_id
    lines = { '{"request_id":789,"error":"success"}' }
    resp_idx, req_id = 1, 999
    assert.same(false, f(lines, resp_idx, req_id, hint))

    -- ok!
    lines = { '{"request_id":8888,"error":"success"}' }
    resp_idx, req_id = 1, 8888
    assert.same(true, f(lines, resp_idx, req_id, hint))
  end)
end)

-- 14-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.bridges.sdcv");
local su = require("env.sutil")

describe("env.bridges.sdcv", function()
  --
  it("format_output", function()
    local output = '' -- from sdcv
    if 0 == 0 then return end
    local res = M.format_output(output)
    for _, line in ipairs(res) do print(line) end
    assert.same(68, #res)
  end)


  it("lang crossroads", function()
    -- if 0 == 0 then return end
    local line = "head\n\n\n[transl] 1. noun условиях all"
    local t = M.format_output(line)
    local exp = {
      'head',
      '',
      '',
      '[transl]',
      '----------------',
      '1. noun',
      '  условиях',
      'all'
    }
    assert.same(exp, t)
  end)
end)

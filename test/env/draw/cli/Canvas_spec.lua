-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local log = require 'alogger'
local Canvas = require("env.draw.Canvas");
local Point = require("env.draw.Point");
local Line = require("env.draw.Line");
-- local Text = require("env.draw.Text");
-- local Rectangle = require("env.draw.Rectangle");
-- local Container = require("env.draw.Container");
local IEditable = require 'env.draw.IEditable'

local get_elements_readable_list = Canvas.get_elements_readable_list
local mkpatch = IEditable.cli().TESTING.mk_patch
local UPDATED = IEditable.cli().CONSTS.UPDATED_KEY
local TYPE_MISMATCH = IEditable.cli().CONSTS.TYPE_MISMATCH


describe("env.draw.Canvas", function()
  after_each(function()
    log.fast_off()
  end)


  it("edit TYPE_MISMATCH", function()
    local height, width = 4, 3
    local canvas = Canvas:new(nil, height, width)
    local exp = { '   ', '   ', '   ', '   ' }
    assert.same(exp, canvas:draw():toLines())
    local modt = { width = 3, height = 4 }
    local patch = mkpatch(canvas, modt)
    local updated, patch0 = canvas:edit(patch)
    assert.same(false, updated)
    assert.equal(patch, patch0)
    local exp_report = {
      errors = {
        border = {
          id = TYPE_MISMATCH,
          details = {
            et = "string", -- expected type
            sr = "nil"     -- string representation of input value
          }
        }
      },
      updated = {},
      rules = {}
    }
    assert.same(exp_report, patch:get_state())
  end)


  it("edit by patch success", function()
    local height, width = 4, 3
    local canvas = Canvas:new(nil, height, width)

    local modt = { top = 8, width = 10, height = 5, border = 'tblr' }
    local patch = mkpatch(canvas, modt)
    local ok, patch0 = canvas:edit(patch)
    assert.same(true, ok)
    assert.equal(patch0, patch0)
    local exp_patch = {
      width = { old = 3, new = 10 },
      height = { old = 4, new = 5 },
      border = { old = '', new = 'tblr' }
    }
    assert.same(exp_patch, patch)
    local exp = {
      rules = {},
      updated = { height = UPDATED, width = UPDATED, border = UPDATED }
    }
    assert.same(exp, patch:get_state())

    local exp2 = {
      '|--------|',
      '|        |',
      '|        |',
      '|        |',
      '|--------|'
    }
    assert.same(exp2, canvas:draw():toLines())

    -- local modt = { top = 8, width = 10, height = 5, border = 'tblr' }
    local patch2 = mkpatch(canvas, { border = 'LRTB' })
    -- local ok, patch0 = canvas:edit(patch)
    assert.same(true, canvas:edit(patch2))
    local exp_patch2 = {
      height = { old = 5, new = 5 },
      width = { old = 10, new = 10 },
      border = { old = 'tblr', new = 'LRTB' }
    }
    assert.same(exp_patch2, patch2)

    local exp3 = {
      '----------',
      '|        |',
      '|        |',
      '|        |',
      '----------'
    }
    assert.same(exp3, canvas:draw():toLines())
  end)
end)

describe("env.draw.Canvas apply_patch", function()
  after_each(function()
    log.fast_off()
    Canvas.MERGE_COLORS = false
  end)


  local function prepare_canvas()
    local canvas = Canvas:new(nil, 5, 16)
    canvas:add(Line:new(nil, 5, 2, 10, 2, '-'))
    canvas:add(Line:new(nil, 5, 4, 10, 4, '~'))
    canvas:add(Point:new(nil, 7, 3, '>'))
    canvas:add(Point:new(nil, 12, 5, '<'))
    return canvas
  end

  it("modifyElements modify props without order", function()
    local canvas = prepare_canvas()
    local exp = {
      '                ',
      '    ------      ',
      '      >         ',
      '    ~~~~~~      ',
      '           <    ',
    }

    assert.same(exp, canvas:draw():toLines())
    exp = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <',
    }
    assert.same(exp, get_elements_readable_list(canvas))

    local p = Point:new(nil, 12, 5, 'X')
    local idx = 4
    local original_elm = canvas.objects[idx]

    local patch = { elm_modt = p:serialize(), old_idx = idx, new_idx = idx }
    local patches = { patch }

    canvas:apply_patch(patches)
    exp = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: X'
    }
    assert.same(exp, get_elements_readable_list(canvas))
    -- ensure that changes are applied to an existing element instance
    assert.equal(original_elm, canvas.objects[idx])
  end)


  it("modifyElements modify order", function()
    local canvas = prepare_canvas()
    local exp = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <',
    }
    assert.same(exp, get_elements_readable_list(canvas))

    local p = Point:new(nil, 12, 5, 'X')
    local idx = 4
    local original_elm = canvas.objects[idx]

    local patch = { elm_modt = p:serialize(), old_idx = idx, new_idx = idx }
    local patches = { patch }

    canvas:apply_patch(patches)
    exp = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: X'
    }
    assert.same(exp, get_elements_readable_list(canvas))
    -- ensure that changes are applied to an existing element instance
    assert.equal(original_elm, canvas.objects[idx])
  end)
end)

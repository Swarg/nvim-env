-- 13-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local log = require 'alogger'

local Point = require("env.draw.Point");
local IEditable = require("env.draw.IEditable");
local H = require 'env.draw.test_helper'

local mk_cli_editor = H.mk_editor
local edit_transaction = IEditable.cli().edit_transaction
local do_edit_transaction = H.do_edit_transaction

local show_element = H.show_element
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local UPDATED = IEditable.cli().CONSTS.UPDATED_KEY
local TOUCHED = IEditable.cli().CONSTS.TOUCHED_KEY



describe("env.draw.Point cli & serialization", function()
  after_each(function()
    log.fast_off()
  end)

  it("orderedkeys", function()
    local p = Point:new(nil, 1, 1, '+')
    local exp_ordered_keys = {
      { x = 'number' },
      { y = 'number' },
      { color = 'string' }
    }
    assert.same(exp_ordered_keys, p:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = Point:new(nil, 1, 1, '+')
    local e = mk_cli_editor(elm, {})
    local exp_orderedkeys = {
      { k = 'x',     t = 'number' },
      { k = 'y',     t = 'number' },
      { k = 'color', t = 'string' }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local e = mk_cli_editor(Point:new(nil, 1, 1, '+'), {})
    local exp_snapshot = {
      x = { s = '1', o = 1 },
      y = { s = '1', o = 1 },
      color = { s = '+', o = '+' }
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  -- instead cli_prompt
  it("build_prompt_oneliner", function()
    local e = mk_cli_editor(Point:new(nil, 1, 1, '+'), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x y color', -- prompt
      '1 1 +'      -- def_line to edit
    }
    assert.same(exp, { prompt, def_line })
  end)

  it("build_prompt_oneliner serialize_data cli = true (w & h)", function()
    local opts = { serialize_data = { cli = true } }
    local e = mk_cli_editor(Point:new(nil, 1, 1, '+'), opts)
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = { 'x y color', '1 1 +' }
    assert.same(exp, { prompt, def_line })
  end)

  it("clieo constants", function()
    assert.same(0, TOUCHED)
    assert.same(1, UPDATED)
  end)

  it("edit_transaction unchanged", function()
    local logs = {}
    local elm = Point:new(nil, 1, 1, '+')
    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- callback to show prompt to user and ask changed input
        -- unchanged
        return line
      end
    })
    local exp_log_entry = { 'x y color', '1 1 +' }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })
  end)


  it("edit_transaction updated", function()
    local logs = {}
    local elm = Point:new(nil, 1, 1, '+')

    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 2 @'
      end
    })
    local exp_log_entry = { 'x y color', '1 1 +' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      x = { new = 2, old = 1 },
      y = { new = 2, old = 1 },
      color = { new = '@', old = '+' }
    }
    assert.same({ true, exp_patch }, { ok, ret })
  end)


  it("edit_transaction updated via helper (same)", function()
    local logs = {}
    local elm = Point:new(nil, 1, 1, '+')

    local input_line = '2 2 @'
    local ok, ret = do_edit_transaction(elm, { logs = logs }, input_line)

    local exp_log_entry = { 'x y color', '1 1 +' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      x = { new = 2, old = 1 },
      y = { new = 2, old = 1 },
      color = { new = '@', old = '+' }
    }
    assert.same({ true, exp_patch }, { ok, ret })
  end)

  --

  it("edit_transaction fail bad input", function()
    local elm = Point:new(nil, 1, 1, '+')

    local input_line = '1'
    local ok, ret = do_edit_transaction(elm, {}, input_line)
    local exp = { false, 'expected at least: 3 values got: 1' }
    assert.same(exp, { ok, ret })
  end)


  -----------------------------------------------------------------------------


  it("edit via patch", function()
    local point = Point:new(nil, 2, 2, '+')
    local exp_obj_before = 'Point 2:2 color: +'
    assert.same(exp_obj_before, v2s(point))
    local exp_before = {
      '                        ',
      ' +                      ',
      '                        '
    }
    assert.same(exp_before, show_element(point, 24, 3))


    local patch = {
      x = { new = 4, old = 2 },
      y = { new = 2, old = 2 },
      color = { new = '@', old = '+' }
    }
    local ok, ret = point:edit(patch)

    local exp_update_report = {
      updated = {
        x = UPDATED, color = UPDATED
      }
    }
    assert.same({ true, exp_update_report }, { ok, ret:get_state() })

    local exp_obj = 'Point 4:2 color: @'
    assert.same(exp_obj, v2s(point))
    local exp_after = {
      '                        ',
      '   @                    ',
      '                        '
    }
    assert.same(exp_after, show_element(point, 24, 3))
  end)

  it("edit_transaction", function()
    local point = Point:new(nil, 2, 2, '+')
    local exp_obj_before = 'Point 2:2 color: +'
    assert.same(exp_obj_before, v2s(point))
    local exp_before = {
      '                        ',
      ' +                      ',
      '                        '
    }
    assert.same(exp_before, show_element(point, 24, 3))

    local called = false
    -- spy
    point.edit = function(self, patch)
      called = true
      return IEditable.edit(self, patch)
    end

    local input_line = '4 2 @'
    local ok, ret = do_edit_transaction(point, {}, input_line)
    local exp_patch = {
      x = { new = 4, old = 2 },
      y = { new = 2, old = 2 },
      color = { new = '@', old = '+' }
    }
    assert.same({ true, exp_patch }, { ok, ret })
    assert.same(true, called)

    local exp_obj = 'Point 4:2 color: @'
    assert.same(exp_obj, v2s(point))
    local exp_after = {
      '                        ',
      '   @                    ',
      '                        '
    }
    assert.same(exp_after, show_element(point, 24, 3))
  end)

  -----------------------------------------------------------------------------
end)

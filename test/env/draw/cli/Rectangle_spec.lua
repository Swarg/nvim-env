-- 12-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local class = require("oop.class")
local log = require 'alogger'

local Rectangle = require "env.draw.Rectangle"
local IEditable = require "env.draw.IEditable"
local H = require 'env.draw.test_helper'

local mk_cli_editor = H.mk_editor
local edit_transaction = IEditable.cli().edit_transaction
local do_edit_transaction = H.do_edit_transaction

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local show_element = H.show_element
local UPDATED = IEditable.cli().CONSTS.UPDATED_KEY


describe("env.draw.Rectangle", function()
  after_each(function()
    log.fast_off()
  end)

  it("orderedkeys", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    local exp = {
      { x1 = 'number' },
      { y1 = 'number' },
      { x2 = 'number' },
      { y2 = 'number' },
      { hline = 'string' },
      { vline = 'string' },
      { corners = 'string' },
      { bg = 'string' },
      { w = 'number',      _def = IEditable.get_width },
      { h = 'number',      _def = IEditable.get_height }
    }
    assert.same(exp, r:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = Rectangle:new(nil, 1, 1, 8, 8)
    local e = mk_cli_editor(elm, {})
    local exp_orderedkeys = {
      { k = 'x1',      t = 'number' },
      { k = 'y1',      t = 'number' },
      { k = 'x2',      t = 'number' },
      { k = 'y2',      t = 'number' },
      { k = 'hline',   t = 'string' },
      { k = 'vline',   t = 'string' },
      { k = 'corners', t = 'string' },
      { k = 'bg',      t = 'string' },
      { k = 'w',       t = 'number', d = IEditable.get_width },
      { k = 'h',       t = 'number', d = IEditable.get_height }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local e = mk_cli_editor(Rectangle:new(nil, 1, 1, 8, 8), {})
    local exp_snapshot = {
      -- s means string representation of the value, o - is original value
      x1 = { s = '1', o = 1 },
      y1 = { s = '1', o = 1 },
      x2 = { s = '8', o = 8 },
      y2 = { s = '8', o = 8 },
      vline = { s = '|', o = '|' },
      hline = { s = '-', o = '-' },
      corners = { s = '++++', o = '++++' },
      bg = { s = '', o = '' },
      h = { s = '8', o = 8 },
      w = { s = '8', o = 8 },
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  -- instead cli_prompt
  it("build_prompt_oneliner", function()
    local e = mk_cli_editor(Rectangle:new(nil, 1, 1, 8, 8), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 hline vline corners bg w h',
      '1 1 8 8 - | ++++ "" 8 8'
    }
    assert.same(exp, { prompt, def_line })
  end)

  -- idea: add phantom fields to quickly and easy change the size of an
  -- object not by coordinates, but by height and width
  it("build_prompt_oneliner serialize_data cli = true (w & h)", function()
    local opts = { serialize_data = { cli = true } }
    local e = mk_cli_editor(Rectangle:new(nil, 1, 1, 8, 8), opts)
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 hline vline corners bg w h',
      '1 1 8 8 - | ++++ "" 8 8'
    }
    assert.same(exp, { prompt, def_line })
  end)

  -- transaction to edit object

  it("edit_transaction unchanged", function()
    local logs = {}
    local elm = Rectangle:new(nil, 1, 1, 8, 8)
    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- callback to show prompt to user and ask changed input
        -- unchanged
        return line
      end
    })
    local exp_log_entry = {
      'x1 y1 x2 y2 hline vline corners bg w h',
      '1 1 8 8 - | ++++ "" 8 8'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })
  end)

  it("edit_transaction updated", function()
    local logs = {}
    local elm = Rectangle:new(nil, 1, 1, 8, 8)

    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '1 1 16 8 "-" "|" "++++" "" 8 8'
      end
    })
    local exp_log_entry = {
      'x1 y1 x2 y2 hline vline corners bg w h',
      '1 1 8 8 - | ++++ "" 8 8'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = {
      x1 = { new = 1, old = 1 },
      y1 = { new = 1, old = 1 },
      x2 = { new = 16, old = 8 },
      y2 = { new = 8, old = 8 },
      bg = { new = '', old = '' },
      vline = { new = '|', old = '|' },
      hline = { new = '-', old = '-' },
      corners = { new = '++++', old = '++++' },
      w = { old = 8, new = 8 },
      h = { old = 8, new = 8 },
    }
    assert.same({ true, exp }, { ok, ret })
  end)

  it("edit_transaction updated via helper (same)", function()
    local logs = {}
    local elm = Rectangle:new(nil, 1, 1, 8, 8)

    local input_line = '1 1 16 8 "-" "|" "++++" "" 8 8'
    local ok, ret = do_edit_transaction(elm, { logs = logs }, input_line)

    local exp_log_entry = {
      'x1 y1 x2 y2 hline vline corners bg w h',
      '1 1 8 8 - | ++++ "" 8 8'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = {
      x1 = { new = 1, old = 1 },
      y1 = { new = 1, old = 1 },
      x2 = { new = 16, old = 8 },
      y2 = { new = 8, old = 8 },
      bg = { new = '', old = '' },
      vline = { new = '|', old = '|' },
      hline = { new = '-', old = '-' },
      corners = { new = '++++', old = '++++' },
      w = { old = 8, new = 8 },
      h = { old = 8, new = 8 },
    }
    assert.same({ true, exp }, { ok, ret })
  end)

  --

  it("edit_transaction fail bad input", function()
    local elm = Rectangle:new(nil, 1, 1, 8, 8)

    local input_line = '1'
    local ok, ret = do_edit_transaction(elm, {}, input_line)
    local exp = 'expected at least: 10 values got: 1'
    assert.same({ false, exp }, { ok, ret })
  end)


  -- edit_transaction

  it("edit element", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    -- before
    local exp_before = {
      '+------+',
      '|      |',
      '|      |',
      '|      |',
      '|      |',
      '|      |',
      '|      |',
      '+------+' }
    assert.same(exp_before, show_element(r, 8, 8))
    assert.same('Rectangle (1:1 8:8) (8x8) bg: ""', v2s(r))

    -- edit
    local opts = { logs = {} }
    local input_line = '2 2 7 7 - | ++++ "" 8 8'

    local ok, errmsg_or_patch = do_edit_transaction(r, opts, input_line)
    assert.is_table(errmsg_or_patch)
    ---@cast errmsg_or_patch table

    -- after applying the patch
    local exp_state = {
      updated = { y2 = UPDATED, x1 = UPDATED, y1 = UPDATED, x2 = UPDATED },
      rules = {}
    }
    assert.same({ true, exp_state }, { ok, errmsg_or_patch:get_state() })

    assert.same('Rectangle (2:2 7:7) (6x6) bg: ""', v2s(r))
    local exp_after = {
      '        ',
      ' +----+ ',
      ' |    | ',
      ' |    | ',
      ' |    | ',
      ' |    | ',
      ' +----+ ',
      '        ' }
    assert.same(exp_after, show_element(r, 8, 8))

    -- from ui_read_line
    local exp = {
      {
        'x1 y1 x2 y2 hline vline corners bg w h',
        '1 1 8 8 - | ++++ "" 8 8'
      }
    }
    assert.same(exp, opts.logs)
  end)


  it("edit by width", function()
    local r = Rectangle:new(nil, 3, 2, 7, 5)
    local exp_before = {
      '                ',
      '  +---+         ',
      '  |   |         ',
      '  |   |         ',
      '  +---+         ',
      '                ' }
    assert.same(exp_before, show_element(r, 16, 6))

    -- def_line: '3 2 7 5 - | ++++ "" 5 4'
    local input_line = '3 2 7 5 - | ++++ "" 10 4'
    local opts = { serialize_data = { cli = true } }

    local ok, errmsg_or_patch = do_edit_transaction(r, opts, input_line)

    assert.is_true(ok)
    assert.is_table(errmsg_or_patch)
    local patch = errmsg_or_patch ---@cast patch table

    local exp = {
      updated = { w = UPDATED }, data = { cli = true }, rules = {}
    }
    assert.same(exp, patch:get_state())
    local exp_after = {
      '                ',
      '  +--------+    ',
      '  |        |    ',
      '  |        |    ',
      '  +--------+    ',
      '                ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit by height", function()
    local r = Rectangle:new(nil, 3, 2, 7, 5)
    local exp_before = {
      '                ',
      '  +---+         ',
      '  |   |         ',
      '  |   |         ',
      '  +---+         ',
      '                ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '3 2 7 5 - | ++++ "" 5 2'
    local opts = { serialize_data = { cli = true } }
    local ok, errmsg_or_patch = do_edit_transaction(r, opts, input_line)

    assert.is_true(ok)
    assert.is_table(errmsg_or_patch)
    local patch = errmsg_or_patch ---@cast patch table

    local exp_box = {
      updated = { h = UPDATED },
      data = { cli = true },
      rules = {}
    }
    assert.same(exp_box, patch:get_state())
    local exp_after = {
      '                ',
      '  +---+         ',
      '  +---+         ',
      '                ',
      '                ',
      '                ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit by height & width", function()
    local r = Rectangle:new(nil, 3, 2, 7, 5)
    local exp_before = {
      '                ',
      '  +---+         ',
      '  |   |         ',
      '  |   |         ',
      '  +---+         ',
      '                ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '3 2 7 5 - | ++++ "" 12 3'
    local opts = { serialize_data = { cli = true } }
    local ok, errmsg_or_patch = do_edit_transaction(r, opts, input_line)

    assert.is_true(ok)
    assert.is_table(errmsg_or_patch)
    local patch = errmsg_or_patch ---@cast patch table

    local exp_box = {
      updated = { w = UPDATED, h = UPDATED },
      data = { cli = true },
      rules = {}
    }
    assert.same(exp_box, patch:get_state())
    local exp_after = {
      '                ',
      '  +----------+  ',
      '  |          |  ',
      '  +----------+  ',
      '                ',
      '                ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit by negative width", function()
    local r = Rectangle:new(nil, 8, 4, 12, 6)
    local exp_before = {
      '                ',
      '                ',
      '                ',
      '       +---+    ',
      '       |   |    ',
      '       +---+    ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '8 4 12 6 - | ++++ "" -5 3'
    local opts = { serialize_data = { cli = true } }
    local ok, _ = do_edit_transaction(r, opts, input_line)
    assert.same(true, ok)

    local exp_after = {
      '                ',
      '                ',
      '                ',
      '   +---+        ',
      '   |   |        ',
      '   +---+        ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit by negative height", function()
    local r = Rectangle:new(nil, 8, 4, 12, 6)
    local exp_before = {
      '                ',
      '                ',
      '                ',
      '       +---+    ',
      '       |   |    ',
      '       +---+    ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '8 4 12 6 - | ++++ "" 5 -3'
    local opts = { serialize_data = { cli = true } }
    local ok, _ = do_edit_transaction(r, opts, input_line)
    assert.same(true, ok)

    local exp_after = {
      '                ',
      '       +---+    ',
      '       |   |    ',
      '       +---+    ',
      '                ',
      '                ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit by negative height & width", function()
    local r = Rectangle:new(nil, 8, 4, 12, 6)
    local exp_before = {
      '                ',
      '                ',
      '                ',
      '       +---+    ',
      '       |   |    ',
      '       +---+    ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '8 4 12 6 - | ++++ "" -5 -3'
    local opts = { serialize_data = { cli = true } }
    local ok, _ = do_edit_transaction(r, opts, input_line)
    assert.same(true, ok)

    local exp_after = {
      '                ',
      '   +---+        ',
      '   |   |        ',
      '   +---+        ',
      '                ',
      '                ' }
    assert.same(exp_after, show_element(r, 16, 6))
  end)


  it("edit style", function()
    local r = Rectangle:new(nil, 8, 4, 12, 6)

    local exp_before = {
      '                ',
      '                ',
      '                ',
      '       +---+    ',
      '       |   |    ',
      '       +---+    ' }
    assert.same(exp_before, show_element(r, 16, 6))

    local input_line = '8 4 12 6 ━ ┃ ┏┓┛┗ "" 5 3'
    local opts = { serialize_data = { cli = true } }
    local ok, _ = do_edit_transaction(r, opts, input_line)
    assert.same(true, ok)

    local exp_after = {
      '                ',
      '                ',
      '                ',
      '       ┏━━━┓    ',
      '       ┃   ┃    ',
      '       ┗━━━┛    '
    }
    assert.same(exp_after, show_element(r, 16, 6))
  end)

  it("desirialize via IEditable", function()
    local serialized = {
      _class = 'env.draw.Rectangle',
      x1 = 2,
      x2 = 4,
      y2 = 4,
      y1 = 2,
      hline = '-',
      vline = '|',
      corners = '+',
      bg = '',
    }

    local rect = IEditable.deserialize(serialized)
    assert.same('env.draw.Rectangle', class.name(rect))
    local exp = 'Rectangle (2:2 4:4) (3x3) bg: ""'
    assert.same(exp, tostring(rect))
  end)
end)

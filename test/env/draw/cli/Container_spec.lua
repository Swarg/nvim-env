-- 15-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local log = require 'alogger'
local Point = require 'env.draw.Point'
-- local Text = require 'env.draw.Text'
-- local Rectangle = require 'env.draw.Rectangle'
local Container = require 'env.draw.Container'
local IEditable = require("env.draw.IEditable")

local H = require 'env.draw.test_helper'
local mk_editor = H.mk_editor
local edit_transaction = IEditable.cli().edit_transaction
-- local do_edit_transaction = H.do_edit_transaction
local UPDATED = IEditable.cli().CONSTS.UPDATED_KEY



describe("env.draw.Container cli", function()
  after_each(function()
    log.fast_off()
  end)

  it("orderedkeys", function()
    local l = Container:new(nil, 1, 2, 8, 2)
    local exp = {
      { x1 = 'number' },
      { y1 = 'number' },
      { x2 = 'number' },
      { y2 = 'number' },
      { inv = 'table' },
      { w = 'number', _def = IEditable.get_width },
      { h = 'number', _def = IEditable.get_height }
    }
    assert.same(exp, l:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = Container:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm, {})
    local exp_orderedkeys = {
      { k = 'x1',  t = 'number' },
      { k = 'y1',  t = 'number' },
      { k = 'x2',  t = 'number' },
      { k = 'y2',  t = 'number' },
      { k = 'inv', t = 'table' },
      { k = 'w',   t = 'number', d = IEditable.get_width },
      { k = 'h',   t = 'number', d = IEditable.get_height }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local elm = Container:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm)
    local exp_snapshot = {
      x1 = { s = '1', o = 1 },
      y1 = { s = '2', o = 2 },
      x2 = { s = '8', o = 8 },
      y2 = { s = '2', o = 2 },
      inv = { s = '::TABLE::', o = {} },
      w = { s = '8', o = 8 },
      h = { s = '1', o = 1 },
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("build_prompt_oneliner", function()
    local elm = Container:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm)
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp, { prompt, def_line })
  end)


  it("edit_transaction unchanged", function()
    local logs = {}
    local elm = Container:new(nil, 1, 2, 8, 2)
    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- callback to show prompt to user and ask changed input
        -- unchanged
        return line
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })
  end)


  it("edit_transaction update x2", function()
    local logs = {}
    local elm = Container:new(nil, 1, 2, 8, 2)
    assert.same('Container (1:2 8:2) (8x1) elms: 0', tostring(elm))

    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '1 2 18 2 ::TABLE:: 8 1'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      true,
      {
        x1 = { new = 1, old = 1 },
        y1 = { new = 2, old = 2 },
        y2 = { new = 2, old = 2 },
        x2 = { new = 18, old = 8 },
        inv = { new = {}, old = {} },
        w = { new = 8, old = 8 },
        h = { new = 1, old = 1 },
      }
    }
    assert.same(exp_patch, { ok, ret })
    assert.same('Container (1:2 18:2) (18x1) elms: 0', tostring(elm))
  end)


  -- by design, inventory - that is, child elements can be edited via the console
  -- using the additional command, "entering" editing by index in the list of
  -- child elements. That's why the inventory doesn't change here yet
  it("edit_transaction update all(except inventory)", function()
    local logs = {}
    local inv = {
      Point:new(nil, 2, 2, '@')
    }
    local elm = Container:new(nil, 1, 2, 8, 2, inv)
    assert.same('Container (1:2 8:2) (8x1) elms: 1', tostring(elm))

    local ok, patch = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 3 42 3 ::TABLE:: 8 1'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp_log_entry, logs[1])

    local exp_point_patch = {
      { _class = "env.draw.Point", x = 2, y = 2, color = "@" }
    }
    local exp_patch = {
      x1 = { new = 2, old = 1 },
      y1 = { new = 3, old = 2 },
      x2 = { new = 42, old = 8 },
      y2 = { new = 3, old = 2 },
      inv = { new = exp_point_patch, old = exp_point_patch },
      w = { new = 8, old = 8 },
      h = { new = 1, old = 1 },
    }
    assert.same({ true, exp_patch }, { ok, patch }) ---@cast patch table
    local exp = {
      rules = {},
      updated = { x1 = UPDATED, y1 = UPDATED, x2 = UPDATED, y2 = UPDATED }
    }
    assert.same(exp, patch:get_state())
    assert.same('Container (2:3 42:3) (41x1) elms: 1', tostring(elm))
  end)


  it("edit_transaction update all", function()
    local logs = {}
    local elm = Container:new(nil, 1, 2, 8, 2)
    assert.same('Container (1:2 8:2) (8x1) elms: 0', tostring(elm))

    local ok, patch = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 3 42 3 ::TABLE:: 8 1'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      x1 = { new = 2, old = 1 },
      y1 = { new = 3, old = 2 },
      x2 = { new = 42, old = 8 },
      y2 = { new = 3, old = 2 },
      inv = { new = {}, old = {} },
      w = { new = 8, old = 8 },
      h = { new = 1, old = 1 },
    }
    assert.same({ true, exp_patch }, { ok, patch }) ---@cast patch table
    local exp = {
      rules = {},
      updated = { x1 = UPDATED, y1 = UPDATED, x2 = UPDATED, y2 = UPDATED }
    }
    assert.same(exp, patch:get_state())
    assert.same('Container (2:3 42:3) (41x1) elms: 0', tostring(elm))
  end)


  it("edit patch with childs modify invetory", function()
    local inv = {
      Point:new(nil, 1, 1, '-')
    }
    local container = Container:new(nil, 1, 2, 8, 2, inv)
    local exp = [[
Container (1:2 8:2) (8x1) elms: 1 inv:
 1: Point 1:1 color: -
]]
    assert.same(exp, container:__tostring(true))

    local point_old = {
      { _class = "env.draw.Point", x = 1, y = 1, color = "-" }
    }
    local point_patch_new = {
      { _class = "env.draw.Point", x = 2, y = 2, color = "@" }
    }
    local patch = {
      x1 = { new = 2, old = 1 },
      y1 = { new = 3, old = 2 },
      x2 = { new = 42, old = 8 },
      y2 = { new = 3, old = 2 },
      inv = { new = point_patch_new, old = point_old },
      w = { new = 8, old = 8 },
      h = { new = 1, old = 1 },
    }

    local updated, _ = container:edit(patch) -- workload

    assert.same(true, updated)
    local exp_report = {
      updated = {
        x1 = UPDATED, y1 = UPDATED, x2 = UPDATED, y2 = UPDATED, inv = UPDATED
      }
    }
    assert.same(exp_report, patch:get_state())
    local exp_after = [[
Container (2:3 42:3) (41x1) elms: 1 inv:
 1: Point 2:2 color: @
]]
    assert.same(exp_after, container:__tostring(true))
  end)

  it("edit_transaction update all", function()
    local logs = {}
    local elm = Container:new(nil, 1, 2, 8, 2)
    assert.same('Container (1:2 8:2) (8x1) elms: 0', tostring(elm))

    local ok, patch = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 3 42 3 ::TABLE:: 8 1'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 inv w h', '1 2 8 2 ::TABLE:: 8 1' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      x1 = { new = 2, old = 1 },
      y1 = { new = 3, old = 2 },
      x2 = { new = 42, old = 8 },
      y2 = { new = 3, old = 2 },
      inv = { new = {}, old = {} },
      w = { new = 8, old = 8 },
      h = { new = 1, old = 1 },
    }
    assert.same({ true, exp_patch }, { ok, patch }) ---@cast patch table
    local exp = {
      rules = {},
      updated = { x1 = UPDATED, y1 = UPDATED, x2 = UPDATED, y2 = UPDATED }
    }
    assert.same(exp, patch:get_state())
    assert.same('Container (2:3 42:3) (41x1) elms: 0', tostring(elm))
  end)
end)

-- 14-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local log = require 'alogger'

local Text = require 'env.draw.Text'
local IEditable = require 'env.draw.IEditable'
local H = require 'env.draw.test_helper'

local mk_cli_editor = H.mk_editor
local edit_transaction = IEditable.cli().edit_transaction
local do_edit_transaction = H.do_edit_transaction

-- local show_element = H.show_element
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local UPDATED = IEditable.cli().CONSTS.UPDATED_KEY



describe("env.draw.Text cli & serialization", function()
  after_each(function()
    log.fast_off()
  end)

  it("orderedkeys", function()
    local p = Text:new(nil, 1, 1, 'text')
    local exp_ordered_keys = {
      { x1 = 'number' },
      { y1 = 'number' },
      { x2 = 'number' },
      { y2 = 'number' },
      { KeepRange = 'bool', _def = false },
      { UseEscape = 'bool', _def = true },
      { text = 'string' }
    }
    assert.same(exp_ordered_keys, p:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = Text:new(nil, 1, 1, 'text')
    local e = mk_cli_editor(elm, {})
    local exp_orderedkeys = {
      { t = 'number',  k = 'x1' },
      { t = 'number',  k = 'y1' },
      { t = 'number',  k = 'x2' },
      { t = 'number',  k = 'y2' },
      { t = 'boolean', k = 'KeepRange', d = false },
      { t = 'boolean', k = 'UseEscape', d = true },
      { t = 'string',  k = 'text' }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'text'), {})
    local exp_snapshot = {
      y2 = { o = 1, s = '1' },
      x1 = { o = 1, s = '1' },
      y1 = { o = 1, s = '1' },
      x2 = { o = 4, s = '4' },
      text = { o = 'text', s = 'text' },
      KeepRange = { o = false, s = '-' },
      UseEscape = { o = true, s = '+' },
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  -- instead cli_prompt
  it("build_prompt_oneliner", function()
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'text'), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 4 1 - + text'
    }
    assert.same(exp, { prompt, def_line })
  end)

  it("build_prompt_oneliner", function()
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'the text'), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 8 1 - + "the text"'
    }
    assert.same(exp, { prompt, def_line })
  end)


  it("build_prompt_oneliner with phantom fields cli = true", function()
    local opts = { serialize_data = { cli = true } }
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'text'), opts)
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 4 1 - + text'
    }
    assert.same(exp, { prompt, def_line })
  end)

  it("parse user input into patch", function()
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'the text'), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 8 1 - + "the text"'
    }
    assert.same(exp, { prompt, def_line })
    local input = '1 1 8 1 - + "NEW TEXT"'
    local ok, patch = e:parse_input(prompt, input)
    assert.same(true, ok)
    local exp_patch = {
      y2 = { old = 1, new = 1 },
      x1 = { old = 1, new = 1 },
      y1 = { old = 1, new = 1 },
      x2 = { old = 8, new = 8 },
      UseEscape = { old = true, new = true },
      KeepRange = { old = false, new = false },
      text = { old = 'the text', new = 'NEW TEXT' }
    }
    assert.same(exp_patch, patch) ---@cast patch table
    assert.same({ rules = {}, updated = {} }, patch:get_state())
    -- this patch can by applyed into Text:edit()
  end)

  it("parse user input into patch fail - no quotes", function()
    local e = mk_cli_editor(Text:new(nil, 1, 1, 'the text'), {})
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 8 1 - + "the text"'
    }
    assert.same(exp, { prompt, def_line })
    local input = '1 1 8 1 - + FORGET QUOTES'
    local ok, patch = e:parse_input(prompt, input)
    assert.same(false, ok)
    local exp_patch = 'too many values expected: 7 got: 8'
    assert.same(exp_patch, patch)
  end)


  it("edit_transaction unchanged", function()
    local logs = {}
    local elm = Text:new(nil, 1, 1, 'text')
    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- callback to show prompt to user and ask changed input
        -- unchanged
        return line
      end
    })
    local exp_log_entry = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 4 1 - + text'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })
  end)


  it("edit_transaction updated", function()
    local logs = {}
    local elm = Text:new(nil, 1, 1, 'text')

    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 2 @'
      end
    })
    local exp_log_entry = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 4 1 - + text'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'expected at least: 7 values got: 3' }
    assert.same(exp, { ok, ret })
  end)


  it("edit_transaction updated via helper (same)", function()
    local logs = {}
    local elm = Text:new(nil, 1, 1, 'text')

    local input_line = '1 1 4 1 - + TEXT'
    local ok, ret = do_edit_transaction(elm, { logs = logs }, input_line)

    local exp_log_entry = {
      'x1 y1 x2 y2 KeepRange UseEscape text',
      '1 1 4 1 - + text'
    }
    assert.same(exp_log_entry, logs[1])

    local exp = {
      true,
      {
        y2 = { old = 1, new = 1 },
        x1 = { old = 1, new = 1 },
        y1 = { old = 1, new = 1 },
        x2 = { old = 4, new = 4 },
        text = { old = 'text', new = 'TEXT' },
        KeepRange = { old = false, new = false },
        UseEscape = { old = true, new = true },
      }
    }
    assert.same(exp, { ok, ret })
  end)

  --

  it("edit_transaction fail bad input", function()
    local elm = Text:new(nil, 1, 1, 'text')

    local input_line = '1'
    local ok, ret = do_edit_transaction(elm, {}, input_line)
    local exp = { false, 'expected at least: 7 values got: 1' }
    assert.same(exp, { ok, ret })
  end)

  it("edit_transaction fail bad input bad_type", function()
    local elm = Text:new(nil, 1, 1, 'text')

    local input_line = 'a b c d - + 0'

    -- require'alogger'.fast_setup(nil, 0)
    local ok, ret = do_edit_transaction(elm, {}, input_line)
    local exp = {
      false,
      "key:'x2' expected value with type: number, got: nil c\n" ..
      "key:'y2' expected value with type: number, got: nil d\n" ..
      "key:'y1' expected value with type: number, got: nil b\n" ..
      "key:'x1' expected value with type: number, got: nil a"
    }
    assert.same(exp, { ok, ret })
  end)


  --------------------------------------------------------------------------------



  it("edit has changes", function()
    local elm = Text:new(nil, 1, 1, 'the text')
    local exp = 'Text (1:1 8:1) (8x1) "the text"'
    assert.same(exp, v2s(elm))

    local input_line = '1 1 8 1 - + "the\ntext"'
    local ok, ret = do_edit_transaction(elm, {}, input_line) -- elm:edit(patch)
    local exp_patch = {
      x1 = { old = 1, new = 1 },
      y1 = { old = 1, new = 1 },
      y2 = { old = 1, new = 1 },
      x2 = { old = 8, new = 8 },
      KeepRange = { old = false, new = false },
      UseEscape = { old = true, new = true },
      text = { old = 'the text', new = "the\ntext" }
    }
    assert.same({ true, exp_patch }, { ok, ret })
    local patch = ret ---@cast patch table
    assert.same({ rules = {}, updated = { text = UPDATED } }, patch:get_state())

    local exp2 = "Text (1:1 8:1) (8x1) \"the\ntext\""
    assert.same(exp2, v2s(elm))
  end)

  it("edit apply patch", function()
    local elm = Text:new(nil, 1, 1, 'the text')
    local patch = {
      x1 = { old = 1, new = 1 },
      y1 = { old = 1, new = 1 },
      y2 = { old = 1, new = 1 },
      x2 = { old = 8, new = 8 },
      KeepRange = { old = false, new = false },
      UseEscape = { old = true, new = true },
      text = { old = 'the text', new = "the\ntext" }
    }
    local updated, patch0 = elm:edit(patch)
    assert.same(true, updated)
    assert.equal(patch, patch0)
    assert.same({ updated = { text = UPDATED } }, patch:get_state())

    local exp2 = "Text (1:1 8:1) (8x1) \"the\ntext\""
    assert.same(exp2, v2s(elm))
  end)


  it("edit has changes", function()
    local elm = Text:new(nil, 1, 1, 'the\ntext')
    assert.same("Text (1:1 4:2) (4x2) \"the\ntext\"", v2s(elm))

    local input_line = '1 1 8 1 - - "the\ntext"'
    local ok, patch = do_edit_transaction(elm, {}, input_line)
    assert.same(true, ok) ---@cast patch table
    local exp = {
      rules = {},
      updated = { x2 = UPDATED, UseEscape = UPDATED, y2 = UPDATED }
    }
    assert.same(exp, patch:get_state())

    assert.same("Text (1:1 8:1) (8x1) \"the\ntext\"", v2s(elm))

    local line = 'a\nb'
    assert.same('a\nb', line)
    assert.same("a\nb", line:gsub('\n', "\n"))
  end)
end)

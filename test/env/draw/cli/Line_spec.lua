-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local log = require 'alogger'
local Line = require("env.draw.Line");
local IEditable = require("env.draw.IEditable");
local H = require 'env.draw.test_helper'

local mk_editor = H.mk_editor
local edit_transaction = IEditable.cli().edit_transaction
local UPD = IEditable.cli().CONSTS.UPDATED_KEY


describe("env.draw.Line cli & serialization", function()
  after_each(function()
    log.fast_off()
  end)

  it("orderedkeys", function()
    local l = Line:new(nil, 1, 2, 8, 2)
    local exp = {
      { x1 = 'number' },
      { y1 = 'number' },
      { x2 = 'number' },
      { y2 = 'number' },
      { color = 'string' }
    }
    assert.same(exp, l:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = Line:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm, {})
    local exp_orderedkeys = {
      { k = 'x1',    t = 'number' },
      { k = 'y1',    t = 'number' },
      { k = 'x2',    t = 'number' },
      { k = 'y2',    t = 'number' },
      { k = 'color', t = 'string' }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local elm = Line:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm)
    local exp_snapshot = {
      x1 = { s = '1', o = 1 },
      y1 = { s = '2', o = 2 },
      x2 = { s = '8', o = 8 },
      y2 = { s = '2', o = 2 },
      color = { s = '#', o = '#' }
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("build_prompt_oneliner", function()
    local elm = Line:new(nil, 1, 2, 8, 2)
    local e = mk_editor(elm)
    local prompt, def_line = e:build_prompt_oneliner()
    local exp = { 'x1 y1 x2 y2 color', '1 2 8 2 #' }
    assert.same(exp, { prompt, def_line })
  end)

  it("edit_transaction unchanged", function()
    local logs = {}
    local elm = Line:new(nil, 1, 2, 8, 2)
    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        -- callback to show prompt to user and ask changed input
        -- unchanged
        return line
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 color', '1 2 8 2 #' }
    assert.same(exp_log_entry, logs[1])

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })
  end)

  it("edit_transaction update x2", function()
    local logs = {}
    local elm = Line:new(nil, 1, 2, 8, 2)
    assert.same('Line (1:2 8:2) color: #', tostring(elm))

    local ok, ret = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '1 2 18 2 #'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 color', '1 2 8 2 #' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      true,
      {
        x1 = { new = 1, old = 1 },
        y1 = { new = 2, old = 2 },
        x2 = { new = 18, old = 8 },
        y2 = { new = 2, old = 2 },
        color = { new = '#', old = '#' }
      }
    }
    assert.same(exp_patch, { ok, ret })
    assert.same('Line (1:2 18:2) color: #', tostring(elm))
  end)

  it("edit_transaction update all", function()
    local logs = {}
    local elm = Line:new(nil, 1, 2, 8, 2)
    assert.same('Line (1:2 8:2) color: #', tostring(elm))

    local ok, patch = edit_transaction(elm, {
      ui_read_line = function(prompt, line)
        logs[1] = { prompt, line }
        return '2 3 42 3 -'
      end
    })
    local exp_log_entry = { 'x1 y1 x2 y2 color', '1 2 8 2 #' }
    assert.same(exp_log_entry, logs[1])

    local exp_patch = {
      x1 = { new = 2, old = 1 },
      y1 = { new = 3, old = 2 },
      x2 = { new = 42, old = 8 },
      y2 = { new = 3, old = 2 },
      color = { new = '-', old = '#' }
    }
    assert.same({ true, exp_patch }, { ok, patch }) ---@cast patch table
    local exp = {
      updated = { x1 = UPD, y1 = UPD, x2 = UPD, y2 = UPD, color = UPD },
      rules = {}
    }
    assert.same(exp, patch:get_state())
    assert.same('Line (2:3 42:3) color: -', tostring(elm))
  end)
end)

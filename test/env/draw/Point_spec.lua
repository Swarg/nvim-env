-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local class = require("oop.class")

local Point = require("env.draw.Point");
local Canvas = require("env.draw.Canvas");
local IEditable = require("env.draw.IEditable");

local SH = class.serialize_helper
local new_cname2id, invert_map = SH.new_cname2id, SH.invert_map
local mkpatch = IEditable.cli().TESTING.mk_patch



describe("env.draw.Point", function()
  it("new", function()
    local p = Point:new(nil, 1, 2, '+')
    local exp = {
      x = 1,
      y = 2,
      color = '+'
    }
    assert.same(exp, p)
  end)


  it("draw", function()
    local canvas = Canvas:new(nil, 1, 8)
    --             123456789
    assert.same({ '        ' }, canvas:toLines())

    canvas:add(Point:new(nil, 1, 1, '+')):draw()
    assert.same({ '+       ' }, canvas:toLines())

    canvas:add(Point:new(nil, 8, 1, 'o')):draw()
    assert.same({ '+      o' }, canvas:toLines())
  end)

  it("serialize", function()
    local point = Point:new(nil, 2, 2, '+')
    assert.same('env.draw.Point', class.name(point))

    local exp = { _clsid = 2, color = '+', y = 2, x = 2 }
    local opts = { cname2id = new_cname2id() }
    assert.same(exp, point:serialize(opts))
    local exp2 = { ['oop.Object'] = 1, ['env.draw.Point'] = 2, next_id = 3 }
    assert.same(exp2, opts.cname2id)
  end)

  it("desirialize via Point Class", function()
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }

    local rp2 = Point.deserialize(serialized)
    assert.same('env.draw.Point', class.name(rp2))
  end)

  it("desirialize 0 tooling Object.fromArray", function()
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }
    local p = Point:fromArray(serialized)
    assert.same('env.draw.Point', class.name(p))
  end)

  -- fetch orderedkeys, build snapshot of current state to take old value into
  -- created patch.
  it("tooling mkpatch for given object", function()
    local p = Point:new(nil, 1, 1, '-')

    local exp_patch = {
      x = { new = 3, old = 1 },
      y = { new = 4, old = 1 },
      color = { new = '@', old = '-' }
    }
    assert.same(exp_patch, mkpatch(p, { x = 3, y = 4, color = '@' }))
  end)

  it("desirialize+edit 0 tooling Object.fromArray", function()
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }
    local p = Point:fromArray(serialized)
    assert.same('env.draw.Point', class.name(p))
    local patch = {
      x = { old = 2, new = 3 },
      y = { old = 2, new = 4 },
      color = { old = '+', new = '@' }
    }
    p:edit(patch)
    assert.same({ x = 3, color = '@', y = 4 }, p)
  end)

  it("desirialize via Point Class", function()
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }
    local rp2 = Point.deserialize(serialized)
    assert.same('env.draw.Point', class.name(rp2))

    local patch = {
      x = { old = 2, new = 4 },
      y = { old = 2, new = 5 },
      color = { old = '+', new = '@' }
    }
    rp2:edit(patch)
    assert.same({ x = 4, y = 5, color = '@' }, rp2)
  end)

  it("desirialize IEditable", function()
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }
    local rp2 = IEditable.deserialize(serialized)
    assert.same('env.draw.Point', class.name(rp2))

    rp2:edit(mkpatch(rp2, { x = 4, y = 5, color = '#' }))
    assert.same({ x = 4, color = '#', y = 5 }, rp2)
  end)

  it("desirialize classmap", function()
    local id2cname = invert_map({ ['oop.Object'] = 1, ['env.draw.Point'] = 2 })
    local serialized = { _class = 'env.draw.Point', x = 2, y = 2, color = '+' }

    local rp2 = Point.deserialize(serialized, id2cname)
    assert.same('env.draw.Point', class.name(rp2))
    rp2:edit(mkpatch(rp2, { x = 4, y = 5, color = '#' })) -- workload
    assert.same({ x = 4, color = '#', y = 5 }, rp2)
  end)

  it("fmtSerialized", function()
    local p = Point:new(nil, 1, 2, '+')
    local opts = { cname2id = new_cname2id() }
    local arr = p:serialize(opts)
    opts.ident = ''
    assert.same("{_clsid=2,x=1,y=2,color='+'}", p:fmtSerialized(arr, 0, opts))

    opts = { ident = ' ' }
    local exp = "{_clsid = 2, x = 1, y = 2, color = '+'}"
    assert.same(exp, p:fmtSerialized(arr, 0, opts))
  end)
end)


--

describe("env.draw.Point Geometry", function()
  it("isIntersects", function()
    local c = Canvas:new(nil, 4, 12)
    local _, p = c:add(Point:new(nil, 1, 1)) ---@cast p env.draw.Point
    -- 123456789012
    local exp = {
      '+           ',
      '            ',
      '            ',
      '            '
    }
    assert.same(exp, c:draw():toLines())
    assert.same('Point 1:1 color: +', tostring(p))
    assert.same(true, p:isIntersects(1, 1, 1, 1))
    assert.same(false, p:isIntersects(1, 1, 1, 0))
    assert.same(false, p:isIntersects(1, 1, 0, 1))
    assert.same(true, p:isIntersects(1, 1, 12, 4))
    assert.same(false, p:isIntersects(1, 2, 12, 4))
    assert.same(false, p:isIntersects(2, 1, 12, 4))
    assert.same(false, p:isIntersects(2, 2, 12, 4))
    p.x, p.y = 7, 3
    local exp2 = {
      '            ',
      '            ',
      '      +     ',
      '            '
    }
    assert.same(exp2, c:draw():toLines())
    assert.same(true, p:isIntersects(1, 1, 12, 4))
    assert.same(true, p:isIntersects(1, 1, 12, 3))
    assert.same(true, p:isIntersects(7, 3, 12, 3))
    assert.same(true, p:isIntersects(1, 1, 7, 3))
    assert.same(false, p:isIntersects(1, 1, 12, 2))
  end)
end)

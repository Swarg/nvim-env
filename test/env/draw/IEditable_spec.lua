-- 31-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local Rectangle = require("env.draw.Rectangle")
-- local IEditable = require("env.draw.IEditable")

describe("env.draw.IEditable", function()
  it("fmtSerialized", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    local serialized = r:serialize()
    local opts = {}
    local res = r:fmtSerialized(serialized, 2, opts)
    local exp = "  {_class='env.draw.Rectangle',x1=1,y1=1,x2=8,y2=8,hline='-',vline='|',corners='++++',bg=''}"
    assert.same(exp, res)
  end)
end)

-- 09-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local sys = nvim:get_os()

_G.TEST = true
local log = require('alogger')
local utbl = require("env.draw.utils.tbl");
local Canvas = require("env.draw.Canvas");
local Line = require("env.draw.Line");
local Point = require("env.draw.Point");
local Rectangle = require("env.draw.Rectangle");
local Selection = require("env.draw.ui.Selection");
local M = require("env.draw.state.canvas");

describe("env.draw.state.canvas clipboard", function()
  after_each(function()
    M.clear_all()
    log.fast_off()
    M.clipboard_set_limit(100)
  end)

  local function mkSelection(t, n)
    local items = {
      Rectangle:new(nil, 0, 0, n, n),
      Rectangle:new(nil, 0, 0, n + 1, n + 1),
    }
    return Selection:new(nil, t, items)
  end

  local sel1copy = mkSelection(Selection.TYPE_COPY, 1)
  local sel2copy = mkSelection(Selection.TYPE_COPY, 3)
  local sel3copy = mkSelection(Selection.TYPE_COPY, 5)
  local sel4cut = mkSelection(Selection.TYPE_CUT, 7)
  local sel5cut = mkSelection(Selection.TYPE_CUT, 9)

  it("add_to_clipboard replace old coied selection on copy", function()
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same({ sel1copy }, M.get_clipboard())

    -- replace old copied selection
    assert.same(2, M.clipboard_add_entry(sel2copy))
    assert.same({ sel1copy, sel2copy }, M.get_clipboard())

    -- replace old copied selection
    assert.same(3, M.clipboard_add_entry(sel3copy))
    assert.same({ sel1copy, sel2copy, sel3copy }, M.get_clipboard())
  end)

  it("add_to_clipboard add to prev(copy, cut, cut, copy)", function()
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same({ sel1copy }, M.get_clipboard())

    -- replace old copied selection
    assert.same(2, M.clipboard_add_entry(sel4cut))
    assert.same({ sel1copy, sel4cut }, M.get_clipboard())

    assert.same(3, M.clipboard_add_entry(sel5cut))
    assert.same({ sel1copy, sel4cut, sel5cut }, M.get_clipboard())

    assert.same(4, M.clipboard_add_entry(sel2copy))
    assert.same({ sel1copy, sel4cut, sel5cut, sel2copy }, M.get_clipboard())
  end)

  it("delete_from_clipboard", function()
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same({ sel1copy }, M.get_clipboard())

    assert.same(sel1copy, M.clipboard_delete_entry(1))
    assert.same({}, M.get_clipboard())

    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same(2, M.clipboard_add_entry(sel2copy))
    assert.same(3, M.clipboard_add_entry(sel3copy))

    assert.same(sel2copy, M.clipboard_delete_entry(2))
    assert.same({ sel1copy, sel3copy }, M.get_clipboard())
    assert.same(sel1copy, M.clipboard_delete_entry(-2))
    assert.same(sel3copy, M.clipboard_delete_entry(-1))
  end)

  it("get_from_clipboard sel_type:copy do not remove on get", function()
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same(sel1copy, M.clipboard_get_entry())
    assert.same({ sel1copy }, M.get_clipboard())
  end)

  it("get_from_clipboard sel_type:cut remove on get", function()
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same(2, M.clipboard_add_entry(sel4cut))
    assert.same(3, M.clipboard_add_entry(sel5cut))
    assert.same(sel5cut, M.clipboard_get_entry())
    assert.same(sel4cut, M.clipboard_get_entry())
    assert.same(sel1copy, M.clipboard_get_entry())
    assert.same(sel1copy, M.clipboard_get_entry())
    assert.same({ sel1copy }, M.get_clipboard())
  end)

  it("CLIPBOARD_HISTORY_LIMIT", function()
    M.clipboard_set_limit(3) -- /2 for delete list
    assert.same({}, M.get_clipboard())
    assert.same(1, M.clipboard_add_entry(sel1copy))
    assert.same(2, M.clipboard_add_entry(sel2copy))
    assert.same(3, M.clipboard_add_entry(sel3copy))
    assert.same(3, M.clipboard_add_entry(sel4cut))
    assert.same(3, M.clipboard_add_entry(sel5cut))
    assert.same({ sel3copy, sel4cut, sel5cut }, M.get_clipboard())
  end)
end)


describe("env.draw.state.canvas with emulated FS", function()
  after_each(function()
    M.clear_all()
    log.fast_off()
    nvim:clear_state()
    sys:clear_state(true, true)
    sys.fs:clear_state()
    M.clipboard_set_limit(100)
  end)

  local canvas1_bin = [[
{
  width = 12, height = 5, border = '',
  objects = {
    {_class = "env.draw.Line",color="-",x1=2,x2=10,y1=2,y2=2},
    {_class = "env.draw.Point",color=">",x=4,y=3},
    {_class = "env.draw.Point",color="<",x=5,y=4},
    {_class = "env.draw.Rectangle",bg="",corners="+",hline="-",vline="|",x1=1,x2=12,y1=1,y2=5},
  },
}
]]

  it("tooling load canvas from file", function()
    local fn = '/tmp/c1.clt'
    assert.is_table(Point:new(nil, 1, 1))      -- load class
    assert.is_table(Line:new(nil, 1, 1, 2, 1)) -- load class
    sys:set_file(fn, utbl.split(canvas1_bin, "\n"))
    local ok, canvas = M.load_canvas(fn)
    assert.same(true, ok) ---@cast canvas env.draw.Canvas
    local exp = {
      '+----------+',
      '|--------- |',
      '|  >       |',
      '|   <      |',
      '+----------+'
    }
    assert.same(exp, canvas:draw():toLines())
  end)


  it("add_ui_item update_ui_item inner storage instances", function()
    local fn = '/tmp/c4x12.clt'
    local canvas = Canvas:new(nil, 4, 12)

    -- function M.add_ui_item(canvas, bufnr, lnum, col, fn, draw_cb)
    local frames = {}
    local draw_cb = function(box)
      frames[#frames] = box.item:draw():toLines()
    end
    -- emulate create new canvas in regualr nvim buff (EnvXC canvas new)
    local bufnr, ceBufnr = 1, 2
    local idx, box1 = M.add_ui_item(canvas, bufnr, 0, 0, nil, draw_cb)
    assert.same({ [bufnr] = { [idx] = box1 } }, M.get_ui_items_map())
    assert.same({ [canvas] = { box1 } }, M.get_ui_item_instances())
    assert.same(nil, box1.filename)

    -- open already existed canvas in CanvasEditor(new buffer)
    local idx2, box2 = M.add_ui_item(canvas, ceBufnr, 0, 0, nil, draw_cb)
    local exp = { [bufnr] = { [idx] = box1 }, [ceBufnr] = { [idx2] = box2 } }
    assert.same(exp, M.get_ui_items_map())
    assert.same(nil, box2.filename)

    box2.filename = fn
    local exps = { true, 'Canvas Saved: true /tmp/c4x12.clt' }
    assert.same(exps, { M.save_canvas(box2, nil, true, false, nil) })
    assert.same(fn, box2.filename)
    assert.same(nil, box1.filename)

    sys:set_file_content(fn, canvas1_bin)
    local old_canvas1 = box2.item
    local ok, new_canvas2 = M.load_canvas(fn)
    assert.same(true, ok) ---@cast new_canvas2 env.draw.Canvas
    local expc = {
      '+----------+',
      '|--------- |',
      '|  >       |',
      '|   <      |',
      '+----------+'
    }
    assert.same(expc, new_canvas2:draw():toLines())

    assert.same(true, ok)
    local expc1 = {
      '            ',
      '            ',
      '            ',
      '            '
    }
    assert.same(expc1, canvas:draw():toLines())

    assert.equal(canvas, box1.item)
    assert.same(2, M.update_ui_item(old_canvas1, new_canvas2, fn))
    assert.same(fn, box1.filename)
    assert.equal(new_canvas2, box1.item)

    local exp_updated = {
      '+----------+',
      '|--------- |',
      '|  >       |',
      '|   <      |',
      '+----------+'
    }
    assert.same(exp_updated, box1.item:draw():toLines())
  end)

  it("get_canvas by abs coords", function()
    local canvas = Canvas:new(nil, 4, 12)

    local bufnr = 1
    local idx, box1 = M.add_ui_item(canvas, bufnr, 0, 0, nil, function() end)
    assert.same(1, idx)
    assert.same(canvas, M.get_canvas(bufnr, 1))
    assert.same(canvas, box1.item)
  end)
end)

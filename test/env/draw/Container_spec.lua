-- 06-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local class = require 'oop.class'
local Canvas = require("env.draw.Canvas");
local Line = require 'env.draw.Line'
local Text = require 'env.draw.Text'
local Point = require 'env.draw.Point'
local Rectangle = require 'env.draw.Rectangle'
local IEditable = require("env.draw.IEditable")
local M = require("env.draw.Container");

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id
local invert_map = SH.invert_map


describe("env.draw.Container", function()
  local function show(e, w, h)
    w, h = w or 32, h or 10
    local canvas = Canvas:new(nil, h, w)
    canvas:add(e)
    if e.x1 == 0 then
      e:move(4, 1)
    end
    return canvas:draw():toLines()
  end

  it("new", function()
    local c = M:new(nil, 1, 1, 20, 6)
    c:add(Rectangle:new(nil, 1, 1, 10, 4))
    c:add(Line:new(nil, 2, 2, 9, 2, '='))
    c:add(Text:new(nil, 3, 3, 'Smthg'))
    -- assert.same("", M.deserialize())
    local exp = {
      '+--------+      ',
      '|========|      ',
      '| Smthg  |      ',
      '+--------+      ',
      '                ',
      '                '
    }
    assert.same(exp, show(c, 16, 6))
  end)

  it("serialize deserialize", function()
    local c = M:new(nil, 1, 1, 20, 6)
    c:add(Rectangle:new(nil, 1, 1, 10, 4))
    c:add(Line:new(nil, 2, 2, 9, 2, '='))
    c:add(Text:new(nil, 3, 3, 'Smthg'))

    local exp_s = [[
Container (1:1 20:6) (20x6) elms: 3 inv:
 1: Rectangle (1:1 10:4) (10x4) bg: ""
 2: Line (2:2 9:2) color: =
 3: Text (3:3 7:3) (5x1) "Smthg"
]]
    assert.same(exp_s, M.__tostring(c, true))

    local cname2id = new_cname2id()
    local t = c:serialize({ cname2id = cname2id })
    local exp_classes_map = {
      ['oop.Object'] = 1,
      ['env.draw.Rectangle'] = 2,
      ['env.draw.Line'] = 3,
      ['env.draw.Text'] = 4,
      ['env.draw.Container'] = 5,
      next_id = 6,
    }
    assert.same(exp_classes_map, cname2id)

    local exp = {
      _clsid = 5, -- env.draw.Container
      x1 = 1,
      y1 = 1,
      x2 = 20,
      y2 = 6,
      inv = {
        {
          _clsid = 2, -- Rectangle
          corners = '++++',
          y2 = 4,
          x1 = 1,
          y1 = 1,
          x2 = 10,
          bg = '',
          hline = '-',
          vline = '|',
        },
        { _clsid = 3, x1 = 2, y1 = 2, x2 = 9, color = '=', y2 = 2 }, -- Line
        { _clsid = 4, x1 = 3, y1 = 3, x2 = 7, y2 = 3,      text = 'Smthg' }
      },
    }
    assert.same(exp, t)

    local id2cname = invert_map(cname2id)
    local rc = IEditable.deserialize(t, id2cname) ---@cast rc env.draw.Container
    local exp2 = {
      '+--------+      ',
      '|========|      ',
      '| Smthg  |      ',
      '+--------+      ',
      '                ',
      '                '
    }
    assert.same(exp2, show(rc, 16, 6))
    assert.same(exp_s, M.__tostring(rc, true))
  end)

  local function prepare_container_rlt()
    local container = M:new(nil, 1, 1, 20, 6)
    container:add(Rectangle:new(nil, 1, 1, 10, 4))
    container:add(Line:new(nil, 2, 2, 9, 2, '='))
    container:add(Text:new(nil, 3, 3, 'Smthg'))
    return container
  end

  it("fmtSerialized", function()
    local container = prepare_container_rlt()

    local cname2id = new_cname2id()
    local t = container:serialize({ cname2id = cname2id })
    local opts = { id2cname = invert_map(cname2id) }
    --inspect = { newline = '', indent = '' }}
    local exp = [[
{
  _clsid=5,x1=1,y1=1,x2=20,y2=6,
  inv = {
    {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},
    {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},
    {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},
  }
}
]]
    assert.same(exp, container:fmtSerialized(t, 0, opts) .. "\n")
  end)


  it("fmtSerialized without id2cname by with cname2id", function()
    local container = prepare_container_rlt()

    local opts = { cname2id = new_cname2id() } -- auto invert on fly inside serialize
    local t = container:serialize(opts)
    local exp = [[
  {
    _clsid=5,x1=1,y1=1,x2=20,y2=6,
    inv = {
      {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},
      {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},
      {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},
    }
  }
]]
    assert.same(exp, container:fmtSerialized(t, 2, opts) .. "\n")
  end)

  it("fmtSerialized deep 6", function()
    local container = prepare_container_rlt()

    local opts = { cname2id = new_cname2id() } -- auto invert on fly inside serialize
    local t = container:serialize(opts)
    local exp = [[
      {
        _clsid=5,x1=1,y1=1,x2=20,y2=6,
        inv = {
          {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},
          {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},
          {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},
        }
      }
]]
    assert.same(exp, container:fmtSerialized(t, 6, opts) .. "\n")
  end)

  it("fmtSerialized without id2cname by with cname2id deep 0", function()
    local container = prepare_container_rlt()

    local opts = { cname2id = new_cname2id() }
    local t = container:serialize(opts)
    local exp = [[
{
  _clsid=5,x1=1,y1=1,x2=20,y2=6,
  inv = {
    {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},
    {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},
    {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},
  }
}
]]
    assert.same(exp, container:fmtSerialized(t, 0, opts) .. "\n")
  end)

  it("fmtSerialized force oneliner", function()
    local container = prepare_container_rlt()

    local cname2id = new_cname2id()
    local t = container:serialize({ cname2id = cname2id })
    local opts = {
      id2cname = invert_map(cname2id),
      force_oneliner = true,
    }
    local exp = [[
  {_clsid=5,x1=1,y1=1,x2=20,y2=6,inv = {  {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},  {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},  {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},}}
]]
    assert.same(exp, container:fmtSerialized(t, 2, opts) .. "\n")
  end)

  it("fmtSerialized force oneliner", function()
    local container = prepare_container_rlt()

    local t = container:serialize(nil)
    local opts = {
      id2cname = nil,
      cname2id = nil,
      force_oneliner = true,
    }
    local exp = [[
  {_class='env.draw.Container',x1=1,y1=1,x2=20,y2=6,inv = {  {_class='env.draw.Rectangle',x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},  {_class='env.draw.Line',x1=2,y1=2,x2=9,y2=2,color='='},  {_class='env.draw.Text',x1=3,y1=3,x2=7,y2=3,text='Smthg'},}}
]]
    assert.same(exp, container:fmtSerialized(t, 2, opts) .. "\n")
  end)

  --

  it("find_by_xy xy any tag", function()
    local c = M:new(nil, 1, 1, 20, 6)
    local l1 = c:add(Line:new(nil, 2, 2, 9, 2, '='))
    local l2 = c:add(Line:new(nil, 9, 2, 9, 5, '|'))
    c:add(Point:new(nil, 9, 2, '+'))

    assert.same({ l1, l2 }, c:find_by_xy(Line, 9, 2))
  end)

  it("find_by_xy tag & limit", function()
    local c = M:new(nil, 1, 1, 20, 6)
    local l0 = c:add(Line:new(nil, 15, 2, 9, 2, '~'):tagged('tag'))
    local l1 = c:add(Line:new(nil, 2, 2, 9, 2, '='):tagged('tag'))
    local l2 = c:add(Line:new(nil, 9, 2, 9, 5, '|'))
    c:add(Point:new(nil, 9, 2, '+'))

    assert.same('tag', l1:tag())

    -- find lines without tag
    assert.same({ l2 }, c:find_by_xy(Line, 9, 2, { tag = false }))

    -- find lines with tag
    assert.same({ l0, l1 }, c:find_by_xy(Line, 9, 2, { tag = 'tag' }))

    assert.same({ l0 }, c:find_by_xy(Line, 9, 2, { tag = 'tag', limit = 1 }))
  end)


  it("get_child_at", function()
    local c = M:new(nil, 1, 1, 20, 6)
    local line = c:add(Line:new(nil, 9, 2, 15, 2, '~'))

    assert.same(line, c:get_child_at(9, 2))
    assert.same(nil, c:get_child_at(8, 1))
  end)

  it("remove_child", function()
    local c = M:new(nil, 1, 1, 20, 6)
    local line1 = c:add(Line:new(nil, 10, 1, 15, 1, '1'))
    local line2 = c:add(Line:new(nil, 11, 2, 16, 2, '2'))
    local line3 = c:add(Line:new(nil, 12, 3, 16, 3, '3'))

    assert.same(2, c:indexof(line2))
    assert.same(line2, c:remove_child(line2))

    assert.same(-1, c:indexof(line2))
    local exp = [[
Container (1:1 20:6) (20x6) elms: 2 inv:
 1: Line (10:1 15:1) color: 1
 2: Line (12:3 16:3) color: 3
]]
    assert.same(exp, c:__tostring(true))
    assert.same(line1, c:remove_child(line1))
    assert.same(line3, c:remove_child(line3))
    local exp2 = 'Container (1:1 20:6) (20x6) elms: 0'
    assert.same(exp2, c:__tostring(true))
  end)
end)

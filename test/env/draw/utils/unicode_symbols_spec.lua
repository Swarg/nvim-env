-- 30-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local M = require("env.draw.utils.unicode_symbols");

describe("env.draw.utils.unicode_symbols", function()
  it("get_name_parts", function()
    local f = function(s, words_in_main)
      local main, part = M.get_name_parts(s, words_in_main)
      return tostring(main) .. '|' .. tostring(part)
    end
    assert.same('a|bc', f('abc'))
    assert.same('ab|c', f('abc', 2))
    assert.same('abc|def-gh', f('abc-def-gh'))
    assert.same('abc-def|gh', f('abc-def-gh', 2))
    assert.same('abc|def-', f('abc-def-', 1))
    assert.same('abc-def|', f('abc-def-', 2))
    assert.same('light|horizontal', f('light-horizontal', 1))
    assert.same('light|top-left', f('light-top-left', 1))
    assert.same('light-top|left', f('light-top-left', 2))
  end)

  it("pick_line_part_by", function()
    local f = M.pick_line_part
    assert.same('─', f('light-horizontal'))
    assert.same('─', f('lh'))
    assert.same('│', f('light-vertical'))
    assert.same('│', f('lv'))
    assert.same('┌', f('ltl'))
    assert.same('┐', f('ltr'))
    assert.same('┘', f('lbr'))
    assert.same('└', f('lbl'))

    assert.same('╭', f('Ltl'))
    assert.same('╮', f('Ltr'))
    assert.same('╯', f('Lbr'))
    assert.same('╰', f('Lbl'))
  end)

  it("pick_rect_style original", function()
    local exp = {
      hl = '─',
      vl = '│',
      corners = { '┌', '┐', '┘', '└' },
      bl = '─',
      rl = '│'
    }
    assert.same(exp, M.pick_rect_style('light'))
  end)

  it("pick_rect_style short", function()
    local f = function(n)
      local t = M.pick_rect_style(n)
      return type(t) == 'table' and { t.hl, t.vl, table.concat(t.corners, '') } or t
    end

    assert.same({ '─', '│', '┌┐┘└' }, f('light'))
    assert.same({ '─', '│', '┌┐┘└' }, f('l'))
    assert.same({ '━', '┃', '┏┓┛┗' }, f('heavy'))
    assert.same({ '━', '┃', '┏┓┛┗' }, f('h'))
    assert.same({ '═', '║', '╔╗╝╚' }, f('double'))
    assert.same({ '═', '║', '╔╗╝╚' }, f('d'))
    assert.same({ '-', '|', '++++' }, f('ascii'))
    assert.same({ '-', '|', '++++' }, f('a'))
    assert.same({ '─', '│', '╭╮╯╰' }, f('light_arc'))
    assert.same({ '─', '│', '╭╮╯╰' }, f('L'))
  end)

  it("sorted_keys", function()
    assert.same({ 'a', 'b', 'c' }, M.sorted_keys({ c = 1, b = 2, a = 4 }))
  end)

  it("find_short", function()
    local res = M.find_short(M.LINES_PARTS.__short_parts_names, 'horizontal')
    assert.same(' h|', res)
  end)

  it("build_lines_part_help", function()
    local exp = [[
-|+++++     -  A|ascii
▄▐█▗▖▘▝▀▋   -  B|block_in
▀▋█▛▜▟▙▄▐   -  b|block_out
-|+▔▁▏▕     -  8|block_out8
═║╬╔╗╝╚     -  d|double
⋯⋮ ⋰⋱⋰⋱     -  e|elipsis
━┃╋┏┓┛┗     -  h|heavy
┉┋          -  Q|heavy_quadruple
┅┇          -  T|heavy_triple
─│┼┌┐┘└     -  l|light
─│┼╭╮╯╰     -  L|light_arc
┈┊          -  q|quadruple
┄┆          -  t|triple

Parts:
   h|horizontal
   v|vertical
   c|cross
  tl|top-left
  tr|top-right
  bl|bottom-left
  br|bottom-right
]]
    assert.same(exp, M.build_help_line_parts())
  end)

  it("pick_line_part", function()
    local f = M.pick_line_part
    assert.same('━', f('hh'))
    assert.same('┃', f('hv'))
    assert.same('╋', f('hc'))
    assert.same('┏', f('htl'))
    assert.same('┓', f('htr'))
    assert.same('┛', f('hbr'))
    assert.same('┗', f('hbl'))

    assert.same('━', f('heavy-horizontal'))
    assert.same('┃', f('heavy-vertical'))
    assert.same('╋', f('heavy-cross'))
    assert.same('┏', f('heavy-top-left'))
    assert.same('┓', f('heavy-top-right'))
    assert.same('┛', f('heavy-bottom-right'))
    assert.same('┗', f('heavy-bottom-left'))
    --
    assert.same('█', f('block_in-cross')) -- full
    -- short
    assert.same('▄', f('Bh'))
    assert.same('▐', f('Bv'))
  end)

  it("pick_line_part default is horizontal(top)", function()
    local f = M.pick_line_part
    local exp = '─'
    assert.same(exp, f('l'))
  end)


  it("build_help_rect", function()
    local exp = [[
-|++++   a|ascii
▄▐▗▖▘▝   B|block_in
▀▋▛▜▟▙   b|block_out
═║╔╗╝╚   d|double
━┃┏┓┛┗   h|heavy
─│┌┐┘└   l|light
─│╭╮╯╰   L|light_arc]]
    assert.same(exp, M.build_help_rect())
  end)

  it("build_help_arrow_head", function()
    local exp = [[
🠜🠝🠞🠟        -  a|acute
⮈⮉⮊⮋        -  c|circle
⪫⩛⪪⩚        -  f|crowsfoot
🡄🡅🡆🡇        -  h|heavy
🠴🠵🠶🠷        -  H|heavy2
🠈🠉🠊🠋        -  o|obtuse
⯇⯅⯈⯆        -  t|triangle
🡐🡑🡒🡓🡔🡕🡖🡗🡘🡙  -  0|wide0
🡠🡡🡢🡣🡤🡥🡦🡧    -  1|wide1
🡨🡩🡪🡫🡬🡭🡮🡯    -  2|wide2
🡰🡱🡲🡳🡴🡵🡶🡷    -  3|wide3
🡸🡹🡺🡻🡼🡽🡾🡿    -  4|wide4
🢀🢁🢂🢃🢄🢅🢆🢇    -  5|wide5

Parts:
   l|left
   t|top
   r|right
   b|bottom
  tl|top-left
  tr|top-right
  br|bottom-right
  bl|bottom-left
   h|horizontal
   v|vertical
]]
    assert.same(exp, M.build_help_arrow())
  end)

  it("pick_arrow", function()
    local f = M.pick_arrow
    -- _G.TEST_DEBUG = true
    assert.same('🡐', f('0-left'))
    assert.same('🡐', f('0l'))
    assert.same('🠜', f('acute-left'))
    assert.same('🠈', f('obtuse-left'))
  end)



  it("match", function()
    local ptrn = '^%s*([^%s]+)%s*$'
    local f = function(s) return string.match(s, ptrn) end
    assert.same('abc', f('abc'))
    assert.same('abc', f(' abc'))
    assert.same('abc', f(' abc '))
  end)

  it("build_help_symbs", function()
    local exp = [[
ABCDEFGHIJKLMNOPQRSTUV
    ╭╮╯╰               - ac|arc-corner
▏▔▕▁                   - bo|block-one
⪫⩛⪪⩚                   - cf|crows-foot
⚞Ѱ⚟₼                   - cF|crows-foot2
╗ ╔     ╦    ╥  ╓╖     - dd|down-double
┐ ┌     ┬   ╤     ╒╕   - ds|down-single
         ╬     ╪       - hd|horizontal-double
         ┼    ╫        - hs|horizontal-single
    ╔╗╝╚═║╬            - ld|line-double
    ┏┓┛┗━┃╋            - lh|line-heavy
    ┌┐┘└─│┼╳        ╱╲ - ll|line-light
╝ ╚     ╩    ╨  ╜╙     - ud|up-double
┘ └     ┴   ╧      ╛   - us|up-single
╠ ╣         ╬╫  ╟╢     - vd|vertical-double
            ╪     ╞╡   - vs|vertical-single

A: left              B: top               C: right           D: bottom
E: top-left          F: top-right         G: bottom-right    H: bottom-left
I: horizontal        J: vertical          K: cross           L: cross-diagonal
M: horizontal-double N: horizontal-single O: vertical-double P: vertical-single
Q: right-single      R: left-single       S: right-double    T: left-double
U: diagonal-right    V: diagonal-left
Example: ╬ -> horizontal-double + (J is vertical)  ->  hdv|horizontal-double-vertical

▓   bds | block-dark-shade
▄   bdh | block-down-half
█    bf | block-full
▌   blh | block-left-half
░   bls | block-light-shade
▒   bms | block-medium-shade
▐   brh | block-right-half
▀   buh | block-up-half
🞈    o2 | circle-l2
╳    cx | cross-diagonal
⁜    c. | cross-doted
╬    cd | cross-double
╋    ch | cross-heavy
┼    cl | cross-light
⊹    cm | cross-matrix
⫽    dd | diagonal-double
⋰    de | diagonal-elipsis
⋱    dE | diagonal-elipsis2
╱    ds | diagonal-single
╲    dS | diagonal-single2]]
    assert.same(exp, M.build_help_symbols())
  end)

  it("pick_symbol", function()
    local f = M.pick_symbol
    --
    assert.same('⪫', f('crows-foot-left'))
    assert.same('⪫', f('cfl'))

    assert.same('╱', f('diagonal-single'))
    assert.same('╱', f('ds'))
  end)
end)

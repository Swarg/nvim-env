-- 22-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'env.draw.utils.cmd4lua'


describe("env.draw.utils.cmd4lua", function()
  it("filter_keys_starts", function()
    local f = M.filter_keys_starts
    assert.same({ 'ab', 'abc' }, f({ abc = 1, ab = 1, ac = 1 }, 'ab'))
    assert.same({ 'ac', 'ab', 'abc' }, f({ abc = 1, ab = 1, ac = 1, d = 1 }, 'a'))
    assert.same({ 'ac', 'd', 'abc' }, f({ abc = 1, ac = 1, d = 1 }, ''))
  end)
  it("get_path", function()
    local f = M.get_path
    assert.same({ ':EnvXC', 'canvas', 'open' }, f(':EnvXC canvas  open', 19))
    assert.same({ 'canvas', 'open' }, f(':EnvXC canvas  open', 19, 1))
  end)

  it("get_cmd_node", function()
    local f = M.get_cmd_node
    local tree = { 'cmd1', 'cmd2' }
    -- c is starts of commandname before TAB
    assert.same({ { 'cmd1', 'cmd2' }, 0 }, { f(tree, { 'c' }) })
  end)

  it("get_cmd_node", function()
    local f = M.get_cmd_node
    local tree = {
      cmd1 = {
        subcmd1 = 'leaf',
        subcmd3 = 'leaf',
        subcmd2 = { subsubcmd1 = 'leaf' }
      },
      cmd2 = {
      }
    }
    local exp = {
      subcmd1 = 'leaf',
      subcmd3 = 'leaf',
      subcmd2 = { subsubcmd1 = 'leaf' }
    }
    assert.same({ exp, 1 }, { f(tree, { 'cmd1', 'sub' }) })

    local exp2 = {
      subcmd1 = 'leaf',
      subcmd3 = 'leaf',
      subcmd2 = { subsubcmd1 = 'leaf' }
    }
    assert.same({ exp2, 1 }, { f(tree, { 'cmd1', 'subcmd1', 'fn' }) })

    local exp3 = {
      subcmd1 = 'leaf',
      subcmd3 = 'leaf',
      subcmd2 = { subsubcmd1 = 'leaf' }
    }
    assert.same({ exp3, 1 }, { f(tree, { 'cmd1', 'fn', 'fn' }) })
  end)
end)

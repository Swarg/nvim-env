-- 09-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.draw.utils.tbl");

describe("env.draw.base", function()
  it("tbl_indexof", function()
    local f = M.tbl_indexof
    assert.same(5, f({ 1, 2, 3, 4, 5 }, 5))
    assert.is_nil(f({ 1, 2, 3, 4, 5 }, 6))
    assert.same(1, f({ 1, 2, 3, 4, 5 }, 1))
    assert.same(3, f({ 1, 2, 3, 4, 5 }, 3))
    assert.is_nil(f({ a = 1, b = 2, c = 3, d = 4 }, 4))
    assert.same(4, f({ [1] = 'a', [2] = 'b', [3] = 'c', [4] = 'd' }, 'd'))
    -- ! with holes in indexes(no 3)
    assert.is_nil(f({ [1] = 'a', [2] = 'b', [4] = 'd' }, 'd'))
    assert.same(2, f({ [1] = 'a', [2] = 'b', [4] = 'd' }, 'b'))
  end)

  it("tbl_keyof", function()
    local f = M.tbl_find_key_of_value
    assert.same('d', f({ a = 1, b = 2, c = 3, d = 4 }, 4))
  end)

  it("tbl_keys_count", function()
    local f = M.tbl_keys_count
    assert.same(4, f({ 1, 2, 3, 4 }))
    assert.same(4, f({ a = 1, b = 2, c = 3, d = 4 }))
  end)

  it("tbl_keys", function()
    local f = M.tbl_keys
    assert.same({ 'a', 'd', 'c', 'b' }, f({ a = 1, b = 2, c = 3, d = 4 }))
  end)

  it("tbl_values_to_keymap", function()
    local f = M.tbl_values_to_keymap
    assert.same({ b = true, c = true, a = true }, f({ 'a', 'b', 'c' }))
  end)

  it("tbl_remove_value", function()
    local f = M.tbl_remove_value
    assert.same({ 1, 3, 4, 5 }, f({ 1, 2, 2, 3, 4, 2, 5 }, 2))
  end)

  it("tbl_keys_sorted", function()
    local f = M.tbl_keys_sorted
    local desc = M.func_compare_descending
    assert.same({ 3, 2, 1 }, f({ [1] = 'a', [2] = 'b', [3] = 'c' }, desc))
  end)

  it("tbl_max_min_num_values", function()
    local f = function(l)
      return { M.tbl_max_min_num_values(l) }
    end
    assert.same({ 4, 1 }, f({ 1, 2, 3, 4 }))
    assert.same({ 8, 1 }, f({ 4, 1, 8, 2 }))
    assert.same({ 2, 2 }, f({ 2 }))
    assert.same({}, f({}))
  end)

  it("join_lists", function()
    local f = M.join_lists
    assert.same({ 1, 2, 3, 4, 5, 6, 7 }, f({ 1, 2, 3, 4 }, { 5, 6 }, { 7 }))
  end)
end)

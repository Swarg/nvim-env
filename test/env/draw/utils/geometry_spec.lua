-- 06-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local log = require('alogger')
local Container = require 'env.draw.Container'
local Canvas = require("env.draw.Canvas");
local Line = require("env.draw.Line");
local Point = require("env.draw.Point");
local Text = require("env.draw.Text");
local M = require("env.draw.utils.geometry");

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local log_trace = log.trace

--
local function show(h, ...)
  local w = 25
  h = h or 4
  local canvas = Canvas:new(nil, h, w)
  for i = 1, select('#', ...) do
    local e = select(i, ...)
    if e then canvas:add(e) end
  end

  return canvas:draw():toLines()
end

--
local function mkLine(x1, y1, x2, y2, c)
  return Line:new(nil, x1, y1, x2, y2, c or '-')
end


describe("env.draw.utils.geometry", function()
  before_each(function()
    log.fast_off()
  end)

  it("def_or_min", function()
    local f = M.def_or_min
    assert.same(5, f(0, 5, 3))
    assert.same(5, f(0, 3, 5))
  end)

  it("get_distance_sq_to_closest_edge inside given range", function()
    local f = M.get_distance_sq_to_closest_edge
    local x1, y1, x2, y2 = 0, 0, 6, 4

    assert.same(0, f(1, 1, x1, y1, x2, y2))
    assert.same(0, f(0, 0, x1, y1, x2, y2))
    assert.same(0, f(6, 4, x1, y1, x2, y2))
    assert.same(0, f(6, 0, x1, y1, x2, y2))
    assert.same(0, f(0, 4, x1, y1, x2, y2))

    assert.same(0, f(5, 3, x1, y1, x2, y2))
    assert.same(0, f(5, 0, x1, y1, x2, y2))
    assert.same(0, f(0, 3, x1, y1, x2, y2))
  end)

  it("get_distance_sq_to_closest_edge outside", function()
    local f = M.get_distance_sq_to_closest_edge
    local x1, y1, x2, y2 = 10, 10, 20, 20
    --  -- 162 is a sqrt of ~13
    assert.same(162, f(1, 1, x1, y1, x2, y2))

    -- vertex
    assert.same({ 2, M.DIRECTION_TOP_LEFT }, { f(9, 9, x1, y1, x2, y2) })

    assert.same({ 2, M.DIRECTION_TOP_RIGHT }, { f(21, 9, x1, y1, x2, y2) })
    assert.same({ 8, M.DIRECTION_TOP_RIGHT }, { f(22, 8, x1, y1, x2, y2) })

    assert.same({ 8, M.DIRECTION_BOTTOM_RIGHT }, { f(22, 22, x1, y1, x2, y2) })

    assert.same({ 8, M.DIRECTION_BOTTOM_LEFT}, { f(8, 22, x1, y1, x2, y2) })

    -- edges
    assert.same({ 4, M.DIRECTION_LEFT }, { f(8, 15, x1, y1, x2, y2) })
    assert.same({ 1, M.DIRECTION_LEFT }, { f(9, 15, x1, y1, x2, y2) })

    assert.same({ 4, M.DIRECTION_RIGHT}, { f(22, 15, x1, y1, x2, y2) })
    assert.same({ 1, M.DIRECTION_RIGHT}, { f(21, 15, x1, y1, x2, y2) })

    assert.same({ 4, M.DIRECTION_TOP}, { f(15, 8, x1, y1, x2, y2) })
    assert.same({ 1, M.DIRECTION_TOP}, { f(15, 9, x1, y1, x2, y2) })

    assert.same({ 4, M.DIRECTION_BOTTOM}, { f(15, 22, x1, y1, x2, y2) })
    assert.same({ 1, M.DIRECTION_BOTTOM}, { f(15, 21, x1, y1, x2, y2) })

    -- hald vertex
    assert.same({ 4, M.DIRECTION_RIGHT}, { f(22, 20, x1, y1, x2, y2) })
    assert.same({ 1, M.DIRECTION_RIGHT}, { f(21, 20, x1, y1, x2, y2) })
  end)


  it("closer_to", function()
    local f = function(x, x1, x2)
      x1, x2 = M.closer_to(x, x1, x2)
      return tostring(x1) .. "|" .. tostring(x2)
    end
    assert.same('8|2', f(10, 2, 8))
    assert.same('2|8', f(3, 2, 8))
    assert.same('11|11', f(10, 11, 11))
    assert.same('11|12', f(10, 11, 12))
    assert.same('11|9', f(10, 11, 09))
    assert.same('9|11', f(10, 9, 11))
  end)

  it("direction_coords", function()
    local f = function(x, y, x1, y1, x2, y2)
      x1, y1, x2, y2 = M.closer_first(x, y, x1, y1, x2, y2)
      return v2s(x1) .. "|" .. v2s(y1) .. '|' .. v2s(x2) .. "|" .. v2s(y2)
    end

    assert.same('1|1|4|4', f(0, 0, 1, 1, 4, 4))
    assert.same('4|4|1|1', f(5, 5, 1, 1, 4, 4))
    --todo diagonals
  end)

  it("find_closed_points", function()
    local f = function(a, b)
      local x1, y1, x2, y2 = M.find_closed_points(a, b)
      return v2s(x1) .. "|" .. v2s(y1) .. '|' .. v2s(x2) .. "|" .. v2s(y2)
    end

    assert.same('4|0|4|0', f(mkLine(0, 0, 4, 0), mkLine(8, 0, 4, 0)))
  end)



  ---@param h number
  ---@param a env.draw.Line
  ---@param b env.draw.Line
  local function show_contact_point(h, a, b)
    local x, y = M.find_contact_point(a, b)
    if x and y then
      local hint = Text:new(nil, x + 2, y + 1, v2s(x) .. ':' .. v2s(y))
      return show(h, a, b, Point:new(nil, x, y, '#'), hint)
    end
    return v2s(x) .. "|" .. v2s(y)
  end

  local get_contact_point_pos = function(a, b)
    local x, y = M.find_contact_point(a, b)
    return v2s(x) .. "|" .. v2s(y)
  end

  it("find_contact_point horizont", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(2, 1, 6, 1), mkLine(10, 1, 6, 1, '=')
    assert.same('6|1', f(line1, line2))
    local exp = {
      ' ----=====               ',
      '                         '
    }
    assert.same(exp, show(2, line1, line2))
    assert.same('6|1', f(line1, line2))
    local exp2 = {
      ' ----#====               ',
      '       6:1               '
    }
    assert.same(exp2, show_contact_point(2, line1, line2))
  end)

  it("find_contact_point horizont no contact", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(2, 1, 6, 1), mkLine(10, 1, 8, 1, '=')
    assert.same('nil|nil', f(line1, line2))
    local exp = {
      ' ----- ===               ',
      '                         '
    }
    assert.same(exp, show(2, line1, line2))
    local exp2 = 'nil|nil'
    assert.same(exp2, show_contact_point(2, line1, line2))
  end)

  it("find_contact_point horizont contact", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(2, 1, 6, 1), mkLine(10, 1, 7, 1, '=')
    assert.same('7|1', f(line1, line2))
    local exp2 = {
      ' -----#===               ',
      '        7:1              '
    }
    assert.same(exp2, show_contact_point(2, line1, line2))
    local exp3 = {
      ' -----#===               ',
      '        7:1              '
    }
    assert.same(exp3, show_contact_point(2, line2, line1))
  end)

  it("find_contact_point vertical contact", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(8, 1, 8, 3, '|'), mkLine(8, 4, 8, 7, '[')
    assert.same('8|4', f(line1, line2))
    local exp2 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '       #                 ',
      '       [ 8:4             ',
      '       [                 '
    }
    assert.same(exp2, show_contact_point(6, line1, line2))
    local exp3 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '       #                 ',
      '       [ 8:4             ',
      '       [                 '
    }
    assert.same(exp3, show_contact_point(6, line2, line1))
  end)

  it("find_contact_point vertical + horizontal ", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(8, 1, 8, 3, '|'), mkLine(8, 4, 16, 4, '=')
    assert.same('8|4', f(line1, line2))
    local exp2 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '       #========         ',
      '         8:4             ',
      '                         '
    }
    assert.same(exp2, show_contact_point(6, line1, line2))
    local exp3 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '       #========         ',
      '         8:4             ',
      '                         '
    }
    assert.same(exp3, show_contact_point(6, line2, line1))
  end)

  it("find_contact_point vertical + horizontal ", function()
    local f = get_contact_point_pos
    local line1, line2 = mkLine(8, 1, 8, 3, '|'), mkLine(8, 4, 1, 4, '=')
    assert.same('8|4', f(line1, line2))
    local exp2 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '=======#                 ',
      '         8:4             ',
      '                         '
    }
    assert.same(exp2, show_contact_point(6, line1, line2))
    local exp3 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '=======#                 ',
      '         8:4             ',
      '                         '
    }
    assert.same(exp3, show_contact_point(6, line2, line1))
  end)

  -- note: Line always store coords in this form: x1 < x2 and y1 < y2
  -- therefore, in order to understand where the line points,
  -- a reference point is needed
  it("get_line_direction", function()
    local f = function(center_x, center_y, line)
      local d = M.get_line_direction(line, center_x, center_y)
      return M.DIRNAME[d] or 'UNKNOWN'
    end

    assert.same('left', f(0, 0, Line:new(nil, 0, 1, 8, 1)))
    assert.same('left', f(0, 0, Line:new(nil, 8, 1, 0, 1))) -- swap in init_

    assert.same('right', f(8, 8, Line:new(nil, 8, 1, 0, 1)))
    assert.same('top', f(0, 0, Line:new(nil, 0, 1, 0, 8)))
    assert.same('top', f(0, 0, Line:new(nil, 0, 8, 0, 1)))

    assert.same('bottom', f(0, 8, Line:new(nil, 0, 8, 0, 1)))
  end)

  it("get_direction_code", function()
    local f = M.get_direction_code
    assert.same(M.DIRECTION_NO, f(0, 0, 0, 0))

    assert.same(M.DIRECTION_TOP_LEFT, f(1, 1, 0, 0))
    assert.same(M.DIRECTION_TOP, f(0, 0, 0, -1))
    assert.same(M.DIRECTION_BOTTOM, f(0, 0, 0, 1))

    assert.same(M.DIRECTION_RIGHT, f(0, 0, 1, 0))
    assert.same(M.DIRECTION_TOP_RIGHT, f(0, 0, 1, -1))
    assert.same(M.DIRECTION_BOTTOM_RIGHT, f(0, 0, 1, 1))

    assert.same(M.DIRECTION_LEFT, f(0, 0, -1, 0))
    assert.same(M.DIRECTION_TOP_LEFT, f(0, 0, -1, -1))
    assert.same(M.DIRECTION_BOTTOM_LEFT, f(0, 0, -1, 1))
  end)

  it("get_corner_rect_direction", function()
    local f = function(a, b)
      local d = M.get_corner_rect_direction_xy(a, b)
      return M.DIRNAME[d] or '?'
    end
    local line1, line2 = mkLine(8, 1, 8, 3, '|'), mkLine(8, 4, 1, 4, '=')

    local exp2 = {
      '       |                 ',
      '       |                 ',
      '       |                 ',
      '=======#                 ',
      '         8:4             ',
      '                         '
    }
    assert.same(exp2, show_contact_point(6, line1, line2))
    assert.same('bottom-right', f(line1, line2))
  end)
end)

--
describe("env.draw.utils.geometry create_combined_line", function()
  before_each(function()
    log.fast_off()
  end)

  local rl_style = { hl = '─', vl = '│', corners = { '┌', '┐', '┘', '└' } }

  local f = function(plist, style)
    local inv, x1, y1, x2, y2 = M.create_combined_line(plist, style)
    local container = Container:new(nil, x1, y1, x2, y2, inv)
    log_trace('new container:%s', container)
    return container
  end


  it("botton-left", function()
    local plist = { { x = 20, y = 7 }, { x = 12, y = 7 }, { x = 12, y = 3 } }

    local c = f(plist, rl_style)
    local exps = [[
Container (12:3 20:7) (9x5) elms: 3 inv:
 1: Line (12:7 20:7) color: ─
 2: Line (12:3 12:7) color: │
 3: Point 12:7 color: └
]]
    assert.same(exps, Container.__tostring(c, true))
    local exp = {
      '                         ',
      '                         ',
      '           │             ',
      '           │             ',
      '           │             ',
      '           │             ',
      '           └────────     ',
      '                         '
    }
    assert.same(exp, show(8, c))
  end)

  it("top-right", function()
    local plist = { { x = 1, y = 5 }, { x = 10, y = 5 }, { x = 10, y = 9 } }

    local c = f(plist, rl_style)
    local exps = [[
Container (1:5 10:9) (10x5) elms: 3 inv:
 1: Line (1:5 10:5) color: ─
 2: Line (10:5 10:9) color: │
 3: Point 10:5 color: ┐
]]
    assert.same(exps, Container.__tostring(c, true))
    local exp2 = {
      '                         ',
      '                         ',
      '                         ',
      '                         ',
      '─────────┐               ',
      '         │               ',
      '         │               ',
      '         │               '
    }
    assert.same(exp2, show(8, c))
  end)

  it("bottom-right", function()
    local plist = { { x = 1, y = 5 }, { x = 10, y = 5 }, { x = 10, y = 1 } }

    local c = f(plist, rl_style)
    local exps = [[
Container (1:1 10:5) (10x5) elms: 3 inv:
 1: Line (1:5 10:5) color: ─
 2: Line (10:1 10:5) color: │
 3: Point 10:5 color: ┘
]]
    assert.same(exps, Container.__tostring(c, true))
    local exp2 = {
      '         │               ',
      '         │               ',
      '         │               ',
      '         │               ',
      '─────────┘               ',
      '                         ',
      '                         ',
      '                         '
    }
    assert.same(exp2, show(8, c))
  end)

  it("top-left", function()
    local plist = { { x = 12, y = 8 }, { x = 12, y = 4 }, { x = 20, y = 4 } }

    local c = f(plist, rl_style)
    local exps = [[
Container (12:4 20:8) (9x5) elms: 3 inv:
 1: Line (12:4 12:8) color: │
 2: Line (12:4 20:4) color: ─
 3: Point 12:4 color: ┌
]]
    assert.same(exps, Container.__tostring(c, true))
    local exp = {
      '                         ',
      '                         ',
      '                         ',
      '           ┌────────     ',
      '           │             ',
      '           │             ',
      '           │             ',
      '           │             '
    }
    assert.same(exp, show(8, c))
  end)

  it("top-left-bottom", function()
    local plist = {
      { x = 8, y = 8 }, { x = 8, y = 4 }, { x = 20, y = 4 }, { x = 20, y = 8 }
    }

    local c = f(plist, rl_style)
    local exps = [[
Container (8:4 20:8) (13x5) elms: 5 inv:
 1: Line (8:4 8:8) color: │
 2: Line (8:4 20:4) color: ─
 3: Line (20:4 20:8) color: │
 4: Point 8:4 color: ┌
 5: Point 20:4 color: ┐
]]
    assert.same(exps, Container.__tostring(c, true))
    local exp = {
      '                         ',
      '                         ',
      '                         ',
      '       ┌───────────┐     ',
      '       │           │     ',
      '       │           │     ',
      '       │           │     ',
      '       │           │     '
    }
    assert.same(exp, show(8, c))
  end)
end)

-- back

describe("env.draw.utils.geometry combined_line_to_plist", function()
  before_each(function()
    log.fast_off()
  end)

  local rl_style = { hl = '─', vl = '│', corners = { '┌', '┐', '┘', '└' } }

  it("combined_line_to_plist 2 point", function()
    local plist = { { x = 12, y = 8 }, { x = 12, y = 4 } }

    local inv, _, _, _, _ = M.create_combined_line(plist, rl_style)

    local fp, lp = { x = 12, y = 8 }, { x = 20, y = 4 }

    local res_plist = M.combined_line_to_plist(inv, fp, lp)
    local exp = { { x = 12, y = 8 }, { x = 20, y = 4 } }
    assert.same(exp, res_plist)
  end)

  it("combined_line_to_plist 3 point", function()
    local plist = { { x = 12, y = 8 }, { x = 12, y = 4 }, { x = 20, y = 4 } }

    local inv, _, _, _, _ = M.create_combined_line(plist, rl_style)

    local fp, lp = { x = 12, y = 8 }, { x = 20, y = 4 }

    local res_plist = M.combined_line_to_plist(inv, fp, lp)
    local exp = { { x = 12, y = 8 }, { x = 12, y = 4 }, { x = 20, y = 4 } }
    assert.same(exp, res_plist)
  end)


  it("combined_line_to_plist 4 point", function()
    local plist = {
      { x = 8, y = 8 }, { x = 8, y = 4 }, { x = 20, y = 4 }, { x = 20, y = 8 }
    }
    local inv = M.create_combined_line(plist, rl_style)

    local fp, lp = plist[1], plist[#plist]

    local res_plist = M.combined_line_to_plist(inv, fp, lp)
    local exp = {
      { x = 8, y = 8 }, { x = 8, y = 4 }, { x = 20, y = 4 }, { x = 20, y = 8 }
    }
    assert.same(exp, res_plist)
  end)
end)

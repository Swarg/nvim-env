-- 09-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.draw.utils.buf");

describe("env.draw.utils.buf", function()
  -- nvimbuf tooling
  it("applay_keymap_layout", function()
    -- local u8 = require("env.util.utf8")
    local layout = { ['a'] = '@', ['x'] = '%' } -- u8.layout_lattin2ru
    local f = M.applay_keymap_layout
    assert.same('@', f('a', layout))
    assert.same('<c-@>', f('<c-a>', layout))
    assert.same('<c-a-@>', f('<c-a-a>', layout))
    assert.same('<c-a-@>', f('<c-a-a>', layout))
    assert.same('<c-a-%>', f('<c-a-x>', layout))
    assert.same('<c-%>', f('<c-x>', layout))
    assert.same('<a-%>', f('<a-x>', layout))
    assert.is_nil(f('<-x>', layout))
    assert.is_nil(f('<x>', layout))
    assert.is_nil(f('<a-xx>', layout))
  end)

  it("applay_keymap_layout <leader>xyz", function()
    local f = M.applay_keymap_layout
    local layout = {
      ['a'] = 'A',
      ['x'] = 'X',
      ['{'] = '+',
      ['}'] = '+',
      [':'] = '+',
      ['<'] = '+',
      ['>'] = '+',
      ['~'] = '+',
      ['?'] = '+',
      ['['] = '+',
      [']'] = '+',
      [';'] = '+',
      [','] = '+',
      ['.'] = '+',
      ['`'] = '+',
      ['/'] = '+',
    }
    assert.is_nil(f('<leader>', layout))
    assert.is_nil(f('<leader>0', layout))
    assert.is_nil(f('<leader>-', layout))
    assert.same('<leader>A', f('<leader>a', layout))
    assert.same('<leader>AA', f('<leader>aa', layout))
    assert.same('<leader>X', f('<leader>x', layout))
    assert.same('<leader>XAX', f('<leader>xax', layout))
    assert.same('<leader>+AX', f('<leader>?ax', layout))
    assert.same('<leader>A', f('<leader>a', layout))
    assert.same('<leader>+++++++++++++', f('<leader>{}[]<>:;,./`~', layout))
  end)

  it("gmatch", function()
    local cmd, t = 'a,b,c', {}
    for str in string.gmatch(cmd, '([^,]+)') do
      table.insert(t, str)
    end
    assert.same({ 'a', 'b', 'c' }, t)
  end)
end)

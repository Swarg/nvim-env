-- 09-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
--

_G.TEST = true
local log = require('alogger')
local M = require("env.draw.utils.base");

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


describe("env.ui.base", function()
  after_each(function()
    log.fast_off()
  end)

  it("normalized", function()
    local f = function(x1, y1, x2, y2)
      x1, y1, x2, y2 = M.normalize(x1, y1, x2, y2)
      return v2s(x1) .. "|" .. v2s(y1) .. "|" .. v2s(x2) .. "|" .. v2s(y2)
    end
    -- x1 y1 x2 y2
    assert.same('1|2|3|4', f(1, 2, 3, 4))
    assert.same('0|0|0|0', f(0, 0, 0, 0))
    assert.same('10|0|20|0', f(10, 0, 20, 0))
    assert.same('4|2|10|7', f(10, 2, 4, 7))
    assert.same('4|7|10|8', f(10, 8, 4, 7))
  end)

  it("center", function()
    local f = M.center
    assert.same(1, f(1, 1))
    assert.same(3, f(1, 5))
    assert.same(3, f(5, 1))
    assert.same(15, f(10, 20))
    assert.same(15, f(20, 10))
  end)

  it("n2letters", function()
    assert.same('A', M.n2letters(1))
    assert.same('Y', M.n2letters(25))
    assert.same('Z', M.n2letters(26))
    assert.same('AA', M.n2letters(27))
    assert.same('AB', M.n2letters(28))
    assert.same('XA', M.n2letters(25 * 25)) -- 625
    assert.same('YZ', M.n2letters(26 * 26)) -- 676
    assert.same('XZ', M.n2letters(25 * 25 + 25))
    assert.same('YY', M.n2letters(675))
    assert.same('ZA', M.n2letters(677))
    assert.same('ZZ', M.n2letters(702))
    assert.same('AAA', M.n2letters(703))
    assert.same('BAZZ', M.n2letters(36530))
  end)

  it("letters2n", function()
    assert.same(1, M.letters2n('A'))
    assert.same(26, M.letters2n('Z'))
    assert.same(27, M.letters2n('AA'))
    assert.same(28, M.letters2n('AB'))
    assert.same(625, M.letters2n('XA'))
    assert.same(626, M.letters2n('XB'))
    assert.same(675, M.letters2n('YY'))
    assert.same(676, M.letters2n('YZ'))
    assert.same(677, M.letters2n('ZA'))
    assert.same(702, M.letters2n('ZZ'))
    assert.same(703, M.letters2n('AAA'))
    assert.same(36530, M.letters2n('BAZZ'))
  end)


  it("range2indexes", function()
    local f = M.range2indexes

    assert.same({ 4, 3, 2, 1 }, f("1-4", 8))
    assert.same({}, f("1-0", 8))
    assert.same({ 1 }, f("1", 8))
    assert.same({ 8, 5, 4, 2, 1 }, f("1,2,4-5,>7", 8))
    assert.same({ 5, 2, 1 }, f("1,2,5", 8))
    assert.same({ 5, 4, 3, 2, 1 }, f("1,2,3-5", 8))
    assert.same({ 8, 7, 6, 5, 3, 2 }, f(">4,2-3", 8))
    assert.same({ 8, 7, 6, 5, 4, 3, 2 }, f(">1", 8))
    assert.same({ 4, 3, 2 }, f(">1,<5", 8))
    assert.same({ 8, 7, 6, 5, 4 }, f(">3", 8))
    assert.same({ 8, 7, 6 }, f(">5,>6", 8))
    assert.same({ 8, 7, 6, 2, 1 }, f("1-8,-4,-5,-3", 8))
  end)

  it("normalize_range", function()
    local f = function(x, y, x2, y2)
      x, y, x2, y2 = M.normalize_range(x, y, x2, y2)
      return tostring(x) .. "|" .. tostring(y) .. "|" .. tostring(x2) .. "|" .. tostring(y2)
    end

    assert.same('1|1|1|1', f(1, 1, 1, 1))
    assert.same('1|1|10|20', f(10, 20, 1, 1))
    assert.same('1|1|10|20', f(1, 1, 10, 20))
    assert.same('1|20|10|40', f(1, 40, 10, 20))
    assert.same('1|4|1|4', f(1, 4, nil, nil))
  end)


  it("exclude_elms", function()
    local te = { '5', '4', '3', '2', '1' }
    local ti = { 5, 4, 3, 2, 1 }
    local f = function(exlude, te0, ti0)
      return { M.exclude_elms(exlude, te0, ti0) }
    end
    assert.same({ { '5', '4' }, { 5, 4 } }, f({ 1, 2, 3 }, te, ti))
    assert.same({ { '4', '2' }, { 4, 2 } }, f({ 3, 5, 1 }, te, ti))
    assert.same({ { '4' }, { 4 } }, f({ 3, 5, 1, 2 }, te, ti))
    assert.same({ { '4' }, { 4 } }, f({ 3, 5, 1, 2, 99 }, te, ti))
  end)

  it("validate_pos_xy", function()
    local f = M.validate_pos_xy
    assert.same(true, f({ x = 1, y = 2 }, 't'))

    assert.has_error(function() f({ 1, 2 }, 't') end)
    assert.has_error(function() f({ x = '1', y = 2 }, 't') end)
    assert.has_error(function() f({ x = 1, y = '2' }, 't') end)
  end)

  it("sizeof_points", function()
    local f = function(a, b)
      local w, h = M.sizeof_points(a, b)
      return tostring(w) .. "|" .. tostring(h)
    end
    assert.same('2|2', f({ x = 1, y = 1 }, { x = 2, y = 2 }))
    assert.same('1|1', f({ x = 1, y = 1 }, { x = 1, y = 1 }))
    assert.same('2|4', f({ x = 2, y = 1 }, { x = 1, y = 4 }))
  end)

  it("split", function()
    assert.same({ 'abc', 'de' }, M.split("abc\nde", "\n"))
  end)

  it("slist2str", function()
    local exp = "{\n[1] = 'abc',\n[2] = 'de',\n}"
    assert.same(exp, M.slist2str({ [1] = 'abc', [2] = 'de' }, ""))
  end)

  it("fmt_kvpair", function()
    local f = M.fmt_kvpair
    local exp = "k=[==[something\nwith\nnewlines]==]"
    assert.same(exp, f('k', "something\nwith\nnewlines", ''))
  end)
end)

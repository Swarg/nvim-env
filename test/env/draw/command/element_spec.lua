-- 29-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local Cmd4Lua = require("cmd4lua")
_G.TEST = true
local log = require('alogger')
local Line = require("env.draw.Line");
local Point = require("env.draw.Point");
local Canvas = require('env.draw.Canvas')
local state = require("env.draw.state.canvas");
local cmd_canvas = require("env.draw.command.canvas");
local cmd_undo = require("env.draw.command.undo")
local M = require("env.draw.command.element");
local CE = require("env.draw.ui.CanvasEditor");

local get_ui_item = state.get_ui_item
local get_elements_readable_list = Canvas.get_elements_readable_list
local get_deleted_readable_list = state.get_deleted_readable_list
local get_clipboard_readable_list = state.get_clipboard_readable_list
local NE = M.NE


describe("env.command.draw.element (new)", function()
  after_each(function()
    state.clear_all()
    nvim:clear_state()
    log.fast_off()
  end)

  it("new rectange", function()
    local lnum, col, lines = 3, 2, {
      '',       -- 0
      ' begin', -- 1
      ' end',   -- 3
      '' }      -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)
    local w = Cmd4Lua.of("-w 8 -h 5 -b ' ' --quiet")

    -- require 'alogger'.fast_setup()
    cmd_canvas.cmd_new(w) --                        <<<< prepare

    local box = get_ui_item(bufnr, 1)
    assert.is_not_nil(box)
    assert.is_nil(get_ui_item(bufnr, 2))

    local exps = 'ItemPH lnum:3:8 col:0:8 (env.draw.Canvas) Canvas w:8 h:5  objects:0'
    assert.same(exps, tostring(box))
    assert.same(3, assert(box).lnum)
    local exp = {
      '',           --  0
      ' begin',     --  1
      '```xcanvas', -- 2
      '        ',
      '        ',
      '        ',
      '        ',
      '        ',
      '```',
      ' end',
      '' }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1, false))
    -- put cursor into range of the new canvas
    vim.api.nvim_win_set_cursor(0, { lnum + 1, col })

    w = Cmd4Lua.of("-w 4 -h 5 --quiet")
    local r = NE.cmd_rectangle(w) --                        <<<< workload
    assert.same(false, w:has_error())
    exp = {
      '',
      ' begin',
      '```xcanvas',
      '  +--+  ',
      '  |  |  ',
      '  |  |  ',
      '  |  |  ',
      '  +--+  ',
      '```',
      ' end',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1, false))
    assert.is_not_nil(r) ---@cast r env.draw.Rectangle
    local exp2 = 'Rectangle (3:1 6:5) (4x5) bg: ""'
    assert.same(exp2, tostring(r))
  end)
end)

describe("env.draw.command.element access to canvas elements by index", function()
  local Rectangle = require('env.draw.Rectangle')

  local outputs = {}
  local printcb = function(...)
    local s = ''
    local cnt = select('#', ...)
    for i = 1, cnt do
      s = s .. ' ' .. tostring(select(i, ...))
    end
    outputs[#outputs + 1] = s
  end

  after_each(function()
    log.fast_off()
    state.clear_all_ui_items()
    state.clear_clipboard()
    state.clear_deleted_history()
    nvim:clear_state()
    outputs = {}
  end)

  local function place_canvas(w, h, lnum, col)
    lnum = lnum or 0
    col = col or 0
    local lines = { '' }
    if lnum > 0 then
      for _ = 1, lnum + 1 do
        table.insert(lines, string.rep(' ', w))
      end
    end

    local bufnr = nvim:new_buf(lines, lnum, col)
    local canvas = Canvas:new(nil, h, w)

    local _, box = CE.add_canvas(canvas, bufnr, lnum, col)
    local can_init_new = true
    local ok, errmsg = CE.prepare_and_place(box, can_init_new)
    assert(ok, tostring(errmsg))

    return box
  end


  it("tooling place_canvas", function()
    local box = place_canvas(12, 4) --                            <<  wordload
    assert.is_not_nil(box)
    assert.same('Canvas w:12 h:4 objects:0', tostring(box.item))
    local exp = {
      '```xcanvas',
      '            ',
      '            ',
      '            ',
      '            ',
      '```',
      ''
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(box.bufnr, 0, -1, true))
  end)

  local exp_canvas_2rect_line_2point = {
    '```xcanvas',
    '        +----+      ',
    '  +--+  |  # |      ',
    '  | ~~~~~~~  |      ',
    '  +--+  +----+ @    ',
    '```',
    ''
  }
  local function prepare_canvas(not_draw, w, h, lnum, col)
    w = w or 20
    h = h or 4
    local box = place_canvas(w, h, lnum, col)
    box:add(Rectangle:new(nil, 0, 0, 3, 2), 2, 2) -- 1
    box:add(Rectangle:new(nil, 0, 0, 5, 3), 1, 8) -- 2
    box:add(Line:new(nil, 0, 0, 6, 0, '~'), 3, 4) -- 3
    box:add(Point:new(nil, 0, 0, '#'), 2, 11)     -- 4
    box:add(Point:new(nil, 0, 0, '@'), 4, 15)     -- 5

    if not not_draw then
      CE.draw_item(box)
    end
    return box
  end

  -- show all buffer with canvas
  local function show_all_buffer(box)
    return vim.api.nvim_buf_get_lines(box.bufnr, 0, -1, true)
  end

  it("tooling place_canvas 2", function()
    local box = prepare_canvas()
    assert.same(exp_canvas_2rect_line_2point, show_all_buffer(box))
  end)

  it("sub_cmd_elements list", function()
    prepare_canvas()
    local w = Cmd4Lua.of('--quiet') --:set_print_callback(printcb)
    local ret = M.cmd_list(w)
    assert.same(false, w:has_error())
    local exp2 = {
      '  1  Rectangle (3:2 6:4) (4x3) bg: ""',
      '  2  Rectangle (9:1 14:4) (6x4) bg: ""',
      '  3  Line (5:3 11:3) color: ~',
      '  4  Point 12:2 color: #',
      '  5  Point 16:4 color: @'
    }
    assert.same(exp2, ret)
  end)

  it("sub_cmd_elements: cut element from canvas by index", function()
    local box = prepare_canvas()
    local w = Cmd4Lua.of('1'):set_print_callback(printcb)

    M.cmd_cut_by_index(w) -- workload
    assert.same(false, w:has_error())

    local exp2 = { ' x [3:2 6:4][0:0] {Rectangle(4x3)(0:0)}' }
    assert.same(exp2, outputs)

    local exp3 = {
      '```xcanvas',
      '        +----+      ',
      '        |  # |      ',
      '    ~~~~~~~  |      ',
      '        +----+ @    ',
      '```',
      ''
    }
    assert.same(exp3, show_all_buffer(box))

    -- ensure element goes into clipboard
    local exp4 = {
      '  1  x [3:2 6:4][0:0] {Rectangle(4x3)(0:0)}',
      'Page: #1/1[10]'
    }
    assert.same(exp4, get_clipboard_readable_list(1, 10))
    assert.same({ 'Page: #1/0[10]' }, get_deleted_readable_list(1, 10))
  end)


  it("sub_cmd_elements: delete canvas element by index -- canceled", function()
    prepare_canvas()
    local w = Cmd4Lua.of('1 '):set_print_callback(printcb)

    M.cmd_delete_by_index(w) -- workload
    assert.same(false, w:has_error())

    local exp2 = { ' canceled' }
    assert.same(exp2, outputs)

    assert.same({ 'Page: #1/0[10]' }, get_deleted_readable_list(1, 10))
  end)


  it("sub_cmd_elements: delete canvas element by index", function()
    local box = prepare_canvas()

    local w = Cmd4Lua.of('1 --yes'):set_print_callback(printcb)
    M.cmd_delete_by_index(w) -- workload
    assert.same(false, w:has_error())

    local exp2 = { ' x [3:2 6:4][0:0] {Rectangle(4x3)(0:0)}' }
    assert.same(exp2, outputs)

    local exp3 = {
      '```xcanvas',
      '        +----+      ',
      '        |  # |      ',
      '    ~~~~~~~  |      ',
      '        +----+ @    ',
      '```',
      ''
    }
    assert.same(exp3, show_all_buffer(box))

    -- make sure the element doesn't go to the clipboard
    assert.same({ 'Page: #1/0[10]' }, get_clipboard_readable_list(1, 10))
    local exp = {
      '  1  x [3:2 6:4][0:0] {Rectangle(4x3)(0:0)}',
      'Page: #1/1[10]'
    }
    -- ensure element goes into deleted history
    assert.same(exp, get_deleted_readable_list(1, 10))
  end)

  -- delete multiple elements as a one selection
  it("sub_cmd_elements: delete canvas elements by indexes list", function()
    local box = prepare_canvas()

    local w = Cmd4Lua.of('1,3,5 --yes'):set_print_callback(printcb)
    M.cmd_delete_by_index(w) --                           << workload
    assert.same(false, w:has_error())

    local exp2 = {
      ' x [3:2 16:4][0:0] {Point(1x1)(13:2) Line(7x1)(2:1) Rectangle(4x3)(0:0)}'
    }
    assert.same(exp2, outputs)

    local exp3 = {
      '```xcanvas',
      '        +----+      ',
      '        |  # |      ',
      '        |    |      ',
      '        +----+      ',
      '```',
      ''
    }
    assert.same(exp3, show_all_buffer(box))

    assert.same({ 'Page: #1/0[10]' }, get_clipboard_readable_list(1, 10))

    local exp = {
      '  1  x [3:2 16:4][0:0] {Point(1x1)(13:2) Line(7x1)(2:1) Rectangle(4x3)(0:0)}',
      'Page: #1/1[10]'
    }
    assert.same(exp, get_deleted_readable_list(1, 10))
  end)


  --
  it("sub_cmd_elements: restore deleted element", function()
    local box = prepare_canvas()

    local exp_before = {
      '  1  Rectangle (3:2 6:4) (4x3) bg: ""',
      '  2  Rectangle (9:1 14:4) (6x4) bg: ""',
      '  3  Line (5:3 11:3) color: ~',
      '  4  Point 12:2 color: #',
      '  5  Point 16:4 color: @'
    }
    assert.same(exp_before, get_elements_readable_list(box.item, 1, 10))

    local w = Cmd4Lua.of('1 --yes'):set_print_callback(printcb)
    M.cmd_delete_by_index(w) --                         << prepare
    assert.same(false, w:has_error())

    local exp = {
      '  1  x [3:2 6:4][0:0] {Rectangle(4x3)(0:0)}',
      'Page: #1/1[10]'
    }
    assert.same(exp, get_deleted_readable_list(1, 10))

    w = Cmd4Lua.of('1 --keep-pos'):set_print_callback(printcb)
    cmd_undo.cmd_restore(w) --                          << workload
    assert.same(false, w:has_error())
    assert.same(2, #outputs)
    assert.same(' 1 elements restored Rectangle(4x3)(3:2)', outputs[2])

    exp = { 'Page: #1/0[10]' }
    assert.same(exp, get_deleted_readable_list(1, 10))
    local exp_after = {
      '  1  Rectangle (9:1 14:4) (6x4) bg: ""',
      '  2  Line (5:3 11:3) color: ~',
      '  3  Point 12:2 color: #',
      '  4  Point 16:4 color: @',
      '  5  Rectangle (3:2 6:4) (4x3) bg: ""'
    }
    assert.same(exp_after, get_elements_readable_list(box.item, 1, 10))
    local exp2 = {
      '```xcanvas',
      '        +----+      ',
      '  +--+  |  # |      ',
      '  | ~|~~~~~  |      ',
      '  +--+  +----+ @    ',
      '```',
      ''
    }
    assert.same(exp2, show_all_buffer(box))
  end)


  it("sub_cmd_elements: restore deleted elements (many in one selection)", function()
    local box = prepare_canvas()

    local w = Cmd4Lua.of('1,3,4,5 --yes'):set_print_callback(printcb)
    -- delete multiple elements as a one selection
    M.cmd_delete_by_index(w) --                         << prepare
    assert.same(false, w:has_error())

    local exp = {
      ' x [3:2 16:4][0:0] {Point(1x1)(13:2) Point(1x1)(9:0) Line(7x1)(2:1) '
      .. 'Rectangle(4x3)(0:0)}'
    }
    assert.same(exp, outputs)
    outputs = {} -- clear

    exp = {
      '  1  x [3:2 16:4][0:0] {Point(1x1)(13:2) Point(1x1)(9:0) Line(7x1)(2:1)'
      .. ' Rectangle(4x3)(0:0)}',
      'Page: #1/1[10]'
    }
    assert.same(exp, get_deleted_readable_list(1, 10))

    -- restore all deleted elements from first selection
    w = Cmd4Lua.of('1 --keep-pos'):set_print_callback(printcb)
    cmd_undo.cmd_restore(w) --                          << workload
    assert.same(false, w:has_error())
    exp = {
      ' 4 elements restored Point(1x1)(16:4) Point(1x1)(12:2) Line(7x1)(5:3)'
    }
    assert.same(exp, outputs)

    exp = { 'Page: #1/0[10]' }
    assert.same(exp, get_deleted_readable_list(1, 10))

    -- all restored elements placed with zero offset relative to the cursor
    -- now cursor at top-left of the canvas so:
    -- this was done specifically to make it easier to find hidden elements
    -- through cli-commands with the ability to insert directly under the cursor
    local exp2 = {
      '```xcanvas',
      '        +----+      ',
      '  +--+  |  # |      ',
      '  | ~|~~~~~  |      ',
      '  +--+  +----+ @    ',
      '```',
      ''
    }
    assert.same(exp2, show_all_buffer(box))
  end)

  it("cmd_inspect", function()
    local box = prepare_canvas() -- nil, nil, nil, 3, 4)
    assert.same(1, box.bufnr)
    local exp = [[
Canvas w:20 h:4 objects:5
   1:  Rectangle (3:2 6:4) (4x3) bg: ""
   2:  Rectangle (9:1 14:4) (6x4) bg: ""
   3:  Line (5:3 11:3) color: ~
   4:  Point 12:2 color: #
   5:  Point 16:4 color: @
]]
    assert.same(exp, Canvas.__tostring(box.item, true))
    vim.api.nvim_win_set_cursor(0, { 3, 4 })

    assert.same({ 3, 4 }, vim.api.nvim_win_get_cursor(0)) -- 3 0

    local w = Cmd4Lua.of(''):set_print_callback(printcb)
    local res = M.cmd_inspect(w)

    local exp2 = [[
Position(2:4,2:4)
Rectangle (3:2 6:4) (4x3) bg: ""]]
    assert.same(exp2, res)
  end)

  it("cmd_inspect verbose", function()
    local box = prepare_canvas() -- nil, nil, nil, 3, 4)
    assert.same(1, box.bufnr)
    local exp = [[
Canvas w:20 h:4 objects:5
   1:  Rectangle (3:2 6:4) (4x3) bg: ""
   2:  Rectangle (9:1 14:4) (6x4) bg: ""
   3:  Line (5:3 11:3) color: ~
   4:  Point 12:2 color: #
   5:  Point 16:4 color: @
]]
    assert.same(exp, Canvas.__tostring(box.item, true))
    vim.api.nvim_win_set_cursor(0, { 3, 4 })

    assert.same({ 3, 4 }, vim.api.nvim_win_get_cursor(0)) -- 3 0

    local w = Cmd4Lua.of('--verbose'):set_print_callback(printcb)
    local res = M.cmd_inspect(w)
    local exp2 = [[
Position(2:4,2:4)
env.draw.Rectangle {
  DEFAULT_CORNERS = <1>{ "+", "+", "+", "+" },
  DEFAULT_HLINE = "-",
  DEFAULT_VLINE = "|",
  bg = "",
  bline = {
    color = "-",
    x1 = 3,
    x2 = 6,
    y1 = 4,
    y2 = 4
  },
  corners = <table 1>,
  lline = {
    color = "|",
    x1 = 3,
    x2 = 3,
    y1 = 3,
    y2 = 3
  },
  rline = {
    color = "|",
    x1 = 6,
    x2 = 6,
    y1 = 3,
    y2 = 3
  },
  tline = {
    color = "-",
    x1 = 3,
    x2 = 6,
    y1 = 2,
    y2 = 2
  }
}
]]
    assert.same(exp2, res)
  end)
end)

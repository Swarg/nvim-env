-- 29-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim

local Cmd4Lua = require("cmd4lua")
_G.TEST = true
local log = require('alogger')
local Line = require("env.draw.Line");
-- local Point = require("env.draw.Point");
-- local Canvas = require('env.draw.Canvas')
local M = require("env.draw.command.canvas");
local CE = require("env.draw.ui.CanvasEditor");
local state = require("env.draw.state.canvas");
local ubuf = require("env.draw.utils.buf")

local get_ui_item = state.get_ui_item
local get_editor_coords = ubuf.get_editor_coords


describe("env.draw.command.canvas", function()
  after_each(function()
    state.clear_all()
    nvim:clear_state()
    log.fast_off()
  end)


  it("new canvas create new syntax block", function()
    local lnum, col, lines = 3, 0, {
      '',      -- 0
      ' ....', -- 1
      ' .  .', -- 2 (lnum = 3) << cursor
      ' ....', -- 3
      '' }     -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.same({ lnum, col }, vim.api.nvim_win_get_cursor(0)) -- 3 0

    local w = Cmd4Lua.of('--quiet -w 8 -h 4')

    -- require 'alogger'.fast_setup()
    M.cmd_new(w)                         --                        <<<< workload

    local box, _ = get_ui_item(bufnr, 1) -- box.item  keep Canvas
    assert.is_not_nil(box) ---@cast box env.draw.ui.ItemPlaceHolder
    assert.is_nil(get_ui_item(bufnr, 2))

    assert.same(3, box.lnum) -- starts from 0
    -- auto jump into canvas
    assert.same({ lnum + 1, col }, vim.api.nvim_win_get_cursor(0))

    local exp = {
      '',
      ' ....',
      '```xcanvas',
      '        ', --
      '        ',
      '        ', --  inserted canvas
      '        ', --
      '```',
      ' .  .',
      ' ....',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1, false))
  end)

  it("new canvas to existed syntax block", function()
    local lnum, col, lines = 3, 0, {
      '',           -- 0
      '```xcanvas', -- 1
      '',           -- 2 (3)
      '```',        -- 2 (lnum = 3) << cursor
      '...',        -- 3
      '' }          -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)
    local w = Cmd4Lua.of('-w 8 -h 4 --quiet')

    M.cmd_new(w) --                        <<<< workload

    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    assert.same({ 3, 0 }, cursor_pos) -- starts from 1

    local exp = {
      '',
      '```xcanvas',
      '        ',
      '        ',
      '        ',
      '        ',
      '```',
      '...',
      ''
    }
    assert.same(exp, nvim:get_lines(bufnr, 0, -1, false))
  end)
end)

-- emulated FS & OS

describe("env.draw.command.canvas save+load (emulated FS)", function()
  local sys = nvim:get_os()
  setup(function()
    -- local sys =
  end)
  after_each(function()
    log.fast_off()
    state.clear_all()
    nvim:clear_state()
    sys:clear_state(nil, true)
  end)

  --

  local serialized_canvas_12x4_with_line = {
    '{',
    "  width = 12, height = 4, border = '',",
    '  classes = {',
    "    [1] = 'oop.Object',",
    "    [2] = 'env.draw.Line',",
    '  },',
    '  objects = {',
    "    {_clsid=2,x1=2,y1=2,x2=10,y2=2,color='-'},",
    '  },',
    '}'
  }
  local serialized_canvas_12x5_with_line = {
    '{',
    "  width = 12, height = 5, border = '',",
    '  objects = {',
    '    {_class = "env.draw.Line",color = "#",x1 = 1,x2 = 11,y1 = 3,y2 = 3},',
    '  },',
    '}'
  }

  it("save canvas", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      '```xcanvas',                  -- 1
      '```',                         -- 2 (lnum = 3) << cursor
      '...',                         -- 3
      '' }                           -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)
    local w = Cmd4Lua.of('-w 12 -h 4 --quiet')

    M.cmd_new(w) --                                 prepare create new canvas
    assert.same(false, w:has_error())

    -- add line
    local box = get_ui_item(bufnr, 1) ---@cast box env.draw.ui.ItemPlaceHolder
    assert.is_not_nil(box)

    -- move cursor to insert line
    vim.api.nvim_win_set_cursor(0, { lnum + 1, col + 1 }) -- firs row in canvas
    local bn, ln, cn = get_editor_coords()
    assert.same('1|3|1', tostring(bn) .. "|" .. tostring(ln) .. "|" .. tostring(cn))
    box:add(Line:new(nil, 0, 0, 8, 0, '-'), ln, cn)
    CE.draw_item(box) -- local res = box.item:draw():toLines()
    local exp = {
      '',
      '```xcanvas',
      '            ',
      ' ---------  ',
      '            ',
      '            ',
      '```',
      '...',
      ''
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local fn = '/tmp/12x4.clt'
    w = Cmd4Lua.of('--quiet -f ' .. fn)
    assert.is_nil(sys:file_type(fn)) -- not existed

    M.cmd_save(w)                    --  workload
    assert.same(false, w:has_error())

    assert.same('file', sys:file_type(fn)) -- sure the file was created
    exp = serialized_canvas_12x4_with_line
    assert.same(exp, sys:get_file_lines(fn))
  end)


  it("load canvas in existed syntax block", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      '```xcanvas',                  -- 1
      '```',                         -- 2 (lnum = 3) << cursor
      '...',                         -- 3
      '' }                           -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    local content = serialized_canvas_12x4_with_line
    sys:set_file_content(fn, content)
    assert.same('file', sys:file_type(fn)) -- existed

    local w = Cmd4Lua.of('--quiet -f ' .. fn)
    M.cmd_load(w) --  workload
    assert.same(false, w:has_error())

    local exp = {
      '',
      '```xcanvas',
      '            ',
      ' ---------  ',
      '            ',
      '            ',
      '```',
      '...',
      ''
    }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))
  end)


  it("load canvas error: no syntax block", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      'abc',                         -- 1
      'de',                          -- 2 (lnum = 3) << cursor
      '...',                         -- 3
      '' }                           -- 4
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    local content = serialized_canvas_12x4_with_line
    sys:set_file_content(fn, content)
    assert.same('file', sys:file_type(fn)) -- existed

    local w = Cmd4Lua.of('--quiet -f ' .. fn)
    M.cmd_load(w) --  workload
    assert.same(true, w:has_error())
    assert.same({ 'cannot insert without syntax block' }, w.errors)
  end)


  it("load canvas no syntax block with --yes opt", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      'abc',                         -- 1
      'de',                          -- 2 (lnum = 3) << cursor
      '' }                           -- 3
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    local content = serialized_canvas_12x4_with_line
    sys:set_file_content(fn, content)
    assert.same('file', sys:file_type(fn)) -- existed

    local w = Cmd4Lua.of('--quiet --yes -f ' .. fn)
    M.cmd_load(w) --                    <<<  workload
    assert.same(false, w:has_error())
    local exp = {
      '',
      'abc',
      '```xcanvas',
      '            ',
      ' ---------  ',
      '            ',
      '            ',
      '```', 'de', '' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))
  end)


  it("load canvas reloaded same fn inside canvas", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      'abc',                         -- 1
      'de',                          -- 2 (lnum = 3) << cursor
      '' }                           -- 3
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    sys:set_file_content(fn, serialized_canvas_12x4_with_line)
    assert.same('file', sys:file_type(fn)) -- existed

    assert.same({ 3, 0 }, vim.api.nvim_win_get_cursor(0))

    local w = Cmd4Lua.of('--quiet --yes -f ' .. fn)
    M.cmd_load(w) --                                <<< workload (1)
    assert.same(false, w:has_error())

    -- ensure auto jump to top-left of the canvas is works
    assert.same({ 4, 0 }, vim.api.nvim_win_get_cursor(0))

    -- ensure canvas was loaded to list
    local box, list = get_ui_item(bufnr, 1)
    assert.same('true|1', tostring(box ~= nil) .. "|" .. tostring(#(list or {})))

    local exp = {
      '',             -- 0
      'abc',          -- 1
      '```xcanvas',   -- 2 (3)
      '            ', -- 3
      ' ---------  ',
      '            ',
      '            ',
      '```', 'de', '' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    -- reload from save filename in same place in buffer

    -- new content(edited)
    sys:set_file_content(fn, serialized_canvas_12x5_with_line)

    w = Cmd4Lua.of('--quiet -f ' .. fn)
    M.cmd_load(w) --                         << workload (2)
    assert.same(false, w:has_error())

    -- ensure reloaded existed canvas is not create a new one
    box, list = get_ui_item(bufnr, 2)
    assert.same('false|1', tostring(box ~= nil) .. "|" .. tostring(#(list or {})))

    -- assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local exp2 = {
      '',
      'abc',
      '```xcanvas',
      '            ',
      '            ',
      '########### ',
      '            ',
      '            ',
      '```',
      'de',
      ''
    }
    assert.same(exp2, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))
  end)


  it("load canvas: load an another canvas into occupied space block", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      'abc',                         -- 1
      'de',                          -- 2 (lnum = 3) << cursor
      '' }                           -- 3
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    local fn2 = '/tmp/12x4_2.clt'
    sys:set_file_content(fn, serialized_canvas_12x4_with_line)
    sys:set_file_content(fn2, serialized_canvas_12x5_with_line)
    assert.same('file', sys:file_type(fn))  -- existed
    assert.same('file', sys:file_type(fn2)) -- existed


    local w = Cmd4Lua.of('--quiet --yes -f ' .. fn)
    M.cmd_load(w) --                                <<< workload (1)
    assert.same(false, w:has_error())

    local exp = {
      '',             -- 0
      'abc',          -- 1
      '```xcanvas',   -- 2 (3)
      '            ', -- 3
      ' ---------  ',
      '            ',
      '            ',
      '```', 'de', '' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local box, list = get_ui_item(bufnr, 1)
    local exp_box = 'ItemPH lnum:3:7 col:0:12 (env.draw.Canvas) Canvas w:12 h:4 objects:1'
    assert.same(exp_box, tostring(box))
    assert.same(1, #list)

    -- no --yes opts - block an attempt to load in the same place
    w = Cmd4Lua.of('--quiet -f ' .. fn2)
    M.cmd_load(w) --                                <<< workload (2)
    assert.same(true, w:has_error())
    assert.same({ 'to replace existed canvas use --yes opts' }, w.errors)
    -- 'an attempt to load a canvas into a occupied space is blocked'

    box, list = get_ui_item(bufnr, 2)
    assert.is_nil(box)
    assert.same(1, #list)

    -- ensure first canvas and box do not changed
    assert.same(exp_box, tostring(get_ui_item(bufnr, 1)))
  end)

  -- canvas from new filename replace the previous loaded canvas
  -- todo ask to save replaced canvas
  it("load canvas: load an another canvas into occupied space with --yes - replace", function()
    local lnum, col, lines = 3, 0, { -- nvim.lnum (from 1)
      '',                            -- 0
      'abc',                         -- 1
      'de',                          -- 2 (lnum = 3) << cursor
      '' }                           -- 3
    local bufnr = nvim:new_buf(lines, lnum, col)

    assert.is_nil(get_ui_item(bufnr, 1)) -- sure empty

    local fn = '/tmp/12x4.clt'
    local fn2 = '/tmp/12x4_2.clt'
    sys:set_file_content(fn, serialized_canvas_12x4_with_line)
    sys:set_file_content(fn2, serialized_canvas_12x5_with_line)
    assert.same('file', sys:file_type(fn))  -- existed
    assert.same('file', sys:file_type(fn2)) -- existed


    local w = Cmd4Lua.of('--quiet --yes -f ' .. fn)
    M.cmd_load(w) --                                <<< workload (1)
    assert.same(false, w:has_error())

    local exp = {
      '',             -- 0
      'abc',          -- 1
      '```xcanvas',   -- 2 (3)
      '            ', -- 3
      ' ---------  ', -- 4
      '            ', -- 5
      '            ', -- 6
      '```',          -- 7 (8)
      'de', '' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local box, list = get_ui_item(bufnr, 1)
    local exp_box = 'ItemPH lnum:3:7 col:0:12 (env.draw.Canvas) Canvas w:12 h:4 objects:1'
    assert.same(exp_box, tostring(box))
    assert.same(1, #list)
    local canvas1 = assert(box).item

    w = Cmd4Lua.of('--quiet --yes -f ' .. fn2)
    M.cmd_load(w) --                                <<< workload (2)
    assert.same(false, w:has_error())

    box, list = get_ui_item(bufnr, 2)
    assert.same('false|1', tostring(box ~= nil) .. "|" .. tostring(#(list or {})))

    -- assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local exp2 = {
      '',
      'abc',
      '```xcanvas',
      '            ',
      '            ',
      '########### ',
      '            ',
      '            ',
      '```',
      'de',
      ''
    }
    assert.same(exp2, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))

    local exp_box2 = 'ItemPH lnum:3:8 col:0:12 (env.draw.Canvas) ' ..
        'Canvas w:12 h:5 objects:1'
    --                 ^
    --
    local updated_box, updated_list = get_ui_item(bufnr, 1)
    assert.same(exp_box2, tostring(updated_box))
    assert.same(1, #updated_list)
    assert.is_true(exp_box ~= exp_box2)
    assert.are_not_same(canvas1, assert(updated_box).item)

    CE.draw_item(assert(get_ui_item(bufnr, 1), 'first canvas'))
    local exp3 = {
      '',
      'abc',
      '```xcanvas',
      '            ',
      '            ',
      '########### ',
      '            ',
      '            ',
      '```',
      'de',
      ''
    }
    assert.same(exp3, vim.api.nvim_buf_get_lines(bufnr, 0, -1, true))
  end)
end)

-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local log = require 'alogger'
local class = require("oop.class")
local utbl = require("env.draw.utils.tbl");
local Point = require("env.draw.Point");
local Line = require("env.draw.Line");
local Text = require("env.draw.Text");
local Rectangle = require("env.draw.Rectangle");
local Container = require("env.draw.Container");
local M = require("env.draw.Canvas");
local E = {}


describe("env.draw.Canvas", function()
  after_each(function()
    log.fast_off()
  end)

  it("cast", function()
    local c = M:new(nil, 5, 10)
    assert.same(c, M.cast(c))

    assert.match_error(function()
      M.cast(Rectangle:new(nil, 1, 2, 3, 4))
    end, 'expected instance of env.draw.Canvas got: Class env.draw.Rectangle')

    assert.match_error(function()
      M.cast(nil)
    end, "attempt to index local 'obj' .a nil value.")

    assert.match_error(function()
      M.cast(1)
    end, "attempt to index local 'obj' .a number value.")

    assert.match_error(function()
      M.cast({})
    end, 'expected instance of env.draw.Canvas got: %? nil')
  end)

  it("new _init", function()
    local res = M:new(nil, 5, 10)
    local exp = {
      bg = {
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' }
      },
      layer = nil,
      objects = {},
      width = 10,
      height = 5,
      border = '',
    }
    assert.same(exp, res)
    local exp_lines = {
      '          ',
      '          ',
      '          ',
      '          ',
      '          '
    }
    assert.same(exp_lines, M.toLines(res))
  end)

  it("toLines", function()
    local c = M:new(nil, 2, 10)

    local exp = { '          ', '          ' }
    assert.same(exp, c:toLines())

    local t = { 'header' }
    c:toLines(t)
    t[#t + 1] = 'footer'
    local exp2 = {
      'header',
      '          ',
      '          ',
      'footer'
    }
    assert.same(exp2, t)
  end)

  -- ensure moved inside table( no indexes gaps)
  it("remove", function()
    local c = M:new(nil, 3, 8)
    local _, p1 = c:add(Point:new(nil, 1, 1))
    local _, p2 = c:add(Point:new(nil, 2, 2))
    local _, p3 = c:add(Point:new(nil, 3, 3))

    assert.same({ p1, p2, p3 }, c.objects)
    assert.same(3, c:indexOf(p3))
    assert.same(true, c:remove(p1) ~= nil)
    assert.same({ p2, p3 }, c.objects)
    assert.same(true, c:remove(p2) ~= nil)
    assert.same({ p3 }, c.objects)
    assert.same(1, c:indexOf(p3))
  end)

  it("indexOfAll", function()
    local c = M:new(nil, 3, 8)
    local _, p1 = c:add(Point:new(nil, 1, 1))
    local _, p2 = c:add(Point:new(nil, 2, 2))
    local _, p3 = c:add(Point:new(nil, 3, 3))

    assert.same({ 3, 2, 1 }, c:indexOfAll({ p3, p2, p1 }))
  end)

  -- it("remove_all_by_indexes", function()
  --   assert.same("", M.remove_all_by_indexes())
  -- end)

  it("remove_all", function()
    local c = M:new(nil, 3, 8)
    local _, p1 = c:add(Point:new(nil, 1, 1))
    local _, p2 = c:add(Point:new(nil, 2, 2))
    local _, p3 = c:add(Point:new(nil, 3, 3))
    local _, p4 = c:add(Point:new(nil, 4, 4))
    local _, p5 = c:add(Point:new(nil, 5, 5))
    assert.same({ p2, p5, p4 }, c:remove_all({ p2, p5, p4 }))
    assert.same({ p1, p3 }, c.objects)
  end)

  it("draw: merge bg with layer", function()
    local res = M:new(nil, 3, 8)
    assert.is_nil(res.layer)
    assert.same({}, res.objects)
    res:draw() -- res.layer = M.create_layer(8, 3, '')
    local exp = {
      { '', '', '', '', '', '', '', '' },
      { '', '', '', '', '', '', '', '' },
      { '', '', '', '', '', '', '', '' },
    }
    assert.same(exp, res.layer)

    local exp_lines = { '        ', '        ', '        ' }
    assert.same(exp_lines, M.toLines(res))

    res.layer[2][4] = '@'
    res.layer[1][2] = 'x'
    res.layer[3][8] = 'X'

    local exp_lines2 = { ' x      ', '   @    ', '       X' }
    assert.same(exp_lines2, M.toLines(res))

    -- recreate(clear) layer to new render
    res:draw()
    local exp_lines3 = { '        ', '        ', '        ' }
    assert.same(exp_lines3, M.toLines(res))
    -- todo "mark dirty" to reuse not changed layer
  end)

  it("new _init", function()
    local res = M:new(nil, 5, 10, ' ', 'r')
    local exp_lines = {
      '         |',
      '         |',
      '         |',
      '         |',
      '         |'
    }
    assert.same(exp_lines, M.toLines(res))
  end)


  it("get_objects_at", function()
    local c = M:new(nil, 10, 20)

    local _, r1 = c:add(Rectangle:new(nil, 1, 1, 4, 4))
    local _, r2 = c:add(Rectangle:new(nil, 5, 5, 8, 8))
    local _, r3 = c:add(Rectangle:new(nil, 3, 3, 6, 6))
    assert.same(r1, c.objects[1])
    assert.same(r2, c.objects[2])
    assert.same(r3, c.objects[3])
    local exp = {
      '+--+                ', -- r1
      '|  |                ',
      '| +--+              ', -- r3 overlap r2
      '+-|+ |              ',
      '  | +|-+            ', -- r2
      '  +--+ |            ',
      '    |  |            ',
      '    +--+            ',
      '                    ',
      '                    '
    }
    assert.same(exp, c:draw():toLines())
    assert.same({ r1 }, c:get_objects_at(1, 1))
    assert.same({ r3 --[[, r2]] }, c:get_objects_at(5, 5)) -- r3 overlap r2
    assert.same({ r3, r2 }, c:get_objects_at(6, 6, 7, 7))  -- reverse
    assert.same({ r2 }, c:get_objects_at(7, 7))

    local limit = 1
    assert.same({ r3 }, c:get_objects_at(5, 5, 5, 5, limit))

    -- corresponding indexes
    local objs, idxs = c:get_objects_at(1, 1, 8, 8, 99)
    assert.same({ 3, 2, 1 }, idxs)
    assert.same({ r3, r2, r1 }, objs)
  end)

  it("serialize desirialize(of) simple(no objects)", function()
    local c = M:new(nil, 4, 8)
    local bin = c:serialize_to_binary()
    local exp = [[
{
  width = 8, height = 4, border = '',
  classes = {
    [1] = 'oop.Object',
  },
  objects = {
  },
}
]]
    assert.same(exp, bin)
    local c2 = M.deserialize_from(bin)
    local exp2 = {
      bg = {
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
        { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' }
      },
      width = 8,
      height = 4,
      bg_color = ' ',
      border = '',
      objects = {},
    }
    assert.same(exp2, c2)
  end)

  -----------------------------------------------------------------------------

  local exp_serialized_canvas_rect_line_point_1 = [[
{
  width = 12, height = 4, border = '',
  classes = {
    [1] = 'oop.Object',
    [2] = 'env.draw.Rectangle',
    [3] = 'env.draw.Line',
    [4] = 'env.draw.Point',
  },
  objects = {
    {_clsid=2,x1=1,y1=1,x2=7,y2=4,hline='-',vline='|',corners='++++',bg=''},
    {_clsid=3,x1=9,y1=2,x2=9,y2=4,color='#'},
    {_clsid=4,x=11,y=1,color='+'},
  },
}
]]
  it("serialize object Rectangle Line Point", function()
    local height, width = 4, 12 -- lnum top of the canvas in the doc
    local c = M:new(nil, height, width)
    c:add(Rectangle:new(nil, 1, 1, 7, 4))
    c:add(Line:new(nil, 9, 2, 9, 4, '#'))
    c:add(Point:new(nil, 11, 1, '+'))
    -- 123456789012
    local exp_view = {
      '+-----+   + ',
      '|     | #   ',
      '|     | #   ',
      '+-----+ #   '
    }
    assert.same(exp_view, c:draw():toLines())
    local bin = c:serialize_to_binary()
    assert.same(exp_serialized_canvas_rect_line_point_1, bin)
  end)

  it("desirialize(back) Rectangle Line Point", function()
    local serialized = exp_serialized_canvas_rect_line_point_1
    -- build from serialized sting
    local canvas = M.deserialize_from(serialized)
    assert.same(3, #(canvas.objects or E))
    assert.same('env.draw.Rectangle', class.name(canvas.objects[1]))
    assert.same('env.draw.Line', class.name(canvas.objects[2]))
    assert.same('env.draw.Point', class.name(canvas.objects[3]))
    -- print("RECT:", require "inspect" (Rectangle.toArray(c2.objects[1])))
    -- print("LINE:", require "inspect" (Rectangle.toArray(c2.objects[2])))
    -- print("POINT:", require "inspect" (Rectangle.toArray(c2.objects[3])))
    local exp_rect = {
      bg = "",
      corners = { "+", "+", "+", "+" },
      bline = { x1 = 1, y1 = 4, x2 = 7, y2 = 4, color = "-" },
      lline = { x1 = 1, y1 = 2, x2 = 1, y2 = 3, color = "|" },
      rline = { x1 = 7, y1 = 2, x2 = 7, y2 = 3, color = "|" },
      tline = { x1 = 1, y1 = 1, x2 = 7, y2 = 1, color = "-" }
    }
    local exp_line = { x1 = 9, y1 = 2, x2 = 9, y2 = 4, color = "#" }
    local exp_point = { color = "+", x = 11, y = 1 }

    assert.same(exp_rect, canvas.objects[1])
    assert.same(exp_line, canvas.objects[2])
    assert.same(exp_point, canvas.objects[3])
    local exp_bg = {
      { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
      { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
      { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
      { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
    }
    assert.same(exp_bg, canvas.bg)

    local canvas_exp = {
      bg = exp_bg,
      border = '',
      width = 12,
      height = 4,
      bg_color = ' ',
      objects = { exp_rect, exp_line, exp_point }
    }

    assert.same(canvas_exp, canvas)
    local exp_view = {
      '+-----+   + ',
      '|     | #   ',
      '|     | #   ',
      '+-----+ #   '
    }
    assert.same(exp_view, canvas:draw():toLines())
  end)

  it("copy", function()
    local height, width = 4, 12
    local c = M:new(nil, height, width)
    c:add(Rectangle:new(nil, 1, 1, 7, 4))
    c:add(Line:new(nil, 9, 2, 9, 4, '#'))
    c:add(Point:new(nil, 11, 1, '+'))
    local exp_view = {
      '+-----+   + ',
      '|     | #   ',
      '|     | #   ',
      '+-----+ #   ' }
    assert.same(exp_view, c:draw():toLines())

    local c_copy = c:copy() -- workload

    assert.same('env.draw.Canvas', class.name(c))
    assert.same('env.draw.Canvas', class.name(c_copy))
    assert.same(exp_view, c_copy:draw():toLines())
    assert.same(c, c_copy)
    c_copy:remove(c_copy.objects[2]) -- remove line
    local exp2 = {
      '+-----+   + ',
      '|     |     ',
      '|     |     ',
      '+-----+     ' }
    assert.same(exp2, c_copy:draw():toLines())

    -- not changed
    assert.same(exp_view, c:draw():toLines())
  end)

  it("draw_text just the mechanics only without IDrawable element", function()
    local height, width = 3, 12
    local canvas = M:new(nil, height, width)
    local x1, y1, x2, y2 = 1, 2, width, height
    local text = 'Hello world!'
    canvas:draw() -- init layer
    canvas:draw_text(nil, x1, y1, x2, y2, text)
    local exp = {
      '            ',
      'Hello world!',
      '            ' }
    assert.same(exp, canvas:toLines())
  end)

  it("draw_text just the mechanics only without IDrawable element", function()
    local height, width = 4, 16
    local canvas = M:new(nil, height, width)
    local x1, y1, x2, y2 = 6, 2, width, height
    local text = 'Hello\nworld!'
    canvas:draw() -- init layer
    canvas:draw_text(nil, x1, y1, x2, y2, text)
    local exp = {
      '                ',
      '     Hello      ',
      '     world!     ',
      '                '
    }
    assert.same(exp, canvas:toLines())
  end)
end)

describe("env.draw.Canvas hooks", function()
  after_each(function()
    log.fast_off()
    M.MERGE_COLORS = false
  end)

  it("apply_hooks", function()
    local canvas = M:new(nil, 4, 12)
    M.MERGE_COLORS = true
    local exp = {
      '            ',
      '            ',
      '            ',
      '            '
    }
    assert.same(exp, canvas:toLines())

    ---@param c env.draw.Canvas
    local cb = function(c)
      c:draw_rect(nil, 1, 1, 12, 4, '.')
    end
    canvas:addHook(M.HOOK_PRE_DRAW, cb)

    local exp2 = {
      '............',
      '.          .',
      '.          .',
      '............'
    }
    assert.same(exp2, canvas:draw():toLines()) -- apply_hooks called in draw()

    ---@param c env.draw.Canvas
    local cb2 = function(c)
      c:draw_rect(nil, 4, 1, 9, 4, '`')
    end
    canvas:addHook(M.HOOK_POST_DRAW, cb2)

    local exp3 = {
      '...`::::`...',
      '.  `    `  .',
      '.  `    `  .',
      '...`::::`...'
    }
    assert.same(exp3, canvas:draw():toLines()) -- apply_hooks called in draw()
  end)

  it("getFirstNotEmptyLayoutCell", function()
    local canvas = M:new(nil, 6, 16)
    canvas:add(Line:new(nil, 5, 4, 10, 4, '-'))
    local exp = {
      --234567890123456
      '                ', -- 1
      '                ', -- 2
      '                ', -- 3
      '    ------      ', -- 4
      '                ',
      '                '
    }

    assert.same(exp, canvas:draw():toLines()) -- apply_hooks called in draw()
    ---@param c env.draw.Canvas
    local f = function(c, x1, y1, direction)
      local x, y, cell = c:getFirstNotEmptyLayoutCell(nil, x1, y1, direction)
      return tostring(x) .. "|" .. tostring(y) .. '|' .. tostring(cell) .. '|'
    end
    assert.same('5|4|-|', f(canvas, 0, 4, 'R'))
    assert.same('10|4|-|', f(canvas, 15, 4, 'L'))
    assert.same('5|4|-|', f(canvas, 5, 6, 'U'))
    assert.same('5|4|-|', f(canvas, 5, 1, 'D'))
  end)
end)

describe("env.draw.Canvas apply changes", function()
  after_each(function()
    log.fast_off()
    M.MERGE_COLORS = false
  end)

  it("get_elements_readable_list", function()
    local Canvas = M
    local f = M.get_elements_readable_list
    local c = Canvas:new(nil, 4, 12)
    c:add(Rectangle:new(nil, 0, 0, 1, 1))
    c:add(Rectangle:new(nil, 0, 0, 2, 2))
    c:add(Rectangle:new(nil, 0, 0, 3, 3))
    c:add(Rectangle:new(nil, 0, 0, 4, 4))
    c:add(Rectangle:new(nil, 0, 0, 5, 5))
    c:add(Rectangle:new(nil, 0, 0, 6, 6))
    local exp = {
      '  1  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""',
      '  3  Rectangle (0:0 3:3) (4x4) bg: ""',
      'Page: #1/2[3]'
    }
    assert.same(exp, f(c, 1, 3)) -- page 1
    local exp_p2 = {
      '  4  Rectangle (0:0 4:4) (5x5) bg: ""',
      '  5  Rectangle (0:0 5:5) (6x6) bg: ""',
      '  6  Rectangle (0:0 6:6) (7x7) bg: ""',
      'Page: #2/2[3]'
    }
    assert.same(exp_p2, f(c, 2, 3)) -- page 1
  end)


  local function prepare_canvas()
    local canvas = M:new(nil, 5, 16)
    canvas:add(Line:new(nil, 5, 2, 10, 2, '-'))
    canvas:add(Line:new(nil, 5, 4, 10, 4, '~'))
    canvas:add(Point:new(nil, 7, 3, '>'))
    canvas:add(Point:new(nil, 12, 5, '<'))
    return canvas
  end

  it("get_indexes_by_elements", function()
    local canvas = prepare_canvas()
    local e4 = canvas.objects[4]
    local e2 = canvas.objects[2]
    local e1 = canvas.objects[1]
    local elms = { e1, e2, e4 }
    assert.same(3, #elms)

    assert.same('Point 12:5 color: <', tostring(e4))
    assert.same(3, utbl.tbl_indexof(elms, e4))

    assert.same({ 4, 2, 1 }, canvas:get_indexes_by_elements(elms))
  end)
end)

describe("env.draw.Canvas serialize/deserialize", function()
  it("1", function()
    local container = Container:new(nil, 1, 1, 20, 6)
    container:add(Rectangle:new(nil, 1, 1, 10, 4))
    container:add(Line:new(nil, 2, 2, 9, 2, '='))
    container:add(Text:new(nil, 3, 3, 'Smthg'))

    local canvas = M:new(nil, 6, 24)
    canvas:add(container)
    local exp_s = [[
Canvas w:24 h:6 objects:1
   1:  Container (1:1 20:6) (20x6) elms: 3 inv:
     1: Rectangle (1:1 10:4) (10x4) bg: ""
     2: Line (2:2 9:2) color: =
     3: Text (3:3 7:3) (5x1) "Smthg"

]]
    assert.same(exp_s, canvas:__tostring(true))

    local bin = canvas:serialize_to_binary()
    local exp = [[
{
  width = 24, height = 6, border = '',
  classes = {
    [1] = 'oop.Object',
    [2] = 'env.draw.Rectangle',
    [3] = 'env.draw.Line',
    [4] = 'env.draw.Text',
    [5] = 'env.draw.Container',
  },
  objects = {
    {
      _clsid=5,x1=1,y1=1,x2=20,y2=6,
      inv = {
        {_clsid=2,x1=1,y1=1,x2=10,y2=4,hline='-',vline='|',corners='++++',bg=''},
        {_clsid=3,x1=2,y1=2,x2=9,y2=2,color='='},
        {_clsid=4,x1=3,y1=3,x2=7,y2=3,text='Smthg'},
      }
    },
  },
}
]]
    assert.same(exp, bin)

    -- desirialize from binary text back to instance
    local rc = M.deserialize_from(bin) ---@cast rc env.draw.Canvas
    assert.same(exp_s, rc:__tostring(true))
  end)
end)

describe("env.draw.Canvas get_distsq_elms_to_pos", function()
  local function prepare_canvas_4rect()
    local Canvas = M
    local c = Canvas:new(nil, 10, 24)
    local _, r1 = c:add(Rectangle:new(nil, 2, 1, 5, 3):tagged('tl'))
    local _, r3 = c:add(Rectangle:new(nil, 20, 1, 24, 3):tagged('tr'))
    local _, r4 = c:add(Rectangle:new(nil, 20, 8, 24, 10):tagged('br'))
    local _, r2 = c:add(Rectangle:new(nil, 2, 8, 5, 10):tagged('bl'))
    return c, r1, r2, r3, r4
  end

  local function readable(t)
    local s = {}
    for _, e in ipairs(t) do
      s[#s + 1] = e.dist .. ' ' .. tostring(e.elm)
    end
    return s
  end

  it("find_closest_elements", function()
    local c = prepare_canvas_4rect()
    local x, y = 7, 5
    c:add(Point:new(nil, x, y))
    local exp = {
      ' +--+              +---+',
      ' |  |              |   |',
      ' +--+              +---+',
      '                        ',
      '      +                 ',
      '                        ',
      '                        ',
      ' +--+              +---+',
      ' |  |              |   |',
      ' +--+              +---+'
    }
    assert.same(exp, c:draw():toLines())
    local expd = {
      '8 Rectangle (2:1 5:3) (4x3) bg: "" tl',
      '13 Rectangle (2:8 5:10) (4x3) bg: "" bl',
      '173 Rectangle (20:1 24:3) (5x3) bg: "" tr',
      '178 Rectangle (20:8 24:10) (5x3) bg: "" br'
    }
    assert.same(expd, readable(c:get_distsq_elms_to_pos(Rectangle, x, y)))
  end)

  it("2", function()
    local c = prepare_canvas_4rect()
    local x, y = 10, 2
    c:add(Point:new(nil, x, y))
    local exp = {
      ' +--+              +---+',
      ' |  |    +         |   |',
      ' +--+              +---+',
      '                        ',
      '                        ',
      '                        ',
      '                        ',
      ' +--+              +---+',
      ' |  |              |   |',
      ' +--+              +---+'
    }
    assert.same(exp, c:draw():toLines())
    local expd = {
      '25 Rectangle (2:1 5:3) (4x3) bg: "" tl',
      '61 Rectangle (2:8 5:10) (4x3) bg: "" bl',
      '100 Rectangle (20:1 24:3) (5x3) bg: "" tr',
      '136 Rectangle (20:8 24:10) (5x3) bg: "" br'
    }
    assert.same(expd, readable(c:get_distsq_elms_to_pos(Rectangle, x, y)))
  end)

  it("2", function()
    local c = prepare_canvas_4rect()
    local x, y = 6, 2
    c:add(Point:new(nil, x, y))
    local exp = {
      ' +--+              +---+',
      ' |  |+             |   |',
      ' +--+              +---+',
      '                        ',
      '                        ',
      '                        ',
      '                        ',
      ' +--+              +---+',
      ' |  |              |   |',
      ' +--+              +---+'
    }
    assert.same(exp, c:draw():toLines())
    local expd = {
      '1 Rectangle (2:1 5:3) (4x3) bg: "" tl',
      '37 Rectangle (2:8 5:10) (4x3) bg: "" bl',
      '196 Rectangle (20:1 24:3) (5x3) bg: "" tr',
      '232 Rectangle (20:8 24:10) (5x3) bg: "" br'
    }
    assert.same(expd, readable(c:get_distsq_elms_to_pos(Rectangle, x, y)))
  end)
end)

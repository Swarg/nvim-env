-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local log = require 'alogger'
local class = require("oop.class")
local Line = require("env.draw.Line");
local Canvas = require("env.draw.Canvas");
local IEditable = require("env.draw.IEditable");
local H = require("env.draw.test_helper");

local SH = class.serialize_helper
local new_cname2id, invert_map = SH.new_cname2id, SH.invert_map
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


describe("env.draw.Line", function()
  after_each(function()
    log.fast_off()
  end)

  it("_init", function()
    local exp = { x1 = 1, x2 = 3, y2 = 4, color = '-', y1 = 2 }
    assert.same(exp, Line:new(nil, 1, 2, 3, 4, '-'))

    local exp2 = { x1 = 1, x2 = 3, y2 = 4, color = '-', y1 = 2 }
    assert.same(exp2, Line:new(nil, 1, 2, 3, 4, '-'))

    local exp3 = { x1 = 1, x2 = 3, y2 = 4, color = '.', y1 = 2 }
    assert.same(exp3, Line:new(nil, 1, 2, 3, 4, '.'))

    local exp4 = { x1 = 1, x2 = 3, y2 = 4, color = '#', y1 = 2 }
    assert.same(exp4, Line:new(nil, 1, 2, 3, 4))
  end)

  it("set_coords", function()
    local l = Line:new(nil, 1, 2, 3, 4, '-')
    assert.same('Line (1:2 3:4) color: -', v2s(l))

    l:set_coords(4, 5, 7, 8)
    assert.same('Line (4:5 7:8) color: -', v2s(l))
  end)


  it("draw horizontal", function()
    local canvas = Canvas:new(nil, 1, 8)
    assert.same({ '        ' }, canvas:toLines())
    local f = function(l) return canvas:add(l):draw():rm_objects():toLines() end

    assert.same({ '########' }, f(Line:new(nil, 1, 1, 8, 1)))

    assert.same({ ' ###### ' }, f(Line:new(nil, 2, 1, 7, 1)))

    canvas:add(Line:new(nil, 3, 1, 4, 1)):draw():rm_objects()
    assert.same({ '  ##    ' }, f(Line:new(nil, 3, 1, 4, 1)))

    assert.same({ '   #    ' }, f(Line:new(nil, 4, 1, 4, 1)))

    -- reversed x1 > x2
    assert.same({ '########' }, f(Line:new(nil, 8, 1, 1, 1)))
    -- assert.has_error(function() f(Line:new(nil, 5, 1, 4, 1)) end)
  end)


  it("draw vertical", function()
    local canvas = Canvas:new(nil, 5, 8)
    local f = function(l) return canvas:add(l):draw():rm_objects():toLines() end
    local exp = { '        ', '        ', '        ', '        ', '        ' }
    assert.same(exp, canvas:toLines())

    exp = {
      '#       ',
      '#       ',
      '#       ',
      '#       ',
      '#       ' }
    assert.same(exp, f(Line:new(nil, 1, 1, 1, 5)))

    exp = {
      '        ',
      '#       ',
      '#       ',
      '#       ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 1, 2, 1, 4)))

    exp = {
      '        ',
      '        ',
      '#       ',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 1, 3, 1, 3)))

    exp = {
      '#       ',
      '#       ',
      '#       ',
      '#       ',
      '#       '
    }
    assert.same(exp, f(Line:new(nil, 1, -10, 1, 10)))
  end)

  it("draw vertical 2", function()
    local canvas = Canvas:new(nil, 5, 8)
    local f = function(l) return canvas:add(l):draw():rm_objects():toLines() end
    local exp = { '        ', '        ', '        ', '        ', '        ' }
    assert.same(exp, canvas:toLines())

    exp = {
      '       #',
      '       #',
      '       #',
      '       #',
      '       #' }
    assert.same(exp, f(Line:new(nil, 8, 1, 8, 5)))

    exp = {
      '        ',
      '       #',
      '       #',
      '       #',
      '        ' }
    assert.same(exp, f(Line:new(nil, 8, 2, 8, 4)))

    exp = {
      '        ',
      '        ',
      '       #',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 8, 3, 8, 3)))

    exp = {
      '       #',
      '       #',
      '       #',
      '       #',
      '       #' }
    assert.same(exp, f(Line:new(nil, 8, -10, 8, 10)))

    -- y1 > y2
    exp = {
      '        ',
      '       #',
      '       #',
      '       #',
      '        '
    }
    assert.same(exp, f(Line:new(nil, 8, 4, 8, 2)))
  end)

  it("draw horizontal out of canvas by y", function()
    local canvas = Canvas:new(nil, 5, 8)
    local f = function(l) return canvas:add(l):draw():rm_objects():toLines() end

    local exp = {
      '        ',
      '        ',
      '        ',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 1, 0, 8, 0))) -- out of canvas by y(top)

    exp = {
      '        ',
      '        ',
      '        ',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 1, 10, 8, 10))) -- out of canvas by y(bottom)
  end)

  it("draw vertical out of canvas by x", function()
    local canvas = Canvas:new(nil, 5, 8)
    local f = function(l) return canvas:add(l):draw():rm_objects():toLines() end

    local exp = {
      '        ',
      '        ',
      '        ',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, -1, 1, -1, 5))) -- out of canvas by x(left)

    exp = {
      '        ',
      '        ',
      '        ',
      '        ',
      '        ' }
    assert.same(exp, f(Line:new(nil, 10, 1, 10, 5))) -- out of canvas by x(rigth)
  end)

  it("isInsidePos horizontal", function()
    local l = Line:new(nil, 1, 1, 8, 1)
    assert.same(true, l:isInsidePos(1, 1))
    assert.same(true, l:isInsidePos(2, 1))
    assert.same(true, l:isInsidePos(3, 1))
    assert.same(true, l:isInsidePos(5, 1))
    assert.same(true, l:isInsidePos(7, 1))
    assert.same(true, l:isInsidePos(8, 1))
    assert.same(false, l:isInsidePos(9, 1))
    assert.same(false, l:isInsidePos(0, 1))
    assert.same(false, l:isInsidePos(8, 8))
    assert.same(false, l:isInsidePos(1, 0))
    assert.same(false, l:isInsidePos(2, 0))
    assert.same(false, l:isInsidePos(3, 0))
    assert.same(false, l:isInsidePos(5, 0))
    assert.same(false, l:isInsidePos(7, 0))
    assert.same(false, l:isInsidePos(8, 0))

    assert.same(false, l:isInsidePos(1, 2))
    assert.same(false, l:isInsidePos(2, 2))
    assert.same(false, l:isInsidePos(3, 2))
    assert.same(false, l:isInsidePos(5, 2))
    assert.same(false, l:isInsidePos(7, 2))
    assert.same(false, l:isInsidePos(8, 2))
  end)

  it("isInsidePos vertical", function()
    local l = Line:new(nil, 1, 1, 1, 5)
    assert.same(true, l:isInsidePos(1, 1))
    assert.same(true, l:isInsidePos(1, 2))
    assert.same(true, l:isInsidePos(1, 3))
    assert.same(true, l:isInsidePos(1, 5))
    assert.same(false, l:isInsidePos(1, 6))
    assert.same(false, l:isInsidePos(1, 0))

    assert.same(false, l:isInsidePos(2, 1))
    assert.same(false, l:isInsidePos(2, 2))
    assert.same(false, l:isInsidePos(2, 3))
    assert.same(false, l:isInsidePos(2, 5))
  end)
end)

------------------------------------------------------------------------------

describe("env.draw.Line cli & serialization", function()
  after_each(function()
    log.fast_off()
  end)
  ------------------------------------------------------------------------------

  it("serialize desirialize", function()
    local line = Line:new(nil, 2, 2, 2, 4, '+')
    local opts = { cname2id = new_cname2id() }
    local arr = line:serialize(opts)
    local exp = { _clsid = 2, x1 = 2, y1 = 2, x2 = 2, y2 = 4, color = '+' }
    assert.same(exp, arr)
    assert.same('env.draw.Line', class.name(line))

    local rl = Line:fromArray(arr)
    assert.same('env.draw.Line', class.name(rl))

    assert.is_function(rl.edit)
  end)

  it("serialize desirialize", function()
    local line = Line:new(nil, 2, 2, 2, 4, '+')

    local opts = { cname2id = new_cname2id() }
    local arr = line:serialize(opts)
    local exp_arr = { _clsid = 2, x1 = 2, color = '+', x2 = 2, y2 = 4, y1 = 2 }
    assert.same(exp_arr, arr)
    assert.same('env.draw.Line', class.name(line))

    -- 1 way to deserialize arr to object powered by oop.Object
    local rl = Line:fromArray(arr)
    assert.same('env.draw.Line', class.name(rl))

    local patch = {
      x1 = { new = 3, old = 2 },
      y1 = { new = 4, old = 2 },
      x2 = { new = 5, old = 2 },
      y2 = { new = 6, old = 4 },
      color = { new = '-', old = '+' },
    }
    rl:edit(patch)
    local exp2 = { x1 = 3, y1 = 4, x2 = 5, y2 = 6, color = '-' }
    assert.same(exp2, rl)

    -- 2 way to desirialize via def method
    local line2 = IEditable.deserialize(arr, invert_map(opts.cname2id))
    assert.same('env.draw.Line', class.name(line2))
    -- ensure line2 is a instance with methods of Line
    --
    local patch2 = {
      x1 = { new = 4, old = 2 },
      y1 = { new = 5, old = 2 },
      x2 = { new = 7, old = 2 },
      y2 = { new = 9, old = 4 },
      color = { new = '#', old = '+' },
    }
    line2:edit(patch2)
    local exp_line_obj = { x1 = 4, y1 = 5, x2 = 7, y2 = 9, color = '#' }
    assert.same(exp_line_obj, line2)
  end)


  it("fmtSerialized", function()
    local line = Line:new(nil, 1, 2, 3, 4, '+')
    local opts = { cname2id = new_cname2id() }

    local arr = line:serialize(opts)
    opts.ident = ''
    local exp = "{_clsid=2,x1=1,y1=2,x2=3,y2=4,color='+'}"
    assert.same(exp, line:fmtSerialized(arr, 0, opts))

    local exp_map = { ['oop.Object'] = 1, ['env.draw.Line'] = 2, next_id = 3 }
    assert.same(exp_map, opts.cname2id)
  end)
end)

describe("env.draw.Line intersects", function()
  after_each(function()
    log.fast_off()
    Canvas.MERGE_COLORS = false
  end)

  it("isIntersects horizontal", function()
    local ci = H.check_intersects
    local c = Canvas:new(nil, 4, 12)
    Canvas.MERGE_COLORS = true
    local _, l = c:add(Line:new(nil, 3, 2, 10, 2, '-')) ---@cast l env.draw.Line
    assert.same('Line (3:2 10:2) color: -', tostring(l))
    assert.same(true, l:isIntersects(3, 2, 10, 2))
    ---  123456789012
    local exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~',
        '~ -------- ~',
        '~          ~',
        '~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 12, 4))
    exp = {
      intersects = true,
      {
        '~~~~~~~~    ',
        '~ -----=--  ', -- ~ and - gives =
        '~      ~    ',
        '~~~~~~~~    '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 8, 4))
    exp = {
      intersects = true,
      {
        '            ',
        '  ========  ', -- ~ and - gives =
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 3, 2, 10, 2))
    exp = {
      intersects = false,
      {
        '~           ',
        '  --------  ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 1, 1))
    exp = {
      intersects = false,
      {
        '            ',
        '  --------  ',
        '          ~ ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 11, 3, 11, 3))
    exp = {
      intersects = false,
      {
        '            ',
        '  --------~ ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 11, 2, 11, 2))
    exp = {
      intersects = false,
      {
        '~~          ',
        '~~--------  ',
        '~~          ',
        '~~          '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 2, 4))
    exp = {
      intersects = true,
      {
        '~~~         ',
        '~ =-------  ',
        '~ ~         ',
        '~~~         '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 3, 4))
    exp = {
      intersects = true,
      {
        '         ~~~',
        '  -------= ~',
        '         ~ ~',
        '         ~~~'
      }
    }
    assert.same(exp, ci(c, l, 10, 1, 12, 4))
    exp = {
      intersects = false,
      {
        '          ~~',
        '  --------~~',
        '          ~~',
        '          ~~'
      }
    }
    assert.same(exp, ci(c, l, 11, 1, 12, 4))
    exp = {
      intersects = true,
      {
        '    ~       ',
        '  --=-----  ',
        '    ~       ',
        '    ~       '
      }
    }
    assert.same(exp, ci(c, l, 5, 1, 5, 4))
    exp = {
      intersects = false,
      {
        '            ',
        '  --------  ',
        '    ~       ',
        '    ~       '
      }
    }
    assert.same(exp, ci(c, l, 5, 3, 5, 4))
    exp = {
      intersects = true,
      {
        '            ',
        '  =-------  ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 3, 2, 3, 2))
    exp = {
      intersects = true,
      {
        '            ',
        '  -------=  ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 10, 2, 10, 2))
    exp = {
      intersects = true,
      {
        '            ',
        '  ----=---  ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 7, 2, 7, 2))
    exp = {
      intersects = true,
      {
        '            ',
        '  --===---  ',
        '            ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 5, 2, 7, 2))
  end)

  it("isIntersects vertical", function()
    local ci = H.check_intersects
    local c = Canvas:new(nil, 4, 12)
    Canvas.MERGE_COLORS = true
    local _, l = c:add(Line:new(nil, 6, 2, 6, 3, '|')) ---@cast l env.draw.Line
    assert.same('Line (6:2 6:3) color: |', tostring(l))
    assert.same(true, l:isIntersects(3, 2, 10, 2))
    ---  123456789012
    local exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~',
        '~    |     ~',
        '~    |     ~',
        '~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 12, 4))
    exp = {
      intersects = true,
      {
        '~~~~~~~~    ',
        '~    | ~    ',
        '~    | ~    ',
        '~~~~~~~~    '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 8, 4))
    exp = {
      intersects = true,
      {
        '~~~~~~      ',
        '~    +      ',
        '~    +      ',
        '~~~~~~      '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 6, 4))
    exp = {
      intersects = true,
      {
        '     ~~~~~~~',
        '     +     ~',
        '     +     ~',
        '     ~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 6, 1, 12, 4))
    exp = {
      intersects = false,
      {
        '     ~~~~~~~',
        '     |      ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 6, 1, 12, 1))
    exp = {
      intersects = false,
      {
        '            ',
        '     |      ',
        '     |      ',
        '     ~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 6, 4, 12, 4))
    exp = {
      intersects = true,
      {
        '     ~~~~~~~',
        '     +~~~~~~',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 6, 1, 12, 2))
    exp = {
      intersects = true,
      {
        '            ',
        '     |      ',
        '     +~~~~~~',
        '     ~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 6, 3, 12, 4))
    exp = {
      intersects = true,
      {
        '            ',
        '  ~~~+~~~~  ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 3, 2, 10, 2))
    exp = {
      intersects = false,
      {
        '~           ',
        '     |      ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 1, 1))
    exp = {
      intersects = false,
      {
        '            ',
        '     |      ',
        '     |    ~ ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 11, 3, 11, 3))
    exp = {
      intersects = false,
      {
        '            ',
        '     |    ~ ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 11, 2, 11, 2))
    exp = {
      intersects = false,
      {
        '~~~~~       ',
        '~~~~~|      ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 5, 2))
    exp = {
      intersects = false,
      {
        '~~~         ',
        '~ ~  |      ',
        '~ ~  |      ',
        '~~~         '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 3, 4))
    exp = {
      intersects = false,
      {
        '~~~~~       ',
        '~   ~|      ',
        '~   ~|      ',
        '~~~~~       '
      }
    }
    assert.same(exp, ci(c, l, 1, 1, 5, 4))
    exp = {
      intersects = false,
      {
        '      ~~~~~~',
        '     |~    ~',
        '     |~    ~',
        '      ~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 7, 1, 12, 4))
    exp = {
      intersects = false,
      {
        '    ~       ',
        '    ~|      ',
        '    ~|      ',
        '    ~       '
      }
    }
    assert.same(exp, ci(c, l, 5, 1, 5, 4))
    exp = {
      intersects = true,
      {
        '     ~      ',
        '     +      ',
        '     +      ',
        '     ~      '
      }
    }
    assert.same(exp, ci(c, l, 6, 1, 6, 4))
    exp = {
      intersects = false,
      {
        '            ',
        '     |      ',
        '    ~|      ',
        '    ~       '
      }
    }
    assert.same(exp, ci(c, l, 5, 3, 5, 4))
    exp = {
      intersects = false,
      {
        '            ',
        '     |~     ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 7, 2, 7, 2))
    exp = {
      intersects = false,
      {
        '            ',
        '    ~|      ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 5, 2, 5, 2))
    exp = {
      intersects = true,
      {
        '            ',
        '    ~+~     ',
        '     |      ',
        '            '
      }
    }
    assert.same(exp, ci(c, l, 5, 2, 7, 2))
    exp = {
      intersects = false,
      {
        '            ',
        '     |      ',
        '     |      ',
        '~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 1, 4, 12, 4))
    exp = {
      intersects = true,
      {
        '            ',
        '     |      ',
        '~~~~~+~~~~~~',
        '~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, l, 1, 3, 12, 4))
  end)
end)

-- 08-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local Canvas = require("env.draw.Canvas");
local Entity = require("env.draw.notation.db.Entity");
local H = require("env.draw.test_helper");

local show = H.show_element

local function newEntity(name, etype)
  local x1, y1, x2, y2 = 0, 0, nil, nil -- use default width and height
  return Entity:new(nil, x1, y1, x2, y2, name, etype)
end

describe("env.draw.notation.db.Entity", function()
  it("new_entity", function()
    local e = newEntity('Person', 'strong')
    assert.same('Entity(strong) Person (0:0 24:8) (25x9) attrs: 1', tostring(e))
    local exp = [[
Entity(strong) Person (0:0 24:8) (25x9) attrs: 1 inv:
 1: Attribute (2:3 22:4): PersonId id M PK:true

]]
    assert.same(exp, Entity.__tostring(e, true) .. "\n")

    local exp2 = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId  id  M       ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    assert.same(exp2, show(e))
  end)

  it("new_entity weak", function()
    local e = newEntity('Position', 'weak')

    local canvas = Canvas:new(nil, 10, 32)
    canvas:add(e)
    e:move(4, 1)
    local exp2 = {
      '   ╔═══════════════════════╗    ',
      '   ║       Position        ║    ',
      '   ║───────────────────────║    ',
      '   ║                       ║    ',
      '   ║                       ║    ',
      '   ║                       ║    ',
      '   ║                       ║    ',
      '   ║                       ║    ',
      '   ╚═══════════════════════╝    ',
      '                                '
    }
    assert.same(exp2, canvas:draw():toLines())
  end)

  it("new_entity association", function()
    local e = newEntity('Position', 'association')

    local canvas = Canvas:new(nil, 10, 32)
    canvas:add(e)
    e:move(4, 1)
    local exp2 = {
      '   ╭───────────────────────╮    ',
      '   │       Position        │    ',
      '   │───────────────────────│    ',
      '   │                       │    ',
      '   │                       │    ',
      '   │                       │    ',
      '   │                       │    ',
      '   │                       │    ',
      '   ╰───────────────────────╯    ',
      '                                '
    }
    assert.same(exp2, canvas:draw():toLines())
  end)

  it("add_attribute", function()
    local e = newEntity('Person')

    -- require'alogger'.fast_setup(nil, 0)
    Entity.add_attribute(e, 'account_id', 'id')
    Entity.add_attribute(e, 'name', 'pname')
    local exp = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId    id     M  ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃ account_id  id        ┃    ',
      '   ┃ name        pname     ┃    ',
      '   ┃                       ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    --      6           18   23
    assert.same(exp, show(e))
  end)

  it("set_coords 25x10", function()
    local e = newEntity('Person')

    -- require'alogger'.fast_setup(nil, 0)
    Entity.add_attribute(e, 'account_id', 'id')
    Entity.add_attribute(e, 'name', 'pname')
    e:set_coords(0, 0, 24, 9)
    local exp = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId    id     M  ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃ account_id  id        ┃    ',
      '   ┃ name        pname     ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    '
    }
    --      6           18   23
    assert.same(exp, show(e))
  end)

  it("set_coords 25x10", function()
    local e = newEntity('Person')

    -- require'alogger'.fast_setup(nil, 0)
    Entity.add_attribute(e, 'account_id', 'id')
    Entity.add_attribute(e, 'name', 'pname')
    Entity.add_attribute(e, 'another', 'longdomain', 'M K1')
    Entity.add_attribute(e, 'field', 'int', 'M K1 PK', true)
    Entity.add_attribute(e, 'something', 'varchar', 'M')
    e:set_coords(0, 0, 34, 11)
    local exp = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓  ',
      '   ┃             Person              ┃  ',
      '   ┃─────────────────────────────────┃  ',
      '   ┃ PersonId    id          M       ┃  ',
      '   ┃ ▔▔▔▔▔▔▔▔                        ┃  ',
      '   ┃ account_id  id                  ┃  ',
      '   ┃ name        pname               ┃  ',
      '   ┃ another     longdomain  M K1    ┃  ',
      '   ┃ field       int         M K1 PK ┃  ',
      '   ┃ ▔▔▔▔▔                           ┃  ',
      '   ┃ something   varchar     M       ┃  ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛  '
    }
    --      6           18   23
    assert.same(exp, show(e, 40, 12))
  end)

  --

  it("fmtSerialized deep 2", function()
    local e = newEntity('Person')

    Entity.add_attribute(e, 'account_id', 'id')
    Entity.add_attribute(e, 'name', 'pname')
    local opts = {}
    local serialized = e:serialize({})
    local exp = [[
  {
    _class='env.draw.notation.db.Entity',x1=0,y1=0,x2=24,y2=8,name='Person',etype='strong',
    attributes = {
      {_class='env.draw.notation.db.Attribute',y=3,x1=2,xne=10,xt=14,xp=21,x2=22,name='PersonId',atype='id',props='M',pk=true},
      {_class='env.draw.notation.db.Attribute',y=5,x1=2,xne=12,xt=14,xp=21,x2=22,name='account_id',atype='id',props=''},
      {_class='env.draw.notation.db.Attribute',y=6,x1=2,xne=6,xt=14,xp=21,x2=22,name='name',atype='pname',props=''},
    }
  }]]
    assert.same(exp, e:fmtSerialized(serialized, 2, opts))
  end)

  it("fmtSerialized deep 0", function()
    local e = newEntity('Person')

    -- require'alogger'.fast_setup(nil, 0)
    Entity.add_attribute(e, 'account_id', 'id')
    Entity.add_attribute(e, 'name', 'pname')
    local opts = {}
    local serialized = e:serialize(opts)
    local exp = [[
{
  _class='env.draw.notation.db.Entity',x1=0,y1=0,x2=24,y2=8,name='Person',etype='strong',
  attributes = {
    {_class='env.draw.notation.db.Attribute',y=3,x1=2,xne=10,xt=14,xp=21,x2=22,name='PersonId',atype='id',props='M',pk=true},
    {_class='env.draw.notation.db.Attribute',y=5,x1=2,xne=12,xt=14,xp=21,x2=22,name='account_id',atype='id',props=''},
    {_class='env.draw.notation.db.Attribute',y=6,x1=2,xne=6,xt=14,xp=21,x2=22,name='name',atype='pname',props=''},
  }
}]]
    assert.same(exp, e:fmtSerialized(serialized, 0, opts))
  end)
end)


-- simplification useful for studying er-modeling
-- the main task is to quickly create entities through console commands
describe("env.draw.notation.db.Entity simplified attrs", function()
  local dbn = require("env.draw.notation.db.base")

  it("add_simplified_attrs EntName & +Something", function()
    local function f(e, attrs)
      dbn.add_simplified_attrs(e, attrs)
      return e:__tostring(true)
    end
    local order = newEntity('Order')
    local exp = [[
Entity(strong) Order (0:0 24:8) (25x9) attrs: 3 inv:
 1: Attribute (2:3 22:4): OrderId id M PK:true
 2: Attribute (2:5 22:5): OrderName   PK:false
 3: Attribute (2:6 22:6): OrderDate   PK:false
]]
    assert.same(exp, f(order, { '+name', '+date' }))
  end)

  it("add_simplified_attrs id+", function()
    local function f(e, attrs)
      dbn.add_simplified_attrs(e, attrs)
      return e:__tostring(true)
    end
    local order = newEntity('Order')
    local exp = [[
Entity(strong) Order (0:0 24:8) (25x9) attrs: 1 inv:
 1: Attribute (2:3 22:3): OrderId   PK:false
]]
    assert.same(exp, f(order, { 'id+' }))
  end)

  -- not same as id+
  it("add_simplified_attrs +id", function()
    local function f(e, attrs)
      dbn.add_simplified_attrs(e, attrs)
      return e:__tostring(true)
    end
    local order = newEntity('Order')
    local exp = [[
Entity(strong) Order (0:0 24:8) (25x9) attrs: 1 inv:
 1: Attribute (2:3 22:4): OrderId id M PK:true
]]
    assert.same(exp, f(order, { '+id' }))
  end)

  -- not same as id+
  it("add_simplified_attrs +id", function()
    local function f(e, attrs)
      dbn.add_simplified_attrs(e, attrs)
      return e:__tostring(true)
    end
    local order = newEntity('Employee')
    local exp = [[
Entity(strong) Employee (0:0 24:8) (25x9) attrs: 10 inv:
 1: Attribute (2:3 22:3): EmployeeId   PK:false
 2: Attribute (2:4 22:4): FirstName   PK:false
 3: Attribute (2:5 22:5): LastName   PK:false
 4: Attribute (2:6 22:6): Street   PK:false
 5: Attribute (2:7 22:7): Apartment   PK:false
 6: Attribute (2:8 22:8): City   PK:false
 7: Attribute (2:9 22:9): State   PK:false
 8: Attribute (2:10 22:10): ZipCode   PK:false
 9: Attribute (2:11 22:11): PhoneNumber   PK:false
10: Attribute (2:12 22:12): EmailAddress   PK:false
]]
    assert.same(exp, f(order, { 'id+', 'name+', 'addr+', 'contact+' }))
  end)
end)

describe("env.draw.notation.db.Entity remove", function()
  local function new_entity(name)
    return Entity:new(nil, 0, 0, 0, 0, name or 'Person')
  end

  it("remove_attribute", function()
    local e = new_entity('Person')
    e:move(4, 1)
    e:add_attribute('attr_2', 'id')
    e:add_attribute('attr_3', 'id')
    e:add_attribute('attr_4', 'id')
    local exp = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId  id  M       ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃ attr_2    id          ┃    ',
      '   ┃ attr_3    id          ┃    ',
      '   ┃ attr_4    id          ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    assert.same(exp, show(e))
    assert.is_not_nil(e:remove_attribute('attr_2'))

    local exp2 = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId  id  M       ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃                       ┃    ',
      '   ┃ attr_3    id          ┃    ',
      '   ┃ attr_4    id          ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    assert.same(exp2, show(e))

    assert.is_not_nil(e:remove_attribute('attr_3'))
    local exp3 = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId  id  M       ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┃ attr_4    id          ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    assert.same(exp3, show(e))

    assert.is_not_nil(e:remove_attribute('attr_4'))
    local exp4 = {
      '   ┏━━━━━━━━━━━━━━━━━━━━━━━┓    ',
      '   ┃        Person         ┃    ',
      '   ┃───────────────────────┃    ',
      '   ┃ PersonId  id  M       ┃    ',
      '   ┃ ▔▔▔▔▔▔▔▔              ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┃                       ┃    ',
      '   ┗━━━━━━━━━━━━━━━━━━━━━━━┛    ',
      '                                '
    }
    assert.same(exp4, show(e))
  end)
end)


describe("env.draw.notation.db.Entity deserialize", function()
  it("weak", function()
    local serialized =
    {
      _class = "env.draw.notation.db.Entity",
      x1 = 2,
      y1 = 1,
      x2 = 24,
      y2 = 8,
      name = 'Mark',
      etype = 'weak',
      attributes = {
      }
    }
    local e = Entity.deserialize(serialized)
    local exp = {
      ' ╔═════════════════════╗        ',
      ' ║        Mark         ║        ',
      ' ║─────────────────────║        ',
      ' ║                     ║        ',
      ' ║                     ║        ',
      ' ║                     ║        ',
      ' ║                     ║        ',
      ' ╚═════════════════════╝        ',
      '                                ',
      '                                '
    }
    assert.same(exp, show(e))
  end)

  it("weak", function()
    local serialized =
    {
      _class = "env.draw.notation.db.Entity",
      x1 = 2,
      y1 = 1,
      x2 = 24,
      y2 = 8,
      name = 'Mark',
      etype = 'association',
      attributes = {
      }
    }
    local e = Entity.deserialize(serialized)
    local exp = {
      ' ╭─────────────────────╮        ',
      ' │        Mark         │        ',
      ' │─────────────────────│        ',
      ' │                     │        ',
      ' │                     │        ',
      ' │                     │        ',
      ' │                     │        ',
      ' ╰─────────────────────╯        ',
      '                                ',
      '                                '
    }
    assert.same(exp, show(e))
  end)
end)

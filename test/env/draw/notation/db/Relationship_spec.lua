-- 17-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local M = require("env.draw.notation.db.Relationship");

local udb = require 'env.draw.notation.db.base'
local H = require("env.draw.test_helper");

local RELATIONSHIP = udb.RELATIONSHIP

local show = H.show_element
local mk_pos_list = H.mk_pos_list
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


-- helpers
--
local conf_width = 24
local conf_height = 6

---@return env.draw.Container
---@return env.draw.notation.db.Relationship
local function mkRelationAndDraw(rt, rs, re, ...)
  assert(select('#', ...) > 1, 'expected points at least two points')

  local ok, link = M.new_from_plist(mk_pos_list(...), rt, rs, re)
  assert(ok, link) -- 'sure successed')
  ---@cast link env.draw.notation.db.Relationship
  return show(link, conf_width, conf_height), link
end


describe("env.draw.notation.db.Relationship from plist", function()
  -- note todo: in association can exists only one leg(end)
  local function parse_relationship(t, s, e)
    local rtype = assert(RELATIONSHIP.CODE[t], 'type')
    local rstart = assert(RELATIONSHIP.CODE[s], 'start')
    local rend = assert(RELATIONSHIP.CODE[e], 'end')
    return rtype, rstart, rend
  end

  it("tooling parse_relationship", function()
    local rtype, rstart, rend = parse_relationship('=', '1..n', '0..n')
    assert.same(RELATIONSHIP.TYPES.identifying, rtype)
    assert.same(RELATIONSHIP.t.many_or_one, rstart)
    assert.same(RELATIONSHIP.t.many, rend)

    rtype, rstart, rend = parse_relationship('-', '0..1', '1..1')
    assert.same(RELATIONSHIP.TYPES.simple, rtype)
    assert.same(RELATIONSHIP.t.one_optional, rstart)
    assert.same(RELATIONSHIP.t.one_mandatory, rend)
  end)


  it("new_from_plist errors in position list", function()
    local f = M.new_from_plist
    local exp = { false, 'expected at least two position got: 0' }
    assert.same(exp, { f({}, '-', '1..n', '1..n') })

    assert.match_error(function()
      f({ { 1, 2 }, { 1, 2 } }, '-', '1..n', '1..n')
    end, 'expected 2.x got: nil')

    assert.match_error(function()
      f({ { x = 1, y = 2 }, { x = 1, y = 2 } }, '-', '1..n', '1..n')
    end, 'expected diff points')
  end)


  it("error: no points", function()
    local f = M.new_from_plist
    local t = RELATIONSHIP.NAME
    local rtype = t[RELATIONSHIP.TYPES.identifying]
    local rstart = t[RELATIONSHIP.t.many_or_one]
    local rend = t[RELATIONSHIP.t.many_or_one]

    local exp = { false, 'expected at least two position got: 0' }
    assert.same(exp, { f({}, rtype, rstart, rend) })
  end)




  local f = M.new_from_plist

  --
  it("horizontal 1..n to 1..n dir: right", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '=', '1-n', '1-n'

    local ok, link = f(pos_list, rtype, rstart, rend) -- workload

    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exp = [[
Relationship (4:2 18:2) (15x1) elms: 1 inv:
 1: Line (4:2 18:2) color: ═
  Text (11:2 10:2) (2x1) ""
  1..n to "" role:""  head: Point 4:2 color: ⪫  mod: Point 5:2 color: ╪
  1..n to "" role:""  head: Point 18:2 color: ⪪  mod: Point 17:2 color: ╪
]]
    assert.same(exp, M.__tostring(link, true)) -- verbose
    local exp_view = {
      '                        ',
      '   ⪫╪═══════════╪⪪      ',
      '                        '
    }
    assert.same(exp_view, show(link, 24, 3))
  end)

  --
  it("vertical ident 1..n 1..n", function()
    local pos_list = mk_pos_list({ 10, 1 }, { 10, 6 })
    local rtype, rstart, rend = '=', '1..n', '1..n'

    local ok, link = f(pos_list, rtype, rstart, rend) -- workload
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exp = [[
Relationship (10:1 10:6) (1x6) elms: 1 inv:
 1: Line (10:1 10:6) color: ║
  Text (10:3 9:3) (2x1) ""
  1..n to "" role:""  head: Point 10:1 color: ⩛  mod: Point 10:2 color: ╫
  1..n to "" role:""  head: Point 10:6 color: ⩚  mod: Point 10:5 color: ╫
]]
    assert.same(exp, M.__tostring(link, true)) -- verbose

    local exp_view = {
      '         ⩛              ',
      '         ╫              ',
      '         ║              ',
      '         ║              ',
      '         ╫              ',
      '         ⩚              '
    }
    assert.same(exp_view, show(link, 24, 6))
  end)

  it("horizontal 1..n to 1..n dir: right +name", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '=', '1-n', '1-n'
    local params = { name = 'Name' }

    local ok, link = f(pos_list, rtype, rstart, rend, params) -- workload

    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exp = 'Relationship Name  1..n ==  1..n'
    assert.same(exp, M.__tostring(link, false))
    local exp_view = {
      '                        ',
      '   ⪫╪═══Name════╪⪪      ',
      '                        '
    }
    assert.same(exp_view, show(link, 24, 3))
  end)

  it("vertical ident 1..n 1..n +name", function()
    local pos_list = mk_pos_list({ 10, 1 }, { 10, 6 })
    local rtype, rstart, rend = '=', '1..n', '1..n'
    local params = { name = 'Name' }

    local ok, link = f(pos_list, rtype, rstart, rend, params) -- workload
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exp = [[
Relationship (10:1 10:6) (1x6) elms: 1 inv:
 1: Line (10:1 10:6) color: ║
  Text (8:3 11:3) (4x1) "Name"
  1..n to "" role:""  head: Point 10:1 color: ⩛  mod: Point 10:2 color: ╫
  1..n to "" role:""  head: Point 10:6 color: ⩚  mod: Point 10:5 color: ╫
]]
    assert.same(exp, M.__tostring(link, true)) -- verbose

    local exp_view = {
      '         ⩛              ',
      '         ╫              ',
      '       Name             ',
      '         ║              ',
      '         ╫              ',
      '         ⩚              '
    }
    assert.same(exp_view, show(link, 24, 6))
  end)


  local mkRel = mkRelationAndDraw


  it("vertical simple 1..n 1..n", function()
    local exp = {
      '           ⩛            ',
      '           ┼            ',
      '           │            ',
      '           ┼            ',
      '           ⩚            ',
      '                        '
    }
    assert.same(exp, mkRel('-', '1..n', '1..n', { 12, 1 }, { 12, 5 }))
  end)

  it("vertical simple 0..1 1..1", function()
    local exp = {
      '           │            ',
      '           │            ',
      '           │            ',
      '           │            ',
      '           ┼            ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..1', '1..1', { 12, 1 }, { 12, 5 }))
  end)

  it("horizontal simple 0..1 1..1", function()
    local exp = {
      '                        ',
      '                        ',
      '       ────────────┼    ',
      '                        ',
      '                        ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..1', '1..1', { 8, 3 }, { 20, 3 }))
  end)

  it("horizontal simple 1..1 0..1", function()
    local exp = {
      '                        ',
      '                        ',
      '       ⪫┼─────────      ',
      '                        ',
      '                        ',
      '                        '
    }
    assert.same(exp, mkRel('-', '1..n', '0..1', { 8, 3 }, { 18, 3 }))
  end)

  it("vert simple 0..n 1..n", function()
    local exp = {
      '           ⩛            ',
      '           │            ',
      '           │            ',
      '           ┼            ',
      '           ⩚            ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..n', '1..n', { 12, 1 }, { 12, 5 }))
  end)

  it("vert simple 0..1 0..1", function()
    local exp = {
      '           │            ',
      '           │            ',
      '           │            ',
      '           │            ',
      '           │            ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..1', '0..1', { 12, 1 }, { 12, 5 }))
  end)

  it("vert simple 0..1 0..1", function()
    local exp = {
      '                        ',
      '                        ',
      '       ─────────────    ',
      '                        ',
      '                        ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..1', '0..1', { 8, 3 }, { 20, 3 }))
  end)

  it("vert simple 0..1 0..1", function()
    local exp = {
      '                        ',
      '                        ',
      '       ────────────┐    ',
      '                   │    ',
      '                   │    ',
      '                        '
    }
    assert.same(exp, mkRel('-', '0..1', '0..1', { 8, 3 }, { 20, 3 }, { 20, 5 }))
  end)
end)

describe("env.draw.notation.db.Relationship tostring", function()
  it("serialize", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '-', '1-n', '0-n'
    local opts = {
      name = 'Kinship',
      a_ref = 'Person',
      a_role = 'Parent',
      b_ref = 'Person',
      b_role = 'Child',
    }
    local ok, link = M.new_from_plist(pos_list, rtype, rstart, rend, opts)
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship

    local exps = [[
Relationship (4:2 18:2) (15x1) elms: 1 inv:
 1: Line (4:2 18:2) color: ─
  Text (7:2 13:2) (7x1) "Kinship"
  1..n to "Person" role:"Parent"  head: Point 4:2 color: ⪫  mod: Point 5:2 color: ┼
  0..n to "Person" role:"Child"  head: Point 18:2 color: ⪪  mod: Point 17:2 color: ─
]]
    assert.same(exps, link:__tostring(true))

    local exp2 = 'Relationship Kinship Person(Parent)1..n -- Person(Child)0..n'
    assert.same(exp2, tostring(link))
  end)
end)

describe("env.draw.notation.db.Relationship serialize", function()
  it("serialize", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '-', '1-n', '0-n'
    local opts = {
      name = 'Kinship',
      a_ref = 'Person',
      a_role = 'Parent',
      b_ref = 'Person',
      b_role = 'Child',
    }
    local ok, link = M.new_from_plist(pos_list, rtype, rstart, rend, opts)
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exps = 'Relationship Kinship Person(Parent)1..n -- Person(Child)0..n'
    assert.same(exps, tostring(link))

    local exp = {
      _class = 'env.draw.notation.db.Relationship',
      name = 'Kinship',
      nx = 7,
      ny = 2,
      rtype = RELATIONSHIP.TYPES.simple,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = RELATIONSHIP.t.many_or_one,
      b_et = RELATIONSHIP.t.many,
      a_ref = 'Person',
      a_role = 'Parent',
      b_ref = 'Person',
      b_role = 'Child',
    }
    assert.same(exp, link:serialize())
  end)


  it("serialize 2", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '-', '1-n', '0-n'
    local opts = {}
    local ok, link = M.new_from_plist(pos_list, rtype, rstart, rend, opts)
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local exp = {
      _class = 'env.draw.notation.db.Relationship',
      name = '',
      nx = 11,
      ny = 2,
      rtype = RELATIONSHIP.TYPES.simple,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = RELATIONSHIP.t.many_or_one,
      b_et = RELATIONSHIP.t.many,
    }
    assert.same(exp, link:serialize())
  end)


  it("deserialize 1", function()
    local serialized = {
      _class = 'env.draw.notation.db.Relationship',
      name = 'Kinship',
      rtype = RELATIONSHIP.TYPES.simple,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = RELATIONSHIP.t.many_or_one,
      b_et = RELATIONSHIP.t.many,
      a_ref = 'Person',
      a_role = 'Parent',
      b_ref = 'Person',
      b_role = 'Child',
    }

    local rel = M.deserialize(serialized, nil) -- workload
    local exp = {
      x1 = 4,
      x2 = 18,
      y1 = 2,
      y2 = 2,
      name = { text = 'Kinship', x1 = 7, x2 = 13, y1 = 2, y2 = 2 },
      rtype = 122,
      inv = { { color = "─", x1 = 4, x2 = 18, y1 = 2, y2 = 2 } },
      a = {
        endtype = 118,
        ref = "Person",
        role = "Parent",
        elms = {
          head = { color = "⪫", x = 4, y = 2 },
          mod = { color = "┼", x = 5, y = 2 }
        },
      },
      b = {
        endtype = 108,
        ref = "Person",
        role = "Child",
        elms = {
          head = { color = "⪪", x = 18, y = 2 },
          mod = { color = "─", x = 17, y = 2 }
        }
      }
    }
    assert.same(exp, rel)
    local exps = [[
Relationship (4:2 18:2) (15x1) elms: 1 inv:
 1: Line (4:2 18:2) color: ─
  Text (7:2 13:2) (7x1) "Kinship"
  1..n to "Person" role:"Parent"  head: Point 4:2 color: ⪫  mod: Point 5:2 color: ┼
  0..n to "Person" role:"Child"  head: Point 18:2 color: ⪪  mod: Point 17:2 color: ─
]]
    assert.same(exps, rel:__tostring(true))
  end)


  it("deserialize 1", function()
    local serialized = {
      _class = 'env.draw.notation.db.Relationship',
      name = '',
      rtype = RELATIONSHIP.TYPES.simple,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = RELATIONSHIP.t.many_or_one,
      b_et = RELATIONSHIP.t.many,
    }

    local rel = M.deserialize(serialized, nil) -- workload
    local exp = {
      x1 = 4,
      x2 = 18,
      y1 = 2,
      y2 = 2,
      rtype = 122,
      name = { text = '', x1 = 11, x2 = 10, y1 = 2, y2 = 2 },
      a = {
        endtype = 118,
        elms = {
          head = { color = "⪫", x = 4, y = 2 },
          mod = { color = "┼", x = 5, y = 2 },
        }
      },
      b = {
        endtype = 108,
        elms = {
          head = { color = "⪪", x = 18, y = 2 },
          mod = { color = "─", x = 17, y = 2 }
        }
      },
      inv = { { color = "─", x1 = 4, x2 = 18, y1 = 2, y2 = 2 } },
    }
    assert.same(exp, rel)
    local exps = [[
Relationship (4:2 18:2) (15x1) elms: 1 inv:
 1: Line (4:2 18:2) color: ─
  Text (11:2 10:2) (2x1) ""
  1..n to "" role:""  head: Point 4:2 color: ⪫  mod: Point 5:2 color: ┼
  0..n to "" role:""  head: Point 18:2 color: ⪪  mod: Point 17:2 color: ─
]]
    assert.same(exps, rel:__tostring(true))
  end)

  it("fmtSerialized", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '-', '1-n', '0-n'
    local opts = {
      name = 'Kinship',
      a_ref = 'Person',
      a_role = 'Parent',
      b_ref = 'Person',
      b_role = 'Child',
    }
    local ok, link = M.new_from_plist(pos_list, rtype, rstart, rend, opts)
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local serialized = link:serialize()
    local exp = {
      _class = 'env.draw.notation.db.Relationship',
      name = 'Kinship',
      nx = 7,
      ny = 2,
      rtype = 122,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = 118,
      a_ref = 'Person',
      a_role = 'Parent',
      b_et = 108,
      b_ref = 'Person',
      b_role = 'Child',
    }
    assert.same(exp, serialized)

    local exps = "{_class='env.draw.notation.db.Relationship'," ..
        "name='Kinship',nx=7,ny=2," ..
        "rtype=122,a_et=118,a_ref='Person',a_role='Parent'," ..
        "b_et=108,b_ref='Person',b_role='Child',  " ..
        "plist = {{x=4,y=2},{x=18,y=2}  }}"
    assert.same(exps, link:fmtSerialized(serialized))
  end)

  it("fmtSerialized 2", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    local rtype, rstart, rend = '-', '1-n', '0-n'
    local opts = {}
    local ok, link = M.new_from_plist(pos_list, rtype, rstart, rend, opts)
    assert.same(true, ok) ---@cast link env.draw.notation.db.Relationship
    local serialized = link:serialize()
    local exp = {
      _class = 'env.draw.notation.db.Relationship',
      name = '',
      nx = 11,
      ny = 2,
      rtype = 122,
      plist = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      a_et = 118,
      b_et = 108
    }
    assert.same(exp, serialized)

    local exps = "{_class='env.draw.notation.db.Relationship'," ..
        "name='',nx=11,ny=2,rtype=122,a_et=118,b_et=108,  " ..
        "plist = {{x=4,y=2},{x=18,y=2}  }}"
    assert.same(exps, link:fmtSerialized(serialized))
  end)

  it("dump_plist", function()
    local pos_list = mk_pos_list({ 4, 2 }, { 18, 2 })
    assert.same('plist = {{x=4,y=2},{x=18,y=2}}', M.dump_plist(pos_list))
  end)
end)

describe("env.draw.notation.db.Attribute change relation leg", function()
  it("set_rel_end_type", function()
    local plist = mk_pos_list({ 4, 2 }, { 18, 2 })
    -- 0..n is many
    local ok, rel = M.new_from_plist(plist, '-', '0..n', '0..n', {})
    assert.same(true, ok) ---@cast rel env.draw.notation.db.Relationship
    assert.same('Relationship   0..n --  0..n', v2s(rel))
    local exp = {
      '                                ',
      '   ⪫─────────────⪪              ',
      '                                '
    }
    assert.same(exp, show(rel, nil, 3))

    M.set_rel_end_type(rel.a, rel.rtype, RELATIONSHIP.t.many_or_one)
    assert.same('Relationship   1..n --  0..n', v2s(rel))

    M.set_rel_end_type(rel.b, rel.rtype, RELATIONSHIP.t.many_or_one)
    assert.same('Relationship   1..n --  1..n', v2s(rel))
    local expv = {
      '                                ',
      '   ⪫┼───────────┼⪪              ',
      '                                '
    }
    assert.same(expv, show(rel, nil, 3))
  end)

  it("set_rel_end_type", function()
    local plist = mk_pos_list({ 10, 1 }, { 10, 5 })
    -- 0..n is many
    local ok, rel = M.new_from_plist(plist, '-', '0..n', '0..n', {})
    assert.same(true, ok) ---@cast rel env.draw.notation.db.Relationship
    assert.same('Relationship   0..n --  0..n', v2s(rel))
    local exp = {
      '         ⩛              ',
      '         │              ',
      '         │              ',
      '         │              ',
      '         ⩚              '
    }
    assert.same(exp, show(rel, 24, 5))

    M.set_rel_end_type(rel.a, rel.rtype, RELATIONSHIP.t.many_or_one)
    assert.same('Relationship   1..n --  0..n', v2s(rel))

    M.set_rel_end_type(rel.b, rel.rtype, RELATIONSHIP.t.many_or_one)
    assert.same('Relationship   1..n --  1..n', v2s(rel))
    local expv = {
      '         ⩛              ',
      '         ┼              ',
      '         │              ',
      '         ┼              ',
      '         ⩚              '
    }
    assert.same(expv, show(rel, 24, 5))
  end)
end)

--

describe("env.draw.notation.db.Relationship drag by relation leg", function()
  local mkRel = mkRelationAndDraw

  it("get_reduce_line_direction R for horiz in up-left", function()
    local cx, cy, direction = 12, 5, 'R' -- on line2
    local v, rel = mkRel('-', '0..1', '1..n', { 14, 1 }, { 14, 5 }, { 12, 5 })
    local exp = {
      '             │          ',
      '             │          ',
      '             │          ',
      '             │          ',
      '           ⪫┼┘          ',
      '                        '
    }
    --           --> reduce line
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (14:1 14:5) color: │', v2s(line1))
    assert.same('Line (12:5 14:5) color: ─', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, 0, 0, 1 }, { up, right, down, left })
    -- 1 from left side, note x1:y1-x2:y2 in Lines always normalized...
    assert.same('L', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction L for horiz on up-right", function()
    local cx, cy, direction = 16, 5, 'L'
    local v, rel = mkRel('-', '0..1', '1..n', { 14, 1 }, { 14, 5 }, { 16, 5 })
    local exp = {
      '             │          ',
      '             │          ',
      '             │          ',
      '             │          ',
      '             └┼⪪        ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (14:1 14:5) color: │', v2s(line1))
    assert.same('Line (14:5 16:5) color: ─', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, -1, 0, 0 }, { up, right, down, left })

    assert.same('R', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction D on vert left-up", function()
    local cx, cy, direction = 16, 2, 'D'
    local v, rel = mkRel('-', '0..1', '1..n', { 8, 4 }, { 16, 4 }, { 16, 2 })
    local exp = {
      '                        ',
      '               ⩛        ',
      '               ┼        ',
      '       ────────┘        ',
      '                        ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (8:4 16:4) color: ─', v2s(line1))
    assert.same('Line (16:2 16:4) color: │', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 1, 0, 0, 0 }, { up, right, down, left })

    assert.same('U', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction U on vet left-down", function()
    local cx, cy, direction = 16, 5, 'U'
    local v, rel = mkRel('-', '0..1', '1..n', { 8, 3 }, { 16, 3 }, { 16, 5 })
    local exp = {
      '                        ',
      '                        ',
      '       ────────┐        ',
      '               ┼        ',
      '               ⩚        ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (8:3 16:3) color: ─', v2s(line1))
    assert.same('Line (16:3 16:5) color: │', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, 0, -1, 0 }, { up, right, down, left })

    assert.same('D', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction R for horiz in down-left", function()
    local cx, cy, direction = 12, 5, 'R' -- on line2
    local v, rel = mkRel('-', '0..1', '1..n', { 14, 5 }, { 14, 1 }, { 12, 1 })
    local exp = {
      '           ⪫┼┐          ',
      '             │          ',
      '             │          ',
      '             │          ',
      '             │          ',
      '                        '
    }
    --           --> reduce line
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (14:1 14:5) color: │', v2s(line1))
    assert.same('Line (12:1 14:1) color: ─', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, 0, 0, 1 }, { up, right, down, left })
    assert.same('L', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction L for horiz on down-right", function()
    local cx, cy, direction = 16, 5, 'L'
    local v, rel = mkRel('-', '0..1', '1..n', { 14, 5 }, { 14, 1 }, { 16, 1 })
    local exp = {
      '             ┌┼⪪        ',
      '             │          ',
      '             │          ',
      '             │          ',
      '             │          ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (14:1 14:5) color: │', v2s(line1))
    assert.same('Line (14:1 16:1) color: ─', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, -1, 0, 0 }, { up, right, down, left })

    assert.same('R', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction D on vert right-up", function()
    local cx, cy, direction = 16, 2, 'D'
    local v, rel = mkRel('-', '0..1', '1..n', { 23, 4 }, { 12, 4 }, { 12, 2 })
    local exp = {
      '                        ',
      '           ⩛            ',
      '           ┼            ',
      '           └─────────── ',
      '                        ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (12:4 23:4) color: ─', v2s(line1))
    assert.same('Line (12:2 12:4) color: │', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 1, 0, 0, 0 }, { up, right, down, left })

    assert.same('U', M.get_reduce_line_direction(line2, up, right, down, left))
  end)

  it("get_reduce_line_direction U on vet left-down", function()
    local cx, cy, direction = 16, 5, 'U'
    local v, rel = mkRel('-', '0..1', '1..n', { 23, 3 }, { 12, 3 }, { 12, 5 })
    local exp = {
      '                        ',
      '                        ',
      '           ┌─────────── ',
      '           ┼            ',
      '           ⩚            ',
      '                        '
    }
    assert.same(exp, v)
    local line1, line2 = rel.inv[1], rel.inv[2]
    assert.same('Line (12:3 23:3) color: ─', v2s(line1))
    assert.same('Line (12:3 12:5) color: │', v2s(line2))

    local up, right, down, left = line2:offsets_on_drag(cx, cy, direction)
    assert.same({ 0, 0, -1, 0 }, { up, right, down, left })

    assert.same('D', M.get_reduce_line_direction(line2, up, right, down, left))
  end)
end)

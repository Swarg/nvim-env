-- 08-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("env.draw.notation.db.Attribute");
local H = require("env.draw.test_helper");

local show = H.show_element

describe("env.draw.notation.db.Attribute", function()
  it("new", function()
    local a = M:new(nil, 'name', 'domain', 'props', true)

    local exp = {
      name = 'name',
      props = 'props',
      atype = 'domain',
      pk = true,
      y = 0,
      x1 = 0,
      xne = 0,
      xt = 0,
      xp = 0,
      x2 = 0,
    }
    assert.same(exp, a)
  end)

  it("new", function()
    local a = M:new(nil, 'name', 'domain', 'props', true)
    a:set_coords(2, 2, 21, 2)

    local exp = {
      name = 'name',
      atype = 'domain',
      props = 'props',
      pk = true,
      y = 2,
      x1 = 2,
      xne = 6,
      xt = 8,
      xp = 16,
      x2 = 21,
    }
    assert.same(exp, a)
  end)

  it("draw", function()
    local a = M:new(nil, 'attr_name', 'domain', 'props', true)
    a:set_coords(4, 1, 30, 2)
    local exp = {
      '   attr_name   domain   props   ',
      '   ▔▔▔▔▔▔▔▔▔                    '
    }
    assert.same(exp, show(a, 32, 2))
  end)

  it("move", function()
    local a = M:new(nil, 'attr_name', 'domain', 'props', true)
    a:set_coords(4, 2, 30, 2)
    local exp = {
      '                                ',
      '   attr_name   domain   props   ',
      '   ▔▔▔▔▔▔▔▔▔                    ',
      '                                '
    }
    assert.same(exp, show(a, 32, 4))

    a:move(3, 1) --

    exp = {
      '                                ',
      '                                ',
      '      attr_name   domain   props',
      '      ▔▔▔▔▔▔▔▔▔                 '
    }
    assert.same(exp, show(a, 32, 4))

    a:move(-5, -2) --

    exp = {
      ' attr_name   domain   props     ',
      ' ▔▔▔▔▔▔▔▔▔                      ',
      '                                ',
      '                                '
    }
    assert.same(exp, show(a, 32, 4))

    a:move(6, 0) --

    exp = {
      '       attr_name   domain   prop',
      '       ▔▔▔▔▔▔▔▔▔                ',
      '                                ',
      '                                '
    }
    assert.same(exp, show(a, 32, 4))
  end)
end)


describe("env.draw.notation.db.Attribute serialization", function()
  it("serialize", function()
    local a = M:new(nil, 'attr_name', 'domain', 'props', true)
    a:set_coords(4, 2, 28, 2)

    local exp_serialized = {
      _class = 'env.draw.notation.db.Attribute',
      y = 2,
      x1 = 4,
      xne = 13,
      xt = 15,
      xp = 23,
      x2 = 28,
      name = 'attr_name',
      atype = 'domain',
      props = 'props',
      pk = true
    }
    assert.same(exp_serialized, a:serialize())
  end)

  it("serialize 2", function()
    local a = M:new(nil, 'a', 'd', 'p', nil)
    a:set_coords(10, 1, 17, 1)

    local exp_serialized = {
      _class = 'env.draw.notation.db.Attribute',
      y = 1,
      x1 = 10,
      xne = 11,
      xt = 13,
      xp = 16,
      x2 = 17,
      name = 'a',
      atype = 'd',
      props = 'p'
    }
    assert.same(exp_serialized, a:serialize())
  end)
end)


describe("env.draw.notation.db.Attribute aligment", function()
  it("calcIdent", function()
    local a = M:new(nil, 'person_id', 'domain', 'props', true)
    a:set_coords(5, 2, 30, 2)

    local exp = {
      '   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~           ',
      '   ~person_id   domain   props~           ',
      '   ~▔▔▔▔▔▔▔▔▔                 ~           ',
      '   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~           '
    }
    assert.same(exp, show(a, 42, 4, { 4, 1, 31, 4 }))
    local exp2 = {
      name = 'person_id',
      atype = 'domain',
      props = 'props',
      pk = true,
      y = 2,
      x1 = 5,
      xne = 14,
      xt = 17,
      xp = 26,
      x2 = 30,
    }
    assert.same(exp2, a)
  end)

  it("calcIdent", function()
    local a = M:new(nil, 'person_id', 'domain', 'props', true)
    a:set_coords(5, 2, 40, 2)

    local exp = {
      '   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         ',
      '   ~person_id        domain        props~         ',
      '   ~▔▔▔▔▔▔▔▔▔                           ~         ',
      '   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~         '
    }
    assert.same(exp, show(a, 50, 4, { 4, 1, 41, 4 }))
  end)

  it("__tostring", function()
    local a = M:new(nil, 'person_id', 'domain', 'props', true)
    a:set_coords(5, 2, 40, 2)
    local exp = 'Attribute (5:2 40:3): person_id domain props PK:true'
    assert.same(exp, a:__tostring())
  end)
end)

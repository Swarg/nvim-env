-- 05-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local M = require("env.draw.notation.db.base");
local H = require("env.draw.test_helper");

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- local show = H.show_element
local mkLine = H.mkLine


describe("env.draw.notation.db.base relationships", function()
  it("validate_relationship_ends", function()
    local f = function(s, e, t)
      local ok, sn, en, tn, err = M.validate_relationship(s, e, t)
      return fmt('%s|%s|%s|%s|%s', v2s(ok), v2s(sn), v2s(en), v2s(tn), v2s(err))
    end
    local r = M.RELATIONSHIP
    local rt = r.TYPES
    assert.same('false|0|0|0|start end type', f(0, 0, 0))
    assert.same('false|nil|nil|nil|start end type', f(nil, nil, nil))
    assert.same('true|108|108|144|nil', f(r.t.many, r.t.many, rt.identifying))
    assert.same('false|nil|0|0|start end type', f(r.many, 0, 0))
    assert.same('false|0|nil|0|start end type', f(0, r.many, 0))
    assert.same('true|101|101|144|nil', f('0..1', '0-1', 'identifying'))
    assert.same('false|101|101|nil|type', f('0..1', '0-1', 'id'))
    assert.same('false|nil|101|nil|start type', f(nil, '0..1'))
    assert.same('false|101|nil|nil|end type', f('0..1', nil, nil))
    assert.same('false|nil|nil|144|start end', f(nil, nil, 'identifying'))
  end)

  -- by direction and line type (single|double) and relationship modifier
  it("get_relationship_notation directions", function()
    local f = function(line, pos, rt, rmod)
      local t = M.get_relationship_notation(line, pos, rt, rmod)
      return { t.head, t.second } --, t.direction }
    end

    local nl = mkLine -- new Line ...

    local r = M.RELATIONSHIP
    local smpl = r.TYPES.simple
    local id = r.TYPES.identifying

    -- horizontal line from 1:1 to 8:1   the p(1,1) marker of line start
    -- note: line keeps its coord in normalized way: x1<x2 and y1 < y2
    local x1y1 = { x = 1, y = 1 }
    -- line, pos{x,y} line_type(single|double), modifier
    assert.same({ '⪫', '─' }, f(nl(1, 1, 8, 1), x1y1, smpl, r.t.many))
    assert.same({ '⪫', '═' }, f(nl(1, 1, 8, 1), x1y1, id, r.t.many))

    assert.same({ '⪫', '┼' }, f(nl(1, 1, 8, 1), x1y1, smpl, r.t.many_or_one))
    assert.same({ '⪫', '╪' }, f(nl(1, 1, 8, 1), x1y1, id, r.t.many_or_one))

    local x8y1 = { x = 8, y = 1 }
    assert.same({ '⪪', '─' }, f(nl(1, 1, 8, 1), x8y1, smpl, r.t.many))
    assert.same({ '⪪', '═' }, f(nl(1, 1, 8, 1), x8y1, id, r.t.many))

    assert.same({ '⪪', '┼' }, f(nl(1, 1, 8, 1), x8y1, smpl, r.t.many_or_one))
    assert.same({ '⪪', '╪' }, f(nl(1, 1, 8, 1), x8y1, id, r.t.many_or_one))

    -- vertical from 1:1 to 1:8
    assert.same({ '⩛', '│' }, f(nl(1, 1, 1, 8), x1y1, smpl, r.t.many))
    assert.same({ '⩛', '║' }, f(nl(1, 1, 1, 8), x1y1, id, r.t.many))

    assert.same({ '⩛', '┼' }, f(nl(1, 1, 1, 8), x1y1, smpl, r.t.many_or_one))
    assert.same({ '⩛', '╫' }, f(nl(1, 1, 1, 8), x1y1, id, r.t.many_or_one))

    local x1y8 = { x = 1, y = 8 }
    assert.same({ '⩚', '│' }, f(nl(1, 1, 1, 8), x1y8, smpl, r.t.many))
    assert.same({ '⩚', '║' }, f(nl(1, 1, 1, 8), x1y8, id, r.t.many))

    assert.same({ '⩚', '┼' }, f(nl(1, 1, 1, 8), x1y8, smpl, r.t.many_or_one))
    assert.same({ '⩚', '╫' }, f(nl(1, 1, 1, 8), x1y8, id, r.t.many_or_one))
  end)

  it("get_relationship_notation inner tbl", function()
    local f = M.get_relationship_notation

    local r = M.RELATIONSHIP
    local id = r.TYPES.identifying

    -- note: line keeps its coord in normalized way: x1<x2 and y1 < y2
    local exp = { head = '⪫', second = '╪', direction = 4, x = 1, y = 1 }
    assert.same(exp, f(mkLine(1, 1, 8, 1), { x = 1, y = 1 }, id, r.t.many_or_one))
  end)

  it("get_relationship_notation inner tbl", function()
    local f = M.get_relationship_notation

    local r = M.RELATIONSHIP
    local id = r.TYPES.identifying

    -- note: line keeps its coord in normalized way: x1<x2 and y1 < y2
    local exp = { head = '⪪', second = '╪', direction = 2, x = 8, y = 1 }
    assert.same(exp, f(mkLine(1, 1, 8, 1), { x = 8, y = 1 }, id, r.t.many_or_one))
  end)

  it("is_known_relationship_end", function()
    local f = M.is_relationship_end
    assert.same(true, f('0..1'))
    assert.same(true, f('1..1'))
    assert.same(true, f('0..n'))
    assert.same(true, f('1..n'))
    --
    assert.same(true, f('0-1'))
    assert.same(true, f('1-1'))
    assert.same(true, f('0-n'))
    assert.same(true, f('1-n'))

    assert.same(false, f('0..2'))

    -- relationship type
    assert.same(false, f('s'))
    assert.same(false, f('s'))
    assert.same(false, f('-'))
    assert.same(false, f('='))
  end)

  it("is_valid_relationship_name", function()
    local f = M.is_valid_relationship_name
    assert.same(false, f('0..1'))
    assert.same(false, f('01'))
    assert.same(true, f('a01'))
    assert.same(false, f('0Person'))
    assert.same(true, f('Person'))
    assert.same(true, f('Per_son'))
    assert.same(false, f('Per-son'))
    assert.same(false, f('Per.son'))
    assert.same(false, f('Per son'))
    assert.same(true, f('P0rson'))
    assert.same(false, f('_P0rson'))
    assert.same(true, f('P_rson'))
  end)

  it("build_attr_name", function()
    local f = M.build_attr_name
    assert.same('PersonId', f('Person'))
    assert.same('PersonId', f('person'))
    assert.same('Id', f(''))
  end)

  it("build_attr_name", function()
    local f = M.build_attr_name
    assert.same('DepartamentName', f('Departament', 'name'))
    assert.same('DepartamentName', f('departament', 'name'))
    assert.same('Departament', f('departament', ''))
    assert.same('DepartamentId', f('departament'))
  end)
end)


describe("env.draw.notation.db.base cli-helpers", function()
  it("normalize_words", function()
    local f = M.CLI.normalize_words
    assert.same({ 'a', 'b', 'c' }, f({ 'a', 'b', 'c' }))
    assert.same({ 'a', 'b', ';', 'c' }, f({ 'a', 'b;', 'c' }))
    assert.same({ 'a', 'b', ';', 'c' }, f({ 'a', 'b', ';', 'c' }))
  end)


  it("build_attr_desc", function()
    local f = M.CLI.build_attr_desc
    local exp = { true, { name = 'name', atype = 'id', props = 'M', pk = true } }
    assert.same(exp, { f({ 'name', 'id', 'M', "PK" }, 1, 4) })

    -- id+
    local exp2 = { name = 'PersonId', atype = 'id', props = 'M', pk = true }
    assert.same({ true, exp2 }, { f({ 'id+' }, 1, 1, 'Person') })

    -- SmthId
    local exp3 = { name = 'SmthId', atype = 'id', props = 'M', pk = false }
    assert.same({ true, exp3 }, { f({ 'SmthId' }, 1, 1, 'Person') })
  end)


  it("parse_attributes", function()
    local f = M.CLI.parse_attributes
    local exp

    exp = {
      [1] = { name = 'name', atype = 'id', props = 'M', pk = true }
    }
    assert.same({ true, exp }, { f('Person', { 'name', 'id', 'M', "PK" }) })

    assert.same({ true, { { name = 'name' } } }, { f('Person', { 'name' }) })

    exp = { { atype = 'id', name = 'name' } }
    assert.same({ true, exp }, { f('Person', { 'name', 'id' }) })

    exp = { true, { { atype = 'id', name = 'name', props = 'M' } } }
    assert.same(exp, { f('Person', { 'name', 'id', "M" }) })

    exp = 'expected PK flag, got:"BAD" use quotes to define attribute props. '

    assert.same({ false, exp }, { f('Person', { 'name', 'id', "M", "BAD" }) })
  end)

  it("parse_attributes id+", function()
    local f = M.CLI.parse_attributes
    local exp
    local bad_input = { 'id+', 'Name', 'pname', 'M' }
    exp = 'expected PK flag, got:"M" use quotes to define attribute props. '
    assert.same({ false, exp }, { f('Person', bad_input) })

    local input = { 'id+', ';', 'Name', 'pname', 'M' }
    local exp2 = {
      { name = 'PersonId', atype = 'id',    props = 'M', pk = true },
      { name = 'Name',     atype = 'pname', props = 'M' }
    }
    assert.same({ true, exp2 }, { f('Person', input) })
  end)

  it("parse_attributes id+ & SmthId", function()
    local f = M.CLI.parse_attributes
    local input = { 'id+', ';', 'SmthId' }
    local exp = {
      { name = 'PersonId', atype = 'id', props = 'M', pk = true },
      { name = 'SmthId',   atype = 'id', props = 'M', pk = false }
    }
    assert.same({ true, exp }, { f('Person', input) })
  end)
end)

describe("env.draw.notation.db.base cli-helpers", function()
  local Entity = require("env.draw.notation.db.Entity");
  local function newEntity(name, etype)
    local x1, y1, x2, y2 = 0, 0, nil, nil -- use default width and height
    return Entity:new(nil, x1, y1, x2, y2, name, etype)
  end

  local relation_time_sheet = {
    name = 'TimeSheet',
    attrs = {
      { name = 'TimeSheetId',   is_key = true },
      { name = 'WorkingHours',  is_key = false },
      { name = 'OvertimeHours', is_key = false }
    }
  }

  it("entity_to_relation", function()
    local f = M.entity_to_relation
    local e = newEntity('TimeSheet')
    M.add_simplified_attrs(e, { 'id+', 'WorkingHours', 'OvertimeHours' })

    local exp = relation_time_sheet
    assert.same(exp, f(e))
  end)

  it("relation_tostring", function()
    local f = M.relation_tostring
    local exp = 'TimeSheet (TimeSheetId (Key), WorkingHours, OvertimeHours)'
    assert.same(exp, f(relation_time_sheet))
  end)

  it("align_str_to", function()
    local f = M.align_str_to
    assert.same('abc       ', f('abc', 10, '- '))
    assert.same('       abc', f('abc', 10, ' -'))
    assert.same('    abc   ', f('abc', 10, ' - '))
    assert.same('a-ver', f('a-very-long-line', 5, ' - '))
  end)

  it("mk_empty_str_table_for_relation", function()
    local f = M.mk_empty_str_table_for_relation
    local exp = [[
TimeSheet
TimeSheetId | WorkingHours | OvertimeHours |
------------+--------------+---------------+
            |              |               |
            |              |               |
            |              |               |
            |              |               |
]]
    assert.same(exp, f(relation_time_sheet))
  end)

  it("mk_empty_str_table_for_relation", function()
    local f = M.mk_empty_str_table_for_relation
    local opts = {
      ttype = 'text',
      rows = 2,
      columns_width = { 16, 16, 16 }
    }
    local exp = [[
TimeSheet
   TimeSheetId   |   WorkingHours   |   OvertimeHours  |
-----------------+------------------+------------------+
                 |                  |                  |
                 |                  |                  |
]]
    assert.same(exp, f(relation_time_sheet, opts))
  end)
end)

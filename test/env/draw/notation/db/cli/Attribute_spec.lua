-- 08-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("env.draw.notation.db.Attribute");
local H = require("env.draw.test_helper");

local mk_cli_editor = H.mk_editor
local do_edit_transaction = H.do_edit_transaction

describe("env.draw.notation.db.Attribute cli", function()
  local exp_ordered_keys = {
    { y = 'number' },
    { x1 = 'number' },
    { xne = 'number' },
    { xt = 'number' },
    { xp = 'number' },
    { x2 = 'number' },
    { name = 'string' },
    { atype = 'string' },
    { props = 'string' },
    { pk = 'boolean' }
  }

  ---@return env.draw.notation.db.Attribute
  local function mk_attr(name, domain, props, pk, x, y)
    name = name or 'attr_name'
    domain = domain or 'domain'
    props = props or 'props'
    pk = pk or true
    x = x or 4
    y = y or 2
    local attr = M:new(nil, name, domain, props, pk)
    attr:set_coords(x, y, x + 24, y)
    return attr
  end

  it("orderedkeys", function()
    local a = mk_attr()
    assert.same(exp_ordered_keys, a:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local a = mk_attr()
    local e = mk_cli_editor(a, {})
    local exp_orderedkeys = {
      { t = 'number',  k = 'y' },
      { t = 'number',  k = 'x1' },
      { t = 'number',  k = 'xne' },
      { t = 'number',  k = 'xt' },
      { t = 'number',  k = 'xp' },
      { t = 'number',  k = 'x2' },
      { t = 'string',  k = 'name' },
      { t = 'string',  k = 'atype' },
      { t = 'string',  k = 'props' },
      { t = 'boolean', k = 'pk' }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local e = mk_cli_editor(mk_attr())
    local exp_snapshot = {
      y = { o = 2, s = '2' },
      x1 = { o = 4, s = '4' },
      xne = { o = 13, s = '13' },
      xt = { o = 15, s = '15' },
      xp = { o = 23, s = '23' },
      x2 = { o = 28, s = '28' },
      name = { o = 'attr_name', s = 'attr_name' },
      atype = { o = 'domain', s = 'domain' },
      props = { o = 'props', s = 'props' },
      pk = { o = true, s = '+' },
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("build_prompt_oneliner", function()
    local attr = M:new(nil, 'attr_name', 'domain', '', true)
    attr:set_coords(4, 2, 23, 2)
    local editor = mk_cli_editor(attr)
    local prompt, def_line = editor:build_prompt_oneliner()
    local exp = {
      'y x1 xne xt xp x2 name atype props pk',
      '2 4 13 15 23 23 attr_name domain "" +'
    }
    assert.same(exp, { prompt, def_line })
  end)


  it("edit_transaction unchanged", function()
    local logs = {}

    local attr = M:new(nil, 'a', 'd', 'p', true)
    attr:set_coords(10, 1, 17, 1)

    local input = '1 10 11 13 16 17 a d p +'
    local ok, ret = do_edit_transaction(attr, { logs = logs }, input)

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'y x1 xne xt xp x2 name atype props pk',
      '1 10 11 13 16 17 a d p +'
    }
    assert.same(exp_log_entry, logs[1])
  end)


  it("edit_transaction changed", function()
    local logs = {}

    local attr = M:new(nil, 'a', 'd', 'p')
    attr:set_coords(10, 1, 17, 1)

    local input = '1 10 11 13 16 17 attr_name domain props +'
    local ok, ret = do_edit_transaction(attr, { logs = logs }, input)

    local exp = {
      true,
      {
        y = { old = 1, new = 1 },
        x1 = { old = 10, new = 10 },
        xne = { old = 11, new = 11 },
        xt = { old = 13, new = 13 },
        xp = { old = 16, new = 16 },
        x2 = { old = 17, new = 17 },
        name = { old = 'a', new = 'attr_name' },
        atype = { old = 'd', new = 'domain' },
        props = { old = 'p', new = 'props' },
        pk = { old_nil = true, new = true },
      }
    }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'y x1 xne xt xp x2 name atype props pk',
      '1 10 11 13 16 17 a d p nil'
    }
    assert.same(exp_log_entry, logs[1])
  end)
end)

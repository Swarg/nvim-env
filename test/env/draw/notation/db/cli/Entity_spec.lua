-- 08-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local Entity = require("env.draw.notation.db.Entity")
-- local Attribute = require("env.draw.notation.db.Attribute")
local IEditable = require 'env.draw.IEditable'
local H = require("env.draw.test_helper")

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- local show = H.show_element
local mk_cli_editor = H.mk_editor
local do_edit_transaction = H.do_edit_transaction


describe("env.draw.notation.db.Attribute cli", function()
  local exp_ordered_keys = {
    { x1 = 'number' },
    { y1 = 'number' },
    { x2 = 'number' },
    { y2 = 'number' },
    { name = 'string' },
    { etype = 'string' },
    { attributes = 'table' },
    { w = 'number',        _def = IEditable.get_width },
    { h = 'number',        _def = IEditable.get_height }
  }

  local function newEntity(name, etype)
    name = name or 'Person'
    etype = etype or 'strong'             -- regular not a weak entity
    local x1, y1, x2, y2 = 0, 0, nil, nil -- use default width and height
    return Entity:new(nil, x1, y1, x2, y2, name, etype)
  end

  it("tooling newEntity", function()
    local exp = 'Entity(strong) Name (0:0 24:8) (25x9) attrs: 1'
    assert.same(exp, v2s(newEntity('Name', 'Type')))
  end)


  it("orderedkeys", function()
    local e = newEntity()
    assert.same(exp_ordered_keys, e:orderedkeys())
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = newEntity()
    local e = mk_cli_editor(elm, {})
    local exp_orderedkeys = {
      { k = 'x1',         t = 'number' },
      { k = 'y1',         t = 'number' },
      { k = 'x2',         t = 'number' },
      { k = 'y2',         t = 'number' },
      { k = 'name',       t = 'string' },
      { k = 'etype',      t = 'string' },
      { k = 'attributes', t = 'table' },
      { k = 'w',          t = 'number', d = IEditable.get_width },
      { k = 'h',          t = 'number', d = IEditable.get_height }
    }
    assert.same(exp_orderedkeys, e.orderedkeys)
  end)

  local attr_person_id = {
    _class = "env.draw.notation.db.Attribute",
    atype = "id",
    name = "PersonId",
    props = "M",
    pk = true,
    y = 3,
    x1 = 2,
    xne = 10,
    xt = 12,
    xp = 16,
    x2 = 22,
  }

  it("snapshot of current state of the object based on orderedkeys", function()
    local ent = newEntity('Person')
    local e = mk_cli_editor(ent)

    local exp_snapshot = {
      x1 = { s = '0', o = 0 },
      y1 = { s = '0', o = 0 },
      x2 = { s = '24', o = 24 },
      y2 = { s = '8', o = 8 },
      name = { s = 'Person', o = 'Person' },
      etype = { s = 'strong', o = 'strong' },
      attributes = { s = '::TABLE::', o = { attr_person_id } },
      w = { s = '25', o = 25 },
      h = { s = '9', o = 9 },
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("build_prompt_oneliner", function()
    local editor = mk_cli_editor(newEntity('Person'))
    local prompt, def_line = editor:build_prompt_oneliner()
    local exp = {
      'x1 y1 x2 y2 name etype attributes w h',
      '0 0 24 8 Person strong ::TABLE:: 25 9'
    }
    assert.same(exp, { prompt, def_line })
  end)


  it("edit_transaction unchanged", function()
    local logs = {}

    local elm = newEntity('Person')

    -- x1 y1 x2 y2 name etype attributes w h
    local input = '0 0 24 8 Person strong ::TABLE:: 25 9'
    local ok, ret = do_edit_transaction(elm, { logs = logs }, input)

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'x1 y1 x2 y2 name etype attributes w h',
      '0 0 24 8 Person strong ::TABLE:: 25 9'
    }
    assert.same(exp_log_entry, logs[1])
  end)


  it("edit_transaction changed", function()
    local logs = {}

    local elm = newEntity('Person')

    -- x1 y1 x2 y2 name etype attributes w h
    local input = '0 0 32 8 Person strong ::TABLE:: 25 9'
    local ok, ret = do_edit_transaction(elm, { logs = logs }, input)

    local exp = {
      true,
      {
        y1 = { new = 0, old = 0 },
        x1 = { new = 0, old = 0 },
        x2 = { new = 32, old = 24 },
        y2 = { new = 8, old = 8 },
        name = { new = 'Person', old = 'Person' },
        etype = { new = 'strong', old = 'strong' },
        attributes = { new = { attr_person_id }, old = { attr_person_id } },
        h = { new = 9, old = 9 },
        w = { new = 25, old = 25 },
      }
    }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'x1 y1 x2 y2 name etype attributes w h',
      '0 0 24 8 Person strong ::TABLE:: 25 9'
    }
    assert.same(exp_log_entry, logs[1])
  end)
end)

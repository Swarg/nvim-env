-- 08-05-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
_G.TEST = true
local udb = require 'env.draw.notation.db.base'
local Relationship = require 'env.draw.notation.db.Relationship'
-- local IEditable = require 'env.draw.IEditable'
local H = require("env.draw.test_helper")

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

-- local show = H.show_element
local mk_cli_editor = H.mk_editor
local do_edit_transaction = H.do_edit_transaction
local get_edit_patch = H.get_edit_patch
-- local show = H.show_element
local mk_pos_list = H.mk_pos_list

local edit_update_ex_endpoint = Relationship.edit_update_ex_endpoint

local RELATIONSHIP = udb.RELATIONSHIP

-- helper
local function newRel(name, rtype, a_et, b_et, plist)
  rtype = rtype or '-'
  plist = plist or mk_pos_list({ 4, 2 }, { 18, 2 })
  a_et = a_et or '0..n'
  b_et = b_et or '0..n'
  local params = {
    name = name or 'Person'
  }
  local _, r = Relationship.new_from_plist(plist, rtype, a_et, b_et, params)
  assert(type(r) == 'table', 'fail')
  return r
end

local def_endpoint_a = {
  endtype = RELATIONSHIP.t.many, -- 108,
  ref = nil,
  role = nil,
  elms = {
    head = { color = '⪫', x = 4, y = 2 },
    mod = { color = '─', x = 5, y = 2 }
  }
}

local def_endpoint_b = {
  endtype = RELATIONSHIP.t.many,
  elms = {
    head = { color = '⪪', x = 18, y = 2 },
    mod = { color = '─', x = 17, y = 2 }
  }
}
local def_relname = { text = 'Person', x1 = 8, y1 = 2, x2 = 13, y2 = 2 }

local def_relationship = {
  x1 = 4,
  y1 = 2,
  x2 = 18,
  y2 = 2,
  name = def_relname,
  rtype = RELATIONSHIP.TYPES.simple,
  a = def_endpoint_a,
  b = def_endpoint_b,
  -- lines
  inv = { { color = '─', x1 = 4, y1 = 2, x2 = 18, y2 = 2 } }
}


describe("env.draw.notation.db.Attribute cli", function()
  it("tooling newEntity", function()
    local exp = 'Relationship Person  0..n --  0..n'
    assert.same(exp, v2s(newRel('Person', '-', '0..n', '0..n')))
  end)

  it("tooling newEntity 2", function()
    local exp = 'Relationship Person  0..n --  0..n'
    assert.same(exp, v2s(newRel()))
  end)

  local exp_orderedkeys = {
    { name = 'string' },
    { nx = 'number' },
    { ny = 'number' },
    { rtype = 'number' },
    { a_et = 'number' },
    { a_ref = 'string' },
    { a_role = 'string' },
    { b_et = 'number' },
    { b_ref = 'string' },
    { b_role = 'string' },
    { plist = 'table' }
  }
  local exp_orderedkeys_cli = {
    { name = 'string' },
    { nx = 'number' },
    { ny = 'number' },
    { rtypen = 'string', _def = Relationship.get_rtype_name },
    { a_etn = 'string',  _def = Relationship.get_endtype_name_a },
    { a_ref = 'string' },
    { a_role = 'string' },
    { b_etn = 'string',  _def = Relationship.get_endtype_name_b },
    { b_ref = 'string', },
    { b_role = 'string' },
    { plist = 'table' }
  }
  local def_plist_2p = { { x = 4, y = 2 }, { x = 18, y = 2 } }

  it("orderedkeys", function()
    local e = newRel()
    assert.same(exp_orderedkeys, e:orderedkeys())
  end)

  it("orderedkeys", function()
    local e = newRel()
    assert.same(exp_orderedkeys_cli, e:orderedkeys({ cli = true }))
  end)

  it("orderedkeys objeined by clieos.Editor via API", function()
    local elm = newRel()
    local e = mk_cli_editor(elm, {})
    local exp_orderedkeyst = {
      { k = 'name',   t = 'string' },
      { k = 'nx',     t = 'number' },
      { k = 'ny',     t = 'number' },
      { k = 'rtype',  t = 'number' },
      { k = 'a_et',   t = 'number' },
      { k = 'a_ref',  t = 'string' },
      { k = 'a_role', t = 'string' },
      { k = 'b_et',   t = 'number' },
      { k = 'b_ref',  t = 'string' },
      { k = 'b_role', t = 'string' },
      { k = 'plist',  t = 'table', },
    }
    assert.same(exp_orderedkeyst, e.orderedkeys)
  end)


  it("snapshot of current state of the object based on orderedkeys", function()
    local ent = newRel('Person')
    local e = mk_cli_editor(ent)

    local exp_snapshot = {
      name = { s = 'Person', o = 'Person' },
      nx = { s = '8', o = 8 },
      ny = { s = '2', o = 2 },
      rtype = { s = '122', o = 122 },
      a_et = { s = '108', o = 108 },
      b_et = { s = '108', o = 108 },
      plist = { s = '::TABLE::', o = def_plist_2p },
      a_role = { s = 'nil', o = nil },
      b_role = { s = 'nil', o = nil },
      a_ref = { s = 'nil', o = nil },
      b_ref = { s = 'nil', o = nil }
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("snapshot of current state of the object based on orderedkeys", function()
    local ent = newRel('Person')
    local e = mk_cli_editor(ent, { data = { cli = true } })

    local exp_snapshot = {
      name = { s = 'Person', o = 'Person' },
      nx = { s = '8', o = 8 },
      ny = { s = '2', o = 2 },
      rtypen = { s = 'simple', o = 'simple' },
      a_etn = { s = '0..n', o = '0..n' },
      b_etn = { s = '0..n', o = '0..n' },
      plist = { s = '::TABLE::', o = def_plist_2p },
      a_role = { s = 'nil', o = nil },
      b_role = { s = 'nil', o = nil },
      a_ref = { s = 'nil', o = nil },
      b_ref = { s = 'nil', o = nil }
    }
    assert.same(exp_snapshot, e.snapshot)
  end)

  it("build_prompt_oneliner", function()
    local editor = mk_cli_editor(newRel('Person'))
    local prompt, def_line = editor:build_prompt_oneliner()
    local exp = {
      'name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist',
      'Person 8 2 122 108 nil nil 108 nil nil ::TABLE::'
    }
    assert.same(exp, { prompt, def_line })
  end)

  it("build_prompt_oneliner cli", function()
    local editor = mk_cli_editor(newRel('Person'), { data = { cli = true } })
    local prompt, def_line = editor:build_prompt_oneliner()
    local exp = {
      'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist',
      'Person 8 2 simple 0..n nil nil 0..n nil nil ::TABLE::'
    }
    assert.same(exp, { prompt, def_line })
  end)


  it("edit_transaction unchanged", function()
    local logs = {}
    local opts = { logs = logs, data = { cli = true } }

    local elm = newRel('Person')

    --  name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist
    local input = 'Person 8 2 simple 0..n nil nil 0..n nil nil ::TABLE::'
    local ok, ret = do_edit_transaction(elm, opts, input)

    local exp = { false, 'unchanged' }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist',
      'Person 8 2 simple 0..n nil nil 0..n nil nil ::TABLE::'
    }
    assert.same(exp_log_entry, logs[1])
  end)


  it("edit_transaction changed", function()
    local logs = {}

    local elm = newRel('Person')
    local opts = { logs = logs, data = { cli = true } }

    -- name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist
    local input = 'Person 8 2 simple 0..n nil nil 1..n nil nil ::TABLE::'
    local ok, ret = do_edit_transaction(elm, opts, input)

    local exp = {
      true,
      {
        name = { new = 'Person', old = 'Person' },
        nx = { new = 8, old = 8 },
        ny = { new = 2, old = 2 },
        a_etn = { new = '0..n', old = '0..n' },
        b_etn = { new = '1..n', old = '0..n' },
        plist = { new = def_plist_2p, old = def_plist_2p },
        rtypen = { new = 'simple', old = 'simple' },
        a_role = { new = nil, old_nil = true },
        b_role = { new = nil, old_nil = true },
        b_ref = { new = nil, old_nil = true },
        a_ref = { new = nil, old_nil = true },
      }
    }
    assert.same(exp, { ok, ret })

    local exp_log_entry = {
      'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist',
      'Person 8 2 simple 0..n nil nil 0..n nil nil ::TABLE::'
    }
    assert.same(exp_log_entry, logs[1])
  end)
end)

-- parts
describe("env.draw.notation.db.Attribute edit_update_ex_endpoint", function()
  it("edit_update_ex_endpoint a_et", function()
    local r = newRel('Person')
    local opts = { data = { cli = false } }

    -- [reg] "name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist"
    local input = "Person 8 2 122 111 nil nil 108 nil nil ::TABLE::"
    --                             ^^^ updated
    local patch, t = get_edit_patch(r, opts, input)
    -- print("[DEBUG] t:", require"inspect"(r.toArray(t.editor)))
    assert.is_true(t.ok) ---@cast patch table

    local epa = r:get_endpoint_a()

    local exp_before = def_endpoint_a
    assert.same(exp_before, epa)

    edit_update_ex_endpoint(patch, r.rtype, epa, 'a', opts.data.cli) -- workload

    local exp_after = {
      endtype = RELATIONSHIP.t.one_mandatory, -- 111,
      elms = {
        head = { color = '┼', x = 4, y = 2 },
        mod = { color = '─', x = 5, y = 2 }
      }
    }
    assert.same(exp_after, epa)
  end)

  it("edit_update_ex_endpoint b_et", function()
    local r = newRel('Person')
    local opts = { data = { cli = false } }

    -- [reg] "name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist"
    local input = "Person 8 2 122 108 nil nil 111 nil nil ::TABLE::"
    --                                         ^^^ updated
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local epb = r:get_endpoint_b()

    local exp_before = def_endpoint_b
    assert.same(exp_before, epb)

    edit_update_ex_endpoint(patch, r.rtype, epb, 'b', opts.data.cli) -- workload

    local exp_after = {
      endtype = RELATIONSHIP.t.one_mandatory,
      elms = {
        head = { color = '┼', x = 18, y = 2 },
        mod = { color = '─', x = 17, y = 2 }
      }
    }
    assert.same(exp_after, epb)
  end)

  -- cli mode

  --
  it("edit_update_ex_endpoint a_et cli", function()
    local r = newRel('Person')
    local opts = { data = { cli = true } }

    -- [cli] 'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist'
    local input = 'Person 8 2 simple 1..n nil nil 0..n nil nil ::TABLE::'
    --                                ^^^^ updated
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local epa = r:get_endpoint_a()

    local exp_before = def_endpoint_a
    assert.same(exp_before, epa)

    edit_update_ex_endpoint(patch, r.rtype, epa, 'a', opts.data.cli) -- workload

    local exp_after = {
      endtype = RELATIONSHIP.t.many_or_one, -- 111,
      elms = {
        head = { color = '⪫', x = 4, y = 2 },
        mod = { color = '┼', x = 5, y = 2 }
      }
    }
    assert.same(exp_after, epa)
  end)

  it("edit_update_ex_endpoint b_et cli", function()
    local r = newRel('Person')
    local opts = { data = { cli = true } }

    -- [cli] 'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist'
    local input = 'Person 8 2 simple 0..n nil nil 1..n nil nil ::TABLE::'
    --                                             ^^^^ updated
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local epb = r:get_endpoint_b()

    local exp_before = def_endpoint_b
    assert.same(exp_before, epb)

    edit_update_ex_endpoint(patch, r.rtype, epb, 'b', opts.data.cli) -- workload

    local exp_after = {
      endtype = RELATIONSHIP.t.many_or_one,
      elms = {
        head = { color = '⪪', x = 18, y = 2 },
        mod = { color = '┼', x = 17, y = 2 }
      }
    }
    assert.same(exp_after, epb)
  end)
end)

-- edit

describe("env.draw.notation.db.Attribute edit", function()
  it("edit update rtype cli", function()
    local r = newRel('Person')
    local opts = { data = { cli = false } }

    -- [reg] "name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist"
    local input = "Person 8 2 144 108 nil nil 108 nil nil ::TABLE::"
    --                          ^ updated
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    assert.same(def_relationship, r:toArray())
    assert.same('─', r.inv[1].color)
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)

    r:edit(patch) -- workload

    assert.same('═', r.inv[1].color)
    assert.same('═', r.a.elms.mod.color)
    assert.same('═', r.b.elms.mod.color)

    local exp_after = {
      x1 = 4,
      y1 = 2,
      x2 = 18,
      y2 = 2,
      name = def_relname, -- 'Person',
      rtype = RELATIONSHIP.TYPES.identifying,
      inv = { { color = '═', x1 = 4, y1 = 2, x2 = 18, y2 = 2 } },
      a = {
        endtype = RELATIONSHIP.t.many,
        elms = {
          head = { color = '⪫', x = 4, y = 2 },
          mod = { color = '═', x = 5, y = 2 }
        }
      },
      b = {
        endtype = RELATIONSHIP.t.many,
        elms = {
          head = { color = '⪪', x = 18, y = 2 },
          mod = { color = '═', x = 17, y = 2 }
        }
      }
    }
    assert.same(exp_after, r:toArray())
  end)

  it("edit update rtype cli", function()
    local r = newRel('Person')
    local opts = { data = { cli = true } }

    -- [cli] 'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist'
    local input = 'Person 8 2 identifying 0..n nil nil 0..n nil nil ::TABLE::'
    --                         ^ updated
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local exp_before = def_relationship
    assert.same(exp_before, r:toArray())

    assert.same('─', r.inv[1].color)
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)

    r:edit(patch) -- workload

    assert.same('═', r.inv[1].color)
    assert.same('═', r.a.elms.mod.color)
    assert.same('═', r.b.elms.mod.color)
  end)

  it("edit update rtype cli", function()
    local r = newRel('Person')
    local opts = { data = { cli = true } }

    -- [cli] 'name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist'
    local input = 'Person 8 2 identifying 0..n nil nil 1..n nil nil ::TABLE::'
    --                         ^^^^^^^^^^^    updated   ^^^^
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local exp_before = def_relationship
    assert.same(exp_before, r:toArray())

    assert.same('─', r.inv[1].color)
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)

    r:edit(patch) -- workload

    assert.same('═', r.inv[1].color)
    assert.same('═', r.a.elms.mod.color)
    assert.same('╪', r.b.elms.mod.color)
  end)

  it("edit update plist", function()
    local r = newRel('Person')
    local opts = { data = { cli = false } }

    -- [cli] name nx ny rtypen a_etn a_ref a_role b_etn b_ref b_role plist
    local input = 'Person 8 2 simple 0..n nil nil 0..n nil nil ::TABLE::'
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    local exp = {
      old = { { x = 4, y = 2 }, { x = 18, y = 2 } },
      new = { { x = 4, y = 2 }, { x = 18, y = 2 } }
    }
    assert.same(exp, patch.plist)
    patch.plist.new = { { x = 8, y = 3 }, { x = 16, y = 3 } }

    local exp_before = def_relationship
    assert.same(exp_before, r:toArray())

    assert.same({ color = '─', x1 = 4, y1 = 2, x2 = 18, y2 = 2 }, r.inv[1])
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)

    r:edit(patch) -- workload

    assert.same({ color = '─', x1 = 8, y1 = 3, x2 = 16, y2 = 3 }, r.inv[1])
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)
  end)

  -- to edit coords via buffer edit_element_as_code
  it("edit update plist + rtype + endtype", function()
    local r = newRel('Person')
    local opts = { data = { cli = false } }

    -- [reg] name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist
    local input = "Person 8 2 144 111 nil nil 118 nil nil ::TABLE::"
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    -- emulate update plist coords
    patch.plist.new = { { x = 8, y = 3 }, { x = 16, y = 3 } }

    local exp_before = def_relationship
    assert.same(exp_before, r:toArray())

    assert.same({ color = '─', x1 = 4, y1 = 2, x2 = 18, y2 = 2 }, r.inv[1])
    assert.same('─', r.a.elms.mod.color)
    assert.same('─', r.b.elms.mod.color)

    r:edit(patch) -- workload

    assert.same({ color = '═', x1 = 8, y1 = 3, x2 = 16, y2 = 3 }, r.inv[1])
    assert.same('═', r.a.elms.mod.color)
    assert.same('╪', r.b.elms.mod.color)
  end)

  -- TODO add support to clieos to edit tables like: {{x=1,y=2}, {x=3, y=4}}


  it("edit name + coords", function()
    local r = newRel('')
    local opts = { data = { cli = false } }

    -- [reg] name nx ny rtype a_et a_ref a_role b_et b_ref b_role plist
    local input = "Person 4 1 144 111 nil nil 118 nil nil ::TABLE::"
    local patch, t = get_edit_patch(r, opts, input)
    assert.is_true(t.ok) ---@cast patch table

    -- emulate update plist coords
    patch.plist.new = { { x = 8, y = 3 }, { x = 16, y = 3 } }

    r:edit(patch) -- workload
    local exp = { text = 'Person', x1 = 4, y1 = 1, x2 = 9, y2 = 1 }
    assert.same(exp, r.name)
  end)
end)

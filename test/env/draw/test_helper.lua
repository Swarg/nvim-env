-- 04-04-2024 @author Swarg
local Canvas = require("env.draw.Canvas");
local Line = require("env.draw.Line");
local ItemPH = require("env.draw.ui.ItemPlaceHolder");
local IEditable = require("env.draw.IEditable");

local M = {}
--

local v2s, fmt = tostring, string.format
--------------------------------------------------------------------------------
local original_print = _G.print
local print_output = {}

function M.wrap_print()
  ---@diagnostic disable-next-line: duplicate-set-field
  _G.print = function(...)
    local s = ''
    for i = 1, select('#', ...) do
      s = s .. ' ' .. tostring(select(i, ...))
    end
    print_output[#print_output + 1] = s
  end
end

function M.restore_print()
  _G.print = original_print
  print_output = {}
end

---@param n number? to return one specified line (use -1 to get latest)
function M.output(n)
  if n then
    if n < 0 then
      n = #print_output + n + 1
    end
    return print_output[n]
  end

  return print_output
end

--------------------------------------------------------------------------------

-- the element intersects with the specified area
---@param canvas env.draw.Canvas
function M.check_intersects(canvas, el, x1, y1, x2, y2, color)
  if x1 and y1 and x2 and y2 then
    canvas:addHook(Canvas.HOOK_PRE_DRAW, function(c)
      c:draw_rect(nil, x1, y1, x2, y2, color)
    end)
  end
  ---@cast el env.draw.IDrawable
  local b = el:isIntersects(x1, y1, x2, y2)
  local ret = { intersects = b, canvas:draw():toLines() }
  canvas:clearHooks('*')
  return ret
end

function M.check_overlap(canvas, el, x1, y1, x2, y2, color)
  if x1 and y1 and x2 and y2 then
    canvas:addHook(Canvas.HOOK_PRE_DRAW, function(c)
      c:draw_rect(nil, x1, y1, x2, y2, color)
    end)
  end
  ---@cast el env.draw.IDrawable
  local b = el:isOverlap(x1, y1, x2, y2)
  local ret = { intersects = b, canvas:draw():toLines() }
  canvas:clearHooks('*')
  return ret
end

-- readable and short
---@return table{string}
function M.patch_to_readable(patches)
  if type(patches) == 'table' then
    local t = {}

    for n, patch in ipairs(patches) do
      local s = ''
      if type(patch) == 'table' then
        s = fmt('idx:(%s -> %s) ', v2s(patch.old_idx), v2s(patch.new_idx))
        if patch.delete then
          s = s .. 'DELETE'
        end
        if patch.elm_modt then
          local modt = patch.elm_modt
          s = s .. fmt('%s:%s', v2s(modt.x1 or modt.x), v2s(modt.y1 or modt.y))
          if modt.x2 then
            s = s .. fmt(' %s:%s', v2s(modt.x2), v2s(modt.y2))
          end
          if modt.color then -- Line and Point
            s = s .. ' color:' .. v2s(modt.color)
          end
        end
      else
        s = 'expected table got: "' .. type(patch) .. '" at n:' .. v2s(n)
      end

      t[#t + 1] = s
    end

    return t
  end
  return 'expected table got: ' .. type(patches)
end

--------------------------------------------------------------------------------

function M.mkBox(h, w)
  local box = ItemPH.wrap(Canvas:new(nil, h, w), 1, 0) -- bufnr:1 lnum:0
  return box
end

--
---@param h number heigth of canvas
---@param w number width of canvas
---@param ... table{number, number}
function M.mkBoxWithMarkers(h, w, ...)
  local box = ItemPH.wrap(Canvas:new(nil, h, w), 1, 0) -- bufnr:1 lnum:0
  for i = 1, select('#', ...) do
    local p = select(i, ...)
    assert(type(p[1]) == 'number', 'has x')
    assert(type(p[2]) == 'number', 'has y')

    box:mark_pos(p[1], p[2])
  end

  return box
end

-- helper
function M.mk_pos_list(...)
  local t = {}
  for i = 1, select('#', ...) do
    local p = select(i, ...)
    assert(type(p[1]) == 'number', 'has x')
    assert(type(p[2]) == 'number', 'has y')
    t[#t + 1] = { x = p[1], y = p[2] }
  end
  return t
end

function M.mkLine(x1, y1, x2, y2, c)
  return Line:new(nil, x1, y1, x2, y2, c or '-')
end

-- local mkPosXY = function(x, y) return { x = x, y = y } end

--
-- create new canvas and draw given element
--
---@param border table?{x1,y1,x2,y2, color}
function M.show_element(e, w, h, border)
  w, h = w or 32, h or 10
  local canvas = Canvas:new(nil, h, w)
  canvas:add(e)
  local ex1, _, _, _ = e:get_coords()
  if ex1 == 0 then
    e:move(4, 1)
  end

  -- draw border for given coords
  if type(border) == 'table' then
    local t = border
    local x1, y1, x2, y2, color = t[1], t[2], t[3], t[4], t[5] or '~'
    if x1 and y1 and x2 and y2 then
      canvas:addHook(Canvas.HOOK_PRE_DRAW, function(c)
        c:draw_rect(nil, x1, y1, x2, y2, color)
      end)
    end
  end

  return canvas:draw():toLines()
end

function M.mk_editor(obj, opts)
  local editor = IEditable.cli().Editor.init(obj, opts)
  return editor
end

---@param obj table
---@param opts table?
---@param user_input string
function M.do_edit_transaction(obj, opts, user_input)
  opts = opts or {}

  ---@diagnostic disable-next-line: unused-local
  opts.ui_read_line = function(prompt, line)
    if type(opts.logs) == 'table' then
      opts.logs[#opts.logs + 1] = { prompt, line }
    end
    return user_input
  end

  local ok, errmsg_or_patch = IEditable.cli().edit_transaction(obj, opts)
  return ok, errmsg_or_patch
end

---@param obj table
---@param opts table?
---@param user_input string
function M.get_edit_patch(obj, opts, user_input)
  local cli = IEditable.cli()
  local editor = cli.Editor.init(obj, opts)

  local prompt, line = editor:build_prompt_oneliner()
  local ok, patch, stop = editor:parse_input(prompt, user_input)
  return patch, {
    ok = ok,
    prompt = prompt,
    def_line = line,
    stop = stop,
    editor = editor
  }
end

return M

-- 30-03-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local class = require("oop.class")
local log = require 'alogger'

local Rectangle = require("env.draw.Rectangle");
local Canvas = require("env.draw.Canvas");
local IEditable = require("env.draw.IEditable");
local H = require("env.draw.test_helper");

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id


describe("env.draw.Rectangle", function()
  after_each(function()
    log.fast_off()
  end)

  it("new + _init", function()
    local exp = {
      tline = { x1 = 1, y1 = 2, x2 = 3, y2 = 2, color = '-' },
      rline = { x1 = 3, y1 = 3, x2 = 3, y2 = 3, color = '|' },
      bline = { x1 = 1, y1 = 4, x2 = 3, y2 = 4, color = '-' },
      lline = { x1 = 1, y1 = 3, x2 = 1, y2 = 3, color = '|' },
      bg = '',
      corners = Rectangle.DEFAULT_CORNERS
    }
    local r = Rectangle:new(nil, 1, 2, 3, 4)
    assert.same(exp, r)
  end)

  it("get_size", function()
    local r = Rectangle:new(nil, 1, 1, 5, 8)
    local w, h = r:size()
    assert.same(5, w)
    assert.same(8, h)
    assert.same('Rectangle (1:1 5:8) (5x8) bg: ""', tostring(r))
  end)

  it("pos", function()
    local r = Rectangle:new(nil, 1, 2, 5, 8)
    local x, y = r:pos()
    assert.same('1 2', tostring(x) .. ' ' .. tostring(y))
  end)

  it("__tostring", function()
    local r = Rectangle:new(nil, 1, 1, 5, 8)
    assert.same('Rectangle (1:1 5:8) (5x8) bg: ""', r:__tostring())
  end)

  it("draw", function()
    local canvas = Canvas:new(nil, 6, 8)
    -- 12345678
    local exp = {
      '        ',
      ' +--+   ',
      ' |  |   ',
      ' |  |   ',
      ' +--+   ',
      '        ' } -- 6
    -- assert.same(exp, canvas:toLines())
    local r = Rectangle:new(nil, 2, 2, 5, 5)
    canvas:add(r)
    assert.same(r, canvas.objects[1])
    canvas:draw()
    assert.same(exp, canvas:toLines())
  end)

  it("draw", function()
    local c = Canvas:new(nil, 6, 8)
    local f = function(r) return c:add(r):draw():rm_objects():toLines() end

    local exp = {
      '+------+',
      '|      |',
      '|      |',
      '|      |',
      '|      |',
      '+------+' }
    assert.same(exp, f(Rectangle:new(nil, 1, 1, 8, 6)))

    exp = { -- top and left lines out of canvas
      '       |',
      '       |',
      '       |',
      '       |',
      '       |',
      '-------+' }
    assert.same(exp, f(Rectangle:new(nil, -1, -1, 8, 6)))
  end)

  it("isInsidePos", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    assert.same(true, r:isInsidePos(1, 1))
    assert.same(true, r:isInsidePos(1, 8))
    assert.same(true, r:isInsidePos(8, 1))
    assert.same(true, r:isInsidePos(8, 8))
    assert.same(true, r:isInsidePos(4, 4))
    assert.same(true, r:isInsidePos(2, 2))
    assert.same(true, r:isInsidePos(7, 7))

    assert.same(false, r:isInsidePos(-8, -8))
    assert.same(false, r:isInsidePos(0, 0))
    assert.same(false, r:isInsidePos(9, 0))
    assert.same(false, r:isInsidePos(9, 9))
  end)

  it("copy", function()
    local r = Rectangle:new(nil, 1, 1, 8, 8)
    local cr = r:copy()
    local exp = {
      lline = { x1 = 1, x2 = 1, y2 = 7, color = '|', y1 = 2 },
      rline = { x1 = 8, x2 = 8, y2 = 7, color = '|', y1 = 2 },
      tline = { x1 = 1, x2 = 8, y2 = 1, color = '-', y1 = 1 },
      bline = { x1 = 1, x2 = 8, y2 = 8, color = '-', y1 = 8 },
      bg = '',
      corners = Rectangle.DEFAULT_CORNERS
    }
    assert.same(exp, r)
    assert.same(exp, cr)
  end)


  it("serialize", function()
    local rect = Rectangle:new(nil, 2, 2, 4, 4)
    assert.same('env.draw.Rectangle', class.name(rect))

    local exp = {
      _class = 'env.draw.Rectangle',
      x1 = 2,
      x2 = 4,
      y2 = 4,
      y1 = 2,
      hline = '-',
      vline = '|',
      corners = '++++',
      bg = '',
    }
    assert.same(exp, rect:serialize())
  end)

  it("fmtSerialized", function()
    local r = Rectangle:new(nil, 1, 2, 3, 4)
    local opts = { cname2id = new_cname2id() }
    local arr = r:serialize(opts)
    local exp = {
      _clsid = 2,
      x1 = 1,
      y1 = 2,
      x2 = 3,
      y2 = 4,
      bg = '',
      vline = '|',
      hline = '-',
      corners = '++++',
    }
    assert.same(exp, arr)
    opts.ident = ''
    local exp2 =
    "{_clsid=2,x1=1,y1=2,x2=3,y2=4,hline='-',vline='|',corners='++++',bg=''}"
    assert.same(exp2, r:fmtSerialized(arr, 0, opts))
  end)

  it("desirialize via IEditable", function()
    local serialized = {
      _class = 'env.draw.Rectangle',
      x1 = 2,
      x2 = 4,
      y2 = 4,
      y1 = 2,
      hline = '-',
      vline = '|',
      corners = '+',
      bg = '',
    }

    local rect = IEditable.deserialize(serialized)
    assert.same('env.draw.Rectangle', class.name(rect))
    local exp = 'Rectangle (2:2 4:4) (3x3) bg: ""'
    assert.same(exp, tostring(rect))
  end)
end)

describe("env.draw.Line intersects", function()
  after_each(function()
    log.fast_off()
    Canvas.MERGE_COLORS = false
  end)

  it("isIntersects horizontal", function()
    local ci = H.check_intersects
    local c = Canvas:new(nil, 5, 16)
    Canvas.MERGE_COLORS = true
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 4, '-')) ---@cast r env.draw.Rectangle
    assert.same('Rectangle (5:2 11:4) (7x3) bg: ""', tostring(r))
    assert.same(true, r:isIntersects(3, 2, 10, 2))
    local exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~~~~~',
        '~   +-----+    ~',
        '~   |     |    ~',
        '~   +-----+    ~',
        '~~~~~~~~~~~~~~~~'
      }
    }
    exp = {
      intersects = true,
      {
        '                ',
        '    #=====#     ',
        '    +     +     ',
        '    #=====#     ',
        '                '
      }
    }
    assert.same(exp, ci(c, r, 5, 2, 11, 4))
    exp = {
      intersects = true,
      {
        '                ',
        '    +-----+     ',
        '    |~~~~~|     ',
        '    +-----+     ',
        '                '
      }
    }
    assert.same(exp, ci(c, r, 6, 3, 10, 3))
    exp = {
      intersects = false,
      {
        '~~~~            ',
        '~  ~+-----+     ',
        '~  ~|     |     ',
        '~  ~+-----+     ',
        '~~~~            '
      }
    }
    assert.same(exp, ci(c, r, 1, 1, 4, 5))
    exp = {
      intersects = false,
      {
        '           ~~~~~',
        '    +-----+~   ~',
        '    |     |~   ~',
        '    +-----+~   ~',
        '           ~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 12, 1, 16, 5))
    exp = {
      intersects = false,
      {
        '~~~~~~~~~~~~~~~~',
        '    +-----+     ',
        '    |     |     ',
        '    +-----+     ',
        '                '
      }
    }
    assert.same(exp, ci(c, r, 1, 1, 16, 1))
    exp = {
      intersects = false,
      {
        '                ',
        '    +-----+     ',
        '    |     |     ',
        '    +-----+     ',
        '~~~~~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 1, 5, 16, 5))
    -- true
    exp = {
      intersects = true,
      {
        '                ',
        '    +-----+     ',
        '    |     |     ',
        '~~~~#=====#~~~~~',
        '~~~~~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 1, 4, 16, 5))
    exp = {
      intersects = true,
      {
        '                ',
        '    +-----+     ',
        '~~~~+~~~~~+~~~~~',
        '~   +-----+    ~',
        '~~~~~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 1, 3, 16, 5))
    exp = {
      intersects = true,
      {
        '                ',
        '~~~~#=====#~~~~~',
        '~   |     |    ~',
        '~   +-----+    ~',
        '~~~~~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 1, 2, 16, 5))
    exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~~~~~',
        '~   +-----+    ~',
        '~   |     |    ~',
        '~   +-----+    ~',
        '~~~~~~~~~~~~~~~~'
      }
    }
    assert.same(exp, ci(c, r, 1, 1, 16, 5))
    exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~~~~~',
        '~~~~#=====#~~~~~',
        '    |     |     ',
        '    +-----+     ',
        '                '
      }
    }
    assert.same(exp, ci(c, r, 1, 1, 16, 2))

    exp = {
      intersects = true,
      {
        '~~~~~~~~~~~~~~~~',
        '~   +-----+    ~',
        '~~~~+~~~~~+~~~~~',
        '    +-----+     ',
        '                '
      }
    }
    assert.same(exp, ci(c, r, 1, 1, 16, 3))
  end)

  it("isOverlap", function()
    local co = H.check_overlap
    local c = Canvas:new(nil, 5, 16)
    Canvas.MERGE_COLORS = true
    local _, r = c:add(Rectangle:new(nil, 4, 1, 12, 5, '-')) ---@cast r env.draw.Rectangle
    assert.same('Rectangle (4:1 12:5) (9x5) bg: ""', tostring(r))
    assert.same(true, r:isOverlap(5, 2, 11, 4))
    local exp = {
      intersects = true,
      {
        '   #=======#    ',
        '   +       +    ',
        '   +       +    ',
        '   +       +    ',
        '   #=======#    '
      }
    }
    assert.same(exp, co(c, r, 4, 1, 12, 5))

    exp = {
      intersects = true,
      {
        '   +-------+    ',
        '   |~~~~~~~|    ',
        '   |~     ~|    ',
        '   |~~~~~~~|    ',
        '   +-------+    '
      }
    }
    assert.same(exp, co(c, r, 5, 2, 11, 4))

    exp = {
      intersects = false,
      {
        '   +-------+    ',
        '~~~+~~~~~~~|    ',
        '~  |      ~|    ',
        '~~~+~~~~~~~|    ',
        '   +-------+    '
      }
    }
    assert.same(exp, co(c, r, 1, 2, 11, 4))

    exp = {
      intersects = false,
      {
        '   +-------+    ',
        '   | ~~~~~~+~~~ ',
        '   | ~     |  ~ ',
        '   | ~~~~~~+~~~ ',
        '   +-------+    '
      }
    }
    assert.same(exp, co(c, r, 6, 2, 15, 4))

    exp = {
      intersects = false,
      {
        '   +-=---=-+    ',
        '   | ~   ~ |    ',
        '   | ~   ~ |    ',
        '   | ~~~~~ |    ',
        '   +-------+    '
      }
    }
    assert.same(exp, co(c, r, 6, 0, 10, 4))

    exp = {
      intersects = false,
      {
        '   +-------+    ',
        '   | ~~~~~ |    ',
        '   | ~   ~ |    ',
        '   | ~   ~ |    ',
        '   +-=---=-+    '
      }
    }
    assert.same(exp, co(c, r, 6, 2, 10, 6))
  end)
end)


describe("env.draw.Line cutoff", function()
  after_each(function()
    log.fast_off()
    Canvas.MERGE_COLORS = false
  end)

  local function replace(c, old_elm, parts)
    c:remove(old_elm)
    for _, elm in ipairs(parts) do
      c:add(elm)
    end
  end

  it("top-left", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    assert.same('Rectangle (5:2 11:6) (7x5) bg: ""', tostring(r))
    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '    |     |     ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    assert.same(exp, c:draw():toLines())

    -- covers the entire rectangle -- remove all from original element no parts
    local has, parts = r:cutoff(4, 0, 12, 6)
    assert.same({ true, {} }, { has, parts })

    -- same range as rect -- remove all parts
    has, parts = r:cutoff(5, 1, 11, 6)
    assert.same({ true, {} }, { has, parts })

    -- top-left
    has, parts = r:cutoff(5, 1, 8, 3)

    exp = {
      '                ',
      '        --+     ',
      '          |     ',
      '    |     |     ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("horizontal top", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    assert.same('Rectangle (5:2 11:6) (7x5) bg: ""', tostring(r))
    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '    |     |     ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    assert.same(exp, c:draw():toLines())

    local _, parts = r:cutoff(5, 1, 12, 3)
    exp = {
      '                ',
      '                ',
      '                ',
      '    |     |     ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("horizontal bottom", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(5, 5, 12, 7)

    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '    |     |     ',
      '                ',
      '                ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("horizontal middle", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(1, 4, 12, 4)

    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '                ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)


  it("vertical right", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(8, 1, 12, 7)

    local exp = {
      '                ',
      '    +--         ',
      '    |           ',
      '    |           ',
      '    |           ',
      '    +--         ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("vertical left", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(1, 1, 8, 7)

    local exp = {
      '                ',
      '        --+     ',
      '          |     ',
      '          |     ',
      '          |     ',
      '        --+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("vertical middle", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(7, 1, 8, 7)

    local exp = {
      '                ',
      '    +-  --+     ',
      '    |     |     ',
      '    |     |     ',
      '    |     |     ',
      '    +-  --+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("top-right", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(8, 1, 12, 4)

    local exp = {
      '                ',
      '    +--         ',
      '    |           ',
      '    |           ',
      '    |     |     ',
      '    +-----+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("bottom-right", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(8, 4, 12, 6)

    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '    |           ',
      '    |           ',
      '    +--         ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("bottom-left", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(1, 4, 8, 6)

    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '          |     ',
      '          |     ',
      '        --+     ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)

  it("bottom-right point", function()
    local c = Canvas:new(nil, 7, 16)
    local _, r = c:add(Rectangle:new(nil, 5, 2, 11, 6, '-')) ---@cast r env.draw.Rectangle
    local _, parts = r:cutoff(11, 6, 11, 6)

    local exp = {
      '                ',
      '    +-----+     ',
      '    |     |     ',
      '    |     |     ',
      '    |     |     ',
      '    +-----      ',
      '                '
    }
    replace(c, r, parts)
    assert.same(exp, c:draw():toLines())
  end)
end)

--[[
Ideas
      +----+
    + |    |
   / \     |
  /   \    |
 /    |\   |
+     +-\--+
 \       +
  \     /
   \   /
    \ /
     +
]]

-- 02-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local class = require("oop.class")
local Text = require("env.draw.Text");
local Canvas = require("env.draw.Canvas");
local IEditable = require("env.draw.IEditable");

local SH = class.serialize_helper
local new_cname2id = SH.new_cname2id

describe("env.draw.Text", function()
  it("calc_text_range", function()
    local f = function(line)
      local w, h = Text.do_calc_text_range(line)
      return tostring(w) .. '|' .. tostring(h)
    end
    --                    1234567890
    assert.same('9|1', f("some line"))
    assert.same('4|2', f("some\nline"))
    assert.same('13|4', f("some\nline\nin-the-middle\nend"))
    assert.same('0|4', f("\n\n\n\n"))
    assert.same('3|6', f("\n\n\n\n\nabc"))
    assert.same('3|6', f("\n\n\n\n\nabc"))
  end)


  it("draw UFT-8", function()
    local height, width = 6, 16
    local canvas = Canvas:new(nil, height, width)
    local x1, y1 = 4, 2
    local text = "       \n        \n       \n    \n    "
    local txtObj = Text:new(nil, x1, y1, text, nil, nil)
    canvas:add(txtObj)
    local exp = {
      '                ',
      '           ',
      '           ',
      '           ',
      '            ',
      '           '
    }
    assert.same(exp, canvas:draw():toLines())
    local w, h = txtObj:size()
    assert.same('13|5', tostring(w) .. '|' .. tostring(h))
  end)


  it("draw UFT-8 limited space", function()
    local height, width = 6, 16
    local canvas = Canvas:new(nil, height, width)
    local x1, y1, x2, y2 = 4, 2, 7, 5 -- limited!
    local text = "       \n        \n       \n    \n    "
    local txtObj = Text:new(nil, x1, y1, text, x2, y2)
    canvas:add(txtObj)
    local exp = {
      '                ',
      '              ',
      '               ',
      '              ',
      '              ',
      '                '
    }
    assert.same(exp, canvas:draw():toLines())
    local w, h = txtObj:size()
    assert.same('4|4', tostring(w) .. '|' .. tostring(h))
  end)


  it("draw_text just the mechanics only (UFT-8 Ru)", function()
    local canvas = Canvas:new(nil, 3, 12) -- height, width
    local text = 'Привет\nМир\n!'
    local _, txtObj = canvas:add(Text:new(nil, 3, 1, text, nil, nil))
    local exp = {
      '  Привет    ',
      '  Мир       ',
      '  !         ' }
    assert.same(exp, canvas:draw():toLines())
    --- cast txtObj env.draw.IEditable  does not work
    ---@diagnostic disable-next-line: undefined-field
    local w, h = txtObj:size()
    assert.same('6|3', tostring(w) .. '|' .. tostring(h))
  end)
end)

describe("env.draw.Text serialize deserialize", function()
  it("tag & serialize deserialize", function()
    local t = Text:new(nil, 0, 0, 'some text')
    local exp = {
      x1 = 0,
      y1 = 0,
      x2 = 8,
      y2 = 0,
      _class = 'env.draw.Text',
      text = 'some text',
    }
    assert.same(exp, t:serialize())
    assert.same(nil, t:tag())

    t:tagged('TAG')
    assert.same('TAG', t:tag())
    local exp2 = {
      y1 = 0,
      x1 = 0,
      x2 = 8,
      y2 = 0,
      _class = 'env.draw.Text',
      text = 'some text',
      _tag = 'TAG',
    }
    assert.same(exp2, t:serialize())

    -- sure tag prersists
    local rept = IEditable.deserialize(exp2) ---@cast rept env.draw.Text
    assert.same('TAG', rept:tag())
  end)

  it("fmtSerialized", function()
    local line = Text:new(nil, 1, 2, 'TEXT')
    local opts = { cname2id = new_cname2id() }
    local arr = line:serialize(opts)
    opts.ident = ''
    local exp = "{_clsid=2,x1=1,y1=2,x2=4,y2=2,text='TEXT'}"
    assert.same(exp, line:fmtSerialized(arr, 0, opts))
  end)
end)

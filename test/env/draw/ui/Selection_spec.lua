-- 05-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.draw.ui.Selection");
local Rectangle = require("env.draw.Rectangle");

describe("env.draw.ui.Selection", function()
  it("get_range_coords", function()
    local list = {
      Rectangle:new(nil, 1, 1, 3, 3),
    }
    local f = function(t)
      local x1, y1, x2, y2 = M.get_range_coords(t)
      return tostring(x1) .. "|" .. tostring(y1) .. "|" .. tostring(x2) .. "|" .. tostring(y2)
    end
    assert.same('1|1|3|3', f(list))

    list[#list + 1] = Rectangle:new(nil, 1, 10, 11, 3)
    assert.same('1|1|11|10', f(list))

    list[#list + 1] = Rectangle:new(nil, 5, 6, 11, 15)
    assert.same('1|1|11|15', f(list))
  end)

  it("__tostring", function()
    local items = {
      Rectangle:new(nil, 0, 0, 1, 1),
      Rectangle:new(nil, 0, 1, 2, 2),
    }
    local x1, y1, x2, y2, xoff, yoff = 1, 2, 3, 4, 5, 6
    local selection = M:new(nil, M.TYPE_SELECT, items, x1, y1, x2, y2, xoff, yoff)
    local exp = 's [1:2 3:4][5:6] {Rectangle(2x2)(0:0) Rectangle(3x2)(0:1)}'
    assert.same(exp, selection:__tostring())
  end)

  it("get_selection_info one object in selection list", function()
    local s = M:new(nil, M.TYPE_COPY, { Rectangle:new(nil, 0, 0, 4, 8) }, 2, 4)
    local exp = 'c [2:4 0:0][0:0] {Rectangle(5x9)(0:0)}'
    assert.same(exp, tostring(s))
  end)

  it("get_selection_info two objects in selection list", function()
    local s = M:new(nil, M.TYPE_COPY, {
      Rectangle:new(nil, 0, 1, 4, 8),
      Rectangle:new(nil, 0, 2, 3, 6)
    }, 2, 4)
    local exp = 'c [2:4 0:0][0:0] {Rectangle(5x8)(0:1) Rectangle(4x5)(0:2)}'
    assert.same(exp, tostring(s))
  end)
end)

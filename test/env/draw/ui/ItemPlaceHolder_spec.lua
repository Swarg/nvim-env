-- 01-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("env.draw.ui.ItemPlaceHolder");

describe("env.draw.ui.ItemPlaceHolder", function()
  it("isIntersects", function()
    -- local f = M.isIntersects
    local item = { width = 4, height = 4 }
    local box = M.wrap(item, 1, 0) -- bufnr:1 lnum:0
    local exp = 'ItemPH lnum:0:4 col:0:4 (table)'
    assert.same(exp, M.__tostring(box))
    assert.same('ItemPH lnum:0:4 col:0:4 (table)', tostring(box))

    local box2 = M.wrap(item, 1, 10) -- bufnr:1 lnum:0
    assert.same('ItemPH lnum:10:14 col:0:4 (table)', M.__tostring(box2))
    assert.same('ItemPH lnum:10:14 col:0:4 (table)', tostring(box2))
  end)

  it("marked position", function()
    local box = M:new()
    local get = function(b)
      local x, y = b:getMarkedPosXY()
      return (tostring(x) .. "|" .. tostring(y))
    end

    assert.is_false(box:has_marked_pos())
    assert.is_nil(box:mark_pos(1, 1))
    assert.same('1|1', get(box))
    assert.is_true(box:has_marked_pos())

    assert.same({ x = 1, y = 1 }, box:mark_pos(2, 2))
    assert.same('2|2', get(box))
    assert.same({ x = 2, y = 2 }, box:mark_pos(3, 3))
    assert.same('3|3', get(box))

    assert.same({ x = 3, y = 3 }, box:pop_marked_pos())
    assert.same({ x = 2, y = 2 }, box:pop_marked_pos())
    assert.same({ x = 1, y = 1 }, box:pop_marked_pos())
    assert.same(nil, box:pop_marked_pos())
    assert.same('nil|nil', get(box))
  end)

  it("pop_marked_positions", function()
    local box = M:new()
    box:mark_pos(1, 1)
    box:mark_pos(2, 2)
    box:mark_pos(3, 3)
    local exp = { { x = 1, y = 1 }, { x = 2, y = 2 }, { x = 3, y = 3 } }
    assert.same(exp, box:pop_marked_positions())
    assert.same(nil, box:pop_marked_positions())
  end)

  it("shortly", function()
    local item = { width = 8, height = 4 }
    local box = M.wrap(item, 1, 0) -- bufnr:1 lnum:0
    assert.same('lnum: 1:5 size:8x4 elms:-1', box:shortly())

    local box2 = {}
    assert.same('lnum: -1:-1 size:-1x-1 elms:-1', M.shortly(box2))
  end)
end)

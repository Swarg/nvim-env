-- 09-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
--

_G.TEST = true
local log = require('alogger')
local Canvas = require("env.draw.Canvas");
local Point = require("env.draw.Point");
local Line = require("env.draw.Line");
local Rectangle = require("env.draw.Rectangle");
local Container = require("env.draw.Container");
local M = require("env.draw.ui.ElementsEditor");
local H = require("env.draw.test_helper");

local get_elements_readable_list = Canvas.get_elements_readable_list
local p2s = H.patch_to_readable

describe("env.ui.ElementsEditor edit elements via UI", function()
  after_each(function()
    log.fast_off()
    require 'dprint'.disable()
  end)

  ---@return env.draw.Canvas
  ---@return env.draw.Line
  ---@return env.draw.Line
  ---@return env.draw.Point
  ---@return env.draw.Point
  local function prepare_canvas()
    local canvas = Canvas:new(nil, 5, 16)
    local _, l1 = canvas:add(Line:new(nil, 5, 2, 10, 2, '-'))
    local _, l2 = canvas:add(Line:new(nil, 5, 4, 10, 4, '~'))
    local _, p3 = canvas:add(Point:new(nil, 7, 3, '>'))
    local _, p4 = canvas:add(Point:new(nil, 12, 5, '<'))
    ---@diagnostic disable-next-line: return-type-mismatch
    return canvas, l1, l2, p3, p4
  end

  it("prepare_canvas", function()
    local canvas = prepare_canvas()
    local exp = {
      '                ',
      '    ------      ',
      '      >         ',
      '    ~~~~~~      ',
      '           <    ',
    }

    assert.same(exp, canvas:draw():toLines())
    exp = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <',
    }
    assert.same(exp, get_elements_readable_list(canvas))
  end)


  it("elements_to_code", function()
    local c = prepare_canvas()
    local elms, idxs = c:get_elements_by_index_range(1, 5)
    assert.same({ 4, 3, 2, 1 }, idxs)
    local exp_code = {
      '  4  Point 12:5 color: <',
      '  3  Point 7:3 color: >',
      '  2  Line (5:4 10:4) color: ~',
      '  1  Line (5:2 10:2) color: -'
    }
    assert.same(exp_code, Canvas.mk_elements_readable(elms, idxs))

    exp_code = {
      "A: {_class='env.draw.Line',x1=5,y1=2,x2=10,y2=2,color='-'}",
      "B: {_class='env.draw.Line',x1=5,y1=4,x2=10,y2=4,color='~'}",
      "C: {_class='env.draw.Point',x=7,y=3,color='>'}",
      "D: {_class='env.draw.Point',x=12,y=5,color='<'}"
    }
    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    assert.same(exp_code, lines_with_code)
    local exp_mappings = {
      lnum2letter = { [1] = 'A', [2] = 'B', [3] = 'C', [4] = 'D' },
      letter2idx = { A = 1, B = 2, C = 3, D = 4 }
    }
    assert.same(exp_mappings, mappings)
  end)

  --
  it("build_edit_elements_patch from lines&mappings", function()
    local canvas = prepare_canvas()
    -- 1.1 gets elements for convert into code
    local elms, idxs = canvas:get_elements_by_index_range(1, 5)

    -- 1.2 convert elements to code for human editing
    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    -- lines - is code representation to place into buff
    -- mappings - used to edit elements order in the canvas list

    -- workload
    -- 2. second part triggered via save buffer event BufWriteCmd(acwrite)
    local ok, cpatch = M.build_edit_elements_patch(lines_with_code, mappings)
    assert.same(true, ok)

    local exp_canvas_patch = {
      {
        elm_modt = {
          _class = 'env.draw.Line', color = '-', x1 = 5, y1 = 2, x2 = 10, y2 = 2,
        },
        new_idx = 1,
        old_idx = 1
      },
      {
        elm_modt = {
          _class = 'env.draw.Line', color = '~', x1 = 5, y1 = 4, x2 = 10, y2 = 4,
        },
        new_idx = 2,
        old_idx = 2
      },
      {
        elm_modt = { y = 3, x = 7, color = '>', _class = 'env.draw.Point' },
        new_idx = 3,
        old_idx = 3
      },
      {
        elm_modt = { y = 5, x = 12, color = '<', _class = 'env.draw.Point' },
        new_idx = 4,
        old_idx = 4
      }
    }
    -- print("[DEBUG] cpatch:", require"inspect"(cpatch))
    assert.same(exp_canvas_patch, cpatch)
  end)

  it("tooling patch_to_readable", function()
    local canvas_patch = {
      {
        old_idx = 1,
        new_idx = 1,
        elm_modt = { x1 = 5, y1 = 2, x2 = 10, y2 = 2, color = '-' }
      },
      {
        old_idx = 2,
        new_idx = 2,
        elm_modt = { x1 = 5, y1 = 4, x2 = 10, y2 = 4, color = '~' }
      },
      { old_idx = 3, new_idx = 3, elm_modt = { x = 7, y = 3, color = '>' } },
      { old_idx = 4, new_idx = 4, elm_modt = { x = 12, y = 5, color = '<' } }
    }
    local exp = {
      'idx:(1 -> 1) 5:2 10:2 color:-',
      'idx:(2 -> 2) 5:4 10:4 color:~',
      'idx:(3 -> 3) 7:3 color:>',
      'idx:(4 -> 4) 12:5 color:<'
    }
    assert.same(exp, H.patch_to_readable(canvas_patch))
    assert.same(exp, p2s(canvas_patch))
  end)

  it("build_edit_elements_patch wihtout changes", function()
    local code = {
      'A: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
      'B: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'C: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      'D: {_class = "env.draw.Point",color = "<",x = 12,y = 5}'
    }
    local mappings = {
      lnum2letter = { [1] = 'A', [2] = 'B', [3] = 'C', [4] = 'D' },
      letter2idx = { A = 1, B = 2, C = 3, D = 4 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp = {
      'idx:(1 -> 1) 5:2 10:2 color:-',
      'idx:(2 -> 2) 5:4 10:4 color:~',
      'idx:(3 -> 3) 7:3 color:>',
      'idx:(4 -> 4) 12:5 color:<'
    }
    assert.same(exp, p2s(patch)) ---@cast patch table

    local canvas = prepare_canvas()

    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(canvas))

    local ok2, ret = canvas:apply_patch(patch) --                  << work load

    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 0, updated = 0, chparts = 0 }, ret)

    assert.same(exp_before_patch, Canvas.get_elements_readable_list(canvas))
  end)


  it("build_edit_elements_patch change order (swap index position in list)", function()
    local code = { -- A <-> B swapped order
      'B: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'A: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
      'C: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      'D: {_class = "env.draw.Point",color = "<",x = 12,y = 5}'
    }
    local mappings = {
      lnum2letter = { [1] = 'A', [2] = 'B', [3] = 'C', [4] = 'D' },
      letter2idx = { A = 1, B = 2, C = 3, D = 4 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp = {
      'idx:(2 -> 1) 5:4 10:4 color:~',
      'idx:(1 -> 2) 5:2 10:2 color:-',
      'idx:(3 -> 3) 7:3 color:>',
      'idx:(4 -> 4) 12:5 color:<'
    }
    assert.same(exp, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()

    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                         << work load

    assert.same(true, ok2)
    assert.same({ updated = 0, chparts = 0, deleted = 0, swapped = 2 }, ret)

    local exp_changed_order = {
      '  1  Line (5:4 10:4) color: ~',
      '  2  Line (5:2 10:2) color: -',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_changed_order, Canvas.get_elements_readable_list(c))
  end)


  it("build_edit_elements_patch change order swap index position 2", function()
    local code = { -- A -> C C -> D D-> A
      'D: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      'B: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'A: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
      'C: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
    }
    local mappings = {
      lnum2letter = { [1] = 'A', [2] = 'B', [3] = 'C', [4] = 'D' },
      letter2idx = { A = 1, B = 2, C = 3, D = 4 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok) ---@cast patch table
    local exp_patch = {
      'idx:(4 -> 1) 12:5 color:<',
      'idx:(2 -> 2) 5:4 10:4 color:~',
      'idx:(1 -> 3) 5:2 10:2 color:-',
      'idx:(3 -> 4) 7:3 color:>'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()

    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -', -- A -> C
      '  2  Line (5:4 10:4) color: ~', -- B
      '  3  Point 7:3 color: >',       -- C -> D
      '  4  Point 12:5 color: <'       -- D -> A
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                         << work load

    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 3, updated = 0, chparts = 0 }, ret)

    local exp_changed_order = {
      '  1  Point 12:5 color: <',      -- A <- D
      '  2  Line (5:4 10:4) color: ~', -- B
      '  3  Line (5:2 10:2) color: -', -- C <- A
      '  4  Point 7:3 color: >'        -- D <- C
    }
    assert.same(exp_changed_order, Canvas.get_elements_readable_list(c))
  end)

  -- attemp to reuse a busy index A & A
  it("build_edit_elements_patch swap index position with error ", function()
    local code = {
      'A: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      'D: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
      'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(false, ok)
    local exp = 'attempt to reuse a busy index at lnum: 2'
    assert.same(exp, patch) ---@cast patch table
  end)


  it("build_edit_elements_patch delete last", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      'B: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      -- D is deleted (Line 5:4 - 10:4)
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 12:5 color:<',
      'idx:(3 -> 3) 7:3 color:>',
      'idx:(2 -> 2) 5:4 10:4 color:~',
      'idx:(1 -> 1) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -', -- to delete
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                              << workload
    assert.same(true, ok2)
    assert.same({ deleted = 1, swapped = 0, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Line (5:4 10:4) color: ~',
      '  2  Point 7:3 color: >',
      '  3  Point 12:5 color: <'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)


  it("build_edit_elements_patch delete first", function()
    local code = {
      -- delete 'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      'B: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'D: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(3 -> 4) 7:3 color:>',
      'idx:(2 -> 3) 5:4 10:4 color:~',
      'idx:(1 -> 2) 5:2 10:2 color:-',
      'idx:(4 -> 4) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                              << workload
    assert.same(true, ok2)
    assert.same({ deleted = 1, swapped = 3, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)


  it("build_edit_elements_patch delete middle", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      -- delete 'B: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      -- delete 'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'D: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 12:5 color:<',
      'idx:(1 -> 3) 5:2 10:2 color:-',
      'idx:(2 -> 2) DELETE',
      'idx:(3 -> 3) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                              << workload
    assert.same(true, ok2)
    assert.same({ deleted = 2, swapped = 1, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Point 12:5 color: <'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)


  it("lua table.remove", function()
    local t = { 1, 2, 3, 4 }
    table.remove(t, 3)
    assert.same({ 1, 2, 4 }, t)
    table.remove(t, 2)
    assert.same({ 1, 4 }, t)
    table.remove(t, 1)
    assert.same({ 4 }, t)

    local t2 = { 1, 2, 3, 4 }
    t2[3] = nil
    t2[2] = nil
    t2[1] = nil
    assert.same({ [4] = 4 }, t2)

    table.remove(t2, 3)
    assert.same({ [3] = 4 }, t2)

    table.remove(t2, 2)
    assert.same({ [3] = 4 }, t2)

    table.remove(t2, 1)
    assert.same({ [3] = 4 }, t2)
  end)

  it("build_edit_elements_patch delete many from bottom", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      -- 'B: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      -- 'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      -- 'D: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 12:5 color:<',
      'idx:(1 -> 1) DELETE',
      'idx:(2 -> 2) DELETE',
      'idx:(3 -> 3) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch) --                              << workload
    assert.same(true, ok2)
    assert.same({ deleted = 3, swapped = 0, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = { '  1  Point 12:5 color: <' }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch delete many from top", function()
    local code = {
      --'A: {_class = "env.draw.Point",color = "<",x = 12,y = 5}',
      -- 'B: {_class = "env.draw.Point",color = ">",x = 7,y = 3}',
      -- 'C: {_class = "env.draw.Line",color = "~",x1 = 5,x2 = 10,y1 = 4,y2 = 4}',
      'D: {_class = "env.draw.Line",color = "-",x1 = 5,x2 = 10,y1 = 2,y2 = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(1 -> 4) 5:2 10:2 color:-',
      'idx:(4 -> 4) DELETE',
      'idx:(2 -> 2) DELETE',
      'idx:(3 -> 3) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 3, swapped = 1, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = { '  1  Line (5:2 10:2) color: -' }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch delete all", function()
    local code = {}
    local mappings = {
      lnum2letter = { 'A', 'B', 'C', 'D' },
      letter2idx = { A = 4, B = 3, C = 2, D = 1 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) DELETE',
      'idx:(1 -> 1) DELETE',
      'idx:(2 -> 2) DELETE',
      'idx:(3 -> 3) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Line (5:2 10:2) color: -',
      '  2  Line (5:4 10:4) color: ~',
      '  3  Point 7:3 color: >',
      '  4  Point 12:5 color: <'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 4, swapped = 0, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {}
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)
end)

--------------------------------------------------------------------------------

--
-- patched element list is a part of all elements in the canvas
describe("env.ui.ElementsEditor edit elements via UI 2", function()
  after_each(function()
    log.fast_off()
    require 'dprint'.disable()
  end)

  ---@return env.draw.Canvas
  ---@return env.draw.Rectangle
  ---@return env.draw.Line
  ---@return env.draw.Line
  ---@return env.draw.Point
  ---@return env.draw.Point
  ---@return env.draw.Point
  local function prepare_canvas()
    local canvas = Canvas:new(nil, 5, 16)
    local _, r1 = canvas:add(Rectangle:new(nil, 1, 1, 15, 5, '='))
    local _, l2 = canvas:add(Line:new(nil, 8, 2, 14, 2, '-'))
    local _, l3 = canvas:add(Line:new(nil, 7, 3, 14, 3, '~'))
    local _, p4 = canvas:add(Point:new(nil, 2, 2, '@'))
    local _, p5 = canvas:add(Point:new(nil, 3, 3, '#'))
    local _, p6 = canvas:add(Point:new(nil, 4, 4, '%'))
    ---@diagnostic disable-next-line: return-type-mismatch
    return canvas, r1, l2, l3, p4, p5, p6
  end

  it("prepare_canvas", function()
    local canvas = prepare_canvas()
    local exp = {
      '+=============+ ',
      '|@     -------| ',
      '| #   ~~~~~~~~| ',
      '|  %          | ',
      '+=============+ '
    }

    assert.same(exp, canvas:draw():toLines())
    exp = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp, get_elements_readable_list(canvas))
  end)

  it("elements_to_code + build_edit_elements_patch", function()
    local c = prepare_canvas()
    local elms, idxs = c:get_elements_by_index_range(4, 6)

    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    local exp_code = {
      "A: {_class='env.draw.Point',x=2,y=2,color='@'}",
      "B: {_class='env.draw.Point',x=3,y=3,color='#'}",
      "C: {_class='env.draw.Point',x=4,y=4,color='%'}"
    }
    assert.same(exp_code, lines_with_code)
    local exp_mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    assert.same(exp_mappings, mappings)

    local ok, patch = M.build_edit_elements_patch(lines_with_code, mappings)
    assert.same(true, ok)
    local exp = {
      'idx:(4 -> 4) 2:2 color:@',
      'idx:(5 -> 5) 3:3 color:#',
      'idx:(6 -> 6) 4:4 color:%'
    }
    assert.same(exp, p2s(patch))
  end)


  it("build_edit_elements_patch without changing", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "@",x = 2,y = 2}',
      'B: {_class = "env.draw.Point",color = "#",x = 3,y = 3}',
      'C: {_class = "env.draw.Point",color = "%",x = 4,y = 4}'
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 2:2 color:@',
      'idx:(5 -> 5) 3:3 color:#',
      'idx:(6 -> 6) 4:4 color:%'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 0, updated = 0, chparts = 0 }, ret)

    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch edit props", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "@",x = 5,y = 2}',
      'B: {_class = "env.draw.Point",color = "#",x = 6,y = 3}',
      'C: {_class = "env.draw.Point",color = "%",x = 7,y = 4}'
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 5:2 color:@',
      'idx:(5 -> 5) 6:3 color:#',
      'idx:(6 -> 6) 7:4 color:%'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    local exp = { chparts = 3, deleted = 0, updated = 3, swapped = 0 }
    assert.same(exp, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 5:2 color: @',
      '  5  Point 6:3 color: #',
      '  6  Point 7:4 color: %'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch swap order", function()
    local code = {
      'B: {_class = "env.draw.Point",color = "#",x = 3,y = 3}',
      'C: {_class = "env.draw.Point",color = "%",x = 4,y = 4}',
      'A: {_class = "env.draw.Point",color = "@",x = 2,y = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(5 -> 4) 3:3 color:#',
      'idx:(6 -> 5) 4:4 color:%',
      'idx:(4 -> 6) 2:2 color:@'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 3, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 3:3 color: #',
      '  5  Point 4:4 color: %',
      '  6  Point 2:2 color: @'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch delete from top of part", function()
    local code = {
      'A: {_class = "env.draw.Point",color = "@",x = 2,y = 2}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 4) 2:2 color:@',
      'idx:(6 -> 6) DELETE',
      'idx:(5 -> 5) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 2, swapped = 0, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch delete from bottom of part", function()
    local code = {
      'C: {_class = "env.draw.Point",color = "%",x = 4,y = 4}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 6, A = 4, B = 5 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(6 -> 4) 4:4 color:%',
      'idx:(4 -> 4) DELETE',
      'idx:(5 -> 5) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 2, swapped = 1, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 4:4 color: %'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  -----------------------------------------------------------------------------

  -- based on IEditable.fmtSerialized and Container.fmtSerialized +
  -- opts.handlers[key] for table values(See Container and Entity)

  it("elements_to_code one_element", function()
    local c = prepare_canvas()

    local elms, idxs = c:get_elements_by_index_range(2, 2)
    assert.same({ 2 }, idxs)

    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    local exp_code = {
      'classes = ',
      '{',
      "  [1] = 'oop.Object',",
      "  [2] = 'env.draw.Line',",
      '},',
      'element = ',
      "{_clsid=2,x1=8,y1=2,x2=14,y2=2,color='-'}"
    }
    assert.same(exp_code, lines_with_code)
    assert.same({ single = true, index = 2 }, mappings)
  end)

  it("elements_to_code one_element container", function()
    local canvas = Canvas:new(nil, 5, 16)
    local container = Container:new(nil, 1, 1, 10, 10)
    container:add(Line:new(nil, 1, 2, 11, 2, '-'))
    container:add(Line:new(nil, 2, 2, 12, 2, '='))

    canvas:add(container)

    local elms, idxs = canvas:get_elements_by_index_range(1, 1)
    assert.same({ 1 }, idxs)

    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    local exp_code = {
      'classes = ',
      '{',
      "  [1] = 'oop.Object',",
      "  [2] = 'env.draw.Line',",
      "  [3] = 'env.draw.Container',",
      '},',
      'element = ',
      '{',
      '  _clsid=3,x1=1,y1=1,x2=10,y2=10,',
      '  inv = {',
      "    {_clsid=2,x1=1,y1=2,x2=11,y2=2,color='-'},",
      "    {_clsid=2,x1=2,y1=2,x2=12,y2=2,color='='},",
      '  }',
      '}'
    }
    assert.same(exp_code, lines_with_code)
    assert.same({ single = true, index = 1 }, mappings)
  end)

  it("elements_to_code + build_edit_elements_patch", function()
    local c = prepare_canvas()

    local elms, idxs = c:get_elements_by_index_range(2, 4)
    assert.same({ 4, 3, 2 }, idxs)

    local lines_with_code, mappings = M.elements_to_code(elms, idxs)
    local exp_code = {
      "A: {_class='env.draw.Line',x1=8,y1=2,x2=14,y2=2,color='-'}",
      "B: {_class='env.draw.Line',x1=7,y1=3,x2=14,y2=3,color='~'}",
      "C: {_class='env.draw.Point',x=2,y=2,color='@'}"
    }
    assert.same(exp_code, lines_with_code)
    local exp_mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 4, A = 2, B = 3 }
    }
    assert.same(exp_mappings, mappings)

    local ok, patch = M.build_edit_elements_patch(lines_with_code, mappings)
    assert.same(true, ok)
    local exp = {
      'idx:(2 -> 2) 8:2 14:2 color:-',
      'idx:(3 -> 3) 7:3 14:3 color:~',
      'idx:(4 -> 4) 2:2 color:@'
    }
    assert.same(exp, p2s(patch))
  end)


  it("build_edit_elements_patch unchanged", function()
    local code = {
      'A: {_class = "env.draw.Line",color = "-",x1 = 8,x2 = 14,y1 = 2,y2 = 2}',
      'B: {_class = "env.draw.Line",color = "~",x1 = 7,x2 = 14,y1 = 3,y2 = 3}',
      'C: {_class = "env.draw.Point",color = "@",x = 2,y = 2}'
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 4, A = 2, B = 3 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(2 -> 2) 8:2 14:2 color:-',
      'idx:(3 -> 3) 7:3 14:3 color:~',
      'idx:(4 -> 4) 2:2 color:@'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 0, updated = 0, chparts = 0 }, ret)

    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))
  end)


  it("build_edit_elements_patch swap", function()
    local code = {
      'C: {_class = "env.draw.Point",color = "@",x = 2,y = 2}',
      'A: {_class = "env.draw.Line",color = "-",x1 = 8,x2 = 14,y1 = 2,y2 = 2}',
      'B: {_class = "env.draw.Line",color = "~",x1 = 7,x2 = 14,y1 = 3,y2 = 3}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 4, A = 2, B = 3 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(4 -> 2) 2:2 color:@',
      'idx:(2 -> 3) 8:2 14:2 color:-',
      'idx:(3 -> 4) 7:3 14:3 color:~'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 0, swapped = 3, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Point 2:2 color: @',
      '  3  Line (8:2 14:2) color: -',
      '  4  Line (7:3 14:3) color: ~',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)

  it("build_edit_elements_patch delete A C", function()
    local code = {
      'B: {_class = "env.draw.Line",color = "~",x1 = 7,x2 = 14,y1 = 3,y2 = 3}',
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 4, A = 2, B = 3 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(3 -> 2) 7:3 14:3 color:~',
      'idx:(2 -> 2) DELETE',
      'idx:(4 -> 4) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 2, swapped = 1, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (7:3 14:3) color: ~',
      '  3  Point 3:3 color: #',
      '  4  Point 4:4 color: %'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)


  it("build_edit_elements_patch delete A C", function()
    local code = {
    }
    local mappings = {
      lnum2letter = { 'A', 'B', 'C' },
      letter2idx = { C = 4, A = 2, B = 3 }
    }
    local ok, patch = M.build_edit_elements_patch(code, mappings)
    assert.same(true, ok)
    local exp_patch = {
      'idx:(2 -> 2) DELETE',
      'idx:(4 -> 4) DELETE',
      'idx:(3 -> 3) DELETE'
    }
    assert.same(exp_patch, p2s(patch)) ---@cast patch table

    local c = prepare_canvas()
    local exp_before_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Line (8:2 14:2) color: -',
      '  3  Line (7:3 14:3) color: ~',
      '  4  Point 2:2 color: @',
      '  5  Point 3:3 color: #',
      '  6  Point 4:4 color: %'
    }
    assert.same(exp_before_patch, Canvas.get_elements_readable_list(c))

    local ok2, ret = c:apply_patch(patch)
    assert.same(true, ok2)
    assert.same({ deleted = 3, swapped = 0, updated = 0, chparts = 0 }, ret)

    local exp_after_patch = {
      '  1  Rectangle (1:1 15:5) (15x5) bg: ""',
      '  2  Point 3:3 color: #',
      '  3  Point 4:4 color: %'
    }
    assert.same(exp_after_patch, Canvas.get_elements_readable_list(c))
  end)
end)

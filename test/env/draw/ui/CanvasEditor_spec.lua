-- 01-04-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local log = require('alogger')
local Canvas = require("env.draw.Canvas");
local Rectangle = require("env.draw.Rectangle");
local Selection = require("env.draw.ui.Selection");
local state = require("env.draw.state.canvas");
local M = require("env.draw.ui.CanvasEditor");
local IPH = require("env.draw.ui.ItemPlaceHolder");
local H = require("env.draw.test_helper")

local v2s, E = tostring, {}
local get_elements_readable_list = Canvas.get_elements_readable_list

describe("env.ui.CanvasEditor", function()
  after_each(function()
    state.clear_all_ui_items()
    state.clear_clipboard()
    state.clear_deleted_history()
    log.fast_off()
    -- nvim:clear_state()
  end)

  it("__tostring", function()
    local c = Canvas:new(nil, 4, 8)
    local box = IPH.wrap(c, 1, 2, 3)
    local exp = 'ItemPH lnum:2:6 col:3:11 (env.draw.Canvas) Canvas w:8 h:4 objects:0'
    assert.same(exp, tostring(box))
  end)


  it("add to item - Rectangle", function()
    local height, width = 4, 12
    local c = Canvas:new(nil, height, width)

    local bufnr, lnum, col = 1, 10, 0 -- abs coords in buffer
    local box = IPH.wrap(c, bufnr, lnum, col)

    local rw, rh = 3, 2
    -- add to item(canvas)

    local r = box:add(Rectangle:new(nil, 0, 0, rw, rh), lnum, col)
    assert.same('Rectangle (1:1 4:3) (4x3) bg: ""', tostring(r))

    local canvas = Canvas.cast(box.item)
    assert.same(r, canvas.objects[1])
    local exp = {
      '+--+        ',
      '|  |        ',
      '+--+        ',
      '            '
    }
    assert.same(exp, canvas:draw():toLines())
  end)


  it("add to item", function()
    local height, width = 4, 12
    local c = Canvas:new(nil, height, width)

    local bufnr, lnum, col = 1, 10, 0
    local box = IPH.wrap(c, bufnr, lnum, col)

    local rw, rh = 3, 2
    local r = box:add(Rectangle:new(nil, 0, 0, rw, rh), lnum + 1, col + 4)

    -- same as bot.item:add(Rectangle:new(x1,y1,rw, rh))
    local exp = 'Rectangle (5:2 8:4) (4x3) bg: ""'
    assert.same(exp, tostring(r))
    -- 123456789012
    local exp2 = {
      '            ',
      '    +--+    ',
      '    |  |    ',
      '    +--+    '
    }
    assert.same(exp2, Canvas.cast(box.item):draw():toLines())
  end)

  -- used to cut object
  it("remove from Canvas", function()
    local height, width = 4, 12
    local c = Canvas:new(nil, height, width)

    local bufnr, lnum, col = 1, 10, 0
    local box = IPH.wrap(c, bufnr, lnum, col)

    local rw, rh = 3, 2
    local r = box:add(Rectangle:new(nil, 0, 0, rw, rh), lnum + 1, col + 4)
    assert.same('Rectangle (5:2 8:4) (4x3) bg: ""', tostring(r))

    assert.same(r, box:remove(r))
    -- ensure remove inCanvasCoords and keep own props
    assert.same('Rectangle (0:0 3:2) (4x3) bg: ""', tostring(r))
  end)
end)


describe("env.ui.CanvasEditor elements in canvas by index", function()
  after_each(function()
    state.clear_all_ui_items()
    state.clear_clipboard()
    state.clear_deleted_history()
    log.fast_off()
    H.restore_print()
  end)


  it("delete_element from canvas by index (cmd)", function()
    local f = get_elements_readable_list
    local c = Canvas:new(nil, 4, 12)
    c:add(Rectangle:new(nil, 0, 0, 1, 1))
    local _, r2 = c:add(Rectangle:new(nil, 0, 0, 2, 2))
    c:add(Rectangle:new(nil, 0, 0, 3, 3))
    local exp = {
      '  1  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""',
      '  3  Rectangle (0:0 3:3) (4x4) bg: ""',
      'Page: #1/1[3]'
    }
    assert.same(exp, f(c, 1, 3)) -- page 1
    local box = IPH.wrap(c, 1, 0, 0)
    local st, msg = M.delete_element_by_idx(box, 2)
    local exp2 = 'true|x [0:0 2:2][0:0] {Rectangle(3x3)(0:0)}'
    assert.same(exp2, tostring(st) .. "|" .. tostring(msg))
    local exp3 = {
      '  1  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  2  Rectangle (0:0 3:3) (4x4) bg: ""'
    }
    assert.same(exp3, f(c, 1, 3)) -- page 1
    -- the deleted element is placed in a special list for
    -- the possibility of recovery (undo)
    local _, dfcb = state.get_clipboard()
    local selection = dfcb[1]
    assert.is_not_nil(selection)

    assert.no_error(function() Selection.cast(selection) end)
    ---@cast selection env.draw.ui.Selection
    assert.same({ r2 }, selection:elements())
  end)

  it("delete_element_by_idx", function()
    local c = Canvas:new(nil, 4, 16)
    local box = IPH.wrap(c, 1, 0, 0)
    c:add(Rectangle:new(nil, 5, 1, 12, 4))

    local exp = {
      '    +------+    ',
      '    |      |    ',
      '    |      |    ',
      '    +------+    '
    }
    assert.same(exp, c:draw():toLines())
    exp = { '  1  Rectangle (5:1 12:4) (8x4) bg: ""' }
    assert.same(exp, get_elements_readable_list(c, 1))

    local ok, ret = M.delete_element_by_idx(box, 1)

    assert.same(true, ok)
    assert.same('x [5:1 12:4][0:0] {Rectangle(8x4)(0:0)}', tostring(ret))

    local _, ret2 = M.restore_deleted(box, 1, 0, 0)
    assert.same('1 elements restored Rectangle(8x4)(5:1)', tostring(ret2))

    local exp2 = {
      '    +------+    ',
      '    |      |    ',
      '    |      |    ',
      '    +------+    '
    }
    assert.same(exp2, c:draw():toLines())
  end)


  it("cut_from_canvas and add_to_canvas", function()
    local c = Canvas:new(nil, 4, 16)
    local box = IPH.wrap(c, 1, 0, 0)
    local _, r = c:add(Rectangle:new(nil, 5, 1, 12, 4))
    assert.same({ r }, c.objects)

    local exp = {
      '    +------+    ',
      '    |      |    ',
      '    |      |    ',
      '    +------+    '
    }
    assert.same(exp, c:draw():toLines())
    exp = { '  1  Rectangle (5:1 12:4) (8x4) bg: ""' }
    assert.same(exp, get_elements_readable_list(c, 1))

    -- prepare: cut the rectangle from the canvas
    local objects, lnum, col, mkcopy = { r }, 1, 8, false
    local _, sel = M.cut_from_canvas(box, objects, lnum, col)
    local exp1 = 'x [5:1 12:4][4:1] {Rectangle(8x4)(0:0)}'
    assert.same(exp1, tostring(sel)) ---@cast sel env.draw.ui.Selection
    assert.same({}, c.objects)

    -- workload: past the rectangle back to the canvas
    --  move left by two chars
    assert.same(1, M.add_to_canvas(box, lnum, col - 2, sel, mkcopy))

    local exp2 = { '  1  Rectangle (3:1 10:4) (8x4) bg: ""' }
    assert.same(exp2, get_elements_readable_list(c, 1))

    local exp3 = {
      '  +------+      ',
      '  |      |      ',
      '  |      |      ',
      '  +------+      '
    }
    assert.same(exp3, c:draw():toLines())
  end)

  it("edit_element_order", function()
    local c = Canvas:new(nil, 4, 12)
    c:add(Rectangle:new(nil, 0, 0, 1, 1))
    c:add(Rectangle:new(nil, 0, 0, 2, 2))
    c:add(Rectangle:new(nil, 0, 0, 3, 3))
    c:add(Rectangle:new(nil, 0, 0, 4, 4))
    c:add(Rectangle:new(nil, 0, 0, 5, 5))
    local elements, indexes = c:get_objects_at(1, 1, 12, 4)
    local msg, def = M.cli_elements_order_prompt(elements, indexes)
    local expt = ' A:Rectangle(0:0)(6x6); B:Rectangle(0:0)(5x5); '
        .. 'C:Rectangle(0:0)(4x4); D:Rectangle(0:0)(3x3); E:Rectangle(0:0)(2x2);'
    assert.same(expt, msg)
    local exp_def = ' A: 5 B: 4 C: 3 D: 2 E: 1'
    assert.same(exp_def, def)

    -- parse unchanged input
    local updated, patch_order = M.cli_elements_order_parse_input(indexes, def)
    assert.same(false, updated)
    assert.same({}, patch_order)

    -- ok
    local input = ' A: 4 B: 5 C: 3 D: 2 E: 1'
    --                 ^----^
    local up, patch = M.cli_elements_order_parse_input(indexes, input)
    local exp = { { old = 5, new = 4 }, { old = 4, new = 5 } }
    assert.same(true, up)
    assert.same(exp, patch)

    -- error one index for two objects
    input = ' A: 4 B: 4 C: 3 D: 2 E: 1'
    --           ^----^
    up, patch = M.cli_elements_order_parse_input(indexes, input)
    assert.same(false, up)
    assert.same('index 4 already in use', patch)

    input = ' A: 5 B: 1 C: 1 D: 2 E: 1'
    --                ^----^
    up, patch = M.cli_elements_order_parse_input(indexes, input)
    assert.same(false, up)
    assert.same('index 1 already in use', patch)

    input = ' A: 8 B: 4 C: 3 D: 2 E: 1'
    up, patch = M.cli_elements_order_parse_input(indexes, input)
    assert.same(false, up)
    assert.same('8 too big! valid range: 1 - 5', patch)

    input = ' A: 0 B: 4 C: 3 D: 2 E: 1'
    up, patch = M.cli_elements_order_parse_input(indexes, input)
    assert.same(false, up)
    assert.same('0 too small! valid range: 1 - 5', patch)
  end)

  it("swapElementsOrder", function()
    local c = Canvas:new(nil, 4, 12)
    c:add(Rectangle:new(nil, 0, 0, 1, 1)) -- 1
    c:add(Rectangle:new(nil, 0, 0, 2, 2)) -- 2
    c:add(Rectangle:new(nil, 0, 0, 3, 3)) -- 3
    c:add(Rectangle:new(nil, 0, 0, 4, 4)) -- 4
    c:add(Rectangle:new(nil, 0, 0, 5, 5)) -- 5
    local elements, indexes = c:get_objects_at(1, 1, 12, 4)
    local msg, def = M.cli_elements_order_prompt(elements, indexes)
    local expt = ' A:Rectangle(0:0)(6x6); B:Rectangle(0:0)(5x5); '
        .. 'C:Rectangle(0:0)(4x4); D:Rectangle(0:0)(3x3); E:Rectangle(0:0)(2x2);'
    assert.same(expt, msg)
    local exp_def = ' A: 5 B: 4 C: 3 D: 2 E: 1'
    assert.same(exp_def, def)

    -- parse unchanged input
    local updated, patch_order = M.cli_elements_order_parse_input(indexes, def)
    assert.same(false, updated)
    assert.same({}, patch_order)

    -- ok
    local input = ' A: 4 B: 5 C: 1 D: 2 E: 3'
    --                 ^----^    ^---------^
    local up, patch = M.cli_elements_order_parse_input(indexes, input)
    local exp = {
      { old = 5, new = 4 },
      { old = 4, new = 5 },
      { old = 3, new = 1 },
      { old = 1, new = 3 }
    }
    assert.same(true, up)
    assert.same(exp, patch) ---@cast patch table
    assert.same(2, c:swapElementsOrder(patch))
    local exps = {
      '  1  Rectangle (0:0 3:3) (4x4) bg: ""',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""',
      '  3  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  4  Rectangle (0:0 5:5) (6x6) bg: ""',
      '  5  Rectangle (0:0 4:4) (5x5) bg: ""'
    }
    assert.same(exps, get_elements_readable_list(c, 1))
  end)
  -- << env.ui.CanvasEditor elements in canvas by index
end)

describe("env.ui.CanvasEditor do_undo_paste", function()
  after_each(function()
    state.clear_all_ui_items()
    state.clear_clipboard()
    state.clear_deleted_history()
    log.fast_off()
    H.restore_print()
  end)

  it("do_undo_paste", function()
    H.wrap_print()
    local c = Canvas:new(nil, 4, 12)
    local box = IPH.wrap(c, 1, 0, 0)
    local _, r1 = c:add(Rectangle:new(nil, 0, 0, 1, 1)) -- 1
    local _, r2 = c:add(Rectangle:new(nil, 0, 0, 2, 2))
    local _, r3 = c:add(Rectangle:new(nil, 0, 0, 3, 3))

    local elements, indexes = { r1, r2, r3 }, {}
    local lnum, col, lnum2, col2 = 1, 1, 3, 3

    ---@diagnostic disable-next-line: unused-local
    M.set_get_ctx(function(find_elm, limit)
      return box, lnum, col, lnum2, col2, elements, indexes
    end)
    M.set_close_visual_mode(function() end)
    M.set_is_visual_mode(function() return false end)

    -- do_cut
    local n = M.do_cut()
    assert.same(3, n)
    local xsel = state.clipboard_get_entry(1, true)
    local exp = 'x [0:0 3:3][2:2] {Rectangle(2x2)(0:0) Rectangle(3x3)(0:0) Rectangle(4x4)(0:0)}'
    assert.same(exp, tostring(xsel))

    -- do_paste
    local psel = M.do_paste()
    local exp_sel = 'x [0:0 3:3][2:2] {Rectangle(2x2)(0:0) Rectangle(3x3)(0:0) Rectangle(4x4)(0:0)}'
    assert.same(exp_sel, v2s(psel)) ---@cast psel table
    local sel0, copy = state.getLastSelection()
    assert.same(false, copy)
    assert.is_table(sel0)

    local exp_list = {
      '  1  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""',
      '  3  Rectangle (0:0 3:3) (4x4) bg: ""'
    }
    assert.same(exp_list, c:get_elements_readable_list())
    local exp_elm1 = 'Rectangle (0:0 1:1) (2x2) bg: ""'
    assert.same(exp_elm1, v2s((psel:elements() or E)[1]))
    assert.same(exp_elm1, v2s((c.objects)[1]))
    assert.equal(c.objects[1], psel:elements()[1]) -- same instance

    -- do_undo_paste
    M.do_undo_paste(box)

    --
    local exp_output = {
      ' cutted 3',
      ' 3 elements pasted',
      ' 3 elements undo back to clipboard'
    }
    assert.same(exp_output, H.output())
  end)
end)

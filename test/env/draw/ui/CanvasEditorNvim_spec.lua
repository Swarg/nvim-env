-- 03-04-2024 @author Swarg

require("busted.runner")()
local assert = require("luassert")

_G.TEST = true
local NVim = require("stub.vim.NVim")
local nvim = NVim:new() ---@type stub.vim.NVim
local log = require('alogger')
-- local sys = nvim:get_os()

local Canvas = require("env.draw.Canvas");
local CE = require("env.draw.ui.CanvasEditor");
local IPH = require("env.draw.ui.ItemPlaceHolder");
-- local IPH = require 'env.draw.ui.ItemPlaceHolder'
local ubuf = require("env.draw.utils.buf");
local state = require("env.draw.state.canvas");
local H = require("env.draw.test_helper")
local CH = require("env.draw.command.helper")
local Rectangle = require("env.draw.Rectangle");
local Container = require("env.draw.Container")

local ts = tostring
local get_editor_coords = ubuf.get_editor_coords
local nvim_find_syntax_block_range = ubuf.nvim_find_syntax_block_range
local get_ctx = state.get_ctx


describe("env.command.draw", function()
  after_each(function()
    state.clear_all()
    nvim:clear_state()
    log.fast_off()
    H.restore_print()
    -- print(H.output())
    -- require 'alogger'.fast_setup()
  end)

  --
  -- Notes:
  --   - vim.api.nvim_win_get_cursor  - gives lnum stats from 1
  --   - vim.api.nvim_buf_get_lines   - take lnum from 0
  --
  --   - cursor on top-left gives: box.lnum: 0 0  nvim.api.win_cursor.lnum: 1 0
  --
  -- box.lnum starts from 0, nvim cursor pos from 1
  it("nvim_find_block_range api.nvim_win_cursor and box.lnum", function()
    local lnum, col, lines = 2, 0, { -- same coords as api.nvim_win_get_cursor
      --            taken  given
      --            nvim   showed-lnum
      '',           -- 0     1
      '```xcanvas', -- 1     2 << cursor here
      '```',        -- 2     3
      '' }          -- 3     4
    local bufnr = nvim:new_buf(lines, lnum, col)

    local cpos = vim.api.nvim_win_get_cursor(0)
    assert.same({ 2, 0 }, cpos) -- lnum:2, col:0

    -- both coords starts from 0:0
    local bn, ln, cn = get_editor_coords(0, 0)
    assert.same('1|1|0', tostring(bn) .. "|" .. tostring(ln) .. "|" .. tostring(cn))

    -- works with nvim_win_get_cursor coords
    local ls, le = nvim_find_syntax_block_range(bufnr, lnum) --
    assert.same('2|3', tostring(ls) .. "|" .. tostring(le))
    local exp = { '', '```xcanvas', '```', '' }
    assert.same(exp, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

    -- take lnum from 0 so -1
    local exp2 = { '```xcanvas' }
    assert.same(exp2, vim.api.nvim_buf_get_lines(bufnr, lnum - 1, lnum, false))

    -- try to insert into found syntax block
    local insertion = { '1', '2', '3', '4' }
    vim.api.nvim_buf_set_lines(bufnr, assert(ls), assert(le) - 1, true, insertion)
    local exp_all = { '', '```xcanvas', '1', '2', '3', '4', '```', '' }
    assert.same(exp_all, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))
  end)

  --
  it("nvim_find_block_range no end", function()
    local lnum, col, lines = 2, 0, {
      '',           -- 0     1
      '```xcanvas', -- 1     2 << cursor here
      '',           -- 2     3
      '' }          -- 3     4
    local bufnr = nvim:new_buf(lines, lnum, col)

    local ls, le, s = nvim_find_syntax_block_range(bufnr, lnum)
    assert.same('2|nil|xcanvas', ts(ls) .. "|" .. ts(le) .. '|' .. ts(s))
  end)

  -- do not treat the block below the cursor as a block for placement
  it("nvim_find_block_range no begin", function()
    local lnum, col, lines = 2, 0, {
      '',        -- 0     1
      'xcanvas', -- 1     2 << cursor here
      '```',     -- 2     3 block below
      '```',     -- 3     4
      '' }
    local bufnr = nvim:new_buf(lines, lnum, col)

    local ls, le, s = nvim_find_syntax_block_range(bufnr, lnum)
    assert.same('nil|nil|nil', ts(ls) .. "|" .. ts(le) .. '|' .. ts(s))
  end)

  -- does not reach the beginning of the block due to the limit
  it("nvim_find_block_range limit up ", function()
    local lnum, col, lines = 7, 0, {
      '',           -- 0     1
      '```xcanvas', -- 1     2
      '',           -- 2     3
      '',           -- 3     4
      '',           -- 4     5
      '',           -- 5     6
      '',           -- 6     7 << cursor here
      '```',        -- 7     8
      '' }
    local bufnr = nvim:new_buf(lines, lnum, col)

    local ls, le, s = nvim_find_syntax_block_range(bufnr, lnum, 4) -- limit:4
    assert.same('nil|nil|nil', ts(ls) .. "|" .. ts(le) .. "|" .. ts(s))

    -- passed
    ls, le, s = nvim_find_syntax_block_range(bufnr, lnum, 20)
    assert.same('2|8|xcanvas', ts(ls) .. "|" .. ts(le) .. "|" .. ts(s))
  end)


  -- does not reach the end of the block due to the limit
  it("nvim_find_block_range limit down", function()
    local lnum, col, lines = 2, 0, {
      '',           -- 0     1
      '```xcanvas', -- 1     2 << cursor
      '',           -- 2     3
      '',           -- 3     4
      '',           -- 4     5
      '',           -- 5     6 -- limit here
      '',           -- 6     7
      '```',        -- 7     8
      '' }
    local bufnr = nvim:new_buf(lines, lnum, col)

    local ls, le, s = nvim_find_syntax_block_range(bufnr, lnum, 4) -- limit:4
    assert.same('2|nil|xcanvas', ts(ls) .. "|" .. ts(le) .. "|" .. ts(s))

    -- passed
    ls, le, s = nvim_find_syntax_block_range(bufnr, lnum, 20)
    assert.same('2|8|xcanvas', ts(ls) .. "|" .. ts(le) .. "|" .. ts(s))
  end)

  it("edit_element", function()
    local f = function(elm)
      local ok, ret = CE.edit_element(elm)
      return tostring(ok) .. "|" .. tostring(ret)
    end
    nvim:add_ui_input('123 \\q')
    -- true means the builtin command succesfuly performed
    assert.same('false|\\q', f(Rectangle:new(nil, 1, 1, 2, 2)))
  end)

  it("edit_element cli", function()
    H.wrap_print()
    local c = Canvas:new(nil, 4, 12)
    local box = IPH.wrap(c, 1, 0, 0)
    local _, r1 = c:add(Rectangle:new(nil, 0, 0, 1, 1))
    local _, _ = c:add(Rectangle:new(nil, 0, 0, 2, 2))
    local exp_list = {
      '  1  Rectangle (0:0 1:1) (2x2) bg: ""',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""'
    }
    assert.same(exp_list, c:get_elements_readable_list())

    -- 'x1 y1 x2 y2 hline vline corners bg w h',
    nvim:add_ui_input('1 1 8 8 - | ++++ "X" 2 2') -- user input to edit rect

    ---@diagnostic disable-next-line: param-type-mismatch
    CE.edit_element(r1, 1, 2, box, 1, 1)

    local exp_list2 = {
      '  1  Rectangle (1:1 8:8) (8x8) bg: X',
      '  2  Rectangle (0:0 2:2) (3x3) bg: ""'
    }
    assert.same(exp_list2, c:get_elements_readable_list())
  end)

  it("edit_element inside container", function()
    H.wrap_print()
    require 'alogger'.fast_setup(nil, 0)
    local c = Canvas:new(nil, 4, 12)
    local box = IPH.wrap(c, 1, 0, 0)

    local inv = {
      Rectangle:new(nil, 1, 1, 4, 2),
      Rectangle:new(nil, 4, 4, 8, 4)
    }
    local _, container = c:add(Container:new(nil, 1, 1, 12, 4, inv))
    local exp_list = { '  1  Container (1:1 12:4) (12x4) elms: 2' }
    assert.same(exp_list, c:get_elements_readable_list())
    local exp = [[
Container (1:1 12:4) (12x4) elms: 2 inv:
 1: Rectangle (1:1 4:2) (4x2) bg: ""
 2: Rectangle (4:4 8:4) (5x1) bg: ""
]]
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same(exp, Container.__tostring(container, true))

    -- 'x1 y1 x2 y2 hline vline corners bg w h',
    nvim:add_ui_input('1 1 4 2 - | ++++ "" 4 2') -- user input to edit rect

    ---@diagnostic disable-next-line: param-type-mismatch
    CE.edit_element(container, 1, 1, box, --[[coords: x,y = ]] 1, 1)

    local exp2 = [[
Container (1:1 12:4) (12x4) elms: 2 inv:
 1: Rectangle (1:1 4:2) (4x2) bg: ""
 2: Rectangle (4:4 8:4) (5x1) bg: ""
]]
    ---@diagnostic disable-next-line: param-type-mismatch
    assert.same(exp2, Container.__tostring(container, true))
  end)
  --

  local function prepare_canvas_editor_buff()
    local lines0, lnum0, col0 = { '' }, 1, 0
    local bufnr0 = nvim:new_buf(lines0, lnum0, col0)
    local canvas = Canvas:new(nil, 5, 12)
    local cebufnr = CH.openInCanvasEditor(true, canvas, bufnr0, lnum0, col0, nil)
    return cebufnr, canvas
  end

  it("new CanvasEditor buffer + get_ctx", function()
    local cebufnr, canvas = prepare_canvas_editor_buff()
    local _, rect = canvas:add(Rectangle:new(nil, 2, 1, 10, 4))
    assert.same(2, cebufnr)

    H.wrap_print() -- be quiet

    local exp = {
      ' +-------+  ',
      ' |       |  ',
      ' |       |  ',
      ' +-------+  ',
      '            '
    }
    assert.same(exp, canvas:draw():toLines())

    local box, lnum, col, lnum2, col2, elements = get_ctx(true, 10)
    assert.is_table(box)
    assert.same(2, box.bufnr)
    assert.same('2|6', ts(lnum) .. "|" .. ts(col))
    assert.same('2|6', ts(lnum2) .. "|" .. ts(col2))
    assert.same({ rect }, box.item.objects)
    assert.same('Rectangle (2:1 10:4) (9x4) bg: ""', tostring(rect))
    assert.same({ rect }, elements)
  end)

  -- experimental unfinished goal was to find problems when selecting an area
  it("CanvasEditor: do_cut do_paste ", function()
    H.wrap_print() -- be quiet

    local cebufnr, canvas = prepare_canvas_editor_buff()
    local _, rect = canvas:add(Rectangle:new(nil, 2, 1, 10, 4))
    local cursor_pos = vim.api.nvim_win_get_cursor(0)
    assert.same({ 3, 6 }, cursor_pos) -- lnum:col
    vim.api.nvim_win_set_cursor(0, { 2, 4 })
    assert.same({ 2, 4 }, vim.api.nvim_win_get_cursor(0))

    -- add point
    CE.do_mark_pos_start()
    CE.do_mark_pos_start()
    local exp_with_point = {
      ' +-------+  ',
      ' |  +    |  ',
      ' |       |  ',
      ' +-------+  ',
      '            '
    }
    assert.same(exp_with_point, canvas:draw():toLines())
    local exp_elements = {
      '  1  Rectangle (2:1 10:4) (9x4) bg: ""',
      '  2  Point 5:2 color: +'
    }
    assert.same(exp_elements, Canvas.get_elements_readable_list(canvas))
    local point = canvas.objects[2]
    assert.same('Point 5:2 color: +', tostring(point))

    vim.api.nvim_win_set_cursor(0, { 2, 3 }) -- move left to one char

    local box, lnum, col, lnum2, col2, objects = get_ctx(true, 10)
    assert.is_table(box)
    assert.same(2, box.bufnr)
    assert.same('1|3', ts(lnum) .. "|" .. ts(col))
    assert.same('1|3', ts(lnum2) .. "|" .. ts(col2))
    assert.same({ rect, point }, box.item.objects)
    assert.same('Rectangle (2:1 10:4) (9x4) bg: ""', tostring(rect))
    assert.same({ rect }, objects)

    -- require 'alogger'.fast_setup(nil, 0)
    assert.same(1, CE.do_cut())
    local exp_after_cut = {
      '            ',
      '    +       ',
      '            ',
      '            ',
      '            '
    }
    assert.same(exp_after_cut, canvas:draw():toLines())

    local selection = CE.do_paste()
    assert.same('x [2:1 10:4][2:1] {Rectangle(9x4)(2:1)}', tostring(selection))
    local exp_after_paste = {
      ' +-------+  ',
      ' |  +    |  ',
      ' |       |  ',
      ' +-------+  ',
      '            '
    }
    assert.same(exp_after_paste, canvas:draw():toLines())

    -- select range with rect (V-block  -- to select point + rect
    nvim.api.nvim_set_mode0(0, cebufnr, 'v')

    vim.fn.setpos('v', { 0, 1, 2, 0 })  -- 2-row 3-col
    vim.fn.setpos('.', { 0, 5, 11, 0 }) -- Note: row starts from 1, col fr 0

    _, lnum, col, lnum2, col2, objects = get_ctx(true, 10)
    assert.same('4|10', ts(lnum) .. "|" .. ts(col))  -- getpos('.') current pos
    assert.same('0|1', ts(lnum2) .. "|" .. ts(col2)) -- getpos('v') start
    local urect = canvas.objects[2]
    assert.same('Rectangle (2:1 10:4) (9x4) bg: ""', tostring(urect))
    assert.same({ urect, point }, objects)

    assert.same(2, CE.do_cut())

    local exp_elements2 = {}
    assert.same(exp_elements2, Canvas.get_elements_readable_list(canvas))

    local selection2 = CE.do_paste()
    local exp_sel2 = 'x [2:1 10:4][9:4] {Rectangle(9x4)(3:1) Point(1x1)(6:2)}'
    assert.same(exp_sel2, tostring(selection2))

    local exp_elements3 = {
      '  1  Rectangle (3:1 11:4) (9x4) bg: ""',
      '  2  Point 6:2 color: +'
    }
    assert.same(exp_elements3, Canvas.get_elements_readable_list(canvas))
    local exp_after_paste_2 = {
      '  +-------+ ',
      '  |  +    | ',
      '  |       | ',
      '  +-------+ ',
      '            '
    }
    assert.same(exp_after_paste_2, canvas:draw():toLines())
  end)
end)

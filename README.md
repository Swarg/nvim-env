# NVim Environment

Convenient environment for development and testing the code.<br>
Quick go to from source to test, and vice versa, run code pieces, tests, debugging.<br>
Show output of commands, and tests in the nvim buffer (q - for close buffer)<br>
Support the LSP, and provide some useful function without the LSP.

# Implemented features:

 ### Java:
   - recognizen build-tool (ant, maven, gradle, eclipse)
   - integration with jdtls, and dap (ide-like features with debugging)
   - run single test class from project
   - "jump(goto)" from source to test and vice versa (toggle source/test file)
   - quick generate simple maven project (from template)

 ### Lua:
   - quick jump(toggle) to pair-file: test/source
   - run function under the cursor in current opened file inside nvim
   - run single test-file of current lua-file (busted)
   - research packaging, nvim plugins, reload own plugin, etc
   - TODO: debugging

 ### Elixir:
   - TODO describe


## Install this plugin via packer.nvim
```lua
local packer = require 'packer'

return packer.startup(function(use)
 use {
    'https://gitlab.com/Swarg/nvim-env',
    -- luarocks dependencies for this plugin
    rocks = {
      { 'alogger', version = '0.5'},
      { 'dpring', version = '0.4'},
      { 'cmd4lua', version = '0.8'},
      { 'oop', version = '0.5'},
      { 'clieos', version = '0.6' },
      { 'db-env', version = '0.1'}, -- luasql.postgres luasql.mysql
      { 'ex-parser', version = '0.3'}, -- based on lpeg
      { 'www-auth', version = '0.1' },
      { 'lua-cjson', version = '2.1.0'},
      { 'lyaml', version = '6.2'},
      { 'xml2lua', version = '1.5'},
      { 'luasocket'},
      { 'luasec'}
      { 'stub', version = '0.10' }, -- testing_dep
    }
  }
end)
```


# Example How to Configure key mappings

```lua

keymap("n", "<F18>", "<cmd> EnvRunMainInFile <cr>", opts)      -- Shift-F6
keymap("n", "<F30>", "<cmd> EnvRunTestFile <cr>", opts)        -- Ctrl-F6
keymap("n", "<leader>ka", "<cmd>EnvRunTestFile <cr>", opts)    -- Ctrl-F6
keymap("n", "<F42>", "<cmd> EnvDebugTestFile <cr>", opts)      -- Ctrl-Shift-F6

-- "GoTo" test/source::
keymap("n", "<leader>gt", "<cmd>EnvToggleTestSourceJump <cr>", opts)

-- Attach to Already runned Debugger
keymap("n", "<leader>da", "<cmd>EnvAttachToDebugger <cr>", opts)

keymap("n", "<F6>", "<cmd>EnvRunProject <cr>", opts)           -- F6
keymap("n", "<F29>", "<cmd>EnvDebugPorject <cr>", opts)        -- Ctrl+F5
keymap("n", "<F47>", "<cmd>EnvFileNamesSaveOpened <cr>", opts) -- Ctrl+Shift+F11
keymap("n", "<F45>", "<cmd>EnvFileNamesOpenSaved <cr>", opts)  -- Ctrl+Shift+F9

keymap("n", "<leader>kr", "<CMD>EnvReload --clear <CR>", opts)
-- Execute line as vim command (Picked from cursor pos, already picked to mem)
-- for run functions via comments in source file:  -- lua require('sdf').func()
keymap("n", "<leader>kl", "<CMD>EnvLineExec current <CR>", opts)
keymap("n", "<leader>klm", "<CMD>EnvLineExec mem <CR>", opts)
keymap("n", "<leader>kll", "<CMD>EnvLineExec last <CR>", opts)
keymap("n", "<leader>klf", "<CMD>EnvLineExec forget <CR>", opts)

keymap("n", "<leader>ke", "<CMD>EnvCallFunc cmd <CR>", opts)     -- simple call
keymap("n", "<leader>kw", "<CMD>EnvCallFunc verbose <CR>", opts) -- with prints
keymap("n", "<leader>kmm", "<CMD>EnvCallFunc mem <CR>", opts)    -- mem for call
keymap("n", "<leader>kmx", "<CMD>EnvCallFunc clean <CR>", opts)  -- clear memed
keymap("n", "<leader>kmi", "<CMD>EnvCallFunc status <CR>", opts)

-- aka code-actions without any lsp
keymap("n", "<leader>i", "<CMD>EnvLineInsert <CR>", opts)
keymap("n", "<leader>lb", "<CMD>EnvLine fold <CR>", opts)
keymap("n", "<leader>lc", "<CMD>EnvLine concat <CR>", opts)

keymap("v", "<leader>z", "<CMD>EnvTranslate selected-text <CR>", opts)
keymap("n", "<leader>kz", "<CMD>EnvTranslate current-line<CR>", opts)
```


# Commands
  - EnvNew          -- create new stuff from templates [See](./doc/EnvNew.md)
  - EnvMake         -- call `make` with given arguments and return output to
                       new vim buffer

  - EnvProjectInfo  -- brief information about the currently open project

  - EnvRunProject EnvDebugProject -- Determine the Project (Root, BuildSystem)
    for current opened file and run it via builder system.

  - EnvRunTestFile EnvDebugTestFile <br>
    Determine by full filename this class|module name and run test for it

  - EnvRunMainInFile EnvDebugMainInFile -- TODO not implemented yet

  - EnvAttachToDebugger -- Attach to an already running external debugger

  - EnvToggleTestSourceJump - jump to source/test for current opened file

  - EnvGoToSource    -- class(module) Name of a opened or given file
                        or jump to source file from stacktrace line

  - EnvGotoAttachedBuf -- Open a buffer already attached to the current buffer.
    for quick navigation between file buffers:  src<->test<->report
       - to quick jump back from report to test use EnvToggleTestSourceJump
       - to quick jump back from test to report use EnvGotoAttachedBuf
    the buffer with a report on running tests is automatically attached to
    the buffer with test-file
    To be able to quickly switch to the results of test runs from the test file.

  - EnvSourceNameOf  -- convert source-file to java-classname or module

  - EnvLineInsert   -- aka code-actions to the current line of buffer(opened file)

  - EnvLineExec     -- Run text from buffer as vim command

  - EnvLine <cmd>   -- Action with current line:
       - apply_escapes  -- Like '\n' into new lines
       - fold           -- Break big current line to several lines aka fold
       - slash          -- convert win to unix (fix backslash)
       - split <how>
               cmd      -- split long-line bash cmd to multilines

  - EnvLinesModify  -- modify selected lines by specic way
        java assert-equals  -- replace assign lines into checking (for tests)

  - EnvGradle daemon stop -- stop all runned gradle daemons

  - EnvTranslate    -- translate selected text or current line

  --
  - EnvHUsePassedIn -- helpers for writing tests
    insert for the current line the passedin value from the diagnostic hint.
    taked from test output
    (now implemented for busted(lua) only)

### Session-like

  - EnvFileNamesSaveOpened - Save names of all opened files

  - EnvFileNamesOpenSaved  - Open last saved filenames


### Configuration

  - EnvSet -- change the plugin config properties on the fly<br>
         props used for DAP and debugging code

  - EnvConfig -- show the current config

### Debugging and Development
  - EnvReload    -- Reload self plugin(all own lua-packages) on the fly
  - EnvLog       -- interact with loggin messages
  - EnvPlugin    -- interact with installed plugins
  - EnvPackages  -- interact with loaded Packages(require)
  - EnvCallFunc  -- quick call the lua-function(inside nvim) under the cursor
                    or os cmmands from md-files in "```sh"-blocks
  - EnvBufInfos  -- Detailed info about current opened file(nvim-buffer)
  - EnvBufDiagnostic -- Research a nvim diagnostics for current buffer
  - EnvCache     -- check how works cache
  - lua require'env'.research() -- run experimental action


# Dependencies

LuaRocks packages (started as simple modules for this project):
- alogger   -- in the past, a built-in module was placed in a separate package
- dprint    -- debug print aka simples logger to stdout or inner table
               fast way to findout errors in self code
- cmd4lua   -- core for writing commands handlers
- oop       -- core for write OOP-code with Classes and Objects based lua tables
- www-auth  -- experimental web authentication (support basic and bearer auth)
               (Optional used in EnvCallFunc to send http-requests from md-file)

```sh
sudo luarocks install cmd4lua alogger dprint oop www-auth
```


# Test Dependencies:

```sh
sudo luarocks install stub
```

Commands:
  Bdelete -- to close buffer and keep nvim alive (moll/vim-bbye)  at (ui.lua)

  env.langs.php.PhpUnit:
  luarocks install xml2lua

  env.lang.DockerCompose:
  apt install libyaml-dev && luarocks install lyaml
  https://github.com/gvvaughan/lyaml

### Usage Example

#### EnvReload
Hotswap lua-code without restart nvim itself

- self -- to reload the "self" - this nvim plugin
- command -- to reload given command from this plugin

Examples:

    :EnvReload command EnvLineInsert

This command Supports multiples commands in one line like this:

    :EnvReload --run [:EnvLog clear :messages clear :DoSomething]

optkey for EnvReload:
  - -c|--clear - actually will execute: ":EnvLog clear" and ":messages clear"
more details see at `:EnvReload help`


#### EnvCallFunc

Create on your source code the lua function for example callMe
```lua
M = {}
function M.callMe()
  print(vim.api.nvim_buf_get_name(vim.api.nvim_get_current_buf()))
end
return M
```
Reload your plugin or nvim or use `:EnvReload` for nvim-env plugin
move cursor to the line with your function and use :EnvCallFunc


#### EnvCallFunc: How to run system commands from md-files(Markdown)

```md
You can run system command line inside the sh or bash syntax block
set cursor to line with command inside syntax block

```sh
cd /etc/ && ls -l
date +%s
```
and run vim cmd:
:EnvCallFunc run      - run sys-command and place output to new buffer in nvim
:EnvCallFunc verbose  - run sys-command in external terminal window

> bash-script development
To run current bash-script with sub-command and args:
put cursor into condition of command-routing

/home/u/dev-tool.sh
```sh
function mk_sections() { ... }
if ...
# to pass args into sub-command
# args: "ab c" d
elif [ "$1" == "mk-sect" -o "$1" == "ssn" ]; then # < place cursor to this line
  # shift + "$@" is a way to avoid the errmsg: "$2: unbound variable"
  shift; mk_sections "$@"
else
  ...
```

And call `:EnvCallFunc` with `run` or `verbose`
this produce shell-command:

```sh
/home/u/dev-tool.sh mk-sect "ab c" d
```
and run it in the new buffer inside nvim (for :EnvCallFunc run) or
in the external x-terminal-emulator (for :EnvCallFunc verbose)

### Send http-request from markdown-inline syntax block
place cursor to begin of the markdown-inline block with http or https syntax
and call `:EnvCallFunc run` or `:EnvCallFunc verbose`
this will send http(s) request and show http-response in the new nvim buffer.

Example:
```http
GET /health HTTP/1.1
Host: localhost
User-Agent: my-custom-ua
Accept: */*
```
`:EnvCallFunc` subcommands:
- `run` - show only body in the new buffer(no http-headers)
- `verbose` - show all: request headers, response headers and body

to send https requests use `https` insted `http` block notation
to send http request through proxy use: `PROXY: http://my-proxy-domain.org:8080`
to send basic-auth use pseudo-headers: `USER+PASSWORD`

Example:
```http
GET /auth-health HTTP/1.1
USER: user
PASSWORD: passwd
PROXY: http://127.0.0.1:80
Host: nginx_status.loc
```
Example2 with localhost & given port
```http
GET /lesson3/hi HTTP/1.1
Host: 127.0.0.1:8080
Accept: */*
```

Example to send POST-request

```http
POST /api/v1/users HTTP/1.1
Host: localhost:8080
Accept: application/json
Content-Type: application/json

{
  "firstName": "Alice",
  "lastName": "White",
  "email": "alice.white@mail.com",
}
```


Dependencies:
 luarocks: luasocket and luasec



#### EnvLog

to view all logger state via table-viewer:

    :EnvLog inspect --view



### EnvLinesModify or EnvLM
modify selected lines by specified way (see `EnvLinesModifyhelp`)

`:EnvLM columns`
create table from selected lines.
Warn by default it used `/t` as columns delemitter.

Examples:

```sh
:EnvLM columns -s " " --wrapper [2 < > 3 `]
```
Input(Selected lines)
```
a b__c d_e
f g__h i_j
x xxxx xxx
```
Result:
```
a  <b__c>  `d_e`
f  <g__h>  `i_j`
x  <xxxx>  `xxx`
```


## :EnvLM shell-command

The main idea here is parsing the saved output of system commands and
performing certain actions.

Goals:
- minify the output of the `tree`-command
- build full path from tree leaf (under the cursor in current buffer),
- provide actions for obtained path: open, file, stat, head, jq, and so on


`:EnvLM shell-command --help`

Modify the Selected Lines in shell output

Usage:
 :EnvLinesModify shell-command <Command>

Commands:
     t tree           - modify output of the tree command(selected lines)
   pft path-from-tree - build full path from the leaf of the tree (and run cmd)

### How its works:

- `:EnvLM sc tree` - just minify the output of the unix `tree` - command
  to save space

- `:EnvLM sc path-from-tree` works with current buffer and position of the cursor
  parse the output of the unit-tree-command from the current buffer and build
  the full path from leaf(under cursor) to the root of this tree.
  And then provides the opportunity to do some action with the received path.
  If the action is not specified, it simply inserts the constructed path directly
  into the current buffer (under the cursor).

```
Usage:
 :EnvLinesModify shell-command path-from-tree [Options]

Options:                          [Default]
    -C, --copy                 -  copy to system clibboard
    -e, --exec       (list)    -  execute given sys command for path
    -f, --file                 -  run sys cmd `file` for this path
    -h, --head       (number)  -  show first n lines
    -j, --jq         (list)    -  show jq output for json file and given filter
    -L, --limit      (number)  -  [128]  limit of the tree lines in buffer
    -b, --new-buffer           -  open output in the new nvim buffer
    -o, --open                 -  open path in the new nvim buffer
    -s, --stat                 -  run sys cmd `stat` for this path
    -t, --tail       (number)  -  show last  n lines

Usage Examples:
  :EnvLM shell-command path-from-tree --exec [ head -n 4 ] -b
  :EnvLM sc pft --jq [ .Name | .[] ] --new-buffer
  :EnvLM sc pft --open
```



## Generate project with flat dir structure for training purposes

When learning a new language, they often start with very simple things such as
solving problems for which one source file + a file with tests for it is enough.
And creating a full-fledged project for each task will be an excessive overhead.
For such cases, it would be more appropriate to simply store all the sources
and tests in one directory, considering it the root of the flat project.
It is for this case that there is support for creating flat projects for
directories in which such files are stored, but there are no config files for
full-fledged projects yet (like pom.xml, build.gradle, packages.json, *.luarocks,
Cargo.toml, mix.exs, and so on).

For these purposes, there is a command `:EnvLang flat-project` that creates
config files for the directory in which the current open source file is located

```vim
:EnvLang flat-project
:EnvProject new
```

Currently, generation has been implemented for the following programming
languages:

- Java
- Elixir


## EnvLsp client

to view details of lsp client you can use:

Show active lsp clients ids
```vim
:EnvLsp client --list
```

Show client infos(in TableViewMode):
```vim
:EnvLsp client -c 1 --view
```
this can be useful:
- if you need to find out lsp settings while it is running (config)
- to see what workspace_folders is used


## Generate new Project for specified Lang

```vim
:EnvProject new <lang> [build-system]
```

> To See debugging information of the Lang/Project for current opened file
```vim
:EnvProject info
```


## See how many resources are consumed by lsp-servers running by nvim
```vim
:EnvOS m -fc nvim
```


## Debugging with alogger level=debug

Hot setting of the debug logging level both for the plugin itself and for
my .dotfiles configuration(NVIM_CONF_DEBUG) via EnvVars:
```sh
ALOGGER_DEBUG=1 NVIM_CONF_DEBUG=1 nvim some_file.ext
```


## EnvDB

Сonvenient interaction with a locally installed database described in a custom
config or standard project config (such as spring-boot for java)

- connect/disconnect
- send (sql) from current sql-syntax block in the markdown file
- supports:
  - custom config(.dbconf),
  - java-projects:
    - database.properties
    - hibernate.properties
    - application.properties (spring-boot)



## env.langs.lua.LuaTester

briefly why is this (Goals):

- supports dependencies definition in lua-test files outside of LuaRockProject
  (without any additional files such us *.rockspec and .luarc.json)
- So that autocompletion works in tests and LSP sees the dependencies used.


To be able to write integration tests in Lua for other programming languages
with support for lua_ls autocompletion, you can either describe the dependencies
used in the test either at the beginning of the file with Lua tests or in
the `test_helper.lua` file.
for example from the luarocks package manager:

```lua
-- @author Me
-- Goals:
--   testing with lua...
--
-- dependencies:
--   - luarocks: busted, alogger, cli-app-base, lhttp, db-env
--

require("busted.runner")()
local assert = require "luassert"

-- ...
```
From this beginning of the file, the names of luarock packages will be
automatically picked up:
- busted
- alogger
- cli-app-base
- lhttp
- db-env

And the paths to these packages installed on the local machine will be
automatically requested and added to lua_ls.
This will allow autocompletion for these dependency modules

- the word `dependencies` is required here on a separate line
- the prefix `luarocks:` is also required before the list of packages used
- package names are separated by spaces, commas are optional

LuaTestet currently supports integration with the following languages:

- Java: in ./src/test/_lua/*

To Update dependencies you can call vim command:
```vim
:EnvProject reload
```


## :EnvGrepCurrentWord

select the word under the cursor and start the search through the GUI
подобрать слово под курсором и запустить поиск через GUI
подберите слово под курсором и выполните grep через UI-плагин для live_grep
pickup the word under the cursor and start the search via UI-plugin for live_grep
(now implemented via telescope)

```vim
:EnvGrepCurrentWord [dir] [word]
```

keybinding example:
```lua
vim.keymap.set("n", "<leader>gr", ':EnvGrepCurrentWord <CR>', opts)
```


## Extra handlers of the :EnvLine insert

- HANDLER_ADD_SAMPLES
 Goal: provide way to pick some text and insert it to current opened buffer
```md
days:lua --> next call :EnvLineInsert via shortcut and type "0"
it will be trigger HANDLER_ADD_SAMPLES (shared for all file extensions) like:
{ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" }
```

> Usage examples:
```html
ipsum::1
Lorem ipsum dolor sit amet, consectetur adipiscing elit,
```

constellations::4
Andromeda
Corvus
Centaurus
Lynx

constellations:lua:4
{ "Andromeda", "Corvus", "Centaurus", "Lynx" }

constellations:json:4
["Andromeda","Corvus","Centaurus","Lynx"]

fruits:inline:8
Ackee Apple Apricot Banana Blueberry Boysenberry Currant Date Durian Fig Gooseberry Kiwi Mango Olive Orange Papaya Pineapple Strawberry Tangerine Watermelon

fruits:inline:4
Ackee Apple Apricot Banana

fruits:inline:4:random
Apple Ackee Banana Apricot

fruits:inline:5-8:random
Kiwi Durian Fig Date

fruits:inline:5-8:random
Durian Date Fig Kiwi

fruits:lua:5-8:random
{ "Fig", "Date", "Kiwi", "Durian" }

vegs:inline:1-4
vegs:inline

vegs:inline:4:r
Carrot Coriander Leave Celery Chinese Potato

vegs:inline:4:r
Pumpkin Arugula String beans Broccoli

help:inline
known: busted ipsum days mvn_dep vegs luahist berries trees late2party lorem
months planets vegetable fruits constellations

constellations:inline:4:random
Canes Venatici Sextans Centaurus Draco
```


## Markdown: Generate HTML code from short html code representation

```markdown
html{head{title'The Title'},body{div{class='A',p'some text',a{href='/','Home'}}}}
```
LineInsert -> `[20] HANDLER_EMIT_HTML`

generated html-code:
```html
<!doctype html>
<html>
  <head>
    <title>The Title</title>
  </head>
  <body>
    <div class="A">
      <p>some text</p>
      <a href="/">Home</a>
    </div>
  </body>
</html>
```
> How its works

Under the hood this works via luarocks `hdgen` package,
and uses lua code to represent html elements.
The names of html tags actually represent ordinary lua functions, and due to
the possibility of shortened calls to lua functions, the syntax is very concise.

```lua
  local hdgen = require 'hdgen'
  local h = hdgen.new({ format = true, tab = '  ' })
  local html, head, body, div, a, p = h.html, h.head, h.body, h.div, h.a, h.p
  -- ... another tags

  local elm = html {
    head = {
      title 'The Title'
    },
    body = {
      div {
        class = "A", -- any key-value pairs will become attributes of the element
        p 'some text',
        a { href='/', 'Home' }
      }
    }
  }
  print(h:render(elm, 0, true))
  --             ^^^ your lua code will be inserted here
```


## EnvJarViewer

JarViewer - is an interactive buffer with keybinding to inspect content of
jar files. Allows to open regular files in jar file and decompile class files
through one selected decompiler from those pre-installed on the system.

In order to open a jar file in JarViewer (a viewer of archive contents in
the form of a tree, with the ability to view regular files and decompile
selected class files:

```vim
:EnvJarViewer open artifact.jar -i path/to/Main.class --decompiler jb_fernflower
```

To open jar file under the cursor in NvimTree buffer(window with FileExplorer)
(no need to specify the path to the jar file - it will be automatically taken
from jar-file-name the current line in NvimTree buffer)
```vim
:EnvJarViewer open -i path/to/Main.class
```


## EnvGradle

- open-source helper for the study of the Gradle source, installed in the
  ~/.gradle/wrapper/dists/gradle-{VER}-all/{HASH}/gradle-{VER}/src
  (Used for EnvGoToSource and for open source file by specified classname)


## EnvCompare
Provides the opportunity to find differences in the given two sets of lines.
For example, in order to take two list's of lines as a set of files and
find the differences in them, which files are on one list but there is no other.


## Usage example of EnvCompare to findout unique lines in two sets(list) of lines

- open buffer with multiple sets of lines to be compared
- jumpt to the first line of the set of lines to pick
- call nvim command `EnvCompare lines-pick`
- jump to first line of the secod set of lines to be compared and repeat the
  same command (to pick second lines to compare)
- to make sure that you definitely have two sets of lines for comparison,
  you can execute the command: `EnvCompare lines-show-selected`
- trigger needed action(comarision) by command `EnvComapare find-unique-lines`
  The result of the execution of this command will be the creation of a
  new buffer in which the differences of two pre -selected sets of lines will
  be given.


## EnvNew -PL java pojo open-api

- ability to generate POJO(java classes for OpenAPI schemas)
- open your java project, then
- `:EnvNew pojo open-api open path/to/openapi.yml`
- `:EnvNew pojo open-api generate #/components/schemas/zones_zones`

this functionality is not implemented yet



